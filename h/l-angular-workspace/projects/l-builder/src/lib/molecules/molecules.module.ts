import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LavashAtomsModule} from '../atoms/atoms.module';

import {LoginComponent} from './login/login.component';
import {UserApi} from './molecules.interfaces';


import {MediaControlComponent} from './media-control/media-control.component';
import {AudioComponent} from './audio/audio.component';
import {VideoComponent} from './video/video.component';
import {FormFieldComponent} from './form-field/form-field.component';
import {ImgSliderComponent} from './img-slider/img-slider.component';
import {ArticleCardComponent} from './article-card/article-card.component';
import {ScrollbarContainerComponent} from './scrollbar-container/scrollbar-container.component';
import {NavigationComponent} from './navigation/navigation.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {PopupComponent} from './popup/popup.component';

@NgModule({
    declarations: [
        LoginComponent,
        FormFieldComponent,
        MediaControlComponent,
        AudioComponent,
        VideoComponent,
        ImgSliderComponent,
        ArticleCardComponent,
        ScrollbarContainerComponent,
        NavigationComponent,
        SidebarComponent,
        PopupComponent
    ],
    imports: [
        CommonModule,
        LavashAtomsModule
    ],
    providers: [
        UserApi
    ],
    exports: [
        LoginComponent,
        FormFieldComponent,
        AudioComponent,
        VideoComponent,
        ImgSliderComponent,
        ArticleCardComponent,
        ScrollbarContainerComponent,
        NavigationComponent,
        SidebarComponent,
        PopupComponent
    ]
})
export class LavashMoleculesModule {
}