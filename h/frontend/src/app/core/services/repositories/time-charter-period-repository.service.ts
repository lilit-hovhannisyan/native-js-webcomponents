import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import {
  ITimeCharterPeriod,
  ITimeCharterPeriodDTO,
  ITimeCharterPeriodCreate,
  TimeCharterPeriodKey,
} from '../../models/resources/ITimeCharterPeriod';
import { TimeCharterKey } from '../../models/resources/ITimeCharter';

export interface ITimeCharterPeriodRepoParams extends RepoParams<ITimeCharterPeriod> {
  queryParams?: {
    timeCharterId: TimeCharterKey;
  };
}

@Injectable({
  providedIn: 'root'
})
export class TimeCharterPeriodRepositoryService extends AbstractRepository<
  ITimeCharterPeriod,
  ITimeCharterPeriodDTO,
  ITimeCharterPeriodCreate,
  ITimeCharterPeriodRepoParams,
  TimeCharterPeriodKey
>  {
  protected resourceType = 'TimeCharterPeriod';
  protected sortKey = 'from';
  protected sortAsce = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/time-charter-periods';

  protected postProcess = (item: ITimeCharterPeriodDTO): ITimeCharterPeriod => item;
}
