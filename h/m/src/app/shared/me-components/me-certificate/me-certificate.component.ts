import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { BaseLoggerComponent } from '@features/tracking-system';
import { MeLabelI } from '../me-label/me-label-inteface';
import { MeCertificateI } from './me-certificate.interface';

@Component({
  selector: 'me-certificate',
  templateUrl: './me-certificate.component.html',
  styleUrls: ['./me-certificate.component.scss']
})
export class MeCertificateComponent extends BaseLoggerComponent implements OnInit {
  componentVersion: string = '0.0.1';
  @Output() eventOccurs: EventEmitter<{ eventName: string; payload?: string }> = new EventEmitter();

  @Input() dataModel!: MeCertificateI;
  statusLabel!: MeLabelI;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.statusLabel = { type: this.dataModel.active ? 'regular' : 'inversed', color: 'success', margin: 'none' };
  }

  customEvent(eventName: string, payload?: string): void {
    this.eventOccurs.emit({ eventName, payload });
  }
}
