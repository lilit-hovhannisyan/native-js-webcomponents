import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
  manualHandledPaths: string[] = ['/auth/password-reset-token', '/events/batch', '/server/info'];

  constructor(private basicAlertService: MeBasicAlertService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (this.isNotManualHandled(request.url)) {
      return next.handle(request).pipe(
        catchError((error: ErrorResponseI) => {
          this.basicAlertService.openBasicAlert({
            mode: 'error',
            title: error.error.title,
            content: error.error.details
          });
          return throwError(error);
        })
      );
    }
    return next.handle(request);
  }

  private isNotManualHandled(path: string): boolean {
    return this.manualHandledPaths.every((elem) => path.indexOf(elem) === -1);
  }
}
