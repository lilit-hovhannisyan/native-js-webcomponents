import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectorRef } from '@angular/core';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { wording } from 'src/app/core/constants/wording/wording';
import {
  gridConfig
} from 'src/app/shared/components/autocomplete/modals/account-grid-config';
import { AbstractControl, FormGroup } from '@angular/forms';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { MandatorAccountRoleRepositoryService } from 'src/app/core/services/repositories/mandator-account-role.repository.service';
import { IMandatorAccount } from 'src/app/core/models/resources/IMandatorAccount';
import { MandatorKey } from 'src/app/core/models/resources/IMandator';
import { IWording } from 'src/app/core/models/resources/IWording';
import { forkJoin, Observable } from 'rxjs';
import { BookingAccountKey, IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { MandatorAccountMainService } from 'src/app/core/services/mainServices/mandator-account-main.service';

@Component({
  selector: 'nv-booking-account-autocomplete',
  templateUrl: './booking-account-autocomplete.component.html',
  styleUrls: ['./booking-account-autocomplete.component.scss']
})
export class BookingAccountAutocompleteComponent implements OnInit {
  public wording = wording;
  public gridConfig = gridConfig;

  @Input() public form: FormGroup;
  @Input() public controlName = 'bookingAccountId';
  @Input() public filters: object;
  @Input() public onlyOfMandator: boolean;
  @Input() public mandatorControlName = 'mandatorId';
  @Input() public showLabel = true;
  @Input() public mandatorId: MandatorKey;
  @Input() public label: IWording;
  @Input() public resource: Observable<IBookingAccount[]>;
  @Input() public relationKey = 'bookingAccountId';

  @Input() public setId = true;
  @Input() public displayOneProperty: boolean;
  @Input() public openListOnInit: boolean;
  @Input() public autofocus: boolean;

  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();
  @Output() public valueChange: EventEmitter<any> = new EventEmitter();

  public rolesFilter: { checkRole: (mandatorAccount: IMandatorAccount) => boolean };
  public defaultValue: BookingAccountKey;

  constructor(
    public bookingAccountRepo: BookingAccountRepositoryService,
    public mandatorBookingAccountRepo: MandatorAccountRepositoryService,
    public mandatorAccountRoleRepo: MandatorAccountRoleRepositoryService,
    private mandatorAccountMainService: MandatorAccountMainService,
    private cdr: ChangeDetectorRef,
  ) {
  }

  public ngOnInit(): void {
    this.init();
  }

  public init(): void {
    forkJoin([
      this.bookingAccountRepo.fetchAll({}),
      this.mandatorBookingAccountRepo.fetchAll({}),
      this.mandatorAccountRoleRepo.fetchAll({}),
    ]).subscribe(() => {
      this.defaultValue = this.form.get(this.controlName).value;
      this.resource = this.resource || this.bookingAccountRepo.getStream();
      this.initRolesFilterObject();
      this.onMandatorChange();
      this.cdr.markForCheck();
    });
  }

  public onMandatorChange(): void {
    if (!this.onlyOfMandator) {
      return;
    }

    this.form.get(this.mandatorControlName).valueChanges.subscribe(value => {
      const currentControl: AbstractControl = this.form.get(this.controlName);
      const selectedAccountNo: number = currentControl.value;
      const selectedAccount: IBookingAccount = this.bookingAccountRepo
        .getStreamValue()
        .find(ba => ba.no === selectedAccountNo);
      const mandatorAccounts: IMandatorAccount[] = this.mandatorBookingAccountRepo
        .getStreamValue();
      const mandatorAccount: IMandatorAccount = mandatorAccounts
        .find(ma => ma.mandatorId === value
          && ma.bookingAccountId === selectedAccount?.id);

      if (mandatorAccount && !this.hasAccessToMandatorAccount(mandatorAccount)) {
        currentControl.reset();
      }
    });
  }

  private initRolesFilterObject(): void {
    this.rolesFilter = {
      checkRole: (mandatorAccount: IMandatorAccount) => {
        return this.hasAccessToMandatorAccount(mandatorAccount);
      }
    };
  }

  private hasAccessToMandatorAccount = (mandatorAccount: IMandatorAccount): boolean => {
    if (mandatorAccount.needsAccountPermission) {
      if (mandatorAccount[this.relationKey] === this.defaultValue) {
        return true;
      }
    }
    return this.mandatorAccountMainService.hasAccessToMandatorAccount(mandatorAccount);
  }
}
