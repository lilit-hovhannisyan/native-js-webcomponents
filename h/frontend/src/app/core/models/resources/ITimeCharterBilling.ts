import { IWithActive } from './IWithActive';
import { IResource } from './IResource';

export type TimeCharterBillingKey = number;

interface ISharedProps extends IWithActive {
  from: string;
  until: string;
  currencyId: number;
  timeCharterId: number;
  billingCycle: number;
  billingCycleType: number;
  isContinually: boolean;
  processing: number;
  dueOnDayOfMonth: number;
  daysBeforeDue: number;
  invoicesInAdvance: number;
  isHire: boolean;
  isHireOff: boolean;
  isLumpsumExpenses: boolean;
  isAtCostExpenses: boolean;
  isCommission: boolean;
}

export interface ITimeCharterBillingBase extends ISharedProps, IResource<TimeCharterBillingKey> { }

export interface ITimeCharterBillingCreate extends ISharedProps { }

export interface ITimeCharterBillingDTO extends ITimeCharterBillingBase { }

export interface ITimeCharterBilling extends ITimeCharterBillingDTO { }
