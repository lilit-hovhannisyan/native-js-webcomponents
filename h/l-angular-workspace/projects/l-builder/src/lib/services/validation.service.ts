import {Injectable} from '@angular/core';

Injectable()
export class ValidationService {
    // succes case if returns true
    validationRules = {
        required: (value, ruleValue) => {
            return value && Boolean(value) === ruleValue;
        },

        regExp: (value, ruleValue) => {
            const pattern = new RegExp(ruleValue, 'g');
            return value && pattern.test(value);
        },

        min: (value, ruleValue) => {
            return value && value.length >= ruleValue;
        },

        max: (value, ruleValue) => {
            return value && value.length <= ruleValue;
        },

        validateDate: (date, format, spliter = '/') => {
            date = new Date(date);
        },

        /*validateAgainstToday: (value, ruleValue) => {
            const today = new Date();
            return compareDates(today, value);
        },

        olderDateThan: (value, ruleValue, formFieldsValueObject) => {
            const date1 = value;
            const date2 = evaluate(formFieldsValueObject, ruleValue);
            if (!date2 || !date1) {
                return;
            }
            return compareDates(date1, date2);
        },

        newerDateThan: (value, ruleValue, formFieldsValueObject) => {
            const date1 = value;
            const date2 = evaluate(formFieldsValueObject, ruleValue);
            if (!date2 || !date1) {
                return;
            }
            return compareDates(date2, date1);
        },*/

        fileFormatImage: (fileName) => {
            const ruleValue = 'gif|jpe?g|bmp|png';
            return fileName && this.validationRules.regExp(fileName, ruleValue);
        },

        fileFormatVideo: (fileName) => {
            const ruleValue = 'mp4|mov';
            return fileName && this.validationRules.regExp(fileName, ruleValue);
        }
    };

    defaultErrorMsgMap = {
        required: 'error.fieldIsRequired',

        // regExp: 'error.fieldDoesNotMatchToRegExp',
        regExp: 'Field doesn\'t match the pattern' ,

        // validateAgainstToday: 'error.validateAgainstToday',
        validateAgainstToday: 'error.validateAgainstToday',

        // olderDateThan: 'error.olderDateThan',
        olderDateThan: 'error.olderDateThan',

        // newerDateThan: 'error.newerDateThan'
        newerDateThan: 'error.newerDateThan'
    };

    defaultValidationpatterns = {
        email: {
            rule: '^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|(".+"))' +
            '@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$',
            customErrMsg: 'Email filed is not correct'
        },
        phone: {
            rule: '^(\\+\\d{1,2,3}\\s)?\\(?\\d{2,3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{3}$',
            customErrMsg: 'Phone filed is not correct: e.g +374 99 555 666'
        },
        url: {
            rule: '^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]' +
            '{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$',
            customErrMsg: 'Url filed is not correct'
        },
    };
}
