export default interface ClickLogI {
  eventType: string;
  componentsData: Array<{
    componentName: string;
    version: string;
    props: Array<{ key: string; value: unknown }>;
  }>;
  clientData: {
    screenResolution: string;
    userAgent: string;
    cookieEnabled: boolean;
  };
  eventData: {
    browserSize: {
      width: string;
      height: string;
    };
    path: string;
    innerText: string;
    cssClasses: string;
    clickCoordinates: {
      clientX: string;
      clientY: string;
    };
  };
  userActionId: string | null;
}
