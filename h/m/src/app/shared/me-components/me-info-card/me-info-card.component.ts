import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnDestroy, OnInit, Output } from '@angular/core';

import { BaseLoggerComponent } from '@features/tracking-system';

import { MeInfoCardI } from './me-info-card.interface';

@Component({
  selector: 'me-info-card',
  templateUrl: './me-info-card.component.html',
  styleUrls: ['./me-info-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeInfoCardComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  componentVersion: string = '0.0.1';
  @Input() dataModel!: MeInfoCardI;
  @Output() eventOccurs: EventEmitter<{ eventName: string; payload: string }> = new EventEmitter();

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {}

  customEvent(eventName: string, payload: string): void {
    this.eventOccurs.emit({ eventName, payload });
  }

  ngOnDestroy(): object {
    return super.ngOnDestroy();
  }
}
