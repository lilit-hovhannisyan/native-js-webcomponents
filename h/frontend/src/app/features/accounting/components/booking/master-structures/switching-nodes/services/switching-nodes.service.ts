import { Injectable, Type } from '@angular/core';
import { IMasterStructureNode, MASTER_STRUCTURE_NODE_TYPE, IMasterStructureNodeTemplate } from 'src/app/core/models/resources/IMasterStructureNode';
import { FormGroup } from '@angular/forms';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { Validators } from 'src/app/core/classes/validators';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { Subscription, ReplaySubject } from 'rxjs';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { SwitchingNodesComponent } from '../switching-nodes.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { NzTreeNode } from 'ng-zorro-antd/tree';
import { takeUntil } from 'rxjs/operators';
import { Language } from 'src/app/core/models/language';
import { MasterStructureNodeHelperService } from '../../master-structure-nodes/services/master-structure-node-helper.service';

@Injectable({
  providedIn: 'root'
})
export class SwitchingNodesService {

  private form: FormGroup;
  private switchingExternalCodeSubscription: Subscription;
  private hasSwitchingNodeSubscription: Subscription;
  private availableSwitchingNodes: IMasterStructureNode[] = [];
  private language: Language;

  constructor(
    private nodesHelperService: MasterStructureNodeHelperService,
    private confirmationService: ConfirmationService,
    private modalService: NzModalService,
  ) { }

  public initForm(form: FormGroup): void {
    this.form = form;
  }

  public setLanguage(language: Language): void {
    this.language = language;
  }

  public getAvailableSwitchingNodes(
    nodes: IMasterStructureNode[] | IMasterStructureNodeTemplate[],
    selectedNode?: IMasterStructureNodeTemplate,
  ): IMasterStructureNodeTemplate[] {
    if (!selectedNode) {
      return [];
    }

    const switchingNodes: IMasterStructureNodeTemplate[] = [];
    let firstLevelParent: IMasterStructureNodeTemplate;

    this.nodesHelperService.mapNodes(
      nodes,
      (
        node: IMasterStructureNodeTemplate,
        i: number,
        parent: IMasterStructureNodeTemplate,
        firstParent: IMasterStructureNodeTemplate,
      ) => {
      if (node.key === selectedNode.id) {
        firstLevelParent = firstParent || selectedNode;
      }
    });

    this.nodesHelperService.mapNodes(nodes, (node: IMasterStructureNodeTemplate) => {
      if (node.key === firstLevelParent.key) {
        return true;
      }

      if (
        (!node.switchingNodeId || node.switchingNodeId === selectedNode?.key)
        && node.level !== 1
        && !node.bookingAccountId
        && !node.hasResultCurrentYear
        && !node.hasResultPreviousYear
      ) {
        switchingNodes.push(node);
      }
    });

    return switchingNodes;
  }

  public isSwitchingNodeChangeValid(foundNode: IMasterStructureNode): boolean {
    const { hasSwitchingNode, switchingNodeId } = this.form.controls;

    if (hasSwitchingNode.value && foundNode.switchingNodeId !== switchingNodeId.value) {
      let hasSwitchingAccount = false;

      this.nodesHelperService.mapNodes(foundNode, (node: IMasterStructureNode) => {
        if (node.nodeType === MASTER_STRUCTURE_NODE_TYPE.SWITCHING_ACCOUNT) {
          hasSwitchingAccount = true;
          return true;
        }
      });

      if (hasSwitchingAccount) {
        this.confirmationService.warning({
          title: null,
          wording: wording.accounting.switchingNodeCanNotBeChanged,
          okButtonWording: wording.general.ok,
          cancelButtonWording: null,
        }).subscribe();
        return false;
      }
    }
    return true;
  }

  public setSwitchingNodeNameValidators(): void {
    const { hasSwitchingNode, switchingNodeName } = this.form.controls;
    if (hasSwitchingNode.value) {
      switchingNodeName
        .setValidators([Validators.required, Validators.maxLength(120)]);
    } else {
      switchingNodeName.setValidators([]);
    }
  }

  public listenSwitchingNodeChanges(
    nodes: IMasterStructureNodeTemplate[],
    isCreationMode: boolean,
    componentDestroyed$: ReplaySubject<boolean>,
  ): void {
    const { hasSwitchingNode } = this.form.controls;

    if (this.hasSwitchingNodeSubscription) {
      return;
    }

    this.hasSwitchingNodeSubscription = hasSwitchingNode.valueChanges.pipe(
      takeUntil(componentDestroyed$)
    ).subscribe(() => {
      this.setSwitchingNodeNameValidators();
      if (!isCreationMode) {
        this.showWarningMessage(nodes);
      }
    });
  }

  public showWarningMessage(nodes: IMasterStructureNodeTemplate[]): void {
    const {
      hasSwitchingNode,
      switchingNodeId,
    } = this.form.controls;

    let switchingNode: IMasterStructureNodeTemplate;
    const label: string = getLabelKeyByLanguage(this.language);

    this.nodesHelperService.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        if (node.key === switchingNodeId.value) {
          switchingNode = node;
        }
      }
    );

    if (
      !hasSwitchingNode.value
      && switchingNodeId.value
      && switchingNode.switchingNodeId
    ) {
      this.confirmationService.confirm({
        title: null,
        wording: setTranslationSubjects(
          wording.accounting.theNodeIsAlreadyLinked,
          { SUBJECT: switchingNode[label] }
        ),
        okButtonWording: wording.general.ok,
        cancelButtonWording: wording.general.cancel,
      }).subscribe(isConfirmed => {
        if (isConfirmed) {
          this.resetSwitchingNodeControls();
        } else {
          hasSwitchingNode.setValue(true, { emitEvent: false });
        }
      });
    } else if (!hasSwitchingNode.value) {
      this.resetSwitchingNodeControls();
    }
  }

  public resetSwitchingNodeControls(): void {
    const {
      switchingNodeId,
      switchingNodeName,
      switchingNodeExternalCode,
    } = this.form.controls;

    switchingNodeId.setValue(null, { emitEvent: false });

    switchingNodeName.setValue(null, { emitEvent: false });
    switchingNodeExternalCode.setValue(null, { emitEvent: false });
  }

  public applyChangesToSwitchingNode(
    foundNode: IMasterStructureNodeTemplate,
    nodes: IMasterStructureNodeTemplate[],
    switchingNode?: IMasterStructureNodeTemplate,
  ): void {
    const { hasSwitchingNode } = this.form.controls;

    if (hasSwitchingNode.value) {
      this.ifCheckedApplyChangesToSwitchingNode(nodes, foundNode, switchingNode);
    } else {
      this.ifUncheckedApplyChangesToSwitchingNode(nodes, foundNode, switchingNode);
    }
  }

  private ifCheckedApplyChangesToSwitchingNode(
    nodes: IMasterStructureNodeTemplate[],
    foundNode: IMasterStructureNodeTemplate,
    switchingNode?: IMasterStructureNodeTemplate,
  ): void {
    const { switchingNodeId, switchingNodeExternalCode, } = this.form.controls;
    this.nodesHelperService.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        if (node.key === switchingNodeId.value) {
          node.hasSwitchingNode = true;
          node.switchingNodeId = foundNode.id;
          node.externalSystemCode = switchingNodeExternalCode.value;
        }

        if (node.key === switchingNode?.id) {
          if (switchingNodeId.value !== switchingNode?.id) {
            node.hasSwitchingNode = false;
            node.switchingNodeId = null;
          }
        }
      }
    );
  }

  private ifUncheckedApplyChangesToSwitchingNode(
    nodes: IMasterStructureNodeTemplate[],
    foundNode: IMasterStructureNodeTemplate,
    switchingNode?: IMasterStructureNodeTemplate,
  ): void {
    const { switchingNodeId, } = this.form.controls;
    this.nodesHelperService.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        if (node.key === switchingNodeId.value) {
          node.hasSwitchingNode = false;
          node.switchingNodeId = null;
        }

        if (node.key === foundNode.id) {
          node.hasSwitchingNode = false;
          node.switchingNodeId = null;
        }

        if (node.key === switchingNode?.id) {
          node.hasSwitchingNode = false;
          node.switchingNodeId = null;
        }
      }
    );
  }

  public defineHasSwitchingNodeCheckboxStatus(
    nodes: IMasterStructureNodeTemplate[],
    selectedNode: NzTreeNode,
  ): void {
    const { hasSwitchingNode } = this.form.controls;
    this.availableSwitchingNodes = this.getAvailableSwitchingNodes(
      nodes,
      selectedNode?.origin as any as IMasterStructureNodeTemplate,
    );

    if (nodes.length <= 1 && !hasSwitchingNode.value) {
      hasSwitchingNode.disable();
    } else {
      hasSwitchingNode.enable();
    }
  }

  public getFilteredSwitchingNodes(): string[] {
    const label: string = getLabelKeyByLanguage(this.language);
    return this.availableSwitchingNodes.map(n => n[label]);
  }

  public unsubscribeSwitchingExternalCode(): void {
    // if user doesn't confirm external code change, subscription stays, if we do not
    // unsubscribe from it we will create another one after and will have multiple
    // subscriptions
    if (this.switchingExternalCodeSubscription) {
      this.switchingExternalCodeSubscription.unsubscribe();
    }
  }

  public searchAndGetSwitchingNode(
    foundNode: IMasterStructureNodeTemplate,
    nodes: IMasterStructureNodeTemplate[],
  ): IMasterStructureNodeTemplate | undefined {
    const { hasSwitchingNode, switchingNodeId, } = this.form.controls;

    if (!hasSwitchingNode.value) {
      return;
    }

    let switchingNode: IMasterStructureNodeTemplate;
    let foundFirstParent: IMasterStructureNodeTemplate;
    let foundNodeParent: IMasterStructureNodeTemplate;

    this.nodesHelperService.mapNodes(
      nodes,
      ( node: IMasterStructureNodeTemplate,
        i: number,
        parent: IMasterStructureNodeTemplate,
        firstParent: IMasterStructureNodeTemplate,
      ): void => {
        if (
          node.key === (foundNode.switchingNodeId || foundNode.origin?.switchingNodeId)
        ) {
          switchingNode = node;
        }

        if (node.key === foundNode.key) {
          foundFirstParent = firstParent;
          foundNodeParent = parent;
        }
    });

    if (!switchingNode) {
      const switchingNodeFirstParent: IMasterStructureNodeTemplate = nodes
        .find(n => n.key !== foundFirstParent.key);
      switchingNode = this.createSwitchingNode(switchingNodeFirstParent, foundNodeParent);
      switchingNodeFirstParent.childNodes.push(switchingNode);
      switchingNodeId.setValue(switchingNode.id);
    }

    return switchingNode;
  }

  public getSwitchingNode(
    nodes: IMasterStructureNodeTemplate[],
    targetNode: IMasterStructureNode,
  ): IMasterStructureNode {
    let switchingNode: IMasterStructureNode;
    this.nodesHelperService.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        if (node.key === targetNode.switchingNodeId) {
          switchingNode = node;
        }
      }
    );

    return switchingNode;
  }

  public getSwitchingNodesHashTable(
    foundNode: IMasterStructureNode,
  ): { [key: string]: IMasterStructureNodeTemplate } {
    const switchingNodesInChildNodes: {[key: string]: IMasterStructureNodeTemplate} = {};

    this.nodesHelperService.mapNodes(
      foundNode,
      (node: IMasterStructureNodeTemplate) => {
        if (node.switchingNodeId) {
          switchingNodesInChildNodes[node.id] = node;
        }
      }
    );

    return switchingNodesInChildNodes;
  }

  private createSwitchingNode(
    switchingNodeFirstParent: IMasterStructureNode,
    foundNodeParent: IMasterStructureNodeTemplate,
  ): IMasterStructureNodeTemplate {
    const { switchingNodeName, switchingNodeExternalCode, } = this.form.controls;

    const label: string = getLabelKeyByLanguage(this.language);
    const newNode: IMasterStructureNodeTemplate = {
      labelEn: '',
      labelDe: '',
      externalSystemCode: switchingNodeExternalCode.value,
      parentNodeId: switchingNodeFirstParent.id,
      nodeType: MASTER_STRUCTURE_NODE_TYPE.NODE,
      level: foundNodeParent.origin
        ? foundNodeParent.origin.level + 1
        : foundNodeParent.level + 1,
    } as IMasterStructureNodeTemplate;

    newNode[label] = switchingNodeName.value;
    return this.nodesHelperService.transformToNodeTemplate(newNode);
  }

  public onSwitchingNameBlur(nodes: IMasterStructureNodeTemplate[]): void {
    const {
      switchingNodeName,
      switchingNodeId,
      switchingNodeExternalCode,
    } = this.form.controls;
    let switchingNode: IMasterStructureNodeTemplate;

    this.nodesHelperService.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        const doesNodeNameMatches: boolean = switchingNodeName.value
          && (node.labelEn === switchingNodeName.value
          || node.labelDe === switchingNodeName.value);

        if (
          doesNodeNameMatches
          && !node.hasSwitchingNode
          && !node.bookingAccountId
          && node.level !== 1
          && !node.hasResultCurrentYear
          && !node.hasResultPreviousYear
        ) {
          switchingNodeId.setValue(node.id);
          switchingNodeExternalCode
            .setValue(node.externalSystemCode, { emitEvent: false });
          switchingNode = node;
          this.confirmExternalCodeChange(switchingNode);
        } else if (
          doesNodeNameMatches
          && switchingNodeName.value
          && node.switchingNodeId !== switchingNodeId.value
        ) {
          this.showUnavailableSwitchingNodeWarning();
          return;
        }
    });
    if (!switchingNode) {
      this.resetSwitchingNode();
    }
  }

  private showUnavailableSwitchingNodeWarning(): void {
    const { switchingNodeName } = this.form.controls;
    this.confirmationService.warning({
      title: null,
      wording: wording.accounting.selectedSwitchingNodeIsUnavailable,
      okButtonWording: wording.general.ok,
      cancelButtonWording: null,
    }).subscribe(() => {
      this.resetSwitchingNode();
      switchingNodeName.setValue(null);
    });
  }

  private resetSwitchingNode(): void {
    const {  switchingNodeId, switchingNodeExternalCode } = this.form.controls;
    switchingNodeId.setValue(null);

    switchingNodeExternalCode.setValue(null, { emitEvent: false });
    this.unsubscribeSwitchingExternalCode();
  }

  public openSwitchingNodesModal(
    nodes: IMasterStructureNodeTemplate[],
    selectedNode: NzTreeNode,
    component: Type<SwitchingNodesComponent>,
  ): void {
    const {
      switchingNodeName,
      switchingNodeExternalCode,
      switchingNodeId,
    } = this.form.controls;

    const modalRef: NzModalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.accounting.selectionSwitchingNodes[this.language],
      nzContent: component,
      nzComponentParams: {
        nodes,
        selectedNode: selectedNode.origin as any as IMasterStructureNodeTemplate,
        selectedNodeControl: switchingNodeId,
      },
      nzClosable: true,
      nzOnOk: (componentInstance: SwitchingNodesComponent): void => {
        const switchingNode: IMasterStructureNodeTemplate = componentInstance
          .getSelectedSwitchingNode();
        const label: string = getLabelKeyByLanguage(this.language);
        switchingNodeName.setValue(switchingNode[label]);
        this.setSwitchingNodeNameValidators();
        switchingNodeExternalCode.setValue(
          switchingNode.externalSystemCode,
          { emitEvent: false }
        );
        switchingNodeId.setValue(switchingNode.id);

        this.confirmExternalCodeChange(switchingNode);
        modalRef.close();
      }
    });
  }

  public onSwitchingNodeSearch(value: string): string[] {
    const label: string = getLabelKeyByLanguage(this.language);
    const lowerCaseValue: string = value.toLocaleLowerCase();

    return this.availableSwitchingNodes
      .filter(n => n.labelEn.toLowerCase().includes(lowerCaseValue)
        || n.labelDe.toLowerCase().includes(lowerCaseValue))
      .map(n => n[label]);
  }

  public getParentSwitchingNode(
    foundNode: IMasterStructureNode,
    nodes: IMasterStructureNodeTemplate[],
  ): IMasterStructureNode {
    let parentSwitchingNode: IMasterStructureNode;
    let parentNode: IMasterStructureNode;

    this.nodesHelperService.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        if (node.id === foundNode.parentNodeId) {
          parentNode = node;
        }
      }
    );

    this.nodesHelperService.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        if (node.key === parentNode.switchingNodeId) {
          parentSwitchingNode = node;
        }
      }
    );

    return parentSwitchingNode;
  }

  public confirmExternalCodeChange(switchingNode: IMasterStructureNodeTemplate): void {
    const { switchingNodeExternalCode } = this.form.controls;
    this.unsubscribeSwitchingExternalCode();
    this.switchingExternalCodeSubscription = switchingNodeExternalCode.valueChanges
      .subscribe(() => {
        this.confirmationService.confirm({
          title: null,
          wording: wording.accounting.changingExternalCodeWillOverwrite,
          okButtonWording: wording.general.ok,
          cancelButtonWording: wording.general.cancel,
        }).subscribe(isConfirmed => {
          if (!isConfirmed) {
            switchingNodeExternalCode.setValue(
              switchingNode?.externalSystemCode,
              { emitEvent: false }
            );
          } else {
            this.unsubscribeSwitchingExternalCode();
          }
        });
      });
  }

  public setSwitchingNodeValuesToForm(switchingNode: IMasterStructureNodeTemplate): void {
    const {
      switchingNodeName,
      switchingNodeExternalCode,
      switchingNodeId,
    } = this.form.controls;
    const label: string = getLabelKeyByLanguage(this.language);

    switchingNodeName.setValue(switchingNode ? switchingNode[label] : '');
    this.setSwitchingNodeNameValidators();
    switchingNodeExternalCode.setValue(
      switchingNode?.externalSystemCode,
      { emitEvent: false }
    );
    switchingNodeId.setValue(switchingNode?.id);
  }
}
