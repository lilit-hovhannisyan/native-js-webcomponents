import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthenticationService } from '../../authentication/services/authentication.service';
import { Observable } from 'rxjs';

@Injectable()
export class LoginGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthenticationService,
  ) {
  }
  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const routerState = this.router.getCurrentNavigation().extras.state;
    const systemAction = routerState && routerState.systemAction;
    if (
      this.authService.isLoggedIn() && !systemAction) {
      this.router.navigate(['/base/main']);
      return false;
    }
    return true;
  }

  public canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivate(route, state);
  }

}
