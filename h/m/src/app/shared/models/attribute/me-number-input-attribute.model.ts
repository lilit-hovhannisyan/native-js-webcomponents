import { FormGroup } from '@angular/forms';
import { MeInputErrorI } from '@shared/me-components/me-attribute/me-input-error/me-input-error.interface';
import { MeNumberInputI } from '@shared/me-components/me-attribute/me-number-input/me-number-input.interface';
import { DMDecimalAttributeModel, DMFloatAttributeModel, DMIntegerAttributeModel, DMLongAttributeModel } from './attribute.model';

export class MeNumberInputAttribute implements MeNumberInputI {
  private _data!: DMDecimalAttributeModel | DMFloatAttributeModel | DMIntegerAttributeModel | DMLongAttributeModel;
  private _form!: FormGroup;
  private _inputErrors!: MeInputErrorI;

  constructor() {}

  public set data(data: DMDecimalAttributeModel | DMFloatAttributeModel | DMIntegerAttributeModel | DMLongAttributeModel) {
    this._data = data;
  }

  public get data(): DMDecimalAttributeModel | DMFloatAttributeModel | DMIntegerAttributeModel | DMLongAttributeModel {
    return this._data;
  }

  public set form(form: FormGroup) {
    this._form = form;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get displayName(): string {
    return this.data.displayName;
  }

  public get internalName(): string {
    return this.data.internalName;
  }

  public get required(): boolean {
    return this.data.required;
  }

  public get optRequired(): boolean {
    return this.data.optRequired;
  }

  public get value(): number {
    return this.data.value;
  }

  public get minValue(): number {
    return this.data.minValue;
  }

  public get maxValue(): number {
    return this.data.maxValue;
  }

  public get disabled(): boolean {
    return this.data.disabled;
  }

  public set inputErrors(inputErrors: MeInputErrorI) {
    this._inputErrors = inputErrors;
  }

  public get inputErrors(): MeInputErrorI {
    return this._inputErrors;
  }
}
