import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NvGridConfig, GridComponent } from 'nv-grid';
import { Observable, of } from 'rxjs';
import { wording } from 'src/app/core/constants/wording/wording';
import { IMasterStructureNodeTemplate, IMasterStructureNode } from 'src/app/core/models/resources/IMasterStructureNode';
import { getSwitchingNodesGridConfig } from './switching-nodes.config';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { AbstractControl } from '@angular/forms';
import { SwitchingNodesService } from './services/switching-nodes.service';
import { MasterStructureNodeHelperService } from '../master-structure-nodes/services/master-structure-node-helper.service';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'nv-switching-nodes',
  templateUrl: './switching-nodes.component.html',
  styleUrls: ['./switching-nodes.component.scss']
})
export class SwitchingNodesComponent implements OnInit {
  @ViewChild('gridComponent') public gridComponent: GridComponent;
  @Input() public nodes: IMasterStructureNodeTemplate[];
  @Input() public selectedNode: IMasterStructureNodeTemplate;
  @Input() public selectedNodeControl: AbstractControl;

  public dataSource$: Observable<IMasterStructureNode[]>;
  public gridConfig: NvGridConfig;
  private selectedSwitchingNode: IMasterStructureNodeTemplate;
  public wording = wording;

  constructor(
    private nodesHelperService: MasterStructureNodeHelperService,
    private modal: NzModalRef,
    public gridConfigService: GridConfigService,
    private switchingNodesService: SwitchingNodesService,
  ) { }

  public ngOnInit(): void {
    this.selectedSwitchingNode = undefined;
    this.gridConfig = getSwitchingNodesGridConfig({ triggerOk: this.triggerOk });
    const nodes: IMasterStructureNodeTemplate[] = this.switchingNodesService
      .getAvailableSwitchingNodes(
        this.nodesHelperService.copyNodes(this.nodes),
        this.selectedNode,
      ).map(n => ({
        ...n,
        _checked: n.id === this.selectedNodeControl.value
      }));
    this.dataSource$ = of(nodes);
  }

  public rowSelect(row: IMasterStructureNodeTemplate): void {
    this.selectedSwitchingNode = row[0];
  }

  public getSelectedSwitchingNode(): IMasterStructureNodeTemplate {
    return this.selectedSwitchingNode;
  }

  public triggerOk = (row?: IMasterStructureNodeTemplate): void => {
    if (row) {
      this.selectedSwitchingNode = row;
    }

    if (this.selectedSwitchingNode) {
      this.modal.triggerOk();
    }
  }
}
