import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { ICostUnitCategory, CostUnitCategoryKey, ICostUnitCategoryCreate, ICostUnitCategoryDTO } from '../../models/resources/ICostUnitCategory';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class CostUnitCategoryRepositoryService extends AbstractRepository<
  ICostUnitCategory,
  ICostUnitCategoryDTO,
  ICostUnitCategoryCreate,
  RepoParams<ICostUnitCategory>,
  CostUnitCategoryKey
> {
  protected resourceType = 'CostUnitCategories';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/cost-unit-categories';
  }

  protected postProcess(item: ICostUnitCategoryDTO): ICostUnitCategory { return item; }

}
