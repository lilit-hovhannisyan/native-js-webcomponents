import { FormGroup } from '@angular/forms';
import { MeMultiSelectInputI } from '@shared/me-components/me-attribute/me-multi-select-input/me-multi-select-input.interface';
import { EnumValueModel } from '../enum/enum-value.model';
import { DMMOAListAttributeModel } from './attribute.model';

export class MeMultiSelectInputAttribute implements MeMultiSelectInputI {
  private _data!: DMMOAListAttributeModel;
  private _form!: FormGroup;

  constructor() {}

  public set data(data: DMMOAListAttributeModel) {
    this._data = data;
  }

  public get data(): DMMOAListAttributeModel {
    return this._data;
  }

  public set form(form: FormGroup) {
    this._form = form;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get displayName(): string {
    return this.data.displayName;
  }

  public get internalName(): string {
    return this.data.internalName;
  }

  public get required(): boolean {
    return this.data.required;
  }

  public get optRequired(): boolean {
    return this.data.optRequired;
  }

  public get value(): string[] {
    return this.data.value;
  }

  public get values(): EnumValueModel[] {
    return this.data.values;
  }

  public get disabled(): boolean {
    return this.data.disabled;
  }
}
