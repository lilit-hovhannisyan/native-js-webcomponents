import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'me-table-date',
  templateUrl: './table-date.component.html',
  styleUrls: ['./table-date.component.scss']
})
export class TableDateComponent implements OnInit {
  @Input() data!: Date;

  constructor() {}

  ngOnInit(): void {}
}
