import { Component, OnInit, Input } from '@angular/core';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/voyages.config';
import { FormControl } from '@angular/forms';
import { IVoyageRow } from 'src/app/core/models/resources/IVoyage';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { MandatorKey } from 'src/app/core/models/resources/IMandator';
import { VoyagesMainService } from 'src/app/core/services/mainServices/voyages-main.service';
import { map } from 'rxjs/operators';
import { AbstractValueAccessor, makeProvider } from 'src/app/core/abstracts/value-accessor';

@Component({
  selector: 'nv-voyage-select-field',
  templateUrl: './voyage-select-field.component.html',
  styleUrls: ['./voyage-select-field.component.scss'],
  providers: [makeProvider(VoyageSelectFieldComponent)],
})
export class VoyageSelectFieldComponent extends AbstractValueAccessor implements OnInit {

  public getGridConfig = getGridConfig;
  public resourceFormControl = new FormControl({ value: null });

  public voyages$: Observable<IVoyageRow[]>;
  @Input() private onlyRelatedToVesselAccounting$: Observable<MandatorKey>;

  public getLabel = (voyage: IVoyageRow) => voyage.yearAndNo;

  constructor(
    private voyagesService: VoyagesMainService,
  ) {
    super();
  }

  public ngOnInit(): void {
    if (this.onlyRelatedToVesselAccounting$) {
      const idBehaviourSubject = new BehaviorSubject(null);
      this.onlyRelatedToVesselAccounting$.subscribe(id => idBehaviourSubject.next(id));
      this.voyages$ = combineLatest([
        idBehaviourSubject,
        this.voyagesService.fetchAll({}),
      ]).pipe(
        map(([vesselAccountingId, voyages]) =>
          this.voyagesService
            .toRows(voyages
              .filter(voyage => voyage.vesselAccountingId === vesselAccountingId))
        )
      );
    }
  }

  public onSelect(value: number): void {
    this.onChange(value);
  }

  public writeValue(value: number): void {
    this.resourceFormControl.setValue(value);
  }

  public setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.resourceFormControl.disable();
    } else {
      this.resourceFormControl.enable();
    }
  }

}
