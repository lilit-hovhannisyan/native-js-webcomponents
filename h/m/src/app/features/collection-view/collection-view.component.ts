import { AfterViewInit, Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BreadcrumbService } from 'xng-breadcrumb';
import { SubSink } from 'subsink';
import { Subject } from 'rxjs';
import { finalize, take } from 'rxjs/operators';

import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { GlobalService } from '@core-services/global.service';
import { RightSidebarSupportService } from '@core-services/right-sidebar-support.service';
import { WindowRef } from '@core-providers/window-ref.provider';

import { BaseLoggerComponent } from '@features/tracking-system';
import { CollectionsWebService } from '@features/collections/collections.web-service';
import { DocumentStoreService } from '@features/documents/documents-store.service';
import { DocumentsWebService } from '@features/documents/documents.web-service';

import { CollectionModel } from '@shared/models/collection/collection.model';
import { MeBasicAlertI } from '@shared/me-components/me-basic-alert/me-basic-alert.interface';
import {
  FORCE_DOWNLOAD,
  MARKETPLACE_BASE_API_URL,
  MATERIAL_ATTRIBUTE_UOM,
  ME_DOMAIN_APPLICATION_COLLECTION,
  ME_DOMAIN_APPLICATION_DOCUMENT,
  ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL_VIDEO,
  ME_DOMAIN_APPLICATION_MATERIAL
} from '@shared/constants';
import { DomainModel } from '@shared/models/domain/domain.model';
import { MaterialModel } from '@shared/models/materials/material-model';
import { MaterialsStoreService } from '@shared/services/materials-store.service';
import { MaterialsWebService } from '@shared/services/materials.web-service';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeDocumentLineI } from '@shared/me-components/me-document-line/me-document-line.interface';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { MeEmptyContentI } from '@shared/me-components/me-empty-content/me-empty-content-interface';
import { MeInfoCardI } from '@shared/me-components/me-info-card/me-info-card.interface';
import { MeMaterialCardI } from '@shared/me-components/me-material-card/me-material-card.interface';
import { MeSearchBarI } from '@shared/me-components/me-search-bar/me-search-bar.interface';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { MePopupStatusChangeI } from '@shared/me-components/me-popup-status-change/me-popup-status-change.interface';
import { MePopupStatusChangeService } from '@shared/me-components/me-popup-status-change/me-popup-status-change.service';
import { MeTabGroupI } from '@shared/me-components/me-tab-group/me-tab-group.interface';
import { QuickSearchModel } from '@shared/models/search/quick-search.model';
import { StateModel } from '@shared/models/state/state.model';
import { MePopupVideoService } from '@shared/me-components/me-popup-video/me-popup-video.service';
import { getFormatedDate, getVisibilityColorForLabel, getVisibilityObject, mapExtensionToIcon } from '@shared/utils';
import { MeSearchDirectionTypes, MeSearchSortByTypes } from '@shared/me-search-sort-by-types';
import { MeAttributeTypes } from '@shared/me-attribute-types';
import { AttributesService } from '@shared/services/attributes.service';
import { AuthStoreService } from '@core-services/auth-store.service';
import { MePermissionTypes } from '@shared/me-permission-types';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-collection-view',
  templateUrl: './collection-view.component.html',
  styleUrls: ['./collection-view.component.scss'],
  providers: [
    MaterialsWebService,
    MaterialsStoreService,
    DocumentStoreService,
    DocumentsWebService,
    CollectionsWebService,
    MePopupStatusChangeService,
    AttributesService
  ]
})
export class CollectionViewComponent extends BaseLoggerComponent implements OnInit, OnDestroy, AfterViewInit {
  componentVersion: string = '0.0.1';

  private subs = new SubSink();

  domain: DomainModel = new DomainModel();
  states: StateModel[] = [];
  collectionOID: string | null = '';
  collection: CollectionModel | null = null;
  isCollectionLoading: boolean = true;
  areMaterialsLoading: boolean = true;
  searchKeyword: string | undefined = '';
  selectionEnabled: boolean = false;
  materialCardItems: MeMaterialCardI[] = [];

  isSearch: boolean = false;
  materialsEmptyContent!: MeEmptyContentI;
  trackForBottomReach: boolean = false;
  searchBarData!: MeSearchBarI;

  private readonly NUMBER_OF_ITEMS_ON_PAGE = 50;
  private _selectedMap: Map<string, void> = new Map();

  i18nStrings: MeTranslationI = {};

  @ViewChild('rightSidebarContent', { read: TemplateRef }) rightSidebarContent!: TemplateRef<Component>;
  @ViewChild('edit', { read: TemplateRef }) edit!: TemplateRef<Component>;
  @ViewChild('info', { read: TemplateRef }) info!: TemplateRef<Component>;

  tabGroupData?: MeTabGroupI;
  @ViewChild('tabOne', { read: TemplateRef }) tabOne!: TemplateRef<Component>;
  @ViewChild('tabTwo', { read: TemplateRef }) tabTwo!: TemplateRef<Component>;

  // Tab one
  collectionInfoCard: MeInfoCardI | null = null;
  // Tab two
  documentsTrackForBottomReach: boolean = false;
  documentIsSearch: boolean = false;
  documentsSearchKeyword: string | undefined = '';
  documentsSearchBarData!: MeSearchBarI;
  areDocumentsLoading: boolean = false;
  documentCards: MeDocumentLineI[] = [];

  isVisibilityEdit: boolean = false;

  constructor(
    injector: Injector,
    private _route: ActivatedRoute,
    private _router: Router,
    private _translate: MeTranslationService,
    private _definitionsStoreService: DefinitionsStoreService,
    private _collectionsWebService: CollectionsWebService,
    private _materialsStoreService: MaterialsStoreService,
    private _materialsWebService: MaterialsWebService,
    private _rightSidebarSupportService: RightSidebarSupportService,
    private _breadcrumbService: BreadcrumbService,
    private _breadcrumbSupportService: BreadcrumbSupportService,
    private _documentStoreService: DocumentStoreService,
    private _documentsWebService: DocumentsWebService,
    private _basicAlertService: MeBasicAlertService,
    private _sweetAlertService: MeSweetAlertService,
    private _popupStatusChangeService: MePopupStatusChangeService,
    private _globalService: GlobalService,
    private _windowRef: WindowRef,
    private _videoPopupService: MePopupVideoService,
    private _attributesService: AttributesService,
    private _authStoreService: AuthStoreService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.subs.sink = this._translate.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;
    });

    this.collectionOID = this._route.snapshot.paramMap.get('id');

    this.subs.sink = this._definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.domain = this._definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_COLLECTION);
        this.states = this.domain.states;
        if (this.collectionOID) {
          this.prepareMaterialCardData();
          this.prepareDocumentCardData();
          this.getCollectionByOID();
        }
      }
    });

    this.searchBarData = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: '',
        minCharacters: 1
      },
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'azAlphabetically', value: this.i18nStrings.azAlphabetically },
          { key: 'zaAlphabetically', value: this.i18nStrings.zaAlphabetically },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      },
      selectButton: {
        enabled: false,
        placeholder: this.i18nStrings.select
      },
      operationButtons: [
        // {
        //   button: { type: 'regular', rounded: false, color: 'link' },
        //   iFeatherName: 'edit',
        //   clickEventName: 'edit',
        //   tooltipText: this.i18nStrings.edit
        // },
        {
          button: { type: 'regular', rounded: false, color: 'link' },
          iFeatherName: 'trash',
          clickEventName: 'remove',
          tooltipText: this.i18nStrings.remove
        }
        // {
        //   button: { type: 'regular', rounded: false, color: 'link' },
        //   iFeatherName: 'copy',
        //   clickEventName: 'copy',
        //   tooltipText: this.i18nStrings.copy
        // },
        // {
        //   button: { type: 'regular', rounded: false, color: 'link' },
        //   iFeatherName: 'more-vertical',
        //   clickEventName: 'more',
        //   tooltipText: this.i18nStrings.moreOptions
        // },
        // {
        //   button: { type: 'regular', rounded: false, color: 'link' },
        //   iFeatherName: 'download',
        //   clickEventName: 'download',
        //   tooltipText: this.i18nStrings.download
        // }
      ]
      // view: 'grid'
    };

    this.materialsEmptyContent = {
      title: this.i18nStrings.addMaterialsToYourCollection,
      comment: this.i18nStrings.startCreatingDigitalVersionsOfYourMaterials,
      buttonText: this.i18nStrings.add,
      emptyIconName: 'empty-materials'
    };

    this.isVisibilityEdit = this._authStoreService.isAllowed(MePermissionTypes.STATE_COLLECTION_CHANGE);
  }

  handleAddButtonClick(): void {
    this._router.navigate(['collections', 'edit', this.collectionOID, { tab: 1 }]);
  }

  ngAfterViewInit(): void {
    this.tabGroupData = {
      tabs: [
        {
          index: 0,
          displayName: this.i18nStrings.overview,
          content: this.tabOne
        },
        {
          index: 1,
          displayName: this.i18nStrings.documents,
          content: this.tabTwo
        }
      ]
    };

    this._breadcrumbSupportService.breadcrumbActions = [
      {
        button: {
          id: '1',
          data: {
            color: 'secondary',
            type: 'regular'
          },
          text: this.i18nStrings.edit,
          content: this.edit
        },
        callBack: () => {
          this._router.navigate(['collections', 'edit', this.collectionOID]);
        }
      },
      {
        button: {
          id: 'info-toggle',
          data: {
            color: 'primary',
            type: 'transparent'
          },
          text: this.i18nStrings.info,
          content: this.info
        },
        callBack: () => {
          this.toggleInfoRightPanel();
        }
      }
    ];

    this.documentsSearchBarData = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: '',
        minCharacters: 1
      },
      operationButtons: []
    };

    this.toggleInfoRightPanel();
  }

  toggleInfoRightPanel(): void {
    if (this._rightSidebarSupportService.rightSidebarContent !== null) {
      this._rightSidebarSupportService.rightSidebarContent = null;
    } else {
      this._rightSidebarSupportService.rightSidebarContent = this.rightSidebarContent;
    }

    const infoButtonAction = this._breadcrumbSupportService.breadcrumbActions.find((a) => a.button.id === 'info-toggle');
    const filteredActionsButtons = this._breadcrumbSupportService.breadcrumbActions.filter((a) => a !== infoButtonAction);
    if (infoButtonAction) {
      this._breadcrumbSupportService.breadcrumbActions = [
        ...filteredActionsButtons,
        {
          ...infoButtonAction,
          button: {
            ...infoButtonAction.button,
            data: {
              color: this._rightSidebarSupportService.rightSidebarContent === null ? 'secondary' : 'primary',
              type: this._rightSidebarSupportService.rightSidebarContent === null ? 'regular' : 'transparent'
            }
          }
        }
      ];
    }
  }

  handleSelectedTabChange(index: number): void {
    if (index === 1) {
      this.getCollectionDocuments();
    }
  }

  getCollectionDocuments(from: number = 0, to: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.areDocumentsLoading = true;
    this.subs.sink = this._collectionsWebService
      .getMeCollectionDocuments(this.collectionOID || '', from, to)
      .pipe(
        finalize(() => {
          this.areDocumentsLoading = false;
        })
      )
      .subscribe((response: ArrayResponseI<MeDocumentModel>) => {
        this._documentStoreService.documentNextId = response.nextID;
        if (append) {
          this._documentStoreService.appendDocumentEntities(response.entities);
        } else {
          this._documentStoreService.documentEntities = response.entities;
          this.documentIsSearch = false;
          this.documentsTrackForBottomReach = true;
        }
        // If reached end of pagination unsubscribe from scroll
        if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
          this.documentsTrackForBottomReach = false;
        }
      });
  }

  getCollectionByOID(): void {
    this.isCollectionLoading = true;
    this._globalService.activateLoader();
    this.subs.sink = this._collectionsWebService
      .getMeCollectionByOid(this.collectionOID as string)
      .pipe(
        finalize(() => {
          this.isCollectionLoading = false;
          this._globalService.deactivateLoader();
        })
      )
      .subscribe((response: CollectionModel) => {
        this.collection = response;

        // Set breadcrumb path - collection name instead of collection id
        this._breadcrumbService.set('@collectionName', this.collection.attributes.name);
        this.prepareInfoCard();
        this.getMaterialsInCollection();
      });
  }

  getMaterialsInCollection(from: number = 0, to: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.areMaterialsLoading = true;
    this.subs.sink = this._collectionsWebService
      .getMeCollectionMaterials(this.collectionOID as string, from, to)
      .pipe(
        finalize(() => {
          this.areMaterialsLoading = false;
        })
      )
      .subscribe((response: ArrayResponseI<MaterialModel>) => {
        this.handleMaterialsResponse(response, append, false);
      });
  }

  private handleMaterialsResponse(response: ArrayResponseI<MaterialModel>, append: boolean = false, isSearch: boolean = false): void {
    this._materialsStoreService.materialNextId = response.nextID;
    if (append) {
      this._materialsStoreService.appendMaterialEntities(response.entities);
    } else {
      this._materialsStoreService.materialEntities = response.entities;
      this.isSearch = isSearch;
      this.trackForBottomReach = true;
      this._selectedMap.clear();
    }
    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.trackForBottomReach = false;
    }
  }

  prepareMaterialCardData(): void {
    this.subs.sink = this._materialsStoreService.materialEntities$.subscribe((data: MaterialModel[] | null) => {
      this.materialCardItems = [];
      if (data) {
        const materialStates: StateModel[] = this._definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_MATERIAL).states;
        const domain: DomainModel = this._definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_MATERIAL);
        this.materialCardItems = data.map((material: MaterialModel) => {
          let unitOfMeasure: string = '';
          if (material.attributes && material.attributes.hasOwnProperty(MATERIAL_ATTRIBUTE_UOM)) {
            unitOfMeasure = this._attributesService.getEnumDisplayNames(
              {
                dataTypeClass: MeAttributeTypes.CHOICE,
                displayName: '',
                internalName: MATERIAL_ATTRIBUTE_UOM,
                value: material.attributes.unitOfMeasure,
                convertedValue: ''
              },
              domain
            );
          }
          return {
            contextMenuButton: { type: 'regular', color: 'link', rounded: true },
            contextMenuItems: [
              {
                key: 'view',
                value: this.i18nStrings.view
              },
              {
                key: 'edit',
                value: this.i18nStrings.edit
              },
              {
                key: 'remove',
                value: this.i18nStrings.remove
              }
            ],
            materialName: material.attributes.name,
            materialPrice: material.attributes.price,
            materialUOM: unitOfMeasure || material.attributes.unitOfMeasure,
            oid: material.oid,
            selected: this._selectedMap.has(material.oid),
            selectionEnabled: this.selectionEnabled,
            supplierName: material.attributesExtended.owner,
            supplierOid: material.ownerOID,
            thumbnail: material.attributes.thumbnail ? `${MARKETPLACE_BASE_API_URL}/files/download/${material.attributes.thumbnail}` : '',
            visibility: getVisibilityObject(material.state, materialStates.find((ms) => ms.internalName === material.state)?.stateType)
          };
        });
      }
    });
  }

  prepareDocumentCardData(): void {
    this.subs.sink = this._documentStoreService.documentEntities$.subscribe((data: MeDocumentModel[]) => {
      this.documentCards = data.map((document: MeDocumentModel) => {
        return {
          id: document.oid,
          oid: document.oid,
          selectionEnabled: false,
          selected: false,
          documentName: document.attributes.title,
          creationDate: getFormatedDate(document.createdOn),
          contextMenu: {
            contextMenuItems: [
              {
                key: 'edit',
                value: this.i18nStrings.edit
              },
              {
                key: 'view',
                value: this.i18nStrings.view
              },
              {
                key: 'download',
                value: this.i18nStrings.download
              },
              {
                key: 'remove',
                value: this.i18nStrings.remove
              }
            ]
          },
          documentThumbnail: document.attributes.thumbnail
            ? `${MARKETPLACE_BASE_API_URL}/files/download/${document.attributes.thumbnail}`
            : undefined,
          documentExtensionIcon: mapExtensionToIcon(document.attributesExtended.extension)
        };
      });
    });
  }

  prepareInfoCard(): void {
    if (this.collection) {
      const collectionStates: StateModel[] = this._definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_COLLECTION).states;
      const stateModel: StateModel | undefined = collectionStates.find((cs) => cs.internalName === this.collection?.state);
      this.collectionInfoCard = {
        thumbnail: `${MARKETPLACE_BASE_API_URL}/files/download/${this.collection.attributes.thumbnail}`,
        title: this.collection.attributes.name,
        subtitle: `${this.i18nStrings.createdBy} ${this.collection.attributesExtended.creator}`,
        contextMenu: {
          contextMenuItems: [
            {
              key: 'edit',
              value: this.i18nStrings.edit
            },
            {
              key: 'duplicate',
              value: this.i18nStrings.duplicate
            }
          ]
        },
        description: stateModel?.displayName || this.collection.state,
        label: {
          color: getVisibilityColorForLabel(stateModel?.stateType || ''),
          type: 'regular',
          margin: 'none'
        }
      };

      if (this.isVisibilityEdit) {
        this.collectionInfoCard.contextMenu.contextMenuItems.push({
          key: 'visibility',
          value: this.i18nStrings.visibility
        });
      }
    }
  }

  toggleCardItemSelection(index: number): void {
    if (this._selectedMap.has(this.materialCardItems[index].oid)) {
      this.materialCardItems[index] = { ...this.materialCardItems[index], selected: false };
      this._selectedMap.delete(this.materialCardItems[index].oid);
    } else {
      this.materialCardItems[index] = { ...this.materialCardItems[index], selected: true };
      this._selectedMap.set(this.materialCardItems[index].oid);
    }
  }

  customEvent(eventName: string, payload?: string, item?: MeMaterialCardI | MeDocumentLineI | CollectionModel): void {
    if (eventName === 'contextMenuItemClick') {
      switch (payload) {
        case 'view':
          this._router.navigate(['materials', 'view', item?.oid]);
          break;
        case 'edit':
          this._router.navigate(['materials', 'edit', item?.oid]);
          break;
        // case 'share':
        //   break;
        // case 'duplicate':
        //   break;
        // case 'addToCollection':
        //   break;
        // case 'changeStatus':
        //   break;
        case 'remove':
          if (item) {
            this.removeMaterialsFromCollection([item.oid]);
          }
          break;
        // case 'report':
        //   break;
        default:
          break;
      }
    } else if (eventName === 'documentContextMenuItemClick') {
      switch (payload) {
        case 'download':
          const file = this._documentStoreService.documentEntities.find((d) => d.oid === item?.oid);
          if (file && file.attributes.primaryContent) {
            const FILE_URL: string = `${MARKETPLACE_BASE_API_URL}/files/download/${file.attributes.primaryContent}${FORCE_DOWNLOAD}`;
            this._windowRef.openInNewWindow(FILE_URL);
          } else {
            this._basicAlertService.openBasicAlert({
              mode: 'error',
              content: this.i18nStrings.noFile,
              title: this.i18nStrings.emptyDocument
            });
          }
          break;
        case 'view':
          const fileToView = this._documentStoreService.documentEntities.find((d) => d.oid === item?.oid);
          if (fileToView && fileToView.attributes.primaryContent) {
            const FILE_URL: string = `${MARKETPLACE_BASE_API_URL}/files/download/${fileToView.attributes.primaryContent}`;
            if (item?.oid.includes(ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL_VIDEO)) {
              this._videoPopupService.openVideoPopupAlert({ videoPath: FILE_URL });
            } else {
              this._windowRef.openInNewWindow(FILE_URL);
            }
          } else {
            this._basicAlertService.openBasicAlert({
              mode: 'error',
              content: this.i18nStrings.noFile,
              title: this.i18nStrings.emptyDocument
            });
          }
          break;
        case 'edit':
          this._router.navigate(['documents', 'edit', item?.oid]);
          break;
        case 'remove':
          if (item?.oid && this.collectionOID) {
            this.handleDocumentsRemoval(this.collectionOID, [item.oid]);
          }
          break;
        default:
          break;
      }
    } else if (eventName === 'infoCardContextMenuClick') {
      switch (payload) {
        case 'edit':
          this._router.navigate(['collections', 'edit', item?.oid]);
          break;
        case 'duplicate':
          this._router.navigate(['collections', 'duplicate', this.collection?.oid]);
          break;
        case 'visibility':
          // Creating of popup data
          const popup: MePopupStatusChangeI = {
            title: this.i18nStrings.setCollectionVisibility,
            cancelButtonPlaceholder: this.i18nStrings.cancel,
            changeButtonPlaceholder: this.i18nStrings.saveChanges
          };
          // open popup
          const popUpSubs: Subject<{ eventName: string; payload?: string }> = this._popupStatusChangeService.openDialog(popup);

          // get States and add data to popup
          if (this.collection) {
            this.subs.sink = this._collectionsWebService.getCollectionStates(this.collection.oid).subscribe((states) => {
              const newPopup = {
                ...popup,
                currentKeyValue: this.collection ? this.collection.state : '',
                radioButtons: states.entities.map((state) => {
                  const displName = this.domain.states.find((s) => s.internalName === state.internalName);
                  return {
                    key: state.internalName,
                    value: displName ? `${displName.displayName} - ${displName.description}` : state.internalName,
                    disabled: !state.enabled
                  };
                })
              };
              this._popupStatusChangeService.refreshDialogData(newPopup);
              popUpSubs.subscribe((ev) => {
                if (ev.eventName === 'change' && ev.payload && this.collection && ev.payload !== this.collection.state) {
                  this.changeCollectionState(ev.payload);
                }
              });
            });
          }
          break;
        default:
          break;
      }
    }
  }

  handleDocumentsRemoval(collectionOid: string, documentOids: string[]): void {
    this.subs.sink = this._sweetAlertService
      .getDataBackFromSweetAlert()
      .pipe(take(1))
      .subscribe((data) => {
        if (data && data.confirmed) {
          this.subs.sink = this._collectionsWebService.removeDocuments(collectionOid, documentOids).subscribe(() => {
            const basicAlertData: MeBasicAlertI = {
              mode: 'success',
              title: this.i18nStrings.success,
              content: this.i18nStrings.removeDocumentsSuccessFromCollection
            };
            this._basicAlertService.openBasicAlert(basicAlertData);
            this.getCollectionDocuments();
          });
        }
      });
    const sweetAlertModel: MeSweetAlertI = {
      mode: 'warning',
      icon: 'alert-triangle',
      type: {
        name: MeSweetAlertTypeEnum.submit,
        buttons: {
          submit: this.i18nStrings.remove,
          cancel: this.i18nStrings.cancel
        }
      },
      title: this.i18nStrings.removeDocuments,
      message: this.i18nStrings.areYouSureYouWantToRemoveDocument
    };
    this._sweetAlertService.openMeSweetAlert(sweetAlertModel);
  }

  removeMaterialsFromCollection(materialOIDs: string[]): void {
    this.subs.sink = this._sweetAlertService.getDataBackFromSweetAlert().subscribe((data) => {
      if (data && data.confirmed && this.collection) {
        this.subs.sink = this._collectionsWebService.removeMaterialsFromCollection(this.collection.oid, materialOIDs).subscribe(() => {
          this._materialsStoreService.removeMaterialEntities(materialOIDs);
          const basicAlertData: MeBasicAlertI = {
            mode: 'success',
            title: this.i18nStrings.success,
            content: this.i18nStrings.materialHaveBeenSuccessfullyRemovedFromCollection
          };
          this._basicAlertService.openBasicAlert(basicAlertData);
        });
      }
    });

    const sweetAlertModel: MeSweetAlertI = {
      mode: 'danger',
      icon: 'alert-triangle',
      type: {
        name: MeSweetAlertTypeEnum.submit,
        buttons: {
          submit: this.i18nStrings.remove,
          cancel: this.i18nStrings.cancel
        }
      },
      title: this.i18nStrings.removeMaterial,
      message: this.i18nStrings.areYouSureYouWantToRemoveTheMaterial
    };
    this._sweetAlertService.openMeSweetAlert(sweetAlertModel);
  }

  showBasicErrorAlert(error: ErrorResponseI): void {
    const basicAlertData: MeBasicAlertI = {
      mode: 'error',
      title: error.error.title,
      content: error.error.details
    };
    this._basicAlertService.openBasicAlert(basicAlertData);
  }

  changeCollectionState(newStates: string): void {
    if (this.collection) {
      this.subs.sink = this._collectionsWebService.changeCollectionState(this.collection.oid, newStates).subscribe(
        (collection: CollectionModel) => {
          this.collection = collection;
          this.prepareInfoCard();
          this._basicAlertService.openBasicAlert({
            mode: 'success',
            title: this.i18nStrings.statusChanged,
            content: this.i18nStrings.collectionStatusChanged
          });
        },
        (error: ErrorResponseI) => {
          this._basicAlertService.openBasicAlert({
            mode: 'error',
            title: error.error.title,
            content: error.error.details
          });
        }
      );
    }
  }

  itemOid(_index: number, card: MeMaterialCardI | MeDocumentLineI): string {
    return card.oid;
  }

  bottomReachedHandler(): void {
    if (this.trackForBottomReach) {
      if (this.isSearch) {
        this.handleSearch(this.searchKeyword || '', this._materialsStoreService.materialNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
      } else {
        this.getMaterialsInCollection(this._materialsStoreService.materialNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
      }
    }
  }

  documentsBottomReachedHandler(): void {
    if (this.documentsTrackForBottomReach) {
      if (this.documentIsSearch) {
        this.documentsHandleSearch(
          this.documentsSearchKeyword || '',
          this._documentStoreService.documentNextId,
          this.NUMBER_OF_ITEMS_ON_PAGE,
          true
        );
      } else {
        this.getCollectionDocuments(this._documentStoreService.documentNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
      }
    }
  }

  handleItemClick(card: MeMaterialCardI, index: number): void {
    if (this.selectionEnabled) {
      this.toggleCardItemSelection(index);
    } else {
      this._router.navigate([`/materials/view/${card.oid}`]);
    }
  }

  handleSearch(searchValue: string, skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.searchKeyword = searchValue;
    this.searchBarData = {
      ...this.searchBarData,
      search: { placeholder: this.i18nStrings.searchFor, keyword: this.searchKeyword as string }
    };
    let orderByAttr: string = '';
    let orderByDir: string = '';
    switch (this.searchBarData.sort?.options[this.searchBarData.sort.selectedIndex].key) {
      case 'azAlphabetically':
        orderByAttr = MeSearchSortByTypes.name;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
      case 'zaAlphabetically':
        orderByAttr = MeSearchSortByTypes.name;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'newest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'oldest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
    }

    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_MATERIAL,
      criteriaQuick: this.searchKeyword,
      collectionOid: this.collectionOID || '',
      orderByAttr,
      orderByDir
    };

    this.areMaterialsLoading = true;
    this._globalService.activateLoader();
    this.subs.sink = this._materialsWebService
      .searchMaterials(searchFilter, skip, top)
      .pipe(
        finalize(() => {
          this.areMaterialsLoading = false;
          this._globalService.deactivateLoader();
        })
      )
      .subscribe((response: ArrayResponseI<MaterialModel>) => {
        this.handleMaterialsResponse(response, append, true);
      });
  }

  documentsHandleSearch(searchValue: string, from: number = 0, to: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.documentsSearchKeyword = searchValue;
    this.documentsSearchBarData = {
      ...this.documentsSearchBarData,
      search: { placeholder: this.i18nStrings.searchFor, keyword: this.documentsSearchKeyword as string }
    };

    let orderByAttr: string = '';
    let orderByDir: string = '';
    switch (this.searchBarData.sort?.options[this.searchBarData.sort.selectedIndex].key) {
      case 'azAlphabetically':
        orderByAttr = MeSearchSortByTypes.title;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
      case 'zaAlphabetically':
        orderByAttr = MeSearchSortByTypes.title;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'newest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'oldest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
    }

    const searchCriteria: QuickSearchModel = {
      collectionOid: this.collectionOID || '',
      domain: ME_DOMAIN_APPLICATION_DOCUMENT,
      criteriaQuick: this.documentsSearchKeyword,
      orderByAttr,
      orderByDir
    };

    this.areDocumentsLoading = true;
    this.subs.sink = this._documentsWebService
      .searchDocuments(searchCriteria, from, to)
      .pipe(
        finalize(() => {
          this.areDocumentsLoading = false;
        })
      )
      .subscribe((response: ArrayResponseI<MeDocumentModel>) => {
        this._documentStoreService.documentNextId = response.nextID;
        if (append) {
          this._documentStoreService.appendDocumentEntities(response.entities);
        } else {
          this._documentStoreService.documentEntities = response.entities;
          this.documentIsSearch = true;
          this.documentsTrackForBottomReach = true;
        }
        // If reached end of pagination unsubscribe from scroll
        if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
          this.documentsTrackForBottomReach = false;
        }
      });
  }

  handleSortByEvent(sortByValue: string): void {
    if (this.searchBarData.sort) {
      this.searchBarData.sort.selectedIndex = this.searchBarData.sort.options.findIndex((x) => x.key === sortByValue);
    }
    this.handleSearch(this.searchKeyword ? this.searchKeyword : '');
  }

  handleToggleSelectEvent(): void {
    this.selectionEnabled = !this.selectionEnabled;
    this.searchBarData = {
      ...this.searchBarData,
      selectButton: {
        ...this.searchBarData.selectButton,
        enabled: this.selectionEnabled,
        placeholder: this.selectionEnabled ? 'Cancel' : 'Select'
      }
    };
    this._selectedMap.clear();
    this.prepareMaterialCardData();
  }

  handleViewTypeEvent(): void {
    this.searchBarData.view === 'grid' ? (this.searchBarData.view = 'list') : (this.searchBarData.view = 'grid');
  }

  handleSearchBarEvents(event: { eventName: string; payload?: string | boolean }): void {
    switch (event.eventName) {
      case 'searchEnabledToggle':
        // TODO
        break;
      case 'edit':
        // TODO
        break;
      case 'remove':
        const materialOIDs: string[] = this.materialCardItems
          .filter((card) => card.selected)
          .map((card) => {
            return card.oid;
          });
        this.removeMaterialsFromCollection(materialOIDs);
        break;
      case 'copy':
        // TODO
        break;
      case 'more':
        // TODO
        break;
      case 'download':
        // TODO
        break;
    }
  }

  ngOnDestroy(): object {
    this.subs.unsubscribe();
    return super.ngOnDestroy();
  }
}
