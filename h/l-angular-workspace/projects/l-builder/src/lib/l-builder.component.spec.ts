import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LBuilderComponent } from './l-builder.component';

describe('LBuilderComponent', () => {
  let component: LBuilderComponent;
  let fixture: ComponentFixture<LBuilderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LBuilderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
