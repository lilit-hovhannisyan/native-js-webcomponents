import { traverseInShadowAndGetElement} from '../../../../../library/helpers/random.helpers.js';
import {BaseComponent} from '../../../../../library/core/base-component/base.component.js';
import {basePath} from '../../../data/base.path.js';
import {paginationComponentContentConfig} from './pagination.config.js';

export class Pagination extends BaseComponent {
  data;
  paginationSlot = 20;
  currentSlot = 0;

  constructor() {
    super(basePath + 'shared-components/pagination/pagination.component.css');
  }

  render = () => {
    this.currentContentConfig = paginationComponentContentConfig[this.getAttribute('component')];
    const requestId = this.getAttribute('requestid');

    // build ui before fetching data
    this.paginationSlot =
      this.getAttribute('itemsperpage') !== null
        ? +this.getAttribute('itemsperpage')
        : 20;

    const config = {
      tagName: 'div',
      attr: {
        class: 'pagination-result slot_' + this.paginationSlot,
      },
      subElements: [
        {
          tagName: 'pagination-btns',
          attr: {
            class: requestId ? 'pagination-btns' : 'hidden',
          },
          subElements: [
            {
              tagName: 'button-component',
              attr: {
                icon: 'icon-arrow_left',
                style: 'rounded',
              },
              events: {
                click: this.prev,
              }
            },
            {
              tagName: 'button-component',
              attr: {
                icon: 'icon-arrow_right',
                style: 'rounded',
              },
              events: {
                click: this.next
              }
            },
          ]
        },
        {
          tagName: 'block-container',
          attr: {
            class: this.getAttribute('component')
          },
          text: requestId ? 'Waiting for data to load...' : this.currentContentConfig.default
        }
      ]
    };
    const el = this.createElementWithAttributes(config);

    this.shadow.appendChild(el);

    // grab fetch info to do fetch
    if (requestId) {
      const requestUrl = this.currentContentConfig.fetchUrl.replace('${id}', requestId);

      fetch(requestUrl)
        .then(response => response.json())
        .then(
          res => {
          this.data = this.currentContentConfig.fetchDataIdentifier ?
            res.items :
            res;

          if (!this.data.length) {
            const container = traverseInShadowAndGetElement(this, 'block-container');
            container.innerHTML = 'No result is found';
            return;
          }

          const dataSlot = this.data.slice(0, this.paginationSlot);
          this.updateData(dataSlot);
          this.currentSlot += this.paginationSlot;
          this.updatePaginationButtons(false, this.data.length > this.paginationSlot);
        }
        );
    }
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }

  updatePaginationButtons(canGoBack, canGoForward) {
    const buttons = traverseInShadowAndGetElement(this, 'pagination-btns').children;
    buttons[0].setAttribute('noevent', canGoBack)
    buttons[1].setAttribute('noevent', canGoForward)
  }

  prev = () => {
    const dataSlot = this.data
      .slice(this.currentSlot - this.paginationSlot*2, this.currentSlot - this.paginationSlot, this.currentSlot);
    this.updateData(dataSlot);
    this.currentSlot -= this.paginationSlot;

    this.updatePaginationButtons(this.currentSlot !== this.paginationSlot, true);
  }

  next = () => {
    const dataSlot = this.data.slice(this.currentSlot, this.currentSlot + this.paginationSlot);
    this.updateData(dataSlot);
    this.currentSlot += this.paginationSlot;

    this.updatePaginationButtons(true, !!this.data.slice(this.currentSlot).lenght);
  }

  updateData(data) {
    const container = traverseInShadowAndGetElement(this, 'block-container');
    const nesdetHtml = this.currentContentConfig.nestedHtml;

    let html = '';
    data.forEach(i => {
      let replacedHtml = this.findAndReplaceVariables(nesdetHtml, i);
      replacedHtml = this.findAndReplaceActions(replacedHtml);

      html += replacedHtml;
    });

    container.innerHTML = html;

    if (data.length >= 6) {
      container.setAttribute('class', container.getAttribute('class') + ' justify')
    }
  }

  findAndReplaceVariables = (str, obj) => {
    let replaceString = str;

    const indexOfVariable = str.indexOf('$');
    if (indexOfVariable >= 0) {
      const start = str.substr(0, indexOfVariable);
      const restWithVariable = str.substr(indexOfVariable);

      const indexOfVarEnd = restWithVariable.indexOf('"');
      const replaceIdentifier = restWithVariable.substr(1, indexOfVarEnd - 1);
      const replaceValue = obj[replaceIdentifier];
      const end = restWithVariable.substr(indexOfVarEnd);

      replaceString = start + replaceValue + end;

      if (replaceString.indexOf('$') >= 0) {
        replaceString = this.findAndReplaceVariables(replaceString, obj);
      }
    }

    return replaceString;
  }

  findAndReplaceActions = (str) => {
    let replaceString = str;

    const indexOfAction = str.indexOf('*');
    if (indexOfAction >= 0) {
      const start = str.substr(0,indexOfAction);
      const restWithAction = str.substr(indexOfAction);

      const indexOfActionEnd = restWithAction.indexOf('"');
      const replaceIdentifier = restWithAction.substr(1, indexOfActionEnd - 1);
      const replaceValue = this[replaceIdentifier];
      const end = restWithAction.substr(indexOfActionEnd);

      replaceString = start + replaceValue + end;

      if (replaceString.indexOf('*') >= 0) {
        replaceString = this.findAndReplaceActions(replaceString);
      }
    }

    return replaceString;
  }

  // history.pushState({}, 'user id ' +userId, 'user/'+userId);
  openInNewPage = (e) => {
    const userId = e.target.parentNode.getRootNode().host.getAttribute('user');
    alert('handle dynamic route')
  }

  openInPopup = (e) => {
    const userId = e.target.parentNode.getRootNode().host.getAttribute('user');

    const config = {
      tagName: 'git-popup-manager',
      attr: {
        userid: userId
      }
    };

    const el = this.createElementWithAttributes(config);
    this.shadowRoot.appendChild(el);
  }
}