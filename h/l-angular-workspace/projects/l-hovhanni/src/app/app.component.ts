import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {appName, routeConf, errorPh} from '../../../shared-layer/l-hovhanni/dal/app.config';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  photographer;
  fixToolBar = false;

  constructor(private title: Title,
              private router: Router) {

      router.events.subscribe(val => {
          if (val instanceof NavigationEnd) {
              let curRoute = val.url.replace('/', '');
              curRoute = curRoute.indexOf('?') >= 0 ? curRoute.substr(0, curRoute.indexOf('?')) : curRoute;

              const confData = routeConf.find( i => i.route === curRoute);
              this.photographer = confData ? confData.photographer : errorPh;
              const _title = confData ? confData.title : '404';

              this.title.setTitle(`${_title} ${appName}`);
          }
      });
  }

  ngOnInit() {
      console.log('l-hovhanni-app --v1.0.0');
  }

  onScroll(event) {
    if (event.scrollDirection.windowY >= 100 ) {
        this.fixToolBar = true;
    } else {
        this.fixToolBar = false;
  }
}
}
