export const appName = '@l_hovhannni';

export const navItems = [
    {
        icon : 'icon-pictures2',
        text : 'Portfolio',
        href : 'portfolio'
    }, {
        icon : 'icon-user-outline',
        text : 'About',
        href : 'about'
    }, {
        icon : 'icon-send-o',
        text : 'Contact',
        href : 'contact'
    }
];

export const routeConf = [
    {
        route: '',
        title: 'Portfolio',
        photographer: 'yes',
    },
    {
        route: 'portfolio',
        title: 'Portfolio',
        photographer: 'yes',
    },
    {
        route: 'videos',
        title: 'Videos',
        photographer: 'mar',
    },
    {
        route: 'about',
        title: 'About',
        photographer: 'ash',
    },
    {
        route: 'contact',
        title: 'Contact',
        photographer: 'kar',
    },
    {
        route: 'story-telling',
        title: 'Story telling',
        photographer: 'kar',
    },
];

export const errorPh = 'mar';

export const socialLinks = [
    {
        icon: 'icon-instagram1',
        href: 'https://www.instagram.com/l_hovhanni/'
    },
    {
        icon: 'icon-social-facebook-circular1',
        href: 'https://www.facebook.com/l.hovhanni'
    },
    {
        icon: 'icon-social-linkedin-circular',
        href: 'https://www.linkedin.com/in/lilit-hovhannisyan-armeni/'
    },
];

