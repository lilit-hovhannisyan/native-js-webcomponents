export interface MeAdvancedFilterI {
  active: boolean;
  icon: string;
  resetMessage: string;
}
