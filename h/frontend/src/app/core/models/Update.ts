import { differenceBy, intersection } from 'lodash';
import { IResource, ResourceKey } from './resources/IResource';

export class Update<T extends IResource<ResourceKey>> {
  data: T;
  actions: { [key: string]: 'created' | 'modified' | 'deleted' };

  /**
   * The constructor checks in what way the provided resource (nextResource) has changed compared
   * to its previous state (prevResource). Based on this check, the actions are inferred.
   * The `nextResource` is used as actual payload (=data) of the update.
   */
  public constructor(prevResource: T, nextResource: T) {
    const prevRelationKeys = Object.keys(prevResource).filter(key => this.isResource(prevResource[key]));
    const curRelationKeys = Object.keys(nextResource).filter(key => this.isResource(nextResource[key]));

    const removedKeys = prevRelationKeys.filter(prevKey => !curRelationKeys.includes(prevKey));
    const addedKeys = curRelationKeys.filter(curKey => !prevRelationKeys.includes(curKey));
    const intersectingKeys = intersection(prevRelationKeys, curRelationKeys);
    const modifiedKeys = intersectingKeys.filter(key =>
      nextResource[key]._type === 'collection'
        ? this.isDifferentCollection(nextResource[key], prevResource[key])
        : this.resourcesDiffer(nextResource[key], prevResource[key])
    );
    const removedActions = removedKeys.reduce((acc, key) => ({ ...acc, [key]: 'removed' }), {});
    const addedActions = addedKeys.reduce((acc, key) => ({ ...acc, [key]: 'added' }), {});
    const modifiedActions = modifiedKeys.reduce((acc, key) => ({ ...acc, [key]: 'modified' }), {});

    this.data = nextResource;
    this.actions = {
      ...removedActions,
      ...addedActions,
      ...modifiedActions
    };

    if (nextResource._type === 'User') { this.actions.roles = 'modified'; }
  }

  private isResource = (r) =>
    r && r.hasOwnProperty('id') && r.hasOwnProperty('_type')

  private resourcesDiffer = (res1, res2) => {
    if (!res1 && !res2) { return false; } // if both are false they are considered as not different
    if (!res1 || !res2) { return true; } // if only one of them is falsy, they are different.
    if (typeof res1 !== 'object' || typeof res2 !== 'object') { return true; }
    return res1._type === res2._type && res1.id === res2.id;
  }

  private isDifferentCollection = (col1, col2) => {
    return !!differenceBy(col1.items, col2.items, 'id').length;
  }
}
