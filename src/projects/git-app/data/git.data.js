// for cache purposes
export let gitUsers = new Map();

export const setCache = (user) => {
  gitUsers.set(user.id, user);
}
