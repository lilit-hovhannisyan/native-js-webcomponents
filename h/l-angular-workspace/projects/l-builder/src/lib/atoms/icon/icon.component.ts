import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'lavash-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.less']
})
export class IconComponent {
  @Input() iconClassName: string;
}
