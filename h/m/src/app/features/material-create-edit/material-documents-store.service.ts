import { Injectable } from '@angular/core';
import { MeDocumentModel } from '@shared/models/document/document.model';

import { BehaviorSubject } from 'rxjs';

@Injectable()
export class MaterialDocumentStoreService {
  private readonly _documentEntities = new BehaviorSubject<MeDocumentModel[]>([]);

  readonly documentEntities$ = this._documentEntities.asObservable();

  private readonly _documentNextId = new BehaviorSubject<number>(0);

  readonly documentNextId$ = this._documentNextId.asObservable();

  private readonly _addedDocumentEntities = new BehaviorSubject<MeDocumentModel[]>([]);

  readonly addedDocumentEntities$ = this._addedDocumentEntities.asObservable();

  private readonly _addedDocumentNextId = new BehaviorSubject<number>(0);

  readonly addedDocumentNextId$ = this._addedDocumentNextId.asObservable();

  get documentEntities(): MeDocumentModel[] {
    return this._documentEntities.getValue();
  }

  set documentEntities(data: MeDocumentModel[]) {
    this._documentEntities.next(data);
  }

  get addedDocumentEntities(): MeDocumentModel[] {
    return this._addedDocumentEntities.getValue();
  }

  set addedDocumentEntities(data: MeDocumentModel[]) {
    this._addedDocumentEntities.next(data);
  }

  appendDocumentEntities(data: MeDocumentModel[]): void {
    this.documentEntities = [...this.documentEntities, ...data];
  }

  appendAddedDocumentEntities(data: MeDocumentModel[]): void {
    this.addedDocumentEntities = [...this.addedDocumentEntities, ...data];
  }

  get documentNextId(): number {
    return this._documentNextId.getValue();
  }

  set documentNextId(data: number) {
    this._documentNextId.next(data);
  }

  get addedDocumentNextId(): number {
    return this._addedDocumentNextId.getValue();
  }

  set addedDocumentNextId(data: number) {
    this._addedDocumentNextId.next(data);
  }
}
