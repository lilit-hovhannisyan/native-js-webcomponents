import { MeContextMenuI } from '../me-context-menu/me-context-menu-inteface';
import { MeDocumentLineI } from '../me-document-line/me-document-line.interface';
import { MeSearchInputI } from '../me-search-input/me-search-input-interface';

export interface MeRightSidebarI {
  tabs: {
    label: string;
    active: boolean;
    type: Overview | Documents;
  }[];
}

export enum TabTypeEnum {
  overview = 'overview',
  documents = 'documents'
}

interface Overview {
  id: TabTypeEnum.overview;
  content: {
    thumbnailUrl: string;
    title: string;
    subtitle: string;
    status: string;
    contextMenu: MeContextMenuI;
    paragraph: string;
    video?: {
      url: string;
      type: string;
      alt: string;
    };
  };
}

interface Documents {
  id: TabTypeEnum.documents;
  content: {
    search: MeSearchInputI;
    allFilesPlaceholder: string;
    extensions: {
      id: string;
      displayValue: string;
    }[];
    documentCards: MeDocumentLineI[];
  };
}
