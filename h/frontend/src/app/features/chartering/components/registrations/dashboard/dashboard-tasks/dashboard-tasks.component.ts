import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Input, ViewChild, SimpleChanges } from '@angular/core';
import * as moment from 'moment';
import { wording } from 'src/app/core/constants/wording/wording';
import { TaskType } from 'src/app/core/models/enums/vessel-registration-tasks';
import { IRegistrationVesselTask } from 'src/app/core/models/resources/IRegistrationVesselTask';
import { IVesselTaskFull } from 'src/app/core/models/resources/IVesselTask';
import { RegistrationVesselTasksMainService } from 'src/app/core/services/mainServices/registration-vessel-tasks-main.service';
import { RegistrationVesselTasksRepositoryService } from 'src/app/core/services/repositories/registration-vessel-tasks-repository.service';
import { VesselTaskListComponent } from '../../../../../../shared/components/vessel-task-components/vessel-task-list/vessel-task-list.component';

@Component({
  selector: 'nv-dashboard-tasks',
  templateUrl: './dashboard-tasks.component.html',
  styleUrls: ['./dashboard-tasks.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardTasksComponent implements OnInit {
  @Input() public calendarRange: { start: moment.Moment, end: moment.Moment };
  @ViewChild('vesselTasksComponent', { static: false })
  public vesselTasksComponent: VesselTaskListComponent;
  public allTasks: IVesselTaskFull<IRegistrationVesselTask>[] = [];
  public wording = wording;
  public taskType = TaskType.RegistrationVesselTask;

  constructor(
    private cdr: ChangeDetectorRef,
    private registrationVesselTasksRepo: RegistrationVesselTasksRepositoryService,
    private registrationVesselTaskMainService: RegistrationVesselTasksMainService,
  ) { }

  public ngOnInit(): void {
    this.fetchResource();
  }

  public ngOnChanges(changes: SimpleChanges): void {
    // we shouldn't call `scrollToTheFirstTaskInCalendarRange` method from `ngOnChanges`
    // in initial calendar range, thats why `changes.calendarRange?.previousValue` check
    // is added, to make sure that there was previous value and this isn't initial
    // value change
    if (changes.calendarRange?.previousValue) {
      this.scrollToTheFirstTaskInCalendarRange();
    }
  }

  /** Fetches all registration-vessel-tasks by `this.vesselId`. */
  private fetchResource(): void {
    this.registrationVesselTasksRepo.fetchAll({});
    // registration-fleet-detail-view is doing fetch request
    this.registrationVesselTaskMainService.getStream(null, true)
      .subscribe((registrationVesselTasks: IRegistrationVesselTask[]) => {
        this.allTasks = registrationVesselTasks.sort((a, b) => a.sortIndex - b.sortIndex);
        this.scrollToTheFirstTaskInCalendarRange();
        this.cdr.detectChanges();
      });
  }

  private scrollToTheFirstTaskInCalendarRange(): void {
    // sometimes vesselTasksComponent isn't fully loaded, setTimeout is added to avoid
    // errors which could happen when vesselTasksComponent isn't fully loaded
    setTimeout(() => {
      const currentRangeTasks: IRegistrationVesselTask[] = this.allTasks
      .filter(t => moment(t.start)
        .isBetween(this.calendarRange.start, this.calendarRange.end, 'days', '[]'))
      .sort((a, b) => {
        const timeA: number = new Date(a.start).getTime();
        const timeB: number = new Date(b.start).getTime();

        return timeA - timeB;
      });

      if (currentRangeTasks.length) {
        this.vesselTasksComponent.getTaskById(currentRangeTasks[0].id)?.scrollIntoView();
      }
    });
  }

  /**
   * Page custom validation to prevent navigation when unsaved-changes exist.
   * Used in parent component.
   */
  public isValid = (): boolean => !this.vesselTasksComponent.hasChanges;
}
