import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

/**
 * This class mostly uses for returning Observable which emits data, by which checks
 * if form is editable or not
 */
@Injectable()
export class WriteProtectionService {
  public writeProtectionRequest$: Subject<boolean> = new BehaviorSubject(false);
  public writeProtection: false;

  public checkWriteProtection() {
    // this.httpClient
    //   .get(`/check-edit-possible`)
    //   .subscribe(data => this.writeProtection = data['editable'])
  }
}
