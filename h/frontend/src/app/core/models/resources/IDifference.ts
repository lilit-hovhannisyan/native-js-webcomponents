import { IResource } from './IResource';
export type DifferenceKey = number;

interface ISharedProps {
  member: string;
  oldValue: any;
  newValue: any;
}

export interface IDifferenceBase extends ISharedProps, IResource<DifferenceKey> { }

export interface IDifferenceCreate extends ISharedProps { }

export interface IDifferenceDTO extends IDifferenceBase { }

export interface IDifference extends IDifferenceDTO { }

