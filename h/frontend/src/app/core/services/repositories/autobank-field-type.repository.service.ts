import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IAutobankFieldType, IAutobankFieldTypeCreate, IAutobankFieldTypeDTO, AutobankFieldTypeKey } from '../../models/resources/IAutobankFieldType';

@Injectable({
  providedIn: 'root'
})
export class AutobankFieldTypeRepositoryService extends AbstractRepository<
IAutobankFieldType,
IAutobankFieldTypeDTO,
IAutobankFieldTypeCreate,
RepoParams<IAutobankFieldType>,
AutobankFieldTypeKey
> {
  protected resourceType = 'Autobank';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IAutobankFieldTypeDTO): IAutobankFieldType => item;

  public getBaseUrl = () => '/api/autobank/rule-field-types';
}
