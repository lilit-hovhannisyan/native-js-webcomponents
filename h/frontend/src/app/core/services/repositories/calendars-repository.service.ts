import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { CalendarKey, ICalendar, ICalendarCreate, ICalendarDTO } from '../../models/resources/ICalendar';

@Injectable({
  providedIn: 'root'
})
export class CalendarsRepositoryService extends AbstractRepository<
  ICalendar,
  ICalendarDTO,
  ICalendarCreate,
  RepoParams<ICalendar>,
  CalendarKey
> {
  protected resourceType = 'Calendar';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/calendars';
  }

  protected postProcess(item: ICalendarDTO): ICalendar { return item; }

}
