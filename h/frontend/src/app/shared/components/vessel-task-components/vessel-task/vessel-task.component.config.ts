import { ModalOptions } from 'ng-zorro-antd/modal';
import { TaskType } from 'src/app/core/models/enums/vessel-registration-tasks';
import { IVesselTask } from 'src/app/core/models/resources/IVesselTask';
import { VesselTaskComponent } from './vessel-task.component';

/** The css-class used when task-component used as a modal. */
export const vesselTaskModalClass = 'vessel-task-modal-class';

export interface ITaskComponentParams<V extends IVesselTask = IVesselTask> {
  value: Partial<V>;
  type: TaskType;
  isDisabled: boolean;
  fetchResources: boolean;
  disableVesselField: boolean;
}

/** Partial Options needed to have a not standard modal with normal UI/UX. */
export function getVesselTaskModalConfig<V extends IVesselTask = IVesselTask>(
  options?: Partial<ITaskComponentParams<V>>,
): Partial<ModalOptions> {
  // `true` by default
  const fetchResources: boolean = options?.fetchResources === false ? false : true;

  return {
    nzComponentParams: {
      ...options,
      fetchResources,
      type: options?.type || TaskType.SimpleVesselTask,
    },
    nzContent: VesselTaskComponent,
    nzClassName: vesselTaskModalClass,
    nzClosable: false,
    nzMaskClosable: false,
    nzWidth: '1060px',
    nzFooter: null,
  };
}
