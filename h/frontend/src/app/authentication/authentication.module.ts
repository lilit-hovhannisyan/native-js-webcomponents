import { NgModule } from '@angular/core';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { LoginViewComponent } from './components/login-view/login-view.component';
import { SendVerificationLinkComponent } from './components/send-verification-link/send-verification-link.component';
import { SharedModule } from '../shared/shared.module';
import { ResetPasswordViewComponent } from './components/reset-password-view/reset-password-view.component';

@NgModule({
  declarations: [
    LoginViewComponent,
    SendVerificationLinkComponent,
    ResetPasswordViewComponent,
  ],
  imports: [
    AuthenticationRoutingModule,
    SharedModule,
  ]
})
export class AuthenticationModule { }
