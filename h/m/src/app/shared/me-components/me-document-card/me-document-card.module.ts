import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { MeIconsProviderModule } from '@shared/modules/me-icons.module';

import { MeDocumentCardComponent } from './me-document-card.component';
import { MeContextMenuModule } from '../me-context-menu/me-context-menu.module';
import { MeEllipsisRowModule } from '../me-ellipsis-row/me-ellipsis-row.module';

@NgModule({
  declarations: [MeDocumentCardComponent],
  imports: [CommonModule, MeEllipsisRowModule, MeContextMenuModule, FormsModule, MatCheckboxModule, MeIconsProviderModule],
  exports: [MeDocumentCardComponent]
})
export class MeDocumentCardModule {}
