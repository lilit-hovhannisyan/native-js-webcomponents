import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseWebService } from './base.web-service';

import { MATE_BASE_API_URL } from '@shared/constants';
import { CsrfModel } from '@shared-models/auth/csrf.model';

@Injectable({
  providedIn: 'root'
})
export class SecurityWebService {
  constructor(private baseWebService: BaseWebService) {}

  getCSRF(): Observable<CsrfModel> {
    return this.baseWebService.getRequest<CsrfModel>(`${MATE_BASE_API_URL}/security/csrf/`, CsrfModel);
  }
}
