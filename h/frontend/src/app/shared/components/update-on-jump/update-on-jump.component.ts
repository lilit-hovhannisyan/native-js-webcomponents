import { Component, ViewChild, Output, EventEmitter, ElementRef, AfterViewInit, Input } from '@angular/core';
import { AbstractValueAccessor, makeProvider } from 'nv-grid';

@Component({
  selector: 'nv-update-on-jump',
  templateUrl: './update-on-jump.component.html',
  styleUrls: ['./update-on-jump.component.scss'],
  providers: [makeProvider(UpdateOnJumpComponent)]

})
export class UpdateOnJumpComponent extends AbstractValueAccessor implements AfterViewInit {

  @Input() public model: any;
  @Input() public type = 'text';
  @Input() public autofocus = true;
  @Input() public disableArrows = false;
  @Input() public maxDecimalDigits = 2;
  @Input() public maxIntegerDigits: number;
  @Input() public saveValueAfterBlur: boolean;
  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();
  @ViewChild('input', { static: true }) private input: ElementRef<any>;

  private decimalSeparator: ',' | '.' = '.';

  constructor() {
    super();
  }

  public ngAfterViewInit() {
    if (this.autofocus) {
      setTimeout(() => {
        this.input.nativeElement.focus();
        this.input.nativeElement.select();
      }, 0);
    }
  }

  private formatValue(value: any): any {
    if (value === null || value === '') {
      return '';
    }

    if (this.type === 'number') {
      const num: string = this.maxIntegerDigits
        ? this.sliceIntegerPart(value)
        : value;
      return (+num).toFixed(this.maxDecimalDigits);
    }

    return value;
  }

  private sliceIntegerPart(value: any): string {
    const stringValue: string = value.toString();
    let finalValue: string;

    if (stringValue.includes(this.decimalSeparator)) {
      const stringParts: string[] = stringValue.split(this.decimalSeparator);
      finalValue = `${stringParts[0]
        .slice(0, this.maxIntegerDigits)}${this.decimalSeparator}${stringParts[1]}`;
    } else {
      finalValue = stringValue.slice(0, this.maxIntegerDigits);
    }

    return finalValue;
  }

  public handleTab(event: KeyboardEvent, value: any) {
    value = this.formatValue(value);
    this.onChange(value);
    this.tab.emit(event);
  }

  public handleEnter(event: KeyboardEvent, value: any) {
    value = this.formatValue(value);
    this.onChange(value);
    this.enter.emit(event);
  }

  public handleEscape(event: any) {
    this.escape.emit(event);
  }

  public handleBlur(event: any, value: any): void {
    if (this.saveValueAfterBlur) {
      value = this.formatValue(value);
      this.onChange(value);
      this.blur.emit(event);
      return;
    }

    event.preventDefault();
    this.blur.emit(event);
  }

  public handleArrows(event: any) {
    if (this.disableArrows) {
      return false;
    }
    return true;
  }

  public setDisabledState(isDisabled: boolean): void {
    this._disabled = isDisabled;
    this.input.nativeElement.disabled = isDisabled;
  }
}
