import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DigitalProfileEditRoutingModule } from './digital-profile-edit-routing.module';
import { DigitalProfileEditComponent } from './digital-profile-edit.component';
import { MeButtonModule } from '@shared/me-components/me-button/me-button.module';
import { MatStepperModule } from '@angular/material/stepper';
import { ReactiveFormsModule } from '@angular/forms';
import { MeEmptyContentModule } from '@shared/me-components/me-empty-content/me-empty-content.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MeAttributeModule } from '@shared/me-components/me-attribute/me-attribute.module';
import { MeComponentLoadingModule } from '@shared/me-directives/me-component-loader/me-component-loader.module';
import { MeUploadFileModule } from '@shared/me-components/me-upload-file/me-upload-file.module';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';
import { MeDocumentLineModule } from '@shared/me-components/me-document-line/me-document-line.module';

@NgModule({
  declarations: [DigitalProfileEditComponent],
  imports: [
    CommonModule,
    DigitalProfileEditRoutingModule,
    IconsModule,
    MeAttributeModule,
    MeButtonModule,
    MatStepperModule,
    ReactiveFormsModule,
    MeEmptyContentModule,
    MeUploadFileModule,
    MeComponentLoadingModule,
    MeIconsProviderModule,
    MeDocumentLineModule
  ]
})
export class DigitalProfileEditModule {}
