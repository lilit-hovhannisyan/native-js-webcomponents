import { Type } from 'class-transformer';
import { DomainModel } from '@shared-models/domain/domain.model';
import { EnumModel } from '@shared-models/enum/enum.model';

export class DefinitionsModel {
  @Type(() => DomainModel)
  domains: DomainModel[] = [];
  @Type(() => EnumModel)
  enums: EnumModel[] = [];
}
