import { Component, ChangeDetectionStrategy, ViewChild, TemplateRef } from '@angular/core';

import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'nv-topic-search',
  templateUrl: './topic-search.component.html',
  styleUrls: ['./topic-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicSearchComponent {
  public modalRef: NzModalRef;

  @ViewChild('searchModal') public searchModal: TemplateRef<any>;

  constructor(
    private nzModalService: NzModalService,
  ) { }

  public openSearchModal() {
    this.modalRef = this.nzModalService.create({
      nzContent: this.searchModal,
      nzClosable: true,
      nzFooter: null,
      nzClassName: 'topic-search-modal',
    });
  }

  public navigatedFromTopicSearch() {
    this.modalRef.close();
  }
}
