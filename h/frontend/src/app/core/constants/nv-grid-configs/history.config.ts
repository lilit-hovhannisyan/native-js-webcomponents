import { NvColumnDataType, NvGridConfig } from 'nv-grid';
import { wordingGeneral } from 'src/app/core/constants/wording/general';
import { GridDataSelectComponent } from 'src/app/shared/navido-components/grid-data-select/grid-data-select.component';

export const getHistoryGridConfig = (): NvGridConfig => {
  return ({
    gridName: 'HistoryGrid',
    rowButtonsPosition: null,
    hideRefreshButton: true,
    showFooter: false,
    clearFiltersButton: true,
    expandableComponentConfig: {
      componentToPort: GridDataSelectComponent
    },
    paging: {
      pageNumber: 1,
      pageSize: 100,
      showTotalItems: true
    },
    columns: [{
      key: 'id',
      visible: false,
    },
    {
      key: 'changeDate',
      title: wordingGeneral.changeDate,
      dataType: NvColumnDataType.DateTime,
      width: 160,
      isSortable: true,
      filter: {
        values: []
      }
    }, {
      key: 'entity',
      title: wordingGeneral.entity,
      dataType: NvColumnDataType.String,
      width: 160,
      isSortable: true,
      filter: {
        values: []
      }
    }, {
      key: 'subEntity',
      title: wordingGeneral.subEntity,
      dataType: NvColumnDataType.String,
      width: 160,
      isSortable: true,
      filter: {
        values: []
      }
    }, {
      key: 'actionType',
      title: wordingGeneral.actionType,
      width: 160,
      isSortable: true,
      filter: {
        values: []
      }
    },
    {
      key: 'editedBy',
      title: wordingGeneral.editedBy,
      width: 160,
      isSortable: true,
      filter: {
        values: []
      }
    }]
  });
};
