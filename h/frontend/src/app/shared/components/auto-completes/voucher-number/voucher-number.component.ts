import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { IWording } from 'src/app/core/models/resources/IWording';
import { wording } from 'src/app/core/constants/wording/wording';
import { Observable, ReplaySubject } from 'rxjs';
import { IInvoiceLedgerFull } from 'src/app/core/models/resources/IInvoiceLedger';
import { getVoucherNumberGridConfig } from 'src/app/core/constants/nv-grid-configs/voucher-no.config';
import { InvoiceLedgerRepositoryService } from 'src/app/core/services/repositories/invoice-ledger-repository.service';
import { InvoiceLedgerMainService } from 'src/app/core/services/mainServices/invoice-ledger-main.service';
import { map, switchMap, startWith, takeUntil } from 'rxjs/operators';
import { MandatorKey } from 'src/app/core/models/resources/IMandator';

@Component({
  selector: 'nv-voucher-number',
  templateUrl: './voucher-number.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VoucherNumberComponent implements OnInit, OnDestroy {
  @Input() public form: FormGroup;
  @Input() public controlName = 'invoiceLedgerId';
  @Input() public useCache = true;
  @Input() public label: IWording = wording.general.noLabel;
  @Input() public dataSource$: Observable<IInvoiceLedgerFull[]>;
  @Input() public setId = true;
  @Input() public displayOneProperty: boolean;
  @Input() public showLabel = false;

  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();
  @Output() public valueChange: EventEmitter<any> = new EventEmitter();

  public wording = wording;
  public gridConfig = getVoucherNumberGridConfig();
  public title: IWording = wording.accounting.selectVoucherNumber;
  private componentDestroyed$ = new ReplaySubject<boolean>(1);

  constructor(
    private invoiceLedgerRepo: InvoiceLedgerRepositoryService,
    private invoiceLedgerMainService: InvoiceLedgerMainService,
  ) { }

  public ngOnInit() {
    this.invoiceLedgerRepo.fetchAll({ useCache: this.useCache }).subscribe();
    this.dataSource$ = this.invoiceLedgerMainService.getStream()
      .pipe(
        takeUntil(this.componentDestroyed$),
        switchMap(invoiceLedgerItems => {
          const mandatorCtrl: AbstractControl = this.form.get('mandatorId');
          return mandatorCtrl.valueChanges
            .pipe(
              startWith(mandatorCtrl.value),
              map((mandatorId: MandatorKey) => {
                return invoiceLedgerItems.filter(i => i.mandatorId === mandatorId);
              })
            );
        }),
        map(invoiceLedgerItems => {
          return this.invoiceLedgerMainService.toRows(invoiceLedgerItems);
        }),
        map(invoiceLedgerItems => invoiceLedgerItems.map(i => ({
          ...i,
          shortVoucherNumber: +i.voucherNumber.toString().slice(7),
        })).filter(i => !i?.booked)
        )
      );
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
