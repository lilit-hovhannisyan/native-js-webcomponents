import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ITodoWidget } from 'src/app/core/models/widgets/ITodoWidget';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-todo-widget',
  templateUrl: './todo-widget.component.html',
  styleUrls: ['./todo-widget.component.scss']
})
export class TodoWidgetComponent implements OnInit, OnDestroy {

  @Input() private widget: ITodoWidget;
  private subscription: Subscription;

  public wording = wording;

  public todos = [
    {
      vessel: 'Sagitta',
      text: 'Invoice issued: REA033900150'
    },
    {
      vessel: 'Aglaia',
      text: '20 days to redelivery'
    },
    {
      vessel: 'Filomena',
      text: '1 day past redelivery: please update fleet list'
    },
    {
      vessel: 'Petrohue',
      text: '3 days past actual delivery: please enter or check bunkers on delivery'
    },
    {
      vessel: 'Ricarda',
      text: 'Advised redelivery today: please enter actual redelivery date',
    },
    {
      vessel: 'Nordschelde',
      text: 'Invoice 3 days past due: REA013900150',
    },
  ];

  constructor(
  ) { }

  public ngOnInit(): void {
  }

  public ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }
}

