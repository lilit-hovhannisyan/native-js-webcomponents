import { Component, OnInit, Input } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { getCompanyCategoryModalGirdConfig } from './company-categories-modal.config';
import { Observable, of } from 'rxjs';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { cloneDeep } from 'lodash';
import { ICategory } from 'src/app/core/models/resources/ICategory';

@Component({
  templateUrl: './company-categories-modal.component.html',
  styleUrls: ['./company-categories-modal.component.scss']
})
export class CompanyCategoriesModalComponent implements OnInit {
  private selectedCategories: ICategory[];
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<ICategory[]>;
  public submitDisabled = true;
  public wording = wording;

  @Input() public unusedCategories: ICategory[];

  constructor(
    private modalRef: NzModalRef,
  ) { }

  public ngOnInit(): void {
    this.gridConfig = getCompanyCategoryModalGirdConfig();
    this.dataSource$ = of(
      // change references to not have previous checked items checked
      cloneDeep(this.unusedCategories),
    );
  }

  public submit(): void {
    if (this.submitDisabled) {
      return;
    }
    this.modalRef.close(this.selectedCategories);
  }

  public rowSelect(categories: ICategory[]) {
    this.selectedCategories = categories;
    this.submitDisabled = !categories.length;
  }
}
