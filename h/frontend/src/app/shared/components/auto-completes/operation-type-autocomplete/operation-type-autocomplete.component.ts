import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { AutobankOperationTypeRepositoryService } from 'src/app/core/services/repositories/autobank-operation-type.repository.service';
import { FieldOperationTypeRepositoryService } from 'src/app/core/services/repositories/field-operation-type-repository.service';

@Component({
  selector: 'nv-operation-type-autocomplete',
  templateUrl: './operation-type-autocomplete.component.html',
  styleUrls: ['./operation-type-autocomplete.component.scss']
})
export class OperationTypeAutocompleteComponent {
  public wording = wording;
  public gridConfig = null;

  @Input() public form: FormGroup;
  @Input() public controlName = 'operationTypeId';
  @Input() public filters: object;
  @Input() public onlyOfField: boolean;
  @Input() public fieldControlName = 'fieldTypeId';
  @Input() public showLabel = true;
  @Input() public setNullIfNotFound = false;
  @Input() public setId = true;
  @Input() public displayOneProperty: boolean;
  @Input() public openListOnInit: boolean;
  @Input() public autofocus: boolean;

  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();


  constructor(
    public autobankOperationTypeRepo: AutobankOperationTypeRepositoryService,
    public fieldOperationTypeRepo: FieldOperationTypeRepositoryService,
  ) { }
}
