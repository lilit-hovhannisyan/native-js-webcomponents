export const getRoutes = () => {
  const routes = [
    {
      path: '/',
      redirectTo: 'git-users'
    },
    {
      title: 'User item',
      path: 'users/:id',
      component: 'git-user-single',
      showInHeader: false,
    },
    {
      title: 'Users',
      path: 'users',
      component: 'git-users',
      showInHeader: true,
    },
    {
      title: 'Repositories',
      path: 'repositories',
      component: 'git-repos',
      showInHeader: true,
    },
    {
      title: '404 not found',
      path: 'not-found',
      component: '404',
      showInHeader: false,
    },
  ];

  return routes;
};