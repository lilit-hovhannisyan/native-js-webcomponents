import { HttpPost } from './../../models/http/http-post';
import { map } from 'rxjs/operators';
import { MandatorKey, IMandatorLookup } from './../../models/resources/IMandator';
import { HttpGet } from 'src/app/core/models/http/http-get';
import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IMandator, IMandatorDTO, IMandatorCreate } from '../../models/resources/IMandator';
import { RepositoryService } from '../repository.service';
import { Observable } from 'rxjs';
import { IBookingLine } from '../../models/resources/IBookingLine';
import { IMandatorBlockedBookingYear } from '../../models/resources/IMandatorBlockedBookingYear';
import { IBlockedYearEntry } from '../../models/resources/IBlockedYearEntry';
import { IClosedYearEntry  } from '../../models/resources/IClosedYearEntry';
import { IMandatorAccount } from '../../models/resources/IMandatorAccount';

@Injectable({
  providedIn: 'root'
})
export class MandatorRepositoryService extends AbstractRepository<
IMandator,
IMandatorDTO,
IMandatorCreate,
RepoParams<IMandator>,
MandatorKey,
IMandatorLookup
> {
  protected resourceType = 'Mandator';
  public sortAsce = true;
  public sortKey = 'no';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/mandators';
  }

  protected postProcess(item: IMandatorDTO): IMandator { return item; }

  public getBlockedYears(mandatorIds: number[] = [], onlyLast = false): Observable<IMandatorBlockedBookingYear[]> {
    const request = new HttpGet(
      '/api/mandator-blocked-years',
      { init: { params: { mandatorIds } } },
    );
    return this.repositoryService.httpClient.request(request).pipe(
      map((response: any) => response.body.data.items)
    );
  }

  public setBlockedYears(mandatorId: MandatorKey, blockedYears: IBlockedYearEntry[]) {
    const url = `/api/mandators/${mandatorId}/blocked-booking-years`;
    const payload = { items: blockedYears.map(b => ({ ...b, mandatorId: mandatorId })) };
    const request = new HttpPost(url, payload);
    return this.repositoryService.httpClient.request(request).pipe(
      map((response: any) => response.body.data.items)
    );
  }

  public setClosedYears(mandatorId: MandatorKey, closedYears: IClosedYearEntry[]) {
    const url = `/api/mandators/${mandatorId}/closed-booking-years`;
    const payload = { items: closedYears.map(closedYear => ({ ...closedYear, mandatorId: mandatorId })) };
    const request = new HttpPost(url, payload);
    return this.repositoryService.httpClient.request(request).pipe(
      map((response: any) => response.body.data.items)
    );
  }

  public getClosedYears(mandatorId: MandatorKey) {
    const request = new HttpGet(`/api/mandators/${mandatorId}/closed-booking-years`);
    return this.repositoryService.httpClient.request(request).pipe(
      map((response: any) => response.body.data.items)
    );
  }

  public getBookingLines(mandatorId: MandatorKey): Observable<IBookingLine[]> {
    const request = new HttpGet(
      `/api/mandators/${mandatorId}/booking-lines`,
      { init: { params: { limit: 1 } } }
    );
    return this.repositoryService.httpClient.request(request).pipe(
      map((response: any) => response.body.data.items)
    );
  }

  public fetchBookingAccounts(mandatorId: number): Observable<IMandatorAccount[]> {
    const request = new HttpGet(this.bookingAccountUrl(mandatorId));
    return this.repositoryService.httpClient.request(request)
      .pipe(this.repositoryService.mapToItemsOperator<IMandatorAccount>());
  }
  public updateBookingAccounts(
    mandatorId: number,
    allAccountsIds: MandatorKey[]
  ): Observable<IMandatorAccount[]> {
    const request = new HttpPost(this.bookingAccountUrl(mandatorId), allAccountsIds);
    return this.repositoryService.httpClient.request(request)
      .pipe(this.repositoryService.mapToItemsOperator());
  }

  private bookingAccountUrl(mandatorId: number): string {
    return `${this.getBaseUrl()}/${mandatorId}/mandator-booking-accounts`;
  }
}
