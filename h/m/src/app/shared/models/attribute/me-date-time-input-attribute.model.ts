import { FormGroup } from '@angular/forms';
import { MeDateTimeInputI } from '@shared/me-components/me-attribute/me-date-time-input/me-date-time-input.interface';
import { MeInputErrorI } from '@shared/me-components/me-attribute/me-input-error/me-input-error.interface';
import { DMDateTimeAttributeModel } from './attribute.model';

export class MeDateTimeInputAttribute implements MeDateTimeInputI {
  private _data!: DMDateTimeAttributeModel;
  private _form!: FormGroup;
  private _inputErrors!: MeInputErrorI;

  constructor() {}

  public set data(data: DMDateTimeAttributeModel) {
    this._data = data;
  }

  public get data(): DMDateTimeAttributeModel {
    return this._data;
  }

  public set form(form: FormGroup) {
    this._form = form;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get displayName(): string {
    return this.data.displayName;
  }

  public get internalName(): string {
    return this.data.internalName;
  }

  public get required(): boolean {
    return this.data.required;
  }

  public get value(): string {
    return this.data.value;
  }

  public get disabled(): boolean {
    return this.data.disabled;
  }

  public set inputErrors(inputErrors: MeInputErrorI) {
    this._inputErrors = inputErrors;
  }

  public get inputErrors(): MeInputErrorI {
    return this._inputErrors;
  }
}
