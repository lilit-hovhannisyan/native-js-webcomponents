import { NvGridButtonsPosition, NvGridConfig, NvFilterControl, NvGridRowSelectionType, NvColumnDataType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getGridConfig = (): NvGridConfig => {
  return ({
    gridName: 'roleUsersGird',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
        {
          key: 'id',
          title: 'ID',
          visible: false,
        },
        {
          key: 'username',
          title: wording.general.userName,
          width: 150,
          filter: {
            values: []
          }
        },
        {
          key: 'displayName',
          title: wording.general.displayName,
          width: 150,
          filter: {
            values: []
          }
        },
        {
          key: 'lastName',
          title: wording.general.lastName,
          width: 150,
          filter: {
            values: []
          }
        },
        {
          key: 'firstName',
          title: wording.general.firstName,
          width: 150,
          filter: {
            values: []
          }
        },
        {
          key: 'tel',
          title: 'Tel',
          width: 100,
          filter: {
            values: []
          },
        },
        {
          key: 'fax',
          title: 'Fax',
          width: 100,
          filter: {
            values: []
          },
        },
        {
          key: 'email',
          title: 'Email',
          width: 145,
          filter: {
            values: []
          },
        },
        {
          key: 'lastLoggedIn',
          title: wording.general.lastLoggedIn,
          dataType: NvColumnDataType.DateTime,
          filter: {
            values: []
          },
          width: 170,
        },
        {
          key: 'location',
          title: wording.general.location,
          width: 100,
          filter: {
            values: [],
            controlType: NvFilterControl.Select
          },
        },
        {
          key: 'isActive',
          title: wording.general.isActive,
          width: 80,
          filter: {
            values: [],
            controlType: NvFilterControl.Boolean
          },
          dataType: NvColumnDataType.Boolean
        }
    ]
  });
};
