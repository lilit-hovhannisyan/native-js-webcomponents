# MEV FRONTEND GUIDE



This guide contains style and rules all frontend developers should follow.



## Import Order

All import should be sorted alphabetically



1. Core Angular modules (`@angular/*`)



2. Third party modules (`ngx-translate`, `rxjs`, etc.)



3. Shared modules that are located under /shared folder (`@modules`, `@services`, etc.)



4. Components/Services etc. that are related to current component



## Variables

### Css

Use dash case (`#color-red`, `.color-red`, `--color-red`)



### JS/TS

Use camel case (`firstName`)



Define only `private`/`protected` variables (`public` is by default, use it only if needed)



Always return a type on methods even if it's a `void`



Use single quotes `'` when writing string values

