import { Type } from 'class-transformer';

import { BaseModel } from '../base-model/base-model';

import { DocumentAttributesModel } from './document-attributes.model';
import { DocumentAttributesExtendedModel } from './document-attributes-extended.model';

export class MeDocumentModel extends BaseModel {
  creatorOID: string = '';
  ownerOID: string = '';
  @Type(() => DocumentAttributesModel)
  attributes: DocumentAttributesModel = new DocumentAttributesModel();
  @Type(() => DocumentAttributesExtendedModel)
  attributesExtended: DocumentAttributesExtendedModel = new DocumentAttributesExtendedModel();
}
