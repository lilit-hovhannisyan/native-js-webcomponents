import { IResource } from './IResource';
import { UserKey } from './IUser';
import { MandatorKey } from './IMandator';
import { IPaymentMandatorUserConnection } from './IPaymentsMandatorUserConnection';
import { IPermissionGroup, PermissionKey } from './IPermissionGroup';

export type PaymentPermissionKey = PermissionKey;

interface ISharedProps extends IPermissionGroup<IPaymentMandatorUserConnection> {
}

export interface IPaymentPermissionBase extends ISharedProps, IResource<PaymentPermissionKey> { }

export interface IPaymentPermissionCreate extends ISharedProps { }

export interface IPaymentPermissionDTO extends IPaymentPermissionBase { }

export interface IPaymentPermission extends IPaymentPermissionDTO { }

export interface IPaymentMandatorsUsersLists {
  usersIds: UserKey[];
  mandatorsIds: MandatorKey[];
}

