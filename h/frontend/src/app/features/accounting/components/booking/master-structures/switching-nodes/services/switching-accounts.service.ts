import { Injectable } from '@angular/core';
import { IMasterStructureNode, MASTER_STRUCTURE_NODE_TYPE, IMasterStructureNodeTemplate } from 'src/app/core/models/resources/IMasterStructureNode';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { NzFormatBeforeDropEvent, NzTreeNode } from 'ng-zorro-antd/tree';
import { Language } from 'src/app/core/models/language';
import { SwitchingNodesService } from './switching-nodes.service';
import { MasterStructureNodeHelperService } from '../../master-structure-nodes/services/master-structure-node-helper.service';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';

@Injectable({
  providedIn: 'root'
})
export class SwitchingAccountsService {
  private language: Language;

  constructor(
    private nodesHelperService: MasterStructureNodeHelperService,
    private confirmationService: ConfirmationService,
    private switchingNodesService: SwitchingNodesService,
  ) { }

  public setLanguage(language: Language): void {
    this.language = language;
  }

  public hasChildSwitchingAccount(parentNode: IMasterStructureNode): boolean {
    let hasChildSwitchingAccount = false;

    this.nodesHelperService.mapNodes(parentNode, (node: IMasterStructureNode) => {
      if (node.nodeType === MASTER_STRUCTURE_NODE_TYPE.SWITCHING_ACCOUNT) {
        hasChildSwitchingAccount = true;
      }
    });

    return hasChildSwitchingAccount;
  }

  public showSwitchingAccountWarningIfNecessary(
    isInvalidSwitchingAccountDrop: boolean,
  ): void {
    if (isInvalidSwitchingAccountDrop) {
      this.confirmationService.confirm({
        wording: wording.accounting.accountCannotBeAddedToThisNode,
        cancelButtonWording: null,
        title: null,
        okButtonWording: wording.general.ok,
      }).subscribe();
    }
  }

  public createAccountOrSwitchingAccount(account: IBookingAccount): IMasterStructureNode {
    return {
      labelEn: `${account.no} ${account.labelEn}`,
      labelDe: `${account.no} ${account.labelDe}`,
      externalSystemCode: '',
      nodeType: account.isSwitchingAccount
        ? MASTER_STRUCTURE_NODE_TYPE.SWITCHING_ACCOUNT
        : MASTER_STRUCTURE_NODE_TYPE.BOOKING_ACCOUNT,
      bookingAccountId: account.id,
    } as IMasterStructureNode;
  }

  public showContainsSwitchingAccountWarning(switchingNode: IMasterStructureNode): void {
    const label: string = getLabelKeyByLanguage(this.language);

    this.confirmationService.confirm({
      wording: setTranslationSubjects(
        wording.accounting.selectionContainsSwitchingAccounts,
       { NAME: switchingNode[label] }
      ),
      cancelButtonWording: null,
      title: null,
      okButtonWording: wording.general.ok,
    }).subscribe();
  }

  public isValidSwitchingAccountDrop(confirm: NzFormatBeforeDropEvent): boolean {
    const dragNode: NzTreeNode = confirm.dragNode;
    const targetNode: NzTreeNode = confirm.node;

    return dragNode.origin.nodeType === MASTER_STRUCTURE_NODE_TYPE.SWITCHING_ACCOUNT
      && (
        (confirm.pos === 0 && targetNode.key !== dragNode.origin.parentNodeId)
        || (confirm.pos !== 0
          && targetNode.parentNode?.key !== dragNode.origin.parentNodeId)
      );
  }

  public removeSwitchingAccount(
    nodes: IMasterStructureNodeTemplate[],
    foundNode: IMasterStructureNode,
  ): void {
    const parentSwitchingNode: IMasterStructureNode = this.switchingNodesService
      .getParentSwitchingNode(foundNode, nodes);

    this.nodesHelperService.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        if (node.key === parentSwitchingNode.id) {
          node.childNodes = node.childNodes.filter(n => n.bookingAccountId !==
            foundNode.bookingAccountId);
        }
      }
    );
  }

  public removeSwitchingNodeAccounts(
    nodes: IMasterStructureNodeTemplate[],
    foundNode: IMasterStructureNode,
    switchingNodesInChildNodes: {[key: string]: IMasterStructureNodeTemplate},
  ): void {
    if (
      foundNode.switchingNodeId
      || Object.keys(switchingNodesInChildNodes).length > 0
    ) {
      this.nodesHelperService.mapNodes(
        nodes,
        (node: IMasterStructureNodeTemplate) => {
          if (
            node.key === foundNode.switchingNodeId
            || switchingNodesInChildNodes[node.switchingNodeId]
          ) {
            node.hasSwitchingNode = false;
            node.switchingNodeId = null;
            node.childNodes = node.childNodes.filter(n => n.nodeType !==
              MASTER_STRUCTURE_NODE_TYPE.SWITCHING_ACCOUNT);
          }
        }
      );
    }
  }
}
