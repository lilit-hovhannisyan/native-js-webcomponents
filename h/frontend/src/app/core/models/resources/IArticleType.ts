import { IWithActive } from 'src/app/core/models/resources/IWithActive';
import { IWithSystemFlag } from 'src/app/core/models/resources/IWithSystemFlag';
import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';
import { IWithLabels } from './IWithLabels';

export type ArticleTypeKey = number;

interface ISharedProps extends IWithLabels, IWithActive, IWithSystemFlag {
  abbreviation: string;
}

export interface IArticleTypeCreate extends ISharedProps { }

export interface IArticleTypeBase extends ISharedProps, IResource<ArticleTypeKey> { }

export interface IArticleTypeDTO extends IArticleTypeBase { }

export interface IArticleType extends IArticleTypeDTO, IWithLabel { }










