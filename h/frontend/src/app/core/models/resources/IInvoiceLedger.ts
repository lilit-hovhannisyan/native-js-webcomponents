import { IResource } from '../resources/IResource';
import { MandatorKey, IMandatorLookup } from './IMandator';
import { DebitorCreditorKey, IDebitorCreditor } from './IDebitorCreditor';
import { CurrencyKey, ICurrency } from './ICurrency';
import { VoyageKey, IVoyage } from './IVoyage';
import { UserKey, IUser } from './IUser';
import { BookingAccountKey, IBookingAccount } from './IBookingAccount';
import { ICompany } from './ICompany';
import { IWording } from './IWording';
import { VesselAccountingKey, IVesselAccounting } from './IVesselAccounting';
import { IMandatorAccount } from './IMandatorAccount';

export type InvoiceLedgerKey = number;

interface ISharedProps {
  mandatorId: MandatorKey;
  debitorCreditorId: DebitorCreditorKey; // this is supplier
  mandatorAccountId: BookingAccountKey; // this is account
  voyageId?: VoyageKey;
  currencyId: CurrencyKey;
  createdBy: UserKey;
  changedBy: UserKey;
  voucherNumber: number;
  orderNumber?: string;
  year: number;
  invoiceDate: string;
  invoiceNumber: string;
  discount: number;
  paymentDate: string;
  amount: number;
  hasCredit?: boolean;
  taxKey?: string;
  remark?: string;
  barcode?: string;
  receiptDate: string;
  creationDate: string;
  changedDate: string;
  booked?: string;
  vesselAccountingId: VesselAccountingKey;
  isDeleted:	boolean;
  deletionReason: string;
}

export interface IInvoiceLedgerBase extends ISharedProps, IResource<InvoiceLedgerKey> { }

export interface IInvoiceLedgerCreate extends ISharedProps { }

export interface IInvoiceLedgerDTO extends IInvoiceLedgerBase { }

export interface IInvoiceLedger extends IInvoiceLedgerDTO { }

export interface IInvoiceLedgerFull extends IInvoiceLedger {
  mandator: IMandatorLookup;
  debitorCreditor: IDebitorCreditor; // this is supplier
  bookingAccount: IBookingAccount;
  mandatorAccount: IMandatorAccount; // this is account
  vesselAccounting?: IVesselAccounting;
  voyage?: IVoyage;
  currency: ICurrency;
  changedByUser?: IUser;
  createdByUser: IUser;
  company: ICompany;
  bookedView: string;
}

export interface IInvoiceLedgerRow extends IInvoiceLedgerFull {
  mandatorName: string;
  companyName: string;
  mandatorAccountName: IWording;
  vesselName: string;
  voyageName: string;
  currencyLabel: string;
  createdByUserFullName: string;
  changedByUserFullName?: string;
  debitorCreditorNo: number;
}
