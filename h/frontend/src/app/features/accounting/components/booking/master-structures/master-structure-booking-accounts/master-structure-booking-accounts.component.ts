import { Component, OnInit, Input } from '@angular/core';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { getMasterStructureBookingAccountsGridConfig } from './booking-accounts.config';
import { IMasterStructureNodeTemplate } from 'src/app/core/models/resources/IMasterStructureNode';
import { map } from 'rxjs/operators';
import { ACCOUNTING_RELATIONS } from 'src/app/core/models/enums/accounting-relations';
import { MASTER_STRUCTURE_TYPES } from 'src/app/core/models/enums/master-structure-type';
import { IMasterStructure } from 'src/app/core/models/resources/IMasterStructure';

@Component({
  selector: 'nv-master-structure-booking-accounts',
  templateUrl: './master-structure-booking-accounts.component.html',
  styleUrls: ['./master-structure-booking-accounts.component.scss']
})
export class MasterStructureBookingAccountsComponent implements OnInit {
  @Input() public selectedNode: IMasterStructureNodeTemplate;
  @Input() public currentStructure: IMasterStructure;
  @Input() public bookingAccounts$: Observable<IBookingAccount[]>;

  public dataSource$: Observable<IBookingAccount[]>;
  public gridConfig: NvGridConfig;
  public selectedAccounts: IBookingAccount[] = [];
  public wording = wording;

  constructor(
  ) { }

  public ngOnInit(): void {
    this.gridConfig = getMasterStructureBookingAccountsGridConfig(
      this.selectedNode.hasSwitchingNode,
    );
    this.dataSource$ = this.currentStructure
      .masterStructureType === MASTER_STRUCTURE_TYPES.BUSINESS_MANAGEMENT_EVALUATION
        ? this.bookingAccounts$.pipe(
            map(accounts => accounts.filter(a => {
              return this.selectedNode.hasExpenseItem
                ? a.accountingRelation === ACCOUNTING_RELATIONS.PROFITANDLOSSEXPENSES
                : a.accountingRelation === ACCOUNTING_RELATIONS.PROFITANDLOSSEARNINGS;
            }))
          )
        : this.bookingAccounts$.pipe(
            map(accounts => accounts.filter(a => {
              return !this.selectedNode.hasSwitchingNode
                ? !a.isSwitchingAccount
                : true;
            })),
          );
  }

  public rowSelect(rows: IBookingAccount[]) {
    this.selectedAccounts = rows;
  }
}
