import { ContactKey } from './../../models/resources/IContact';
import { Injectable } from '@angular/core';
import { IContactType, IContactTypeDTO, IContactTypeCreate } from '../../models/resources/IContactType';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { TTL_LONG } from 'src/app/shared/services/resource-tracker.service';
import { getLabelKeyByLanguage } from '../../helpers/general';
import { SettingsService } from '../settings.service';

@Injectable({
  providedIn: 'root'
})
export class ContactTypeRepositoryService extends AbstractRepository<
IContactType,
IContactTypeDTO,
IContactTypeCreate,
RepoParams<IContactType>,
ContactKey
> {
  constructor(repositoryService: RepositoryService,
    private settingsService: SettingsService) {
    super(repositoryService);
  }

  protected resourceTTL = TTL_LONG;
  protected useCachePerDefault = true;
  protected resourceType = 'ContactType';
  public sortAsce = true;
  public sortKey = getLabelKeyByLanguage(this.settingsService.language);

  protected postProcess(item: IContactTypeDTO): IContactType { return item; }

  public getBaseUrl(params: RepoParams<IContactType>): string {
    return '/api/contact-types';
  }
}
