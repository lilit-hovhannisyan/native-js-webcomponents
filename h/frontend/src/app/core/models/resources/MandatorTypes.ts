export enum MandatorTypes {
  parentCompany = 0,
  vesselCompany,
  multivesselCompany,
  administrativeCompany
}
