import { Injectable } from '@angular/core';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { ICommissionTypeFull, ICommissionType, ICommissionTypeRow, COMMISSIONTYPES_GROUP } from 'src/app/core/models/resources/ICommissionType';
import { map, switchMap } from 'rxjs/operators';
import { IBookingCode } from '../../models/resources/IBookingCode';
import { CommissionTypeRepositoryService } from '../repositories/commission-type-repository.service';
import { BookingCodeRepositoryService } from '../repositories/booking-code-repository.service';
import { sortBy } from 'lodash';
import { ISelectOption } from '../../models/misc/selectOption';
import { wording } from '../../constants/wording/wording';

export const commissionTypesGroupOptions = [
  { value: COMMISSIONTYPES_GROUP.Chartering, displayLabel: wording.chartering.commissionsTypeGroupChartering },
  { value: COMMISSIONTYPES_GROUP.Broker, displayLabel: wording.chartering.commissionsTypeGroupBroker },
  { value: COMMISSIONTYPES_GROUP.Address, displayLabel: wording.chartering.commissionsTypeGroupAddress },
  { value: COMMISSIONTYPES_GROUP.Other, displayLabel: wording.chartering.commissionsTypeGroupOther },
];

@Injectable({
  providedIn: 'root'
})
export class CommissionTypeMainService {
  private commissionTypesGroup: ISelectOption[] = commissionTypesGroupOptions;

  constructor(
    private commissionTypeRepo: CommissionTypeRepositoryService,
    private bookingCodeRepo: BookingCodeRepositoryService,
  ) { }

  public fetchAll(): Observable<ICommissionTypeFull[]> {
    return combineLatest([
      this.commissionTypeRepo.fetchAll({}),
      this.bookingCodeRepo.fetchAll({}),
    ]).pipe(
      map(res => this.getCommissionTypesFullList(...res)),
    );
  }

  public fetchOne(id: number, params = {}): Observable<ICommissionTypeFull> {
    return this.composeCommissionTypeFull(
      this.commissionTypeRepo.fetchOne(id, params),
      this.bookingCodeRepo.fetchAll({}),
    );
  }

  private composeCommissionTypeFull(
    commissionType$: Observable<ICommissionType>,
    bookingCodes$: Observable<IBookingCode[]>,
  ): Observable<ICommissionTypeFull> {
    return forkJoin([commissionType$, bookingCodes$]).pipe(
      map((res) => this.getCommissionTypeFull(...res))
    );
  }

  private getCommissionTypesFullList(
    commissionTypes: ICommissionType[],
    bookingCodes: IBookingCode[],
  ): ICommissionTypeFull[] {
    return commissionTypes.map(ct => {
      return this.getCommissionTypeFull(ct, bookingCodes);
    });
  }

  private getCommissionTypeFull(
    commissionType: ICommissionType,
    bookingCodes: IBookingCode[],
  ): ICommissionTypeFull {
    const foundBookingCode = bookingCodes.find(bookingCode => bookingCode.id === commissionType.bookingCodeId);
    return {
      ...commissionType,
      bookingCode: foundBookingCode ? foundBookingCode.displayLabel : null
    };
  }

  public getStream(): Observable<ICommissionTypeFull[]> {
    return forkJoin([
      this.commissionTypeRepo.fetchAll({}),
      this.bookingCodeRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.commissionTypeRepo.getStream(),
        this.bookingCodeRepo.getStream(),
      ])),
      map(res => this.getCommissionTypesFullList(...res))
    );
  }

  public toRow(commissionType: ICommissionTypeFull): ICommissionTypeRow {
    const group = this.commissionTypesGroup.find(opt => opt.value === commissionType.group);
    const groupName = group ? group.displayLabel : null;

    return ({
      ...commissionType,
      bookingCodeName: commissionType.bookingCode ? commissionType.bookingCode : null,
      groupName
    });
  }

  public toRows(commissionTypes: ICommissionTypeFull[]): ICommissionTypeRow[] {
    return sortBy(commissionTypes.map(db => this.toRow(db)), ['code']);
  }
}
