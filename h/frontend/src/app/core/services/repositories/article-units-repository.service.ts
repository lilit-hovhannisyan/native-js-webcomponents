import { Injectable } from '@angular/core';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IArticleUnitDTO, IArticleUnit, ArticleUnitKey, IArticleUnitCreate, IArticleUnitBase } from '../../models/resources/IArticleUnit';

@Injectable({
  providedIn: 'root'
})
export class ArticleUnitsRepositoryService extends AbstractRepository<
  IArticleUnit,
  IArticleUnitBase,
  IArticleUnitCreate,
  RepoParams<IArticleUnit>,
  ArticleUnitKey
> {
  protected resourceType = 'ExchangeRate';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IArticleUnitDTO): IArticleUnit => item;

  public getBaseUrl = (): string => '/api/article-units';
}
