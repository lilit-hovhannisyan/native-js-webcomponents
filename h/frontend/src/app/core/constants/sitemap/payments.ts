/* tslint:disable:max-line-length*/
import { SitemapNode } from './sitemap';
import { wording } from '../wording/wording';

export const payments: SitemapNode = {
  path: 'payments',
  description: wording.general.sitemapDescription,
  title: wording.payments.paymentsTitle,
  icon: 'euro',
  zone: 'payments',
  children: {
    misc: {
      path: 'misc',
      title: wording.general.sitemapMisc,
      description: wording.general.sitemapDescription,
      icon: 'assets/icons/88-512.png',
      zone: 'misc',
      children: {
        paymentsPermissions: {
          path: 'payments-permissions',
          description: wording.general.sitemapDescription,
          keywords: [ ],
          title: wording.payments.paymentsPermissionsTitle,
          zone: 'paymentsPermissions',
          children: {
            id: {
              path: ':id',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: wording.accounting.basic,
                },
                history: {
                  path: 'history',
                  zone: '*',
                  title: wording.general.history
                },
              }
            }
          }
        },
      }
    },
  },
};
