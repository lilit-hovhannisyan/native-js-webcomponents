import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { ICompanyLegalEntity, ICompanyLegalEntityCreate, ICompanyLegalEntityDTO, CompanyLegalEntityKey } from '../../models/resources/ICompanyLegalEntity';
import { Observable } from 'rxjs';
import { CompanyKey } from '../../models/resources/ICompany';
import { map } from 'rxjs/operators';
import { CAPITAL_TYPE } from 'src/app/core/models/enums/company-legal-form';


export interface CompanyLegalEntityRepoParams extends RepoParams<ICompanyLegalEntity> {}

@Injectable({
  providedIn: 'root'
})
export class CompanyLegalEntityRepositoryService extends AbstractRepository<
ICompanyLegalEntity,
ICompanyLegalEntityDTO,
ICompanyLegalEntityCreate,
CompanyLegalEntityRepoParams,
CompanyLegalEntityKey
>  {
  protected resourceType = 'CompanyLegalEntity';
  public sortAsce = true;
  public sortKey = 'id';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: ICompanyLegalEntityDTO): ICompanyLegalEntity => item;

  public getBaseUrl(): string {
    return '/api/company-legals';
  }

  // TODO: Move this to a main service
  public fetchOne(
    companyId: CompanyKey,
    options: CompanyLegalEntityRepoParams
  ): Observable<ICompanyLegalEntity> {
    const resourceDefault = { companyId, partners: [], capitalType: CAPITAL_TYPE.ParValueShare } as ICompanyLegalEntity;

    return super.fetchAll(options)
      .pipe(
        map((data: ICompanyLegalEntity[]) => {
          return data.find(legalEntity =>
            legalEntity._type === 'CompanyLegal' && legalEntity.companyId === companyId
          ) || resourceDefault;
        })
      );
  }
}
