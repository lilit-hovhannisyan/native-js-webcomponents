import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { wording } from '../wording/wording';
import { ISilentPartnerRaw } from '../../models/resources/ISilentPartnerRepresentative';
import { formatNumber } from '@angular/common';
import { SettingsService } from '../../services/settings.service';
import { nvLocalMapper } from 'src/app/shared/pipes/localDecimal';
import { IWording } from '../../models/resources/IWording';

export const getLegalEntitySilentPartnersGridConfig = (
  actions: IGridConfigActions,
  canPerform: (operation: Operations) => boolean,
  settingsService: SettingsService,
): NvGridConfig => {
  const localeCode: string = nvLocalMapper[settingsService.locale];
  const digitsInfo = '.2-2';

  return ({
    gridName: 'legalEntitySilentPartnersGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'id',
        visible: false,
      },
      {
        key: 'partnerCompanyName',
        title: wording.system.silentPartner,
        dataType: NvColumnDataType.String,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'contribution',
        title: wording.system.contribution_v1,
        dataType: NvColumnDataType.String,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        customFormatFn: (row: ISilentPartnerRaw) => {
          return formatNumber(row.contribution, localeCode, digitsInfo);
        },
      },
      {
        key: 'currencyIsoCode',
        title: wording.system.currency,
        dataType: NvColumnDataType.String,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'type',
        title: wording.accounting.type,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.String,
        filter: {
          values: []
        },
        customFormatFn: (row: ISilentPartnerRaw) => {
          const valueWording: IWording = row.isAtypicalSilentPartner
            ? wording.system.atypical
            : wording.system.typical;

          return valueWording[settingsService.language];
        },
      },
      {
        key: 'isActive',
        title: wording.general.active,
        width: 75,
        dataType: NvColumnDataType.Boolean,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: ['1']
        },
      },
      {
        key: 'from',
        title: wording.general.from,
        dataType: NvColumnDataType.Date,
        width: 150,
        isSortable: true,
        hidden: true,
        filter: {
          values: []
        }
      },
      {
        key: 'to',
        title: wording.general.to,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.Date,
        hidden: true,
        filter: {
          values: []
        }
      },
      {
        key: 'remark',
        title: wording.system.remark,
        dataType: NvColumnDataType.String,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editSilentPartner',
        tooltip: wording.general.edit,
        actOnDoubleClick: true,
        func: actions.edit,
        hidden: !canPerform(Operations.UPDATE)
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteSilentPartner',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        func: actions.delete,
        hidden: !canPerform(Operations.DELETE)
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        func: actions.add,
        hidden: () => !canPerform(Operations.CREATE)
      }
    ],
  });
};
