import { Component, OnInit } from '@angular/core';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { ActivatedRoute } from '@angular/router';
import { RemarksRepositoryService, IMandatorRemarkRepoParams } from 'src/app/core/services/repositories/remarks-repository.service';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { IMandatorRemarkCreate, IMandatorRemark, IMandatorRemarkFull } from 'src/app/core/models/resources/IMandatorRemark';
import { FormBuilder } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { catchError, switchMap, map, tap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { of, Observable, forkJoin } from 'rxjs';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { getDateTimeFormat } from 'src/app/shared/helpers/general';
import * as moment from 'moment';
import { IUser } from 'src/app/core/models/resources/IUser';
import { MandatorMainService } from 'src/app/core/services/mainServices/mandator-main.service';
import { IMandatorFull } from 'src/app/core/models/resources/IMandator';
@Component({
  selector: 'nv-mandator-remarks',
  templateUrl: './mandator-remarks.component.html',
  styleUrls: ['./mandator-remarks.component.scss']
})
export class MandatorRemarksComponent extends ResourceDetailView<
IMandatorRemark,
IMandatorRemarkCreate
> implements OnInit {
  protected repoParams: IMandatorRemarkRepoParams;
  protected resourceDefault: IMandatorRemarkFull = {} as IMandatorRemarkFull;
  public resource: IMandatorRemarkFull;
  private mandatorId: number;
  public routeBackUrl?: string;
  private dateTimeFormat: string;
  private mandator: IMandatorFull;

  constructor(
    public baseComponentService: BaseComponentService,
    public remarksRepository: RemarksRepositoryService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private userRepo: UserRepositoryService,
    private mandatorService: MandatorMainService,
  ) {
    super(remarksRepository, baseComponentService);
  }

  public ngOnInit() {
    this.dateTimeFormat = getDateTimeFormat(this.settingsService.locale);
    this.mandatorId = parseInt(this.route.parent.snapshot.params.id, 10);
    this.resourceDefault = { ...this.resourceDefault, mandatorId: this.mandatorId };

    this.repoParams = {
      mandatorId: this.mandatorId,
      errorHandler: () => true,
    };
    this.fetchRemarks();
  }

  public getTitle = () => '';

  private fetchRemarks() {
    this.mandatorService.fetchOne(+this.mandatorId, {})
      .pipe(
        tap(mandator => this.mandator = mandator),
        switchMap(() => this.remarksRepository.getRemarkByMandator(this.repoParams)),
        map(data => data || this.resourceDefault),
        catchError(this.errorHandler),
        switchMap(remark => {
          if (remark.createdBy && remark.createdBy === remark.editedBy) {
            return this.userRepo.fetchOne(remark.createdBy, {})
              .pipe(map(user => {
                return {
                  ...remark,
                  createdByFull: this.formatUserAndDateTime(user, remark.createdAt),
                  editedByFull: this.formatUserAndDateTime(user, remark.editedAt),
                };
              }));
          } else if (remark.createdBy && !remark.editedBy) {
            return this.userRepo.fetchOne(remark.createdBy, {})
              .pipe(map(createdBy => {
                return {
                  ...remark,
                  createdByFull: this.formatUserAndDateTime(createdBy, remark.createdAt),
                  editedByFull: '',
                };
              }));
          } else if (remark.editedBy) {
            return forkJoin([
              this.userRepo.fetchOne(remark.createdBy, {}),
              this.userRepo.fetchOne(remark.editedBy, {}),
            ]).pipe(map(([createdBy, editedBy]) => {
              return {
                ...remark,
                createdByFull: this.formatUserAndDateTime(createdBy, remark.createdAt),
                editedByFull: this.formatUserAndDateTime(editedBy, remark.editedAt),
              };
            }));
          } else {
            return of({
              ...remark,
              createdByFull: '',
              editedByFull: '',
            });
          }
        }),
      ).subscribe((remark: IMandatorRemarkFull) => this.initializeResource(remark));
  }

  private errorHandler = (err: HttpErrorResponse): Observable<IMandatorRemarkFull> => {
    if (err.status === 404) {
      return of(this.resourceDefault);
    }
    this.messageService.error(err.message);
    throw new Error(err.message);
  }

  public init(remark: IMandatorRemark = this.resourceDefault): void {
    this.initForm(this.formBuilder.group({
      text: [remark.text, [Validators.required, Validators.maxLength(4000)]]
    }));
  }

  public submit() {
    if (!this.form.valid) { return; }

    const payload = {
      ...this.resource,
      text: this.form.value.text,
    };

    this.leaveEditMode();

    this.resource.id
      ? this.remarksRepository.update(payload, this.repoParams)
        .subscribe(() => this.fetchRemarks())
      : this.remarksRepository.create(payload, this.repoParams)
        .subscribe(() => this.fetchRemarks());
  }

  private formatUserAndDateTime(user: IUser, dateTime: string): string {
    return `${moment.utc(dateTime).local()
      .format(this.dateTimeFormat)} / ${user.firstName} ${user.lastName}`;
  }
}
