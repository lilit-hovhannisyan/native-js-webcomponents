import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CompanyKey } from 'src/app/core/models/resources/ICompany';
import { ITrusteePartnerContribution, TrusteePartnerContributionKey, ITrusteePartnerContributionDTO, ITrusteePartnerContributionCreate } from '../../models/resources/ITrusteePartnerContribution';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { COMPANY_CONTRIBUTION_TYPES } from '../../../features/system/shared/trustee-partner-contributions/enums/company-contributions';


export interface ITrusteePartnerFilterParams extends Partial<ITrusteePartnerContribution> {
  _type: string;
}

export interface ITrusteePartnerContributionRepoParams extends RepoParams<ITrusteePartnerContribution> {
  queryParams?: {
    cid: CompanyKey;
  };
  contributorType: COMPANY_CONTRIBUTION_TYPES;
}

@Injectable({
  providedIn: 'root'
})
export class TrusteePartnerContributionsRepositoryService extends AbstractRepository<
ITrusteePartnerContribution,
ITrusteePartnerContributionDTO,
ITrusteePartnerContributionCreate,
ITrusteePartnerContributionRepoParams,
TrusteePartnerContributionKey
>  {
  protected resourceType = 'TrusteePartnerContribution';
  protected sortKey = 'label';
  protected sortAsce = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = (params: ITrusteePartnerContributionRepoParams) => {
    return params.contributorType === COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution
      ? '/api/sub-participant-contributions'
      : '/api/trustor-contributions';
  }

  protected postProcess = (item: ITrusteePartnerContributionDTO): ITrusteePartnerContribution => item;

  public fetchTrusteePartnerContributions(
    companyId: CompanyKey,
    contributorType: COMPANY_CONTRIBUTION_TYPES
  ): Observable<ITrusteePartnerContribution[]> {
    return this.fetchAll({ queryParams: { cid: companyId }, contributorType }).pipe(
      map((contributions: ITrusteePartnerContribution[]) => {
        return contributions.filter(c => c.companyId === companyId);
      })
    );
  }
}
