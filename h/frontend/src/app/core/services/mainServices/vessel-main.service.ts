import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { combineLatest, Observable, forkJoin } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { IVessel, VesselKey, IVesselWithAccountingsRow } from 'src/app/core/models/resources/IVessel';
import { IVesselPreDelayCharterName } from 'src/app/core/models/resources/IVesselPreDelayCharterName';
import { IVoyageCharterName } from 'src/app/core/models/resources/IVoyageCharterName';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';
import { IVesselRepoParams, VesselRepositoryService } from 'src/app/core/services/repositories/vessel-repository.service';
import { VoyageCharternameRepositoryService } from 'src/app/core/services/repositories/voyage-chartername-repository.service';
import { arrayToKeyHashTable, findClosestWithFromUntil } from '../../helpers/general';
import { VoyagesMainService } from './voyages-main.service';
import { IVesselAccounting, VesselAccountingKey } from '../../models/resources/IVesselAccounting';
import { IVoyageFull } from '../../models/resources/IVoyage';
import { sortBy } from 'lodash';
import { IVesselAccountingListItem } from '../../models/resources/IVesselAccounting';
import { RepoParams } from '../../abstracts/abstract.repository';
import { VesselRoleRepositoryService } from '../repositories/vessel-role.repository.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { IVesselRole } from '../../models/resources/IVesselRole';
import { IUser } from '../../models/resources/IUser';
import { RoleKey } from '../../models/resources/IRole';
import { IWithFromUntil } from '../../models/resources/IWithFromUntil';

@Injectable({ providedIn: 'root' })
export class VesselMainService {
  constructor(
    private vesselRepo: VesselRepositoryService,
    private vesselRoleRepo: VesselRoleRepositoryService,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
    private charternameRepo: VoyageCharternameRepositoryService,
    private voyagesMainService: VoyagesMainService,
    private authService: AuthenticationService,
  ) { }

  public fetchAll(
    params: IVesselRepoParams = {}
  ): Observable<IVesselWithAccountingsRow[]> {
    this.vesselRepo.fetchAll(params).subscribe();
    this.vesselAccountingRepo.fetchAll({}).subscribe();

    return combineLatest([
      this.vesselRepo.getStream(),
      this.vesselAccountingRepo.getStream(),
      this.charternameRepo.fetchAll({}),
      this.voyagesMainService.fetchAll({}),
      this.charternameRepo.fetchAllPreDeliveryCharternames(),
    ]).pipe(map(([vessels, accountings, charternames, voyages, predeliveries]) => {
      const predeliveriesKeyHashTable = arrayToKeyHashTable(predeliveries, 'vesselId');

      return vessels.map(vessel => {
        const vesselAccountings = accountings.filter(elem => elem.vesselId === vessel.id);
        const vesselAccountingsKeyHashTable = arrayToKeyHashTable(vesselAccountings);
        const vesselVoyages = voyages.filter(voyage =>
          vesselAccountingsKeyHashTable[voyage.vesselAccountingId]
        );

        const vesselVoyagesKeyHashTable = arrayToKeyHashTable(vesselVoyages);
        const vesselCharternames = charternames.filter(chartername =>
          vesselVoyagesKeyHashTable[chartername.voyageId]
        );

        const preDeliveryChartername = predeliveriesKeyHashTable[vessel.id];
        const lastVesselAccounting: IVesselAccounting = findClosestWithFromUntil(vesselAccountings);

        return {
          ...vessel,
          vesselAccountings,
          lastVesselAccounting,
          vesselCharternames,
          lastVesselChartername: this.findClosestChartername(vesselCharternames, preDeliveryChartername),
          lastVesselAccountingNo: lastVesselAccounting?.vesselNo,
          lastVesselAccountingName: lastVesselAccounting?.vesselName,
        };
      });
    }));
  }

  public findClosestChartername(
    charternames: IVoyageCharterName[],
    vesselPreDelivery?: IVesselPreDelayCharterName,
  ): undefined | IVoyageCharterName {
    if (!charternames.length && !vesselPreDelivery) {
      return undefined;
    }

    const charternamesWithPreDelivery = [...charternames];

    if (vesselPreDelivery) {
      const preDeliveryChartername = {
        ...vesselPreDelivery,
        from: vesselPreDelivery.preDelay,
      } as any as IVoyageCharterName;

      charternamesWithPreDelivery.push(preDeliveryChartername);
    }

    // On particular cases, the backend can give not sorted array.
    // For example seeded data can be not sorted.
    const sortedCharternames = charternamesWithPreDelivery.sort((a: IVoyageCharterName, b: IVoyageCharterName) => {
      return moment(a.from).isAfter(b.from) ? 1 : -1;
    });

    const pastCharternames = sortedCharternames.filter(chartername => {
      return moment().diff(chartername.from, 'minutes') >= 0;
    });

    // If there are past charternames return last one
    if (pastCharternames.length) {
      /**
       * If exists another past chartername than preDelayChartername, then return that one.
       */
      const lastChartername: IVoyageCharterName = pastCharternames[pastCharternames.length - 1];
      if (lastChartername._type !== 'VesselPreDelayCharterName') {
        return lastChartername;
      }

      return pastCharternames[pastCharternames.length - 2] || lastChartername;
    }

    const futureCharternames = sortedCharternames.filter(chartername => {
      return moment().diff(chartername.from, 'minutes') < 0;
    });

    // If there are future charternames return closest one
    if (futureCharternames.length) {
      /*
        In case when closest one is PreDelivered
        and there is another chartername on same day,
        then take chartername
      */
      if (
        futureCharternames.length > 1
        && futureCharternames[0]._type === 'VesselPreDelayCharterName'
        && futureCharternames[0].from === futureCharternames[1].from
      ) {
        return futureCharternames[1];
      }

      return futureCharternames[0];
    }

    // In case there are charternames with wrong date (from)
    return sortedCharternames[sortedCharternames.length - 1];
  }

  public findClosestWithFromUntil<T extends IWithFromUntil>(
    vesselAccountings: T[],
    shouldGetOnlyFutureEntity = false,
  ): undefined | T {
    if (!vesselAccountings.length) {
      return undefined;
    }

    // On particular cases, the backend can give not sorted array.
    // For example seeded data can be not sorted.
    const sortedAccountings = vesselAccountings.sort((a: T, b: T) => {
      return moment(a.from).isAfter(b.from) ? 1 : -1;
    });

    const pastAccountings = sortedAccountings.filter(accounting => {
      return moment().diff(accounting.from, 'minutes') >= 0;
    });

    // If there are past accountings return last one
    if (pastAccountings.length && !shouldGetOnlyFutureEntity) {
      return pastAccountings[pastAccountings.length - 1];
    }

    const futureAccountings = sortedAccountings.filter(vesselAccounting => {
      return moment().diff(vesselAccounting.from, 'minutes') < 0;
    });

    // If there are future accountings return first one
    if (futureAccountings.length) {
      return futureAccountings[0];
    }

    // if we reach here, it means that there isn't any future entity, and if
    // `shouldGetOnlyFutureEntity` is true, we should return undefined as there isn't any
    // future entity
    if (shouldGetOnlyFutureEntity) {
      return undefined;
    }

    // In case there are accountings with wrong date (from)
    return sortedAccountings[sortedAccountings.length - 1];
  }

  public fetchVesselsWithClosestAccounting(
    onlyWithRoles = false
  ): Observable<IVesselAccountingListItem[]> {
    return forkJoin([
      this.vesselRepo.fetchAll({}),
      this.vesselAccountingRepo.fetchAll({}),
      this.charternameRepo.fetchAll({}),
      this.charternameRepo.fetchAllPreDeliveryCharternames(),
      this.voyagesMainService.fetchAll(),
      this.vesselRoleRepo.fetchAll({}),
    ]).pipe(
      map(([
        vessels,
        allVesselAccountings,
        voyageCharterNames,
        charterNames,
        voyages,
        roles,
      ]) => {
        const vesselsToMap: IVessel[] = onlyWithRoles
          ? sortBy(
            vessels.filter(v => this.vesselIsAssignedToRole(v.id, roles)),
            ['imo']
          )
          : vessels;

        return vesselsToMap
          .map(vessel => {
            const vesselAccountings: IVesselAccounting[] = allVesselAccountings
              .filter(vesselAccounting => vesselAccounting.vesselId === vessel.id);
            return findClosestWithFromUntil(vesselAccountings);
          })
          .filter(v => v)
          .map(vesselAccounting => {
            const vesselPreDelivery: IVesselPreDelayCharterName = charterNames
              .find(c => c.vesselId === vesselAccounting.vesselId);

            const otherVesselAccountingIdsRelatedToSameVessel: VesselAccountingKey[] = allVesselAccountings
              .filter(v => v.vesselId === vesselAccounting.vesselId)
              .map(v => v.id);

            const vesselVoyages: IVoyageFull[] = voyages
              .filter(v => otherVesselAccountingIdsRelatedToSameVessel.includes(v.vesselAccountingId));

            const vesselVoyageCharterNames: IVoyageCharterName[] = voyageCharterNames
              .filter(voyageCharterName => {
                return vesselVoyages.find(v => voyageCharterName.voyageId === v.id);
              });
            const charterName: string = this
              .findClosestChartername(vesselVoyageCharterNames, vesselPreDelivery)?.charterName;
            return {
              ...vesselAccounting,
              charterName,
              vesselVoyages,
            };
          });
      }),
      map(vesselAccountings => sortBy(vesselAccountings, 'vesselName'))
    );
  }

  public getVesselsWithClosestAccountingStream(
    onlyWithRoles = false
  ): Observable<IVesselAccountingListItem[]> {
    return combineLatest([
      this.vesselRepo.getStream(),
      this.vesselAccountingRepo.getStream(),
      this.charternameRepo.fetchAll({}), // doesn't have getStream
      this.charternameRepo.fetchAllPreDeliveryCharternames(),
      this.voyagesMainService.getStream(),
      this.vesselRoleRepo.getStream(),
    ]).pipe(
      map(([
        vessels,
        allVesselAccountings,
        voyageCharterNames,
        charterNames,
        voyages,
        roles,
      ]) => {
        const vesselsToMap: IVessel[] = onlyWithRoles
          ? sortBy(
            vessels.filter(v => this.vesselIsAssignedToRole(v.id, roles)),
            ['imo']
          )
          : vessels;

        return vesselsToMap
          .map(vessel => {
            const vesselAccountings: IVesselAccounting[] = allVesselAccountings
              .filter(vesselAccounting => vesselAccounting.vesselId === vessel.id);
            return findClosestWithFromUntil(vesselAccountings);
          })
          .filter(v => v)
          .map(vesselAccounting => {
            const vesselPreDelivery: IVesselPreDelayCharterName = charterNames
              .find(c => c.vesselId === vesselAccounting.vesselId);

            const otherVesselAccountingIdsRelatedToSameVessel: VesselAccountingKey[] = allVesselAccountings
              .filter(v => v.vesselId === vesselAccounting.vesselId)
              .map(v => v.id);

            const vesselVoyages: IVoyageFull[] = voyages
              .filter(v => otherVesselAccountingIdsRelatedToSameVessel.includes(v.vesselAccountingId));

            const vesselVoyageCharterNames: IVoyageCharterName[] = voyageCharterNames
              .filter(voyageCharterName => {
                return vesselVoyages.find(v => voyageCharterName.voyageId === v.id);
              });
            const charterName: string = this
              .findClosestChartername(vesselVoyageCharterNames, vesselPreDelivery)?.charterName;
            return {
              ...vesselAccounting,
              charterName,
              vesselVoyages,
            };
          });
      }),
      map(vesselAccountings => sortBy(vesselAccountings, 'vesselName'))
    );
  }

  /**
   * Checks if user has permission(Role) for a vessel
   */
  public vesselIsAssignedToRole = (
    vesselId: VesselKey,
    vesselRoles: IVesselRole[]
  ): boolean => {
    const user: IUser = this.authService.getUser();
    const userRoles: RoleKey[] = this.authService.getRolesIds();
    return (user.isSuperAdmin || user.isAdmin)
      || !!vesselRoles
        .filter(vesselRole => userRoles.includes(vesselRole.roleId))
        .find(vesselRole => vesselRole.vesselId === vesselId);
  }

  public fetchWithRolesOnly(
    params: RepoParams<IVessel> = {},
  ): Observable<IVesselWithAccountingsRow[]> {
    return combineLatest([
      this.vesselRoleRepo.fetchAll({}),
      this.fetchAll(params),
    ]).pipe(
      map(([roles, vessels]) =>
        sortBy(
          vessels.filter(v => this.vesselIsAssignedToRole(v.id, roles)),
          ['imo']
        )
      ),
      startWith([]),
    );
  }
}
