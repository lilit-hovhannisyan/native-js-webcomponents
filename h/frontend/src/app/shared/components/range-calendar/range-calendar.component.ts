import { map, takeUntil, tap } from 'rxjs/operators';
import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  Input,
  OnDestroy
} from '@angular/core';
import * as moment from 'moment';
import { IExchangeRate, IForeignExchangeRate } from 'src/app/core/models/resources/IExchangeRate';
import { FormGroup, FormBuilder, ValidationErrors } from '@angular/forms';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { Validators } from 'src/app/core/classes/validators';
import { Observable, forkJoin, ReplaySubject, BehaviorSubject } from 'rxjs';
import { copyToClipboard } from 'src/app/core/helpers/general';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { DatesRatesTableComponent } from '../dates-rates-table/dates-rates-table.component';
import { gridConfig } from 'src/app/shared/components/autocomplete/modals/currency-grid-config';
import { NvGridConfig } from 'nv-grid';
import { ExchangeRateRepositoryService } from 'src/app/core/services/repositories/exchange-rate-repository.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { NotificationType } from 'src/app/core/models/INotification';
import { SettingsService } from 'src/app/core/services/settings.service';
import { nvLocalMapper } from '../../pipes/localDecimal';

/**
 * This component shows a calendar with rates, it fetches rates, quarter by quarter and
 * caches the fetched ones
 */

@Component({
  selector: 'nv-range-calendar',
  templateUrl: './range-calendar.component.html',
  styleUrls: ['./range-calendar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RangeCalendarComponent implements OnInit, OnDestroy {
  @Input() public localCurrencyId: number;
  @Input() public currencies: ICurrency[] = [];
  @Input() public isLocalCurrencyDisabled = true;
  public form: FormGroup;
  public currencyGridConfig: NvGridConfig;
  public swapped: boolean;
  public wording = wording;
  public nvLocalMapper = nvLocalMapper;
  public foreignRates: BehaviorSubject<IForeignExchangeRate> = new BehaviorSubject<IForeignExchangeRate>({});
  public midRate: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public label: string;
  public calendarDate: Date = moment().utc()
    .subtract(1, 'month').startOf('month').startOf('day').toDate();
  private modalRef: NzModalRef;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  private midRateRates: IExchangeRate[] = [];

  constructor(
    private fb: FormBuilder,
    private modalService: NzModalService,
    private exchangeRate: ExchangeRateRepositoryService,
    private notificationService: NotificationsService,
    public settingsService: SettingsService,
  ) { }

  public ngOnInit() {
    this.currencyGridConfig = gridConfig;
    this.initForm();
    this.calculateMidRateOfRange();
    this.fetchRates();
  }

  public swap(): void {
    this.swapped = !this.swapped;
    const newMidRate = this.midRate.value ? 1 / this.midRate.value : this.midRate.value;
    this.midRate.next(newMidRate);
  }

  private initForm(): void {
    const euroCurrency = this.currencies.find(currency => currency.isoCode === 'EUR');
    const usdCurrency = this.currencies.find(currency => currency.isoCode === 'USD');
    let foundLocalCurrency = this.currencies
      .find(currency => currency.id === this.localCurrencyId);
    if (!foundLocalCurrency) {
      foundLocalCurrency = euroCurrency;
    }
    this.form = this.fb.group({
      from: [moment().utc().subtract(1, 'month').startOf('month').startOf('day').toISOString()],
      until: [moment().utc().subtract(1, 'month').endOf('month').startOf('day').toISOString(),
      [Validators.isAfterFieldDate('from')]],

      localCurrency: [{
        value: foundLocalCurrency.isoCode,
        disabled: this.isLocalCurrencyDisabled
      }, Validators.required],
      foreignCurrency: [foundLocalCurrency.id === euroCurrency.id
        ? usdCurrency.isoCode
        : euroCurrency.isoCode, Validators.required
      ],
    }, {
      validators: [this.startEndDateValidator]
    });

    const { localCurrency, foreignCurrency } = this.form.controls;
    foreignCurrency.valueChanges.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(() => this.fetchRates());

    localCurrency.valueChanges.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(() => this.fetchRates());

    this.form.valueChanges.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(() => this.form.valid && this.calculateMidRateOfRange());
  }

  private startEndDateValidator = (formGroup: FormGroup): ValidationErrors | null => {
    const { from, until } = formGroup.controls;

    if (from.value && until.value) {
      const range = moment(until.value).diff(moment(from.value), 'days');
      const message = wording.general.rangeCannotBeGreaterThanAYear;
      return range > 365
        ? { error: { message } }
        : null;
    }
    return null;
  }

  private fetchRates(): void {
    const { localCurrency, foreignCurrency } = this.form.controls;
    const foundLocalCurrency = this.currencies
      .find(currency => currency.isoCode === localCurrency.value);
    const foundForeignCurrency = this.currencies
      .find(currency => currency.isoCode === foreignCurrency.value);

    if (foundLocalCurrency && foundForeignCurrency) {
      forkJoin([
        this.fetchRatesOfCurrency(foundLocalCurrency.id, this.calendarDate),
        this.fetchRatesOfCurrency(foundForeignCurrency.id, this.calendarDate)
      ])
        .pipe(
          takeUntil(this.componentDestroyed$),
          map(([
            localRates,
            foreignRates
          ]) => this.mapCurrencyRates(localRates, foreignRates)),
          map(rates => this.convertExchangeRatesToObject(rates)))
        .subscribe(foreignRates => {
          this.foreignRates.next({ ...this.foreignRates.value, ...foreignRates });
        });
    }
  }

  public mapCurrencyRates(
    localRates: IExchangeRate[],
    foreignRates: IExchangeRate[]
  ): IExchangeRate[] {
    return localRates.map(rate => {
      const foundForeignCurrency = foreignRates
        .find(foreignRate => foreignRate.date === rate.date);
      return {
        ...foundForeignCurrency,
        rate: foundForeignCurrency ? (foundForeignCurrency.rate / rate.rate) : null
      };
    });
  }

  private convertExchangeRatesToObject(exchangeRates: IExchangeRate[]): IForeignExchangeRate {
    const initialValue = {};
    return exchangeRates.reduce((object, exchangeRate) => {
      return {
        ...object,
        [moment(exchangeRate.date).format('DDMMYYYY')]: exchangeRate,
      };
    }, initialValue);
  }

  public openHistoryModal(): void {
    if (this.form.valid) {
      this.modalRef = this.modalService.create({
        nzFooter: null,
        nzWidth: 'fit-content',
        nzTitle: wording.general.midrateCalculation[this.settingsService.language],
        nzClosable: true,
        nzContent: DatesRatesTableComponent,
        nzComponentParams: {
          rates: this.midRateRates.map(rate => ({
            date: rate.date,
            rate: this.swapped && rate.rate ? (1 / rate.rate) : rate.rate
          })),
        },
        nzOnCancel: () => this.modalRef.destroy(),
        nzWrapClassName: 'history-modal'
      });
    }
  }

  private fetchRatesOfCurrency(currencyId: number, date: Date): Observable<IExchangeRate[]> {
    const startOfQuarter = moment(date)
      .quarter(moment(date).quarter()).startOf('quarter');
    const endOfQuarter = moment(date)
      .quarter(moment(date).quarter()).endOf('quarter');
    return this.fetchDateRangeRates(
      startOfQuarter.toDate(),
      endOfQuarter.toDate(),
      currencyId
    );
  }

  public exportToExcel(): void {
    const { foreignCurrency, from, until } = this.form.controls;
    const startDate = moment(from.value).toISOString();
    const untilDate = moment(until.value).toISOString();
    const foundForeignCurrency = this.currencies
      .find(currency => currency.isoCode === foreignCurrency.value);

    if (foundForeignCurrency) {
      this.exchangeRate.createDocument({
        startDate: startDate,
        endDate: untilDate,
        currencyId: foundForeignCurrency.id
      }).pipe(
        takeUntil(this.componentDestroyed$)
      ).subscribe(() => {
        this.notificationService.notify(
          NotificationType.Success,
          wording.general.newDocument,
          wording.general.newDocumentRequest
        );
      });
    }
  }

  private calculateMidRateOfRange(): void {
    const { until, from, localCurrency, foreignCurrency } = this.form.controls;
    const fromDate = moment(from.value).toDate();
    const untilDate = moment(until.value).toDate();

    const foundLocalCurrency = this.currencies
      .find(currency => currency.isoCode === localCurrency.value);
    const foundForeignCurrency = this.currencies
      .find(currency => currency.isoCode === foreignCurrency.value);

    if (foundLocalCurrency && foundForeignCurrency) {
      forkJoin([
        this.fetchDateRangeRates(fromDate, untilDate, foundLocalCurrency.id),
        this.fetchDateRangeRates(fromDate, untilDate, foundForeignCurrency.id)
      ])
        .pipe(
          takeUntil(this.componentDestroyed$),
          map(([
            localRates,
            foreignRates
          ]) => this.mapCurrencyRates(localRates, foreignRates)),
          tap(rates => {
            this.updateLabel();
            this.midRateRates = rates;
            const newMidRate = rates.reduce((prev, b) => {
              const rate = b.rate;
              if (rate) {
                return prev + rate;
              }
              return prev;
            }, 0) / rates.length;
            this.midRate.next(newMidRate);
          })).subscribe();
    }
  }

  private fetchDateRangeRates(
    startDate: Date,
    endDate: Date,
    currencyId: number
  ): Observable<IExchangeRate[]> {

    return this.exchangeRate.fetchAll({
      queryParams: {
        startDate: startDate.toISOString(),
        endDate: endDate.toISOString(),
        currencyId: currencyId,
      }
    });
  }

  public copyToClipboard(): void {
    if (this.midRate) {
      copyToClipboard(this.midRate.value.toFixed(5));
    }
  }

  public changes(date: Date): void {
    if (!this.foreignRates.value[moment(date).format('DDMMYYYY')]) {
      this.fetchRates();
    }
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  private updateLabel(): void {
    this.label = `${this.form.get('localCurrency').value}/${this.form.get('foreignCurrency').value}`;
  }
}
