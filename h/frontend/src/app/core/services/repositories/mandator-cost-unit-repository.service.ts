import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IMandatorCostUnit, MandatorCostUnitKey, IMandatorCostUnitCreate, IMandatorCostUnitDTO } from '../../models/resources/IMandatorCostUnit';
import { RepositoryService } from '../repository.service';
import { Observable } from 'rxjs';
import { HttpPost } from '../../models/http/http-post';
import { map, tap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';
import { IResourceCollection } from '../../models/resources/IResourceCollection';
import { cleanUrl, sortByKey, urlConcat } from '../../helpers/general';
import { CostUnitKey } from '../../models/resources/ICostUnit';
import { HttpDelete } from '../../models/http/http-delete';

@Injectable({
  providedIn: 'root'
})
export class MandatorCostUnitRepositoryService extends AbstractRepository<
  IMandatorCostUnit,
  IMandatorCostUnitDTO,
  IMandatorCostUnitCreate,
  RepoParams<IMandatorCostUnit>,
  MandatorCostUnitKey
> {
  protected resourceType = 'MandatorCostUnits';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/mandator-cost-units';
  }

  protected postProcess(item: IMandatorCostUnitDTO): IMandatorCostUnit { return item; }

  public updateAll(entities: IMandatorCostUnit[]): Observable<IMandatorCostUnit[]> {
    const url = this.getBaseUrl();
    const request = new HttpPost(url, entities);

    return this.repositoryService.httpClient.request(request).pipe(
      map((response: HttpResponse<IDataResponseBody<
        IResourceCollection<IMandatorCostUnitDTO>>>) => response.body.data.items),
      map((items) => items.map((dto) => this.postProcess(dto))),
      map((items) => items.sort(sortByKey(this.sortKey, this.sortAsce))),
      tap((items) => this.updateStream(items)),
    );
  }

  /**
   * This method is created separately and AbstractRepository.delete is not used here,
   * because this method usually removes all entities at once and it needs to update
   * the stream in a different way, also AbstractRepository.delete will do it wrong,
   * because it will think that the id parameter belongs to IMandatorCostUnit and will
   * try to find and remove it from the stream by that id, but the id parameter belongs to
   * ICostUnit, so AbstractRepository.delete will not work for this case correctly.
   */
  public deleteByCostUnitId(
    costUnitId: CostUnitKey,
    options: RepoParams<IMandatorCostUnit>,
  ): Observable<undefined> {
    const baseUrl: string = options.baseUrl || this.getBaseUrl();
    const url: string = cleanUrl(urlConcat([baseUrl, costUnitId]));
    const request: HttpDelete<IMandatorCostUnit> = new HttpDelete(
      url,
      {
        isNotificationHidden: options.isNotificationHidden,
        init: {
          params: options.queryParams,
        }
      }
    );

    return this.repositoryService.httpClient.request(request).pipe(
      tap(() => this.updateStream([])),
      // delete request shouldn't return anything back, the same approach used in
      // AbstractRepository.delete method
      map(() => undefined),
    );
  }

}
