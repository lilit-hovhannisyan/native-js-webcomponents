import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { CompanyCategoryRepositoryService } from 'src/app/core/services/repositories/company-category-repository.service';
import { ICategory } from 'src/app/core/models/resources/ICategory';
import { CategoryRepositoryService } from 'src/app/core/services/repositories/category-repository.service';
import { ICompanyCategoryFull, ICompanyCategory } from 'src/app/core/models/resources/ICompanyCategory';
import { getCompanyCategoriesTabGridConfig } from 'src/app/core/constants/nv-grid-configs/company-categories-tab.config';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Operations } from 'src/app/core/models/Operations';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { CompanyCategoriesModalComponent } from './company-categories-modal/company-categories-modal.component';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { SettingsService } from 'src/app/core/services/settings.service';
import { cloneDeep } from 'lodash';

@Component({
  templateUrl: './company-categories-tab.component.html',
  styleUrls: ['./company-categories-tab.component.scss'],
})
export class CompanyCategoriesTabComponent implements OnInit {
  public companyId: number;
  public dataSource$: BehaviorSubject<ICompanyCategoryFull[]> = new BehaviorSubject([]);
  public gridConfig: NvGridConfig;
  public activeCategories: ICategory[] = [];

  constructor(
    private route: ActivatedRoute,
    private companyCategoriesRepo: CompanyCategoryRepositoryService,
    private categoriesRepo: CategoryRepositoryService,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private modalService: NzModalService,
    private settingsService: SettingsService,
  ) { }

  public ngOnInit() {
    this.companyId = +this.route.parent.snapshot.params.id;
    this.categoriesRepo.fetchAll({}).subscribe(allCategories => {
      this.activeCategories = allCategories.filter(c => c.isActive);

      this.getCompanyCategories();
      this.setGridConfig();
    });
  }

  private getCompanyCategories(): void {
    this.companyCategoriesRepo.fetchAll({ companyId: this.companyId }).subscribe(companyCategories => {
      this.dataSource$.next(this.companyCategories2Full(companyCategories));
    });
  }

  private companyCategories2Full(companyCategories: ICompanyCategory[]): ICompanyCategoryFull[] {
    return companyCategories.map(companyCategory => {
      const category = this.activeCategories.find(c => c.id === companyCategory.categoryId);

      return {
        ...companyCategory,
        displayLabel: category?.displayLabel,
      };
    });
  }

  private deleteConnectedCategory = (category: ICompanyCategoryFull): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: setTranslationSubjects(
        wording.general.areYouSureYouWantToDelete,
        { subject: category.displayLabel },
      ),
    }).subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      this.companyCategoriesRepo.delete(category.id, { companyId: this.companyId }).subscribe(() => {
        const categories = this.dataSource$.getValue();
        const index = categories.findIndex(c => c.id === category.id);
        categories.splice(index, 1);
        this.dataSource$.next(cloneDeep(categories));
      });
    });
  }

  private addCategory = (): void => {
    const usedCategories = this.dataSource$.getValue();
    const unusedCategories = this.activeCategories.filter(c => {
      return usedCategories.every(used => used.categoryId !== c.id);
    });

    const modalRef: NzModalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.general.add[this.settingsService.language],
      nzClosable: true,
      nzContent: CompanyCategoriesModalComponent,
      nzComponentParams: {
        unusedCategories,
      },
      nzFooter: null,
      nzOnCancel: () => modalRef.destroy(),
    });

    modalRef.afterClose.subscribe((result: ICategory[]) => {
      if (!result?.length) {
        return;
      }

      const list: ICompanyCategoryFull[] = result.map(c => ({
        categoryId: c.id,
        companyId: this.companyId,
        displayLabel: c.displayLabel,
      } as ICompanyCategoryFull));

      const selectedItems = this.dataSource$.getValue().concat(list);

      this.companyCategoriesRepo.updateCategories(selectedItems, { companyId: this.companyId })
        .subscribe((updatedCompanyCategories) => {
          this.dataSource$.next(this.companyCategories2Full(updatedCompanyCategories));
        });
    });
  }

  private setGridConfig(): void {
    const canPerform = (operation: Operations) => this.authService.canPerformOperationOnCurrentUrl(operation);
    const actions: IGridConfigActions = {
      add: this.addCategory,
      delete: this.deleteConnectedCategory,
    };

    this.gridConfig = getCompanyCategoriesTabGridConfig(actions, canPerform);
  }
}
