import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '../../../../../../../../core/classes/validators';
import { ITaxKey } from '../../../../../../../../core/models/resources/ITaxKeys';
import { TaxKeyRepositoryService } from 'src/app/core/services/repositories/tax-key-repository.service';
import { Observable, combineLatest, forkJoin } from 'rxjs';
import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { MandatorTaxKeysRepositoryService } from 'src/app/core/services/repositories/mandator-tax-keys-repository.service';
import { map } from 'rxjs/operators';
import { wording } from '../../../../../../../../core/constants/wording/wording';
import { getMandatorTaxKeyGridConfig } from 'src/app/core/constants/nv-grid-configs/mandator-tax-keys-detail-view.config';

export enum TaxCategory {
  InputText = 1,
  ValueAddedText = 2
}

@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class MandatorTaxKeyDetailViewComponent implements OnInit {
  @Input() public mandatorId: number;
  @Input() public closeModal: () => void;
  public submitDisabled = false;

  public wording = wording;
  public form: FormGroup;
  public dataSource$: Observable<ITaxKey[]>;
  public mandatorTaxKeyConfig: NvGridConfig;

  constructor(
    private taxKeyRepo: TaxKeyRepositoryService,
    private fb: FormBuilder,
    private mandatorTaxKeyRepo: MandatorTaxKeysRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.mandatorTaxKeyConfig = getMandatorTaxKeyGridConfig();
    this.initForm();

    this.dataSource$ = combineLatest([
      this.taxKeyRepo.fetchAll({}),
      this.mandatorTaxKeyRepo.fetchAll({ queryParams: { mandatorId: this.mandatorId } })
    ]).pipe(
      map(([taxKeys, _mandatorTaxKeys]) => {
        // This filter is neccessary, as long as queryParams do not filter on server-side
        const mandatorTaxKeys = _mandatorTaxKeys.filter(m => m.mandatorId === this.mandatorId);
        // only take taxKeys that are not connected to this mandator already
        return taxKeys.filter(tk => {
          return tk.isActive && !mandatorTaxKeys.find(m => m.taxKeyId === tk.id);
        });
      }),
    );
  }

  private initForm() {
    this.form = this.fb.group({
      mandatorId: [this.mandatorId, Validators.required],
      taxKeysIdsArray: [],
    });
  }

  /**
   * The current implementation is doing multiple requests for multiple selected tax-keys.
   *
   * TODO: change/improve this snippet to send a single request instead of multiple when
   * will be created backend endpoint for this entity to send multiple tax-keys.
   */
  public save() {
    const { mandatorId, taxKeysIdsArray } = this.form.value;

    const allRequests = taxKeysIdsArray.map((taxKeyId: number) => {
      return this.mandatorTaxKeyRepo.create({ mandatorId, taxKeyId }, {});
    });
    forkJoin(allRequests).subscribe(() => this.closeModal());
  }

  public rowSelect(taxes: ITaxKey[]) {
    const ids: number[] = taxes.map(t => t.id);
    this.form.controls.taxKeysIdsArray?.setValue(ids);

    this.submitDisabled = !ids.length;
  }
}
