import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { isEmpty } from 'lodash';
import { IWording } from 'src/app/core/models/resources/IWording';
import { wording } from '../constants/wording/wording';
import { NotificationType } from '../models/INotification';
import { NotificationsService } from '../services/notification.service';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { toUpperSnakeCase } from '../helpers/general';

export interface IErrorArguments {
  [key: string]: string;
}

export interface INavidoHTTPResponseError {
  message?: string;
  key?: string;
  args?: IErrorArguments;
  field?: string;
}

const fieldsIds = {
  'from-Until': 'from',
  'from-until': 'from',
};

/**
 * Handles HTTP error and shows it as notification
 */
@Injectable({
  providedIn: 'root'
})
export class HttpErrorHandler {
  constructor(
    private notificationService: NotificationsService,
  ) { }

  public handleError = (errorRes: HttpErrorResponse): HttpErrorResponse => {
    if (errorRes.error && errorRes.error.errors && errorRes.error.errors.length) {

      const errorsWithWording = errorRes.error.errors.map((error: INavidoHTTPResponseError) => {
        return this.getErrorWithWording(error);
      });

      return {
        ...errorRes,
        error: {
          errors: [
            ...errorsWithWording,
          ],
        },
      };
    }

    this.notificationService.notify(
      NotificationType.Error,
      wording.general.error,
      wording.httpStatus.somethingWentWrong,
    );

    return errorRes;
  }

  public getErrorWithWording(error: INavidoHTTPResponseError, isValidationError: boolean = false) {
    const upperCaseArgs = this.argsKeysToUpperSnakeCase(error.args) as IErrorArguments;
    const messageWording = this.isTranslationExist(error.key)
      ? this.getErrorMessage(error.key, upperCaseArgs)
      : this.getErrorMessage(error.message, upperCaseArgs);

    if (!isValidationError) {
      this.notificationService.notify(
        NotificationType.Error,
        wording.general.error,
        messageWording,
      );
    }

    return {
      ...error,
      messageWording,
      field: fieldsIds[error.field] || error.field,
    };
  }

  private getErrorMessage(error: string, args?: IErrorArguments): IWording {
    const wordingError = this.getIWording(error);

    return isEmpty(args) ? wordingError : this.replaceArgs(wordingError, args);
  }

  private getIWording(error: string): IWording {
    const wordingError = wording.errors[error]
      ? wording.errors[error]
      : { en: error, de: error };

    return wordingError;
  }

  private isTranslationExist(errorKey: string) {
    return !!wording.errors[errorKey];
  }

  private replaceArgs(error: IWording, args: IErrorArguments | string): IWording {
    if (!args || typeof args !== 'object') {
      return error;
    }

    return setTranslationSubjects(error, args);
  }

  private argsKeysToUpperSnakeCase(obj: Object | undefined): Object | undefined {
    if (isEmpty(obj)) {
      return obj;
    }

    return Object.keys(obj).reduce((convertedObj, key) => {
      convertedObj[toUpperSnakeCase(key)] = obj[key];

      return convertedObj;
    }, {});
  }
}


