import { Injectable } from '@angular/core';
import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { EntitiesUpdateI } from '@core-interfaces/entities-update.interface';
import { EntityI } from '@core-interfaces/entity.interface';
import { BaseWebService } from '@core-services/base.web-service';
import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { CertificateModel } from '@shared/models/certificate/certificate.model';
import { CompanyModel } from '@shared/models/company/company.model';
import { Observable } from 'rxjs';

@Injectable()
export class DigitalProfileEditWebService {
  constructor(private baseWebService: BaseWebService) {}

  updateCompany(data: EntitiesUpdateI): Observable<CompanyModel> {
    return this.baseWebService.putRequest<CompanyModel, EntitiesUpdateI>(`${MARKETPLACE_BASE_API_URL}/companies`, data, CompanyModel);
  }

  updateCompanyImage(data: EntitiesUpdateI): Observable<ArrayResponseI<EntityI>> {
    return this.baseWebService.putRequest<ArrayResponseI<EntityI>, EntitiesUpdateI>(`${MARKETPLACE_BASE_API_URL}/companies`, data);
  }

  createCertificate(data: EntitiesUpdateI): Observable<CertificateModel> {
    return this.baseWebService.postRequest<CertificateModel, EntitiesUpdateI>(
      `${MARKETPLACE_BASE_API_URL}/companies/certificates/relationships`,
      data,
      CertificateModel
    );
  }

  updateCertificate(associationOID: string, data: EntitiesUpdateI): Observable<CertificateModel> {
    return this.baseWebService.putRequest<CertificateModel, EntitiesUpdateI>(
      `${MARKETPLACE_BASE_API_URL}/companies/certificates/relationships/${associationOID}`,
      data,
      CertificateModel
    );
  }
}
