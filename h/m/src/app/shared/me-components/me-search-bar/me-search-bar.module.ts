import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';

import { IconsModule } from '@shared/modules/icons.module';

import { MeSearchBarComponent } from './me-search-bar.component';
import { MeButtonModule } from '../me-button/me-button.module';
import { MeSearchInputModule } from '../me-search-input/me-search-input.module';
import { MeContextMenuModule } from '../me-context-menu/me-context-menu.module';
import { MeAttributeModule } from '../me-attribute/me-attribute.module';

@NgModule({
  declarations: [MeSearchBarComponent],
  imports: [
    CommonModule,
    MeButtonModule,
    MeSearchInputModule,
    IconsModule,
    MeContextMenuModule,
    MatButtonModule,
    MatMenuModule,
    MatTooltipModule,
    MeAttributeModule
  ],
  exports: [MeSearchBarComponent]
})
export class MeSearchBarModule {}
