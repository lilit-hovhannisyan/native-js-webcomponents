import { Component, OnInit, Input } from '@angular/core';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { Observable, combineLatest } from 'rxjs';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { map } from 'rxjs/operators';
import { NvGridConfig } from 'nv-grid';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { BookingAccountMainService } from 'src/app/core/services/mainServices/booking-account-main.service';
import { getMandatorAccountCreateViewGridConfig } from 'src/app/core/constants/nv-grid-configs/mandator-account-create.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { MandatorAccountRoleRepositoryService } from 'src/app/core/services/repositories/mandator-account-role.repository.service';
import { MandatorAccountMainService } from 'src/app/core/services/mainServices/mandator-account-main.service';

@Component({
  selector: 'nv-mandator-account-create-view',
  templateUrl: './mandator-account-create-view.component.html',
  styleUrls: ['./mandator-account-create-view.component.scss']
})
export class MandatorAccountCreateViewComponent implements OnInit {

  @Input() public mandatorId: number;
  @Input() public closeModal: ({refresh: boolean}) => void;
  @Input() public onlyOpenItemFalse: boolean;
  @Input() public setOpenItemTrueOnSelect: boolean;

  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IBookingAccount[]>;
  public selectedBookingAccounts: number[] = [];
  public wording = wording;
  constructor(
    public bookingAccountService: BookingAccountMainService,
    private mandatorAccountRepo: MandatorAccountRepositoryService,
    public gridConfigService: GridConfigService,
    private mandatorAccountRoleRepo: MandatorAccountRoleRepositoryService,
    private mandatorAccountMainService: MandatorAccountMainService,
  ) { }

  public ngOnInit() {
    this.gridConfig = getMandatorAccountCreateViewGridConfig();
    this.dataSource$ = combineLatest([
      this.bookingAccountService.getStream(),
      this.mandatorAccountRepo.fetchAll({ queryParams: { mandatorId: this.mandatorId } }),
      this.mandatorAccountRoleRepo.fetchAll({}),
    ]).pipe(
      map(([bookingAccounts, _mandatorBookingAccounts]) => {
        return bookingAccounts.filter(ba => {
          // only take bookingAccounts that are not connected to this mandator already
          const mandatorBookingAccounts = _mandatorBookingAccounts
            .filter(m => m.mandatorId === this.mandatorId
              && this.mandatorAccountMainService.hasAccessToMandatorAccount(m));
          const foundMandatorAccount = mandatorBookingAccounts.find(mba => mba.bookingAccountId === ba.id);
          const isRelatedToMandator = ba.isActive && !foundMandatorAccount;
          // filter only where open item = false
          if (this.onlyOpenItemFalse) {
            if (!foundMandatorAccount) {
              return false;
            }
            return !foundMandatorAccount.isOpenItem && ba.isActive;
          } else {
            return isRelatedToMandator;
          }
        }).map(bookingAccount => {
          if (this.setOpenItemTrueOnSelect) {
            const foundMandatorAccount = _mandatorBookingAccounts
              .find(mba => mba.bookingAccountId === bookingAccount.id);
            return {
              ...bookingAccount,
              isOpenItem: foundMandatorAccount.isOpenItem,
              isOpex: foundMandatorAccount.isOpex,
              currencyId: foundMandatorAccount.restrictedCurrencyId,
            };
          }
          return bookingAccount;
        });
      }),
    );
  }

  public rowSelectionChanged(bookingAccounts: IBookingAccount[]): void {
    this.selectedBookingAccounts = bookingAccounts.map(bookingAccount => bookingAccount.id);
  }

  public submit(): void {
    if (this.setOpenItemTrueOnSelect) {
      const mandatorAccounts = this.mandatorAccountRepo.getStreamValue();
      this.mandatorAccountRepo.updateMandatorAccountsIsOpenItem(
        this.mandatorId,
        mandatorAccounts
          .filter(ma => this.selectedBookingAccounts.includes(ma.bookingAccountId))
          .map(ma => ma.id)
      ).subscribe(() => this.closeModal({refresh: true}));
    } else {
      this.mandatorAccountRepo
        .createMandatorAccounts(this.mandatorId, this.selectedBookingAccounts)
        .subscribe(() => this.closeModal({refresh: false}));
    }
  }
}
