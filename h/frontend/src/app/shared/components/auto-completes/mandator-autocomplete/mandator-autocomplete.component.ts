import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { mandatorNoModifier } from 'src/app/core/models/resources/IMandator';
import { gridConfig } from 'src/app/shared/components/autocomplete/modals/mandator-grid-config';
import { FormGroup } from '@angular/forms';
import { MandatorRoleRepositoryService } from 'src/app/core/services/repositories/mandator-role.repository.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { IWording } from 'src/app/core/models/resources/IWording';
import { MandatorMainService } from 'src/app/core/services/mainServices/mandator-main.service';
import { IMandatorLookupRow } from '../../../../core/models/resources/IMandator';
import { Observable } from 'rxjs';
import { StreamAutocompleteInputWrapperComponent } from '../stream-autocomplete-input-wrapper/stream-autocomplete-input-wrapper.component';

@Component({
  selector: 'nv-mandator-autocomplete',
  templateUrl: './mandator-autocomplete.component.html',
  styleUrls: ['./mandator-autocomplete.component.scss']
})
export class MandatorAutocompleteComponent implements OnInit {
  @Input() public form: FormGroup;
  @Input() public controlName = 'mandatorId';
  @Input() public filters: object;
  @Input() public onlyWithRoles = true;
  @Input() public autofocus: boolean;
  @Input() public label: IWording = wording.accounting.mandator;
  @Input() public dataSource$: Observable<IMandatorLookupRow[]>;
  @Input() public title: IWording;
  @Input() public setId = true;
  @Input() public displayOneProperty: boolean;
  @Input() public openListOnInit: boolean;
  @Input() public showLabel = true;

  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();
  @Output() public valueChange: EventEmitter<any> = new EventEmitter();

  @ViewChild('autocompleteInputWrapper', { static: true }) public autocompleteInputWrapper: StreamAutocompleteInputWrapperComponent;

  public wording = wording;
  public gridConfig = gridConfig();
  public mandatorNoModifier = mandatorNoModifier;
  public isAdmin: boolean;
  public mandators$: Observable<IMandatorLookupRow[]>;

  constructor(
    public mandatorService: MandatorMainService,
    public mandatorRoleRepo: MandatorRoleRepositoryService,
    public authService: AuthenticationService
  ) {
  }

  public focus() {
    this.autocompleteInputWrapper.focus();
  }

  public ngOnInit() {
    const currentUser = this.authService.getUser();
    this.isAdmin = currentUser.isSuperAdmin || currentUser.isAdmin;
    if (!this.dataSource$) {
      this.mandators$ = !this.isAdmin && this.onlyWithRoles
        ? this.mandatorService.fetchLookupsWithRolesOnly()
        : this.mandatorService.getLookupStream();
    } else {
      this.mandators$ = this.dataSource$;
    }
  }
}
