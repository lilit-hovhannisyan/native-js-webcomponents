import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { GroupModel } from '@shared-models/groups/group.model';
import { UserModel } from '@shared-models/user/user.model';

@Injectable()
export class GroupsStoreService {
  // groups Entities
  private readonly _groupEntities = new BehaviorSubject<GroupModel[]>([]);
  readonly groupEntities$ = this._groupEntities.asObservable();
  // groups Total Count
  private readonly _groupTotalCount = new BehaviorSubject<number>(0);
  readonly groupTotalCount$ = this._groupTotalCount.asObservable();
  // Group nextID for lazyload
  private readonly _groupNextId = new BehaviorSubject<number>(0);
  readonly groupNextId$ = this._groupNextId.asObservable();

  // Group members entities
  private readonly _groupMembersEntities = new BehaviorSubject<UserModel[]>([]);
  readonly groupMembersEntities$ = this._groupMembersEntities.asObservable();
  // Group memebers total count
  private readonly _groupMembersTotalCount = new BehaviorSubject<number>(0);
  readonly groupMembersTotalCount$ = this._groupMembersTotalCount.asObservable();

  // Group Members nextID for lazyload
  private readonly _groupMembersNextId = new BehaviorSubject<number>(0);
  readonly groupMembersNextId$ = this._groupMembersNextId.asObservable();

  get groupEntities(): GroupModel[] {
    return this._groupEntities.getValue();
  }

  set groupEntities(data: GroupModel[]) {
    this._groupEntities.next(data);
  }

  appendgroupEntities(data: GroupModel[]): void {
    this.groupEntities = [...this.groupEntities, ...data];
  }

  get groupTotalCount(): number {
    return this._groupTotalCount.getValue();
  }

  set groupTotalCount(data: number) {
    this._groupTotalCount.next(data);
  }

  get groupNextId(): number {
    return this._groupNextId.getValue();
  }

  set groupNextId(data: number) {
    this._groupNextId.next(data);
  }

  get groupMembersEntities(): UserModel[] {
    return this._groupMembersEntities.getValue();
  }

  set groupMembersEntities(data: UserModel[]) {
    this._groupMembersEntities.next(data);
  }

  appendgroupMembersEntities(data: UserModel[]): void {
    this.groupMembersEntities = [...this.groupMembersEntities, ...data];
  }

  get groupMembersTotalCount(): number {
    return this._groupMembersTotalCount.getValue();
  }

  set groupMembersTotalCount(data: number) {
    this._groupMembersTotalCount.next(data);
  }

  get groupMembersNextId(): number {
    return this._groupMembersNextId.getValue();
  }

  set groupMembersNextId(data: number) {
    this._groupMembersNextId.next(data);
  }
}
