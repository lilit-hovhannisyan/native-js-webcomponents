import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { PROCESSING_TYPES } from './../../../../../../core/models/resources/IVesselCommission';
import { Component, ElementRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ITimeCharterBilling } from 'src/app/core/models/resources/ITimeCharterBilling';
import { ResourceDetailView, NVHttpErrorResponse } from 'src/app/core/abstracts/ResourceDetailView';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Validators } from 'src/app/core/classes/validators';
import { Subscription } from 'rxjs';
import { TimeCharterKey } from 'src/app/core/models/resources/ITimeCharter';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { TimeCharterBillingRepositoryService } from 'src/app/core/services/repositories/time-charter-billing-repository.service';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { billingCycleTypesOptions, processingOptions, BILLING_CYCLE_TYPES } from 'src/app/core/models/resources/IVesselCommission';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { ICurrency, CurrencyKey } from 'src/app/core/models/resources/ICurrency';
import { startWith, distinctUntilChanged, map } from 'rxjs/operators';
import { wording } from 'src/app/core/constants/wording/wording';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { getDefaultIntervalListItem } from 'src/app/shared/helpers/general';

@Component({
  selector: 'nv-time-charter-billing',
  templateUrl: './time-charter-billing.component.html',
  styleUrls: ['./time-charter-billing.component.scss']
})
export class TimeCharterBillingComponent implements OnInit {
  public node = sitemap.chartering.children.misc.children.timeCharters.children.id.children.periods;
  public zone = SitemapEntry.getByNode(this.node).toZone();
  public form: FormGroup;
  public inEditMode = false;
  public inCreationMode = false;
  public selectedInterval?: ITimeCharterBilling;
  private formFieldSubscriptions: Subscription[] = [];
  public timeCharterId: TimeCharterKey;
  public billings: ITimeCharterBilling[] = [];
  public currencies: ICurrency[] = [];

  public deleteObservable = null;
  public wording = wording;
  private resourceDefault = {
    isActive: true,
    daysBeforeDue: 7,
    processing: PROCESSING_TYPES.information,
  } as ITimeCharterBilling;

  public billingCycleTypesOptions: ISelectOption[] = billingCycleTypesOptions;
  public processingOptions: ISelectOption[] = processingOptions;
  private smallIntTwoDigitsValidator = [Validators.wholeNumber, Validators.positiveNumber, Validators.max(99)];
  private dueOnDayOfMonthValidators = [Validators.wholeNumber, Validators.positiveNumber, Validators.max(31)];

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    private nzMessageService: NzMessageService,
    private timeCharterBillingRepo: TimeCharterBillingRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    public authService: AuthenticationService,
    private confirmService: ConfirmationService,
    private el: ElementRef,
  ) { }

  public ngOnInit() {
    const { id } = this.route.parent.snapshot.params;
    this.timeCharterId = parseInt(id, 10);
    this.fetchCurrencies();
    this.init();
    this.fetchBillings();
  }

  private loadCurrencies(currencyId?: CurrencyKey) {
    this.currencyRepo.getStream()
      .pipe(map(currencies => this.currencyRepo.filterActiveOnly(currencies, currencyId)))
      .subscribe(currencies => this.currencies = currencies);
  }

  private fetchCurrencies() {
    this.currencyRepo.fetchAll({}).subscribe();
    this.loadCurrencies();
  }

  private fetchBillings(idToSelect?: number) {
    const queryParams = { timeCharterId: this.timeCharterId };
    this.timeCharterBillingRepo.fetchAll({ queryParams }).subscribe(billings => {
      this.billings = billings || [];

      if (this.billings.length) {
        const billingToSelect = idToSelect
          ? this.billings.find(s => s.id === +idToSelect)
          : getDefaultIntervalListItem(this.billings);
        this.selectInterval(billingToSelect || this.billings[this.billings.length - 1]);
      }
    });
  }

  private init(billing: ITimeCharterBilling = this.resourceDefault) {
    this.form = this.fb.group({
      isActive: [billing.isActive],
      from: [billing.from, [Validators.required]],
      until: [billing.until, [Validators.isAfterFieldDate('from')]],
      currencyId: [billing.currencyId, [Validators.required]],
      billingCycle: [billing.billingCycle, [Validators.required, Validators.wholeNumber, Validators.positiveNumber, Validators.max(999)]],
      billingCycleType: [billing.billingCycleType, Validators.required],
      isContinually: [billing.isContinually],
      processing: [billing.processing, Validators.required],
      dueOnDayOfMonth: [billing.dueOnDayOfMonth, this.dueOnDayOfMonthValidators],
      daysBeforeDue: [billing.daysBeforeDue, [...this.smallIntTwoDigitsValidator]],
      invoicesInAdvance: [billing.invoicesInAdvance, [...this.smallIntTwoDigitsValidator]],
      isHire: [billing.isHire],
      isHireOff: [billing.isHireOff],
      isLumpsumExpenses: [billing.isLumpsumExpenses],
      isAtCostExpenses: [billing.isAtCostExpenses],
      isCommission: [billing.isCommission]
    });
    this.formFieldSubscriptions.forEach(s => s.unsubscribe());
    this.form.disable();
    this.form.markAsPristine();
    this.updateFromFieldOnUntilChange();
    this.loadCurrencies(billing.currencyId);

    const cycleTypeSub = this.form.get('billingCycleType').valueChanges
      .pipe(startWith(this.resourceDefault.billingCycleType))
      .subscribe(this.billingCycleTypeChanged);

    this.formFieldSubscriptions.push(cycleTypeSub);
  }

  public billingCycleTypeChanged = (type?: BILLING_CYCLE_TYPES) => {
    this.updateDueOnDayOfMonthValidators(type);
    this.updateBillingCycleValidators(type);
    this.updateBillingCycleDisabled(type);
  }

  public updateBillingCycleDisabled = (type?: BILLING_CYCLE_TYPES) => {
    const billingCylceControl = this.form.get('billingCycle');

    if (!this.inEditMode) { return; }

    if (
      type === BILLING_CYCLE_TYPES.months ||
      type === BILLING_CYCLE_TYPES.days ||
      type === BILLING_CYCLE_TYPES.weeks
    ) {
      billingCylceControl.enable();
    } else {
      billingCylceControl.disable();
      if (type) {
        billingCylceControl.setValue(undefined);
      }
    }
  }

  public updateDueOnDayOfMonthValidators = (type?: BILLING_CYCLE_TYPES) => {
    const dueOnDayOfMonthControl = this.form.get('dueOnDayOfMonth');
    const validators = (type === BILLING_CYCLE_TYPES.months)
      ? [...this.dueOnDayOfMonthValidators, Validators.required]
      : [...this.dueOnDayOfMonthValidators];


    dueOnDayOfMonthControl.setValidators(validators);
    dueOnDayOfMonthControl.updateValueAndValidity();
  }

  public updateBillingCycleValidators = (type?: BILLING_CYCLE_TYPES) => {
    const billingCylceControl = this.form.get('billingCycle');

    const validators = [];
    if (
      type === BILLING_CYCLE_TYPES.months ||
      type === BILLING_CYCLE_TYPES.days ||
      type === BILLING_CYCLE_TYPES.weeks
    ) {
      validators.push(Validators.required);
    }

    billingCylceControl.setValidators(validators);
    billingCylceControl.updateValueAndValidity();
  }

  public updateFromFieldOnUntilChange() {
    this.formFieldSubscriptions.push(
      this.form.get('from').valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
        this.form.get('until').updateValueAndValidity();
      })
    );
  }

  public enterEditMode = () => {
    this.inEditMode = true;
    // form needs to be enabled in next event-cycle. otherwise it stays disabled
    setTimeout(() => this.form.enable());
  }

  public cancelEditing = () => {
    this.inEditMode = false;
    this.inCreationMode = false;
    this.init(this.selectedInterval);
    // form needs to be disabled in next event-cycle. otherwise it stays enabled
    setTimeout(() => this.form.disable());
  }

  public addIntervalClicked = () => {
    this.inCreationMode = true;
    this.selectedInterval = {} as ITimeCharterBilling;
    const latestEntry = this.billings[this.billings.length - 1];
    const isCurrentCurrencyActive = latestEntry && latestEntry.currencyId
      ? this.currencies
        .find(c => c.id === latestEntry.currencyId).isActive
      : false;
    const clonedPeriod = latestEntry
      ? {
        ...this.resourceDefault,
        ...latestEntry,
        from: latestEntry.until,
        until: undefined,
        currencyId: isCurrentCurrencyActive ? latestEntry.currencyId : null,
      }
      : this.resourceDefault as ITimeCharterBilling;

    delete clonedPeriod.id; // we don't need the id in creation mode
    this.init(clonedPeriod); // cloning the most recent one
    this.enterEditMode();
  }

  public saveClicked = () => {
    if (this.form.invalid) { return; }

    const freshResource = this.selectedInterval.id
      ? {
        ...this.form.value,
        timeCharterId: this.timeCharterId,
        id: this.selectedInterval.id,
      }
      : {
        ...this.form.value,
        timeCharterId: this.timeCharterId,
      };

    this.inCreationMode
      ? this.timeCharterBillingRepo.create(freshResource, {}).subscribe(billing => {
        this.cancelEditing();
        this.fetchBillings(billing.id);
      }, this.handleSubmitError)
      : this.timeCharterBillingRepo.update(freshResource, {}).subscribe(billing => {
        this.cancelEditing();
        this.fetchBillings(billing.id);
      }, this.handleSubmitError);

  }

  public handleSubmitError = (error: NVHttpErrorResponse) => {
    ResourceDetailView.handleSubmitError(error, this.form, this.nzMessageService);
  }

  public intervalClicked = (periodId: number) => {
    const period = this.billings.find(s => s.id === periodId);
    this.selectInterval(period);
  }

  private selectInterval = (period: ITimeCharterBilling) => {
    this.selectedInterval = period;
    this.init(period);
  }

  public deleteClicked(): void {
    this.confirmService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: '' },
    }).subscribe(confirmed => {
      if (confirmed) {
        this.timeCharterBillingRepo.delete(this.selectedInterval.id, {}).subscribe(() => {
          this.selectedInterval = {} as ITimeCharterBilling;
          this.fetchBillings();
          this.cancelEditing();
        });
      }
    });
  }
}
