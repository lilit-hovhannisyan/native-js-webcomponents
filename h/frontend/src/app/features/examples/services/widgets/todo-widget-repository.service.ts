import { Injectable } from '@angular/core';
import { AbstractWidgetsService } from './abstract-widgets-repository';
import { ITodoWidget, ITodoWidgetDTO } from 'src/app/core/models/widgets/ITodoWidget';
import { RepositoryService } from '../../../../core/services/repository.service';

@Injectable({
  providedIn: 'root'
})
export class TodoWidgetService extends AbstractWidgetsService {

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl() {
    return '/api/user-account/widgets/todo';
  }

  public preProcess(w: ITodoWidget): ITodoWidgetDTO {
    return {
      id: w.id,
      type: w.type,
      _type: w._type,
      position: {
        x: w.x,
        y: w.y,
        width: w.cols,
        height: w.rows
      },
    };
  }

  public updateWidget(widget: ITodoWidget) {
    return this.update(this.preProcess(widget), {});
  }
}
