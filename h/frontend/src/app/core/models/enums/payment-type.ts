export enum PaymentType {
  BOTH = 1,
  RECEIPT,
  OUTGOING
}
