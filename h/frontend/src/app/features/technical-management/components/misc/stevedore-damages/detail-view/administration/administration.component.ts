import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { IStevedoreDamageReport, IStevedoreDamageReportCreate, IStevedoreDamageReportRow, StevedoreDamageReportKey, StevedoreDamageReportStatus } from 'src/app/core/models/resources/IStevedoreDamageReport';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { StevedoreDamageReportRepositoryService } from 'src/app/core/services/repositories/stevedore-damage-report-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { switchMap, tap, map } from 'rxjs/operators';
import { StevedoreDamageReportMainService } from 'src/app/core/services/mainServices/stevedore-damage-report-main.service';
import { StevedoreDamagesHelperService } from '../stevedore-damages.helper.service';
import { Validators } from 'src/app/core/classes/validators';
import { Language } from 'src/app/core/models/language';
import { Observable } from 'rxjs';
import { getLocalDateTime, getDateTimeFormat } from 'src/app/shared/helpers/general';

export type NzStepsStatus = 'wait' | 'process' | 'finish' | 'error';
@Component({
  selector: 'nv-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.scss']
})
export class AdministrationComponent
  extends ResourceDetailView<IStevedoreDamageReport, IStevedoreDamageReportCreate>
  implements OnInit {

  public resourceDefault = {} as IStevedoreDamageReport;
  public repoParams = {};
  public routeBackUrl = url(sitemap.technicalManagement.children.misc.children.stevedoreDamages);
  private id: StevedoreDamageReportKey;
  public canCreateInvoiceApproved: boolean;
  public lang$: Observable<Language>;
  public StevedoreDamageReportStatus = StevedoreDamageReportStatus;
  public formElement: ElementRef;
  private dateTimeFormat: string;

  constructor(
    private stevedoreDamageReportRepositoryService: StevedoreDamageReportRepositoryService,
    baseComponentService: BaseComponentService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private el: ElementRef,
    private stevedoreDamageReportMainService: StevedoreDamageReportMainService,
    private stevedoreDamagesHelperService: StevedoreDamagesHelperService,
  ) {
    super(stevedoreDamageReportRepositoryService, baseComponentService);
  }

  public ngOnInit() {
    this.dateTimeFormat = getDateTimeFormat(this.settingsService.locale);
    this.lang$ = this.settingsService.languageSubject.asObservable();
    this.canCreateInvoiceApproved = this.authService
      .canCreate('technicalManagement.misc.stevedoreDamages.releaseInvoice');
    this.id = +this.route.parent.snapshot.params.id;
    this.stevedoreDamageReportMainService.fetchOne(this.id, {})
      .pipe(
        map(report => this.stevedoreDamageReportMainService.toRow(report)),
        tap(report => this.initializeResource(report))
      ).subscribe();
  }

  public init(sdReport: IStevedoreDamageReportRow = {} as IStevedoreDamageReportRow) {
    this.initForm(this.fb.group({
      // disabled view fields
      createdOn: [getLocalDateTime(sdReport.createdOn, this.dateTimeFormat)],
      createdBy: [sdReport.createdByUserFullName],
      invoiceApprovedBy: [sdReport.invoiceApprovedByUserFullName],
      invoiceApprovedOn: [getLocalDateTime(sdReport.invoiceApprovedOn, this.dateTimeFormat)],
      invoiceWaivedBy: [sdReport.invoiceWaivedByUserFullName],
      invoiceWaivedOn: [getLocalDateTime(sdReport.invoiceWaivedOn, this.dateTimeFormat)],
      // checkboxes (editable)
      isInvoiceApproved: [sdReport.isInvoiceApproved],
      isInvoiceWaivedAndSetToCleared: [sdReport.isInvoiceWaivedAndSetToCleared],
      // textarea
      remark: [sdReport.remark, [Validators.maxLength(500)]],
      // Not implemented fields
      invoiceNumber: [sdReport.invoiceNumber],
      invoiceCreatedOn: [getLocalDateTime(sdReport.invoiceCreatedOn, this.dateTimeFormat),
      [Validators.isDateInputInvalid]],
      invoiceCreatedBy: [sdReport.invoiceCreatedByUserFullName, Validators.maxLength(255)],
      invoicePaidOn: [getLocalDateTime(sdReport.invoicePaidOn, this.dateTimeFormat), [Validators.isDateInputInvalid]],
    }));
    const disabledFields = [
      'createdOn',
      'createdBy',
      'invoiceApprovedBy',
      'invoiceApprovedOn',
      'invoiceWaivedBy',
      'invoiceWaivedOn',
      'invoiceNumber',
      'invoiceCreatedOn',
      'invoiceCreatedBy',
      'invoicePaidOn',
    ];

    // After adding at least 1 article in the "Repair/Documents" tab "Invoice approved"
    // checkbox is becoming editable
    if (
      !sdReport.stevedoreRepair?.stevedoreDamageRepairItems
      || sdReport.stevedoreRepair?.stevedoreDamageRepairItems.length < 1
      || !this.canCreateInvoiceApproved
      || sdReport.isInvoiceWaivedAndSetToCleared
    ) {
      disabledFields.push('isInvoiceApproved');
    }
    this.setDisabledFields(disabledFields);
  }

  public getTitle = () => this.resource.sdReportNo;

  public submit = () => {
    if (!this.form.valid) { return; }

    const {
      isInvoiceApproved,
      isInvoiceWaivedAndSetToCleared,
      remark
    } = this.form.getRawValue();

    const resource: IStevedoreDamageReportRow =
      this.resource as IStevedoreDamageReportRow;


    const payload = {
      ...this.resource,
      isInvoiceApproved,
      isInvoiceWaivedAndSetToCleared,
      remark,
      status: this.getNewStatus(
        resource,
        isInvoiceApproved,
        isInvoiceWaivedAndSetToCleared,
      ),
    };

    this.stevedoreDamageReportRepositoryService.update(payload, {})
      .pipe(
        switchMap(() => {
          return this.stevedoreDamageReportMainService.fetchOne(this.id, {});
        }),
        map(res => this.stevedoreDamageReportMainService.toRow(res)),
        tap((reportFull: IStevedoreDamageReportRow) => {
          this.stevedoreDamagesHelperService.generateTitle(reportFull);
          this.leaveEditMode();
          this.initializeResource(reportFull);
        })
      ).subscribe();
  }

  private getNewStatus(
    resource: IStevedoreDamageReportRow,
    isInvoiceApproved: boolean,
    isInvoiceWaivedAndSetToCleared: boolean
  ): StevedoreDamageReportStatus {
    if (isInvoiceWaivedAndSetToCleared) {
      return StevedoreDamageReportStatus.Cleared;
    }
    return isInvoiceApproved
      ? StevedoreDamageReportStatus.InProgress
      // TODO: refactor this once we have all other invoice implemented
      : !resource.stevedoreRepair?.stevedoreDamageRepairItems
        || resource.stevedoreRepair.stevedoreDamageRepairItems.length < 1
        ? StevedoreDamageReportStatus.RatingRequired
        : StevedoreDamageReportStatus.ReleaseRequired;
  }

  public getStatus(
    status: StevedoreDamageReportStatus
  ): NzStepsStatus {
    if (status === this.resource.status) {
      return 'process';
    } else if (status < this.resource.status) {
      return 'finish';
    } else {
      return 'wait';
    }
  }

  public getCustomIcon(status: StevedoreDamageReportStatus): string {
    const { isInvoiceWaivedAndSetToCleared } = this.resource;
    if (isInvoiceWaivedAndSetToCleared && status < this.resource.status) {
      return 'close-circle';
    }
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
