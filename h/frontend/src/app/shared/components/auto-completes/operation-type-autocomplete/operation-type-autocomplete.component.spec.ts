import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OperationTypeAutocompleteComponent } from './operation-type-autocomplete.component';

describe('OperationTypeAutocompleteComponent', () => {
  let component: OperationTypeAutocompleteComponent;
  let fixture: ComponentFixture<OperationTypeAutocompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OperationTypeAutocompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperationTypeAutocompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
