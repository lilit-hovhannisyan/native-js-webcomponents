import { wording } from './wording/wording';
import { Language } from '../models/language';
import { ModalWordingTranslateComponent } from 'src/app/shared/components/modal-wording-translate/modal-wording-translate.component';

export const DefaultModalOptions = {
  nzMaskClosable: false,
  nzBodyStyle: { padding: '0px' },
  nzWidth: 'min-content',
  nzFooter: null,
};

export const getDefaultConfirmModalOptions = (lang: Language) => ({
  nzContent: ModalWordingTranslateComponent,
  nzComponentParams: {
    wording: wording.general.areYouSureYouWantToDelete,
    label: '?'
  },
  nzOkText: wording.general.yes[lang],
  nzCancelText: wording.general.cancel[lang],
});
