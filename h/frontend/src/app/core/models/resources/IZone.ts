import { IResource } from './IResource';
import { IWording } from './IWording';

export type ZoneKey = string;

interface ISharedProps {
  description?: string;
  supports: number;
}

export interface IZoneBase extends ISharedProps, IResource<ZoneKey> { }

export interface IZoneCreate extends ISharedProps { }

export interface IZoneDTO extends IZoneBase { }

export interface IZone extends IZoneDTO { }

export interface IZoneRow extends IZoneDTO {
  fullTitle?: IWording;
}
