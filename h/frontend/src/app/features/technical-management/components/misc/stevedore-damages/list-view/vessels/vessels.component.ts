import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from 'src/app/core/abstracts/BaseComponent';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';
import { LoadingService } from 'src/app/core/services/loading.service';
import { Observable, forkJoin, BehaviorSubject } from 'rxjs';
import { IVesselAccounting } from 'src/app/core/models/resources/IVesselAccounting';
import { tap } from 'rxjs/operators';
import { VesselSpecificationRepositoryService, vesselTypes } from 'src/app/core/services/repositories/vessel-specification-repository.service';
import { IVesselSpecification } from 'src/app/core/models/resources/IVesselSpecification';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { orderBy } from 'lodash';
export interface IVesselIdMap {
  [id: number]: boolean;
}

export interface ICharterNamesMap {
  [name: string]: boolean;
}

export interface IVesselAccountingSpecification
  extends IVesselAccounting, IVesselSpecification {
  vesselTypeDisplayValue: string;
  chartererName?: string;
  imo?: number;
}

@Component({
  selector: 'nv-vessels-list',
  templateUrl: './vessels.component.html',
  styleUrls: ['./vessels.component.scss']
})
export class VesselsComponent extends BaseComponent implements OnInit {
  @Input() private availableVesselIds: number[] = [];
  @Input() private chartererNames: string[];
  @Output() public vesselChecked = new EventEmitter<IVesselIdMap>();
  @Output() public chartersChecked = new EventEmitter<IVesselIdMap>();

  public vesselTypes: ISelectOption[] = vesselTypes;
  public allSelected: boolean;
  public allCleared: boolean;
  public searchInputValue: string;
  public isLoading$: Observable<boolean>;
  public vessels: IVesselAccountingSpecification[] = [];
  public vesselsViewList = new BehaviorSubject<IVesselAccountingSpecification[]>([]);
  // mapped with ids
  public checkedVessels: IVesselIdMap = {};
  public checkedCharters: ICharterNamesMap = {};

  constructor(
    bcs: BaseComponentService,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
    private loadingService: LoadingService,
    private vesselSpecification: VesselSpecificationRepositoryService,
  ) {
    super(bcs);
  }

  public ngOnInit() {
    this.isLoading$ = this.loadingService.isLoading$;
    forkJoin([
      this.vesselAccountingRepo.fetchAll({}),
      this.vesselSpecification.fetchAll({})
    ])
      .pipe(
        tap(([accountings, specifications]) => {
          this.vessels =
            accountings
              .filter(a => this.availableVesselIds.includes(a.id))
              .map(a => {
                const filteredSpecs = specifications
                  .filter(s => s.vesselId === a.vesselId);
                const specs = orderBy(
                  filteredSpecs,
                  [s => s.from],
                  ['desc']
                );
                const spec = specs ? specs[0] : null;
                return {
                  ...spec,
                  ...a,
                  vesselTypeDisplayValue: spec && spec.vesselType
                    ? this.vesselTypes.find(v => v.value === spec.vesselType)
                      .displayLabel[this.settingsService.language]
                    : ''
                };
              });
          this.vessels = [
            ...this.vessels,
            ...this.chartererNames.map(c =>
              ({ chartererName: c } as IVesselAccountingSpecification)
            ),
          ];
          this.vesselsViewList.next(this.vessels);
          this.onAllSelected();
          this.onVesselChecked();
          this.onCharterChecked();
        })).subscribe();
  }

  public onAllSelected() {
    this.vesselsViewList.value.forEach(v => v.id
      ? this.checkedVessels[v.id] = true
      : null);
    this.chartererNames.forEach(c => this.checkedCharters[c] = true);
    this.onVesselChecked();
    this.onCharterChecked();
  }

  public onAllCleared() {
    this.checkedVessels = {};
    this.checkedCharters = {};
    this.onVesselChecked();
    this.onCharterChecked();
  }

  public onSeach() {
    if (!this.searchInputValue) {
      return this.vesselsViewList.next(this.vessels);
    }
    this.vesselsViewList.next(this.vessels.filter(v => {
      const usrInput = this.searchInputValue.toLowerCase().trim();

      if (v.id) {
        const vesselName = v.vesselName.toLowerCase();
        const vesselNo = v.vesselNo.toString();
        const vesselTypeDisplayValue = v.vesselTypeDisplayValue.toLowerCase();

        const case1 = `${vesselName} ${vesselNo} ${vesselTypeDisplayValue}`;
        const case2 = `${vesselNo} ${vesselName} ${vesselTypeDisplayValue}`;
        return case1.startsWith(usrInput) || case2.startsWith(usrInput);
      }
      if (v.chartererName) {
        const chartererName = v.chartererName.toLowerCase();
        return chartererName.startsWith(usrInput);
      }
    }));
  }

  public onVesselChecked() {
    this.vesselChecked.emit(this.checkedVessels);
  }

  public onCharterChecked() {
    this.chartersChecked.emit(this.checkedCharters);
  }
}
