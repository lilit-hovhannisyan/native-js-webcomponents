import { Injectable } from '@angular/core';
import { NzTreeNode } from 'ng-zorro-antd/tree';
import { IMasterStructureNodeTemplate, IMasterStructureNode, MASTER_STRUCTURE_NODE_TYPE } from 'src/app/core/models/resources/IMasterStructureNode';
import { IMasterStructure } from 'src/app/core/models/resources/IMasterStructure';
import { Language } from 'src/app/core/models/language';
import { MASTER_STRUCTURE_TYPES } from 'src/app/core/models/enums/master-structure-type';
import { MasterStructureNodeHelperService } from './master-structure-node-helper.service';
import { IWording } from 'src/app/core/models/resources/IWording';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { wording } from 'src/app/core/constants/wording/wording';
import { SwitchingNodesService } from '../../switching-nodes/services/switching-nodes.service';
import { SwitchingAccountsService } from '../../switching-nodes/services/switching-accounts.service';
import { MasterStructureBookingAccountService } from '../../master-structure-booking-accounts/master-structure-booking-account.service';
import { NodeSummationService } from '../../node-summation/node-summation.service';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class MasterStructureNodeSharedService {

  private currentStructure: IMasterStructure;
  private language: Language;
  private form: FormGroup;

  constructor(
    private switchingNodesService: SwitchingNodesService,
    private switchingAccountsService: SwitchingAccountsService,
    private bookingAccountsService: MasterStructureBookingAccountService,
    private nodeSummationService: NodeSummationService,
    private nodesHelperService: MasterStructureNodeHelperService,
  ) { }

  public initForm(form: FormGroup): void {
    this.form = form;
  }

  public initCurrentStructure(structure: IMasterStructure): void {
    this.currentStructure = structure;
  }

  public setLanguage(language: Language): void {
    this.language = language;
  }

  public validateYearResults(
    fieldName: 'hasResultCurrentYear' | 'hasResultPreviousYear',
    nodes: IMasterStructureNodeTemplate[],
    selectedNode: NzTreeNode,
  ): boolean {
    if (
      !selectedNode
      || this.currentStructure.masterStructureType !== MASTER_STRUCTURE_TYPES.BALANCE_SHEET
    ) {
      return false;
    }

    let isYearResultUsed = false;

    this.nodesHelperService.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        if (node[fieldName] && node.id !== selectedNode.origin.id) {
          isYearResultUsed = true;
          return true;
        }
      }
    );

    return !isYearResultUsed
      && !selectedNode?.origin.bookingAccountId
      && !selectedNode?.origin[fieldName === 'hasResultCurrentYear'
        ? 'hasResultPreviousYear'
        : 'hasResultCurrentYear'];
  }

  public isDropInvalidForExpenseAndEarning(
    draggedNode: NzTreeNode,
    targetNode: NzTreeNode,
    dropPosition: number,
  ): { isInvalidForExpense: boolean, isInvalidForEarning: boolean } {
    const isDraggedParentExpense: boolean = draggedNode.parentNode?.origin.hasExpenseItem;
    const isDraggedNodeExpense: boolean = draggedNode.origin.hasExpenseItem;
    const isTargetParentExpense: boolean = targetNode.parentNode?.origin.hasExpenseItem;
    const isTargetNodeExpense: boolean = targetNode.origin.hasExpenseItem;
    const isBwa: boolean = this.currentStructure.masterStructureType
      === MASTER_STRUCTURE_TYPES.BUSINESS_MANAGEMENT_EVALUATION;
    const isInvalidForExpense: boolean = isBwa
      && (!isDraggedParentExpense && !isDraggedNodeExpense)
      && (isTargetParentExpense || isTargetNodeExpense && dropPosition === 0);
    const isInvalidForEarning: boolean = isBwa
      && (isDraggedParentExpense || isDraggedNodeExpense)
      && ((targetNode.parentNode && !isTargetParentExpense)
        || !isTargetNodeExpense && dropPosition === 0);
    return { isInvalidForExpense, isInvalidForEarning };
  }

  public getDeleteMessage(
    nodes: IMasterStructureNodeTemplate[],
    foundNode: IMasterStructureNode,
    switchingNode?: IMasterStructureNode,
  ): IWording {
    if (!!foundNode.childNodes.length) {
      return this.getDeleteMessageWithChildren(foundNode, switchingNode);
    } else {
      return this.getDeleteMessageWithoutChildren(nodes, foundNode, switchingNode);
    }
  }

  private getDeleteMessageWithChildren(
    foundNode: IMasterStructureNode,
    switchingNode: IMasterStructureNode,
  ): IWording {
    const label: string = getLabelKeyByLanguage(this.language);

    if (
      this.currentStructure.masterStructureType === MASTER_STRUCTURE_TYPES.BALANCE_SHEET
    ) {
      if (
        this.switchingAccountsService.hasChildSwitchingAccount(foundNode)
        && foundNode.switchingNodeId
      ) {
        return  setTranslationSubjects(
          wording.accounting.deleteSwitchingNodeWithSwitchingAccount,
          { NAME: switchingNode[label] },
        );
      } else {
        return wording.accounting.deleteNodeWithChildrenAndSwitchingNode;
      }
    } else {
      return wording.accounting.deleteNodeWithChildren;
    }
  }

  private getDeleteMessageWithoutChildren(
    nodes: IMasterStructureNodeTemplate[],
    foundNode: IMasterStructureNode,
    switchingNode: IMasterStructureNode,
  ): IWording {
    const label: string = getLabelKeyByLanguage(this.language);

    if (this.currentStructure.isActive && foundNode.bookingAccountId) {
      return wording.accounting.deletingAnAccountWillSetStructureToInactive;
    }

    if (
      foundNode.nodeType === MASTER_STRUCTURE_NODE_TYPE.SWITCHING_ACCOUNT
      && foundNode.bookingAccountId
    ) {
      const parentSwitchingNode: IMasterStructureNode = this.switchingNodesService
        .getParentSwitchingNode(foundNode, nodes);
      return  setTranslationSubjects(
        wording.accounting.deleteSwitchingAccount,
        { NAME: parentSwitchingNode[label] },
      );
    } else {
      if (switchingNode) {
        return setTranslationSubjects(
          wording.accounting.afterDeleteNoSwitchingNodeAssigned,
          { SUBJECT: switchingNode[label] },
        );
      } else {
        return wording.accounting.doYouWantToDeleteNode;
      }
    }
  }

  public onDeleteModalConfirm(
    nodes: IMasterStructureNodeTemplate[],
    foundNode: IMasterStructureNode,
    parent: IMasterStructureNode,
  ): IMasterStructureNodeTemplate[] {
    let changedNodes: IMasterStructureNodeTemplate[] = [...nodes];

    if (parent) {
      parent.childNodes = parent.childNodes.filter(n => n.id !== foundNode.id);
    } else {
      changedNodes = changedNodes.filter(n => n.key !== foundNode.id.toString());
    }

    if (foundNode.nodeType === MASTER_STRUCTURE_NODE_TYPE.SUMMATION) {
      changedNodes = this.nodeSummationService
        .removeSummationNode(changedNodes, foundNode);
    }

    this.switchingAccountsService.removeSwitchingNodeAccounts(
      changedNodes,
      foundNode,
      this.switchingNodesService.getSwitchingNodesHashTable(foundNode),
    );

    if (foundNode.nodeType === MASTER_STRUCTURE_NODE_TYPE.SWITCHING_ACCOUNT) {
      this.switchingAccountsService.removeSwitchingAccount(changedNodes, foundNode);
    }

    if (foundNode.bookingAccountId) {
      this.bookingAccountsService.deleteUsedBookingAccounts(foundNode.bookingAccountId);
    }

    const usedBookingAccountIds: number[] = this.getUsedChildBookingAccounts(foundNode);

    if (usedBookingAccountIds.length > 0) {
      this.bookingAccountsService.deleteUsedBookingAccounts(usedBookingAccountIds);
    }

    return changedNodes;
  }

  private getUsedChildBookingAccounts(foundNode: IMasterStructureNode): number[] {
    const usedBookingAccountIds: number[] = [];

    this.nodesHelperService.mapNodes(
      foundNode,
      (node: IMasterStructureNodeTemplate) => {
        if (node.bookingAccountId) {
          usedBookingAccountIds.push(node.bookingAccountId);
        }
      }
    );

    return usedBookingAccountIds;
  }

  public onEditModalSave(
    nodes: IMasterStructureNodeTemplate[],
    foundNode: IMasterStructureNodeTemplate,
    oldSwitchingNode: IMasterStructureNodeTemplate,
  ): boolean {
      const {
        labelEn,
        labelDe,
        externalSystemCode,
        hasSwitchingNode,
        switchingNodeId,
      } = this.form.controls;
      validateAllFormFields(this.form);

      if (this.form.invalid) {
        return false;
      }

      if (!this.switchingNodesService.isSwitchingNodeChangeValid(foundNode)) {
        return false;
      }

      this.switchingNodesService.unsubscribeSwitchingExternalCode();
      const switchingNode: IMasterStructureNodeTemplate = this.switchingNodesService
        .searchAndGetSwitchingNode(
          { ...foundNode, switchingNodeId: switchingNodeId.value },
          nodes,
        );

      foundNode.labelEn = labelEn.value;
      foundNode.labelDe = labelDe.value;
      foundNode.title = { en: labelEn.value, de: labelDe.value };
      foundNode.externalSystemCode = externalSystemCode.value;
      foundNode.hasSwitchingNode = hasSwitchingNode.value;
      foundNode.switchingNodeId = switchingNodeId.value;
      this.switchingNodesService
        .applyChangesToSwitchingNode(foundNode, nodes, oldSwitchingNode || switchingNode);
      switchingNodeId.setValue(null, { emitEvent: false });
      return true;
  }
}
