import { Injectable } from '@angular/core';
import { PortRepositoryService } from '../repositories/port-repository.service';
import { CountryRepositoryService } from '../repositories/country-repository.service';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { IPortFull, IPort } from 'src/app/core/models/resources/IPort';
import { map, switchMap } from 'rxjs/operators';
import { ICountry, CountryKey } from '../../models/resources/ICountry';

@Injectable({
  providedIn: 'root'
})
export class PortMainService {
  private countriesMap = new Map<CountryKey, ICountry>();

  constructor(
    private portRepo: PortRepositoryService,
    private countryRepo: CountryRepositoryService
  ) { }

  public fetchAll(): Observable<IPortFull[]> {
    return forkJoin([
      this.portRepo.fetchAll({}),
      this.countryRepo.fetchAll({}),
    ]).pipe(map(res => this.getPortsFull(...res)));
  }

  private getPortsFull(ports: IPort[], countries: ICountry[]): IPortFull[] {
    countries.forEach(c => this.countriesMap.set(c.id, c));
    return ports.map(port => {
      const foundCountry = this.countriesMap.get(port.countryId);
      return {
        ...port,
        countryName: foundCountry ? foundCountry.displayLabel : null,
        countryIsoAlpha3: foundCountry?.isoAlpha3,
        countryFlagPath: foundCountry?.countryFlagPath,
      };
    });
  }

  public getStream(): Observable<IPortFull[]> {
    return forkJoin([
      this.portRepo.fetchAll({}),
      this.countryRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.portRepo.getStream(),
        this.countryRepo.getStream()
      ])),
      map(res => this.getPortsFull(...res))
    );
  }
}
