import { Directive, Input, HostBinding } from '@angular/core';

@Directive({
  selector: 'img[default]',
  host: {
    '(error)': 'updateUrl()',
    '(load)': 'load()',
    '[src]': 'src'
  }
})
export class ImgPreloadDirective {
  @Input() public src: string;
  @Input() public default: string;
  @HostBinding('class') public className: string;

  public updateUrl() {
    this.src = this.default;
  }

  public load() {
    this.className = 'image-loaded';
  }
}
