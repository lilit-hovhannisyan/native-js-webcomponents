import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { IArticleType } from 'src/app/core/models/resources/IArticleType';
import { Language } from 'src/app/core/models/language';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { ArticleTypeRepositoryService } from 'src/app/core/services/repositories/article-type-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { ConfirmationService } from '../../../../../../core/services/confirmation.service';
import { getArticleTypesGridConfig } from 'src/app/core/constants/nv-grid-configs/article-types.config';
import { Router } from '@angular/router';
import { wording } from 'src/app/core/constants/wording/wording';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';

@Component({
  selector: 'nv-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ArticleTypesListViewComponent implements OnInit {
  public articleTypeGridConfig: NvGridConfig;
  public dataSource$: Observable<IArticleType[]>;

  constructor(
    private articleTypeRepo: ArticleTypeRepositoryService,
    public gridConfigService: GridConfigService,
    private settingsService: SettingsService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private authService: AuthenticationService,
  ) { }

  public ngOnInit(): void {
    this.articleTypeRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.articleTypeRepo.getStream();

    const gridActions = {
      edit: (id: number) => this.router.navigate([`${this.router.url}/${id}`]),
      delete: this.deleteArticleTypeClicked,
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    this.articleTypeGridConfig = getArticleTypesGridConfig(
      gridActions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
    );
  }

  public deleteArticleTypeClicked = async (articleType: IArticleType): Promise<void> => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: articleType.displayLabel },
    }).subscribe(confirmed => confirmed && this.articleTypeRepo.delete(articleType.id, {}).subscribe());
  }
}
