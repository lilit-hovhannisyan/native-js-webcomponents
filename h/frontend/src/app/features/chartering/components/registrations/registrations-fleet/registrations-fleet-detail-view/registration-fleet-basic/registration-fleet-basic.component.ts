import { Component, OnInit, ElementRef, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, ReplaySubject } from 'rxjs';
import { RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { Language } from 'src/app/core/models/language';
import { IVessel, VesselKey } from 'src/app/core/models/resources/IVessel';
import { IVesselRegister, IVesselRegisterCreate } from 'src/app/core/models/resources/IVesselRegister';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { VesselRegisterRepositoryService } from 'src/app/core/services/repositories/vessel-registeries-repository.service';
import { VesselRepositoryService } from 'src/app/core/services/repositories/vessel-repository.service';
import { RegistrationTypes } from '../../../../../../../core/models/resources/IVesselRegister';
import { IVesselSpecification } from '../../../../../../../core/models/resources/IVesselSpecification';
import { VesselSpecificationRepositoryService, vesselTypes } from '../../../../../../../core/services/repositories/vessel-specification-repository.service';
import { ISelectOption } from '../../../../../../../core/models/misc/selectOption';
import { PortMainService } from '../../../../../../../core/services/mainServices/port-main.service';
import { IPortFull, PortKey } from '../../../../../../../core/models/resources/IPort';
import { RegistrationFleetHelperService } from '../registrations-fleet-helper.service';
import { takeUntil } from 'rxjs/operators';
import { VoyageCharternameRepositoryService } from '../../../../../../../core/services/repositories/voyage-chartername-repository.service';
import { IVesselPreDelayCharterName } from '../../../../../../../core/models/resources/IVesselPreDelayCharterName';
import { IVesselAccountingListItem } from '../../../../../../../core/models/resources/IVesselAccounting';

const disabledFields: string[] = [
  'vesselId',
  'imoNo',
  'chartername',
  'imoNo',
  'ssr',
  'mmsi',
  'officialNumber',
  'portOfRegistry',
  'vesselType',
  'from',
  'until',
  'countryId',
  'isTraining',
];

@Component({
  selector: 'nv-registration-fleet-basic',
  templateUrl: './registration-fleet-basic.component.html',
  styleUrls: ['./registration-fleet-basic.component.scss'],
})
export class RegistrationFleetBasicComponent extends ResourceDetailView<
  IVesselRegister,
  IVesselRegisterCreate
> implements OnInit, OnDestroy {

  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  protected repoParams: RepoParams<IVesselRegister> = {};
  public form: FormGroup;
  protected resourceDefault: IVesselRegister = {} as IVesselRegister;
  public routeBackUrl: string = url(
    sitemap.chartering.children.registrations.children.registrationsFleet
  );
  private vesselId: VesselKey;
  private vesselRegister: IVesselRegister;
  private language: Language;
  public vesselTypes: ISelectOption[] = vesselTypes;
  public isInEditMode: boolean;
  public formElement: ElementRef;
  private allPorts: IPortFull[] = [];
  private charternames: IVesselPreDelayCharterName[];
  private vesselAccounting: IVesselAccountingListItem;

  constructor(
    private fb: FormBuilder,
    baseComponentService: BaseComponentService,
    private vesselRegisterRepo: VesselRegisterRepositoryService,
    private route: ActivatedRoute,
    private vesselRepo: VesselRepositoryService,
    private vesselMainService: VesselMainService,
    private vesselSpecificationRepo: VesselSpecificationRepositoryService,
    private portMainService: PortMainService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef,
    private helperService: RegistrationFleetHelperService,
    private charternameRepo: VoyageCharternameRepositoryService,
  ) {
    super(vesselRegisterRepo, baseComponentService);
  }

  public getTitle: () =>  '';

  public ngOnInit(): void {
    this.vesselId = +this.route.snapshot.parent.params.id;
    this.language = this.settingsService.language;
    this.initResource();

    this.inEditMode.subscribe(mode => {
      this.isInEditMode = mode;

      /** Should be disabled on editMode. */
      this.helperService.registersSliderDisabledSubject.next(mode);
    });
  }

  public init(vesselRegister: IVesselRegister = this.vesselRegister): void {
    this.initForm(this.fb.group({
      vesselId: [this.vesselId],
      // editable fields
      client: [vesselRegister?.client],
      remarks: [vesselRegister?.remarks],

      // register
      registerCourt: [vesselRegister?.registerCourt, Validators.maxLength(30)],
      primaryPortId: [vesselRegister?.primaryPortId],
      secondaryPortId: [vesselRegister?.secondaryPortId],
      ssrNo: [vesselRegister?.ssrNo],
      type: [vesselRegister?.type || RegistrationTypes.Bareboat],
      primaryCountryId: [vesselRegister?.primaryCountryId, Validators.required],
      secondaryCountryId: [vesselRegister?.secondaryCountryId],
      primaryCallSign: [vesselRegister?.primaryCallSign, Validators.maxLength(10)],
      secondaryCallSign: [vesselRegister?.secondaryCallSign, Validators.maxLength(10)],
      primaryCompanyId: [vesselRegister?.primaryCompanyId],
      secondaryCompanyId: [vesselRegister?.secondaryCompanyId],
      from: [vesselRegister?.from, Validators.required],
      until: [vesselRegister?.until, Validators.isAfterFieldDate('from')],

      // autofill: vessel main page
      chartername: [''],
      imoNo: [''],
      ssr: [''],
      mmsi: [''],
      officialNumber: [''],
      portOfRegistry: [''],
      vesselType: [null],

      // flag
      countryId: [null],
      isTraining: [vesselRegister?.isTraining],
    }));
    this.setDisabledFields(disabledFields);

    this.onAutofillVesselMainFields();
  }

  private initResource(): void {
    this.vesselRegisterRepo.fetchAll({queryParams: {vesselId: this.vesselId}}).subscribe();

    forkJoin([
      this.vesselMainService.fetchVesselsWithClosestAccounting(true),
      this.portMainService.fetchAll(),
      this.charternameRepo.fetchAllPreDeliveryCharternames(),
      this.vesselSpecificationRepo.fetchAll({}),
    ]).subscribe(([vesselAccountings, ports, charternames]) => {
      this.allPorts = ports;
      this.vesselAccounting = vesselAccountings.find(v => v.vesselId === this.vesselId);

      this.charternames = charternames;
      this.handleRegisterChange();
    });
  }

  private handleRegisterChange(): void {
    this.helperService.selectedRegisterSubject.pipe(
      takeUntil(this.componentDestroyed$),
    ).subscribe(selectedRegister => {
      this.vesselRegister = selectedRegister;
      this.init(this.vesselRegister);
    });
  }

  private setPortOfRegistry(portId: PortKey): void {
    const port: IPortFull = this.allPorts.find(p => p.id === portId);
    const portValue: string = port ? `${port.shortName} (${port.countryIsoAlpha3})` : '';
    this.form.controls.portOfRegistry.patchValue(portValue);
  }

  private onAutofillVesselMainFields(): void {
    const {
      imoNo,
      chartername,
      mmsi,
      officialNumber,
      vesselType,
      countryId,
      isTraining,
    } = this.form.controls;

    const vessel: IVessel = this.vesselRepo.getStreamValue()
      .find(v => v.id === this.vesselId);

    let charternameValue: string = this.charternames.find(c => c.vesselId === vessel.id)?.charterName;
    if (this.vesselAccounting) {
      charternameValue = this.vesselAccounting?.charterName;
    }

    const specification: IVesselSpecification = this.vesselSpecificationRepo.getStreamValue()
      .filter(vs => vs.vesselId === this.vesselId)
      .reverse()
      .find(vs => new Date(vs.from) <= new Date());

    imoNo.patchValue(vessel?.imo);
    mmsi.patchValue(vessel?.mmsi);
    officialNumber.patchValue(vessel?.officialNumber);
    vesselType.patchValue(specification?.vesselType);
    countryId.patchValue(this.vesselRegister?.secondaryCountryId
      || this.vesselRegister?.primaryCountryId);
    isTraining.patchValue(this.vesselRegister?.isTraining);


    chartername.patchValue(charternameValue);

    const portId: PortKey = this.vesselRegister?.secondaryPortId
      || this.vesselRegister?.primaryPortId;
    this.setPortOfRegistry(portId);
  }

  public submit(): void {
    if (!this.form.valid) {
      return;
    }

    const { value } = this.form;

    this.updateResource({ ...this.vesselRegister, ...value })
      .subscribe(() => this.helperService.registersSliderDisabledSubject.next(false));
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
    this.helperService.registersSliderDisabledSubject.next(false);
  }
}
