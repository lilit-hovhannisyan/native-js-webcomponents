import { Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { IStevedoreRepair, IStevedoreRepairFull } from '../../models/resources/IStevedoreRepair';
import { IArticle } from '../../models/resources/IArticle';
import { RepoParams } from '../../abstracts/abstract.repository';
import { StevedoreRepairRepositoryService } from '../repositories/stevedore-repair-repository.service';
import { ArticleRepositoryService } from 'src/app/core/services/repositories/article-repository.service';
import { IRepairItemFull, IRepairItemRow } from '../../models/resources/IRepairItem';
import { ArticleUnitsRepositoryService } from '../repositories/article-units-repository.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { IArticleUnit } from 'src/app/core/models/resources/IArticleUnit';
import { ICurrency } from '../../models/resources/ICurrency';
import * as FileSaver from 'file-saver';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpGet } from '../../models/http/http-get';

@Injectable({
  providedIn: 'root'
})
export class StevedoreRepairMainService {

  constructor(
    private httpClient: HttpClient,
    private stevedoreRepairRepo: StevedoreRepairRepositoryService,
    private articleRepo: ArticleRepositoryService,
    private articleUnitRepo: ArticleUnitsRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
  ) { }

  public fetchAll(params: RepoParams<IStevedoreRepair>) {
    return this.composeStevedoreRepair(
      this.stevedoreRepairRepo.fetchAll(params).pipe(
        switchMap(stevedoreRepairs => of(stevedoreRepairs[0] || {} as IStevedoreRepair))
      ),
      this.articleRepo.fetchAll({}),
      this.articleUnitRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
    );
  }

  private composeStevedoreRepair(
    stevedoreRepair$: Observable<IStevedoreRepair>,
    articles$: Observable<IArticle[]>,
    articleUnits$: Observable<IArticleUnit[]>,
    currencies$: Observable<ICurrency[]>,
  ): Observable<IStevedoreRepairFull> {
    return forkJoin([stevedoreRepair$, articles$, articleUnits$, currencies$]).pipe(
      map(([stevedoreRepair, articles, articleUnits, currencies]) => {
        return this.getStevedoreRepairFull(stevedoreRepair, articles, articleUnits, currencies);
      })
    );
  }

  private getStevedoreRepairFull(
    stevedoreRepair: IStevedoreRepair,
    articles: IArticle[],
    articleUnits: IArticleUnit[],
    currencies: ICurrency[]
  ): IStevedoreRepairFull {
    const currency = currencies.find(c => c.id === stevedoreRepair.currencyId);
    const stevedoreDamageRepairItems = stevedoreRepair.stevedoreDamageRepairItems
      ? stevedoreRepair.stevedoreDamageRepairItems
          .map(item => {
            const article = articles.find(a => a.id === item.articleId);
            const articleUnit = articleUnits.find(au => au.id === article.articleUnitId);
            const repairItemCurrency = currencies.find(c => c.id === article.currencyId);
            return { ...item, article, articleUnit, currency: currency || repairItemCurrency };
          })
      : [];
    return {...stevedoreRepair, stevedoreDamageRepairItems, currency };
  }

  public toRowRepairItems(repairItem: IRepairItemFull): IRepairItemRow {
    const { article, articleUnit, currency } = repairItem;
    return ({
      ...repairItem,
      articleName: article && article.displayLabel,
      unitName: articleUnit && articleUnit.displayLabel,
      pricePerUnit: repairItem.pricePerUnit || repairItem.pricePerUnit === 0
        ? repairItem.pricePerUnit
        : article
            ? article.salePrice || 0
            : 0,
      isoCode: currency && currency.isoCode,
      code: article && article.no,
    });
  }

  public downloadInvoice(id: number): Observable<void> {
    const request = new HttpGet(
      `/api/stevedore-damage-repairs/${id}/invoice`,
      {
        init: {
          responseType: 'blob',
          observe: 'response'
        }
      }
    );
    const source = this.httpClient.request(request)
      .pipe(
        map((response: HttpResponse<Blob>) => {
          const contentType = response.headers.get('content-type');
          const blob = new Blob([response.body], { type: contentType });
          const disposition = response.headers.get('content-disposition');
          const matches = /"([^"]*)"/.exec(disposition);
          const filename = (matches != null && matches[1]) ? matches[1] : 'sdinvoice.txt';
          FileSaver.saveAs(blob, filename);
        }));

    return source;
  }

}
