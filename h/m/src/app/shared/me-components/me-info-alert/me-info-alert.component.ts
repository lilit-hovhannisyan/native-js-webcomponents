import { Component, EventEmitter, Inject, Injector, Output } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { BaseLoggerComponent } from '@features/tracking-system';

import { MeInfoAlertI } from './me-info-alert.interface';

@Component({
  templateUrl: './me-info-alert.component.html',
  styleUrls: ['./me-info-alert.component.scss']
})
export class MeInfoAlertComponent extends BaseLoggerComponent {
  @Output() eventOccurs: EventEmitter<{ eventName: string }> = new EventEmitter();
  componentVersion: string = '0.0.1';

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: MeInfoAlertI, injector: Injector) {
    super(injector);
  }

  customEvent(eventName: string): void {
    this.eventOccurs.emit({ eventName });
  }
}
