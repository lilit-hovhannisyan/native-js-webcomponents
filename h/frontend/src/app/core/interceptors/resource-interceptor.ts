import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpResponse, HttpEventType } from '@angular/common/http';
import { EHttpRequest } from '../models/http/EHttpRequest';
import { Observable } from 'rxjs';
import { skipWhile, map } from 'rxjs/operators';
import { IDataResponseBody } from '../models/responses/IDataResponseBody';
import { ILabelsEnum } from '../models/resources/IWithLabels';

/**
 * Sets `displayLabel` which is of type `IWording` on entities that contain `labelEn` `labelDe`
 */
@Injectable()
export class ResourceInterceptor implements HttpInterceptor {
  public intercept(req: EHttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      map((response: HttpResponse<IDataResponseBody<any>>) => {
        const isEntity = response.body
          && response.body.data
          && !response.body.data.items;

        const isCollection = response.body
          && response.body.data
          && response.body.data.items
          && response.body.data.items.length > 0;

        if (isEntity && this.containsLanguageLabels(response.body.data)) {
          this.addLabel(response.body.data);
        } else if (isCollection && this.containsLanguageLabels(response.body.data.items[0])) {
          response.body.data.items.forEach((entity: any) => {
            this.addLabel(entity);
            return entity;
          });
        }
        return response;
      }),
      skipWhile((event: HttpEvent<any>) => event.type === HttpEventType.Sent),
    );
  }

  private addLabel(entity: any): void {
    entity[ILabelsEnum.displayLabel] = {
      en: entity[ILabelsEnum.labelEn] || '',
      de: entity[ILabelsEnum.labelDe] || ''
    };
  }

  private containsLanguageLabels(entity: any): boolean {
    return Object.keys(entity)
      .some(key =>
        key.indexOf(ILabelsEnum.labelEn) > -1
        || key.indexOf(ILabelsEnum.labelDe) > -1
      );
  }
}
