export interface MeContextMenuI {
  contextMenuItems: { key: string; value: string; disabled?: boolean }[];
}
