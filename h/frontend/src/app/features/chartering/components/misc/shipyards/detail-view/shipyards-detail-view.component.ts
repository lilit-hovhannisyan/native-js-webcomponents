import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { sitemap } from '../../../../../../core/constants/sitemap/sitemap';
import { url } from '../../../../../../core/constants/sitemap/sitemap-entry';
import { BaseComponentService } from '../../../../../../core/services/base-component.service';
import { ResourceDetailView } from '../../../../../../core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { IShipyardCreate, IShipyard } from 'src/app/core/models/resources/IShipyard';
import { ShipyardRepositoryService } from 'src/app/core/services/repositories/shipyard-repository.service';

@Component({
  templateUrl: './shipyards-detail-view.component.html',
  styleUrls: ['./shipyards-detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShipyardsDetailViewComponent
  extends ResourceDetailView<IShipyard, IShipyardCreate>
  implements OnInit {

  public repoParams = {};
  public routeBackUrl = url(sitemap.chartering.children.misc.children.shipyards);

  protected resourceDefault = {} as IShipyard;

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    shipyardRepo: ShipyardRepositoryService,
    baseComponentService: BaseComponentService,
    private cdr: ChangeDetectorRef,
  ) {
    super(shipyardRepo, baseComponentService);
  }

  public ngOnInit() {
    this.init();
    const { id } = this.route.snapshot.params;
    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => this.resource.label;

  public init(shipyard: IShipyard = this.resourceDefault) {
    this.initForm(
      this.fb.group({
        label: [shipyard.label, [Validators.required]],
      }));
    this.cdr.detectChanges();
  }

  public submit() {
    if (!this.form.valid) { return; }

    const { value } = this.form;
    const shipyard = this.creationMode
      ? { ...value }
      : { ...this.resource, ...value };

    this.creationMode
      ? this.createResource(shipyard)
      : this.updateResource(shipyard);
  }
}
