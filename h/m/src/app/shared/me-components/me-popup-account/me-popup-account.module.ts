import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MePopupAccountComponent } from './me-popup-account.component';
import { IconsModule } from '@shared/modules/icons.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MeButtonModule } from '../me-button/me-button.module';

@NgModule({
  declarations: [MePopupAccountComponent],
  imports: [CommonModule, IconsModule, MatDialogModule, MeButtonModule],
  exports: [MePopupAccountComponent]
})
export class MePopupAccountModule {}
