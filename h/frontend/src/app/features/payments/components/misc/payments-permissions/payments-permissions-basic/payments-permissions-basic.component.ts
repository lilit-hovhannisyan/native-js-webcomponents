import { Component, OnInit, SkipSelf, OnDestroy, ViewChild } from '@angular/core';
import { NvGridConfig, GridComponent } from 'nv-grid';
import { Observable, ReplaySubject, combineLatest } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { MandatorKey, IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { IUser, UserKey } from 'src/app/core/models/resources/IUser';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { getUniqueResources } from 'src/app/shared/helpers/general';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { Language } from 'src/app/core/models/language';
import { wording } from 'src/app/core/constants/wording/wording';
import { IPaymentPermission } from 'src/app/core/models/resources/IPaymentsPermission';
import { getPermissionGroupMandatorsGridConfig } from 'src/app/core/constants/nv-grid-configs/permission-group-mandators.config';
import { getPermissionGroupUsersGridConfig } from 'src/app/core/constants/nv-grid-configs/permission-group-users.config';
import { PermissionsGroupsUserModalComponent } from 'src/app/shared/components/permissions-groups-users-modal/permissions-groups-users-modal.component';
import { PermissionsGroupsMandatorModalComponent } from 'src/app/shared/components/permissions-groups-mandators-modal/permissions-groups-mandators-modal.component';
import { PermissionsGroupsHelperService } from 'src/app/core/services/helperService/permissions-groups-helper.service';
import { IPaymentMandatorUserConnection } from 'src/app/core/models/resources/IPaymentsMandatorUserConnection';

@Component({
  templateUrl: './payments-permissions-basic.component.html',
  styleUrls: ['./payments-permissions-basic.component.scss']
})
export class PaymentsPermissionsBasicComponent implements OnInit, OnDestroy {
  private componentDestroyed$ = new ReplaySubject<boolean>(1);
  public language: Language = this.baseComponentService.settingsService.language;

  public mandators: IMandatorLookup[] = [];
  public allUsers: IUser[] = [];
  public selectedGroup: IPaymentPermission;

  public mandatorsGridConfig: NvGridConfig;
  public usersGridConfig: NvGridConfig;
  @ViewChild('mandatorsGrid', { read: GridComponent, static: false })
  public mandatorsGridComponent: GridComponent;
  @ViewChild('usersGrid', { read: GridComponent, static: false })
  public usersGridComponent: GridComponent;

  constructor(
    @SkipSelf() public helperService: PermissionsGroupsHelperService<
      IPaymentMandatorUserConnection,
      IPaymentPermission
    >,
    private authService: AuthenticationService,
    private baseComponentService: BaseComponentService,
  ) { }

  public ngOnInit() {
    this.initGrids(this.selectedGroup);
    this.configureMandatorsGrid();
    this.configureUsersGrid();
    this.listenSelectedGroupChange();
    this.listenEditModeChange();
  }

  private listenSelectedGroupChange(): void {
    combineLatest([
      this.getSelectedGroup(),
      this.getMandators(),
      this.getUsers(),
    ]).pipe(takeUntil(this.componentDestroyed$)).subscribe(() => {
      this.initGrids(this.selectedGroup);
    });
  }

  private cleanGridsFilters(): void {
    if (!this.usersGridComponent || !this.mandatorsGridComponent) {
      return;
    }

    this.usersGridConfig.columns.forEach(col => {
      col.filter = {
        ...col.filter,
        values: [],
      };
    });
    this.usersGridComponent.filterGrid();

    this.mandatorsGridConfig.columns.forEach(col => {
      col.filter = {
        ...col.filter,
        values: [],
      };
    });
    this.mandatorsGridComponent.filterGrid();
  }

  private listenEditModeChange(): void {
    this.helperService.inCreateMode$.pipe(takeUntil(this.componentDestroyed$)).subscribe(createMode => {
      if (createMode) {
        this.initGrids();
      }
    });
    this.helperService.inEditMode$.pipe(takeUntil(this.componentDestroyed$)).subscribe(() => {
      // to show all actions buttons on grids and clean filters
      this.cleanGridsFilters();
      this.configureMandatorsGrid();
      this.configureUsersGrid();
    });
  }

  private getSelectedGroup(): Observable<IPaymentPermission> {
    return this.helperService.selectedGroup$.pipe(
      tap(group => this.selectedGroup = group),
    );
  }
  private getMandators(): Observable<IMandatorLookup[]> {
    return this.helperService.mandators$.pipe(
      tap(mandators => this.mandators = mandators)
    );
  }
  private getUsers(): Observable<IUser[]> {
    return this.helperService.users$.pipe(
      tap(users => this.allUsers = users)
    );
  }

  private initGrids(group?: IPaymentPermission): void {
    const { usersIds, mandatorsIds } = this.helperService
      .connections2Ids(group?.connections || []);

    this.helperService.selectedMandators$.next(this.getRelatedMandators(mandatorsIds));
    this.helperService.selectedUsers$.next(this.getRelatedUsers(usersIds));

    // to clean grids filters
    this.cleanGridsFilters();
    this.configureMandatorsGrid();
    this.configureUsersGrid();
  }

  // get relations
  private getRelatedMandators(mandatorsIds: MandatorKey[]): IMandatorLookup[] {
    if (!mandatorsIds.length || !this.mandators.length) {
      return [];
    }

    return this.mandators.filter(mandator => {
      return mandatorsIds.indexOf(mandator.id) !== -1;
    });
  }
  private getRelatedUsers(usersIds: UserKey[]): IUser[] {
    if (!usersIds.length || !this.allUsers.length) {
      return [];
    }

    return this.allUsers.filter(user => {
      return usersIds.indexOf(user.id) !== -1;
    });
  }

  // configure grids
  private configureMandatorsGrid(): void {
    const actions = {
      add: this.addMandator,
      delete: this.deleteMandator,
    };
    this.mandatorsGridConfig = getPermissionGroupMandatorsGridConfig(
      actions,
      (operation: number) => this.helperService.editMode && this.authService.canPerformOperationOnCurrentUrl(operation),
      false, // isAutobankPage - needed to set correct gridName
    );
  }
  private configureUsersGrid(): void {
    const actions = {
      add: this.addUser,
      delete: this.deleteUser,
    };
    this.usersGridConfig = getPermissionGroupUsersGridConfig(
      actions,
      (operation: number) => this.helperService.editMode && this.authService.canPerformOperationOnCurrentUrl(operation),
      false, // isAutobankPage - needed to set correct gridName
    );
  }

  private deleteMandator = (mandator: IMandatorLookup): void => {
    const mandators = this.helperService.selectedMandators$.getValue().filter(m => m.id !== mandator.id);
    this.helperService.selectedMandators$.next(mandators);

    this.helperService.hasChanges$.next(true);
  }

  private deleteUser = (user: IUser): void => {
    const users = this.helperService.selectedUsers$.getValue().filter(u => u.id !== user.id);
    this.helperService.selectedUsers$.next(users);

    this.helperService.hasChanges$.next(true);
  }

  private addUser = (): void => {
    this.baseComponentService.nzModalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.system.users[this.language],
      nzClosable: true,
      nzFooter: null,
      nzBodyStyle: { padding: '0', width: '600px' },
      nzContent: PermissionsGroupsUserModalComponent,
      nzComponentParams: {
        users: getUniqueResources(this.helperService.selectedUsers$.getValue(), this.allUsers),
        isAutobankPage: false,
      },
    }).afterClose.subscribe((result: IUser[]) => {
      if (!result) {
        return;
      }
      this.helperService.selectedUsers$.next([ ...this.helperService.selectedUsers$.getValue(), ...result ]);
      this.helperService.hasChanges$.next(true);
    });
  }

  private addMandator = (): void => {
    this.baseComponentService.nzModalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.accounting.mandators[this.language],
      nzClosable: true,
      nzFooter: null,
      nzBodyStyle: { padding: '0', width: '600px' },
      nzContent: PermissionsGroupsMandatorModalComponent,
      nzComponentParams: {
        mandators: getUniqueResources(this.helperService.selectedMandators$.getValue(), this.mandators),
      },
    }).afterClose.subscribe((result: IMandatorLookup[]) => {
      if (!result) {
        return;
      }
      this.helperService.selectedMandators$.next([ ...this.helperService.selectedMandators$.getValue(), ...result ]);
      this.helperService.hasChanges$.next(true);
    });
  }

  public customValidation = (): boolean => !this.helperService.hasChanges$.getValue();

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
