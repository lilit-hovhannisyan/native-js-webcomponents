import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IBank, IBankCreate } from 'src/app/core/models/resources/IBank';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { Validators } from 'src/app/core/classes/validators';
import { BankRepositoryService } from 'src/app/core/services/repositories/bank-repository.service';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';

@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BankDetailViewComponent
extends ResourceDetailView<IBank, IBankCreate>
implements OnInit {

  protected repoParams = {};
  protected resourceDefault = { isActive: true } as IBank;

  public routeBackUrl = url(sitemap.system.children.misc.children.banks);
  public form: FormGroup;

  constructor(
    private cdr: ChangeDetectorRef,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    protected bankRepositoryService: BankRepositoryService,
    baseComponentService: BaseComponentService
  ) {
    super(bankRepositoryService, baseComponentService);
  }

  public ngOnInit(): void {
    this.init();

    const { id } = this.route.snapshot.params;

    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id).subscribe(() => this.cdr.detectChanges());
  }

  public getTitle = () => `${this.resource.name} (${this.resource.bic})`;

  public init(bank: IBank = this.resourceDefault): void {
    this.initForm(this.fb.group({
      name: [bank.name, [Validators.required]],
      code: [bank.code, [Validators.required, Validators.min(0)]],
      bic: [bank.bic, [Validators.required, Validators.isBic]],
      zipCode: [bank.zipCode, [Validators.required, Validators.min(0)]],
      city: [bank.city, [Validators.required]],
      street: [bank.street, [Validators.required]],
      countryId: [bank.countryId, [Validators.required]],
      differentAccountHolder: [bank.differentAccountHolder, [Validators.required]],
      isActive: [bank.isActive]
    }));
  }

  public submit(): void {
    if (!this.form.valid) { return; }

    const { value } = this.form;
    const bankEdited = this.creationMode
      ? { ...value }
      : { ...this.resource, ...value };

    this.creationMode
      ? this.createResource(bankEdited)
      : this.updateResource(bankEdited);
  }
}
