import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IWording, ReplacementSubjects } from 'src/app/core/models/resources/IWording';
import { setTranslationSubjects } from '../../helpers/general';


@Component({
  selector: 'nv-modal-confirmation-content',
  templateUrl: './modal-confirmation-content.component.html',
  styleUrls: ['./modal-confirmation-content.component.scss'],
})
export class ModalConfirmationContentComponent implements OnInit {
  @Input() public wording: IWording;
  @Input() public subjects: ReplacementSubjects = {};

  public ngOnInit(): void {
    // if there is no wording, replace it with empty wording

    if (this.wording) {
      this.replaceSubjects();
    } else {
      this.wording = { en: '', de: '' };
    }
  }

  private replaceSubjects(): void {
    // if there is no subject, keep wording as it it.
    if (this.subjects) {
      this.wording = setTranslationSubjects(this.wording, this.subjects);
    }
  }
}
