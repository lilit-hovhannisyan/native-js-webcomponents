import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { IDebitorCreditor, IDebitorCreditorCreate, IDebitorCreditorBasicTab } from 'src/app/core/models/resources/IDebitorCreditor';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { DebitorCreditorRepositoryService } from 'src/app/core/services/repositories/debitor-creditor-repository.service';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { FormBuilder } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { ActivatedRoute } from '@angular/router';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { ICompany } from 'src/app/core/models/resources/ICompany';
import { startWith, mergeMap, map, tap, finalize } from 'rxjs/operators';
import { forkJoin, of } from 'rxjs';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { NvGridConfig } from 'nv-grid';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/companies.config';
import { DebitorAndCreditorsHelperService } from '../debitor-and-creditors.helper.service';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';

@Component({
  selector: 'nv-basic',
  templateUrl: './basic.component.html',
  styleUrls: ['./basic.component.scss']
})
export class DebitorCreditorBasicTabComponent extends ResourceDetailView<
IDebitorCreditor,
IDebitorCreditorCreate
> implements OnInit {
  public routeBackUrl = this.url(sitemap.accounting.children.booking.children.debitorCreditors);
  public countryPath = this.url(sitemap.system.children.misc.children.companies);
  public companies: ICompany[] = [];
  public logoId: string;
  private debitorCreditorId: string;
  private creditorAccountNo: number;
  private debitorAccountNo: number;
  public gridConfig: NvGridConfig = getGridConfig(this.router, () => false);
  public formElement: ElementRef;

  protected resourceDefault = {
    isActive: true
  } as IDebitorCreditorBasicTab;
  protected repoParams = {};

  constructor(
    baseComponentService: BaseComponentService,
    debitorCreditorRepo: DebitorCreditorRepositoryService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private companyRepo: CompanyRepositoryService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef,
    private bookingAccountService: BookingAccountRepositoryService,
    private debitorAndCreditorsHelperService: DebitorAndCreditorsHelperService,
  ) {
    super(debitorCreditorRepo, baseComponentService);
  }

  public ngOnInit() {
    this.debitorCreditorId = this.route.parent.snapshot.params.id;

    this.initDebitorCreditor();
  }

  public getTitle = () => this.resource.no.toString();

  public init(dc: IDebitorCreditorBasicTab = this.resourceDefault) {
    let baseFormGroup: any = {
      companyId: [dc.companyId, Validators.required],
      customersGroup: [dc.customersGroup, [Validators.required, Validators.maxLength(150)]],
      isActive: [dc.isActive],
      isIntercompanyRelevant: [dc.isIntercompanyRelevant, Validators.required]
    };

    if (!this.creationMode) {
      baseFormGroup = {
        ...baseFormGroup,
        no: [dc.no, [Validators.required, Validators.positiveNumber]],
        debitorAccountNo: [dc.debitorAccountNo],
        creditorAccountNo: [dc.creditorAccountNo],
        debitorAccountId: [dc.debitorAccountId],
        creditorAccountId: [dc.creditorAccountId],
      };
      this.setDisabledFields(['debitorAccountNo', 'creditorAccountNo', 'no']);
    }
    this.initForm(this.fb.group(baseFormGroup));
    this.form.get('companyId').valueChanges
      .pipe(startWith(dc.companyId))
      .subscribe(cid => {
        const currentCompany = this.companies.find(company => company.id === cid);
        if (currentCompany) {
          this.logoId = currentCompany.logoId;
        }
      });
  }

  public submit() {
    if (this.form.invalid) { return; }

    const formValue = this.form.getRawValue();
    const freshResource = this.creationMode
      ? formValue
      : ({ ...this.resource, ...formValue });

    this.creationMode
      ? this.createResource(freshResource)
        .subscribe(createdResource => {
          this.debitorAndCreditorsHelperService.generateTitle(createdResource, this.companies);
          this.debitorCreditorId = createdResource.id.toString();
          this.initDebitorCreditorAccountNos(createdResource);
        })
      : this.updateResource(freshResource)
        .subscribe(updatedResource => {
          this.debitorAndCreditorsHelperService.generateTitle(updatedResource, this.companies);
          this.initDebitorCreditorAccountNos(updatedResource);
        });
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

  private fetchDebitorCreditorCompany(dc: IDebitorCreditor) {
    return this.companyRepo.fetchOne(dc.companyId, {})
      .pipe(
        tap(company => this.companies = [...this.companies, company])
      );
  }

  private initDebitorCreditor() {
    this.companyRepo.fetchAllWithoutDebitorCreditor().subscribe(companies => {
      this.companies = [...this.companies, ...companies];

      this.route.parent.snapshot.params.id === 'new'
        ? this.enterCreationMode()
        : this.loadResource(this.debitorCreditorId)
          .pipe(
            mergeMap(dc => forkJoin([
              this.bookingAccountService.fetchOne(dc.creditorAccountId, {}),
              this.bookingAccountService.fetchOne(dc.debitorAccountId, {}),
              of(dc),
              this.fetchDebitorCreditorCompany(dc),
            ])),
            map(this.mergeWithDebitorCreditorAccountNos),
          )
          .subscribe((dc) => this.initializeResource(dc));
    });
  }

  private initDebitorCreditorAccountNos = (debitorCreditor: IDebitorCreditor): void => {
    if (!this.debitorAccountNo || !this.creditorAccountNo) {
      forkJoin([
        this.bookingAccountService.fetchOne(debitorCreditor.creditorAccountId, {}),
        this.bookingAccountService.fetchOne(debitorCreditor.debitorAccountId, {}),
        of(debitorCreditor),
      ])
        .pipe(
          map(this.mergeWithDebitorCreditorAccountNos))
        .subscribe((dc) => this.initializeResource(dc));
    } else {
      const debitorCreditorWithNos = {
        ...debitorCreditor,
        creditorAccountNo: this.creditorAccountNo,
        debitorAccountNo: this.debitorAccountNo,
      };

      this.initializeResource(debitorCreditorWithNos);
    }
  }

  private mergeWithDebitorCreditorAccountNos = (
    [cAccount, dAccount, dc]: [
      IBookingAccount,
      IBookingAccount,
      IDebitorCreditorBasicTab,
      ICompany?,
    ]
  ) => {
    this.creditorAccountNo = cAccount.no;
    this.debitorAccountNo = dAccount.no;

    return {
      ...dc,
      creditorAccountNo: cAccount.no,
      debitorAccountNo: dAccount.no,
    } as any as IDebitorCreditorBasicTab;
  }
}
