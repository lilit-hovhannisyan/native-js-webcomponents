import { IResource } from './IResource';
import { CompanyKey } from './ICompany';
import { IWithActive } from './IWithActive';
import { CurrencyKey, ICurrency } from './ICurrency';

export type SilentPartnerRepresentativeKey = number;

export enum LegalRepresentativeType {
  PrivateCompany = 1,
  SilentPartner = 2
}

interface ISharedProps extends IWithActive {
  companyId: CompanyKey;
  partnerId: CompanyKey;
  from: string;
  to: string;
  remark: string;
  isAtypicalSilentPartner: boolean;
  type: LegalRepresentativeType;
  currencyId: CurrencyKey;
  contribution: number;
}

export interface ISilentPartnerRepresentativeBase extends ISharedProps, IResource<SilentPartnerRepresentativeKey> { }

export interface ISilentPartnerRepresentativeCreate extends ISharedProps { }

export interface ISilentPartnerRepresentativeDTO extends ISilentPartnerRepresentativeBase { }

export interface ISilentPartnerRepresentative extends ISilentPartnerRepresentativeDTO { }

export interface ISilentPartnerRaw extends ISilentPartnerRepresentative {
  currencyIsoCode: string;
  partnerCompanyName: string;
}
