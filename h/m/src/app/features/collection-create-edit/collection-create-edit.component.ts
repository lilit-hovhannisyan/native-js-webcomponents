import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { AfterViewInit, Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatHorizontalStepper, MatStep } from '@angular/material/stepper';
import { Router, ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';

import { forkJoin, Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, finalize, skip, take } from 'rxjs/operators';
import { SubSink } from 'subsink';

import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { WindowRef } from '@core-providers/window-ref.provider';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { GlobalService } from '@core-services/global.service';
import { DeactivatableComponent } from '@core-interfaces/deactivatable-component.interface';
import { RouterStoreService } from '@core-services/router-store.service';

import { CollectionCreateEditWebService } from '@features/collection-create-edit/collection-create-edit.web-service';
import { CollectionEntityI } from '@features/collection-create-edit/collection-entity.interface';
import { BaseLoggerComponent } from '@features/tracking-system';
import { MaterialsWebService } from '@shared/services/materials.web-service';
import { CollectionsWebService } from '@features/collections/collections.web-service';
import { DocumentStoreService } from '@features/documents/documents-store.service';
import { DocumentsWebService } from '@features/documents/documents.web-service';

import {
  ME_DOMAIN_APPLICATION_COLLECTION,
  MARKETPLACE_BASE_API_URL,
  ME_DOMAIN_APPLICATION_BUCKET,
  ME_DOMAIN_APPLICATION_MATERIAL,
  ME_DOMAIN_APPLICATION_DOCUMENT,
  ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL,
  ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL,
  ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL_VIDEO
} from '@shared/constants';
import { FileWebService } from '@shared/services/file.web-service';
import { InputAttributeService } from '@shared/services/input-attribute.service';
import { MaterialsStoreService } from '@shared/services/materials-store.service';
import { MeAttributeLayoutTypes } from '@shared/me-attribute-layout-types';
import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';
import { MeBasicAlertI } from '@shared/me-components/me-basic-alert/me-basic-alert.interface';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeButtonI } from '@shared/me-components/me-button/me-button.interface';
import { MeEmptyContentI } from '@shared/me-components/me-empty-content/me-empty-content-interface';
import { MeMaterialCardI } from '@shared/me-components/me-material-card/me-material-card.interface';
import { MePopupSelectI } from '@shared/me-components/me-popup-select/me-popup-select.interface';
import { MePopupSelectService } from '@shared/me-components/me-popup-select/me-popup-select.service';
import { MeSearchBarI } from '@shared/me-components/me-search-bar/me-search-bar.interface';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { MeUploadImageI } from '@shared/me-components/me-upload-image/me-upload-image.interface';
import { RequiredConstraintState } from '@shared/models/attribute/attribute.constraint.model';
import { AttributeModelTypes, DMChoiceAttributeModel } from '@shared/models/attribute/attribute.model';
import { CollectionModel } from '@shared/models/collection/collection.model';
import { DomainModel } from '@shared/models/domain/domain.model';
import { FileUploadResponseModel } from '@shared/models/file-upload/file-upload-response.model';
import { LayoutGroupAttributeModel } from '@shared/models/layout/layout-group-attribute.model';
import { LayoutGroupModel } from '@shared/models/layout/layout-group.model';
import { MaterialModel } from '@shared/models/materials/material-model';
import { bytesToSize, getVisibilityObject, mapExtensionToIcon } from '@shared/utils';
import { QuickSearchModel } from '@shared/models/search/quick-search.model';
import { MeDocumentCardI } from '@shared/me-components/me-document-card/me-document-card.interface';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { LayoutRowModel } from '@shared/models/layout/layout-row.model';
import { MeBreadcrumbActionI } from '@shared/me-components/me-breadcrumb/me-breadcrumb-action.interface';
import { StateModel } from '@shared/models/state/state.model';
import { MeSearchDirectionTypes, MeSearchSortByTypes } from '@shared/me-search-sort-by-types';
import { PropertiesModel } from '@shared/models/attribute/attribute.properties.model';
import { MeSelectInputAttribute } from '@shared/models/attribute/me-select-input-attribute.model';
import { MeSelectInputComponent } from '@shared/me-components/me-attribute/me-select-input/me-select-input.component';
import { MePopupVideoService } from '@shared/me-components/me-popup-video/me-popup-video.service';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-collection-create-edit',
  templateUrl: './collection-create-edit.component.html',
  styleUrls: ['./collection-create-edit.component.scss'],
  providers: [
    CollectionCreateEditWebService,
    CollectionsWebService,
    MePopupSelectService,
    MaterialsWebService,
    DocumentsWebService,
    MaterialsStoreService,
    DocumentStoreService,
    FileWebService,
    InputAttributeService
  ]
})
export class CollectionCreateEditComponent extends BaseLoggerComponent implements OnInit, OnDestroy, AfterViewInit, DeactivatableComponent {
  componentVersion: string = '0.0.1';

  applyCheck: boolean = false;
  leaveWarningModel!: MeSweetAlertI;

  private subs = new SubSink();

  domain: DomainModel | undefined;
  groups: LayoutGroupModel[] = [];
  collection!: CollectionModel;
  extendedGroups: LayoutGroupModel[] = [];
  attributes: AttributeModelTypes[] = []; // array of general collection attribute objects (tab 1)
  componentLoading: boolean = true;
  stepOneGroups: string[] = ['generalAttributes'];
  generalInfoLayoutGroups!: LayoutGroupModel[];
  selectedCollectionType!: string;
  buttonNext!: { label: string; data: MeButtonI };
  buttonAdd!: { label: string; data: MeButtonI };
  buttonBack!: { label: string; data: MeButtonI };
  buttonCancel!: { label: string; data: MeButtonI };
  buttonCreate!: { label: string; data: MeButtonI };
  buttonSubmit!: { label: string; data: MeButtonI };

  private appliedState: RequiredConstraintState = RequiredConstraintState.draft;
  breadcrumbActions: MeBreadcrumbActionI[] = [];

  // Tab forms
  collectionInfoAttributeForm!: FormGroup;
  collectionTypeAttributeItem!: MeAttributeItem;
  collectionInfoAttributeItemMap!: Map<string, MeAttributeItem>;
  generalAttributeItemMap!: Map<string, MeAttributeItem>;
  collectionPropertiesAttributeItemMap!: Map<string, MeAttributeItem>;
  uploadImageData!: MeUploadImageI;

  collectionTypesAttribute: DMChoiceAttributeModel = new DMChoiceAttributeModel();

  private thumbnailAttributeName = 'thumbnail';
  invalidThumbnail: boolean = false;
  private collectionId: string | null = '';
  private queryTab: string | null = '';

  @ViewChild('stepper') stepper!: MatHorizontalStepper;

  // Buttons used in form
  buttons: Map<string, { label: string; data: MeButtonI }> = new Map();

  // Variables needed for Manager Materials tab - TAB 2
  materialsForm: FormGroup = new FormGroup({});
  @ViewChild('createButton', { read: TemplateRef }) createButton!: TemplateRef<Component>;
  private readonly NUMBER_OF_ITEMS_ON_PAGE = 50;
  // Collection materials
  emptyContentCollectionMaterials: MeEmptyContentI | null = null;
  collectionMaterialCardItems!: MeMaterialCardI[];
  selectionCollectionMaterialEnabled: boolean = false;
  searchBarDataCollectionMaterial!: MeSearchBarI;
  isSearchCollectionMaterial: boolean = false;
  trackForBottomReachCollectionMaterials: boolean = false;
  // Popup materials
  materialCardItemsPopup: MeMaterialCardI[] = [];
  @ViewChild('popupMaterialGrid', { read: TemplateRef }) materialGridRefPopup!: TemplateRef<Component>;
  materialDataPopup!: MePopupSelectI;
  searchBarDataMaterialsPopup!: MeSearchBarI;
  trackForBottomReachMaterialsPopup: boolean = false;
  selectedMaterialsStorePopup!: MaterialsStoreService;
  showSelectedMaterialsPopup: boolean = false;

  // Variables needed for Manager Documents tab - TAB 3
  // Collection documents
  emptyContentCollectionDocuments: MeEmptyContentI | null = null;
  cardItemsCollectionDocuments: MeDocumentCardI[] = [];
  selectionCollectionDocumentEnabled: boolean = false;
  searchBarDataCollectionDocuments!: MeSearchBarI;
  trackForBottomReachCollectionDocuments: boolean = false;
  // Popup documents
  cardItemsDocumentsPopup: MeDocumentCardI[] = [];
  searchBarDataDocumentsPopup!: MeSearchBarI;
  @ViewChild('popupDocumentGrid', { read: TemplateRef }) documentGridRefPopup!: TemplateRef<Component>;
  trackForBottomReachDocumentsPopup: boolean = false;
  private selectedDocumentType: string = ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL;
  private documentFilterChanged: Subject<void> = new Subject();
  selectedDocumentsStorePopup!: DocumentStoreService;
  showSelectedDocumentsPopup: boolean = false;
  filterDelay: number = 500;

  documentsForm: FormGroup = new FormGroup({});
  buttonFinish!: { label: string; data: MeButtonI };
  materialsStepEnabled: boolean = false;
  documentsStepEnabled: boolean = false;
  isDuplicateCollection: boolean = false;

  areMaterialsLoadingPopup: boolean = false;
  areDocumentsPopupLoading: boolean = false;
  isDocumentPopupSearch: boolean = false;
  isMaterialPopupSearch: boolean = false;
  isCollectionMaterialsSearch: boolean = false;
  isCollectionDocumentSearch: boolean = false;
  areMaterialsLoading: boolean = false;
  areDocumentsLoading: boolean = false;
  autoDeactivateLoader: boolean = false;

  private documentPopupData!: MePopupSelectI;

  _listenForDocumentTypeChangeSubscription!: Subscription;
  _listenForDocumentFilterChangeSubscription!: Subscription;
  _removeMaterialSweetAlert!: Subscription;

  i18nStrings: MeTranslationI = {};

  constructor(
    injector: Injector,
    private translate: MeTranslationService,
    private definitionsStoreService: DefinitionsStoreService,
    private meBasicAlertService: MeBasicAlertService,
    private meSweetAlertService: MeSweetAlertService,
    private inputAttributeService: InputAttributeService,
    private collectionWebService: CollectionCreateEditWebService,
    private fileWebService: FileWebService,
    private router: Router,
    private route: ActivatedRoute,
    private materialsStoreServicePopup: MaterialsStoreService,
    private popupSelectService: MePopupSelectService,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private globalService: GlobalService,
    private _materialsWebService: MaterialsWebService,
    private _documentStoreServicePopup: DocumentStoreService,
    private _materialsStoreServiceCollectionMaterials: MaterialsStoreService,
    private _documentStoreServiceCollectionDocument: DocumentStoreService,
    private routerStoreService: RouterStoreService,
    private documentsWebService: DocumentsWebService,
    private collectionsWebService: CollectionsWebService,
    private _windowRef: WindowRef,
    private _videoPopupService: MePopupVideoService
  ) {
    super(injector);
    this._documentStoreServiceCollectionDocument = new DocumentStoreService();
    this._materialsStoreServiceCollectionMaterials = new MaterialsStoreService();
    this.selectedMaterialsStorePopup = new MaterialsStoreService();
    this.selectedDocumentsStorePopup = new DocumentStoreService();
  }

  ngOnInit(): void {
    this.collectionId = this.route.snapshot.paramMap.get('id');
    this.queryTab = this.route.snapshot.paramMap.get('tab');
    this.isDuplicateCollection = this.router.url.indexOf('/collections/duplicate') > -1;

    // subscribe to translations
    this.subs.sink = this.translate.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;
      this.prepareButtons();
    });

    this.leaveWarningModel = {
      icon: 'alert-triangle',
      mode: 'warning',
      title: this.i18nStrings.warning,
      message: this.i18nStrings.youWillLoseAllUnsavedData,
      type: { name: MeSweetAlertTypeEnum.submit, buttons: { submit: 'Leave', cancel: 'Stay on page' } }
    };

    this.breadcrumbActions = [
      {
        button: {
          id: 'cancelButton',
          data: { color: 'secondary', type: 'regular' },
          text: this.i18nStrings.cancel
        },
        callBack: () => {
          const lastRoute = this.routerStoreService.getPreviousUrl();
          if (lastRoute === decodeURI(this.router.routerState.snapshot.url)) {
            this.router.navigate(['collections']);
          } else {
            this.router.navigate([lastRoute]);
          }
        }
      }
    ];
    this.breadcrumbSupportService.breadcrumbActions = this.breadcrumbActions;

    this.setSearchBarDataCollectionMaterials();
    this.setSearchBarDataCollectionDocuments();
    this.setSearchBarDataDocumentsPopup();

    this.generateEmptyContentMaterial();
    this.generateEmptyContentDocument();

    this.globalService.activateLoader();
    this.subs.sink = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.prepareCardData();
        if (this.collectionId) {
          this.initializeEdit();
        } else {
          this.initializeCreate();
        }
      }
    });
  }

  ngAfterViewInit(): void {
    // TODO: can be sync with new loader, now there is small delay on loading finish
    if (this.collectionId) {
      this.subs.sink = this.stepper.animationDone.pipe(take(1)).subscribe(() => {
        this.stepper.steps.forEach((s) => (s.interacted = true));
      });
    }
  }

  private prepareFilterData(): void {
    // Override document type dispaly value with 'All documents'
    const documentTypes = [ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL, ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL].map((dom) => {
      const domainObj = this.definitionsStoreService.getDomain(dom);
      return {
        displayName: domainObj.displayName,
        value: domainObj.internalName
      };
    });

    const dmDocumentTypeFilterAttribute: DMChoiceAttributeModel = {
      constraints: [],
      dataTypeClass: 'moa',
      displayName: this.i18nStrings.documentType,
      internalName: 'documentType',
      properties: new PropertiesModel(),
      required: false,
      optRequired: false,
      value: this.selectedDocumentType,
      values: documentTypes,
      disabled: false,
      unique: false
    };

    const meDocumentTypeFilterAttribute: MeSelectInputAttribute = new MeSelectInputAttribute();
    meDocumentTypeFilterAttribute.data = dmDocumentTypeFilterAttribute;
    meDocumentTypeFilterAttribute.form = new FormGroup({
      [dmDocumentTypeFilterAttribute.internalName]: new FormControl(dmDocumentTypeFilterAttribute.value)
    });

    this.searchBarDataDocumentsPopup = {
      ...this.searchBarDataDocumentsPopup,
      singleValueFilter: new MeAttributeItem(MeSelectInputComponent, meDocumentTypeFilterAttribute)
    };

    this._listenForDocumentTypeChangeSubscription = meDocumentTypeFilterAttribute.form.controls.documentType.valueChanges
      .pipe(skip(1))
      .subscribe((value) => {
        this.selectedDocumentType = value;
        this.documentFilterChanged.next();
      });
    this.subs.sink = this._listenForDocumentTypeChangeSubscription;
  }

  private initializeCreate(): void {
    if (this.definitionsStoreService.thumbnailProperties) {
      const splitString = this.definitionsStoreService.thumbnailProperties.supportedFiles.split(',');
      this.uploadImageData = {
        title: this.i18nStrings.collectionThumbnail,
        thumbnail: '',
        thumbnailProperties: this.definitionsStoreService.thumbnailProperties,
        uploadImageButton: { type: 'regular', color: 'primary' },
        isThumbnailStatusInvalid: false,
        acceptPhotoExtensions: `.${splitString.join(',.')}`,
        supportedThumbnailFiles: this.definitionsStoreService.thumbnailProperties.supportedFiles.replace(/,/g, ', '),
        maxUploadSize: bytesToSize(this.definitionsStoreService.thumbnailProperties.maxSize)
      };
    }

    this.domain = this.definitionsStoreService.getDescendantDomain(ME_DOMAIN_APPLICATION_BUCKET, ME_DOMAIN_APPLICATION_COLLECTION);
    this.globalService.deactivateLoader();

    if (this.domain) {
      // transform each domain attribute to AttributeModel class or its extensions (classes that extend it)
      this.attributes = this.inputAttributeService.transformAttributesArray(this.domain.attributes);

      // populate groups from selected layout
      this.groups = this.inputAttributeService.getLayoutGroups(this.domain.layouts, MeAttributeLayoutTypes.CREATE);

      // sort and generate rows
      this.groups = this.inputAttributeService.generateLayoutRows(this.groups);

      // generate angular forms which will have form group as root for every tab
      // each attribute will be a form control
      // add validators to attribtes where applicable
      // load enums to single/multiple choices elements
      this.generalInfoLayoutGroups = this.getLayoutForGroups(this.stepOneGroups);
      if (this.generalInfoLayoutGroups && this.generalInfoLayoutGroups.length) {
        this.collectionInfoAttributeForm = this.inputAttributeService.createFormGroup(
          this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups),
          this.appliedState
        );

        this.collectionInfoAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
          this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups),
          this.collectionInfoAttributeForm,
          this.appliedState
        );
      }

      // get attribute and create control for thumbnail
      const isRequired: boolean = this.inputAttributeService.isAttributeRequired(
        this.attributes,
        this.thumbnailAttributeName,
        this.appliedState
      );
      const validators: ValidatorFn[] = [];
      if (isRequired) {
        validators.push(Validators.required);
      }

      this.collectionInfoAttributeForm.addControl(this.thumbnailAttributeName, new FormControl(null, validators));

      this.subs.sink = this.collectionInfoAttributeForm.valueChanges.subscribe(() => {
        if (this.collectionInfoAttributeForm.dirty) {
          this.applyCheck = true;
        }
      });
    }
  }

  initializeEdit(): void {
    this.collectionWebService.getCollectionById(encodeURI(this.collectionId as string)).subscribe((response) => {
      this.collection = response;
      if (!this.isDuplicateCollection) {
        this.materialsStepEnabled = true;
        this.documentsStepEnabled = true;
      }

      forkJoin([this.getCollectionMaterials(), this.getCollectionDocuments()]).subscribe(
        () => {
          this.globalService.deactivateLoader();
        },
        () => {
          this.globalService.deactivateLoader();
        }
      );

      if (this.collection.state) {
        this.appliedState = this.collection.state;
      }
      if (this.definitionsStoreService.thumbnailProperties) {
        const splitString = this.definitionsStoreService.thumbnailProperties.supportedFiles.split(',');
        this.uploadImageData = {
          title: this.i18nStrings.collectionThumbnail,
          thumbnail: this.collection.attributes.thumbnail
            ? `${MARKETPLACE_BASE_API_URL}/files/download/${this.collection.attributes.thumbnail}`
            : '',
          thumbnailProperties: this.definitionsStoreService.thumbnailProperties,
          uploadImageButton: { type: 'regular', color: 'primary' },
          isThumbnailStatusInvalid: false,
          acceptPhotoExtensions: `.${splitString.join(',.')}`,
          supportedThumbnailFiles: this.definitionsStoreService.thumbnailProperties.supportedFiles.replace(/,/g, ', '),
          maxUploadSize: bytesToSize(this.definitionsStoreService.thumbnailProperties.maxSize)
        };
      }
      this.domain = this.definitionsStoreService.getDescendantDomain(ME_DOMAIN_APPLICATION_BUCKET, this.collection.domainName);

      if (this.domain) {
        this.attributes = this.inputAttributeService.transformAttributesArray(this.domain.attributes);
        this.inputAttributeService.applyUpdateFormValues(this.attributes, this.collection);
        this.groups = this.inputAttributeService.getLayoutGroups(this.domain.layouts, MeAttributeLayoutTypes.UPDATE);
        this.groups = this.inputAttributeService.generateLayoutRows(this.groups);

        this.generalInfoLayoutGroups = this.getLayoutForGroups(this.stepOneGroups);
        if (this.generalInfoLayoutGroups && this.generalInfoLayoutGroups.length) {
          this.collectionInfoAttributeForm = this.inputAttributeService.createFormGroup(
            this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups),
            this.appliedState
          );

          // get attribute and create control for thumbnail
          const isRequired: boolean = this.inputAttributeService.isAttributeRequired(
            this.attributes,
            this.thumbnailAttributeName,
            this.appliedState
          );
          const validators: ValidatorFn[] = [];
          if (isRequired) {
            validators.push(Validators.required);
          }
          this.collectionInfoAttributeForm.addControl(
            this.thumbnailAttributeName,
            new FormControl(this.collection.attributes.thumbnail, validators)
          );

          this.collectionInfoAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
            this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups),
            this.collectionInfoAttributeForm,
            this.appliedState
          );
        }

        this.subs.sink = this.collectionInfoAttributeForm.valueChanges.subscribe(() => {
          if (this.collectionInfoAttributeForm.dirty) {
            this.applyCheck = true;
          }
        });
        if (this.queryTab && Number(this.queryTab) >= 0) {
          setTimeout(() => {
            this.stepper.selectedIndex = Number(this.queryTab);
            this.addMaterialsPopUp();
          }, 0);
        }
        if (this.isDuplicateCollection) {
          this.collectionInfoAttributeForm.markAsDirty();
        }
      }
    });
  }

  prepareCardData(): void {
    // Create material cards for collection materials
    this.subs.sink = this._materialsStoreServiceCollectionMaterials.materialEntities$.subscribe((data: MaterialModel[] | null) => {
      this.materialCardItemsPopup = [];
      if (data) {
        const materialStates: StateModel[] = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_MATERIAL).states;
        this.collectionMaterialCardItems = data.map((material: MaterialModel) => {
          return {
            contextMenuButton: { type: 'regular', color: 'link', rounded: true },
            contextMenuItems: [{ key: 'remove', value: this.i18nStrings.remove }],
            materialName: material.attributes.name,
            materialPrice: material.attributes.price,
            oid: material.oid,
            selected: false,
            selectionEnabled: this.selectionCollectionMaterialEnabled,
            supplierName: material.attributesExtended.owner,
            supplierOid: material.ownerOID,
            visibility: getVisibilityObject(material.state, materialStates.find((ms) => ms.internalName === material.state)?.stateType),
            thumbnail: material.attributes.thumbnail ? `${MARKETPLACE_BASE_API_URL}/files/download/${material.attributes.thumbnail}` : ''
          };
        });
      }
    });

    // Create material cards for add materials pop up
    this.subs.sink = this.materialsStoreServicePopup.materialEntities$.subscribe((data: MaterialModel[] | null) => {
      this.materialCardItemsPopup = [];
      if (data) {
        const materialStates: StateModel[] = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_MATERIAL).states;
        this.materialCardItemsPopup = data.map((material: MaterialModel) => {
          return {
            contextMenuButton: { type: 'regular', color: 'link', rounded: true },
            contextMenuItems: [],
            materialName: material.attributes.name,
            materialPrice: material.attributes.price,
            oid: material.oid,
            selected: !!this.selectedMaterialsStorePopup.materialEntities.find((mat) => mat.oid === material.oid),
            selectionEnabled: true,
            supplierName: material.attributesExtended.owner,
            supplierOid: material.ownerOID,
            thumbnail: material.attributes.thumbnail ? `${MARKETPLACE_BASE_API_URL}/files/download/${material.attributes.thumbnail}` : '',
            visibility: getVisibilityObject(material.state, materialStates.find((ms) => ms.internalName === material.state)?.stateType)
          };
        });
      }
    });

    // Create documents cards for add document pop up
    this.subs.sink = this._documentStoreServicePopup.documentEntities$.subscribe((data: MeDocumentModel[]) => {
      if (data) {
        this.cardItemsDocumentsPopup = data.map((document: MeDocumentModel) => {
          return {
            contextMenu: {
              contextMenuItems: []
            },
            creationDate: formatDate(document.createdOn, 'dd/MM/yyyy', 'en-US'),
            documentName: document.attributes.title,
            oid: document.oid,
            selected: !!this.selectedDocumentsStorePopup.documentEntities.find((doc) => doc.oid === document.oid),
            selectionEnabled: true,
            documentThumbnail: document.attributes.thumbnail
              ? `${MARKETPLACE_BASE_API_URL}/files/download/${document.attributes.thumbnail}`
              : undefined,
            documentExtensionIcon: mapExtensionToIcon(document.attributesExtended.extension)
          };
        });
      }
    });

    // Create documents cards for collection documents
    this.subs.sink = this._documentStoreServiceCollectionDocument.documentEntities$.subscribe((data: MeDocumentModel[]) => {
      if (data) {
        this.cardItemsCollectionDocuments = data.map((document: MeDocumentModel) => {
          return {
            contextMenu: {
              contextMenuItems: [
                { key: 'remove', value: this.i18nStrings.remove },
                { key: 'view', value: this.i18nStrings.view }
              ]
            },
            creationDate: formatDate(document.createdOn, 'dd/MM/yyyy', 'en-US'),
            documentName: document.attributes.title,
            oid: document.oid,
            selected: false,
            selectionEnabled: false,
            documentThumbnail: document.attributes.thumbnail
              ? `${MARKETPLACE_BASE_API_URL}/files/download/${document.attributes.thumbnail}`
              : undefined,
            documentExtensionIcon: mapExtensionToIcon(document.attributesExtended.extension)
          };
        });
      }
    });
  }

  checkStepBeforeSwitch(switchedTo: number): void {
    if (switchedTo !== 0) {
      return;
    }
    const currentStep: MatStep = this.stepper.steps.toArray()[this.stepper.selectedIndex];
    if (!currentStep.completed) {
      if (this.stepper.selectedIndex !== switchedTo) {
        currentStep.stepControl.markAllAsTouched();
      }
      if (this.stepper.selectedIndex < switchedTo) {
        const emptyRequiredAttributes: AttributeModelTypes[] = [];
        const currentStepControl: FormGroup = currentStep.stepControl as FormGroup;
        // Collect all empty required attributes
        Object.keys(currentStepControl.controls).forEach((controlName) => {
          const validationErrors: ValidationErrors | null = currentStepControl.controls[controlName].errors;
          if (validationErrors && validationErrors.required) {
            const attribute = [...this.attributes, this.collectionTypesAttribute].find((attr) => attr.internalName === controlName);
            if (attribute) {
              emptyRequiredAttributes.push(attribute);
            }
          }
        });

        if (emptyRequiredAttributes.length > 0) {
          const basicAlertData: MeBasicAlertI = {
            mode: 'error',
            title: this.i18nStrings.insertRequiredData,
            content: this.i18nStrings.requiredFields
          };
          emptyRequiredAttributes.forEach((attribute, index) => {
            basicAlertData.content += ` ${attribute.displayName}`;
            basicAlertData.content += emptyRequiredAttributes.length - 1 !== index ? ',' : '';
          });
          this.meBasicAlertService.openBasicAlert(basicAlertData);
        }
      }
    }
  }

  // On tab index change set previous step form as touched
  markFormAsTouched(changeObj: StepperSelectionEvent): void {
    changeObj.previouslySelectedStep.stepControl.markAllAsTouched();
  }

  prepareButtons(): void {
    this.buttonNext = { label: this.i18nStrings.next, data: { type: 'regular', color: 'primary' } };
    this.buttonBack = { label: this.i18nStrings.back, data: { type: 'regular', color: 'secondary' } };
    this.buttonCancel = { label: this.i18nStrings.cancel, data: { type: 'regular', color: 'secondary' } };
    this.buttonSubmit = {
      label: this.isDuplicateCollection ? this.i18nStrings.duplicate : this.collectionId ? this.i18nStrings.next : this.i18nStrings.create,
      data: { type: 'regular', color: 'primary' }
    };
    this.buttonFinish = { label: this.i18nStrings.finish, data: { type: 'regular', color: 'primary' } };
  }

  getLayoutForGroups(layoutGroupNames: string[]): LayoutGroupModel[] {
    const layoutGroups = this.groups.filter((group) => layoutGroupNames.findIndex((name) => group.internalName === name) > -1);
    return layoutGroups;
  }

  getLayoutForGroupsExcluding(layoutGroupNames: string[]): LayoutGroupModel[] {
    const layoutGroups = this.groups.filter((group) => layoutGroupNames.findIndex((name) => group.internalName === name) === -1);
    return layoutGroups;
  }

  handleCollection(form: FormGroup): void {
    if (this.collectionId) {
      this.updateCollection(form);
    } else {
      this.createCollection(form);
    }
  }

  handleCreateEditCollection(form: FormGroup): void {
    if (this.isDuplicateCollection) {
      this.duplicateCollection(form);
    } else if (!this.isDuplicateCollection && this.collectionId) {
      this.stepper.next();
      this.checkStepBeforeSwitch(1);
    } else if (!this.isDuplicateCollection && !this.collectionId) {
      this.handleCollection(form);
      this.stepper.next();
      this.checkStepBeforeSwitch(1);
    }
  }

  handleCollectionFinish(form: FormGroup): void {
    if (this.collectionId) {
      this.handleCollection(form);
    } else {
      this.openCollection();
    }
  }

  createCollection(form: FormGroup): void {
    // If collection is already created then do update
    if (this.collection) {
      this.updateCollection(form);
      return;
    }

    form.markAllAsTouched();
    if (form.valid) {
      this.globalService.activateLoader();
      const collectionEntity: CollectionEntityI = {
        // Set collection domain name
        domainName: ME_DOMAIN_APPLICATION_COLLECTION,
        attributes: {}
      };

      // Add step1 attributes and values to collection entity
      if (this.generalInfoLayoutGroups && this.generalInfoLayoutGroups.length) {
        this.inputAttributeService
          .getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups)
          .forEach((attribute) => this.setAttributeWithValueToEntity(collectionEntity, attribute, this.collectionInfoAttributeForm));
      }

      // Add thumbnail
      collectionEntity.attributes[this.thumbnailAttributeName] = this.collectionInfoAttributeForm.controls[
        this.thumbnailAttributeName
      ].value;
      this.subs.sink = this.inputAttributeService
        .checkUniqueAttributes('', this.attributes, ME_DOMAIN_APPLICATION_COLLECTION, form)
        .pipe(
          take(1),
          finalize(() => {
            this.globalService.deactivateLoader();
          })
        )
        .subscribe(
          () => {
            this.subs.sink = this.collectionWebService
              .createCollection(collectionEntity)
              .pipe(
                finalize(() => {
                  this.globalService.deactivateLoader();
                })
              )
              .subscribe((response) => {
                // start tracking for response from sweet alert
                this.subs.sink = this.meSweetAlertService.getDataBackFromSweetAlert().subscribe((data) => {
                  this.collectionInfoAttributeForm.markAsUntouched();
                  this.collectionInfoAttributeForm.markAsPristine();
                  this.buttonSubmit.label = this.i18nStrings.next;
                  this.collection = response;
                  this.materialsStepEnabled = true;
                  this.documentsStepEnabled = true;
                  this.getCollectionDocuments();
                  this.applyCheck = false;
                  if (data && data.confirmed) {
                    // this.router.navigate([`collections/edit/${response.oid}`]);
                  } else {
                    this.router.navigate(['collections']);
                  }
                });

                const sweetAlertModel: MeSweetAlertI = {
                  mode: 'success',
                  icon: 'check',
                  type: {
                    name: MeSweetAlertTypeEnum.submit,
                    buttons: {
                      submit: this.i18nStrings.proceed,
                      cancel: this.i18nStrings.finish
                    }
                  },
                  title: this.i18nStrings.collectionCreatedAlertTitle,
                  message: this.i18nStrings.collectionCreatedAlertMessage
                };
                this.meSweetAlertService.openMeSweetAlert(sweetAlertModel);
              });
          },
          () => {
            this.globalService.deactivateLoader();
            this.showAttributesNotUniqueError();
          }
        );
    }
  }

  updateCollection(form: FormGroup): void {
    // If form isn't changed go to next step
    if (!form.dirty) {
      this.openCollection();
      return;
    }

    form.markAllAsTouched();
    if (form.valid) {
      const collectionChanges: CollectionEntityI = {
        domainName: this.collection.domainName,
        attributes: {}
      };

      // Add step1 attributes and values to collection entity
      if (this.generalInfoLayoutGroups && this.generalInfoLayoutGroups.length) {
        this.generalInfoLayoutGroups[0].attributes.forEach((attribute) =>
          this.setAttributeWithValueToEntity(collectionChanges, attribute, this.collectionInfoAttributeForm)
        );
      }

      // Add thumbnail
      collectionChanges.attributes[this.thumbnailAttributeName] = this.collectionInfoAttributeForm.controls[
        this.thumbnailAttributeName
      ].value;
      this.globalService.activateLoader();
      this.subs.sink = this.inputAttributeService
        .checkUniqueAttributes(this.collection.oid, this.attributes, ME_DOMAIN_APPLICATION_COLLECTION, form)
        // .pipe(
        //   take(1),
        //   finalize(() => {
        //     // this.globalService.deactivateLoader();
        //   })
        // )
        .subscribe(
          () => {
            this.collectionWebService.updateCollection(encodeURI(this.collection.oid), collectionChanges).subscribe(() => {
              form.markAsUntouched();
              form.markAsPristine();
              this.applyCheck = false;

              const basicAlertData: MeBasicAlertI = {
                mode: 'success',
                title: this.i18nStrings.collectionUpdated,
                content: this.i18nStrings.collectionIsSuccessfullyUpdated
              };
              this.meBasicAlertService.openBasicAlert(basicAlertData);
              this.globalService.deactivateLoader();
              this.openCollection();
            });
          },
          () => {
            this.globalService.deactivateLoader();
            this.stepper.selectedIndex = 0;
            this.showAttributesNotUniqueError();
          }
        );
    }
  }

  duplicateCollection(form: FormGroup): void {
    form.markAllAsTouched();
    if (form.valid) {
      const collectionChanges: CollectionEntityI = {
        domainName: this.collection.domainName,
        attributes: {}
      };

      // Add step1 attributes and values to collection entity
      if (this.generalInfoLayoutGroups && this.generalInfoLayoutGroups.length) {
        this.generalInfoLayoutGroups[0].attributes.forEach((attribute) =>
          this.setAttributeWithValueToEntity(collectionChanges, attribute, this.collectionInfoAttributeForm)
        );
      }

      // Add thumbnail
      collectionChanges.attributes[this.thumbnailAttributeName] = this.collectionInfoAttributeForm.controls[
        this.thumbnailAttributeName
      ].value;
      this.globalService.activateLoader();
      this.subs.sink = this.inputAttributeService
        .checkUniqueAttributes('', this.attributes, ME_DOMAIN_APPLICATION_COLLECTION, form)
        .pipe(
          take(1),
          finalize(() => {
            this.globalService.deactivateLoader();
          })
        )
        .subscribe(
          () => {
            this.collectionWebService.duplicateCollection(encodeURI(this.collection.oid), collectionChanges).subscribe((collection) => {
              // start tracking for response
              const successSubscription = this.meSweetAlertService.getDataBackFromSweetAlert().subscribe(() => {
                form.markAsUntouched();
                form.markAsPristine();
                this.applyCheck = false;
                this.router.navigate([`collections/view/${collection.oid}`]);
                successSubscription.unsubscribe();
              });

              const successAlertModel: MeSweetAlertI = {
                mode: 'success',
                icon: 'check',
                type: {
                  name: MeSweetAlertTypeEnum.confirm,
                  buttons: {
                    confirm: this.i18nStrings.continue
                  }
                },
                title: this.i18nStrings.success,
                message: this.i18nStrings.collectionDuplicated
              };
              this.meSweetAlertService.openMeSweetAlert(successAlertModel);
            });
          },
          () => {
            this.globalService.deactivateLoader();
            this.showAttributesNotUniqueError();
          }
        );
    }
  }

  private setAttributeWithValueToEntity(
    entity: CollectionEntityI,
    attribute: AttributeModelTypes | LayoutGroupAttributeModel,
    formGroup: FormGroup
  ): void {
    if (
      formGroup.controls.hasOwnProperty(attribute.internalName) &&
      formGroup.controls[attribute.internalName].hasOwnProperty('value') &&
      formGroup.controls[attribute.internalName].value !== null
    ) {
      entity.attributes[attribute.internalName] = formGroup.controls[attribute.internalName].value;
    }
  }

  updateThumbnail(thumbnailOR: string | null): void {
    if (this.collectionInfoAttributeForm) {
      this.collectionInfoAttributeForm.markAsDirty();
      this.collectionInfoAttributeForm.controls[this.thumbnailAttributeName].setValue(thumbnailOR);
    }
  }

  handleImageFormData(recievedFile: File): void {
    const formData = new FormData();
    formData.append(
      'meta',
      JSON.stringify({
        type: this.definitionsStoreService.thumbnailProperties?.type
      })
    );
    formData.append('file', recievedFile, recievedFile.name);

    this.subs.sink = this.fileWebService.uploadFile(formData, FileUploadResponseModel).subscribe(
      (response) => {
        this.uploadImageData = {
          thumbnail: `${MARKETPLACE_BASE_API_URL}/files/download/${response.oid}`,
          showProgress: false
        };
        if (response.oid) {
          this.updateThumbnail(response.oid);
        } else {
          this.updateThumbnail(null);
        }
      },
      (_error: ErrorResponseI) => {
        this.uploadImageData = { ...this.uploadImageData, isThumbnailStatusInvalid: true };
      }
    );
  }

  addButtonToBreadcrumb(): void {
    setTimeout(() => {
      if ([1, 2].includes(this.stepper.selectedIndex)) {
        this.breadcrumbSupportService.breadcrumbActions = [
          ...this.breadcrumbActions,
          {
            button: {
              id: 'createButton',
              data: { color: 'primary', type: 'transparent' },
              text: this.i18nStrings.add,
              content: this.createButton
            },
            callBack: () => {
              if (this.stepper.selectedIndex === 1) {
                this.addMaterialsPopUp();
              } else {
                this.handleAddButtonClick();
              }
            }
          }
        ];
      } else {
        this.breadcrumbSupportService.breadcrumbActions = [...this.breadcrumbActions];
      }
    });
  }

  addMaterialsToCollection(OIDs: string[]): void {
    this.collectionWebService.addMaterialsToCollection(this.collection.oid, OIDs).subscribe(() => {
      const basicAlertData: MeBasicAlertI = {
        mode: 'success',
        title: this.i18nStrings.materialsAdded,
        content: this.i18nStrings.materialsSuccessfullyAdded
      };
      this.meBasicAlertService.openBasicAlert(basicAlertData);
      this.globalService.activateLoader();
      this.autoDeactivateLoader = true;
      this.getCollectionMaterials();
    });
  }

  getCollectionMaterials(
    from: number = 0,
    top: number = this.NUMBER_OF_ITEMS_ON_PAGE,
    append: boolean = false
  ): Observable<ArrayResponseI<MaterialModel>> {
    this.areMaterialsLoading = true;
    const tempObservable = this.collectionWebService.getCollectionMaterials(this.collection.oid, from, top);
    this.subs.sink = tempObservable
      .pipe(
        finalize(() => {
          this.areMaterialsLoading = false;
          if (this.autoDeactivateLoader) {
            this.globalService.deactivateLoader();
            this.autoDeactivateLoader = false;
          }
        })
      )
      .subscribe((response) => {
        if (response && response.entities) {
          this.handleCollectionMaterialsResponse(response, append);
        }
      });
    return tempObservable;
  }

  addMaterialsPopUp(): void {
    this.materialDataPopup = { selectedCount: 0, title: this.i18nStrings.addMaterials, showActions: true, showOnlySelected: false };
    this.getAllMaterials();
    this.setSearchBarDataMaterialsPopup();
    this.showSelectedMaterialsPopup = false;

    this.popupSelectService.openPopupSelect(this.materialDataPopup, this.materialGridRefPopup).subscribe((event) => {
      switch (event) {
        case 'close':
          this.selectedMaterialsStorePopup.materialEntities = [];
          break;
        case 'clearSelection':
          this.selectedMaterialsStorePopup.materialEntities = [];
          this.handleSearchInputEventMaterialsPopup(this.searchBarDataMaterialsPopup.search?.keyword || '');
          this.setIsShowSelectedActiveMaterialsPopup(false);
          break;
        case 'showSelected':
          this.materialsStoreServicePopup.materialEntities = this.selectedMaterialsStorePopup.materialEntities;
          this.setIsShowSelectedActiveMaterialsPopup(true);
          this.trackForBottomReachMaterialsPopup = false;
          break;
        case 'showAll':
          this.handleSearchInputEventMaterialsPopup(this.searchBarDataMaterialsPopup.search?.keyword || '');
          this.setIsShowSelectedActiveMaterialsPopup(false);
          break;
        case 'addItems':
          const materialOids: string[] = this.selectedMaterialsStorePopup.materialEntities.map((material) => {
            return material.oid;
          });
          if (materialOids.length) {
            this.addMaterialsToCollection(materialOids);
            this.buttonFinish = { ...this.buttonFinish, data: { ...this.buttonFinish.data, disabled: false } };
          }
          this.selectedMaterialsStorePopup.materialEntities = [];
          break;
        default:
          break;
      }
    });
  }

  private setIsShowSelectedActiveMaterialsPopup(status: boolean): void {
    this.showSelectedMaterialsPopup = status;
    this.updateSelectedActiveMaterialsPopup({
      showOnlySelected: status
    });
  }

  updateSelectedActiveMaterialsPopup(materialPopup: Partial<MePopupSelectI>): void {
    const selectCount = this.materialCardItemsPopup.filter((material) => {
      return material.selected;
    }).length;
    this.materialDataPopup = { ...this.materialDataPopup, ...materialPopup, selectedCount: selectCount };
    this.popupSelectService.pushDataModelToPopup(this.materialDataPopup);
  }

  getMaterialOid(_index: number, materialCard: MeMaterialCardI): string {
    return materialCard.oid;
  }

  handleViewTypeEventCollectionMaterials(): void {
    this.searchBarDataCollectionMaterial.view === 'grid'
      ? (this.searchBarDataCollectionMaterial.view = 'list')
      : (this.searchBarDataCollectionMaterial.view = 'grid');
  }

  handleSearchBarEventsCollectionMaterials(event: { eventName: string; payload?: string | boolean }): void {
    if (event.eventName === 'remove') {
      const OIDs: string[] = this.collectionMaterialCardItems
        .filter((card) => card.selected)
        .map((card) => {
          return card.oid;
        });
      if (OIDs && OIDs.length > 0) {
        this._removeMaterialSweetAlert = this.meSweetAlertService
          .getDataBackFromSweetAlert()
          .pipe(take(1))
          .subscribe((data) => {
            if (data && data.confirmed) {
              this.subs.sink = this.collectionsWebService.removeMaterialsFromCollection(this.collection.oid, OIDs).subscribe(() => {
                this.globalService.activateLoader();
                this.autoDeactivateLoader = true;
                this.getCollectionMaterials();
                this.handleToggleSelectEventCollectionMaterials();
                const basicAlertData: MeBasicAlertI = {
                  mode: 'success',
                  title: this.i18nStrings.materialRemoved,
                  content: this.i18nStrings.materialHaveBeenSuccessfullyRemoved
                };
                this.meBasicAlertService.openBasicAlert(basicAlertData);
              });
            }
            this._removeMaterialSweetAlert.unsubscribe();
          });

        const sweetAlertModel: MeSweetAlertI = {
          mode: 'danger',
          icon: 'alert-triangle',
          type: {
            name: MeSweetAlertTypeEnum.submit,
            buttons: {
              submit: this.i18nStrings.remove,
              cancel: this.i18nStrings.cancel
            }
          },
          title: this.i18nStrings.removeMaterial,
          message: this.i18nStrings.areYouSureYouWantToRemoveTheMaterial
        };
        this.meSweetAlertService.openMeSweetAlert(sweetAlertModel);
      }
    }
  }

  handleSearchBarEventsCollectionDocuments(event: { eventName: string; payload?: string | boolean }): void {
    switch (event.eventName) {
      case 'searchEnabledToggle':
        // TODO
        break;
      case 'edit':
        // TODO
        break;
      case 'remove':
        const documentOIDs: string[] = this.cardItemsCollectionDocuments
          .filter((card) => card.selected)
          .map((card) => {
            return card.oid;
          });
        if (documentOIDs && documentOIDs.length > 0) {
          this.handleDocumentsRemoval(this.collection.oid, documentOIDs);
        }
        break;
      case 'copy':
        // TODO
        break;
      case 'more':
        // TODO
        break;
      case 'download':
        // TODO
        break;
    }
  }

  handleContextMenuEventsCollectionDocuments(event: { eventName: string; payload?: string | boolean }, documentOid: string): void {
    if (event.eventName === 'contextMenuItemClick') {
      if (event.payload === 'remove') {
        this.handleDocumentsRemoval(this.collection.oid, [documentOid]);
      }
      if (event.payload === 'view') {
        const fileToView = this._documentStoreServiceCollectionDocument.documentEntities.find((d) => d.oid === documentOid);
        if (fileToView && fileToView.attributes.primaryContent) {
          const FILE_URL: string = `${MARKETPLACE_BASE_API_URL}/files/download/${fileToView.attributes.primaryContent}`;
          if (documentOid.includes(ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL_VIDEO)) {
            this._videoPopupService.openVideoPopupAlert({ videoPath: FILE_URL });
          } else {
            this._windowRef.openInNewWindow(FILE_URL);
          }
        } else {
          this.meBasicAlertService.openBasicAlert({
            mode: 'error',
            content: this.i18nStrings.noFile,
            title: this.i18nStrings.emptyDocument
          });
        }
      }
    }
  }

  removeMaterial(event: { eventName: string; payload?: string }, materialOid: string): void {
    if (event.eventName === 'contextMenuItemClick' && event.payload === 'remove') {
      this.subs.sink = this.meSweetAlertService
        .getDataBackFromSweetAlert()
        .pipe(take(1))
        .subscribe((data) => {
          if (data && data.confirmed) {
            this.subs.sink = this.collectionsWebService.removeMaterialsFromCollection(this.collection.oid, [materialOid]).subscribe(() => {
              this.getCollectionMaterials();
              const basicAlertData: MeBasicAlertI = {
                mode: 'success',
                title: this.i18nStrings.materialRemoved,
                content: this.i18nStrings.materialHaveBeenSuccessfullyRemoved
              };
              this.meBasicAlertService.openBasicAlert(basicAlertData);
            });
          }
        });
      const sweetAlertModel: MeSweetAlertI = {
        mode: 'danger',
        icon: 'alert-triangle',
        type: {
          name: MeSweetAlertTypeEnum.submit,
          buttons: {
            submit: this.i18nStrings.remove,
            cancel: this.i18nStrings.cancel
          }
        },
        title: this.i18nStrings.removeMaterial,
        message: this.i18nStrings.areYouSureYouWantToRemoveTheMaterial
      };
      this.meSweetAlertService.openMeSweetAlert(sweetAlertModel);
    }
  }

  getCollectionDocuments(
    from: number = 0,
    to: number = this.NUMBER_OF_ITEMS_ON_PAGE,
    append: boolean = false
  ): Observable<ArrayResponseI<MeDocumentModel>> {
    this.areDocumentsLoading = true;
    const tempObs = this.collectionWebService.getCollectionDocuments(this.collection.oid, from, to);
    this.subs.sink = tempObs
      .pipe(
        finalize(() => {
          this.areDocumentsLoading = false;
          if (this.autoDeactivateLoader) {
            this.globalService.deactivateLoader();
            this.autoDeactivateLoader = false;
          }
        })
      )
      .subscribe((response: ArrayResponseI<MeDocumentModel>) => {
        this._documentStoreServiceCollectionDocument.documentNextId = response.nextID;
        if (append) {
          this._documentStoreServiceCollectionDocument.appendDocumentEntities(response.entities);
        } else {
          this._documentStoreServiceCollectionDocument.documentEntities = response.entities;
        }
      });
    return tempObs;
  }

  handleAddButtonClick(): void {
    this.documentPopupData = { selectedCount: 0, title: this.i18nStrings.addDocuments, showActions: true, showOnlySelected: false };
    this.prepareFilterData();
    this.handleSearchInputEventDocumentsPopup(this.searchBarDataDocumentsPopup.search?.keyword || '');
    this.showSelectedDocumentsPopup = false;
    // Put in comment this line of code to stop producing popup
    this.popupSelectService.openPopupSelect(this.documentPopupData, this.documentGridRefPopup).subscribe((event: string) => {
      switch (event) {
        case 'close':
          this.resetDocumentPopupAfterClose();
          break;
        case 'clearSelection':
          this.selectedDocumentsStorePopup.documentEntities = [];
          this.handleSearchInputEventDocumentsPopup(this.searchBarDataDocumentsPopup.search?.keyword || '');
          this.setIsShowSelectedActiveDocumentsPopup(false);
          break;
        case 'showSelected':
          this._documentStoreServicePopup.documentEntities = this.selectedDocumentsStorePopup.documentEntities;
          this.setIsShowSelectedActiveDocumentsPopup(true);
          this.trackForBottomReachDocumentsPopup = false;
          break;
        case 'showAll':
          this.handleSearchInputEventDocumentsPopup(this.searchBarDataDocumentsPopup.search?.keyword || '');
          this.setIsShowSelectedActiveDocumentsPopup(false);
          break;
        case 'addItems':
          const documentOids: string[] = this.selectedDocumentsStorePopup.documentEntities.map((document) => {
            return document.oid;
          });
          if (documentOids.length) {
            this.addDocumentsToCollection(this.collection.oid, documentOids);
            this.buttonFinish = { ...this.buttonFinish, data: { ...this.buttonFinish.data, disabled: false } };
          }
          this.resetDocumentPopupAfterClose();
          break;
        default:
          break;
      }
    });
    this._listenForDocumentFilterChangeSubscription = this.documentFilterChanged.pipe(debounceTime(this.filterDelay)).subscribe(() => {
      this.handleSearchInputEventDocumentsPopup(this.searchBarDataDocumentsPopup.search?.keyword || '');
    });
    this.subs.sink = this._listenForDocumentFilterChangeSubscription;
  }

  private resetDocumentPopupAfterClose(): void {
    this.selectedDocumentsStorePopup.documentEntities = [];
    if (this._listenForDocumentFilterChangeSubscription) {
      this._listenForDocumentFilterChangeSubscription.unsubscribe();
    }
    if (this._listenForDocumentTypeChangeSubscription) {
      this._listenForDocumentTypeChangeSubscription.unsubscribe();
    }
  }

  private setIsShowSelectedActiveDocumentsPopup(status: boolean): void {
    this.showSelectedDocumentsPopup = status;
    this.updateSelectedActiveDocumentsPopup({
      showOnlySelected: status
    });
  }

  updateSelectedActiveDocumentsPopup(materialPopup: Partial<MePopupSelectI>): void {
    const selectCount = this.cardItemsDocumentsPopup.filter((document) => {
      return document.selected;
    }).length;
    this.documentPopupData = { ...this.documentPopupData, ...materialPopup, selectedCount: selectCount };
    this.popupSelectService.pushDataModelToPopup(this.documentPopupData);
  }

  handleDocumentsRemoval(collectionOid: string, documentOids: string[]): void {
    this.subs.sink = this.meSweetAlertService
      .getDataBackFromSweetAlert()
      .pipe(take(1))
      .subscribe((data) => {
        if (data && data.confirmed) {
          this.subs.sink = this.collectionWebService.removeDocuments(collectionOid, documentOids).subscribe(() => {
            const basicAlertData: MeBasicAlertI = {
              mode: 'success',
              title: this.i18nStrings.documentRemoved,
              content: this.i18nStrings.removeDocumentsSuccess
            };
            this.meBasicAlertService.openBasicAlert(basicAlertData);
            this.globalService.activateLoader();
            this.autoDeactivateLoader = true;
            this.getCollectionDocuments();
            if (this.searchBarDataCollectionDocuments.selectButton && this.searchBarDataCollectionDocuments.selectButton.enabled) {
              this.handleToggleSelectEventCollectionDocuments();
            }
          });
        }
      });
    const sweetAlertModel: MeSweetAlertI = {
      mode: 'danger',
      icon: 'alert-triangle',
      type: {
        name: MeSweetAlertTypeEnum.submit,
        buttons: {
          submit: this.i18nStrings.remove,
          cancel: this.i18nStrings.cancel
        }
      },
      title: this.i18nStrings.removeDocuments,
      message: this.i18nStrings.areYouSureYouWantToRemoveDocument
    };
    this.meSweetAlertService.openMeSweetAlert(sweetAlertModel);
  }

  addDocumentsToCollection(collectionOid: string, documentOids: string[]): void {
    this.subs.sink = this.collectionWebService.submitDocuments(collectionOid, documentOids).subscribe(() => {
      const basicAlertData: MeBasicAlertI = {
        mode: 'success',
        title: this.i18nStrings.documentsAdded,
        content: this.i18nStrings.addDocumentsSuccess
      };
      this.meBasicAlertService.openBasicAlert(basicAlertData);
      this.globalService.activateLoader();
      this.autoDeactivateLoader = true;
      this.getCollectionDocuments();
    });
  }

  openCollection(): void {
    this.router.navigate([`collections/view/${this.collection.oid}`]);
  }

  componentHeaderEventHandler(event: { eventName: string; payload?: string | boolean }): void {
    console.log(event);
  }

  getDocumentOid(_index: number, document: MeDocumentCardI): string {
    return document.oid;
  }

  getRowIndex(_index: number, row: LayoutRowModel): number {
    return row.rowIndex;
  }

  getInternalName(_index: number, attribute: LayoutGroupAttributeModel): string {
    return attribute.internalName;
  }

  isDocSelected(document: MeDocumentCardI): boolean {
    return document.selected;
  }

  isMatSelected(material: MeMaterialCardI): boolean {
    return material.selected;
  }

  showAttributesNotUniqueError(): void {
    const sweetAlertModel: MeSweetAlertI = {
      mode: 'danger',
      icon: 'x-octagon',
      type: {
        name: MeSweetAlertTypeEnum.confirm,
        buttons: {
          confirm: this.i18nStrings.close
        }
      },
      title: this.i18nStrings.error,
      message: this.i18nStrings.attributesNotUnique
    };

    this.meSweetAlertService.openMeSweetAlert(sweetAlertModel);
  }

  bottomReachedHandler(gridItemsInUse: 'materialsPopup' | 'collectionMaterials' | 'documentsPopup' | 'collectionDocuments'): void {
    if (gridItemsInUse === 'materialsPopup' && this.trackForBottomReachMaterialsPopup) {
      // If we are using search keyword or sort then call they method otherwise getAllMaterials
      this.handleSearchInputEventMaterialsPopup(
        this.searchBarDataMaterialsPopup.search?.keyword || '',
        this.materialsStoreServicePopup.materialNextId,
        this.NUMBER_OF_ITEMS_ON_PAGE,
        true
      );
    } else if (gridItemsInUse === 'collectionMaterials' && this.trackForBottomReachCollectionMaterials) {
      // If we are using search keyword or sort then call they method otherwise getAllCollectionMaterials
      if (
        (this.searchBarDataCollectionMaterial.search && this.searchBarDataCollectionMaterial.search.keyword.length > 0) ||
        (this.searchBarDataCollectionMaterial.sort && this.searchBarDataCollectionMaterial.sort.selectedIndex > 0)
      ) {
        this.handleSearchInputEventCollectionMaterials(
          this.searchBarDataMaterialsPopup.search?.keyword || '',
          this._materialsStoreServiceCollectionMaterials.materialNextId,
          this.NUMBER_OF_ITEMS_ON_PAGE,
          true
        );
      } else {
        this.getCollectionMaterials(this._materialsStoreServiceCollectionMaterials.materialNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
      }
    } else if (gridItemsInUse === 'documentsPopup' && this.trackForBottomReachDocumentsPopup) {
      // If we are using search keyword or sort then call they method otherwise getAllDocuments
      this.handleSearchInputEventDocumentsPopup(
        this.searchBarDataDocumentsPopup.search?.keyword || '',
        this._documentStoreServicePopup.documentNextId,
        this.NUMBER_OF_ITEMS_ON_PAGE,
        true
      );
    } else if (gridItemsInUse === 'collectionDocuments' && this.trackForBottomReachCollectionDocuments) {
      // If we are using search keyword or sort then call they method otherwise getAllCollectionMaterials
      if (
        (this.searchBarDataCollectionDocuments.search && this.searchBarDataCollectionDocuments.search.keyword.length > 0) ||
        (this.searchBarDataCollectionDocuments.sort && this.searchBarDataCollectionDocuments.sort.selectedIndex > 0)
      ) {
        this.handleSearchInputEventCollectionDocuments(
          this.searchBarDataCollectionDocuments.search?.keyword || '',
          this._documentStoreServiceCollectionDocument.documentNextId,
          this.NUMBER_OF_ITEMS_ON_PAGE,
          true
        );
      }
    }
  }

  // NEXT METHODS ARE VERY SIMILIAR TO DOC AND MAT SO IN FUTURE WE NEED TO FIND BETTER SOLUTION AND OPTIMIZE CODE

  // SET SEARCH BAR DATA FOR ALL 4 CASES
  setSearchBarDataCollectionMaterials(): void {
    this.searchBarDataCollectionMaterial = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: ''
      },
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'azAlphabetically', value: this.i18nStrings.azAlphabetically },
          { key: 'zaAlphabetically', value: this.i18nStrings.zaAlphabetically },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      },
      selectButton: {
        enabled: false,
        placeholder: this.i18nStrings.select
      },
      operationButtons: [
        {
          button: { type: 'regular', rounded: false, color: 'link' },
          iFeatherName: 'trash',
          clickEventName: 'remove',
          tooltipText: this.i18nStrings.remove
        }
      ]
    };
  }

  setSearchBarDataCollectionDocuments(): void {
    this.searchBarDataCollectionDocuments = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: ''
      },
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'azAlphabetically', value: this.i18nStrings.azAlphabetically },
          { key: 'zaAlphabetically', value: this.i18nStrings.zaAlphabetically },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      },
      selectButton: {
        enabled: false,
        placeholder: this.i18nStrings.select
      },
      operationButtons: [
        {
          button: { type: 'regular', rounded: false, color: 'link' },
          iFeatherName: 'trash',
          clickEventName: 'remove',
          tooltipText: this.i18nStrings.remove
        }
      ]
    };
  }

  setSearchBarDataDocumentsPopup(): void {
    this.searchBarDataDocumentsPopup = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: ''
      },
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'azAlphabetically', value: this.i18nStrings.azAlphabetically },
          { key: 'zaAlphabetically', value: this.i18nStrings.zaAlphabetically },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      },
      selectButton: {
        enabled: false,
        placeholder: ''
      },
      operationButtons: []
    };
  }

  setSearchBarDataMaterialsPopup(): void {
    this.searchBarDataMaterialsPopup = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: ''
      },
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'azAlphabetically', value: this.i18nStrings.azAlphabetically },
          { key: 'zaAlphabetically', value: this.i18nStrings.zaAlphabetically },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      }
    };
  }

  // GENERATE EMPTY CONTENT FOR BOTH CASES
  generateEmptyContentMaterial(): void {
    this.emptyContentCollectionMaterials = {
      title: this.i18nStrings.addYourFirstMaterial,
      comment: this.i18nStrings.startCreatingDigitalVersionsOfYourMaterials,
      buttonText: this.i18nStrings.add,
      emptyIconName: 'empty-materials'
    };
  }

  generateEmptyContentDocument(): void {
    this.emptyContentCollectionDocuments = {
      title: this.i18nStrings.addYourFirstDocument,
      comment: this.i18nStrings.documentsCanBeAttachedToOtherSystemObjectsAndShared,
      buttonText: this.i18nStrings.add,
      emptyIconName: 'empty-documents'
    };
  }

  // GET ALL MATERIALS OR DOCUMENTS
  getAllMaterials(from: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.areMaterialsLoadingPopup = true;
    this.subs.sink = this._materialsWebService
      .getMeMaterials(from, top)
      .pipe(
        finalize(() => {
          this.areMaterialsLoadingPopup = false;
        })
      )
      .subscribe((response: ArrayResponseI<MaterialModel>) => {
        this.handleMaterialsResponsePopup(response, append, true);
      });
  }

  // HANDLE RESPONSE FOR MAT OR DOC
  private handleMaterialsResponsePopup(response: ArrayResponseI<MaterialModel>, append: boolean = false, isSearch: boolean = false): void {
    this.materialsStoreServicePopup.materialNextId = response.nextID;
    if (append) {
      this.materialsStoreServicePopup.appendMaterialEntities(response.entities);
    } else {
      this.materialsStoreServicePopup.materialEntities = response.entities;
      this.isSearchCollectionMaterial = isSearch;
      this.trackForBottomReachMaterialsPopup = true;
    }
    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.trackForBottomReachMaterialsPopup = false;
    }
  }

  private handleCollectionMaterialsResponse(response: ArrayResponseI<MaterialModel>, append: boolean = false): void {
    this._materialsStoreServiceCollectionMaterials.materialNextId = response.nextID;
    if (append) {
      this._materialsStoreServiceCollectionMaterials.appendMaterialEntities(response.entities);
    } else {
      this._materialsStoreServiceCollectionMaterials.materialEntities = response.entities;
      this.trackForBottomReachCollectionMaterials = true;
    }
    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.trackForBottomReachCollectionMaterials = false;
    }
  }

  private handleCollectionDocumentsResponse(response: ArrayResponseI<MeDocumentModel>, append: boolean = false): void {
    this._documentStoreServiceCollectionDocument.documentNextId = response.nextID;
    if (append) {
      this._documentStoreServiceCollectionDocument.appendDocumentEntities(response.entities);
    } else {
      this._documentStoreServiceCollectionDocument.documentEntities = response.entities;
      this.trackForBottomReachDocumentsPopup = true;
    }
    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.trackForBottomReachDocumentsPopup = false;
    }
  }

  private handleDocumentsResponsePopup(response: ArrayResponseI<MeDocumentModel>, append: boolean = false): void {
    this._documentStoreServicePopup.documentNextId = response.nextID;
    if (append) {
      this._documentStoreServicePopup.appendDocumentEntities(response.entities);
    } else {
      this._documentStoreServicePopup.documentEntities = response.entities;
      this.trackForBottomReachDocumentsPopup = true;
    }
    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.trackForBottomReachDocumentsPopup = false;
    }
  }

  // HANDLE SEARCH INPUTS FOR ALL 4 TYPES
  handleSearchInputEventMaterialsPopup(
    searchValue: string,
    from: number = 0,
    top: number = this.NUMBER_OF_ITEMS_ON_PAGE,
    append: boolean = false
  ): void {
    this.searchBarDataMaterialsPopup = {
      ...this.searchBarDataMaterialsPopup,
      search: { placeholder: this.i18nStrings.searchFor, keyword: searchValue }
    };
    let orderByAttr: string = '';
    let orderByDir: string = '';
    switch (this.searchBarDataMaterialsPopup.sort?.options[this.searchBarDataMaterialsPopup.sort.selectedIndex].key) {
      case 'azAlphabetically':
        orderByAttr = MeSearchSortByTypes.name;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
      case 'zaAlphabetically':
        orderByAttr = MeSearchSortByTypes.name;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'newest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'oldest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
    }
    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_MATERIAL,
      criteriaQuick: searchValue,
      orderByAttr,
      orderByDir
    };
    this.areMaterialsLoadingPopup = true;
    this.subs.sink = this._materialsWebService
      .searchMaterials(searchFilter, from, top)
      .pipe(
        finalize(() => {
          this.areMaterialsLoadingPopup = false;
          this.isMaterialPopupSearch = true;
        })
      )
      .subscribe((response: ArrayResponseI<MaterialModel>) => {
        this.handleMaterialsResponsePopup(response, append, true);
      });
  }

  handleSearchInputEventCollectionMaterials(
    searchValue: string,
    from: number = 0,
    top: number = this.NUMBER_OF_ITEMS_ON_PAGE,
    append: boolean = false
  ): void {
    this.searchBarDataCollectionMaterial = {
      ...this.searchBarDataCollectionMaterial,
      search: { placeholder: this.i18nStrings.searchFor, keyword: searchValue }
    };
    let orderByAttr: string = '';
    let orderByDir: string = '';
    switch (this.searchBarDataCollectionMaterial.sort?.options[this.searchBarDataCollectionMaterial.sort.selectedIndex].key) {
      case 'azAlphabetically':
        orderByAttr = MeSearchSortByTypes.name;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
      case 'zaAlphabetically':
        orderByAttr = MeSearchSortByTypes.name;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'newest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'oldest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
    }

    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_MATERIAL,
      criteriaQuick: searchValue,
      orderByAttr,
      orderByDir
    };
    this.areMaterialsLoading = true;
    this.globalService.activateLoader();
    this.subs.sink = this.collectionWebService
      .searchCollectionMaterials(this.collection.oid, searchFilter, from, top)
      .pipe(
        finalize(() => {
          this.globalService.deactivateLoader();
          this.isCollectionMaterialsSearch = true;
          this.areMaterialsLoading = false;
        })
      )
      .subscribe(
        (response: ArrayResponseI<MaterialModel>) => {
          if (response && response.entities) {
            this.handleCollectionMaterialsResponse(response, append);
          }
        },
        (error: ErrorResponseI | HttpErrorResponse) => {
          console.error(error);
        }
      );
  }

  handleSearchInputEventDocumentsPopup(
    searchValue: string,
    from: number = 0,
    top: number = this.NUMBER_OF_ITEMS_ON_PAGE,
    append: boolean = false
  ): void {
    this.searchBarDataDocumentsPopup = {
      ...this.searchBarDataDocumentsPopup,
      search: { placeholder: this.i18nStrings.searchFor, keyword: searchValue }
    };
    let orderByAttr: string = '';
    let orderByDir: string = '';
    switch (this.searchBarDataDocumentsPopup.sort?.options[this.searchBarDataDocumentsPopup.sort.selectedIndex].key) {
      case 'azAlphabetically':
        orderByAttr = MeSearchSortByTypes.title;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
      case 'zaAlphabetically':
        orderByAttr = MeSearchSortByTypes.title;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'newest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'oldest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
    }
    const searchFilter: QuickSearchModel = {
      domain: this.selectedDocumentType,
      criteriaQuick: searchValue,
      orderByAttr,
      orderByDir
    };
    this.areDocumentsPopupLoading = true;
    this.subs.sink = this.documentsWebService
      .searchDocuments(searchFilter, from, top)
      .pipe(
        finalize(() => {
          this.areDocumentsPopupLoading = false;
          this.isDocumentPopupSearch = true;
        })
      )
      .subscribe((response: ArrayResponseI<MeDocumentModel>) => {
        this.handleDocumentsResponsePopup(response, append);
      });
  }

  handleSearchInputEventCollectionDocuments(
    searchValue: string,
    from: number = 0,
    top: number = this.NUMBER_OF_ITEMS_ON_PAGE,
    append: boolean = false
  ): void {
    this.searchBarDataCollectionDocuments = {
      ...this.searchBarDataCollectionDocuments,
      search: { placeholder: this.i18nStrings.searchFor, keyword: searchValue }
    };
    let orderByAttr: string = '';
    let orderByDir: string = '';
    switch (this.searchBarDataCollectionDocuments.sort?.options[this.searchBarDataCollectionDocuments.sort.selectedIndex].key) {
      case 'azAlphabetically':
        orderByAttr = MeSearchSortByTypes.title;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
      case 'zaAlphabetically':
        orderByAttr = MeSearchSortByTypes.title;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'newest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'oldest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
    }
    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_DOCUMENT,
      criteriaQuick: searchValue,
      orderByAttr,
      orderByDir
    };
    this.areDocumentsLoading = true;
    this.globalService.activateLoader();
    this.subs.sink = this.collectionWebService
      .searchCollectionDocuments(this.collection.oid, searchFilter, from, top)
      .pipe(
        finalize(() => {
          this.globalService.deactivateLoader();
          this.areDocumentsLoading = false;
          this.isCollectionDocumentSearch = true;
        })
      )
      .subscribe((response: ArrayResponseI<MeDocumentModel>) => {
        this.handleCollectionDocumentsResponse(response, append);
      });
  }

  // HANDLE SORT BY OPTION FOR ALL 4 TYPES
  handleSortByEventCollectionMaterials(sortByValue: string): void {
    if (this.searchBarDataCollectionMaterial.sort) {
      this.searchBarDataCollectionMaterial.sort.selectedIndex = this.searchBarDataCollectionMaterial.sort.options.findIndex(
        (x) => x.key === sortByValue
      );
    }
    this.handleSearchInputEventCollectionMaterials(
      this.searchBarDataCollectionMaterial.search ? this.searchBarDataCollectionMaterial.search.keyword : ''
    );
  }

  handleSortByEventMaterialsPopup(sortByValue: string): void {
    if (this.searchBarDataMaterialsPopup.sort) {
      this.searchBarDataMaterialsPopup.sort.selectedIndex = this.searchBarDataMaterialsPopup.sort.options.findIndex(
        (x) => x.key === sortByValue
      );
    }
    this.handleSearchInputEventMaterialsPopup(
      this.searchBarDataMaterialsPopup.search ? this.searchBarDataMaterialsPopup.search.keyword : ''
    );
  }

  handleSortByEventCollectionDocuments(sortByValue: string): void {
    if (this.searchBarDataCollectionDocuments.sort) {
      this.searchBarDataCollectionDocuments.sort.selectedIndex = this.searchBarDataCollectionDocuments.sort.options.findIndex(
        (x) => x.key === sortByValue
      );
    }
    this.handleSearchInputEventCollectionDocuments(
      this.searchBarDataCollectionDocuments.search ? this.searchBarDataCollectionDocuments.search.keyword : ''
    );
  }

  handleSortByEventDocumentsPopup(sortByValue: string): void {
    if (this.searchBarDataDocumentsPopup.sort) {
      this.searchBarDataDocumentsPopup.sort.selectedIndex = this.searchBarDataDocumentsPopup.sort.options.findIndex(
        (x) => x.key === sortByValue
      );
    }
    this.handleSearchInputEventDocumentsPopup(
      this.searchBarDataDocumentsPopup.search ? this.searchBarDataDocumentsPopup.search.keyword : ''
    );
  }

  // TOGGLE CARD ITEM SELECTION
  handleToggleSelectEventCollectionMaterials(): void {
    if (this.searchBarDataCollectionMaterial.selectButton) {
      this.selectionCollectionMaterialEnabled = !this.searchBarDataCollectionMaterial.selectButton.enabled;
      this.searchBarDataCollectionMaterial = {
        ...this.searchBarDataCollectionMaterial,
        selectButton: {
          ...this.searchBarDataCollectionMaterial.selectButton,
          enabled: !this.searchBarDataCollectionMaterial.selectButton.enabled,
          placeholder: this.selectionCollectionMaterialEnabled ? this.i18nStrings.cancel : this.i18nStrings.select
        }
      };

      this.collectionMaterialCardItems = this.collectionMaterialCardItems.map((item) => {
        return { ...item, selectionEnabled: this.selectionCollectionMaterialEnabled, selected: false };
      });
    }
  }

  handleToggleSelectEventCollectionDocuments(): void {
    if (this.searchBarDataCollectionDocuments.selectButton) {
      this.selectionCollectionDocumentEnabled = !this.searchBarDataCollectionDocuments.selectButton.enabled;
      this.searchBarDataCollectionDocuments = {
        ...this.searchBarDataCollectionDocuments,
        selectButton: {
          ...this.searchBarDataCollectionDocuments.selectButton,
          enabled: !this.searchBarDataCollectionDocuments.selectButton.enabled,
          placeholder: this.selectionCollectionDocumentEnabled ? this.i18nStrings.cancel : this.i18nStrings.select
        }
      };

      this.cardItemsCollectionDocuments = this.cardItemsCollectionDocuments.map((item) => {
        return { ...item, selectionEnabled: this.selectionCollectionDocumentEnabled, selected: false };
      });
    }
  }

  // SELECTION CARD HANDLERS
  materialCardSelectHandler(oid: string): void {
    this.materialCardItemsPopup = this.materialCardItemsPopup.map((material) => {
      return material.oid === oid ? { ...material, selected: !material.selected } : material;
    });
    let selectCount: number = 0;
    selectCount = this.materialCardItemsPopup.filter((material) => {
      return material.selected;
    }).length;
    this.popupSelectService.pushDataModelToPopup({ ...this.materialDataPopup, selectedCount: selectCount });
  }

  selectedMaterialCardSelectHandler(oid: string): void {
    this.materialCardSelectHandler(oid);
    this.selectedMaterialsStorePopup.materialEntities = this.materialsStoreServicePopup.materialEntities;
  }

  documentCardSelectHandler(oid: string): void {
    this.cardItemsDocumentsPopup = this.cardItemsDocumentsPopup.map((document) => {
      return document.oid === oid ? { ...document, selected: !document.selected } : document;
    });
    let selectCount: number = 0;
    selectCount = this.cardItemsDocumentsPopup.filter((document) => {
      return document.selected;
    }).length;
    this.popupSelectService.pushDataModelToPopup({ ...this.documentPopupData, selectedCount: selectCount });
  }

  selectedDocumentCardSelectHandler(oid: string): void {
    this.documentCardSelectHandler(oid);
    this.selectedDocumentsStorePopup.documentEntities = this._documentStoreServicePopup.documentEntities;
  }

  toggleDocumentCardItemSelection(documentOid: string, index: number): void {
    const docObject = this._documentStoreServicePopup.documentEntities.find((doc) => doc.oid === documentOid);
    if (docObject) {
      if (this.selectedDocumentsStorePopup.documentEntities.includes(docObject)) {
        this.cardItemsDocumentsPopup[index] = { ...this.cardItemsDocumentsPopup[index], selected: false };
        this.selectedDocumentsStorePopup.documentEntities = this.selectedDocumentsStorePopup.documentEntities.filter(
          (doc) => doc.oid !== documentOid
        );
      } else {
        this.cardItemsDocumentsPopup[index] = { ...this.cardItemsDocumentsPopup[index], selected: true };
        this.selectedDocumentsStorePopup.documentEntities = [...this.selectedDocumentsStorePopup.documentEntities, docObject];
      }
    }
    this.updateSelectedActiveDocumentsPopup({ selectedCount: this.selectedDocumentsStorePopup.documentEntities.length });
  }

  toggleMaterialCardItemSelection(materialOid: string, index: number): void {
    const matObject = this.materialsStoreServicePopup.materialEntities.find((mat) => mat.oid === materialOid);
    if (matObject) {
      if (this.selectedMaterialsStorePopup.materialEntities.includes(matObject)) {
        this.materialCardItemsPopup[index] = { ...this.materialCardItemsPopup[index], selected: false };
        this.selectedMaterialsStorePopup.materialEntities = this.selectedMaterialsStorePopup.materialEntities.filter(
          (mat) => mat.oid !== materialOid
        );
      } else {
        this.materialCardItemsPopup[index] = { ...this.materialCardItemsPopup[index], selected: true };
        this.selectedMaterialsStorePopup.materialEntities = [...this.selectedMaterialsStorePopup.materialEntities, matObject];
      }
    }
    this.updateSelectedActiveMaterialsPopup({ selectedCount: this.selectedMaterialsStorePopup.materialEntities.length });
  }

  // SELECTING CARD FOR DELETE
  selectMaterialCardForDelete(cardIndex: number): void {
    if (this.searchBarDataCollectionMaterial.selectButton && !this.searchBarDataCollectionMaterial.selectButton.enabled) {
      return;
    }
    this.collectionMaterialCardItems[cardIndex] = {
      ...this.collectionMaterialCardItems[cardIndex],
      selected: !this.collectionMaterialCardItems[cardIndex].selected
    };
  }

  selectDocumentCardForDelete(cardIndex: number): void {
    if (this.searchBarDataCollectionDocuments.selectButton && !this.searchBarDataCollectionDocuments.selectButton.enabled) {
      return;
    }
    this.cardItemsCollectionDocuments[cardIndex] = {
      ...this.cardItemsCollectionDocuments[cardIndex],
      selected: !this.cardItemsCollectionDocuments[cardIndex].selected
    };
  }

  goToCreateDocument(): void {
    this.popupSelectService.closePopup();
    this.router.navigate(['documents', 'create']);
  }

  goToCreateMaterials(): void {
    this.popupSelectService.closePopup();
    this.router.navigate(['materials', 'create']);
  }

  ngOnDestroy(): object {
    this.subs.unsubscribe();
    if (this._removeMaterialSweetAlert) {
      this._removeMaterialSweetAlert.unsubscribe();
    }
    return super.ngOnDestroy();
  }
}
