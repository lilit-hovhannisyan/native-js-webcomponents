import { FormGroup } from '@angular/forms';

export interface MeSelectInputI {
  form: FormGroup;
  internalName: string;
  displayName: string;
  required: boolean;
  optRequired: boolean;
  value: string;
  values: { displayName: string; value: string }[];
  disabled: boolean;
}
