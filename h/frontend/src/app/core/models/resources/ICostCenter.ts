import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';
import { COST_CENTER_TYPE } from '../enums/cost-center-type';
import { IWording } from './IWording';

export type CostCenterKey = number;

interface ISharedProps extends IWithLabels {
  no: number;
  remark: string;
  isActive: boolean;
  type: COST_CENTER_TYPE;
}

export interface ICostCenterCreate extends ISharedProps { }

export interface ICostCenterBase extends ISharedProps, IResource<CostCenterKey> { }

export interface ICostCenterDTO extends ICostCenterBase { }

export interface ICostCenter extends ICostCenterDTO, IWithLabel { }

export interface ICostCenterRow extends ICostCenter {
  typeLabel: IWording;
}
