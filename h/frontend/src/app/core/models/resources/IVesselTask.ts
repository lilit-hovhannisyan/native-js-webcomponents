import { IResource } from './IResource';
import { VesselKey } from './IVessel';
import { IWithLabels } from './IWithLabels';
import { IVesselTaskTodoItem } from './IVesselTaskTodoItem';
import { TaskType, VesselTaskStatus } from '../enums/vessel-registration-tasks';
import { IWithLabel } from './IWithLabel';

export type VesselTaskKey = number;

export enum TaskLanguageType {
  None = 0,
  En = 1,
  De = 2,
  All = 3,
}


interface ISharedProps extends IWithLabels {
  vesselId: VesselKey;
  type: TaskType;
  status: VesselTaskStatus;
  color: string;
  start: string;
  languageType?: TaskLanguageType;
  end?: string;
  remark?: string;
  useStartDate?: boolean;
  todos?: IVesselTaskTodoItem[];
}

export interface IVesselTaskCreate extends ISharedProps { }

export interface IVesselTaskBase extends ISharedProps, IResource<VesselTaskKey> { }

export interface IVesselTaskDTO extends IVesselTaskBase { }

export interface IVesselTask extends IVesselTaskDTO { }


export interface ITaskFullAdditions extends IWithLabel {
  vesselAccountingName?: string;
  uniqueId?: string;
}

type WithAdditions<T extends IVesselTask = IVesselTask> = ITaskFullAdditions & T;

export declare type IVesselTaskFull<T extends IVesselTask = IVesselTask> = {
  [P in keyof WithAdditions<T>]: WithAdditions<T>[P];
};
