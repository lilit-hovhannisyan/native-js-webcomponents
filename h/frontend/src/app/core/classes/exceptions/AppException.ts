/**
 * Base class for all errors
 */
export class AppException extends Error {
  constructor(m: string) {
    super(m);

    // this is needed!
    // see @see https://github.com/Microsoft/TypeScript-wiki/blob/master/Breaking-Changes.md
    Object.setPrototypeOf(this, AppException.prototype);
  }
}
