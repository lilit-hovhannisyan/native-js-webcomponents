import { Component, OnInit, Input } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup, FormBuilder } from '@angular/forms';

import { IStevedoreDamageDocument } from 'src/app/core/models/resources/IStevedoreDamageDocument';
import { Validators } from 'src/app/core/classes/validators';


@Component({
  selector: 'nv-edit-document-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.scss']
})
export class StevedoreDamegeDocumentEditComponent implements OnInit {
  @Input() public closeModal: () => void;
  @Input() public document: IStevedoreDamageDocument;
  @Input() public formChange: (status: any) => void;
  public wording = wording;
  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
  ) { }

  public ngOnInit() {
    this.init(this.document);
  }

  public init(data: IStevedoreDamageDocument = {} as IStevedoreDamageDocument) {
    this.form = this.fb.group({
      isAttached: [{ value: data.isAttached, disabled: false }],
      remark: [{ value: data.remark, disabled: false }, Validators.maxLength(150)],
    });

    this.form.statusChanges
      .subscribe(status => this.formChange(status));
  }
}
