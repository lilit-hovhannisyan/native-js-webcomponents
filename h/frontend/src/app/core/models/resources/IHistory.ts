import { IDifference } from './IDifference';
import { IResource } from './IResource';
export type HistoryKey = number;

interface ISharedProps {
  changeDate: string;
  entity: string;
  actionType: string;
  editedBy: string;
  differences: IDifference[];
}

export interface IHistoryBase extends ISharedProps, IResource<HistoryKey> { }

export interface IHistoryCreate extends ISharedProps { }

export interface IHistoryDTO extends IHistoryBase { }

export interface IHistory extends IHistoryDTO { }

