import { BaseSearchModel } from './search-base.model';

export class AdvancedSearchModel extends BaseSearchModel {
  // tslint:disable-next-line:no-any
  [key: string]: any;
}
