import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { BookingAccountKey } from 'src/app/core/models/resources/IBookingAccount';
import { AccountCostTypeRepositoryService } from 'src/app/core/services/repositories/account-cost-type-repository.service';
import { CostTypeKey, ICostType } from 'src/app/core/models/resources/ICostTypes';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { wording } from 'src/app/core/constants/wording/wording';
import { IAccountCostType } from 'src/app/core/models/resources/IAccountCostType';
import { CostTypeRepositoryService } from 'src/app/core/services/repositories/cost-type-repository.service';

@Injectable({
  providedIn: 'root'
})
export class CostTypeValidatorsService {

  constructor(
    private accountCostTypeRepo: AccountCostTypeRepositoryService,
    private costTypeRepo: CostTypeRepositoryService,
  ) { }

  public isCostTypeRelatedToBookingAccount = (control: AbstractControl): ValidationErrors | null => {
    if (
      !control.value
      || !control.parent
      || !control.parent.get('bookingAccountId').value
    ) {
      return null;
    }

    const bookingAccountId: BookingAccountKey = +control.parent.get('bookingAccountId').value;
    const costTypeId: CostTypeKey = +control.parent.get('costTypeId').value;

    const foundCostType: IAccountCostType = this.accountCostTypeRepo.getStreamValue()
      .filter(c => c.bookingAccountId === bookingAccountId)
      .find(c => c.costTypeId === costTypeId);

    if (foundCostType) {
      return null;
    }

    markControlAsTouched(control);
    return {
      notRelatedToAccount: {
        message: wording.general.notYetRelated
      }
    };
  }

  public doesCostTypeExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.parent || !control.value) {
      return null;
    }
    const costTypeId: CostTypeKey = +control.parent.get('costTypeId').value;

    const foundCostType: ICostType = this.costTypeRepo.getStreamValue()
      .find(c => c.id === costTypeId);

    if (foundCostType) {
      return null;
    }

    markControlAsTouched(control);
    return {
      doesNotExist: {
        message: wording.general.doesNotExist
      }
    };
  }
}
