enum SkipOptions {
  DEFAULT = 1,
  SKIP_SELF = 2,
  SKIP_TREE = 3
}

export default SkipOptions;
