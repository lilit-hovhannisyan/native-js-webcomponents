import { HttpResponse, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, } from 'rxjs';
import { map } from 'rxjs/operators';
import { IDocumentBlobInfo } from '../models/IDocumentBlobInfo';
import { HttpGet } from '../models/http/http-get';

/**
 * This class is used to download documents
 */
@Injectable({
  providedIn: 'root'
})
export class StoredDocumentService {
  constructor(
    private httpClient: HttpClient
  ) { }

  public getBaseUrl(): string {
    return '/api/stored-documents/';
  }

  public fetchDocument(documentGuid: string): Observable<IDocumentBlobInfo> {
    const request = new HttpGet(
      this.getBaseUrl(),
      {
        init: {
          responseType: 'blob',
          observe: 'response',
          params: { documentGuid },
        },
      }
    );
    const source = this.httpClient.request(request)
      .pipe(
        map((response: HttpResponse<Blob>) => {
          const contentType = response.headers.get('content-type');
          const blob = new Blob([response.body], { type: contentType });
          const disposition = response.headers.get('content-disposition');
          const matches = /"([^"]*)"/.exec(disposition);
          const filename = (matches != null && matches[1]) ? matches[1] : 'download.xlsx';

          return { blob, filename };
        }));

    return source;
  }
}
