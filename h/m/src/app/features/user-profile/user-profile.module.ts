import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MeButtonModule } from '@shared/me-components/me-button/me-button.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MeUploadImageModule } from '@shared/me-components/me-upload-image/me-upload-image.module';

import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';

import { UserProfileRoutingModule } from './user-profile-routing.module';
import { UserProfileComponent } from './user-profile.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatStepperModule } from '@angular/material/stepper';
import { I18nModule } from '@shared/modules/i18n.module';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  declarations: [UserProfileComponent],
  imports: [
    CommonModule,
    UserProfileRoutingModule,
    IconsModule,
    MeButtonModule,
    ReactiveFormsModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatStepperModule,
    MatSelectModule,
    FormsModule,
    MatButtonModule,
    MeUploadImageModule,
    MatTooltipModule,
    I18nModule
  ],
  exports: [UserProfileComponent]
})
export class UserProfileModule {}
