import { Injectable } from '@angular/core';
import { AbstractWidgetsService } from './abstract-widgets-repository';
import { IWeatherWidget, IWeatherWidgetDTO } from 'src/app/core/models/widgets/IWeatherWidget';
import { RepositoryService } from '../../../../core/services/repository.service';

@Injectable({
  providedIn: 'root'
})
export class WeatherWidgetService extends AbstractWidgetsService {

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl() {
    return '/api/user-account/widgets/weather';
  }

  public preProcess(w: IWeatherWidget): IWeatherWidgetDTO {
    return {
      id: w.id,
      type: w.type,
      _type: w._type,
      position: {
        x: w.x,
        y: w.y,
        width: w.cols,
        height: w.rows
      },
      geoLocation: w.geoLocation
    };
  }

  public updateWidget(widget: IWeatherWidget) {
    return this.update(this.preProcess(widget), {});
  }
}
