import {BaseComponent} from '../../../../../library/core/base-component/base.component.js';
import {basePath} from '../../../data/base.path.js';

export class AppRoot extends BaseComponent {
  constructor() {
    super(basePath + 'app-base-components/app-root/app-root.component.css');
  }

  render = () => {
    const config = {
      tagName: 'div',
      attr: {
        class: 'app-root'
      },
      subElements: [
        {
          tagName: 'git-app-header',
        },
        {
          tagName: 'git-app-body',
        }
      ]
    };
    const el = this.createElementWithAttributes(config);

    this.shadow.appendChild(el);
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }
}