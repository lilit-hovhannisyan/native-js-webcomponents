import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';


export type AutobankOperationTypeKey = number;

interface ISharedProps extends IWithLabels { }

export interface IAutobankOperationTypeBase extends ISharedProps, IResource<AutobankOperationTypeKey> { }

export interface IAutobankOperationTypeCreate extends ISharedProps { }

export interface IAutobankOperationTypeDTO extends IAutobankOperationTypeBase { }

export interface IAutobankOperationType extends IAutobankOperationTypeDTO, IWithLabel { }

