import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Observable } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { wording } from 'src/app/core/constants/wording/wording';
import { Language } from 'src/app/core/models/language';
import { TaskLanguageType } from 'src/app/core/models/resources/IVesselTask';
import { IWording } from 'src/app/core/models/resources/IWording';
import { ITaskTemplate } from 'src/app/core/models/resources/Task-Templates/ITaskTemplate';
import { TaskTemplateCategoryKey } from 'src/app/core/models/resources/Task-Templates/ITaskTemplateCategory';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { TaskTemplateRepositoryService } from 'src/app/core/services/repositories/task-templates/task-template-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { TaskTemplatesHelperService } from '../task-templates-helper.service';
import { TaskTemplateCreateComponent } from './task-template-create/task-template-create.component';

export interface ICollapsableStatus {
  // TaskTemplateKey
  [id: number]: {
    disabled: boolean;
    active: boolean;
  };
}

@Component({
  selector: 'nv-task-templates-detail',
  templateUrl: './task-templates-detail.component.html',
  styleUrls: ['./task-templates-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskTemplatesDetailComponent implements OnInit {
  public wording = wording;
  public form: FormGroup;
  public taskTemplates$: Observable<ITaskTemplate[]>;
  public collapsableStatus: ICollapsableStatus = {};
  public taskLanguageType = TaskLanguageType;

  private modalRef: NzModalRef;
  private language: Language;

  constructor(
    public taskTemplatesHelperService: TaskTemplatesHelperService,
    private taskTemplateRepo: TaskTemplateRepositoryService,
    private modalService: NzModalService,
    private settingsService: SettingsService,
    private confirmationService: ConfirmationService,
  ) { }

  public ngOnInit(): void {
    this.language = this.settingsService.language;
    this.fetchData();
  }

  private fetchData(): void {
    this.taskTemplates$ = this.taskTemplatesHelperService.selectedCategory$
      .pipe(
        filter(id => !!id),
        switchMap((categoryId: TaskTemplateCategoryKey) => {
          return this.taskTemplateRepo.fetchAll({
            queryParams: {
              categoryId
            },
            loadingIndicatorHidden: true,
          });
        }),
        switchMap(() => {
          const categoryId: TaskTemplateCategoryKey = this.taskTemplatesHelperService
            .selectedCategory
            .value;

          return this.taskTemplateRepo.getStream()
            .pipe(map(tasks => tasks.filter(t => t.categoryId === categoryId)));
        }),
        map(taskTemplates => {
          taskTemplates.forEach(t => {
            if (!this.collapsableStatus[t.id]) {
              this.collapsableStatus[t.id] = {
                disabled: false,
                active: false,
              };
            }
          });
          return [...taskTemplates] as ITaskTemplate[];
        })
      );
  }

  public onTaskCreateOrEdit = (task?: ITaskTemplate): void => {
    if (task && !this.taskTemplatesHelperService.checkForSystemDefault(task)) {
      return;
    }
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzWidth: 800,
      nzTitle: task
        ? wording.system.editTaskTemplate[this.language]
        : wording.system.createTaskTemplate[this.language],
      nzClosable: true,
      nzContent: TaskTemplateCreateComponent,
      nzComponentParams: {
        closeModal: this.closeModal,
        taskTemplate: task,
      }
    });
  }

  private closeModal = () => {
    this.modalRef.destroy();
  }

  public onTaskDelete(task: ITaskTemplate): void {
    if (task && !this.taskTemplatesHelperService.checkForSystemDefault(task)) {
      return;
    }
    const subject: IWording = {
      en: task.labelEn,
      de: task.labelDe,
    };

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject },
    }).subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.taskTemplateRepo.delete(task.id, {})
          .subscribe();
      }
    });
  }
}
