import { RequiredConstraintState } from '../attribute/attribute.constraint.model';

export class BaseModel {
  createdOn: string = '';
  domainName: string = '';
  id: string = '';
  modifiedOn: string = '';
  oid: string = '';
  state: RequiredConstraintState = RequiredConstraintState.draft;
  ownerOID?: string = '';
}
