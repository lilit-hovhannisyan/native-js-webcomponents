import { Component } from '@angular/core';

@Component({
  selector: 'nv-baz',
  templateUrl: './baz.component.html',
  styleUrls: ['./baz.component.scss']
})
export class BazComponent { }
