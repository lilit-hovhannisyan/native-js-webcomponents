import { Injectable, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { forkJoin, from, Observable, throwError } from 'rxjs';
import { concatMap } from 'rxjs/operators';

import { ArrayRequestI } from '@core-interfaces/array-request.interface';
import { EntityI } from '@core-interfaces/entity.interface';
import { ME_DOMAIN_APPLICATION_USER } from '@shared/constants';
import { UserModel } from '@shared/models/user/user.model';
import { BackOfficeService } from '../backoffice.web-service';
import { MeAddListCardI } from '@shared/me-components/me-add-list-card/me-add-list-card.interface';
import { getUserInitials } from '@shared/utils';

@Injectable()
export class UserActionsService implements OnDestroy {
  private groupsIntersectionMap: Map<string, void> = new Map();
  private subs = new SubSink();
  addGroupListCardItems: MeAddListCardI[] = [];
  addUserListCardItemsToSend: string[] = [];
  removeUserListCardItemsToSend: string[] = [];
  userStatusEntity: ArrayRequestI<EntityI> = {
    entities: [
      {
        domainName: ME_DOMAIN_APPLICATION_USER,
        attributes: {}
      }
    ]
  };

  constructor(private backOfficeService: BackOfficeService) {}

  activateUser(userOID: string): Observable<UserModel> {
    this.userStatusEntity.entities[0].attributes.isActive = true;

    return this.updateMeUserStatus(userOID);
  }

  deActivateUser(userOID: string) {
    this.userStatusEntity.entities[0].attributes.isActive = false;

    return this.updateMeUserStatus(userOID);
  }

  removeOrAddUsersFromGroup(userOID: string): Observable<never> {
    const fjObject: { [key: string]: Observable<boolean> } = {};

    if (this.addUserListCardItemsToSend && this.addUserListCardItemsToSend.length > 0) {
      fjObject.add = from(this.addUserListCardItemsToSend).pipe(concatMap((oid) => this.backOfficeService.addUserToBoGroup(oid, userOID)));
    }

    if (this.removeUserListCardItemsToSend && this.removeUserListCardItemsToSend.length > 0) {
      fjObject.remove = from(this.removeUserListCardItemsToSend).pipe(
        concatMap((oid) => this.backOfficeService.deleteUserFromBoGroup(oid, userOID))
      );
    }

    if (Object.keys(fjObject).length > 0) {
      return forkJoin(fjObject);
    }

    throw throwError('There is no user to add or delete from group');
  }

  getUserGroups(userOID: string): Promise<void> {
    return new Promise((resolve, reject) => {
      this.subs.sink = this.backOfficeService.getBoUserGroups(userOID).subscribe(
        (response) => {
          this.groupsIntersectionMap.clear();
          for (const { oid } of response.entities) {
            this.groupsIntersectionMap.set(oid);
          }
          resolve();
        },
        (error) => reject(error)
      );
    });
  }

  async prepareAddListGroupCardData(userOID: string): Promise<MeAddListCardI[]> {
    console.log('user service prepareAddListGroupCardData');
    return new Promise(async (resolve, reject) => {
      try {
        await this.getUserGroups(userOID);
      } catch (error) {
        reject(error);
      }

      this.subs.sink = this.backOfficeService.getBoGroups(0, 50).subscribe(
        (response) => {
          if (response && response.entities) {
            let addGroupListCardItems: MeAddListCardI[] = [];

            addGroupListCardItems = response.entities.map((group) => {
              const {
                attributes: { name, description },
                attributesExtended: { roleRef },
                oid
              } = group;

              return {
                groupName: name,
                groupRole: roleRef,
                description,
                oid,
                initials: getUserInitials(name),
                thumbnail: '',
                type: 'group',
                showButton: true,
                isInGroup: this.groupsIntersectionMap.has(oid)
              };
            });

            resolve(addGroupListCardItems);
          }
        },
        (error) => reject(error)
      );
    });
  }

  handleAddListCardEvent(event: { eventName: string; payload: MeAddListCardI }, index: number, cardList: MeAddListCardI[]) {
    switch (event.eventName) {
      case 'add':
        if (event.payload && event.payload.oid) {
          if (!this.addUserListCardItemsToSend.includes(event.payload.oid)) {
            this.addUserListCardItemsToSend.push(event.payload.oid);
          }
        }
        break;
      case 'remove':
        if (event.payload && event.payload.oid) {
          if (
            !this.removeUserListCardItemsToSend.includes(event.payload.oid) &&
            !this.addUserListCardItemsToSend.includes(event.payload.oid)
          ) {
            this.removeUserListCardItemsToSend.push(event.payload.oid);
          } else {
            this.addUserListCardItemsToSend = this.addUserListCardItemsToSend.filter((card) => card !== event.payload.oid);
          }
        }
        break;
    }

    cardList[index] = {
      ...cardList[index],
      isInGroup: this.addUserListCardItemsToSend.includes(event.payload.oid)
    };
  }

  private updateMeUserStatus(oid: string) {
    return this.backOfficeService.updateBoUser(oid, this.userStatusEntity);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
