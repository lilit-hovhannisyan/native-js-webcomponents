#!/bin/bash

# TODO: check if all (env) vars are available

ssh -p ${SSH_PORT} ${SSH_ADDRESS} "
  mkdir -p ${REMOTE_PROJECT_ROOT} &&
  mkdir -p ${REMOTE_PROJECT_ROOT}/tmp
"

scp -P ${SSH_PORT} ./.env ${SSH_ADDRESS}:${REMOTE_PROJECT_ROOT}
scp -P ${SSH_PORT} ./docker-compose.yml ${SSH_ADDRESS}:${REMOTE_PROJECT_ROOT}

ssh -p ${SSH_PORT} ${SSH_ADDRESS} "
  cd ${REMOTE_PROJECT_ROOT} &&
  docker login -u ${DOCKER_USER} -p ${CI_JOB_TOKEN} ${CI_REGISTRY} &&
  MAIN_IMAGE=${MAIN_IMAGE} docker-compose pull --include-deps ${MAIN_SERVICE} &&
  COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME} docker-compose down --remove-orphans &&
  COMPOSE_PROJECT_NAME=${COMPOSE_PROJECT_NAME} MAIN_IMAGE=${MAIN_IMAGE} APP_PORT=${APP_PORT} docker-compose up --no-build --detach --force-recreate --remove-orphans ${MAIN_SERVICE} &&
  docker logout ${CI_REGISTRY}
"
