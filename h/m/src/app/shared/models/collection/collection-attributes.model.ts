export class CollectionAttributesModel {
  name: string = '';
  description?: string = '';
  thumbnail: string = '';
  status: string = '';
  ownerRef: string = '';
  creator: string = '';
  modifier: string = '';
}
