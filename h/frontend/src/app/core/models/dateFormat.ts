export enum NvLocale {
  DE = 0,
  UK = 1,
  US = 2,
}

export const dateFormatTypes = {
  [NvLocale.DE]: 'DD.MM.YYYY',
  [NvLocale.UK]: 'DD/MM/YYYY',
  [NvLocale.US]: 'MM/DD/YYYY',
};

export const timeFormatTypes = {
  [NvLocale.DE]: 'HH:mm',
  [NvLocale.UK]: 'hh:mm A',
  [NvLocale.US]: 'hh:mm A',
};

export const nzDatePickerDateFormatTypes = {
  [NvLocale.DE]: 'dd.MM.yyyy',
  [NvLocale.UK]: 'dd/MM/yyyy',
  [NvLocale.US]: 'MM/dd/yyyy',
};
