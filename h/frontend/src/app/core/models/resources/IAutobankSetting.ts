import { IResource } from './IResource';

export type AutobankSettingKey = number;

interface ISharedProps {
  bookingCodeBankAccountBookingId: number;
  bookingCodeBankForeignIncomingPaymentId: number;
  matchingAccountIncomingPaymentId: number;
  matchingAccountOutgoingPaymentId: number;
  bankFeesAccountId: number;
  exchangeRateProfitAccountId: number;
  exchangeRateLossAccountId: number;
  path: string;
}

export interface IAutobankSettingBase extends ISharedProps, IResource<AutobankSettingKey> { }

export interface IAutobankSettingCreate extends ISharedProps { }

export interface IAutobankSettingDTO extends IAutobankSettingBase { }

export interface IAutobankSetting extends IAutobankSettingDTO { }

