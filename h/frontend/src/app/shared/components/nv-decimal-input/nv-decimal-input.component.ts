import { NvLocale } from 'src/app/core/models/dateFormat';
import { Component, Input, forwardRef, OnInit, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { formatNumber } from '@angular/common';
import { nvLocalMapper } from '../../pipes/localDecimal';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  selector: 'nv-decimal-input',
  templateUrl: './nv-decimal-input.component.html',
  styleUrls: ['./nv-decimal-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => NvDecimalInputComponent),
    }
  ]
})
export class NvDecimalInputComponent implements OnInit, ControlValueAccessor {

  @Input() public showSeparator = false;
  @Input() public showThousandsSeparator = true;
  @Input() public onlyInteger = false;
  @Input() public decimalPlaces = 0; // the minimum number of decimal places
  @Input() public inputFieldStyle = {};

  /**
   * used to give the input we have in template an id <input [id]="formControlName"/>
   * so we select it easier in our e2e tests
   */
  @Input() public formControlName: string;

  /**
   * Used to pass value to the component via @Input when the component is not formControl
   */
  @Input() public set valueAsInput(value: number) {
    this.writeValue(value);
  }

  @Input() public set disabledState(value: boolean) {
    this.setDisabledState(value);
  }

  /**
   * This is needed to emit event when value change is done when the component is not
   * formControl
   */
  @Output() public valueChangeEvent: EventEmitter<number> = new EventEmitter<number>();
  @Output() public valueChangeOnBlur: EventEmitter<number> = new EventEmitter<number>();

  public value = '';
  public disabled = false;

  private _onChange: (any: number) => void;
  private decimalSeparator: ',' | '.';
  private thousandsSeparator: ',' | '.';
  private germanFormat: boolean;
  private parsedValue?: number;
  private digitsInfo: string;
  private initialized = false;

  constructor(
    private cdr: ChangeDetectorRef,
    private settingsService: SettingsService
  ) { }

  public ngOnInit() {
    if (!this.initialized) { this.initialize(); }
  }

  public initialize() {
    // we use this method instead of ngOnInit because angular calls in some cases
    // writeValue before ngOnInit.
    this.initialized = true;
    this.germanFormat = this.settingsService.locale === NvLocale.DE;
    this.decimalSeparator = this.germanFormat ? ',' : '.';
    this.thousandsSeparator = this.germanFormat ? '.' : ',';

    if (this.decimalPlaces !== undefined) {
      this.digitsInfo = `1.${this.decimalPlaces}-100`;
    }
  }

  public writeValue = (value?: number) => {
    if (!this.initialized) { this.initialize(); }

    if (value === undefined || value === null) {
      this.value = '';
      this.parsedValue = undefined;
      return;
    }

    if (this.parsedValue === value) {
      return;
    }

    this.value = this.showThousandsSeparator
      ? formatNumber(value, nvLocalMapper[this.settingsService.locale], this.digitsInfo)
      : value.toString();
  }

  public registerOnChange(fn: (_: any) => void): void {
    this._onChange = fn;
  }

  public registerOnTouched(fn: any): void { }

  public keypress = (e: any) => {
    const allowedCharacters = '1234567890-.,';
    const keyNotAllowed = !allowedCharacters.includes(e.key);
    // checks that decimal separator is only used once.
    const isAnotherSeparator = this.value.includes(this.decimalSeparator) && (e.key === this.decimalSeparator);
    const triesUnallowedFloat = this.onlyInteger && e.key === this.decimalSeparator;
    if (keyNotAllowed || isAnotherSeparator || triesUnallowedFloat) { e.preventDefault(); }
  }

  public valueChange = (_val: string) => {
    this.value = _val;
    this.parsedValue = this.checkSeparatorsAndParse(_val);
    this._onChange && this._onChange(this.parsedValue);
    this.valueChangeEvent.emit(this.parsedValue);
  }

  /**
   * This method needed when the component is not formControl and value transfers via event
   * emitters
  */
  public onBlur() {
    this.parsedValue = this.checkSeparatorsAndParse(this.value);

    // fixes infinity symbol value getting bug
    this.value = (this.parsedValue || this.parsedValue === 0)
      ? this.showThousandsSeparator
        ? formatNumber(
            this.parsedValue,
            nvLocalMapper[this.settingsService.locale],
            this.digitsInfo,
          )
        : this.parsedValue.toString()
      : '';

    this.valueChangeOnBlur.emit(this.parsedValue);
  }

  public checkSeparatorsAndParse(_val: string): number | undefined {
    let val = this.replaceAll(_val, this.thousandsSeparator, ''); // remove thousandsSeparators
    if (this.decimalSeparator === ',') { val = this.replaceAll(val, ',', '.'); } // make sure decimalSeparator is a dot
    const parsedValue = parseFloat(val);
    return isNaN(parsedValue) ? undefined : parsedValue;
  }

  public setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
    // need 'detectChanges', otherwise ng-zorro input element is not disabling / enabling immediately
    this.cdr.detectChanges();
  }

  public replaceAll = (targetStr: string, searchStr: string, replaceStr: string): string => {
    return targetStr.replace(new RegExp('\\' + searchStr, 'g'), replaceStr);
  }
}
