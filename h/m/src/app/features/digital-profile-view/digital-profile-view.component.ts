import { DigitalProfileModel } from './../../shared/models/digital-profile/digital-profile.model';
import { AfterViewInit, Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BaseLoggerComponent } from '@features/tracking-system';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';
import { DigitalProfileStoreService } from '@features/digital-profile/digital-profile-store.service';
import { Subscription } from 'rxjs';
import { DigitalProfileWebService } from '@features/digital-profile/digital-profile.web-service';
import { MeSearchBarI } from '@shared/me-components/me-search-bar/me-search-bar.interface';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { DomainModel } from '@shared/models/domain/domain.model';
import {
  MARKETPLACE_BASE_API_URL,
  ME_DOMAIN_APPLICATION_ADDRESS,
  ME_DOMAIN_APPLICATION_COLLECTION,
  ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL
} from '@shared/constants';
import { finalize } from 'rxjs/operators';
import { StateModel } from '@shared/models/state/state.model';
import { AttributeModel } from '@shared/models/attribute/attribute.model';
import { LayoutGroupModel } from '@shared/models/layout/layout-group.model';
import { MeAttributeLayoutTypes } from '@shared/me-attribute-layout-types';
import { AttributeGroupModel } from '@shared/models/attribute/attribute.group.model';
import { MeLabelI } from '@shared/me-components/me-label/me-label-inteface';
import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { DigitalProfileAttributesModel } from '@shared/models/digital-profile/digital-profile-attributes.model';
import { CollectionsWebService } from '@features/collections/collections.web-service';
import { CollectionModel } from '@shared/models/collection/collection.model';
import { CollectionsStoreService } from '@features/collections/collections-store.service';
import { MeNewCollectionCardI } from '@shared/me-components/me-new-collection-card/me-new-collection-card.interface';
import { bytesToSize, getVisibilityObject } from '@shared/utils';
import { QuickSearchModel } from '@shared/models/search/quick-search.model';
import { GlobalService } from '@core-services/global.service';
import { AttributesService } from '@shared/services/attributes.service';
import { MeCertificateI } from '@shared/me-components/me-certificate/me-certificate.interface';
import { MeEmptyContentI } from '@shared/me-components/me-empty-content/me-empty-content-interface';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { CertificateAssociationModel } from '@shared/models/certificate-association/certificate-association.model';
import { DigitalProfileEditStoreService } from '@features/digital-profile-edit/digital-profile-edit-store.service';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { FileWebService } from '@shared/services/file.web-service';
import { MeLogoUploadI } from '@shared/me-components/me-logo-upload/me-logo-upload.interface';
import { FileUploadResponseModel } from '@shared/models/file-upload/file-upload-response.model';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { MeBasicAlertI } from '@shared/me-components/me-basic-alert/me-basic-alert.interface';
import { EntitiesUpdateI } from '@core-interfaces/entities-update.interface';
import { DigitalProfileEditWebService } from '@features/digital-profile-edit/digital-profile-edit.web-service';
import { FormControl, FormGroup } from '@angular/forms';
import { MeCoverImageUploadI } from '@shared/me-components/me-cover-image-upload/me-cover-image-upload.interface';
import { MeFileUploadTypes } from '@shared/me-file-upload-types';
import { AuthStoreService } from '@core-services/auth-store.service';
import { CompanyModel } from '@shared/models/company/company.model';
import { EntityI } from '@core-interfaces/entity.interface';
import { plainToClass } from 'class-transformer';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MeIntroductionMessageI } from '@shared/me-components/me-introduction-message/me-introduction-message.interface';
import { CertificateCreateEditDialogComponent } from '@features/collection-create-edit/certificate-create-edit-dialog/certificate-create-edit-dialog.component';
import { MePermissionTypes } from '@shared/me-permission-types';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-digital-profile-view',
  templateUrl: './digital-profile-view.component.html',
  styleUrls: ['./digital-profile-view.component.scss'],
  providers: [
    AttributesService,
    DigitalProfileEditStoreService,
    FileWebService,
    DigitalProfileEditWebService,
    CollectionsStoreService,
    CollectionsWebService
  ]
})
export class DigitalProfileViewComponent extends BaseLoggerComponent implements OnInit, AfterViewInit, OnDestroy {
  componentVersion: string = '0.0.1';

  i18nStrings: MeTranslationI = {};

  digitalProfile: DigitalProfileModel[] = [];
  company!: DigitalProfileModel;
  companyAttributes!: DigitalProfileAttributesModel;
  address!: DigitalProfileModel;
  languageLabel!: MeLabelI;
  isCompanyLoading: boolean = true;
  searchBarData!: MeSearchBarI;
  companyDomain: DomainModel | undefined;
  addressDomain: DomainModel | undefined;
  companyLayoutAttributes: AttributeModel<unknown>[] = [];
  companyLayoutGroups: LayoutGroupModel[] = [];
  companyGroups: LayoutGroupModel[] | undefined = [];
  companyInfoAttributeGroups: Array<AttributeGroupModel> = new Array<AttributeGroupModel>();
  addressAttributes: AttributeModel<unknown>[] = [];
  addressLayoutGroups: LayoutGroupModel[] = [];
  addressGroups: AttributeGroupModel = new AttributeGroupModel();
  collectionCardItems: MeNewCollectionCardI[] = [];
  certificateCardItems: MeCertificateI[] = [];
  trackForBottomReach: boolean = false;
  openTabsIndex: number = 0;
  sustainabilityCount: number = 0;
  collectionSearchKeyword: string | undefined = '';
  emptyCertificateContent!: MeEmptyContentI;
  introMsgData!: MeIntroductionMessageI;

  uploadLogoData!: MeLogoUploadI;
  uploadCoverImageData!: MeCoverImageUploadI;
  companyImageForm!: FormGroup;

  firstTabGroup: string = 'companyOverview';

  private readonly NUMBER_OF_ITEMS_ON_PAGE = 50;

  isSearch: boolean = false;

  // Certificate permissions
  isCertificateView: boolean = false;
  isCertificateCreate: boolean = false;
  isCertificateEdit: boolean = false;
  isCertificateDelete: boolean = false;

  // Company edit permissions
  isCompanyEdit: boolean = false;

  @ViewChild('editButton', { read: TemplateRef }) editButton!: TemplateRef<Component>;

  private _getI18nStringsSubscription!: Subscription;
  private _getDefinitionsDataLoadedSubscription!: Subscription;
  private _getCompanySubscription!: Subscription;
  private _getAllCollectionsSubscription!: Subscription;
  private _getCollectionDataForCollectionCardSubscription!: Subscription;
  private _searchCollectionSubscription!: Subscription;
  private _getAllCertificatesSubscription!: Subscription;
  private _getCertificateDataForCertificateCardSubscription!: Subscription;
  private _sweetAletSubscription!: Subscription;
  private _removeCertificateSubscription!: Subscription;
  private _getDocumentSubscription!: Subscription;
  private _searchcertificateSubscription!: Subscription;
  private _uploadImgSubscription!: Subscription;
  private _companyUpdateSubscription!: Subscription;

  constructor(
    injector: Injector,
    private router: Router,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private translate: MeTranslationService,
    private webService: DigitalProfileWebService,
    private digitalProfileStoreService: DigitalProfileStoreService,
    private definitionsStoreService: DefinitionsStoreService,
    private attributesService: AttributesService,
    private collectionsWebService: CollectionsWebService,
    private collectionsStoreService: CollectionsStoreService,
    private globalService: GlobalService,
    private meSweetAlertService: MeSweetAlertService,
    private basicAlertService: MeBasicAlertService,
    private digitalProfileEditStoreService: DigitalProfileEditStoreService,
    private fileWebService: FileWebService,
    private digitalProfileEditWebService: DigitalProfileEditWebService,
    private authStoreService: AuthStoreService,
    private matDialog: MatDialog
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isCertificateView = this.authStoreService.isAllowed(MePermissionTypes.CERTIFICATE_VIEW);
    this.isCertificateCreate = this.authStoreService.isAllowed(MePermissionTypes.COMPANY_MODIFY);
    this.isCertificateEdit = this.authStoreService.isAllowed(MePermissionTypes.COMPANY_MODIFY);
    this.isCertificateDelete = this.authStoreService.isAllowed(MePermissionTypes.COMPANY_MODIFY);
    this.isCompanyEdit = this.authStoreService.isAllowed(MePermissionTypes.COMPANY_MODIFY);

    // subscribe to translations
    this._getI18nStringsSubscription = this.translate.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;
    });

    this._getDefinitionsDataLoadedSubscription = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.companyDomain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL);
        this.addressDomain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_ADDRESS);
        this.getCollections();
        this.prepareCollectionCardData();
        this.getCertificatesList();
        this.prepareCertificatesCardData();
        this.initializeForms();
        if (this.digitalProfileStoreService.digitalprofile.length > 0) {
          this.digitalProfile = this.digitalProfileStoreService.digitalprofile;
          this.initializeView();
          this.isCompanyLoading = false;
        } else {
          this.getdigitalProfile();
        }
      }
    });

    this.setData();
  }

  initializeForms(): void {
    this.companyImageForm = new FormGroup({
      logo: new FormControl(''),
      coverImage: new FormControl('')
    });
  }

  ngAfterViewInit(): void {
    if (this.isCompanyEdit) {
      this.breadcrumbSupportService.breadcrumbActions = [
        {
          button: {
            id: '1',
            data: { color: 'secondary', type: 'regular' },
            text: this.i18nStrings.edit,
            content: this.editButton
          },
          callBack: () => {
            this.router.navigate(['digital-profile/edit']);
          }
        }
      ];
    }
  }

  btnEventHandler(): void {
    window.open('https://5883447.hs-sites.com/knowledge/setting-up-digital-profile-suppliers', '_blank');
  }

  getdigitalProfile(): void {
    this.globalService.activateLoader();
    this._getCompanySubscription = this.webService
      .getCompnay()
      .pipe(
        finalize(() => {
          this.globalService.deactivateLoader();
          this.isCompanyLoading = false;
        })
      )
      .subscribe((val: ArrayResponseI<DigitalProfileModel>) => {
        this.digitalProfile = val.entities;
        this.digitalProfileStoreService.digitalprofile = val.entities;
        this.initializeView();
      });
  }

  initializeView(): void {
    this.company = this.digitalProfileStoreService.getDigitalProfile(ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL);
    this.companyAttributes = this.company.attributes;
    if (this.companyAttributes.logo && this.companyImageForm) {
      this.companyImageForm.controls.logo.setValue(this.companyAttributes.logo);
    }
    if (this.companyAttributes.coverImage && this.companyImageForm) {
      this.companyImageForm.controls.coverImage.setValue(this.companyAttributes.coverImage);
    }
    this.address = this.digitalProfileStoreService.getDigitalProfile(ME_DOMAIN_APPLICATION_ADDRESS);

    if (this.addressDomain) {
      this.addressAttributes = this.attributesService.transformAttributesArray(this.addressDomain.attributes);
      this.addressLayoutGroups = this.attributesService.getLayoutGroups(this.addressDomain.layouts, MeAttributeLayoutTypes.VIEW);
      for (const group of this.addressLayoutGroups) {
        this.addressGroups = this.attributesService.getAttributesLayoutByGroup(group, this.addressAttributes, this.address.attributes);
      }
      this.company.attributes.address = [];
      let addressValues = '';
      this.addressGroups = this.attributesService.convertAttributesForDisplaying([this.addressGroups], this.addressDomain)[0];
      for (const item of this.addressGroups.attributes) {
        addressValues = addressValues === '' ? item.convertedValue : `${addressValues}, ${item.convertedValue}`;
      }
      this.company.attributes.address.push(addressValues);
    }

    if (this.companyDomain) {
      this.companyLayoutAttributes = this.attributesService.transformAttributesArray(this.companyDomain.attributes);
      this.companyLayoutGroups = this.attributesService.getLayoutGroups(this.companyDomain.layouts, MeAttributeLayoutTypes.VIEW);
      this.companyGroups = this.attributesService.getLayoutGroupsExceptGeneral(this.companyLayoutGroups, this.firstTabGroup);
      if (this.companyGroups) {
        for (const group of this.companyGroups) {
          let attribute = this.attributesService.getAttributesLayoutByGroup(group, this.companyLayoutAttributes, this.companyAttributes);
          attribute = this.attributesService.convertAttributesForDisplaying([attribute], this.companyDomain)[0];
          attribute = this.handleLanguageCapabilities(attribute);
          this.companyInfoAttributeGroups.push(attribute);
        }
      }
    }

    if (this.definitionsStoreService.logoProperties) {
      const splitString = this.definitionsStoreService.logoProperties.supportedFiles.split(',');
      this.uploadLogoData = {
        thumbnail: this.companyAttributes.logo ? MARKETPLACE_BASE_API_URL + '/files/download/' + this.companyAttributes.logo : '',
        acceptPhotoExtensions: splitString ? `.${splitString.join(',.')}` : '',
        // showProgress: false,
        backgroundimage: '/assets/images/company-no-image.svg',
        maxUploadSize: bytesToSize(this.definitionsStoreService.logoProperties.maxSize),
        dimensions: `${this.definitionsStoreService.logoProperties.minWidth} x ${this.definitionsStoreService.logoProperties.minHeight} px`,
        disabled: !this.isCompanyEdit
      };
    }

    if (this.definitionsStoreService.coverImageProperties) {
      const splitString = this.definitionsStoreService.coverImageProperties.supportedFiles.split(',');
      this.uploadCoverImageData = {
        thumbnail: this.companyAttributes.coverImage
          ? MARKETPLACE_BASE_API_URL + '/files/download/' + this.companyAttributes.coverImage
          : '',
        acceptPhotoExtensions: splitString ? `.${splitString.join(',.')}` : '',
        backgroundimage: '/assets/images/cover-no-image.png',
        maxUploadSize: bytesToSize(this.definitionsStoreService.coverImageProperties.maxSize),
        dimensions: `${this.definitionsStoreService.coverImageProperties.minWidth} x ${this.definitionsStoreService.coverImageProperties.minHeight} px`,
        disabled: !this.isCompanyEdit
      };
    }
  }

  handleLanguageCapabilities(attribute: AttributeGroupModel): AttributeGroupModel {
    const attr = attribute.attributes.filter((x) => x.internalName === 'languageCapabilities');
    attr.forEach((element) => {
      element.value = element.convertedValue.split(',').map((item) => {
        return item.trim();
      });
    });
    return attribute;
  }

  handleImageFormData(recievedFile: File, type: string): void {
    const formData = new FormData();
    formData.append(
      'meta',
      JSON.stringify({
        type: type === 'logo' ? MeFileUploadTypes.logo : MeFileUploadTypes.coverImage
      })
    );
    formData.append('file', recievedFile, recievedFile.name);
    this._uploadImgSubscription = this.fileWebService.uploadFile(formData, FileUploadResponseModel).subscribe(
      (response) => {
        if (type === 'logo') {
          this.uploadLogoData = {
            ...this.uploadLogoData,
            thumbnail: `${MARKETPLACE_BASE_API_URL}/files/download/${response.oid}`,
            showProgress: false
          };
        } else {
          this.uploadCoverImageData = {
            ...this.uploadCoverImageData,
            thumbnail: `${MARKETPLACE_BASE_API_URL}/files/download/${response.oid}`,
            showProgress: false
          };
        }
        if (response.oid) {
          this.updateCompanyImg(response.oid, type);
        }
      },
      (error: ErrorResponseI) => {
        this.uploadLogoData = { ...this.uploadLogoData };
        this.uploadCoverImageData = { ...this.uploadCoverImageData };
        const basicAlertData: MeBasicAlertI = {
          mode: 'error',
          title: error.error.title,
          content: error.error.details
        };
        this.basicAlertService.openBasicAlert(basicAlertData);
      }
    );
  }

  private updateCompanyImg(id: string, type: string): void {
    const companyUpdate: EntitiesUpdateI = {
      entities: []
    };
    if (type === 'logo') {
      this.companyImageForm.controls.logo.setValue(id);
    } else {
      this.companyImageForm.controls.coverImage.setValue(id);
    }
    const companyentity = {
      domainName: this.company ? this.company.domainName : ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL,
      attributes: this.companyImageForm.value
    };
    companyUpdate.entities.push(companyentity);
    this._companyUpdateSubscription = this.digitalProfileEditWebService.updateCompanyImage(companyUpdate).subscribe((company) => {
      const basicAlertData: MeBasicAlertI = {
        mode: 'success',
        title: type === 'logo' ? this.i18nStrings.logoUpdate : this.i18nStrings.coverImageUpdate,
        content: type === 'logo' ? this.i18nStrings.logoUpdateSuccessfully : this.i18nStrings.coverImageUpdateSuccessfully
      };
      this.basicAlertService.openBasicAlert(basicAlertData);
      const updatedCompany: EntityI | undefined = company.entities.find((entity: EntityI) => {
        return entity.domainName === this.company.domainName;
      });
      if (updatedCompany) {
        this.authStoreService.company = plainToClass(CompanyModel, updatedCompany);
      }
    });
  }

  getCollections(skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.globalService.activateLoader();
    this._getAllCollectionsSubscription = this.collectionsWebService
      .getMeCollections(skip, top)
      .pipe(
        finalize(() => {
          this.globalService.deactivateLoader();
        })
      )
      .subscribe((response: ArrayResponseI<CollectionModel>) => {
        this.handleCollectionsResponse(response, append, false);
      });
  }

  private handleCollectionsResponse(response: ArrayResponseI<CollectionModel>, append: boolean = false, isSearch: boolean = false): void {
    this.collectionsStoreService.collectionNextId = response.nextID;
    if (append) {
      this.collectionsStoreService.appendCollectionEntities(response.entities);
    } else {
      this.collectionsStoreService.collectionEntities = response.entities;
      this.isSearch = isSearch;
      this.trackForBottomReach = true;
    }
    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.trackForBottomReach = false;
    }
  }

  prepareCollectionCardData(): void {
    this._getCollectionDataForCollectionCardSubscription = this.collectionsStoreService.collectionEntities$.subscribe(
      (data: CollectionModel[]) => {
        if (data) {
          const collectionStates: StateModel[] = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_COLLECTION).states;
          this.collectionCardItems = data.map((collection: CollectionModel) => {
            return {
              selectionEnabled: false,
              selected: false,
              oid: collection.oid,
              collectionName: collection.attributes.name,
              numberOfMaterials: collection.numberOfMaterials,
              numberOfCustomers: collection.numberOfCustomers,
              numberOfTeams: collection.numberOfTeams,
              contextMenu: {
                contextMenuItems: [
                  {
                    key: 'view',
                    value: this.i18nStrings.view
                  }
                ]
              },
              thumbnail: collection.attributes.thumbnail
                ? `${MARKETPLACE_BASE_API_URL}/files/download/${collection.attributes.thumbnail}`
                : '',
              supplierName: collection.attributesExtended.ownerRef,
              supplierOid: collection.attributes.ownerRef,
              visibility: getVisibilityObject(
                collection.state,
                collectionStates.find((cs) => cs.internalName === collection.state)?.stateType
              )
            };
          });
        }
      }
    );
  }

  handleItemClick(card: MeNewCollectionCardI): void {
    this.router.navigate(['collections', 'view', card.oid]);
  }

  customEvent(eventName: string, payload?: string, item?: MeNewCollectionCardI): void {
    if (eventName === 'contextMenuItemClick') {
      switch (payload) {
        case 'view':
          this.router.navigate(['collections', 'view', item?.oid]);
          break;
        default:
          break;
      }
    }
  }

  bottomReachedHandler(): void {
    if (this.openTabsIndex === 2 && this.trackForBottomReach) {
      if (this.isSearch) {
        this.collectionHandleSearch(
          this.collectionSearchKeyword || '',
          this.collectionsStoreService.collectionNextId,
          this.NUMBER_OF_ITEMS_ON_PAGE,
          true
        );
      } else {
        this.getCollections(this.collectionsStoreService.collectionNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
      }
    }
  }

  setData(): void {
    this.searchBarData = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: ''
      },
      usersGroupsSelect: false,
      forceShowButton: false,
      addButton: this.isCertificateCreate
        ? {
            placeholder: this.i18nStrings.addCertificate
          }
        : undefined
    };

    this.languageLabel = { type: 'inversed', color: 'primary', margin: 'none' };
    this.emptyCertificateContent = {
      title: this.i18nStrings.addYourFirstCertificate,
      comment: this.i18nStrings.addYourFirstCertificateDescription,
      buttonText: this.i18nStrings.add,
      emptyIconName: 'empty-certificates'
    };

    this.introMsgData = {
      content:
        'Use your company profile to introduce yourself to prospective customers and partners. Most company profile info is public, but certain information (such as Contact info) is only visible to approved community members.',
      buttonText: this.i18nStrings.learnMore
    };
  }

  goToEditProfile(): void {
    this.router.navigate(['digital-profile/edit']);
  }

  // certificates search handle
  certificateHandleSearch(searchValue: string): void {
    if (searchValue && searchValue.length >= 3) {
      const searchCriteria = {
        criteriaQuick: searchValue
      };
      this.globalService.activateLoader();
      this._searchcertificateSubscription = this.webService
        .searchCertificateAssociation(searchCriteria)
        .pipe(
          finalize(() => {
            this.globalService.deactivateLoader();
          })
        )
        .subscribe((response: ArrayResponseI<CertificateAssociationModel>) => {
          this.digitalProfileEditStoreService.certificateAssociation = response.entities;
        });
    } else if (searchValue && searchValue.length < 3) {
      return;
    } else {
      this.getCertificatesList();
    }
  }

  // collections search handle
  collectionHandleSearch(searchValue: string, skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.collectionSearchKeyword = searchValue;
    if (this.collectionSearchKeyword && this.collectionSearchKeyword.length >= 3) {
      const searchCriteria: QuickSearchModel = {
        domain: ME_DOMAIN_APPLICATION_COLLECTION,
        criteriaQuick: this.collectionSearchKeyword,
        orderByAttr: 'name',
        orderByDir: 'ASC'
      };
      this.globalService.activateLoader();
      this._searchCollectionSubscription = this.collectionsWebService
        .searchCollections(searchCriteria, skip, top)
        .pipe(
          finalize(() => {
            this.globalService.deactivateLoader();
          })
        )
        .subscribe((response: ArrayResponseI<CollectionModel>) => {
          this.handleCollectionsResponse(response, append, true);
        });
    } else if (this.collectionSearchKeyword && this.collectionSearchKeyword.length < 3) {
      return;
    } else {
      this.getCollections();
    }
  }

  // Get open tab index
  tabChanged(e: { index: number }): void {
    this.openTabsIndex = e.index;
  }

  getCertificatesList(): void {
    this.globalService.activateLoader();
    this._getAllCertificatesSubscription = this.webService
      .getCertificateAssociation()
      .pipe(
        finalize(() => {
          this.globalService.deactivateLoader();
        })
      )
      .subscribe((response: ArrayResponseI<CertificateAssociationModel>) => {
        this.digitalProfileEditStoreService.certificateAssociation = response.entities;
      });
  }

  prepareCertificatesCardData(): void {
    this._getCertificateDataForCertificateCardSubscription = this.digitalProfileEditStoreService.certificateassociation$.subscribe(
      (data) => {
        if (data) {
          this.certificateCardItems = data.map((certificate: CertificateAssociationModel) => {
            if (certificate.certificate.attributes.group === 'sustainability') {
              this.sustainabilityCount++;
            }
            const certificateItem: MeCertificateI = {
              oid: certificate.oid,
              certificateOid: certificate.certificate.oid,
              name: certificate.certificate.attributes.name,
              logoDefault: `${MARKETPLACE_BASE_API_URL}/files/download/${certificate.certificate.attributes.logoDefault}`,
              link: certificate.certificate.attributes.link,
              description: certificate.certificate.attributes.description,
              group: certificate.certificate.attributes.group,
              mediaFiles: this.i18nStrings.mediaFiles,
              issuedDate: certificate.attributes.issuedDate,
              expirationDate: certificate.attributes.expirationDate,
              certDocument: '',
              certDocumentExtension: '',
              contextMenu: {
                contextMenuItems: []
              },
              active: certificate.active,
              urn: certificate.attributes.urn || '',
              companyCertificateName: certificate.attributes.name || ''
            };

            if (this.isCertificateEdit) {
              certificateItem.contextMenu.contextMenuItems.push({
                key: 'edit',
                value: this.i18nStrings.edit
              });
            }

            if (this.isCertificateDelete) {
              certificateItem.contextMenu.contextMenuItems.push({
                key: 'remove',
                value: this.i18nStrings.remove
              });
            }

            if (certificate.attributes.certDocument) {
              this.getDocument(certificate.attributes.certDocument).then((document: MeDocumentModel) => {
                certificateItem.certDocument = `${MARKETPLACE_BASE_API_URL}/files/download/${document.attributes.primaryContent}`;
                certificateItem.certDocumentExtension = document.attributesExtended.extension;
                return certificateItem;
              });
            }
            return certificateItem;
          });
        }
      }
    );
  }

  getDocument(documentId: string): Promise<MeDocumentModel> {
    return new Promise((resolve, reject) => {
      this._getDocumentSubscription = this.webService.getDocument(documentId).subscribe((res: MeDocumentModel) => {
        res ? resolve(res) : reject();
      });
    });
  }

  customCertificateEvent(eventName: string, payload?: string, item?: MeCertificateI): void {
    if (eventName === 'selectedContextMenu') {
      switch (payload) {
        case 'edit':
          this.addEditCertificate(item);
          break;
        case 'remove':
          if (item?.oid) {
            this.removeCertificate(item.oid);
          }
          break;
        default:
          break;
      }
    }
  }

  public addEditCertificate(item: MeCertificateI | undefined): void {
    const certificateModel: CertificateAssociationModel | undefined = this.digitalProfileEditStoreService.certificateAssociation?.find(
      (model: CertificateAssociationModel) => {
        return model.oid === item?.oid;
      }
    );

    const config: MatDialogConfig = new MatDialogConfig();
    config.data = {
      certificateModel
    };

    this.matDialog
      .open(CertificateCreateEditDialogComponent, config)
      .afterClosed()
      .subscribe((updated: boolean) => {
        if (updated) {
          this.getCertificatesList();
        }
      });
  }

  removeCertificate(oid: string): void {
    this._sweetAletSubscription = this.meSweetAlertService.getDataBackFromSweetAlert().subscribe((data: MeSweetAlertI) => {
      if (data.confirmed) {
        this._removeCertificateSubscription = this.webService.removeCertificate(oid).subscribe(() => {
          this.basicAlertService.openBasicAlert({
            mode: 'success',
            content: this.i18nStrings.removeCertificateSuccess,
            title: this.i18nStrings.success
          });
          this.getCertificatesList();
        });
      }
      this._sweetAletSubscription.unsubscribe();
    });

    this.meSweetAlertService.openMeSweetAlert({
      icon: 'alert-octagon',
      mode: 'danger',
      message: this.i18nStrings.removeCertificateDescription,
      title: this.i18nStrings.removeCertificate,
      type: {
        name: MeSweetAlertTypeEnum.submit,
        buttons: {
          submit: this.i18nStrings.remove,
          cancel: this.i18nStrings.cancel
        }
      }
    });
  }

  ngOnDestroy(): object {
    if (this._getI18nStringsSubscription) {
      this._getI18nStringsSubscription.unsubscribe();
    }
    if (this._getDefinitionsDataLoadedSubscription) {
      this._getDefinitionsDataLoadedSubscription.unsubscribe();
    }
    if (this._getCompanySubscription) {
      this._getCompanySubscription.unsubscribe();
    }
    if (this._getAllCollectionsSubscription) {
      this._getAllCollectionsSubscription.unsubscribe();
    }
    if (this._getCollectionDataForCollectionCardSubscription) {
      this._getCollectionDataForCollectionCardSubscription.unsubscribe();
    }
    if (this._searchCollectionSubscription) {
      this._searchCollectionSubscription.unsubscribe();
    }
    if (this._getAllCertificatesSubscription) {
      this._getAllCertificatesSubscription.unsubscribe();
    }
    if (this._getCertificateDataForCertificateCardSubscription) {
      this._getCertificateDataForCertificateCardSubscription.unsubscribe();
    }
    if (this._removeCertificateSubscription) {
      this._removeCertificateSubscription.unsubscribe();
    }
    if (this._getDocumentSubscription) {
      this._getDocumentSubscription.unsubscribe();
    }
    if (this._searchcertificateSubscription) {
      this._searchcertificateSubscription.unsubscribe();
    }
    if (this._uploadImgSubscription) {
      this._uploadImgSubscription.unsubscribe();
    }
    if (this._companyUpdateSubscription) {
      this._companyUpdateSubscription.unsubscribe();
    }
    return super.ngOnDestroy();
  }
}
