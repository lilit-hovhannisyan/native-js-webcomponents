import { Component, OnInit } from '@angular/core';
import { IStevedoreDamageReport, StevedoreDamageReportStatus } from 'src/app/core/models/resources/IStevedoreDamageReport';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { StevedoreDamageReportRepositoryService } from 'src/app/core/services/repositories/stevedore-damage-report-repository.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { StevedoreDamageReportMainService } from 'src/app/core/services/mainServices/stevedore-damage-report-main.service';
import { IStevedoreDamageReportRow } from 'src/app/core/models/resources/IStevedoreDamageReport';
import { map, tap } from 'rxjs/operators';
import {
  getStevedoreDamageReportGridConfig, IStevedoreDamageReportGridConfig
} from 'src/app/features/technical-management/components/misc/stevedore-damages/list-view/stevedore-report.config';
import { IWording } from 'src/app/core/models/resources/IWording';
import { BaseComponent } from 'src/app/core/abstracts/BaseComponent';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { Language } from 'src/app/core/models/language';
import * as moment from 'moment';
import { IVesselIdMap, ICharterNamesMap } from './vessels/vessels.component';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';
import { orderBy } from 'lodash';
import { VesselMainService } from '../../../../../../core/services/mainServices/vessel-main.service';
import { IVesselWithAccountings, VesselKey } from 'src/app/core/models/resources/IVessel';
import { convertToMap } from 'src/app/shared/helpers/general';


export interface IAlarmResubmission {
  title: IWording;
  key: string;
  className: string;
}

export enum ResubmissionKeys {
  'overdue' = 'overdue',
  'expiring' = 'expiring',
  'onTime' = 'onTime',
}

@Component({
  selector: 'nv-stevedore-damages-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
})
export class StevedoreDamagesListViewComponent extends BaseComponent implements OnInit {
  public gridConfig: IStevedoreDamageReportGridConfig;
  public dataSource$ = new BehaviorSubject<IStevedoreDamageReportRow[]>([]);
  private dataSource: IStevedoreDamageReportRow[] = [];
  private vesselChecked: IVesselIdMap = {};
  private checkedCharters: ICharterNamesMap = {};

  public resubmissionCounts = {
    [ResubmissionKeys.overdue]: 0,
    [ResubmissionKeys.expiring]: 0,
    [ResubmissionKeys.onTime]: 0,
  };

  public resubmissionRelativeFrequency = {
    [ResubmissionKeys.overdue]: 0,
    [ResubmissionKeys.expiring]: 0,
    [ResubmissionKeys.onTime]: 0,
  };

  public activeBars = {
    [ResubmissionKeys.overdue]: false,
    [ResubmissionKeys.expiring]: false,
    [ResubmissionKeys.onTime]: false,
  };

  public alarmResubmission: IAlarmResubmission[] = [
    { title: wording.technicalManagement.overdue, key: ResubmissionKeys.overdue, className: 'overdue' },
    { title: wording.technicalManagement.expiring, key: ResubmissionKeys.expiring, className: 'expiring' },
    { title: wording.technicalManagement.onTime, key: ResubmissionKeys.onTime, className: 'on-time' },
  ];

  public customTitle = '';
  private activeToolbarButtons = {
    [StevedoreDamageReportStatus.Cleared]: true,
    [StevedoreDamageReportStatus.InProgress]: true,
    [StevedoreDamageReportStatus.InvoiceCreated]: true,
    [StevedoreDamageReportStatus.RatingRequired]: true,
    [StevedoreDamageReportStatus.ReleaseRequired]: true
  };
  private language: Language;

  public totalCount = 0;
  private routeBackUrl = this.url(this.sitemap.technicalManagement.children.misc);
  public availableVesselIds: number[];
  public chartererNames: string[] = [];
  private vesselsMap: Map<VesselKey, IVesselWithAccountings>;

  constructor(
    public gridConfigService: GridConfigService,
    private stevedoreReportService: StevedoreDamageReportMainService,
    private stevedoreReportRepo: StevedoreDamageReportRepositoryService,
    baseComponentService: BaseComponentService,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
    private vesselMainService: VesselMainService,
  ) { super(baseComponentService); }

  public ngOnInit() {
    this.language = this.settingsService.language;
    this.stevedoreReportService.fetchAll({}).subscribe();
    combineLatest([
      this.vesselAccountingRepo.fetchAll({}),
      this.vesselMainService.fetchWithRolesOnly().pipe(tap(vessels => {
        this.vesselsMap = convertToMap<IVesselWithAccountings, VesselKey>(vessels);
      })),
      this.stevedoreReportService.getStream({}, this.filterByVesselId),
    ]).pipe(
      map(([vesselAccounings, vessels, stevedoreReports]) => {
        this.dataSource = this.stevedoreReportService.toRows(stevedoreReports)
          .map(report => {
            if (!report.vesselAccountingId
              && report.isSettlementWithoutAutomaticCharterReference
              && report.vesselId
            ) {
              const accountings = orderBy(
                vesselAccounings.filter(a => a.vesselId === report.vesselId) || [],
                [a => a.from],
                ['desc']
              );

              const vesselAccounting = accountings.length && accountings[0];
              return {
                ...report,
                vesselAccounting,
                vesselAccountingId: vesselAccounting.id,
              };
            } else {
              return report;
            }
          });
        return this.dataSource;
      }),
      tap(stevedoreReports => {
        this.dataSource$.next(stevedoreReports);
        this.calcAlarmResubmissions();
        this.availableVesselIds = null;
        this.availableVesselIds = [
          ...new Set(
            stevedoreReports
              .filter(s => {
                if (!s.vesselAccountingId) {
                  if (!this.chartererNames.includes(s.charterName)) {
                    this.chartererNames.push(s.charterName);
                  }
                  return false;
                }
                return true;
              }).map(s => s.vesselAccountingId)
          )
        ];
      })
    ).subscribe();

    const gridActions = {
      edit: (id: number) => this.router.navigate([this.router.url, id]),
      add: () => this.router.navigate([this.router.url, 'new']),
      delete: this.deleteClicked,
      select: this.statusSelect,
    };

    this.gridConfig = getStevedoreDamageReportGridConfig(
      gridActions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
    );
  }

  private getTitles(): string[] {
    const titles = [];
    if (this.activeBars[ResubmissionKeys.expiring]) {
      titles.push(this.wording.technicalManagement.expiring[this.language]);
    }
    if (this.activeBars[ResubmissionKeys.onTime]) {
      titles.push(this.wording.technicalManagement.onTime[this.language]);
    }
    if (this.activeBars[ResubmissionKeys.overdue]) {
      titles.push(this.wording.technicalManagement.overdue[this.language]);
    }
    return titles;
  }

  private deleteClicked = (stevedoreReport: IStevedoreDamageReport) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: null },
    }).subscribe(confirmed => confirmed && this.stevedoreReportRepo.delete(stevedoreReport.id, {}).subscribe());
  }

  public barClick(key: ResubmissionKeys) {
    this.activeBars[key] = !this.activeBars[key];
    this.filterObservations();
    this.customTitle = this.getTitles().reduce((prev, title) => {
      return prev ? `${prev} / ${title}` : title;
    }, '');
  }

  private filterObservations() {
    let activeToolbarBtnFilter = false; // filter button above the grid
    let activeBarsFound = false; // filter bars below the grid

    for (const i in this.activeBars) {
      if (this.activeBars[i]) {
        activeBarsFound = true;
        break;
      }
    }

    for (const i in this.activeToolbarButtons) {
      if (this.activeToolbarButtons[i]) {
        activeToolbarBtnFilter = true;
        break;
      }
    }

    if (!activeToolbarBtnFilter) {
      this.activateAllToolbarFilters();
    }


    const filteredByVesselsData = this.dataSource.filter(stevedoreDamageReportRow => {
      const barEnabled = this.hasEnabledBar(stevedoreDamageReportRow) || !activeBarsFound;
      const statusEnabled = this.activeToolbarButtons[stevedoreDamageReportRow.status];
      const vesselChecked = this.vesselChecked[stevedoreDamageReportRow.vesselAccountingId]
        && !!this.vesselChecked[stevedoreDamageReportRow.vesselAccountingId]
        || !!this.checkedCharters[stevedoreDamageReportRow.charterName];
      return statusEnabled
        && barEnabled
        && vesselChecked;
    });

    this.dataSource$.next(filteredByVesselsData);
  }

  private filterByVesselId = (report: IStevedoreDamageReport): boolean => {
    return this.vesselsMap.has(report.vesselId);
  }

  private hasEnabledBar(sdReport: IStevedoreDamageReportRow): boolean {
    const after3Weeks = moment().add(3, 'weeks');
    if (
      this.activeBars[ResubmissionKeys.expiring] &&
      moment(sdReport.resubmissionDate).isSameOrAfter(moment()) &&
      moment(sdReport.resubmissionDate).isSameOrBefore(after3Weeks)
    ) {
      return true;
    }

    if (
      this.activeBars[ResubmissionKeys.onTime] &&
      moment(sdReport.resubmissionDate).isAfter(after3Weeks)
    ) {
      return true;
    }

    if (
      this.activeBars[ResubmissionKeys.overdue] &&
      moment(sdReport.resubmissionDate).isBefore(moment())
    ) {
      return true;
    }

    return false;
  }

  private calcAlarmResubmissions() {
    this.resubmissionCounts = {
      [ResubmissionKeys.expiring]: 0,
      [ResubmissionKeys.onTime]: 0,
      [ResubmissionKeys.overdue]: 0
    };
    this.totalCount = 0;
    this.dataSource.forEach(i => {
      if (!i.resubmissionDate) {
        return;
      }
      ++this.totalCount;

      // https://navido.atlassian.net/browse/NAVIDO-1618 check the comments
      const after3Weeks = moment().add(3, 'weeks');

      if (moment(i.resubmissionDate).isSameOrAfter(moment()) && moment(i.resubmissionDate).isSameOrBefore(after3Weeks)) {
        ++this.resubmissionCounts[ResubmissionKeys.expiring];
      }

      if (moment(i.resubmissionDate).isAfter(after3Weeks)) {
        ++this.resubmissionCounts[ResubmissionKeys.onTime];
      }

      if (moment(i.resubmissionDate).isBefore(moment())) {
        ++this.resubmissionCounts[ResubmissionKeys.overdue];
      }
    });

    this.resubmissionRelativeFrequency = {
      [ResubmissionKeys.expiring]: +((this.resubmissionCounts[ResubmissionKeys.expiring]
        / this.totalCount) * 100).toFixed(2) || 0,
      [ResubmissionKeys.overdue]: +((this.resubmissionCounts[ResubmissionKeys.overdue]
        / this.totalCount) * 100).toFixed(2) || 0,
      [ResubmissionKeys.onTime]: +((this.resubmissionCounts[ResubmissionKeys.onTime]
        / this.totalCount) * 100).toFixed(2) || 0,
    };
  }

  public routeBack = () => this.router.navigate([this.routeBackUrl]);

  public statusSelect = (
    status: StevedoreDamageReportStatus,
    toggle = true
  ) => {
    let allToolbarBtnsActive = true;
    let toolbarBtns = [];
    // tslint:disable: forin
    for (const i in this.activeToolbarButtons) {
      allToolbarBtnsActive = allToolbarBtnsActive && this.activeToolbarButtons[i];
    }
    if (status == null) {
      allToolbarBtnsActive = false;
    }
    if (allToolbarBtnsActive) {
      for (const i in this.activeToolbarButtons) {
        this.activeToolbarButtons[i] = false;
      }
      this.activeToolbarButtons[status] = true;
    } else {
      if (toggle) {
        this.activeToolbarButtons[status] = !this.activeToolbarButtons[status];
      }
    }

    toolbarBtns = this.gridConfig.toolbarButtons.map(btn => {
      const buttonClasses = (btn.class || '').trim().split(' ');
      const classesSet = new Set(buttonClasses);

      if (this.activeToolbarButtons[btn.status]) {
        classesSet.delete('inactive');
      } else {
        classesSet.add('inactive');
      }

      return { ...btn, class: Array.from(classesSet).join(' ') };
    });

    this.gridConfig = {
      ...this.gridConfig,
      toolbarButtons: toolbarBtns,
    };
    this.filterObservations();
  }

  public onVesselChecked(checkedVessels: IVesselIdMap) {
    this.vesselChecked = checkedVessels;
    this.filterObservations();
  }

  public onCharterChecked(checkedCharters: ICharterNamesMap) {
    this.checkedCharters = checkedCharters;
    this.filterObservations();
  }

  private activateAllToolbarFilters() {
    this.activeToolbarButtons = {
      [StevedoreDamageReportStatus.Cleared]: true,
      [StevedoreDamageReportStatus.InProgress]: true,
      [StevedoreDamageReportStatus.InvoiceCreated]: true,
      [StevedoreDamageReportStatus.RatingRequired]: true,
      [StevedoreDamageReportStatus.ReleaseRequired]: true
    };
    this.statusSelect(null, false);
  }
}
