export interface ICacheTagMeta {
  name: string;
  creationDate: Date;
}

export interface ICacheItem<T> {
  /**
   * The value corresponding to this cache item's key, or undefined if not found.
   */
  value?: T;
  /**
   * The key string for this cache item.
   */
  key: string;
  /**
   * Confirms if the cache item lookup resulted in a cache hit.
   *
   * True if the request resulted in a cache hit. False otherwise.
   */
  isHit: boolean;
  /**
   * The period of time from the present after which the item is considered
   * expired.
   */
  expiresAt?: Date;
}
