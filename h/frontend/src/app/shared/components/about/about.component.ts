import { Component, OnInit, Input } from '@angular/core';
import { IGitVersion } from 'src/app/core/models/IGitVersion';
import { BaseComponent } from 'src/app/core/abstracts/BaseComponent';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { copyToClipboard } from 'src/app/core/helpers/general';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { NotificationType } from 'src/app/core/models/INotification';

@Component({
  selector: 'nv-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent extends BaseComponent implements OnInit {
  @Input() public closeModal: () => void;
  @Input() public version: IGitVersion;
  constructor(
    bcs: BaseComponentService,
    private notificationService: NotificationsService,
  ) {
    super(bcs);
  }

  public ngOnInit() { }

  public copy = (val: string) => {
    copyToClipboard(val);
    this.notificationService.notify(NotificationType.Success,
      this.wording.general.success,
      this.wording.general.copiedToClipboard,
    );
  }
}
