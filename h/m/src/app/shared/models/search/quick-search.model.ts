import { BaseSearchModel } from './search-base.model';

export class QuickSearchModel extends BaseSearchModel {
  criteriaQuick?: string = '';
}
