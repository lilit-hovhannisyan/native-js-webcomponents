import { Component, OnInit, OnDestroy } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { IDebitorCreditorRow } from 'src/app/core/models/resources/IDebitorCreditor';
import { takeUntil, map } from 'rxjs/operators';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { DebitorAndCreditorsHelperService } from './debitor-and-creditors.helper.service';
import { DebitorCreditorMainService } from 'src/app/core/services/mainServices/debitor-creditor-main.service';

@Component({
  templateUrl: './debitor-and-creditor-detial-view.component.html',
  styleUrls: ['./debitor-and-creditor-detial-view.component.scss'],
  providers: [DebitorAndCreditorsHelperService],
})
export class DebitorAndCreditorDetialViewComponent implements OnInit, OnDestroy {

  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public debitorCreditor: IDebitorCreditorRow;
  public id: string;
  public title: string;
  public sitemap = sitemap;
  public wording = wording;

  public get isCreationMode() {
    return this.route.snapshot.params.id === 'new';
  }

  constructor(
    private debitorAndCreditorsHelperService: DebitorAndCreditorsHelperService,
    private debitorCreditorService: DebitorCreditorMainService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  public ngOnInit() {
    const idParam = this.route.snapshot.params.id;

    if (this.isCreationMode) {
      this.debitorAndCreditorsHelperService.generateTitle();
    } else {
      const id = parseInt(idParam, 10);

      this.debitorCreditorService.fetchOne(id)
        .pipe(
          takeUntil(this.componentDestroyed$),
          map((db) => this.debitorCreditorService.toRow(db))
        )
        .subscribe(debitorCreditor => {
          this.debitorAndCreditorsHelperService.generateTitle(
            debitorCreditor,
            [debitorCreditor.company],
          );
        });
    }

    this.debitorAndCreditorsHelperService.editViewTitle$.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(title => {
      this.title = title;
    });
  }

  public routeBack = () =>
    this.router.navigate([
      url(sitemap.accounting.children.booking.children.debitorCreditors)
    ])

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public tabDisabler = (node: SitemapNode) => {
    const basicNode = sitemap.accounting.children.booking.children.debitorCreditors.children.id.children.basic;
    return this.isCreationMode ? node === basicNode : true;
  }
}
