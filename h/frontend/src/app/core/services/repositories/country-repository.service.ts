import { TTL_LONG } from './../../../shared/services/resource-tracker.service';
import { CountryKey } from './../../models/resources/ICountry';
import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { ICountry, ICountryDTO, ICountryCreate } from '../../models/resources/ICountry';
import { RepositoryService } from '../repository.service';
import { getLabelKeyByLanguage, isoAlpha2ToFlagEmoji } from '../../helpers/general';
import { SettingsService } from '../settings.service';

/**
 * By default backend returns only 100 countries, we use 250 to get all countries
 * according to Google there are 195 countries in the world by the time of this commit
 * and we have 238 countries in our db
 * I'm setting it to 250 just in case, it'd better if doing GET returned all countries
 * if no limit is specified
 */
export const MAX_COUNTRIES = 250;
export interface CountryRepoParams extends RepoParams<ICountry> {
  queryParams?: {
    limit?: number;
    offset?: number;
  };
}

@Injectable({
  providedIn: 'root'
})
export class CountryRepositoryService extends AbstractRepository<
ICountry,
ICountryDTO,
ICountryCreate,
CountryRepoParams,
CountryKey
>  {
  protected resourceTTL = TTL_LONG;
  protected useCachePerDefault = true;
  protected resourceType = 'Country';

  public sortAsce = true;
  public sortKey = getLabelKeyByLanguage(this.settingsService.language);

  constructor(repositoryService: RepositoryService,
    private settingsService: SettingsService) {
    super(repositoryService);
  }

  protected postProcess(item: ICountryDTO): ICountry {
    return  {
      ...item,
      countryEmoji: isoAlpha2ToFlagEmoji(item.isoAlpha2),
      countryFlagPath: `assets/country-flags/${item.isoAlpha3.toLowerCase()}.svg`,
    };
  }

  public getBaseUrl(params: CountryRepoParams): string {
    return '/api/countries/';
  }
}
