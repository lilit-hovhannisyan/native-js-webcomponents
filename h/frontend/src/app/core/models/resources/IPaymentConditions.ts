import { IWithLabels } from './IWithLabels';
import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';
import { IWording } from './IWording';

interface ISharedProps extends IWithLabels {
  daysDiscountLevel1: number;
  daysDiscountLevel2: number;
  daysDiscountLevel3: number;
  percentageDiscountLevel1: number;
  percentageDiscountLevel2: number;
  percentageDiscountLevel3: number;
  net: number;
  customerNo: number;
  termsAgreed: string;
  debitorCreditorId: number;
}

export interface IPaymentConditionBase extends ISharedProps, IResource<number> { }

export interface IPaymentConditionCreate extends ISharedProps { }

export interface IPaymentConditionDTO extends IPaymentConditionBase { }

export interface IPaymentCondition extends IPaymentConditionDTO, IWithLabel { }

export interface IPaymentConditionLevel {
  level: number;
  daysDiscount: number;
  percentageDiscount: number;
}

export interface IPaymentStepRow {
  step: IWording;
  percentage: number;
  days: number;
  paymentDate: string; // ISO String
  paymentDateLabel?: string; // Current Format
}

