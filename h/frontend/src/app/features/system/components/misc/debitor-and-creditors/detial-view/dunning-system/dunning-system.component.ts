import { Component, OnInit, OnDestroy, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { IDunningDays, IDunningDaysCreate } from 'src/app/core/models/resources/IDunningDays';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { DunningDaysRepositoryService } from 'src/app/core/services/repositories/dunning-days-repository.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { catchError, startWith, map, distinctUntilChanged } from 'rxjs/operators';
import { Validators } from 'src/app/core/classes/validators';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { forkJoin, of, Subscription } from 'rxjs';
import { ICurrency, CurrencyKey } from 'src/app/core/models/resources/ICurrency';
import { HttpErrorResponse } from '@angular/common/http';
import { inOrder } from './dunning-levels-validator';
import { DebitorCreditorRepositoryService } from 'src/app/core/services/repositories/debitor-creditor-repository.service';
import { IDebitorCreditor } from 'src/app/core/models/resources/IDebitorCreditor';
import { NvGridConfig } from 'nv-grid';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/currencies.config';
@Component({
  selector: 'nv-dunning-system',
  templateUrl: './dunning-system.component.html',
  styleUrls: ['./dunning-system.component.scss']
})
export class DebitorCreditorDunningSystemTabComponent extends ResourceDetailView<
IDunningDays,
IDunningDaysCreate
> implements OnInit, OnDestroy {
  public routeBackUrl = this.url(sitemap.accounting.children.booking.children.debitorCreditors);
  public currencies: ICurrency[];
  public dunningLevelErrorKey = 'dunningLevelError';

  protected resourceDefault = {
    isActivated: false
  } as IDunningDays;
  protected repoParams = {};
  private subscriptions: Subscription[] = [];
  private debitorCreditor: IDebitorCreditor;
  public gridConfig: NvGridConfig = getGridConfig({}, () => false);
  public formElement: ElementRef;

  constructor(
    baseComponentService: BaseComponentService,
    private dunningDaysRepo: DunningDaysRepositoryService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private el: ElementRef,
    private currencyRepo: CurrencyRepositoryService,
    private debitorCreditorService: DebitorCreditorRepositoryService,
  ) {
    super(dunningDaysRepo, baseComponentService);
  }

  public ngOnInit() {
    const { id } = this.route.parent.snapshot.params;
    this.resourceDefault = { ...this.resourceDefault, debitorCreditorId: id };
    forkJoin([
      this.debitorCreditorService.fetchOne(id, {}),
      this.fetchDunningDaysForDebitorCreditor(id),
      this.currencyRepo.fetchAll({}),
    ]).subscribe(([debitorCreditor, dunnningDays]) => {
      this.debitorCreditor = debitorCreditor;
      this.loadCurrencies();
      this.initializeResource(dunnningDays);
    });
  }

  public getTitle = () => this.debitorCreditor.no.toString();

  private loadCurrencies(currencyId?: CurrencyKey) {
    this.currencyRepo.getStream()
      .pipe(map(currencies => this.currencyRepo.filterActiveOnly(currencies, currencyId)))
      .subscribe(currencies => this.currencies = currencies);
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  public init(dd: IDunningDays = this.resourceDefault) {
    this.initForm(this.fb.group({
      daysLevel1: [dd.daysLevel1, [Validators.number, Validators.integerBetween(1, 91)]],
      daysLevel2: [dd.daysLevel2, [Validators.number, Validators.integerBetween(1, 91)]],
      daysLevel3: [dd.daysLevel3, [Validators.number, Validators.integerBetween(1, 91)]],
      daysCollectionProcedure: [dd.daysCollectionProcedure, [Validators.number, Validators.integerBetween(1, 91)]],
      lateFee: [dd.lateFee, [Validators.positiveNumber]],
      currencyId: [dd.currencyId],
      debitorCreditorId: [dd.debitorCreditorId],
      isActivated: [dd.isActivated],
    }, {
      validator: [inOrder('daysLevel1', 'daysLevel2', 'daysLevel3', 'daysCollectionProcedure', this.dunningLevelErrorKey)]
    }));
    const currencyCtrl = this.form.get('currencyId');

    this.subscriptions.push(currencyCtrl.valueChanges
      .pipe(
        startWith(currencyCtrl.value),
        distinctUntilChanged(),
      )
      .subscribe(v => this.loadCurrencies(v)));

    this.subscriptions.push(
      this.form.get('daysCollectionProcedure').valueChanges
        .pipe(startWith(this.form.value.daysCollectionProcedure))
        .subscribe(val => {
          const lateFee = this.form.get('lateFee');
          const currencyId = this.form.get('currencyId');
          if (val) {
            lateFee.setValidators([Validators.required, Validators.positiveNumber]);
            currencyId.setValidators([Validators.required]);
          } else {
            lateFee.setValidators([Validators.positiveNumber]);
            currencyId.clearValidators();
          }
          lateFee.updateValueAndValidity();
          currencyId.updateValueAndValidity();
        }));
  }

  private fetchDunningDaysForDebitorCreditor(debitorCreditorId: number) {
    return this.dunningDaysRepo.fetchDunningDaysOfDebitorCreditor({ debitorCreditorId, errorHandler: () => true })
      .pipe(catchError(this.errorHandler));
  }

  public submit() {
    if (this.form.invalid) {
      return;
    }
    const { value } = this.form;

    this.resource && this.resource.id
      ? this.updateResource({ ...value, id: this.resource.id }).subscribe()
      : this.createResource(value, false).subscribe();
  }

  private errorHandler = (err: HttpErrorResponse) => {
    if (err && err.status === 404) {
      return of(this.resourceDefault);
    }
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
