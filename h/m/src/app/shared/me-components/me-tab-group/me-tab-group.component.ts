import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnDestroy, Output } from '@angular/core';
import { BaseLoggerComponent } from '@features/tracking-system';

import { MeTabGroupI } from './me-tab-group.interface';

@Component({
  selector: 'me-tab-group',
  templateUrl: './me-tab-group.component.html',
  styleUrls: ['./me-tab-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeTabGroupComponent extends BaseLoggerComponent implements OnDestroy {
  componentVersion: string = '0.0.1';

  @Input() dataModel!: MeTabGroupI;
  @Output() selectedTabChanged: EventEmitter<number> = new EventEmitter();

  constructor(injector: Injector) {
    super(injector);
  }

  selectedTabChange(index: number): void {
    this.selectedTabChanged.emit(index);
  }

  ngOnDestroy(): object {
    return super.ngOnDestroy();
  }
}
