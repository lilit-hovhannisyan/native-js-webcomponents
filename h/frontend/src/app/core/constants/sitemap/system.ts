import { wording } from '../wording/wording';
import { SitemapNode } from './sitemap';

export const legalEntitySiteMap: {
  [key: string]: SitemapNode;
} = {
  legalEntity: {
    path: 'legal-entity',
    zone: '*',
    title: wording.system.legalEntity,
  },
  representatives: {
    path: 'representatives',
    zone: '*',
    title: wording.system.representatives
  },
  shareholdersPartners: {
    path: 'shareholders-partners',
    zone: '*',
    title: wording.system.shareholdersPartners
  },
  trustors: {
    path: 'trustors',
    zone: '*',
    title: wording.system.trustors,
    notAllowedTabMessage: wording.system.shareholderRequired
  },
  subParticipants: {
    path: 'subParticipants',
    zone: '*',
    title: wording.system.subParticipants,
    notAllowedTabMessage: wording.system.shareholderRequired
  },
  silentPartners: {
    path: 'silent-partners',
    zone: '*',
    title: wording.system.silentPartners
  },
  documents: {
    path: 'documents',
    zone: '*',
    title: wording.system.documents
  },
  meta: {
    path: 'meta',
    zone: '*',
    title: wording.accounting.metaTab
  },
};

export const system: SitemapNode = {
  path: 'system',
  title: { en: 'System', de: 'System' },
  description: { en: 'System', de: 'System...' },
  icon: 'desktop',
  zone: 'system',
  children: {
    misc: {
      path: 'misc',
      title: { en: 'Misc', de: 'Sonstiges' },
      description: { en: 'miscelanious', de: 'verschiedenes' },
      icon: 'assets/icons/88-512.png',
      zone: 'misc',
      children: {
        countries: {
          path: 'countries',
          description: {
            en: 'See all attributes of countries needed in the system, add and update country information.',
            de: 'Finden Sie die Informationen zu Länderschlüsseln und passen Sie selbige bei Bedarf an.'
          },
          title: { en: 'Countries', de: 'Länder' },
          keywords: [
            'IBAN',
            'ISO',
            'Code',
            'Alpha2',
            'Alpha3',
            'Participant',
            'IBAN Prüfung',
            'Iso Code',
            'LKZ',
            'Bankland',
            'SEPA-Teilnehmer',
            'Alpha',
          ],
          zone: 'countries',
        },
        companies: {
          path: 'companies',
          description: {
            en: 'Find, add and update main data of your business partners.',
            de: 'Definieren und pflegen Sie die Basisdaten Ihrer Geschäftspartner.'
          },
          title: { en: 'Companies', de: 'Geschäftspartner' },
          keywords: [
            'Supplier',
            'Customer',
            'Colleagues',
            'User',
            'Agents',
            'Partner',
            'Lieferanten',
            'Partner',
            'Kunden',
            'Dienstleister',
            'Kollegen',
            'Mitarbeiter',
            'Agenten',
          ],
          zone: 'companies',
          children: {
            id: {
              path: ':companyId',
              zone: '*',
              title: { en: 'Companies', de: 'Geschäftspartner' },
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: { en: 'Basic', de: 'Basic' }
                },
                bankAccounts: {
                  path: 'bankAccounts',
                  zone: 'bankAccount',
                  title: { en: 'Bank Accounts', de: 'Bank Accounts' }
                },
                addresses: {
                  path: 'addresses',
                  zone: 'addresses',
                  title: { en: 'Addresses', de: 'Addresses' }
                },
                contacts: {
                  path: 'contacts',
                  zone: 'contacts',
                  title: { en: 'Contacts', de: 'Contacts' }
                },
                categories: {
                  path: 'categories',
                  zone: 'categories',
                  title: wording.system.categories,
                },
                legal: {
                  path: 'legal',
                  zone: 'legals',
                  title: wording.system.legal,
                  notAllowedTabMessage: wording.system.legalFormIsMissing,
                  children: legalEntitySiteMap
                },
              },
            }
          }
        },
        companyCategories: {
          path: 'company-categories',
          description: {
            en: 'See and add company categories here.',
            de: 'Sehen Sie, welche Geschäftspartnerkategorien im System verfügbar sind und fügen Sie bei Bedarf weitere hinzu.'
          },
          title: wording.system.companyCategoriesTitle,
          keywords: [], // todo: when client submit data
          zone: 'companyCategories',
        },
        companyGroups: {
          path: 'company-groups',
          description: {
            en: 'See and add groups of company here.',
            de: 'Sehen Sie, welche Konzerngruppen im System verfügbar sind und fügen Sie bei Bedarf weitere hinzu.',
          },
          title: wording.system.companyGroupsTitle,
          keywords: [
            'group of company',
            'groups of company',
            'company group',
            'company groups',
            'konzerngruppe',
            'konzerngruppen',
            'gruppe',
            'gruppen',
          ],
          zone: 'groupOfCompanies',
        },
        banks: {
          path: 'banks',
          description: {
            en: 'List all banks, edit information and add new banks, SWIFT validation will lead to high data quality',
            // tslint:disable-next-line:max-line-length
            de: 'Öffnen Sie eine Liste der vorhandenen Bankinstitute, fügen Sie neue Banken hinzu, profitieren Sie dabei von der SWIFT-Prüfung.'
          },
          title: { en: 'Banks', de: 'Banken' },
          keywords: [
            'Financial',
            'SWIFT',
            'Validation',
            'Code',
            'Bankinstitute',
            'Kontrahenten',
            'BIC',
          ],
          zone: 'banks',
        },
        currencies: {
          path: 'currencies',
          description: {
            en: 'Inform yourself about latest currency rates provided by the ECB',
            de: 'Informieren Sie sich über die aktuellen Kursverhältnisse, welche automatisch von der EZB importiert werden'
          },
          title: { en: 'Currencies', de: 'Währungen' },
          keywords: [
            'Exchange',
            'Price',
            'Währung',
            'Iso',
            'Iso-Code',
            'Kurs',
            'EUR',
            'USD',
          ],
          zone: 'currencies',
          children: {
            id: {
              path: ':currencyId',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: { en: 'Basic', de: 'Basic' }
                },
                exchangeRates: {
                  path: 'exchangeRates',
                  zone: '*',
                  title: { en: 'Exchange Rates', de: 'Exchange Rates' }
                },
              }
            }
          }
        },
        roles: {
          path: 'roles',
          description: {
            en: 'Check existing roles, define new ones or change relations',
            de: 'Legen Sie hier neue Rollen an und bearbeiten Sie vorhandene Einstellungen'
          },
          title: { en: 'Roles', de: 'Rollen' },
          keywords: [
            'Group',
            'Gruppe',
          ],
          zone: 'roles',
          children: {
            id: {
              path: ':roleId',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: { en: 'Basic', de: 'Basic' }
                },
                mandators: {
                  path: 'mandators',
                  zone: 'mandators',
                  isFakeChild: true,
                  fullZone: 'accounting.booking.mandators.roles',
                  fullTitle: {
                    en: 'Accounting/Booking/Mandators/Roles',
                    de: 'Buchhaltung/Booking/Mandanten/Rollen',
                  },
                  title: wording.accounting.mandators,
                },
                users: {
                  path: 'users',
                  zone: 'users',
                  isFakeChild: true,
                  fullZone: 'system.misc.users.roles',
                  fullTitle: {
                    en: 'System/Users/Roles',
                    de: 'System/Benutzer/Rollen',
                  },
                  title: { en: 'Users', de: 'Benutzer' }
                },
                vessels: {
                  path: 'vessels',
                  zone: 'vessels',
                  isFakeChild: true,
                  fullZone: 'chartering.misc.vessel.roles',
                  fullTitle: {
                    en: 'Chartering/Misc/Vessels/Roles',
                    de: 'Chartering/Misc/Schiffe/Rollen',
                  },
                  title: wording.chartering.vessels,
                },
              }
            }
          }
        },
        users: {
          path: 'users',
          description: {
            en: 'Check existing users, add new ones or change relations',
            de: 'Administrieren Benutzer und legen Sie User an.'
          },
          title: { en: 'Users', de: 'Benutzer' },
          keywords: [
            'Account',
            'Profil',
          ],
          zone: 'users',
          children: {
            id: {
              path: ':userId',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: { en: 'Basic', de: 'Basic' }
                },
                roles: {
                  path: 'roles',
                  zone: 'roles',
                  title: { en: 'Roles', de: 'Roles' }
                },
              }
            }
          }
        },
        shareholdings: {
          path: 'shareholdings',
          description: {
            en: 'Find and edit legal information of companies such as shareholdings.',
            de: 'Suchen und bearbeiten Sie rechtliche Daten zu Unternehmen wie z.B. Beteiligungsverhältnisse.'
          },
          title: wording.system.shareholdings,
          keywords: [
            'shareholdings',
            'legal',
            'company',
            'companies',
            'Beteiligungen',
            'Beteiligungsverhältnisse',
            'Rechtliches',
            'Gesellschaften',
            'Firma',
            'Firmen',
          ],
          zone: 'shareholdings',
          children: {
            id: {
              path: ':shareholdingsId',
              zone: '*',
              title: { en: 'Shareholdings', de: 'Beteiligungen' },
              children: legalEntitySiteMap
            }
          }
        },
        taskTemplates: {
          path: 'task-templates',
          description: {
            en: 'View and edit your task and checklist templates, which can be used in the whole system.',
            de: 'Sehen und bearbeiten Sie Ihre Vorlagen für Aufgaben und Checklisten, die im gesamten System genutzt werden können.'
          },
          title: wording.system.taskChecklistTemplates,
          keywords: [
            'Tasks', 'Checklists', 'Templates', 'Aufgaben', 'Checklisten', 'Vorlagen'
          ],
          zone: 'taskTemplates',
        }
      }
    },
    settings: {
      path: 'settings',
      title: { en: 'Settings', de: 'Einstellungen' },
      description: { en: 'Settings', de: 'Einstellungen' },
      icon: 'assets/icons/88-512.png',
      zone: 'settings',
      children: {
        global: {
          path: 'global',
          description: { en: 'Global Settings', de: 'Globale Einstellungen' },
          title: { en: 'Global Settings', de: 'Globale Einstellungen' },
          zone: 'global',
        },
        maintenance: {
          path: 'maintenance',
          description: { en: 'Create Maintenance Schedules', de: 'Wartungsmoduszeiten erstellen' },
          title: { en: 'Maintenance mode', de: 'Wartungsmodus' },
          zone: '*',
        },
      }
    }
  }
};
