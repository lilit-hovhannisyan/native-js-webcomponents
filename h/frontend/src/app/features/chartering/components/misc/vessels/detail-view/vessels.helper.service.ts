import { Injectable } from '@angular/core';
import { BehaviorSubject, forkJoin, Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { wording } from 'src/app/core/constants/wording/wording';
import { findClosestWithFromUntil } from 'src/app/core/helpers/general';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { IVesselPreDelayCharterName } from 'src/app/core/models/resources/IVesselPreDelayCharterName';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { VesselRepositoryService } from 'src/app/core/services/repositories/vessel-repository.service';
import { VoyageCharternameRepositoryService } from 'src/app/core/services/repositories/voyage-chartername-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';

@Injectable({
  providedIn: 'root'
})
export class VesselsHelperService {
  public editViewTitleSubject: BehaviorSubject<string> = new BehaviorSubject('');
  public editViewTitle$ = this.editViewTitleSubject.asObservable();
  public isCommissionsMissing$ = new BehaviorSubject<boolean>(false);
  public isAccountingsMissing$ = new BehaviorSubject<boolean>(false);
  public isSpecificationsMissing$ = new BehaviorSubject<boolean>(false);
  public isSettingsMissing$ = new BehaviorSubject<boolean>(false);
  public isRegistrationMissing$ = new BehaviorSubject<boolean>(false);

  constructor(
    public settingsService: SettingsService,
    private charternameRepo: VoyageCharternameRepositoryService,
    private vesselRepo: VesselRepositoryService,
    private vesselMainService: VesselMainService,
  ) { }

  private fetchPreDeliveryChartername(id: VesselKey): Observable<IVesselPreDelayCharterName> {
    return this.vesselRepo.fetchPreDeliveryChartername(id).pipe(
      // first time vesselPreDelivery doesn't exist and will throw an error
      catchError(() => of(undefined)),
    );
  }

  public generateTitle = (vesselId?: VesselKey) => {
    if (vesselId) {
      forkJoin([
        this.vesselRepo.fetchOne(vesselId, {}),
        this.vesselRepo.fetchAccountingsByVesselId(vesselId),
        this.charternameRepo.fetchAll({ queryParams: { vesselId } }),
        this.fetchPreDeliveryChartername(vesselId),
      ]).pipe(
        tap(([vessel, accountings, charternames, vesselPreDelivery]) => {
          const accounting = findClosestWithFromUntil(accountings);
          const chartername = this.vesselMainService.findClosestChartername(charternames, vesselPreDelivery);
          const vesselWording = wording.chartering.vessel[this.settingsService.language];
          const vesselName = accounting?.vesselName || '';
          const charterName = chartername?.charterName
            ? `(${chartername?.charterName})`
            : '';
          const title = `${vesselWording}: ${accounting?.vesselNo || vessel.imo}
            ${vesselName} ${charterName}`;

          this.editViewTitleSubject.next(title);
        })
      ).subscribe();
    } else {
      const title = wording.general.create[this.settingsService.language];

      this.editViewTitleSubject.next(title);
    }
  }
}
