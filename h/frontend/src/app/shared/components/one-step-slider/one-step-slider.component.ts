import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { IResource, ResourceKey } from 'src/app/core/models/resources/IResource';

@Component({
  selector: 'nv-one-step-slider',
  templateUrl: './one-step-slider.component.html',
  styleUrls: ['./one-step-slider.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OneStepSliderComponent<
  K extends ResourceKey = ResourceKey,
  T extends IResource<K> = IResource<K>
> implements OnChanges {
  @Input() private keyPropName = 'id';
  @Input() public dataArray: T[] = [];
  @Input() private value: T;
  @Input() public disabled = false;
  @Output() private valueChange: EventEmitter<T> = new EventEmitter();

  public selectedItemIndex = 0;

  public ngOnChanges(): void {
    if (this.isValueExist()) {
      this.selectedItemIndex = this.getSafeIndex(this.value);
    } else {
      this.selectItem(this.selectedItemIndex || 0);
    }
  }

  /** Won't increase index if selected index is already at the max value. */
  public next(): void {
    this.selectItem(this.selectedItemIndex + 1);
  }
  /** Won't decrease index if selected index is already 0. */
  public prev(): void {
    this.selectItem(this.selectedItemIndex - 1);
  }

  /** Returns 0 if value doesn't belong to `dataArray`. */
  private getSafeIndex(value: T): number {
    const index: number = this.dataArray.findIndex(v => v === value);
    return index < 0 ? 0 : index;
  }

  private selectItem(index: number): void {
    this.selectedItemIndex = index;
    const value = this.dataArray[this.selectedItemIndex];
    this.valueChange.emit(value);
  }

  private isValueExist(): boolean {
    if (!this.value || !this.dataArray?.length) {
      return false;
    }

    return this.dataArray.some(v => {
      return v === this.value || v[this.keyPropName] === this.value[this.keyPropName];
    });
  }
}
