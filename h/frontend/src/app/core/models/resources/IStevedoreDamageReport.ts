import { wording } from 'src/app/core/constants/wording/wording';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';
import { IVoyage } from './IVoyage';
import { IDebitorCreditor } from './IDebitorCreditor';
import { IPort } from './IPort';
import { IVesselAccounting } from './IVesselAccounting';
import { IWording } from './IWording';
import { ICompany } from './ICompany';
import { IVessel } from './IVessel';
import { IStevedoreRepairFull } from './IStevedoreRepair';
import { IUser, UserKey } from './IUser';


export enum StevedoreDamageReportType {
  Stevedore = 1,
  CrewWork = 2
}

export enum PositionSBPS {
  SB = 1,
  PS = 2,
  CENTER = 3,
}

export enum PositionFWDAFT {
  FWD = 1,
  AFT = 2,
  CENTER = 3,
}

export enum Incident {
  CargoHolds = 1,
  CellGuides = 2,
  Cranes = 3,
  Deck = 4,
  HatchCover = 5,
  LashingMaterial = 6,
  Leakage = 7,
  Miscellaneous = 8,
  ReeferRepair = 9,
  ShipRenaming = 10
}

export enum StevedoreDamageReportStatus {
  RatingRequired = 1,
  ReleaseRequired = 2, // approval required
  InProgress = 3, // invoice required
  InvoiceCreated = 4, // inovice created
  Cleared = 5, // invoice cleraed
}

export enum RepairBy {
  Crew = 1,
  RepairCompany = 2,
  Shipyard = 3,
  Stevedores = 4,
  Charterer = 5,
  Terminal = 6,
  NotByCrew = 7
}

export type StevedoreDamageReportKey = number;

interface ISharedProps {
  isSettlementWithoutAutomaticCharterReference: boolean;
  reportType: StevedoreDamageReportType;
  resubmissionDate?: string;
  vesselAccountingId?: number;
  repairBy: RepairBy;
  vesselId: number;
  voyageId?: number;
  clearedDate?: string;
  clearedBy?: number;
  crewReportNo: string;
  crewReportDate?: string;
  sdReportNo: string;
  sdReportDate?: string;
  date: string;
  cpDate?: string;
  timeCharterId: number;
  hold?: number;
  bay?: number;
  row?: number;
  tier?: number;
  positionSBPS?: PositionSBPS;
  positionFWDAFT?: PositionFWDAFT;
  chartererId: number; // debitorCreditorId
  incident: Incident;
  portId: number;
  description: string;
  status: StevedoreDamageReportStatus;
  charterName: string;
  billingAddress: string;
  isChartererInformed?: boolean;
  startOfTimeCharter: string;
  approximateDateOfRedelivery: string;
  // for administration tab
  createdBy: UserKey; // uid
  createdOn: string; // datetime
  invoiceApprovedOn: string; // datetime
  invoiceApprovedBy: UserKey; // uid
  invoiceWaivedOn: string; // datetime
  invoiceWaivedBy: UserKey;
  isInvoiceWaivedAndSetToCleared: boolean;
  isInvoiceApproved: boolean;
  invoiceNumber: number; // not implemented
  invoiceCreatedOn: string; // datetime
  invoiceCreatedBy: UserKey;
  invoicePaidOn: string; // datetime
  remark: string;
}

export interface IStevedoreDamageReportCreate extends ISharedProps {}

export interface IStevedoreDamageReportBase extends ISharedProps, IResource<StevedoreDamageReportKey> {}

export interface IStevedoreDamageReportDTO extends IStevedoreDamageReportBase {}

export interface IStevedoreDamageReport extends IStevedoreDamageReportDTO, IWithLabel {}

export interface IStevedoreDamageReportFull extends IStevedoreDamageReport {
  voyage: IVoyage;
  vesselAccounting: IVesselAccounting;
  debitorCreditor: IDebitorCreditor;
  port: IPort;
  company: ICompany;
  vessel: IVessel;
  stevedoreRepair: IStevedoreRepairFull;
  // Used in administration tab
  createdByUser: IUser;
  invoiceApprovedByUser: IUser;
  invoiceWaivedByUser: IUser;
  invoiceCreatedByUser: IUser;
}

export interface IStevedoreDamageReportRow extends IStevedoreDamageReportFull {
  reportNo: string;
  reportDate: string;
  vesselName: string;
  vesselNo: number;
  charterer: string;
  voyageNo: string;
  voyageEnd: string;
  reportTypeLabel: IWording;
  positionSBPSLabel: IWording | string;
  positionFWDAFTLabel: IWording | string;
  incidentLabel: IWording;
  statusLabel: IWording;
  repairByLabel?: IWording;
  expenses: number;
  iso: string;
  portName?: string;
  createdByUserFullName: string;
  invoiceApprovedByUserFullName: string;
  invoiceWaivedByUserFullName: string;
  invoiceCreatedByUserFullName: string;
}

/**
 * Enum options for displaying in views
 */
export const reportTypeOptions: ISelectOption[] = [
  { displayLabel: wording.technicalManagement.reportTypeStevedore, value: StevedoreDamageReportType.Stevedore },
  { displayLabel: wording.technicalManagement.reportTypeCrewWork, value: StevedoreDamageReportType.CrewWork },
];

export const positionSBPSOptions: ISelectOption[] = [
  { displayLabel: wording.technicalManagement.positionSB, value: PositionSBPS.SB },
  { displayLabel: wording.technicalManagement.positionPS, value: PositionSBPS.PS },
  { displayLabel: wording.technicalManagement.positionCenter, value: PositionSBPS.CENTER },
];

export const positionFWDAFTOptions: ISelectOption[] = [
  { displayLabel: wording.technicalManagement.positionFWD, value: PositionFWDAFT.FWD },
  { displayLabel: wording.technicalManagement.positionAFT, value: PositionFWDAFT.AFT },
  { displayLabel: wording.technicalManagement.positionCenter, value: PositionFWDAFT.CENTER },
];

export const repairByOptions: ISelectOption[] = [
  { displayLabel: wording.technicalManagement.crew, value: RepairBy.Crew },
  { displayLabel: wording.technicalManagement.repairCompany, value: RepairBy.RepairCompany },
  { displayLabel: wording.technicalManagement.shipyard, value: RepairBy.Shipyard },
  { displayLabel: wording.technicalManagement.stevedores, value: RepairBy.Stevedores },
  { displayLabel: wording.technicalManagement.charterer, value: RepairBy.Charterer },
  { displayLabel: wording.technicalManagement.terminal, value: RepairBy.Terminal },
  { displayLabel: wording.technicalManagement.notByCrew, value: RepairBy.NotByCrew },
];

export const positionFWDAFTSOptions: ISelectOption[] = [
  { displayLabel: wording.technicalManagement.positionFWD, value: PositionFWDAFT.FWD },
  { displayLabel: wording.technicalManagement.positionAFT, value: PositionFWDAFT.AFT },
  { displayLabel: wording.technicalManagement.positionCenter, value: PositionFWDAFT.CENTER },
];

export const incidentOptions: ISelectOption[] = [
  { displayLabel: wording.technicalManagement.incidentCargoHolds, value: Incident.CargoHolds },
  { displayLabel: wording.technicalManagement.incidentCellGuides, value: Incident.CellGuides },
  { displayLabel: wording.technicalManagement.incidentCranes, value: Incident.Cranes },
  { displayLabel: wording.technicalManagement.incidentDeck, value: Incident.Deck },
  { displayLabel: wording.technicalManagement.incidentHatchCover, value: Incident.HatchCover },
  { displayLabel: wording.technicalManagement.incidentLashingMaterial, value: Incident.LashingMaterial },
  { displayLabel: wording.technicalManagement.incidentLeakage, value: Incident.Leakage },
  { displayLabel: wording.technicalManagement.incidentMiscellaneous, value: Incident.Miscellaneous },
  { displayLabel: wording.technicalManagement.incidentReeferRepair, value: Incident.ReeferRepair },
  { displayLabel: wording.technicalManagement.incidentShipRenaming, value: Incident.ShipRenaming },
];

export const stevedoreDamageStatusOptions: ISelectOption[] = [
  { displayLabel: wording.technicalManagement.ratingRequired, value: StevedoreDamageReportStatus.RatingRequired },
  { displayLabel: wording.technicalManagement.approvalRequired, value: StevedoreDamageReportStatus.ReleaseRequired },
  { displayLabel: wording.technicalManagement.invoiceRequired, value: StevedoreDamageReportStatus.InProgress },
  { displayLabel: wording.technicalManagement.administratorTabInvoiceCreated, value: StevedoreDamageReportStatus.InvoiceCreated },
  { displayLabel: wording.technicalManagement.invoiceCleared, value: StevedoreDamageReportStatus.Cleared },
];
