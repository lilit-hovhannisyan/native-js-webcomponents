import { Component, ChangeDetectionStrategy, TemplateRef, ChangeDetectorRef, Inject, EventEmitter, Output } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { MeButtonI } from '../me-button/me-button.interface';
import { MePopupBasicInputI } from './me-popup-basic-input.interface';

@Component({
  selector: 'me-popup-basic-input',
  templateUrl: './me-popup-basic-input.component.html',
  styleUrls: ['./me-popup-basic-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MePopupBasicInputComponent {
  @Output() popupBasicInputEvent: EventEmitter<{ eventName: string }> = new EventEmitter();

  inviteButton: MeButtonI = { color: 'primary', type: 'regular' };
  cancelButton: MeButtonI = { color: 'secondary', type: 'regular' };

  private _dataModel!: MePopupBasicInputI;
  content!: TemplateRef<Component>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { dataModel: MePopupBasicInputI; content: TemplateRef<Component> },
    private cdr: ChangeDetectorRef
  ) {
    this._dataModel = data.dataModel;
    this.content = data.content;
  }

  set dataModel(data: MePopupBasicInputI) {
    this._dataModel = data;
    this.cdr.detectChanges();
  }

  get dataModel(): MePopupBasicInputI {
    return this._dataModel;
  }

  customEvent(eventName: string): void {
    this.popupBasicInputEvent.emit({ eventName });
  }
}
