import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/mandator-tax-keys.config';
import {
  MandatorTaxKeysRepositoryService
} from 'src/app/core/services/repositories/mandator-tax-keys-repository.service';
import {
  MandatorTaxKeysMainService,
  IMandatorTaxKeyRow,
  IMandatorTaxKeyFull
} from 'src/app/core/services/mainServices/mandator-tax-keys-main.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { Observable } from 'rxjs';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { MandatorTaxKeyDetailViewComponent } from '../detail-view/detail-view.component';
import { MandatorTaxKeyDetailsComponent } from './details/details.component';
import { map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
})
export class MandatorTaxKeyListViewComponent implements OnInit {
  public mandatorId: number;
  private modalRef?: NzModalRef;

  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IMandatorTaxKeyRow[]>;

  public wordingTaxKeys = wording.accounting;

  constructor(
    private modalService: NzModalService,
    public gridConfigService: GridConfigService,
    public route: ActivatedRoute,
    private mainService: MandatorTaxKeysMainService,
    private mandatorTaxKeysRepo: MandatorTaxKeysRepositoryService,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private settingsService: SettingsService
  ) { }

  public ngOnInit() {
    this.mandatorId = parseInt(this.route.parent.snapshot.params.id, 10);

    this.dataSource$ = this.mainService.getStream({ mandatorId: this.mandatorId })
      .pipe(map((mandatorTaxKeys) => this.mainService.toRows(mandatorTaxKeys)));

    const actions = {
      show: this.openDetailsModal,
      delete: this.deleteClicked,
      add: this.openDetailViewModal
    };

    const canPerform = (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation);
    this.gridConfig = getGridConfig(actions, canPerform);
  }

  public deleteClicked = (taxKeyFull: IMandatorTaxKeyFull) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: taxKeyFull.taxKey.displayLabel },
    }).subscribe(confirmed => confirmed && this.mandatorTaxKeysRepo.delete(taxKeyFull.id, {}).subscribe());
  }

  private openDetailViewModal = (taxKey?: IMandatorTaxKeyRow) => {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: this.wordingTaxKeys.titleTaxKeys[this.settingsService.language],
      nzClosable: true,
      nzContent: MandatorTaxKeyDetailViewComponent,
      nzComponentParams: {
        closeModal: this.closeModal,
        mandatorId: this.mandatorId
      },
      nzFooter: null
    });
  }

  private openDetailsModal = (mandatorTaxKey: IMandatorTaxKeyRow) => {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzWidth: 530,
      nzTitle: this.wordingTaxKeys.taxRelCodes[this.settingsService.language],
      nzClosable: true,
      nzContent: MandatorTaxKeyDetailsComponent,
      nzComponentParams: {
        closeModal: this.closeModal,
        mandatorTaxKey,
      }
    });
  }

  private closeModal = () => {
    this.modalRef.destroy();
  }
}
