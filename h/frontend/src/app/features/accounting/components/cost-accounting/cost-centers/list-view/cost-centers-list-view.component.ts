import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { Observable } from 'rxjs';
import { ICostCenter } from 'src/app/core/models/resources/ICostCenter';
import { NvGridConfig } from 'nv-grid';
import { CostCenterRepositoryService } from 'src/app/core/services/repositories/cost-center-repository.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { getCostCentersGridConfig } from 'src/app/core/constants/nv-grid-configs/cost-centers.config';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { map } from 'rxjs/operators';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';


@Component({
  selector: 'nv-cost-centers-list-view',
  templateUrl: './cost-centers-list-view.component.html',
  styleUrls: ['./cost-centers-list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CostCentersListViewComponent implements OnInit {
  public dataSource$: Observable<ICostCenter[]>;
  public gridConfig: NvGridConfig;

  constructor(
    public gridConfigService: GridConfigService,
    private costCenterRepo: CostCenterRepositoryService,
    private router: Router,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
  ) { }

  public ngOnInit(): void {
    this.initGridConfig();
    this.costCenterRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.costCenterRepo.getStream()
      .pipe(
        map(costCenters => this.costCenterRepo.toRows(costCenters)),
      );
  }

  private initGridConfig(): void {
    const gridActions: IGridConfigActions = {
      edit: (row: ICostCenter) => { this.router.navigate([`${this.router.url}/${row.id}`]); },
      delete: (row: ICostCenter) => this.delete(row),
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    const canPerform: (operation: number) => boolean = (operation: number) => this.authService
      .canPerformOperationOnCurrentUrl(operation);
    this.gridConfig = getCostCentersGridConfig(
      gridActions,
      canPerform
    );
  }

  private delete = (costCenter: ICostCenter): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: setTranslationSubjects(
        wording.general.areYouSureYouWantToDelete,
        { subject: costCenter.displayLabel },
      ),
    }).subscribe(confirmed => {
      if (confirmed) {
        this.costCenterRepo.delete(costCenter.id, {}).subscribe();
      }
    });
  }

}
