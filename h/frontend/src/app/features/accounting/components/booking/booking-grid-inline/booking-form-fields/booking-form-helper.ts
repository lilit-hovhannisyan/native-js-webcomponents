import { ACCOUNTING_TYPES } from 'src/app/core/models/enums/accounting-types';
import { MandatorKey } from 'src/app/core/models/resources/IMandator';
import { VoucherTypeKey } from 'src/app/core/models/resources/IVoucherType';

export interface IBookingFormState {
  accountingType: ACCOUNTING_TYPES;
  defineInvoice: boolean;
  accrualAccounting: boolean;
  invoiceDate: string;
  leadingAccount: boolean;
  mandatorId: MandatorKey;
  mandatorNoDisabled: string;
  period: number;
  serialNo: string;
  voucherDate: string;
  voucherTypeId: VoucherTypeKey;
  year: string;
  yearDisabled: string;
}
