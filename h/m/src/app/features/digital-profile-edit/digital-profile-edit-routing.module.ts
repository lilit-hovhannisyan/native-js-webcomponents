import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaveCheckGuard } from '@core-guards/leave-check.guard';
import { MePermissionTypes } from '@shared/me-permission-types';

import { DigitalProfileEditComponent } from './digital-profile-edit.component';

const routes: Routes = [
  {
    path: '',
    component: DigitalProfileEditComponent,
    data: {
      breadcrumb: 'Edit Digital Profile',
      permission: MePermissionTypes.COMPANY_MODIFY
    },
    canDeactivate: [LeaveCheckGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DigitalProfileEditRoutingModule {}
