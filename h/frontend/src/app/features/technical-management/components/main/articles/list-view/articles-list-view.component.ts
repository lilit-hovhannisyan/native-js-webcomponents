import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { getArticlesGridConfig } from 'src/app/core/constants/nv-grid-configs/articles.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { IArticle } from 'src/app/core/models/resources/IArticle';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { ArticleRepositoryService } from 'src/app/core/services/repositories/article-repository.service';

@Component({
  templateUrl: './articles-list-view.component.html',
  styleUrls: ['./articles-list-view.component.scss']
})
export class ArticlesListViewComponent implements OnInit {
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IArticle[]>;

  constructor(
    public gridConfigService: GridConfigService,
    private articleRepo: ArticleRepositoryService,
    private router: Router,
    private confirmService: ConfirmationService,
    private authService: AuthenticationService,
  ) { }

  public ngOnInit() {
    this.articleRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.articleRepo.getStream();

    const gridActions = {
      add: () => this.router.navigate([this.router.url, 'new']),
      edit: (id: number) => this.router.navigate([this.router.url, id]),
      delete: this.deleteClicked,
    };

    this.gridConfig = getArticlesGridConfig(
      gridActions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
    );
  }

  private deleteClicked = (article: IArticle) => {
    this.confirmService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: article.displayLabel },
    }).subscribe(confirmed => confirmed && this.articleRepo.delete(article.id, {}).subscribe());
  }
}
