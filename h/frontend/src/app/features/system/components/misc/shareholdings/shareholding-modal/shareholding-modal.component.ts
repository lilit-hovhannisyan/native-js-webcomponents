import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { COMPANY_LEGAL_FORM, COMPANY_LEGAL_FORM_TYPE, getCompanyLegalFormTypes } from 'src/app/core/models/enums/company-legal-form';
import { wording } from 'src/app/core/constants/wording/wording';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { Router } from '@angular/router';
import { ShareholdingsHelperService } from '../shareholdings.helper.service';
import { ILegalFormFilter } from '../../../../shared/legal-type-tree/legal-type-tree.component';

@Component({
  selector: 'nv-shareholding-modal',
  templateUrl: './shareholding-modal.component.html',
  styleUrls: ['./shareholding-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareholdingModalComponent implements OnInit {
  @Input() public legalForm: COMPANY_LEGAL_FORM;
  @Input() public legalFormType: COMPANY_LEGAL_FORM_TYPE;
  public legalFormTypeLength: number;

  public wording = wording;

  constructor(
    private modalRef: NzModalRef,
    private router: Router,
    private shareholdingsHelperService: ShareholdingsHelperService,
  ) { }

  public ngOnInit(): void {
    this.legalFormTypeLength = getCompanyLegalFormTypes(this.legalForm).length;
  }

  public onLegalFormChange(legalFormFilter: ILegalFormFilter): void {
    this.legalForm = legalFormFilter.legalForm;
    this.legalFormType = legalFormFilter.legalFormType;
    this.legalFormTypeLength = getCompanyLegalFormTypes(this.legalForm).length;
  }

  public cancel(): void {
    this.modalRef.close();
    this.shareholdingsHelperService.setLegalData(null);
  }

  public create(): void {
    this.cancel();
    this.shareholdingsHelperService.setLegalData({
      legalForm: this.legalForm,
      legalFormType: this.legalFormType,
    });
    this.router.navigate([`${this.router.url}/new`]);
  }
}
