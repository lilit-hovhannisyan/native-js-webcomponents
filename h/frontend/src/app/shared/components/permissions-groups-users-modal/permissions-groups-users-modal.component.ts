import { Component, OnInit, Input } from '@angular/core';
import { IUser } from 'src/app/core/models/resources/IUser';
import { NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { getPermissionsGroupsUsersModalGridConfig } from './permissions-groups-users-modal.config';
import { Observable, of } from 'rxjs';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { cloneDeep } from 'lodash';

@Component({
  templateUrl: './permissions-groups-users-modal.component.html',
  styleUrls: ['./permissions-groups-users-modal.component.scss']
})
export class PermissionsGroupsUserModalComponent implements OnInit {
  @Input() public users: IUser[];
  @Input() public isAutobankPage = true;
  public selectedUsers: IUser[];
  public gridConfig: NvGridConfig;
  public wording = wording;
  public dataSource$: Observable<IUser[]>;
  public submitDisabled = true;

  constructor(
    private modalRef: NzModalRef,
  ) { }

  public ngOnInit(): void {
    this.gridConfig = getPermissionsGroupsUsersModalGridConfig(this.isAutobankPage);
    this.dataSource$ = of(
      // change references to not have previous checked items checked
      cloneDeep(this.users),
    );
  }

  public submit(): void {
    if (this.submitDisabled) {
      return;
    }

    this.modalRef.close(this.selectedUsers);
  }

  public rowSelect(users: IUser[]) {
    this.selectedUsers = users;
    this.submitDisabled = !users.length;
  }
}
