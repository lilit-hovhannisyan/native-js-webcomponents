import { IWidgetDTO, IWidgetBase } from './IWidget';
import { GridsterItem } from 'angular-gridster2';

export interface IBookmarkWidget extends GridsterItem, IWidgetBase {
  bookmarkedTopics: string[];
}

export interface IBookmarkWidgetDTO extends IWidgetDTO {
  bookmarkedTopics?: string[];
}
