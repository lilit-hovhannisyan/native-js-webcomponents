import { Injectable, Type } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ArrayResponseI } from '../interfaces/array-response.interface';
import { plainToClass } from 'class-transformer';

@Injectable({
  providedIn: 'root'
})
export class BaseWebService {
  constructor(private http: HttpClient) {}

  getRequest<T>(url: string, classType: Type<T>): Observable<T> {
    const options = this.addOptionsForRequest();
    return this.http.get<T>(url, options).pipe(
      map((res) => {
        return classType ? plainToClass(classType, res as T) : res;
      })
    );
  }

  getRequestForArray<T>(url: string, classType: Type<T>): Observable<ArrayResponseI<T>> {
    const options = this.addOptionsForRequest();
    return this.http.get<ArrayResponseI<T>>(url, options).pipe(
      map((res) => {
        const result: ArrayResponseI<T> = {
          entities: res.entities ? res.entities.map((entity: T) => plainToClass(classType, entity)) : [],
          nextID: res.nextID,
          totalCount: res.totalCount
        };
        return result;
      })
    );
  }

  postRequest<T, K>(url: string, data?: K, classType?: Type<T>, headers?: object): Observable<T> {
    const options = this.addOptionsForRequest(headers);
    return this.http.post<T>(url, data, options).pipe(
      map((res) => {
        return classType ? plainToClass(classType, res as T) : res;
      })
    );
  }

  postRequestForArray<T, K>(url: string, data: K, classType: Type<T>): Observable<ArrayResponseI<T>> {
    const options = this.addOptionsForRequest();
    return this.http.post<ArrayResponseI<T>>(url, data, options).pipe(
      map((res) => {
        const result: ArrayResponseI<T> = {
          entities: res.entities ? res.entities.map((entity: T) => plainToClass(classType, entity)) : [],
          nextID: res.nextID,
          totalCount: res.totalCount
        };
        return result;
      })
    );
  }

  putRequest<T, K>(url: string, data: K, classType?: Type<T>): Observable<T> {
    const options = this.addOptionsForRequest();
    return this.http.put<T>(url, data, options).pipe(
      map((res) => {
        return classType ? plainToClass(classType, res as T) : res;
      })
    );
  }

  deleteRequest<T>(url: string, bodyParams?: unknown, classType?: Type<T>): Observable<T> {
    // need to add body params to DELETE request because backend is not completely in RESTful standard
    const options = bodyParams ? this.addOptionsForRequest({}, 'json', bodyParams) : this.addOptionsForRequest();
    return this.http.delete<T>(url, options).pipe(
      map((res) => {
        return classType ? plainToClass(classType, res as T) : res;
      })
    );
  }

  private addOptionsForRequest(additionalHeaders?: object, responseType: string = 'json', body?: unknown): object {
    // Create headers
    const headers: HttpHeaders = new HttpHeaders({
      Accept: 'application/json',
      ...additionalHeaders
    });
    const options = {
      headers,
      responseType,
      reportProgress: false,
      observe: 'body',
      withCredentials: false,
      body
    };

    options.withCredentials = true;

    return options;
  }
}
