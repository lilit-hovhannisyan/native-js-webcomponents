import { NvGridConfig, NvGridRowSelectionType, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';


export const gridConfig: NvGridConfig = {
  gridName: 'accountSelectModalGrid',
  rowSelectionType: NvGridRowSelectionType.RadioButton,
  rowButtonsPosition: null,
  hideRefreshButton: true,
  hideSettingsButton: true,
  paging: {
    pageNumber: 1,
    pageSize: 10,
    showTotalItems: true
  },
  columns: [
    {
      key: 'no',
      title: wording.accounting.accountNumber,
      width: 120,
      isSortable: true,
      filter: {
        values: []
      }
    }, {
      key: 'displayLabel',
      title: wording.general.label,
      width: 360,
      isSortable: true,
      filter: {
        values: []
      }
    },
    {
      key: 'isRelatedToMandator',
      title: wording.accounting.related,
      width: 130,
      isSortable: true,
      filter: {
        controlType: NvFilterControl.Boolean,
        values: [],
      },
      dataType: NvColumnDataType.Boolean
    }
  ],
  sortBy: 'no',
  isSortAscending: true,
};
