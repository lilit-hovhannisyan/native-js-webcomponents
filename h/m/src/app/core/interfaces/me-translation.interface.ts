export interface MeTranslationI {
  [key: string]: string;
}
