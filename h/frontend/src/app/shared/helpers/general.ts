import { dateFormatTypes, NvLocale, timeFormatTypes } from 'src/app/core/models/dateFormat';
import { formatNumber } from '@angular/common';
import { nvLocalMapper } from '../pipes/localDecimal';
import { IWording, ReplacementSubjects } from 'src/app/core/models/resources/IWording';
import { ResourceKey, IResource } from 'src/app/core/models/resources/IResource';
import * as moment from 'moment';
import { IWithLabels } from 'src/app/core/models/resources/IWithLabels';
import { SettingsService } from 'src/app/core/services/settings.service';

// used in many select-fields that hold resources as the [compareWith] input
export const compareById = (opt1, opt2) => {
  const areSame = (
    opt1 && opt2 &&
    typeof opt1 === 'object' &&
    typeof opt2 === 'object' &&
    opt1.id === opt2.id
  );
  return areSame;
};

// check if value is not empty , undefined or null
export const isEmpty = (value) => {
  return value === ''
    || value === null
    || typeof value === 'undefined'
    || (typeof value === 'string' && value.replace(/ /g, '') === '');
};

export const isNumber = (x: any) => typeof x === 'number' && !isNaN(x);

export const sortArray = (unsortedArray: any[], key: string, order: 'ascend' | 'descend' = 'ascend') => {
  const compare = (A, B) => {

    // empty values are treated as 'high' values
    if (isEmpty(A)) { return 1; }
    if (isEmpty(B)) { return -1; }

    return isNumber(A) && isNumber(B) // if is number
      ? A - B
      : (A + '').localeCompare(B + '', undefined, { numeric: true, sensitivity: 'base' });
  };

  return unsortedArray.sort(
    (a, b) => order === 'ascend'
      ? compare(a[key], b[key])
      : compare(b[key], a[key])
  ).slice();
};

export const hasValue = (value: number | string): boolean => {
  return !!value || value === 0 || value === '';
};

export const getDateFormat = (locale: NvLocale) => {
  if (locale !== undefined) {
    return dateFormatTypes[locale];
  }
  return dateFormatTypes[NvLocale.DE];
};

export const getDateTimeFormat = (locale: NvLocale) => {
  if (locale !== undefined) {
    return `${dateFormatTypes[locale]} ${timeFormatTypes[locale]}`;
  }
  return `${dateFormatTypes[NvLocale.DE]} ${timeFormatTypes[NvLocale.DE]}`;
};

export const formatNumberWrapper = (
  value: number,
  minIntegerDigits = 1,
  minFractionDigits = 0,
  maxFractionDigits = 100,
  locale: NvLocale
): string => {
  const digitsInfo = `${minIntegerDigits}.${minFractionDigits}-${maxFractionDigits}`;
  return formatNumber(value, nvLocalMapper[locale], digitsInfo);
};

export const dateToTimestamp = (date: string): number => {
  return Math.round(new Date(date).getTime() / 1000);
};

export const filter = (array: any[], filters: object): any[] => {
  const filterKeys = Object.keys(filters);
  return array.filter(item => {
    return filterKeys.every(key => {
      return typeof filters[key] === 'function'
        ? filters[key](item)
        : filters[key] === item[key];
    });
  });
};

export function setTranslationSubjects(
  wording: IWording,
  subjects: ReplacementSubjects = {},
): IWording {
  // copy the wording
  const newWording: IWording = { ...wording };

  return Object.keys(subjects).reduce((result: IWording, key: string) => {
    const subjectVal = subjects[key];
    const type = typeof subjectVal;

    if (type === 'string' || type === 'number' || subjectVal === null) {
      const value = subjectVal || subjectVal === 0 ? `${subjectVal}` : '';

      // split join replaces all occurrences of a string
      result.de = result.de?.split(`{${key}}`).join(value);
      result.en = result.en?.split(`{${key}}`).join(value);
    } else if (type === 'object') {
      const value = subjectVal as IWording;
      result.de = result?.de.split(`{${key}}`).join(value.de);
      result.en = result?.en.split(`{${key}}`).join(value.en);
    }

    return result;
  }, newWording);
}

export function isJson(str: string) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }

  return true;
}

export function isPrimitive(value: any) {
  const type = typeof value;

  return value === null || (type !== 'object' && type !== 'function');
}

export function getUniqueResources<T extends IResource<ResourceKey>>(used: T[], all: T[]) {
  return all.filter(item => {
    return used.every(usedItem => usedItem.id !== item.id);
  });
}

export function convertToMap<T extends IResource<K>, K extends ResourceKey>(
  resource: T[],
  keyPath?: string,
): Map<K, T> {
  const map: Map<K, T> = new Map();

  resource.forEach(r => map.set(keyPath ? r[keyPath] : r.id, r));

  return map;
}

export function safeFloatSum(v1: number, v2: number, fractionDigits: number = 2): number {
  return parseFloat((v1 + v2).toFixed(fractionDigits));
}

export function safeFloatReduction(v1: number, v2: number, fractionDigits: number = 2): number {
  return parseFloat((v1 - v2).toFixed(fractionDigits));
}

/**
 * @param date date from backend
 * backend returns iso date without last 'Z' which is necessary
 * for moment.js
 */
export function fixIsoDate(date?: string): string {
  if (!date) {
    return date;
  }

  return date[date.length - 1] === 'Z'
    ? date
    : `${date}Z`;
}

export function getDateWithinRange<T extends { from?: string; until?: string }>(list: T[], date: string): T {
  const momentDate: moment.Moment = moment(date);

  return list.find(v => moment(fixIsoDate(v.from)).isSameOrBefore(momentDate)
    && moment(fixIsoDate(v.until)).isSameOrAfter(momentDate));
}

export function getDefaultIntervalListItem<T extends { from?: string; until?: string }>
  (list: T[]): T {
  const today = new Date().getTime();
  const intervalListItemToSelect = list.find(s =>
    new Date(s.from).getTime() <= today &&
    (!s.until || new Date(s.until).getTime() >= today));

  if (intervalListItemToSelect) {
    return intervalListItemToSelect;
  } else {
    const pastDatesTable = list.filter(entry => entry.until
      && new Date(entry.until).getTime() <= today);
    const intervalListItemClosestToToday = pastDatesTable.length > 0
      ? pastDatesTable.reduce((a, b) =>
        (Math.abs(new Date(a.from).getTime() - today) <
          Math.abs(new Date(b.from).getTime() - today))
          ? a : b)
      : list[0];
    return (intervalListItemClosestToToday);
  }
}

export function getFormmattedDate(date: moment.MomentInput, formatString: string): string {
  const momentDate: moment.Moment = moment(date);
  return momentDate.format(formatString);
}
/**
 * @param maxLength of uid
 * Real max-length is about 12-13 symbols
 *
 * Returns unique id.
 * It is not guaranteed that returned uid will have maxLength!
 */
export function uid(maxLength?: number, radix = 16): string {
  return Math.random().toString(radix).slice(2, maxLength || undefined);
}

export function label2Wording<T extends IWithLabels>(object: T): IWording {
  return {
    en: object.labelEn,
    de: object.labelDe,
  };
}

export const isValidDateMomentDate = (date: string) => {
  return moment(date, 'YYYY-MM-DDTHH:mm:ss.SSSS', true).isValid()
  || moment(date, 'YYYY-MM-DDTHH:mm:ss.SSSSZ', true).isValid()
  || moment(date, 'YYYY-MM-DDTHH:mm:ss', true).isValid();
};

export const getLocalDateTime = (date: string, format: string) => {
  return date && isValidDateMomentDate(date)
    ? moment.utc(date).local().format(format)
    : date;
};

export const getUTCDateTime = (date: string, format: string) => {
  return date && isValidDateMomentDate(date)
    ? moment(date, format).utc().format(format)
    : date;
};

export function fetchFormattedIntervals<T extends { from?: string; until?: string }>
(list: T[]): T[] {
  return list.map(
    entry => ({
      ...entry,
      from: getLocalDateTime(entry.from, 'YYYY-MM-DDTHH:mm:ss'),
      until: getLocalDateTime(entry.until, 'YYYY-MM-DDTHH:mm:ss')
    })
  );
}

export function fetchFormattedFormValue<T extends { from?: string; until?: string }>
(formValue: T): T {
  return  {
    ...formValue,
    from: getUTCDateTime(formValue.from, 'YYYY-MM-DDTHH:mm:ss'),
    until: getUTCDateTime(formValue.until, 'YYYY-MM-DDTHH:mm:ss')
  };
}


