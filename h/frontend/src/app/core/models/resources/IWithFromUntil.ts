export interface IWithFromUntil<T = string> {
  from: T;
  until?: T;
}
