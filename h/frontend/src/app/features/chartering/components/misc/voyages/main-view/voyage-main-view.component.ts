import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { sortBy } from 'lodash';
import { NvGridConfig } from 'nv-grid';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { map, switchMap, tap, takeUntil } from 'rxjs/operators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { Language } from 'src/app/core/models/language';
import { IVesselAccounting, IVesselAccountingListItem, VesselAccountingKey } from 'src/app/core/models/resources/IVesselAccounting';
import { IVoyage } from 'src/app/core/models/resources/IVoyage';
import { AccountService } from 'src/app/core/services/account.service';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { convertToMap } from 'src/app/shared/helpers/general';

@Component({
  selector: 'nv-voyage-main',
  templateUrl: './voyage-main-view.component.html',
  styleUrls: ['./voyage-main-view.component.scss']
})
export class VoyageMainComponent implements OnInit, OnDestroy {
  public searchValue = '';
  /**
   * used so we keep a backup and don't lose the values after filtering
   */
  private _vessels: IVesselAccountingListItem[] = [];
  /**
   * used to show the list filtered in the template
   */
  public viewVessels: IVesselAccountingListItem[] = [];


  public activeNode = null;
  public dataSource$: Observable<IVoyage[]>;
  public gridConfig: NvGridConfig;
  public selectedVesselAccountingId: number;
  public favotiteActive: boolean;
  public selectedVesselAccoutning: IVesselAccountingListItem;
  public wording = wording;
  public vesselTitle: string;
  public language: Language;

  private voyagesRoute: string;
  private allVesselAccountings: IVesselAccounting[];
  private allVesselAccountingsMap: Map<VesselAccountingKey, IVesselAccounting>;
  private favoriteVesselAccountingIds = new BehaviorSubject<number[]>([]);
  private componentDestroyed$ = new ReplaySubject<boolean>(1);

  constructor(
    private vesselAccountingRepo: VesselAccountingRepositoryService,
    private accountService: AccountService,
    private route: ActivatedRoute,
    private router: Router,
    private settingsService: SettingsService,
    private vesselMainService: VesselMainService,
  ) { }

  public ngOnInit() {
    this.voyagesRoute = url(sitemap.chartering.children.misc.children.voyages);
    this.language = this.settingsService.language;
    this.fetchVessels();

    this.route.queryParams.subscribe(params => {
      this.selectedVesselAccountingId = +params['vesselAccountingId'];
    });
  }

  public search() {
    if (!this.searchValue) {
      return this.viewVessels = this.favotiteActive
        ? this._vessels.filter(v => v.isFavorite)
        : this._vessels;
    }
    this.viewVessels = this._vessels.filter(n => {
      const title = `${n.vesselNo} ${n.vesselName.toLowerCase()} (${n.charterName?.toLowerCase()})
      ${n.vesselNo} ${n.vesselName.toLowerCase()} ${n.charterName?.toLowerCase()}`;
      return title.includes(this.searchValue.toLowerCase()) && (this.favotiteActive ? n.isFavorite : true);
    });
  }

  public vesselSelect(vesselAccounting: IVesselAccountingListItem) {
    const queryParams = { vesselAccountingId: vesselAccounting.id };
    this.selectedVesselAccoutning = vesselAccounting;
    this.vesselTitle = `
      ${wording.chartering.voyages[this.language]}:
      ${this.selectedVesselAccoutning.vesselNo.toString().padStart(4, '0')}
      ${this.selectedVesselAccoutning.vesselName}
      ${this.selectedVesselAccoutning.charterName ? `(${this.selectedVesselAccoutning.charterName})` : ''}
    `;
    this.router.navigate([this.voyagesRoute], { queryParams });
  }

  private fetchVessels() {
    this.vesselMainService.fetchVesselsWithClosestAccounting(true).pipe(
      tap(() => {
        const vesselAccountings: IVesselAccounting[] = this.vesselAccountingRepo
          .getStreamValue();
        this.allVesselAccountingsMap = convertToMap<
          IVesselAccounting,
          VesselAccountingKey
        >(vesselAccountings);
        this.allVesselAccountings = vesselAccountings;
      }),
      map(vesselAccountings => sortBy(vesselAccountings, 'vesselName')),
      switchMap((vesselAccountings) => {
        return this.accountService.getFavoriteVesselAccountings()
          .pipe(
            takeUntil(this.componentDestroyed$),
            tap(res => this.favoriteVesselAccountingIds.next(res)),
            map(favoriteVesselAccountingsIds => {
              const favoriteVesselIds: VesselKey[] = favoriteVesselAccountingsIds
                .map(vesselAccountingId => {
                  const vesselAccounting = this.allVesselAccountingsMap.get(vesselAccountingId);
                  return vesselAccounting?.vesselId;
                })
                .filter(v => v);
              return vesselAccountings.map(v => {
                return {
                  ...v,
                  isFavorite: favoriteVesselIds.includes(v.vesselId),
                };
              });
            })
          );
      }),
      tap(res => {
        this._vessels = this.viewVessels = res;
        if (this.selectedVesselAccountingId) {
          const vessel: IVesselAccountingListItem = this.viewVessels
            .find(v => v.id === this.selectedVesselAccountingId);
          if (vessel) {
            this.vesselSelect(vessel);
          } else {
            this.router.navigate([this.voyagesRoute]);
          }
        }
      }),
    ).subscribe();
  }

  public filterFavorites() {
    this.favotiteActive = !this.favotiteActive;
    if (this.favotiteActive) {
      this.viewVessels = this._vessels.filter(v => v.isFavorite);
    } else {
      this.viewVessels = this._vessels;
    }
    this.search();
  }

  public toggleFavorite(vesselAccounting: IVesselAccountingListItem) {
    if (vesselAccounting.isFavorite) {
      const vesselAccountingIdsToRemoveAsFavorite: VesselAccountingKey[] = this.allVesselAccountings
        .filter(v => v.vesselId === vesselAccounting.vesselId)
        .map(v => v.id);
      this.accountService.removeFavoriteVesselAccounting(vesselAccountingIdsToRemoveAsFavorite)
        .subscribe(() => this.search());
    } else {
      this.accountService.setFavoriteVesselAccountings([
        ...this.favoriteVesselAccountingIds.value,
        vesselAccounting.id
      ]).subscribe(() => this.search());
    }
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
