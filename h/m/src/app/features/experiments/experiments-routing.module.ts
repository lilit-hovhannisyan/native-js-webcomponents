import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaveCheckGuard } from '@core-guards/leave-check.guard';

import { ExperimentsComponent } from './experiments.component';

const routes: Routes = [{ path: '', component: ExperimentsComponent, canDeactivate: [LeaveCheckGuard] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExperimentsRoutingModule {}
