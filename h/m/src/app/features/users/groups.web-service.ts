import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseWebService } from '../../core/services/base.web-service';

import { ArrayResponseI } from '@core-interfaces/array-response.interface';

import { GroupModel } from '@shared/models/groups/group.model';
import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { UserModel } from '@shared/models/user/user.model';
import { constructUrl } from '@shared/utils';
import { BaseSearchModel } from '@shared/models/search/search-base.model';

@Injectable()
export class GroupsWebService {
  constructor(private baseWebService: BaseWebService) {}

  getMeGroups(from?: number, to?: number): Observable<ArrayResponseI<GroupModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/groups`, from, to);
    return this.baseWebService.getRequestForArray<GroupModel>(url, GroupModel);
  }

  getMeGroupsMembers(groupOID: string, skip?: number, top?: number): Observable<ArrayResponseI<UserModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/groups/${encodeURI(groupOID)}/members`, skip, top);
    return this.baseWebService.getRequestForArray<UserModel>(url, UserModel);
  }

  addMembersToMeGroup(groupOID: string, data: string[]): Observable<boolean> {
    return this.baseWebService.postRequest<boolean, string[]>(`${MARKETPLACE_BASE_API_URL}/groups/${encodeURI(groupOID)}/members`, data);
  }

  removeMembersFromMeGroup(groupOID: string, data: string[]): Observable<boolean> {
    return this.baseWebService.deleteRequest(`${MARKETPLACE_BASE_API_URL}/groups/${encodeURI(groupOID)}/members`, data);
  }

  searchGroups(data: BaseSearchModel, skip?: number, top?: number): Observable<ArrayResponseI<GroupModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/groups/search`, skip, top);
    return this.baseWebService.postRequestForArray<GroupModel, BaseSearchModel>(url, data, GroupModel);
  }

  searchMembersInGroups(groupOID: string, data: BaseSearchModel, skip?: number, top?: number): Observable<ArrayResponseI<UserModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/groups/${encodeURI(groupOID)}/members/search`, skip, top);
    return this.baseWebService.postRequestForArray<UserModel, BaseSearchModel>(url, data, UserModel);
  }
}
