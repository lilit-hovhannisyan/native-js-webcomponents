import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { BaseComponentService } from '../../../../../../core/services/base-component.service';
import { sitemap } from '../../../../../../core/constants/sitemap/sitemap';
import { Validators } from '../../../../../../core/classes/validators';
import { ResourceDetailView } from '../../../../../../core/abstracts/ResourceDetailView';
import { CategoryRepositoryService } from 'src/app/core/services/repositories/category-repository.service';
import { ICategory, ICategoryCreate, CategoryKey } from 'src/app/core/models/resources/ICategory';
import { Observable, of } from 'rxjs';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class CompanyCategoriesDetailViewComponent extends ResourceDetailView<
  ICategory, ICategoryCreate
> implements OnInit {
  private allDeLabels: string[] = [];
  private allEnLabels: string[] = [];

  protected repoParams = {};
  public routeBackUrl: string;
  private disabledFieldsArray: string[] = [];
  protected resourceDefault = {
    isActive: true,
    isSystemDefault: false,
  } as ICategory;
  private hasConnection = false;
  private id: string;
  public formElement: ElementRef;

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public categoryRepo: CategoryRepositoryService,
    baseComponentService: BaseComponentService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef
  ) {
    super(categoryRepo, baseComponentService);
  }

  public ngOnInit(): void {
    this.routeBackUrl = this.url(sitemap.system.children.misc.children.companyCategories);
    this.id = this.route.snapshot.params.id;

    if (!this.baseComponentService.authService.getSessionInfo().user.isSuperAdmin) {
      this.disabledFieldsArray = [ 'isSystemDefault' ];
    }

    /**
     * Needed to fetch all categories before init to set unique-value validator for
     * labelEn and labelDe fields
     */
    this.categoryRepo.fetchAll({}).subscribe(allCategories => {
      // on time of edit mode it can have the value on the backend
      const otherCategories = allCategories.filter(c => +c.id !== +this.id);

      this.allDeLabels = otherCategories.map(c => c.labelDe);
      this.allEnLabels = otherCategories.map(c => c.labelEn);

      this.id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(this.id);
    });
  }

  private checkCategoryHasConnection(): void {
    this.categoryRepo.fetchAll({ queryParams: { withCompanyConnection: true } })
      .subscribe(connectedCategories => {
        this.hasConnection = connectedCategories.some(c => c.id === +this.id);
      });
  }

  public getTitle = () => this.resource.displayLabel[this.settingsService.language];

  private labelsEqualityChecker = (controlValue: string, itemValue: string) => {
    return controlValue?.trim().toLowerCase() === itemValue?.trim().toLowerCase();
  }

  public init(companyCategory: ICategory = this.resourceDefault): void {
    this.initForm(this.fb.group({
      labelEn: [companyCategory.labelEn, [
        Validators.required,
        Validators.maxLength(100),
        Validators.unique(this.allEnLabels, this.labelsEqualityChecker),
      ]],
      labelDe: [companyCategory.labelDe, [
        Validators.required,
        Validators.maxLength(100),
        Validators.unique(this.allDeLabels, this.labelsEqualityChecker),
      ]],
      isActive: [companyCategory.isActive],
      isSystemDefault: [companyCategory.isSystemDefault],
    }));

    this.setDisabledFields(this.disabledFieldsArray);
    this.checkCategoryHasConnection();
  }

  private showDeactivationWarning(isActive: boolean): Observable<boolean> {
    return (this.hasConnection && !isActive && this.resource?.isActive)
      ? this.confirmationService.warning({
        wording: wording.system.companyCategoryDeactivationWarning,
        cancelButtonWording: wording.general.cancel,
        okButtonWording: wording.system.saveAndDeactivateCategory,
      })
      : of(true);
  }

  public submit(): void {
    if (!this.form.valid) { return; }

    const { value } = this.form as { value: ICategory };

    this.showDeactivationWarning(value?.isActive).subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      const freshEntity: ICategory = this.creationMode
      ? { ...value }
      : { ...this.resource, ...value };

      this.creationMode
        ? this.createResource(freshEntity)
        : this.updateResource(freshEntity);
    });
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
