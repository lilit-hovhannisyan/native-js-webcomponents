import { NvGridButtonsPosition, NvGridConfig, NvFilterControl, NvColumnDataType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getGridConfig = (): NvGridConfig => {
  return ({
    title: wording.chartering.typesOfMaritimeServices,
    gridName: 'vesselSettingsConfig',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    showPaging: false,
    clearFiltersButton: true,
    columns: [
      {
        key: 'name',
        title: wording.general.service,
        width: 250,
        isSortable: true,
        filter: {
          values: [],
        },
      },
    ],
  });
};
