import { Component, OnDestroy, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ReplaySubject, Observable, forkJoin } from 'rxjs';
import { takeUntil, startWith, tap } from 'rxjs/operators';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { ACCOUNTING_RELATIONS } from 'src/app/core/models/enums/accounting-relations';
import { ACCOUNTING_TYPES } from 'src/app/core/models/enums/accounting-types';
import { DEBITOR_CREDITOR } from 'src/app/core/models/enums/debitor-creditor';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { StructureRepositoryService } from 'src/app/core/services/repositories/structure-repository.service';
import { getAccountingRelationTypesOptions, getCreditorDebitorWithNoneOptions } from 'src/app/features/system/components/settings/global/options';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { BookingAccountKey, IBookingAccount, IBookingAccountCreate } from '../../../../../../core/models/resources/IBookingAccount';
import { AuthenticationService } from './../../../../../../authentication/services/authentication.service';
import { IStartEndRange, ISettings } from './../../../../../../core/models/resources/ISettings';
import { SettingsRepositoryService } from './../../../../../../core/services/repositories/settings-repository.service';
import { IMasterStructure } from 'src/app/core/models/resources/IMasterStructure';
import { AccountCostTypeMainService } from '../../../../../../core/services/mainServices/account-cost-type-main.service';
import { ICostType } from '../../../../../../core/models/resources/ICostTypes';
import { ACCOUNT_TYPE_ACCRUALS } from 'src/app/core/models/enums/account-type-accruals';
import { AccountValidatorsService } from '../../../../../../core/services/validators/account-validators.service';
import { CurrencyValidatorsService } from 'src/app/core/services/validators/currency-validators.service';

@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class BookingAccountsDetailViewComponent
  extends ResourceDetailView<IBookingAccount, IBookingAccountCreate>
  implements OnInit, OnDestroy {

  public repoParams = {};
  public routeBackUrl = url(sitemap.accounting.children.booking.children.bookingAccounts);
  public form: FormGroup;
  public accountingTypeBitmask: number;
  private globalSettings: ISettings;
  private disabledFieldsArray: string[] = [];
  protected resourceDefault = {
    isActive: true,
    accountTypeAccruals: ACCOUNT_TYPE_ACCRUALS.AccountPrepaidExpenses
  } as IBookingAccount;

  protected isCloningAllowed = true;
  protected fieldsToClone = [
    'labelDe',
    'labelEn',
    'currencyId',
    'accountingType',
    'isCostAccounting',
    'isOpenItem',
    'isOpex',
    'assetAccount',
  ];
  public accountWording = this.wording.accounting;
  public ACCOUNTING_RELATIONS = ACCOUNTING_RELATIONS;
  public ACCOUNT_TYPE_ACCRUALS = ACCOUNT_TYPE_ACCRUALS;
  public DEBITOR_CREDITOR = DEBITOR_CREDITOR;
  private usedStructuresAccountingTypes: number[];
  private id: number | string;
  public bookingAccountId: number;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  private uncheckedAccountTypes: { [key: number]: boolean } = {};
  public accountingRelationTypesOptions: ISelectOption[] = [];
  public debitorCreditorTypesOptions: ISelectOption[] = [];
  public isInBalanceAccountRange = false;
  public accountCostRelations: ICostType[] = [];
  public isInProfitAndLossEarningsRange = false;
  public isInProfitAndLossExpensesRange = false;

  public formElement: ElementRef;

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    private currencyRepo: CurrencyRepositoryService,
    private settingsRepo: SettingsRepositoryService,
    public authService: AuthenticationService,
    bookingAccountRepo: BookingAccountRepositoryService,
    baseComponentService: BaseComponentService,
    private currencyValidatorsService: CurrencyValidatorsService,
    private masterStructuresRepo: StructureRepositoryService,
    private accountCostTypeMainService: AccountCostTypeMainService,
    private accountValidatorsService: AccountValidatorsService,
    public el: ElementRef,
    private cdr: ChangeDetectorRef
  ) {
    super(bookingAccountRepo, baseComponentService);
  }

  public ngOnInit(): void {
    const { id } = this.route.snapshot.params;
    this.id = id;
    this.bookingAccountId = id === 'new' ? id : +id;
    this.accountingRelationTypesOptions = getAccountingRelationTypesOptions();
    this.debitorCreditorTypesOptions = getCreditorDebitorWithNoneOptions();

    forkJoin([
      this.settingsRepo.fetchAll(),
      this.currencyRepo.fetchAll({}),
      this.fetchUsedStructuresIds(),
    ]).subscribe(([globalSettings]) => {
      this.globalSettings = globalSettings;

      this.id === 'new'
        ? this.enterCreationMode()
        : this.loadResource(this.id);
    });

    // `accountingRelation` and `debitorCreditorType` are auto-filling fields
    this.disabledFieldsArray.push('accountingRelation', 'debitorCreditorType',
      'isAccrualDefaultPrepaid', 'isAccrualDefaultDeferred');
  }

  private fetchUsedStructuresIds(): Observable<IMasterStructure[]> {
    return this.masterStructuresRepo.fetchAll({}).pipe(
      tap(structures => {
        this.usedStructuresAccountingTypes = structures
          .filter(s => s.nodes.find(n => n.bookingAccountId === +this.id))
          .map(s => s.accountingType);
      })
    );
  }

  public getTitle = () => `${this.resource.no} ${this.resource[this.labelKey]}`;

  public init(bookingAccount: IBookingAccount = this.resourceDefault): void {
    this.initForm(this.fb.group({
      no: [bookingAccount.no, [
        Validators.required,
        Validators.min(0),
        Validators.max(100000000),
        Validators.wholeNumber,
        this.isWithinSettingsRange,
      ]],
      isCostAccounting: [bookingAccount.isCostAccounting],
      isOpenItem: [bookingAccount.isOpenItem],
      isOpex: [bookingAccount.isOpex],
      remark: [bookingAccount.remark, [Validators.maxLength(1000)]],
      accountingCheckboxGroup: this.fb.group({
        isHgbAccounting: [this.isAccountingTypeEnabled(ACCOUNTING_TYPES.HGB, bookingAccount.accountingType)],
        isIfrsAccounting: [this.isAccountingTypeEnabled(ACCOUNTING_TYPES.IFRS, bookingAccount.accountingType)],
        isUsgaapAccounting: [this.isAccountingTypeEnabled(ACCOUNTING_TYPES.USGAAP, bookingAccount.accountingType)],
        isEBalanceAccounting: [this.isAccountingTypeEnabled(ACCOUNTING_TYPES.EBalance, bookingAccount.accountingType)],
      }, { validator: Validators.requiredCheckboxes(1, wording.accounting.minOneAccountingType) }),
      currencyId: [bookingAccount.currencyId, [
        this.currencyValidatorsService.doesCurrencyExist,
        this.currencyValidatorsService.isCurrencyActive
      ]],
      assetAccount: [bookingAccount.assetAccount],
      isSwitchingAccount: [bookingAccount.isSwitchingAccount],
      isActive: [bookingAccount.isActive],
      labelDe: [bookingAccount.labelDe, [Validators.maxLength(150), Validators.required]],
      labelEn: [bookingAccount.labelEn, [Validators.maxLength(150), Validators.required]],
      accountingType: [bookingAccount.accountingType],
      changeIsOpenForMandators: [bookingAccount.changeIsOpenForMandators],
      changeIsOpexForMandators: [bookingAccount.changeIsOpexForMandators],
      changeRestrictedCurrencyForMandators: [bookingAccount.changeRestrictedCurrencyForMandators],
      debitorCreditorType: [ // the field is always disabled
        bookingAccount.debitorCreditorType
        || DEBITOR_CREDITOR.NONE, // always `NONE` for custom created ones
        Validators.required
      ],
      accountingRelation: [bookingAccount.accountingRelation],
      isAccruedAndDeferredItem: [bookingAccount.isAccruedAndDeferredItem],
      isAccrualDefaultDeferred: this.bookingAccountId === this.globalSettings.defaultAccountDeferredIncomeId,
      isAccrualDefaultPrepaid: this.bookingAccountId === this.globalSettings.defaultAccountPrepaidExpensesId,
      accountTypeAccruals: bookingAccount.accountTypeAccruals,
      accrualBookingAccountId: [bookingAccount.accrualBookingAccountId, [
        this.accountValidatorsService.doesBookingAccountExist,
        this.accountValidatorsService.isBookingAccountActive
      ]]
    }));

    this.subscribeToChanges('currencyId', 'changeRestrictedCurrencyForMandators');
    this.subscribeToChanges('isOpenItem', 'changeIsOpenForMandators');
    this.subscribeToChanges('isOpex', 'changeIsOpexForMandators');
    this.onAccountingChange();

    if (bookingAccount.no && this.isInDebitorCreditorRange(bookingAccount.no)) {
      this.disabledFieldsArray.push('no', 'isOpenItem');
    }

    this.setDisabledFields(this.disabledFieldsArray);
    this.listenNoChange();

    // check dataset items
    this.accountCostTypeMainService.bookingIdRelatedCostTypeList$.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(relatedCt => {
      this.accountCostRelations = relatedCt;
    });
  }

  public subscribeToChanges(controlName: string, targetControlName: string) {
    const control = this.form.get(controlName);
    const targetControl = this.form.get(targetControlName);
    control.valueChanges
      .subscribe((value) => {
        if (
          !this.creationMode
          && this.editMode
          && value !== this.resource[controlName]
          && control.valid
        ) {
          this.changeForAllMandators(targetControlName);
        } else {
          targetControl.setValue(false);
        }
      });
  }

  private listenNoChange(): void {
    const { no } = this.form.controls;

    no.valueChanges.pipe(startWith(no.value)).subscribe((value: number) => {
      const balanceRanges = this.globalSettings.system.accountingRelationRanges
        .filter(range => range.type === ACCOUNTING_RELATIONS.BALANCE);

      this.isInBalanceAccountRange = balanceRanges.some(range => this.isInRange(value, range));

      const profitAndLossEarningsRanges = this.globalSettings.system.accountingRelationRanges
        .filter(range => range.type === ACCOUNTING_RELATIONS.PROFITANDLOSSEARNINGS);

      this.isInProfitAndLossEarningsRange = profitAndLossEarningsRanges.some(range => this.isInRange(value, range));

      const profitAndLossExpensesRanges = this.globalSettings.system.accountingRelationRanges
        .filter(range => range.type === ACCOUNTING_RELATIONS.PROFITANDLOSSEXPENSES);

      this.isInProfitAndLossExpensesRange = profitAndLossExpensesRanges.some(range => this.isInRange(value, range));
    });
  }

  public isAccountingTypeEnabled(accountingBitmask: number, accountingType?: number): boolean {
    const typeBitmask = accountingType || this.resource?.accountingType;

    return (typeBitmask & accountingBitmask) === accountingBitmask;
  }

  public changeForAllMandators(controlName: string): void {
    this.confirmationService.confirm({
      wording: wording.accounting.changeForAllMandators,
      okButtonWording: wording.general.changeForAll,
      cancelButtonWording: wording.general.cancel,
    }).subscribe(confirmed => confirmed && this.form.get(controlName).setValue(true));
  }

  public submit(): void {
    if (!this.form.valid) { return; }

    if (this.form.get('accountingRelation').value !== ACCOUNTING_RELATIONS.BALANCE
      && this.form.get('isCostAccounting').value
      && this.accountCostRelations.length === 0
    ) {
      return;
    }

    const rawValue = this.form.getRawValue();
    const value = {
      ...rawValue,
      /**
       * When the NO field isn't in range 'Balance' and the 'isSwitchingAccount'
       * is true, the backend will throw an error
       */
      isSwitchingAccount: this.isInBalanceAccountRange
        ? rawValue.isSwitchingAccount
        : false,
      accountTypeAccruals: this.isInBalanceAccountRange
        ? rawValue.accountTypeAccruals
        : null,
      isAccruedAndDeferredItem: this.isInBalanceAccountRange
        ? rawValue.isAccruedAndDeferredItem
        : false,
      accrualBookingAccountId: this.isInBalanceAccountRange
        ? null
        : rawValue.accrualBookingAccountId,
      isAccrualDefaultPrepaid: null,
      isAccrualDefaultDeferred: null
    };
    const { accountingCheckboxGroup, ...bookingAccountValue } = value;
    bookingAccountValue.accountingType = this.getAccountingTypeBitmask(); // todo: accountingType is unresolved

    if (this.creationMode) {
      this.createResource(bookingAccountValue).subscribe(bookingAccount => {
        this.id = bookingAccount.id;

        // create ct relations
        this.createCtRelations(bookingAccount.id);
      });
    } else {
      this.updateResource({ ...this.resource, ...bookingAccountValue }).subscribe(() => {
        this.fetchUsedStructuresIds().subscribe();

        // update ct relations
        this.createCtRelations(this.resource.id);
      });
    }
  }

  private createCtRelations(id: BookingAccountKey): void {
    if (this.form.get('accountingRelation').value !== ACCOUNTING_RELATIONS.BALANCE) {
      this.accountCostTypeMainService.createCtRelations(id);
    }
  }

  public accountNumberChanged(_accountNumber: string) {
    const accNum = _accountNumber && parseInt(_accountNumber, 10);
    if (!accNum) { return; } // if input is not a number, stop here

    const relationRange = this.globalSettings.system.accountingRelationRanges.find(range => this.isInRange(accNum, range));
    this.form.patchValue({ accountingRelation: relationRange ? relationRange.type : undefined });
  }

  public isWithinSettingsRange = (c: AbstractControl | null): ValidationErrors | null => {
    const { value } = c;
    if (value) {
      const credDebRange = this.isInDebitorCreditorRange(value);
      if (credDebRange) {
        const { start, end } = credDebRange;
        return {
          numberIsWithinRange: {
            message: setTranslationSubjects(
              wording.general.numberIsWithinRange,
              { from: start, to: end },
            )
          }
        };
      }
      return null;
    }
  }

  private isInDebitorCreditorRange(value: number | string): IStartEndRange<DEBITOR_CREDITOR> | null {
    const numValue = +value;
    if (!numValue) {
      return null;
    }

    return this.globalSettings.system.debitorCreditorRanges
      .find(range => this.isInRange(numValue, range));
  }

  public isInRange(num: number, range: IStartEndRange<any>) {
    return num >= range.start && num <= range.end;
  }

  public getAccountingTypeBitmask(accountingTypes?: { [key: string]: number }): number {
    let sum = 0;
    const boxes = accountingTypes
      ? accountingTypes
      : this.form.value.accountingCheckboxGroup;

    if (boxes.isHgbAccounting) { sum += ACCOUNTING_TYPES.HGB; }
    if (boxes.isIfrsAccounting) { sum += ACCOUNTING_TYPES.IFRS; }
    if (boxes.isUsgaapAccounting) { sum += ACCOUNTING_TYPES.USGAAP; }
    if (boxes.isEBalanceAccounting) { sum += ACCOUNTING_TYPES.EBalance; }

    return sum;
  }

  public onAccountingChange(): void {
    const { accountingCheckboxGroup } = this.form.controls;
    accountingCheckboxGroup.valueChanges.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(value => {
      if (!this.form.value.accountingCheckboxGroup) {
        return;
      }
      const accountingTypeBitmask = this.getAccountingTypeBitmask(value);
      let isMessageShown = false;
      this.usedStructuresAccountingTypes.forEach(accountingType => {
        if (isMessageShown) {
          return;
        }

        const isInValidChange = (accountingTypeBitmask & accountingType) !== accountingType;

        if (isInValidChange && !this.uncheckedAccountTypes[accountingType]) {
          isMessageShown = true;
          this.confirmationService.confirm({
            wording: wording.accounting.accountWillBeRemovedFromMasterStructures,
            okButtonWording: wording.general.saveAndDelete,
            cancelButtonWording: wording.general.cancel,
          }).subscribe(isConfirmed => {
            if (!isConfirmed) {
              this.setAccountingTypes(
                accountingCheckboxGroup as FormGroup,
                accountingTypeBitmask + accountingType
              );
              this.uncheckedAccountTypes[accountingType] = !this.isAccountingTypeEnabled(
                accountingType,
                accountingTypeBitmask + accountingType,
              );
            }
          });
        }

        this.uncheckedAccountTypes[accountingType] = !this.isAccountingTypeEnabled(
          accountingType,
          accountingTypeBitmask,
        );
      });
    });
  }

  public onIsCostAccountingChange(state: boolean): void {
    if (this.creationMode || this.form.get('accountingRelation').value === 0) {
      return;
    }

    // check if there are data sets in cost accounting table
    if (this.accountCostRelations.length) {
      this.openWarningMessageForCostTypeRelation();
    }
  }

  private openWarningMessageForCostTypeRelation(): void {
    this.confirmationService.warning({
      wording: wording.accounting.costTypeRelationWarning,
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
    }).subscribe(confirmed => {
      if (!confirmed) {
        this.form.get('isCostAccounting').patchValue(true);
        return;
      }

      this.accountCostTypeMainService.deleteCtRelations();
    });
  }

  private setAccountingTypes(formGroup: FormGroup, bitmask: number): void {
    const {
      isHgbAccounting,
      isIfrsAccounting,
      isUsgaapAccounting,
      isEBalanceAccounting,
    } = formGroup.controls;

    isHgbAccounting.setValue(
      this.isAccountingTypeEnabled(ACCOUNTING_TYPES.HGB, bitmask),
      { emitEvent: false }
    );
    isIfrsAccounting.setValue(
      this.isAccountingTypeEnabled(ACCOUNTING_TYPES.IFRS, bitmask),
      { emitEvent: false }
    );
    isUsgaapAccounting.setValue(
      this.isAccountingTypeEnabled(ACCOUNTING_TYPES.USGAAP, bitmask),
      { emitEvent: false }
    );
    isEBalanceAccounting.setValue(
      this.isAccountingTypeEnabled(ACCOUNTING_TYPES.EBalance, bitmask),
      { emitEvent: false }
    );
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

}
