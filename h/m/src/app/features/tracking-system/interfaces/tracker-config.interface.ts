interface TrackerConfigI {
  appVersion: string;
  trackerEnabled: boolean;
  enableDebugging?: boolean;
}

export default TrackerConfigI;
