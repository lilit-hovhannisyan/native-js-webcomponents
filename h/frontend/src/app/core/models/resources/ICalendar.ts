import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';

export type CalendarKey = number;

interface ISharedProps extends IWithLabels {
  isSystemDefault: boolean;
}

export interface ICalendarCreate extends ISharedProps { }

export interface ICalendarBase extends ISharedProps, IResource<CalendarKey> { }

export interface ICalendarDTO extends ICalendarBase { }

export interface ICalendar extends ICalendarDTO, IWithLabel { }
