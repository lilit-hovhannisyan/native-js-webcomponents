import { ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { IWording } from 'src/app/core/models/resources/IWording';

export const inOrder = (
  first: string,
  second: string,
  third: string,
  forth: string,
  errorKey: string,
  message: IWording = wording.system.valuesMustBeInCorrectOrder
): ValidatorFn => {
  return (fg: FormGroup): ValidationErrors | null => {
    const controls =
      [
        fg.get(first),
        fg.get(second),
        fg.get(third),
        fg.get(forth)
      ];

    const numsArray = [];

    controls.forEach(c => {
      if (c && c.value) {
        numsArray.push(+c.value);
      }
    });

    const isArraySorted = numsArray.every((val, index) => {
      if (index === numsArray.length - 1) {
        return true;
      }
      return val < numsArray[index + 1];
    });

    return isArraySorted
      ? null
      : {
        [errorKey]: {
          message
        }
      };
  };
};
