import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';
import { CompanyKey } from './ICompany';
import { CAPITAL_TYPE } from '../enums/company-legal-form';

export type CompanyLegalEntityKey = number;

interface ISharedProps {
  // Commercial Register
  companyId: CompanyKey;
  hra?: number;
  hrb?: number;
  commercialRegister?: string;
  dateOfEntry?: string;
  sumOfContributions?: number;
  shareCapital?: number;
  currencyId?: number;
  capitalType?: CAPITAL_TYPE;
  sumOfNonParValueShares?: number;
  companyNumber?: string;
  partners: CompanyKey[];
  formerCompanyId: CompanyKey;

  // Classification
  groupOfCompanyId?: number;
  isShellCompany?: boolean;
  isDormantCompany?: boolean;
  isUnitaryLimitedPartnership?: boolean;

  // Remark
  remark?: string;

  // Status
  isPreliminaryInsolvencyProceeding?: boolean;
  preliminaryInsolvencyProceedingStartDate?: string;
  preliminaryInsolvencyAdministrator?: string;
  isInsolvencyProceeding?: boolean;
  insolvencyProceedingStartDate?: string;
  isInLiquidation?: boolean;
  liquidationStartDate?: string;
  liquidator?: string;
  isDeleted?: boolean;
  deletedStartDate?: string;
  isSold?: boolean;
  soldStartDate?: string;
  buyer?: string;
  _meta: string;
}

export interface ICompanyLegalEntityBase extends ISharedProps, IResource<CompanyLegalEntityKey> { }

export interface ICompanyLegalEntityCreate extends ISharedProps { }

export interface ICompanyLegalEntityDTO extends ICompanyLegalEntityBase { }

export interface ICompanyLegalEntity extends ICompanyLegalEntityDTO { }

export interface ICompanyLegalEntityFull extends ICompanyLegalEntity, IWithLabel { }
