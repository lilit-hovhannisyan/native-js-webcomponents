import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { MeIconsProviderModule } from '@shared/modules/me-icons.module';

import { MeDocumentLineComponent } from './me-document-line.component';
import { MeContextMenuModule } from '../me-context-menu/me-context-menu.module';
import { MeEllipsisRowModule } from '../me-ellipsis-row/me-ellipsis-row.module';

@NgModule({
  declarations: [MeDocumentLineComponent],
  imports: [CommonModule, MeEllipsisRowModule, MeContextMenuModule, FormsModule, MatCheckboxModule, MeIconsProviderModule],
  exports: [MeDocumentLineComponent]
})
export class MeDocumentLineModule {}
