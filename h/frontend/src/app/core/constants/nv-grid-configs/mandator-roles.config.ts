import { NvFilterControl, NvGridButtonsPosition, NvGridConfig, NvColumnDataType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getGridConfig = (): NvGridConfig => {

  return ({
    gridName: 'userRolesGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'displayLabel',
        title: wording.general.name,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'description',
        title: wording.general.description,
        width: 450,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isSystemDefault',
        title: wording.general.systemDefault,
        width: 200,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      }
    ]
  });
};
