import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { wording } from 'src/app/core/constants/wording/wording';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'openItemAccounts',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 10
    },
    columns: [
      {
        key: 'no',
        title: wording.accounting.no,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wording.accounting.accountName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isOpenItem',
        title: wording.accounting.openItem,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editBookingAccount',
        tooltip: wording.general.edit,
        actOnDoubleClick: canPerform(Operations.UPDATE),
        func: actions.edit,
        hidden: !canPerform(Operations.UPDATE),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteBookingAccount',
        tooltip: wording.general.delete,
        func: actions.delete,
        hidden: !canPerform(Operations.DELETE),
      },
    ],
    toolbarButtons: [
      {
        title: wording.general.add,
        tooltip: wording.general.add,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.UPDATE),
        func: () => actions.add()
      }
    ],
  });
};
