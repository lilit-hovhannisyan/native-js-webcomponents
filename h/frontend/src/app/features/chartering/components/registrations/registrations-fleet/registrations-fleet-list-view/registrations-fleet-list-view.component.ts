import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { orderBy } from 'lodash';
import { NvGridConfig } from 'nv-grid';
import { combineLatest, forkJoin, Observable, Subject } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { getRegistrationsFleetGridConfig } from 'src/app/core/constants/nv-grid-configs/registrations-fleet.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { LoadingService } from 'src/app/core/services/loading.service';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';
import { VesselSettingsRepositoryService } from 'src/app/core/services/repositories/vessel-settings-repository.service';
import { VesselSpecificationRepositoryService, vesselTypes } from 'src/app/core/services/repositories/vessel-specification-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { IVesselAccountingSpecification } from 'src/app/features/technical-management/components/misc/stevedore-damages/list-view/vessels/vessels.component';
import { VesselMainService } from '../../../../../../core/services/mainServices/vessel-main.service';
import { getFilteredVesselsList, hasValidSettingRangeAndType } from './registrations-fleet-list-view.helper';
import { VesselRoleRepositoryService } from 'src/app/core/services/repositories/vessel-role.repository.service';
import { IVesselAccounting } from 'src/app/core/models/resources/IVesselAccounting';
import { IVesselSpecification } from 'src/app/core/models/resources/IVesselSpecification';
import { VESSEL_SETTING_TYPE } from 'src/app/core/models/enums/vessel-setting-type';
import { IVesselSetting } from 'src/app/core/models/resources/IVesselSetting';
import { RegistrationFleetMainService } from 'src/app/core/services/mainServices/registration-fleet-main.service';
import { IRegistrationFleet } from 'src/app/core/models/IRegistrationFleet';
import { VESSEL_TYPE } from 'src/app/core/models/enums/vessel-type';
import { VesselRepositoryService } from 'src/app/core/services/repositories/vessel-repository.service';
import { IVessel } from 'src/app/core/models/resources/IVessel';
import { findClosestWithFromUntil } from 'src/app/core/helpers/general';

export interface CheckedVesselsDict {
  // index signature parameter type cannot be a type alias
  [key: number]: boolean;
}

@Component({
  selector: 'nv-registrations-fleet-list-view',
  templateUrl: './registrations-fleet-list-view.component.html',
  styleUrls: ['./registrations-fleet-list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegistrationsFleetListViewComponent implements OnInit {
  public dataSource$: Observable<IRegistrationFleet[]>;
  public gridConfig: NvGridConfig;
  public wording = wording;
  public vesselTypes = vesselTypes;
  public vesselAccountings$: Observable<IVesselAccountingSpecification[]>;
  public vesselTypeCtrl = new FormControl([]);
  public vesselSearchCtrl = new FormControl();
  public activeCtrl = new FormControl(true);
  public inActiveCtrl = new FormControl(false);
  public isLoading$: Observable<boolean>;
  public checkedVessels: CheckedVesselsDict = {};
  private currentVesselAccountingsSpecifications: IVesselAccountingSpecification[] = [];
  private vesselChecked = new Subject<void>();
  private checkAllCalled: boolean;

  constructor(
    public gridConfigService: GridConfigService,
    private router: Router,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private vesselMainService: VesselMainService,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
    private loadingService: LoadingService,
    private vesselSpecification: VesselSpecificationRepositoryService,
    private settingsService: SettingsService,
    private vesselSettings: VesselSettingsRepositoryService,
    private vesselRoleRepo: VesselRoleRepositoryService,
    private registrationFleetMainService: RegistrationFleetMainService,
    private vesselsRepo: VesselRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.isLoading$ = this.loadingService.isLoading$;
    this.initNvGrid();
    this.fetchVesselAccountings();
    this.onAllSelected();
  }

  private fetchVesselAccountings(): void {
    forkJoin([
      this.vesselsRepo.fetchAll({}),
      this.vesselSpecification.fetchAll({}),
      this.vesselSettings.fetchAll({}),
      this.vesselAccountingRepo.fetchAll({}),
      this.vesselRoleRepo.fetchAll({}),
    ]).subscribe();
    this.vesselAccountings$ = combineLatest([
      combineLatest([
        this.vesselsRepo.getStream(),
        this.vesselAccountingRepo.getStream(),
        this.vesselSpecification.getStream({}),
        this.vesselSettings.getStream({}),
        this.vesselRoleRepo.getStream(),
      ]).pipe(map(([
        vessels,
        accountings,
        specifications,
        settings,
        roles,
      ]) => {
        const vesselsWithRole: IVessel[] = vessels.filter(v => {
          return this.vesselMainService.vesselIsAssignedToRole(v.id, roles);
        });

        const vesselsWithRoleAndValidSetting: IVessel[] =
          vesselsWithRole.filter(vessel => {

            const currentVesselSetting: IVesselSetting = settings.find(s => {
              if (s.vesselId !== vessel.id) {
                return false;
              }

              const isValid: boolean =
                !!(s.serviceType & VESSEL_SETTING_TYPE.Registration);

              return isValid;
            });
            return !!currentVesselSetting;
          });

        return vesselsWithRoleAndValidSetting
          .map((vessel: IVessel): IVesselAccountingSpecification => {
            const specificationsOfVessel: IVesselSpecification[] = orderBy(
              specifications
                .filter(s => s.vesselId === vessel.id),
              [s => s.from],
              ['desc']
            );

            const latestSpecification: IVesselSpecification = specificationsOfVessel
              ? specificationsOfVessel[0]
              : null;

            const currentVesselAccountings: IVesselAccounting[] = accountings
              .filter(accounting => accounting.vesselId === vessel.id);

            const currentVesselAccounting: IVesselAccounting
              = findClosestWithFromUntil(currentVesselAccountings);

            return {
              ...latestSpecification,
              ...currentVesselAccounting,
              vesselTypeDisplayValue: latestSpecification?.vesselType
                ? this.vesselTypes.find(v => v.value === latestSpecification.vesselType)
                  .displayLabel[this.settingsService.language]
                : '',
                imo: vessel.imo,
                id: vessel.id,
            };
          });
      })),
      this.vesselSearchCtrl.valueChanges.pipe(startWith(this.vesselSearchCtrl.value)),
    ]).pipe(
      map(([
        vesselAccountingsSpecifications,
        query,
      ]) => {
        this.currentVesselAccountingsSpecifications = vesselAccountingsSpecifications;
        const filteredVesselsList: IVesselAccountingSpecification[] =
          getFilteredVesselsList(
            vesselAccountingsSpecifications,
            query,
          );
        if (!this.checkAllCalled && filteredVesselsList.length) {
          this.checkAllCalled = true;
          this.onAllSelected();
        }
        return filteredVesselsList;
      })
    );
  }

  private initNvGrid(): void {
    const gridActions: IGridConfigActions = {
      edit: (row: IRegistrationFleet) => {
        this.router.navigate([`${this.router.url}/${row.vesselId}`]);
      },
      delete: (row: IRegistrationFleet) => this.delete(row),
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    const canPerform: (operation: number) => boolean = (operation: number) => {
      return this.authService.canPerformOperationOnCurrentUrl(operation);
    };

    this.gridConfig = getRegistrationsFleetGridConfig(gridActions, canPerform);
    this.registrationFleetMainService.fetchAll().subscribe();
    this.dataSource$ = this.registrationFleetMainService.getStream()
      .pipe(
        switchMap(listItems => {
          return combineLatest([
            this.vesselChecked.asObservable().pipe(startWith(this.checkedVessels)),
            this.vesselTypeCtrl.valueChanges
              .pipe(
                startWith(this.vesselTypeCtrl.value),
                map((selectedVesselTypes: VESSEL_TYPE[]) => selectedVesselTypes || []),
              ),
            this.activeCtrl.valueChanges.pipe(startWith(this.activeCtrl.value)),
            this.inActiveCtrl.valueChanges.pipe(startWith(this.inActiveCtrl.value)),
          ]).pipe(
            map(([
              vesselChecked,
              selectedVesselTypes,
              isActive,
              isNotActive,
            ]) => {
              return listItems.filter(i => {
                // #1 checked vessels check
                if (!this.checkedVessels[i.vesselId]) {
                  return false;
                }

                // #2 checked vessel types check
                const hasValidVesselType: boolean = selectedVesselTypes?.length
                  ? selectedVesselTypes.includes(i.vesselType)
                  : true;
                if (!hasValidVesselType) {
                  return false;
                }

                // #3 active/inactive checkboxes check
                let isRangeValid = false;

                if (isActive && isNotActive) {
                  // we show all when both checkboxes are checked
                  isRangeValid = true;
                } else if (!isActive && !isNotActive) {
                  // we show nothing
                  isRangeValid = false;
                } else if (isNotActive && !isActive) {
                  // have at least one non-active time slot(s)
                  // on vessel maindata “Settings” tab
                  // & service Registration = true
                  isRangeValid = !hasValidSettingRangeAndType(i.settings);
                } else if (isActive && !isNotActive) {
                  // Show vessels that have a timeslot in
                  // “Chartering and Operating /Misc /Vessels /Settings”
                  // which contains the current date and have Registration = true
                  isRangeValid = hasValidSettingRangeAndType(i.settings);
                }

                return isRangeValid;
              });
            })
          );
        })
      );
  }

  private delete = (fleetRegistration: IRegistrationFleet): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.chartering.removeRegistration,
      cancelButtonWording: wording.general.cancel,
      wording: wording.chartering.fleetRegistrationDeleteConfirm,
    }).subscribe(confirmed => {
      if (confirmed) {
        this.vesselsRepo.deleteRegistrationSetting(fleetRegistration.vesselId)
          .pipe(switchMap(() => this.registrationFleetMainService.fetchAll()))
          .subscribe();
      }
    });
  }

  public onAllSelected(): void {
    this.activeCtrl.patchValue(true);
    this.inActiveCtrl.patchValue(false);
    this.vesselSearchCtrl.patchValue(null);
    this.vesselTypeCtrl.patchValue([]);
    this.currentVesselAccountingsSpecifications.forEach(v => {
      this.checkedVessels[v.id] = true;
    });
    this.onVesselChecked();
  }

  public onAllCleared(): void {
    this.activeCtrl.patchValue(true);
    this.inActiveCtrl.patchValue(false);
    this.vesselSearchCtrl.patchValue(null);
    this.vesselTypeCtrl.patchValue([]);
    this.currentVesselAccountingsSpecifications.forEach(v => {
      this.checkedVessels[v.id] = false;
    });
    this.onVesselChecked();
  }

  public onVesselChecked() {
    this.vesselChecked.next();
  }
}
