import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { ICommissionType } from '../../../../../../core/models/resources/ICommissionType';
import { CommissionTypeRepositoryService } from 'src/app/core/services/repositories/commission-type-repository.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { CommissionTypeMainService } from 'src/app/core/services/mainServices/commissiont-type-main.service';
import { map } from 'rxjs/operators';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/commission-types.config';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';

@Component({
  templateUrl: './commission-types-list-view.component.html',
  styleUrls: ['./commission-types-list-view.component.scss']
})
export class CommissionTypesListViewComponent implements OnInit {
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<ICommissionType[]>;

  constructor(
    private commissionTypeService: CommissionTypeMainService,
    private commissionTypeRepo: CommissionTypeRepositoryService,
    public gridConfigService: GridConfigService,
    private authService: AuthenticationService,
    private router: Router,
    private confirmationService: ConfirmationService
  ) { }
  public ngOnInit() {
    this.commissionTypeService.fetchAll().subscribe();
    this.dataSource$ = this.commissionTypeService.getStream()
      .pipe(map(res => this.commissionTypeService.toRows(res)));

    const actions = {
      edit: (id) => this.router.navigate([`${this.router.url}/${id}`]),
      delete: this.deleteClicked,
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    this.gridConfig = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  public deleteClicked = (commissionType: ICommissionType) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: commissionType.displayLabel},
    }).subscribe(confirmed => confirmed && this.commissionTypeRepo.delete(commissionType.id, {}).subscribe());
  }

}
