import { IResource } from './IResource';
import { IBookingLineCreate, IBookingLine } from './IBookingLine';
import { InvoiceLedgerKey } from './IInvoiceLedger';

export type BookingKey = number;

interface ISharedProps {
  mandatorId: number;
  voucherTypeId: number;
  voucherDate: string;
  year: number;
  serialNo: string;
  period: number;
  invoiceDate: string;
  bookingLines: IBookingLine[];
  accountingType: number;
  defineInvoice: boolean;
  leadingAccount: boolean;
  accrualAccounting: boolean;
  voucherNumber?: string;
  userName?: string;
  bookingDate?: Date;
  invoiceLedgerId?: InvoiceLedgerKey;
}

export interface IBookingBase extends ISharedProps, IResource<BookingKey> { }

export interface IBookingCreate extends ISharedProps { }

export interface IBookingDTO extends IBookingBase { }

export interface IBooking extends IBookingDTO { }


