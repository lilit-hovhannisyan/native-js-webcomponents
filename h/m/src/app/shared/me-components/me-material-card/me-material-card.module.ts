import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NgModule } from '@angular/core';

import { MeContextMenuModule } from '../me-context-menu/me-context-menu.module';
import { MeEllipsisRowModule } from '../me-ellipsis-row/me-ellipsis-row.module';
import { MeMaterialCardComponent } from './me-material-card.component';
import { MeIconsModule } from '../me-icons/me-icons.module';
@NgModule({
  declarations: [MeMaterialCardComponent],
  imports: [CommonModule, FormsModule, MatCheckboxModule, MeEllipsisRowModule, MeContextMenuModule, MeIconsModule],
  exports: [MeMaterialCardComponent]
})
export class MeMaterialCardModule {}
