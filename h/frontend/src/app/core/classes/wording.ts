import { IWording } from '../models/resources/IWording';

export class Wording {

  public static concat(w1: IWording, w2: IWording, ws: string = ' '): IWording {
    return {
      en: `${w1.en}${ws}${w2.en}`,
      de: `${w1.de}${ws}${w2.de}`,
    };
  }

  /**
   *
   * @param wording any IWording
   * @param subject example: `{SUBJECT}`, `{}` is used by default
   * @param word word to replace both English and German translations
   */
  public static replace(wording: IWording, subject = '{}', word: IWording): IWording {
    return {
      en: wording.en.replace(subject, word.en),
      de: wording.de.replace(subject, word.de),
    };
  }
}
