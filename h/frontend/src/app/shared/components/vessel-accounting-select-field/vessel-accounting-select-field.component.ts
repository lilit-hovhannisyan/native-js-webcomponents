import { Component, OnInit, Input } from '@angular/core';
import { makeProvider, AbstractValueAccessor } from 'src/app/core/abstracts/value-accessor';
import { FormControl } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { IVesselAccounting } from 'src/app/core/models/resources/IVesselAccounting';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';
import { map, tap } from 'rxjs/operators';
import { MandatorKey } from 'src/app/core/models/resources/IMandator';
import { getVesselAccountingGridConfig } from 'src/app/core/constants/nv-grid-configs/vessel-accounting.config';

@Component({
  selector: 'nv-vessel-accounting-select-field',
  templateUrl: './vessel-accounting-select-field.component.html',
  styleUrls: ['./vessel-accounting-select-field.component.scss'],
  providers: [makeProvider(VesselAccountingSelectFieldComponent)],
})
export class VesselAccountingSelectFieldComponent extends AbstractValueAccessor implements OnInit {

  public getGridConfig = getVesselAccountingGridConfig;
  public resourceFormControl = new FormControl({ value: null });

  public vesselAccountings$: Observable<IVesselAccounting[]>;
  @Input() public queryParams = {};
  @Input() private onlyRelatedToMandatorId$: Observable<MandatorKey>;

  public getLabel = (vesselAccounting: IVesselAccounting) =>
    `${vesselAccounting.vesselNo} ${vesselAccounting.vesselName}`

  constructor(
    private vesselAccountingService: VesselAccountingRepositoryService,
  ) {
    super();
  }

  public ngOnInit(): void {
    if (this.onlyRelatedToMandatorId$) {
      this.vesselAccountings$ = combineLatest([
        this.onlyRelatedToMandatorId$,
        this.vesselAccountingService.fetchAll({}),
      ]).pipe(
        map(([mandatorId, vesselaccountings]) => {
          return mandatorId
            ? vesselaccountings.filter(v => v.mandatorId === mandatorId)
            : [];
        }
        ),
        tap(vesselaccountings => {
          if (vesselaccountings.length === 1 && !this.resourceFormControl.value && this.resourceFormControl.enabled) {
            this.onSelect(vesselaccountings[0].id);
            this.writeValue(vesselaccountings[0].id);
          } else if (!vesselaccountings.length && this.value) {
            this.onSelect(null);
            this.writeValue(null);
          }
        })
      );
    }
  }

  public onSelect(value: number): void {
    this.onChange(value);
  }

  public writeValue(value: number): void {
    this.resourceFormControl.setValue(value);
  }

  public setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.resourceFormControl.disable();
    } else {
      this.resourceFormControl.enable();
    }
  }

}
