import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Injector, OnDestroy, OnInit, TemplateRef } from '@angular/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { BaseLoggerComponent } from '@features/tracking-system';

import { RightSidebarSupportService } from '@core-services/right-sidebar-support.service';

@Component({
  selector: 'me-new-right-sidebar',
  templateUrl: './me-new-right-sidebar.component.html',
  styleUrls: ['./me-new-right-sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeNewRightSidebarComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  componentVersion: string = '0.0.1';

  private _unsubscribeOnDestroy$: Subject<void> = new Subject();

  content: TemplateRef<Component> | null = null;

  constructor(injector: Injector, private cd: ChangeDetectorRef, private rightSidebarSupportService: RightSidebarSupportService) {
    super(injector);
  }

  ngOnInit(): void {
    this.rightSidebarSupportService.rightSidebarContent$
      .pipe(takeUntil(this._unsubscribeOnDestroy$))
      .subscribe((content: TemplateRef<Component> | null) => {
        this.content = content;
        this.cd.detectChanges();
      });
  }

  ngOnDestroy(): object {
    this._unsubscribeOnDestroy$.next();
    this._unsubscribeOnDestroy$.complete();
    return super.ngOnDestroy();
  }
}
