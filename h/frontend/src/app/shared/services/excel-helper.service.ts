import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ExcelHelperService {
  private options;
  constructor() {
    this.options =  {
      title: '',
      filename: '',
    };
  }

  /**
   * @param  {{[key:string]:any}[]} data
   * @param  {string} filename
   * @returns void
   *
   * For setting headers in CSV set the first item of the array the headers object
   * Example: ```
   * [
   *   { firstField: 'First Field Header', secondField: 'Second Field Header' },
   *   { firstField: 'first field value', secondField: 'second field value' },
   *   // ...
   * ]
   * ```
   */
  public saveDataAsCsvFile(data: {[key: string]: any}[], filename: string): void {
    const csvBuffer = this.toCsvString(data);
    this.saveStringAsFile(csvBuffer, filename);
  }

  private toCsvString(data: {[key: string]: any}[]): string {
    return data.map(item => {
      return Object.values(item).map(v => `"${v}"`).join(',');
    }).join('\n');
  }

  private saveStringAsFile(buffer: string, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: 'text/csv'});
    FileSaver.saveAs(data, `${fileName}.csv`);
  }

  public saveBlobAsFile(blob: Blob, fileName: string): void {
    FileSaver.saveAs(blob, fileName);
  }
}
