import { NvColumnDataType, NvGridConfig, NvGridRowSelectionType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IMandatorLookup, mandatorNoModifier } from 'src/app/core/models/resources/IMandator';

export const gridConfig: (rowSelectionType?: NvGridRowSelectionType) => NvGridConfig = (
  rowSelectionType: NvGridRowSelectionType = NvGridRowSelectionType.RadioButton,
): NvGridConfig => {
  return {
    gridName: 'mandatorsSelectModalGrid',
    rowSelectionType,
    rowButtonsPosition: null,
    hideRefreshButton: true,
    hideSettingsButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 10,
      showTotalItems: true
    },
    columns: [
      {
        key: 'no',
        title: wording.accounting.no,
        width: 50,
        isSortable: true,
        filter: {
          values: []
        },
        customFormatFn: (mandator: IMandatorLookup) => mandatorNoModifier(mandator.no)
      },
      {
        key: 'name',
        title: wording.general.name,
        width: 200,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isoCode',
        title: wording.accounting.currency,
        width: 80,
        isSortable: true,
        filter: {
          values: [],
        }
      },
      {
        key: 'lastClosedYearView',
        title: wording.accounting.lastClosedYear,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText,
        }
      },
      {
        key: 'lastClosedPeriod',
        title: wording.accounting.lastClosedPeriod,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText,
        }
      },
      {
        key: 'hgb',
        title: wording.accounting.hgb,
        width: 55,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'ifrs',
        title: wording.accounting.ifrs,
        width: 55,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'usgaap',
        title: wording.accounting.usgaap,
        width: 80,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'companyName',
        title: wording.accounting.companyName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'isActive',
        title: wording.accounting.active,
        width: 65,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isCostAccounting',
        title: wording.accounting.costAccounting,
        width: 55,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isInvoiceWorkflow',
        title: wording.accounting.invoiceWorkflow,
        width: 55,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'startFiscalYear',
        title: wording.accounting.startFiscalYear,
        width: 130,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'mandatorTypeName',
        title: wording.accounting.mandatorType,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Select,
          values: []
        }
      }, {
        key: 'firstBookingYear',
        title: wording.accounting.firstBookingYear,
        width: 154,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'start',
        title: wording.accounting.start,
        width: 130,
        isSortable: true,
        dataType: NvColumnDataType.DateTime,
        filter: {
          values: []
        }
      }
    ],
    sortBy: 'no',
    isSortAscending: true,
  };
};
