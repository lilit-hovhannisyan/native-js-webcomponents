import { IResource } from './IResource';
import { ScopeOfRepresentationType } from '../enums/ScopeOfRepresentationType';
import { RepresentativeFunctionType } from '../enums/RepresentativeFunctionType';
import { ICompany, CompanyKey } from './ICompany';
import { IWording } from './IWording';

export type CompanyRepresentativeKey = number;

interface ISharedProps {
  companyId: CompanyKey;
  partnerId: CompanyKey;
  isActive?: boolean;
  from:	string;
  to?: string;
  releaseSec181_1?: boolean;
  releaseSec181_2?: boolean;
  functionType: RepresentativeFunctionType;
  scopeOfRepresentationType: ScopeOfRepresentationType;
  remark: string;
}

export interface ICompanyRepresentativeCreate extends ISharedProps { }

export interface ICompanyRepresentativeCreate extends ISharedProps { }

export interface ICompanyRepresentativeBase
  extends ISharedProps, IResource<CompanyRepresentativeKey> {}

export interface ICompanyRepresentativeDTO extends ICompanyRepresentativeBase { }

export interface ICompanyRepresentative extends ICompanyRepresentativeDTO { }

export interface ICompanyRepresentative extends ICompanyRepresentativeDTO { }

export interface ICompanyRepresentativeFull extends ICompanyRepresentative {
  company: ICompany;
  privatePersonCompany: ICompany;
}

export interface ICompanyRepresentativeRow extends ICompanyRepresentativeFull {
  firstName: string;
  lastName: string;
  functionTypeLabel: IWording;
  scopeOfRepresentationTypeLabel: IWording;
}
