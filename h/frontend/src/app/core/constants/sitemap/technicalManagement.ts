import { wording } from '../wording/wording';
import { SitemapNode } from './sitemap';

export const technicalManagement: SitemapNode = {
  path: 'technical-management',
  title: {en: 'Technical Management', de: 'Technical Management'},
  description: { en: 'Description...', de: 'Beschreibung...' },
  icon: 'cluster',
  zone: 'technicalManagement',
  children: {
    misc: {
      path: 'misc',
      title: {en: 'Misc', de: 'Sonstiges'},
      description: { en: 'Description...', de: 'Beschreibung...' },
      icon: 'assets/icons/88-512.png',
      zone: 'misc',
      children: {
        stevedoreDamages: {
          path: 'stevedore-damages',
          description: { en: 'desc..', de: 'desc..' },
          title: {en: 'Crewwork & Stevedore Damages', de: 'Crewwork & Stevedore Damages'},
          zone: 'stevedoreDamages',
          children: {
            id: {
              path: ':stevedoreDamageReportId',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: { en: 'Basic', de: 'Basic' },
                  description: { en: 'desc..', de: 'desc..' },
                },
                repairDocuments: {
                  path: 'repair-documents',
                  zone: '*',
                  title: { en: 'Repair/Documents', de: 'Reparatur/Dokumente' },
                  description: { en: 'desc..', de: 'desc..' },
                },
                administration: {
                  path: 'administration',
                  zone: '*',
                  title: { en: 'Administration', de: 'Verwaltung' },
                  description: { en: 'desc..', de: 'desc..' },
                },
              }
            }
          }
        },
        orderManagement: {
          path: 'order-management',
          description: { en: 'desc..', de: 'desc..' },
          title: {en: 'Order Management', de: 'Bestellwesen'},
          zone: 'orderManagement',
        },
        technicalShippings: {
          path: 'technical-budget',
          description: { en: 'desc..', de: 'desc..' },
          title: {en: 'Technical Budget', de: 'Technisches Budget'},
          zone: 'technicalShippings',
        },
      }
    },
    main: {
      path: 'main',
      title: {en: 'Main Data', de: 'Stammdaten'},
      description: { en: 'Description...', de: 'Beschreibung...' },
      icon: 'assets/icons/88-512.png',
      zone: 'main',
      children: {
        articleTypes: {
          path: 'article-types',
          description: { en: 'desc..', de: 'desc..' },
          title: {en: 'Article Types', de: 'Artikeltypen'},
          zone: 'articleTypes',
        },
        articleClasses: {
          path: 'article-classes',
          description: { en: 'desc..', de: 'desc..' },
          title: {en: 'Article Classes', de: 'Artikelklassen'},
          zone: 'articleClasses',
        },
        articleUnits: {
          path: 'article-units',
          description: { en: 'desc..', de: 'desc..' },
          title: {en: 'Article Units', de: 'Artikeleinheiten'},
          zone: 'articleUnits',
        },
        articles: {
          path: 'articles',
          description: { en: 'desc..', de: 'desc..' },
          title: {en: 'Articles', de: 'Artikel'},
          zone: 'articles',
        },
      }
    }
  }
};
