export const portfolioPath = '../l-hovhanni-assets/img/portfolio';
export const storyPath = '../l-hovhanni-assets/img/stories';

export const ph = {
    kar: 'Karen Petrosyan',
    yes: 'Yesayi Durmuzyan',
    mar: 'Marat Karapetyan',
    mane: 'Maneh Zakaryan',
    aram: 'Aram Kirakosyan',
    ash: 'Ashot Avagyan',
};
export const phLink = {
    kar: 'https://www.instagram.com/karenpetrosjan/',
    yes: 'https://www.instagram.com/yesayi.durmuzyan/',
    mar: 'https://www.instagram.com/karapetyan_photoworks/',
    mane: 'https://www.facebook.com/maneh.zakaryan',
    aram: 'https://www.instagram.com/aramkirakosyan/',
    ash: 'https://www.instagram.com/theashphoto/',
};

export const topics = {
    eu: 'eunoia...',
    lent: '#filmPhoto',
    sevan: '#sevan',
    hands: 'hands && senses',
    gram: 'gramaphone',
    curosity: 'curosity',
    retro: 'retro chick',
    inner: 'inner beauty',
    qochar: 'sculptural souls',
    nature: 'nature apocalipsis',
    mermaid: '#mermaid',
    details: '#detailsd',
    portrait: '#plain #portrait',
    abstract: '#abstract',
    backstage: '#backstage',
    dance: '#dance',
    self: '#selfportrait',
    red: 'red apples',
    fem : 'always keep your feminine',
    corona: 'isolation | quarantine | corona',
    signs: 'water | fire | air | earth'
};

export const exLink = {
    eu: {
        platform: 'Behancè',
        link: 'https://www.behance.net/gallery/93133889/eunoia?fbclid=IwAR3smM_zuqlCh9SaQ_9wvd-WrqeRZmt-Yzu0o25CWWcjWNR_66024mIS7eo'
    },
    inner: {
        platform: 'Behancè',
        link: 'https://www.behance.net/gallery/72305283/The-inside-beauty'
    },
    qochar: {
        platform: 'Behancè',
        link: 'https://www.behance.net/gallery/76440739/Sculptural-Souls'
    }
};

export const portfolioItems2 = [
    {
        src: 'lent2.jpg',
        photographer: 'kar',
        project: 'lent'
    },
    {
        src: 'hands1.jpg',
        photographer: 'yes',
        project: 'hands'
    },
    {
        src: 'gramaphone3.jpg',
        photographer: 'mar',
        project: 'gram',
        recent: true
    },
    {
        src: 'retro1.jpg',
        photographer: 'mar',
        project: 'retro'
    },
];

export const portfolioItems = [
    {
        src: 'lent2.jpg',
        photographer: 'kar',
        project: 'lent'
    },
    {
        src: 'hands1.jpg',
        photographer: 'yes',
        project: 'hands'
    },
    {
        src: 'gramaphone3.jpg',
        photographer: 'mar',
        project: 'gram'
    },
    {
        src: 'retro1.jpg',
        photographer: 'mar',
        project: 'retro'
    },
    {
        src: 'inner-beauty4.jpg',
        photographer: 'kar',
        project: 'inner'
    },
    {
        src: 'ktav.jpg',
        photographer: 'kar',
        project: 'sevan'
    },
    {
        src: 'anrak.jpg',
        photographer: 'yes',
        project: 'eu',
        recent: true,
    },
    {
        src: 'fantan1.jpg',
        photographer: 'kar',
        project: 'nature'
    },
    {
        src: 'aram2.jpg',
        photographer: 'aram',
        project: 'abstract'
    },
    {
        src: 'sevan-bw.jpg',
        photographer: 'kar',
        project: 'sevan'
    },
    {
        src: 'self1.png',
        photographer: null,
        project: 'self'
    },
    {
        src: 'sexy-hell.jpg',
        photographer: 'kar',
        project: 'fem',
        recent: true
    },
    {
        src: 'sevanK.jpg',
        photographer: 'kar',
        project: 'mermaid'
    },
    {
        src: 'yesayi-details.jpg',
        photographer: 'yes',
        project: 'details'
    },
    {
        src: 'meditation.jpg',
        photographer: 'kar',
        project: 'nature',
    },
    {
        src: 'mane.jpg',
        photographer: 'mane',
        project: 'nature'
    },
    {
        src: 'eunoia3.jpg',
        photographer: 'yes',
        project: 'eu',
        recent: true
    },
    {
        src: 'karen5.jpg',
        photographer: 'kar',
        project: 'mermaid'
    },
    {
        src: 'feminine5.jpg',
        photographer: 'kar',
        project: 'fem',
        recent: true
    },
    {
        src: 'marat1.jpg',
        photographer: 'mar',
        project: 'portrait'
    },
    {
        src: 'karen7.jpg',
        photographer: 'kar',
        project: 'portrait'
    },
    {
        src: 'karen1.jpg',
        photographer: 'kar',
        project: 'details'
    },
    {
        src: 'marat3.jpg',
        photographer: 'mar',
        project: 'portrait'
    },
    {
        src: 'karen8.jpg',
        photographer: 'kar',
        project: 'nature'
    },
    {
        src: 'self6.jpg',
        photographer: null,
        project: 'self'
    },
    {
        src: 'gramaphone5.jpg',
        photographer: 'mar',
        project: 'gram'
    },
    // 26

    // 2
    {
        src: 'tradition2.jpg',
        photographer: 'kar',
        project: 'red'
    },
    {
        src: 'fantan2.jpg',
        photographer: 'kar',
        project: 'nature'
    },
    {
        src: 'feminine.jpg',
        photographer: 'kar',
        project: 'fem',
        recent: true
    },
    {
        src: 'fantan3.jpg',
        photographer: 'kar',
        project: 'nature'
    },
    {
        src: 'storm.jpg',
        photographer: 'kar',
        project: 'signs'
    },
    {
        src: 'qochar6.jpg',
        photographer: 'kar',
        project: 'qochar'
    },
    {
        src: 'hands2.jpg',
        photographer: 'yes',
        project: 'hands'
    },
    {
        src: 'gramaphone2.jpg',
        photographer: 'mar',
        project: 'gram'
    },
    {
        src: 'eunoia.jpg',
        photographer: 'yes',
        project: 'eu',
        recent: true
    },
    {
        src: 'self4.jpg',
        photographer: null,
        project: 'self'
    },
    {
        src: 'feminine3.jpg',
        photographer: 'kar',
        project: 'fem',
        recent: true
    },
    {
        src: 'yesayi-feminine.jpg',
        photographer: 'yes',
        project: 'fem',
        recent: true
    },
    {
        src: 'aram1.jpg',
        photographer: 'aram',
        project: 'abstract'
    },
    {
        src: 'feminine4.jpg',
        photographer: 'kar',
        project: 'fem',
        recent: true
    },
    {
        src: 'yesayi3.jpg',
        photographer: 'yes',
        project: 'portrait'
    },
    {
        src: 'white-store.jpg',
        photographer: 'kar',
        project: 'inner'
    },
    {
        src: 'aram3.jpg',
        photographer: 'aram',
        project: 'abstract'
    },
    {
        src: 'fantan5.jpg',
        photographer: 'kar',
        project: 'nature'
    },
    {
        src: 'karen4.jpg',
        photographer: 'kar',
        project: 'portrait'
    },
    {
        src: 'inner-beauty10.jpg',
        photographer: 'kar',
        project: 'inner'
    },
    {
        src: 'eunoia6.jpg',
        photographer: 'yes',
        project: 'eu',
        recent: true
    },
    {
        src: 'fly.jpg',
        photographer: 'kar',
        project: 'sevan'
    },
    {
        src: 'yesayi7.jpg',
        photographer: 'yes',
        project: 'hands'
    },
    {
        src: 'self2.jpg',
        photographer: null,
        project: 'self'
    },
    {
        src: 'isolation.jpg',
        photographer: 'kar',
        project: 'corona'
    },
    {
        src: 'sevan1.jpg',
        photographer: 'kar',
        project: 'sevan'
    },
    // 26

    // 3
    {
        src: 'sevan.jpg',
        photographer: 'kar',
        project: 'sevan'
    },
    {
        src: 'eunoia1.jpg',
        photographer: 'yes',
        project: 'eu',
        recent: true
    },
    {
        src: 'inner-beauty1.jpg',
        photographer: 'kar',
        project: 'inner'
    },
    {
        src: 'fantan4.jpg',
        photographer: 'kar',
        project: 'nature'
    },
    {
        src: 'qochar5.jpg',
        photographer: 'kar',
        project: 'qochar'
    },
    {
        src: 'dance.jpg',
        photographer: 'kar',
        project: 'fem',
        recent: true
    },
    {
        src: 'curosity.jpg',
        photographer: 'mar',
        project: 'curosity'
    },
    {
        src: 'tradition3.jpg',
        photographer: 'kar',
        project: 'red'
    },
    {
        src: 'yesayi.jpg',
        photographer: 'yes',
        project: 'portrait'
    },
    {
        src: 'self3.jpg',
        photographer: null,
        project: 'self'
    },
    {
        src: 'inner-beauty5.jpg',
        photographer: 'kar',
        project: 'inner'
    },
    {
        src: 'hands3.jpg',
        photographer: 'yes',
        project: 'hands'
    },
    {
        src: 'marat.jpg',
        photographer: 'mar',
        project: 'portrait'
    },
    {
        src: 'karen.jpg',
        photographer: 'kar',
        project: 'portrait'
    },
    {
        src: 'eunoia4.jpg',
        photographer: 'yes',
        project: 'eu',
        recent: true
    },
    {
        src: 'self5.jpg',
        photographer: null,
        project: 'self'
    },
    {
        src: 'smoke.jpg',
        photographer: 'kar',
        project: 'sevan'
    },
    {
        src: 'yesayi-details.jpg',
        photographer: 'yes',
        project: 'details'
    },
    /*{
        src: 'mane2.jpg',
        photographer: 'mane',
        project: 'nature'
    },*/
    {
        src: 'fantan7.jpg',
        photographer: 'kar',
        project: 'nature'
    },
    {
        src: 'sevan2.jpg',
        photographer: 'kar',
        project: 'signs'
    },
    {
        src: 'self8.jpg',
        photographer: null,
        project: 'self'
    },
    {
        src: 'retro5.jpg',
        photographer: 'mar',
        project: 'retro'
    },
    {
        src: 'yesayi6.jpg',
        photographer: 'yes',
        project: 'portrait'
    },
    {
        src: 'inner-beauty3.jpg',
        photographer: 'kar',
        project: 'inner'
    },
    {
        src: 'yesayi4.jpg',
        photographer: 'yes',
        project: 'eu',
        recent: true
    },
    {
        src: 'eunonia8.jpg',
        photographer: 'yes',
        project: 'eu',
        recent: true
    },
    /*{
        src: 'fantan9.jpg',
        photographer: 'kar',
        project: 'nature'
    },*/
    // 26

    // 4
    {
        src: 'anrak2.jpg',
        photographer: 'yes',
        project: 'eu',
        recent: true
    },
    {
        src: 'feminine1.jpg',
        photographer: 'kar',
        project: 'fem',
        recent: true
    },
    {
        src: 'ash.png',
        photographer: 'ash',
        project: 'portrait'
    },
    {
        src: 'hands4.jpg',
        photographer: 'kar',
        project: 'hands'
    },
    {
        src: 'self9.jpg',
        photographer: null,
        project: 'self'
    },
    {
        src: 'feminine2.jpg',
        photographer: 'kar',
        project: 'fem',
        recent: true
    },
    {
        src: 'tradition1.jpg',
        photographer: 'kar',
        project: 'red'
    },
    {
        src: 'eunoia2.jpg',
        photographer: 'yes',
        project: 'eu',
        recent: true
    },
    {
        src: 'gramaphone4.jpg',
        photographer: 'mar',
        project: 'gram'
    },
    {
        src: 'qochar1.jpg',
        photographer: 'kar',
        project: 'qochar'
    },
    {
        src: 'self7.jpg',
        photographer: null,
        project: 'self'
    },
    {
        src: 'karen2.jpg',
        photographer: 'kar',
        project: 'portrait'
    },
    {
        src: 'yesayi2.jpg',
        photographer: 'yes',
        project: 'portrait'
    },
    {
        src: 'aram.jpg',
        photographer: 'aram',
        project: 'abstract'
    },
    {
        src: 'retro2.jpg',
        photographer: 'mar',
        project: 'retro'
    },
    {
        src: 'inner-beauty2.jpg',
        photographer: 'kar',
        project: 'inner'
    },
    {
        src: 'yesayi-dance.jpg',
        photographer: 'yes',
        project: 'dance'
    },
    {
        src: 'aram4.jpg',
        photographer: 'aram',
        project: 'abstract'
    },
    {
        src: 'lent1.jpg',
        photographer: 'kar',
        project: 'lent',
    },
    /*{
        src: 'marat2.jpg',
        photographer: 'mar',
        project: 'retro'
    },*/
    {
        src: 'karen3.jpg',
        photographer: 'kar',
        project: 'portrait'
    },
    /*{
        src: 'retro4.jpg',
        photographer: 'mar',
        project: 'portrait'
    },*/
    {
        src: 'feminine6.jpg',
        photographer: 'kar',
        project: 'fem',
        recent: true
    },
    {
        src: 'qochar.jpg',
        photographer: 'kar',
        project: 'qochar'
    },
    {
        src: 'mane1.jpg',
        photographer: 'mane',
        project: 'mermaid'
    },
    {
        src: 'feminine7.jpg',
        photographer: 'kar',
        project: 'fem',
        recent: true
    },
    {
        src: 'karen-back.jpg',
        photographer: 'kar',
        project: 'backstage'
    },
    {
        src: 'ash3.jpg',
        photographer: 'ash',
        project: 'portrait'
    },
    // 26
];


export const videos = [
    /*{
        src: '1.mp4',
        photographer: 'mar',
        project: 'retro'
    },
    {
        src: 'retro.mp4',
        photographer: 'mar',
        project: 'retro'
    },*/
    {
        src: 'backstage.mp4',
        poster: 'backstage.jpg',
        photographer: 'kar',
        project: 'backstage'
    }
];

export const stories = [
    {
        src: 'story',
        imgSrc: 'lilitik.jpg',
        photographer: 'yes',
        title: '#մանկություն',
            description: '<p>Փոքր ժամանակ լղար, նիհարակազմ երեխա էի: Գլուխս մարմնիս համեմատ մեծ էր թվում: Մաշկս ճեպ-ճերմակ էր, մազերս`արևագույն, կարճ, հենց արմատներից արդեն կլորացող, գիժ խուճուճներ, որ չէին փռվում ուսերիս, այլ հենց էդպես օդում էին մնում:<br /><br />Մի ամենասովորական օր էր մեր տանը: Սուսուփուս, խելոք, մոմի լույսի տակ նկարչություն էի անում: ՈՒ հանկարծ... հայրիկս մամայիս, թե բա նայի, աղջկադ գլուխը վառվում է... մազերս մոմի<span class="text_exposed_show">ն էին կպել ու այրվում էին, բայց ես շարունակում էի խաղաղ նկարչություն անել... ծնողներս չեն շտապել ինձ հանգցնել: Մի կուշտ ծիծաղել են, հետո նոր միայն փրկել ինձ քաչալանալուց:<br /><br />Մանկությանս էս դրվագը ինձ պատմել են ու պատմությունը էնպեսա մեխվել հիշողությանս մեջ, ասես ինքս եմ տեսել վառվող Լիլիթիկին ու ժպտացող ծնողներիս:<br /><br />Ծնողներս այդ ժամանակ ճիշտ նույն տարիքի էին, ինչ ես հիմա...</span></p>\n' +
        '<p>&nbsp;</p>\n' +
        '<p>@Եսայի<br />@Սարյան փողոցն ու շուռ եկած անձրևանոցները</p>\n' +
        '<p><a href="https://www.instagram.com/camera.portr8/?fbclid=IwAR1XmMaqAmcvVQeQ5ho0HGR2SuTALgN9W37bGJqUnsunElfsojgz8M_wxAo" target="_blank" rel="noopener nofollow" data-ft="{&quot;tn&quot;:&quot;-U&quot;}" data-lynx-mode="asynclazy" data-lynx-uri="https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.instagram.com%2Fcamera.portr8%2F%3Ffbclid%3DIwAR1XmMaqAmcvVQeQ5ho0HGR2SuTALgN9W37bGJqUnsunElfsojgz8M_wxAo&amp;h=AT07tXQthm9D9oy1l5kcFWP3qPt9Q9qpu4QdPt64sLFsDSGEKdpXoyixJY4TQ5vrM92AErFaTq6pYFutqDcDUQRDmPyipJQqXHaOcydrZrYszVwpQ6dvTzvmoWkCrRyjdRUEKiN48TZC8GU7dmgtJ-nBzdu8HiqXUVgM6lc">https://www.instagram.com/camera.portr8/</a><br /><a class="profileLink" href="https://www.facebook.com/durmuzyan/?__tn__=%2CdK-R-R&amp;eid=ARBVRXeq_IYco3qM4qxODp-RchvFBGDBUmO98I53_Gui0hMoAFLAyrlJlZjZCNcsbcRE9iYMuJJ8I5W2&amp;fref=mentions" data-hovercard="/ajax/hovercard/page.php?id=137159330029672&amp;extragetparams=%7B%22__tn__%22%3A%22%2CdK-R-R%22%2C%22eid%22%3A%22ARBVRXeq_IYco3qM4qxODp-RchvFBGDBUmO98I53_Gui0hMoAFLAyrlJlZjZCNcsbcRE9iYMuJJ8I5W2%22%2C%22fref%22%3A%22mentions%22%7D" data-hovercard-prefer-more-content-show="1">https://www.facebook.com/durmuzyan/</a></p>\n' +
        '<p>&nbsp;</p>\n' +
        '<p>&nbsp;</p>'
    },
    {
        src: 'story',
        imgSrc: 'mane.jpg',
        photographer: 'mane',
        title: '#զավեշտալի #խնդալու #երեսով #չտալու',
        description: '<p>&laquo;Հետևանքը արտեֆակտ&raquo; կինոդիտմանը սրտանց սպասում էի: Ֆիլմը սկսելուց 1-2 րոպե առաջ տեղ հասանք: Վերևում ազատ տեղեր կաին: Շտապեցինք, տեղավորվեցինք, ու սկսվեց: Քանի որ մեր ֆիլմը ինչ-որ փառատոնի ֆինալում էր հայտնվել, մեզ բնավ չզարմացրեցին ոչ հռետորները բեմում, ոչ էլ փառատոնային պլակատը նրանց թիկունքում: Բնականաբար սկզբում զավզակվում էինք, ու առանձնապես չէինք լսում հռետորների տափակ հումորները Բրիտանիայի եղանակի մասին, ու միայն մի 20 րոպե հանդիսատեսի գլուխը արդուկելուց հետո լսեցինք &laquo;բրիտանական ֆիլմերի փառատոն&raquo; բառակազմը, ու հասկացանք, որ գրողը տանի մենք սխալ սրահում ենք:<br /><br />Ակտ 2<br />Որպես &laquo;ամենաբիթի&raquo; որոշեցի գնալ գտնել մեր ֆիլմը, հետո ընկերներիս կանչել: Չնայած ամֆիթատրոնում պարտերից անհամեմատ մութա, այնուամենայնիվ տեսա, որ դիմացս՝ գետնին, րյուգզագ կար դրված, տերն էլ բրիտանացի մի սիրուն, ոտքերը փռել էր դիմացի աթոռին, ու կյանքն էր վայելում: Ինքնավստահ &laquo;ճկուն լոնք&raquo; արեցի... չէի նկատել, որ բրիտանուհիս նաև ոտաբոբիկ էր... լոնքս վայրէջք կատարեց ուղիղ կոշիկների վրա, ու գրավիտացիան քաշեց ինձ ներքև: Բնազդաբար ու բնականաբար ձեռս գցեցի առաջին պատահած բանին...բրիտանուհուս կողքի երիտասարդը ցնցվեց՜: երկու ձեռքով մխրճվել էի վերջինիս ոտքի մեջ: Թող գնա &laquo;մոմ վառի&raquo;, ամեն ինչ ավելի &laquo;ցավոտ&raquo; կարող էր լինել...<br />Շենշող կիսավայրէջքից հետո միակ բանը, որ ուզում էի վերանալ, մաքսիմալ արագ սրահից դուրս թռնելն էր, ու հանկարծ լսում եմ մի տիկին, թե բա աղջի՜կ ջան մարդկանց &laquo;դեմքով&raquo; են դուրս գալիս ոչ թե &laquo;հետևով&raquo;:<br /><br />Ակտ 3<br />Վերջապես մի պապիի շնորհիվ գտա մեր կինոն: Մտանք սրահ, որտեղ կոնկրետ աուրա կար, հաստատ խիստ տարբեր &laquo;բրիտանական եղանակից&raquo;: Հենվեցինք պատին ու փորձեցինք գոնե մի բան ընկալել ֆիլմից: Վարդակը ուղիղ հետևս էր՜: Միակ բանը որ ընկալեցի բազմաթիվ աչքերն էին, արյունը որ երակներովս սրընթաց վեր բարձրացավ, ու կուտակվեց դեմքիս, աչքերս արբիտրից դուրս էին եկել. ուզում էի վերանալ(էլի՜), թափանցիկ դառնալ: Ձեռքս տարա հետ, լույսը անջատեցի: Արյունս հետ իջավ ոտքեր, ու թուլացած գլուխս դրեցի ընկերոջս ուսին: Հուսահատ &laquo;բեզզվուկ&raquo; խնդում էի: <br />Մի կուշտ խնդացի, պրծա, կինոն էլ պրծավ՜: Ընկերոջս կամաց հարցրի &laquo;իսկ հիմա կարո՞ղ եմ լույսը միացնել&raquo;...Լուռ ծիծաղի նոր ալիք լցվեց ներսս...<br />Պապին եկավ, լույսը միացրեց: Ընկերներս արագ իմ արտեֆակտը սրահից դուրս շպրտեցին: Մեր աշխույժ տրամադրությունը, ու ներքին զգացողությունն ընդհանրապես չէր տեղավորվում ֆիլմը դիտած մարդկանց տրամադրության փազլի մեջ՜:<br /><br />Հ.Գ. Հիմա մտածում եմ ինչպես ճարել ֆիլմը, որ կատարյալ իզոլացիայի մեջ դիտեմ: Մարդ ես, հանկարծ նույն վերնագրով ուրիշ ֆիլմ չնայեմ էլի ))</p>'
    }
];
