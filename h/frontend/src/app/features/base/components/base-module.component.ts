import { Language } from 'src/app/core/models/language';
import { Component, OnInit } from '@angular/core';
import { WordingService } from '../../../core/services/wording.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { Observable } from 'rxjs';
import { registerLocaleData } from '@angular/common';
import { NvLocale } from 'src/app/core/models/dateFormat';
import localeDE from '@angular/common/locales/de';
import localeUK from '@angular/common/locales/en-GB';
import { NvGridI18nService } from 'nv-grid';
import { nvLocalMapper } from '../../../shared/pipes/localDecimal';
import { InternetConnectionService } from 'src/app/shared/services/internet-connection.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { DEBIT_CREDIT } from 'src/app/core/models/enums/debitCredit';
import { RouteConfigLoadStart, RouteConfigLoadEnd, Router } from '@angular/router';
import { LoadingService } from 'src/app/core/services/loading.service';
import { Theme } from 'src/app/core/models/Theme.enum';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';

@Component({
  templateUrl: './base-module.component.html',
  styleUrls: ['./base-module.component.scss']
})

export class BaseModuleComponent implements OnInit {
  public initialized = true;
  public isOnline$: Observable<boolean>;
  public isLoading$: Observable<boolean>;

  constructor(
    private wordingService: WordingService,
    private gridConfigService: GridConfigService,
    private gridI18nService: NvGridI18nService,
    private internetConnectionService: InternetConnectionService,
    private settingsService: SettingsService,
    private loadingService: LoadingService,
    private authService: AuthenticationService,
    private router: Router
  ) {
    registerLocaleData(localeDE);
    registerLocaleData(localeUK);
  }

  public ngOnInit() {
    this.isOnline$ = this.internetConnectionService.online$;
    this.isLoading$ = this.loadingService.isLoading$;
    this.initRouteConfigLoad();
    this.updateLanguage(this.authService.getSessionInfo().user.language || Language.EN);
    this.updateTheme(this.authService.getSessionInfo().user.theme || Theme.light);
    this.updateLocale(this.authService.getSessionInfo().user.locale || NvLocale.DE);
    this.updateDebitCredit(this.authService.getSessionInfo().user.debitCredit || DEBIT_CREDIT.plusMinus);
    this.updateNvGridConfig();

    this.wordingService.init();
    this.gridConfigService.initGridConfigs();
  }

  public updateLanguage(language: Language) {
    this.settingsService.language = language;
  }

  public updateLocale(locale: NvLocale) {
    this.settingsService.locale = locale;
  }

  public updateDebitCredit(debitCredit: DEBIT_CREDIT) {
    this.settingsService.debitCredit = debitCredit;
  }

  public updateNvGridConfig() {
    this.gridI18nService.setConfiguration({
      gridLanguage: this.settingsService.language,
      gridLocale: nvLocalMapper[this.settingsService.locale]
    });
  }

  public initRouteConfigLoad() {
    this.router.events.subscribe(event => {
      if (event instanceof RouteConfigLoadStart) {
        this.loadingService.isLoadingSubject.next(true);
      }
      if (event instanceof RouteConfigLoadEnd) {
        this.loadingService.isLoadingSubject.next(false);
      }
    });
  }

  private updateTheme(theme: Theme) {
    this.settingsService.theme = theme;
  }
}
