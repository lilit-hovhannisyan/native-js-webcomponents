import {EventEmitter, Injectable, Output} from '@angular/core';
import {portfolioItems, topics} from '../../../../shared-layer/l-hovhanni/dal/portfolio.model';
import {BehaviorSubject, Subject} from 'rxjs';
import {RouteManagementService} from 'l-builder';

@Injectable()
export class ProjectsService {
    private project;
    projects = [];

    _currentProject;
    currentProject = new BehaviorSubject(this._currentProject);

    _currentPhoto;
    currentPhoto = new BehaviorSubject(this._currentPhoto);

    constructor(private routeManagementService: RouteManagementService) {}


    setCurrentPhoto(photo?) {
        this._currentPhoto = this._currentProject.items.find( p => p === photo);
    }

    setCurrentPhotoQuery() {
        if (!this._currentPhoto) {
            this._currentPhoto = this.project.items[0];
        }

        this.routeManagementService.setRouteQuery('photo',  this._currentPhoto.src, false);
    }

    setProjectQuery() {
        this.routeManagementService.setRouteQuery('project', this._currentProject['key'], false);
    }

    setCurrentProject(projectKey) {
        if (!this.projects.length) {
            this.getProjects();
        }

        this.project = this.projects.find( pr => pr.key === projectKey);

        this._currentProject = this.project;

        this.setCurrentPhoto();
        this.getCurrentProject();
    }

    getCurrentProject() {
        if (!this.project) {
            this.setCurrentProject('all');
            this._currentPhoto = this.project.items[0];
        }

        this.currentPhoto.next(this._currentPhoto);
        this.currentProject.next(this._currentProject);
    }

    setProjects() {
        this.projects.push({
            ...portfolioItems[0],
            items: portfolioItems,
            name: 'All',
            key: 'all'
        });

        for (const topicKey of Object.keys(topics)) {
            const projectFistEl = portfolioItems.find( pr => pr.project === topicKey);
            const projectitems = portfolioItems.filter( pr => pr.project === topicKey);

            if (projectFistEl) {
                this.projects.push({
                    ...projectFistEl,
                    items: projectitems,
                    name: topics[topicKey],
                    key: topicKey
                });
            }
        }
    }

    getProjects() {
        if (!this.projects.length) {
            this.setProjects();
        }

        return this.projects;
    }
}