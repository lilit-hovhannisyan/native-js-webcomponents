import { Type } from 'class-transformer';
import { BaseModel } from '../base-model/base-model';
import { CertificateAttributesExtendedModel } from './certificate-attribute-extended.model';
import { CertificateAttributeModel } from './certificate-attribute.model';

export class CertificateModel extends BaseModel {
  @Type(() => CertificateAttributeModel)
  attributes: CertificateAttributeModel = new CertificateAttributeModel();
  @Type(() => CertificateAttributesExtendedModel)
  attributesExtended: CertificateAttributesExtendedModel = new CertificateAttributesExtendedModel();
  creatorOID: string = '';
}
