import { sitemap } from './../../../../core/constants/sitemap/sitemap';
import { ContactTypeRepositoryService } from './../../../../core/services/repositories/contact-type-repository.service';
import { ContactRoleRepositoryService } from './../../../../core/services/repositories/contact-role-repository.service';
import { ContactRepositoryService } from './../../../../core/services/repositories/contact-repository.service';
import { CompanyKey } from './../../../../core/models/resources/ICompany';
import { IContactType } from './../../../../core/models/resources/IContactType';
import { IContactRole } from './../../../../core/models/resources/IContactRole';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ContactKey, IContact } from 'src/app/core/models/resources/IContact';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { AuthenticationService, ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  templateUrl: './contact-detail-view.component.html',
  styleUrls: ['./contact-detail-view.component.scss']
})
export class ContactDetailViewComponent implements OnInit {

  public contactRoles$: Observable<IContactRole[]>;
  public contactTypes$: Observable<IContactType[]>;

  @Input() public contactId: ContactKey;
  @Input() public companyId: CompanyKey;
  @Input() public closeModal: () => void;

  public creationMode: boolean;
  public form: FormGroup;
  public isValid: Observable<boolean>;

  public contactsNode = sitemap.system.children.misc.children.companies.children.id.children.contacts;
  public contactsZone = SitemapEntry.getByNode(this.contactsNode).toZone();
  public wording = wording;
  public ACL_ACTIONS = ACL_ACTIONS;
  constructor(
    private contactRepo: ContactRepositoryService,
    private contactRoleRepo: ContactRoleRepositoryService,
    private contactTypeRepo: ContactTypeRepositoryService,
    private fb: FormBuilder,
    private authService: AuthenticationService
  ) { }

  public ngOnInit(): void {
    this.creationMode = !this.contactId;

    this.contactRoles$ = this.contactRoleRepo.fetchAll({});
    this.contactTypes$ = this.contactTypeRepo.fetchAll({});

    this.creationMode
      ? this.initForm()
      : this.contactRepo.fetchOne(this.contactId, {})
        .subscribe((contact: IContact) => { this.initForm(contact); });
  }

  private initForm(contact = {} as IContact): void {
    this.form = this.fb.group({
      contactPerson: [contact.contactPerson, [Validators.required]],
      value: [contact.value, [Validators.required]],
      typeId: [contact.typeId, Validators.required],
      roleId: [contact.roleId, Validators.required],
      remark: [contact.remark],
    });

    this.isValid = this.form.valueChanges.pipe(
      startWith(null),
      map(() => this.form.valid)
    );

    if (!this.authService.canUpdate(this.contactsZone)) {
      this.form.disable();
    }
  }

  public saveClicked(): void {
    const companyId = this.companyId;
    const freshContact = this.creationMode
      ? { ...this.form.value, companyId }
      : { ...this.form.value, companyId, id: this.contactId };

    this.creationMode
      ? this.createContact(freshContact)
      : this.upateContact(freshContact);
  }

  private createContact(contact: any) {
    this.contactRepo.create(contact, {})
      .subscribe(() => { this.closeModal(); });
  }

  private upateContact(contact: IContact) {
    this.contactRepo.update(contact, {})
      .subscribe(() => { this.closeModal(); });
  }
}
