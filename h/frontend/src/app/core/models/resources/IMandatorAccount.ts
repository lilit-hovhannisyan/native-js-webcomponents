import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';
import { CurrencyKey } from './ICurrency';
import { BookingAccountKey } from './IBookingAccount';
import { MandatorKey } from './IMandator';

export type MandatorAccountKey = number;

interface ISharedProps {
  mandatorId: MandatorKey;
  bookingAccountId: BookingAccountKey;
  isOpenItem: boolean;
  isOpex: boolean;
  restrictedCurrencyId?: CurrencyKey;
  needsAccountPermission: boolean;
}

export interface IMandatorAccountBase extends ISharedProps, IResource<MandatorAccountKey> { }

export interface IMandatorAccountCreate extends ISharedProps { }

export interface IMandatorAccountDTO extends IMandatorAccountBase { }

export interface IMandatorAccount extends IMandatorAccountDTO, IWithLabel { }

export interface IMandatorAccountFull extends IMandatorAccount {
  no: number;
  isoCode: string;
  restrictedCurrency: string;
}

export interface IMandatorAccountRow extends IMandatorAccountFull {
}
