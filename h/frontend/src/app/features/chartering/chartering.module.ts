import { CHARTERING_DECLARATIONS } from './chartering-routing.module';
import { SystemRoutingModule } from './chartering-routing.module';
import { WriteProtectionService } from 'src/app/core/services/ui/write-protection.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { ChartererRatesComponent } from 'src/app/features/chartering/components/misc/time-charter/time-charter-periods/charterer-rates/charterer-rates.component';
import { RegistrationFleetBasicComponent } from './components/registrations/registrations-fleet/registrations-fleet-detail-view/registration-fleet-basic/registration-fleet-basic.component';
import { RegistrationFleetTechnicalComponent } from './components/registrations/registrations-fleet/registrations-fleet-detail-view/registration-fleet-technical/registration-fleet-technical.component';
import { DashboardTasksComponent } from './components/registrations/dashboard/dashboard-tasks/dashboard-tasks.component';

@NgModule({
  imports: [
    SystemRoutingModule,
    SharedModule,
  ],
  declarations: [
    CHARTERING_DECLARATIONS,
    ChartererRatesComponent,
    RegistrationFleetBasicComponent,
    RegistrationFleetTechnicalComponent,
    DashboardTasksComponent,
  ],
  providers: [
    WriteProtectionService,
  ],
})
export class CharteringModule {
}
