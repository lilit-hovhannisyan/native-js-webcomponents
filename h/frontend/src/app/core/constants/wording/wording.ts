import { wordingHttpStatus } from 'src/app/core/constants/wording/http-status';
import { wordingMain } from './main';
import { wordingGeneral } from './general';
import { wordingAccounting } from './accounting';
import { IWording } from '../../models/resources/IWording';
import { wordingChartering } from './chartering';
import { wordingExamples } from './examples';
import { wordingAutocomplete } from './autocomplete';
import { system } from './system';
import { wordingTechnicalManagement } from './technicalManagement';
import { wordingErrors } from './errors';
import { wordingPayments } from './payments';

/**
 * This is the root-wording object.
 * Each key of this object represents a category and as such contains
 * the wording, that is being used in the components of that category.
 *
 * Apart from that, there is the key 'general'. It contains commonly used
 * phrases that are shared between multiple categories.
 *
 * The keys of each wording are hardcoded.
 * The values are set to empty objects. When the wording is loaded it is
 * populated by something of this format: { en: 'Welcome', de: 'Willkommen' }
 */

type wordingRoot = {
  [category: string]: {
    [key: string]: {} | IWording
  }
};

// the type 'wordingRoot' is not applied because it kills autocomplete for the object
// only option to type is:
// https://stackoverflow.com/questions/52146544/why-autocomplete-stop-working-in-an-object-with-type-in-typescript

export const wording = {
  general: wordingGeneral,
  main: wordingMain,
  accounting: wordingAccounting,
  chartering: wordingChartering,
  examples: wordingExamples,
  autocomplete: wordingAutocomplete,
  system,
  technicalManagement: wordingTechnicalManagement,
  httpStatus: wordingHttpStatus,
  errors: wordingErrors,
  payments: wordingPayments,
};
