import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDividerModule } from '@angular/material/divider';

import { IconsModule } from '@shared-modules/icons.module';
import { MeAttributeModule } from '@shared/me-components/me-attribute/me-attribute.module';
import { MeButtonModule } from '@shared-components/me-button/me-button.module';
import { MeCollectionCardModule } from '@shared/me-components/me-collection-card/me-collection-card.module';
import { MeComponentLoadingModule } from '@shared-directives/me-component-loader/me-component-loader.module';
import { MeDocumentCardModule } from '@shared/me-components/me-document-card/me-document-card.module';
import { MeEmptyContentModule } from '@shared/me-components/me-empty-content/me-empty-content.module';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';
import { MeImageLoaderModule } from '@shared-directives/me-image-loader/me-image-loader.module';
import { MeLabelModule } from '@shared-components/me-label/me-label.module';
import { MeMaterialCardModule } from '@shared-components/me-material-card/me-material-card.module';
import { MePopupSelectModule } from '@shared/me-components/me-popup-select/me-popup-select.module';
import { MePopupSelectService } from '@shared/me-components/me-popup-select/me-popup-select.service';
import { MeRightSidebarModule } from '@shared/me-components/me-right-sidebar/me-right-sidebar.module';
import { MeSearchBarModule } from '@shared/me-components/me-search-bar/me-search-bar.module';
import { MeUploadFileModule } from '@shared/me-components/me-upload-file/me-upload-file.module';
import { MeUploadImageModule } from '@shared-components/me-upload-image/me-upload-image.module';
import { SharedModule } from '@shared/shared.module';

import { ExperimentsRoutingModule } from './experiments-routing.module';
import { ExperimentsComponent } from './experiments.component';
import { UploadImageExampleComponent } from './upload-image-example/upload-image-example.component';
import { UploadFileExampleComponent } from './upload-file-example/upload-file-example.component';
import { MeAddListCardModule } from '@shared/me-components/me-add-list-card/me-add-list-card.module';
import { MeUserGroupCardModule } from '@shared/me-components/me-user-group-card/me-user-group-card.module';
import { MeTabGroupModuel } from '@shared/me-components/me-tab-group/me-tab-group.module';
import { MeInfoCardModule } from '@shared/me-components/me-info-card/me-info-card.module';
import { MeMaterialTypeCardModule } from '@shared/me-components/me-material-type-card/me-material-type-card.module';
import { MeAdvancedFilterModule } from '@shared/me-components/me-advanced-filter/me-advanced-filter.module';
import { MePopupVideoModule } from '@shared/me-components/me-popup-video/me-popup-video.module';

@NgModule({
  imports: [
    CommonModule,
    ExperimentsRoutingModule,
    IconsModule,
    MatDividerModule,
    MeAddListCardModule,
    MeAttributeModule,
    MeButtonModule,
    MeCollectionCardModule,
    MeComponentLoadingModule,
    MeDocumentCardModule,
    MeEmptyContentModule,
    MeIconsProviderModule,
    MeImageLoaderModule,
    MeLabelModule,
    MeMaterialCardModule,
    MePopupSelectModule,
    MeRightSidebarModule,
    MeSearchBarModule,
    MeUploadFileModule,
    MeUploadImageModule,
    MeUserGroupCardModule,
    SharedModule,
    MePopupSelectModule,
    MeAttributeModule,
    MePopupSelectModule,
    MeIconsProviderModule,
    MeUploadFileModule,
    MeTabGroupModuel,
    MeInfoCardModule,
    MeMaterialTypeCardModule,
    MeAdvancedFilterModule,
    MePopupVideoModule
  ],
  declarations: [ExperimentsComponent, UploadImageExampleComponent, UploadFileExampleComponent],
  providers: [MePopupSelectService]
})
export class ExperimentsModule {}
