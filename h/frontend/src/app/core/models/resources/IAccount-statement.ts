import { IResource } from './IResource';
import { IWording } from './IWording';
import { CostTypeKey } from './ICostTypes';
import { CostCenterKey } from './ICostCenter';
import { CostUnitKey } from './ICostUnit';
import { BookingCodeKey } from './IBookingCode';

export type AccountStatementKey = number;

interface ISharedProps {
  amountLocal: number;
  amountTransaction: number;
  bookingAccountId: number;
  bookingCodeId: BookingCodeKey;
  bookingDate: string;
  bookingId: number;
  currencyId: number;
  taxKeyId: number;
  debitCreditType: number;
  discount: number;
  exchangeRate: number;
  id: number;
  invoiceDate: Date;
  isOpen: boolean;
  lineIndex: number;
  mandatorId: number;
  oppositeBookingAccountId: number;
  paymentDate: Date;
  period: number;
  text: string;
  userName: string;
  voucherDate: Date;
  voucherNumber: string;
  year: number;
  costTypeId: CostTypeKey;
  costCenterId: CostCenterKey;
  costUnitId: CostUnitKey;
}

export interface IAccountStatementCreate extends ISharedProps { }

export interface IAccountStatementBase extends ISharedProps, IResource<number> { }

export interface IAccountStatementDTO extends IAccountStatementBase { }

export interface IAccountStatement extends IAccountStatementDTO { }

export interface IAccountStatementFull extends IAccountStatement {
  accountName: IWording;
  currencyIsoCode: string;
  oppositeAccountNo: number | string;
  costTypeNo: number;
  costTypeName: string;
  costCenterNo: number;
  costCenterName: string;
  costUnitNo: string;
  costUnitName: string;
  bookingCode?: IWording;
}

