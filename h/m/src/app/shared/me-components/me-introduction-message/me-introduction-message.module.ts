import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeIntroductionMessageComponent } from './me-introduction-message.component';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [MeIntroductionMessageComponent],
  imports: [CommonModule, MeIconsProviderModule, MatButtonModule],
  exports: [MeIntroductionMessageComponent]
})
export class MeIntroductionMessageModule {}
