import { Injectable } from '@angular/core';
import { combineLatest, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { VesselKey, IVesselWithAccountingsRow } from 'src/app/core/models/resources/IVessel';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { RegistrationVesselTasksRepositoryService } from 'src/app/core/services/repositories/registration-vessel-tasks-repository.service';
import { IRegistrationVesselTask } from 'src/app/core/models/resources/IRegistrationVesselTask';
import { convertToMap } from 'src/app/shared/helpers/general';
import { TaskType } from 'src/app/core/models/enums/vessel-registration-tasks';
import * as moment from 'moment';
import { IVesselTaskFull } from '../../models/resources/IVesselTask';

@Injectable({ providedIn: 'root' })
export class RegistrationVesselTasksMainService {
  constructor(
    private vesselMainService: VesselMainService,
    private registrationVesselTasksRepo: RegistrationVesselTasksRepositoryService,
  ) { }

  public getStream(
    vesselId?: VesselKey,
    filterByStartDate = false,
  ): Observable<IVesselTaskFull<IRegistrationVesselTask>[]> {
    let filterParams: Partial<IRegistrationVesselTask>;

    if (vesselId) {
      filterParams = { vesselId };
    }

    return combineLatest([
      this.registrationVesselTasksRepo.getStream(filterParams),
      this.vesselMainService.fetchAll().pipe(take(1)),
    ]).pipe(map(([allTasks, vesselsWithAccountings]) => {
      const vesselsMap: Map<VesselKey, IVesselWithAccountingsRow>
        = convertToMap(vesselsWithAccountings);

      return allTasks
        .filter(task => {
          const startDate: moment.Moment = moment(task.start);
          const untilDate: moment.Moment = moment().add(60, 'days');
          const isVesselTask: boolean = task.type === TaskType.RegistrationVesselTask;

          return filterByStartDate
            ? startDate.isSameOrBefore(untilDate) && isVesselTask
            : isVesselTask;
        })
        .map(task => {
          const vessel: IVesselWithAccountingsRow = vesselsMap.get(task.vesselId);
          return {
            ...task,
            vesselAccountingName: vessel.lastVesselAccountingName,
          };
        });
    }));
  }
}
