import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { BaseLoggerComponent } from '@features/tracking-system';
import { MeButtonI } from '../me-button/me-button.interface';
import { MeAdvancedFilterI } from './me-advanced-filter.interface';

@Component({
  selector: 'me-advanced-filter',
  templateUrl: './me-advanced-filter.component.html',
  styleUrls: ['./me-advanced-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeAdvancedFilterComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  componentVersion = '0.0.1';

  @Input() dataModel!: MeAdvancedFilterI;
  @Output() eventOccurs: EventEmitter<{ eventName: string }> = new EventEmitter();

  resetButton: MeButtonI = {
    color: 'link',
    type: 'regular'
  };

  filterButton: MeButtonI = {
    color: 'link',
    type: 'regular'
  };

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {}

  emitToggleFilter(eventName: string): void {
    this.eventOccurs.emit({ eventName });
  }

  ngOnDestroy(): object {
    return super.ngOnDestroy();
  }
}
