import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';
import { IWording } from './IWording';

export type SalutationKey = number;

export interface ISharedProps extends IResource<number>, IWithLabels, IWithLabel { }

export interface ISalutationBase extends ISharedProps, IResource<SalutationKey> { }

export interface ISalutationCreate extends ISharedProps { }

export interface ISalutationDTO extends ISalutationBase { }

export interface ISalutation extends ISalutationDTO { }

export interface ISalutationFull extends ISalutation {
  salutation?: ISalutation;
}

export interface ISalutationRow extends ISalutationFull {
  salutationLabel?: IWording;
}
