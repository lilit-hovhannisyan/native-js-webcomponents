
import { ValidationErrors, AbstractControl } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { Injectable } from '@angular/core';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';

@Injectable({
  providedIn: 'root'
})
export class CurrencyValidatorsService {

  constructor(
    private currencyRepo: CurrencyRepositoryService
  ) { }

  public doesCurrencyExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundCurrency = this.currencyRepo.getStreamValue()
      .find(currency => currency.id === control.value);

    const isValid = foundCurrency ? true : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      currencyNotFound: {
        message: wording.accounting.notFoundCurrency
      }
    };
  }

  public isCurrencyActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const foundCurrency = this.currencyRepo.getStreamValue()
      .find(currency => currency.id === control.value);

    const isValid = foundCurrency
      ? foundCurrency.isActive
      : false;

    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      currencyNotActive: {
        message: wording.accounting.notActiveCurrency
      }
    };
  }

  public doesCurrencyIsoExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const currencyIsoCode = control.value;
    const foundCurrency = this.currencyRepo.getStreamValue()
      .find(currency => currency.isoCode === currencyIsoCode);

    const isValid = foundCurrency ? true : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      currencyNotFound: {
        message: wording.accounting.notFoundCurrency
      }
    };
  }

  public isCurrencyIsoActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const currencyIsoCode = control.value;
    const foundCurrency = this.currencyRepo.getStreamValue()
      .find(currency => currency.isoCode === currencyIsoCode);

    const isValid = foundCurrency ? foundCurrency.isActive : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      currencyNotActive: {
        message: wording.accounting.notActiveCurrency
      }
    };
  }
}
