import { Component, Injectable, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MePopupAddListComponent } from './me-popup-add-list.component';
import { MePopupAddListI } from './me-popup-add-list.interface';

@Injectable()
export class MeAddListPopupService {
  private _takeUntilObservable$: Subject<void> = new Subject<void>();
  private _currentInstance: MePopupAddListComponent | undefined;

  constructor(private dialog: MatDialog) {}

  openPopup(dataModel: MePopupAddListI, content?: TemplateRef<Component>): Subject<string> {
    const subjectToReturn = new Subject<string>();
    const dialogRef = this.dialog.open(MePopupAddListComponent, { data: { dataModel, content }, panelClass: 'add-list-container' });

    this._currentInstance = dialogRef.componentInstance;
    this._currentInstance.popupAddListEvent
      .pipe(takeUntil(this._takeUntilObservable$))
      .subscribe((event: { eventName: string; items?: [] }) => {
        if (event.eventName === 'close') {
          dialogRef.close();
        } else if (event.eventName === 'addUsersAndClose') {
          subjectToReturn.next(event.eventName);
          dialogRef.close();
        }
      });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this._takeUntilObservable$))
      .subscribe(() => {
        this._takeUntilObservable$.next();
        this._takeUntilObservable$.complete();
        this._currentInstance = undefined;
        subjectToReturn.complete();
      });

    return subjectToReturn;
  }

  pushDataModelToPopup(data: MePopupAddListI): void {
    if (this._currentInstance) {
      this._currentInstance.dataModel = data;
    }
  }
}
