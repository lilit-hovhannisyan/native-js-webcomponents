import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';

import { IconsModule } from '@shared/modules/icons.module';

import { MeTabGroupComponent } from './me-tab-group.component';

@NgModule({
  declarations: [MeTabGroupComponent],
  imports: [CommonModule, MatTabsModule, IconsModule],
  exports: [MeTabGroupComponent]
})
export class MeTabGroupModuel {}
