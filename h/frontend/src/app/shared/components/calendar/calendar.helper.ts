import { wording } from '../../../core/constants/wording/wording';
import { ACL_ACTIONS } from '../../../authentication/services/authentication.service';
import { IWording } from '../../../core/models/resources/IWording';
import * as moment from 'moment';

export const months = {
  en: [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ],
  de: [
    'Jan',
    'Feb',
    'Mär',
    'Apr',
    'Mai',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Okt',
    'Nov',
    'Dez',
  ]
};

export enum CalendarEnvironment {
  chartering = 'Chartering'
}

export enum ContextTypes {
  none = 'none',
  add = 'add',
  editTask = 'editModeTask',
  editTodo = 'editModeTodo',
  editEvn = 'editModeEvent'
}

export enum CalendarEventsTypes {
  task = 'RegistrationVesselTask',
  evn = 'CalendarEvent',
  todo = 'todo',
}

const enum Icons {
  add = 'plus-circle',
  eye = 'eye',
  edit = 'edit',
  del = 'delete',
}

export type contextMenuActionName = 'showTaskModal' | 'showCalendarEventModal';

interface ICalendarContextMenuButton {
  operation: ACL_ACTIONS;
  eventType: CalendarEventsTypes;
  wording: IWording;
  icon: Icons;
  action?: contextMenuActionName;
}

interface ICalendarContextMenuConfig {
  contextType: ContextTypes;
  contextClassname: string;
  buttons: ICalendarContextMenuButton[];
}

export const calendarEventZone = 'system.misc.calendarEvents';

export const buttonContextMenuConfig: ICalendarContextMenuConfig[] = [
  {
    contextType: ContextTypes.add,
    contextClassname: '',
    buttons: [
      {
        operation: ACL_ACTIONS.CREATE,
        eventType: CalendarEventsTypes.task,
        wording: wording.chartering.addNewTask,
        icon: Icons.add,
        action: 'showTaskModal'
      },
      {
        operation: ACL_ACTIONS.CREATE,
        eventType: CalendarEventsTypes.evn,
        wording: wording.chartering.addNewEvent,
        icon: Icons.add,
        action: 'showCalendarEventModal'
      },
      /*{
        operation: ACL_ACTIONS.CREATE,
        eventType: CalendarEventsTypes.todo,
        wording: wording.chartering.addNewTodo,
        icon: Icons.add,
        action: 'showCalendarTodoModal'
      }*/ // TODO: uncomment when "todo" type of event is implemented
    ]
  },
  {
    contextType: ContextTypes.editTask,
    contextClassname: 'edit edit-task',
    buttons: [
      {
        operation: ACL_ACTIONS.READ,
        eventType: CalendarEventsTypes.task,
        wording: wording.chartering.showTaskDetails,
        icon: Icons.eye,
        action: 'showTaskModal'
      },
      {
        operation: ACL_ACTIONS.UPDATE,
        eventType: CalendarEventsTypes.task,
        wording: wording.chartering.editTask,
        icon: Icons.edit,
        action: 'showTaskModal'
      },
      {
        operation: ACL_ACTIONS.DELETE,
        eventType: CalendarEventsTypes.task,
        wording: wording.chartering.deleteTask,
        icon: Icons.del
      }
    ]
  },
  {
    contextType: ContextTypes.editEvn,
    contextClassname: 'edit edit-evn',
    buttons: [
      {
        operation: ACL_ACTIONS.READ,
        eventType: CalendarEventsTypes.evn,
        wording: wording.chartering.showEventDetails,
        icon: Icons.eye,
        action: 'showCalendarEventModal'
      },
      {
        operation: ACL_ACTIONS.UPDATE,
        eventType: CalendarEventsTypes.evn,
        wording: wording.chartering.editEvent,
        icon: Icons.edit,
        action: 'showCalendarEventModal'
      },
      {
        operation: ACL_ACTIONS.DELETE,
        eventType: CalendarEventsTypes.evn,
        wording: wording.chartering.deleteEvent,
        icon: Icons.del
      }
    ]
  },
  /*{
    contextType: ContextTypes.editTodo,
    contextClassname: 'edit edit-todo',
    buttons: [
      {
        operation: ACL_ACTIONS.READ,
        eventType: CalendarEventsTypes.todo,
        wording: wording.chartering.showTodoDetails,
        icon: Icons.eye,
        action: 'showTodoModal'
      },
      {
        operation: ACL_ACTIONS.UPDATE,
        eventType: CalendarEventsTypes.todo,
        wording: wording.chartering.editToDo,
        icon: Icons.edit,
        action: 'showTodoModal'
      },
      {
        operation: ACL_ACTIONS.DELETE,
        eventType: CalendarEventsTypes.evn,
        wording: wording.chartering.deleteToDo,
        icon: Icons.del
      }
    ]
  },*/ // TODO: uncomment when "todo" type of event is implemented
];

export const formatWithHours = (date: string | Date): string => {
  return moment(date).format('YYYY-MM-DDThh:mm');
}
