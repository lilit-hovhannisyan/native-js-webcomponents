import { Component, ElementRef, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { sortBy } from 'lodash';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NvGridConfig } from 'nv-grid';
import { Observable, Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { getGridConfig as getShipyardTypesGridConfig } from 'src/app/core/constants/nv-grid-configs/shipyard-types.config';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/shipyards.config';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { VESSEL_TYPE } from 'src/app/core/models/enums/vessel-type';
import { IShipyardRow } from 'src/app/core/models/resources/IShipyard';
import { IShipyardType } from 'src/app/core/models/resources/IShipyardType';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { ShipyardRepositoryService } from 'src/app/core/services/repositories/shipyard-repository.service';
import { ShipyardTypeRepositoryService } from 'src/app/core/services/repositories/shipyard-type-repository.service';
import { VesselRepositoryService } from 'src/app/core/services/repositories/vessel-repository.service';
import { VesselSpecificationRepositoryService, vesselTypes } from 'src/app/core/services/repositories/vessel-specification-repository.service';
import { AccountService } from '../../../../../../../core/services/account.service';
import { VesselsHelperService } from '../vessels.helper.service';
import { NVHttpErrorResponse } from './../../../../../../../core/abstracts/ResourceDetailView';
import { ISelectOption } from './../../../../../../../core/models/misc/selectOption';
import { IVesselSpecification } from './../../../../../../../core/models/resources/IVesselSpecification';
import { getDefaultIntervalListItem, fetchFormattedIntervals, fetchFormattedFormValue } from 'src/app/shared/helpers/general';
import { map } from 'rxjs/operators';

@Component({
  templateUrl: './vessel-specification.component.html',
  styleUrls: ['./vessel-specification.component.scss']
})
export class VesselSpecificationComponent implements OnInit, OnDestroy {
  public wording = wording;
  public form: FormGroup;
  public vesselId: VesselKey;
  public specifications: Array<IVesselSpecification> = [];
  public selectedSpecification?: IVesselSpecification;
  public inEditMode = false;
  public inCreationMode = false;
  public node = sitemap.chartering.children.misc.children.vessels.children.id.children.specification;
  public zone = SitemapEntry.getByNode(this.node).toZone();
  public VESSEL_TYPE = VESSEL_TYPE;
  public shipyards$: Observable<IShipyardRow[]>;
  public shipyardTypes$: Observable<IShipyardType[]>;
  private subscriptions: Subscription[] = [];

  public vesselTypes: ISelectOption[] = vesselTypes;
  public shipyardGridConfig: NvGridConfig = getGridConfig({}, () => false);
  public shipyardTypesGridConfig: NvGridConfig = getShipyardTypesGridConfig({}, () => false);
  public formElement: ElementRef;

  constructor(
    private fb: FormBuilder,
    private confirmationService: ConfirmationService,
    public accountService: AccountService,
    public route: ActivatedRoute,
    protected specificationRepo: VesselSpecificationRepositoryService,
    private vesselRepositoryService: VesselRepositoryService,
    private shipyardRepo: ShipyardRepositoryService,
    private shipyardTypesRepo: ShipyardTypeRepositoryService,
    private nzMessageService: NzMessageService,
    public authService: AuthenticationService,
    private vesselsHelperService: VesselsHelperService,
    private el: ElementRef,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    const idParam = this.route.parent.snapshot.params.id;
    this.vesselId = parseInt(idParam, 10);
    this.init();
    this.fetchSpecifications();
    this.shipyards$ = this.shipyardRepo.fetchAll({});
    this.shipyardTypes$ = this.shipyardTypesRepo.fetchAll({});
  }

  public fetchSpecifications = (idToSelect?: number) => {
    this.vesselRepositoryService.fetchSpecificationsByVesselId(this.vesselId)
      .pipe(
        map(specifications => fetchFormattedIntervals(specifications))
      )
      .subscribe(specifications => {
        this.specifications = specifications || [];
        this.specifications = sortBy(this.specifications, ['from']);

        if (this.specifications.length) {
          const specificationToSelect = idToSelect
            ? this.specifications.find(s => s.id === +idToSelect)
            : getDefaultIntervalListItem(this.specifications);
          this.selectSpecification(specificationToSelect
            || this.specifications[this.specifications.length - 1]);
        }
      });
  }

  public init(specification: IVesselSpecification = { vesselId: this.vesselId } as any): void {
    this.form = this.fb.group({
      from: [specification.from, [Validators.required]],
      until: [specification.until, [Validators.isAfterFieldDate('from')]],
      vesselType: [specification.vesselType],
      shipyardId: [specification.shipyardId],
      shipyardTypeId: [specification.shipyardTypeId],

      hullNo: [specification.hullNo, Validators.maxLength(100)],
      hatches: [specification.hatches, [Validators.wholeNumber, Validators.min(1), Validators.max(99)]],
      gear: [specification.gear, Validators.maxLength(100)],
      class: [specification.class, Validators.maxLength(100)],

      dwats: [specification.dwats, [Validators.maxLength(6), Validators.number]],
      dwatw: [specification.dwatw, [Validators.maxLength(6), Validators.number]],
      dwccw: [specification.dwccw, [Validators.maxLength(6), Validators.number]],
      dwccs: [specification.dwccs, [Validators.maxLength(6), Validators.number]],
      grt: [specification.grt, [Validators.maxLength(6), Validators.number]],
      nrt: [specification.nrt, [Validators.maxLength(6), Validators.number]],
      // "for bulkcarrier"
      cbftGrain: [specification.cbftGrain, [Validators.number, Validators.maxLength(6)]],
      cbftBale: [specification.cbftBale, [Validators.number, Validators.maxLength(6)]],
      cbmGrain: [specification.cbmGrain, [Validators.number, Validators.maxLength(6)]],
      cbmBale: [specification.cbmBale, [Validators.number, Validators.maxLength(6)]],
      // for "container" and "conro" types

      container20ft: [specification.container20ft, [Validators.number, Validators.maxLength(6)]],
      container40ft: [specification.container40ft, [Validators.number, Validators.maxLength(6)]],
      homog14ts: [specification.homog14ts, [Validators.number, Validators.maxLength(6)]],
      reefer: [specification.reefer, [Validators.number, Validators.maxLength(6)]],
    });
    this.form.disable();
    this.form.markAsPristine();
    this.updateFromFieldOnUntilChange();
  }

  public updateFromFieldOnUntilChange() {
    this.subscriptions = [
      this.form.get('from').valueChanges.subscribe(() => {
        this.form.get('until').updateValueAndValidity();
      }),
      ...this.subscriptions
    ];
  }

  public intervalClicked = (specificationId: number) => {
    const specification = this.specifications.find(s => s.id === specificationId);
    this.selectSpecification(specification);
  }

  public selectSpecification = (specification: IVesselSpecification) => {
    this.selectedSpecification = specification;
    this.init(specification);
  }

  public enterEditMode = () => {
    this.inEditMode = true;
    // for some strange reason, form needs to be enabled in next event-cycle.
    // otherwise it stays disabled
    setTimeout(() => { this.form.enable(); });
  }

  public cancelEditing = () => {
    this.inEditMode = false;
    this.inCreationMode = false;
    this.init(this.selectedSpecification);
    // for some strange reason, form needs to be disabled in next event-cycle.
    // otherwise it stays enabled
    setTimeout(() => this.form.disable());
  }

  public addIntervalClicked = () => {
    this.inCreationMode = true;
    this.selectedSpecification = {} as IVesselSpecification;
    const latestEntry = this.specifications[this.specifications.length - 1];
    const clonedSpecification = latestEntry
      ? { ...latestEntry, from: latestEntry.until, until: undefined }
      : {} as IVesselSpecification;

    delete clonedSpecification.id; // we don't need the id in creation mode
    this.init(clonedSpecification); // cloning the latest most recent one

    this.enterEditMode();
  }

  public deleteClicked = () => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: '' },
    }).subscribe(confirmed => confirmed && this.specificationRepo.delete(this.selectedSpecification.id, {}).subscribe());
  }

  public saveClicked = () => {
    if (this.form.invalid) {
      return;
    }

    const value = fetchFormattedFormValue(this.form.value);

    const freshResource = this.selectedSpecification.id
      ? { ...this.selectedSpecification, ...value }
      : { ...value, vesselId: this.vesselId };

    this.selectedSpecification.id
      ? this.specificationRepo.update(freshResource, {}).subscribe(spec => {
        this.cancelEditing();
        this.fetchSpecifications(spec.id);
      }, this.handleSubmitError)

      : this.specificationRepo.create(freshResource, {}).subscribe(spec => {
        this.cancelEditing();
        this.fetchSpecifications(spec.id);
        this.vesselsHelperService.isSpecificationsMissing$.next(false);
      }, this.handleSubmitError);
  }

  public handleSubmitError = (error: NVHttpErrorResponse) => {
    ResourceDetailView.handleSubmitError(error, this.form, this.nzMessageService);
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
