import { ElementRef, QueryList, ViewChildren } from '@angular/core';
import { Component, OnInit, ChangeDetectionStrategy, Input, OnDestroy } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ReplaySubject } from 'rxjs';
import { distinctUntilChanged, startWith, takeUntil, tap } from 'rxjs/operators';
import { Validators } from 'src/app/core/classes/validators';
import { wording } from 'src/app/core/constants/wording/wording';
import { TaskLanguageType } from 'src/app/core/models/resources/IVesselTask';
import { ITaskTemplate } from 'src/app/core/models/resources/Task-Templates/ITaskTemplate';
import { ITaskTemplateTodoItem } from 'src/app/core/models/resources/Task-Templates/ITaskTemplateTodoItem';
import { TaskTemplateRepositoryService } from 'src/app/core/services/repositories/task-templates/task-template-repository.service';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { TaskTemplatesHelperService } from '../../task-templates-helper.service';

@Component({
  selector: 'nv-task-template-create',
  templateUrl: './task-template-create.component.html',
  styleUrls: ['./task-template-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskTemplateCreateComponent implements OnInit, OnDestroy {
  @Input() public closeModal: () => void;
  @Input() public taskTemplate?: ITaskTemplate;
  public wording = wording;
  public form: FormGroup;
  public todosCtrl: FormArray;
  public TaskLanguageType = TaskLanguageType;
  @ViewChildren('todoInput') private inputs: QueryList<ElementRef<HTMLInputElement>>;


  private labelValidators = [
    Validators.required,
    Validators.maxLength(50),
    Validators.minLength(5)
  ];
  private componentDestroyed$ = new ReplaySubject<boolean>(1);

  constructor(
    private taskTemplatesHelperService: TaskTemplatesHelperService,
    private fb: FormBuilder,
    private taskTemplateRepo: TaskTemplateRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.initForm(this.taskTemplate);
  }

  private initForm(
    taskTemplate: ITaskTemplate = {
      color: '#FF0000',
      useStartDate: true,
      todos: [],
      languageType: TaskLanguageType.All,
    } as ITaskTemplate
  ): void {
    this.form = this.fb.group({
      labelDe: [taskTemplate.labelDe, [...this.labelValidators]],
      labelEn: [taskTemplate.labelEn, [...this.labelValidators]],
      useStartDate: [taskTemplate.useStartDate, [Validators.required]],
      color: [taskTemplate.color, [Validators.required]],
      isSystemDefault: [taskTemplate.isSystemDefault],
      languageType: [taskTemplate.languageType, [Validators.required]],
      todos: this.fb.array([], [Validators.required])
    });

    const systemDefaultCtrl = this.form.get('isSystemDefault');
    const languageCtrl = this.form.get('languageType');

    // If user sets Systemdefault, language must be set to “all” and be disabled
    systemDefaultCtrl.valueChanges
      .pipe(
        startWith(systemDefaultCtrl.value),
        takeUntil(this.componentDestroyed$),
        distinctUntilChanged(),
        tap(isSystemDefault => {
          if (isSystemDefault) {
            languageCtrl.setValue(TaskLanguageType.All);
            languageCtrl.disable();
          } else {
            languageCtrl.enable();
          }
          languageCtrl.updateValueAndValidity();
        })
      ).subscribe();

    this.todosCtrl = this.form.get('todos') as FormArray;

    if (taskTemplate.todos.length) {
      taskTemplate.todos.forEach(todo => this.addTodoForm(todo));
    }

    if (!this.todosCtrl.length) {
      this.addTodoForm();
    }
    languageCtrl.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed$),
        distinctUntilChanged(),
        tap(() => {
          const labelEn: AbstractControl = this.form.get('labelEn');
          const labelDe: AbstractControl = this.form.get('labelDe');

          this.enableOrDisableCtrl(labelEn, TaskLanguageType.En);
          this.enableOrDisableCtrl(labelDe, TaskLanguageType.De);

          this.todosCtrl.controls.forEach(formGroup => {
            const deCtrl: AbstractControl = formGroup.get('labelDe');
            const enCtrl: AbstractControl = formGroup.get('labelEn');

            this.enableOrDisableCtrl(enCtrl, TaskLanguageType.En);
            this.enableOrDisableCtrl(deCtrl, TaskLanguageType.De);
          });
        })
      ).subscribe();
  }

  private enableOrDisableCtrl(ctrl: AbstractControl, language: TaskLanguageType): void {
    if (this.isSameLanguageOrAll(language)) {
      ctrl.setValidators(this.labelValidators);
    } else {
      ctrl.setValidators(this.labelValidators.slice(1));
      if (!ctrl.valid) {
        ctrl.patchValue('');
      }
    }
    ctrl.updateValueAndValidity();
  }

  public addTodoForm(todo: ITaskTemplateTodoItem = {} as ITaskTemplateTodoItem): void {
    const languageCtrl = this.form.get('languageType');
    this.todosCtrl.push(this.createTodoForm(todo));

    setTimeout(() => {
      if (
        !this.inputs // no inputs (which shouldn't really happen this case)
        || this.todosCtrl.length === this.taskTemplate?.todos.length // inputs not changed
        || this.todosCtrl.length === 1 // default input (in create mode)
      ) { return; }

      const inputElems: HTMLInputElement[] = this.inputs.map(i => i.nativeElement);

      if (languageCtrl.value === TaskLanguageType.All) {
        inputElems[inputElems.length - 2].focus();
        return;
      }

      inputElems[inputElems.length - 1].focus();
    });
  }

  private createTodoForm(
    todo: ITaskTemplateTodoItem = {} as ITaskTemplateTodoItem
  ): FormGroup {
    return this.fb.group({
      labelDe: [todo.labelDe, this.isSameLanguageOrAll(TaskLanguageType.De)
        ? this.labelValidators
        : []
      ],
      labelEn: [todo.labelEn, this.isSameLanguageOrAll(TaskLanguageType.En)
        ? this.labelValidators
        : []
      ],
    });
  }

  public isSameLanguageOrAll(lang: TaskLanguageType): boolean {
    const languageCtrl = this.form.get('languageType');
    return languageCtrl.value === lang || languageCtrl.value === TaskLanguageType.All;
  }

  public removeTodoForm(i: number) {
    this.todosCtrl.removeAt(i);
  }

  public save(): void {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
      return;
    }

    const {
      useStartDate,
      color,
      isSystemDefault,
      labelDe,
      labelEn,
      languageType,
      todos,
    } = this.form.getRawValue();

    const payload = {
      ...this.taskTemplate,
      useStartDate,
      color,
      isSystemDefault,
      labelDe,
      labelEn,
      languageType,
      todos,
    };

    this.taskTemplate?.id
      ? this.taskTemplateRepo.update({
        ...payload
      }, {}).subscribe(() => this.closeModal())
      : this.taskTemplateRepo.create({
        categoryId: this.taskTemplatesHelperService.selectedCategory.value,
        ...payload
      }, {}).subscribe(() => this.closeModal());

  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
