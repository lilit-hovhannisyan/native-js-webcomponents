import {BaseComponent} from '../../core/base-component/base.component.js';
import {
  externalStyleBasePath,
  canElementBeRendered,
  traverseInShadowAndGetElement
} from '../../helpers/random.helpers.js';

export class DataBindingComponent extends BaseComponent {
  inputValue;

  constructor() {
    super(externalStyleBasePath + 'data-binding-component/data-binding.component.css');
  }

  render = () => {
    this.inputValue = this.getAttribute('value') || '';
    this.placeholder = this.getAttribute('placeholder') || '';
    this.intro = this.getAttribute('intro') || '';

    const dataBingingConfig = {
      tagName: 'div',
      attr: {
        class: 'l-data-binding'
      },
      subElements: [
        {
          tagName: 'input-component',
          attr: {
            value: this.inputValue,
            placeholder: this.placeholder,
          },
          events: {
            input: this.onInput
          }
        },
        {
          tagName: 'div',
          subElements: [
            {
              tagName: 'text-component',
              attr: {
                selector: 'intro-data',
                text: this.intro,
              }
            },
            {
              tagName: 'text-component',
              attr: {
                selector: 'bound-data',
                text: this.inputValue,
                render: true
              }
            },
          ]
        }
      ]
    };

    const dataBingingEl= this.createElementWithAttributes(dataBingingConfig);
    this.removeStaticAtributesFromElNotToRender(this, ['placeholder'])
    this.shadow.appendChild(dataBingingEl);

  }

  connectedCallback() {
      super.connectedCallback(this.render);
  }

  static get observedAttributes() {
    return ['value'];
  }

  onInput = () => {
    this.inputValue = traverseInShadowAndGetElement(this, 'input').value;
    this.setAttribute('value', this.inputValue);

    const textEl = traverseInShadowAndGetElement(this, 'bound-data');
    textEl.textContent = this.inputValue;
  }
}