import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {

  return ({
    gridName: 'taxKeysGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'displayLabel',
        title: wording.general.name,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'code',
        title: wording.accounting.taxKeysLabelCode,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isSystemDefault',
        title: wording.general.systemDefault,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'percentage',
        title: wording.accounting.taxKeysLabelPercentage,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          selectValues: []
        }
      },
      {
        key: 'calculationTypeLabel',
        title: wording.accounting.calculationSettings,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'taxCategoryLabel',
        title: wording.accounting.taxType,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isActive',
        title: wording.accounting.active,
        width: 100,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      }
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editTaxKey',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: actions.edit,
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteTaxKey',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: actions.delete,
      },
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: actions.add,
      }
    ],
    contextMenuButtons: canPerform(Operations.READ) ? getContextMenuButtonsConfig() : [],
  });
};
