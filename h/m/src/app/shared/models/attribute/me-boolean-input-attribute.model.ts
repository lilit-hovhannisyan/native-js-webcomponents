import { FormGroup } from '@angular/forms';
import { MeBooleanInputI } from '@shared/me-components/me-attribute/me-boolean-input/me-boolean-input.interface';
import { DMBooleanAttributeModel } from './attribute.model';

export class MeBooleanInputAttribute implements MeBooleanInputI {
  private _data!: DMBooleanAttributeModel;
  private _form!: FormGroup;

  constructor() {}

  public set data(data: DMBooleanAttributeModel) {
    this._data = data;
  }

  public get data(): DMBooleanAttributeModel {
    return this._data;
  }

  public set form(form: FormGroup) {
    this._form = form;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get displayName(): string {
    return this.data.displayName;
  }

  public get internalName(): string {
    return this.data.internalName;
  }

  public get required(): boolean {
    return this.data.required;
  }

  public get value(): boolean {
    return this.data.value;
  }

  public get disabled(): boolean {
    return this.data.disabled;
  }
}
