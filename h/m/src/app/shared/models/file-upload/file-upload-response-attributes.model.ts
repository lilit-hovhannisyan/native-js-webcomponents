export class FileUploadResponseAttributesModel {
  hash: string = '';
  name: string = '';
  size: number = 0;
}
