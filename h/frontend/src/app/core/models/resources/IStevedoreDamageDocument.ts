import { IResource } from './IResource';

export type StevedoreDamageDocumentKey = number;

interface ISharedProps {
  id: number;
  uploadId: string;
  filename: string;
  remark: string;
  isAttached: boolean;
  uploadedOn: string;
  stevedoreDamageRepairId: number;
}

export interface IStevedoreDamageDocumentBase extends ISharedProps, IResource<StevedoreDamageDocumentKey> { }

export interface IStevedoreDamageDocumentCreate extends ISharedProps { }

export interface IStevedoreDamageDocumentDTO extends IStevedoreDamageDocumentBase { }

export interface IStevedoreDamageDocument extends IStevedoreDamageDocumentDTO { }

export interface IStevedoreDamageDocumentFull extends IStevedoreDamageDocument { }

export interface IStevedoreDamageReportRow extends IStevedoreDamageDocumentFull { }


