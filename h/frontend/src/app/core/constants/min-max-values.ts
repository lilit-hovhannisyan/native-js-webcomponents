export const MAX_SMALL_INT = 32767;
export const MIN_SMALL_INT = -32768;
export const MAX_INT32 = 2147483647;
export const DECIMAL_14_2 = 999999999999.99;
