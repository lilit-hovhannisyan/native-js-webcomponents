import { UserKey } from './../../models/resources/IUser';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IUser, IUserCreate, IUserDTO } from '../../models/resources/IUser';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { UserActivation } from '../../../authentication/models/UserActivation';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { HttpPost } from '../../models/http/http-post';
import { HttpGet } from '../../models/http/http-get';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';

@Injectable({
  providedIn: 'root'
})
export class UserRepositoryService extends AbstractRepository<
IUser,
IUserDTO,
IUserCreate,
RepoParams<IUser>,
UserKey
> {

  protected resourceType = 'User';
  public sortAsce = true;
  public sortKey = 'username';

  constructor(
    repositoryService: RepositoryService,
    private authService: AuthenticationService,
    private httpClient: HttpClient
  ) {
    super(repositoryService);
  }

  protected postProcess(item: IUserDTO): IUser {
    return item;
  }

  public getBaseUrl = () => '/api/users';

  public sendActivation(userIdent: string | number): Observable<any> {
    let api = 'api/user-account';
    let method = 'send-activation';
    const payload: UserActivation = {
      baseUrl: `${location.origin}/auth/reset-password`,
    };

    if (userIdent && Number(userIdent)) {
      if (this.authService.getUser().isSuperAdmin || this.authService.getUser().isAdmin) {
        payload.userId = +userIdent;
        api = '/api/users';
      } else {
        method = '/send-activation-user';
      }
    } else {
      payload.email = <string>userIdent;
    }

    const request = new HttpPost(`/${api}/${method}`, payload);
    return this.repositoryService.httpClient.request(request);
  }

  public isConfirmationCodeValid(userId: number, confirmationCode: string): Observable<boolean> {
    const url = '/api/user-account/password/validate-user-confirmationcode';
    const request = new HttpGet(
      url,
      {
        init: {
          params: {
            UserId: userId.toString(),
            ConfirmationCode: confirmationCode,
          }
        }
      }
    );
    return this.httpClient.request(request)
      .pipe(
        map((response: HttpResponse<IDataResponseBody<{ isValid: boolean }>>) => {
          return response.body.data.isValid;
        })
      );
  }

  /**
   * We use this method to set password for newly created users without an email
   * this might be used for other cases in future too
   * @param uid user id
   * @param password
   */
  public setPassword(uid: number, password: string): Observable<void> {
    const url = `${this.getBaseUrl()}/${uid}/password`;
    const request = new HttpPost(url, { password });
    const request$ = this.repositoryService.httpClient.request(request)
      .pipe(map(() => null));
    return request$;
  }
}
