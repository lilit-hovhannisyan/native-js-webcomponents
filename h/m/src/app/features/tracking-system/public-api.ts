/*
 * Public API Surface of ui-components
 */

export * from './services/logging.service';
export * from './base-logger.component';
export * from './interceptors/tracking-system.interceptor';

export type { default as LoggingConfigI } from './interfaces/config.interface';
export type { default as TrackerConfigI } from './interfaces/tracker-config.interface';
import SkipOptionsEnum from './enums/config-values.enum';
import EventTypesEnum from './enums/config-values.enum';

export const SkipOptions = SkipOptionsEnum;
export const EventTypes = EventTypesEnum;
