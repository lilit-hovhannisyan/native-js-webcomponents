import { map, share } from 'rxjs/operators';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IUpload } from '../../core/models/resources/IUpload';
import { Observable, of } from 'rxjs';
import { IDataResponseBody } from 'src/app/core/models/responses/IDataResponseBody';
import { IResourceCollection } from 'src/app/core/models/resources/IResourceCollection';
import { getExtension } from 'mime';
import * as FileSaver from 'file-saver';
import { HttpGet } from 'src/app/core/models/http/http-get';
import { HttpPost } from 'src/app/core/models/http/http-post';

/**
 * It is used to for almost all kind of files, to upload those to the server
 */
@Injectable({
  providedIn: 'root'
})
export class FileUploaderService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  public upload(data: FormData, folder: string): Observable<IUpload> {
    const request = new HttpPost(`/api/uploads/${folder}`, data);
    const c = this.httpClient.request<IDataResponseBody<IResourceCollection<IUpload>>>
      (request).pipe(
        map((upload: any) => upload.body.data.items[0]),
        share()
      );

    return c;
  }

  public getUpload(id): Observable<IUpload> {
    const request = new HttpGet(`/api/uploads/${id}`);
    const c = this.httpClient.request<IDataResponseBody<IResourceCollection<IUpload>>>
      (request).pipe(
        map((upload: any) => upload.body.data),
        share()
      );

    return c;
  }

  public getUploads(ids: string[]): Observable<IUpload[]> {
    if (ids.length === 0) {
      return of([]);
    }

    const request = new HttpGet(`/api/uploads`, { init: { params: { ids } } });
    const c = this.httpClient.request<IDataResponseBody<IResourceCollection<IUpload>>>
      (request).pipe(
        map((upload: any) => upload.body.data.items),
        share()
      );

    return c;
  }

  public download(id: string, filename?: string): Observable<void> {
    const request = new HttpGet(
      `/api/uploads/${id}/file`,
      {
        init: {
          responseType: 'blob',
          observe: 'response'
        }
      }
    );
    const source = this.httpClient.request(request)
      .pipe(
        map((response: HttpResponse<Blob>) => {
          const contentType = response.headers.get('content-type');
          const ext = getExtension(contentType);
          const blob = new Blob([response.body], { type: contentType });
          const _filename = filename || `download.${ext}`;

          FileSaver.saveAs(blob, _filename);
        }));

    return source;
  }
}
