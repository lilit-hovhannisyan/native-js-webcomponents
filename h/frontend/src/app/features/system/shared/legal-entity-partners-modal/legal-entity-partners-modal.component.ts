import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { wording } from 'src/app/core/constants/wording/wording';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { ICompany, CompanyKey } from 'src/app/core/models/resources/ICompany';
import { Observable, of } from 'rxjs';
import { COMPANY_LEGAL_FORM, getCompanyLegalFormTypes, COMPANY_LEGAL_FORM_TYPE, getCompanyLegalForm } from 'src/app/core/models/enums/company-legal-form';
import { distinctUntilChanged, map, switchMap, startWith, finalize } from 'rxjs/operators';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { ISalutation } from 'src/app/core/models/resources/ISalutation';
import { SalutationRepositoryService } from 'src/app/core/services/repositories/salutation-repository.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { Validators } from 'src/app/core/classes/validators';
import { ICompanyLegalEntity } from 'src/app/core/models/resources/ICompanyLegalEntity';
import { CompanyLegalEntityRepositoryService } from 'src/app/core/services/repositories/company-legal-entity-repository.service';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';

@Component({
  templateUrl: './legal-entity-partners-modal.component.html',
  styleUrls: ['./legal-entity-partners-modal.component.scss']
})
export class LegalEntityPartnersModalComponent implements OnInit {
  @Input() public partnersIds: CompanyKey[];
  @Input() public isGeneralPartners = false;
  @Input() public onlyCreate = false;
  public form: FormGroup;
  public wording = wording;
  public legalFormOptions: ISelectOption<COMPANY_LEGAL_FORM>[] = getCompanyLegalForm();
  public legalFormTypesOptions: ISelectOption<COMPANY_LEGAL_FORM_TYPE>[] = [];
  public companiesObservable: Observable<ICompany[]>;
  public salutations: ISalutation[] = [];
  public hasCommercialFields = false;
  public isHRA = false;
  public isRequestPending = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private companyRepo: CompanyRepositoryService,
    private salutationService: SalutationRepositoryService,
    private confirmationService: ConfirmationService,
    private companyLegalEntityRepo: CompanyLegalEntityRepositoryService,
  ) { }

  public ngOnInit() {
    this.companyRepo.fetchAll({}).subscribe();
    this.companiesObservable = this.companyRepo.getStream().pipe(map(companies => {
      return companies.filter(c => this.partnersIds.every(id => c.id !== id));
    }));

    this.fetchSalutations();
    this.init();
  }

  private init(): void {
    this.form = this.fb.group({
      id: [null, [Validators.required]],
      defineManually: [this.onlyCreate],
      isPrivatePerson: [],
      salutationId: [],
      firstName: [],
      lastName: [],
      companyName: [],
      legalForm: [],
      legalFormType: [],
      hra: [null, [Validators.number]],
      hrb: [null, [Validators.number]],
      commercialRegister: [null, [Validators.maxLength(100)]],
    });

    this.legalFormHandler();
    this.partnerChangeHandler();
    this.defineManuallyHandler();
  }

  private fetchSalutations(): void {
    this.salutationService.fetchAll({}).subscribe(salutations => {
      this.salutations = salutations;
    });
  }

  private defineManuallyHandler(): void {
    const { id, defineManually, isPrivatePerson } = this.form.controls;

    defineManually.valueChanges.pipe(
      startWith(defineManually.value),
      distinctUntilChanged(),
    ).subscribe((value: boolean) => {
      if (value) {
        isPrivatePerson.setValue(true);
        id.setValue(null);
        id.clearValidators();
        id.updateValueAndValidity();
        id.disable();
        return;
      }

      /**
       * When `defineManually` checkbox is unchecked those controls are unnecessary and
       * shouldn't be used - they shouldn't have validators and values.
       */
      const unnecessaryControlsNames: string[] = ['isPrivatePerson', 'salutationId', 'firstName',
        'lastName', 'companyName', 'legalForm', 'legalFormType', 'hra', 'hrb', 'commercialRegister'];

      unnecessaryControlsNames.forEach(controlName => {
        const control: AbstractControl = this.form.get(controlName);
        control.patchValue(null);
        control.clearValidators();
        control.updateValueAndValidity();
      });

      id.enable();
      id.setValidators(Validators.required);
      id.updateValueAndValidity();
    });
  }

  private defineManuallyDisablingModal(): void {
    this.confirmationService.confirm({
      wording: wording.system.businessPartnerDeletionWarning,
      okButtonWording: wording.general.save,
      cancelButtonWording: wording.general.cancel,
    }).subscribe(confirmed => {
      if (!confirmed) {
        this.form.controls.defineManually.setValue(true);
        return;
      }

      const value: { id: CompanyKey } = this.form.value;
      this.form.reset();
      this.form.patchValue({ id: value.id });
    });
  }

  public defineManuallyClickHandler(): void {
    const { defineManually } = this.form.controls;

    if (defineManually.value) {
      if (this.hasBusinessPartnerChanges()) {
        return this.defineManuallyDisablingModal();
      }
      return defineManually.setValue(false);
    }

    defineManually.setValue(true);
  }

  private hasBusinessPartnerChanges(): boolean {
    const { isPrivatePerson } = this.form.controls;

    const fields: string[] = isPrivatePerson.value
      ? ['salutationId', 'firstName', 'lastName']
      : ['companyName', 'legalForm', 'legalFormType', 'hra', 'hrb', 'commercialRegister'];

    return fields.some(fieldName => this.form.get(fieldName).value);
  }

  private partnerChangeHandler(): void {
    const {
      isPrivatePerson,
      salutationId,
      firstName,
      lastName,
      companyName,
      legalForm,
      legalFormType,
      hra,
      hrb,
      commercialRegister,
    } = this.form.controls;
    const privatePersonFields: AbstractControl[] = [salutationId, firstName, lastName];

    isPrivatePerson.valueChanges.pipe(distinctUntilChanged()).subscribe((value: boolean) => {
      if (value) {
        privatePersonFields.forEach(control => {
          control.setValidators(Validators.required);
          control.updateValueAndValidity();
        });
        this.clearValidationAndValue(
          // company fields
          [companyName, legalForm, legalFormType, hra, hrb, commercialRegister]
        );
      } else {
        companyName.setValidators(Validators.required);
        companyName.updateValueAndValidity();
        hra.setValidators(Validators.number);
        hra.updateValueAndValidity();
        hrb.setValidators(Validators.number);
        hrb.updateValueAndValidity();
        commercialRegister.setValidators(Validators.maxLength(100));
        commercialRegister.updateValueAndValidity();
        this.clearValidationAndValue(privatePersonFields);
      }
    });
  }

  private clearValidationAndValue(controls: AbstractControl[]): void {
    controls.forEach(c => {
      c.clearValidators();
      c.patchValue(null);
      c.updateValueAndValidity();
    });
  }

  public submit(): void {
    validateAllFormFields(this.form);
    if (!this.form.valid || this.isRequestPending) {
      return;
    }

    const value: ICompany & ICompanyLegalEntity = this.form.value;

    if (value.id) {
      // already existed company
      this.modalRef.close(value.id);
      return;
    }

    const freshValue: ICompany = {
      ...value,
      companyName: value.companyName || `${value.lastName}, ${value.firstName}`
    };

    const hasLegalEntityFields: boolean = !!(value.hra || value.hrb || value.commercialRegister);

    this.isRequestPending = true;
    this.companyRepo.create(freshValue, {}).pipe(
      switchMap((newCompany: ICompany) => {
        if (!hasLegalEntityFields) {
          return of(newCompany.id);
        }

        const legalEntity: ICompanyLegalEntity = {
          hra: value.hra,
          hrb: value.hrb,
          commercialRegister: value.commercialRegister,
          companyId: newCompany.id,
          partners: [],

        } as ICompanyLegalEntity;

        return this.companyLegalEntityRepo.create(legalEntity, {}).pipe(
          map(() => newCompany.id),
        );
      }),
    ).subscribe(
      newCompanyId => this.modalRef.close(newCompanyId),
      () => this.isRequestPending = false,
    );
  }

  public cancel() {
    this.modalRef.close();
  }

  private legalFormHandler(): void {
    this.form.controls.legalForm.valueChanges.pipe(
      distinctUntilChanged(),
    ).subscribe((value: COMPANY_LEGAL_FORM) => {
      this.setCompanyLegalFormTypes(value);
      this.toggleCommercialFields(value);
    });
  }

  private toggleCommercialFields(legalForm: COMPANY_LEGAL_FORM): void {
    switch (legalForm) {
      case COMPANY_LEGAL_FORM.LimitedPartnership: // KG
        this.hasCommercialFields = true;
        this.isHRA = true;
        break;
      case COMPANY_LEGAL_FORM.CompanyWithLimitedLiability: // GmbH
      case COMPANY_LEGAL_FORM.StockCorporation: // AG
        this.hasCommercialFields = true;
        this.isHRA = false;
        break;
      case COMPANY_LEGAL_FORM.CivilLawPartnership: // GbR
      case COMPANY_LEGAL_FORM.ForeignCompany: // (Foreign Company)
      default:
        this.hasCommercialFields = false;
        break;
    }
  }

  private setCompanyLegalFormTypes(legalForm: COMPANY_LEGAL_FORM) {
    const legalFormTypeControl: AbstractControl = this.form.controls.legalFormType;
    this.legalFormTypesOptions = getCompanyLegalFormTypes(legalForm);

    if (this.legalFormTypesOptions.length) {
      legalFormTypeControl.setValidators(Validators.required);
      legalFormTypeControl.enable();
      const defaultType: ISelectOption<COMPANY_LEGAL_FORM_TYPE> = this.legalFormTypesOptions.find(t => t.default);
      legalFormTypeControl.patchValue(defaultType.value);
    } else {
      legalFormTypeControl.clearValidators();
      legalFormTypeControl.patchValue(null);
      legalFormTypeControl.disable();
    }
    legalFormTypeControl.updateValueAndValidity();
  }
}
