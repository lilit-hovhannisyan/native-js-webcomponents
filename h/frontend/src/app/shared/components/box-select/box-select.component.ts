import { AfterViewInit, Component, ContentChildren, QueryList } from '@angular/core';
import { AbstractValueAccessor, makeProvider } from 'src/app/core/abstracts/value-accessor';
import { wording } from 'src/app/core/constants/wording/wording';
import { BoxSelectItemComponent } from 'src/app/shared/components/box-select/box-select-item/box-select-item.component';

@Component({
  selector: 'nv-box-select',
  templateUrl: './box-select.component.html',
  styleUrls: [ './box-select.component.scss' ],
  providers: [ makeProvider(BoxSelectComponent) ]
})
export class BoxSelectComponent extends AbstractValueAccessor implements AfterViewInit {
  public wording = wording;
  @ContentChildren(BoxSelectItemComponent, { descendants: true })
  public selectItems: QueryList<BoxSelectItemComponent>;
  public selectedItem: BoxSelectItemComponent;

  public ngAfterViewInit(): void {
    this.selectItems.changes.subscribe(items => {
      /***
       * we don't need to unsubscribe from `clickEmitter`
       * because every time we have new instances of `BoxSelectItemComponent`
       */
      items.forEach(item => item.clickEmitter.subscribe(() => this.onItemClick(item)));

      this.selectedItem = items.length ? items.find(item => item.value === this.value) : null;

      // avoid ExpressionChangedAfterItHasBeenCheckedError
      setTimeout(() => {
        if (this.selectedItem) {
          this.selectedItem.handleClick();
        }
      });
    });
  }

  public onItemClick(item: BoxSelectItemComponent) {
    const prevState = item.selected;
    this.unselect();
    item.selected = !prevState;
    this.selectedItem = item.selected ? item : null;
    this.onChange(this.selectedItem ? this.selectedItem.value : null);
  }

  private unselect(): void {
    if (this.selectedItem) {
      this.selectedItem.selected = false;
      this.selectedItem = null;
    }
  }
}
