import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserCreateEditComponent } from './user-create-edit.component';

const routes: Routes = [{ path: '', component: UserCreateEditComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserCreateEditRoutingModule {}
