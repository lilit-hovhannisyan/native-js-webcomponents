import { ISelectOption } from './../../../../../core/models/misc/selectOption';
import { url } from './../../../../../core/constants/sitemap/sitemap-entry';
import { Component, ElementRef, OnInit, ChangeDetectorRef } from '@angular/core';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { FormGroup, FormBuilder, FormArray, AbstractControl } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { getAccountingRelationTypesOptions, getCreditorDebitorOptions } from './options';
import { SettingsRepositoryService } from 'src/app/core/services/repositories/settings-repository.service';
import { IStartEndRange, hasOverlaps, ISettings, hasGaps } from 'src/app/core/models/resources/ISettings';
import { ACCOUNTING_RELATIONS } from 'src/app/core/models/enums/accounting-relations';
import { DEBITOR_CREDITOR } from 'src/app/core/models/enums/debitor-creditor';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { NotificationType } from 'src/app/core/models/INotification';
import { wording } from 'src/app/core/constants/wording/wording';
import { Router } from '@angular/router';
import { ACCOUNT_TYPE_ACCRUALS } from 'src/app/core/models/enums/account-type-accruals';
import { forkJoin } from 'rxjs';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { AccountValidatorsService } from 'src/app/core/services/validators/account-validators.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';

@Component({
  selector: 'nv-global-settings',
  templateUrl: './global-settings.component.html',
  styleUrls: ['./global-settings.component.scss']
})
export class GlobalSettingsComponent implements OnInit {
  private accountingRelationRangesForm: FormArray;
  private debitorCreditorRangesForm: FormArray;
  public form: FormGroup;
  public accountingRelationTypesOptions: ISelectOption[] = [];
  public creditorDebitorOptions: ISelectOption[] = [];
  public repoParams = {};
  public routeBackUrl = url(sitemap.system.children.settings);
  public overlappedAccountingRelation: string;
  public overlappedDebitorCreditor: string;
  private settings: ISettings;
  public wording = wording;
  public ACCOUNT_TYPE_ACCRUALS = ACCOUNT_TYPE_ACCRUALS;
  private defaultPrepaidExpensesId = null;
  private defaultPrepaidExpensesNo = null;
  private defaultDeferredIncomeId = null;
  private defaultDeferredIncomeNo = null;
  public formElement: ElementRef;

  constructor(
    private fb: FormBuilder,
    private settingsRepo: SettingsRepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    private notificationsService: NotificationsService,
    private router: Router,
    private bookingAccountValidatorsService: AccountValidatorsService,
    private confirmationService: ConfirmationService,
    private el: ElementRef,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit() {
    this.initOptions();
    forkJoin([
      this.settingsRepo.fetchAll(),
      this.bookingAccountRepo.fetchAll({})
    ])
      .subscribe(([settings]) => {
        this.settings = settings;
        this.initForm(settings);
      });
  }

  public initForm(systemSettings: ISettings) {
    this.form = this.fb.group({
      debitorCreditorRanges: this.fb.array(
        systemSettings.system.debitorCreditorRanges.map(ranges => this.rangeFormBuilder(ranges))
      ),
      accountingRelationRanges: this.fb.array(
        systemSettings.system.accountingRelationRanges.map(ranges => this.rangeFormBuilder(ranges))
      ),
      emailDomains: [systemSettings.system.emailDomains],
      defaultAccountPrepaidExpensesId: [systemSettings.defaultAccountPrepaidExpensesId, [
        Validators.required,
        this.bookingAccountValidatorsService.doesBookingAccountExist,
        this.bookingAccountValidatorsService.isBookingAccountActive,
        this.bookingAccountValidatorsService.isAccountPrepaidExpenses
      ]],
      defaultAccountDeferredIncomeId: [systemSettings.defaultAccountDeferredIncomeId, [
        Validators.required,
        this.bookingAccountValidatorsService.doesBookingAccountExist,
        this.bookingAccountValidatorsService.isBookingAccountActive,
        this.bookingAccountValidatorsService.isAccountDeferredIncome
      ]],
    });

    this.defaultPrepaidExpensesId = this.form.get('defaultAccountPrepaidExpensesId')?.value;
    this.defaultPrepaidExpensesNo = this.defaultPrepaidExpensesId
      && this.bookingAccountRepo.getPropertyById(this.defaultPrepaidExpensesId, 'no');

    this.defaultDeferredIncomeId = this.form.get('defaultAccountDeferredIncomeId')?.value;
    this.defaultDeferredIncomeNo = this.defaultDeferredIncomeId
      && this.bookingAccountRepo.getPropertyById(this.defaultDeferredIncomeId, 'no');

    this.accountingRelationRangesForm = this.form.get('accountingRelationRanges') as FormArray;
    this.debitorCreditorRangesForm = this.form.get('debitorCreditorRanges') as FormArray;

    this.form.disable({ emitEvent: false });

    const {
      defaultAccountPrepaidExpensesId,
      defaultAccountDeferredIncomeId
    } = this.form.controls;

    defaultAccountPrepaidExpensesId.valueChanges.subscribe(value => {
      if (value) {
        const foundBookingAccount: IBookingAccount = this.bookingAccountRepo.getStreamValue()
          .find(bookingAccount => bookingAccount.id === value);

        this.selectValueChanged(foundBookingAccount, ACCOUNT_TYPE_ACCRUALS.AccountPrepaidExpenses);
      }
    });

    defaultAccountDeferredIncomeId.valueChanges.subscribe(value => {
      if (value) {
        const foundBookingAccount: IBookingAccount = this.bookingAccountRepo.getStreamValue()
          .find(bookingAccount => bookingAccount.id === value);

        this.selectValueChanged(foundBookingAccount, ACCOUNT_TYPE_ACCRUALS.AccountDeferredIncome);
      }
    });
  }

  private initOptions() {
    this.accountingRelationTypesOptions = getAccountingRelationTypesOptions();
    this.creditorDebitorOptions = getCreditorDebitorOptions();
  }

  private rangeFormBuilder(value = {} as any): FormGroup {
    return this.fb.group({
      start: [value.start, [Validators.required, Validators.wholeNumber]],
      end: [value.end, [
        Validators.required,
        Validators.wholeNumber,
        Validators.isGreaterThanFieldValue('start'),
      ]],
      type: [value.type, [Validators.required]]
    });
  }

  public addAccountingRelation = () => this.accountingRelationRangesForm.push(this.rangeFormBuilder());

  public addAccountingType = () => this.debitorCreditorRangesForm.push(this.rangeFormBuilder());

  public removeAccountingRelation = (i: number) => this.accountingRelationRangesForm.removeAt(i);

  public removeAccountingType = (i: number) => this.debitorCreditorRangesForm.removeAt(i);

  public submit() {
    this.removeErrors(this.form.get('accountingRelationRanges') as FormArray);
    this.removeErrors(this.form.get('debitorCreditorRanges') as FormArray);
    const noOverlaps: boolean = this.validateOverlapping();
    const noGaps: boolean = this.validateGaps();
    if (this.form.invalid) { return; }
    if (this.form.valid && noOverlaps && noGaps) {
      const rawValue = this.form.getRawValue();
      const value = {
        system: {
          accountingRelationRanges: rawValue.accountingRelationRanges,
          debitorCreditorRanges: rawValue.debitorCreditorRanges,
          emailDomains: rawValue.emailDomains
        },
        defaultAccountDeferredIncomeId: rawValue.defaultAccountDeferredIncomeId,
        defaultAccountPrepaidExpensesId: rawValue.defaultAccountPrepaidExpensesId
      };

      this.settingsRepo.update(value).subscribe(settings => {
        this.settings = settings;
        this.form.disable();
      });
    } else if (!noOverlaps) {
      this.notificationsService.notify(NotificationType.Warning,
        wording.general.warning,
        wording.system.overlappedRangesFound
      );
    } else if (!noGaps) {
      this.notificationsService.notify(NotificationType.Warning,
        wording.general.warning,
        wording.system.gapsRangesFound,
      );
    } else {
      this.notificationsService.notify(NotificationType.Warning,
        wording.general.warning,
        wording.general.invalidFields
      );
    }
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

  public routeBack = () => {
    const routeBackPath = url(sitemap.system.children.settings);
    this.router.navigate([routeBackPath]);
  }

  /**
  * Returns true if all ranges are valid and there are no overlaps
  */
  private validateOverlapping = (): boolean => {
    const { accountingRelationRanges, debitorCreditorRanges }:
      {
        accountingRelationRanges: IStartEndRange<ACCOUNTING_RELATIONS>[],
        debitorCreditorRanges: IStartEndRange<DEBITOR_CREDITOR>[]
      }
      = this.form.value;
    const accountingRelationOverlaps = hasOverlaps(accountingRelationRanges);
    const debitorCreditorOverlaps = hasOverlaps(debitorCreditorRanges);
    if (accountingRelationOverlaps.length) {
      const accountingRelationsFormArray =
        this.form.get('accountingRelationRanges') as FormArray;
      this.setErrorsOnControls(accountingRelationsFormArray, accountingRelationOverlaps);
    }

    if (debitorCreditorOverlaps.length) {
      const debitorCreditorRangesForm =
        this.form.get('debitorCreditorRanges') as FormArray;
      this.setErrorsOnControls(debitorCreditorRangesForm, debitorCreditorOverlaps);
    }

    return !(debitorCreditorOverlaps.length || accountingRelationOverlaps.length);
  }

  /**
  * Returns true if all accounting relation ranges are valid and there are no gaps
  */
  private validateGaps = (): boolean => {
    const { accountingRelationRanges }:
      {
        accountingRelationRanges: IStartEndRange<ACCOUNTING_RELATIONS>[],
      }
      = this.form.value;

    const accountingRelationGaps = hasGaps(accountingRelationRanges);

    if (accountingRelationGaps.length) {
      const accountingRelationsFormArray =
        this.form.get('accountingRelationRanges') as FormArray;
      this.setErrorsOnControls(accountingRelationsFormArray, accountingRelationGaps);
    }

    return !accountingRelationGaps.length;
  }

  private setErrorsOnControls(rangesFormArr: FormArray, overlaps: IStartEndRange<any>[]) {
    const error = {
      error: { message: wording.system.overlappedRange }
    };
    overlaps.forEach(range => {
      const fb: AbstractControl = rangesFormArr.controls.find(c => {
        const val: IStartEndRange<any> = c.value;
        return val.start === range.start
          && val.end === range.end;
      });
      const startCtrl = fb.get('start');
      const endCtrl = fb.get('end');
      startCtrl.setErrors(error, { emitEvent: false });
      endCtrl.setErrors(error, { emitEvent: false });
      startCtrl.markAsTouched();
      endCtrl.markAsTouched();
    });
  }

  private removeErrors(formArray: FormArray) {
    formArray.controls.map(
      fb => {
        fb.get('start').updateValueAndValidity();
        fb.get('end').updateValueAndValidity();
      }
    );
  }

  public enterEditMode() {
    this.form.enable({ emitEvent: false });
  }

  public exitEditMode() {
    this.initForm(this.settings);
    // need a little delay to disable autocomplete fields
    setTimeout(() => this.form.disable());
  }

  public selectValueChanged(bookingAccount: IBookingAccount, accountTypeAccruals: ACCOUNT_TYPE_ACCRUALS): void {
    if (bookingAccount) {
      if (accountTypeAccruals === ACCOUNT_TYPE_ACCRUALS.AccountPrepaidExpenses
        && this.defaultPrepaidExpensesId !== bookingAccount?.id) {
        this.confirmationService.confirm({
          subjects: {
            NEWACCOUNT: bookingAccount?.no,
            DEFAULTACCOUNT: this.defaultPrepaidExpensesNo
          },
          wording: wording.accounting.requestIsGoingToChangeDefaultAccountPrepaidExpenses,
        }).subscribe(confirmed => {
          if (confirmed) {
            this.defaultPrepaidExpensesId = bookingAccount?.id;
            this.defaultPrepaidExpensesNo = bookingAccount?.no;
          } else {
            this.form.get('defaultAccountPrepaidExpensesId').setValue(this.defaultPrepaidExpensesId);
          }
        });
      } else {
        if (accountTypeAccruals === ACCOUNT_TYPE_ACCRUALS.AccountDeferredIncome
          && this.defaultDeferredIncomeId !== bookingAccount?.id) {
          this.confirmationService.confirm({
            subjects: {
              NEWACCOUNT: bookingAccount?.no,
              DEFAULTACCOUNT: this.defaultDeferredIncomeNo
            },
            wording: wording.accounting.requestIsGoingToChangeDefaultAccountDeferredIncome,
          }).subscribe(confirmed => {
            if (confirmed) {
              this.defaultDeferredIncomeId = bookingAccount?.id;
              this.defaultDeferredIncomeNo = bookingAccount?.no;
            } else {
              this.form.get('defaultAccountDeferredIncomeId').setValue(this.defaultDeferredIncomeId);
            }
          });
        }
      }
    }
  }
}
