import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { ICostUnit, ICostUnitCreate, CostUnitKey } from '../../../../../../core/models/resources/ICostUnit';
import { FormGroup, FormBuilder } from '@angular/forms';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { CostUnitRepositoryService } from 'src/app/core/services/repositories/cost-unit-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { ActivatedRoute } from '@angular/router';
import { Validators } from 'src/app/core/classes/validators';
import { CostUnitCategoryRepositoryService } from 'src/app/core/services/repositories/cost-unit-category-repository.service';
import { RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ICostUnitCategory } from '../../../../../../core/models/resources/ICostUnitCategory';
import { MandatorCostUnitRepositoryService } from '../../../../../../core/services/repositories/mandator-cost-unit-repository.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { GridComponent, NvGridConfig } from 'nv-grid';
import { getMandatorCostUnitsGridConfig } from 'src/app/core/constants/nv-grid-configs/mandator-cost-units.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { concatMap, map, switchMap, tap } from 'rxjs/operators';
import { MandatorCostUnitMainService } from 'src/app/core/services/mainServices/mandator-cost-unit-main.service';
import { IMandatorCostUnit, IMandatorCostUnitRow } from '../../../../../../core/models/resources/IMandatorCostUnit';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { NzModalService } from 'ng-zorro-antd/modal';
import { CostUnitMandatorsModalComponent } from './cost-unit-mandators-modal/cost-unit-mandators-modal.component';
import { Language } from 'src/app/core/models/language';

@Component({
  selector: 'nv-cost-units-detail-view',
  templateUrl: './cost-units-detail-view.component.html',
  styleUrls: ['./cost-units-detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CostUnitsDetailViewComponent
  extends ResourceDetailView<ICostUnit, ICostUnitCreate>
  implements OnInit {

  @ViewChild('mandatorsCostUnitGrid') public mandatorsCostUnitGrid: GridComponent;

  protected repoParams: RepoParams<ICostUnit> = {};
  public form: FormGroup;
  protected resourceDefault: ICostUnit = { isActive: true } as ICostUnit;
  public routeBackUrl: string = url(
    sitemap.accounting.children.costAccounting.children.costUnits
  );
  public costUnitCategories$: Observable<ICostUnitCategory[]>;
  public mandatorCostUnitsSubject$: BehaviorSubject<IMandatorCostUnitRow[]>
    = new BehaviorSubject<IMandatorCostUnitRow[]>([]);
  public mandatorCostUnits$: Observable<IMandatorCostUnitRow[]>
    = this.mandatorCostUnitsSubject$.asObservable();
  private id: CostUnitKey;
  public mandatorCostUnitGridConfig: NvGridConfig;
  private language: Language;
  private defaultMandatorCostUnits: IMandatorCostUnit[] = [];
  public formElement: ElementRef;

  constructor(
    private fb: FormBuilder,
    costUnitRepo: CostUnitRepositoryService,
    baseComponentService: BaseComponentService,
    private route: ActivatedRoute,
    private costUnitCategoryRepo: CostUnitCategoryRepositoryService,
    private mandatorCostUnitRepo: MandatorCostUnitRepositoryService,
    private mandatorCostUnitService: MandatorCostUnitMainService,
    public gridConfigService: GridConfigService,
    private modalService: NzModalService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef
  ) {
    super(costUnitRepo, baseComponentService);
  }

  public getTitle: () => string = (): string => (
    `${this.resource.no} ${this.resource.displayLabel[this.settingsService.language]}`
  )

  public ngOnInit(): void {
    this.language = this.settingsService.language;
    this.fetchCostUnitCategories();
    this.initResource();
    this.initMandatorCostUnits();
    this.initEditModeListener();
  }

  public init(costUnit: ICostUnit = this.resourceDefault): void {
    this.initForm(this.fb.group({
      costUnitCategoryId: [costUnit.costUnitCategoryId, [Validators.required]],
      no: [costUnit.no],
      labelDe: [costUnit.labelDe, [Validators.required, Validators.maxLength(150)]],
      labelEn: [costUnit.labelEn, [Validators.required, Validators.maxLength(150)]],
      remark: [costUnit.remark],
      isActive: [costUnit.isActive],
    }));

    this.disableField('no');
    if (!this.creationMode) {
      this.disableField('costUnitCategoryId');
    }
  }

  private fetchCostUnitCategories(): void {
    this.costUnitCategoryRepo.fetchAll({}).subscribe();
    this.costUnitCategories$ = this.costUnitCategoryRepo.getStream();
  }

  private initResource(): void {
    const { id } = this.route.snapshot.params;
    this.id = +id;

    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(this.id);
  }

  private initMandatorCostUnits(): void {
    if (!this.creationMode) {
      this.mandatorCostUnitService
        .getStream(this.id)
        .pipe(
          map(mandatorCostUnits => this.mandatorCostUnitService
            .toRows(mandatorCostUnits)),
            concatMap((value, index) => index === 0
              ? of(value).pipe(
                  // this is done to catch and save the first emitted(initial) value to
                  // compare changes later on submit method
                  tap(() => this.defaultMandatorCostUnits = value)
                )
              : of(value)
            )
        ).subscribe(mandatorCostUnits => {
          if (!this.editMode) {
            this.mandatorCostUnitsSubject$.next(mandatorCostUnits);
          }
        });
    }

    const gridActions: IGridConfigActions = {
      edit: () => {},
      delete: (row: IMandatorCostUnitRow) => this.deleteMandatorCostUnit(row),
      add: () => this.openMandatorsModal(),
    };

    const canPerform: (operation: number) => boolean = (operation: number) => {
      return this.authService.canPerformOperationOnCurrentUrl(operation);
    };
    this.mandatorCostUnitGridConfig = getMandatorCostUnitsGridConfig(
      gridActions,
      canPerform,
      this.isInEditMode,
    );
  }

  /**
   * We need to refresh the grid every time edit mode changes, so it could correctly
   * detect buttons disabled status
   */
  private initEditModeListener(): void {
    this.inEditMode$.subscribe(() => {
      this.mandatorsCostUnitGrid?.refreshGrid();
    });
  }

  private openMandatorsModal(): void {
    this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.accounting.mandators[this.language],
      nzClosable: true,
      nzContent: CostUnitMandatorsModalComponent,
      nzComponentParams: {
        mandatorCostUnits: this.mandatorCostUnitsSubject$.getValue(),
      },
      nzFooter: [
        {
          label: wording.general.cancel[this.language],
          onClick: (componentInstance: CostUnitMandatorsModalComponent): void => {
            componentInstance.nzModalRef.close();
          },
        },
        {
          label: wording.general.add[this.language],
          disabled: componentInstance => !componentInstance.selectedMandators.length,
          onClick: this.onMandatorsAdding(),
        },
      ]
    });
  }

  private onMandatorsAdding(): (
    componentInstance: CostUnitMandatorsModalComponent
  ) => void {
    return (componentInstance: CostUnitMandatorsModalComponent): void => {
      const newMandatorCostUnits: IMandatorCostUnitRow[] = [];
      componentInstance.selectedMandators.forEach(m => {
        const mandatorCostUnit: IMandatorCostUnitRow = {
          mandator: m,
          mandatorId: m.id,
          costUnitId: this.creationMode ? null : this.id,
        } as IMandatorCostUnitRow;
        newMandatorCostUnits
          .push(this.mandatorCostUnitService.toRow(mandatorCostUnit));
      });
      this.mandatorCostUnitsSubject$
        .next([...componentInstance.mandatorCostUnits, ...newMandatorCostUnits]);
      componentInstance.nzModalRef.close();
    };
  }

  private deleteMandatorCostUnit: (mandatorCostUnit: IMandatorCostUnitRow) => void = (
    mandatorCostUnit: IMandatorCostUnitRow
  ): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: setTranslationSubjects(
        wording.general.areYouSureYouWantToDelete,
        { subject: mandatorCostUnit.shortName },
      ),
    }).subscribe(confirmed => {
      if (confirmed) {
        const existingEntities: IMandatorCostUnitRow[] = this.mandatorCostUnitsSubject$
          .getValue()
          .filter(mcu => mcu.mandatorId !== mandatorCostUnit.mandatorId);
        this.mandatorCostUnitsSubject$.next(existingEntities);
      }
    });
  }

  private isInEditMode: () => boolean = (): boolean => {
    this.cdr.markForCheck();
    return this.editMode;
  }

  public submit(): void {
    if (!this.form.valid) {
      return;
    }

    const { value } = this.form;
    const mandatorCostUnits: IMandatorCostUnitRow[] = this.mandatorCostUnitsSubject$
      .getValue();

    const request$: Observable<ICostUnit> = this.creationMode
      ? this.createResource(value)
      : this.updateResource({ ...this.resource, ...value });

    const wasMcuListEmptyOnInit: boolean = this.defaultMandatorCostUnits.length === 0;
    const isMcuListEmptyNow: boolean = mandatorCostUnits.length === 0;

    // if there wasn't any mandatorCostUnit entity before form editing and there isn't
    // after editing and save, no need to send request to the backend,
    // otherwise it will throw an error
    if (wasMcuListEmptyOnInit && isMcuListEmptyNow) {
      return;
    }

    // if user removed all mandatorCostUnit entities we shouldn't update it with
    // updateAll method of mandatorCostUnitRepo, as we do for other deletions if the list
    // isn't empty, because the backend wouldn't know for which cost units all mandators
    // should be removed, for that we use separate deleteByCostUnitId method
    if (isMcuListEmptyNow) {
      request$.pipe(
        switchMap((costUnit: ICostUnit) => this.mandatorCostUnitRepo
          .deleteByCostUnitId(costUnit.id, {}))
      ).subscribe(() => this.defaultMandatorCostUnits = mandatorCostUnits);
      return;
    }

    request$.pipe(
      switchMap((costUnit: ICostUnit) => this.mandatorCostUnitRepo
        .updateAll(mandatorCostUnits.map(mcu => ({ ...mcu, costUnitId: costUnit.id }))))
    ).subscribe(() => this.defaultMandatorCostUnits = mandatorCostUnits);
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
