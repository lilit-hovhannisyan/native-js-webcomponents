import { Injectable } from '@angular/core';
import { CollectionModel } from '@shared/models/collection/collection.model';

import { BehaviorSubject } from 'rxjs';

@Injectable()
export class CollectionsStoreService {
  private readonly _collectionEntities = new BehaviorSubject<CollectionModel[]>([]);
  readonly collectionEntities$ = this._collectionEntities.asObservable();

  private readonly _collectionNextId = new BehaviorSubject<number>(0);
  readonly collectionNextId$ = this._collectionNextId.asObservable();

  get collectionEntities(): CollectionModel[] {
    return this._collectionEntities.getValue();
  }

  set collectionEntities(data: CollectionModel[]) {
    this._collectionEntities.next(data);
  }

  appendCollectionEntities(data: CollectionModel[]): void {
    this.collectionEntities = [...this.collectionEntities, ...data];
  }

  get collectionNextId(): number {
    return this._collectionNextId.getValue();
  }

  set collectionNextId(data: number) {
    this._collectionNextId.next(data);
  }
}
