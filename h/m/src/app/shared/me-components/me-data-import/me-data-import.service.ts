import { Injectable } from '@angular/core';
import { BaseWebService } from '@core-services/base.web-service';
import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { DataImportStatusModel } from '@shared/models/data-import/data-import-status.model';
import { DataImportModel } from '@shared/models/data-import/data-import.model';
import { Observable, Subscription } from 'rxjs';
import { MePopupAccountService } from '../me-popup-account/me-popup-account.service';
import { MeInfoAlertService } from '@shared/me-components/me-info-alert/me-info-alert.service';
import { finalize } from 'rxjs/operators';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Injectable()
export class MeDataImportService {
  private _getI18nStringsSubscription!: Subscription;
  private _importDataStatusSubscription!: Subscription;
  private _importDataSubscription!: Subscription;

  i18nStrings: MeTranslationI = {};

  interval!: ReturnType<typeof setInterval>;

  constructor(
    private baseWebService: BaseWebService,
    private _translate: MeTranslationService,
    private meInfoAlertService: MeInfoAlertService,
    private popupAccountService: MePopupAccountService
  ) {}

  importData(data: DataImportModel): Observable<DataImportModel> {
    return this.baseWebService.postRequest<DataImportModel, DataImportModel>(
      `${MARKETPLACE_BASE_API_URL}/import/data`,
      data,
      DataImportModel
    );
  }

  importDataStatus(): Observable<DataImportStatusModel> {
    return this.baseWebService.getRequest<DataImportStatusModel>(`${MARKETPLACE_BASE_API_URL}/import/status`, DataImportStatusModel);
  }

  private getTranslations(): void {
    this._getI18nStringsSubscription = this._translate.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;
      if (this._getI18nStringsSubscription) {
        this._getI18nStringsSubscription.unsubscribe();
      }
    });
  }

  private checkImportDataStatus(): void {
    this.interval = setInterval(() => {
      this._importDataStatusSubscription = this.importDataStatus().subscribe((response: DataImportStatusModel) => {
        if (response) {
          this.stopInterval();
        }
        if (this._importDataStatusSubscription) {
          this._importDataStatusSubscription.unsubscribe();
        }
      });
    }, 5000);
  }

  private stopInterval(): void {
    clearInterval(this.interval);
    this.meInfoAlertService.closeInfoAlert();
  }

  addSettings(data: DataImportModel): void {
    this.getTranslations();
    this._importDataSubscription = this.importData(data)
      .pipe(
        finalize(() => {
          this.checkImportDataStatus();
        })
      )
      .subscribe(() => {
        this.meInfoAlertService.openInfoAlert({
          mode: 'loading',
          title: this.i18nStrings.importingData,
          content: this.i18nStrings.importingDataDescription,
          buttonText: ''
        });
        this.popupAccountService.closePopup();
        if (this._importDataSubscription) {
          this._importDataSubscription.unsubscribe();
        }
      });
  }
}
