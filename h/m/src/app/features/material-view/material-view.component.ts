import { ActivatedRoute } from '@angular/router';
import { BaseLoggerComponent } from '@features/tracking-system/base-logger.component';
import { Component, OnDestroy, OnInit, ViewChild, Renderer2, ElementRef, Injector, TemplateRef, AfterViewInit } from '@angular/core';
import { KeyValue } from '@angular/common';
import { Router } from '@angular/router';

import { BreadcrumbService } from 'xng-breadcrumb';
import { MatCarouselComponent } from '@ngmodule/material-carousel';
import { finalize } from 'rxjs/operators';
import { fromEvent, Observable, Subject, Subscription } from 'rxjs';

import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { AttributesService } from '@shared/services/attributes.service';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { GlobalService } from '@core-services/global.service';
import { WindowRef } from '@core-providers/window-ref.provider';

import { DocumentStoreService } from '@features/documents/documents-store.service';

import { MaterialsWebService } from '@shared/services/materials.web-service';
import { AttributeGroupModel } from '@shared/models/attribute/attribute.group.model';
import { AttributeModel } from '@shared/models/attribute/attribute.model';
import { DomainModel } from '@shared/models/domain/domain.model';
import { LayoutGroupModel } from '@shared/models/layout/layout-group.model';
import {
  ME_DOMAIN_APPLICATION_MATERIAL,
  MATERIAL_VIEW_GENERAL_INFO,
  MARKETPLACE_BASE_API_URL,
  ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL_VIDEO,
  MATERIAL_ATTRIBUTE_UOM
} from '@shared/constants';
import { MaterialAttributesModel } from '@shared/models/materials/material-attributes.model';
import { MaterialModel } from '@shared/models/materials/material-model';
import { MeAttributeLayoutTypes } from '@shared/me-attribute-layout-types';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeButtonI } from '@shared-components/me-button/me-button.interface';
import { MeDocumentCardI } from '@shared/me-components/me-document-card/me-document-card.interface';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { MePopupStatusChangeI } from '@shared/me-components/me-popup-status-change/me-popup-status-change.interface';
import { MePopupStatusChangeService } from '@shared/me-components/me-popup-status-change/me-popup-status-change.service';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { StateModel } from '@shared/models/state/state.model';
import { getFormatedDate, mapExtensionToIcon } from '@shared/utils';
import { getOid, getVisibilityObject } from '@shared/utils';
import { MeVisibilityI } from '@shared/me-components/me-material-card/me-material-card.interface';
import { MePopupVideoService } from '@shared/me-components/me-popup-video/me-popup-video.service';
import { MeAttributeTypes } from '@shared/me-attribute-types';
import { AuthStoreService } from '@core-services/auth-store.service';
import { MePermissionTypes } from '@shared/me-permission-types';
import { MeTranslationService } from '@core-services/me-translation.service';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';

@Component({
  selector: 'me-material-view',
  templateUrl: './material-view.component.html',
  styleUrls: ['./material-view.component.scss'],
  providers: [MaterialsWebService, MePopupStatusChangeService, AttributesService, DocumentStoreService]
})
export class MaterialViewComponent extends BaseLoggerComponent implements OnInit, AfterViewInit, OnDestroy {
  componentVersion: string = '0.0.1';

  private _takeUntilDestroy$: Subject<void> = new Subject();

  domain: DomainModel = new DomainModel();
  states: StateModel[] = [];
  materialState: StateModel = new StateModel();
  layoutAttributes: AttributeModel<unknown>[] = [];
  layoutGroups: LayoutGroupModel[] = [];
  restOflayoutGroups: LayoutGroupModel[] | undefined = [];
  generalInfolayoutGroup: LayoutGroupModel | undefined = new LayoutGroupModel();
  generalInfoAttributeGroup: AttributeGroupModel = new AttributeGroupModel();
  restOfAttributeGroups: Array<AttributeGroupModel> = new Array<AttributeGroupModel>();
  singleGroupForRestAttributesGroup!: AttributeGroupModel;

  materialOID: string | null = '';
  materialType: string = '';
  unitOfMeasure: string = '';
  material: MaterialModel = new MaterialModel();
  materialAttributes: MaterialAttributesModel = new MaterialAttributesModel();
  materialAttributesExtended: MaterialAttributesModel = new MaterialAttributesModel();
  isMaterialLoading: boolean = true;
  viewMaterialContextMenuItems!: KeyValue<string, string>[];

  documentsImages: string[] = [];
  documentCardItems: MeDocumentCardI[] = [];
  getCardOid = getOid;

  _getI18nStringsSubscription!: Subscription;
  _getDefinitionsDataLoadedSubscription!: Subscription;
  _getMaterialByOIDSubscription!: Subscription;
  _getDocumentsForThisMaterialSubscription!: Subscription;
  _getWindowResizingSubscription!: Subscription;
  _downloadImageSubscription!: Subscription;
  _getMaterialStates!: Subscription;
  _changeMaterialStateSubscription!: Subscription;
  _sweetAletSubscription!: Subscription;
  _deleteDocumentSubscription!: Subscription;
  _documentStoreServiceSubscription!: Subscription;

  @ViewChild('editButton', { read: TemplateRef }) editButton!: TemplateRef<Component>;
  @ViewChild('carousel') private carouselContainer!: MatCarouselComponent;
  @ViewChild('carouselDiv') carouselDiv!: ElementRef;
  selectedCarouselImageIndex: number = 0;
  resizeCarouselObservable!: Observable<Event>;
  presentationHeight: number = 100;

  downloadButton: MeButtonI = { type: 'transparent', color: 'warning' };

  visibility?: MeVisibilityI;

  i18nStrings: MeTranslationI = {};

  isVisibilityEdit: boolean = false;

  constructor(
    injector: Injector,
    private attributesService: AttributesService,
    private basicAlertService: MeBasicAlertService,
    private breadcrumbService: BreadcrumbService,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private definitionsStoreService: DefinitionsStoreService,
    private globalService: GlobalService,
    private materialsWebService: MaterialsWebService,
    private popupStatusChangeService: MePopupStatusChangeService,
    private renderer: Renderer2,
    private route: ActivatedRoute,
    private router: Router,
    private sweetAlertService: MeSweetAlertService,
    private meTranslationService: MeTranslationService,
    private windowRef: WindowRef,
    private documentStoreService: DocumentStoreService,
    private videoPopupService: MePopupVideoService,
    private authStoreService: AuthStoreService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    // subscribe to translations
    this._getI18nStringsSubscription = this.meTranslationService.getTranslations().subscribe((translations: MeTranslationI) => {
      this.i18nStrings = translations;
    });

    this.materialOID = this.route.snapshot.paramMap.get('id');

    this._getDefinitionsDataLoadedSubscription = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.domain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_MATERIAL);
        this.states = this.domain.states;
        if (this.materialOID) {
          this.prepareDocumentCard();
          this.getMaterialByOID(this.materialOID);
          this.getDocumentsForThisMaterial(this.materialOID);
        }
      }
    });

    this.resizeCarousel();

    this.isVisibilityEdit = this.authStoreService.isAllowed(MePermissionTypes.STATE_MATERIAL_CHANGE);

    this.viewMaterialContextMenuItems = [
      // { key: 'addToCollection', value: this.i18nStrings.addToCollections },
      { key: 'duplicate', value: this.i18nStrings.duplicate }
    ];

    if (this.isVisibilityEdit) {
      this.viewMaterialContextMenuItems.unshift({ key: 'visibility', value: this.i18nStrings.visibility });
    }
  }

  ngAfterViewInit(): void {
    this.breadcrumbSupportService.breadcrumbActions = [
      {
        button: {
          id: '1',
          data: {
            color: 'secondary',
            type: 'regular'
          },
          text: this.i18nStrings.edit,
          content: this.editButton
        },
        callBack: () => {
          this.router.navigate([`materials/edit/${this.materialOID}`]);
        }
      }
    ];

    if (this.carouselDiv) {
      setTimeout(() => (this.presentationHeight = this.carouselDiv.nativeElement.offsetHeight));
    } else {
      this.presentationHeight = 500;
    }
  }

  prepareDocumentCard(): void {
    this._documentStoreServiceSubscription = this.documentStoreService.documentEntities$.subscribe((data) => {
      this.documentCardItems = data.map((document: MeDocumentModel) => {
        return {
          contextMenu: {
            contextMenuItems: [
              {
                key: 'edit',
                value: this.i18nStrings.edit
              },
              {
                key: 'download',
                value: this.i18nStrings.download
              },
              {
                key: 'remove',
                value: this.i18nStrings.remove
              },
              {
                key: 'view',
                value: this.i18nStrings.view
              }
            ]
          },
          creationDate: getFormatedDate(document.createdOn),
          documentName: document.attributes.title,
          oid: document.oid,
          selected: false,
          selectionEnabled: false,
          documentThumbnail: document.attributes.thumbnail
            ? `${MARKETPLACE_BASE_API_URL}/files/download/${document.attributes.thumbnail}`
            : undefined,
          primaryContent: document.attributes.primaryContent
            ? `${MARKETPLACE_BASE_API_URL}/files/download/${document.attributes.primaryContent}`
            : undefined,
          documentExtensionIcon: mapExtensionToIcon(document.attributesExtended.extension)
        };
      });
    });
  }

  getMaterialByOID(materialOID: string): void {
    this.globalService.activateLoader();
    this._getMaterialByOIDSubscription = this.materialsWebService
      .getMeMaterialById(materialOID)
      .pipe(
        finalize(() => {
          this.isMaterialLoading = false;
          this.globalService.deactivateLoader();
        })
      )
      .subscribe((response: MaterialModel) => {
        if (response) {
          this.material = response;
          this.materialAttributes = response.attributes;
          this.materialAttributesExtended = response.attributesExtended;

          for (const state of this.states) {
            if (state.internalName === this.material.state) {
              this.materialState = state;
            }
          }

          // Set breadcrumb path - material name instead of material id
          this.breadcrumbService.set('@materialName', this.material.attributes.name);
          this.domain = this.definitionsStoreService.getDomain(this.material.domainName);
          if (this.domain) {
            this.materialType = this.domain.displayName;
            if (this.materialAttributes.hasOwnProperty(MATERIAL_ATTRIBUTE_UOM)) {
              this.unitOfMeasure = this.attributesService.getEnumDisplayNames(
                {
                  dataTypeClass: MeAttributeTypes.CHOICE,
                  displayName: '',
                  internalName: MATERIAL_ATTRIBUTE_UOM,
                  value: this.materialAttributes.unitOfMeasure,
                  convertedValue: ''
                },
                this.domain
              );
            }
            this.layoutAttributes = this.attributesService.transformAttributesArray(this.domain.attributes);
            this.layoutGroups = this.attributesService.getLayoutGroups(this.domain.layouts, MeAttributeLayoutTypes.VIEW);

            this.generalInfolayoutGroup = this.attributesService.getLayoutGroupByInternalName(
              this.layoutGroups,
              MATERIAL_VIEW_GENERAL_INFO
            );

            this.generalInfoAttributeGroup = this.attributesService.getAttributesLayoutByGroup(
              this.generalInfolayoutGroup,
              this.layoutAttributes,
              this.materialAttributes
            );

            this.generalInfoAttributeGroup = this.attributesService.convertAttributesForDisplaying(
              [this.generalInfoAttributeGroup],
              this.domain
            )[0];

            this.restOflayoutGroups = this.attributesService.getLayoutGroupsExceptGeneral(this.layoutGroups, MATERIAL_VIEW_GENERAL_INFO);
            if (this.restOflayoutGroups) {
              for (const group of this.restOflayoutGroups) {
                this.restOfAttributeGroups.push(
                  this.attributesService.getAttributesLayoutByGroup(group, this.layoutAttributes, this.materialAttributes)
                );
              }
            }
            this.restOfAttributeGroups = this.attributesService.convertAttributesForDisplaying(this.restOfAttributeGroups, this.domain);

            const emptyGroup = new AttributeGroupModel();
            this.restOfAttributeGroups.forEach((group) => emptyGroup.attributes.push(...group.attributes));
            this.singleGroupForRestAttributesGroup = emptyGroup;

            this.visibility = getVisibilityObject(
              this.material.state,
              this.domain.states.find((ms) => ms.internalName === this.material.state)?.stateType
            );
          }
        }
      });
  }

  getDocumentsForThisMaterial(materialOID: string): void {
    this._getDocumentsForThisMaterialSubscription = this.materialsWebService
      .getMeMaterialDocuments(materialOID)
      .subscribe((response: ArrayResponseI<MeDocumentModel>) => {
        if (response) {
          this.documentStoreService.documentEntities = response.entities;

          this.documentsImages = [];
          for (const document of response.entities) {
            if (document.attributes.thumbnail) {
              const documentImage = `${MARKETPLACE_BASE_API_URL}/files/download/${document.attributes.thumbnail}`;
              this.documentsImages.push(documentImage);
            }
          }

          // In case there are no thumbnails or documents, add default material thumbnail
          if (!this.documentsImages?.length) {
            if (this.materialAttributes && this.materialAttributes.thumbnail) {
              const materialThumbnailImage = `${MARKETPLACE_BASE_API_URL}/files/download/${this.materialAttributes.thumbnail}`;
              this.documentsImages.push(materialThumbnailImage);
            }
          }
        }
      });
  }

  handleDocumentCardEvent(eventName: string, payload?: string, document?: MeDocumentCardI): void {
    if (['infoCardContextMenuClick', 'contextMenuItemClick'].includes(eventName)) {
      switch (payload) {
        case 'edit':
          this.router.navigate(['documents', 'edit', document?.oid]);
          break;
        case 'download':
          if (document && document.primaryContent) {
            const FORCE_DOWNLOAD: string = '?forceDownload=true';
            const FILE_URL: string = document.primaryContent.concat(FORCE_DOWNLOAD);
            this.windowRef.openInNewWindow(FILE_URL);
          } else {
            this.basicAlertService.openBasicAlert({
              mode: 'error',
              content: this.i18nStrings.noFile,
              title: this.i18nStrings.emptyDocument
            });
          }
          break;
        case 'remove':
          if (document?.oid && this.materialOID) {
            this.deleteDocument(this.materialOID, document.oid);
          }
          break;
        case 'view':
          const fileToView = this.documentStoreService.documentEntities.find((d) => d.oid === document?.oid);
          if (fileToView && fileToView.attributes.primaryContent) {
            const FILE_URL: string = `${MARKETPLACE_BASE_API_URL}/files/download/${fileToView.attributes.primaryContent}`;
            if (fileToView.domainName === ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL_VIDEO) {
              this.videoPopupService.openVideoPopupAlert({ videoPath: FILE_URL });
            } else {
              this.windowRef.openInNewWindow(FILE_URL);
            }
          } else {
            this.basicAlertService.openBasicAlert({
              mode: 'error',
              content: this.i18nStrings.noFile,
              title: this.i18nStrings.emptyDocument
            });
          }
          break;
        default:
          break;
      }
    }
  }

  private deleteDocument(materialOID: string, documentOID: string): void {
    this._sweetAletSubscription = this.sweetAlertService.getDataBackFromSweetAlert().subscribe((data: MeSweetAlertI) => {
      if (data.confirmed) {
        const documentOIDs = [documentOID];
        this._deleteDocumentSubscription = this.materialsWebService.deleteMeMaterialDocuments(materialOID, documentOIDs).subscribe(() => {
          this.basicAlertService.openBasicAlert({
            mode: 'success',
            content: this.i18nStrings.removeDocumentsSuccess,
            title: this.i18nStrings.success
          });

          this.getDocumentsForThisMaterial(materialOID);
        });
      }
      this._sweetAletSubscription.unsubscribe();
    });

    this.sweetAlertService.openMeSweetAlert({
      icon: 'alert-triangle',
      mode: 'warning',
      message: this.i18nStrings.areYouSureYouWantToRemoveDocument,
      title: this.i18nStrings.removeDocuments,
      type: {
        name: MeSweetAlertTypeEnum.submit,
        buttons: {
          submit: this.i18nStrings.remove,
          cancel: this.i18nStrings.cancel
        }
      }
    });
  }

  viewMaterialContextMenuEvent(event: string): void {
    switch (event) {
      case 'addToCollection':
        // TODO
        break;
      case 'visibility':
        // Creating of popup data
        const popup: MePopupStatusChangeI = {
          title: this.i18nStrings.setMaterialVisibility,
          cancelButtonPlaceholder: this.i18nStrings.cancel,
          changeButtonPlaceholder: this.i18nStrings.saveChanges
        };
        // open popup
        const popUpSubs: Subject<{ eventName: string; payload?: string }> = this.popupStatusChangeService.openDialog(popup);

        // get States and add data to popup
        this._getMaterialStates = this.materialsWebService.getMaterialStates(this.material.oid).subscribe((states) => {
          const newPopup = {
            ...popup,
            currentKeyValue: this.material.state,
            radioButtons: states.entities.map((state) => {
              const displName = this.domain.states.find((s) => s.internalName === state.internalName);
              return {
                key: state.internalName,
                value: displName ? `${displName.displayName} - ${displName.description}` : state.internalName,
                disabled: !state.enabled
              };
            })
          };
          this.popupStatusChangeService.refreshDialogData(newPopup);
          popUpSubs.subscribe((ev) => {
            if (ev.eventName === 'change' && ev.payload && ev.payload !== this.material.state) {
              this.changeMaterialState(ev.payload);
            }
          });
        });
        break;
      case 'delete':
        // TODO
        break;
      case 'duplicate':
        this.router.navigate(['materials', 'duplicate', this.material.oid]);
        break;
      case 'report':
        // TODO
        break;
      default:
        throw new Error('No default option.');
    }
  }

  changeMaterialState(newStates: string): void {
    this._changeMaterialStateSubscription = this.materialsWebService.changeMaterialState(this.material.oid, newStates).subscribe(
      (material: MaterialModel) => {
        this.material = material;
        for (const state of this.states) {
          if (state.internalName === this.material.state) {
            this.materialState = state;
          }
        }
        this.basicAlertService.openBasicAlert({
          mode: 'success',
          title: this.i18nStrings.statusChanged,
          content: this.i18nStrings.materialStatusChanged
        });
      },
      (error: ErrorResponseI) => {
        this.basicAlertService.openBasicAlert({
          mode: 'error',
          title: error.error.title,
          content: error.error.details
        });
      }
    );
  }

  resizeCarousel(): void {
    this.resizeCarouselObservable = fromEvent(window, 'resize');
    this._getWindowResizingSubscription = this.resizeCarouselObservable.subscribe(() => {
      const newHeight = this.carouselDiv.nativeElement.offsetHeight;
      if (this.presentationHeight !== newHeight) {
        this.presentationHeight = newHeight;
      }
    });
  }

  download2dImage(): void {
    const FORCE_DOWNLOAD: string = '?forceDownload=true';
    const IMAGE_URL: string = this.documentsImages[this.selectedCarouselImageIndex].concat(FORCE_DOWNLOAD);
    this.windowRef.openInNewWindow(IMAGE_URL);
  }

  changeImage(action: string): void {
    if (action === 'next') {
      this.carouselContainer.next();
    } else if (action === 'previous') {
      this.carouselContainer.previous();
    }
  }

  carouselNavigateToIndex(index: number): void {
    this.carouselContainer.slideTo(index);
  }

  scrollToImageThumbnail(index: number): void {
    try {
      const imageThumbnail = this.renderer.selectRootElement('.index-' + index);
      imageThumbnail.scrollIntoView();
    } catch {}
  }

  updatePage(pageNumber: number): void {
    this.selectedCarouselImageIndex = pageNumber;
    this.scrollToImageThumbnail(pageNumber);
  }

  ngOnDestroy(): object {
    if (this._getI18nStringsSubscription) {
      this._getI18nStringsSubscription.unsubscribe();
    }
    this._takeUntilDestroy$.next();
    this._takeUntilDestroy$.complete();

    if (this._getDefinitionsDataLoadedSubscription) {
      this._getDefinitionsDataLoadedSubscription.unsubscribe();
    }
    if (this._getMaterialByOIDSubscription) {
      this._getMaterialByOIDSubscription.unsubscribe();
    }
    if (this._getDocumentsForThisMaterialSubscription) {
      this._getDocumentsForThisMaterialSubscription.unsubscribe();
    }
    if (this._getWindowResizingSubscription) {
      this._getWindowResizingSubscription.unsubscribe();
    }
    if (this._downloadImageSubscription) {
      this._downloadImageSubscription.unsubscribe();
    }
    if (this._getMaterialStates) {
      this._getMaterialStates.unsubscribe();
    }
    if (this._changeMaterialStateSubscription) {
      this._changeMaterialStateSubscription.unsubscribe();
    }
    if (this._sweetAletSubscription) {
      this._sweetAletSubscription.unsubscribe();
    }
    if (this._deleteDocumentSubscription) {
      this._deleteDocumentSubscription.unsubscribe();
    }
    if (this._documentStoreServiceSubscription) {
      this._documentStoreServiceSubscription.unsubscribe();
    }

    return super.ngOnDestroy();
  }
}
