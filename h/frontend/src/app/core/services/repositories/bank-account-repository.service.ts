import { BankAccountKey } from './../../models/resources/IBankAccount';
import { Injectable } from '@angular/core';
import {
  IBankAccount,
  IBankAccountDTO,
  IBankAccountCreate
} from '../../models/resources/IBankAccount';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';

export interface BankAccountParams extends RepoParams<IBankAccount> {
}

@Injectable({
  providedIn: 'root'
})
export class BankAccountRepositoryService extends AbstractRepository<
  IBankAccount,
  IBankAccountDTO,
  IBankAccountCreate,
  BankAccountParams,
  BankAccountKey
> {
  protected resourceType = 'BankAccount';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(params: BankAccountParams): any {
    return `/api/bank-accounts`;
  }

  protected postProcess(item: IBankAccountDTO): IBankAccount { return item; }
}
