import {BaseComponent} from '../../core/base-component/base.component.js';
import {
  externalStyleBasePath,
  canElementBeRendered,
} from '../../helpers/random.helpers.js';

export class TextComponent extends BaseComponent {
  constructor() {
    super (externalStyleBasePath + 'text-component/text.component.css');
  }

  render = () => {
    const render = this.getAttribute('render');
    this.renderedText = this.getAttribute('text') || this.innerHTML;

    if (!render && !this.renderedText) {
      throw error();
    }
    const textElConfig = {
      tagName: this.getAttribute('selector') || 'p',
      text: this.renderedText,
      render: canElementBeRendered(this.renderedText)
    };

    const textEl = this.createElementWithAttributes(textElConfig);
    this.removeStaticAtributesFromElNotToRender(this, ['text', 'render']);
    this.shadow.appendChild(textEl);
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }
}