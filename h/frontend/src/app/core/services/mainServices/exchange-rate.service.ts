import { Injectable } from '@angular/core';
import { ExchangeRateRepositoryService, IExchangeRateRepoParams } from '../repositories/exchange-rate-repository.service';
import { BehaviorSubject, forkJoin, Observable } from 'rxjs';
import { IExchangeRateFull } from '../../models/resources/IExchangeRate';
import { tap, map } from 'rxjs/operators';
import { CurrencyRepositoryService } from '../repositories/currency-repository.service';

@Injectable({
  providedIn: 'root'
})
export class ExchangeRateService {
  public exchangeRates: BehaviorSubject<IExchangeRateFull[]> = new BehaviorSubject([]);
  constructor(
    private exchangeRateRepo: ExchangeRateRepositoryService,
    private currencyService: CurrencyRepositoryService,
  ) { }

  public fetchAll(params: IExchangeRateRepoParams): Observable<IExchangeRateFull[]> {
    return forkJoin([
      this.exchangeRateRepo.fetchAll(params),
      this.currencyService.fetchAll({}),
    ])
      .pipe(
        map(([_exchangeRates, currencies]) => {
          const exchangeRates = currencies.map(currency => {
            const foundExchangeRate = _exchangeRates.find(exchangeRate => currency.id === exchangeRate.currencyId);
            return {
              ...foundExchangeRate,
              ...currency,
              isFavorite: !!currency.isFavorite
            } as IExchangeRateFull;
          });

          const euroCurrency = currencies.find(currency => currency.isoCode === 'EUR');
          /* We do this because we get all rates for EURO currency,
             but we don't get rate of EURO itself which must be 1 and when we try to find
             rate for EUR we fail and in some cases we need to just find EUR rate which is 1
          */
          if (!euroCurrency) {
            const euroExchangeRate: IExchangeRateFull = {
              rate: 1,
              rateUSD: 0,
              currencyId: 0,
              date: null,
              source: 'Web Service',
              importDate: null,
            } as IExchangeRateFull;

            exchangeRates.push(euroExchangeRate);
          }
          return exchangeRates;
        }),
        tap(res => this.exchangeRates.next(res))
      );
  }

  public getRate(currencyId: number): IExchangeRateFull {
    return this.exchangeRates.getValue()
      .find(rate => rate.currencyId === currencyId);
  }
}
