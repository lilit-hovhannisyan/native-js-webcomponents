import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IconsModule } from '@shared-modules/icons.module';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { MeButtonModule } from '../me-button/me-button.module';

import { MeSweetAlertComponent } from './me-sweet-alert.component';
import { MeSweetAlertService } from './me-sweet-alert.service';

@NgModule({
  imports: [CommonModule, IconsModule, FormsModule, MatFormFieldModule, MatInputModule, MatDialogModule, MeButtonModule],
  declarations: [MeSweetAlertComponent],
  exports: [MeSweetAlertComponent],
  providers: [MeSweetAlertService]
})
export class MeSweetAlertModule {}
