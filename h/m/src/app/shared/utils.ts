import { formatDate } from '@angular/common';
import { MeVisibilityI } from './me-components/me-material-card/me-material-card.interface';

export function bytesToSize(bytes: number): string {
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes === 0) {
    return 'n/a';
  }

  const i = Math.floor(Math.log(bytes) / Math.log(1024));
  if (i === 0) {
    return bytes + ' ' + sizes[i];
  }

  return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
}

export function constructUrl(endpoint: string, from?: number, to?: number): string {
  let queryParamsStr: string = '';
  if (from !== undefined && to !== undefined) {
    queryParamsStr = `?skip=${from}&top=${to}`;
  }
  return endpoint + queryParamsStr;
}

export function getFormatedDate(date: string): string {
  return formatDate(date, 'dd/MM/yyyy', 'en-US');
}

export function getOid(_index: number, card: { oid: string }): string {
  return card.oid;
}

export function getVisibilityObject(state: 'public' | 'draft' | 'exclusive' | 'internal', stateType?: string): MeVisibilityI | undefined {
  switch (stateType) {
    case 'OPEN':
      return { label: state, backgroundColorHex: 'var(--color-warning, #ffb822)', fontColorHex: 'var(--color-black, #000000)' };
    case 'IN_PROGRESS':
      return { label: state, backgroundColorHex: 'var(--color-primary, #5849e0)', fontColorHex: 'var(--color-white, #ffffff)' };
    case 'DONE':
      return { label: state, backgroundColorHex: 'var(--color-success, #43ad48)', fontColorHex: 'var(--color-white, #ffffff)' };
    default:
      return undefined;
  }
}

export function getVisibilityColorForLabel(stateType: string): 'warning' | 'primary' | 'success' | 'danger' | 'info' {
  switch (stateType) {
    case 'OPEN':
      return 'warning';
    case 'IN_PROGRESS':
      return 'primary';
    case 'DONE':
      return 'success';
    default:
      return 'primary';
  }
}

export function getUserInitials(name: string): string {
  return name
    .split(' ')
    .map((n) => n[0])
    .join('')
    .toLocaleUpperCase();
}

export function mapExtensionToIcon(extension?: string): string {
  switch (extension) {
    case '3dm':
      return 'file-threedm';
    case 'docx':
    case 'jpg':
    case 'pdf':
    case 'png':
    case 'xlsx':
      return `file-${extension}`;
    default:
      return 'file-other';
  }
}
