import { Type } from 'class-transformer';

import { ConstraintModel } from '@shared-models/attribute/attribute.constraint.model';
import { PropertiesModel } from '@shared-models/attribute/attribute.properties.model';
import { EnumValueModel } from '@shared-models/enum/enum-value.model';

export type AttributeModelTypes =
  | DMBooleanAttributeModel
  | DMChoiceAttributeModel
  | DMCompositeAttributeModel
  | DMDateTimeAttributeModel
  | DMDecimalAttributeModel
  | DMFloatAttributeModel
  | DMIntegerAttributeModel
  | DMLongAttributeModel
  | DMMOAListAttributeModel
  | DMObjectRefAttributeModel
  | DMObjectRefListAttributeModel
  | DMStringAttributeModel
  | DMTextAttributeModel
  | DMUrlAttributeModel
  | DefaultAttributeModel;
export class AttributeModel<T> {
  @Type(() => ConstraintModel)
  constraints: ConstraintModel[] = [];
  dataTypeClass: string = '';
  displayName: string = '';
  internalName: string = '';
  @Type(() => PropertiesModel)
  properties: PropertiesModel = new PropertiesModel();
  required: boolean = false;
  optRequired: boolean = false;
  disabled: boolean = false;
  unique: boolean = false;
  value!: T;
}

export class DMBooleanAttributeModel extends AttributeModel<boolean> {}

export class DMChoiceAttributeModel extends AttributeModel<string> {
  @Type(() => EnumValueModel)
  values: EnumValueModel[] = [];
}

export class DMCompositeAttributeModel extends AttributeModel<{ components: { name: string; percentage: number }[] }> {
  @Type(() => EnumValueModel)
  values: EnumValueModel[] = [];
}

export class DMDateTimeAttributeModel extends AttributeModel<string> {}

export class DMDecimalAttributeModel extends AttributeModel<number> {
  minValue: number = 0;
  maxValue: number = 1000;
}

export class DMFloatAttributeModel extends AttributeModel<number> {
  minValue: number = 0;
  maxValue: number = 1000;
}

export class DMIntegerAttributeModel extends AttributeModel<number> {
  minValue: number = 0;
  maxValue: number = 1000;
}

export class DMLongAttributeModel extends AttributeModel<number> {
  minValue: number = 0;
  maxValue: number = 1000;
}

export class DMMOAListAttributeModel extends AttributeModel<string[]> {
  @Type(() => EnumValueModel)
  values: EnumValueModel[] = [];
}

export class DMObjectRefAttributeModel extends AttributeModel<string> {
  refAttribute: string = '';
  refDomain: string = '';
}

export class DMObjectRefListAttributeModel extends AttributeModel<string[]> {}

export class DMStringAttributeModel extends AttributeModel<string> {
  minLength: number = 0;
  maxLength: number = 255;
}

export class DMTextAttributeModel extends AttributeModel<string> {
  minLength: number = 0;
  maxLength: number = 4000;
}

export class DMUrlAttributeModel extends AttributeModel<string> {
  minLength: number = 0;
  maxLength: number = 255;
}

export class DefaultAttributeModel extends AttributeModel<string> {}
