import { Component, OnInit, ChangeDetectionStrategy, Input, ViewChild, ElementRef, HostListener, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { CalendarOptions, FullCalendarComponent } from '@fullcalendar/angular';
import { Calendar } from '@fullcalendar/core';
import { EventInput } from '@fullcalendar/angular';
import { DateInput } from '@fullcalendar/common';

import dayGridPlugin from '@fullcalendar/daygrid';
import timeGrigPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import interactionPlugin, { DateClickArg } from '@fullcalendar/interaction';

import enLocale from '@fullcalendar/core/locales/en-gb';
import deLocale from '@fullcalendar/core/locales/de';
import { Language } from '../../../core/models/language';
import { SettingsService } from '../../../core/services/settings.service';
import { wording } from '../../../core/constants/wording/wording';
import {
  months, ContextTypes, CalendarEventsTypes, buttonContextMenuConfig, calendarEventZone, CalendarEnvironment,
  formatWithHours, contextMenuActionName
} from './calendar.helper';
import { CalendarKey, ICalendar } from '../../../core/models/resources/ICalendar';
import { CalendarsRepositoryService } from '../../../core/services/repositories/calendars-repository.service';
import { combineLatest, forkJoin, of } from 'rxjs';
import { CalendarEventsRepositoryService } from '../../../core/services/repositories/calendar-events-repository.service';
import { ACL_ACTIONS, AuthenticationService } from '../../../authentication/services/authentication.service';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { DefaultModalOptions } from '../../../core/constants/modalOptions';
import { CalendarModalComponent } from './calendar-modal/calendar-modal.component';
import { switchMap } from 'rxjs/operators';
import { ICalendarEvent } from '../../../core/models/resources/ICalendarEvent';
import { min, max } from 'lodash';
import { IHTMLInputEvent } from '../../../core/models/IHTMLInputEvent';
import { CalendarService } from './calendar.service';

enum Direction {
  up = 'up',
  down = 'down'
}

enum Unit {
  month = 'month',
  year = 'year'
}

@Component({
  selector: 'nv-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarComponent implements OnInit, AfterViewInit {
  public language: Language = this.settingsService.language;
  public wording = wording;

  // calendar config
  @Input() public isFourWeekCalendar: boolean;
  @Input() public contextMenuBtnConfig: any[] = buttonContextMenuConfig;
  @Input() public calendarEnvironment: CalendarEnvironment = CalendarEnvironment.chartering;
  @Output() public calendarRangeChange: EventEmitter<
    { start: moment.Moment, end: moment.Moment }
  > = new EventEmitter();

  public calendarEvents: EventInput[];
  public calendarOptions: CalendarOptions;
  public calendarApi: Calendar;

  // context menu
  public isContextMenuVisibile: boolean;
  public context: ContextTypes;

  // search
  public searchValue: string = null;
  public searchValueError: boolean;
  private allEventsToSearchFrom: EventInput[] = [];

  // filters
  public months = months;
  public currentMonthIndex: number;
  public currentMonth: string;
  public currentDate: Date;
  public allCalendars: ICalendar[];
  public listOfSelectedCalendars: ICalendar[];
  public listOfSelectedCalendarIds: CalendarKey[] = [];
  public dateUntilFor4Weeks: Date;

  // permissions
  public hasAccessToCreateCalendar = this.authService.can(ACL_ACTIONS.CREATE, 'system.misc.calendars');

  // CRUD's related (event, task, todos)
  private selectedEvent: EventInput;
  private selectedDate: string;
  private modalRef: NzModalRef;

  // mouse wheel controll
  private isWheelStarted: boolean;
  private wheelDirection: Direction;
  private wheelCounterStart = 0;
  private wheelCounterEnd: number;

  @ViewChild('eventsBlock', { static: true }) public eventsBlock: ElementRef;
  @ViewChild('calendar', { static: false }) public calendarComponent: FullCalendarComponent;
  @ViewChild('contextMenu', { static: false }) public contextMenu: ElementRef;

  constructor(
    private settingsService: SettingsService,
    private calendarsRepo: CalendarsRepositoryService,
    private calendarEventsRepo: CalendarEventsRepositoryService,
    private authService: AuthenticationService,
    private modalService: NzModalService,
    private calendarService: CalendarService,
  ) {
    const name = Calendar.name; // this is done library to instanisiate  first, otherwise it throws an exception
  }

  public ngOnInit(): void {
    this.initCalendar();
    this.initData();
  }

  public ngAfterViewInit(): void {
    this.calendarApi = this.calendarComponent.getApi();
  }

  private initData(): void {
    forkJoin([
      this.calendarsRepo.fetchAll({}),
      this.calendarEventsRepo.fetchAll({}),
      this.calendarService.getCalendarData(this.calendarEnvironment)
    ]).pipe(
      switchMap(() => combineLatest([
        this.calendarsRepo.getStream(),
        this.calendarEventsRepo.getStream(),
      ]))
    ).subscribe(([calendars, calendarEvents]: [
      ICalendar[],
      ICalendarEvent[]
      ]) => {
      this.allCalendars = calendars;

      this.calendarService.initCalendarService(
        this.calendarEnvironment,
        this.calendarApi,
        this.language,
        this.allCalendars,
        this.fetchEvents
      );

      this.allEventsToSearchFrom = [...this.calendarApi.getEvents()];
    });
  }

  private initCalendar(): void {
    let calendarConfig: CalendarOptions = {
      initialView: 'dayGridMonth',
      headerToolbar: {
        left: 'dayGridMonth,timeGridWeek,timeGridDay listCustomButton',
        center: 'title',
        right: 'today prevYear,prev,next,nextYear'
      },
      height: 700
    };

    if (this.isFourWeekCalendar) {
      calendarConfig = {
        initialView: 'dayGrid',
        headerToolbar: {
          left: '',
          center: 'title',
          right: 'today prevYear,prev,next,nextYear'
        },
        duration: { weeks: 4 },
        firstDay: new Date().getDay(), // which weekDay index tart week
        customButtons: {
          today: {
            text: wording.chartering.today[this.language],
            click: () => {
              this.calendarComponent.getApi().today();
              const startDate: Date = this.calendarComponent.getApi().getDate();
              const endDate: Date = moment(startDate).add(27, 'days').toDate();
              this.jumpToDayAndSwitchWeekDayStart(startDate, endDate);

              this.currentDate = startDate;
              this.dateUntilFor4Weeks = this.currentDate;
            }
          },
          prevYear: { click: () => { this.toggleShownPeriodFor4Weeks(Unit.year, Direction.down); } },
          nextYear: { click: () => { this.toggleShownPeriodFor4Weeks(Unit.year, Direction.up); } },
          prev: { click: () => { this.toggleShownPeriodFor4Weeks(Unit.month, Direction.down); } },
          next: { click: () => { this.toggleShownPeriodFor4Weeks(Unit.month, Direction.up); } },
        },
        height: 400,
      };
    }

    this.calendarOptions = {
      editable: true, // to drag event
      // selectable: true, // highlight if date selected
      ...calendarConfig,
      views: {
        listCustomButton: {
          type: 'list',
          duration: { days: 7 },
          buttonText: wording.chartering.calendarListViewBtnText[this.language]
        }
      },
      events: this.calendarEvents,
      plugins: [dayGridPlugin, timeGrigPlugin, listPlugin, timeGrigPlugin, interactionPlugin],
      weekends: true,
      weekNumberCalculation: 'ISO', // by default starts from Sun, 'local' -> Sun | 'ISO' ->  Mon | custom function
      locales: [ enLocale, deLocale ],
      locale: this.language + '', // bind api locale to project language
      navLinks: true,
      navLinkDayClick: this.onShowContextMenuAdd,
      dateClick: this.onDateClick,
      eventClick: this.onEventClick,
      eventDrop: this.onEventDrop,
    };

    // init filters
    this.initMonthFilter();
    this.currentDate = new Date(); // init year
  }

  private initMonthFilter(): void {
    const today = new Date();
    this.currentMonthIndex = today.getMonth();
    this.currentMonth = months[this.language][this.currentMonthIndex];
  }

  // -- CALENDAR EVENTS that emit data
  // drag drop events
  private onEventClick = (event: EventInput): void => {
    this.selectedEvent = event;
    const extendedProps = event.event.extendedProps; // TODO: type

    if (extendedProps._type === CalendarEventsTypes.task) {
      this.showContextMenu(event.jsEvent, ContextTypes.editTask);
    }
    if (extendedProps._type === CalendarEventsTypes.evn) {
      this.showContextMenu(event.jsEvent, ContextTypes.editEvn);
    }
    if (extendedProps._type === CalendarEventsTypes.todo) {
      this.showContextMenu(event.jsEvent, ContextTypes.editTodo);
    }
  }

  private onEventDrop = (event: EventInput): void => {
    this.selectedDate =  formatWithHours(event.event.start);
    const extendedProps = event.event.extendedProps; // TODO: type

    this.calendarService.updateOnEventDrop(extendedProps, this.selectedDate);
  }

  // show context menu on dayClick and handle context menu outside click
  private onDateClick = (dateCellInput: DateClickArg): void => {
    this.onShowContextMenuAdd(dateCellInput.date, dateCellInput.jsEvent);
  }

  private onShowContextMenuAdd = (date: Date, event: MouseEvent): void => {
    this.selectedDate = formatWithHours(date);
    this.showContextMenu(event, ContextTypes.add);
  }

  private showContextMenu(event: MouseEvent, context: ContextTypes): void {
    const hasPermission: boolean = this.hasPermissionAccess(ACL_ACTIONS.CREATE);
    if (!hasPermission) { return; }

    const windowWidth: number = window.innerWidth;
    const windowHeight: number = window.innerHeight;
    const el: HTMLElement = this.contextMenu.nativeElement;
    const contextMenuWidth: number = el.clientWidth;
    const contextMenuHeight: number = el.clientHeight;

    /**
     * this is absolute positioned box, which is visible on window click
     * but when the mouse is in the right corner I have to showcase it from rigth side
     * so it is not out ow window range
     * and also I add 5px deviation, not to cover calendar date number
     * the same from bottom
     */
    const deviationForPadding = 5;

    const topPos: number = windowHeight - event.pageY > contextMenuHeight
      ? event.pageY + deviationForPadding
      : event.pageY - contextMenuHeight;

    const leftPos: number = windowWidth - event.pageX >= windowWidth / 2
      ? event.pageX + deviationForPadding
      : event.pageX - contextMenuWidth;

    el.style.top = topPos + 'px';
    el.style.left = leftPos + 'px';

    this.toggleContextMenu(true, context);
  }

  private toggleContextMenu(state: boolean, context: ContextTypes): void {
    this.isContextMenuVisibile = state;
    this.context = context;
  }

  // contextMenu permission && actions: aka CRUD handling
  public hasPermissionAccess(operation): boolean {
    return this.authService.can(operation, calendarEventZone);
  }

  public handleContextMenuAction(
    operation: ACL_ACTIONS,
    type: CalendarEventsTypes,
    action: contextMenuActionName
  ): void {
    operation === ACL_ACTIONS.CREATE
      ? this.addNewCalendarEvent(action)
      : operation === ACL_ACTIONS.UPDATE
        ? this.editCalendarEvent(action)
        : operation === ACL_ACTIONS.DELETE
          ? this.deleteCalendarEvent(type)
          : this.showDetailsCalendarEvent(action);
  }

  private addNewCalendarEvent(action: contextMenuActionName): void {
    this.toggleContextMenu(false, ContextTypes.none);
    this.calendarService[action](this.selectedDate);
  }

  private editCalendarEvent(action: contextMenuActionName): void {
    this.toggleContextMenu(false, ContextTypes.none);
    this.calendarService[action](null, this.selectedEvent);
  }

  private showDetailsCalendarEvent(action: contextMenuActionName): void {
    this.toggleContextMenu(false, ContextTypes.none);
    this.calendarService[action](null, this.selectedEvent, true);
  }

  private deleteCalendarEvent(type: CalendarEventsTypes): void {
    this.toggleContextMenu(false, ContextTypes.none);
    const id: number = this.selectedEvent.event.extendedProps.id;

    this.calendarService.deleteCalendarEvent(id, type);
  }

  // -- Inner functionality
  // filter
  public onMonthChange(month: string): void {
    this.currentMonthIndex = months[this.language].findIndex(m => m === month) + 1;
    const currentMonth: string = this.currentMonthIndex.toString().padStart(2, '0');

    const currentYear: number = new Date(this.calendarApi.getDate()).getFullYear();

    const newDate = `${currentYear}-${currentMonth}-01`;
    this.calendarApi.gotoDate(newDate);
  }

  public onYearChange(date: Date): void {
    const currentYear: number = new Date(date).getFullYear();
    const currentMonth: string = (new Date(date).getMonth() + 1).toString().padStart(2, '0');

    const newDate = `${currentYear}-${currentMonth}-01`;
    this.calendarApi.gotoDate(newDate);
  }

  private updateMonthAndYearFilters(date: Date): void {
    const monthIndex: number = date.getMonth();
    this.currentMonth = months[this.language][monthIndex];
    this.currentDate = date;
  }

  public onCalendarSelect(calendarIdList: CalendarKey[]): void {
    this.listOfSelectedCalendarIds = calendarIdList;
    this.fetchEvents();
  }

  private fetchEvents = (): void => {
    // reset calendar events aka event, task, todos
    this.calendarApi.removeAllEvents();
    // init tasks as they are default
    this.calendarService.initCalendarService(
      this.calendarEnvironment,
      this.calendarApi,
      this.language,
      this.allCalendars,
      this.fetchEvents
    );

    const isSuperAdmin: boolean = this.authService.getSessionInfo().user.isSuperAdmin;

    // init those events that are related to selected calendars: aka holidays
    const allCalendarEvents: ICalendarEvent[] = this.calendarEventsRepo.getStreamValue();

    allCalendarEvents.forEach(event => {
      if (this.listOfSelectedCalendarIds.includes(event.calendarId)) {
        this.calendarApi.addEvent({
          id: `${event.id} ${event._type}`,
          classNames: !event.isSystemDefault || isSuperAdmin
            ? `nv-holiday ${event.id} ${event._type}`
            : `no-edit nv-holiday ${event.id} ${event._type}`,
          title: event.displayLabel[this.language],
          start: event.start,
          end: event.end,
          allDay: event.isWholeDayEvent,
          borderColor: event.color,
          backgroundColor: event.color,
          startEditable: event.isSystemDefault ? isSuperAdmin : true, // date can't be changed anyways for holidays
          editable: event.isSystemDefault ? isSuperAdmin : true,
          display: 'block', // types: 'auto', 'block', 'list-item', 'background', 'inverse-background', or 'none'
          extendedProps: { // keep task data for update evn on drag&drop
            ...event
          }
        });
      }
    });

    this.allEventsToSearchFrom = [...this.calendarApi.getEvents()];
  }

  public searchValueChange(searchData: IHTMLInputEvent): void {
    const filteredEvents: EventInput[] = [];
    this.searchValueError = false;
    this.searchValue = searchData.target.value;

    if (!this.searchValue) {
      this.resetCalendarView();
      return;
    }
    const dateList: DateInput[] = [];
    this.allEventsToSearchFrom.filter(e => {
      const searchInString = e.extendedProps.displayLabel[this.language].toLowerCase();
      const searchAgainstString = this.searchValue.toLowerCase();
      if (searchInString.indexOf(searchAgainstString) >= 0) {
        filteredEvents.push(e);
        dateList.push(e.start);
        return e;
      }
    });

    if (!filteredEvents.length) {
      this.searchValueError = true;
      this.resetCalendarView();
      return;
    }

    const minDate: DateInput = min(dateList);
    const maxDate: DateInput = max(dateList);

    this.calendarApi.changeView('list');

    this.calendarApi.setOption(
      'visibleRange',
      {
        start: minDate,
        end: maxDate
      }
    )

    this.calendarApi.removeAllEvents();
    this.calendarApi.setOption('events', filteredEvents);
  }

  private resetCalendarView(): void {
    this.calendarApi.gotoDate(new Date());
    this.calendarApi.changeView('dayGridMonth');
    this.fetchEvents();
    this.updateMonthAndYearFilters(new Date());
  }

  public onDateUntilFor4WeeksChange(date: Date): void {
    const startDate: Date = moment(date).subtract(27, 'days').toDate();
    this.jumpToDayAndSwitchWeekDayStart(startDate, date);
  }

  private jumpToDayAndSwitchWeekDayStart(startDate: Date, endtDate: Date): void {
    const weekIndexOfStartDate: number =  startDate.getDay();

    this.calendarOptions.firstDay = weekIndexOfStartDate;
    this.calendarApi.gotoDate(startDate);
    this.calendarRangeChange.emit({ start: moment(startDate), end: moment(endtDate) });
  }

  private toggleShownPeriodFor4Weeks(unit: Unit , direction: Direction ): void {
    let currentYear: number = this.currentDate.getFullYear();

    if (unit === Unit.month ) {
      if (this.currentMonthIndex === 0 && direction === Direction.down) {
        this.currentMonthIndex = 11;
        currentYear -= 1;
      } else if (this.currentMonthIndex === 11 && direction === Direction.up) {
        this.currentMonthIndex = 0;
        currentYear += 1;
      } else if (direction === Direction.down) {
        this.currentMonthIndex -= 1;
      } else {
        this.currentMonthIndex += 1;
      }
    } else {
      currentYear = direction === Direction.down ? --currentYear : ++currentYear;
    }

    const month: string = this.currentMonthIndex <= 9
      ? this.currentMonthIndex.toString().padStart(2, '0')
      : this.currentMonthIndex.toString();

    const startDate = new Date(`${this.currentMonthIndex + 1}-01-${currentYear}`);
    const endDate: Date = moment(startDate).add(27, 'days').toDate();
    this.jumpToDayAndSwitchWeekDayStart(startDate, endDate);

    this.currentDate = startDate;
    this.dateUntilFor4Weeks = this.currentDate;
  }

  public createCalendar(): void {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.chartering.createCalendar[this.language],
      nzContent: CalendarModalComponent,
      nzClosable: true,
      nzWidth: 600,
      nzBodyStyle: {overflow: 'hidden', padding: '0'},
      nzComponentParams: {
        closeModal: () => {
          this.closeModal();
          this.fetchEvents();
        }
      },
    });
  }

  private closeModal = (): void => {
    this.modalRef.destroy();
  }

  // keyboard control
  @HostListener('document:keyup.arrowup')
  private onArrowUp(): void {
    this.calendarApi.next();
    this.updateMonthAndYearFilters(this.calendarApi.getDate());
  }

  @HostListener('document:keyup.arrowdown')
  private onArrowDown(): void {
    this.calendarApi.prev();
    this.updateMonthAndYearFilters(this.calendarApi.getDate());
  }

  @HostListener('document:click', ['$event'])
  private onContextMenuOutClick(event): void {
    const classList: DOMTokenList = event.toElement.classList;

    const eventEl: HTMLElement = event.target.closest('.fc-daygrid-event');
    const dateCellEl: HTMLElement = event.target.closest('.fc-daygrid-day');

    if (this.isContextMenuVisibile
      && !dateCellEl
      && !eventEl
      && !this.contextMenu.nativeElement.contains(event.target)) {
        this.toggleContextMenu(false, ContextTypes.none);
    }

    if (classList.contains('fc-today-button')
        || classList.contains('fc-icon-chevron-left')
        || classList.contains('fc-icon-chevrons-left')
        || classList.contains('fc-icon-chevron-right')
        || classList.contains('fc-icon-chevrons-right')
    ) {
      this.updateMonthAndYearFilters(this.calendarApi.getDate());
    }
  }

  public onRightClick(event): void {
    event.preventDefault();
    const eventEl: HTMLElement = event.target.closest('.fc-daygrid-event');
    const dateCellEl: HTMLElement = event.target.closest('.fc-daygrid-day');

    if (eventEl) {
      const evIdentifier: string = eventEl.getAttribute('class')
        .replace( /^\D+/g, ''); // filter string starting from numbers
      const calendarEvent: EventInput = this.calendarApi.getEvents().find(e => e.id === evIdentifier);
      this.onEventClick({event: calendarEvent, jsEvent: event});
      return;
    }

    if (dateCellEl) {
      const dateStr: string = dateCellEl.getAttribute('data-date');
      this.onShowContextMenuAdd(new Date(dateStr), event);
    }
  }

  @HostListener('mousewheel', ['$event'])
  private onMousewheel(event): void {
    if (!this.isFourWeekCalendar) {
      return;
    }
    this.wheelCounterStart += 1;
    this.wheelDirection = event.deltaY > 0 ? Direction.up : Direction.down;
    if (!this.isWheelStarted) { this.wheelStart(); }
  }

  private wheelStart(): void {
    this.isWheelStarted = true;

    this.wheelCounterEnd = this.wheelCounterStart;

    setTimeout(() => {
      if (this.wheelCounterEnd === this.wheelCounterStart) {
        this.wheelEnd();
      } else {
        this.wheelStart();
      }
    }, 30);
  }

  private wheelEnd(): void {
    this.toggleShownPeriodFor4Weeks(Unit.month, this.wheelDirection);
    this.isWheelStarted = false;
    this.wheelCounterStart = 0;
    this.wheelCounterEnd = null;
    this.wheelDirection = null;
  }
}
