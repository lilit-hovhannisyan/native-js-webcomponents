import { FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { ACCOUNTING_TYPES } from 'src/app/core/models/enums/accounting-types';

export const isNumberBetween = (formControl: FormControl) => {
  const { value } = formControl;
  const number = value;
  const isValid = !/^([1-9][0-9]{0,3})$/.test(number);
  const message = wording.accounting.numberNeedsToTeBetween;
  return isValid
    ? null
    : { dateTimeInvalid: message };
};

// Validation Rule 37
// https://navido.atlassian.net/browse/NAVIDO-591
// https://navido.atlassian.net/wiki/spaces/TE/pages/376274945/Booking+form
export const isChosenBookingAccountAllowed = (
  getAccountingType: () => ACCOUNTING_TYPES,
  bookingAccounts: IBookingAccount[],
): ValidatorFn => {
  return (control: AbstractControl | null) => {
    const accountingType = getAccountingType();
    const foundBookingAccount = bookingAccounts
      .find(bookingAccount => bookingAccount.no === control.value);
    if (
      foundBookingAccount &&
      (accountingType !== (accountingType & foundBookingAccount.accountingType))
    ) {
      return {
        accountNotAllowed: {
          message: wording.accounting.accountIsNotAllowed
        }
      };
    }
    return null;
  };
};

export const isAccountingTypeValid = (validAccountingType: number, toCheckAccountingType: number) => {
  return validAccountingType === (validAccountingType & toCheckAccountingType);
};

