import { AccountingRelationKey } from './IAccountingRelation';
import { IBankBase } from './IBank';
import { BookingAccountKey } from './IBookingAccount';
import { ICurrency } from './ICurrency';
import { MandatorKey } from './IMandator';
import { IResource } from './IResource';
import { IWording } from './IWording';

export type BankAccountKey = number;

interface ISharedProps {
  accountNo: string;
  iban: string;
  viaBank: string;
  viaAccount: string;
}

export interface IBankAccountCreate extends ISharedProps { }

export interface IBankAccountBase extends ISharedProps, IResource<BankAccountKey> {
  viaBic: string;
  viaBankCode: string;
  forInvoices: boolean;
  forPayments: boolean;
  forAccountStatements: boolean;
  companyId: number;
  bankId: number;
  currencyId: number;
}

export interface IBankAccountDTO extends IBankAccountBase { }

export interface IBankAccount extends IBankAccountDTO { }

export interface IBankAccountFull extends IBankAccountDTO {
  bank: IBankBase;
  currency: ICurrency;
  bic: string;
}

export interface IBankAccountRow extends IBankAccountFull {
  bankName: string;
  currencyLabel: IWording;
}

export interface IBankAccountRelation extends IBankAccount {
  mandatorId: MandatorKey;
  bookingAccountId: BookingAccountKey;
  bankAccountNo: string;
  relationId: AccountingRelationKey;
}


export const bankAccountNoModifier = (value: string): string | null => {
  if (value) {
    return value.toString().padStart(10, '0');
  }
  return null;
};
