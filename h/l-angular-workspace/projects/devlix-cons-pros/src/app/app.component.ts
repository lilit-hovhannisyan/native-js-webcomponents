import { Component } from '@angular/core';
import {appName, routeConf} from '../../../shared-layer/l-hovhanni/dal/app.config';
import {Title} from '@angular/platform-browser';
import {NavigationEnd, Router} from '@angular/router';
import {AppDataService} from './dal/app-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

    constructor(private title: Title,
                private router: Router,
                private appDataService: AppDataService) {

        this.appDataService.appData.subscribe( res => {
            if (!Object.keys(res).length) {
              this.router.navigate(['select-group']);
            }
        });
    }
}
