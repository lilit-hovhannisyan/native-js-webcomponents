import { Component, ElementRef, OnDestroy, Optional, Self, OnInit, Injector } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { FormGroup, ControlValueAccessor, NgControl, AbstractControl, FormControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';

import { ReplaySubject, Subject, Subscription } from 'rxjs';

import { MeAttributeComponentInterface } from '../me-attribute-component.interface';
import { MeMultiSelectInputI } from './me-multi-select-input.interface';
import { BaseLoggerComponent } from '@features/tracking-system';
import { takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './me-multi-select-input.component.html',
  styleUrls: ['./me-multi-select-input.component.scss'],
  providers: [
    {
      provide: MatFormFieldControl,
      useExisting: MeMultiSelectInputComponent
    }
  ]
})
export class MeMultiSelectInputComponent
  extends BaseLoggerComponent
  implements MeAttributeComponentInterface, ControlValueAccessor, MatFormFieldControl<string>, OnDestroy, OnInit {
  // variables for MatFormFieldControl interface
  static nextId = 0;
  componentVersion: string = '0.0.1';
  currentValue!: string[] | undefined;
  name: string = '';
  data!: MeMultiSelectInputI;
  form!: FormGroup;
  multiselectLabels: string[] = [];

  // True when some error state change are happened
  private _stateChanged: boolean = false;
  private _placeholder!: string;
  private _required = false;
  private _disabled = false;
  private _valueChangeSubscription!: Subscription | undefined;
  stateChanges = new Subject<void>();
  focused = false;
  _errorState = false;
  controlType = 'me-multi-select-input';
  id = `me-multi-select-input-${MeMultiSelectInputComponent.nextId++}`;
  describedBy = '';

  filteredValues: ReplaySubject<{ displayName: string; value: string }[]> = new ReplaySubject(1);

  searchFilterCtrl: FormControl = new FormControl('');

  private _onDestroy: Subject<void> = new Subject<void>();

  // tslint:disable-next-line:no-any
  onChange = (_: any) => {};
  onTouched = () => {
    this.formControl?.markAsTouched();
    // tslint:disable-next-line:semicolon
  };

  get errorState(): boolean {
    // If attribute is required and something interacted with him
    if (this.data.required && this.formControl?.touched) {
      // If there is change on error state, set it and set _stateChanged indicator
      if (this._errorState !== !this.formControl?.valid) {
        this._stateChanged = true;
        this._errorState = !this.formControl?.valid;
      }
      // Actualy update error state only if there is error state change
      if (this._stateChanged) {
        this._stateChanged = false;
        this.stateChanges.next();
      }
      return this._errorState;
    }
    this._errorState = false;
    return this._errorState;
  }

  get formControl(): AbstractControl | null {
    return this.form.get(this.data.internalName);
  }

  constructor(
    injector: Injector,
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,

    @Optional() @Self() public ngControl: NgControl
  ) {
    super(injector);
    _focusMonitor.monitor(_elementRef, true).subscribe((origin) => {
      if (this.focused && !origin) {
        this.onTouched();
      }
      this.focused = !!origin;
      this.stateChanges.next();
    });

    if (this.ngControl !== null) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit(): void {
    this.form = this.data.form;
    this.name = this.data.internalName;
    this.currentValue = this.formControl?.value;
    this.disabled = this.data.disabled;
    this.refreshChips();
    this._valueChangeSubscription = this.formControl?.valueChanges.subscribe((val: string[]) => {
      this.currentValue = val;
    });

    this.filteredValues.next(this.data.values.slice());

    this.searchFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {
      this.filterValues();
    });
  }

  ngOnDestroy(): object {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
    if (this._valueChangeSubscription) {
      this._valueChangeSubscription.unsubscribe();
    }

    this._onDestroy.next();
    this._onDestroy.complete();
    return super.ngOnDestroy();
  }

  // methods required by MatFormFieldControl interface
  get empty(): boolean {
    const optionsValues = this.formControl?.value as string[];
    return optionsValues === null || optionsValues.length === 0;
  }

  get shouldLabelFloat(): boolean {
    return this.focused || !this.empty;
  }

  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  get required(): boolean {
    return this._required;
  }
  set required(value: boolean) {
    this._required = value;
    this.stateChanges.next();
  }

  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = value;

    setTimeout(() => {
      if (this.form) {
        this._disabled ? this.formControl?.disable() : this.formControl?.enable();
        this.stateChanges.next();
      }
    }, 0);
  }

  get value(): string {
    return this.formControl?.value;
  }
  set value(value: string) {
    this.formControl?.setValue(value);
    this.stateChanges.next();
  }

  setDescribedByIds(ids: string[]): void {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(): void {}

  writeValue(value: string): void {
    this.value = value;
  }

  // tslint:disable-next-line:no-any
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  // tslint:disable-next-line:no-any
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  // component methods
  refreshChips(): void {
    this.multiselectLabels = [];
    const optionsValues = this.formControl?.value as string[];

    if (optionsValues) {
      optionsValues.forEach((optionValue) => {
        const optionLabel: string | undefined = this.data.values.find((value) => value.value === optionValue)?.displayName;
        if (optionLabel) {
          this.multiselectLabels.push(optionLabel);
        }
      });
    }
  }

  filterValues(): void {
    if (!this.data.values) {
      return;
    }
    // get the search keyword
    let search = this.searchFilterCtrl.value;
    if (!search) {
      this.filteredValues.next(this.data.values.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredValues.next(this.data.values.filter((value) => value.displayName.toLowerCase().indexOf(search) > -1));
  }

  multiselectItemRemoved(removedItem: string): void {
    const remainingItems = this.formControl?.value as string[];
    this.formControl?.setValue(this.removeFirst(remainingItems, removedItem));
    this.refreshChips();
  }

  private removeFirst(array: string[], toRemove: string): string[] {
    const optionId = this.data.values.find((value) => value.displayName === toRemove)?.value;
    const index = optionId ? array.indexOf(optionId) : -1;
    if (index !== -1) {
      array.splice(index, 1);
    }

    // Works only if return new array
    return [...array];
  }
}
