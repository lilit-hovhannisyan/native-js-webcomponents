import { Injectable } from '@angular/core';
import { IComment } from '../../core/models/resources/IComment';
import { Observable, BehaviorSubject } from 'rxjs';
import { commentsMock } from '../../core/api-mocks/comments/comments.mock';
import { AuthenticationService } from '../../authentication/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private comments: IComment[] = commentsMock;
  public commentsSubject: BehaviorSubject<IComment[]> = new BehaviorSubject<IComment[]>(commentsMock);
  constructor(
    private authSerivce: AuthenticationService
  ) { }

  public update(comment: IComment): void {
    const index = this.comments.findIndex(res => res.id === comment.id);
    this.comments[index].text = comment.text;
    this.commentsSubject.next(this.comments);
  }

  public create(text: string): Observable<IComment[]> {
    const sessionInfo = this.authSerivce.getSessionInfo();
    const data = {
      creationDate: new Date().toString(),
      id: this.comments.length,
      text: text,
      author: {
        id: sessionInfo.user.id,
        firstName: sessionInfo.user.firstName,
        email: sessionInfo.user.email,
        lastName: sessionInfo.user.lastName
      },
      _type: 'comment'
    };
    this.comments.push(data);
    this.commentsSubject.next(this.comments);
    return this.commentsSubject.asObservable();
  }

  public fetchAll(): Observable<IComment[]> {
    return this.commentsSubject.asObservable();
  }

  public deleteComment(id: number): void {
    this.comments = this.comments.filter(res => {
      return res.id !== id;
    });
    this.commentsSubject.next(this.comments);
  }
}
