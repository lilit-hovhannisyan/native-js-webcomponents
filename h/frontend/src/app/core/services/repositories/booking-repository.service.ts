import { BookingKey } from './../../models/resources/IBooking';
import { map } from 'rxjs/operators';
import { HttpGet } from 'src/app/core/models/http/http-get';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IBookingDTO, IBookingCreate, IBooking } from '../../models/resources/IBooking';
import { BookingAccountRepositoryService } from './booking-account-repository.service';
import { IBookingAccount } from '../../models/resources/IBookingAccount';
import { RepositoryService } from '../repository.service';
import { IBookingValidation } from '../../models/resources/IBookingValidation';
import { HttpPost } from '../../models/http/http-post';
import { HttpErrorHandler } from 'src/app/core/interceptors/http-error-handler';
import { HttpResponse } from '@angular/common/http';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';


export interface BookingRepoParams extends RepoParams<IBooking> {
}

@Injectable({
  providedIn: 'root'
})
export class BookingRepositoryService extends AbstractRepository<
IBooking,
IBookingDTO,
IBookingCreate,
BookingRepoParams,
BookingKey
>  {

  public bookingAccounts: IBookingAccount[];

  protected resourceType = 'Booking';
  constructor(
    private httpErrorHandler: HttpErrorHandler,
    repositoryService: RepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService
  ) {
    super(repositoryService);
  }

  protected postProcess(item: IBookingDTO): IBooking { return item; }

  public getBaseUrl(): string {
    return `/api/bookings`;
  }

  public getBookingsByMandator(mandatorId: number): Observable<IBooking[]> {
    const url = `/api/mandators/${mandatorId}/bookings`;
    const request = new HttpGet(url);
    return this.repositoryService.httpClient.request(request).pipe(
      map((response: any) => response.body.data.items as IBooking[]),
    );
  }

  public checkValidations(booking: IBooking): Observable<IBookingValidation> {
    const url = `${this.getBaseUrl()}/check-validations`;
    const request = new HttpPost(url, booking);
    return this.repositoryService.httpClient.request(request).pipe(
      map((response: HttpResponse<IDataResponseBody<IBookingValidation>>) => {
        if (response.body.data.errors && response.body.data.errors.length > 0) {
          const errorsWithWording = response.body.data.errors.map(error => {
            return this.httpErrorHandler.getErrorWithWording(error, true);
          });

          return {
            ...response.body.data,
            errors: [
              ...errorsWithWording,
            ]
          };
        }
        return response.body.data as IBookingValidation;
      }),
    );
  }
}
