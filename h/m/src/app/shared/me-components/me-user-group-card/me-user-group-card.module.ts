import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCheckboxModule } from '@angular/material/checkbox';

import { MeUserGroupCardComponent } from './me-user-group-card.component';
import { MeContextMenuModule } from '../me-context-menu/me-context-menu.module';

import { MeUserImageModule } from '@shared-components/me-user-image/me-user-image.module';
import { MeLabelModule } from '@shared-components/me-label/me-label.module';

@NgModule({
  declarations: [MeUserGroupCardComponent],
  imports: [CommonModule, MatCheckboxModule, MeContextMenuModule, MeUserImageModule, MeLabelModule],
  exports: [MeUserGroupCardComponent]
})
export class MeUserGroupCardModule {}
