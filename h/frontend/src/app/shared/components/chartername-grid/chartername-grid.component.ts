import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NvGridConfig } from 'nv-grid';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { AbstractValueAccessor, makeProvider } from 'src/app/core/abstracts/value-accessor';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { getCharternameGridConfig } from 'src/app/core/constants/nv-grid-configs/chartername-grid.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { getUniqueId } from 'src/app/core/helpers/general';
import { IVoyageCharterName } from 'src/app/core/models/resources/IVoyageCharterName';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { CharternameDetailViewModalComponent } from '../chartername-detail-view-modal/chartername-detail-view-modal.component';

/**
 * It is possible to create/edit/delete more than one chartername and
 * save them at once on edition mode. The field `_id` which is a unique
 * identifier - needed for editing unsaved but new charternames and
 * needed only inside of current component.
 * Backend will ignore this property.
 */
interface ICharterName extends IVoyageCharterName {
  _id: string;
}

@Component({
  selector: 'nv-chartername-grid',
  templateUrl: './chartername-grid.component.html',
  styleUrls: ['./chartername-grid.component.scss'],
  providers: [ makeProvider(CharternameGridComponent) ],
})
export class CharternameGridComponent
  extends AbstractValueAccessor<ICharterName[]>
  implements OnChanges {

  @Input() public isEditableGrid = false;
  @Input() public voyageFrom?: string;
  @Input() public voyageUntil?: string;
  @Input() public isDisabled = false;
  public dataSource: ICharterName[];
  public gridConfig: NvGridConfig;
  private readonly zone = 'chartering.misc.voyages.charternames';

  constructor(
    private authService: AuthenticationService,
    private modalService: NzModalService,
    private confirmationService: ConfirmationService,
    private settingsService: SettingsService,
  ) {
    super();
  }

  public ngOnChanges(changes: SimpleChanges) {
    const { isEditableGrid, disabled } = changes;
    if (
      isEditableGrid && isEditableGrid.currentValue !== isEditableGrid.previousValue
      || disabled
    ) {
      this.setGridConfig();
    }
  }

  public writeValue(value: ICharterName[]) {
    super.writeValue(value);
    this.dataSource = Array.from(value || []);
  }

  private setGridConfig(): void {
    const actions = {
      edit: (entity: ICharterName) => this.openDetailView(entity),
      delete: this.deleteConfirm,
      add: () => this.openDetailView(),
    };
    const isActionsDisabled = (entity: IVoyageCharterName): boolean => {
      return this.dataSource[0] === entity;
    };

    this.gridConfig = getCharternameGridConfig(
      actions,
      (operation: number) => this.authService.can(operation, this.zone),
      this.isEditableGrid,
      isActionsDisabled,
      this.isDisabled,
    );
  }

  private deleteConfirm = (entity: ICharterName): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: entity.charterName },
    }).subscribe(confirmed => confirmed && this.deleteChartername(entity));
  }

  private openDetailView(entity?: ICharterName): void {
    const editWord = wording.general.edit[this.settingsService.language];
    const addWord = wording.general.add[this.settingsService.language];
    const title = entity ? `${editWord} ${entity.charterName}` : addWord;
    this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: title,
      nzClosable: true,
      nzContent: CharternameDetailViewModalComponent,
      nzComponentParams: {
        chartername: entity,
        from: this.getCharternameFrom(entity),
        until: this.voyageUntil,
        voyageFrom: this.voyageFrom,
      },
      nzWidth: 450,
    }).afterClose.subscribe(this.entitiesChangeHandler);
  }

  private getCharternameFrom(entity?: IVoyageCharterName): string {
    const lastChartername = this.dataSource[0] === entity
      ? this.dataSource[1]
      : this.dataSource[0];
    return lastChartername?.from;
  }

  private entitiesChangeHandler = (result?: ICharterName) => {
    if (!result) {
      return;
    }

    /**
     * 1. When `result.id && item.id === result.id` is a truthy value, then
     * the current entity which is being edited - is already saved in
     * backend and has `id` property.
     * 2. When `result._id && item._id === result._id` is true, then the current
     * entity which is being edited is created already but not saved in
     * the backend yet.
     * 3. When both are falsy values, thus the value of `index` is -1,
     * then it's a creation of a new entity(createMode).
     */
    const index = this.value.findIndex(item => {
      return result.id && item.id === result.id
      || result._id && item._id === result._id;
    });

    if (index !== -1) {
      this.value.splice(index, 1, result);
    } else {
      this.value.push({
        ...result,
        _id: getUniqueId(10),
      });
    }

    this.writeValue(this.value);
    this.onChange(this.value);
  }

  private deleteChartername(entity: ICharterName): void {
    const index = this.value.findIndex((item: ICharterName) => item === entity);
    if (index < 0) {
      return;
    }

    this.value.splice(index, 1);
    this.writeValue(this.value);
    this.onChange(this.value);
  }
}
