import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/booking-codes.config';
import { Language } from 'src/app/core/models/language';
import { Observable } from 'rxjs';
import { IBookingCode } from 'src/app/core/models/resources/IBookingCode';
import { SettingsService } from 'src/app/core/services/settings.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { Operations } from 'src/app/core/models/Operations';

@Component({
  selector: 'nv-booking-codes-list-view',
  templateUrl: './booking-codes-list-view.component.html',
  styleUrls: ['./booking-codes-list-view.component.scss']
})
export class BookingCodesListViewComponent implements OnInit {
  public bookingCodeGridConfig: NvGridConfig;
  public dataSource$: Observable<IBookingCode[]>;
  public language: Language;
  public isAdmin: boolean;

  constructor(
    private bookingCodeRepo: BookingCodeRepositoryService,
    public gridConfigService: GridConfigService,
    private settingsService: SettingsService,
    private router: Router,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService
  ) { }

  public ngOnInit(): void {
    this.isAdmin = this.authService.getUser().isAdmin;
    this.language = this.settingsService.language;
    this.bookingCodeRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.bookingCodeRepo.getStream();
    const actions = {
      edit: (id: number) => this.router.navigate([`${this.router.url}/${id}`]),
      delete: this.delete,
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    this.bookingCodeGridConfig = getGridConfig(
      actions,
      (operation: Operations) => {
        return this.authService.canPerformOperationOnCurrentUrl(operation)
          && this.isAdmin;
      }
    );
  }

  public delete = (bookingCode: IBookingCode) => {
    if (bookingCode.isSystemDefault && !this.authService.getUser().isSuperAdmin) {
      this.confirmationService.warning({
        title: wording.general.systemTypesCanNotBeDeleted,
        okButtonWording: wording.general.ok,
        cancelButtonWording: null,
      });
      return;
    }
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: bookingCode.displayLabel },
    }).subscribe(confirmed => confirmed && this.bookingCodeRepo.delete(bookingCode.id, {}).subscribe());
  }
}
