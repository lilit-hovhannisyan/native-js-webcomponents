import { Pipe, PipeTransform } from '@angular/core';
import { IForeignExchangeRate } from 'src/app/core/models/resources/IExchangeRate';

@Pipe({
  name: 'rateDate',
  pure: true
})
export class RateDatePipe implements PipeTransform {
  public transform(date: string, rates: IForeignExchangeRate, swapped: boolean): number | null {
    const exchangeRate = rates[date];
    return exchangeRate
      ? swapped
        ? 1 / exchangeRate.rate
        : exchangeRate.rate
      : null;
  }
}
