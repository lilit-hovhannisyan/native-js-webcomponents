import { Type } from 'class-transformer';
import { BaseModel } from '../base-model/base-model';
import { FileUploadResponseAttributesModel } from './file-upload-response-attributes.model';

export class FileUploadResponseModel extends BaseModel {
  @Type(() => FileUploadResponseAttributesModel)
  attributes!: FileUploadResponseAttributesModel;
}
