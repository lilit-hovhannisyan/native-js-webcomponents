/* tslint:disable:max-line-length*/

export const wordingAccounting = {
  // taxkeys
  taxRelCodes: { en: 'Related Tax Codes', de: 'Verknüpfte Steuerschlüssel' },
  taxRelInfo: { en: 'Tax related information', de: 'Steuer-Informationen' },
  taxKeysLabelCode: { en: 'Code', de: 'Code' },
  taxKey: { en: 'Tax Key', de: 'Steuerschlüssel' },
  taxKeysLabelPercentage: { en: 'Percentage', de: 'Prozentsatz' },
  taxKeysLabelCalculationType: { en: 'Calculation Type', de: 'Calculation Type' },
  taxKeysLabelTaxCategory: { en: 'Tax Category', de: 'Tax Category' },
  taxKeysLabelCalculationInclude: { en: 'Basic amount includes tax rate', de: 'Steuersatz ist im Basisbetrag enthalten' },
  taxKeysLabelCalculationExclude: { en: 'Basic amount excludes tax rate', de: 'Steuersatz ist nicht im Basisbetrag enthalten' },

  taxKeysLabelEn: { en: 'Label En', de: 'Label En' },
  taxKeysLabelDe: { en: 'Label De', de: 'Label De' },

  taxKeysLabelActive: { en: 'Is Active', de: 'Is Active' },
  taxKeysLabelBookingAccount: { en: 'Account Number', de: 'Kontonummer' },
  taxKeysLabelAccountNo: { en: 'Account No', de: 'Konto Nr' },
  inputTax: { en: 'Input Tax', de: 'Vorsteuer' },
  valueAddedTax: { en: 'Value added Tax', de: 'Mehrwertsteuer' },

  accountSheets: { en: 'Account Sheets', de: 'Kontoblätter' },

  buttonDescriptionEditTaxKey: { en: 'Edit Tax Key', de: 'Edit Tax Key' },
  buttonDescriptionDeleteTaxKey: { en: 'Delete Tax Key', de: 'Delete Tax Key' },
  buttonDescriptionCreateTaxKey: { en: 'Create New Tax Key', de: 'Create New Tax Key' },
  buttonDescriptionAddTaxKey: { en: 'Add New Tax Key', de: 'Add New Tax Key' },

  tooltipTextEditTaxKey: { en: 'Edit Tax Key', de: 'Edit Tax Key' },
  tooltipTextDeleteTaxKey: { en: 'Delete Tax Key', de: 'Delete Tax Key' },
  tooltipTextCreateTaxKey: { en: 'Create New Tax Key', de: 'Create New Tax Key' },
  tooltipTextAddTaxKey: { en: 'Add New Tax Key', de: 'Add New Tax Key' },

  titleTextCreateTaxKey: { en: 'Creating new Tax Key', de: 'Creating new Tax Key' },
  titleTextDetailsTaxKey: { en: 'Tax Key Details', de: 'Tax Key Details' },
  titleTaxKeys: { en: 'Tax Keys', de: 'Tax' },

  // modal contexts"
  modalContextDeleteTaxKey: {
    en: 'Are you sure you want to delete the Tax Key?',
    de: 'Are you sure you want to delete the Tax Key?'
  },

  labelEn: { en: 'Label EN', de: 'Etikette EN' },
  labelDe: { en: 'Label DE', de: 'Etikette DE' },
  abbreviation: { en: 'Abbreviation', de: 'Abkürzung' },
  useCase: { en: 'Use Case', de: 'Anwendungsfall' },
  active: { en: 'Active', de: 'Aktiv' },

  // Voucher Types
  voucherTypeDetail: { en: 'Voucher Type Detail', de: 'Belegartendetail' },
  newVoucherType: { en: 'New Voucher Type', de: 'Neuer Gutscheintyp' },
  editVoucherType: { en: 'Edit Voucher Type', de: 'Belegart bearbeiten' },
  voucherTypestitle: { en: 'Voucher Types', de: 'Belegarten' },
  autoNumbering: { en: 'Automatic Numbering', de: 'Automatische Nummerierung' },
  canNotDeleteSystemVoucherTypes: {
    en: 'System Voucher Types can not be deleted.',
    de: 'System  Belegarten können nicht gelöscht werden.'
  },
  canNotEditSystemDefaultTypes: {
    en: 'System Default Types can not be edited.',
    de: 'System Default Types can not be edited.'
  },
  // chart of accounts
  bookingAccountsTitle: { en: 'Chart of Accounts', de: 'Chart of Accounts' },
  no: { en: 'No.', de: 'Nr.' },
  costAccounting: { en: 'Cost Accounting', de: 'Kostenrechnung' },
  openItem: { en: 'Open Item', de: 'Open Item' },
  openItems: { en: 'Open Items', de: 'Offene Posten' },
  opex: { en: 'OPEX', de: 'OPEX' },
  remark: { en: 'Remark', de: 'Bemerkung' },
  assetAccount: { en: 'Asset Account', de: 'Asset Account' },
  restrictedCurrency: { en: 'Restricted Currency', de: 'Währungseinschränkung' },
  hgb: { en: 'HGB', de: 'HGB' },
  ifrs: { en: 'IFRS', de: 'IFRS' },
  usgaap: { en: 'USGAAP', de: 'USGAAP' },
  hgbAccounting: { en: 'HGB Accounting', de: 'HGB Accounting' },
  ifrsAccounting: { en: 'IFRS Accounting', de: 'IFRS Accounting' },
  usgaapAccounting: { en: 'USGAAP Accounting', de: 'USGAAP Accounting' },
  eBalanceAccounting: { en: 'E-Balance Accounting', de: 'E-Bilanz Accounting' },
  eBalance: { en: 'E-Balance', de: 'E-Bilanz' },
  account: { en: 'Account', de: 'Konto' },
  accountNo: { en: 'Account No', de: 'Konto Nr' },
  accountName: { en: 'Account Name', de: 'Account Name' },
  paymentsRelevance: { en: 'Payments Relevance', de: 'Payments Relevance' },
  bookingCodeTitle: { en: 'Booking Code', de: 'Booking Code' },
  bookingCodes: { en: 'Booking Codes', de: 'Booking Codes' },
  bookingCode: { en: 'Booking Code', de: 'Vorgangsschlüssel' },
  chooseBookingCode: { en: 'Choose Booking Code', de: 'Choose Booking Code' },
  isLocked: { en: 'Is locked', de: 'Is Locked' },
  defineInvoice: { en: 'Define Invoice', de: 'Define Invoice' },
  leadingAccount: { en: 'Leading Account', de: 'Leading Account' },
  mandator: { en: 'Mandator', de: 'Mandant' },
  mandatorNo: { en: 'Mandator No', de: 'Mandant Nr' },
  mandatorNumber: { en: 'Mandator Number', de: 'Mandator Number' },
  mandatorName: { en: 'Mandator Name', de: 'Mandator Name' },
  mandatorCompanyName: { en: 'Mandator Company Name', de: 'Mandator Company Name' },
  legalForm: { en: 'Legal Form', de: 'Rechtsform' },
  firstBookingYear: { en: 'First Booking Year', de: 'First Booking Year' },
  mandatorType: { en: 'Mandator Type', de: 'Mandator Type' },
  accounting: { en: 'Accounting', de: 'Accounting' },
  fiscalYear: { en: 'Fiscal Year', de: 'Fiscal Year' },
  localCurrency: { en: 'Local Currency', de: 'Hauswährung' },
  foreignCurrency: { en: 'Foreign Currency', de: 'Foreign Currency' },
  invoiceWorkflow: { en: 'Invoice Workflow', de: 'Invoice Workflow' },
  autobankWorkflow: { en: 'Autobank Workflow', de: 'Autobank Workflow' },
  paymentTransfer: { en: 'Payment Transfer', de: 'Payment Transfer' },
  lastClosedYear: { en: 'Last closed year', de: 'Erfolgter Jahresabschluss' },
  lastClosedPeriod: { en: 'Last closed period', de: 'Letzte geschlossene Periode' },
  blockedUntil: { en: 'Blocked until', de: 'Blocked until' },
  blockedAt: { en: 'Blocked at', de: 'Blocked at' },
  blockedBy: { en: 'Blocked by', de: 'Blockiert von' },
  debitorAccount: { en: 'Debtor account', de: 'Debitor account' },
  creditorAccount: { en: 'Creditor account', de: 'Creditor account' },
  selectDebitorCreditor: { en: 'Select Debtor Creditor', de: 'Select Debitor Creditor' },
  accruedAndDeferredItem: { en: 'Account Accrued and Deferred Item', de: 'Konto RAP' },
  accrualAccounting: { en: 'Accrual Accounting', de: 'Periodengerechte Abgrenzung' },
  defaultAccountsAccrualAccounting: {
    en: 'Default accounts for accrual accounting are administrated in the global settings.',
    de: 'Die Default-Konten für die Rechnungsabgrenzung werden über die globalen Einstellungen festgelegt.' },
  typeOfAccount: { en: 'Type of Account', de: 'Kontotyp RAP' },
  accountPrepaidExpenses: { en: 'Account Prepaid Expenses', de: 'Konto ARAP' },
  accountDeferredIncome: { en: 'Account Deferred Income', de: 'Konto PRAP' },
  defaultAccountPrepaidExpenses: { en: 'Default Account Prepaid Expenses', de: 'Default-Konto ARAP' },
  defaultAccountDeferredIncome: { en: 'Default Account Deferred Income', de: 'Default-Konto PRAP' },
  // BANKS
  banks: { en: 'Banks', de: 'Banks' },
  bank: { en: 'Bank', de: 'Bank' },
  bankName: { en: 'Bank Name', de: 'Bank Name' },
  bankCode: { en: 'Bank Code', de: 'Bank Code' },
  bic: { en: 'BIC', de: 'BIC' },
  bicSwift: { en: 'BIC/Swift', de: 'BIC/Swift' },
  differentAccountHolder: { en: 'Different Account Holder', de: 'Unterschiedlicher Kontoinhaber' },

  // AUTOBANK SETTINGS BUSINESS CASE
  gvc: { en: 'GVC', de: 'GVC' },

  // PORTS
  ports: { en: 'Ports', de: 'Ports' },
  unLoCode: { en: 'UN/LO Code', de: 'UN/LO Code' },

  // COMPANY
  businessPartner: { en: 'Business Partner', de: 'Geschäftspartner' },
  privatePerson: { en: 'Private Person', de: 'Privatperson' },
  customer: { en: 'Customer', de: 'Kunde' },
  companyName: { en: 'Company Name', de: 'Firmenname' },
  taxNo: { en: 'Tax No.', de: 'Steuernummer' },
  vatId: { en: 'Vat ID', de: 'UstID' },
  bankAccount: { en: 'Bank Account', de: 'Bankkonto' },
  importBankAccount: { en: 'Import Bank Account', de: 'Import Bankkonto' },
  bankAccounts: { en: 'Bank Accounts', de: 'Bankkonten' },
  salutation: { en: 'Salutation', de: 'Anrede' },
  chooseSalutation: { en: 'Choose Salutation', de: 'Choose Salutation' },
  basic: { en: 'Basic', de: 'Allgemein' },
  address: { en: 'Address', de: 'Adresse' },
  addresses: { en: 'Addresses', de: 'Adressen' },
  contact: { en: 'Contact', de: 'Kontakt' },
  contacts: { en: 'Contacts', de: 'Kontakte' },
  taxNumber: { en: 'Tax Number', de: 'Steuernummer' },
  addNewBank: { en: 'Add Bank Account', de: 'Bankkonto hinzufügen' },

  // BOOKING ACCOUNT
  minOneAccountingType: {
    en: 'You need to check minimum 1 accounting type',
    de: 'You need to check minimum 1 accounting type'
  },
  changeForAllMandators: {
    en: 'Do you want to change it for all mandators?',
    de: 'Wollen Sie die Änderung für alle Mandanten durchführen?'
  },

  // BANK ACCOUNT
  viaBic: { en: 'Via Bic', de: 'ViaBIC' },
  viaBank: { en: 'Via Bank', de: 'Über Bank' },
  viaAccount: { en: 'Via Account', de: 'Via Konto' },
  accountNumber: { en: 'Account Number', de: 'Kontonummer' },
  iban: { en: 'IBAN', de: 'IBAN' },
  viaBankCode: { en: 'Via Bank Code', de: 'Via Bankleitzahl' },
  forAccountStatements: { en: 'For Account Statements', de: 'Für Kontoauszüge' },
  forPayments: { en: 'For Payments', de: 'Für Zahlungen' },
  forInvoices: { en: 'For Invoices', de: 'Für Rechnungen' },

  accountStatements: { en: 'Account Statements', de: 'Kontoauszüge' },
  payments: { en: 'Payments', de: 'Zahlungen' },
  invoices: { en: 'Invoices', de: 'Rechnungen' },
  currency: { en: 'Currency', de: 'Währung' },

  // COMPANY CONTACTS
  contactPerson: { en: 'Contact Person', de: 'Contact Person' },
  value: { en: 'Value', de: 'Value' },
  type: { en: 'Type', de: 'Typ' },
  role: { en: 'Role', de: 'Role' },

  // COUNTRIES
  isoNumber: { en: 'ISO Number', de: 'ISO Number' },
  isoAlpha2: { en: 'Iso Alpha 2', de: 'Iso Alpha 2 ' },
  isoAlpha3: { en: 'Iso Alpha 3', de: 'Iso Alpha 3' },
  nationality: { en: 'Nationality', de: 'Nationality' },
  euMember: { en: 'EU Member', de: 'EU Member' },
  sepaParticipant: { en: 'Sepa Participant', de: 'Sepa Participant' },
  currencyIsoCode: { en: 'Currency Iso Code', de: 'Currency Iso Code' },
  bbkShort: { en: 'bbk Short', de: 'bbk Short' },
  bbkNo: { en: 'bbk No', de: 'bbk No' },
  bbkCode: { en: 'bbkCode', de: 'bbk Code' },
  isoNo: { en: 'isoNo', de: 'isoNo' },
  ibanLength: { en: 'Iban Length', de: 'Iban Length' },

  // CURRENCIES
  isoCode: { en: 'ISO code', de: 'ISO-Code' },
  symbol: { en: 'Symbol', de: 'Symbol' },
  lastRateFrom: { en: 'Last rate from', de: 'Letzte Rate von' },

  // MANDATORS
  startFiscalYear: { en: 'Start Fiscal Year', de: 'Start Fiscal Year' },
  accountingType: { en: 'Accounting Type', de: 'Accounting Type' },
  debitorCreditor: { en: 'Debtor / Creditor', de: 'Debitor / Kreditor' },
  debitorCreditorNo: { en: 'Debtor / Creditor No', de: 'Debitor / Kreditor Nr.' },
  start: { en: 'Start', de: 'Start' },
  accountingRelation: { en: 'Accounting Relation', de: 'Buchhalterische Beziehung' },
  category: { en: 'Category', de: 'Kategorie' },
  balanceAccount: { en: 'Balance Account', de: 'Balance Account' },
  profitAndLossAccount: { en: 'Profit and Loss Account', de: 'Profit and Loss Account' },
  profitAndLossEarnings: { en: 'Profit and Loss (Earnings)', de: 'Gewinn- und Verlustrechnung (Ertrag)' },
  profitAndLossExpenses: { en: 'Profit and Loss (Expenses)', de: 'Gewinn- und Verlustrechnung (Aufwand)' },
  showClosedYears: { en: 'Show closed years', de: 'Geschlossene Jahre anzeigen' },

  mustBeLessThanCharacters: {
    en: 'Must be less than {subject} characters',
    de: 'Must be less than {subject} characters'
  },
  // Validation error messages
  voucherDateLessOrEqualCurrentDate: {
    en: 'Voucher Date must be less or equal than the current date.',
    de: 'Voucher Date must be less or equal than the current date.'
  },
  voucherDateGreaterOrEqualFirstBookingYear: {
    en: 'VoucherDate must be greater or equal than the first day of chosen mandator\'s FirstBookingYear',
    de: 'VoucherDate must be greater or equal than the first day of chosen mandator\'s FirstBookingYear'
  },
  invalidIban: { en: 'Invalid IBAN', de: 'Invalid IBAN' },
  invalidBic: { en: 'Invalid BIC', de: 'Invalid BIC' },
  passwordDoNotMatch: { en: 'Passwords do not match', de: 'Passwords do not match' },
  invalidDecimal: { en: 'Invalid decimal (4,4)', de: 'Invalid decimal (4,4)' },
  startDateFirstBookingYear: {
    en: 'Start date should be lower than or equal to first booking year',
    de: 'Start date should be lower than or equal to first booking year'
  },
  mandatorLocalCurrencies: {
    en: 'Mandator localCurrencies differ',
    de: 'Mandator localCurrencies differ'
  },
  mandatorFiscalYear: {
    en: 'Mandator fiscalYears differ',
    de: 'Mandator fiscalYears differ'
  },
  accountIsRequired: { en: 'Account is required', de: 'Account is required' },
  amountLocalIsRequired: { en: 'Local Amount is required', de: 'Local Amount is required' },
  bookingCodeIsRequired: { en: 'BookingCode is required', de: 'BookingCode is required' },
  bookingTextIsRequired: {
    en: 'The Booking text is required',
    de: 'The Booking text is required'
  },
  currencyIsRequired: { en: 'Currency is required', de: 'Currency is required' },
  exchangerateIsRequired: {
    en: 'Exchangerate is required',
    de: 'Exchangerate is required'
  },
  invoiceNumberIsRequired: {
    en: 'The invoice number is required',
    de: 'The invoice number is required'
  },
  paymentDateIsRequired: {
    en: 'Payment date is required',
    de: 'Payment date is required'
  },
  theSignInIsRequired: {
    en: 'The Sign is required',
    de: 'The Sign is required'
  },
  taxKeyIsRequired: {
    en: 'Tax key is required',
    de: 'Tax key is required'
  },
  mandatorIsLocked: {
    en: `
          Mandator can not be used at the moment - please choose another or
          speak to person in charge
        `,
    de: `
          Mandator can not be used at the moment - please choose another or
          speak to person in charge
        `
  },
  yearIsBlockedForMandator: {
    en: '{YEAR} is not allowed - Chosen booking year is blocked for mandator {NUMBER} by {USER} since {TIME}',
    de: '{YEAR} is not allowed - Chosen booking year is blocked for mandator {NUMBER} by {USER} since {TIME}'
  },
  yearIsClosedForMandator: {
    en: '{YEAR} is not allowed - Chosen booking year is closed for mandator {NUMBER}',
    de: '{YEAR} is not allowed - Chosen booking year is closed for mandator {NUMBER}',
  },
  invalidBookingYear: {
    en: 'Booking year must be equal or higher than mandators first booking year {FIRST} and be less or equal to the fiscal calendar year {SECOND}',
    de: 'Booking year must be equal or higher than mandators first booking year {FIRST} and be less or equal to the fiscal calendar year {SECOND}',
  },
  yearIsLaterThanCurrentYear: {
    en: 'Chosen booking year {LATER} is in the future ({YEAR})',
    de: 'Chosen booking year {LATER} is in the future ({YEAR})',
  },
  periodIsBlockedForMandator: {
    en: 'Period {PERIOD} in {YEAR} is not allowed to be booked - Chosen booking period is blocked for mandator {NUMBER} by {USER} since {TIME}',
    de: 'Period {PERIOD} in {YEAR} is not allowed to be booked - Chosen booking period is blocked for mandator {NUMBER} by {USER} since {TIME}'
  },
  periodIsClosedForMandator: {
    en: 'Period {PERIOD} in {YEAR} is not allowed to be booked - Chosen booking period is closed for mandator {NUMBER}',
    de: 'Period {PERIOD} in {YEAR} is not allowed to be booked - Chosen booking period is closed for mandator {NUMBER}',
  },
  mandatorNumberNeedsToBe: {
    en: 'Mandator number needs to be between 1 - 9999',
    de: 'Mandator number needs to be between 1 - 9999'
  },
  noMandatorFound: {
    en: 'No mandator found with entered MandatorNo',
    de: 'No mandator found with entered MandatorNo'
  },
  startMustBeLessThanEnd: {
    en: '{START_LABEL} must be less than {END_LABEL}',
    de: '{START_LABEL} must be less than {END_LABEL}'
  },

  startMustBeLessOrEqualThanEnd: {
    en: '{START_LABEL} must be less or equal than {END_LABEL}',
    de: '{START_LABEL} must be less or equal than {END_LABEL}'
  },
  startDateShouldBeLowerThenFirstBookingYear: {
    en: 'Start date should be lower than or equal to first booking year',
    de: 'Start date should be lower than or equal to first booking year'
  },
  needsToBeDigitsLong: {
    en: 'Needs to be {subject} digits long',
    de: 'Needs to be {subject} digits long'
  },
  needsToBeNumberBetween: {
    en: 'Needs to be a number between {subject1} and {subject2}',
    de: 'Needs to be a number between {subject1} and {subject2}'
  },
  youNeedToCheckAcccountingType: {
    en: 'You need to check at least 1 accounting type',
    de: 'You need to check at least 1 accounting type'
  },
  dateMustHaveTheDDMMYYYYHHmmFormat: {
    en: 'Date must have the DDMMYYYY format',
    de: 'Date must have the DDMMYYYY format'
  },
  dateTimeMustHaveTheDDMMYYYYHHmmFormat: {
    en: 'DateTime must have the DDMMYYYY HH:mm format',
    de: 'DateTime must have the DDMMYYYY HH:mm format'
  },
  numberNeedsToTeBetween: {
    en: 'Number needs to be between 1 - 9999',
    de: 'Number needs to be between 1 - 9999'
  },
  accountIsNotAllowed: {
    en: `Chosen account is not allowed for chosen accounting standard, Please consider
      using another account or speak to a person in charge`,
    de: `Chosen account is not allowed for chosen accounting standard, Please consider
      using another account or speak to a person in charge`
  },

  accountDateFormat: { en: ' Account DateFormat', de: ' Account DateFormat' },
  accountLocale: { en: ' Account Locale', de: ' Account Locale' },
  debitCredit: { en: 'Debit/Credit', de: 'Soll/Haben' },
  DC: { en: 'D/C', de: 'S/H' },
  DEBIT: { en: 'D', de: 'S' },
  CREDIT: { en: 'C', de: 'H' },
  accountLanguage: { en: 'Account Language', de: 'Account Sprache' },
  chooseDateFormat: { en: 'Choose Date Format', de: 'Choose Date Format' },
  chooseDebitCreditFormat: { en: 'Choose Debit Credit Format', de: 'Choose Debit Credit Format' },
  bookingAccount: { en: 'Booking Account', de: 'Buchungskonto' },
  selectAccountingRelationType: { en: 'Select Accounting Relation Type', de: 'Wählen Sie den Rechnungsbezugstyp aus' },
  accountingRelationType: { en: 'Accounting relation type', de: 'Kontozugehörigkeit' },
  selectAccountType: { en: 'Select Account Type', de: 'Wählen Sie den Kontotyp' },
  paymentDateEqualOrAfterInvoiceDate: {
    en: 'Payment date must be equal to or after the invoice',
    de: 'Das Zahlungsdatum muss dem Rechnungsdatum entsprechen oder nach ihm liegen'
  },
  receiptDateAfterInvoiceDate: {
    en: 'Receipt Date must be after the Invoice Date',
    de: 'Eingangsdatum muß nach dem Rechnungsdatum'
  },
  shouldUpdateDiscountFromPaymentDate: {
    en: 'Chosen payment date doesn\'t fit to selected discount, please set the date to be in the range of {START} - {END}',
    de: 'Chosen payment date doesn\'t fit to selected discount, please set the date to be in the range of {START} - {END}'
  },
  paymentDateExceedsMaxPeriod: {
    en: 'Chosen payment date exceeds max payment date of creditor. Do you really want to change to this date?',
    de: 'Chosen payment date exceeds max payment date of creditor. Do you really want to change to this date?'
  },
  systemDefaultTaxKeyCannotBeDeleted: {
    en: 'Cannot delete system default tax key',
    de: 'Der Standardsteuerschlüssel des Systems kann nicht gelöscht werden'
  },
  systemDefaultTaxKeyCannotBeEdited: {
    en: 'Cannot edit system default tax key',
    de: 'Der Standardsteuerschlüssel des Systems kann nicht bearbeitet werden'
  },
  related: { en: 'Related', de: 'Verbunden' },
  customersGroup: { en: 'Customers Group', de: 'Kundengruppe' },
  notValidAccountingType: {
    en: '{SUBJECT} {NUMBER} is not allowed for {NAME}-accounting. Please choose another or speak to a person in charge',
    de: '{SUBJECT} {NUMBER} is not allowed for {NAME}-accounting. Please choose another or speak to a person in charge'
  },
  notActiveMandator: {
    en: 'Mandator isn\'t active',
    de: 'Mandator ist nicht aktiv'
  },
  notActiveAccount: {
    en: 'Account isn\'t active',
    de: 'Konto ist nicht aktiv'
  },
  notActiveDebitorCreditor: {
    en: 'Debtor Creditor isn\'t active',
    de: 'Debitor Creditor ist nicht aktiv'
  },
  notActiveCompany: {
    en: 'Company isn\'t active',
    de: 'Company ist nicht aktiv'
  },
  notActiveVoucherType: {
    en: 'VoucherType isn\'t active',
    de: 'VoucherType ist nicht aktiv'
  },
  notActiveCountry: {
    en: 'Country isn\'t active',
    de: 'Land ist nicht aktiv'
  },
  notActiveCode: {
    en: 'Code isn\'t active',
    de: 'Code ist nicht aktiv'
  },
  notActiveCurrency: {
    en: 'Currency isn\'t active',
    de: 'Währung ist nicht aktiv'
  },
  notFoundMandator: {
    en: 'Mandator doesn\'t exist',
    de: 'Mandator existiert nicht'
  },
  notFoundAccount: {
    en: 'Account doesn\'t exist',
    de: 'Konto existiert nicht'
  },
  notFoundVoucherType: {
    en: 'VoucherType doesn\'t exist',
    de: 'VoucherType existiert nicht'
  },
  notFoundDebitorCreditor: {
    en: 'Debtor Creditor doesn\'t exist',
    de: 'Debitor Creditor existiert nicht'
  },
  notFoundCompany: {
    en: 'Company doesn\'t exist',
    de: 'Company existiert nicht'
  },
  notFoundCountry: {
    en: 'Country doesn\'t exist',
    de: 'Land existiert nicht'
  },
  notFoundCode: {
    en: 'Code doesn\'t exist',
    de: 'Code existiert nicht'
  },
  notFoundCurrency: {
    en: 'Currency doesn\'t exist',
    de: 'Währung existiert nicht'
  },
  mandatorHasNoRoles: {
    en: 'Mandator has no roles',
    de: 'Mandator hat eine Rollen'
  },
  enterRecord: {
    en: 'Enter Record',
    de: 'Buchung erfassen'
  },
  relateNow: {
    en: 'Relate now',
    de: 'Jetzt verknüpfen'
  },
  mandatorTypeParentCompany: { en: 'Parent Company', de: 'Dachgesellschaft' },
  mandatorTypeVesselCompany: { en: 'Vessel Company', de: 'Schiffsgesellschaft' },
  mandatorTypeMultivesselCompany: { en: 'Multivessel Company', de: 'Mehrschiffsgesellschaft' },
  mandatorTypeAdministrativeCompany: { en: 'Administrative Company', de: 'Verwaltungsgesellschaft' },
  voucher: { en: 'Voucher', de: 'Beleg' },
  voucherNo: { en: 'Voucher No', de: 'BelegNr' },
  voucherNo_2: { en: 'Voucher No', de: 'Belegnummer' },
  voucherDate: { en: 'Voucher Date', de: 'Belegdatum' },
  amountTransaction: { en: 'Amount TC', de: 'Betrag TW' },
  amountLocal: { en: 'Amount LC', de: 'Betrag HW' },
  bookingText: { en: 'Booking Text', de: 'Buchtext' },
  period: { en: 'Period', de: 'Periode' },
  invoiceDate: { en: 'Invoice Date', de: 'Rechnungsdatum' },
  invoiceNo: { en: 'Invoice No', de: 'Rechnungs Nr' },
  paymentDate: { en: 'Payment Date', de: 'Zahlungsdatum' },
  bookingDate: { en: 'Booking Date', de: 'Buchdatum' },
  bookingYear: { en: 'Year', de: 'Buchjahr' },
  amountBroughtForward: { en: 'Balance carried forward', de: 'Saldovortrag' },
  oppositeAccount: { en: 'Opposite Account', de: 'Gegenkonto' },
  bookedBy: { en: 'Booked by', de: 'Gebucht von' },
  ct: { en: 'CT', de: 'KA' },
  cc: { en: 'CC', de: 'KST' },
  cu: { en: 'CU', de: 'KT' },
  tax: { en: 'Tax', de: 'Steuersatz' },
  rate: { en: 'Rate', de: 'Kurs' },
  voyage: { en: 'Voyage', de: 'Reise' },
  port: { en: 'Port', de: 'Hafen' },
  loc: { en: 'Loc', de: 'Land' },
  clearingNo: { en: 'Clearing No.', de: 'Zuordgs-Nr.' },
  clearingDate: { en: 'Clearing Date', de: 'Zuordgs Datum' },
  clearingUser: { en: 'Clearing User', de: 'Zuordg Benutzer' },
  clearingType: { en: 'Clearing Type', de: 'Zuordgs Typ' },
  clearingMethod: { en: 'Clearing Method', de: 'Zuordgs Methode' },
  debit: { en: 'Debit', de: 'Soll' },
  credit: { en: 'Credit', de: 'Haben' },
  balanceDebit: { en: 'Balance Debit', de: 'Saldo Soll' },
  balanceCredit: { en: 'Balance Credit', de: 'Saldo Haben' },
  bc: { en: 'BC', de: 'Buchcode' },
  diverse: { en: 'Diverse', de: 'Diverse' },
  discount: { en: 'Discount', de: 'Skonto' },
  amountBalance: { en: 'Amount Balance', de: 'Abstimmsumme' },
  cloneBooking: { en: 'Use Booking as template  ', de: 'Buchung als Vorlage nutzen' },
  showBooking: { en: 'Show Booking', de: 'Buchung anzeigen' },
  isClosed: { en: 'Is closed', de: 'Ist geschlossen' },
  closedPeriod: { en: 'Closed period', de: 'Geschlossene Periode' },
  blockedPeriod: { en: 'Blocked period', de: 'Blockierte Periode' },
  blockedOn: { en: 'Blocked on', de: 'Blockiert am' },
  closedOn: { en: 'Closed on', de: 'Geschlossen am' },
  closedBy: { en: 'Closed by', de: 'Geschlossen von' },
  year: { en: 'Year', de: 'Jahr' },
  periodLessOrEqual: {
    en: 'Period must be less or equal to mandator\'s current period',
    de: 'Periode muss kleiner oder gleich der aktuellen Periode des Mandanten'
  },
  step: { en: 'Step', de: 'Stufe' },
  step1: { en: 'Step 1', de: 'Stufe 1' },
  step2: { en: 'Step 2', de: 'Stufe 2' },
  step3: { en: 'Step 3', de: 'Stufe 3' },
  net: { en: 'Net', de: 'Netto' },

  percentage: { en: 'Percentage', de: 'Prozent' },
  days: { en: 'Days', de: 'Tage' },
  created: { en: 'Created', de: 'Erstellt' },
  edited: { en: ' Edited', de: 'Geändert' },
  remarks: { en: 'Remarks', de: 'Bemerkungen' },
  voucherNoCreated: {
    en: 'The voucher number {NUMBER} was successfully created',
    de: 'Die Belegnummer {NUMBER} wurde erfolgreich erstellt'
  },
  taxType: { en: 'Tax Type', de: 'Steuerart' },
  calculationSettings: { en: 'Calculation Settings', de: 'Berechnung' },
  currentAccount: { en: 'Current Account', de: 'Aktuelles Konto' },
  corporateGroupRelevance: {
    en: 'Corporate/Group Relevance',
    de: 'InterCompany / Konzern relevant'
  },
  corporateAccountingReference: {
    en: 'Corporate Accounting Reference',
    de: 'Referenz Konzernbuchhaltung'
  },
  intercompanyIdentityDebitorCreditor: {
    en: 'Intercompany Identity Debtor/Creditor',
    de: 'Referenz Intercompany Debitor/Kreditor'
  },
  balance: { en: 'Balance', de: 'Saldo' },
  current: { en: 'Current', de: 'Aktuell' },
  new: { en: 'New', de: 'Neu' },
  bookingCodeDeactivationWarning: {
    en: 'Deactivating a booking code will cause in deleting its relations to all mandators.',
    de: 'Das Deaktivieren eines Buchungscodes wird zum Entfernen aller Mandanten-Relationen zu diesem Buchungscode führen.'
  },
  taxKeyDeactivationWarning: {
    en: 'Deactivating a tax key will cause in deleting its relations to all mandators.',
    de: 'Alle Verknüpfungen der Mandanten auf inaktive Steuerschlüssel würden gelöscht werden.'
  },
  bookingCodeDeactivationTitle: {
    en: 'Do you really want to deactivate the booking code?',
    de: 'Wollen Sie den Buchungscode wirklich inaktiv setzen?',
  },
  taxKeyDeactivationTitle: {
    en: 'Do you really want to deactivate the tax key?',
    de: 'Wollen Sie den Steuerschlüssel wirklich deaktivieren?',
  },

  deactivateVoucherTypeTitle: {
    en: 'Do you really want to deactivate the voucher type?',
    de: 'Wollen Sie die Belegart wirklich inaktiv setzen?',
  },
  deactivateVoucherType: {
    en: 'Deactivating a voucher type will cause in deleting its relations to all mandators.',
    de: 'Das Deaktivieren einer Belegart wird zum Entfernen aller Mandanten-Relationen zu dieser Belegart führen.',
  },
  descriptionEn: { en: 'Description EN', de: 'Bezeichnung EN' },
  descriptionDe: { en: 'Description DE', de: 'Bezeichnung DE' },
  description: { en: 'Description', de: 'Bezeichnung' },
  bookingValidation: { en: 'Booking validation', de: 'Buchungsvalidierung' },
  checkValidation: { en: 'Check validations', de: 'Validierungen prüfen' },
  noValidationErrors: { en: 'No errors occurred during the validation process.', de: 'Keine Fehler während des Validierungsprozesses gefunden.' },

  // Open-Item Account Sheets
  openItemAccountSheets : { en: 'Open-Item Account Sheets', de: 'Anzeige OP-Konten' },

  // Invoice Ledger
  purchaseInvoices: { en: 'Purchase Invoices', de: 'Eingangsrechnungen' },
  invoiceLedger: { en: 'Invoice ledger', de: 'Eingangsrechnungen' },
  orderNumber: { en: 'Order number', de: 'Bestellnummer' },
  supplier: { en: 'Supplier', de: 'Lieferant' },
  ledgerPaymentDate: { en: 'Payment Date', de: 'Zahldatum' },
  amount: { en: 'Amount', de: 'Betrag' },
  creditNote: { en: 'Credit note', de: 'Gutschrift' },
  vessel: { en: 'Vessel', de: 'Schiff' },
  ledgerVoyage: { en: 'Voyage', de: 'Reise' },
  barcode: { en: 'Barcode', de: 'Barcode' },
  receiptDate: { en: 'Receipt date', de: 'Eingangsdatum' },
  booked: { en: 'Booked', de: 'Gebucht' },
  invoiceLedgerBookingYear: { en: 'Booking Years', de: 'Buchungsjahr' },
  supplierMissingConditionOfPayment: {
    en: 'Supplier does not have condition of payment',
    de: 'Supplier does not have condition of payment'
  },
  filterName: { en: 'Filter Name', de: 'Filter Name' },
  filterValidity: {
    en: 'Filter valid for all bank accounts of this mandator',
    de: 'Filter valid for all bank accounts of this mandator'
  },
  filterOnlyValidFor: {
    en: 'Filter only valid for',
    de: 'Filter only valid for'
  },
  paymentType: { en: 'Payment Type', de: 'Payment Type' },
  paymentReceipt: { en: 'Payment receipt', de: 'Payment receipt' },
  outgoingPayment: { en: 'Outgoing payment', de: 'Outgoing payment' },
  both: { en: 'Both', de: 'Beides' },
  fieldName: { en: 'Fieldname', de: 'Field Name' },
  operation: { en: 'Operation', de: 'Operation' },
  details: { en: 'Details', de: 'Details' },
  bookingMandator: { en: 'Booking Mandator', de: 'Booking Mandator' },
  costCenter: { en: 'Cost Center', de: 'Cost Center' },
  country: { en: 'Country', de: 'Land' },
  notes: { en: 'Notes', de: 'Notes' },
  mandators: { en: 'Mandators', de: 'Mandanten' },
  selectAllMandators: { en: 'Select all', de: 'Alle Mandanten' },
  selectAllBookingYears: { en: 'Select all', de: 'Alle wählen' },
  clearAll: { en: 'Clear All', de: 'Auswahl entfernen' },
  invoiceLedgerDeleteMessage: {
    en: 'Deleting an invoice needs to be explained. Please enter a reason for deletion',
    de: 'Das Löschen einer Rechnung muss begründet werden, bitte beschreiben sie den Grund'
  },
  invoiceLedgerDeleteFooterMessage: {
    en: 'Deleting an invoice can not be undone!',
    de: 'Das Löschen einer Rechnung kann nicht rückgängig gemacht werden!'
  },
  openItemAccounts: {
    en: 'Open-Item Account Master',
    de: 'OP-Konten Master'
  },
  mandatorAccounts: {
    en: 'Mandator Accounts',
    de: 'Mandator Accounts',
  },
  // master structures
  masterStructures: {
    en: 'Master Structures',
    de: 'Masterstrukturen'
  },
  structures: {
    en: 'Structures',
    de: 'Strukturen'
  },
  node: {
    en: 'Node',
    de: 'Knotenpunkt',
  },
  addNode: {
    en: 'Add Node',
    de: 'Erstellen Knotenpunkt'
  },
  editNode: {
    en: 'Edit Node',
    de: 'Bearbeiten Knotenpunkt'
  },
  deleteNode: {
    en: 'Delete Node',
    de: 'Löschen Knotenpunkt'
  },
  deleteAccount: {
    en: 'Delete Account',
    de: 'Konto löschen'
  },
  externalSystemCode: {
    en: 'External system code',
    de: 'Externer System Code'
  },
  doYouWantToDeleteNode: {
    en: 'Do you really want to delete this node?',
    de: 'Wollen Sie diesen Knotenpunkt wirklich löschen?',
  },
  deleteNodeWithChildren: {
    en: `Do you really want to delete this node and ALL its sub nodes?
      All sub nodes will be deleted as well!`,
    de: `Wollen Sie den Knotenpunkt und alle Unterknotenpunkte wirklich löschen?
      Alle betroffenen Knotenpunkte werden gelöscht!`,
  },
  deleteNodeWithChildrenAndSwitchingNode: {
    en: `Do you really want to delete this node and ALL its sub nodes?
      All sub nodes and switching nodes connections will be deleted as well!`,
    de: `Wollen Sie den Knotenpunkt und alle Unterknotenpunkte wirklich löschen?
      Alle betroffenen Knotenpunkte werden gelöscht!`,
  },
  modalContextDeleteMasterStructure: {
    en: 'Are you sure you want to delete the *{subject}*?',
    de: 'Möchten Sie *{subject}* wirklich löschen ?'
  },
  addAccount: { en: 'Add account', de: 'Konto hinzufügen' },
  accountNameEn: { en: 'Account Name [EN]', de: 'Kontobezeichnung [EN]' },
  accountNameDe: { en: 'Account Name [DE]', de: 'Kontobezeichnung [DE]' },
  structureTitle: { en: 'Structure Title', de: 'Strukturbezeichnung' },
  typeOfStructure: { en: 'Type of Structure', de: 'Strukturtyp' },
  accountingNorm: { en: 'Accounting Norm', de: 'Rechnungslegung' },
  businessManagementEvaluation: { en: 'Business Management Evaluation', de: 'BWA' },
  balanceSheet: { en: 'Balance Sheet', de: 'Bilanz' },
  profitAndLossStatement: { en: 'Profit and Loss Statement ', de: 'GuV' },
  openTree: { en: 'Open tree-view ', de: 'Struktur aufklappen' },
  closeTree: { en: 'Close tree-view ', de: 'Struktur zuklappen' },
  structureTypeCannotBeChanged: {
    en: 'Type cannot be changed! Accounts are assigned which do not belong to the selected type.',
    de: 'Typ kann nicht geändert werden. Der Struktur sind Konten zugewiesen, die nicht dem ausgewählten neuen Typen entsprechen.'
  },
  structureAccountNormCannotBeChanged: {
    en: `Accounting Norm cannot be changed! Accounts are assigned which
      do not belong to the selected norm.`,
    de: `Rechnungslegungsstandard kann nicht geändert werden. Der Struktur sind Konten
      zugewiesen, die nicht dem ausgewählten neuen Standard entsprechen.`
  },
  theAccountAssignmentIsIncomplete: {
    en: 'The account assignment is incomplete! Not all required accounts have been assigned yet.',
    de: 'Die Kontozuweisung ist unvollständig! Es wurden noch nicht alle erforderlichen Konten zugewiesen.',
  },
  theStructureIsIncomplete: {
    en: 'The structure is incomplete! Not all required accounts have been assigned yet. ',
    de: 'Die Struktur ist unvollständig! Es wurden noch nicht alle erforderlichen Konten zugewiesen. ',
  },
  deletingAnAccountWillSetStructureToInactive: {
    en: `This structure is set to active. Deleting an account results in the structure
      being set to inactive. Do you want to continue?`,
    de: `Diese Struktur ist auf aktiv gesetzt. Das Löschen eines Kontos führt dazu, dass
      die Struktur auf inaktiv gesetzt wird. Wollen Sie fortfahren?`,
  },
  accountSelection: { en: 'Account Selection', de: 'Kontoauswahl' },
  downloadWithAssignedAccounts: {
    en: 'Download with assigned accounts?',
    de: 'Struktur inklusive Konten exportieren?'
  },
  structureIsUnderDevelopment: {
    en: 'This structure is still under development. Do you want to continue?',
    de: 'Diese Struktur wird noch entwickelt. Wollen Sie fortfahren?'
  },
  accountPermissions: {
    en: 'Account Permissions',
    de: 'Kontoberechtigungen',
  },
  confirmSaveWithoutPermission: {
    en: 'No permissions were assigned. Do you want to continue?',
    de: 'Es wurden keine Berechtigungen zugeteilt. Möchten Sie fortfahren?',
  },
  summationOf: {
    en: 'Summation of …',
    de: 'Summe bilden aus …'
  },
  assignedNodes: {
    en: 'Assigned Nodes',
    de: 'Zugeordnete Knoten',
  },
  unassignedNodes: {
    en: 'Unassigned Nodes',
    de: 'Nicht Zugeordnete Knoten',
  },
  highlightNode: {
    en: 'Highlight node',
    de: 'Knoten hervorheben',
  },
  expenseItem: {
    en: 'Expense Item',
    de: 'Aufwandsposition'
  },
  resultCurrentYear: {
    en: 'Result cur. year',
    de: 'Ergebnis lfd. Jahr'
  },
  resultPrevYear: {
    en: 'Result prev. year',
    de: 'Ergebnis Vorjahr'
  },
  noNodesWereAssigned: {
    en: 'No nodes were assigned to the summation.',
    de: 'Der Summe wurden keine Knoten zugewiesen.'
  },
  nodeUsedInSummationCannotBeDeleted: {
    en: `The node cannot be deleted. It is already used in a summation. Please remove
      the affected node from the summation or delete the summation itself to continue.`,
    de: `Der Knoten kann nicht gelöscht werden. Er wird bereits in einer Summe verwendet.
      Entfernen Sie erst den betroffenen Knoten aus der Summe oder löschen Sie
      die Summe selbst, um fortzufahren.`
  },
  severalFunctionsWereUsedForStructureType: {
    en: `In this structure, several functions were used that can only be used in the
      current structure type. Please remove all used functions first before you change
      the Type of Structure.`,
    de: `In dieser Struktur wurden verschiedene Funktionen verwendet, die nur in dem
      aktuellen Strukturtyp angewendet werden können. Bitte entfernen Sie erst alle
      verwendeten Funktionen, bevor Sie den Strukturtyp ändern.`
  },
  mandatorHasNoParentOrVesselType: {
    en: 'Mandator type isn\'t parent company or vessel company',
    de: 'Mandator type isn\'t parent company or vessel company',
  },
  copyInBookingForm: {
    en: 'Copy in booking form',
    de: 'Kopieren in die Buchungsmaske'
  },
  bookingInvoiceMode: {
    en: 'Booking invoice mode',
    de: 'Eingangsrechnungsmodus',
  },
  bookingFormDisabledFieldsWarning: {
    en: 'If you need to change any disabled value, you need to cancel this booking and change the related entry in invoice ledger.',
    de: 'Um einen deaktivierten Wert zu ändern, müssen Sie die Buchung abbrechen und zunächst den Wert der zu buchenden Rechnung in der Eingangsrechnung anpassen.',
  },
  accountNotYetRelated: {
    en: 'Account *{subject}* is not yet related to chosen mandator, create relation?',
    de: 'Konto *{subject}* ist noch nicht mit dem ausgewählten Mandanten verbunden, eine Beziehung erstellen?'
  },
  accountWillBeRemovedFromMasterStructures: {
    en: `Attention. The account will be removed from master structures where the assigned
      accounting is no longer allowed after your changes.`,
    de: `Achtung. Das Konto wird aus Masterstrukturen entfernt, bei denen die zugewiesene
      Rechnungslegung nach Ihren Änderungen nicht mehr zulässig ist.`
  },
  accountsHaveBeenRemoved: {
    en: `Attention. One or more accounts have been automatically removed from the
      structure. Their changed accounting norm no longer corresponds to this structure.`,
    de: `Achtung. Ein oder mehrere Konten wurden automatisch aus der Struktur entfernt.
      Ihre geänderte Rechnungslegung entspricht nicht mehr dieser Struktur.`,
  },
  nodeType: { en: 'Node Type', de: 'Knotentyp' },
  earningsItem: { en: 'Earnings item', de: 'Ertragsposition' },
  accountTypeDoesNotMatchNodeType: {
    en: 'The account type does not match with the node type. The account cannot be added to this node.',
    de: 'Der Kontentyp entspricht nicht dem Knotentyp. Das Konto kann diesem Knoten nicht hinzugefügt werden.'
  },
  nodesTypesDoNotMatch: {
    en: 'The node types are different. The node can therefore not be added.',
    de: 'Die Knotentypen unterscheiden sich. Der Knoten kann daher nicht hinzugefügt werden.'
  },
  permissionGroups: { en: 'Permission Groups', de: 'Permission Groups' },
  areYourSureToDeletePermissionGroup: {
    en: 'Are you sure you want to delete the *{subject}* permission group?',
    de: 'Wollen Sie die Berechtigungsgruppe *{subject}* wirklich löschen?',
  },
  permissionGroupName: { en: 'Permission group name', de: 'Name der Berechtigungsgruppe' },
  switchingAccount: { en: 'Switching Account', de: 'Wechselkonto' },
  switchingNode: { en: 'Switching Node', de: 'Wechselknoten' },
  nameOfSwitchingNode: {
    en: 'Name  of Switching Node',
    de: 'Bezeichnung des Wechselknotens'
  },
  selectionSwitchingNodes: {
    en: 'Selection Switching Nodes',
    de: 'Auswahl Wechselkonten'
  },
  externalSystemCodeSwitchingNode: {
    en: 'External System Code Switching Node',
    de: 'Externer System Code des Wechselknotens'
  },
  switchingNodeNameEn: {
    en: 'Switching Node Name [EN]',
    de: 'Wechselkonto Name [EN]'
  },
  switchingNodeNameDe: {
    en: 'Switching Node Name [DE]',
    de: 'Wechselkonto Name [DE]'
  },
  changingExternalCodeWillOverwrite: {
    en: `Changing the external system code in this field will overwrite the external
      system code of this node in the corresponding settings. Do you want to continue?`,
    de: `Das Ändern des externen System Codes in diesem Feld führt dazu, dass der Externe
      System Code dieses Knotenpunktes in den dazugehörigen Settings überschrieben wird.
      Wollen Sie fortfahren?`
  },
  afterDeleteNoSwitchingNodeAssigned: {
    en: `This node is linked to *{SUBJECT}* within a switching situation.
      Deleting this node means that no switching node is assigned to *{SUBJECT}* anymore.
      Do you want to continue?`,
    de: `Dieser Knoten ist mit *{SUBJECT}* innerhalb einer Wechselsituation verbunden.
      Das Löschen dieses Knotens hat zur Folge, dass *{SUBJECT}* kein Wechselknoten mehr
      zugewiesen ist. Wollen Sie fortfahren?`
  },
  selectedSwitchingNodeIsUnavailable: {
    en: `This node already exists and is linked to another node. Please enter a new node
      name or a name of a node haven't been linked to another switching node yet.`,
    de: `Dieser Knoten existiert bereits und ist mit einem anderen Knoten innerhalb einer
      Wechselsituation verbunden. Bitte geben Sie einen in dieser Struktur noch nicht
      vorhandenen Namen ein oder einen bereits vorhandenen Knoten, der noch nicht mit
      einem anderen Knoten verbunden ist.`
  },
  permissionGroupRequirementsMessage: {
    en: 'To save this group at least one mandator and one user must be related.',
    de: 'Speichern nicht möglich. Bitte fügen Sie mindestens einen Mandanten und einen User zur Gruppe hinzu.',
  },
  saveAndDeleteFromAutobank: {
    en: 'Save and Delete from Autobank',
    de: 'Änderungen speichern und Mandanten für Autobank sperren',
  },
  saveAndDeleteFromPaymentsPermissions: {
    en: 'Save and Delete from Payments permissions',
    de: 'Änderungen speichern und Mandanten für ZV sperren',
  },
  mandatorWorkflowRemoveAutobankPermissionWarning: {
    en: 'You turned off autobank workflow. This Mandator will be deleted from all autobank permission groups. Do you really want to save your changes and remove this mandator from all Autobank functions?',
    de: 'Sie haben den Autobank Workflow deaktiviert. Der Mandant wird aus allen Autobank Berechtigungsgruppen entfernt. Wollen Sie Ihre Änderungen speichern und den Mandanten für die Autobank entfernen?',
  },
  mandatorPaymentTransferCheckboxWarningWarning: {
    en: 'You turned off the Payments feature. This Mandator will be deleted from all Payments permission groups. Do you really want to save your changes and remove this mandator from all Payments functions?',
    de: 'Sie haben den Zahlungsverkehr deaktiviert. Der Mandant wird aus allen ZV-Berechtigungsgruppen entfernt. Wollen Sie Ihre Änderungen speichern und den Mandanten für den ZV ausschliessen?',
  },
  theNodeIsAlreadyLinked: {
    en: `This node is already linked to *{SUBJECT}*. Deactivating the checkbox will cause
      the link to be broken and both nodes will no longer function as switching nodes.
      Do you want to continue?`,
    de: `Der Knoten ist bereits mit *{SUBJECT}* verlinkt. Das Deaktivieren der Checkbox
      führt dazu, dass die Verlinkung aufgelöst wird und beide Knoten nicht mehr als
      Wechselknoten fungieren. Wollen Sie fortfahren?
    `
  },
  selectionContainsSwitchingAccounts: {
    en: `The selection contains one or more Switching Accounts. The affected accounts are
      automatically assigned to the corresponding Switching Node *{NAME}*.`,
    de: `Die Auswahl beinhaltet ein oder mehrere Wechselkonten. Die betroffenen Konten
      werden dem zugehörenden Wechselknoten *{NAME}* automatisch zugeordnet.`,
  },
  deleteSwitchingAccount: {
    en: `This account is a Swtiching Account. Deleting this account causes the account
      to also be deleted in the associated Switching Node *{NAME}*.Do you want to continue?`,
    de: `Dieses Konto ist ein Wechselkonto. Das Löschen dieses Konto führt dazu, dass das
      Konto ebenfalls im zugehörenden Wechselknoten *{NAME}* gelöscht wird.
      Wollen Sie fortfahren?`
  },
  deleteSwitchingNodeWithSwitchingAccount: {
    en: `One or more Switching Accounts have already been assigned to this Switching Node.
      Deleting this Switching Node will also remove the Switching Account from node
      *{NAME}*. Do you want to continue?`,
    de: `Diesem Wechselknoten wurden bereits ein oder mehrere Wechselkonten zugewiesen.
      Das Löschen des Wechselknotens führt dazu, dass das Wechselkonto ebenfalls aus
      Wechselknoten *{NAME}* entfernt wird. Wollen Sie fortfahren?`
  },
  accountCannotBeAddedToThisNode: {
    en: `The account type does not match with the node type. The account
      cannot be added to this node.`,
    de: `Der Kontentyp entspricht nicht dem Knotentyp. Das Konto kann diesem Knoten
      nicht hinzugefügt werden.`
  },
  switchingNodeCanNotBeChanged: {
    en: `The assigned Switching Node cannot be changed because Switching Accounts have
      already been assigned. Please remove the Switching Accounts first to continue.`,
    de: `Der zugewiesene Wechselknoten kann nicht geändert werden, da bereits
      Wechselkonten zugewiesen wurden. Bitte entfernen Sie erst die Wechselkonten,
      um fortzufahren. `,
  },
  showAccountSheet: {
    en: 'Show account sheet',
    de: 'Kontoblatt anzeigen'
  },
  settings: { en: 'Settings', de: 'Einstellungen' },
  businessCases: { en: 'Business cases', de: 'Geschäftsvorfälle' },
  businessCase: { en: 'Business case', de: 'Geschäftsvorfall' },
  bookingCodeBankAccountBookings: {
    en: 'Booking Code bank account bookings',
    de: 'Vorgang Bankbuchungen'
  },
  bookingCodeForeignIncomingPayments: {
    en: 'Booking Code foreign incoming payments',
    de: 'Vorgang Bankbuchungen Zahlungseingang Ausland'
  },
  matchingAccountIncomingPayments: {
    en: 'Matching-account incoming Payments',
    de: 'Abstimmkonto Eingang'
  },
  matchingAccountOutgoingPayments: {
    en: 'Matching-account outgoing Payments',
    de: 'Abstimmkonto Ausgang'
  },
  bankFeesAccount: { en: 'Bank fees account', de: 'Gebührenkonto Ausgang' },
  exchangeRateProfitAccount: {
    en: 'Exchange rate profit account',
    de: 'Konto Kursgewinn'
  },
  exchangeRateLossAccount: { en: 'Exchange rate loss account', de: 'Konto Kursverlust' },
  pathMT950Files: { en: 'Path MT940-files', de: 'Verzeichnis MT940-Dateien' },
  alternativeAccountExchangeRate: {
    en: 'Alternative account for exchange rate differences incoming payments',
    de: 'Abweichendes Konto Kursdifferenz bei Zahlungseingängen'
  },
  mandatorNotAutobankWorkflow: {
    en: 'Mandator is not accepted for autobank workflow',
    de: 'Das Mandat wird für den Autobank-Workflow nicht akzeptiert'
  },
  forAccountStatementsWarning: {
    en: 'Autobank Rules for this bank account will be deleted if you remove the Autobank relation, do you want to deactivate the bank account for Autobank and delete all explicit connected filters?',
    de: 'Sie haben dieses Bankkonto für den Autobank workflow deaktiviert, dadurch werden alle explizit zugewiesenen Autobank Regeln für dieses Bankkonto gelöscht. Möchten Sie fortfahren?'
  },
  insurance: { en: 'Insurance', de: 'Versicherung' },
  initialSupply: { en: 'Initial supply', de: 'Erstausrüstung' },
  personalCosts: { en: 'Personel costs (Seafarer)', de: 'Personalkosten (Seefahrer)' },
  maintenance: { en: 'Maintenance', de: 'Instandhaltung' },
  otherCosts: { en: 'Other ship operation costs', de: 'Sonstige Schiffsbetriebskosten' },
  ctName: { en: 'Cost type name', de: 'Kostenartbezeichnung' },
  opexVesselOperation: { en: 'Opex (Vessel operation)', de: 'Opex (Schiffsbetrieb)' },
  opexType: { en: 'OPEX Type', de: 'OPEX Typ' },
  earnings: { en: 'Earnings', de: 'Erlöse' },
  costs: { en: 'Costs', de: 'Kosten' },
  opexTypeMustBeSelected: {
    en: 'Opex type must be selected',
    de: 'Opex type must be selected'
  },
  costTypeRelationWarning: {
    en: 'If you deactivate this account for cost accounting, all related cost types will be deleted.',
    de: 'Wenn Sie dieses Konto von der KLR ausschließen werden alle Verknüpfungen zu Kostenarten gelöscht'
  },
  atLeastOneCostTypeMustBeSelected: {
    en: 'Account is active for cost accounting, please relate at least one cost type for this account',
    de: 'Das Konto ist für die KLR aktiviert, bitte weisen Sie mindestens eine Kostenart zu'
  },
  costType: { en: 'Cost types', de: 'Kostenarten' },
  ctDeactivationNotPossible: {
    en: 'Deactivation not possible due to active relations to chart of accounts',
    de: 'Deaktivieren nicht möglich. Es liegen aktive Verknüpfungen zum Masterkontenrahmen vor'
  },
  ctDeletionNotPossible: {
    en: 'Deletion not possible due to active relations to chart of accounts',
    de: 'Löschen nicht möglich. Es liegen aktive Verknüpfungen zum Masterkontenrahmen vor.'
  },
  costCenters: { en: 'Cost centers', de: 'Kostenstellen' },
  mainCostCenter: { en: 'Main cost center',  de: 'Hauptkostenstelle' },
  preCostCenter: { en: 'Pre-cost center',  de: 'Vorkostenstelle' },
  costCenterName: { en: 'Cost Center Name', de: 'Kostenstellenbezeichnung' },
  costUnits: { en: 'Cost units', de: 'Kostenträger' },
  costUnitCategory: { en: 'Cost unit category', de: 'Kostenträger Kategorie' },
  costUnitCategories: { en: 'Cost unit categories ', de: 'Kostenträgerarten' },
  costUnitName: { en: 'Cost Unit Name', de: 'Kostenträgerbezeichnung' },
  costUnitBookedAndCannotBeDeleted: {
    en: 'Cost unit has been booked and cannot be deleted',
    de: 'Der Kostenträger wurde gebucht und kann daher nicht mehr gelöscht werden',
  },
  deletionNotPossibleRelatedToCostUnits: {
    en: 'Deletion not possible due to active relations to cost units.',
    de: 'Löschen nicht möglich. Es liegen aktive Verknüpfungen zum Kostenträger vor.'
  },
  historyTab: { en: 'History', de: 'Verlauf' },
  metaTab: { en: 'Meta', de: 'Meta' },
  debtorsCreditors: { en: 'Debtors & Creditors', de: 'Debitoren & Kreditoren' },
  accountingNorm_v2: { en: 'Accounting norm', de: 'Rechnungslegungsnorm' },
  takeInvoice: { en: 'Take invoice', de: 'Rechnung ziehen' },
  selectIncomingInvoice: { en: 'Select incoming invoice', de: 'Eingangsrechnung auswählen' },
  selectVoucherNumber: { en: 'Select voucher number', de: 'Belegnummer auswählen' },
  barcodeNotFound: {
    en: 'No invoice with barcode {} could be found or invoice has already been booked.',
    de: 'Es konnte keine Rechnung mit dem Barcode {} gefunden werden oder die Rechnung wurde bereits verbucht.'
  },
  accruedAccountDeferredIncome: {
    en: 'The selected account is not defined as an accrual account for deferred income',
    de: 'Das ausgewählte Konto ist nicht als PRAP-Konto definiert',
  },
  accruedAccountPrepaidExpenses: {
    en: 'The selected account is not defined as an accrual account for prepaid expenses',
    de: 'Das ausgewählte Konto ist nicht als ARAP-Konto definiert',
  },
  atLeastOnePLAccountMustBeChecked: {
    en: 'At least one P&L account needs to be checked for accrual accounting.',
    de: 'Mindestens ein Erlös- oder Ertragskonto muss zur Abgrenzung definiert sein.'
  },
  requestIsGoingToChangeDefaultAccountPrepaidExpenses: {
    en: 'Do you really want to replace {DEFAULTACCOUNT} with {NEWACCOUNT} as default account prepaid expenses?',
    de: 'Möchten Sie {DEFAULTACCOUNT} durch {NEWACCOUNT} als Default-Konto ARAP ersetzen?'
  },
  requestIsGoingToChangeDefaultAccountDeferredIncome: {
    en: 'Do you really want to replace {DEFAULTACCOUNT} with {NEWACCOUNT} as default account deferred income?',
    de: 'Möchten Sie {DEFAULTACCOUNT} durch {NEWACCOUNT} als Default-Konto PRAP ersetzen?',
  },
  notMoreThanOneGVCFilterOptionIsAllowed: {
    en: 'Only one GVC filter option is allowed per autobank rule',
    de: 'Nur eine GVC filter option ist erlaubt pro Autobank Regel'
  }
};
