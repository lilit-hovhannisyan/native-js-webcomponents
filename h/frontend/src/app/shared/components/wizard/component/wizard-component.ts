import {
  Component,
  AfterContentInit,
  OnInit,
  OnDestroy,
  ContentChildren,
  QueryList,
  Output,
  EventEmitter
} from '@angular/core';
import { WizardStep } from '../WizardStep.abstract';
import { WizardService } from '../../../services/wizard.service';
import { Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';

// This is how the Wizard can be used
//
// <wizard (complete)="stepsCompleted($event)">
//   <nv-form1 [stepId]="1" title='form You Title'></nv-form1>
//   <nv-form2 [stepId]="2" title='form Me Title'></nv-form2>
//   <nv-form3 [stepId]="3" title='form Him Title'></nv-form3>
// </wizard>
//
// Every Step component needs to extend WizardStep and assign its formGroup to the
// 'form' property of the WizardStep. Additionally WizardService needs to be injected
// and passed to the parent 'WizardStep' by calling super(wizardService)

/**
 * If displaying the wizard in a Modal:
 * every component that gets rendered in a modal by a portal needs to be listed in
 * 'entryComponents' of the app.module. This goes also for Wizard-Step-Components.
 * https://angular.io/guide/entry-components#the-entrycomponents-array
 */

/**
 * This component is used for defining steps and control how each step works.
 * You can pass accounting as step via ng-content, also
 * you can enable/disable navigation button based on validity of the current step
 */
@Component({
  selector: 'nv-wizard',
  templateUrl: './wizard-component.html',
  styleUrls: ['./wizard-component.scss']
})
export class WizardComponent implements OnInit, AfterContentInit, OnDestroy {
  @ContentChildren(WizardStep) public wizardSteps: QueryList<WizardStep>;

  @Output() public complete = new EventEmitter<{}>();

  public sub: Subscription;
  public stepValid: boolean;
  public currentStep: number;
  public steps: WizardStep[];
  public currentTitle: string;
  public stepIsValid: boolean;
  public isLastStep: boolean;
  public isFirstStep: boolean;

  constructor(private wizardService: WizardService) { }

  public ngOnInit(): void {
    this.wizardService.stepIsValid$.pipe(delay(0)).subscribe(isValid => this.stepIsValid = isValid);
  }

  public ngAfterContentInit(): void {
    this.steps = this.wizardSteps.toArray();



    /**
     * asking everey WizardStep component to initialize.
     * They use __init instead of ngOnInit because ngOnInit
     * can easily be overridden by the extending wizard-step-class
     */
    this.steps.forEach((step: WizardStep) => step._init());

    this.sub = this.wizardService.currentStep$.subscribe(step => {
      this.isFirstStep = step === 1;
      this.isLastStep = step === this.steps.length;
      const previousStep = this.currentStep;
      previousStep && this.getStepById(previousStep).turnInactive();

      this.currentStep = step;
      this.updateTitle(step);
      this.getStepById(step).turnActive();
    });
  }

  /**
   * Goes to the previous step by saving current step state
   */
  public stepBack(): void {
    this.saveFormValue();
    this.wizardService.setCurrentStep(this.currentStep - 1);
  }

  /**
   * Goes to the next step by saving current step state
   */
  public stepForward(): void {
    this.saveFormValue();
    this.isLastStep
      ? this.submit()
      : this.wizardService.setCurrentStep(this.currentStep + 1);
  }

  /**
   * Saves current step's form state so it will not lost after changing step
   */
  public saveFormValue(): void {
    const formValue = this.getStepById(this.currentStep).form.value;
    this.wizardService.saveToState(formValue);
  }

  /**
   * Updates step's title on step change
   */
  public updateTitle(stepId: number): void {
    this.currentTitle = this.getStepById(stepId).title;
  }

  /**
   * Gets step by id from steps array, to show current step data or save it's state
   */
  public getStepById(id: number): WizardStep {
    return (this.steps.find(s => s.stepId === id) as WizardStep);
  }

  /**
   * This emits when in the last step clicked on DONE button
   */
  public submit(): void {
    this.complete.emit(this.flattenValues(this.wizardService.getState()));
  }

  public flattenValues(formValues: any): any {
    // before flattening values have a structure like:
    // { 1: formValue1, 2: formValue2, 3: formValue3 }
    // after flattening: { ...formValue1, ....formValue2, ...formValue3 }
    return Object.values(formValues).reduce((acc: object, val: object) => ({ ...acc, ...val }) , {});
  }

  public ngOnDestroy(): void {
    this.sub.unsubscribe;
  }
}

