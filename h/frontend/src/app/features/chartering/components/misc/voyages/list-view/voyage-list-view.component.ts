import { sitemap } from './../../../../../../core/constants/sitemap/sitemap';
import { ActivatedRoute, Router } from '@angular/router';
import { VoyagesRepositoryService } from '../../../../../../core/services/repositories/voyages-repository.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { IVoyageRow } from 'src/app/core/models/resources/IVoyage';
import { VoyagesMainService } from 'src/app/core/services/mainServices/voyages-main.service';
import { map, switchMap, filter } from 'rxjs/operators';
import { VesselAccountingKey } from 'src/app/core/models/resources/IVesselAccounting';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/voyages.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { TranslatePipe } from 'src/app/shared/pipes/translate.pipe';
import { SettingsService } from 'src/app/core/services/settings.service';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';

@Component({
  selector: 'app-voyage-list',
  templateUrl: './voyage-list-view.component.html',
  styleUrls: ['./voyage-list-view.component.scss']
})
export class VoyageListViewComponent implements OnInit {
  public dataSource$: Observable<IVoyageRow[]>;
  public gridConfig: NvGridConfig;
  public vesselAccountingId: number;
  private translatePipe: TranslatePipe;
  constructor(
    private voyagesMainService: VoyagesMainService,
    private voyagesRepositoryService: VoyagesRepositoryService,
    private confirmationService: ConfirmationService,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private router: Router,
    private settingsService: SettingsService,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
  ) { }

  public ngOnInit() {
    this.setGridConfig();
    this.translatePipe = new TranslatePipe(this.settingsService);

    this.dataSource$ = this.route.queryParams.pipe(
      filter(params => params['vesselAccountingId']),
      switchMap(params => {
        this.vesselAccountingId = +params['vesselAccountingId'];
        this.voyagesRepositoryService.fetchAll({}).subscribe();
        return this.vesselAccountingRepo.fetchOne(this.vesselAccountingId, {})
          .pipe(
            switchMap(vesselAccounting => {
              return this.vesselAccountingRepo
                .fetchAll({ queryParams: { vesselId: vesselAccounting.vesselId } });
            })
          ).pipe(
            switchMap(vesselAccountings => {
              return this.voyagesRepositoryService.getStream({}).pipe(
                map(voyages => {
                  const vesselAccountingIds: VesselAccountingKey[] = vesselAccountings.map(va => va.id);
                  return voyages.filter(v => vesselAccountingIds.includes(v.vesselAccountingId));
                }),
                map(voyages => this.voyagesMainService.toRows(voyages))
              );
            }),
          );
      })
    );
  }

  private setGridConfig() {
    const _url = url(sitemap.chartering.children.misc.children.voyages);
    const actions = {
      edit: (row: IVoyageRow) => this.router.navigate([_url, row.id], { queryParamsHandling: 'preserve' }),
      add: () => this.router.navigate([_url, 'new'], { queryParamsHandling: 'preserve' }),
      delete: (rowData: IVoyageRow) => this.deleteRowClicked(rowData),
    };

    this.gridConfig = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  private deleteRowClicked = (voyage: IVoyageRow): void => {
    const voyageNameAndYear = `${this.translatePipe.transform(wording.chartering.voyage)} ${voyage.yearAndNo}`;

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: voyageNameAndYear },
    }).subscribe(confirmed => confirmed && this.voyagesRepositoryService.delete(voyage.id, {}).subscribe());
  }
}
