import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { ICompany, CompanyKey } from 'src/app/core/models/resources/ICompany';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { wording } from 'src/app/core/constants/wording/wording';
import { Validators } from 'src/app/core/classes/validators';
import { ICompanyPartnerContribution, typePartnerOptions, PartnerType } from 'src/app/core/models/resources/ICompanyPartnerContribution';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { Observable } from 'rxjs';
import { map, distinctUntilChanged, startWith, take } from 'rxjs/operators';
import { IWording } from 'src/app/core/models/resources/IWording';
import { COMPANY_LEGAL_FORM, contributionGridCompanyName, contributionGridContributionAmount, getContributionWordingKey, CAPITAL_TYPE, contributionChangeAmountLabels } from 'src/app/core/models/enums/company-legal-form';
import { ICompanyLegalEntity } from 'src/app/core/models/resources/ICompanyLegalEntity';
import { ContributionMainService } from 'src/app/core/services/mainServices/company-contributions-main.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import * as moment from 'moment';

@Component({
  templateUrl: './contribution-edit-modal.component.html',
  styleUrls: ['./contribution-edit-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContributionEditModalComponent implements OnInit {
  @Input() public contribution: ICompanyPartnerContribution;
  @Input() public company: ICompany;
  @Input() public legalEntity: ICompanyLegalEntity;
  @Input() public currency: ICurrency;
  @Input() public currencyEuro: ICurrency;
  @Input() public partnersTotalContributionsMap: Map<CompanyKey, ICompanyPartnerContribution>;
  @Input() public allContributions: ICompanyPartnerContribution[];
  private otherContributionsMap: Map<CompanyKey, ICompanyPartnerContribution>;

  public showCurrency = true;
  public companyNameWording: IWording;
  public contributionAmountWording: IWording;
  public isManagingPartnersVisible: boolean;

  public form: FormGroup;
  public wording = wording;
  public typePartnerOptions: Map<number, IWording> = typePartnerOptions;
  public companyLegalFormTypes = COMPANY_LEGAL_FORM;
  public companiesObservable: Observable<ICompany[]>;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private companyRepo: CompanyRepositoryService,
    private contributionMainService: ContributionMainService,
    private settingsService: SettingsService,
  ) { }

  public ngOnInit() {
    this.showCurrency = this.legalEntity.capitalType === CAPITAL_TYPE.ParValueShare;
    this.companyNameWording = contributionGridCompanyName.get(this.company.legalForm);
    this.contributionAmountWording = contributionChangeAmountLabels.get(this.company.legalForm);
    this.setOtherContributions();

    this.setCompaniesGetStream()
      .pipe(take(1))
      .subscribe(() => this.init());
  }

  private init(): void {
    this.form = this.fb.group({
      from: [
        this.contribution.from,
        [Validators.required], // Other validators will be added dynamically
      ],
      partnerId: [this.contribution.partnerId, [Validators.required]],
      companyId: [this.company.id, [Validators.required]],
      contributionAmount: [
        this.contribution.contributionAmount,
        [Validators.required, Validators.maxDecimalDigits(2)], // Other validators will be added dynamically
      ],
      currency: [{ value: this.currency.isoCode, disabled: true }],
      liabilitySum: [
        {
          value: this.contribution.liabilitySum,
          disabled: this.contribution.partnerType === PartnerType.General,
        },
        // all necessary Validators will be added dynamically
      ],
      liabilityCurrency: [{ value: this.currencyEuro.isoCode, disabled: true }],
      partnerType: [this.contribution.partnerType || PartnerType.None, [Validators.required]],
      remark: [this.contribution.remark],
      managingPartner: [{ value: this.legalEntity.partners.includes(this.contribution.partnerId) }],
    });

    this.partnerTypeChangeHandler();
    this.form.get('partnerType').disable();

    this.isManagingPartnersVisible
      = this.company.legalForm === COMPANY_LEGAL_FORM.CivilLawPartnership;

    if (this.isManagingPartnersVisible) {
      this.form.controls.managingPartner.patchValue(
        this.legalEntity.partners.includes(this.contribution.partnerId) || null
      );

    }
  }

  private getContributionAmountValidators(): ValidatorFn[] {
    let validators: ValidatorFn[] = [
      Validators.required,
      Validators.maxDecimalDigits(2),
      Validators.min((this.getMinAmount()).toFixed(2)),
    ];

    if (!this.contribution.contributionIsZero) {
      validators = [
        ...validators,
        Validators.valueShouldNotBeEqualToFloat(0),
      ];
    }
    return validators;
  }

  private getMinAmount(): number {
    /**
     * To access form value(getRawValue) used optional chaining, because this method can be
     * called on time of initialization of the form when it doesn't exist yet.
     * Used `getRawValue().partnerId` because `form.value.partnerId` is being changed later.
     */
    const partnerId = this.form?.getRawValue().partnerId;
    const contribution: ICompanyPartnerContribution = this.partnersTotalContributionsMap.get(
      partnerId || this.contribution.partnerId
    );

    const amount = contribution?.contributionAmount || 0;
    // needed to exclude the amount of the current contribution
    const minAmount = partnerId === this.contribution.partnerId
      ? amount - this.contribution.contributionAmount
      : amount;
    return minAmount <= 0 ? 0 : -minAmount;
  }

  private setOtherContributions(): void {
    const otherContributions: ICompanyPartnerContribution[]
      = this.allContributions.filter(c => c.id !== this.contribution.id);
    this.otherContributionsMap = this.contributionMainService
      .getCompaniesLatestContributions(otherContributions);
  }

  private setMinDate(): void {
    const { partnerId, from } = this.form.controls;
    const latestContribution: ICompanyPartnerContribution = this.otherContributionsMap.get(partnerId.value);

    const minDate: string = latestContribution?.from
      && moment(latestContribution.from).startOf('day').utc().toISOString();

    const minDateValidatorArr: ValidatorFn[] = minDate
      ? [Validators.dateShouldBeAfter(minDate, this.settingsService.locale, true)]
      : [];

    from.setValidators([
      Validators.required,
      ...minDateValidatorArr,
    ]);
    from.updateValueAndValidity();
  }

  private partnerTypeChangeHandler(): void {
    const { partnerId, partnerType, liabilitySum, contributionAmount } = this.form.controls;

    partnerId.valueChanges.pipe(startWith(partnerId.value)).subscribe(() => {
      // needed to calculate and set validators one more time
      contributionAmount.setValidators(this.getContributionAmountValidators());
      contributionAmount.markAsDirty();
      contributionAmount.updateValueAndValidity();
      this.setMinDate();
    });

    if (this.company.legalForm !== COMPANY_LEGAL_FORM.LimitedPartnership) {
      return;
    }

    partnerType.valueChanges.pipe(
      distinctUntilChanged(),
      // needed to trigger change event to initially set validators
      startWith(partnerType.value),
    ).subscribe((type: PartnerType) => {
      switch (type) {
        case PartnerType.General:
          liabilitySum.setValue(0);
          liabilitySum.disable();
          break;
        case PartnerType.Limited:
        default:
          liabilitySum.enable();
      }
      liabilitySum.setValidators([
        Validators.required,
        Validators.min(0),
        Validators.maxDecimalDigits(2),
      ]);
      contributionAmount.setValidators(this.getContributionAmountValidators());

      liabilitySum.markAsDirty();
      liabilitySum.updateValueAndValidity();
      contributionAmount.markAsDirty();
      contributionAmount.updateValueAndValidity();
    });
  }

  private setCompaniesGetStream(): Observable<ICompany[]> {
    return this.companiesObservable = this.companyRepo.getStream().pipe(
      map((companies: ICompany[]) => {
        return companies.filter(c => c.id !== this.company.id);
      }),
    );
  }

  public submit(): void {
    validateAllFormFields(this.form);
    if (this.form.invalid) { return; }

    this.modalRef.close(this.form.getRawValue());
  }

  public cancel(): void {
    this.modalRef.close();
  }
}
