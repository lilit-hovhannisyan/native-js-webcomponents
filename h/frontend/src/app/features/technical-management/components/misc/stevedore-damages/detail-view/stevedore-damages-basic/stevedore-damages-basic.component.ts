import { Component, OnDestroy, OnInit, ChangeDetectorRef, ElementRef } from '@angular/core';
import { FormBuilder, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { combineLatest, merge, Observable, Subject, of, ReplaySubject, forkJoin } from 'rxjs';
import { distinctUntilChanged, filter, map, skipWhile, startWith, switchMap, takeUntil, tap } from 'rxjs/operators';
import { NvGridConfig } from 'nv-grid';
import { ResourceDetailView, NVCustomErrors } from 'src/app/core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { IDebitorCreditorRow, DebitorCreditorKey } from 'src/app/core/models/resources/IDebitorCreditor';
import { IPort } from 'src/app/core/models/resources/IPort';
import {
  incidentOptions,
  IStevedoreDamageReport,
  IStevedoreDamageReportCreate,
  positionFWDAFTSOptions,
  positionSBPSOptions,
  reportTypeOptions,
  StevedoreDamageReportType,
  StevedoreDamageReportStatus,
} from 'src/app/core/models/resources/IStevedoreDamageReport';
import { ITimeCharter } from 'src/app/core/models/resources/ITimeCharter';
import { IVoyage } from 'src/app/core/models/resources/IVoyage';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { DebitorCreditorMainService } from 'src/app/core/services/mainServices/debitor-creditor-main.service';
import { PortRepositoryService } from 'src/app/core/services/repositories/port-repository.service';
import { StevedoreDamageReportRepositoryService } from 'src/app/core/services/repositories/stevedore-damage-report-repository.service';
import { TimeCharterRepositoryService } from 'src/app/core/services/repositories/timeCharter.repository.service';
import { VoyagesRepositoryService } from 'src/app/core/services/repositories/voyages-repository.service';
import { getDateFormat, getDateTimeFormat } from 'src/app/shared/helpers/general';
import { StevedoreDamagesHelperService } from '../stevedore-damages.helper.service';
import { StevedoreDamageReportMainService } from 'src/app/core/services/mainServices/stevedore-damage-report-main.service';
import { StevedoreRepairRepositoryService } from 'src/app/core/services/repositories/stevedore-repair-repository.service';
import { RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { IStevedoreRepair } from 'src/app/core/models/resources/IStevedoreRepair';
import { getGridConfig as getDebitorsCreditorsGridConfig } from 'src/app/core/constants/nv-grid-configs/debitors-creditors.config';
import { getGridConfig as getPortsGridConfig } from 'src/app/core/constants/nv-grid-configs/ports.config';
import { IVesselAccounting, IVesselAccountingListItem, VesselAccountingKey } from 'src/app/core/models/resources/IVesselAccounting';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';
import { IVoyageCharterNameFull } from 'src/app/core/models/resources/IVoyageCharterName';
import { TimeCharterPeriodRepositoryService } from 'src/app/core/services/repositories/time-charter-period-repository.service';
import { ITimeCharterPeriod } from 'src/app/core/models/resources/ITimeCharterPeriod';
import { getDateWithinRange, setTranslationSubjects } from 'src/app/shared/helpers/general';
import { AddressParams } from 'src/app/core/services/repositories/address-repository.service';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { AddressMainService } from 'src/app/core/services/mainServices/address-main.service';

const REPORT_NO_PATTERN = /^[a-zA-Z]{3}\s([a-zA-Z]\s)?\d{3}-\d{2}$/su;

function getReportNoPatternValidator(): ValidatorFn {
  return Validators.pattern(
    REPORT_NO_PATTERN,
    wording.technicalManagement.invalidReportNo,
  );
}

@Component({
  selector: 'nv-stevedore-damages-basic',
  templateUrl: './stevedore-damages-basic.component.html',
  styleUrls: ['./stevedore-damages-basic.component.scss'],
})
export class StevedoreDamagesBasicComponent
  extends ResourceDetailView<IStevedoreDamageReport, IStevedoreDamageReportCreate>
  implements OnInit, OnDestroy {

  public dateNowISOString = new Date().toISOString();
  public repoParams = {};
  public resourceDefault = {} as IStevedoreDamageReport;
  public routeBackUrl = url(sitemap.technicalManagement.children.misc.children.stevedoreDamages);
  public dateTimeFormat: string;
  public dateFormat: string;

  public reportTypes: ISelectOption[] = reportTypeOptions;
  public positionsSBPS: ISelectOption[] = positionSBPSOptions;
  public positionsFWDAFT: ISelectOption[] = positionFWDAFTSOptions;
  public incidents: ISelectOption[] = incidentOptions;
  public charterers: IDebitorCreditorRow[];
  public ports: IPort[];

  public disabledFieldsArray: string[] = [];
  public unsubscribe$ = new Subject();
  public incidentDateControl: FormControl;
  public theCheckboxControl: FormControl;
  public incidentDateChange$: Observable<string>;
  public isFirstTimeVoyageGet = true;
  protected customErrors: NVCustomErrors = {
    'not-found': {
      changeFieldTo: 'sdReportDate',
      messageWording: wording.technicalManagement.dateShouldBeWithinChosenAccountingDates,
    },
  };

  private id: string;
  private repairQueryParams: RepoParams<IStevedoreRepair> = {};
  private stevedoreDamageRepair: IStevedoreRepair;
  private componentDestroyed$ = new ReplaySubject<boolean>(1);

  public debitorsCreditorsGridConfig: NvGridConfig = getDebitorsCreditorsGridConfig({}, () => false);

  public portsGridConfig: NvGridConfig = getPortsGridConfig({}, () => false);
  public vesselDataSource$: Observable<IVesselAccountingListItem[]> = of([]);
  private currentVessel: IVesselAccountingListItem;
  private currentVoyage: IVoyage;
  public formElement: ElementRef;

  constructor(
    stevedoreDamageReportRepositoryService: StevedoreDamageReportRepositoryService,
    private stevedoreDamageReportMainService: StevedoreDamageReportMainService,
    baseComponentService: BaseComponentService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private debitorCreditorRepo: DebitorCreditorMainService,
    private voyageRepo: VoyagesRepositoryService,
    private portsRepo: PortRepositoryService,
    private timeCharterRepo: TimeCharterRepositoryService,
    private stevedoreDamagesHelperService: StevedoreDamagesHelperService,
    private stevedoreRepairRepository: StevedoreRepairRepositoryService,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
    private timeCharterPeriodRepositoryService: TimeCharterPeriodRepositoryService,
    private addressMainService: AddressMainService,
    private vesselMainService: VesselMainService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef,
  ) {
    super(stevedoreDamageReportRepositoryService, baseComponentService);
  }

  public async ngOnInit() {
    this.id = this.route.parent.snapshot.params.id;
    this.repairQueryParams = { queryParams: { stevedoreDamageReportId: this.id } };

    this.dateTimeFormat = getDateTimeFormat(this.settingsService.locale);
    this.dateFormat = getDateFormat(this.settingsService.locale);
    this.getPorts();
    await this.vesselAccountingRepo.fetchAll({}).toPromise();
    const { id } = this.route.parent.snapshot.params;

    if (id === 'new') {
      this.resource = {} as IStevedoreDamageReport;
      this.enterCreationMode();
    } else {
      this.loadResource(id).pipe(
        switchMap(() => {
          return this.stevedoreRepairRepository.fetchAll(this.repairQueryParams).pipe(
            tap(res => this.stevedoreDamageRepair = res[0]),
          );
        }),
      ).subscribe();
    }
    this.pushInDisabledList(
      'voyageNo',
      'clearedByAndDate',
      'cpDate',
      'charterName',
      'vesselReadOnlyData',
      'startOfTimeCharter',
      'approximateDateOfRedelivery',
      'chartererId',
    );
  }

  public getTitle = () => this.resource.sdReportNo;

  public init(stevedoreReport: IStevedoreDamageReport = {} as IStevedoreDamageReport) {
    // unsubscribe all subscribers
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.isFirstTimeVoyageGet = true;

    const noPort = !(this.creationMode || stevedoreReport.portId);
    const theCheckboxChecked = stevedoreReport.isSettlementWithoutAutomaticCharterReference;
    const cpDateValidators: ValidatorFn[] = theCheckboxChecked
      ? [Validators.required, Validators.dateShouldBeInThePast]
      : [];

    this.initForm(this.fb.group({
      // section `General`
      isSettlementWithoutAutomaticCharterReference: [theCheckboxChecked],
      reportType: [stevedoreReport.reportType, [Validators.required]],
      resubmissionDate: [stevedoreReport.resubmissionDate],
      vesselAccountingId: [stevedoreReport.vesselAccountingId, theCheckboxChecked ? [] : [Validators.required]], // not in UI
      vesselId: [stevedoreReport.vesselId, Validators.required],
      voyageId: [stevedoreReport.voyageId],
      voyageNo: [],
      clearedByAndDate: [],
      clearedBy: [stevedoreReport.clearedBy], // doesn't exist on template
      clearedDate: [this.formatDateToISO(stevedoreReport.clearedDate)], // doesn't exist on template
      cpDate: [stevedoreReport.cpDate, cpDateValidators],
      sdReportNo: [stevedoreReport.sdReportNo, getReportNoPatternValidator()],
      sdReportDate: [stevedoreReport.sdReportDate],
      crewReportNo: [stevedoreReport.crewReportNo, getReportNoPatternValidator()],
      crewReportDate: [stevedoreReport.crewReportDate],
      hold: [stevedoreReport.hold, [Validators.number, Validators.min(1), Validators.max(99)]],
      bay: [stevedoreReport.bay, [Validators.number, Validators.min(1), Validators.max(99)]],
      row: [stevedoreReport.row, [Validators.number, Validators.min(1), Validators.max(99)]],
      tier: [stevedoreReport.tier, [Validators.number, Validators.min(1), Validators.max(99)]],
      positionSBPS: [stevedoreReport.positionSBPS],
      positionFWDAFT: [stevedoreReport.positionFWDAFT],
      // section `Charter`
      chartererId: [stevedoreReport.chartererId, [Validators.required]],
      charterName: [stevedoreReport.charterName, [Validators.required]],
      billingAddress: [stevedoreReport.billingAddress, [Validators.required]],
      // section `Event`
      incident: [stevedoreReport.incident, [Validators.required]],
      noPort: [noPort],
      portId: [stevedoreReport.portId, noPort ? [] : [Validators.required]],
      description: [stevedoreReport.description],
      status: [stevedoreReport.status],
      // new set
      isChartererInformed: [stevedoreReport.isChartererInformed],
      startOfTimeCharter: [stevedoreReport.startOfTimeCharter],
      approximateDateOfRedelivery: [stevedoreReport.approximateDateOfRedelivery],
      vesselReadOnlyData: null,
    }));
    // it is hard to every time use 'isSettlementWithoutAutomaticCharterReference' form control name
    this.theCheckboxControl = this.form.get('isSettlementWithoutAutomaticCharterReference') as FormControl;
    this.initIncidentDateValueChange();
    this.initVesselNameChangeListener();
    this.handleReportTypeChange();
    this.initSettlementCheckboxChangeListener();
    this.initReportTypeChangeListener();
    this.initCrewReportNoChangeListener();
    this.initPortChangeFunctionality();
    this.initVoyageChangeListener();
    this.getCharterersAndInitChangeListener();
    this.initAutoSelectVesselAccounting();
    this.initVesselReadOnlyData();
    this.initVesselAndVoyage();
  }

  private initVesselReadOnlyData(): void {
    const id: VesselAccountingKey = this.form.get('vesselAccountingId').value;

    if (!id) {
      return;
    }
    this.vesselAccountingRepo.fetchOne(id, {} ).subscribe(vessel => {
      this.form.get('vesselReadOnlyData')
        .patchValue(vessel.vesselNo + ' ' + vessel.vesselName);
    });
  }

  private initVesselAndVoyage() {
    const vesselId: VesselKey = this.form.get('vesselId').value;
    if (!vesselId) { return; }

    this.vesselMainService.fetchVesselsWithClosestAccounting().subscribe(vessels => {
      const vessel: IVesselAccountingListItem = vessels.find(v => v.vesselId === vesselId);
      this.currentVessel = vessel;

      const voyage: IVoyage = getDateWithinRange(this.currentVessel.vesselVoyages, this.incidentDateControl.value);
      this.currentVoyage = voyage;

      const voyageNo: string = this.currentVoyage?.no
        ? `${this.currentVoyage.no}`.padStart(3, '0')
        : null;

      this.form.get('voyageNo').patchValue(voyageNo);
    });
  }

  private formatDateToISO(date: string): string | null {
    // TODO: remove this function when backend will give `clearedDate` in correct format
    /**
     * On-time of [POST] and [PUT] backend is ignoring sent `clearedDate`
     * and  generating a new one in `yyyy-MM-dd'T'hh:mm:ss.ffffff'Z'` format.
     * After setting that entity(`StevedoreReport`) in DB that format is being
     * changed(by DB).
     * So when we doing [GET] request backend gives us
     * `clearedDate`  in `yyy-MM-dd'T'hh:mm:sss.fff` format
     */
    if (!date) { return null; }
    if (date[date.length - 1] !== 'Z') { date += 'Z'; }
    return moment(date).toISOString();
  }

  private initIncidentDateValueChange(): void {
    /***
     * when `reportType` value is `stevedore` => `incidentDate` is `sdReportDate`,
     * when `reportType` value is `crewwork` => `incidentDate` is `crewReportDate`,
     * when `reportType` is not selected => there is no `incidentDate`,
     */
    this.incidentDateChange$ = merge(
      this.form.get('sdReportDate').valueChanges.pipe(distinctUntilChanged()),
      this.form.get('crewReportDate').valueChanges.pipe(distinctUntilChanged()),
      this.form.get('reportType').valueChanges.pipe(distinctUntilChanged()),
    ).pipe(
      filter(value => this.incidentDateControl && this.incidentDateControl.value === value),
      map(() => this.incidentDateControl?.value),
      distinctUntilChanged(),
    );
  }

  private initSettlementCheckboxChangeListener(): void {
    /***
     * When `isSettlementWithoutAutomaticCharterReference` checkbox field is CHECKED:
     *  1.`cpDate` is required and  not disabled
     *  2.`vesselAccountingId` is not required anymore
     *  3.`charterName` is not disabled but required still
     * When it is UNCHECKED:
     *  1.`cpDate` is not required, but disabled
     *  2.`charterName` is disabled and required(autofill)
     *  3.`vesselAccountingId` is required
     *  4.`voyage number` is editable
     *  4.`chartererId` disable and autocomplete
     */
    this.theCheckboxControl.valueChanges.pipe(
      skipWhile(() => !this.editMode),
      distinctUntilChanged(),
      takeUntil(this.unsubscribe$),
    ).subscribe(isChecked => {
      if (isChecked) {
        this.deleteFromDisabledList(
          'cpDate',
          'charterName',
          'chartererId',
          'startOfTimeCharter',
          'approximateDateOfRedelivery',
          'voyageNo',
          'chartererId',
        );
        this.changeValidatorAndUpdate('cpDate', [
          Validators.required,
          Validators.dateShouldBeInThePast,
        ]);
        this.changeValidatorAndUpdate('vesselAccountingId', []);
        this.clearAutofillCharterReferenceFields();
      } else {
        this.pushInDisabledList(
          'cpDate',
          'charterName',
          'startOfTimeCharter',
          'approximateDateOfRedelivery',
          'voyageNo',
          'chartererId',
        );
        this.changeValidatorAndUpdate('cpDate', [Validators.dateShouldBeInThePast]);
        this.changeValidatorAndUpdate('vesselAccountingId', [Validators.required]);
        this.autofillCharterCharterReferenceFields();
      }
      setTimeout(() => {
        if (this.form.enabled) {
          this.form.enable({ emitEvent: false });
          this.setDisabledFields(this.disabledFieldsArray);
          this.disableMarkedFields();
        }
      });
    });
  }

  private handleReportTypeChange(): void {
    const { reportType, crewReportNo } = this.form.controls;

    if (reportType.value === StevedoreDamageReportType.CrewWork) {
      this.incidentDateControl = this.form.controls.crewReportDate as FormControl;
      this.changeValidatorAndUpdate('crewReportNo', [Validators.required, getReportNoPatternValidator()]);
      this.changeValidatorAndUpdate('crewReportDate', [Validators.required, Validators.dateShouldBeInThePast]);
      this.changeValidatorAndUpdate('sdReportNo', []);
      this.changeValidatorAndUpdate('sdReportDate', []);
      this.pushInDisabledList('sdReportNo', 'sdReportDate');
      this.form.get('sdReportNo').setValue(null);
      this.form.get('sdReportDate').setValue(null);
    } else if (this.form.controls.reportType.value === StevedoreDamageReportType.Stevedore) {
      this.incidentDateControl = this.form.controls.sdReportDate as FormControl;
      this.deleteFromDisabledList('sdReportNo', 'sdReportDate');
      this.changeValidatorAndUpdate('crewReportNo', [getReportNoPatternValidator()]);
      this.changeValidatorAndUpdate('crewReportDate', crewReportNo.value
        ? [Validators.required, Validators.dateShouldBeInThePast]
        : [Validators.dateShouldBeInThePast],
      );
      this.changeValidatorAndUpdate('sdReportNo', [Validators.required, getReportNoPatternValidator()]);
      this.changeValidatorAndUpdate('sdReportDate', [Validators.required, Validators.dateShouldBeInThePast]);
    }
    setTimeout(() => {
      if (this.form.enabled) {
        this.form.enable({ emitEvent: false });
        this.setDisabledFields(this.disabledFieldsArray);
        this.disableMarkedFields();
      }
    });
  }

  private initPortChangeFunctionality() {
    this.form.get('noPort').valueChanges.pipe(
      distinctUntilChanged(),
      takeUntil(this.unsubscribe$),
    ).subscribe(checked => this.handleNoPortChange(checked));
  }

  private handleNoPortChange(checked): void {
    /**
     * when `noPort` is CHECKED:
     *   `portId` is optional(without asterisk) and disabled
     *   and has to be empty
     * when it's UNCHECKED:
     *   `portId` is required and enabled
     */
    const portId = this.form.get('portId');
    if (checked) {
      this.pushInDisabledList('portId');
      this.changeValidatorAndUpdate('portId', []);
      portId.setValue(null);
      portId.disable();
    } else {
      this.deleteFromDisabledList('portId');
      this.changeValidatorAndUpdate('portId', [Validators.required]);
      portId.setValue(this.resource.portId);
      portId.enable();
    }
  }

  private initReportTypeChangeListener(): void {
    this.form.controls.reportType.valueChanges
      .pipe(distinctUntilChanged(), takeUntil(this.unsubscribe$))
      .subscribe(() => this.handleReportTypeChange());
  }

  private initVesselNameChangeListener(): void {
    const { vesselAccountingId } = this.form.controls;

    vesselAccountingId.valueChanges.pipe(
      distinctUntilChanged(),
      takeUntil(this.unsubscribe$),
    ).subscribe((vaId: VesselAccountingKey) => {
      const vesselAccounting: IVesselAccounting = this.vesselAccountingRepo
        .getStreamValue()
        .find(v => v.id === vaId);
    });
  }

  private getCharterersAndInitChangeListener(): void {
    this.debitorCreditorRepo.fetchAll().subscribe(charterers => {
      this.charterers = this.debitorCreditorRepo.toRows(charterers);
    });
  }

  private initCrewReportNoChangeListener(): void {
    const { crewReportNo, crewReportDate, reportType } = this.form.controls;

    crewReportNo.valueChanges.pipe(
      distinctUntilChanged(),
      takeUntil(this.unsubscribe$),
    ).subscribe(reportNoValue => {
      // `crewReportDate` when `crewReportNo` is not empty by Acceptance Criteria
      crewReportDate.setValidators(
        reportType.value === StevedoreDamageReportType.CrewWork || reportNoValue
          ? [Validators.required]
          : [],
      );
      crewReportDate.updateValueAndValidity();
    });
  }

  private setVoyage(): void {
    this.currentVoyage = null;
    const { vesselAccountingId } = this.form.controls;
    const date = this.incidentDateControl?.value;
    if (vesselAccountingId.value && date) {
      const voyage: IVoyage = getDateWithinRange(this.currentVessel.vesselVoyages, date);
      this.setVoyageValue(voyage);
    }
  }

  private setVoyageValue(voyage?: IVoyage): void {
    const { voyageId } = this.form.controls;
    voyageId.patchValue(voyage ? voyage.id : null);

    if (!voyage) {
      this.clearAutofillCharterReferenceFields();
    } else {
      this.currentVoyage = voyage;

      const vesselAccounting: IVesselAccounting = this.getVesselAccounting();

      vesselAccounting
        ? this.autofillCharterCharterReferenceFields()
        : this.showVesselAccountingWarning();
    }

    this.isFirstTimeVoyageGet = false;
  }

  private initVoyageChangeListener(): void {
    const { vesselAccountingId, sdReportDate, crewReportDate } = this.form.controls;

    combineLatest([
      sdReportDate.valueChanges,
      crewReportDate.valueChanges,
      vesselAccountingId.valueChanges,
    ]).pipe(
      distinctUntilChanged((
        [xSdReportDate, xCrewReportDate, xVesselAccountingId],
        [ySdReportDate, yCrewReportDate, yVesselAccountingId],
      ) => xSdReportDate === ySdReportDate
        && xCrewReportDate === yCrewReportDate
        && xVesselAccountingId === yVesselAccountingId),
      startWith(),
      skipWhile((value, index) => {
        // emit value(`null`) for fetching voyageNo only when page is not in editMode
        // and not emit the `null` value when page in editMode
        return this.editMode ? !index : !!value;
      }),
      takeUntil(this.unsubscribe$),
      tap(() => {
        const date = this.incidentDateControl?.value;
        if (date && vesselAccountingId.value) {
          this.setVoyage();
        }
      })
    ).subscribe();
  }

  private openSuggestionPopup(): void {
    /**
     * The suggestion popup should not be shown when the checkbox is already checked.
     */
    if (this.theCheckboxControl.value) {
      return;
    }
    const incidentDate = moment(this.incidentDateControl.value)
      .utc(false)
      .format(this.dateTimeFormat);

    this.confirmationService.confirm({
      wording: wording.technicalManagement.withoutCharterReferenceMessage,
      okButtonWording: wording.general.create,
      cancelButtonWording: wording.general.cancel,
      subjects: { INCIDENT_DATE: incidentDate },
    }).subscribe(confirmed => {
      if (confirmed) {
        this.theCheckboxControl.setValue(true);
      } else {
        this.incidentDateControl.patchValue(null);
      }
    });
  }

  private getPorts(): void {
    this.portsRepo.fetchAll({}).subscribe(ports => this.ports = ports);
  }

  public submit(): void {

    if (this.form.invalid) {
      return;
    }

    this.calculateStatus();

    let value = {
      ...this.resource,
      ...this.form.getRawValue(),
    };

    /**
     * in case when there are multiple accountings attached to vessel, we must grab the one which is
     * within selected date range, and overwrite current `vesselAccountingId`
     */

    const vesselAccounting: IVesselAccounting = this.getVesselAccounting();
    const actualVesselAccountingId: VesselAccountingKey = vesselAccounting?.id;
    if (actualVesselAccountingId) {
      value = {
        ...value,
        vesselAccountingId: actualVesselAccountingId
      };
    }

    if (this.creationMode) {
      this.createResource(value).subscribe(freshStevedoreDamage => {
        this.updateDetailViewTitle(freshStevedoreDamage);
      });
    } else {
      this.updateResource(value).subscribe(freshStevedoreDamage => {
        this.updateDetailViewTitle(freshStevedoreDamage);
      });
    }
  }

  public calculateStatus() {
    const {
      reportType,
      sdReportNo,
      sdReportDate,
      crewReportNo,
      crewReportDate,
      chartererId,
      charterName,
      billingAddress,
      incident,
      status,
      vesselAccountingId,
    } = this.form.controls;

    const isThereSelectedReport = reportType.value === StevedoreDamageReportType.CrewWork
      ? crewReportNo.value && crewReportDate.value
      : sdReportNo.value && sdReportDate.value;

    if (
      reportType.value
      && vesselAccountingId.value
      && chartererId.value
      && charterName.value
      && billingAddress.value
      && incident.value
      && isThereSelectedReport
      && status.value !== StevedoreDamageReportStatus.RatingRequired
    ) {
      if (this.resource.isInvoiceApproved) {
        status.setValue(StevedoreDamageReportStatus.InProgress);
      } else if (this.stevedoreDamageRepair?.stevedoreDamageRepairItems.length > 0) {
        status.setValue(StevedoreDamageReportStatus.ReleaseRequired);
      } else {
        status.setValue(StevedoreDamageReportStatus.RatingRequired);
      }
    } else {
      status.setValue(StevedoreDamageReportStatus.RatingRequired);
    }
  }

  public updateDetailViewTitle(stevedoreDamageReport: IStevedoreDamageReport) {
    const subscription = this.stevedoreDamageReportMainService
      .fetchFullStevedoreDamageReport(stevedoreDamageReport)
      .subscribe((stevedoreDamage) => {
        const stevedoreDamageRow = this.stevedoreDamageReportMainService
          .toRow(stevedoreDamage);
        this.stevedoreDamagesHelperService.generateTitle(stevedoreDamageRow);
        subscription.unsubscribe();
      });
  }

  private deleteFromDisabledList(...fieldNames: string[]): void {
    fieldNames.forEach(field => {
      const index = this.disabledFieldsArray.findIndex(value => value === field);
      if (index >= 0) {
        this.disabledFieldsArray.splice(index, 1);
      }
    });
  }

  private pushInDisabledList(...fieldNames: string[]): void {
    fieldNames.forEach(field => {
      if (!this.disabledFieldsArray.find(value => value === field)) {
        this.disabledFieldsArray.push(field);
      }
    });
  }

  private changeValidatorAndUpdate(controlName: string, validators: ValidatorFn[]): void {
    const control = this.form.get(controlName);
    control.setValidators(validators);
    control.updateValueAndValidity();
  }

  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  private autofillCharterCharterReferenceFields(): void {
    if (this.theCheckboxControl.value) {
      // if `Settlement without charter reference`: true don't autofill, even if there is data on that damage date
      return;
    }
    const { voyageId } = this.form.controls;

    const date = this.incidentDateControl?.value;
    if ( voyageId.value && date) {
      forkJoin([
        this.timeCharterRepo.fetchAll({}),
        this.timeCharterPeriodRepositoryService.fetchAll({}),
        this.voyageRepo.fetchCharternames(voyageId.value),
      ]).subscribe(([timeCharters, timeCharterPeriods, voyageCharternames]) => {
        const timeCharterList: ITimeCharter[] = timeCharters
          .filter(t => t.voyageId === voyageId.value);

        const timeCharter: ITimeCharter = getDateWithinRange(timeCharterList, date);

        const chartername: IVoyageCharterNameFull = getDateWithinRange(
          this.stevedoreDamageReportMainService.toCharternameFull(voyageCharternames),
          date,
        );

        const period: ITimeCharterPeriod = timeCharterPeriods
          .find(p => p.timeCharterId === timeCharter?.id);

        const chartererIdValue: DebitorCreditorKey = timeCharter?.debitorCreditorId;

        if (timeCharter) {
          this.form.get('startOfTimeCharter').patchValue(timeCharter.from);
          this.form.get('voyageNo').patchValue(`${this.currentVoyage.no}`.padStart(3, '0'));
          this.form.get('approximateDateOfRedelivery').patchValue(period?.endApprox);
          this.form.get('charterName').patchValue(chartername?.charterName);
          this.form.get('cpDate').patchValue(timeCharter.charterPartyDate);
          this.form.get('chartererId').setValue(chartererIdValue, { emitEvent: false });

          timeCharter?.billingAddress
            ? this.form.get('billingAddress').setValue(timeCharter.billingAddress)
            : this.getAndSetBillingAddress(chartererIdValue);
        } else {
          this.clearAutofillCharterReferenceFields();
        }
      });
    }
  }

  private getAndSetBillingAddress(debitorCreditorId: DebitorCreditorKey): void {
    this.debitorCreditorRepo.fetchOne(debitorCreditorId).pipe(
      switchMap(debitorCreditor => {
        const queryParams: AddressParams = { companyId: debitorCreditor.companyId };
        return this.addressMainService.fetchAll({ queryParams });
      }),
      map(addresses => this.addressMainService.toRows(addresses)),
      map(([address]) => this.addressMainService.rowAddressToText(address)),
    ).subscribe(billingAddress => {
      this.form.get('billingAddress').patchValue(billingAddress);
    });
  }

  private clearAutofillCharterReferenceFields() {
    this.form.get('startOfTimeCharter').patchValue(null);
    this.form.get('voyageNo').patchValue(null);
    this.form.get('approximateDateOfRedelivery').patchValue(null);
    this.form.get('charterName').patchValue(null);
    this.form.get('billingAddress').patchValue(null);
    this.form.get('cpDate').patchValue(null);
    this.form.get('chartererId').setValue(null, { emitEvent: false });
  }

  private initAutoSelectVesselAccounting(): void {
    const reportDateCtrl: AbstractControl = this.form.get('sdReportDate');
    const crewReportDateCtrl: AbstractControl = this.form.get('crewReportDate');
    const reportTypeCtrl: AbstractControl = this.form.get('reportType');
    const vesselCtrl: AbstractControl = this.form.get('vesselId');

    /**
     * In case of report type=stevedore
     * vessel.accounting.id should result from the field “date of damage” entered in “stevedore damage date” and “vessel”
     *
     * In case of report type=crewwork:
     * vessel.accounting.id should result from the field “date of damage” entered in “crewwork damage date” and “vessel”
     *
     * so observing them, we pass as a date the result of `this.incidentDateControl`
     *
     * first wed check wheter or not date is in range of vessel accounting period
     * afterwards sort time charter by `vesselId`
     * and autofill `START OF TIME CHARTER` && `START OF TIME CHARTER` correspondingly
     * if `Settlement without charter reference` === false
     */
    combineLatest([
      reportTypeCtrl.valueChanges,
      vesselCtrl.valueChanges,
      reportDateCtrl.valueChanges,
      crewReportDateCtrl.valueChanges,
    ]).pipe(
      takeUntil(this.componentDestroyed$),
      distinctUntilChanged(
        ([reportType1, vessel1, reportDate1, crewReportDate1],
                  [reportType2, vessel2, reportDate2, crewReportDate2]) => {
        return reportType1 === reportType2 && vessel1 === vessel2
          && reportDate1 === reportDate2 && crewReportDate1 === crewReportDate2;
      }),
      tap(([reportType, vessel, reportDate, crewReportDate]) => {
        const date = this.incidentDateControl?.value;
        if (date && vesselCtrl.value) {
          this.patchClosestVesselAccounting();
        } else {
          this.clearAutofillCharterReferenceFields();
        }
      })
    ).subscribe();
  }

  private patchClosestVesselAccounting(): void {
    const vesselAccounting: IVesselAccounting = this.getVesselAccounting();

    if (!this.currentVoyage) {
      if (!vesselAccounting) {
        this.showVesselAccountingWarning();
      } else {
        this.openSuggestionPopup();
      }
    }
  }

  private getVesselAccounting(): IVesselAccounting {
    const date = this.incidentDateControl?.value;
    const vesselCtrl: AbstractControl = this.form.get('vesselId');
    if (!vesselCtrl.valid) {
      return;
    }
    const vesselAccountingList: IVesselAccounting[] = this.vesselAccountingRepo.getStreamValue()
      .filter(v => v.vesselId === vesselCtrl.value);

    const vesselAccounting: IVesselAccounting = getDateWithinRange(
      vesselAccountingList,
      date
    );

    return vesselAccounting;
  }

  // NAVIDO-2817: If no accounting.id covers the inserted “date of damage”, show following warning: ...
  private showVesselAccountingWarning(): void {
    const incidentDate = moment(this.incidentDateControl.value)
      .utc(false)
      .format(this.dateTimeFormat);

    this.confirmationService.warning({
      title: null,
      wording: setTranslationSubjects(
        wording.technicalManagement.dateNotCoveredByAccountingPeriod,
        {
          enteredDateTime: incidentDate
        },
      ),
      okButtonWording: wording.general.ok,
      cancelButtonWording: null,
    }).subscribe(() => {
      this.incidentDateControl.patchValue(null);
    });
  }

  public onVesselSelect(vessel: IVesselAccountingListItem): void {
    const readOnlyData = vessel && vessel.vesselNo ? vessel.vesselNo + ' ' + vessel.vesselName : '';
    this.currentVessel = vessel ? vessel : null;

    if (!vessel) { return; }

    this.form.get('vesselAccountingId').patchValue(vessel.vesselAccountingId);
    this.form.get('vesselReadOnlyData').patchValue(readOnlyData);
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
