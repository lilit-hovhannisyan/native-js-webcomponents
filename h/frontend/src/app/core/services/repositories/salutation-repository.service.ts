import { SalutationKey } from './../../models/resources/ISalutation';
import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { ISalutation, ISalutationDTO, ISalutationCreate } from '../../models/resources/ISalutation';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class SalutationRepositoryService extends AbstractRepository<
  ISalutation,
  ISalutationDTO,
  ISalutationCreate,
  RepoParams<ISalutation>,
  SalutationKey
> {
  protected resourceType = 'Salutation';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/salutations';
  }

  protected postProcess(item: ISalutationDTO): ISalutation { return item; }
}
