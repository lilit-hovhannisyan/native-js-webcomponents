import { VesselTaskStatus } from '../enums/vessel-registration-tasks';
import { IWithLabels } from './IWithLabels';

export interface IVesselTaskTodoItem extends IWithLabels {
  status: VesselTaskStatus;
  started?: string;
  completed?: string;
}
