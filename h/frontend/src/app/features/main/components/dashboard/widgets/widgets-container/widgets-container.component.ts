import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { WidgetType } from 'src/app/core/models/widgets/WidgetType';
import { IWidget } from 'src/app/core/models/widgets/IWidget';

@Component({
  selector: 'nv-widgets-container',
  templateUrl: './widgets-container.component.html',
  styleUrls: ['./widgets-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WidgetsContainerComponent {
  @Input() public widget: IWidget;
  @Input() public editMode: boolean;
  public widgetType = WidgetType;

  @Output() public widgetDelete: EventEmitter<number> = new EventEmitter<number>();

  public removeWidget(id: number): void {
    this.widgetDelete.emit(id);
  }
}
