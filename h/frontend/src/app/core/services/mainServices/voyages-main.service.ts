import { Injectable } from '@angular/core';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { VoyagesRepositoryService, IVoyageRepoParams } from '../repositories/voyages-repository.service';
import { IVoyageFull, IVoyage, IVoyageRow } from '../../models/resources/IVoyage';
import { sortBy } from 'lodash';
import { VesselAccountingRepositoryService } from '../repositories/vessel-accounting-repository.service';
import { IVesselAccounting } from '../../models/resources/IVesselAccounting';

@Injectable({
  providedIn: 'root'
})
export class VoyagesMainService {
  constructor(
    private voyagesRepo: VoyagesRepositoryService,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
  ) { }

  public fetchAll(params: IVoyageRepoParams = {}): Observable<IVoyageFull[]> {
    return combineLatest([
      this.voyagesRepo.fetchAll(params),
      this.vesselAccountingRepo.fetchAll({}),
    ]).pipe(
      map(res => this.getFullList(...res)),
    );
  }

  public fetchOne(id: number, params: IVoyageRepoParams = {}): Observable<IVoyageFull> {
    return this.composeFull(
      this.voyagesRepo.fetchOne(id, params),
      this.vesselAccountingRepo.fetchAll({})
    );
  }

  private composeFull(
    voyages$: Observable<IVoyage>,
    vesselAccountings$: Observable<IVesselAccounting[]>,
  ): Observable<IVoyageFull> {
    return forkJoin([voyages$, vesselAccountings$]).pipe(
      map(([voyage, vesselAccounting]) => this.getFull(voyage, vesselAccounting))
    );
  }

  private getFullList(
    voyages: IVoyage[],
    vesselAccountings: IVesselAccounting[]
  ): IVoyageFull[] {
    return voyages.map(v => this.getFull(v, vesselAccountings));
  }

  private getFull(voyage: IVoyage, vesselAccountings: IVesselAccounting[]): IVoyageFull {
    const vesselAccounting = vesselAccountings.find(va => va.id === voyage.vesselAccountingId);
    return { ...voyage, vesselAccounting };
  }

  public getStream(): Observable<IVoyageFull[]> {
    return this.voyagesRepo.fetchAll({})
      .pipe(
        switchMap(() => combineLatest([
          this.voyagesRepo.getStream(),
          this.vesselAccountingRepo.getStream(),
        ])),
        map(res => this.getFullList(...res))
      );
  }

  public toRow(voyage: IVoyageFull): IVoyageRow {
    return ({
      ...voyage,
      yearAndNo: `${voyage.year}-${voyage.no.toString().padStart(3, '0')}`
    });
  }

  public toRows = (voyages: IVoyageFull[]): IVoyageRow[] =>
    sortBy(voyages.map(v => this.toRow(v)), ['yearAndNo'])
}
