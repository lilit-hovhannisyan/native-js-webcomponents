import { Component, OnInit, forwardRef, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { map } from 'rxjs/operators';
import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { GridDataSelectComponent } from '../../navido-components/grid-data-select/grid-data-select.component';
import { IResource } from 'src/app/core/models/resources/IResource';
import { wording } from 'src/app/core/constants/wording/wording';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  selector: 'nv-resource-select',
  templateUrl: './resource-select.component.html',
  styleUrls: ['./resource-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => ResourceSelectComponent),
    }
  ]
})
export class ResourceSelectComponent implements OnInit,
  ControlValueAccessor {
  @Input() public dataSource$: Observable<IResource<any>[]>;
  @Input() public getLabel: (e: IResource<any>) => string;
  @Input() public getGridConfig: ({ select: Function }) => any;
  @Input() public autoFocus: boolean;
  @Output() public valuChange = new EventEmitter<number>();

  private modalRef: NzModalRef;
  public gridConfig: NvGridConfig;
  public value: number;
  public disabled: boolean;

  public onChanged: any = () => { };
  public onTouched: any = () => { };

  constructor(
    private modalService: NzModalService,
    private cdr: ChangeDetectorRef,
    private settingsService: SettingsService
  ) { }

  public ngOnInit() {
    this.gridConfig = {
      ...this.getGridConfig({ select: this.onSelect }),
      rowSelectionType: NvGridRowSelectionType.RadioButton
    };
  }

  public writeValue = (value: number) => this.value = value;

  public registerOnChange = (fn: Function) => this.onChanged = fn;

  public registerOnTouched = (fn: Function) => this.onTouched = fn;

  public openSelectModal() {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzBodyStyle: {
        padding: '10px',
        height: 'calc(100% - 66px)',
      },
      nzStyle: {
        top: '0px',
        padding: '30px',
        height: '100%',
        'max-width': '100%',
      },
      nzWidth: 'min-content',
      nzTitle: wording.general.select[this.settingsService.language],
      nzClosable: true,
      nzContent: GridDataSelectComponent,
      nzComponentParams: {
        dataSource$: this.dataSource$.pipe(map(res => {
          return res.map(e => ({
            ...e,
            _checked: this.value && e.id === this.value
          }));
        })),
        gridConfig: this.gridConfig,
      },
    });
  }

  private onSelect = (row: IResource<any>) => {
    this.value = row.id;
    this.onChanged(this.value);
    this.valuChange.emit(this.value);
    this.modalRef.destroy();
  }

  public selectValueChanged(id: any) {
    this.onTouched();
    this.value = id;
    if (id || id === 0 || id === null) {
      this.onChanged(id);
      this.valuChange.emit(id);
    }
  }

  public setDisabledState(disabled: boolean) {
    this.disabled = disabled;
    this.cdr.detectChanges();
  }

  public keyup(e: KeyboardEvent) {
    if (e.code === 'F2') {
      this.openSelectModal();
    }
  }
}
