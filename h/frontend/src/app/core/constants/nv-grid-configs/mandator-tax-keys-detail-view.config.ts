import { NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { wording } from '../wording/wording';

const wordingTaxKeys = wording.accounting;
const wordingGeneral = wording.general;

export const getMandatorTaxKeyGridConfig = (): NvGridConfig => {

  return ({
    gridName: 'MandatorTaxKeysGridTab',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    rowSelectionType: NvGridRowSelectionType.Checkbox,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'code',
        title: wordingTaxKeys.taxKeysLabelCode,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wordingGeneral.name,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'percentage',
        title: wordingTaxKeys.taxKeysLabelPercentage,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          selectValues: []
        }
      }
    ]
  });
};
