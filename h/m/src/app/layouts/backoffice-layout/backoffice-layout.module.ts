import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { BackofficeLayoutComponent } from './backoffice-layout.component';
import { DefinitionsWebService } from '@shared/services/definitions.web-service';
import { MeComponentLoadingModule } from '@shared/me-directives/me-component-loader/me-component-loader.module';
import { MeSidebarModule } from '@shared-components/me-sidebar/me-sidebar.module';
import { MeHeaderModule } from '@shared-components/me-header/me-header.module';
import { MeBreadcrumbModule } from '@shared/me-components/me-breadcrumb/me-breadcrumb.module';
@NgModule({
  declarations: [BackofficeLayoutComponent],
  imports: [CommonModule, RouterModule, MeComponentLoadingModule, MeSidebarModule, MeHeaderModule, MeBreadcrumbModule],
  providers: [DefinitionsWebService]
})
export class BackofficeLayoutModule {}
