import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { Validators } from 'src/app/core/classes/validators';
import { wording } from 'src/app/core/constants/wording/wording';
import { NvLocale } from 'src/app/core/models/dateFormat';
import { IVoyageCharterName } from 'src/app/core/models/resources/IVoyageCharterName';
import { SettingsService } from 'src/app/core/services/settings.service';
import { validateAllFormFields } from '../../helpers/validateAllFormFields';

/**
 * 1. The frontend always sends dates to the backend on ISO(ISO 8601) formate
 * (example: 2020-04-27T09:07:20Z), which has 'Z' at the end.
 * 2. When the  backend gets the date on ISO format, it removes the character 'Z'
 * and keeps that value.
 * 3. When the frontend gets the same date from the backend - it is already
 * without 'Z' at the end. The frontend uses that date as it's a locale date
 * which is different from ISO date.
 *
 * Example: Frontend sends the value `2020-04-27T09:07:20Z` to the backend.
 * Then backend changes the value to `2020-04-27T09:07:20`. Frontend gets that
 * value and converts it to ISO string which is already `2020-04-27T05:07:20Z`
 * if the current location is Armenia(for example) which time zone is +4.
 *
 * The main validation of this component doesn't work correctly.
 * TODO: fix validation once all mixed up things related to dates on project
 * will be fixed.
 */
@Component({
  selector: 'nv-chartername-detail-view-modal',
  templateUrl: './chartername-detail-view-modal.component.html',
  styleUrls: ['./chartername-detail-view-modal.component.scss'],
})
export class CharternameDetailViewModalComponent implements OnInit {
  @Input() public chartername: IVoyageCharterName;
  /**
   * The `from` field of chartername has to be bigger or equal than `voyageFrom`.
   */
  @Input() public voyageFrom: string;
  /**
   * If exists the previous chartername, than the `from` field of current
   * chartername has to be bigger than the previous one.
   */
  @Input() public from: string;
  /**
   * The 'from' field has to be smaller or equal than `until` which is `voyageUntil`.
   */
  @Input() public until: string;

  public wording = wording;
  public form: FormGroup;
  public isCreateMode = false;
  public locale: NvLocale;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private settingsService: SettingsService,
  ) { }

  public ngOnInit(): void {
    this.locale = this.settingsService.locale;
    this.form = this.fb.group({
      from: [
        this.chartername?.from,
        [
          Validators.required,
          Validators.dateShouldBeAfter(
            this.from || this.voyageFrom,
            this.locale,
            // when this.from is falsy, then date can be equal to voyageFrom
            !this.from,
            false,
            'day',
          ),
          Validators.dateShouldBeBefore(this.until, this.locale, true, false, 'day'),
        ],
      ],
      charterName: [
        this.chartername?.charterName,
        [ Validators.required, Validators.maxLength(150) ],
      ],
    });

    this.isCreateMode = !this.chartername;
  }

  public cancel(): void {
    this.modalRef.close();
  }

  public submit(): void {
    validateAllFormFields(this.form);
    if (!this.form.valid) {
      return;
    }

    this.modalRef.close({
      ...this.chartername,
      ...this.form.value,
    });
  }
}
