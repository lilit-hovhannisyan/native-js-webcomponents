import { Language } from 'src/app/core/models/language';
import { Validators } from '../../../../../../../core/classes/validators';
import { UserRepositoryService } from '../../../../../../../core/services/repositories/user-repository.service';
import { BaseComponentService } from '../../../../../../../core/services/base-component.service';
import { IUserCreate } from '../../../../../../../core/models/resources/IUser';
import { IUser } from 'src/app/core/models/resources/IUser';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { Component, OnInit, ChangeDetectorRef, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { sitemap } from '../../../../../../../core/constants/sitemap/sitemap';
import { NotificationType } from '../../../../../../../core/models/INotification';
import { NotificationsService } from '../../../../../../../core/services/notification.service';
import { IUpload } from 'src/app/core/models/resources/IUpload';
import { PHONENUMBER } from 'src/app/core/constants/regexp';
import { wording } from 'src/app/core/constants/wording/wording';
import { tap, switchMap } from 'rxjs/operators';
import { CurrentUserService } from 'src/app/shared/services/current-user.service';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import * as moment from 'moment';
import { UsersHelperService } from '../users.helper.service';
import { SettingsRepositoryService } from 'src/app/core/services/repositories/settings-repository.service';
import { IEmailDomain } from 'src/app/core/models/resources/ISettings';
import { Observable, of } from 'rxjs';

@Component({
  templateUrl: './user-basic.component.html',
  styleUrls: ['./user-basic.component.scss']
})
export class UserBasicComponent extends ResourceDetailView<IUser, IUserCreate> implements OnInit {
  protected repoParams = { useCache: false };
  protected resourceDefault = {
    isActive: true,
    isAdmin: false,
    language: Language.DE,
    hasEmail: true
  } as IUser;

  public routeBackUrl: string;
  public form: FormGroup;
  public Language = Language;
  private allowedEmailDomainsList: IEmailDomain[] = [];
  public expirationDateStart = moment().toISOString();
  private passwordValidators: ValidatorFn[] = [Validators.minLength(12)];
  public formElement: ElementRef;

  constructor(
    public route: ActivatedRoute,
    public fb: FormBuilder,
    public userRepository: UserRepositoryService,
    baseComponentService: BaseComponentService,
    private userRepositoryService: UserRepositoryService,
    public usersHelperService: UsersHelperService,
    private notificationsService: NotificationsService,
    private cd: ChangeDetectorRef,
    private currentUserService: CurrentUserService,
    private settingsRepoService: SettingsRepositoryService,
    private el: ElementRef,
  ) {
    super(userRepository, baseComponentService);
  }

  public ngOnInit(): void {
    this.routeBackUrl = this.url(sitemap.system.children.misc.children.users);
    const { id } = this.route.parent.snapshot.params;

    /**
     * Getting an emails list is not needed only on the edition
     * mode of users when the user has no email.
     */
    if (id === 'new') {
      this.enterCreationMode();
      this.getEmailList();
    } else {
      this.loadResource(id).pipe(
        tap(user => {
          if (user.hasEmail) {
            this.getEmailList();
          }
        })
      ).subscribe();
    }
  }

  public getTitle = () => this.resource.displayName;

  public init(user: IUser = this.resourceDefault): void {
    const emailValidators = this.getEmailValidators(!user.hasEmail);

    this.initForm(this.form = this.fb.group({
      username: [user.username, [Validators.required, Validators.minLength(4)]],
      firstName: [user.firstName, [Validators.required]],
      lastName: [user.lastName, [Validators.required]],
      email: [user.email, [...emailValidators]],
      displayName: [user.displayName, [Validators.required]],
      tel: [user.tel, Validators.pattern(PHONENUMBER)],
      fax: [user.fax],
      location: [user.location],
      language: [user.language],
      isActive: [user.isActive],
      isAdmin: [user.isAdmin],
      avatarId: [user.avatarId],
      expirationDate: [user.expirationDate],
      noEmail: [!user.hasEmail], // not in dto, shown only in creation mode. Used to make email field optional
      password: [null, this.passwordValidators], // only required in creation mode
    }));

    if (!this.authService.getUser().isSuperAdmin) {
      this.setDisabledFields(['isAdmin']);
      this.disableMarkedFields();
    }
    this.withoutEmailChange();
    this.cd.detectChanges();
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cd.detectChanges();
  }

  public submit(): void {
    /**
     * On creation mode will be shown a message when email is
     * disallowed and user `isActive` will always be set `false`.
     *
     * On Edition mode
     * 1. when `isActive` is `true` and the value of `email` is disallowed,
     *    then will be shown a message and save action will be prevented.
     * 2. when `isActive` is `false`, then the user will be saved
     *    and no matter email is allowed or not.
     */
    const value = this.form.getRawValue();
    let password = '';
    if (value.email) {
      this.checkEmailDomainRestriction();
    } else {
      password = value.password;
    }

    /**
     * If the written domain does not exist in domains-list, then
     * `this.checkEmailDomainRestriction()` will check and add
     * domain-restriction-error to the email-control and form will become invalid.
     */
    if (!this.form.valid) {
      return;
    }

    const freshUser: IUser = this.creationMode
      ? { ...value, hasEmail: !!value.email }
      : { ...this.resource, ...value };

    if (this.creationMode) {
      this.createResource(freshUser).subscribe(createdResource => {
        // set nv-paper-view title
        this.usersHelperService.generateTitle(createdResource);

        // User without email has to have password which will be set
        // automatically after creation
        if (createdResource.email) {
          // new and active user has to get activation-email
          if (createdResource.isActive) {
            this.sendActivationLinkToUsersEmail();
          }
        } else {
          this.userRepository.setPassword(createdResource.id, password).subscribe();
        }
      });
    } else {
      let sendActivationEmail = false;

      /**
       * The reset-password modal will be shown only when the user has email
       * and the field `isActive` being changed from 'false' to 'true'.
       */
      const resetPasswordModal = !this.resource.isActive && freshUser.isActive && freshUser.hasEmail
        ? this.openResetPasswordModal()
        : of(false);

      resetPasswordModal.pipe(
        tap(confirmed => sendActivationEmail = confirmed),
        switchMap(() => this.updateResource(freshUser)),
      ).subscribe(updatedUser => {
          if (updatedUser.id === this.authService.getUser().id) {
            this.authService.updateProfile(updatedUser);
            this.currentUserService.avatarChanged();
          }

          if (!updatedUser.hasEmail && password?.length) {
            this.userRepository.setPassword(updatedUser.id, password).subscribe();
          }

          this.usersHelperService.generateTitle(updatedUser);

          if (sendActivationEmail) {
            this.sendActivationLinkToUsersEmail();
          }
        });
    }
  }

  private checkEmailDomainRestriction(): void {
    const email = this.form.get('email').value;
    const isActive = this.form.get('isActive').value;

    if (!isActive || this.isEmailDomainAllowed(email)) {
      return;
    }

    const wordingMessage = this.creationMode
      ? wording.system.notAcceptedEmailDomain
      : wording.system.userCantBeActivatedWithCurrentEmail;
    this.confirmationService.warning({
      wording: wordingMessage,
      okButtonWording: this.wording.general.ok,
      cancelButtonWording: null,
    });

    if (this.creationMode) {
      return;
    }
    this.setDisallowedEmailError();
  }

  private getEmailList(): void {
    this.settingsRepoService.fetchAll().subscribe(settings => {
      this.allowedEmailDomainsList = settings.system.emailDomains;
    });
  }

  private isEmailDomainAllowed(email: string): boolean {
    const emailParts = email.split('@');

    if (emailParts.length !== 2) {
      return false;
    }

    return this.allowedEmailDomainsList.some(item => {
      return item.domain === emailParts[1];
    });
  }

  private setDisallowedEmailError(): void {
    const control = this.form.get('email');
    const email = control.value;
    const isActive = this.form.get('isActive').value;

    const errors = !isActive || email && this.isEmailDomainAllowed(email)
      ? control.errors
      : {
        ...control.errors,
        disallowedEmail: { message: null },
      };

    control.setErrors(errors);
    control.markAsTouched();
  }

  public updateAvatar(upload: IUpload): void {
    this.form.patchValue({ avatarId: upload.id });
    this.cd.detectChanges();
  }

  private openResetPasswordModal(): Observable<boolean> {
    return this.confirmationService.confirm({
      okButtonWording: wording.system.sendPassword,
      cancelButtonWording: wording.general.cancel,
      wording: wording.system.resetCurrentPasswordMessage,
    });
  }

  private sendActivationLinkToUsersEmail() {
    const email = this.form.get('email').value;
    this.userRepositoryService.sendActivation(email).subscribe(() => {
      this.notificationsService.notify(
        NotificationType.Success,
        wording.general.sent,
        setTranslationSubjects(
          wording.general.requestSent,
          { EMAIL: email },
        ),
      );
    });
  }

  public withoutEmailChange() {
    const emailCtrl = this.form.get('email');
    const passCtrl = this.form.get('password');
    const noEmail = this.form.get('noEmail').value;
    if (noEmail) {
      // don't change to '',
      // backend only works with `null`, will thorw an error if ''
      emailCtrl.patchValue(null);
      emailCtrl.disable();
      if (this.creationMode) {
        passCtrl.enable();
        passCtrl.setValidators([Validators.required, ...this.passwordValidators]);
      } else {
        passCtrl.setValidators(this.passwordValidators);
      }
    } else {
      this.form.enabled && emailCtrl.enable();
      this.disableAndClearPasswordField(passCtrl);
    }
    const emailValidators = this.getEmailValidators(noEmail);
    emailCtrl.setValidators(emailValidators);
    emailCtrl.updateValueAndValidity();
    passCtrl.updateValueAndValidity();
  }

  private getEmailValidators(noEmail: boolean): ValidatorFn[] {
    return noEmail
      ? [Validators.email]
      : [Validators.required, Validators.email];
  }

  private disableAndClearPasswordField(passCtrl: AbstractControl) {
    passCtrl.disable();
    passCtrl.clearValidators();
  }
}
