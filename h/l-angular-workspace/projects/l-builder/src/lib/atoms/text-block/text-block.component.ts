import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'lavash-text-block',
  templateUrl: './text-block.component.html',
  styleUrls: ['./text-block.component.less']
})
export class TextBlockComponent implements OnInit {
  @Input() textBlock: string;
  _textBlockArr: any[] = [];
  @Input() highlight: boolean;
  @Input() highlightAllMatches: boolean;
  @Input() tooltip: boolean;

  ngOnInit() {
    this.textBlock = this.textBlock
        .replace(/\s+/g, ' '); // replace all extra whitespaces

    this.textBlock.split(' ').map( word => {
        const sentenceEndingChars = /[!.,?:;]/;
        word.replace(/\s/g, '');

        if (sentenceEndingChars.test(word)) {
            const exec = sentenceEndingChars.exec(word);
            const specialCharPosition = exec.index;

            if (word.length - specialCharPosition > 1) { // check if character is not the last in string
                if ( specialCharPosition === 0) { // if character is first member
                    const prevItem = this._textBlockArr[this._textBlockArr.length - 1];
                    prevItem.value = prevItem.value + word[0];
                    word = word.substr(1);
                } else { // if character is somewhre in the middle
                    const word1 = word.substr(0, specialCharPosition + 1);
                    const word2 = word.substr(specialCharPosition + 1);

                    this._textBlockArr.push({
                        value: word1
                    });

                    word = word2;
                }

            }
        }

        this._textBlockArr.push({
            value: word
        });
    });

    console.log('text block', this._textBlockArr)
  }

  onHover(mouseEvent, word, index) {
      if (this.highlight) {
          this._textBlockArr[index].state = 'highlight';
      }

      if (this.highlightAllMatches) {
          this._textBlockArr.filter( i => {
              if (i.value.toLocaleLowerCase() === word.toLowerCase()) {
                  i.state = 'highlightMatches';
              }
          });

          this._textBlockArr[index].state = 'highlight';
      }
  }

  offHover(mouseEvent, word, index) {
     this.resetState();
  }

  resetState() {
      this._textBlockArr.map( i => {
          delete i.state;
      });
  }
}
