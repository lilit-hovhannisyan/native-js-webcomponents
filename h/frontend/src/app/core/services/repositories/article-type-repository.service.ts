import { Injectable } from '@angular/core';
import { RepositoryService } from '../repository.service';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IArticleType, IArticleTypeDTO, IArticleTypeCreate, ArticleTypeKey } from '../../models/resources/IArticleType';

@Injectable({
  providedIn: 'root'
})
export class ArticleTypeRepositoryService extends AbstractRepository<
  IArticleType,
  IArticleTypeDTO,
  IArticleTypeCreate,
  RepoParams<IArticleType>,
  ArticleTypeKey
> {

  protected resourceType = 'articleType';
  public sortAsce = true;
  public sortKey = 'id';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess(item: IArticleTypeDTO): IArticleType { return item; }

  public getBaseUrl(params: RepoParams<IArticleType>): string {
    return `/api/article-types`;
  }
}

