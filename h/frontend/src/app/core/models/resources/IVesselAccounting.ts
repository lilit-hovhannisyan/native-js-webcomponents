import { IWithActive } from './IWithActive';
import { IResource } from './IResource';
import { IVoyage } from './IVoyage';

export type VesselAccountingKey = number;

interface ISharedProps extends IWithActive {
  from: string;
  until: string;
  endOfTermReason: END_OF_TERM_REASONS;
  vesselNo: number;
  vesselName: string;
  vesselId: number;
  mandatorId: number;
  owningCompanyId: number;
  bareboatCompanyId: number;
  commercialManagerId: number;
  technicalManagerId: number;
  bookkeepingCompanyId: number;
  crewingCompanyId: number;
  insolvencyAdministrator: string;
}

export interface IVesselAccountingCreate extends ISharedProps { }

export interface IVesselAccountingBase extends ISharedProps, IResource<number> { }

export interface IVesselAccountingDTO extends IVesselAccountingBase { }

export interface IVesselAccounting extends IVesselAccountingDTO { }

export interface IVesselAccountingFormattedDates extends IVesselAccounting {
  formattedFrom: string;
  formattedUntil: string;
}

export enum END_OF_TERM_REASONS {
  solid = 0,
  poolExit = 1,
  changeOfOwner = 2,
  contractTermination = 3,
  liquidation = 4,
}

export interface IVesselAccountingListItem extends IVesselAccounting {
  isFavorite?: boolean;
  charterName: string;
  vesselAccountingId?: VesselAccountingKey;
  vesselVoyages?: IVoyage[];
}
