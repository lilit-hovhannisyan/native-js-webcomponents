import { Component, TemplateRef } from '@angular/core';
export interface MePopupAccountI {
  title: string;
  content: TemplateRef<Component>;
}
