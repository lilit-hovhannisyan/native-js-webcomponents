import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { EHttpRequest } from 'src/app/core/models/http/EHttpRequest';
import { NotificationsService } from '../services/notification.service';
import { NotificationType } from '../models/INotification';
import { wording } from '../constants/wording/wording';
import { HttpErrorHandler } from './http-error-handler';
import { NOT_FOUND } from 'http-status-codes';

/**
 * Handles error and shows it as notification
 * @see: https://medium.com/@amcdnl/global-error-handling-with-angular2-6b992bdfb59c
 */

/**
 * ErrorHandler not working with lazyloaded child modules
 * @see: https://github.com/angular/angular/issues/22197
 */
/**
 * Notifies global errors
 *
 * calls HTTP error handler to handle HTTP erors
 */
@Injectable()
export class GlobalErrorHandler implements HttpInterceptor {
  private exceptionsStatuses = [
    NOT_FOUND,
  ];

  constructor(
    private httpErrorHandler: HttpErrorHandler,
    private notificationService: NotificationsService,
  ) { }

  public intercept(request: EHttpRequest<any>, next: HttpHandler): Observable<any> {
    return next.handle(request).pipe(
      catchError(error => {
        if (
          request.options && request.options.isNotificationHidden
          || this.exceptionsStatuses.some(status => status === error.status)
        ) {
          return throwError(error);
        }

        if (error instanceof HttpErrorResponse) {
          const errorWithWording = this.httpErrorHandler.handleError(error);

          return throwError(errorWithWording);
        }

        const IWordingError = {
          en: error.message,
          de: error.message,
        };

        this.notificationService.notify(NotificationType.Error, wording.general.error, IWordingError);

        return throwError(error);
      }));
  }
}
