import { Component } from '@angular/core';
import {Field} from 'l-builder';

@Component ({
  selector : 'app-contact',
  templateUrl : './contact.component.html',
  styleUrls : ['./contact.component.less']
})

export class ContactComponent {
    fieldsConfig: Field[] = [
        {
            component: 'input',
            name: 'name',
            descriptors: {
                placeholder: 'name'
            },
            validationRules: {
                required: {
                    value: true,
                    customErrMsg: 'Please, enter your name.'
                },
                min: {
                    value: 3,
                    customErrMsg: 'At least 3 symbols, please'
                }
            }
        },
        {
            component: 'input',
            name: 'phone',
            descriptors: {
                type: 'phone',
                placeholder: 'phone'
            },
            validationRules: {
                required: {
                    value: true,
                    customErrMsg: 'Contact number is important for me.'
                }
            }
        },
        {
            component: 'input',
            name: 'url',
            descriptors: {
                type: 'url',
                placeholder: 'social media url',
            },
        },
        {
            component: 'textarea',
            name: 'msg',
            descriptors: {
                placeholder: 'msg',
            },
            validationRules: {
                required: {
                    value: true,
                    customErrMsg: 'Please, provide some clue on the subject of your message.'
                },
                min: {
                    value: 15,
                    customErrMsg: 'At least 3 symbols, please'
                }
            }
        },
    ];

    onSubmit(event) {
        if (event.formErrorCount === 0) {
            // todo hadle real api
            console.log('Success, form is submited');
        }
    }
}
