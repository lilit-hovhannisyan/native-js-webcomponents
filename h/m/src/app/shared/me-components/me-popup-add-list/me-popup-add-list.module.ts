import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';

import { MeButtonModule } from '../me-button/me-button.module';
import { MePopupAddListComponent } from './me-popup-add-list.component';

import { MeIconsProviderModule } from '@shared-modules/me-icons.module';

@NgModule({
  imports: [CommonModule, MeIconsProviderModule, MatDialogModule, MeButtonModule],
  declarations: [MePopupAddListComponent],
  exports: [MePopupAddListComponent]
})
export class MePopupAddListModule {}
