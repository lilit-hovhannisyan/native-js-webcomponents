import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef,
  Type
} from '@angular/core';
import { NzTabComponent } from 'ng-zorro-antd/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

export interface TabComponent {
  component: Type<any>;
  inputs: Object;
  label: string;
  disabled?: boolean;
  initiallyActive?: boolean;
  id?: string; // can be used to identify a tab. e.g to open it by url-queryParams
}

/**
* This component uses nz-zorro nz-tab component to display our custom tabs.
* If a component nees to pass data up to the parent Component it needs to
* get callbacks as inputs, to use these.
*/
@Component({
  selector: 'nv-tab-manager',
  templateUrl: './tab-manager.component.html',
  styleUrls: ['./tab-manager.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabManagerComponent implements OnInit, OnDestroy {

  @Input() public tabComponents: TabComponent[];
  @ViewChild('vc', { static: true, read: ViewContainerRef }) public viewContainerRef: ViewContainerRef;

  public currentTabIndex = 0;
  public currentTabComponent: TabComponent;

  /**
   * While allTabsDisabled is true, it is not possible to switch to any other tab
   */
  public allTabsDisabled = new BehaviorSubject(false);
  public allTabsDisabled$ = this.allTabsDisabled.asObservable();
  public disabledTabs: number[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private componentFactoryResolver: ComponentFactoryResolver,
    private cdf: ChangeDetectorRef
  ) {
  }

  public ngOnInit(): void {
    // if a TabComponent has 'initiallyActive', activate it -> else: its the first tab
    this.tabComponents.forEach((tabComponent, i) => {
      if (tabComponent.initiallyActive) { this.currentTabIndex = i; }
    });
    const queryParam = this.route.snapshot.queryParamMap.get('tab');
    if (queryParam) {
      this.tabComponents.forEach((tabComponent, i) => {
        if (tabComponent.id === queryParam) {this.currentTabIndex = i; }
      });
    }
    this.loadComponent();
  }

  public onTabChange(changedTab: { index: number, tab: NzTabComponent }): void {
    this.currentTabIndex = changedTab.index;
    this.loadComponent();
    this.cdf.detectChanges();
  }

  public addQueryParam(): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {tab: this.currentTabComponent.id}
    });
  }

  public loadComponent(): void {
    this.currentTabComponent = this.tabComponents[this.currentTabIndex];

    const component = this.currentTabComponent.component;
    const inputs = this.currentTabComponent.inputs;
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);

    this.viewContainerRef.clear();

    const componentRef = this.viewContainerRef.createComponent(componentFactory);
    Object.keys(inputs).forEach((key) => { componentRef.instance[key] = inputs[key]; });

    componentRef.instance['disableTabs'] = this.disableAllTabs;
    componentRef.instance['enableTabs'] = this.enableAllTabs;
    this.cdf.detectChanges();
    this.addQueryParam();
  }

  public enableAllTabs = (): void => this.allTabsDisabled.next(false);
  public disableAllTabs = (): void => this.allTabsDisabled.next(true);

  public ngOnDestroy(): void {
    this.viewContainerRef.clear();
  }
}
