import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { WordingService } from './wording.service';
import { AccountService } from './account.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { SettingsService } from './settings.service';
import { BrowserTitleService } from 'src/app/routing/services/browser-title.service';
import { CommonNotificationsService } from './helperService/common-notifications.service';

@Injectable({
  providedIn: 'root'
})
export class BaseComponentService {

  constructor(
    public router: Router,
    public wordingService: WordingService,
    public accountService: AccountService,
    public messageService: NzMessageService,
    public authService: AuthenticationService,
    public nzModalService: NzModalService,
    public browserTitleService: BrowserTitleService,
    public settingsService: SettingsService,
    public confirmationService: ConfirmationService,
    public commonNotificationsService: CommonNotificationsService,
  ) {
  }
}
