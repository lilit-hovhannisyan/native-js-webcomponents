import { DigitalProfileModel } from '../../shared/models/digital-profile/digital-profile.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { plainToClass } from 'class-transformer';
// import { CertificateModel } from '@shared/models/certificate/certificate.model';

@Injectable()
export class DigitalProfileStoreService {
  private readonly _digitalprofile = new BehaviorSubject<DigitalProfileModel[]>([]);
  readonly digitalprofile$ = this._digitalprofile.asObservable();

  get digitalprofile(): DigitalProfileModel[] {
    return this._digitalprofile.getValue();
  }

  set digitalprofile(data: DigitalProfileModel[]) {
    this._digitalprofile.next(data);
  }

  getDigitalProfile(name: string): DigitalProfileModel {
    return this.findDomain(this._digitalprofile.getValue(), name);
  }

  private findDomain(digitalProfileArray: DigitalProfileModel[] = [], name: string): DigitalProfileModel {
    let result!: DigitalProfileModel;
    for (const domain of digitalProfileArray) {
      const matchDomain: DigitalProfileModel = domain.domainName === name ? domain : result;

      if (matchDomain) {
        result = plainToClass(DigitalProfileModel, { ...matchDomain });
        return result;
      }
    }
    return result;
  }
}
