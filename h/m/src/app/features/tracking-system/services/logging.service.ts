import { Inject, Injectable, OnDestroy } from '@angular/core';
import { ActivationEnd, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { v4 as uuidv4 } from 'uuid';
import { Observable, Subject, Subscription } from 'rxjs';
import { LocalStorageService } from './local-storage.service';
import RouterLoadedComponentLogI from '../interfaces/router-loaded-component-log.interface';
import RouterNavigationStartLogI from '../interfaces/router-navigation-start-log.interface';
import ComponentDataI from '../interfaces/component-data.interface';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { TrackerConfigI } from '..';
import EventTypes from '../enums/event-types.enum';
import { SKIP_ME_TRACKING_SYSTEM_REQUEST } from '../constants';
import BackendConfigI from '../interfaces/backend-config.interface';
import { ME_TRACKING_BASE_API_URL } from '@shared/constants';

const ME_LOG_NAME = 'ME_USER_ACTIVITY_LOGS';
const ME_LOG_ARCHIVE = 'ME_USER_ACTIVITY_LOGS_ARCHIVE';
const SEPARATOR = '?**?';
const SEND_LOGS_INTERVAL = 15000;
const LOGGER_CLIENT_VERSION = '0.0.1';

// const TRACKING_BACKEND_ENDPOINT = 'https://httpbin.org/response-headers?LOGGER';
const TRACKING_BACKEND_ENDPOINT = `${ME_TRACKING_BASE_API_URL}/events/batch`;
const TRACKING_CONFIG_ENDPOINT = `${ME_TRACKING_BASE_API_URL}/server/info`;

@Injectable({
  providedIn: 'root'
})
export class LoggingService implements OnDestroy {
  private _routerEventsSubscription!: Subscription;
  private _logEventsSubscription!: Subscription;
  private _stateSubject = new Subject<string>();

  private _orderNumber: number = 0;
  private _state: Array<string> = [];
  private _pending: boolean = false;
  private _sessionId!: string;
  private _appVersion: string;
  private _enabled: boolean = true;
  private _enableDebugging: boolean = false;

  private _userActionId: string | null = null;
  private _additionalHeaders: Array<{ name: string; value: string }> = [];
  private _initialServerTime: number = 0;
  private _initialLocalTime: number = 0;
  private _maxEventsInBatch: number = 500;

  constructor(
    private router: Router,
    private localStorageService: LocalStorageService,
    private http: HttpClient,
    @Inject('tracker-config')
    trackerConfig: TrackerConfigI
  ) {
    // set appVersion
    this._appVersion = trackerConfig.appVersion;
    // is tracking enabled
    this._enabled = trackerConfig.trackerEnabled;
    // enable Debugging print logs in the console
    if (trackerConfig.enableDebugging) {
      this._enableDebugging = true;
    }

    if (!this._enabled) {
      return;
    }

    // create uuid for this sequence
    this._sessionId = uuidv4();

    // get saved logs from local storage
    const savedLogs: string = this.localStorageService.get(ME_LOG_NAME);
    if (savedLogs) {
      this._state = [...(savedLogs.split(SEPARATOR) || [])];
    }

    // router events
    this._routerEventsSubscription = this.router.events.subscribe((event) => {
      if (event instanceof ActivationEnd) {
        // tslint:disable-next-line:no-any
        const component: any = (event as ActivationEnd).snapshot.component;
        if (component) {
          const log: RouterLoadedComponentLogI = {
            eventType: EventTypes.ROUTER_LOADED_COMPONENT,
            data: { componentName: component.name },
            userActionId: this._userActionId
          };

          this.emitLog(log);
        }
      }

      if (event instanceof NavigationStart) {
        const log: RouterNavigationStartLogI = {
          eventType: EventTypes.ROUTER_NAVIGATION_START,
          data: { url: event.url },
          userActionId: this._userActionId
        };

        this.emitLog(log);
      }

      if (event instanceof NavigationEnd) {
        // Hide loading indicator
      }

      if (event instanceof NavigationError) {
        // Hide loading indicator
        // Present error to user
        // console.log(event.error);
      }
    });

    // state
    this._logEventsSubscription = this._stateSubject.asObservable().subscribe((data: string) => {
      /**
       * check if we are not in pending state and have logs which we have sent to backend but didn't get response yet
       * get that logs from local storage and append to current stack
       */
      if (!this._pending) {
        const archivedLogs: string = this.localStorageService.get(ME_LOG_ARCHIVE);
        if (archivedLogs) {
          this._state = [...(archivedLogs.split(SEPARATOR) || []), ...this._state];
        }
      }
      this._state.push(data);
      const valueToSave = this._state.join(SEPARATOR);
      this.localStorageService.set(ME_LOG_NAME, valueToSave);
    });

    let headersForGet: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      [SKIP_ME_TRACKING_SYSTEM_REQUEST]: SKIP_ME_TRACKING_SYSTEM_REQUEST
    });
    for (const headerRow of this._additionalHeaders) {
      headersForGet = headersForGet.append(headerRow.name, headerRow.value);
    }
    const httpOptionsForGet = {
      headers: headersForGet
    };

    // setting timeout because interceptor is dependant on this service
    setTimeout(() => {
      this.http
        .get<BackendConfigI>(TRACKING_CONFIG_ENDPOINT, httpOptionsForGet)
        .pipe(retry(2), catchError(this.handleError))
        .subscribe((config: BackendConfigI) => {
          // console.log(config);
          this._initialServerTime = config.serverTime.time;
          this._initialLocalTime = new Date().getTime();
          if (config.maxEventsInBatch > 0) {
            this._maxEventsInBatch = config.maxEventsInBatch;
            console.log(this._maxEventsInBatch);
          }
          this.registerSendLogsInterval();
        });
    });
  }

  getCalculatedServerTime(): number {
    const d = new Date().getTime();
    const offset = d - this._initialLocalTime;
    return this._initialServerTime + offset;
  }

  private registerSendLogsInterval(): void {
    // send logs
    setInterval(() => {
      if (this._state.length > 0 && !this._pending) {
        if (this._enableDebugging) {
          console.warn('----- SEND TO TRACKING SERVER -----: ', this._state);
        }

        let headers: HttpHeaders = new HttpHeaders({
          'Content-Type': 'application/json',
          [SKIP_ME_TRACKING_SYSTEM_REQUEST]: SKIP_ME_TRACKING_SYSTEM_REQUEST
        });
        for (const headerRow of this._additionalHeaders) {
          headers = headers.append(headerRow.name, headerRow.value);
        }

        const httpOptions = {
          headers
        };

        // parse to JSON
        const dataToSend = this._state.map((str) => JSON.parse(str));

        // send only if we have something to send
        if (dataToSend.length > 0) {
          this.http
            .post<Array<string>>(TRACKING_BACKEND_ENDPOINT, dataToSend, httpOptions)

            .pipe(retry(2), catchError(this.handleError))
            .subscribe(() => {
              this.localStorageService.remove(ME_LOG_ARCHIVE);
              // this.localStorageService.localStorageSpace();
              this._pending = false;
            });

          const logsFromLS = this.localStorageService.get(ME_LOG_NAME);
          if (logsFromLS) {
            try {
              this.localStorageService.set(ME_LOG_ARCHIVE, logsFromLS);
            } catch (e) {
              if (this._enableDebugging) {
                console.warn('Local storage is full, emptying it...');
              }
              this.localStorageService.remove(ME_LOG_ARCHIVE);
            }
          }
          this._pending = true;
          this._state = [];
          this.localStorageService.remove(ME_LOG_NAME);
        }
      }
    }, SEND_LOGS_INTERVAL);
  }

  public get enabled(): boolean {
    return this._enabled;
  }

  handleError(error: HttpErrorResponse): Observable<never> {
    if (this._enableDebugging) {
      console.error('error while sending TRACKING logs: ', error.message);
    }
    // return an observable with a user-facing error message
    return throwError('(Front) Something bad happened; please try again later.');
  }

  public get enableDebugging(): boolean {
    return this._enableDebugging;
  }

  public setUserActionId(): void {
    const uuId = uuidv4();
    this._userActionId = uuId;
  }

  public deleteUserActionId(): void {
    this._userActionId = null;
  }

  public getUserActionId(): string | null {
    return this._userActionId;
  }

  public getRequestUniqueIdentifier(): string {
    return this._sessionId + '_' + this._orderNumber + 1;
  }
  // tslint:disable-next-line:no-any
  emitLog(data: any): void {
    if (data) {
      data.sessionId = this._sessionId;
      data.loggerClientVersion = LOGGER_CLIENT_VERSION;
      data.orderNumber = ++this._orderNumber;
      data.clientDate = new Date().toISOString();
      const timeStamp = this.getCalculatedServerTime();
      data.serverDate = new Date(timeStamp).toISOString();
      data.appVersion = this._appVersion;
      this._stateSubject.next(JSON.stringify(data));
      if (this._enableDebugging) {
        console.warn(data);
      }
    }
  }
  getSessionId(): string {
    return this._sessionId;
  }

  getOrderNumber(): number {
    return this._orderNumber;
  }

  addComponentDataToEventLog(userActionId: string | null, data: ComponentDataI): void {
    // tslint:disable-next-line:no-any
    const logsArr: Array<any> = this._state.map((logStr) => JSON.parse(logStr));

    const foundLogEntry = logsArr.find((log) => log.userActionId === userActionId);

    if (foundLogEntry) {
      foundLogEntry.componentsData = [data, ...foundLogEntry.componentsData];
    }

    this._state = [];
    logsArr.forEach((log) => {
      this._state.push(JSON.stringify(log));
    });
    const valueToSave = this._state.join(SEPARATOR);
    this.localStorageService.set(ME_LOG_NAME, valueToSave);
  }

  public appendToHeaders(name: string, value: string): void {
    for (const entry of this._additionalHeaders) {
      if (entry.name === name) {
        entry.value = value;
        return;
      }
    }
    this._additionalHeaders.push({ name, value });
  }

  public removeFromTheHeaders(name: string): void {
    this._additionalHeaders = this._additionalHeaders.filter((header) => header.name !== name);
  }

  ngOnDestroy(): void {
    if (this._routerEventsSubscription) {
      this._routerEventsSubscription.unsubscribe();
    }
    if (this._routerEventsSubscription) {
      this._logEventsSubscription.unsubscribe();
    }
  }
}
