export interface MeLabelI {
  type: 'regular' | 'inversed';
  color: 'primary' | 'info' | 'success' | 'danger' | 'warning';
  margin: 'none' | 'small';
}
