import { IResource } from './IResource';

export type ShipyardTypeKey = number;

interface ISharedProps {
  label: string;
}

export interface IShipyardTypeBase extends ISharedProps, IResource<number> { }

export interface IShipyardTypeCreate extends ISharedProps { }

export interface IShipyardTypeDTO extends IShipyardTypeBase { }

export interface IShipyardType extends IShipyardTypeDTO { }
