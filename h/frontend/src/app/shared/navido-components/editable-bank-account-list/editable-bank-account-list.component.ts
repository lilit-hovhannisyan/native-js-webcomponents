import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { BankAccountDetailViewComponent } from './detail-view/bank-account-detail-view.component';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { BankAccountRepositoryService } from './../../../core/services/repositories/bank-account-repository.service';
import { BankAccountMainService } from './../../../core/services/mainServices/bank-account-main.service';
import { IBankAccountRow, BankAccountKey, IBankAccountFull } from './../../../core/models/resources/IBankAccount';
import { Component, ChangeDetectionStrategy, OnInit, Input } from '@angular/core';
import { getBankGridConfig } from './bankGridConfig';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { NvGridConfig } from 'nv-grid';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { TranslatePipe } from '../../pipes/translate.pipe';
import { SettingsService } from 'src/app/core/services/settings.service';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { orderBy } from 'lodash';
@Component({
  selector: 'nv-editable-bank-account-list',
  templateUrl: './editable-bank-account-list.component.html',
  styleUrls: ['./editable-bank-account-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditableBankAccountListComponent implements OnInit {

  public bankAccountGridConfig: NvGridConfig;
  @Input() public companyId: number;
  private modalRef?: NzModalRef;

  public dataSource$: Observable<IBankAccountRow[]>;

  constructor(
    private mainService: BankAccountMainService,
    private bankAccountRepo: BankAccountRepositoryService,
    private modalService: NzModalService,
    public gridConfigService: GridConfigService,
    public companyService: CompanyRepositoryService,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private settingsService: SettingsService
  ) { }

  public ngOnInit(): void {
    const params = { companyId: this.companyId };

    this.dataSource$ = this.mainService.getStream(params)
      .pipe(
        map((bankAccounts: IBankAccountFull[]) =>
          orderBy(this.mainService.toRows(bankAccounts),
            // To ignore accents when sorting like Českomoravská záruční a rozvojová banka
            [
              b => b.bankName.normalize('NFD').replace(/[\u0300-\u036f]/g, ''),
              b => b.currencyLabel,
              // iban can be null and null will not be sorted correctly
              b => b.iban || '',
              b => b.accountNo
            ],
            ['asc', 'asc', 'asc', 'asc'])
        )
      );

    const gridActions: IGridConfigActions = {
      edit: (row: IBankAccountRow) => this.openDetailViewModal(row.id),
      delete: (row: IBankAccountRow) => this.deleteRowClicked(row.id),
      add: () => this.openDetailViewModal(),
    };

    const bankAccountsNode = sitemap.system.children.misc.children.companies.children.id.children.addresses;
    this.bankAccountGridConfig = getBankGridConfig(
      gridActions,
      (operation: number) => this.authService.canPerformOperationOnNode(operation, bankAccountsNode),
      {
        currency: (bankAccount: IBankAccountRow) =>
          this.formatCurrencyLabel(bankAccount),
      }
    );
  }

  public openDetailViewModal(bankAccountId?: BankAccountKey): void {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.accounting.bankAccount[this.settingsService.language],
      nzClosable: false,
      nzContent: BankAccountDetailViewComponent,
      nzComponentParams: {
        bankAccountId: bankAccountId,
        companyId: this.companyId,
        closeModal: () => this.modalRef.destroy()
      },
      nzFooter: null
    });
  }

  public deleteRowClicked = (bankAccountId: BankAccountKey): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: '' },
    }).subscribe(confirmed => confirmed && this.delete(bankAccountId));
  }
  public delete(id: BankAccountKey): void {
    this.bankAccountRepo.delete(id, {})
      .subscribe();
  }

  public formatCurrencyLabel = (bankAccount: IBankAccountRow): string => {
    const translatePipe = new TranslatePipe(this.settingsService);
    return `${bankAccount.currency.isoCode} / ${translatePipe
      .transform(bankAccount.currencyLabel)}`;
  }
}
