import { Type } from 'class-transformer';
import { CertificateAssociationModel } from './certificate-association.model';

export class CertificateAssociationEntitiesModel {
  @Type(() => CertificateAssociationModel)
  entities: CertificateAssociationModel[] = [];
}
