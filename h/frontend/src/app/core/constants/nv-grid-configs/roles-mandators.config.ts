import { NvGridButtonsPosition, NvGridConfig, NvFilterControl, NvGridRowSelectionType, NvColumnDataType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getGridConfig = (): NvGridConfig => {
  return ({
    gridName: 'roleMandatorsGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'no',
        title: wording.accounting.no,
        width: 50,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'name',
        title: wording.general.name,
        width: 264,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'currencyName',
        title: wording.accounting.currency,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Select,
          selectValues: []
        }
      }, {
        key: 'companyName',
        title: wording.accounting.companyName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'isActive',
        title: wording.accounting.active,
        width: 85,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isCostAccounting',
        title: wording.accounting.costAccounting,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isInvoiceWorkflow',
        title: wording.accounting.invoiceWorkflow,
        width: 154,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      }, {
        key: 'startFiscalYear',
        title: wording.accounting.startFiscalYear,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'mandatorType',
        title: wording.accounting.mandatorType,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'accountingType',
        title: wording.accounting.accountingType,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'firstBookingYear',
        title: wording.accounting.firstBookingYear,
        width: 154,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'start',
        title: wording.accounting.start,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.DateTime,
        filter: {
          values: []
        }
      }
    ]
  });
};
