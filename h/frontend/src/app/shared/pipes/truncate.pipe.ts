import { Pipe, PipeTransform } from '@angular/core';

/**
 * This pipe is used to truncate string and add to it's end '...' if it's length
 * is bigger then the limit of chars
 */
@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {
  public transform(value: any, limit = 50, completeWords = false, ellipsis = '...'): string {
    if (!value) { return ''; }
    limit = limit - ellipsis.length;
    if (typeof value !== 'string') {
      value = value.toString();
    }
    if (limit >= value.length) { return value; }
    if (completeWords) {
      limit = value.substr(0, limit).lastIndexOf(' ');
    }
    return `${value.substr(0, limit)}${ellipsis}`;
  }
}
