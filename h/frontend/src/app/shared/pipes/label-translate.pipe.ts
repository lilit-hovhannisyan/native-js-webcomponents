import { Pipe, PipeTransform } from '@angular/core';
import { invalidPipeArgumentError } from 'src/app/shared/pipes/invalid-pipe-argument-error';
import { SettingsService } from 'src/app/core/services/settings.service';
import { IWithLabels } from 'src/app/core/models/resources/IWithLabels';
import { Language } from 'src/app/core/models/language';

/**
 * returns obj.labelEn or obj.labelDe according to selected language
 */
@Pipe({
  name: 'labelTranslate',
  pure: false
})
export class LabelTranslatePipe implements PipeTransform {

  constructor(private settingsService: SettingsService) {
  }

  public transform(wording: IWithLabels): string | null {
    if (!wording) { return null; }

    if (typeof wording !== 'object'
      || typeof wording.labelEn !== 'string'
      || typeof wording.labelDe !== 'string'
    ) {
      throw invalidPipeArgumentError(LabelTranslatePipe, wording);
    }

    return this.settingsService.language === Language.EN
      ? wording.labelEn
      : wording.labelDe;
  }
}
