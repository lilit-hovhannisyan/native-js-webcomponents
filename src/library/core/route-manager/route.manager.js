import {
  createElementWithAttributes,
  removeStaticAtributesFromElNotToRender,
  updateHistoryAndDocumentTitle,
  traverseInShadowAndGetElement
} from '../../helpers/random.helpers.js';
import {BaseComponent} from '../base-component/base.component.js';

export const updateOutlet = (
  currentRoute,
  routerOutlet = traverseInShadowAndGetElement(document.body, 'route-outlet')
) => {
  routerOutlet.innerHTML = `<${currentRoute.component}></${currentRoute.component}>`;
  document.title = currentRoute.title || document.title;
}

export const getParsedUrl = () => {
  // todo detect dynamic routes
  let path = location.pathname;
  const [...pathArr] = path.replace(/(^\/+|\/+$)/g, '').split('/');

  path = pathArr[0] === '' ? '/' : pathArr.length === 1 ? pathArr[0] : pathArr.join('/');

  return path;
}


export class RouteManager extends BaseComponent {
  routes;

  constructor(routes) {
    super();
  }

  get route() {
    return location.pathname;
  }

  get outlet() {
    return this.shadow.querySelector('route-outlet')
  }

  getCurrentRoute() {
    const currentPath = getParsedUrl();

    let currentRoute = this.routes.find(r => r.path === currentPath);

    if (!currentRoute) { // check if there is dynamic match
      console.log(currentPath)
      currentRoute = this.routes
        .filter(r => r.path.indexOf(':') >= 0)
        .find(r => currentPath.indexOf(r.path.split('/')[0]) >= 0);
    }

    if (!currentRoute) { // if no ordinary or dynamic route redirect to 404
      window.history.pushState(null, null, 'not-found');
    }

    if (currentRoute?.redirectTo) {
      currentRoute = this.routes.find(r => r.component === currentRoute.redirectTo);
    }

    return currentRoute;
  }

  createOutlet(currentRoute) {
    const config = {
      tagName: 'route-outlet',
      subElements: [
        {
          tagName: currentRoute.component,
        }
      ]
    };
    const el = createElementWithAttributes(config);
    removeStaticAtributesFromElNotToRender(this, ['routes']);

    this.shadow.appendChild(el.cloneNode(true));
  }

  connectedCallback() {
    this.routes = eval(this.getAttribute('routes'))();

    const currentRoute = this.getCurrentRoute();
    this.createOutlet(currentRoute);


    this.handleBrowserBackForward();
    this.handleBrowserRefresh();
  }

  handleBrowserBackForward = () => {
    window.addEventListener('popstate', (ev) => {
      const currentRoute = this.getCurrentRoute();
      updateOutlet(currentRoute, this.outlet);
    });
  }

  handleBrowserRefresh = () => {
    window.addEventListener('beforeunload', (event) =>  {
      //event.preventDefault();
      //debugger
      //alert('Are you really want to perform the action?')
    });
  }

  disconnectedCallback() {
    // remove all listeners
    window.removeEventListener('popstate', this.handleBrowserBackForward);
    window.removeEventListener('beforeunload', this.handleBrowserRefresh);
  }
}