import { Component, ChangeDetectionStrategy, Input, HostBinding, Output, EventEmitter, ViewChildren, QueryList, ChangeDetectorRef, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { VesselTaskTodoItemComponent } from '../vessel-task-todo-item/vessel-task-todo-item.component';
import { IVesselTaskTodoItem } from 'src/app/core/models/resources/IVesselTaskTodoItem';
import { TaskLanguageType } from 'src/app/core/models/resources/IVesselTask';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { VesselTaskHelperService } from '../vessel-task/vessel-task-helper.service';

@Component({
  selector: 'nv-vessel-task-todo-list',
  templateUrl: './vessel-task-todo-list.component.html',
  styleUrls: ['./vessel-task-todo-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VesselTaskTodoListComponent implements OnInit, OnDestroy {
  public wording = wording;
  public form: FormGroup;

  /** For firing event when component destroys. */
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  /** Sets [disabled] html-attribute. */
  @HostBinding('attr.disabled')
  @Input()
  public disabled = false;

  /** Language type from task. By default is system language. */
  @Input() public languageType: TaskLanguageType;
  /** Todo-items array which should be passed from parent component. */
  @Input() public value: IVesselTaskTodoItem[] = [];
  /**
   * When `true` - `Name EN` and `Name DE` texts are hidden
   * and if checklist items label with current system language is missing
   * will be shown the value of opposite label.
   */
  @Input() public showOppositeLangueIfFieldEmpty = false;

  /** Emits deleted item. */
  @Output() public delete: EventEmitter<IVesselTaskTodoItem> = new EventEmitter(true);
  /** Emits added new item. */
  @Output() public add: EventEmitter<IVesselTaskTodoItem> = new EventEmitter(true);
  /** Emits event only when one of tasks statuses changes. */
  @Output() public statusChange: EventEmitter<void> = new EventEmitter(true);
  /** Emits event when some of inputs in one of rows got focused. */
  @Output() public focus: EventEmitter<FocusEvent> = new EventEmitter(true);
  /** Emits event when some of inputs in one of rows got blurred. */
  @Output() public blur: EventEmitter<FocusEvent> = new EventEmitter(true);

  /**
   * Emits every time when
   * 1. new todo-item being added
   * 2. todo-item being deleted
   * 3. one of todo-items being changed
   */
  @Output() private valueChange: EventEmitter<IVesselTaskTodoItem[]>
    = new EventEmitter(true);

  @ViewChildren(VesselTaskTodoItemComponent, { read: VesselTaskTodoItemComponent })
  private todoItemsList: QueryList<VesselTaskTodoItemComponent>;

  /** Shows if color picker is opened. */
  public isColorPickerOpened = false;

  /** Shows if exists focused input in one of rows to disable drag-drop. */
  public hasFocusedInput = false;

  /** Shows is system language chosen English or not. */
  public isSystemEnglish = false;

  public get showEnLabel(): boolean {
    return !(this.showOppositeLangueIfFieldEmpty && this.disabled)
      && (this.languageType === TaskLanguageType.All || this.isSystemEnglish);
  }
  public get showDeLabel(): boolean {
    return !(this.showOppositeLangueIfFieldEmpty && this.disabled)
      && (this.languageType === TaskLanguageType.All || !this.isSystemEnglish);
  }
  public get titleNzSpan(): number {
    return (this.showOppositeLangueIfFieldEmpty && this.disabled) ? 7 : 24;
  }


  constructor(
    private cdr: ChangeDetectorRef,
    public helperService: VesselTaskHelperService,
  ) {}

  public ngOnInit(): void {
    this.handleIsEnglishChange();
  }

  /** Keeps `isEnglish` value. */
  private handleIsEnglishChange(): void {
    this.helperService.isEnglish
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(isEnglish => this.isSystemEnglish = isEnglish);
  }

  /** Emits event with whole list value. */
  public handleTodoChange(): void {
    // to emit event
    this.valueChange.emit(this.value);
    this.cdr.detectChanges();
  }

  /**
   * Adds new item at specified index and emits add and change events.
   * @param atIndex index of item which below will be added new item.
   */
  public addTodo(atIndex = 0): void {
    const newItem: IVesselTaskTodoItem = {} as IVesselTaskTodoItem;
    this.value.splice(atIndex, 0, newItem);
    this.add.emit(newItem);
    this.handleTodoChange();
  }

  /**
   * Deletes item from `value` and emits event with deleted item.
   * @param index index of item which should be delete
   */
  public deleteTodo(index: number): void {
    const deletedItem: IVesselTaskTodoItem = this.value.splice(index, 1)[0];

    this.delete.emit(deletedItem);
    this.handleTodoChange();
  }

  /** Moves dropped item in the array and emits change event. */
  public handleDrop(event: CdkDragDrop<IVesselTaskTodoItem[]>) {
    moveItemInArray(this.value, event.previousIndex, event.currentIndex);
    this.handleTodoChange();
  }

  /** Validates all todo items and scrolls to invalid item. */
  public validate(): void {
    this.todoItemsList.forEach(todoItemComponent => {
      todoItemComponent.validate();
      if (todoItemComponent.isInvalid) {
        todoItemComponent.scrollIntoView();
      }
    });
  }

  /** Keeps if exists focused input in one of rows. */
  private setFocusState(isFocused: boolean): void {
    this.hasFocusedInput = isFocused;
  }

  /** Bubbles focus event. */
  public handleFocus(event: FocusEvent): void {
    this.setFocusState(true);
    this.focus.emit(event);
  }

  /** Bubbles blur event. */
  public handleBlur(event: FocusEvent): void {
    this.setFocusState(false);
    this.blur.emit(event);
  }

  /** Returns false when all todo items are valid. */
  public isInvalid(): boolean {
    return this.todoItemsList.some(todoItemComponent => {
      return todoItemComponent.isInvalid;
    });
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
