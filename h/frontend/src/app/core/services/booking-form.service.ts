import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class BookingFormService {
  public bookingFormSubject: BehaviorSubject<FormGroup> = new BehaviorSubject<FormGroup>(null);
  /**
    * used to notify booking form that we are in book invoice mode when we are
    * using the Take invoice feature without changing the route params
   */
  public bookInvoiceMode = false;

  public getLeadingAccountValue(): boolean | undefined {
    if (this.bookingFormSubject.value) {
      return this.bookingFormSubject
        .value
        .get('leadingAccount')
        .value;
    }
  }
}
