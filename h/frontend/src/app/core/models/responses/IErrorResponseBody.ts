import { IResponseBody } from './IResponseBody';

export interface IErrorResponseBody<T> extends IResponseBody {
  error: T;
}
