import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { GroupModel } from '@shared/models/groups/group.model';
import { UserModel } from '@shared-models/user/user.model';

@Injectable()
export class UsersStoreService {
  // Users Entities
  private readonly _userEntities = new BehaviorSubject<UserModel[]>([]);
  readonly userEntities$ = this._userEntities.asObservable();

  // Users Total Count
  private readonly _userTotalCount = new BehaviorSubject<number>(0);
  readonly userTotalCount$ = this._userTotalCount.asObservable();

  // User nextID for lazyload
  private readonly _userNextId = new BehaviorSubject<number>(0);
  readonly userNextId$ = this._userNextId.asObservable();

  // User groups entities
  private readonly _userGroupsEntities = new BehaviorSubject<GroupModel[]>([]);
  readonly userGroupsEntities$ = this._userGroupsEntities.asObservable();

  // Users groups total count
  private readonly _userGroupsTotalCount = new BehaviorSubject<number>(0);
  readonly userGroupsTotalCount$ = this._userGroupsTotalCount.asObservable();

  // User groups for lazyload
  private readonly _userGroupsNextId = new BehaviorSubject<number>(0);
  readonly userGroupsNextId$ = this._userGroupsNextId.asObservable();

  get userEntities(): UserModel[] {
    return this._userEntities.getValue();
  }

  set userEntities(data: UserModel[]) {
    this._userEntities.next(data);
  }

  appenduserEntities(data: UserModel[]): void {
    this.userEntities = [...this.userEntities, ...data];
  }

  get userTotalCount(): number {
    return this._userTotalCount.getValue();
  }

  set userTotalCount(data: number) {
    this._userTotalCount.next(data);
  }

  get userNextId(): number {
    return this._userNextId.getValue();
  }

  set userNextId(data: number) {
    this._userNextId.next(data);
  }

  get userGroupsEntities(): GroupModel[] {
    return this._userGroupsEntities.getValue();
  }

  set userGroupsEntities(data: GroupModel[]) {
    this._userGroupsEntities.next(data);
  }

  get userGroupsTotalCount(): number {
    return this._userTotalCount.getValue();
  }

  set userGroupsTotalCount(data: number) {
    this._userTotalCount.next(data);
  }

  get userGroupsNextId(): number {
    return this._userGroupsNextId.getValue();
  }

  set userGroupsNextId(data: number) {
    this._userGroupsNextId.next(data);
  }
}
