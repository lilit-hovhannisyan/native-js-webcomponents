import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { BankRepositoryService } from './../../../../core/services/repositories/bank-repository.service';
import { BankAccountRepositoryService } from './../../../../core/services/repositories/bank-account-repository.service';
import { CompanyKey } from './../../../../core/models/resources/ICompany';
import { IBankAccount, BankAccountKey } from './../../../../core/models/resources/IBankAccount';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { forkJoin, Observable } from 'rxjs';
import { startWith, tap, debounceTime, map } from 'rxjs/operators';
import { IBank, BankKey } from 'src/app/core/models/resources/IBank';
import { CountryRepositoryService, MAX_COUNTRIES } from 'src/app/core/services/repositories/country-repository.service';
import { ICountry } from 'src/app/core/models/resources/ICountry';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { AuthenticationService, ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { NotificationType } from 'src/app/core/models/INotification';
import { NvGridConfig } from 'nv-grid';
import { getGridConfig as bankGridConfig } from 'src/app/core/constants/nv-grid-configs/banks.config';
import { ConfirmationService } from '../../../../core/services/confirmation.service';
import { AutobankRuleRepositoryService } from '../../../../core/services/repositories/autobank-rule-repository.service';
import { MandatorRepositoryService } from '../../../../core/services/repositories/mandator-repository.service';
import { IAutobankRule } from '../../../../core/models/resources/IAutobankRule';

@Component({
  templateUrl: './bank-account-detail-view.component.html',
  styleUrls: ['./bank-account-detail-view.component.scss'],
})
export class BankAccountDetailViewComponent implements OnInit {
  private banks: IBank[] = [];
  private countries: ICountry[] = [];
  public banks$: Observable<IBank[]>;

  public bankAccountsNode = sitemap.system.children.misc.children.companies.children.id.children.bankAccounts;
  public bankAccountsZone = SitemapEntry.getByNode(this.bankAccountsNode).toZone();

  @Input() public bankAccountId: BankAccountKey;
  @Input() public companyId: CompanyKey;
  @Input() public closeModal: (IBankAccountCompact?) => void;

  public creationMode: boolean;
  public form: FormGroup;

  public ACL_ACTIONS = ACL_ACTIONS;
  public wording = wording;
  public bankGridConfig: NvGridConfig = bankGridConfig({}, () => false);

  constructor(
    private bankAccountRepo: BankAccountRepositoryService,
    private bankRepo: BankRepositoryService,
    private fb: FormBuilder,
    private countriesService: CountryRepositoryService,
    private authService: AuthenticationService,
    private notificationService: NotificationsService,
    private confirmationService: ConfirmationService,
    private autobankRuleRepo: AutobankRuleRepositoryService,
    private mandatorRepo: MandatorRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.banks$ = this.bankRepo.fetchAll({})
      .pipe(
        tap(banks => this.banks = banks),
        map(banks => banks.filter(b => b.isActive)),
      );
    this.initForm();

    this.countriesService.fetchAll({ queryParams: { limit: MAX_COUNTRIES } })
      .subscribe(countries => {
        this.countries = countries;
        this.creationMode = !this.bankAccountId;

        if (!this.creationMode) {
          this.bankAccountRepo.fetchOne(this.bankAccountId, {})
            .subscribe((bankAccount: IBankAccount) => { this.initForm(bankAccount); });
        }
      });
  }

  private initForm(bank = {} as IBankAccount): void {
    this.form = this.fb.group({
      accountNo: [bank.accountNo],
      iban: [bank.iban, []],
      viaBank: [bank.viaBank],
      viaAccount: [bank.viaAccount],
      viaBic: [bank.viaBic, [Validators.isBic]],
      viaBankCode: [bank.viaBankCode],
      forInvoices: [bank.forInvoices],
      forPayments: [bank.forPayments],
      forAccountStatements: [bank.forAccountStatements],
      bankId: [bank.bankId, Validators.required],
      currencyId: [bank.currencyId, Validators.required],
      bic: [{ value: null, disabled: true }],
    });

    if (!this.authService.canUpdate(this.bankAccountsZone)) {
      this.form.disable();
    }

    this.form.get('bankId').valueChanges
      .pipe(
        startWith(this.form.get('bankId').value),
        debounceTime(100),
      )
      .subscribe((bankIdValue: BankKey) => {
        bankIdValue
          ? this.setBicValueAndIbanValidators(bankIdValue)
          : this.enableBankDisabledOtherFields();
      });

    const { iban, accountNo, bankId } = this.form.controls;
    iban.valueChanges
      .subscribe((value: string) => {
        if (
          // Note: field is dirty when change was made by user
          iban.dirty
          && value
          && value.startsWith('DE')
          && iban.valid
        ) {
          accountNo.patchValue(this.extractAccountNoOutOfIban(value));
        }
      });
  }

  private extractAccountNoOutOfIban(iban: string) {
    const accountNumberPositionStartsAfterDigits = 12;
    return iban.substring(accountNumberPositionStartsAfterDigits);
  }

  private enableBankDisabledOtherFields() {
    this.form.disable({ emitEvent: false });
    this.form.markAsUntouched();
    this.form.get('bankId').enable({ emitEvent: false });
  }

  private setBicValueAndIbanValidators(bankId: BankKey) {
    this.form.enable({ emitEvent: false });
    const bic = this.form.get('bic');
    bic.disable({ emitEvent: false });
    const bank = this.banks.find(b => b.id === bankId);
    // Set BIC value
    if (bank && bic.value !== bank.bic) {
      bic.patchValue(bank.bic);
    }

    // Add/Remove required validator of IBAN based on bank's country
    // if sepa === true iban is mandatory
    const country = this.countries.find(c => c.id === bank.countryId);
    if (country) {
      const iban = this.form.get('iban');
      country.sepaParticipant
        ? iban.setValidators([Validators.required, Validators.isIban])
        : iban.setValidators([Validators.isIban]);

      iban.updateValueAndValidity();
    }
  }

  public saveClicked(): void {
    const { value } = this.form;
    /**
     * it is not allowed to save the bank account
     * if both fields (IBAN and account number) are empty
     * https://navido.atlassian.net/browse/NAVIDO-960
     */
    if (!(value.iban || value.accountNo)) {
      this.notificationService.notify(NotificationType.Warning,
        wording.general.warning,
        wording.system.pleaseEnterIbanOrAccountNumber
      );
      return;
    }
    const companyId = this.companyId;
    const freshBankAccount = this.creationMode
      ? { ...this.form.value, companyId }
      : { ...this.form.value, companyId, id: this.bankAccountId };

    this.creationMode
      ? this.createBankAccount(freshBankAccount)
      : this.upateBankAccount(freshBankAccount); // todo : variable structure doesn't match the interface, do checkup
  }

  private createBankAccount(bankAccount: any) {
    this.bankAccountRepo.create(bankAccount, {})
      .subscribe(() => { this.closeModal(); });
  }

  private upateBankAccount(bankAccount: IBankAccount) {
    this.bankAccountRepo.update(bankAccount, {})
      .subscribe(() => { this.closeModal(); });
  }

  public onForAccountStatementsChange(checkboxState: boolean) {
    if (!checkboxState) {
      this.getAutoBankRules().subscribe(mandatorRelatedRules => {
        if (!mandatorRelatedRules.length) {
          return;
        }

        this.showAccountStatementsWarning().subscribe(confirmed => {
          /**
           * recheck programmically the checkbox
           * if user cancel action
           */
          confirmed
            ? this.deleteAutobankRules(mandatorRelatedRules)
            : this.changeValue('forAccountStatements', true);
        });
      });

    }
  }

  private getAutoBankRules(): Observable<IAutobankRule[]> {
    return forkJoin([
      this.autobankRuleRepo.fetchAll({}),
      this.mandatorRepo.fetchLookups({})
    ])
      .pipe(
        map(([rules, mandators]) => {
          const mandator = mandators.find (_mandator => _mandator.companyId === this.companyId);
          if (!mandator) {
            return;
          }
          const mandatorId = mandator.id;

          const mandatorRelatedRules: IAutobankRule[] = rules.filter(rule => (!rule.isValidForAllBankAccounts &&
            rule.mandatorId === mandatorId &&
            rule.bankAccountId === this.bankAccountId)
          );

          return mandatorRelatedRules;
        }),
      );
  }

  private showAccountStatementsWarning() {
    return this.confirmationService.warning({
      wording: this.wording.accounting.forAccountStatementsWarning,
      okButtonWording: this.wording.general.delete,
      cancelButtonWording: this.wording.general.cancel
    });
  }

  private changeValue(key: string, value: any): void {
    this.form.get(key).patchValue(value);
  }

  private deleteAutobankRules(rules: IAutobankRule[]) {
    rules.map(rule => {
      this.autobankRuleRepo.delete(rule.id, {}).subscribe();
    });
  }
}
