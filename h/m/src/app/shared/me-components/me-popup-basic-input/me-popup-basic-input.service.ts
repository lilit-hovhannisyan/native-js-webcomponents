import { Component, Injectable, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MePopupBasicInputComponent } from './me-popup-basic-input.component';
import { MePopupBasicInputI } from './me-popup-basic-input.interface';

@Injectable()
export class MeBasicInputPopupService {
  private _takeUntilObservable$: Subject<void> = new Subject<void>();
  private _currentInstance: MePopupBasicInputComponent | undefined;

  constructor(private dialog: MatDialog) {}

  openPopup(dataModel: MePopupBasicInputI, content?: TemplateRef<Component>): Subject<string> {
    const subjectToReturn = new Subject<string>();
    const dialogRef = this.dialog.open(MePopupBasicInputComponent, { data: { dataModel, content }, panelClass: 'basic-input-container' });

    this._currentInstance = dialogRef.componentInstance;
    this._currentInstance.popupBasicInputEvent
      .pipe(takeUntil(this._takeUntilObservable$))
      .subscribe((event: { eventName: string; items?: [] }) => {
        if (event.eventName === 'close') {
          dialogRef.close();
        } else if (event.eventName === 'invite') {
          subjectToReturn.next(event.eventName);
          dialogRef.close();
        }
      });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this._takeUntilObservable$))
      .subscribe(() => {
        this._takeUntilObservable$.next();
        this._takeUntilObservable$.complete();
        this._currentInstance = undefined;
        subjectToReturn.complete();
      });

    return subjectToReturn;
  }

  pushDataModelToPopup(data: MePopupBasicInputI): void {
    if (this._currentInstance) {
      this._currentInstance.dataModel = data;
    }
  }
}
