import { wording } from 'src/app/core/constants/wording/wording';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  TemplateRef,
  ViewChild,
  ChangeDetectorRef, Injector
} from '@angular/core';
import { NvGridConfig, NvGridRowSelectionType, GridComponent, NvColumnConfig } from 'nv-grid';
import { NzModalRef, NzModalService, ModalOptions } from 'ng-zorro-antd/modal';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
  AbstractValueAccessor,
  makeProvider
} from 'src/app/core/abstracts/value-accessor';
import { FormBuilder, FormGroup, AbstractControl, NgControl, ValidationErrors, FormControl } from '@angular/forms';

export type IAutocompleteCustomOptionsFilter = (resourseItem: any[], searchValue: string | number) => any[];

@Component({
  selector: 'nv-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss'],
  providers: [makeProvider(AutocompleteComponent)],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutocompleteComponent extends AbstractValueAccessor
  implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  /**
    * true by default
   */
  @Input() private autofocus = true;
  @Input() private minLengthToOpen = 1;
  @Input() public title: string;
  @Input() public modalGridConfig: NvGridConfig;
  @Input() public properities: string[];
  @Input() public hiddenProperities: string[] = [];
  @Input() public maxLength = 100;
  @Input() public resource: any[] = [];
  @Input() public rawResource: any[];
  @Input() public propertyToDisplayModifier: (value: any) => any;
  @Input() public setNullIfNotFound = false;
  @Input() public displayOneProperty: boolean;
  @Input() public openListOnInit = false;
  @Input() public openListOnClick = true;
  @Input() public takeFirst = true;
  @Input() public placeholder = '';


  /**
   * Used to pass image property name that has the path
   * of the image to show
   */
  @Input() public imageProp?: string;

  @Input() public customOptionsFilter: IAutocompleteCustomOptionsFilter;

  /**
    True by default
   */
  @Input() public setId = true;
  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();
  @Output() public valueChange = new EventEmitter<any>();
  @Output() public optionSelected = new EventEmitter<boolean>();
  @ViewChild('gridTemplate') private gridTemplate: TemplateRef<any>;
  @ViewChild('modalGrid', { static: false }) public modalGrid: GridComponent;
  @ViewChild('modalTitle') private modalTitle: TemplateRef<any>;
  @ViewChild('input') private input: ElementRef<any>;

  public options: any[] = [];
  public modalIsOpen = false;
  public wording = wording;
  public showingAll = false;
  public selectedItem: ReplaySubject<object>;
  public gridConfig: NvGridConfig;
  private modalRef: NzModalRef;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public form: FormGroup;
  private formControl: AbstractControl;
  public isControlRequired: boolean;

  /**
   * Used when we have `takeFirst = false` to filter resource used in the grid in modal
   * the we use it to reset the filtered data for the grid
  */
  private backupResource: any[] = [];
  private controls: { customModel: AbstractControl };

  /**
   * holds the acutal path from the current selected item
   */
  public imageSrc?: string;

  constructor(
    private modalService: NzModalService,
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private injector: Injector,
  ) {
    super();
  }

  public initForm() {
    this.form = this.formBuilder.group({
      customModel: null
    });
    this.controls = { customModel: this.form.get('customModel') };
  }

  public ngOnInit() {
    this.initForm();
    this.selectedItem = new ReplaySubject<object>(1);
    this.selectedItem.pipe(
      takeUntil(this.componentDestroyed$),
    ).subscribe(item => {
      this.valueChange.next(item);
      if (this.form) {
        if (item) {
          if (this.setId) {
            this.input.nativeElement.setAttribute('data-id', item['id']);
          }

          const newValue = this.setId
            ? item['id']
            : item[this.properities[0]];
          this.value = newValue;
          this.updateLabelwithItem(item);
        } else {
          if (this.setNullIfNotFound) {
            this.value = null;
            this.updateLabel(null);
          } else {
            const { customModel } = this.form.controls;
            this.value = customModel.value
              ? customModel.value
              : null;
          }
        }
        this.updateImage(this.value);
      }
    });
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.resource && this.form) {
      this.updateLabel(this.value);
    }
  }

  private updateImage(value: any) {
    if (this.setId) {
      const foundItem = this.resource.find(item => item.id === value);
      this.imageSrc = foundItem && foundItem[this.imageProp];
      this.cdr.detectChanges();
    }
  }

  private updateLabelwithItem = (item: object) => {
    const { customModel } = this.form.controls;
    customModel.patchValue(
      this.getLabelOfItem(item));
  }

  private updateLabel = (value: any) => {
    if (this.resource) {
      const { customModel } = this.form.controls;
      customModel.patchValue(
        this.resource ? this.getCustomModel(value, customModel.value) : value);

      if (this.imageProp) {
        this.updateImage(this.value);
      }
    }
  }

  public ngAfterViewInit() {
    const ngControl: NgControl = this.injector.get(NgControl, null);

    if (ngControl) {
      this.formControl = ngControl.control as FormControl;
      const validator: ValidationErrors = this.formControl?.validator
        && typeof this.formControl.validator === 'function'
        && this.formControl.validator({} as AbstractControl);
      this.isControlRequired = !!(validator && validator.required);
    }

    if (this.autofocus) {
      setTimeout(() => {
        this.input.nativeElement.focus();
        this.selectValue();
        if (this.openListOnInit) {
          this.openList();
        }
      }, 0);
    }
  }

  public focus() {
    this.input.nativeElement.focus();
  }

  public openList() {
    this.onChangeInput(this.controls.customModel.value);
    this.form.markAsDirty();
  }

  public handleClick(event: MouseEvent) {
    event.preventDefault();
    this.selectValue();
    if (this.openListOnClick) {
      this.openList();
    }
  }

  public selectValue() {
    this.input.nativeElement.select();
  }

  private updateGridConfig() {
    if (this.gridConfig) {
      this.gridConfig.buttons = [{
        actOnDoubleClick: true,
        func: (row: any) => {
          this.selectedItem.next(row);
          this.markModalAsClose();
        },
        description: wording.general.select,
        icon: 'select',
        actOnEnter: true,
        tooltip: wording.general.select,
        name: 'select'
      }];

      if (
        !this.gridConfig.toolbarButtons
        || this.gridConfig.toolbarButtons.length === 0
      ) {
        this.gridConfig.hideToolbar = true;
      }
    }
  }

  public onChangeInput(searchValue: string): void {
    if (searchValue) {
      this.options = this.getOptions(searchValue, this.resource);
    } else {
      this.options = this.sortOptions(this.resource);
    }
  }

  private getOptions(searchValue: string | number, resource: any[] = []) {
    if (searchValue != null && searchValue.toString().length >= this.minLengthToOpen) {
      const searchText = searchValue.toString().toLowerCase().trimLeft();
      const properities = [...this.properities, ...this.hiddenProperities];

      if (this.customOptionsFilter) {
        return this.customOptionsFilter(resource, searchText);
      }

      const foundOptions = resource.filter(
        resourceObject => {
          let resourceString = '';
          properities.forEach(property => {
            const value = this.propertyToDisplayModifier
              && (property === this.properities[0])
              ? this.propertyToDisplayModifier(resourceObject[property])
              : resourceObject[property];

            resourceString += ' ' + value;
          });

          const resourceStringCleaned = resourceString
            .toString()
            .toLowerCase()
            .trimLeft();

          const foundResult = resourceStringCleaned.includes(searchText);
          return foundResult;
        }
      );

      return foundOptions
        .every((object, index, array) =>
          !index
          || array[index - 1][this.properities[0]]
          <= object[this.properities[0]]
        )
        ? foundOptions
        : this.sortOptions(foundOptions);
    }
    return [];
  }

  private sortOptions(options: any[]) {
    return JSON.parse(JSON.stringify(options))
      .sort((a, b) => a[this.properities[0]] > b[this.properities[0]]
        ? 1
        : -1
      );
  }

  private find(searchValue: string): any[] {
    const foundOptions: any[] = this.setId && searchValue
      ? [this.resource.find(item => item.id === Number(searchValue))]
      : this.getOptions(searchValue, this.resource);
    let foundOptionsInRaw: any[];
    if (this.rawResource) {
      foundOptionsInRaw = this.setId
        ? [this.rawResource.find(item => item.id === Number(searchValue))]
        : this.getOptions(searchValue, this.rawResource);
    }

    if (searchValue != null && foundOptions.length > 0) {
      return foundOptions;
    } else if (foundOptionsInRaw && foundOptionsInRaw.length > 0) {
      return foundOptionsInRaw;
    }
    return [];
  }

  private getCustomModel(value: any, customModelValue: any): string | null {
    const resourceToUse = this.rawResource || this.resource;
    const foundItem = this.setId
      ? resourceToUse.find(item => item.id === value)
      : this.find(value)[0];

    if (foundItem) {
      return this.getLabelOfItem(foundItem);
    }

    return this.setNullIfNotFound
      ? null
      : value && customModelValue != null
        ? customModelValue.toString()
        : value;
  }

  private getLabelOfItem(item: object): string {
    const inputValueFirstPart: number | string = item
      ? this.propertyToDisplayModifier
        ? this.propertyToDisplayModifier(item[this.properities[0]])
        : item[this.properities[0]]
      : '';

    const inputValueSecondPart = item && this.properities[1]
      ? ' ' + item[this.properities[1]]
      : '';

    return this.displayOneProperty
      ? inputValueFirstPart.toString()
      : inputValueFirstPart + inputValueSecondPart;
  }

  public handleEscape(event: any) {
    if (!this.modalIsOpen) {
      this.options = [];
      this.escape.emit(event);
    }
  }

  public autocompleteCurrentValue(value: any, dirty = false, fromEnter = false) {
    if (!this.modalIsOpen && (dirty || this.form.dirty)) {
      const foundItems = this.find(value);
      // To don't call event valueChange twice
      if (foundItems.length !== 0 && fromEnter) {
        return;
      }

      if (this.takeFirst) {
        this.selectedItem.next(foundItems[0]);
      } else {
        if (foundItems.length === 1) {
          this.selectedItem.next(foundItems[0]);
        } else {
          this.openSearchGridModal();
        }
      }
      this.form.markAsPristine();
    }
    this.optionSelected.emit(true);
  }

  private markModalAsOpen() {
    this.options = [];
    this.modalIsOpen = true;
    this.input.nativeElement.blur();
  }

  private markModalAsClose() {
    this.modalGridConfig?.columns
      .forEach((column: NvColumnConfig) => {
        if (column.filter && column.filter.values && column.filter.values.length > 0) {
          column.filter.form.get('value').patchValue(null);
          column.filter.values = [];
        }
      });

    this.modalIsOpen = false;
    this.modalRef.destroy();
    this.input.nativeElement.focus();
  }

  public handleTab(event: KeyboardEvent, value: any) {
    this.options = [];
    if (!this.modalIsOpen) {
      this.autocompleteCurrentValue(value);
      this.tab.emit(event);
    }
  }

  public openSearchGridModal = (event?: KeyboardEvent) => {
    if (!this.modalGridConfig || (event && event.key === 'F2' && !this.takeFirst)) {
      return;
    }
    // Validation Rule 24
    // https://navido.atlassian.net/wiki/spaces/TE/pages/376274945/Booking+form
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }
    const options: ModalOptions = {
      nzContent: this.gridTemplate,
      nzTitle: this.modalTitle,
      nzFooter: null,
    };

    this.gridConfig = Object.assign({}, {
      ...this.modalGridConfig,
      rowSelectionType: NvGridRowSelectionType.RadioButton,
    });
    if (!this.takeFirst && this.controls.customModel.value) {
      const userInput = this.controls.customModel.value.toLowerCase();
      this.backupResource = this.resource;
      this.resource = this.resource.filter(resource => {
        return resource[this.properities[0]].toLowerCase().includes(userInput)
          || resource[this.properities[1]].toLowerCase().includes(userInput);
      });
    }
    this.updateGridConfig();

    this.createModal(options);
  }

  private createModal(options: any) {
    this.markModalAsOpen();

    const modalOptions: ModalOptions = {
      nzClosable: false,
      nzWidth: 'fit-content',
      nzFooter: options.nzFooter,
      nzOkText: options.nzOkText,
      nzCancelText: options.nzCancelText,
      nzTitle: options.nzTitle,
      nzStyle: { 'max-width': '100%' },
      nzContent: options.nzContent,
      nzOnCancel: () => this.markModalAsClose(),
      nzNoAnimation: true
    };

    this.modalRef = this.modalService.create(modalOptions);
    if (this.backupResource.length) {
      this.modalRef.afterClose
        .subscribe(() => {
          this.resource = this.backupResource;
          this.backupResource = [];
        });
    }
  }

  public onBlur(event: FocusEvent, value: any) {
    this.options = [];
    if (this.modalIsOpen || !this.takeFirst) {
      event.preventDefault();
    } else {
      this.autocompleteCurrentValue(value);
      this.blur.emit(event);
    }
  }

  public setDisabledState(isDisabled: boolean): void {
    this._disabled = isDisabled;
    isDisabled
      ? this.controls.customModel.disable()
      : this.controls.customModel.enable();
  }

  public handleEnter(event: KeyboardEvent, value: any) {
    if (!this.modalIsOpen) {
      this.autocompleteCurrentValue(value, false, true);
      this.enter.emit(event);
    } else {
      event.preventDefault();
    }
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public writeValue(value: any) {
    this._value = value;
    if (this.form) {
      this.updateLabel(value);
    }
  }

  public handleSearchIconMouseDown(event: any) {
    // Workaround: Because click on search icon causes
    // blur on autocomplete first, cell goes into view mode and removes icon,
    // so click on icon never occurs.
    // Solution is to prevent mousedown on icon, which prevents autocomplete blur from firing, allowing click event on icon.
    // See https://stackoverflow.com/questions/7621711/how-to-prevent-blur-running-when-clicking-a-link-in-jquery
    // Alternative solution is to treat mousedown as click, which has downsides.
    event.preventDefault();
  }

  public handleSearchIconClick(event: any) {
    if (!this._disabled) {
      this.openSearchGridModal(event);
    }
  }
}
