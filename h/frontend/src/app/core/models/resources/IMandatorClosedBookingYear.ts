import { IResource } from './IResource';

export type MandatorClosedBookingYearKey = number;

interface ISharedProps {
  mandatorId?: number;
  year: number;
  period: number;
  closedBy: number;
  closedAt: string;
}

export interface IMandatorClosedBookingYearBase extends ISharedProps, IResource<MandatorClosedBookingYearKey> { }

export interface IMandatorClosedBookingYearCreate extends ISharedProps { }

export interface IMandatorClosedBookingYearDTO extends IMandatorClosedBookingYearBase { }

export interface IMandatorClosedBookingYear extends IMandatorClosedBookingYearDTO { }


