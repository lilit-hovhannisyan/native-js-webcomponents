import { Injectable } from '@angular/core';
import { IDunningDays, IDunningDaysDTO, IDunningDaysCreate } from '../../models/resources/IDunningDays';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { map } from 'rxjs/operators';
import { HttpGet } from '../../models/http/http-get';

export interface IDunningDaysRepoParams extends RepoParams<IDunningDays> {
  debitorCreditorId?: number;
}
@Injectable({
  providedIn: 'root'
})
export class DunningDaysRepositoryService extends AbstractRepository<
IDunningDays,
IDunningDaysDTO,
IDunningDaysCreate,
IDunningDaysRepoParams,
number
> {
  protected resourceType = 'DunningDays';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IDunningDaysDTO): IDunningDays => item;

  public getBaseUrl = () => '/api/dunning-days';

  public fetchDunningDaysOfDebitorCreditor(options: IDunningDaysRepoParams) {
    const url = `/api/debitor-creditors/${options.debitorCreditorId}/dunning-days`;
    const request = new HttpGet(url, { init: { params: options.queryParams } });
    request.setErrorHandler(options.errorHandler);
    return this.repositoryService.httpClient.request(request)
      .pipe(map((response: any) => response.body && response.body.data as IDunningDays));
  }
}
