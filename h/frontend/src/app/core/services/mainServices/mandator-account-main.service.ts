import { MandatorKey } from './../../models/resources/IMandator';
import { Injectable } from '@angular/core';
import { forkJoin, Observable, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { BookingAccountRepositoryService } from '../repositories/booking-account-repository.service';
import { IMandatorAccountFull, IMandatorAccount } from '../../models/resources/IMandatorAccount';
import { MandatorAccountRepositoryService } from '../repositories/mandator-account-repository.service';
import { IBookingAccount } from '../../models/resources/IBookingAccount';
import { sortBy } from 'lodash';
import { CurrencyRepositoryService } from '../repositories/currency-repository.service';
import { ICurrency } from '../../models/resources/ICurrency';
import { SettingsService } from '../settings.service';
import { getLabelKeyByLanguage } from '../../helpers/general';
import { IMandatorAccountRole } from '../../models/resources/IMandatorAccountRole';
import { MandatorAccountRoleRepositoryService } from '../repositories/mandator-account-role.repository.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class MandatorAccountMainService {

  constructor(
    private mandatorAccountRepo: MandatorAccountRepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    private currecniesRepo: CurrencyRepositoryService,
    private settingsService: SettingsService,
    private mandatorAccountRoleRepo: MandatorAccountRoleRepositoryService,
    private authService: AuthenticationService,
  ) { }

  public getStream(params: { mandatorId: MandatorKey }): Observable<IMandatorAccountFull[]> {
    return forkJoin([
      this.mandatorAccountRepo.fetchAll({ queryParams: params }),
      this.bookingAccountRepo.fetchAll({}),
      this.currecniesRepo.fetchAll({}),

    ]).pipe(
      switchMap(() => combineLatest([
        this.mandatorAccountRepo.getStream()
          .pipe(
            map(mandatorAccounts => mandatorAccounts
              .filter(mandatorAccount => mandatorAccount.mandatorId === params.mandatorId)
            )
          ),
        this.bookingAccountRepo.getStream(),
        this.currecniesRepo.getStream(),
      ])),
      map(res => sortBy(this.getMandatorAccountFullList(...res), ['no'], ['asce']))
    );
  }

  private getMandatorAccountFullList(
    mandatorAccounts: IMandatorAccount[],
    bookingAccounts: IBookingAccount[],
    currencies: ICurrency[],
  ): IMandatorAccountFull[] {
    return mandatorAccounts.map(mandatorAccount => {
      return this.getMandatorAccountsFull(mandatorAccount, bookingAccounts, currencies);
    });
  }

  private getMandatorAccountsFull(
    mandatorAccount: IMandatorAccount,
    bookingAccounts: IBookingAccount[],
    currencies: ICurrency[],
  ): IMandatorAccountFull {
    const lang = this.settingsService.language;
    const labelKey = getLabelKeyByLanguage(lang);
    const foundBookingAccount = bookingAccounts.find(bookingAccount => bookingAccount.id === mandatorAccount.bookingAccountId);
    let foundCurrency: ICurrency;

    if (foundBookingAccount) {
      foundCurrency = currencies.find(currency => currency.id === foundBookingAccount.currencyId);
    }
    const foundMandatorAccountCurrency = currencies.find(c =>
      c.id === mandatorAccount.restrictedCurrencyId);

    const restrictedCurrency: string = foundMandatorAccountCurrency
      ? `${foundMandatorAccountCurrency.isoCode} / ${foundMandatorAccountCurrency[labelKey]}`
      : '';

    return {
      ...mandatorAccount,
      no: foundBookingAccount.no,
      isoCode: foundCurrency ? foundCurrency.isoCode : null,
      displayLabel: foundBookingAccount.displayLabel,
      restrictedCurrency
    };
  }

  public hasAccessToMandatorAccount = (mandatorAccount: IMandatorAccount): boolean => {
    if (mandatorAccount?.needsAccountPermission) {
      const foundAccountRoles: IMandatorAccountRole[] = this.mandatorAccountRoleRepo
        .getStreamValue()
        .filter(accountRole => accountRole.mandatorAccountId === mandatorAccount.id);

      if (foundAccountRoles.length === 0) {
        return false;
      }

      return this.authService.getRolesIds()
        .some(userRole => foundAccountRoles
          .some(mandatorAccountRole => mandatorAccountRole.roleId === userRole));
    }
    return true;
  }
}
