import { NvColumnDataType, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { IResource } from 'src/app/core/models/resources/IResource';
import { IVesselWithAccountings } from 'src/app/core/models/resources/IVessel';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'VesselsGridMain',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'lastVesselAccountingNo',
        title: wording.general.number,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.Number,
        filter: {
          values: []
        },
      },
      {
        key: 'lastVesselAccountingName',
        title: wording.general.name,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.String,
        filter: {
          values: []
        },
      },
      {
        key: 'imo',
        title: wording.chartering.imoNumber,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        customFormatFn: ((row: IVesselWithAccountings) => row.lastVesselChartername?.charterName),
        key: 'lastVesselChartername',
        title: wording.technicalManagement.charterName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'deliveryDate',
        title: wording.chartering.deliveredFromShipyard,
        width: 250,
        isSortable: true,
        dataType: NvColumnDataType.Date,
        filter: {
          values: []
        }
      }
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editPort',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (entity: IResource<number>) => actions.edit(entity.id),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deletePort',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.UPDATE),
        func: (entity: IResource<number>) => actions.delete(entity),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.UPDATE),
        func: () => actions.add()
      }
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
