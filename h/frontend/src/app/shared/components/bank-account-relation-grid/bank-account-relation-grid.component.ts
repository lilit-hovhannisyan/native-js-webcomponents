import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NvGridConfig, GridComponent } from 'nv-grid';
import { BankAccountRepositoryService } from 'src/app/core/services/repositories/bank-account-repository.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { BankKey } from 'src/app/core/models/resources/IBank';
import { IBankAccount, IBankAccountRelation } from 'src/app/core/models/resources/IBankAccount';
import { map } from 'rxjs/operators';
import { MandatorKey } from 'src/app/core/models/resources/IMandator';
import { CompanyKey } from 'src/app/core/models/resources/ICompany';
import { AccountingRelationMainService } from 'src/app/core/services/mainServices/accounting-relations-main.service';
import { FormGroup } from '@angular/forms';

@Component({
  templateUrl: './bank-account-relation-grid.component.html',
  styleUrls: ['./bank-account-relation-grid.component.scss']
})
export class BankAccountRelationGridComponent implements OnInit {
  @ViewChild('grid', { static: true }) public grid: GridComponent;
  /** - GridConfig: The expanded component configuration */
  @Input() public gridConfig: NvGridConfig;
  /** The exapnded grid belongs to one bank Id. It is needed to filter the entites */
  @Input() public bankId: BankKey;
  /** mandatorId: bankAccount entity doesn't nave mandatorId.
   *    a) BookingAccountAutocomplete needs to show only accounts which belong to this mandatorId
   *    b) This mandator Id is the selected item from the left list
   *  */
  @Input() public mandatorId: MandatorKey;
  @Input() public companyId: CompanyKey;

  /** If user collapse the row and reopens it, this component gets destroyed and reinitialized
   *    a) If user selects other mandator, without saving the current changes, changes will get lost
   *    b) When this component calls setAccountingRelation, it checks first if there is a temp change else check stream
   */
  @Input() private tempChanges: IBankAccountRelation[];
  @Input() public changesSubscriber: BehaviorSubject<IBankAccountRelation>;

  public dataSource$: Observable<IBankAccountRelation[]>;

  constructor(
    private bankAccountRepo: BankAccountRepositoryService,
    private accountingRelationMainService: AccountingRelationMainService
  ) { }

  public ngOnInit() {
    this.dataSource$ = this.bankAccountRepo.getStream()
      .pipe(map(
        (bankAccounts: IBankAccount[]) =>
          bankAccounts
            .filter(bankAccount => bankAccount.forAccountStatements)
            .map(bankAccount => this.accountingRelationMainService
              .setAccountingRelation(bankAccount, this.mandatorId, this.tempChanges))
      ));

    this.gridConfig.editForm.onFormValuesChange = (childForm: FormGroup) => {
      const bankAccountRelation = childForm.value as IBankAccountRelation;
      this.changesSubscriber.next(bankAccountRelation);
      return '';
    };
  }
}
