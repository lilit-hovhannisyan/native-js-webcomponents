import { StructureKey } from '../../models/resources/IMasterStructure';
import { Injectable } from '@angular/core';
import { IMasterStructure, IMasterStructureDTO, IMasterStructureCreate } from '../../models/resources/IMasterStructure';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { getLabelKeyByLanguage } from '../../helpers/general';
import { SettingsService } from '../settings.service';
import { Language } from '../../models/language';
import { HttpGet } from '../../models/http/http-get';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StructureRepositoryService extends AbstractRepository<
  IMasterStructure,
  IMasterStructureDTO,
  IMasterStructureCreate,
  RepoParams<IMasterStructure>,
  StructureKey
> {
  protected resourceType = 'Structure';
  public sortAsce = true;
  public sortKey = getLabelKeyByLanguage(this.settingsService.language);

  constructor(
    repositoryService: RepositoryService,
    private settingsService: SettingsService,
    private http: HttpClient,
  ) {
    super(repositoryService);
  }

  public getBaseUrl(params: RepoParams<IMasterStructure>): string {
    return '/api/master-structures';
  }

  public postProcess(item: IMasterStructureDTO): IMasterStructure { return item; }

  public downloadExcel = (
    id: number,
    language: Language,
    withAccounts: boolean,
  ): Observable<ArrayBuffer> => {
    const request = new HttpGet(`/api/master-structures/${id}/excel`, {
      init: {
        headers: {
          'Accept-Language': language
        },
        responseType: 'arraybuffer',
        observe: 'response',
        params: {
          withAccounts
        }
      }
    });
    return this.http.request(request)
      .pipe(
        map((res: HttpResponse<ArrayBuffer>) => res.body),
        tap(data => {
          const blob: Blob = new Blob([data]);
          const file: File = new File([blob], 'master-structures.xlsx');
          saveAs(file);
      }));
  }

}
