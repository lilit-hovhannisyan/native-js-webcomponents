import { IResource } from './IResource';

export type ShipyardKey = number;

interface ISharedProps {
  label: string;
}

export interface IShipyardBase extends ISharedProps, IResource<number> { }

export interface IShipyardCreate extends ISharedProps { }

export interface IShipyardDTO extends IShipyardBase { }

export interface IShipyard extends IShipyardDTO { }

export interface IShipyardRow extends IShipyard { }
