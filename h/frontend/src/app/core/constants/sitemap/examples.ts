import { SitemapNode } from './sitemap';

export const examples: SitemapNode = {
  path: 'examples',
  description: { en: 'Description...', de: 'Beschreibung...' },
  title: { en: 'Examples', de: 'Beispiele' },
  icon: 'container',
  zone: 'examples',
  children: {
    mixed: {
      path: 'mixed',
      title: { en: 'Mixed', de: 'Verschiedenes' },
      description: { en: 'Description...', de: 'Beschreibung...' },
      icon: 'assets/icons/88-512.png',
      zone: '*',
      children: {
        wizard: {
          path: 'wizard',
          description: { en: '', de: '' },
          title: { en: 'Wizard', de: 'Wizard' },
          zone: '*',
        },
        timeline: {
          path: 'timeline',
          description: { en: '', de: '' },
          title: { en: 'Timeline', de: 'Timeline' },
          zone: '*',
        },
        topicSearch: {
          path: 'topic-search',
          description: { en: 'a topic-search example', de: '' },
          title: { en: 'Topic Search', de: 'Topic Suche' },
          keywords: ['topic-search', 'search'],
          zone: '*',
        },
        logs: {
          path: 'logs',
          description: { en: '', de: '' },
          title: { en: 'Logs', de: 'Logs' },
          zone: '*',
        },
        acl: {
          path: 'acl',
          description: { en: 'acl', de: 'acl'},
          title: { en: 'acl', de: 'acl'},
          zone: '*',
        },
        formFrame: {
          path: 'form-frame',
          description: { en: 'Form Frame', de: 'Form Frame'},
          title: { en: 'Form Frame', de: 'Form Frame'},
          zone: '*',
        },
        datebox: {
          path: 'datebox',
          description: { en: 'Datebox', de: 'Datebox'},
          title: { en: 'Datebox', de: 'Datebox'},
          zone: '*',
        },
        userCompact: {
          path: 'user-compact',
          description: { en: '', de: '' },
          title: { en: 'User Compact', de: 'User Compact' },
          zone: '*',
        },
        numberInputFormat: {
          path: 'number-input-format',
          description: { en: '', de: '' },
          title: { en: 'Number Input Format', de: 'Number Input Format' },
          zone: 'numberInputFormat',
        },
        // TODO: uncomment this when cors issue with signalR fixed in azure
        // signalR: {
        //   path: 'signalR',
        //   description: { en: '', de: '' },
        //   title: { en: 'Chat room', de: 'Chat room' },
        //   zone: '*',
        // },
      }
    }
  }
};
