import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { IBookingAccount, IBookingAccountWithCurrency } from '../../../../../../core/models/resources/IBookingAccount';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { BookingAccountRepositoryService } from '../../../../../../core/services/repositories/booking-account-repository.service';
import { BookingAccountMainService } from '../../../../../../core/services/mainServices/booking-account-main.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/booking-accounts.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { openNewTab } from 'src/app/core/helpers/general';
import { SettingsRepositoryService } from 'src/app/core/services/repositories/settings-repository.service';
import { IStartEndRange } from 'src/app/core/models/resources/ISettings';
import { DEBITOR_CREDITOR } from 'src/app/core/models/enums/debitor-creditor';

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class BookingAccountsListViewComponent implements OnInit {
  public bookingAccountGridConfig: NvGridConfig;
  public dataSource$: Observable<IBookingAccount[]>;
  public wordingGeneral = wording.general;

  constructor(
    private bookingService: BookingAccountMainService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    public gridConfigService: GridConfigService,
    private router: Router,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private settingsRepo: SettingsRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.dataSource$ = this.bookingService.getStream();

    const gridActions = {
      edit: (id: number) => this.router.navigate([`${this.router.url}/${id}`]),
      delete: this.deleteBookingAccountClicked,
      add: () => this.router.navigate([`${this.router.url}/new`]),
      clone: (bookingAccount: IBookingAccountWithCurrency) => {
        openNewTab(`${this.router.url}/new?clone_id=${bookingAccount.id}`);
      },
    };

    this.settingsRepo.fetchAll()
      .subscribe(settings => {
        this.bookingAccountGridConfig = getGridConfig(
          gridActions,
          this.isInDebitorCreditorRange(settings.system.debitorCreditorRanges),
          (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
        );
      });
  }

  private isInDebitorCreditorRange(
    ranges: IStartEndRange<DEBITOR_CREDITOR>[]
  ): (bookingAccount: IBookingAccount) => boolean {
    return (bookingAccount: IBookingAccount) => {
      return ranges.some(range => bookingAccount.no >= range.start
        && bookingAccount.no <= range.end);
    };
  }

  public deleteBookingAccountClicked = (bookingAccount: IBookingAccount): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: bookingAccount.displayLabel, },
    }).subscribe(confirmed => {
      if (confirmed) {
        this.bookingAccountRepo.delete(bookingAccount.id, {}).subscribe();
      }
    });
  }
}
