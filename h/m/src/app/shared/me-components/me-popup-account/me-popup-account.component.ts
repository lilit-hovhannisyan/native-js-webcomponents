import { MePopupAccountI } from './me-popup-account.interface';
import {
  Component,
  ChangeDetectionStrategy,
  EventEmitter,
  Output,
  Inject,
  ChangeDetectorRef,
  Injector,
  OnDestroy,
  ViewEncapsulation
} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BaseLoggerComponent } from '@features/tracking-system';

@Component({
  selector: 'me-me-popup-account',
  templateUrl: './me-popup-account.component.html',
  styleUrls: ['./me-popup-account.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class MePopupAccountComponent extends BaseLoggerComponent implements OnDestroy {
  @Output() popupSelectEvent: EventEmitter<{ eventName: string }> = new EventEmitter();
  componentVersion: string = '0.0.1';
  private _dataModel!: MePopupAccountI;

  constructor(injector: Injector, @Inject(MAT_DIALOG_DATA) public data: { dataModel: MePopupAccountI }, private cdr: ChangeDetectorRef) {
    super(injector);
    this._dataModel = data.dataModel;
  }

  set dataModel(data: MePopupAccountI) {
    this._dataModel = data;
    this.cdr.detectChanges();
  }

  get dataModel(): MePopupAccountI {
    return this._dataModel;
  }

  customEvent(eventName: string): void {
    this.popupSelectEvent.emit({ eventName });
  }

  ngOnDestroy(): object {
    return super.ngOnDestroy();
  }
}
