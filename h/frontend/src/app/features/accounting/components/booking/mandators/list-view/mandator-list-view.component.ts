import { MandatorMainService } from '../../../../../../core/services/mainServices/mandator-main.service';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { Observable } from 'rxjs';
import { IMandatorRow } from '../../../../../../core/models/resources/IMandator';
import { map } from 'rxjs/operators';
import { MandatorRepositoryService } from '../../../../../../core/services/repositories/mandator-repository.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/mandator.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';

@Component({
  templateUrl: './mandator-list-view.component.html',
  styleUrls: ['./mandator-list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MandatorListViewComponent implements OnInit {

  public mandatorGridConfig: NvGridConfig;
  public dataSource$: Observable<IMandatorRow[]>;

  constructor(
    private mainService: MandatorMainService,
    private repo: MandatorRepositoryService,
    public gridConfigService: GridConfigService,
    private router: Router,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    this.dataSource$ = this.mainService.fetchMandatorsWithRolesOnly();

    this.mandatorGridConfig = getGridConfig(
      { delete: this.deleteRowClicked },
      this.router,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  public deleteRowClicked = (mandator: IMandatorRow): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: mandator.name },
    }).subscribe(confirmed => confirmed && this.delete(mandator.id));
  }

  public delete(id: number): void {
    this.repo.delete(id, {})
      .subscribe(() => {
        // TODO: try to use getStream method inside fetchMandatorsWithRolesOnly() method
        // to avoid adding this line, and update stream automatically
        this.dataSource$ = this.mainService.fetchMandatorsWithRolesOnly();
        this.cdr.markForCheck();
      });
  }
}
