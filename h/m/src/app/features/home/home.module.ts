import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';
import { MatStepperModule } from '@angular/material/stepper';
import { MeIntroductionMessageModule } from '@shared/me-components/me-introduction-message/me-introduction-message.module';
import { MePopupAccountModule } from '@shared/me-components/me-popup-account/me-popup-account.module';
import { MeDataImportModule } from '@shared/me-components/me-data-import/me-data-import.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MePopupVideoModule } from '@shared/me-components/me-popup-video/me-popup-video.module';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatButtonModule,
    MeIconsProviderModule,
    MatStepperModule,
    MeIntroductionMessageModule,
    MePopupAccountModule,
    MeDataImportModule,
    MatFormFieldModule,
    MatInputModule,
    MePopupVideoModule
  ]
})
export class HomeModule {}
