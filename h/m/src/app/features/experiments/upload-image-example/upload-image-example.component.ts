import { Component, OnInit } from '@angular/core';

import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';

import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { bytesToSize } from '@shared/utils';
import { FileWebService } from '@shared/services/file.web-service';

import { MeButtonI } from '@shared-components/me-button/me-button.interface';
import { MeUploadImageI } from '@shared-components/me-upload-image/me-upload-image.interface';

import { ThumbnailDocument, FileUploadType } from '@shared-models/file-upload/file-upload.model';

import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { FileUploadResponseModel } from '@shared/models/file-upload/file-upload-response.model';

@Component({
  selector: 'me-upload-image-example',
  templateUrl: './upload-image-example.component.html',
  styleUrls: ['./upload-image-example.component.scss'],
  providers: [FileWebService]
})
export class UploadImageExampleComponent implements OnInit {
  uploadImageDataInterface!: MeUploadImageI;
  uploadImageButton: MeButtonI;
  thumbnailProperties!: ThumbnailDocument;
  supportedThumbnailFiles: string = '';
  acceptPhotoExtensions: string = '';
  maxUploadSize: string = '';
  metaDataToSend: FileUploadType;

  constructor(private definitionsStoreService: DefinitionsStoreService, private fileWebService: FileWebService) {
    this.uploadImageButton = { type: 'regular', color: 'primary', disabled: false, rounded: false };
    this.metaDataToSend = {
      type: 'THUMBNAIL'
    };
  }

  ngOnInit(): void {
    this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        if (this.definitionsStoreService.thumbnailProperties) {
          this.thumbnailProperties = this.definitionsStoreService.thumbnailProperties;

          this.supportedThumbnailFiles = this.definitionsStoreService.thumbnailProperties.supportedFiles.replace(/,/g, ', ');

          this.maxUploadSize = bytesToSize(this.definitionsStoreService.thumbnailProperties.maxSize);

          const splitString = this.definitionsStoreService.thumbnailProperties.supportedFiles.split(',');
          this.acceptPhotoExtensions = `.${splitString.join(',.')}`;
        }

        this.loadInitialImageData();
      }
    });
  }

  loadInitialImageData(): void {
    this.uploadImageDataInterface = {
      thumbnail: '',
      thumbnailProperties: this.thumbnailProperties,
      uploadImageButton: this.uploadImageButton,
      isThumbnailStatusInvalid: false,
      acceptPhotoExtensions: this.acceptPhotoExtensions,
      supportedThumbnailFiles: this.supportedThumbnailFiles,
      maxUploadSize: this.maxUploadSize
    };
  }

  openImageFormData(recievedFile: File): void {
    const formData = new FormData();
    formData.append('meta', JSON.stringify(this.metaDataToSend));
    formData.append('file', recievedFile, recievedFile.name);

    this.fileWebService.uploadFile(formData, FileUploadResponseModel).subscribe(
      (response) => {
        this.uploadImageDataInterface = {
          thumbnail: `${MARKETPLACE_BASE_API_URL}/files/download/${response.oid}`,
          showProgress: false
        };
        // this.globalService.setReferenceID(response.oid);
      },
      (error: ErrorResponseI) => {
        console.error(error);
        this.uploadImageDataInterface = {
          thumbnail: '',
          thumbnailProperties: this.thumbnailProperties,
          uploadImageButton: this.uploadImageButton,
          isThumbnailStatusInvalid: true,
          acceptPhotoExtensions: this.acceptPhotoExtensions,
          supportedThumbnailFiles: this.supportedThumbnailFiles,
          maxUploadSize: this.maxUploadSize
        };
        /* const basicAlertData = new BasicAlertModel();
        basicAlertData.mode = 'error';
        basicAlertData.title = error.error.title;
        basicAlertData.content = error.error.details;
        this.alertsService.openBasicAlert(basicAlertData);

        this.globalService.setThumbnailStatusInvalid(true); */
      }
    );
  }
}
