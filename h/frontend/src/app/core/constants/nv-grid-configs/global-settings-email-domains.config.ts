import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';
import { Operations } from '../../models/Operations';
import { IResource } from '../../models/resources/IResource';
import { getContextMenuButtonsConfig } from '../../helpers/general';
import { IEmailDomain } from '../../models/resources/ISettings';

export const getEmailDomainsConfig  = (
  actions: IGridConfigActions,
  canPerform: (operation: Operations) => boolean,
  isGridDisabled: boolean,
): NvGridConfig => {
  return ({
    gridName: 'EmailDomainsConfig',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    showPaging: false,
    showFooter: false,
    clearFiltersButton: true,
    columns: [
      {
        key: 'domain',
        title: wording.general.domain,
        width: 250,
        isSortable: true,
        filter: {
          values: []
        },
        customFormatFn: (row: IEmailDomain) => `@${row.domain}`,
      },
      {
        key: 'isSystemDefault',
        title: wording.general.systemDefault,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editEmailDomain',
        tooltip: wording.general.edit,
        hidden: !canPerform(Operations.UPDATE),
        disabled: isGridDisabled,
        func: (entity: IResource<number>) => actions.edit(entity),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteEmailDomain',
        tooltip: wording.general.delete,
        hidden: !canPerform(Operations.DELETE),
        disabled: isGridDisabled,
        func: (entity: IResource<number>) => actions.delete(entity),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        disabled: () => isGridDisabled,
        func: () => actions.add()
      }
    ],
    contextMenuButtons: canPerform(Operations.READ) ? getContextMenuButtonsConfig() : [],
  });
};
