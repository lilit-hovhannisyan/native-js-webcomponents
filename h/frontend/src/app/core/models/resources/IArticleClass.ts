import { IWithActive } from 'src/app/core/models/resources/IWithActive';
import { IWithLabels } from 'src/app/core/models/resources/IWithLabels';
import { IWithSystemFlag } from 'src/app/core/models/resources/IWithSystemFlag';
import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';

export type ArticleClassKey = number;

interface ISharedProps extends IWithLabels, IWithActive, IWithSystemFlag {
  abbreviation: string;
}

export interface IArticleClassCreate extends ISharedProps { }

export interface IArticleClassBase extends ISharedProps, IResource<ArticleClassKey> { }

export interface IArticleClassDTO extends IArticleClassBase { }

export interface IArticleClass extends IArticleClassDTO, IWithLabel { }
