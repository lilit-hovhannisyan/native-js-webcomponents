import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupsTableComponent } from './groups-table.component';
import { MeTableModule } from '@shared/me-components/me-table/me-table.module';
import { MeSearchInputModule } from '@shared-components/me-search-input/me-search-input.module';
import { MeAddListCardModule } from '@shared-components/me-add-list-card/me-add-list-card.module';
import { UsersStoreService } from '@features/users/users-store.service';
import { MeAddListPopupService } from '@shared/me-components/me-popup-add-list/me-popup-add-list.service';
import { UserActionsService } from '@features/backoffice/users-groups/user-actions.service';
import { MeScrollToBottomModule } from '@shared/me-directives/me-scroll-to-bottom.module';

@NgModule({
  declarations: [GroupsTableComponent],
  imports: [CommonModule, MeTableModule, MeSearchInputModule, MeAddListCardModule, MeScrollToBottomModule],
  exports: [GroupsTableComponent],
  providers: [UsersStoreService, MeAddListPopupService, UserActionsService]
})
export class GroupsTableModule {}
