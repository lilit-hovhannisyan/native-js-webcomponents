import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MePermissionTypes } from '@shared/me-permission-types';

import { DocumentsComponent } from './documents.component';

const routes: Routes = [
  {
    path: '',
    component: DocumentsComponent,
    data: {
      permissions: MePermissionTypes.DOCUMENT_VIEW
    }
  },
  {
    path: 'visuals',
    component: DocumentsComponent,
    data: {
      permissions: MePermissionTypes.DOCUMENT_VIEW
    }
  },
  {
    path: 'create',
    data: {
      // This should be tested will it work when change language...
      breadcrumb: 'Create Document',
      permissions: MePermissionTypes.DOCUMENT_CREATE
    },
    loadChildren: () => import('../document-create-edit/document-create-edit.module').then((m) => m.DocumentCreateModule)
  },
  {
    path: 'edit/:id',
    data: {
      // This should be tested will it work when change language...
      breadcrumb: 'Edit Document',
      permissions: MePermissionTypes.DOCUMENT_MODIFY
    },
    loadChildren: () => import('../document-create-edit/document-create-edit.module').then((m) => m.DocumentCreateModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentsRoutingModule {}
