import { IDataResponseBody } from '../models/responses/IDataResponseBody';
import { map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { IIdCollection } from '../models/responses/IIDCollection';
import { IUser } from 'src/app/core/models/resources/IUser';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { NotificationType } from 'src/app/core/models/INotification';
import { HttpPost } from '../models/http/http-post';
import { HttpGet } from 'src/app/core/models/http/http-get';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';

interface IFavoriteVesselsReponse extends HttpResponse<IDataResponseBody<IIdCollection<number>>> { }

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private favoriteVesselAccountingsInitiallyFetched = false;
  private _favoriteVesselAccountings$: BehaviorSubject<number[]> = new BehaviorSubject([]);
  private readonly favoriteVesselAccountings$: Observable<number[]> = this._favoriteVesselAccountings$.asObservable();

  constructor(
    private http: HttpClient,
    private confirmationService: ConfirmationService,
    private userRepositoryService: UserRepositoryService,
    private notificationsService: NotificationsService
  ) { }

  public getFavoriteVesselAccountings(shouldRefresh = false) {
    if (!this.favoriteVesselAccountingsInitiallyFetched || shouldRefresh) {
      this.favoriteVesselAccountingsInitiallyFetched = true;
      this.http.request(new HttpGet('/api/user-account/chartering/favorite-vessel-accounting-ids'))
        .pipe(map((res: IFavoriteVesselsReponse) => res.body.data.items))
        .subscribe(res => this._favoriteVesselAccountings$.next(res));
    }
    return this.favoriteVesselAccountings$;
  }

  public removeFavoriteVesselAccounting(vesselAccountingIds: number[]): Observable<number[]> {
    const vesselIAccountingsIds = this._favoriteVesselAccountings$.getValue();
    this.setFavoriteVesselAccountings(vesselIAccountingsIds.filter(vid => !vesselAccountingIds.includes(vid)));
    return this.favoriteVesselAccountings$;
  }

  public setFavoriteVesselAccountings(vesselAccountingIds: number[]): Observable<number[]> {
    const request = new HttpPost('/api/user-account/chartering/favorite-vessel-accounting-ids', vesselAccountingIds);
    this.http.request(request).subscribe(() => this._favoriteVesselAccountings$.next([...vesselAccountingIds]));
    return this.favoriteVesselAccountings$;
  }

  public resetPassword(user: IUser) {
    this.confirmationService.confirm({
      subjects: { EMAIL: user.email },
      wording: wording.general.requestIsGoingToBeSent,
    }).subscribe(confirmed => {
      if (confirmed) {
        this.userRepositoryService
          .sendActivation(user.id)
          .subscribe(() => {
            this.notificationsService.notify(
              NotificationType.Success,
              wording.general.sent,
              setTranslationSubjects(
                wording.general.requestSent,
                { EMAIL: user.email },
              ),
            );
          });
      }
      });
  }
}
