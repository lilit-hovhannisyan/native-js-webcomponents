import { TrackingSystemInterceptor } from './../../features/tracking-system';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { CSRFInterceptor } from './csrf.interceptor';
import { ErrorHandlerInterceptor } from './error-handler.interceptor';

export const HttpInterceptorProviders = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: CSRFInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorHandlerInterceptor,
    multi: true
  },
  { provide: HTTP_INTERCEPTORS, useClass: TrackingSystemInterceptor, multi: true }
];
