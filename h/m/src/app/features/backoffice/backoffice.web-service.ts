import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { BaseWebService } from '@core-services/base.web-service';
import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { EntityI } from '@core-interfaces/entity.interface';
import { ArrayRequestI } from '@core-interfaces/array-request.interface';
import { UserModel } from '@shared/models/user/user.model';
import { GroupModel } from '@shared/models/groups/group.model';
import { CompanyModel } from '@shared/models/company/company.model';
import { DigitalProfileModel } from '@shared/models/digital-profile/digital-profile.model';
import { BACKOFFICE_BASE_API_URL } from '@shared/constants';
import { constructUrl } from '@shared/utils';
import { BaseSearchModel } from '@shared/models/search/search-base.model';

@Injectable()
export class BackOfficeService {
  constructor(private baseWebService: BaseWebService) {}

  getBoUsers(from?: number, to?: number): Observable<ArrayResponseI<UserModel>> {
    const url: string = constructUrl(`${BACKOFFICE_BASE_API_URL}/users`, from, to);
    return this.baseWebService.getRequestForArray<UserModel>(url, UserModel);
  }

  getBoUser(userOID: string) {
    return this.baseWebService.getRequest<UserModel>(`${BACKOFFICE_BASE_API_URL}/users/${userOID}`, UserModel);
  }

  searchBoUsers(data: BaseSearchModel, skip = 0, top = 50): Observable<ArrayResponseI<UserModel>> {
    const url: string = constructUrl(`${BACKOFFICE_BASE_API_URL}/users/search`, skip, top);
    return this.baseWebService.postRequestForArray<UserModel, BaseSearchModel>(url, data, UserModel);
  }

  updateBoUser(userOID: string, data: ArrayRequestI<EntityI>): Observable<UserModel> {
    return this.baseWebService.putRequest<UserModel, ArrayRequestI<EntityI>>(
      `${BACKOFFICE_BASE_API_URL}/users/${userOID}`,
      data,
      UserModel
    );
  }

  deleteBoUser(userOID: string): Observable<boolean> {
    return this.baseWebService.deleteRequest(`${BACKOFFICE_BASE_API_URL}/users/${userOID}`);
  }

  getBoUserGroups(userOID: string): Observable<ArrayResponseI<GroupModel>> {
    return this.baseWebService.getRequestForArray<GroupModel>(`${BACKOFFICE_BASE_API_URL}/users/${userOID}/groups`, GroupModel);
  }

  getBoGroups(from?: number, to?: number): Observable<ArrayResponseI<GroupModel>> {
    const url: string = constructUrl(`${BACKOFFICE_BASE_API_URL}/groups`, from, to);
    return this.baseWebService.getRequestForArray<GroupModel>(url, GroupModel);
  }

  getBoGroup(groupOID: string): Observable<GroupModel> {
    return this.baseWebService.getRequest<GroupModel>(`${BACKOFFICE_BASE_API_URL}/groups/${groupOID}`, GroupModel);
  }

  createBoGroup(data: EntityI): Observable<GroupModel> {
    return this.baseWebService.postRequest<GroupModel, EntityI>(`${BACKOFFICE_BASE_API_URL}/groups`, data);
  }

  updateBoGroup(groupOID: string, data: EntityI): Observable<GroupModel> {
    return this.baseWebService.putRequest<GroupModel, EntityI>(`${BACKOFFICE_BASE_API_URL}/groups/${groupOID}`, data, GroupModel);
  }

  addUserToBoGroup(groupOID: string, userOID: string): Observable<boolean> {
    return this.baseWebService.postRequest<boolean, any>(`${BACKOFFICE_BASE_API_URL}/groups/${groupOID}/users/${userOID}`);
  }

  deleteUserFromBoGroup(groupOID: string, userOID: string): Observable<boolean> {
    return this.baseWebService.deleteRequest<boolean>(`${BACKOFFICE_BASE_API_URL}/groups/${groupOID}/users/${userOID}`);
  }

  searchBoGroups(data: BaseSearchModel, skip = 0, top = 50): Observable<ArrayResponseI<GroupModel>> {
    const url: string = constructUrl(`${BACKOFFICE_BASE_API_URL}/groups/search`, skip, top);
    return this.baseWebService.postRequestForArray<GroupModel, BaseSearchModel>(url, data, GroupModel);
  }

  getBoCompanies(): Observable<ArrayResponseI<DigitalProfileModel>> {
    return this.baseWebService.getRequestForArray<DigitalProfileModel>(`${BACKOFFICE_BASE_API_URL}/companies`, DigitalProfileModel);
  }

  getBoCompany(companyOID: string): Observable<DigitalProfileModel> {
    return this.baseWebService.getRequest<DigitalProfileModel>(`${BACKOFFICE_BASE_API_URL}/companies/${companyOID}`, DigitalProfileModel);
  }

  searchBoCompany(data: BaseSearchModel, skip = 0, top = 50): Observable<ArrayResponseI<DigitalProfileModel>> {
    const url: string = constructUrl(`${BACKOFFICE_BASE_API_URL}/companies/search`, skip, top);
    return this.baseWebService.postRequestForArray<DigitalProfileModel, BaseSearchModel>(url, data, DigitalProfileModel);
  }

  updateBoCompany(companyOID: string, data: ArrayRequestI<EntityI>): Observable<CompanyModel> {
    return this.baseWebService.putRequest<CompanyModel, ArrayRequestI<EntityI>>(
      `${BACKOFFICE_BASE_API_URL}/companies/${companyOID}`,
      data,
      CompanyModel
    );
  }

  createBoCompany(data: ArrayRequestI<EntityI>): Observable<CompanyModel> {
    return this.baseWebService.postRequest<CompanyModel, ArrayRequestI<EntityI>>(`${BACKOFFICE_BASE_API_URL}/companies`, data);
  }

  getBoComapanyUsers(companyOID: string, from?: number, to?: number): Observable<ArrayResponseI<UserModel>> {
    const url: string = constructUrl(`${BACKOFFICE_BASE_API_URL}/companies/${companyOID}/users`, from, to);
    return this.baseWebService.getRequestForArray<UserModel>(url, UserModel);
  }

  getBoCompanyGroups(companyOID: string, from?: number, to?: number): Observable<ArrayResponseI<GroupModel>> {
    const url: string = constructUrl(`${BACKOFFICE_BASE_API_URL}/companies/${companyOID}/groups`, from, to);
    return this.baseWebService.getRequestForArray<GroupModel>(url, GroupModel);
  }

  getBoGroupUsers(groupOID: string): Observable<ArrayResponseI<UserModel>> {
    return this.baseWebService.getRequestForArray<UserModel>(`${BACKOFFICE_BASE_API_URL}/groups/${groupOID}/users`, UserModel);
  }

  getBoRoles(from?: number, to?: number): Observable<any> {
    const url: string = constructUrl(`${BACKOFFICE_BASE_API_URL}/roles`, from, to);
    return this.baseWebService.getRequest<any>(url, Array);
  }
}
