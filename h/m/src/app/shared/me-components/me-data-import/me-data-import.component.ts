import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, Injector, OnDestroy, ViewEncapsulation } from '@angular/core';
import { MePopupAccountService } from '../me-popup-account/me-popup-account.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BaseLoggerComponent } from '@features/tracking-system';
import { DataImportModel } from '@shared/models/data-import/data-import.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'me-data-import',
  templateUrl: './me-data-import.component.html',
  styleUrls: ['./me-data-import.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class MeDataImportComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  componentVersion: string = '0.0.1';

  public importDataForm!: FormGroup;
  @Output() sendFormData: EventEmitter<DataImportModel> = new EventEmitter<DataImportModel>();

  private _importCollectionsSubscription!: Subscription | undefined;
  private _importMaterialssSubscription!: Subscription | undefined;

  constructor(injector: Injector, private popupAccountService: MePopupAccountService, private formBuilder: FormBuilder) {
    super(injector);
  }

  ngOnInit(): void {
    this.importDataForm = this.formBuilder.group({
      importMaterials: [false],
      importCollections: [false],
      importUsers: [false]
    });

    this._importCollectionsSubscription = this.importDataForm.get('importCollections')?.valueChanges.subscribe((value: boolean) => {
      if (value) {
        this.importDataForm.get('importMaterials')?.setValue(true);
      }
    });

    this._importMaterialssSubscription = this.importDataForm.get('importMaterials')?.valueChanges.subscribe((value: boolean) => {
      if (!value) {
        this.importDataForm.get('importCollections')?.setValue(false);
      }
    });
  }

  submitForm(): void {
    this.sendFormData.emit(this.importDataForm.value);
  }

  closePopup(): void {
    this.popupAccountService.closePopup();
  }

  ngOnDestroy(): object {
    if (this._importCollectionsSubscription) {
      this._importCollectionsSubscription.unsubscribe();
    }
    if (this._importMaterialssSubscription) {
      this._importMaterialssSubscription.unsubscribe();
    }
    return super.ngOnDestroy();
  }
}
