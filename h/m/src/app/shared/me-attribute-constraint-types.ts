export enum MeAttributeConstraintTypes {
  DATE_TIME_RANGE = 'core.fc.constraints.rules.DateTimeRangeConstraint',
  DECIMAL_RANGE = 'core.fc.constraints.rules.DecimalRangeConstraint',
  FLOAT_RANGE = 'core.fc.constraints.rules.FloatRangeConstraint',
  IMMUTABLE = 'core.fc.constraints.rules.ImmutableConstraint',
  INTEGER_RANGE = 'core.fc.constraints.rules.IntegerRangeConstraint',
  MULTI_VALUED = 'core.fc.constraints.rules.MultiValuedConstraint',
  OBJECT_REF = 'core.fc.constraints.rules.ObjectRefConstraint',
  REQUIRED = 'core.fc.constraints.rules.RequiredConstraint',
  SINGLE_VALUED = 'core.fc.constraints.rules.SingleValuedConstraint',
  STRING_LENGTH = 'core.fc.constraints.rules.StringLengthConstraint'
}
