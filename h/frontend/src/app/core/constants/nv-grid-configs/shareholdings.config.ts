import { NvColumnDataType, NvFilterControl, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';
import { getContextMenuButtonsConfig } from '../../helpers/general';
import { system } from '../wording/system';
import { wordingGeneral } from '../wording/general';
import { Operations } from '../../models/Operations';

export const getShareholdingsGridConfig = (
  actions: IGridConfigActions,
  canPerform: (operations: Operations) => boolean,
): NvGridConfig => {
  return ({
    gridName: 'shareholdingsGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 50
    },
    columns: [
      {
        key: 'businessName',
        title: system.businessName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'shareCapitalOption',
        title: system.shareCapital_v4,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.Decimal,
        filter: {
          values: []
        }
      },
      {
        key: 'currency',
        title: system.currency,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isDeleted',
        title: system.deleted,
        width: 100,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: ['0']
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isSold',
        title: system.sold,
        width: 100,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: ['0']
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wordingGeneral.edit,
        name: wordingGeneral.edit,
        tooltip: wordingGeneral.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        func: actions.edit,
        hidden: !canPerform(Operations.UPDATE)
      },
      {
        icon: 'delete',
        description: wordingGeneral.delete,
        name: wordingGeneral.delete,
        tooltip: wordingGeneral.delete,
        actOnDoubleClick: false,
        func: actions?.delete,
        hidden: !canPerform(Operations.DELETE)
      }
    ],
    toolbarButtons: [
      {
        title: wordingGeneral.create,
        tooltip: wordingGeneral.create,
        icon: 'plus-circle-o',
        func: actions?.add,
        hidden: () => !canPerform(Operations.CREATE)
      }
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};

