import { RouteGuard } from './services/route-guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessControlGuard } from './services/access-control.guard';
import { LoginGuard } from './services/login-guard';
import { ResetPasswordGuard } from './services/reset-password.guard';

const appRoutes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('../authentication/authentication.module').then(m => m.AuthenticationModule),
  },
  {
    path: 'base',
    canActivateChild: [RouteGuard, AccessControlGuard],
    loadChildren: () => import('../features/base/base.module').then(m => m.BaseModule),
  },
  {
    path: '**',
    redirectTo: '/base/main'
  }
];

/**
 * @see https://angular.io/guide/lazy-loading-ngmodules
 */
@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false // only for debugging
      }
    )
  ],
  providers: [
    RouteGuard,
    LoginGuard,
    ResetPasswordGuard
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
