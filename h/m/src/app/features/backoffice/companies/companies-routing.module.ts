import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompaniesComponent } from './companies.component';

const routes: Routes = [
  { path: '', component: CompaniesComponent },
  { path: 'create', loadChildren: () => import('./company-create-edit/company-create-edit.module').then((m) => m.CompanyCreateEditModule) },
  {
    path: 'edit/:id',
    data: {
      breadcrumb: 'Edit Company'
    },
    loadChildren: () => import('./company-create-edit/company-create-edit.module').then((m) => m.CompanyCreateEditModule)
  },
  {
    path: 'view/:id',
    data: {
      breadcrumb: 'View Company'
    },
    loadChildren: () => import('./company-view/company-view.module').then((m) => m.CompanyViewModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompaniesRoutingModule {}
