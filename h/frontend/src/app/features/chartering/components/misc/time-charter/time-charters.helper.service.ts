
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as moment from 'moment';
import { ITimeCharter } from 'src/app/core/models/resources/ITimeCharter';
import { SettingsService } from 'src/app/core/services/settings.service';
import { IVoyageFull } from 'src/app/core/models/resources/IVoyage';
import { wording } from 'src/app/core/constants/wording/wording';
import { dateFormatTypes } from 'src/app/core/models/dateFormat';

@Injectable()
export class TimeChartersHelperService {
  public editViewTitleSubject: BehaviorSubject<string> = new BehaviorSubject('');
  public editViewTitle$ = this.editViewTitleSubject.asObservable();
  constructor(
    public settingsService: SettingsService,
  ) { }

  public generateTitle = (voyage: IVoyageFull, timeCharter?: ITimeCharter) => {
    const dateFormat = dateFormatTypes[this.settingsService.locale];
    const charterRange = timeCharter
      ? `(${moment(timeCharter.from).format(dateFormat)}
        - ${moment(timeCharter.until).format(dateFormat)})`
      : '';

    const title = `
      <p>
        ${wording.chartering.timeCharter[this.settingsService.language]}
        ${charterRange}
      </p>
      <p>
        ${wording.chartering.voyage[this.settingsService.language]}:
        ${voyage.vesselAccounting.vesselNo.toString().padStart(4, '0')}
        ${voyage.vesselAccounting.vesselName}
        ${voyage.year} -
        ${voyage.no.toString().padStart(3, '0')}
        (${moment(voyage.from).format(dateFormat)} -
        ${moment(voyage.until).format(dateFormat)})
      </p>`;

    this.editViewTitleSubject.next(title);
  }
}
