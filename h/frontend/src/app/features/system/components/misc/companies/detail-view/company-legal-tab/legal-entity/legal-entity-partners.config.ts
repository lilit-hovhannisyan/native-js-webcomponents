import { NvGridButtonsPosition, NvGridConfig, NvToolbarButton, NvButton } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { ICompany } from 'src/app/core/models/resources/ICompany';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { COMPANY_LEGAL_FORM } from 'src/app/core/models/enums/company-legal-form';

const companiesUrl = url(sitemap.system.children.misc.children.companies);
const legalTabPath: string = sitemap.system.children.misc.children.companies.children.id.children.legal.path;
const basicTabPath: string = sitemap.system.children.misc.children.companies.children.id.children.basic.path;

export const getLegalEntityPartnersGridConfig = (
  legalForm: COMPANY_LEGAL_FORM,
): NvGridConfig => {

  const toolbarButtons: NvToolbarButton[] = [];
  const buttons: NvButton[] = [
    {
      icon: 'eye',
      description: wording.system.show,
      name: 'showCompany',
      tooltip: wording.system.show,
      actOnEnter: false,
      actOnDoubleClick: false,
      func: (company: ICompany) => {
        company.isPrivatePerson
        ? window.open(`${companiesUrl}/${company.id}/${basicTabPath}`, '_blank')
        : window.open(`${companiesUrl}/${company.id}/${legalTabPath}`, '_blank');
      },
    }
  ];

  const config: NvGridConfig = {
    gridName: 'LegalEntityPartners',
    hideSettingsButton: true,
    hideAllFilters: true,
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    showPaging: false,
    editForm: {
      allowCreateNewRow: false,
      showDiscardChangesButton: true,
    },
    columns: [
      {
        key: 'id',
        hidden: true,
        visible: false,
      },
      {
        key: 'companyName',
        title: legalForm === COMPANY_LEGAL_FORM.LimitedPartnership
          ? wording.system.generalPartner
          : wording.system.managingPartners,
        width: 450,
        editControl: {
          editable: false,
        }
      },
    ],
    buttons,
    toolbarButtons,
  };

  return config;
};
