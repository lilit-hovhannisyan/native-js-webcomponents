export interface IWidgetListItem {
  name: string;
  type: number;
  icon: string;
}
