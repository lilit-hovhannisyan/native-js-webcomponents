import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from 'src/app/core/abstracts/abstract.repository';
import {
  CharteringExpenseKey,
  ICharteringExpense,
  ICharteringExpenseCreate,
  ICharteringExpenseDTO,
} from 'src/app/core/models/resources/ICharteringExpense';
import { RepositoryService } from 'src/app/core/services/repository.service';

@Injectable({
  providedIn: 'root',
})
export class ExpensesRepositoryService extends AbstractRepository<ICharteringExpense,
  ICharteringExpenseDTO,
  ICharteringExpenseCreate,
  RepoParams<ICharteringExpense>,
  CharteringExpenseKey> {
  protected resourceType = 'Expense';
  protected sortAsce = true;
  protected sortKey = 'shortName';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = (): string => '/api/chartering-expenses';

  public postProcess = (item: ICharteringExpenseDTO): ICharteringExpense => item;
}
