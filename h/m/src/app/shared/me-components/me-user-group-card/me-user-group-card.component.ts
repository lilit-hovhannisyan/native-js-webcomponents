import { BaseLoggerComponent } from '@features/tracking-system';
import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, Injector, OnInit } from '@angular/core';
import { MeUserGroupCardI } from './me-user-group-card.interface';
import { MeUserImageI } from '../me-user-image/me-user-image.interface';

@Component({
  selector: 'me-user-group-card',
  templateUrl: './me-user-group-card.component.html',
  styleUrls: ['./me-user-group-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeUserGroupCardComponent extends BaseLoggerComponent implements OnInit {
  componentVersion: string = '0.0.1';
  userImage!: MeUserImageI;

  @Input() dataModel!: MeUserGroupCardI;
  @Output() userGroupCardOutputEvent: EventEmitter<{ eventName: string; payload: unknown }> = new EventEmitter();

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.userImage = {
      type: this.dataModel.thumbnail ? 'avatar' : 'initials',
      size: 'large',
      shape: 'rounded',
      initials: this.dataModel.userInitials || undefined,
      imageUrl: this.dataModel.thumbnail,
      alt: ''
    };
  }

  customEvent(eventName: string, payload: unknown = null): void {
    this.userGroupCardOutputEvent.emit({ eventName, payload });
  }

  preventOtherEvents(e: Event): void {
    e.stopPropagation();
  }
}
