
export interface IWriteProtection {
  editable: boolean;
}
