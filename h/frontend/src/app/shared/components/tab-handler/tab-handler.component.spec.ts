import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabHandlerComponent } from './tab-handler.component';

describe('TabHandlerComponent', () => {
  let component: TabHandlerComponent;
  let fixture: ComponentFixture<TabHandlerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabHandlerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabHandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
