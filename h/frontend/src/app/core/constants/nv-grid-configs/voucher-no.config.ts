import { NvGridConfig, NvColumnDataType, NvGridRowSelectionType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getVoucherNumberGridConfig = (): NvGridConfig => {
  return ({
    gridName: 'voucherNumberConfig',
    hideRefreshButton: true,
    rowSelectionType: NvGridRowSelectionType.RadioButton,
    rowButtonsPosition: null,
    sortBy: 'voucherNumber',
    isSortAscending: true,
    clearFiltersButton: true,
    columns: [
      {
        key: 'voucherNumber',
        title: wording.accounting.voucherNo_2,
        width: 150,
        isSortable: true
      },
      {
        key: 'barcode',
        title: wording.accounting.barcode,
        width: 150,
        isSortable: true
      },
      {
        key: 'companyName',
        title: wording.accounting.supplier,
        width: 150,
        isSortable: true
      },
      {
        key: 'invoiceNumber',
        title: wording.accounting.invoiceNo,
        width: 150,
        isSortable: true
      },
      {
        key: 'invoiceDate',
        title: wording.accounting.invoiceDate,
        dataType: NvColumnDataType.Date,
        width: 150,
        isSortable: true
      },
      {
        key: 'amount',
        title: wording.accounting.amount,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.Decimal
      },
      {
        key: 'currencyLabel',
        title: wording.accounting.isoCode,
        width: 150,
        isSortable: true
      },
    ],
    buttons: [],
    toolbarButtons: [],
  });
};
