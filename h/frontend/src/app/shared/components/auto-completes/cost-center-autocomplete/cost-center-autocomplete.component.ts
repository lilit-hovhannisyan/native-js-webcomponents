import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { CostCenterRepositoryService } from 'src/app/core/services/repositories/cost-center-repository.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup } from '@angular/forms';
import { IWording } from 'src/app/core/models/resources/IWording';
import { getCostCentersGridConfig } from 'src/app/core/constants/nv-grid-configs/cost-centers.config';

@Component({
  selector: 'nv-cost-center-autocomplete',
  templateUrl: './cost-center-autocomplete.component.html',
  styleUrls: ['./cost-center-autocomplete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CostCenterAutocompleteComponent implements OnInit {
  public wording = wording;
  public gridConfig = getCostCentersGridConfig({}, () => false);

  @Input() public predefineIfOnlyField: boolean;
  @Input() public form: FormGroup;
  @Input() public controlName = 'costCenterId';
  @Input() public filters: object;
  @Input() public onlyOfBookingAccount: boolean;
  @Input() public showLabel = true;
  @Input() public label: IWording;

  @Input() public setId = true;
  @Input() public displayOneProperty: boolean;
  @Input() public openListOnInit: boolean;
  @Input() public autofocus: boolean;

  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();

  constructor(
    public costCenterRepo: CostCenterRepositoryService,
  ) { }

  public ngOnInit() {
    this.costCenterRepo.fetchAll({}).subscribe();
  }
}
