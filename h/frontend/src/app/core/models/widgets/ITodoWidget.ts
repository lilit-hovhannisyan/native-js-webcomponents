import { IWidgetDTO } from './IWidget';
import { GridsterItem } from 'angular-gridster2';

export interface ITodoWidget extends GridsterItem, IWidgetDTO {
}

export interface ITodoWidgetDTO extends IWidgetDTO {
}
