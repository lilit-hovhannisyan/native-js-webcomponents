import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GridConfigService } from '../../../../../../core/services/grid-config.service';
import { ICostTypeFull, ICostTypeRow } from '../../../../../../core/models/resources/ICostTypes';
import { CostTypeMainService } from '../../../../../../core/services/mainServices/cost-type-main.service';
import { AuthenticationService } from '../../../../../../authentication/services/authentication.service';
import { getGridConfig } from '../../../../../../core/constants/nv-grid-configs/cost-type.config';
import { wording } from '../../../../../../core/constants/wording/wording';
import { ConfirmationService } from '../../../../../../core/services/confirmation.service';
import { CostTypeRepositoryService } from '../../../../../../core/services/repositories/cost-type-repository.service';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { AccountCostTypeMainService } from '../../../../../../core/services/mainServices/account-cost-type-main.service';

@Component({
  selector: 'nv-cost-types',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CostTypesListViewComponent implements OnInit {
  public dataSource$: Observable<ICostTypeRow[]>;
  public costTypeGridConfig: NvGridConfig;

  constructor(
    public gridConfigService: GridConfigService,
    private mainService: CostTypeMainService,
    private costTypeRepo: CostTypeRepositoryService,
    private router: Router,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private accountCostTypeMainService: AccountCostTypeMainService
  ) { }

  public ngOnInit(): void {
    this.costTypeRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.costTypeRepo
      .getStream()
      .pipe(
        map(costTypes => this.mainService.toRows(costTypes))
      );

    const gridActions = {
      edit: (row: ICostTypeFull) => { this.router.navigate([`${this.router.url}/${row.id}`]); },
      delete: (row: ICostTypeRow) => this.deleteCostType(row),
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    const canPerform = (operation: number) => this.authService
      .canPerformOperationOnCurrentUrl(operation);
    this.costTypeGridConfig = getGridConfig(
      gridActions,
      canPerform
    );

  }

  private deleteCostType(costType: ICostTypeRow) {
    this.accountCostTypeMainService.getAccountRelatedToCostType(costType.id)
      .subscribe( ctRelAccounts => {
        if (ctRelAccounts.length) {
          this.showdeletionNotPossibleConfirmationMsgBox();
        } else {
          this.showDeleteConfirmationMsgBox(costType);
        }
      });
  }

  private showdeletionNotPossibleConfirmationMsgBox(): void {
    this.confirmationService.confirm({
      okButtonWording: wording.general.ok,
      cancelButtonWording: null,
      wording: wording.accounting.ctDeletionNotPossible,
    }).subscribe();
  }

  private showDeleteConfirmationMsgBox(costType: ICostTypeRow): void {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: setTranslationSubjects(
        wording.general.areYouSureYouWantToDelete,
        { subject: costType.displayLabel },
      ),
    }).subscribe(confirmed => {
      if (confirmed) {
        this.costTypeRepo.delete(costType.id, {}).subscribe();
      }
    });
  }
}
