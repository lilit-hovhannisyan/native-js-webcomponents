import {Component, EventEmitter, Output} from '@angular/core';
import {UserApi} from '../molecules.interfaces';

@Component({
    selector: 'lavash-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.less']
})
export class LoginComponent {
    constructor(private userApi: UserApi) {}

    @Output() sendSuccess = new EventEmitter();

    onSend(event) {
        console.log('minchev hello', event);
        this.userApi.login({aa: 44}, true).subscribe(
            (data) => {
                console.log('manipulate data');
                this.sendSuccess.emit('eeeeee');
            },
            (err) => {

            },
            () => {
                console.log('complete');
            }
        );
    }
}