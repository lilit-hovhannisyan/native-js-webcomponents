import { MeContextMenuI } from '../me-context-menu/me-context-menu-inteface';

export interface MeDocumentLineI {
  id: string;
  oid: string;
  selectionEnabled: boolean;
  selected: boolean;

  documentName: string;
  creationDate: string;
  documentThumbnail?: string;
  documentExtensionIcon?: string;

  contextMenu: MeContextMenuI;
}
