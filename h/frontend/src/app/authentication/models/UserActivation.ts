export interface UserActivation {
  baseUrl: string;
  userId?: number;
  email?: string;
}
