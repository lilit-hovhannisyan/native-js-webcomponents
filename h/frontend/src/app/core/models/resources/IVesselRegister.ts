import { IResource } from './IResource';
import { CountryKey } from './ICountry';
import { PortKey } from './IPort';
import { VesselKey } from './IVessel';
import { HttpResponse } from '@angular/common/http';
import { IDataResponseBody } from '../responses/IDataResponseBody';
import { IResourceCollection } from './IResourceCollection';
import { CompanyKey } from './ICompany';

export type VesselRegisterKey = number;

export enum RegistrationTypes {
  Bareboat = 1,
  Permanent = 2,
}
interface ISharedProps {
  registerCourt?:	string;
  primaryPortId?: PortKey;
  secondaryPortId?: PortKey;
  vesselId: VesselKey;
  client: string;
  remarks: string;
  ssrNo: number;
  type: RegistrationTypes;
  primaryCountryId: CountryKey;
  secondaryCountryId?: CountryKey;
  primaryCallSign?:	string;
  secondaryCallSign?:	string;
  primaryCompanyId?: CompanyKey;
  secondaryCompanyId?: CompanyKey;
  from:	string;
  until: string;
  isTraining: boolean;
}

export interface IVesselRegisterCreate extends ISharedProps { }

export interface IVesselRegisterBase extends ISharedProps, IResource<VesselRegisterKey> { }

export interface IVesselRegisterDTO extends IVesselRegisterBase { }

export interface IVesselRegister extends IVesselRegisterDTO { }

export declare type IVesselRegisterCollection = HttpResponse<IDataResponseBody<IResourceCollection<IVesselRegisterDTO>>>;

export declare type IVesselRegisterCollectionItem = HttpResponse<IDataResponseBody<IVesselRegisterDTO>>;
