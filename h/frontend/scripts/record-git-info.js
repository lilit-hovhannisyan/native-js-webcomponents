const { promisify } = require("util");
const exec = promisify(require("child_process").exec);
const { writeFileSync } = require("fs");
const { join } = require("path");

const getGitInfo = async () => {
  const commits = (await exec("git log --oneline -n 50")).stdout
    .split("\n")
    .filter(commit => !commit.includes('Merge branch'))
    .slice(0, 10);

  const tag = (await exec("git describe --abbrev=0")).stdout.trimRight("\n");
  return { tag, commits };
};

getGitInfo().then(({ tag, commits }) => {
  let info = {
    commits: commits.map(c => ({
      hash: c.split(" ")[0],
      msg: c
        .split(" ")
        .slice(1)
        .join(" ")
    })),
    tag
  };
  writeFileSync(
    join(__dirname, "../src/git-version.json"),
    JSON.stringify(info, null, 2)
  );
});
