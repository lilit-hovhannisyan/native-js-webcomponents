import { RoleUsersComponent } from './components/misc/roles/detail-view/users/role-users.component';
import { RoleMandatorsComponent } from './components/misc/roles/detail-view/mandators/role-mandators.component';
import { CountryListViewComponent } from './components/misc/countries/listView/list-view.component';
import { CountryDetailViewComponent } from './components/misc/countries/detailView/detail-view.component';
import { BankDetailViewComponent } from './components/misc/banks/detail-view/detail-view.component';
import { BankListViewComponent } from './components/misc/banks/list-view/list-view.component';
import { CompanyContactTabComponent } from './components/misc/companies/detail-view/company-contact-tab/company-contact-tab.component';
import { CompanyAddressTabComponent } from './components/misc/companies/detail-view/company-address-tab/company-address-tab.component';
import { CompanyBankAccountsTabComponent } from './components/misc/companies/detail-view/company-bank-accounts-tab/company-bank-accounts-tab.component';
import { CompanyBasicComponent } from './components/misc/companies/detail-view/company-basic/company-basic.component';
import { WriteProtectionService } from 'src/app/core/services/ui/write-protection.service';
import { FileUploaderService } from 'src/app/shared/services/file-uploader.service';
import { SystemRoutingModule, SYSTEM_DECLARATIONS } from './system-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { RoleBasicComponent } from './components/misc/roles/detail-view/role-basic/role-basic.component';
import { UserRolesComponent } from './components/misc/users/detail-view/user-roles/user-roles.component';
import { UserBasicComponent } from './components/misc/users/detail-view/user-basic/user-basic.component';
import { RangeStartEndComponent } from './components/settings/global/range-start-end/range-start-end.component';
import { CurrencyBasicComponent } from './components/misc/currencies/detail-view/currency-basic/currency-basic.component';
import { CurrencyExchangeRatesComponent } from './components/misc/currencies/detail-view/currency-exchange-rates/currency-exchange-rates.component';
import { EmailDomainsComponent } from './components/settings/global/email-domains/email-domains.component';
import { MaintenanceDetailViewComponent } from './components/settings/maintenance/maintenance-detail-view/maintenance-detail-view.component';
import { CompanyCategoriesModalComponent } from './components/misc/companies/detail-view/company-categories-tab/company-categories-modal/company-categories-modal.component';
import { ContributionModalComponent } from './components/misc/companies/detail-view/company-legal-tab/shareholders-partners/contribution-modal/contribution-modal.component';
import { ContributionEditModalComponent } from './components/misc/companies/detail-view/company-legal-tab/shareholders-partners/contribution-edit-modal/contribution-edit-modal.component';
import { RepresentativesDetailViewModalComponent } from './components/misc/companies/detail-view/company-legal-tab/representatives/representatives-detail-view-modal/representatives-detail-view-modal.component';
import { ShareholdingModalComponent } from './components/misc/shareholdings/shareholding-modal/shareholding-modal.component';
import { SilentPartnersModalComponent } from './components/misc/companies/detail-view/company-legal-tab/silent-partners/silent-partners-modal/silent-partners-modal.component';
import { CompanyLegalEntityGuardService } from './services/company-legal-entity.guard';
import { TaskCategoriesComponent } from './components/misc/task-templates/task-categories/task-categories.component';
import { CreateCategoryComponent } from './components/misc/task-templates/task-categories/create-category/create-category.component';
import { TaskTemplatesDetailComponent } from './components/misc/task-templates/task-templates-detail/task-templates-detail.component';
import { TaskTemplateCreateComponent } from './components/misc/task-templates/task-templates-detail/task-template-create/task-template-create.component';
import { TaskTemplateChecklistComponent } from './components/misc/task-templates/task-templates-detail/task-template-checklist/task-template-checklist.component';
import { LegalEntityBusinessNamesModalComponent } from './shared/legal-entity-business-names-modal/legal-entity-business-names-modal.component';
import { LegalEntityPartnersModalComponent } from './shared/legal-entity-partners-modal/legal-entity-partners-modal.component';
import { LegalTypeDropdownComponent } from './shared/legal-type-dropdown/legal-type-dropdown.component';
import { LegalTypeTreeComponent } from './shared/legal-type-tree/legal-type-tree.component';
import { TrusteePartnerContributionEditModalComponent } from './shared/trustee-partner-contributions/trustee-partner-contribution-edit-modal/trustee-partner-contribution-edit-modal.component';
import { TrusteePartnerContributionModalComponent } from './shared/trustee-partner-contributions/trustee-partner-contribution-modal/trustee-partner-contribution-modal.component';
import { TrusteePartnerContributionsDetailComponent } from './shared/trustee-partner-contributions/trustee-partner-contributions-detail/trustee-partner-contributions-detail.component';
import { TrusteePartnerContributionsListComponent } from './shared/trustee-partner-contributions/trustee-partner-contributions-list/trustee-partner-contributions-list.component';
import { LegalEntityBusinessNamesComponent } from './components/misc/companies/detail-view/company-basic/business-names/business-names.component';

const ENTRY_COMPONENTS = [
  CompanyCategoriesModalComponent,
  ContributionModalComponent,
  ContributionEditModalComponent,
  SilentPartnersModalComponent,
  LegalEntityBusinessNamesModalComponent,
  LegalEntityPartnersModalComponent,
  TrusteePartnerContributionEditModalComponent,
  TrusteePartnerContributionModalComponent,
];

@NgModule({
  imports: [
    SystemRoutingModule,
    SharedModule
  ],
  declarations: [
    ...SYSTEM_DECLARATIONS,
    ...ENTRY_COMPONENTS,
    CountryListViewComponent,
    CountryDetailViewComponent,
    BankDetailViewComponent,
    BankListViewComponent,
    CompanyBasicComponent,
    CompanyBankAccountsTabComponent,
    CompanyAddressTabComponent,
    CompanyContactTabComponent,
    UserBasicComponent,
    UserRolesComponent,
    RoleBasicComponent,
    RoleMandatorsComponent,
    RangeStartEndComponent,
    RoleUsersComponent,
    CurrencyBasicComponent,
    CurrencyExchangeRatesComponent,
    EmailDomainsComponent,
    MaintenanceDetailViewComponent,
    RepresentativesDetailViewModalComponent,
    ShareholdingModalComponent,
    LegalEntityBusinessNamesComponent,
    TaskCategoriesComponent,
    CreateCategoryComponent,
    TaskTemplatesDetailComponent,
    TaskTemplateCreateComponent,
    TaskTemplateChecklistComponent,
    LegalTypeDropdownComponent,
    LegalTypeTreeComponent,
    TrusteePartnerContributionsDetailComponent,
    TrusteePartnerContributionsListComponent,
  ],
  entryComponents: ENTRY_COMPONENTS,
  providers: [
    FileUploaderService,
    WriteProtectionService,
    CompanyLegalEntityGuardService,
  ]
})
export class SystemModule {
}
