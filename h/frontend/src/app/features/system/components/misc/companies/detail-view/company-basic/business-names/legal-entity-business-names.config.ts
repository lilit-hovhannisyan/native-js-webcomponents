import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { SettingsService } from 'src/app/core/services/settings.service';
import { IBusinessName } from 'src/app/core/models/resources/IBusinessName';
import { NvLocale, dateFormatTypes } from 'src/app/core/models/dateFormat';
import { Language } from 'src/app/core/models/language';
import * as moment from 'moment';
import { IWording } from 'src/app/core/models/resources/IWording';

export const getLegalEntityBusinessNamesGridConfig = (
  actions: IGridConfigActions,
  isGridDisabled: () => boolean,
  canPerform: (operation: Operations) => boolean,
  settingsService: SettingsService,
  businessNameWording: IWording,
  latestBusinessName: IBusinessName,
): NvGridConfig => {
  const language: Language = settingsService.language;
  const locale: NvLocale = settingsService.locale;
  const dateFormat: string = dateFormatTypes[locale];

  return ({
    gridName: 'legalEntityBusinessNamesGrid',
    hideSettingsButton: true,
    hideAllFilters: true,
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    tooltipDisabled: isGridDisabled(),
    showPaging: false,
    editForm: {
      allowCreateNewRow: false,
      showDiscardChangesButton: true,
    },
    columns: [
      {
        key: 'id',
        visible: false,
      },
      {
        key: 'name',
        title: businessNameWording,
        width: 300,
      },
      {
        key: 'from',
        width: 200,
        customFormatFn: (row: IBusinessName) => {
          const fromWord: string = wording.system.from[language];
          const fromValue: string = moment(row.from).format(dateFormat);
          return `${fromWord} ${fromValue}`;
        }
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editBusinessName',
        tooltip: wording.general.edit,
        actOnDoubleClick: false,
        hidden: (businessName: IBusinessName) => {
          return !canPerform(Operations.UPDATE);
        },
        func: actions.edit,
        disabled: (businessName: IBusinessName) => {
          return isGridDisabled() || businessName?.id !== latestBusinessName?.id;
        },
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteBusinessName',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: () => !canPerform(Operations.DELETE),
        func: actions.delete,
        disabled: isGridDisabled,
      },
      {
        icon: 'eye',
        description: wording.system.show,
        name: 'showBusinessName',
        tooltip: wording.system.show,
        actOnEnter: false,
        actOnDoubleClick: false,
        func: row => actions.show(row),
        disabled: isGridDisabled,
        hidden: () => !canPerform(Operations.READ),
      },
    ],
    toolbarButtons: [
      {
        title: wording.general.add,
        tooltip: wording.general.add,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: actions.add,
        disabled: isGridDisabled,
      }
    ]
  });
};
