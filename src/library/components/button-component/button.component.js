import {BaseComponent} from '../../core/base-component/base.component.js';
import {
  externalStyleBasePath,
  canElementBeRendered,
} from '../../helpers/random.helpers.js';


export class ButtonComponent extends BaseComponent {
  constructor() {
    super(externalStyleBasePath + 'button-component/button.component.css');
  }

  render = () => {
    this.clickAction = this.getAttribute('click');
    this.btnIcon = this.getAttribute('icon') || '';
    this.btnText = this.getAttribute('text') || this.innerText || '';

    const customClassName = canElementBeRendered(this.getAttribute('style'))
      ? this.getAttribute('style') : '';

    const subElements = [
      {
        tagName: 'icon-component',
        attr: {
          icon: this.btnIcon,
          render: canElementBeRendered(this.getAttribute('icon'))
        }
      },
      {
        tagName: 'text-component',
        attr: {
          type: 'span',
          text: this.btnText,
          render: canElementBeRendered(this.getAttribute('text'))
        }
      }
    ];

    const btnElConfig = {
      tagName: 'button',
      attr: {
        class: 'l-button ' + customClassName, // styles: 'fill' | 'auto' | 'fixed'
      },
      events: {
        click: eval(this.clickAction)
      },
      subElements
    };

    const btnEl = this.createElementWithAttributes(btnElConfig);

    this.removeStaticAtributesFromElNotToRender(this, ['click', 'icon', 'text']);
    this.shadow.appendChild(btnEl);
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }
}