import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { wording } from 'src/app/core/constants/wording/wording';
import { taskTypes } from 'src/app/core/models/enums/vessel-registration-tasks';
import { ITaskTemplateCategory } from 'src/app/core/models/resources/Task-Templates/ITaskTemplateCategory';
import { TaskTemplateCategoryRepositoryService } from 'src/app/core/services/repositories/task-templates/task-template-category-repository.service';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';

// modal
@Component({
  selector: 'nv-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateCategoryComponent implements OnInit {
  @Input() public closeModal: () => void;
  @Input() public category?: ITaskTemplateCategory;

  public form: FormGroup;
  public taskTypes = taskTypes;
  public wording = wording;

  constructor(
    private fb: FormBuilder,
    private taskTemplateCategoryRepositoryService: TaskTemplateCategoryRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.initForm(this.category);
  }

  public initForm(category: ITaskTemplateCategory = {} as ITaskTemplateCategory) {
    this.form = this.fb.group({
      isSystemDefault: [category.isSystemDefault],
      type: [category.type, [Validators.required]],
      labelDe: [category.labelDe, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      labelEn: [category.labelEn, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    });
  }

  public save(): void {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
      return;
    }

    const { isSystemDefault, type, labelDe, labelEn } = this.form.value;

    const payload = {
      ...this.category,
      isSystemDefault,
      type,
      labelDe,
      labelEn,
    };

    this.category?.id
      ? this.taskTemplateCategoryRepositoryService.update({
        ...this.category,
        ...payload,
      }, {}).subscribe(() => this.closeModal())
      : this.taskTemplateCategoryRepositoryService.create({
        ...payload
      }, {}).subscribe(() => this.closeModal());
  }

}
