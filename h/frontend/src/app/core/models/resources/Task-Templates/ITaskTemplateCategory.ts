import { TaskType } from '../../enums/vessel-registration-tasks';
import { IResource } from '../IResource';
import { IWithLabels } from '../IWithLabels';

export type TaskTemplateCategoryKey = number;

interface ISharedProps extends IWithLabels {
  /**
   * extends
   * labelDe: string;
   * labelEn: string;
   */
  isSystemDefault: boolean;
  type: TaskType;
}

export interface ITaskTemplateCategoryCreate extends ISharedProps { }

export interface ITaskTemplateCategoryBase extends ISharedProps, IResource<TaskTemplateCategoryKey> { }

export interface ITaskTemplateCategoryDTO extends ITaskTemplateCategoryBase { }

export interface ITaskTemplateCategory extends ITaskTemplateCategoryDTO { }
