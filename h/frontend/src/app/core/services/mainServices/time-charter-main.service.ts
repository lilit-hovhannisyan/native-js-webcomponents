import { Injectable } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { DebitorCreditorRepositoryService } from './../repositories/debitor-creditor-repository.service';
import { ICompany } from './../../models/resources/ICompany';
import { IVoyage } from './../../models/resources/IVoyage';
import { IDebitorCreditor } from './../../models/resources/IDebitorCreditor';
import { TimeCharterRepositoryService, ITimeCharterRepoParams } from 'src/app/core/services/repositories/timeCharter.repository.service';
import { ITimeCharter, ITimeCharterFull } from '../../models/resources/ITimeCharter';
import { VoyagesRepositoryService } from '../repositories/voyages-repository.service';
import { CompanyRepositoryService } from '../repositories/company-repository.service';


export interface ITimeCharterRow extends ITimeCharterFull {
  companyName: string;
}

@Injectable({ providedIn: 'root' })
export class TimeCharterMainService {
  constructor(
    private timeCharterRepo: TimeCharterRepositoryService,
    private debitorCreditorRepo: DebitorCreditorRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private voyagesRepo: VoyagesRepositoryService,
  ) {}

  public fetchAll(params: ITimeCharterRepoParams): Observable<ITimeCharterFull[]> {
    return combineLatest([
      this.timeCharterRepo.fetchAll(params),
      this.debitorCreditorRepo.fetchAll({}),
      this.voyagesRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
    ]).pipe(map(res => this.getTimeChartersFullList(...res)));
  }

  private getTimeChartersFullList(
    timeCharters: ITimeCharter[],
    debitorAndCreditors: IDebitorCreditor[],
    voyagesList: IVoyage[],
    companiesList: ICompany[],
  ): ITimeCharterFull[] {
    return timeCharters.map(tc => {
      return this.getTimeCharterFull(tc, debitorAndCreditors, voyagesList, companiesList);
    });
  }

  private getTimeCharterFull(
    timeCharter: ITimeCharter,
    debitorAndCreditorsList: IDebitorCreditor[],
    voyagesList: IVoyage[],
    companiesList: ICompany[],
  ): ITimeCharterFull {
    const voyage: IVoyage = voyagesList.find(v => v.id === timeCharter.voyageId);
    const debitorCreditor: IDebitorCreditor = debitorAndCreditorsList.find(dc => {
      return dc.id === timeCharter.debitorCreditorId;
    });
    const company: ICompany = companiesList.find(c => c.id === debitorCreditor.companyId);

    return { ...timeCharter, debitorCreditor, voyage, company } as ITimeCharterFull;
  }

  public toRow(timeCharterFull: ITimeCharterFull): ITimeCharterRow {
    const companyName = timeCharterFull.company.companyName;
    return { ...timeCharterFull, companyName } as ITimeCharterRow;
  }

  public toRows(timeChartersFullList: ITimeCharterFull[]): ITimeCharterRow[] {
    return timeChartersFullList.map(tcf => this.toRow(tcf));
  }
}
