import { Component, OnInit } from '@angular/core';
import { MaintenanceService } from 'src/app/authentication/services/maintenance.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

/**
 * The drawer shown when website is in maintenance mode
 */
@Component({
  selector: 'nv-maintenance-mode',
  templateUrl: './maintenance-mode.component.html',
  styleUrls: ['./maintenance-mode.component.scss']
})
export class MaintenanceModeComponent implements OnInit {
  public wording = wording;
  public visible: boolean;
  public estimatedEndTime$: Observable<string>;
  constructor(
    private maintenanceModeService: MaintenanceService,
  ) { }

  public ngOnInit(): void {
    this.maintenanceModeService.inMaintenanceMode
      .pipe(
        tap(inMaintenanceMode => this.visible = inMaintenanceMode)
      ).subscribe();
    this.estimatedEndTime$ = this.maintenanceModeService.getNextMaintenanceDate(true);
  }

  public ok() {
    this.visible = false;
  }
}
