import { AutobankFieldTypeKey } from './IAutobankFieldType';
import { AutobankOperationTypeKey } from './IAutobankOperationType';
import { IResource } from './IResource';

export type FieldOperationTypeKey = number;

interface ISharedProps {
  fieldTypeId: AutobankFieldTypeKey;
  operationTypeId: AutobankOperationTypeKey;
}

export interface IFieldOperationTypeBase extends ISharedProps, IResource<FieldOperationTypeKey> { }

export interface IFieldOperationTypeCreate extends ISharedProps { }

export interface IFieldOperationTypeDTO extends IFieldOperationTypeBase { }

export interface IFieldOperationType extends IFieldOperationTypeDTO { }

