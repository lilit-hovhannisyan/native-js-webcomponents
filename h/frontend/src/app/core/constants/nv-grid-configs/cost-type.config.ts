import { NvColumnDataType, NvFilterControl, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';
import { getContextMenuButtonsConfig } from '../../helpers/general';
import { wordingAccounting } from '../wording/accounting';
import { wordingGeneral } from '../wording/general';
import { Operations } from '../../models/Operations';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'costTypeGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    sortBy: 'no',
    isSortAscending: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 50
    },
    columns: [
      {
        key: 'no',
        title: wordingAccounting.no,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wordingAccounting.ctName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'accountingRelationName',
        title: wordingAccounting.accountingRelation,
        width: 200,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'opexTypeName',
        title: wordingAccounting.opexType,
        width: 200,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isOpex',
        title: wordingAccounting.opexVesselOperation,
        width: 200,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isActive',
        title: wordingAccounting.active,
        width: 75,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wordingGeneral.edit,
        name: 'editCostType',
        tooltip: wordingGeneral.edit,
        actOnDoubleClick: true,
        func: actions.edit,
        hidden: !canPerform(Operations.UPDATE)
      },
      {
        icon: 'delete',
        description: wordingGeneral.delete,
        name: 'deleteCostType',
        tooltip: wordingGeneral.delete,
        actOnDoubleClick: false,
        func: actions.delete,
        hidden: !canPerform(Operations.DELETE)
      }
    ],
    toolbarButtons: [
      {
        title: wordingGeneral.create,
        tooltip: wordingGeneral.create,
        icon: 'plus-circle-o',
        func: actions.add,
        hidden: () => !canPerform(Operations.CREATE)
      }
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};

