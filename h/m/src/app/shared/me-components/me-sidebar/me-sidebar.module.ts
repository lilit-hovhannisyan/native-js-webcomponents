import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IconsModule } from '@shared/modules/icons.module';

import { MeSidebarComponent } from './me-sidebar.component';
import { MeButtonModule } from '../me-button/me-button.module';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';

@NgModule({
  declarations: [MeSidebarComponent],
  imports: [CommonModule, MeButtonModule, IconsModule, MeIconsProviderModule],
  exports: [MeSidebarComponent]
})
export class MeSidebarModule {}
