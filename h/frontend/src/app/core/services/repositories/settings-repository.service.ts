import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpPut } from 'src/app/core/models/http/http-put';
import { ISettings } from 'src/app/core/models/resources/ISettings';
import { IDataResponseBody } from 'src/app/core/models/responses/IDataResponseBody';
import { map } from 'rxjs/operators';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpGet } from 'src/app/core/models/http/http-get';

@Injectable({
  providedIn: 'root'
})
export class SettingsRepositoryService {
  private baseUrl = '/api/settings';
  constructor(private http: HttpClient) { }

  public fetchAll(): Observable<ISettings> {
    const request = new HttpGet(this.baseUrl);
    return this.http.request(request).pipe(
      map((res: HttpResponse<IDataResponseBody<ISettings>>) => res.body.data)
    );
  }

  public update(settings: ISettings): Observable<ISettings> {
    const payload = { system: settings.system, _type: 'Settings', id: 'settings',
      defaultAccountPrepaidExpensesId: settings.defaultAccountPrepaidExpensesId,
      defaultAccountDeferredIncomeId: settings.defaultAccountDeferredIncomeId };
    const request = new HttpPut(this.baseUrl, payload);
    return this.http.request(request)
      .pipe(map((res: HttpResponse<IDataResponseBody<ISettings>>) => res.body.data));
  }
}
