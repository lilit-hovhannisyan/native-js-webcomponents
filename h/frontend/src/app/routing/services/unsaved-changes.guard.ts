import { CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { FormGroup } from '@angular/forms';

export interface DirtyComponent {
  customValidation?: () => Observable<boolean>;
  form?: FormGroup;
}

@Injectable()
export class UnsavedChangesGuard implements CanDeactivate<DirtyComponent> {

  constructor(private confirmationService: ConfirmationService) { }

  public isClean(component: DirtyComponent) {
    /**
     * There is some case when the `component` can be `null`.
     * It appears when this guard is used on some routing,
     * but its component isn't used yet.
     */
    if (!component) { return true; }

    if (component.customValidation) {
      return component.customValidation();
    } else if (component.form) {
      return !component.form.dirty;
    }
    return true;
  }

  public canDeactivate(component: DirtyComponent): boolean | Observable<boolean> {
    return this.isClean(component)
      ? true
      : this.confirmationService.confirm(
        {
          title: wording.general.doYouWantToLeave,
          wording: wording.general.youHaveUnsavedChanges,
          okButtonWording: wording.general.quit,
          cancelButtonWording: wording.general.cancel,
        }
      );
  }
}
