import { CHARTER_DURATION_UNIT } from './../../../../../../core/models/enums/CharterDurationUnit';
import { CHARTER_PERIOD_TYPE } from './../../../../../../core/models/enums/CharterPeriodType';
import { ISelectOption } from './../../../../../../core/models/misc/selectOption';
import { ITimeCharterPeriod, ITimeChartererRate } from './../../../../../../core/models/resources/ITimeCharterPeriod';
import { TimeCharterKey, ITimeCharter } from '../../../../../../core/models/resources/ITimeCharter';
import { ResourceDetailView, NVHttpErrorResponse } from 'src/app/core/abstracts/ResourceDetailView';
import { Component, OnInit, ViewChildren, QueryList, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, ValidatorFn, ValidationErrors, FormControl, AbstractControl } from '@angular/forms';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { NzMessageService } from 'ng-zorro-antd/message';
import { ActivatedRoute } from '@angular/router';
import { Validators } from 'src/app/core/classes/validators';
import { sortBy } from 'lodash';
import { Subscription, Observable, merge, combineLatest } from 'rxjs';
import { TimeCharterPeriodRepositoryService } from 'src/app/core/services/repositories/time-charter-period-repository.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { CHARTER_DURATION_TYPE } from 'src/app/core/models/enums/CharterDurationType';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { CHARTER_RATE_TYPE } from 'src/app/core/models/enums/CharterRateType';
import { startWith, distinctUntilChanged, filter, map } from 'rxjs/operators';
import { ChartererRatesComponent } from './charterer-rates/charterer-rates.component';
import * as moment from 'moment';
import { roundFractionalPart } from 'src/app/core/helpers/general';
import { TimeCharterRepositoryService } from 'src/app/core/services/repositories/timeCharter.repository.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { scrollToFirstInvalidControl } from 'src/app/shared/helpers/form-fields-error-scroll';
import { getDefaultIntervalListItem } from 'src/app/shared/helpers/general';

// 1 day = 86400 seconds
const DAY_IN_SECONDS = 86400;

@Component({
  templateUrl: './time-charter-periods.component.html',
  styleUrls: ['./time-charter-periods.component.scss']
})
export class TimeCharterPeriodsComponent implements OnInit {
  public form: FormGroup;
  public timeCharterId: TimeCharterKey;
  public periods: Array<ITimeCharterPeriod> = [];
  public selectedPeriod?: ITimeCharterPeriod;
  public inEditMode = false;
  public inCreationMode = false;
  public currencies$: Observable<ICurrency[]>;
  public declarationOfOptionShown = true;
  public addendumSectionShown = true;
  public timeCharter: ITimeCharter;

  public wording = wording;
  public node = sitemap.chartering.children.misc.children.timeCharters.children.id.children.periods;
  public zone = SitemapEntry.getByNode(this.node).toZone();
  public CHARTER_RATE_TYPE = CHARTER_RATE_TYPE;

  private formFieldSubscriptions: Subscription[] = [];
  public timeChartererRates: ITimeChartererRate[] = [{ from: '', charterRate: 0 }];

  public periodTypeOptions: ISelectOption[] = [
    { value: CHARTER_PERIOD_TYPE.Period, displayLabel: wording.chartering.period },
    { value: CHARTER_PERIOD_TYPE.Option, displayLabel: wording.chartering.option },
    { value: CHARTER_PERIOD_TYPE.OptedPeriod, displayLabel: wording.chartering.optedPeriod },
    { value: CHARTER_PERIOD_TYPE.NonDeclaredOption, displayLabel: wording.chartering.nonDeclaredOption },
    { value: CHARTER_PERIOD_TYPE.Extension, displayLabel: wording.chartering.extension },
  ];

  public durationTypeOptions: ISelectOption[] = [
    { value: CHARTER_DURATION_TYPE.RedeliveryDate, displayLabel: wording.chartering.redeliveryDate },
    { value: CHARTER_DURATION_TYPE.FixedPeriod, displayLabel: wording.chartering.fixedPeriod },
    { value: CHARTER_DURATION_TYPE.MinMaxPeriod, displayLabel: wording.chartering.minMaxPeriod },
    { value: CHARTER_DURATION_TYPE.MinMaxDates, displayLabel: wording.chartering.minMaxDates },
  ];

  public durationUnitOptions: ISelectOption[] = [
    { value: CHARTER_DURATION_UNIT.Days, displayLabel: wording.chartering.days },
    { value: CHARTER_DURATION_UNIT.Months, displayLabel: wording.chartering.months }
  ];

  public charterRateTypeOptions: ISelectOption[] = [
    { value: CHARTER_RATE_TYPE.Fixed, displayLabel: wording.chartering.fixed },
    { value: CHARTER_RATE_TYPE.Variable, displayLabel: wording.chartering.variable }
  ];

  @ViewChildren(ChartererRatesComponent) public chartererRatesComponents: QueryList<ChartererRatesComponent>;

  public get isDurationTypeRedeliveryDate(): boolean {
    const durationType = this.form && this.form.get('durationType');
    return durationType ? durationType.value === CHARTER_DURATION_TYPE.RedeliveryDate : false;
  }

  private disabledFields = [];
  public formElement: ElementRef;

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    private nzMessageService: NzMessageService,
    private timeCharterPeriodRepo: TimeCharterPeriodRepositoryService,
    private timeCharterRepo: TimeCharterRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    public authService: AuthenticationService,
    private settingsService: SettingsService,
    private el: ElementRef,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    const { id } = this.route.parent.snapshot.params;
    this.timeCharterId = parseInt(id, 10);
    this.currencyRepo.fetchAll({ useCache: true }).subscribe();
    this.timeCharterRepo.fetchOne(this.timeCharterId, {}).subscribe(timeCharter => {
      this.timeCharter = timeCharter;
      const dateRangeValidator = Validators.dateShouldBeAfterBefore(
        this.timeCharter.from,
        this.timeCharter.until,
        this.settingsService.locale
      );

      this.form.get('from').setValidators([Validators.required, dateRangeValidator]);
      this.form.get('begin').setValidators([Validators.required, dateRangeValidator]);
      this.form.get('until').setValidators([
        Validators.required,
        Validators.isAfterFieldDate('from', false, 'seconds'),
        dateRangeValidator
      ]);
    });
    this.init();
    this.fetchPeriods();
  }

  private loadCurrencies() {
    const currencyControl = this.form.get('currencyId');
    this.currencies$ = combineLatest([
      this.currencyRepo.getStream(),
      currencyControl.valueChanges.pipe(startWith(currencyControl.value))
    ]).pipe(
      map(([currencies, currencyId]) =>
        this.currencyRepo.filterActiveOnly(currencies, currencyId))
    );
  }

  private fetchPeriods = (idToSelect?: number) => {
    const queryParams = { timeCharterId: this.timeCharterId };
    this.timeCharterPeriodRepo.fetchAll({ queryParams }).subscribe(periods => {

      this.periods = periods || [];
      this.periods = sortBy(this.periods, ['from']);

      if (this.periods.length) {
        const periodToSelect = idToSelect
          ? this.periods.find(s => s.id === +idToSelect)
          : getDefaultIntervalListItem(this.periods);
        this.selectPeriod(periodToSelect || this.periods[this.periods.length - 1]);
      }
    });
  }

  private init(period: ITimeCharterPeriod = { timeCharterId: this.timeCharterId } as ITimeCharterPeriod) {
    this.form = this.fb.group({
      from: [period.from, [Validators.required]],
      until: [period.until, [
        Validators.required,
        Validators.isAfterFieldDate('from', false, 'seconds'),
      ]],
      periodType: [period.periodType, [Validators.required]],
      durationType: [period.durationType, [Validators.required]],
      begin: [period.begin, [Validators.required]],
      endApprox: [period.endApprox, [
        Validators.required,
        Validators.isAfterFieldDate('begin', false, 'seconds'),
      ]],
      endActual: [period.endActual],
      duration: [period.duration, [Validators.positiveNumber, Validators.max(999999.9999)]],
      durationUnit: [period.durationUnit || CHARTER_DURATION_UNIT.Days, [Validators.required]],
      minPeriod: [period.minPeriod, Validators.max(999999.9999)],
      maxPeriod: [period.maxPeriod, Validators.max(999999.9999)],
      minDate: [period.minDate],
      maxDate: [period.maxDate, [Validators.isAfterFieldDate('minDate')]],
      plusDays: [period.plusDays, [
        Validators.min(0),
        Validators.wholeNumber,
        Validators.isGreaterThanFieldValue('additionalOffHire'),
      ]],
      minusDays: [period.minusDays, [this.minusDaysCustomValidator, Validators.wholeNumber]],
      additionalOffHire: [period.additionalOffHire],
      declarationToBe: [period.declarationToBe, [Validators.required]],
      declared: [period.declared],
      addendumDated: [period.addendumDated, [Validators.required]],
      addendumNo: [period.addendumNo, [Validators.required, Validators.maxLength(3)]],
      currencyId: [period.currencyId, [Validators.required, Validators.wholeNumber]],
      charterRateType: [period.charterRateType, [Validators.required]],
    });
    this.disabledFields = ['from'];
    this.formFieldSubscriptions.forEach(s => s.unsubscribe());
    this.formFieldSubscriptions = [];
    this.timeChartererRates = period.timeChartererRates
      ? [...period.timeChartererRates.map(obj => ({ ...obj }))] // clone array with objects
      : [{ from: '', charterRate: 0 }];
    this.loadCurrencies();
    this.updateFromFieldOnUntilChange();
    this.initDeclarationOfOptionValidation();
    this.initDeclared();
    this.initBeginDate();
    this.onDurationTypeChange();

    this.checkEndApproxValidity();
    this.durationCalculationForRedeliveryDate();
    this.minDateCalculationForRedeliveryDate();
    this.maxDateCalculationForRedeliveryDate();
    this.minPeriodCalculationForRedeliveryDate();
    this.maxPeriodCalculationForRedeliveryDate();

    this.form.disable();
    this.form.markAsPristine();
  }

  // this validator is specificated only for `minusDays` field
  // and not necessary to place this validator into `Validators`
  private minusDaysCustomValidator: ValidatorFn = (
    control: FormControl,
  ): ValidationErrors | null => {
    const endApprox = control.parent && control.parent.get('endApprox');
    const begin = control.parent && control.parent.get('begin');
    const durationDays = endApprox && begin ? this.getDatesDiff(endApprox.value, begin.value) : null;

    if (!(
      endApprox
      && begin
      && this.isNumericValue(durationDays)
      && this.isDurationTypeRedeliveryDate
    )) {
      return Validators.min(0)(control);
    }

    return Validators.integerBetween(0, durationDays, true)(control);
  }

  private onDurationTypeChange(): void {
    const { durationType, minusDays, plusDays, begin, endApprox } = this.form.controls;
    const durationTypeControlSub = durationType.valueChanges.pipe(
      startWith(durationType.value),
      distinctUntilChanged(),
    ).subscribe(() => {
      if (this.isDurationTypeRedeliveryDate) {
        this.disabledFields = [
          'from',
          'duration',
          'minPeriod',
          'maxPeriod',
          'minDate',
          'maxDate',
          'additionalOffHire',
        ];

        this.checkControlValidity(minusDays);
        this.checkControlValidity(plusDays);
        this.checkControlValidity(begin);
        this.checkControlValidity(endApprox);
      } else {
        // TODO change when requirements will be ready for other duration types
        this.disabledFields = ['from'];
      }

      this.form.enable();
      this.disableMarkedFields();
    });

    this.formFieldSubscriptions.push(durationTypeControlSub);
  }

  private checkControlValidity(control: AbstractControl): void {
    // to avoid showing the `required` validation error message
    // in untouched and unfilled fields
    if (!control.value) {
      return;
    }

    control.updateValueAndValidity();
    control.markAllAsTouched();
  }

  private checkEndApproxValidity(): void {
    const { begin, endApprox } = this.form.controls;

    this.formFieldSubscriptions.push(
      begin.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
        endApprox.updateValueAndValidity();
      })
    );
  }

  /**
   * I guess that in the future will add other calculations for other durationTypes
   * so I have named all this calculation functions with suffix `ForRedeliveryDate`
   */
  private durationCalculationForRedeliveryDate(): void {
    const { begin, endApprox, durationUnit, duration, durationType } = this.form.controls;

    const sub = this.mergeValueChanges([begin, endApprox, durationUnit, durationType])
      .pipe(filter(() => this.isDurationTypeRedeliveryDate))
      .subscribe(() => {
        const diff = this.getDatesDiff(
          endApprox.value,
          begin.value,
          durationUnit.value === CHARTER_DURATION_UNIT.Days,
        );
        duration.setValue(diff > 0 ? diff : null);
      });

    this.formFieldSubscriptions.push(sub);
  }

  private minDateCalculationForRedeliveryDate(): void {
    const { minusDays, duration, durationType, minDate, endApprox } = this.form.controls;

    const sub = this.mergeValueChanges([minusDays, duration, durationType])
      .pipe(filter(() => this.isDurationTypeRedeliveryDate))
      .subscribe(() => {
        const minusDaysNumber = this.isNumericValue(minusDays.value && +minusDays.value)
          ? +minusDays.value
          : null;
        const value: string = moment(endApprox.value)
          .subtract(minusDaysNumber * DAY_IN_SECONDS, 'seconds')
          .toISOString();

        // when formControl disabled, it's `valid` property is false
        const isValid = !(endApprox.invalid || minusDays.invalid);
        const valuesExist = !!(
          endApprox.value
          && this.isNumericValue(minusDaysNumber)
          && value
        );
        minDate.setValue(isValid && valuesExist ? value : null);
      });
    this.formFieldSubscriptions.push(sub);
  }

  private maxDateCalculationForRedeliveryDate(): void {
    const { endApprox, plusDays, duration, durationType, maxDate } = this.form.controls;

    const sub = this.mergeValueChanges([plusDays, duration, durationType])
      .pipe(filter(() => this.isDurationTypeRedeliveryDate))
      .subscribe(() => {
        const plusDaysNumber = this.isNumericValue(plusDays.value && +plusDays.value)
          ? +plusDays.value
          : null;

        const value: string = moment(endApprox.value)
          .add(plusDaysNumber * DAY_IN_SECONDS, 'seconds')
          .toISOString();

        // when formControl disabled, it's `valid` property is false
        const isValid: boolean = !(endApprox.invalid || plusDays.invalid);
        const valuesExist = !!(
          endApprox.value
          && this.isNumericValue(plusDaysNumber)
          && value
        );
        maxDate.setValue(isValid && valuesExist ? value : null);
      });
    this.formFieldSubscriptions.push(sub);
  }

  private minPeriodCalculationForRedeliveryDate(): void {
    const { minusDays, duration, begin, minDate, minPeriod, durationUnit } = this.form.controls;

    const sub = this.mergeValueChanges([duration, minusDays])
      .pipe(filter(() => this.isDurationTypeRedeliveryDate))
      .subscribe(() => {
        const diff = this.getDatesDiff(
          minDate.value,
          begin.value,
          durationUnit.value === CHARTER_DURATION_UNIT.Days,
        );
        minPeriod.setValue(this.isNumericValue(diff) && diff >= 0 ? diff : null);
      });

    this.formFieldSubscriptions.push(sub);
  }

  private maxPeriodCalculationForRedeliveryDate(): void {
    const { plusDays, duration, begin, maxDate, maxPeriod, durationUnit, } = this.form.controls;

    const sub = this.mergeValueChanges([duration, plusDays])
      .pipe(filter(() => this.isDurationTypeRedeliveryDate))
      .subscribe(() => {

        const diff = this.getDatesDiff(
          maxDate.value,
          begin.value,
          durationUnit.value === CHARTER_DURATION_UNIT.Days,
        );
        maxPeriod.setValue(this.isNumericValue(diff) && diff >= 0 ? diff : null);
      });

    this.formFieldSubscriptions.push(sub);
  }

  /**
   * From should be predefined by Begin date.
   * Whenever we change Begin, From needs to be synced to that.
   */
  private initBeginDate() {
    this.form.get('begin').valueChanges
      .pipe(startWith(this.form.get('begin').value))
      .subscribe(v => {
        this.form.get('from').patchValue(v);
      });
  }

  private initDeclarationOfOptionValidation() {
    this.formFieldSubscriptions.push(this.form.get('periodType').valueChanges
      .pipe(startWith(this.form.get('periodType').value))
      .subscribe(periodTypeValue => {
        const declaredControl = this.form.get('declared');
        if (periodTypeValue === CHARTER_PERIOD_TYPE.Period
          || periodTypeValue === CHARTER_PERIOD_TYPE.Extension) {
          /**
           * For `Period` and Extension types we hide declaration options section.
           * If user changes the Period Type so that the section becomes invisible,
           * the fields should be cleared. 👇
           */
          this.disableDeclarationFields();
        } else if (periodTypeValue === CHARTER_PERIOD_TYPE.NonDeclaredOption) {
          /**
           * If user selects `Non-Declared Option` ☝️ as the period type,
           * `Declared` should be cleared and the field `Declared` should be disabled.
           */
          this.enableDeclarationFields();
          declaredControl.patchValue(null);
          // needs to be disabled in next event-cycle. otherwise it stays enabled
          setTimeout(() => declaredControl.disable());
        } else if (periodTypeValue === CHARTER_PERIOD_TYPE.OptedPeriod) {
          /**
           * If the user selects `Opted Period` manually,
           * `Declared` becomes a required field.
           */
          this.enableDeclarationFields();
          declaredControl.setValidators([Validators.required]);
          declaredControl.updateValueAndValidity();
        } else {
          declaredControl.patchValue(null);
          declaredControl.markAsPristine();
          this.enableDeclarationFields();
        }

        if (periodTypeValue === CHARTER_PERIOD_TYPE.Extension) {
          /**
           * NAVIDO-
           * only show addendum section when period type ☝️ is Extension
           */
          this.enableAddendumFields();
        } else {
          this.clearAddendumFields();
        }
      }));
  }

  private initDeclared() {
    const declaredControl = this.form.get('declared');
    this.formFieldSubscriptions.push(declaredControl.valueChanges.subscribe(declaredValue => {
      const periodTypeControl = this.form.get('periodType');
      /**
       * Entering a value for `Declared` should
       * change the period type from `Option` to `Opted Period`.
       */
      if (declaredValue
        && periodTypeControl.value === CHARTER_PERIOD_TYPE.Option
      ) {
        periodTypeControl.patchValue(CHARTER_PERIOD_TYPE.OptedPeriod);
      }
      /**
       * Removing a value for `Declared` should
       * change period type from `Opted Period` to `Option`
       */
      if (!declaredValue
        && periodTypeControl.value === CHARTER_PERIOD_TYPE.OptedPeriod
        && !declaredControl.pristine
      ) {
        periodTypeControl.patchValue(CHARTER_PERIOD_TYPE.Option);
      }
    }));
  }

  private enableDeclarationFields() {
    const declaredControl = this.form.get('declared');
    const declarationToBe = this.form.get('declarationToBe');
    this.declarationOfOptionShown = true;
    if (this.form.enabled) {
      if (declaredControl.disabled) {
        declaredControl.enable();
      }
      declaredControl.clearValidators();
      declaredControl.updateValueAndValidity();
      declarationToBe.markAsPristine();
      declarationToBe.enable();
      declarationToBe.setValidators([Validators.required]);
    }
    declaredControl.markAsPristine();
  }

  private disableDeclarationFields() {
    const declarationToBeDate = this.form.get('declarationToBe');
    const declaredDate = this.form.get('declared');
    this.declarationOfOptionShown = false;
    if (declaredDate.enabled || declarationToBeDate.enabled) {
      declarationToBeDate.patchValue(null);
      declaredDate.patchValue(null);
      declarationToBeDate.disable();
      declarationToBeDate.clearValidators();
      declaredDate.disable();
    }
  }

  private clearAddendumFields() {
    const addendumDated = this.form.get('addendumDated');
    const addendumNo = this.form.get('addendumNo');
    if (addendumDated.enabled || addendumNo.enabled) {
      addendumDated.patchValue(null);
      addendumNo.patchValue(null);
      addendumDated.disable();
      addendumDated.clearValidators();
      addendumNo.disable();
      addendumNo.clearValidators();
    }
    this.addendumSectionShown = false;
  }

  private enableAddendumFields() {
    const addendumDated = this.form.get('addendumDated');
    const addendumNo = this.form.get('addendumNo');
    if (this.form.enabled && (addendumDated.disabled || addendumNo.disabled)) {
      addendumDated.enable();
      addendumDated.setValidators([Validators.required]);
      addendumNo.enable();
      addendumNo.setValidators([Validators.required, Validators.maxLength(3)]);
      addendumDated.markAsPristine();
      addendumNo.markAsPristine();
    }
    this.addendumSectionShown = true;
  }

  public updateFromFieldOnUntilChange() {
    this.formFieldSubscriptions.push(
      this.form.get('from').valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
        this.form.get('until').updateValueAndValidity();
      })
    );

    this.formFieldSubscriptions.push(
      this.form.get('additionalOffHire').valueChanges
        .pipe(distinctUntilChanged())
        .subscribe(() => {
          this.form.get('plusDays').updateValueAndValidity();
        })
    );
  }

  public intervalClicked = (periodId: number) => {
    const period = this.periods.find(s => s.id === periodId);
    this.selectPeriod(period);
  }

  private selectPeriod = (period: ITimeCharterPeriod) => {
    this.selectedPeriod = period;
    this.init(period);
  }

  public enterEditMode = () => {
    this.inEditMode = true;
    // form needs to be enabled in next event-cycle. otherwise it stays disabled
    setTimeout(() => {
      this.form.enable();
      this.disableMarkedFields();
    });
  }

  public cancelEditing = () => {
    this.inEditMode = false;
    this.inCreationMode = false;
    this.init(this.selectedPeriod);
    // form needs to be disabled in next event-cycle. otherwise it stays enabled
    setTimeout(() => this.form.disable());
  }

  /**
   * For clear delivery fields on creating new time-charter-period.
   * Used in the `addIntervalClicked` function.
   */
  private getEmptyDeliveryFieldsObject(): Object {
    return {
      endApprox: undefined,
      endActual: undefined,
      duration: undefined,
      minPeriod: undefined,
      maxPeriod: undefined,
      minDate: undefined,
      maxDate: undefined,
      plusDays: undefined,
      minusDays: undefined,
      additionalOffHire: undefined,
    };
  }

  public addIntervalClicked = () => {
    this.inCreationMode = true;
    this.selectedPeriod = {} as ITimeCharterPeriod;
    const latestEntry = this.periods[this.periods.length - 1];
    this.currencyRepo.fetchAll({ useCache: true }).subscribe((currencies) => {
      const isCurrentCurrencyActive = latestEntry && latestEntry.currencyId
        ? currencies
          .find(c => c.id === latestEntry.currencyId).isActive
        : false;

      const clonedPeriod = latestEntry
        ? {
          ...latestEntry,
          ...this.getEmptyDeliveryFieldsObject(),
          until: undefined,
          durationType: undefined,
          begin: latestEntry.until,
          currencyId: isCurrentCurrencyActive ? latestEntry.currencyId : null,
        }
        : {} as ITimeCharterPeriod;
      delete clonedPeriod.id; // we don't need the id in creation mode
      this.init(clonedPeriod); // cloning the most recent one
      this.enterEditMode();
    });
  }

  public saveClicked = () => {
    const isChartererRateListValid = this.chartererRatesComponents.toArray()
      .every(component => {
        component.validateList();
        return component.isListValid();
      });

    if (!isChartererRateListValid) {
      setTimeout(() => {
        scrollToFirstInvalidControl(this.el.nativeElement);
      });
    }
    if (this.form.invalid || !isChartererRateListValid) { return; }
    const timeChartererRates = this.form.get('charterRateType').value === CHARTER_RATE_TYPE.Fixed
      ? [this.timeChartererRates[0]]
      : this.timeChartererRates;
    const rawValue = this.form.getRawValue();

    const freshResource = this.selectedPeriod.id
      ? {
        id: this.selectedPeriod.id,
        timeCharterId: this.selectedPeriod.timeCharterId,
        ...this.form.value,
        timeChartererRates,
        from: rawValue.from,
      }
      : {
        ...this.form.value,
        timeCharterId: this.timeCharterId,
        timeChartererRates,
        from: rawValue.from,
      };

    this.selectedPeriod.id
      ? this.timeCharterPeriodRepo.update(freshResource, {}).subscribe(period => {
        this.cancelEditing();
        this.fetchPeriods(period.id);
      }, this.handleSubmitError)

      : this.timeCharterPeriodRepo.create(freshResource, {}).subscribe(period => {
        this.cancelEditing();
        this.fetchPeriods(period.id);
      }, this.handleSubmitError);

  }

  public handleSubmitError = (error: NVHttpErrorResponse) => {
    ResourceDetailView.handleSubmitError(error, this.form, this.nzMessageService);

    if (error.error.errors.find(e => e.field === 'from')) {
      scrollToFirstInvalidControl(this.el.nativeElement, 'form', 'nv-input-wrapper');
    }
  }

  public onChartererRateChange(chartererRates: ITimeChartererRate[]) {
    this.timeChartererRates = [...chartererRates];
  }

  private mergeValueChanges(controls: AbstractControl[]): Observable<any> {
    return merge(...controls.map(c => c.valueChanges.pipe(distinctUntilChanged())));
  }

  private getDatesDiff(firstDate: string, secondDate: string, inDays = true): number {
    const diffDays = moment(firstDate).diff(secondDate, 'seconds') / DAY_IN_SECONDS;
    return roundFractionalPart(inDays ? diffDays : this.daysToMonths(diffDays), 4);
  }

  private daysToMonths(days: number): number {
    // TODO change when months calculation logic will be determined
    return days / 30;
  }

  private isNumericValue(value: any): boolean {
    return (typeof value === 'number') && !isNaN(value);
  }

  private disableMarkedFields() {
    this.disabledFields.forEach(f => this.form.get(f).disable());
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
