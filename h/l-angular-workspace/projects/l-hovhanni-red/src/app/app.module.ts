import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

import { appRoutes } from './routes';
import {
    PortfolioComponent,
    AboutComponent,
    ContactComponent,
    VideosComponent,
    StoryTellingComponent,
    NotFoundComponent
} from './pages';


import {LavashAtomsModule, LavashMoleculesModule, LavashOrganismsModule} from 'l-builder';
import {WidgetsModule} from './widgets/widgets.module';
import {ProjectsService} from './services/projects.service';

@NgModule({
    declarations: [
        AppComponent,
        PortfolioComponent,
        AboutComponent,
        ContactComponent,
        VideosComponent,
        StoryTellingComponent,
        NotFoundComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes),
        WidgetsModule,
        LavashAtomsModule,
        LavashMoleculesModule,
        LavashOrganismsModule
    ],
    providers: [ProjectsService],
    bootstrap: [AppComponent]
})
export class AppModule { }
