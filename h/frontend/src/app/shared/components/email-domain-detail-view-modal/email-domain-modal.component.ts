import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { validateAllFormFields } from '../../helpers/validateAllFormFields';
import { IEmailDomain } from 'src/app/core/models/resources/ISettings';
import { Validators } from 'src/app/core/classes/validators';


@Component({
  selector: 'nv-email-domain-modal',
  templateUrl: './email-domain-modal.component.html',
  styleUrls: ['./email-domain-modal.component.scss'],
})
export class EmailDomainModalComponent implements OnInit {
  @Input() public allDomains: IEmailDomain[] = [];
  @Input() public emailDomain: IEmailDomain;
  @Input() public isSuperAdmin = false;

  public wording = wording;
  public form: FormGroup;
  public isCreateMode = false;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
  ) { }

  public ngOnInit(): void {
    const { isSystemDefault, domain } = this.emailDomain || {} as IEmailDomain;

    const domainExistenceChecker = (value: string, item: IEmailDomain) => {
      if (this.isCreateMode) {
        return value === item.domain;
      }

      /**
       * In editMode when `item` equals to `this.emailDomain`
       * means that they are exactly the same object.
       */
      return item !== this.emailDomain && value === item.domain;
    };

    this.form = this.fb.group({
      domain: [
        domain, [
          Validators.required,
          Validators.minLength(3),
          Validators.unique(this.allDomains, domainExistenceChecker),
        ]
      ],
      isSystemDefault: [{ value: !!isSystemDefault, disabled: !this.isSuperAdmin }],
    });

    this.isCreateMode = !domain;
  }

  public cancel(): void {
    this.modalRef.close();
  }

  public submit(): void {
    validateAllFormFields(this.form);
    if (!this.form.valid) {
      return;
    }

    this.modalRef.close({
      ...this.emailDomain,
      ...this.form.getRawValue(),
    });
  }
}
