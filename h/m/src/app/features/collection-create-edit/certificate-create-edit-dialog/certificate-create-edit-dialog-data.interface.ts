import { CertificateAssociationModel } from '@shared/models/certificate-association/certificate-association.model';

export interface CertificateCreateEditDialogDataI {
  certificateModel: CertificateAssociationModel | undefined;
}
