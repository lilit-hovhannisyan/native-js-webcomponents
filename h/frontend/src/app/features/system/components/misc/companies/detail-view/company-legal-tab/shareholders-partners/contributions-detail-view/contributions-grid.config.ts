import { NvColumnDataType, NvGridConfig, NvGridButtonsPosition, NvColumnConfig, NvFilterControl, NvButton } from 'nv-grid';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { wording } from 'src/app/core/constants/wording/wording';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { ICompanyPartnerContribution, typePartnerOptions, AmountChangeType, PartnerType } from 'src/app/core/models/resources/ICompanyPartnerContribution';
import { formatNumber, formatPercent } from '@angular/common';
import { nvLocalMapper } from 'src/app/shared/pipes/localDecimal';
import { COMPANY_LEGAL_FORM, CAPITAL_TYPE, contributionGridCompanyName, contributionGridContributionAmount, contributionGridSumContribution, getContributionWordingKey } from 'src/app/core/models/enums/company-legal-form';
import { IWording } from 'src/app/core/models/resources/IWording';
import { ICompany, CompanyKey } from 'src/app/core/models/resources/ICompany';
import { ICompanyLegalEntity } from 'src/app/core/models/resources/ICompanyLegalEntity';
import { SettingsService } from 'src/app/core/services/settings.service';

export const getContributionsGridConfig = (
  settingsService: SettingsService,
  company: ICompany,
  currency: ICurrency,
  currencyEuro: ICurrency, // needed only for legalForm=KG
  legalEntity: ICompanyLegalEntity,
  latestContributionsMap: Map<CompanyKey, ICompanyPartnerContribution>,
  totalContributionAmount: number,
  canPerform?: (operation: Operations) => boolean,
  actions?: IGridConfigActions,
): NvGridConfig => {
  const isHistoryGrid: boolean = !!actions;
  const localeCode: string = nvLocalMapper[settingsService.locale];
  const digitsInfo = '.2-2';

  const legalFormCapitalTypeKey = getContributionWordingKey(company.legalForm, legalEntity.capitalType);
  const companyNameWording: IWording = contributionGridCompanyName.get(company.legalForm);
  const contributionAmountWording: IWording = contributionGridContributionAmount.get(
    legalFormCapitalTypeKey
  );
  const sumContributionsWording: IWording = contributionGridSumContribution.get(
    legalFormCapitalTypeKey
  );

  const colId: NvColumnConfig = {
    key: 'id',
    visible: false,
  };

  const colCompanyName: NvColumnConfig = {
    key: 'companyName',
    title: companyNameWording,
    width: 200,
    dataType: NvColumnDataType.String,
    footer: isHistoryGrid ? null : { label: sumContributionsWording },
    filter: {
      values: []
    }
  };

  /**
   * Both Grids
   * Only for legalForm=AG
   */
  const colTypeOfStock: NvColumnConfig = {
    key: 'typeOfStock',
    title: wording.system.typeOfStock,
    width: 200,
    dataType: NvColumnDataType.String,
    customFormatFn: () => {
      const valueWording: IWording = legalEntity.capitalType === CAPITAL_TYPE.ParValueShare
        ? wording.system.parValueShare
        : wording.system.nonParValueShare;

      return valueWording[settingsService.language];
    },
    filter: {
      values: []
    }
  };


  // Only for History Grid
  const colFrom: NvColumnConfig = {
    key: 'from',
    title: wording.general.date,
    width: 100,
    dataType: NvColumnDataType.Date,
    filter: {
      values: []
    }
  };
  const colContributionAmount: NvColumnConfig = {
    key: 'contributionAmount',
    title: contributionAmountWording,
    width: 150,
    dataType: NvColumnDataType.Decimal,
    customFormatFn: (row: ICompanyPartnerContribution) => {
      if (!row) {
        return;
      }
      const { contributionAmount } = row;
      const absValue: number = Math.abs(contributionAmount);
      const formattedValue: string = formatNumber(absValue, localeCode, digitsInfo);
      const sign = row.amountChangeType === AmountChangeType.Increase ? '+' : '-';

      return `${sign}${formattedValue}`;
    },
    footer: isHistoryGrid
      ? {}
      : { label: formatNumber(totalContributionAmount, localeCode, digitsInfo) },
    filter: {
      values: []
    }
  };

  // both Grids
  const colCurrency: NvColumnConfig = {
    key: 'currency',
    title: wording.system.currency,
    width: 100,
    dataType: NvColumnDataType.Currency,
    customFormatFn: (row: ICompanyPartnerContribution) => row && currency.isoCode,
    footer: isHistoryGrid ? {} : { label: currency.isoCode },
    filter: {
      values: []
    }
  };

  // Only Result grid
  const colPercentage: NvColumnConfig = {
    key: 'percentage',
    title: wording.system.percentage,
    width: 100,
    customFormatFn: (row: ICompanyPartnerContribution) => {
      // `row` can be `undefined` when footer exists
      if (!row) {
        return;
      }

      const percentValue = row.contributionAmount && totalContributionAmount
        ? row.contributionAmount / totalContributionAmount
        : 0;

      return formatPercent(percentValue, localeCode, digitsInfo);
    },
    footer: {
      label: formatPercent(1, localeCode, digitsInfo),
    },
    filter: {
      values: []
    }
  };

  /**
   * Only for legalForm=KG
   * Both Grids
   */
  const colLiabilitySum: NvColumnConfig = {
    key: 'liabilitySum',
    title: wording.system.liabilitySum,
    width: 150,
    dataType: NvColumnDataType.Decimal,
    filter: {
      values: []
    },
  };

  /**
   * Only for legalForm=KG
   * Both grids
   * Always EURO
   */
  const colLiabilityCurrency: NvColumnConfig = {
    key: 'liabilityCurrency',
    title: wording.system.currency,
    width: 100,
    dataType: NvColumnDataType.Currency,
    customFormatFn: (row: ICompanyPartnerContribution) => row && currencyEuro.isoCode,
    filter: {
      values: []
    }
  };

  /**
   * Only for legalForm = KG
   * Both Grids
   */
  const colTypePartner: NvColumnConfig = {
    key: 'partnerType',
    title: wording.system.typePartner,
    width: 150,
    dataType: NvColumnDataType.String,
    customFormatFn: (row: ICompanyPartnerContribution) => {
      return typePartnerOptions.get(row.partnerType)?.[settingsService.language];
    },
    filter: {
      values: []
    }
  };

  /**
   * Only for legalForm = GbR
   * Both Grids
   */
  const colIsManagingPartner: NvColumnConfig = {
    key: 'partnerType',
    title: wording.system.managingPartner,
    width: 150,
    dataType: NvColumnDataType.Boolean,
    customFormatFn: (row: ICompanyPartnerContribution) => {
      return row.partnerType === PartnerType.General;
    },
    filter: {
      controlType: NvFilterControl.Boolean,
      values: []
    }
  };

  // Both Grids
  const colRemark: NvColumnConfig = {
    key: 'remark',
    title: wording.system.remark,
    width: 300,
    filter: {
      values: []
    },
  };

  const colFromArr: NvColumnConfig[] = isHistoryGrid ? [colFrom] : [];
  const colPercentageArr: NvColumnConfig[] = isHistoryGrid ? [] : [colPercentage];
  const colTypeOfStockArr: NvColumnConfig[] = company.legalForm === COMPANY_LEGAL_FORM.StockCorporation
    ? [colTypeOfStock] : [];
  const colCurrencyArr: NvColumnConfig[] = company.legalForm === COMPANY_LEGAL_FORM.StockCorporation
    && legalEntity.capitalType === CAPITAL_TYPE.NonParValueShare
    ? [] : [colCurrency];
  const colLiabilityFieldsArr: NvColumnConfig[] = company.legalForm === COMPANY_LEGAL_FORM.LimitedPartnership
    ? [colLiabilitySum, colLiabilityCurrency, colTypePartner] : [];
  const colGbRFieldsArr: NvColumnConfig[] = company.legalForm === COMPANY_LEGAL_FORM.CivilLawPartnership
    ? [colIsManagingPartner] : [];

  const columns: NvColumnConfig[] = [
    colId,
    colCompanyName,
    ...colGbRFieldsArr,
    ...colFromArr,
    ...colTypeOfStockArr,
    colContributionAmount,
    ...colCurrencyArr,
    ...colPercentageArr,
    ...colLiabilityFieldsArr,
    colRemark,
  ];

  const deleteActionDisabler = (row: ICompanyPartnerContribution) => {
    const latestContributionOfCompany = latestContributionsMap.get(row.partnerId);
    return latestContributionOfCompany && latestContributionOfCompany.id !== row.id;
  };

  const editActionDisabler = (row: ICompanyPartnerContribution) => {
    const latestContributionOfCompany = latestContributionsMap.get(row.partnerId);

    return (
      row.contributionIsZero ||
      (
        latestContributionOfCompany
        && latestContributionOfCompany.id !== row.id
      )
    );
  };

  const editButton: NvButton = {
    icon: 'edit',
    description: wording.general.edit,
    name: 'edit',
    tooltip: wording.general.edit,
    hidden: !canPerform(Operations.UPDATE),
    disabled: editActionDisabler,
    func: (contribution: ICompanyPartnerContribution) => actions.edit(contribution),
  };
  const deleteButton: NvButton = {
    icon: 'delete',
    description: wording.general.delete,
    name: 'delete',
    tooltip: wording.general.delete,
    hidden: !canPerform(Operations.DELETE),
    disabled: deleteActionDisabler,
    func: (contribution: ICompanyPartnerContribution) => actions.delete(contribution),
  };

  return {
    gridName: `contributions${isHistoryGrid ? 'Historical' : 'Result'}GridConfig`,
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    sortBy: `${isHistoryGrid ? 'from' : 'partnerType'}`,
    isSortAscending: isHistoryGrid ? false : true,
    showPaging: false,
    hideToolbar: isHistoryGrid,
    showFooter: !isHistoryGrid,
    columns,
    buttons: isHistoryGrid ? [editButton, deleteButton] : [],
  };
};
