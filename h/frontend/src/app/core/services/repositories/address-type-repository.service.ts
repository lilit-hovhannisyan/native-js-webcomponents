import { AddressTypeKey } from './../../models/resources/IAddressType';
import { Injectable } from '@angular/core';
import { IAddressType, IAddressTypeDTO, IAddressTypeCreate } from '../../models/resources/IAddressType';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { getLabelKeyByLanguage } from '../../helpers/general';
import { SettingsService } from '../settings.service';

@Injectable({
  providedIn: 'root'
})
export class AddressTypeRepositoryService extends AbstractRepository<
IAddressType,
IAddressTypeDTO,
IAddressTypeCreate,
RepoParams<IAddressType>,
AddressTypeKey
> {
  protected resourceType = 'AddressType';
  public sortAsce = true;
  public sortKey = getLabelKeyByLanguage(this.settingsService.language);

  constructor(repositoryService: RepositoryService,
    private settingsService: SettingsService) {
    super(repositoryService);
  }

  public getBaseUrl(params: RepoParams<IAddressType>): string {
    return '/api/address-types';
  }

  public postProcess(item: IAddressTypeDTO): IAddressType { return item; }

}
