import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IMandatorRemark, IMandatorRemarkCreate, MandatorRemarkKey, IMandatorRemarkDTO } from '../../models/resources/IMandatorRemark';
import { RepositoryService } from '../repository.service';
import { MandatorKey } from '../../models/resources/IMandator';
import { Observable } from 'rxjs';
import { HttpGet } from '../../models/http/http-get';
import { map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';

export interface IMandatorRemarkRepoParams extends RepoParams<IMandatorRemark> {
  mandatorId: MandatorKey;
}

@Injectable({
  providedIn: 'root'
})
export class RemarksRepositoryService extends AbstractRepository<
  IMandatorRemark,
  IMandatorRemarkDTO,
  IMandatorRemarkCreate,
  IMandatorRemarkRepoParams,
  MandatorRemarkKey
> {
  protected resourceType: string;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = (): string => '/api/mandator-remarks';

  public postProcess = (data: IMandatorRemarkDTO): IMandatorRemark => data;

  public getRemarkByMandator(options: IMandatorRemarkRepoParams): Observable<IMandatorRemark> {
    const url = `/api/mandators/${options.mandatorId}/remark`;
    const request = new HttpGet(url);
    request.setErrorHandler(options.errorHandler);
    return this.repositoryService.httpClient
      .request<HttpResponse<IDataResponseBody<IMandatorRemark>>>(request)
      .pipe(map((res: any) => res.body && res.body.data));
  }
}
