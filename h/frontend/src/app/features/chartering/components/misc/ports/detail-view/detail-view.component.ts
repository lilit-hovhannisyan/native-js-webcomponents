import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { sitemap } from '../../../../../../core/constants/sitemap/sitemap';
import { url } from '../../../../../../core/constants/sitemap/sitemap-entry';
import { BaseComponentService } from '../../../../../../core/services/base-component.service';
import { PortRepositoryService } from '../../../../../../core/services/repositories/port-repository.service';
import { ResourceDetailView } from '../../../../../../core/abstracts/ResourceDetailView';
import { IPortCreate, IPort } from '../../../../../../core/models/resources/IPort';
import { Validators } from 'src/app/core/classes/validators';

@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PortDetailViewComponent
  extends ResourceDetailView<IPort, IPortCreate>
  implements OnInit {

  public repoParams = {};
  public routeBackUrl = url(sitemap.chartering.children.misc.children.ports);
  public form: FormGroup;

  protected resourceDefault = { isActive: true } as IPort;

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    portRepo: PortRepositoryService,
    baseComponentService: BaseComponentService,
    private cdr: ChangeDetectorRef,
  ) {
    super(portRepo, baseComponentService);
  }

  public ngOnInit() {
    this.init();

    const { id } = this.route.snapshot.params;
    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => `${this.resource.name}`;

  public init(port: IPort = this.resourceDefault) {
    this.initForm(this.fb.group({
      shortName: [port.shortName, Validators.required],
      name: [port.name, Validators.required],
      unLoCode: [port.unLoCode, [Validators.required, Validators.exactLength(3)]],
      cordsLat: [port.cordsLat, [Validators.required, Validators.latitude]],
      cordsLong: [port.cordsLong, [Validators.required, Validators.longitude]],
      countryId: [port.countryId, Validators.required],
      isActive: [port.isActive]
    }));
    this.cdr.detectChanges();
  }

  public submit() {
    if (!this.form.valid) { return; }

    const { value } = this.form;
    const port = this.creationMode
      ? { ...value }
      : { ...this.resource, ...value };

    this.creationMode
      ? this.createResource(port)
      : this.updateResource(port);
  }
}
