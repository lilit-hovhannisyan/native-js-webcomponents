import { Component, ChangeDetectionStrategy, TemplateRef, ChangeDetectorRef, Inject, EventEmitter, Output } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { MePopupAddListI } from './me-popup-add-list.interface';
import { MeButtonI } from '../me-button/me-button.interface';

@Component({
  selector: 'me-popup-add-list',
  templateUrl: './me-popup-add-list.component.html',
  styleUrls: ['./me-popup-add-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MePopupAddListComponent {
  @Output() popupAddListEvent: EventEmitter<{ eventName: string }> = new EventEmitter();

  finishButton: MeButtonI = { color: 'primary', type: 'regular' };

  private _dataModel!: MePopupAddListI;
  content!: TemplateRef<Component>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { dataModel: MePopupAddListI; content: TemplateRef<Component> },
    private cdr: ChangeDetectorRef
  ) {
    this._dataModel = data.dataModel;
    this.content = data.content;
  }

  set dataModel(data: MePopupAddListI) {
    this._dataModel = data;
    this.cdr.detectChanges();
  }

  get dataModel(): MePopupAddListI {
    return this._dataModel;
  }

  customEvent(eventName: string): void {
    this.popupAddListEvent.emit({ eventName });
  }
}
