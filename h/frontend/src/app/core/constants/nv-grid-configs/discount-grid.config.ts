import { NvGridConfig, NvGridRowSelectionType, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wordingAccounting } from 'src/app/core/constants/wording/accounting';

export const gridConfig: NvGridConfig = {
  gridName: 'discountsSelectModalGrid',
  rowSelectionType: NvGridRowSelectionType.RadioButton,
  rowButtonsPosition: null,
  hideRefreshButton: true,
  hideSettingsButton: true,
  clearFiltersButton: true,
  paging: {
    pageNumber: 1,
    pageSize: 10,
    showTotalItems: true
  },
  columns: [{
    key: 'step',
    title: wordingAccounting.step,
    width: 100,
    isSortable: false,
  },
  {
    key: 'percentage',
    title: wordingAccounting.percentage,
    dataType: NvColumnDataType.Decimal,
    width: 120,
    isSortable: true,
    filter: {
      values: []
    }
  }, {
    key: 'days',
    title: wordingAccounting.days,
    dataType: NvColumnDataType.Number,
    width: 60,
    isSortable: true,
    filter: {
      values: []
    }
  },
  {
    key: 'paymentDate',
    title: wordingAccounting.paymentDate,
    dataType: NvColumnDataType.Date,
    width: 250,
    isSortable: true,
    filter: {
      values: []
    }
  },
  ]
};
