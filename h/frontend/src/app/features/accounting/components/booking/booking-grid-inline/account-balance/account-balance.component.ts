import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { NvLocale } from 'src/app/core/models/dateFormat';
import { SettingsService } from 'src/app/core/services/settings.service';
import { nvLocalMapper } from 'src/app/shared/pipes/localDecimal';
import { formatNumber } from '@angular/common';
import { AccountBalanceMainService } from 'src/app/core/services/mainServices/accountbalance-main.service';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import { Separator } from 'src/app/core/models/resources/SeparatorType';

@Component({
  selector: 'nv-account-balance',
  templateUrl: './account-balance.component.html',
  styleUrls: ['./account-balance.component.scss']
})
export class AccountBalanceComponent implements OnInit, OnChanges, OnDestroy {
  @Input() public current = 0;
  @Input() public currentFormat = '';
  @Input() public differ = 0;
  @Input() public init = 0;
  @Input() public newFormat = '';
  @Input() public key = '';
  public form: FormGroup;
  public wording = wording;
  public decimalSeparator: Separator;
  public thousandsSeparator: Separator;
  public decimalPlaces = 2;
  private digitsInfo: string;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(private formBuilder: FormBuilder,
    private settingsService: SettingsService,
    private accountBalanceMainService: AccountBalanceMainService) {
  }

  public initForm() {
    this.form = this.formBuilder.group({
      current: [
        {
          value: formatNumber(
                  Math.abs(this.current),
                  nvLocalMapper[this.settingsService.locale],
                  this.digitsInfo),
          disabled: true
        }
      ],
      new: [
        {
          value: formatNumber(
                  Math.abs(this.init + this.differ),
                  nvLocalMapper[this.settingsService.locale],
                  this.digitsInfo),
          disabled: true
        }
      ]
    });
  }

  public ngOnInit() {
    this.decimalSeparator = this.settingsService.locale === NvLocale.DE ? ',' : '.';
    this.thousandsSeparator = this.settingsService.locale === NvLocale.DE ? '.' : ',';
    this.digitsInfo = `1.${this.decimalPlaces}-100`;
    this.initForm();
    this.accountBalanceMainService.getUpdatedSubject()
      .pipe(
        takeUntil(this.componentDestroyed$)
      ).subscribe((updatedValues) => {
        if (updatedValues[this.key] !== undefined) {
          this.current = updatedValues[this.key].current;
          this.init = updatedValues[this.key].init;
          this.differ = updatedValues[this.key].differ;
          if (this.form) {
            this.update();
          }
        }
      });
  }

  public ngOnChanges() {
    if (this.form) {
      this.update();
    }
  }

  public update() {
    this.form.reset({
      current: formatNumber(
                Math.abs(this.current),
                nvLocalMapper[this.settingsService.locale],
                this.digitsInfo),
      new: formatNumber(
            Math.abs(this.init + this.differ),
            nvLocalMapper[this.settingsService.locale],
            this.digitsInfo)
    });
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

}
