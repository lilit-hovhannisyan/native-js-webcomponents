export interface IWithLabels {
  labelDe: string;
  labelEn: string;
}


export enum ILabelsEnum {
  labelEn = 'labelEn',
  labelDe = 'labelDe',
  displayLabel = 'displayLabel'
}
