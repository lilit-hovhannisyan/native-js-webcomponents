import { Injectable } from '@angular/core';
import { BehaviorSubject, forkJoin, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { wording } from 'src/app/core/constants/wording/wording';
import { SettingsService } from 'src/app/core/services/settings.service';
import { ConfirmationService } from '../../../../../../core/services/confirmation.service';
import { setTranslationSubjects } from '../../../../../../shared/helpers/general';
import { CompanyRepositoryService } from '../../../../../../core/services/repositories/company-repository.service';
import { CompanyLegalEntityRepositoryService } from '../../../../../../core/services/repositories/company-legal-entity-repository.service';
import { CompanyRepresentativeRepositoryService } from '../../../../../../core/services/repositories/company-representative-repository.service';
import { ContributionMainService } from '../../../../../../core/services/mainServices/company-contributions-main.service';
import { SilentPartnerRepositoryService } from '../../../../../../core/services/repositories/silent-partner-repository.service';
import { CompanyKey, ICompany } from 'src/app/core/models/resources/ICompany';
import { Language } from '../../../../../../core/models/language';
import { getCompanyLegalForm, getCompanyLegalFormTypes } from '../../../../../../core/models/enums/company-legal-form';

@Injectable({
  providedIn: 'root'
})
export class CompaniesHelperService {
  private editViewTitleSubject: BehaviorSubject<string> = new BehaviorSubject('');
  public editViewTitle$ = this.editViewTitleSubject.asObservable();
  public legalTabDataMissing$ = new BehaviorSubject<boolean>(false);

  public company: ICompany;
  public company$: BehaviorSubject<ICompany> = new BehaviorSubject<ICompany>(null);
  public companyHasLegalData = false;
  public companyHasLegalData$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);


  constructor(
    public settingsService: SettingsService,
    private confirmationService: ConfirmationService,
    private companyRepository: CompanyRepositoryService,
    private companyLegalEntityRepo: CompanyLegalEntityRepositoryService,
    private companyRepresentativeRepo: CompanyRepresentativeRepositoryService,
    private contributionMainService: ContributionMainService,
    private silentPartnerRepo: SilentPartnerRepositoryService,
  ) { }

  public setCompany(id: CompanyKey): void {
    this.companyRepository.fetchOne(id, {})
      .subscribe((company: ICompany) => {
        this.doesCompanyHasStoredLegalData(id).subscribe(() => {
          this.company$.next(company);
          this.company = company;
        });
      });
  }

  private doesCompanyHasStoredLegalData(companyId: CompanyKey): Observable<Observable<boolean>> {
    return forkJoin([
      this.companyLegalEntityRepo.fetchAll({ queryParams: { cid: companyId } }),
      this.companyRepresentativeRepo.fetchAll({ queryParams: { cid: companyId } }),
      this.contributionMainService.fetchAll({ queryParams: { cid: companyId } }),
      this.silentPartnerRepo.fetchAll({ queryParams: { cid: companyId } }),
    ]).pipe(
      map(([legalEntities, representatives, contributions, silentPartners]) => {
        this.companyHasLegalData = !!(legalEntities.length
          || representatives.length
          || contributions.length
          || silentPartners.length);

        this.setLegalDataState(this.companyHasLegalData);
        return of(this.companyHasLegalData);
      })
    );
  }

  public setLegalDataState(state: boolean): void {
    this.companyHasLegalData = state;
    this.companyHasLegalData$.next(this.companyHasLegalData);
  }

  public generateTitle = (company?: ICompany) => {
    let title = wording.general.create[this.settingsService.language];

    if (company) {
      title = company.companyName;
    }

    this.editViewTitleSubject.next(title);
  }

  public confirmThatLegalFormIsNoLongerEditable = (): Observable<boolean> => {
    const language: Language = this.settingsService.language;

    const legalForm: string = getCompanyLegalForm()
      .find(f => f.value === this.company.legalForm).displayLabel[language];

    let legalFormType = '';
    if (this.company.legalFormType) {
      legalFormType = getCompanyLegalFormTypes(this.company.legalForm)
        .find(ft => ft.value === this.company.legalFormType).displayLabel[language];

      legalFormType = `(${legalFormType})`;
    }

    const subject = this.company.legalFormType
      ? `${legalForm} ${legalFormType}`
      : `${legalForm}`;

    return this.confirmationService.confirm({
      okButtonWording: wording.general.save,
      cancelButtonWording: wording.general.cancel,
      wording: setTranslationSubjects(
        wording.system.legalDataIsNoLongerEditable,
        { subject }
      ),
    });
  }

  public checkAndCreateLegalData(callback: () => void): void {
    if (this.companyHasLegalData) {
      callback();
      return;
    }

    this.confirmThatLegalFormIsNoLongerEditable().subscribe(confirmed => {
      if (confirmed) {
        callback();
      }
    });

  }

  public reset(): void {
    this.setLegalDataState(false);
  }
}
