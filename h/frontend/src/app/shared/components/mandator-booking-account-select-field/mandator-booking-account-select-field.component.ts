import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { IBookingAccount, BookingAccountKey } from 'src/app/core/models/resources/IBookingAccount';
import { BookingAccountMainService } from 'src/app/core/services/mainServices/booking-account-main.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/booking-accounts.config';
import { AbstractValueAccessor, makeProvider } from 'src/app/core/abstracts/value-accessor';
import { SettingsService } from 'src/app/core/services/settings.service';
import { MandatorKey } from 'src/app/core/models/resources/IMandator';
import { map, switchMap, tap } from 'rxjs/operators';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';

@Component({
  selector: 'nv-mandator-booking-account-select-field',
  templateUrl: './mandator-booking-account-select-field.component.html',
  providers: [makeProvider(MandatorBookingAccountSelectFieldComponent)],
})
export class MandatorBookingAccountSelectFieldComponent extends AbstractValueAccessor implements OnInit {
  @Input() public bookingAccounts$: Observable<IBookingAccount[]>;
  @Input() public queryParams = {};
  @Input() public useFullEntity = false;
  @Input() public mandatorId$: Observable<MandatorKey>;

  @Output() public valueChange = new EventEmitter<IBookingAccount>();

  private defaultId: number;
  private bookingAccounts: IBookingAccount[] = [];

  public getGridConfig = getGridConfig;
  public resourceFormControl = new FormControl({ value: null });

  constructor(
    private bookingAccountService: BookingAccountMainService,
    private mandatorAccounts: MandatorAccountRepositoryService,
    private settingsService: SettingsService,
  ) {
    super();
  }

  public ngOnInit(): void {
    const language = this.settingsService.language;
    this.getLabel = (bookingAccount: IBookingAccount) => `${bookingAccount.no} ${bookingAccount.displayLabel[language]}`;
    if (!this.bookingAccounts$) {
      this.bookingAccounts$ = this.mandatorAccounts.fetchAll({}).pipe(
        switchMap((mandatorAccounts) => {
          return combineLatest([
            this.mandatorId$,
            this.bookingAccountService.fetchAll(),
          ]).pipe(
            map(([mandatorId, bookingAccounts]) => {
              const currentMandatorAccounts = mandatorAccounts
                .filter(ma => ma.mandatorId === mandatorId);

              const currentMandatorBookingAccoutns = bookingAccounts.filter(ba => {
                return !!currentMandatorAccounts.find(m => m.bookingAccountId === ba.id);
              }).map(ba => {
                const mandatorAccount = currentMandatorAccounts
                  .find(ma => ma.bookingAccountId === ba.id);
                return { ...ba, ...mandatorAccount };
              })
                .filter(ma => ma.isActive || ma.id === this.defaultId);

              return mandatorId
                ? currentMandatorBookingAccoutns
                : [];
            }),
            tap(bookingAccounts => this.bookingAccounts = bookingAccounts)
          );
        }));
    }
  }

  public getLabel = (bookingAccount: IBookingAccount) =>
    `${bookingAccount.no} ${bookingAccount.displayLabel}`

  public onSelect(value: number): void {
    this.onChange(value);
    this.onTouched(value);
    this.onBookingAccount(value);
  }

  public writeValue(value: number): void {
    this.onBookingAccount(value);
    this.resourceFormControl.setValue(value);
  }

  public setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.resourceFormControl.disable();
    } else {
      this.resourceFormControl.enable();
    }
  }

  private onBookingAccount(id: BookingAccountKey) {
    const bookingAccount = this.bookingAccounts.find(b => b.id === id);
    this.valueChange.emit(bookingAccount);
  }
}
