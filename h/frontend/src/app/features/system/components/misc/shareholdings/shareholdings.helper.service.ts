import { Injectable } from '@angular/core';
import { ICompany } from 'src/app/core/models/resources/ICompany';
import { BehaviorSubject } from 'rxjs';
import { ILegalFormFilter } from '../../../shared/legal-type-tree/legal-type-tree.component';


@Injectable({
  providedIn: 'root'
})
export class ShareholdingsHelperService {
  public legalData: ILegalFormFilter;
  public company: ICompany;
  public isInCreationMode$ = new BehaviorSubject<boolean>(false);

  constructor(
  ) {}

  public setCompany(company: ICompany): void {
    this.company = company;
  }

  public setLegalData(legalData: ILegalFormFilter): void {
    this.legalData = legalData;
  }

  public updateModeState(state: boolean): void {
    this.isInCreationMode$.next(state);
  }
}
