import { Component, OnInit, Input } from '@angular/core';
import { IAutobankBusinessCase } from 'src/app/core/models/resources/IAutobankBusinessCase';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AutobankBusinessCaseRepositoryService } from 'src/app/core/services/repositories/autobank-business-case-repository.service';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-autobank-business-cases-edit-modal',
  templateUrl: './autobank-business-cases-edit-modal.component.html',
  styleUrls: ['./autobank-business-cases-edit-modal.component.scss'],
})
export class AutobankBusinessCasesEditModalComponent implements OnInit {
  @Input() public closeModal: () => void;
  @Input() public autobankBusinessCase: IAutobankBusinessCase;
  public wording = wording;
  public form: FormGroup;

  constructor(
    private autobankBusinessCaseRepo: AutobankBusinessCaseRepositoryService,
    private fb: FormBuilder,
  ) { }

  public ngOnInit() {
    this.init(this.autobankBusinessCase);
  }

  public init(data: IAutobankBusinessCase = {} as IAutobankBusinessCase) {
    this.form = this.fb.group({
      gvc: [ data.gvc, [Validators.required, Validators.max(999), Validators.min(1)]],
      labelEn: [data.labelEn,  [Validators.required, Validators.maxLength(150)]],
      labelDe: [data.labelDe, [Validators.required, Validators.maxLength(150)]],
    });
  }

  private getFormValue(): IAutobankBusinessCase {
    return {
      ... this.autobankBusinessCase,
      ... this.form.value,
    };
  }

  public saveClicked(): void {
    this.autobankBusinessCaseRepo.update(this.getFormValue(), {})
      .subscribe(this.closeModal);
  }
}
