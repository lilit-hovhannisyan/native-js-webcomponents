import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { SettingsService } from 'src/app/core/services/settings.service';
import { NvLocale } from 'src/app/core/models/dateFormat';

export const dateTimeFormatTypes = {
  [NvLocale.DE]: 'dd.MM.y HH:mm',
  [NvLocale.UK]: 'MM/dd/y h:mm a',
  [NvLocale.US]: 'dd/MM/y h:mm a',
};

export const dateFormatTypes = {
  [NvLocale.DE]: 'dd.MM.y',
  [NvLocale.UK]: 'MM/dd/y',
  [NvLocale.US]: 'dd/MM/y',
};

export const timeFormatTypes = {
  [NvLocale.DE]: 'HH:mm',
  [NvLocale.UK]: 'h:mm a',
  [NvLocale.US]: 'h:mm a',
};

@Pipe({
  name: 'currentDateFormat',
})
export class CurrentDateFormatPipe implements PipeTransform {
  public ISO8601_DATE_REGEX
    = /^(\d{4})-?(\d\d)-?(\d\d)(?:T(\d\d)(?::?(\d\d)(?::?(\d\d)(?:\.(\d+))?)?)?(Z|([+-])(\d\d):?(\d\d))?)?$/;


  constructor(
    private datePipe: DatePipe,
    private settingsService: SettingsService
  ) { }
  public transform(
    value: any,
    timeDisplay?: { withTime?: boolean, justTime?: boolean },
  ): string | null {

    if (!value) {
      return null;
    }
    if ((typeof value === 'string') && value.match(this.ISO8601_DATE_REGEX)) {
      if (timeDisplay?.withTime) {
        return this.datePipe.transform(
          value,
          dateTimeFormatTypes[this.settingsService.locale]
        );
      } else if (timeDisplay?.justTime) {
        return this.datePipe.transform(
          value,
          timeFormatTypes[this.settingsService.locale]
        );
      }

      return this.datePipe.transform(
        value,
        dateFormatTypes[this.settingsService.locale]
      );
    }

    return value;
  }
}
