import {
  Component,
  OnInit
} from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { IPort, PortKey } from '../../../../../../core/models/resources/IPort';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { Observable } from 'rxjs';
import { PortRepositoryService } from '../../../../../../core/services/repositories/port-repository.service';
import { PortMainService } from '../../../../../../core/services/mainServices/port-main.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/ports.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
})
export class PortListViewComponent implements OnInit {
  public portsGridConfig: NvGridConfig;
  public dataSource$: Observable<IPort[]>;
  constructor(
    private portService: PortMainService,
    private portRepoService: PortRepositoryService,
    private confirmationService: ConfirmationService,
    private authService: AuthenticationService,
    private router: Router,
    public gridConfigService: GridConfigService
  ) { }

  public ngOnInit() {
    this.dataSource$ = this.portService.getStream();
    const actions = {
      edit: (id: PortKey) => this.router.navigate([`${this.router.url}/${id}`]),
      delete: this.deletePortClicked,
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    this.portsGridConfig = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  public deletePortClicked = (port: IPort) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: port.name },
    }).subscribe(confirmed => confirmed && this.portRepoService.delete(port.id, {}).subscribe());
  }
}
