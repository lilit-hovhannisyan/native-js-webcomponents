import { IWording } from '../resources/IWording';
import { wording } from '../../constants/wording/wording';

/** Used for css-level status determining and defining status color by data-attribute. */
export type TaskStatusState = 'not-started' | 'open' | 'in-progress' | 'done';

/** Custom interface added to be used in `taskStatusMap` for better manipulation. */
export interface ITaskStatus {
  /** Current state of status. */
  state: TaskStatusState;
  /** Next status value. */
  next: VesselTaskStatus;
  /** Current status wording. */
  wording: IWording;
}

/**
 * Originated from backend-DTO. Most likely enum members
 * are not complete and can be added in the future.
 */
export enum TaskType {
  /** Default task type. */
  SimpleVesselTask,
  /** Registration page task type. */
  RegistrationVesselTask,
}

/** Members are strongly ordered and the order can be used in codes. */
export enum VesselTaskStatus {
  NotStarted,
  Open,
  InProgress,
  Done,
}


/** Comfortable to use for getting the current state info. */
export const taskStatusMap: Map<VesselTaskStatus, ITaskStatus> = new Map([
  [
    VesselTaskStatus.NotStarted,
    {
      state: 'not-started',
      next: VesselTaskStatus.Open,
      wording: wording.chartering.notStarted,
    },
  ],
  [
    VesselTaskStatus.Open,
    {
      state: 'open',
      next: VesselTaskStatus.InProgress,
      wording: wording.chartering.open,
    },
  ],
  [
    VesselTaskStatus.InProgress,
    {
      state: 'in-progress',
      next: VesselTaskStatus.Done,
      wording: wording.chartering.inProgress,
    },
  ],
  [
    VesselTaskStatus.Done,
    {
      state: 'done',
      next: VesselTaskStatus.NotStarted,
      wording: wording.chartering.done,
    },
  ],
]);

export type VesselSettingTypeMap<T = IWording> = Map<TaskType, T>;

export const taskTypes: VesselSettingTypeMap = new Map([
  [
    TaskType.RegistrationVesselTask,
    wording.system.registrationVesselTask,
  ],
  [
    TaskType.SimpleVesselTask,
    wording.system.simpleVesselTask,
  ]
]);
