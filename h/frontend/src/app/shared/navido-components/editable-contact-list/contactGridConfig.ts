import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { NvGridConfig, NvGridButtonsPosition, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getContactGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
  ): NvGridConfig => {
  return ({

    gridName: 'contactsGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    showPaging: true,
    hideRefreshButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'contactPerson',
        title: wording.accounting.contactPerson,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        }
      },
      {
        key: 'roleName',
        title: wording.accounting.role,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        }
      },
      {
        key: 'typeName',
        title: wording.accounting.type,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        }
      },
      {
        key: 'value',
        title: wording.accounting.value,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        }
      }
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editContact',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: actions.edit
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteAddress',
        tooltip: wording.general.delete,
        hidden: !canPerform(Operations.DELETE),
        func: actions.delete
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        hidden: () => !canPerform(Operations.CREATE),
        icon: 'plus-circle-o',
        func: actions.add
      }
    ]
  });
};

