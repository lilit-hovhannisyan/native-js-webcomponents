import { IResource } from './IResource';
export type AccountBalanceKey = number;

interface ISharedProps {
  year: number;
  mandatorId: number;
  bookingAccountId: number;
  balanceLocal: number;
  balance: number;
  currencyId: number;
}

export interface IAccountBalanceCreate extends ISharedProps { }

export interface IAccountBalanceCreate extends ISharedProps { }

export interface IAccountBalanceBase extends ISharedProps, IResource<number> { }

export interface IAccountBalanceDTO extends IAccountBalanceBase { }

export interface IAccountBalance extends IAccountBalanceDTO { }
