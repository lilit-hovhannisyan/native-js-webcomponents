import { Component, ChangeDetectionStrategy, ViewChild, AfterViewInit } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { IRegistrationDashboardButton, registrationDashboardButtons as buttons } from './registrations-dashboard.helper';
import { CalendarComponent } from 'src/app/shared/components/calendar/calendar.component';
import { DashboardTasksComponent } from './dashboard-tasks/dashboard-tasks.component';
import * as moment from 'moment';

@Component({
  selector: 'nv-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements AfterViewInit {
  @ViewChild('calendar', { static: false }) public calendar: CalendarComponent;
  @ViewChild(DashboardTasksComponent, { static: false }) public dashboard: DashboardTasksComponent;

  public wording = wording;
  public registrationDashboardButtons: IRegistrationDashboardButton[] = buttons;
  public calendarRange: { start: moment.Moment, end: moment.Moment };

  constructor() { }

  public ngAfterViewInit(): void {
    this.calendarRange = {
      start: moment(this.calendar.calendarApi.view.currentStart),
      end: moment(this.calendar.calendarApi.view.currentEnd),
    };
  }

  public onCalendarRangeChange(
    event: { start: moment.Moment, end: moment.Moment }
  ): void {
    this.calendarRange = {
      start: event.start,
      end: event.end,
    };
  }

  /** Page custom validation to prevent navigation when unsaved-changes exist. */
  public customValidation = (): boolean => this.dashboard.isValid();
}
