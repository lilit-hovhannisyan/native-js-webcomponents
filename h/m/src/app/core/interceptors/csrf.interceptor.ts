import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, from } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';

import { SecurityWebService } from '@core-services/security.web-service';

@Injectable()
export class CSRFInterceptor implements HttpInterceptor {
  private addCSRFToMethods: string[] = ['POST', 'PUT', 'DELETE'];
  private excludedRequestUrls: string[] = [
    'auth/login',
    'auth/logout',
    'auth/password',
    'auth/password-reset',
    'auth/password-reset-token',
    'events/batch',
    'users/user-invite-token'
  ];

  constructor(private securityWebService: SecurityWebService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (this.addCSRFToMethods.includes(request.method) && this.isExcluded(request.url)) {
      // request token
      return this.getCSRFToken().pipe(
        mergeMap((token: string) => {
          if (token) {
            // clone and modify the request
            const clonedRequest = request.clone({
              headers: request.headers.set('X-CSRF-Nonce', token)
            });

            // call cloned request which now has CSRF token
            return next.handle(clonedRequest);
          }

          // if token is not found continue with request
          return next.handle(request);
        })
      );
    } else {
      // continue with GET request
      return next.handle(request);
    }
  }

  private isExcluded(requestUrl: string): boolean {
    return this.excludedRequestUrls.every((url) => requestUrl.indexOf(url) === -1);
  }

  private getCSRFToken(): Observable<string> {
    return from(
      this.securityWebService.getCSRF().pipe(
        map((response) => {
          return response.nonce;
        })
      )
    );
  }
}
