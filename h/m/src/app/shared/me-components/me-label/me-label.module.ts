import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeLabelComponent } from './me-label.component';

@NgModule({
  declarations: [MeLabelComponent],
  imports: [CommonModule],
  exports: [MeLabelComponent]
})
export class MeLabelModule {}
