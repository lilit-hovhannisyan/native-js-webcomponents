interface PaginationI {
  totalCount: number;
  pageIndex: number; // The zero-based index of the current page.
  pageSize: number;
  pageSizeOptions?: number[];
}

export default PaginationI;
