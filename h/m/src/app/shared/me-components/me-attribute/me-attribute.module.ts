import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IconsModule } from '@shared-modules/icons.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

import { MeBooleanInputComponent } from './me-boolean-input/me-boolean-input.component';
import { MeAttributeComponent } from './me-attribute.component';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MeTextInputComponent } from './me-text-input/me-text-input.component';
import { MeMultiSelectInputComponent } from './me-multi-select-input/me-multi-select-input.component';
import { MatNativeDateModule, MatOptionModule } from '@angular/material/core';
import { MatSelectModule, MAT_SELECT_CONFIG } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { MeSelectInputComponent } from './me-select-input/me-select-input.component';
import { MeTextAreaInputComponent } from './me-text-area-input/me-text-area-input.component';
import { MeInputErrorComponent } from './me-input-error/me-input-error.component';
import { MeNumberInputComponent } from './me-number-input/me-number-input.component';
import { MeButtonModule } from '../me-button/me-button.module';
import { MeCompositeInputComponent } from './me-composite-input/me-composite-input.component';
import { MeDateTimeInputComponent } from './me-date-time-input/me-date-time-input.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

@NgModule({
  declarations: [
    MeAttributeComponent,
    MeBooleanInputComponent,
    MeTextInputComponent,
    MeMultiSelectInputComponent,
    MeSelectInputComponent,
    MeTextAreaInputComponent,
    MeNumberInputComponent,
    MeInputErrorComponent,
    MeCompositeInputComponent,
    MeDateTimeInputComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IconsModule,
    MatChipsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatNativeDateModule,
    MatOptionModule,
    MatSelectModule,
    MatSlideToggleModule,
    MeButtonModule,
    NgxMatSelectSearchModule,
    ReactiveFormsModule
  ],
  exports: [
    MeAttributeComponent,
    MeBooleanInputComponent,
    MeTextInputComponent,
    MeMultiSelectInputComponent,
    MeSelectInputComponent,
    MeTextAreaInputComponent,
    MeNumberInputComponent,
    MeInputErrorComponent,
    MeCompositeInputComponent,
    MeDateTimeInputComponent
  ],
  providers: [
    {
      provide: MAT_SELECT_CONFIG,
      useValue: {
        disableOptionCentering: true
      }
    }
  ]
})
export class MeAttributeModule {}
