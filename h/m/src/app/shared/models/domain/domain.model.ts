import { Type } from 'class-transformer';

import { AttributeModel } from '@shared-models/attribute/attribute.model';
import { LayoutModel } from '@shared-models/layout/layout.model';
import { StateModel } from '../state/state.model';

export class DomainModel {
  rootDomain: string = '';
  instantiable: boolean = false;
  internalName: string = '';
  displayName: string = '';
  icon: string = '';
  @Type(() => AttributeModel)
  // tslint:disable-next-line:no-any
  attributes: AttributeModel<any>[] = [];
  @Type(() => LayoutModel)
  layouts: LayoutModel[] = [];
  @Type(() => DomainModel)
  descendants: DomainModel[] = [];
  @Type(() => StateModel)
  states: StateModel[] = [];
}

export class DomainsModel {
  @Type(() => DomainModel)
  domains: DomainModel[] = [];
}
