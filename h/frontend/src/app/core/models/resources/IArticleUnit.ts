import { IResource } from './IResource';
import { IWithActive } from './IWithActive';
import { IWithSystemFlag } from './IWithSystemFlag';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';

export type ArticleUnitKey = number;

interface ISharedProps extends IWithLabels, IWithActive, IWithSystemFlag {
  code: string;
  decimalPlaces: number;
}

export interface IArticleUnitBase extends ISharedProps, IResource<ArticleUnitKey> { }

export interface IArticleUnitCreate extends ISharedProps { }

export interface IArticleUnitDTO extends IArticleUnitBase { }

export interface IArticleUnit extends IArticleUnitDTO, IWithLabel { }
