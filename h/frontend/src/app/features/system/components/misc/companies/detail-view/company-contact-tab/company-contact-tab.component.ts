import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './company-contact-tab.component.html',
  styleUrls: ['./company-contact-tab.component.scss'],
})
export class CompanyContactTabComponent  implements OnInit {
  public companyId: number;

  constructor(
    private route: ActivatedRoute
  ) {}

  public ngOnInit() {
    this.companyId = +this.route.parent.snapshot.params.id;
  }
}
