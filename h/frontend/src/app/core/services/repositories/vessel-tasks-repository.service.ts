import { CompanyKey } from 'src/app/core/models/resources/ICompany';
import { AddressKey } from '../../models/resources/IAddress';
import { Injectable } from '@angular/core';
import { IAddress, IAddressDTO } from '../../models/resources/IAddress';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IVesselTaskDTO, IVesselTask, IVesselTaskCreate, VesselTaskKey } from '../../models/resources/IVesselTask';
import { AddressParams } from './address-repository.service';

export interface VesselTaskParams extends RepoParams<IVesselTask> {
}

@Injectable({
  providedIn: 'root'
})
export class VesselTasksRepositoryService extends AbstractRepository<
  IVesselTask,
  IVesselTaskDTO,
  IVesselTaskCreate,
  VesselTaskParams,
  VesselTaskKey
> {
  protected resourceType = 'VesselTask';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(params: VesselTaskParams): string {
    return `/api/vessel-tasks`;
  }

  public postProcess(item: IVesselTaskDTO): IVesselTask { return item; }

}
