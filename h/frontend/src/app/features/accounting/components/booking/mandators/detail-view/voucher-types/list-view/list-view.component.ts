import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { IMandatorVoucherType } from 'src/app/core/models/resources/IMandatorVoucherType';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { MandatorVoucherTypeRepositoryService } from 'src/app/core/services/repositories/mandator-voucher-type-repository.service';
import { MandatorVoucherTypeMainService } from '../../../../../../../../core/services/mainServices/mandator-voucher-type-main.service';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { MandatorVoucherTypesDetailViewComponent } from '../detail-view/detail-view.component';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/mandator-voucher-types.config';
import { MandatorVoucherTypeEditComponent } from '../edit-modal/edit-modal.component';
import { wording } from 'src/app/core/constants/wording/wording';
import { IVoucherType } from 'src/app/core/models/resources/IVoucherType';
import { BaseComponent } from 'src/app/core/abstracts/BaseComponent';
import { BaseComponentService } from 'src/app/core/services/base-component.service';

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class MandatorVoucherTypesListViewComponent extends BaseComponent implements OnInit {
  public mandatorId: number;
  private modalRef?: NzModalRef;
  public config: NvGridConfig;
  public dataSource$: Observable<IMandatorVoucherType[]>;
  public wordingRef = wording.accounting;

  constructor(
    private modalService: NzModalService,
    private mainService: MandatorVoucherTypeMainService,
    private mandatorVoucherTypeRepo: MandatorVoucherTypeRepositoryService,
    public gridConfigService: GridConfigService,
    public route: ActivatedRoute,
    private bcs: BaseComponentService,
  ) {
    super(bcs);
  }

  public ngOnInit() {
    this.mainService.fetchAll().subscribe();
    this.mandatorId = parseInt(this.route.parent.snapshot.params.id, 10);
    this.dataSource$ = this.mainService.getStream(this.mandatorId);

    const actions = {
      edit: this.openEditModal,
      delete: this.deleteClicked,
      add: this.openDetailViewModal
    };

    const canPerform = (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation);
    this.config = getGridConfig(actions, canPerform);
  }

  private deleteClicked = (voucherType: IVoucherType) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: voucherType.displayLabel },
    }).subscribe(confirmed => confirmed && this.mandatorVoucherTypeRepo.delete(voucherType.id, {}).subscribe());
  }

  private openEditModal = (voucherType: IMandatorVoucherType) => {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzWidth: 350,
      nzTitle: voucherType.abbreviation,
      nzClosable: true,
      nzContent: MandatorVoucherTypeEditComponent,
      nzComponentParams: {
        closeModal: this.closeModal,
        voucherType,
      }
    });
  }

  private closeModal = () => {
    this.modalRef.destroy();
    this.mainService.getStream(this.mandatorId).subscribe();
  }

  private openDetailViewModal = () => {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzWidth: 750,
      nzTitle: this.wordingRef.voucherTypestitle[this.settingsService.language],
      nzClosable: true,
      nzContent: MandatorVoucherTypesDetailViewComponent,
      nzComponentParams: {
        closeModal: this.closeModal,
        mandatorId: this.mandatorId
      }
    });
  }
}
