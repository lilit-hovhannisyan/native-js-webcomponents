import { Injectable } from '@angular/core';
import { CompanyLegalEntityRepositoryService } from 'src/app/core/services/repositories/company-legal-entity-repository.service';
import { ICompanyLegalEntity } from 'src/app/core/models/resources/ICompanyLegalEntity';
import { CompanyKey } from '../../models/resources/ICompany';
import { Observable } from 'rxjs';
import { CurrencyKey } from '../../models/resources/ICurrency';
import { CAPITAL_TYPE } from 'src/app/core/models/enums/company-legal-form';

const defaultEntity: ICompanyLegalEntity = {
  companyId: null,
  hra: null,
  hrb: null,
  commercialRegister: null,
  dateOfEntry: null,
  sumOfContributions: null,
  shareCapital: null,
  currencyId: null,
  capitalType: CAPITAL_TYPE.ParValueShare,
  sumOfNonParValueShares: null,
  companyNumber: null,
  partners: [],

  // Classification
  groupOfCompanyId: null,
  isShellCompany: null,
  isDormantCompany: null,
  isUnitaryLimitedPartnership: null,

  // Remark
  remark: null,

  // Status
  isPreliminaryInsolvencyProceeding: null,
  preliminaryInsolvencyProceedingStartDate: null,
  preliminaryInsolvencyAdministrator: null,
  isInsolvencyProceeding: null,
  insolvencyProceedingStartDate: null,
  isInLiquidation: null,
  liquidationStartDate: null,
  liquidator: null,
  isDeleted: null,
  deletedStartDate: null,
  isSold: null,
  soldStartDate: null,
  buyer: null,
} as ICompanyLegalEntity;

@Injectable({
  providedIn: 'root',
})
export class CompanyLegalEntityMainService {

  constructor(
    private legalEntityRepo: CompanyLegalEntityRepositoryService,
  ) { }

  public createDefault(
    companyId: CompanyKey,
    currencyId: CurrencyKey,
  ): Observable<ICompanyLegalEntity> {
    const newEntity = {
      ...defaultEntity,
      companyId,
      currencyId,
    } as ICompanyLegalEntity;

    return this.legalEntityRepo.create(newEntity, {});
  }
}
