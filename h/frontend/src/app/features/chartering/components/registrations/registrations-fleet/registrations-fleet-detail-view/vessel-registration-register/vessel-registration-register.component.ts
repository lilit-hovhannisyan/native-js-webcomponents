import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { IVesselRegister } from 'src/app/core/models/resources/IVesselRegister';
import { RegistrationFleetHelperService } from '../registrations-fleet-helper.service';

@Component({
  selector: 'nv-vessel-registration-register',
  templateUrl: './vessel-registration-register.component.html',
  styleUrls: ['./vessel-registration-register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VesselRegistrationRegisterComponent implements OnInit {
  public node: SitemapNode = sitemap.chartering.children.registrations.children
    .registrationsFleet.children.id.children.register;
  public vesselId: VesselKey;

  constructor(
    private route: ActivatedRoute,
    private helperService: RegistrationFleetHelperService,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    this.vesselId = +this.route.snapshot.parent.params.id;
  }

  /** Handles editMode change and disables/enables registers-slider. */
  public editModeChangeHandler(isEditMode: boolean): void {
    this.helperService.registersSliderDisabledSubject.next(isEditMode);
  }

  public registerSelectHandler(register: IVesselRegister): void {
    this.helperService.selectedRegisterSubject.next(register);
    this.cdr.detectChanges();
  }
}
