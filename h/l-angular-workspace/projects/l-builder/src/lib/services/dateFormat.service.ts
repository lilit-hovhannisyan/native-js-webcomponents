export const dateFormat = (date, format = 'dd/mm/yyyy HH:MM:SS') => {
    date = new Date(date);
    let mm = date.getDate();

    if (isNaN(mm)) {
        console.warn('L-builder: invalid date');
    }

    mm = mm < 10 ? '0' + mm : mm;
    let dd = date.getMonth() + 1;
    dd = dd < 10 ? '0' + dd : dd;
    const yyyy = date.getFullYear();

    let HH = date.getHours();
    HH = HH < 10 ? '0' + HH : HH;
    let MM = date.getMinutes();
    MM = MM < 10 ? '0' + MM : MM;
    let SS = date.getSeconds();
    SS = SS < 10 ? '0' + SS : SS;


    const _date = format.replace('dd', dd)
        .replace('mm', mm)
        .replace('yyyy', yyyy)
        .replace('HH', HH)
        .replace('MM', MM)
        .replace('SS', SS);

    return _date;
};
