import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { IconsModule } from '@shared/modules/icons.module';

import { MeRightSidebarComponent } from './me-right-sidebar.component';
import { MeSearchInputModule } from '../me-search-input/me-search-input.module';
import { MeContextMenuModule } from '../me-context-menu/me-context-menu.module';
import { MeEllipsisRowModule } from '../me-ellipsis-row/me-ellipsis-row.module';
import { MeDocumentLineModule } from '../me-document-line/me-document-line.module';

@NgModule({
  declarations: [MeRightSidebarComponent],
  imports: [
    CommonModule,
    MatTabsModule,
    MeSearchInputModule,
    MeContextMenuModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MeEllipsisRowModule,
    MeContextMenuModule,
    FormsModule,
    MatCheckboxModule,
    IconsModule,
    MeDocumentLineModule
  ],
  exports: [MeRightSidebarComponent]
})
export class MeRightSidebarModule {}
