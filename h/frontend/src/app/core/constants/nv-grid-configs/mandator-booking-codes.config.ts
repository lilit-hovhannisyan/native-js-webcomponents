import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IMandatorBookingCode } from 'src/app/core/models/resources/IMandatorBookingCode';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'mandatorBookingCodeGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'no',
        title: wording.accounting.no,
        width: 90,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wording.general.label,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteBookingCode',
        tooltip: wording.general.delete,
        hidden: !canPerform(Operations.DELETE),
        func: (mandatorBookingCode: IMandatorBookingCode) => {
          actions.delete(mandatorBookingCode);
        }
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.add,
        tooltip: wording.general.add,
        icon: 'plus-circle-o',
        func: () => actions.add(),
        hidden: () => !canPerform(Operations.CREATE),
      }
    ]
  });
};
