import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';
import { IWording } from './IWording';

export type TitleKey = number;

export interface ISharedProps extends IResource<number>, IWithLabels, IWithLabel { }

export interface ITitleBase extends ISharedProps, IResource<TitleKey> { }

export interface ITitleCreate extends ISharedProps { }

export interface ITitleDTO extends ITitleBase { }

export interface ITitle extends ITitleDTO { }

export interface ITitleFull extends ITitle {
  title?: ITitle;
}

export interface ITitleRow extends ITitleFull {
  TitleLabel?: IWording;
}
