import { Component, OnInit, ChangeDetectionStrategy, Input, OnDestroy } from '@angular/core';
import { IWording } from 'src/app/core/models/resources/IWording';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ICompanyRepresentative, ICompanyRepresentativeFull, ICompanyRepresentativeRow } from '../../../../../../../../../core/models/resources/ICompanyRepresentative';
import { Validators } from 'src/app/core/classes/validators';
import { ICompany } from 'src/app/core/models/resources/ICompany';
import { ReplaySubject, Observable } from 'rxjs';
import { takeUntil, switchMap } from 'rxjs/operators';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { RepresentativeFunctionMap, getFunctionTypesForLimitedPartnership, getFunctionTypesForLimitedLiability, getFunctionTypesForStockCorporation, getFunctionTypesForCivilLawPartnership, getFunctionTypesForForeignCompany } from 'src/app/core/models/enums/RepresentativeFunctionType';
import { COMPANY_LEGAL_FORM } from 'src/app/core/models/enums/company-legal-form';
import { ScopeOfRepresentationMap, scopeOfRepresentationTypeOptions } from 'src/app/core/models/enums/ScopeOfRepresentationType';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { wording } from 'src/app/core/constants/wording/wording';
import * as moment from 'moment';
import { SalutationRepositoryService } from 'src/app/core/services/repositories/salutation-repository.service';
import { ISalutation } from 'src/app/core/models/resources/ISalutation';
import { CompanyRepresentativeRepositoryService } from '../../../../../../../../../core/services/repositories/company-representative-repository.service';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { CompaniesHelperService } from '../../../companies.helper.service';

@Component({
  selector: 'nv-representatives-detail-view-modal',
  templateUrl: './representatives-detail-view-modal.component.html',
  styleUrls: ['./representatives-detail-view-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RepresentativesDetailViewModalComponent implements OnInit, OnDestroy {
  @Input() public title?: IWording;
  @Input() public representative?: ICompanyRepresentativeFull;
  @Input() public company: ICompany;
  @Input() public representatives: ICompanyRepresentativeRow[];
  @Input() public defineCheckboxTitle?: IWording;
  @Input() public memberToDefineTitle?: IWording;

  public form: FormGroup;
  public privatePersonForm: FormGroup;
  private resourceDefault: ICompanyRepresentative = {} as ICompanyRepresentative;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  public availableFunctionTypes: RepresentativeFunctionMap<IWording | string>;
  public scopeOfRepresentationTypes: ScopeOfRepresentationMap;
  public wording = wording;
  public salutations$: Observable<ISalutation[]>;
  public isSaveDisabled: boolean;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private salutationsRepo: SalutationRepositoryService,
    private representativeRepo: CompanyRepresentativeRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private companiesHelperService: CompaniesHelperService,
  ) { }

  public ngOnInit(): void {
    this.availableFunctionTypes = this.getAvailableFunctionTypes();
    this.scopeOfRepresentationTypes = scopeOfRepresentationTypeOptions;
    this.fetchSalutations();
    this.initForm(this.representative || this.resourceDefault);
    this.initPrivatePersonForm();
    this.listenToManagingPartnerChanges();
    this.listenToDateChanges();
  }

  private fetchSalutations(): void {
    this.salutations$ = this.salutationsRepo.fetchAll({});
  }

  private initForm(representative: ICompanyRepresentative = this.resourceDefault): void {
    this.form = this.fb.group({
      partnerId: [
        representative.partnerId,
        this.representative ? [] : [Validators.required]
      ],
      defineManagingPartner: [false],
      isActive: [this.representative ? representative.isActive : !representative.to],
      from: [representative.from, [Validators.required]],
      to: [
        representative.to,
        [Validators.isAfterFieldDate('from', false)],
      ],
      releaseSec181_1: [representative.releaseSec181_1],
      releaseSec181_2: [representative.releaseSec181_2],
      functionType: [representative.functionType, [Validators.required]],
      scopeOfRepresentationType: [
        representative.scopeOfRepresentationType,
        [Validators.required],
      ],
      remark: [representative.remark],
    });
  }

  private initPrivatePersonForm(): void {
    this.privatePersonForm = this.fb.group({
      salutationId: [],
      firstName: [],
      lastName: [],
    });
  }

  private listenToManagingPartnerChanges(): void {
    const { defineManagingPartner, partnerId } = this.form.controls;
    const { salutationId, firstName, lastName } = this.privatePersonForm.controls;

    defineManagingPartner.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(shouldDefinePartner => {
        if (shouldDefinePartner) {
          partnerId.setValidators([]);
          partnerId.reset();
          partnerId.disable();
          salutationId.setValidators([Validators.required]);
          firstName.setValidators([Validators.required]);
          lastName.setValidators([Validators.required]);
        } else {
          partnerId.enable();
          partnerId.setValidators([Validators.required]);
          salutationId.setValidators([]);
          firstName.setValidators([]);
          lastName.setValidators([]);
        }
        partnerId.markAsDirty();
        partnerId.markAsTouched();
        partnerId.updateValueAndValidity();
      });
  }

  private listenToDateChanges(): void {
    const { isActive, to } = this.form.controls;

    to.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(date => {
        if (!date || moment(date).isSameOrAfter(moment.now(), 'day')) {
          isActive.setValue(true);
        } else {
          isActive.setValue(false);
        }
      });
  }

  private getAvailableFunctionTypes(): RepresentativeFunctionMap<IWording | string> {
    let availableFunctionTypes: RepresentativeFunctionMap<IWording | string>;

    switch (this.company.legalForm) {
      case COMPANY_LEGAL_FORM.LimitedPartnership:
        availableFunctionTypes = getFunctionTypesForLimitedPartnership();
        break;
      case COMPANY_LEGAL_FORM.CompanyWithLimitedLiability:
        availableFunctionTypes = getFunctionTypesForLimitedLiability();
        break;
      case COMPANY_LEGAL_FORM.StockCorporation:
        availableFunctionTypes = getFunctionTypesForStockCorporation();
        break;
      case COMPANY_LEGAL_FORM.CivilLawPartnership:
        availableFunctionTypes = getFunctionTypesForCivilLawPartnership();
        break;
      case COMPANY_LEGAL_FORM.ForeignCompany:
          availableFunctionTypes = getFunctionTypesForForeignCompany();
          break;
      default:
        availableFunctionTypes = new Map();
    }

    return availableFunctionTypes;
  }


  public triggerCancel(): void {
    this.modalRef.triggerCancel();
  }

  public submit(): void {
    validateAllFormFields(this.form);

    if (!this.form.valid) {
      return;
    }

    const { defineManagingPartner } = this.form.controls;

    if (defineManagingPartner.value) {
      validateAllFormFields(this.privatePersonForm);
      if (!this.privatePersonForm.valid) {
        return;
      }

      this.companiesHelperService.checkAndCreateLegalData(this.createPrivatePerson);
    } else {
      this.companiesHelperService.checkAndCreateLegalData(this.createOrUpdateRepresentativeAndCloseModal);
    }
  }

  public createOrUpdateRepresentativeAndCloseModal = (): void => {
    this.createOrUpdateRepresentative({ ...this.form.value }).subscribe(representative => {
      this.companiesHelperService.setLegalDataState(!!representative);
      this.modalRef.triggerOk();
    });
  }

  private createPrivatePerson = (): void => {
    const { value } = this.privatePersonForm;
    const payload: ICompany = {
      ...value,
      isPrivatePerson: true,
      companyName: `${value.lastName}, ${value.firstName}`,
    };
    this.isSaveDisabled = true;
    this.companyRepo
      .create(payload, {})
      .pipe(
        switchMap(privatePerson => this.createOrUpdateRepresentative({
          ...this.form.value,
          partnerId: privatePerson.id,
        }))
      ).subscribe(representative => {
        this.companiesHelperService.setLegalDataState(!!representative);
        this.modalRef.triggerOk();
      });
  }

  private createOrUpdateRepresentative(
    representative: ICompanyRepresentative,
  ): Observable<ICompanyRepresentative> {
    if (this.representative) {
      return this.representativeRepo.update(
        { ...this.representative, ...representative },
        {}
      );
    } else {
      return this.representativeRepo.create(
        { ...representative, companyId: this.company.id },
        {}
      );
    }
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

}
