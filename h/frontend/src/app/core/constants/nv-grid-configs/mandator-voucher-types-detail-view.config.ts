import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getConfig = (): NvGridConfig => {
  return ({
    gridName: 'viewVoucherTypesConfig',
    rowSelectionType: NvGridRowSelectionType.RadioButton,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 10,
      showTotalItems: true
    },
    columns: [
      {
        key: 'abbreviation',
        title: wording.accounting.abbreviation,
        width: 150,
        isSortable: true
      }, {
        key: 'displayLabel',
        title: wording.general.label,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      }
    ]
  });
};
