import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { MeNewCollectionCardI } from './me-new-collection-card.interface';

@Component({
  selector: 'me-new-collection-card',
  templateUrl: './me-new-collection-card.component.html',
  styleUrls: ['./me-new-collection-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeNewCollectionCardComponent {
  @Input() dataModel!: MeNewCollectionCardI;
  @Output() eventOccurs: EventEmitter<{ eventName: string; payload?: string }> = new EventEmitter();

  constructor() {}

  customEvent(eventName: string, payload?: string): void {
    this.eventOccurs.emit({ eventName, payload });
  }

  handleItemClick(payload: string): void {
    this.eventOccurs.emit({ eventName: 'contextMenuItemClick', payload });
  }

  preventOtherEvents(e: Event): void {
    e.stopPropagation();
  }
}
