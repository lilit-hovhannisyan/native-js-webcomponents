import { MandatorVoucherTypeRepositoryService } from 'src/app/core/services/repositories/mandator-voucher-type-repository.service';
import { IVoucherTypeCreate } from 'src/app/core/models/resources/IVoucherType';
import { IVoucherType } from 'src/app/core/models/resources/IVoucherType';
import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Validators } from 'src/app/core/classes/validators';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { VoucherTypeRepositoryService } from 'src/app/core/services/repositories/voucher-type-repository.service';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class VoucherTypeDetailViewComponent extends ResourceDetailView<IVoucherType, IVoucherTypeCreate> implements OnInit {

  public repoParams = {};
  public routeBackUrl = url(sitemap.accounting.children.booking.children.voucherTypes);
  public form: FormGroup;
  public formElement: ElementRef = this.el.nativeElement.querySelector('form');
  protected resourceDefault = { isActive: true } as IVoucherType;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    voucherTypesRepo: VoucherTypeRepositoryService,
    private mandatorVoucherTypesRepo: MandatorVoucherTypeRepositoryService,
    baseComponentService: BaseComponentService,
    private el: ElementRef,
    private cdr: ChangeDetectorRef,
  ) {
    super(voucherTypesRepo, baseComponentService);
  }

  public ngOnInit() {
    this.init();
    const { id } = this.route.parent.snapshot.params;
    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => this.resource[this.labelKey];

  public init(voucherType: IVoucherType = this.resourceDefault): void {
    this.initForm(this.fb.group({
      abbreviation: [voucherType.abbreviation, [Validators.required, Validators.maxLength(3)]],
      isAutomaticNumbering: [voucherType.isAutomaticNumbering],
      labelDe: [voucherType.labelDe, [Validators.required, Validators.maxLength(50)]],
      labelEn: [voucherType.labelEn, [Validators.required, Validators.maxLength(50)]],
      useCase: [voucherType.useCase, [Validators.required]],
      isActive: [voucherType.isActive],
    }));
  }

  public submit(): void {
    if (!this.form.valid) { return; }

    this.isGettingDeactivated()
      ? this.showDeactivationConfirmModal()
      : this.confirmedSubmit(false);
  }

  private confirmedSubmit(isGettingDeactivated: boolean): void {

    const { value } = this.form;
    const voucherType = this.creationMode
      ? { ...value }
      : { ...this.resource, ...value };

    this.creationMode
      ? this.createResource(voucherType)
      : this.updateResource(voucherType).subscribe(
        () => { if (isGettingDeactivated) { this.mandatorVoucherTypesRepo.clearStream(); } },
        () => { if (isGettingDeactivated) { this.mandatorVoucherTypesRepo.clearStream(); } },
      );
  }

  public isGettingDeactivated(): boolean {
    const isActive = this.form.controls.isActive.value;
    return !isActive && (this.resource && isActive !== this.resource.isActive);
  }

  public showDeactivationConfirmModal(): void {
    this.confirmationService.confirm({
      title: wording.accounting.deactivateVoucherTypeTitle,
      wording: wording.accounting.deactivateVoucherType,
      okButtonWording: wording.general.deactivate,
      cancelButtonWording: wording.general.cancel,
    }).subscribe(confirmed => confirmed && this.confirmedSubmit(true));
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
