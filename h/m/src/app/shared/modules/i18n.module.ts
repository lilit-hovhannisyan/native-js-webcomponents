import { NgModule } from '@angular/core';
import { TranslatePipe } from 'src/app/core/pipes/translate.pipe';

@NgModule({
  declarations: [TranslatePipe],
  exports: [TranslatePipe]
})
export class I18nModule {}
