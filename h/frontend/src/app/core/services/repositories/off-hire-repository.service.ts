import { VoyageKey } from 'src/app/core/models/resources/IVoyage';
import { OffHireKey, IOffHire, IOffHireDTO, IOffHireCreate } from './../../models/resources/IOffHire';
import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';


export interface IOffHireRepoParams extends RepoParams<IOffHire> {
  queryParams?: {
    voyageId: VoyageKey;
  };
}

@Injectable({
  providedIn: 'root'
})
export class OffHireRepositoryService extends AbstractRepository<
  IOffHire,
  IOffHireDTO,
  IOffHireCreate,
  IOffHireRepoParams,
  OffHireKey
>  {
  protected resourceType = 'OffHire';
  protected sortKey = 'from';
  protected sortAsce = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/off-hires';

  protected postProcess = (item: IOffHireDTO): IOffHire => item;
}
