import { IResource } from './IResource';
import { OPEX_TYPES } from '../enums/opex-types';
import { IWithLabels } from './IWithLabels';
import { IWording } from './IWording';
import { IWithLabel } from './IWithLabel';
import { ACCOUNTING_RELATION_CATEGORY } from '../enums/accounting-relation-category';

export type CostTypeKey = number;

interface ISharedProps extends IWithLabels {
  no: number;
  accountingRelation: ACCOUNTING_RELATION_CATEGORY;
  remark?: string;
  isOpex: boolean;
  opexType?: OPEX_TYPES;
  isActive: boolean;
}

export interface ICostTypeCreate extends ISharedProps { }

export interface ICostTypeBase extends ISharedProps, IResource<CostTypeKey> { }

export interface ICostTypeDTO extends ICostTypeBase { }

export interface ICostType extends ICostTypeDTO, IWithLabel { }

export interface ICostTypeFull extends ICostType { }

export interface ICostTypeRow extends ICostTypeFull {
  opexTypeName: IWording;
  accountingRelationName: IWording;
}
