import { CompanyKey } from 'src/app/core/models/resources/ICompany';

export type TrusteeContributionsMapKey = string;

export class TrusteeContributionsMap<
  PK extends CompanyKey,
  TPK extends CompanyKey,
  T
  > {
  private map: Map<TrusteeContributionsMapKey, T>
    = new Map<TrusteeContributionsMapKey, T>();

  private getMapKey = (
    partnerId: CompanyKey,
    shareholderId: CompanyKey,
  ): TrusteeContributionsMapKey => {
    return `${partnerId}-${shareholderId}`;
  }

  public set(
    partnerId: PK,
    shareholderId: TPK,
    contributions: T,
  ): void {
    const mapKey: string = this.getMapKey(partnerId, shareholderId);

    this.map.set(mapKey, contributions);
  }

  public get(
    partnerId: PK,
    shareholderId: TPK,
  ): T {
    const mapKey: string = this.getMapKey(partnerId, shareholderId);

    return this.map.get(mapKey);
  }

  public values(): IterableIterator<T> {
    return this.map.values();
  }
}
