import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MeContextMenuI } from '@shared/me-components/me-context-menu/me-context-menu-inteface';

@Component({
  selector: 'me-table-context-menu',
  templateUrl: './table-context-menu.component.html',
  styleUrls: ['./table-context-menu.component.scss']
})
export class TableContextMenuComponent implements OnInit {
  @Input() data!: MeContextMenuI;
  @Output() tableDataChangeEvent = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  customEvent(payload: string) {
    this.tableDataChangeEvent.emit(payload);
  }
}
