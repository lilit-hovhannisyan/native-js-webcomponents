import { Pipe, PipeTransform } from '@angular/core';
import { getOperationName } from '../../core/models/Operations';

/**
 * This pipe returns the Bitmask ACL Operation name, which
 * can be one of the following values:
 * 1. CREATE
 * 2. READ
 * 3. UPDATE
 * 4. DELETE
 */
@Pipe({
  name: 'bitmaskName'
})
export class BitmaskNamePipe implements PipeTransform {
  public transform(value: number, args?: any): string {
    return getOperationName(value);
  }
}
