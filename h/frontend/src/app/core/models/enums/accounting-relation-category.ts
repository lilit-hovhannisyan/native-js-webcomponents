import { ISelectOption } from '../misc/selectOption';
import { wording } from '../../constants/wording/wording';

/**
 * Accounting relation category supposed to be
 * conceptually another entity, then ACCOUNTING_RELATIONS enum is
 * thus new enum is created
 */
export enum ACCOUNTING_RELATION_CATEGORY {
  PROFITANDLOSSEARNINGS = 1,
  PROFITANDLOSSEXPENSES = 2,
}

export const accountingRelationCategoryOptions: ISelectOption[] = [
  { value: ACCOUNTING_RELATION_CATEGORY.PROFITANDLOSSEARNINGS, displayLabel: wording.accounting.earnings },
  { value: ACCOUNTING_RELATION_CATEGORY.PROFITANDLOSSEXPENSES, displayLabel: wording.accounting.costs },
];
