import { VESSEL_TYPE } from './enums/vessel-type';
import { VesselKey } from './resources/IVessel';
import { VesselAccountingKey } from './resources/IVesselAccounting';
import { RegistrationTypes, VesselRegisterKey } from './resources/IVesselRegister';
import { IVesselSetting } from './resources/IVesselSetting';
import { IWording } from './resources/IWording';

export interface IRegistrationFleet {
  vesselId: VesselKey;
  vesselAccountingId: VesselAccountingKey;
  vesselRegisterId: VesselRegisterKey;
  vesselName: string;
  charterName: string;
  imo: number;
  vesselType: VESSEL_TYPE;
  vesselTypeName: IWording;
  client: string;
  comments: string;
  flagPath: string;
  periodFrom: string; // date
  periodUntil: string; // date
  typeOfRegistration: RegistrationTypes;
  typeOfRegistrationName: IWording;
  ssrNo: number;
  callSign: string;
  officialNo: string;
  registeredOwner: string;
  bbCharterer: string;
  settings: IVesselSetting[];
  doesPeriodStartWithinSixtyDays: boolean;
  nextPeriodStartsWording: IWording;
}
