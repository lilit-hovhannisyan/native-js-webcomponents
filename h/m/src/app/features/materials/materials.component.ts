import { AfterViewInit, Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { SubSink } from 'subsink';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, finalize, skip } from 'rxjs/operators';

import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { AuthStoreService } from '@core-services/auth-store.service';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { GlobalService } from '@core-services/global.service';

import { BaseLoggerComponent } from '@features/tracking-system';
import { CollectionsStoreService } from '@features/collections/collections-store.service';
import { CollectionsWebService } from '@features/collections/collections.web-service';

import { CollectionModel } from '@shared/models/collection/collection.model';
import { DMMOAListAttributeModel } from '@shared/models/attribute/attribute.model';
import { DomainModel } from '@shared/models/domain/domain.model';
import {
  MARKETPLACE_BASE_API_URL,
  MATERIAL_ATTRIBUTE_UOM,
  ME_DOMAIN_APPLICATION_COLLECTION,
  ME_DOMAIN_APPLICATION_MATERIAL
} from '@shared/constants';
import { MaterialModel } from '@shared/models/materials/material-model';
import { MaterialsStoreService } from '@shared/services/materials-store.service';
import { MaterialsWebService } from '@shared/services/materials.web-service';
import { MeAdvancedFilterI } from '@shared/me-components/me-advanced-filter/me-advanced-filter.interface';
import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';
import { MeBasicAlertI } from '@shared/me-components/me-basic-alert/me-basic-alert.interface';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeEmptyContentI } from '@shared/me-components/me-empty-content/me-empty-content-interface';
import { MeMaterialCardI } from '@shared-components/me-material-card/me-material-card.interface';
import { MeMaterialTypeCardI } from '@shared/me-components/me-material-type-card/me-material-type-card.interface';
import { MeMultiSelectInputAttribute } from '@shared/models/attribute/me-multi-select-input-attribute.model';
import { MeMultiSelectInputComponent } from '@shared/me-components/me-attribute/me-multi-select-input/me-multi-select-input.component';
import { MeNewCollectionCardI } from '@shared/me-components/me-new-collection-card/me-new-collection-card.interface';
import { MePermissionTypes } from '@shared/me-permission-types';
import { MePopupSelectI } from '@shared/me-components/me-popup-select/me-popup-select.interface';
import { MePopupSelectService } from '@shared/me-components/me-popup-select/me-popup-select.service';
import { MePopupStatusChangeI } from '@shared/me-components/me-popup-status-change/me-popup-status-change.interface';
import { MePopupStatusChangeService } from '@shared/me-components/me-popup-status-change/me-popup-status-change.service';
import { MeSearchBarI } from '@shared/me-components/me-search-bar/me-search-bar.interface';
import { MeSearchDirectionTypes, MeSearchSortByTypes } from '@shared/me-search-sort-by-types';
import { MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { PropertiesModel } from '@shared/models/attribute/attribute.properties.model';
import { QuickSearchModel } from '@shared/models/search/quick-search.model';
import { getOid, getVisibilityObject } from '@shared/utils';
import { MeIntroductionMessageI } from '@shared/me-components/me-introduction-message/me-introduction-message.interface';
import { MeAttributeTypes } from '@shared/me-attribute-types';
import { AttributesService } from '@shared/services/attributes.service';
import { KeyValue } from '@angular/common';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-materials',
  templateUrl: './materials.component.html',
  styleUrls: ['./materials.component.scss'],
  providers: [
    MaterialsStoreService,
    MaterialsWebService,
    MePopupSelectService,
    CollectionsStoreService,
    CollectionsWebService,
    MePopupStatusChangeService,
    AttributesService
  ]
})
export class MaterialsComponent extends BaseLoggerComponent implements OnInit, AfterViewInit, OnDestroy {
  componentVersion: string = '0.0.1';

  private subs = new SubSink();

  gridItemsInUse: 'collections' | 'materials' = 'materials';
  materialCRUD: boolean = false;
  getCardOid = getOid;
  materialCardItems: MeMaterialCardI[] = [];
  domain: DomainModel = new DomainModel();
  selectionEnabled: boolean = false;
  areMaterialsLoading: boolean = true;
  searchKeyword: string | undefined = '';
  i18nStrings: MeTranslationI = {};

  private readonly NUMBER_OF_ITEMS_ON_PAGE = 50;

  @ViewChild('createButton', { read: TemplateRef }) createButton!: TemplateRef<Component>;
  emptyContent!: MeEmptyContentI;
  isSearch: boolean = false;
  trackForBottomReach: boolean = false;
  searchBarData!: MeSearchBarI;
  sortByValue: string = '';
  sortByDirection: '' | 'ASC' | 'DESC' = '';
  filterDelay: number = 500;
  filterChanged: Subject<void> = new Subject();
  private selectedMap: Map<string, void> = new Map();

  // Filter Properties
  filterActivated: Subject<void> = new Subject();
  filterDelayTime: number = 500;
  advancedFilter!: MeAdvancedFilterI;
  materialTypeCards: MeMaterialTypeCardI[] = [];
  selectedFilterDomainName: string = ME_DOMAIN_APPLICATION_MATERIAL;
  materialStatesAttribute!: MeAttributeItem;
  selectedFilterMaterialStates: string[] = [];

  // Select collections popup variables
  @ViewChild('collectionsPopupGrid', { read: TemplateRef }) collectionsGridRef!: TemplateRef<Component>;
  collectionsPopupSelect!: MePopupSelectI;
  selectedMaterialOid!: string;

  collectionsEmptyContent!: MeEmptyContentI;
  collectionsIsSearch: boolean = false;
  collectionCardItems: MeNewCollectionCardI[] = [];
  areCollectionsLoading: boolean = true;
  collectionsSearchKeyword: string | undefined = '';

  collectionsTrackForBottomReach: boolean = false;
  collectionsSearchBarData!: MeSearchBarI;
  private collectionsSectedMap: Map<string, void> = new Map();

  introMsgData!: MeIntroductionMessageI;

  private _changeStatusPopupSubscription!: Subscription;

  isVisibilityEdit: boolean = false;

  constructor(
    injector: Injector,
    private materialsWebService: MaterialsWebService,
    private definitionsStoreService: DefinitionsStoreService,
    private materialsStoreService: MaterialsStoreService,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private router: Router,
    private translate: MeTranslationService,
    private basicAlertService: MeBasicAlertService,
    private sweetAlertService: MeSweetAlertService,
    private popupSelectService: MePopupSelectService,
    private collectionsStoreService: CollectionsStoreService,
    private collectionsWebService: CollectionsWebService,
    private globalService: GlobalService,
    private authStoreService: AuthStoreService,
    private popupStatusChangeService: MePopupStatusChangeService,
    private attributesService: AttributesService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    // subscribe to translations
    this.subs.sink = this.translate.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;
    });

    this.searchBarData = {
      filterEnabled: true,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: '',
        minCharacters: 1
      },
      selectButton: { enabled: false, placeholder: this.i18nStrings.select },
      operationButtons: [
        {
          button: {
            type: 'regular',
            color: 'link'
          },
          iFeatherName: 'trash',
          clickEventName: 'delete'
        }
      ],
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'azAlphabetically', value: this.i18nStrings.azAlphabetically },
          { key: 'zaAlphabetically', value: this.i18nStrings.zaAlphabetically },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      }
    };

    this.advancedFilter = {
      active: false,
      icon: 'filter',
      resetMessage: this.i18nStrings.resetAllFilters
    };

    this.emptyContent = {
      title: this.i18nStrings.createYourFirstMaterial,
      comment: this.i18nStrings.addYourFirstMaterialDescription,
      buttonText: this.i18nStrings.add,
      emptyIconName: 'empty-materials'
    };

    this.collectionsEmptyContent = {
      title: this.i18nStrings.createYourFirstCollection,
      comment: this.i18nStrings.addYourFirstCollectionDescription,
      buttonText: this.i18nStrings.add,
      emptyIconName: 'empty-collections'
    };

    this.introMsgData = {
      content:
        'Materials are the products that your company produces or makes available. Create, edit, and configure the visibility of your materials.',
      buttonText: this.i18nStrings.learnMore
    };

    this.globalService.activateLoader();
    this.subs.sink = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.domain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_MATERIAL);
        this.handleSearch('');
        this.prepareMaterialCardData();
        this.prepareAdvancedFilter();
      }
    });

    this.subs.sink = this.filterChanged.pipe(debounceTime(this.filterDelay)).subscribe(() => {
      this.handleSearch(this.searchKeyword || '');
    });

    this.isVisibilityEdit = this.authStoreService.isAllowed(MePermissionTypes.STATE_MATERIAL_CHANGE);
  }

  ngAfterViewInit(): void {
    this.subs.sink = this.authStoreService.permissions$.subscribe((permissions) => {
      if (permissions) {
        this.materialCRUD = this.authStoreService.isAllowed(MePermissionTypes.MATERIAL_ANY);
        if (this.materialCRUD) {
          this.breadcrumbSupportService.breadcrumbActions = [
            {
              button: {
                id: '1',
                data: { color: 'primary', type: 'transparent' },
                text: this.i18nStrings.create,
                content: this.createButton
              },
              callBack: () => {
                this.router.navigate(['materials/create']);
              }
            }
          ];
        }
      }
    });
  }

  btnEventHandler(): void {
    window.open('https://5883447.hs-sites.com/knowledge/materials-suppliers', '_blank');
  }

  toggleAdvancedFilter(): void {
    this.advancedFilter = {
      ...this.advancedFilter,
      active: !this.advancedFilter.active
    };
    this.searchBarData = {
      ...this.searchBarData,
      filterEnabled: !this.searchBarData.filterEnabled
    };
  }

  prepareMaterialCardData(): void {
    this.subs.sink = this.materialsStoreService.materialEntities$.subscribe((data: MaterialModel[] | null) => {
      if (data) {
        this.materialCardItems = data.map((material: MaterialModel) => {
          let unitOfMeasure: string = '';
          const domain: DomainModel = this.definitionsStoreService.getDomain(material.domainName);
          if (material.attributes && material.attributes.hasOwnProperty(MATERIAL_ATTRIBUTE_UOM)) {
            unitOfMeasure = this.attributesService.getEnumDisplayNames(
              {
                dataTypeClass: MeAttributeTypes.CHOICE,
                displayName: '',
                internalName: MATERIAL_ATTRIBUTE_UOM,
                value: material.attributes.unitOfMeasure,
                convertedValue: ''
              },
              domain
            );
          }
          return {
            contextMenuButton: { type: 'regular', color: 'link', rounded: true },
            contextMenuItems: this.prepareMaterialContextmenu(),
            materialName: material.attributes.name,
            materialPrice: material.attributes.price,
            materialUOM: unitOfMeasure || material.attributes.unitOfMeasure,
            oid: material.oid,
            selected: this.selectedMap.has(material.oid),
            selectionEnabled: this.selectionEnabled,
            supplierOid: material.ownerOID,
            thumbnail: material.attributes.thumbnail ? `${MARKETPLACE_BASE_API_URL}/files/download/${material.attributes.thumbnail}` : '',
            visibility: getVisibilityObject(material.state, this.domain.states.find((ms) => ms.internalName === material.state)?.stateType),
            state: material.state
          };
        });
      }
    });
  }

  prepareMaterialContextmenu(): KeyValue<string, string>[] {
    if (this.materialCRUD) {
      const menuItems = [
        {
          key: 'view',
          value: this.i18nStrings.view
        },
        {
          key: 'edit',
          value: this.i18nStrings.edit
        },
        {
          key: 'duplicate',
          value: this.i18nStrings.duplicate
        },
        {
          key: 'delete',
          value: this.i18nStrings.delete
        }
      ];

      if (this.isVisibilityEdit) {
        menuItems.splice(3, 0, { key: 'visibility', value: this.i18nStrings.visibility });
      }

      return menuItems;
    }
    return [];
  }

  prepareAdvancedFilter(): void {
    this.subs.sink = this.filterActivated.pipe(debounceTime(this.filterDelayTime)).subscribe(() => {
      this.handleSearch(this.searchKeyword || '');
    });

    this.materialTypeCards = this.domain.descendants.map((domain: DomainModel) => {
      return {
        title: domain.displayName,
        type: domain.internalName,
        icon: domain.icon,
        selected: false
      };
    });

    const dmMaterialStatusAttribute: DMMOAListAttributeModel = {
      constraints: [],
      dataTypeClass: 'moa',
      displayName: this.i18nStrings.visibility,
      internalName: 'materialStates',
      properties: new PropertiesModel(),
      required: false,
      optRequired: false,
      value: [],
      values: this.domain.states.map((state) => {
        return {
          displayName: state.displayName,
          value: state.internalName
        };
      }),
      disabled: false,
      unique: false
    };

    const meMaterialStatusAttribute: MeMultiSelectInputAttribute = new MeMultiSelectInputAttribute();
    meMaterialStatusAttribute.data = dmMaterialStatusAttribute;
    meMaterialStatusAttribute.form = new FormGroup({
      [dmMaterialStatusAttribute.internalName]: new FormControl(dmMaterialStatusAttribute.value)
    });

    this.subs.sink = meMaterialStatusAttribute.form.valueChanges.pipe(skip(1)).subscribe((value) => {
      this.selectedFilterMaterialStates = value.materialStates;
      this.filterActivated.next();
    });

    this.materialStatesAttribute = new MeAttributeItem(MeMultiSelectInputComponent, meMaterialStatusAttribute);
  }

  resetFilters(): void {
    this.selectedFilterMaterialStates = [];
    this.materialStatesAttribute.data.form.controls.materialStates.setValue([]);
    this.selectedFilterDomainName = ME_DOMAIN_APPLICATION_MATERIAL;
    this.materialTypeCards = this.materialTypeCards.map((typeCard) => {
      return {
        ...typeCard,
        selected: false
      };
    });
    this.filterActivated.next();
  }

  trackByType(_index: number, card: MeMaterialTypeCardI): string {
    return card.type;
  }

  handleAddButtonClick(): void {
    if (this.gridItemsInUse === 'collections') {
      this.popupSelectService.closePopup();
    }
    this.router.navigate([this.gridItemsInUse, 'create']);
  }

  bottomReachedHandler(): void {
    if (this.gridItemsInUse === 'materials') {
      if (this.trackForBottomReach) {
        this.handleSearch(this.searchKeyword || '', this.materialsStoreService.materialNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
      }
    } else {
      if (this.collectionsTrackForBottomReach) {
        if (this.collectionsIsSearch) {
          this.collectionsHandleSearch(
            this.collectionsSearchKeyword || '',
            this.collectionsStoreService.collectionNextId,
            this.NUMBER_OF_ITEMS_ON_PAGE,
            true
          );
        } else {
          this.getCollections(this.collectionsStoreService.collectionNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
        }
      }
    }
  }

  private handleMaterialsResponse(response: ArrayResponseI<MaterialModel>, append: boolean = false, isSearch: boolean = false): void {
    this.materialsStoreService.materialNextId = response.nextID;
    if (append) {
      this.materialsStoreService.appendMaterialEntities(response.entities);
    } else {
      this.materialsStoreService.materialEntities = response.entities;
      this.isSearch = isSearch;
      this.trackForBottomReach = true;
      this.selectedMap.clear();
    }

    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.trackForBottomReach = false;
    }
  }

  handleSearch(searchValue: string, _skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.searchKeyword = searchValue;
    this.searchBarData = {
      ...this.searchBarData,
      search: { placeholder: this.i18nStrings.searchFor, keyword: this.searchKeyword as string }
    };

    const searchFilter: QuickSearchModel = {
      domain: this.selectedFilterDomainName,
      criteriaQuick: this.searchKeyword.trim() || undefined,
      states: this.selectedFilterMaterialStates.length ? this.selectedFilterMaterialStates : undefined,
      orderByAttr: this.sortByValue.trim() || undefined,
      orderByDir: this.sortByDirection.trim() || undefined
    };

    this.areMaterialsLoading = true;
    this.globalService.activateLoader();
    this.subs.sink = this.materialsWebService
      .searchMaterials(searchFilter, _skip, top)
      .pipe(
        finalize(() => {
          this.areMaterialsLoading = false;
          this.globalService.deactivateLoader();
        })
      )
      .subscribe((response: ArrayResponseI<MaterialModel>) => {
        this.handleMaterialsResponse(response, append, true);
      });
  }

  handleToggleSelectEvent(): void {
    this.selectionEnabled = !this.selectionEnabled;
    this.searchBarData = {
      ...this.searchBarData,
      selectButton: {
        ...this.searchBarData.selectButton,
        enabled: this.selectionEnabled,
        placeholder: this.selectionEnabled ? this.i18nStrings.cancel : this.i18nStrings.select
      }
    };
    this.selectedMap.clear();
    this.materialsStoreService.materialEntities = [...this.materialsStoreService.materialEntities];
  }

  // Used in both view
  customEvent(eventName: string, payload?: string, item?: MeMaterialCardI): void {
    if (eventName === 'contextMenuItemClick') {
      switch (payload) {
        case 'view':
          this.router.navigate([this.gridItemsInUse, 'view', item?.oid]);
          if (this.gridItemsInUse === 'materials') {
            this.popupSelectService.closePopup();
          }
          break;
        case 'edit':
          this.router.navigate([this.gridItemsInUse, 'edit', item?.oid]);
          if (this.gridItemsInUse === 'materials') {
            this.popupSelectService.closePopup();
          }
          break;
        case 'visibility':
          if (item && item.state) {
            // Creating of popup data
            const popup: MePopupStatusChangeI = {
              title: this.i18nStrings.setMaterialVisibility,
              cancelButtonPlaceholder: this.i18nStrings.cancel,
              changeButtonPlaceholder: this.i18nStrings.saveChanges
            };
            // open popup
            this._changeStatusPopupSubscription = this.popupStatusChangeService.openDialog(popup).subscribe((ev) => {
              if (ev.eventName === 'change' && ev.payload && ev.payload !== item.state) {
                this.changeMaterialState(item.oid, ev.payload);
              }
              this._changeStatusPopupSubscription.unsubscribe();
            });

            // get States and add data to popup
            this.subs.sink = this.materialsWebService.getMaterialStates(item.oid).subscribe((states) => {
              const newPopup = {
                ...popup,
                currentKeyValue: item.state,
                radioButtons: states.entities.map((state) => {
                  const displName = this.domain.states.find((s) => s.internalName === state.internalName);
                  return {
                    key: state.internalName,
                    value: displName ? `${displName.displayName} - ${displName.description}` : state.internalName,
                    disabled: !state.enabled
                  };
                })
              };
              this.popupStatusChangeService.refreshDialogData(newPopup);
            });
          }
          break;
        case 'delete':
          if (item?.oid) {
            const materialOIDS: string[] = [item.oid];
            this.deleteMaterial(materialOIDS);
          }
          break;
        // case 'addToCollection':
        //   if (item?.oid) {
        // TODO: BACKEND NEED TO PROVIDE SERVICE
        //     this.showAddToCollectionsPopup(item.oid);
        //   }
        //   break;
        case 'duplicate':
          if (item) {
            this.handleDuplicateMaterial(item);
          }
          break;
        default:
          break;
      }
    }
  }

  showBasicErrorAlert(error: ErrorResponseI): void {
    const basicAlertData: MeBasicAlertI = {
      mode: 'error',
      title: error.error.title,
      content: error.error.details
    };
    this.basicAlertService.openBasicAlert(basicAlertData);
  }

  handleItemClick(card: MeMaterialCardI | MeNewCollectionCardI, index: number): void {
    if (this.selectionEnabled) {
      this.toggleCardItemSelection(index);
    } else {
      this.router.navigate([`materials/view/${card.oid}`]);
    }
  }

  toggleCardItemSelection(index: number): void {
    const items = this.gridItemsInUse === 'materials' ? this.materialCardItems : this.collectionCardItems;
    const itemSelectionMap = this.gridItemsInUse === 'materials' ? this.selectedMap : this.collectionsSectedMap;
    if (itemSelectionMap.has(items[index].oid)) {
      items[index] = { ...items[index], selected: false };
      itemSelectionMap.delete(items[index].oid);
    } else {
      items[index] = { ...items[index], selected: true };
      itemSelectionMap.set(items[index].oid);
    }
    if (this.gridItemsInUse === 'collections') {
      this.updateCollectionsPopup({ selectedCount: itemSelectionMap.size });
    }
  }

  handleSortByEvent(sortByValue: string): void {
    if (this.gridItemsInUse === 'materials') {
      if (this.searchBarData.sort) {
        const selectedIndex: number = this.searchBarData.sort?.options.findIndex((x) => x.key === sortByValue) || 0;
        this.searchBarData = {
          ...this.searchBarData,
          sort: { ...this.searchBarData.sort, selectedIndex }
        };
      }
    } else if (this.gridItemsInUse === 'collections') {
      if (this.collectionsSearchBarData.sort) {
        const selectedIndex: number = this.collectionsSearchBarData.sort?.options.findIndex((x) => x.key === sortByValue) || 0;
        this.collectionsSearchBarData = {
          ...this.collectionsSearchBarData,
          sort: { ...this.collectionsSearchBarData.sort, selectedIndex }
        };
      }
    }

    switch (sortByValue) {
      case 'azAlphabetically':
        this.sortByValue = MeSearchSortByTypes.name;
        this.sortByDirection = MeSearchDirectionTypes.ascending;
        break;
      case 'zaAlphabetically':
        this.sortByValue = MeSearchSortByTypes.name;
        this.sortByDirection = MeSearchDirectionTypes.descending;
        break;
      case 'newest':
        this.sortByValue = MeSearchSortByTypes.created;
        this.sortByDirection = MeSearchDirectionTypes.descending;
        break;
      case 'oldest':
        this.sortByValue = MeSearchSortByTypes.created;
        this.sortByDirection = MeSearchDirectionTypes.ascending;
        break;
      default:
        this.sortByValue = '';
        this.sortByDirection = '';
        break;
    }

    this.filterChanged.next();
  }

  handleAdvancedFilterEvent(event: { eventName: string }): void {
    if (event.eventName === 'close') {
      this.toggleAdvancedFilter();
    } else if (event.eventName === 'clear') {
      this.resetFilters();
    }
  }

  handleToggleTypeSelection(index: number): void {
    if (this.selectedFilterDomainName === this.materialTypeCards[index].type) {
      this.selectedFilterDomainName = ME_DOMAIN_APPLICATION_MATERIAL;
    } else {
      this.selectedFilterDomainName = this.materialTypeCards[index].type;
    }
    this.materialTypeCards = this.materialTypeCards.map((c) => {
      return {
        ...c,
        selected: c.type === this.selectedFilterDomainName
      };
    });
    this.filterActivated.next();
  }

  handleSearchBarEvents(event: { eventName: string; payload?: string | boolean }): void {
    switch (event.eventName) {
      case 'toggleFilter':
        this.toggleAdvancedFilter();
        break;
      case 'edit':
        // TODO
        break;
      case 'delete':
        const materialOIDs: string[] = [...this.selectedMap.keys()];
        if (materialOIDs.length > 0) {
          this.deleteMaterial(materialOIDs);
        }
        break;
      case 'copy':
        // TODO
        break;
      case 'more':
        // TODO
        break;
      case 'download':
        // TODO
        break;
    }
  }

  private deleteMaterial(materialOIDs: string[]): void {
    this.subs.sink = this.sweetAlertService.getDataBackFromSweetAlert().subscribe((data) => {
      if (data && data.confirmed) {
        this.subs.sink = this.materialsWebService.deleteMaterials(materialOIDs).subscribe(() => {
          this.materialsStoreService.removeMaterialEntities(materialOIDs);
          this.basicAlertService.openBasicAlert({
            mode: 'success',
            title: this.i18nStrings.materialDeleted,
            content: this.i18nStrings.materialHaveBeenSuccessfullyDeleted
          });
          this.prepareMaterialCardData();
          this.selectedMap.clear();
        });
      }
    });

    this.sweetAlertService.openMeSweetAlert({
      mode: 'danger',
      icon: 'alert-triangle',
      type: {
        name: MeSweetAlertTypeEnum.submit,
        buttons: {
          submit: this.i18nStrings.delete,
          cancel: this.i18nStrings.cancel
        }
      },
      title: this.i18nStrings.deleteMaterial,
      message: this.i18nStrings.areYouSureYouWantToDeleteTheMaterial
    });
  }

  // Collections popup grid handler section
  showAddToCollectionsPopup(collectionOid: string): void {
    this.selectedMaterialOid = collectionOid;
    this.gridItemsInUse = 'collections';
    this.collectionsPopupSelect = {
      title: this.i18nStrings.addToCollections,
      selectedCount: this.collectionsSectedMap.size,
      showOnlySelected: false,
      showActions: true
    };
    this.getCollections();
    this.prepareCollectionCardData();

    this.collectionsSearchBarData = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: ''
      },
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'name', value: this.i18nStrings.name },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      },
      forceShowButton: true
    };

    this.subs.sink = this.popupSelectService
      .openPopupSelect(this.collectionsPopupSelect, this.collectionsGridRef)
      .subscribe((event: string) => {
        switch (event) {
          case 'close':
            this.resetAfterPopupClose();
            break;
          case 'clearSelection':
            this.collectionsSectedMap.clear();
            this.getCollections();
            this.updateCollectionsPopup({
              selectedCount: this.collectionsSectedMap.size
            });
            break;
          case 'showSelected':
            this.collectionCardItems = this.collectionCardItems.filter((item: MeNewCollectionCardI) => item.selected === true);
            this.updateCollectionsPopup({
              showOnlySelected: true
            });
            break;
          case 'showAll':
            this.handleSearch(this.searchKeyword || '');
            this.updateCollectionsPopup({
              showOnlySelected: false
            });
            break;
          case 'addItems':
            this.addMaterialToCollections();
            this.resetAfterPopupClose();
            break;
          default:
            break;
        }
      });
  }

  updateCollectionsPopup(collectionsPopup: Partial<MePopupSelectI>): void {
    this.collectionsPopupSelect = { ...this.collectionsPopupSelect, ...collectionsPopup };
    this.popupSelectService.pushDataModelToPopup(this.collectionsPopupSelect);
  }

  resetAfterPopupClose(): void {
    this.gridItemsInUse = 'materials';
    this.selectedMaterialOid = '';
    this.collectionsSectedMap.clear();
  }

  addMaterialToCollections(): void {
    // TODO: IMPLEMENT ADD TO MULTIPLE COLLECTIONS
    // WHEN BACKEND PROVIDE SERVICE FOR IT
    // const joinObject: { [key: string]: Observable<void> } = {};
    // this._collectionsSectedMap.forEach((_value: void, key: string) => {
    //   joinObject[key] = this.collectionsWebService.addMaterialsToMeCollection(key, [this.selectedMaterialOid]);
    // });
    // forkJoin(joinObject).subscribe(
    //   () => {
    //     this.getAllMaterials();
    //     this.basicAlertService.openBasicAlert({
    //       mode: 'success',
    //       title: 'Materials Added',
    //       content: 'The materials have been successfully added.'
    //     });
    //   },
    //   (error: ErrorResponseI) => {
    //     this.basicAlertService.openBasicAlert({
    //       mode: 'error',
    //       title: error.error.title,
    //       content: error.error.details
    //     });
    //   }
    // );
  }

  prepareCollectionCardData(): void {
    this.subs.sink = this.collectionsStoreService.collectionEntities$.subscribe((data: CollectionModel[] | null) => {
      if (data) {
        this.collectionCardItems = data.map((collection: CollectionModel) => {
          return {
            selectionEnabled: this.selectionEnabled,
            selected: this.collectionsSectedMap.has(collection.oid),
            oid: collection.oid,
            collectionName: collection.attributes.name,
            numberOfMaterials: collection.numberOfMaterials,
            numberOfCustomers: collection.numberOfCustomers,
            numberOfTeams: collection.numberOfTeams,
            contextMenu: {
              contextMenuItems: [
                {
                  key: 'view',
                  value: this.i18nStrings.view
                },
                {
                  key: 'edit',
                  value: this.i18nStrings.edit
                }
              ]
            },
            thumbnail: collection.attributes.thumbnail
              ? `${MARKETPLACE_BASE_API_URL}/files/download/${collection.attributes.thumbnail}`
              : '',
            supplierName: collection.attributesExtended.ownerRef,
            supplierOid: collection.attributes.ownerRef
          };
        });
      }
    });
  }

  getCollections(_skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.areCollectionsLoading = true;
    this.subs.sink = this.collectionsWebService
      .getMeCollections(_skip, top)
      .pipe(
        finalize(() => {
          this.areCollectionsLoading = false;
        })
      )
      .subscribe((response: ArrayResponseI<CollectionModel>) => {
        this.handleCollectionsResponse(response, append, false);
      });
  }

  private handleCollectionsResponse(response: ArrayResponseI<CollectionModel>, append: boolean = false, isSearch: boolean = false): void {
    this.collectionsStoreService.collectionNextId = response.nextID;
    if (append) {
      this.collectionsStoreService.appendCollectionEntities(response.entities);
    } else {
      this.collectionsIsSearch = isSearch;
      if (this.collectionsIsSearch) {
        this.collectionsSectedMap.clear();
      }
      this.collectionsStoreService.collectionEntities = response.entities;
      this.collectionsTrackForBottomReach = true;
      this.updateCollectionsPopup({
        selectedCount: this.collectionsSectedMap.size
      });
    }
    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.collectionsTrackForBottomReach = false;
    }
  }

  collectionsHandleSearch(
    searchValue: string,
    _skip: number = 0,
    top: number = this.NUMBER_OF_ITEMS_ON_PAGE,
    append: boolean = false
  ): void {
    this.collectionsSearchKeyword = searchValue;
    this.collectionsSearchBarData = {
      ...this.collectionsSearchBarData,
      search: { placeholder: this.i18nStrings.searchFor, keyword: this.collectionsSearchKeyword as string }
    };

    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_COLLECTION,
      criteriaQuick: this.collectionsSearchKeyword.length ? this.collectionsSearchKeyword : undefined
    };

    this.areCollectionsLoading = true;
    this.subs.sink = this.collectionsWebService
      .searchCollections(searchFilter, _skip, top)
      .pipe(
        finalize(() => {
          this.areCollectionsLoading = false;
        })
      )
      .subscribe((response: ArrayResponseI<CollectionModel>) => {
        this.handleCollectionsResponse(response, append, true);
      });
  }

  changeMaterialState(materialOid: string, newStates: string): void {
    this.subs.sink = this.materialsWebService.changeMaterialState(materialOid, newStates).subscribe((material: MaterialModel) => {
      this.materialsStoreService.updateMaterialsState(material);
      this.basicAlertService.openBasicAlert({
        mode: 'success',
        title: this.i18nStrings.statusChanged,
        content: this.i18nStrings.materialStatusChanged
      });
    });
  }

  private handleDuplicateMaterial(materialCard: MeMaterialCardI): void {
    this.router.navigate(['materials', 'duplicate', materialCard.oid]);
  }

  ngOnDestroy(): object {
    if (this._changeStatusPopupSubscription) {
      this._changeStatusPopupSubscription.unsubscribe();
    }
    this.subs.unsubscribe();
    return super.ngOnDestroy();
  }
}
