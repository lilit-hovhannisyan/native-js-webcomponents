export interface EntitiesUpdateI {
  entities: { domainName: string; attributes: FormData | {} }[];
}
