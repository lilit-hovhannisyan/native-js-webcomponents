import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AuthLayoutComponent } from './auth-layout.component';
import { I18nModule } from '@shared/modules/i18n.module';
@NgModule({
  declarations: [AuthLayoutComponent],
  imports: [CommonModule, RouterModule, I18nModule]
})
export class AuthLayoutModule {}
