import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { IGridConfigDTO, IGridConfig, IGridConfigCreate } from '../models/resources/IGridConfig';
import { HttpGet } from 'src/app/core/models/http/http-get';
import { IDataResponseBody } from 'src/app/core/models/responses/IDataResponseBody';
import { HttpPut } from '../models/http/http-put';
import { NvGridConstants } from 'nv-grid';
import { IResourceCollection } from '../models/resources/IResourceCollection';
import { map, shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';

type CollectionResponse = HttpResponse<IDataResponseBody<IResourceCollection<IGridConfigCreate>>>;

@Injectable({
  providedIn: 'root'
})
export class GridConfigService {

  public baseUrl = '/api/user-account/grid-configs';

  constructor(private http: HttpClient) { }

  private preProcess(config: IGridConfig): IGridConfigCreate {
    return ({ ...config, id: config.gridName, _type: 'GridConfig' });
  }

  private postProcessList(configs: IGridConfigCreate[]): IGridConfig[] {
    return configs.map(config => {
      return ({
        gridName: config.gridName,
        rowHeight: config.rowHeight,
        pin: {
          pinActive: config.pin.pinActive,
          pinTill: config.pin.pinTill
        },
        columns: config.columns.map(column => ({
          key: column.key, width: column.width, hidden: column.hidden
        })),
        version: config.version
      });
    });
  }


  private fetchAll(): Observable<IGridConfig[]> {
    const request = new HttpGet(this.baseUrl);
    const response$ = this.http.request(request).pipe(
      map((response: CollectionResponse) => this.postProcessList(response.body.data.items)),
    );
    return response$;
  }

  public update(config: IGridConfig): void {
    const request = new HttpPut(`${this.baseUrl}/${config.gridName}`, this.preProcess(config));
    this.http.request(request).pipe(
      map((res: HttpResponse<IDataResponseBody<IGridConfigDTO>>) => res.body.data),
      shareReplay(1)
    ).subscribe();
  }

  public initGridConfigs() {
    this.fetchAll().subscribe(response => response.forEach(config =>
      localStorage.setItem(`${NvGridConstants.GRID_LOCAL_STORAGE_PREFIX_NAME}${config.gridName}`, JSON.stringify(config))));
  }
}
