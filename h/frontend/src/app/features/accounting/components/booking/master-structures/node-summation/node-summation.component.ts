import { Component, OnInit, Input } from '@angular/core';
import { IMasterStructureNodeTemplate, IMasterStructureNode, IMasterStructureNodeHashTable, MASTER_STRUCTURE_NODE_TYPE } from 'src/app/core/models/resources/IMasterStructureNode';
import { wording } from 'src/app/core/constants/wording/wording';
import { NzTreeNode } from 'ng-zorro-antd/tree';
import { NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'nv-node-summation',
  templateUrl: './node-summation.component.html',
  styleUrls: ['./node-summation.component.scss']
})
export class NodeSummationComponent implements OnInit {
  @Input() public nodes: IMasterStructureNodeTemplate[] = [];
  @Input() public selectedNode: NzTreeNode;

  public assignedNodes: IMasterStructureNode[] = [];
  public unassignedNodes: IMasterStructureNode[] = [];
  public wording = wording;
  public selectedNodes = {};
  public selectedNodesType: 'assigned' | 'unassigned';
  private lastSelectedNodeIndex: number;

  constructor(
    private modal: NzModalRef
  ) { }

  public ngOnInit(): void {
    this.setAvailableAndAssignedNodes();
  }

  private setAvailableAndAssignedNodes(): void {
    this.assignedNodes = this.nodes
      .filter(n => n.parentNodeId === this.selectedNode.origin.id);
    this.unassignedNodes = this.nodes
      .filter(node => !node.parentNodeId
        && node.id !== this.selectedNode.origin.id
        && (this.hasAssignedBookingAccount(node)
          || node.nodeType === MASTER_STRUCTURE_NODE_TYPE.SUMMATION)
      );
  }

  private hasAssignedBookingAccount(
    node: IMasterStructureNode | IMasterStructureNode[],
    hasAccount = false,
  ): boolean {
    let isEmptyNode: boolean = hasAccount;
    if (Array.isArray(node)) {
      for (let i = 0; i < node.length; i++) {
        isEmptyNode = this.hasAssignedBookingAccount(node[i], hasAccount) || hasAccount;
        if (isEmptyNode === true) {
          return isEmptyNode;
        }
      }
    } else if (node.bookingAccountId) {
      return true;
    }

    if (!Array.isArray(node) && node.childNodes?.length > 0) {
      isEmptyNode = this.hasAssignedBookingAccount(node.childNodes, hasAccount)
        || hasAccount;
    }
    return isEmptyNode;
  }

  public unassignNode(): void {
    const assignedNodes: IMasterStructureNode[] = this.assignedNodes
      .filter(n => this.selectedNodes[n.id]);
    this.assignedNodes = this.assignedNodes.filter(n => !this.selectedNodes[n.id]);
    this.unassignedNodes = [...this.unassignedNodes, ...assignedNodes];
    this.unselectNodes();
  }

  public assignNode(): void {
    const unassignedNodes: IMasterStructureNode[] = this.unassignedNodes
      .filter(n => this.selectedNodes[n.id]);
    this.unassignedNodes = this.unassignedNodes.filter(n => !this.selectedNodes[n.id]);
    this.assignedNodes = [...this.assignedNodes, ...unassignedNodes];
    this.unselectNodes();
  }

  private unselectNodes(): void {
    this.selectedNodes = {};
  }

  public selectNode(
    node: IMasterStructureNodeTemplate,
    nodesType: 'assigned' | 'unassigned',
    index: number,
    event: MouseEvent,
  ): void {
    if (nodesType !== this.selectedNodesType) {
      this.selectedNodes = {};
    }

    const nodes: IMasterStructureNode[] = this[`${nodesType}Nodes`];

    if (event.shiftKey) {
      const selectedNodes: IMasterStructureNode[] = Object.values(this.selectedNodes);
      if (selectedNodes.length > 0) {
        const firstSelectedIndex: number = this.lastSelectedNodeIndex > index
          ? index
          : this.lastSelectedNodeIndex;
        const lastSelectedIndex: number = this.lastSelectedNodeIndex > index
          ? this.lastSelectedNodeIndex
          : index;

        for (let i = firstSelectedIndex; i <= lastSelectedIndex; i++) {
          this.selectedNodes[nodes[i].id] = nodes[i];
        }
        this.lastSelectedNodeIndex = index;
        this.selectedNodesType = nodesType;
        return;
      }
    }
    this.selectedNodes[node.id] = this.selectedNodes[node.id] ? undefined : node;
    this.lastSelectedNodeIndex = index;
    this.selectedNodesType = nodesType;
  }

  public getAssignedNodesHashTable(): IMasterStructureNodeHashTable {
    const hashtable: IMasterStructureNodeHashTable = {};

    this.assignedNodes.forEach(n => {
      hashtable[n.id] = n as IMasterStructureNodeTemplate;
    });

    return hashtable;
  }

  public getUnassignedNodesHashTable(): IMasterStructureNodeHashTable {
    const hashtable: IMasterStructureNodeHashTable = {};

    this.unassignedNodes.forEach(n => {
      hashtable[n.id] = n as IMasterStructureNodeTemplate;
    });

    return hashtable;
  }

  public triggerCancel(): void {
    this.modal.triggerCancel();
  }

  public triggerOk(): void {
    if (this.assignedNodes.length > 0) {
      this.modal.triggerOk();
    }
  }
}
