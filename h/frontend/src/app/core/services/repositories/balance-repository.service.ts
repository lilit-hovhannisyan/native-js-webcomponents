import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IBalance, IBalanceDTO, IBalanceCreate, BalanceKey } from '../../models/resources/IBalance';


@Injectable({
  providedIn: 'root'
})
export class BalanceRepositoryService extends AbstractRepository<
IBalance,
IBalanceDTO,
IBalanceCreate,
RepoParams<IBalance>,
BalanceKey
> {
  protected resourceType = 'Balance';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(params: RepoParams<IBalance>): string {
    return '/api/balances';
  }

  public postProcess(item: IBalanceDTO): IBalance {
    return item;
  }

}
