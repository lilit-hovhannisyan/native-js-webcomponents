import { TTL_LONG } from './../../../shared/services/resource-tracker.service';
import { PortKey } from './../../models/resources/IPort';
import { Injectable } from '@angular/core';
import { IPort, IPortDTO, IPortCreate } from '../../models/resources/IPort';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class PortRepositoryService extends AbstractRepository<
  IPort,
  IPortDTO,
  IPortCreate,
  RepoParams<IPort>,
  PortKey
> {
  protected resourceType = 'Port';
  protected resourceTTL = TTL_LONG;
  protected useCachePerDefault = true;
  public sortAsce = true;
  public sortKey = 'shortName';
  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/ports/';
  }

  protected postProcess(item: IPortDTO): IPort { return item; }
}

