import { FormGroup } from '@angular/forms';
import { MeInputErrorI } from '../me-input-error/me-input-error.interface';

export interface MeTextAreaInputI {
  form: FormGroup;
  inputErrors: MeInputErrorI;
  internalName: string;
  displayName: string;
  required: boolean;
  optRequired: boolean;
  value: string;
  minLength: number;
  maxLength: number;
  disabled: boolean;
}
