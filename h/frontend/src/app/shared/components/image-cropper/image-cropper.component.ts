import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  ViewChild,
  ElementRef,
  HostListener, OnInit, Output, EventEmitter,
} from '@angular/core';

import { Subject } from 'rxjs';
import { ICropper } from 'src/app/core/models/ICropper';

@Component({
  selector: 'nv-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.scss']
})
export class ImageCropperComponent implements OnChanges, OnDestroy, OnInit {
  public imageFile: File;
  @Output() public cropped = new EventEmitter<Blob | null>();
  @Output() public cancel = new EventEmitter();
  @ViewChild('canvas', { static: true }) public canvas: ElementRef<HTMLCanvasElement>;
  @ViewChild('scaleSlider', { static: true }) public scaleSlider: ElementRef<any>;
  public mainX: number;
  public mainY: number;
  public lastXCoordinate: number;
  public lastYCoordinate: number;
  public scale: number;
  public scaleS = new Subject<number>();
  public scaleO = this.scaleS.asObservable().pipe();
  public frameLength: number;
  public frameOutLength: number;
  public canvasWidth: number;
  public canvasHeight: number;
  public zoomInputText: string;
  public zoomType: string;
  public zoomMin: number;
  public zoomMax: number;
  public zoomStep: number;
  public cropButtonText: string;
  public fillStyle: string;
  public image;
  public ctx: CanvasRenderingContext2D | null;
  public mouseDown = false;
  public defaultOptions: ICropper;

  @Input()
  public set optionsInp(opt: ICropper) {
    this.setDefaultOptions(opt);
    this.defaultOptions = opt;
  }

  public setDefaultOptions(opt: ICropper): void {
    ({
      imageFile: this.imageFile,
      mainX: this.mainX = 0,
      mainY: this.mainY = 0,
      lastXCoordinate: this.lastXCoordinate = 0,
      lastYCoordinate: this.lastYCoordinate = 0,
      scale: this.scale = 1,
      frameLength: this.frameLength = 192,
      frameOutLength: this.frameOutLength = 4,
      canvasWidth: this.canvasWidth = 200,
      canvasHeight: this.canvasHeight = 200,
      zoomInputText: this.zoomInputText = 'Zoom Image',
      zoomType: this.zoomType = 'range',
      zoomMin: this.zoomMin = 0,
      zoomMax: this.zoomMax = 2,
      zoomStep: this.zoomStep = 0.01,
      cropButtonText: this.cropButtonText = 'Crop Image',
      fillStyle: this.fillStyle = 'rgba(34, 34, 34, 0.6)'
    } = opt);
  }

  @HostListener('input', ['$event'])
  public onScale(value): void {
    this.scaleS.next(value.target.value);
  }

  public ngOnInit() {
    this.scaleO.subscribe(x => {
      this.updateScale(x);
    });
  }

  public ngOnChanges() {
    this.setDefaultOptions(this.defaultOptions);
    this.initImage();
  }

  public ngOnDestroy() {
    this.scaleS.unsubscribe();
  }

  public initImage(): void {
    this.image = new Image();
    this.image.src = URL.createObjectURL(this.imageFile);
    this.image.onload = this.onImageLoad;
  }

  public onImageLoad = (): void => {
    this.ctx = this.canvas.nativeElement.getContext('2d');
    this.checkImagesSize();
    this.drawImage(0, 0);
    this.ctx.canvas.onmousedown = this.onMouseDown;
    this.ctx.canvas.onmousemove = this.onMouseMove;
    document.onmouseup = this.onMouseUp;
  }

  public checkImagesSize(): void {
    let scale = 0;
    if (this.image.height > this.image.width) {
      scale = +(this.frameLength / this.image.width).toFixed(2) + this.zoomStep;
      this.mainY = (this.canvasHeight - this.image.height * scale) / 2;
    } else {
      scale = +(this.frameLength / this.image.height).toFixed(2) + this.zoomStep;
      this.mainX = (this.canvasWidth - this.image.width * scale) / 2;
    }
    this.scale = this.zoomMin = scale;
    this.scaleSlider.nativeElement.value = this.scale;
  }

  /**
   * Used some source from the link below
   * @see {@link https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage}
   * */
  public drawImage = (x: number, y: number): void => {
    const {width, height} = this.ctx.canvas;
    let {width: imageWidth, height: imageHeight} = this.image;
    ({imageWidth, imageHeight} = {imageWidth: imageWidth * this.scale, imageHeight: imageHeight * this.scale});
    this.ctx.clearRect(0, 0, width, height);
    this.mainX = this.mainX + (x - this.lastXCoordinate);
    this.mainY = this.mainY + (y - this.lastYCoordinate);
    this.lastXCoordinate = x;
    this.lastYCoordinate = y;

    this.doNotLeaveFrame(imageWidth, imageHeight, width, height);

    this.ctx.drawImage(this.image, this.mainX, this.mainY,
      imageWidth,
      imageHeight);
    this.drawCropContent();
  }

  public doNotLeaveFrame(imageWidth: number, imageHeight: number, width: number, height: number): void {
    if (this.mainX > this.frameOutLength) {
      this.mainX = this.frameOutLength;
    }
    if (this.mainY > this.frameOutLength) {
      this.mainY = this.frameOutLength;
    }
    if (this.mainX + imageWidth < width - this.frameOutLength) {
      this.mainX = width - imageWidth - this.frameOutLength;
    }
    if (this.mainY + imageHeight < height - this.frameOutLength) {
      this.mainY = height - imageHeight - this.frameOutLength;
    }
  }

  /**
   * Took advantage of
   * @see {@link drawCropContent}
   * @see {@link https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/rect
   *       @link https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/fillRect}
   * */
  public drawCropContent = (): void => {
    this.ctx.beginPath();
    this.ctx.rect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    this.ctx.moveTo(this.frameOutLength, this.frameOutLength);
    this.ctx.lineTo(this.frameOutLength, this.frameLength + this.frameOutLength);
    this.ctx.lineTo(this.frameOutLength + this.frameLength, this.frameOutLength + this.frameLength);
    this.ctx.lineTo(this.frameOutLength + this.frameLength, this.frameOutLength);
    this.ctx.closePath();
    this.ctx.fillStyle = this.fillStyle;
    this.ctx.fill();
  }

  public onMouseDown = (e): void => {
    e.preventDefault();
    const coordinates = this.transformCoordinates(e.clientX, e.clientY);
    this.mouseDown = true;
    this.lastXCoordinate = coordinates.x;
    this.lastYCoordinate = coordinates.y;
  }

  public onMouseMove = (e): void => {
    e.preventDefault();
    if (this.mouseDown) {
      const coordinates = this.transformCoordinates(e.clientX, e.clientY);
      this.drawImage(coordinates.x, coordinates.y);
    }
  }

  public onMouseUp = (e): void => {
    e.preventDefault();
    this.mouseDown = false;
  }

  public transformCoordinates = (x: number, y: number): {x: number, y: number} => {
    const canvas = this.ctx.canvas,
      box = canvas.getBoundingClientRect();
    return {
      x: x - box.left * (canvas.width / box.width),
      y: y - box.top * (canvas.height / box.height)
    };
  }

  public updateScale(e: number): void {
    this.scale = e;
    this.drawImage(this.lastXCoordinate, this.lastYCoordinate);
  }

  public cropImage(): void {
    let temp_ctx, temp_canvas;
    temp_canvas = document.createElement('canvas');
    temp_ctx = temp_canvas.getContext('2d');
    temp_canvas.width = this.frameLength;
    temp_canvas.height = this.frameLength;
    temp_ctx.drawImage(
      this.ctx.canvas,
      this.frameOutLength,
      this.frameOutLength,
      this.frameLength,
      this.frameLength,
      0,
      0,
      this.frameLength,
      this.frameLength
    );
    temp_canvas.toBlob((blob) => { this.cropped.emit(blob); });
  }
}
