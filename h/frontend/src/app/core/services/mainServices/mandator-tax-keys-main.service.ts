import { TaxKeyRepositoryService } from './../repositories/tax-key-repository.service';
import { Injectable } from '@angular/core';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { IMandatorTaxKey } from '../../models/resources/IMandatorTaxKey';
import { ITaxKey } from '../../models/resources/ITaxKeys';
import { MandatorTaxKeysRepositoryService } from '../repositories/mandator-tax-keys-repository.service';
import { BookingAccountRepositoryService } from '../repositories/booking-account-repository.service';
import { IBookingAccount } from '../../models/resources/IBookingAccount';
import { MandatorKey } from '../../models/resources/IMandator';
import { IWording } from '../../models/resources/IWording';

export interface IMandatorTaxKeyFull extends IMandatorTaxKey {
  taxKey: ITaxKey;
  bookingAccount: IBookingAccount;
}
export interface IMandatorTaxKeyRow extends IMandatorTaxKeyFull {
  accountLabel: IWording;
  accountNo: number;
  code: string;
  percentage: number;
  name: IWording;
}
@Injectable({
  providedIn: 'root'
})
export class MandatorTaxKeysMainService {

  constructor(
    private mandatorTaxKeyRepo: MandatorTaxKeysRepositoryService,
    private taxKeyRepo: TaxKeyRepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService,
  ) { }

  public fetchAll(params: { mandatorId: MandatorKey }): Observable<IMandatorTaxKeyFull[]> {
    return forkJoin([
      this.mandatorTaxKeyRepo.fetchAll({ queryParams: params }),
      this.taxKeyRepo.fetchAll({}),
      this.bookingAccountRepo.fetchAll({})
    ]).pipe(
      map(mandatorTaxKey => this.getMandatorTaxKeysFullList(...mandatorTaxKey))
    );
  }

  private getMandatorTaxKeysFullList(
    mandatorTaxKeys: IMandatorTaxKey[],
    taxKeys: ITaxKey[],
    accounts: IBookingAccount[]
  ): IMandatorTaxKeyFull[] {
    return mandatorTaxKeys.map(mandatorTaxKey => this.getMandatorTaxKeysFull(mandatorTaxKey, taxKeys, accounts));
  }

  private getMandatorTaxKeysFull(
    mandatorTaxKey: IMandatorTaxKey,
    taxKeys: ITaxKey[],
    accounts: IBookingAccount[]
  ): IMandatorTaxKeyFull {
    const taxKey = taxKeys.find(tk => tk.id === mandatorTaxKey.taxKeyId);
    const bookingAccount = accounts.find(ac => ac.id === taxKey.bookingAccountId);
    return {
      ...mandatorTaxKey,
      taxKey,
      bookingAccount
    };
  }

  public getStream(params: { mandatorId?: MandatorKey } = {}): Observable<IMandatorTaxKeyFull[]> {
    return forkJoin([
      this.mandatorTaxKeyRepo.fetchAll({}),
      this.taxKeyRepo.fetchAll({}),
      this.bookingAccountRepo.fetchAll({})
    ]).pipe(
      switchMap(() => combineLatest([
        this.mandatorTaxKeyRepo.getStream()
          .pipe(map(mandatorTaxKeys => this.getFilteredMandatorTaxKeys(mandatorTaxKeys, params.mandatorId))),
        this.taxKeyRepo.getStream(),
        this.bookingAccountRepo.getStream()
      ])),
      map(mandatorTaxKey => this.getMandatorTaxKeysFullList(...mandatorTaxKey))
    );
  }

  public getFilteredMandatorTaxKeys(
    mandatorTaxKeys: IMandatorTaxKey[],
    mandatorId: MandatorKey
  ): IMandatorTaxKey[] {
    if (!mandatorId) { return mandatorTaxKeys; }
    return mandatorTaxKeys.filter(mandatorTaxKey => mandatorTaxKey.mandatorId === mandatorId);
  }

  private toRow(mandatorTaxKey: IMandatorTaxKeyFull): IMandatorTaxKeyRow {
    const bookingAccount = mandatorTaxKey.bookingAccount;
    const taxKey = mandatorTaxKey.taxKey;
    return {
      ...mandatorTaxKey,
      accountNo: bookingAccount ? bookingAccount.no : null,
      accountLabel: bookingAccount ? bookingAccount.displayLabel : null,
      bookingAccount: bookingAccount,
      code: taxKey.code,
      percentage: taxKey.percentage,
      name: taxKey.displayLabel
    };
  }

  public toRows(taxKeys: IMandatorTaxKeyFull[]): IMandatorTaxKeyRow[] {
    // no need to sort here, list sorted in repo by label
    return taxKeys.map((taxKey) => this.toRow(taxKey));
  }
}
