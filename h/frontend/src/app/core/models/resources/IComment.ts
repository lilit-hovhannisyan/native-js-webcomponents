import { IResource } from './IResource';

export interface IComment extends IResource<number> {
  readonly creationDate: string;
  readonly lastEditDate?: string;
  text: string;
  readonly author: { id: number, firstName: string; lastName: string; email: string };
}
