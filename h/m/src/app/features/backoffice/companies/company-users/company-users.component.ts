import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { SubSink } from 'subsink';

import { BackOfficeService } from '@features/backoffice/backoffice.web-service';
import { TableViewService } from '@shared/me-components/me-table/table-view.service';
import { GlobalService } from '@core-services/global.service';
import { TranslateService } from '@ngx-translate/core';

import { MeTableActionButtonPropertiesI, MeTableColumnI, MeTableColumnOptionsI, MeTableDataSourceI } from '@shared/me-components/me-table';
import { TableDateComponent } from '@shared/me-components/me-table/table-date/table-date.component';
import { TableImageWithTitleComponent } from '@shared/me-components/me-table/table-image-with-title/table-image-with-title.component';
import { TableLabelComponent } from '@shared/me-components/me-table/table-label/table-label.component';
import { UserModel } from '@shared/models/user/user.model';
import { UserActionsService } from '@features/backoffice/users-groups/user-actions.service';
import { finalize } from 'rxjs/operators';
import { getOid } from '@shared/utils';
import { MeAddListCardI } from '@shared/me-components/me-add-list-card/me-add-list-card.interface';

@Component({
  selector: 'me-company-users',
  templateUrl: './company-users.component.html',
  styleUrls: ['./company-users.component.scss'],
  providers: [UserActionsService]
})
export class CompanyUsersComponent implements OnInit, OnDestroy {
  private subs = new SubSink();
  getCardOid = getOid;
  companyOID: string | null = '';
  tableLoader = false;
  tableItems: MeTableDataSourceI[] = [];
  tableColumns: MeTableColumnI[] = [];
  tableColumnOptions: MeTableColumnOptionsI[] = [];
  addGroupListCardItems: MeAddListCardI[] = [];

  _updateTableDataEventSubject!: Subscription;
  updateTableDataEventSubject = new Subject<void>();

  i18nStrings: { [key: string]: string } = {
    statusChange: '',
    statusChangeSuccess: '',
    users: '',
    usersInGroupUpdated: ''
  };

  constructor(
    private globalService: GlobalService,
    private backOfficeService: BackOfficeService,
    private tableViewService: TableViewService,
    public userActionsService: UserActionsService,
    private translate: TranslateService,
    private _activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.companyOID = this._activeRoute.snapshot.paramMap.get('id');

    if (this.companyOID) {
      this.setTableColumns();
      this.getUsers();
    }

    this.subs.sink = this.translate.get(Object.keys(this.i18nStrings)).subscribe((translations) => {
      Object.keys(this.i18nStrings).forEach((key) => {
        this.i18nStrings[key] = translations[key];
      });
    });

    this._updateTableDataEventSubject = this.updateTableDataEventSubject.subscribe(() => this.getUsers());
  }

  getUsers(): void {
    if (this.companyOID) {
      this.globalService.activateLoader();
      this.subs.sink = this.backOfficeService
        .getBoComapanyUsers(this.companyOID)
        .pipe(finalize(() => this.globalService.deactivateLoader()))
        .subscribe((response) => {
          if (response && response.entities) {
            this.prepareTableData(response.entities);
          }
        });
    }
  }

  setTableColumns(): void {
    this.tableColumns = [
      {
        field: 'user',
        headerName: 'User'
      },
      {
        field: 'createdOn',
        headerName: 'Creation Date'
      },
      {
        field: 'status',
        headerName: 'Status'
      },
      {
        field: 'actions',
        headerName: 'Actions'
      }
    ];
  }

  prepareTableData(data: UserModel[]) {
    this.tableLoader = true;
    const actionButtons: MeTableActionButtonPropertiesI[] = [
      {
        iconName: 'edit-2',
        type: 'edit'
      },
      {
        contextMenuItems: [
          {
            isActiveAttrType: true,
            key: '',
            value: ''
          },
          { key: 'manageGroups', value: 'Manage Groups' }
        ],
        type: 'selectedContextMenu'
      }
    ];

    const customComponents: MeTableDataSourceI[] = data.map((d) => {
      const {
        attributes: { avatar, firstName, lastName, isActive }
      } = d;
      const generatedActionButtons = this.tableViewService.genereateActionButtons(actionButtons, isActive);

      return {
        rowOID: d.oid,
        renderedCells: [
          generatedActionButtons,
          {
            forField: 'user',
            component: TableImageWithTitleComponent,
            componentData: { url: avatar, title: `${firstName} ${lastName}` }
          },
          {
            forField: 'createdOn',
            component: TableDateComponent,
            // @ts-ignore
            componentData: new Date(d.createdOn)
          },
          {
            forField: 'status',
            component: TableLabelComponent,
            componentData: isActive
          }
        ]
      };
    });

    this.tableItems = customComponents;
    this.tableLoader = false;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();

    if (this._updateTableDataEventSubject) {
      this.updateTableDataEventSubject.unsubscribe();
    }
  }
}
