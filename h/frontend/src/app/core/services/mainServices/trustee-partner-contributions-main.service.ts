import { Injectable } from '@angular/core';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { CompanyRepositoryService } from '../repositories/company-repository.service';
import { ITrusteePartnerContribution, ITrusteePartnerContributionFull } from '../../models/resources/ITrusteePartnerContribution';
import { ICompany, CompanyKey } from '../../models/resources/ICompany';
import * as moment from 'moment';
import { TrusteePartnerContributionsRepositoryService, ITrusteePartnerContributionRepoParams, ITrusteePartnerFilterParams } from '../repositories/trustee-partner-contribution-repository.service';
import { CompanyContributionsRepositoryService } from 'src/app/core/services/repositories/company-partner-contribution-repository.service';
import { ICompanyPartnerContribution, ICompanyPartnerContributionFull } from 'src/app/core/models/resources/ICompanyPartnerContribution';
import { ContributionMainService } from 'src/app/core/services/mainServices/company-contributions-main.service';
import { TrusteeContributionsMap } from '../../../features/system/shared/trustee-partner-contributions/trustee-partner-contribution.helper';
import { COMPANY_CONTRIBUTION_TYPES } from '../../../features/system/shared/trustee-partner-contributions/enums/company-contributions';

export type LatestContributionsMap = TrusteeContributionsMap<
  CompanyKey,
  CompanyKey,
  ITrusteePartnerContributionFull
>;

@Injectable({
  providedIn: 'root'
})
export class TrusteePartnerContributionMainService {
  constructor(
    private trusteeContributionsRepo: TrusteePartnerContributionsRepositoryService,
    private shareholderContributionsRepo: CompanyContributionsRepositoryService,
    private shareholderContributionsMainService: ContributionMainService,
    private companyRepo: CompanyRepositoryService,
  ) { }

  public fetchAll(params: ITrusteePartnerContributionRepoParams): Observable<ITrusteePartnerContributionFull[]> {
    return forkJoin([
      this.trusteeContributionsRepo.fetchAll(params),
      this.shareholderContributionsRepo.fetchAll(params),
      this.companyRepo.fetchAll({}),
    ]).pipe(
      map(res => this.getFullList(...res)),
    );
  }

  public getStream(params: ITrusteePartnerContributionRepoParams): Observable<ITrusteePartnerContributionFull[]> {
    const getStreamParams: ITrusteePartnerFilterParams = {
      _type: COMPANY_CONTRIBUTION_TYPES[params.contributorType],
      companyId: params?.queryParams?.cid,
    };

    return forkJoin([
      this.trusteeContributionsRepo.fetchAll(params),
      this.shareholderContributionsRepo.fetchAll(params),
      this.companyRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => {
        return combineLatest([
          this.trusteeContributionsRepo.getStream(getStreamParams),
          this.shareholderContributionsRepo.fetchAll(params),
          this.companyRepo.getStream({}),
        ]);
      }),
      map(res => this.getFullList(...res)),
    );
  }

  private getFullList(
    contributions: ITrusteePartnerContribution[],
    shareholdersContributions: ICompanyPartnerContribution[],
    partners: ICompany[]
  ): ITrusteePartnerContributionFull[] {
    const shareholdersLatestContribution: Map<CompanyKey, ICompanyPartnerContribution>
      = this.shareholderContributionsMainService.getCompaniesLatestContributions(shareholdersContributions);
    const partnersMap: Map<CompanyKey, ICompany> = new Map(
      partners.map(p => ([p.id, p]))
    );

    return contributions.map(c => this.getFull(c, partnersMap, shareholdersLatestContribution));
  }

  private getFull(
    contribution: ITrusteePartnerContribution,
    partnersMap: Map<CompanyKey, ICompany>,
    shareholderContributionsMap: Map<CompanyKey, ICompanyPartnerContribution>,
  ): ITrusteePartnerContributionFull {
    const partnerCompany: ICompany = partnersMap.get(contribution.partnerId);
    const trusteePartner: ICompany = partnersMap.get(contribution.shareholderId);
    const shareholderContribution: ICompanyPartnerContribution
      = shareholderContributionsMap.get(contribution.shareholderId);
    const trusteePartnerFull: ICompanyPartnerContributionFull = {
      ...trusteePartner,
      ...shareholderContribution,
    };

    return {
      ...partnerCompany,
      ...contribution,
      trusteePartner: trusteePartnerFull,
      trusteePartnerName: trusteePartner.companyName,
    };
  }

  public filterContributions<T extends ITrusteePartnerContribution>(
    contributions?: T[],
    date?: string,
  ): T[] {
    const dateMoment = moment(date);
    if (!contributions || !dateMoment.isValid()) {
      return contributions || [];
    }

    return contributions.filter(c => moment(c.from).isSameOrBefore(date, 'd'));
  }

  public getCompaniesLatestContributions(
    contributions: ITrusteePartnerContributionFull[],
  ): LatestContributionsMap {
    const contributionsMap: LatestContributionsMap
      = new TrusteeContributionsMap<CompanyKey, CompanyKey, ITrusteePartnerContributionFull>();

    contributions?.forEach(item => {
      const contribution: ITrusteePartnerContributionFull = contributionsMap.get(
        item.partnerId,
        item.shareholderId,
      );
      const contributionAmount = (contribution?.contributionAmount || 0) + item.contributionAmount;

      // needed to keep contribution with latest date
      const latestContributionOfCompany: ITrusteePartnerContributionFull
        = contribution && moment(contribution.from).isSameOrAfter(item.from) ? contribution : item;

      contributionsMap.set(item.partnerId, item.shareholderId, {
        ...latestContributionOfCompany,
        contributionAmount,
      });
    });

    return contributionsMap;
  }

  public getFinalContribution(
    contributorType: COMPANY_CONTRIBUTION_TYPES,
    companyId: CompanyKey,
    partnerId: CompanyKey,
    shareholderIdValue: CompanyKey,
    from?: string
  ): Observable<number> {
    return this.trusteeContributionsRepo.fetchTrusteePartnerContributions(companyId, contributorType)
      .pipe(
        map((contributions: ITrusteePartnerContribution[]) => {
          const partnerContributions: ITrusteePartnerContributionFull[]
            = contributions.filter(
              c => c.partnerId === partnerId && c.shareholderId === shareholderIdValue
            ) as ITrusteePartnerContributionFull[];

          const filteredContributions: ITrusteePartnerContributionFull[] = !!from
            ? this.filterContributions(partnerContributions, from)
            : partnerContributions;

          return filteredContributions.reduce((
            contributionAmount: number,
            contribution: ITrusteePartnerContributionFull
          ) => {
            contributionAmount += contribution.contributionAmount;

            return contributionAmount;
          }, 0);
        })
      );
  }

  public getTotalContributionAmount(
    contributions: ITrusteePartnerContributionFull[],
  ): number {
    return contributions.reduce((sum, c) => sum + c.contributionAmount, 0);
  }

  public sortByDate<T extends ITrusteePartnerContribution>(
    contributions: T[],
  ): T[] {
    return contributions.sort((a, b) => {
      return moment(a.from).isAfter(b.from) ? -1 : 1;
    });
  }

  public getPartnersSummaryContributions(
    latestContributionsMap: LatestContributionsMap,
  ): Map<CompanyKey, ITrusteePartnerContributionFull> {
    const summaryContributionsMap: Map<CompanyKey, ITrusteePartnerContributionFull> = new Map();
    for (const contribution of latestContributionsMap.values()) {
      const prevVal: ITrusteePartnerContributionFull = summaryContributionsMap
      .get(contribution.partnerId);

      summaryContributionsMap.set(contribution.partnerId, {
        ...prevVal,
        ...contribution,
        contributionAmount: (prevVal?.contributionAmount || 0) + contribution.contributionAmount,
      });
    }

    return summaryContributionsMap;
  }

  public getConnectedTrustees(
    trustorId: CompanyKey,
    companies: ICompany[],
    contributions: ITrusteePartnerContribution[],
    fromDate?: string
  ): ICompany[] {
    const connectedCompaniesIds: CompanyKey[] = contributions
      .filter(c => {
        const isPartner: boolean = c.partnerId === trustorId;
        return fromDate ? isPartner && moment(fromDate).isSameOrBefore(c.from, 'd')
        : isPartner;
      })
      .map(c => c.shareholderId);
    const connectedCompaniesIdsSet: Set<CompanyKey> = new Set(connectedCompaniesIds);

    return companies.filter(c => {
      return connectedCompaniesIdsSet.has(c.id);
    });
  }
}
