import { MeTabI } from './me-tab.interface';

export interface MeTabGroupI {
  tabs: MeTabI[];
}
