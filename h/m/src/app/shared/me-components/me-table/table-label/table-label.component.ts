import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'me-table-label',
  templateUrl: './table-label.component.html',
  styleUrls: ['./table-label.component.scss']
})
export class TableLabelComponent implements OnInit {
  @Input() data!: boolean;

  constructor() {}

  ngOnInit(): void {}
}
