import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-range-start-end',
  templateUrl: './range-start-end.component.html',
  styleUrls: ['./range-start-end.component.scss']
})
export class RangeStartEndComponent {
  @Input() public form: FormGroup;
  @Input() public showLabel = false;
  public wording = wording;
  constructor() { }
}
