import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

/**
 * Please use the safeHTML pipe with cautious.
 * It should not be used to mark any html content as safe which comes
 * from other users. Otherwise the application would be vulnerable
 * to cross site scripting attempts.
 */
@Pipe({
  name: 'safeHTML'
})
export class SafeHTMLPipe implements PipeTransform {
  constructor(private sanitized: DomSanitizer) { }
  public transform(value: string): SafeHtml {
    return this.sanitized.bypassSecurityTrustHtml(value);
  }
}
