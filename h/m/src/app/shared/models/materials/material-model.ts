import { Type } from 'class-transformer';
import { BaseModel } from '../base-model/base-model';
import { MaterialAttributesModel } from './material-attributes.model';

export class MaterialModel extends BaseModel {
  @Type(() => MaterialAttributesModel)
  attributes!: MaterialAttributesModel;
  @Type(() => MaterialAttributesModel)
  attributesExtended!: MaterialAttributesModel;
  selected?: boolean = false;
}
