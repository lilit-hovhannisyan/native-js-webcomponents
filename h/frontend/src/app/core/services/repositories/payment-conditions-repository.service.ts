import { Injectable } from '@angular/core';
import { IPaymentCondition, IPaymentConditionDTO, IPaymentConditionCreate } from '../../models/resources/IPaymentConditions';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { map } from 'rxjs/operators';
import { HttpGet } from '../../models/http/http-get';

export interface IPaymentConditionRepoParams extends RepoParams<IPaymentCondition> {
  debitorCreditorId?: number;
}

type PaymentConditionKey = number;

@Injectable({
  providedIn: 'root'
})
export class PaymentConditionsRepositoryService extends AbstractRepository<
  IPaymentCondition,
  IPaymentConditionDTO,
  IPaymentConditionCreate,
  IPaymentConditionRepoParams,
  PaymentConditionKey
> {

  protected resourceType = 'PaymentConditions';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess(item: IPaymentConditionDTO): IPaymentCondition { return item; }

  public getBaseUrl = () => '/api/payment-conditions';

  public fetchByDebitorCreditor(params: IPaymentConditionRepoParams) {
    const url = `/api/debitor-creditors/${params.debitorCreditorId}/payment-conditions`;
    const req = new HttpGet(url);
    // So we don't get error notification when payment of condition doesn't exist yet
    req.setErrorHandler(params.errorHandler);
    return this.repositoryService.httpClient.request(req)
      .pipe(map((response: any) => response.body && response.body.data as IPaymentCondition));
  }
}
