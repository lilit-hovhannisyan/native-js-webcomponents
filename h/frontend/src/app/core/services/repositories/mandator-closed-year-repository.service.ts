import { Injectable } from '@angular/core';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { IMandatorClosedBookingYear, MandatorClosedBookingYearKey, IMandatorClosedBookingYearCreate, IMandatorClosedBookingYearDTO } from '../../models/resources/IMandatorClosedBookingYear';
import { Observable } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { HttpGet } from '../../models/http/http-get';
import { RepositoryService } from '../repository.service';

export interface MandatorClosedBookingYearParams extends RepoParams<IMandatorClosedBookingYear> {
  mandatorIds?: number[];
  onlyLast?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class MandatorClosedYearsRepositoryService extends AbstractRepository<
IMandatorClosedBookingYear,
IMandatorClosedBookingYearDTO,
IMandatorClosedBookingYearCreate,
RepoParams<IMandatorClosedBookingYear>,
MandatorClosedBookingYearKey
> {
  protected resourceType = 'MandatorClosedBookingYear';

  constructor(public repositoryService: RepositoryService) {
    super(repositoryService);
  }
  /**
   *
   * @param mandatorIds ids of mandators you want the LastClosedYear
   * @param onlyLast
   */
  public fetchByMandators(mandatorIds: number[] = [], onlyLast = false): Observable<IMandatorClosedBookingYear[]> {
    const request = new HttpGet(this.getBaseUrl(), { init: { params: { mandatorIds } } });
    return this.repositoryService.httpClient.request(request).pipe(
      map((response: any) => response.body.data.items as IMandatorClosedBookingYear[]),
      shareReplay(1)
    );
  }

  protected postProcess(item: IMandatorClosedBookingYearDTO): IMandatorClosedBookingYear { return item; }

  public getBaseUrl = () => '/api/mandator-closed-years';
}
