export enum MeFileUploadTypes {
  coverImage = 'COVER_IMAGE',
  document = 'DOCUMENT',
  document2d = 'DOCUMENT_2D',
  geometry3d = 'GEOMETRY_3D',
  material3d = 'MATERIAL_3D',
  video = 'VIDEO',
  logo = 'LOGO',
  profile = 'PROFILE',
  testReport = 'TEST_REPORT',
  certificate = 'CERTIFICATE',
  thumbnail = 'THUMBNAIL',
  wechatQRCode = 'WECHAT_QR_CODE'
}
