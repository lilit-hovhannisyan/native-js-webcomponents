import { CertificateAssociationModel } from '@shared/models/certificate-association/certificate-association.model';
import { CertificateModel } from './../../shared/models/certificate/certificate.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { plainToClass } from 'class-transformer';

@Injectable()
export class DigitalProfileEditStoreService {
  private readonly _certificate = new BehaviorSubject<CertificateModel[] | null>(null);
  readonly certificate$ = this._certificate.asObservable();

  private readonly _certificateassociation = new BehaviorSubject<CertificateAssociationModel[] | null>(null);
  readonly certificateassociation$ = this._certificateassociation.asObservable();

  get certificates(): CertificateModel[] | null {
    return this._certificate.getValue();
  }

  set certificates(data: CertificateModel[] | null) {
    this._certificate.next(data);
  }

  get certificateAssociation(): CertificateAssociationModel[] | null {
    return this._certificateassociation.getValue();
  }

  set certificateAssociation(data: CertificateAssociationModel[] | null) {
    this._certificateassociation.next(data);
  }

  getCertificate(name: string): CertificateModel {
    return this.findcertificatebyId(this._certificate.getValue(), name);
  }

  getAssociationbyCertificateId(name: string): CertificateAssociationModel | null {
    return this.findAssociationcertificatebyId(this._certificateassociation.getValue(), name);
  }

  private findcertificatebyId(certificateArray: CertificateModel[] | null, name: string): CertificateModel {
    let result!: CertificateModel;
    if (certificateArray) {
      for (const domain of certificateArray) {
        const matchDomain: CertificateModel = domain.oid === name ? domain : result;

        if (matchDomain) {
          result = plainToClass(CertificateModel, { ...matchDomain });
          return result;
        }
      }
    }
    return result;
  }

  private findAssociationcertificatebyId(
    certificateArray: CertificateAssociationModel[] | null,
    name: string
  ): CertificateAssociationModel {
    let result!: CertificateAssociationModel;
    if (certificateArray) {
      for (const domain of certificateArray) {
        const matchDomain: CertificateAssociationModel = domain.attributes.certificate === name ? domain : result;

        if (matchDomain) {
          result = plainToClass(CertificateAssociationModel, { ...matchDomain });
          return result;
        }
      }
    }
    return result;
  }
}
