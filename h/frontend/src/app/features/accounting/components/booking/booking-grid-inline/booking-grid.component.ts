import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, TemplateRef, ViewChild, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { GridComponent, NvColumnConfig, NvGridConfig } from 'nv-grid';
import { of, ReplaySubject, Subject, Observable, BehaviorSubject, forkJoin } from 'rxjs';
import { switchMap, takeUntil, map, skipWhile, take } from 'rxjs/operators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { ACCOUNTING_TYPES } from 'src/app/core/models/enums/accounting-types';
import { DEBIT_CREDIT } from 'src/app/core/models/enums/debitCredit';
import { IBooking, BookingKey } from 'src/app/core/models/resources/IBooking';
import { IBookingAccount, accountNoModifier } from 'src/app/core/models/resources/IBookingAccount';
import { IInvoiceLedgerRow, InvoiceLedgerKey } from 'src/app/core/models/resources/IInvoiceLedger';
import { ILogCreate } from 'src/app/core/models/resources/ILog';
import { mandatorNoModifier, IMandatorLookup, MandatorKey, IMandatorLookupRow } from 'src/app/core/models/resources/IMandator';
import { IMandatorAccount } from 'src/app/core/models/resources/IMandatorAccount';
import { IVoucherType } from 'src/app/core/models/resources/IVoucherType';
import { IWording } from 'src/app/core/models/resources/IWording';
import { ActivityLogsService } from 'src/app/core/services/activity-logs.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { AccountBalanceMainService } from 'src/app/core/services/mainServices/accountbalance-main.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { DebitorCreditorRepositoryService } from 'src/app/core/services/repositories/debitor-creditor-repository.service';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { MandatorBookingCodeRepositoryService } from 'src/app/core/services/repositories/mandator-booking-code-repository.service';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { MandatorTaxKeysRepositoryService } from 'src/app/core/services/repositories/mandator-tax-keys-repository.service';
import { TaxKeyRepositoryService } from 'src/app/core/services/repositories/tax-key-repository.service';
import { VoucherTypeRepositoryService } from 'src/app/core/services/repositories/voucher-type-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { ToCurrentDateFormatPipe } from 'src/app/shared/pipes/to-current-date-format.pipe';
import { gridConfig as accountGridConfig } from 'src/app/shared/components/autocomplete/modals/account-grid-config';
import { gridConfig as bookingCodeGridConfig } from 'src/app/shared/components/autocomplete/modals/booking-code-grid-config';
import { gridConfig as currencyGridConfig } from 'src/app/shared/components/autocomplete/modals/currency-grid-config';
import { gridConfig as mandatorGridConfig } from 'src/app/shared/components/autocomplete/modals/mandator-grid-config';
import { gridConfig as taxGridConfig } from 'src/app/shared/components/autocomplete/modals/tax-grid-config';
import { BookingGridBehavioursService } from './booking-grid-behaviours/booking-grid-behaviours.service';
import { HttpParams } from '@angular/common/http';
import { autocompleteDiscountOptionsFilter } from 'src/app/shared/components/autocomplete/custom-filter-functions';
import { IAutocompleteCustomOptionsFilter } from 'src/app/shared/components/autocomplete/autocomplete.component';
import { CostTypeRepositoryService } from 'src/app/core/services/repositories/cost-type-repository.service';
import { ICostType } from 'src/app/core/models/resources/ICostTypes';
import { CostCenterHelperService } from './booking-grid-behaviours/cost-center-helper.service';
import { MandatorMainService } from 'src/app/core/services/mainServices/mandator-main.service';
import { ICostCenter } from 'src/app/core/models/resources/ICostCenter';
import { CostCenterRepositoryService } from 'src/app/core/services/repositories/cost-center-repository.service';
import { CostUnitHelperService } from './booking-grid-behaviours/cost-unit-helper.service';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { markControlsAsTouched } from 'src/app/core/helpers/form';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { IncomingInvoiceComponent } from './incoming-invoice/incoming-invoice.component';
import { BookingFormFieldsInlineComponent } from './booking-form-fields/booking-form-fields.component';
import { IBookingValidation } from 'src/app/core/models/resources/IBookingValidation';
import { INavidoHTTPResponseError } from 'src/app/core/interceptors/http-error-handler';
import { ICurrency, CurrencyKey } from 'src/app/core/models/resources/ICurrency';
import { ExchangeRateService } from 'src/app/core/services/mainServices/exchange-rate.service';
import { IExchangeRateFull, IExchangeRate } from 'src/app/core/models/resources/IExchangeRate';
import { AmountAndRate } from './helpers/amounts-and-rate';
import { SettingsRepositoryService } from 'src/app/core/services/repositories/settings-repository.service';
import { IStartEndRange, ISettings } from 'src/app/core/models/resources/ISettings';
import { DEBITOR_CREDITOR } from 'src/app/core/models/enums/debitor-creditor';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { MandatorAccountRoleRepositoryService } from 'src/app/core/services/repositories/mandator-account-role.repository.service';
import { NvAction } from 'nv-grid/lib/components/nv-button/nv-button.component';
import { AccountValidatorsService } from 'src/app/core/services/validators/account-validators.service';
import { CodeValidatorsService } from 'src/app/core/services/validators/code-validators.service';
import { CostTypeValidatorsService } from 'src/app/core/services/validators/cost-type-validators.service';
import { CurrencyValidatorsService } from 'src/app/core/services/validators/currency-validators.service';
import { MandatorValidatorsService } from 'src/app/core/services/validators/mandator-validators.service';
import { PaymentDateValidatorsService } from 'src/app/core/services/validators/payment-date-validators.service';
import { BookingFormService } from 'src/app/core/services/booking-form.service';
import { IBookingGridConfigActions, getBookinglineGridConfig } from 'src/app/core/constants/nv-grid-configs/booking-grid.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { IBookingLineRow, IBookingLine } from 'src/app/core/models/resources/IBookingLine';
import { ICostUnit } from 'src/app/core/models/resources/ICostUnit';
import { BookingResourceService } from 'src/app/core/services/booking-resource.service';
import { BookingRepositoryService } from 'src/app/core/services/repositories/booking-repository.service';
import { CostUnitRepositoryService } from 'src/app/core/services/repositories/cost-unit-repository.service';
import { isAccountingTypeValid } from 'src/app/core/services/validators/validators';
import { gridConfig as discountGridConfig } from 'src/app/core/constants/nv-grid-configs/discount-grid.config';
import { BookingCalculatorService } from 'src/app/core/services/booking-calculator.service';
import { DatesRatesTableComponent, IDateRate } from 'src/app/shared/components/dates-rates-table/dates-rates-table.component';
import { ExchangeRateRepositoryService } from 'src/app/core/services/repositories/exchange-rate-repository.service';
import * as moment from 'moment';
import { IExchangeRateCalculated } from 'src/app/core/models/resources/IExchangeRateCalculated';

export interface BookingFieldsData {
  mandator: IMandatorLookup;
  voucherType: IVoucherType;
  voucherTypes: IVoucherType[];
  booking?: IBooking;
}
@Component({
  selector: 'nv-booking-grid',
  templateUrl: './booking-grid.component.html',
  styleUrls: ['./booking-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class BookingGridComponent implements OnInit, OnDestroy {
  @ViewChild('bookingFormFields', { static: true }) public bookingFormFields: BookingFormFieldsInlineComponent;
  @ViewChild('bookingGrid', { static: true }) public bookingGrid: GridComponent;
  @ViewChild('accountNoIsNotRelatedTemplate', { static: true }) public accountNoIsNotRelatedTemplate: TemplateRef<any>;
  @ViewChild('bookingCodeIsNotRelatedTemplate', { static: true }) public bookingCodeIsNotRelatedTemplate: TemplateRef<any>;
  @ViewChild('discountDiffersWithPaymentDateTemplate', { static: true }) public discountDiffersWithPaymentDateTemplate: TemplateRef<any>;

  public wording = wording;
  public gridConfig: NvGridConfig;
  public accountGridConfig: NvGridConfig;
  public mandatorGridConfig: NvGridConfig;
  public currencyGridConfig: NvGridConfig;
  public discountGridConfig: NvGridConfig;
  public bookingCodeGridConfig: NvGridConfig;
  public taxGridConfig: NvGridConfig;

  public currentBookingId: number | string;
  public clonesBookingId: number;

  private bookingLinesSubject: ReplaySubject<IBookingLineRow[]> = new ReplaySubject<IBookingLineRow[]>(1);
  public bookingLines$ = this.bookingLinesSubject.asObservable();

  public currentBookingDataSubject: ReplaySubject<BookingFieldsData> = new ReplaySubject<BookingFieldsData>(1);
  public currentBookingData$ = this.currentBookingDataSubject.asObservable();

  public createMode: boolean;
  public cloneMode: boolean;
  public readMode: boolean;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public customValidation: () => boolean;
  public mandatorNoModifier = mandatorNoModifier;
  public currentAccountNo: number;
  public currentAccountName: IWording;
  public bookInvoiceMode: boolean;
  public changeMandator$ = new Subject<IMandatorLookup>();
  public autocompleteDiscountOptionsFilter: IAutocompleteCustomOptionsFilter = autocompleteDiscountOptionsFilter;
  public validationErrors$: BehaviorSubject<INavidoHTTPResponseError[]> = new BehaviorSubject<INavidoHTTPResponseError[]>([]);

  private bookingId: BookingKey;
  private incomingInvoideModalRef: NzModalRef<IncomingInvoiceComponent>;
  private datesRatesModalRef: NzModalRef<DatesRatesTableComponent>;
  private invoiceLedgerId: InvoiceLedgerKey;

  private accountSheetPath: string = url(sitemap.accounting
    .children.booking
    .children.accountSheets
  );
  public mandatorLookups$: Observable<IMandatorLookupRow[]>;

  public shouldShowNoValidationErrorMessage: boolean;
  private globalSettings: ISettings;

  constructor(
    public mandatorRepo: MandatorRepositoryService,
    public bookingAccountRepo: BookingAccountRepositoryService,
    public bookingRepo: BookingRepositoryService,
    private behavioursService: BookingGridBehavioursService,
    public mandatorAccountRepo: MandatorAccountRepositoryService,
    private mandatorBookingCodeRepo: MandatorBookingCodeRepositoryService,
    private bookingFormService: BookingFormService,
    public bookingResourceService: BookingResourceService,
    public route: ActivatedRoute,
    private mandatorValidatorsService: MandatorValidatorsService,
    private accountValidatorsService: AccountValidatorsService,
    private codeValidatorsService: CodeValidatorsService,
    private paymentDateValidatorsService: PaymentDateValidatorsService,
    private currencyValidatorsService: CurrencyValidatorsService,
    public settingsService: SettingsService,
    private activityLogsService: ActivityLogsService,
    public accountBalanceMainService: AccountBalanceMainService,
    public mandatorCodeRepo: MandatorBookingCodeRepositoryService,
    public bookingCodeRepo: BookingCodeRepositoryService,
    private voucherTypeRepo: VoucherTypeRepositoryService,
    public mandatorTaxKeyRepo: MandatorTaxKeysRepositoryService,
    public taxKeyRepo: TaxKeyRepositoryService,
    public currencyRepo: CurrencyRepositoryService,
    private debitorCreditorRepo: DebitorCreditorRepositoryService,
    private confirmationService: ConfirmationService,
    private costTypeRepo: CostTypeRepositoryService,
    private costTypeValidatorService: CostTypeValidatorsService,
    private costCenterHelper: CostCenterHelperService,
    private mandatorMainService: MandatorMainService,
    private costCenterRepo: CostCenterRepositoryService,
    private costUnitRepo: CostUnitRepositoryService,
    private costUnitHelper: CostUnitHelperService,
    private modalService: NzModalService,
    private exchangeRateService: ExchangeRateService,
    private amountAndRate: AmountAndRate,
    private settingsRepo: SettingsRepositoryService,
    private companyRepo: CompanyRepositoryService,
    public mandatorAccountRoleRepo: MandatorAccountRoleRepositoryService,
    private bookingCalculatorService: BookingCalculatorService,
    private exchangeRateRepo: ExchangeRateRepositoryService,
  ) { }

  public ngOnInit() {

    this.accountBalanceMainService.reset();
    this.currentBookingId = this.route.snapshot.params.id;
    this.mandatorLookups$ = this.mandatorMainService.fetchLookupsWithRolesOnly();

    this.route.queryParams.subscribe(queryParams => {
      this.clonesBookingId = queryParams.clones;
      this.readMode = queryParams.readOnly ? queryParams.readOnly : false;
    });

    this.createMode = this.currentBookingId === 'new';
    this.cloneMode = this.clonesBookingId !== undefined;
    this.bookInvoiceMode = !!this.route.snapshot.queryParams['book-invoice'];

    this.initGridConfigs();

    forkJoin([
      this.settingsRepo.fetchAll(),
      this.bookingResourceService.fetch(),
      this.mandatorAccountRepo.fetchAll({}),
      this.bookingAccountRepo.fetchAll({}),
      this.mandatorAccountRoleRepo.fetchAll({}),
    ])
      .pipe(
        takeUntil(this.componentDestroyed$)
      ).subscribe(([globalSettings]) => {
        this.globalSettings = globalSettings;

        if (this.bookInvoiceMode) {
          this.setupCloneFromInvoiceLedger();
        } else {
          this.setBookingLines();
        }

        if (!this.readMode) {
          this.subscribeToFirstForm();
        }

        this.updateCurrentBalance(true);
      });

    this.subscribeToFocusedCell();
    this.subscribeToFormsDataSubject();
  }

  public ngOnDestroy() {
    this.bookingFormService.bookInvoiceMode = false;
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  private setBookingLines(): void {
    this.bookingId = +this.currentBookingId || +this.clonesBookingId;
    if (!this.bookingId) {
      return this.bookingLinesSubject.next([]);
    }
    const foundBooking: IBooking = this.bookingRepo.getStreamValue()
      .find(booking => booking.id === this.bookingId);

    if (foundBooking) {
      const bookingMandator: IMandatorLookup = this.mandatorRepo
        .getLookupStreamValue()
        .find(mandator => mandator.id === foundBooking.mandatorId);

      const bookingLines: IBookingLineRow[] = [...foundBooking.bookingLines].map(bookingLine => {
        const foundBookingAccount = this.bookingAccountRepo.getStreamValue()
          .find(bookingAccount => bookingAccount.id === bookingLine.bookingAccountId);
        const foundMandator: IMandatorLookup = this.mandatorRepo
          .getLookupStreamValue()
          .find(mandator => mandator.id === bookingLine.mandatorId);
        const foundBCurrency = this.currencyRepo.getStreamValue()
          .find(currency => currency.id === bookingLine.currencyId);
        const foundBookingCode = this.bookingCodeRepo.getStreamValue()
          .find(bookingCode => bookingCode.id === bookingLine.bookingCodeId);
        const foundVoucherType = this.voucherTypeRepo.getStreamValue()
          .find(voucherType => voucherType.id === foundBooking.voucherTypeId);
        const foundTaxKey = this.taxKeyRepo.getStreamValue()
          .find(taxKey => taxKey.id === bookingLine.taxKeyId);
        const costType: ICostType | null = bookingLine.costTypeId
          ? this.costTypeRepo.getStreamValue()
            .find(c => c.id === bookingLine.costTypeId)
          : null;
        const costCenter: ICostCenter | null = bookingLine.costCenterId
          ? this.costCenterRepo.getStreamValue()
            .find(c => c.id === bookingLine.costCenterId)
          : null;
        const costUnit: ICostUnit | null = bookingLine.costUnitId
          ? this.costUnitRepo.getStreamValue()
            .find(c => c.id === bookingLine.costUnitId)
          : null;

        this.currentBookingDataSubject.next({
          booking: foundBooking,
          mandator: bookingMandator,
          voucherType: foundVoucherType,
          voucherTypes: this.voucherTypeRepo.getStreamValue()
        });

        return {
          ...bookingLine,
          tax: foundTaxKey?.code,
          accountNo: foundBookingAccount?.no,
          currencyIsoCode: foundBCurrency?.isoCode,
          mandatorNo: foundMandator?.no,
          bookingCodeNo: foundBookingCode?.no,
          '+/-': this.getPlusMinusValue(bookingLine.debitCreditType),
          costTypeNo: costType?.no,
          costCenterNo: costCenter?.no,
          costUnitNo: costUnit?.no,
          '_readonly': bookingLine.readOnly
        };
      });

      this.bookingLinesSubject.next(bookingLines);

      bookingLines.forEach(bookingLine => bookingLine.readOnly ||
        this.bookingGrid.createNewForm(bookingLine.id));
      this.bookingGrid.formsDataSubject.value.forEach(b => {
        validateAllFormFields(b.form);
        this.behavioursService.onMandatorChange(b.form, this.bookInvoiceMode, this.bookingId);
        this.behavioursService.onCurrencyChange(b.form);
      });
      const formsData = this.bookingGrid.formsDataSubject.value;
      if (formsData.length > 0) {
        this.calculatePlusMinusSignAndAmountLocal(null);
        this.updateDefaultCurrencyValue();
      }
    } else {
      this.currentBookingDataSubject.next({
        booking: null,
        mandator: null,
        voucherType: null,
        voucherTypes: [],
      });
      this.bookingLinesSubject.next([]);
    }
  }

  private initGridConfigs() {
    this.accountGridConfig = accountGridConfig;
    this.currencyGridConfig = currencyGridConfig;
    this.mandatorGridConfig = mandatorGridConfig();
    this.bookingCodeGridConfig = bookingCodeGridConfig;
    this.discountGridConfig = discountGridConfig;
    this.taxGridConfig = taxGridConfig;
    const actions: IBookingGridConfigActions = {
      updateDefaultMandatorNo: this.updateDefaultMandatorNo,
      onMandatorChange: (form: FormGroup) =>
        this.onMandatorChange(form, this.bookInvoiceMode, this.bookingId),
      setMandatorId: this.behavioursService.setMandatorId,
      setBookingCodeId: this.behavioursService.setBookingCodeId,
      setTaxKeyId: this.behavioursService.setTaxKeyId,
      setBookingAccount: this.behavioursService.setBookingAccount,
      onBookingAccountChange: (form: FormGroup) => this.behavioursService
        .onBookingAccountChange(form, this.bookInvoiceMode, this.bookingId),
      setDefaultCurrencyValue: this.bookInvoiceMode ? () => null : this.updateDefaultCurrencyValue,
      onAmountLocalChange: this.onAmountLocalChange,
      calculatePlusMinusSignAndAmountLocal: this.calculatePlusMinusSignAndAmountLocal,
      onAmountTransactionChange: this.onAmountTransactionChange,
      onRateChange: this.onRateChange,
      setCurrencyId: this.behavioursService.setCurrencyId,
      onCurrencyChange: (form: FormGroup) => this.onCurrencyChange(form),
      onTaxKeyChange: this.onTaxKeyChange,
      onPlusMinusChange: this.behavioursService.onPlusMinusChange,
      isBookingSaveButtonDisabled: this.isBookingSaveButtonDisabled,
      saveBooking: this.saveBooking,
      onDebitCreditTypeChange: this.onDebitCreditTypeChange,
      onDiscountChange: this.behavioursService.onDiscountChange,
      updateDiffer: this.updateDiffer,
      showInAccountSheet: this.showInAccountSheet,
      bookingFormSubject: this.bookingFormService.bookingFormSubject,
      setCostTypeId: this.behavioursService.setCostTypeId,
      setCostCenterId: this.behavioursService.setCostCenterId,
      setCostUnitId: this.behavioursService.setCostUnitId,
      takeInvoice: this.takeInvoice,
      predefineTextFromAccountChange: (form: FormGroup) => this.predefineTextFromAccountChange(form),
      onTextChange: (form: FormGroup) => this.onTextChange(form)
    };

    const validations = {
      validatePaymentDate: this.paymentDateValidatorsService.validatePaymentDate,
      paymentDateOfDiscount: this.paymentDateValidatorsService.paymentDateOfDiscount,
      validateMandatorAccountingType: this.mandatorValidatorsService.validateMandatorAccountingType,
      doesMandatorExist: this.mandatorValidatorsService.doesMandatorNoExist,
      isMandatorActive: this.mandatorValidatorsService.isMandatorNoActive,
      isYearClosedForMandator: this.mandatorValidatorsService.isYearClosedForMandator,
      validateFirstBookingYear: this.mandatorValidatorsService.validateFirstBookingYear,
      isPeriodClosedForMandator: this.mandatorValidatorsService.isPeriodClosedForMandator,
      isPeriodBlockedForMandator: this.mandatorValidatorsService.isPeriodBlockedForMandator,
      validateAccountNoAccountType: this.accountValidatorsService.validateAccountNoAccountType,
      doesBookingAccountExist: this.accountValidatorsService.doesBookingAccountNoExist,
      isBookingAccountActive: this.accountValidatorsService.isBookingAccountNoActive,
      isBookingAccountRelatedToMandator: this.accountValidatorsService.isBookingAccountNoRelatedToMandator,
      doesBookingCodeExist: this.codeValidatorsService.doesBookingCodeNoExist,
      isBookingCodeActive: this.codeValidatorsService.isBookingCodeNoActive,
      isBookingCodeRelatedToMandator: this.codeValidatorsService.isBookingCodeNoRelatedToMandator,
      doesCurrencyExist: this.currencyValidatorsService.doesCurrencyIsoExist,
      isCurrencyActive: this.currencyValidatorsService.isCurrencyIsoActive,
      isCostTypeRelatedToBookingAccount: this.costTypeValidatorService.isCostTypeRelatedToBookingAccount,
      doesCostTypeExist: this.costTypeValidatorService.doesCostTypeExist,
      doesCostCenterExist: this.costCenterHelper.doesCostCenterExist,
      isCostCenterActive: this.costCenterHelper.isCostCenterActive,
      doesCostUnitExist: this.costUnitHelper.doesCostUnitExist,
    };

    const customFormat = {
      debitCredit: this.debitCreditCustomFormat,
      discount: this.discountCustomFormat
    };

    const templates = {
      accountNoIsNotRelatedTemplate: this.accountNoIsNotRelatedTemplate,
      bookingCodeIsNotRelatedTemplate: this.bookingCodeIsNotRelatedTemplate,
      discountDiffersWithPaymentDateTemplate: this.discountDiffersWithPaymentDateTemplate
    };

    this.gridConfig = getBookinglineGridConfig(actions, validations, templates, customFormat);
    this.customizeGridConfigOnReadMode();

    this.customValidation = (): boolean => {
      if (this.readMode) {
        return true;
      }
      if (this.bookingFormService.bookingFormSubject.value) {
        return !this.bookingFormService.bookingFormSubject.value.dirty && this.isAnyFormDirty();
      }
      return this.isAnyFormDirty();
    };
  }

  private customizeGridConfigOnReadMode() {
    if (this.readMode) {
      this.gridConfig.editForm.disableEditColumns = true;
      this.gridConfig.disableDragAndDrop = false;
      this.gridConfig.disableHideColumns = false;
      this.gridConfig.gridName = 'viewBookingLinesGrid';
      this.gridConfig.toolbarButtons = [];
    }
  }

  public debitCreditCustomFormat = (debitCredit: number) => {
    return this.getPlusMinusValue(debitCredit);
  }

  public discountCustomFormat = (row: IBookingLine) => {
    return row.discount !== undefined && row.discount !== null
      ? `${row.discount}% / ${new ToCurrentDateFormatPipe(this.settingsService).transform(row.paymentDate)}`
      : null;
  }

  private onDebitCreditTypeChange = (form: FormGroup) => {
    const plusMinusNewValue = this.getPlusMinusValue(+form.get('debitCreditType').value);
    form.get('+/-').patchValue(plusMinusNewValue, { emitEvent: false });
  }

  private calculatePlusMinusSignAndAmountLocal = (form: FormGroup): void => {
    if (this.bookingGrid) {
      let sum = 0;

      this.bookingGrid.formsDataSubject.value.forEach((nvForm, index) => {

        const amountLocal = nvForm.form.get('amountLocal').value;

        if (amountLocal) {
          if (
            this.bookingFormService.getLeadingAccountValue()
            && index === 0
          ) {
            return false;
          }
          return sum += nvForm.form.get('debitCreditType').value === 1
            ? +amountLocal
            : -amountLocal;
        }
      });
      sum = +sum.toFixed(2);

      const debitCreditType = sum === 0
        ? null
        : sum > 0
          ? -1
          : 1;

      const plusMinusColumn = this.gridConfig.columns
        .find(column => column.key === '+/-');
      const debitCreditTypeColumn = this.gridConfig.columns
        .find(column => column.key === 'debitCreditType');
      const currencyIsoCodeColumn = this.gridConfig.columns
        .find(column => column.key === 'currencyIsoCode');
      const amountLocalColumn = this.gridConfig.columns
        .find(column => column.key === 'amountLocal');
      const exchangeRateColumn = this.gridConfig.columns
        .find(column => column.key === 'exchangeRate');
      const amountTransactionColumn = this.gridConfig.columns
        .find(column => column.key === 'amountTransaction');

      const plusMinusNewValue = this.getPlusMinusValue(debitCreditType);

      this.behavioursService.setDefaultColumnValue(plusMinusNewValue, plusMinusColumn);
      this.behavioursService.setDefaultColumnValue(debitCreditType, debitCreditTypeColumn);
      this.behavioursService.setDefaultColumnValue(Math.abs(sum), amountLocalColumn);
      const rate = this.behavioursService.getDefaultColumnValue(exchangeRateColumn);
      const defaultAmountTC = +(Math.abs(Number(rate) * Number(sum)).toFixed(2));
      this.behavioursService.setDefaultColumnValue(defaultAmountTC, amountTransactionColumn);

      const firstForm: FormGroup = this.firstForm;
      if (
        firstForm !== form
        && this.bookingFormService.getLeadingAccountValue()
      ) {
        firstForm.get('amountTransaction').patchValue(null);
        firstForm.get('amountLocal').patchValue(Math.abs(sum));
        firstForm.get('+/-').patchValue(plusMinusNewValue);
      }
    }
  }

  private validateBookingInvoiceMode(booking: IBooking): Observable<boolean> {
    if (!this.createMode || !this.bookInvoiceMode) {
      return of(true);
    }

    /**
      When mandator has no relation to the creditors account, when the user tries to
      save the booking invoice mode, the message should come up,
      “Account is not yet related to chosen mandator, create relation?
    */
    const firstLine = booking.bookingLines[0];

    const firstBookingLineMandatorAccount: IMandatorAccount = this.mandatorAccountRepo
      .getStreamValue()
      .filter(account => account.mandatorId === firstLine.mandatorId)
      .find(account => account.bookingAccountId === firstLine.bookingAccountId);

    const firstBookingLineAccount: IBookingAccount = this.bookingAccountRepo
      .getStreamValue()
      .find(account => account.id === firstLine.bookingAccountId);


    const bookingHasValidType = isAccountingTypeValid(
      booking.accountingType,
      firstBookingLineAccount.accountingType
    );

    if (!bookingHasValidType) {
      this.confirmationService.warning({
        okButtonWording: wording.general.ok,
        cancelButtonWording: wording.general.cancel,
        wording: setTranslationSubjects(
          wording.accounting.notValidAccountingType,
          {
            SUBJECT: 'Account',
            NUMBER: accountNoModifier(firstBookingLineAccount.no),
            NAME: ACCOUNTING_TYPES[booking.accountingType]
          },
        ),
        title: wording.general.warning,
        className: 'accounting-type-warning-modal',
      });
      return of(false);
    }

    if (!firstBookingLineMandatorAccount) {
      const accountNo = accountNoModifier(firstBookingLineAccount.no);
      const subject: IWording = {
        en: accountNo,
        de: accountNo,
      };
      return this.confirmationService.confirm({
        okButtonWording: wording.general.relate,
        cancelButtonWording: wording.general.cancel,
        wording: wording.accounting.accountNotYetRelated,
        subjects: { subject },
      })
        .pipe(
          switchMap(confirmed => {
            if (confirmed) {
              return this.mandatorAccountRepo
                .createMandatorAccounts(firstLine.mandatorId, [firstLine.bookingAccountId])
                .pipe(map(() => true));
            } else {
              return of(false);
            }
          })
        );
    }

    return of(true);
  }

  private validateBooking(booking: IBooking): Observable<boolean> {
    if (!this.validateAccrualAccountingSetting(booking)) {
      this.confirmationService.error({
        okButtonWording: wording.general.ok,
        wording: wording.accounting.atLeastOnePLAccountMustBeChecked,
        title: wording.general.error,
        className: 'accounting-type-warning-modal',
      });
      return of(false);
    }

    return this.validateBookingInvoiceMode(booking);
  }

  private resetAfterSave(booking: IBooking) {
    this.bookingGrid.clearUnsavedChanges();
    if (this.createMode) {
      this.bookingLinesSubject.next([]);
      this.bookingFormFields.resetForm();
      this.firstForm.markAsPristine();
      const logInfo: ILogCreate = {
        message: setTranslationSubjects(
          wording.accounting.voucherNoCreated,
          { NUMBER: booking.voucherNumber },
        ),
        subject: 'Booking',
      };
      this.activityLogsService.addLog(logInfo);
      this.accountBalanceMainService.reset();

      if (this.bookInvoiceMode) {
        const bookingPath = url(sitemap.accounting.children.booking.children.bookings);
        window.location.href = `${bookingPath}/${booking.id}?readOnly=true`;
      }
    } else {
      this.bookingFormService.bookingFormSubject.value.markAsPristine();
      this.markBookingGridFormsAsPristine();
    }
  }

  private saveBooking = (): NvAction => {
    return ({
      action$: () => {
        const booking: IBooking = this.prepareBooking();

        return this.validateBooking(booking)
          .pipe(switchMap((isValidated: boolean) => {
            if (!isValidated) {
              return of(null);
            }

            if (this.createMode) {
              return this.bookingRepo.create(booking, {});
            } else {
              return this.bookingRepo.update(booking, {});
            }
          }));
      },
      subscribe: (booking: IBooking) => {
        if (booking === null) {
          return;
        }
        this.resetAfterSave(booking);
      }
    });
  }

  private prepareBooking(): IBooking {
    const bookingForm = this.bookingFormService.bookingFormSubject.value;
    const lines: IBookingLineRow[] = this.bookingGrid.rawRows;
    const booking: IBooking =
      ({
        accountingType: bookingForm.get('accountingType').value,
        defineInvoice: bookingForm.get('defineInvoice').value,
        mandatorId: bookingForm.get('mandatorId').value,
        id: this.createMode ? null : +this.currentBookingId,
        _type: 'Booking',
        invoiceDate: bookingForm.get('invoiceDate').value,
        leadingAccount: bookingForm.get('leadingAccount').value,
        accrualAccounting: bookingForm.get('accrualAccounting').value,
        period: bookingForm.get('period').value,
        serialNo: bookingForm.get('serialNo').value
          ? bookingForm.get('serialNo').value
          : null,
        bookingLines: [],
        voucherTypeId: bookingForm.get('voucherTypeId').value,
        voucherDate: bookingForm.get('voucherDate').value,
        year: bookingForm.get('year').value,
        invoiceLedgerId: this.bookInvoiceMode
          ? this.invoiceLedgerId
          : null
      });

    lines.forEach((row: IBookingLine, index) => {
      const bookingLine: IBookingLine = {
        id: !this.createMode && Number(row.id) ? row.id : null,
        amountTransaction: row.amountTransaction,
        bookingAccountId: row.bookingAccountId,
        amountLocal: row.amountLocal,
        bookingCodeId: row.bookingCodeId,
        bookingId: this.createMode ? null : +this.currentBookingId,
        currencyId: row.currencyId,
        debitCreditType: row.debitCreditType,
        discount: row.discount,
        exchangeRate: row.exchangeRate,
        lineIndex: index,
        mandatorId: row.mandatorId,
        paymentDate: row.paymentDate,
        portId: row.portId,
        text: row.text,
        invoiceNo: row.invoiceNo,
        countryId: row.countryId,
        taxKeyId: row.taxKeyId,
        costTypeId: row.costTypeId,
        costCenterId: row.costCenterId,
        costUnitId: row.costUnitId,
        accrualAccounting: row.accrualAccounting,
        readOnly: !!row['_readonly'],
        _type: 'Booking',
      };

      this.createMode
        ? booking.bookingLines.push(bookingLine)
        : booking.bookingLines[index] = bookingLine;
    });

    return booking;
  }

  private validateAccrualAccountingSetting(booking: IBooking): boolean {
    return !booking.accrualAccounting || booking.bookingLines.some(bl => bl.accrualAccounting === true);
  }

  public setMandatorToFirstRow(mandator: IMandatorLookup): void {
    if (mandator && this.firstForm) {
      // Workaround: Clear mandator related object collections because they might be outdated
      // (e.g. might contain items already deleted by a different user or in a different tab)
      this.mandatorTaxKeyRepo.clearStream();
      this.mandatorBookingCodeRepo.clearStream();
      this.firstForm.get('mandatorNo').patchValue(mandator.no);
    }
  }

  public disableFirstRowAmounts(isLeadingAccount: boolean) {
    const formsData = this.bookingGrid.formsDataSubject.value;

    const fourColumns: NvColumnConfig[] = [];
    const plusMinusColumn = this.gridConfig
      .columns.find(column => column.key === '+/-');
    const amountTransactionColumn = this.gridConfig
      .columns.find(column => column.key === 'amountTransaction');
    const exchangeRateColumn = this.gridConfig
      .columns.find(column => column.key === 'exchangeRate');
    const AmountLCColumn = this.gridConfig
      .columns.find(column => column.key === 'amountLocal');

    fourColumns.push(plusMinusColumn);
    fourColumns.push(amountTransactionColumn);
    fourColumns.push(AmountLCColumn);
    fourColumns.push(exchangeRateColumn);

    if (formsData.length > 0) {
      const firstForm: FormGroup = this.firstForm;

      fourColumns.forEach(column => {
        isLeadingAccount && column
          ? firstForm.get(column.key).disable()
          : firstForm.get(column.key).enable();
      });
      this.calculatePlusMinusSignAndAmountLocal(null);
    }
  }

  public handleAccountingChanged(isAccrualAccounting: boolean) {
    this.updateAccrualAccountingColumn(isAccrualAccounting);
  }

  public updateAccrualAccountingColumn(isAccrualAccounting: boolean) {
    const accrualAccountingColumn = this.gridConfig.columns.find(column => column.key === 'accrualAccounting');

    accrualAccountingColumn.visible = isAccrualAccounting;
    this.bookingGrid.configurationService.updateVisibleColumns(
      this.bookingGrid.configurationService.getVisibleAndNotHiddenColumns(this.gridConfig.columns));
  }

  public focusFirstEditableCellInGrid() {
    this.cloneMode
      ? this.bookingGrid.focusFirstCellOfFirstRow()
      : this.bookingGrid.focusFirstCellOfLastRow();
  }

  public jumpIntoGrid() {
    this.focusFirstEditableCellInGrid();
  }

  public refreshFilteredMandatorsOnYearOrPeriodChange() {
    this.validateBookingLine('mandatorNo');
  }

  public refreshBookingLinesOnAccountingTypeChange() {
    this.validateBookingLine('mandatorNo');
    this.validateBookingLine('accountNo');
  }

  public assignBookingAccountToMandator(
    bookingAccountId: number,
    mandatorId: number
  ): NvAction {
    return ({
      action$: () =>
        this.mandatorAccountRepo.create({
          mandatorId,
          bookingAccountId: bookingAccountId,
          isOpenItem: undefined,
          isOpex: undefined,
          needsAccountPermission: undefined,
        }, {}),
      subscribe: () => { this.validateBookingLine('accountNo'); }
    });
  }

  public getPaymentConditionOfAccount(bookingAccountId: number): any[] {
    return this.bookingResourceService.paymentConditionsOfAccounts.value[bookingAccountId] || [];
  }

  public assignBookingCodeToMandator(bookingCodeId: number, mandatorId: number): NvAction {
    return ({
      action$: () => this.mandatorBookingCodeRepo.create({
        mandatorId,
        bookingCodeId,
      }, {}),
      subscribe: () => { this.validateBookingLine('bookingCodeNo'); }
    });
  }

  public validateBookingLine(columnName: string) {
    this.bookingGrid.formsDataSubject.value.forEach(formsData => {
      formsData.form.get(columnName).updateValueAndValidity({ emitEvent: false });
      formsData.changed.next(true);
    });
  }


  public isBookingSaveButtonDisabled = (): boolean => {
    const isDisabled: boolean = this.bookingFormService.bookingFormSubject.value
      && !this.bookingFormService.bookingFormSubject.value.invalid
      && this.bookingGrid.formsDataSubject.value.length > 1
      && this.bookingGrid.formsDataSubject
        .value
        .every(formData => formData.form.valid)
      && (this.bookInvoiceMode || !this.isAnyFormDirty()
      || this.bookingFormService.bookingFormSubject.value.dirty)
      ? false
      : true;
    return isDisabled;
  }

  private isAnyFormDirty(): boolean {
    return this.bookingGrid.formsDataSubject.value
      ? this.bookingGrid.formsDataSubject
        .value
        .every(nvform => !nvform.form.dirty)
      : false;
  }

  private markBookingGridFormsAsPristine(): void {
    this.bookingGrid.formsDataSubject
      .value
      .forEach(nvform => nvform.form.markAsPristine());
  }

  private subscribeToFormsDataSubject(): void {
    this.bookingGrid.formsDataSubject.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe((forms) => {
      this.updateReadonlyLines();
    });
  }

  private subscribeToFocusedCell(): void {
    this.bookingGrid.focusedCellSubject.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(focusedCell => {
      if (focusedCell && focusedCell.rowIndex > -1) {
        const row = this.bookingGrid.rawRows[focusedCell.rowIndex];
        if (row && row.accountNo) {
          const foundBookingAccount = this.bookingAccountRepo.getStreamValue()
            .find(bookingAccount => bookingAccount.no === +row.accountNo);

          this.currentAccountNo = foundBookingAccount?.no;
          this.currentAccountName = foundBookingAccount?.displayLabel;
        } else {
          this.currentAccountNo = null;
          this.currentAccountName = null;
        }
      } else {
        this.currentAccountNo = null;
        this.currentAccountName = null;
      }
    });
  }

  public updateDefaultMandatorNo = (form: FormGroup) => {
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === +form.get('mandatorId').value);

    if (foundMandator) {
      this.updateDefaultValueOfColumn('mandatorId', foundMandator.id);
      this.updateDefaultValueOfColumn('mandatorNo', foundMandator.no);

      this.behavioursService.onCurrencyChange(form);
      this.onAmountLocalChange(form);
    }
  }

  public updateDefaultCurrencyValue = () => {
    const firstRowExchangeRate: number = this.bookingGrid.rows[0].exchangeRate;
    const firstRowCurrencyId: CurrencyKey = this.bookingGrid.rows[0].currencyId;

    const foundCurrency: ICurrency = this.currencyRepo.getStreamValue()
      .find(currency => currency.id === firstRowCurrencyId);

    if (foundCurrency) {
      // Check if more than one currencies are present on Grid
      const multipleCurrenciesOnGrid: boolean = this.bookingGrid && this.bookingGrid.rows.length > 1
        && this.bookingGrid.rows.some(e => e.currencyIsoCode !== foundCurrency.isoCode);
      const mandatorNoColumn: NvColumnConfig = this.gridConfig.columns
        .find(column => column.key === 'mandatorNo');
      const mandatorNo = this.behavioursService.getDefaultColumnValue(mandatorNoColumn);

      const foundDefaultCurrency: ICurrency = this.findLocalCurrency(mandatorNo);
      if (foundDefaultCurrency) {
        if (multipleCurrenciesOnGrid) {
          if (foundDefaultCurrency.id === foundCurrency.id) {
            this.updateDefaultValueOfColumn('currencyId', foundDefaultCurrency.id);
            this.updateDefaultValueOfColumn('currencyIsoCode', foundDefaultCurrency.isoCode);
            this.updateDefaultValueOfColumn('exchangeRate', 1);
            this.enableDisableRateAmountLocal(true);
          } else {
            this.updateDefaultValueOfColumn('currencyId', foundCurrency.id);
            this.updateDefaultValueOfColumn('currencyIsoCode', foundCurrency.isoCode);
            this.getRate(foundCurrency, foundDefaultCurrency);
            this.enableDisableRateAmountLocal(false);
          }
        } else {
          this.updateDefaultValueOfColumn('currencyId', foundCurrency.id);
          this.updateDefaultValueOfColumn('currencyIsoCode', foundCurrency.isoCode);
          if (foundDefaultCurrency.id === foundCurrency.id) {
            this.updateDefaultValueOfColumn('exchangeRate', 1);
            this.enableDisableRateAmountLocal(true);
          } else {
            this.updateDefaultValueOfColumn('exchangeRate', firstRowExchangeRate);
            this.enableDisableRateAmountLocal(false);
          }
        }
      }
    }
  }

  public getRate = (currency: ICurrency, defaultCurrency: ICurrency) => {
    this.exchangeRateService.exchangeRates
      .pipe(
        skipWhile((res) => !res.length),
        take(1),
      )
      .subscribe(() => {
        let rate: number;
        const exchangeRateUserCurr: IExchangeRateFull = this.exchangeRateService.getRate(currency.id);
        let exchangeRateLC = 1;
        const rateLC: IExchangeRateFull = this.exchangeRateService.getRate(defaultCurrency.id);
        if (rateLC) {
          exchangeRateLC = rateLC.rate;
        }
        if (exchangeRateUserCurr && exchangeRateLC) {
          if (rateLC && rateLC.isoCode === 'EUR') {
            rate = (exchangeRateUserCurr.rate * exchangeRateLC);
          } else {
            rate = (exchangeRateUserCurr.rate / exchangeRateLC);
          }
        }
        this.updateDefaultValueOfColumn('exchangeRate', rate);
      });
  }

  public enableDisableRateAmountLocal = (disabledValue: boolean) => {
    const rateColumn = this.gridConfig.columns
      .find(column => column.key === 'exchangeRate');
    const amountLocalColumn = this.gridConfig.columns
      .find(column => column.key === 'amountLocal');

    rateColumn.editControl.disabled = disabledValue;
    amountLocalColumn.editControl.disabled = disabledValue;
  }


  public updateDefaultExchangeRateValue = () => {
    const firstRowCurrencyId: CurrencyKey = this.bookingGrid.rows[0].currencyId;
    const firstRowExchangeRate = this.bookingGrid.rows[0].exchangeRate;
    const firstRowMandatorNo: number = this.bookingGrid.rows[0].mandatorNo;
    const foundFirstRowDefaultCurrency: ICurrency = this.findLocalCurrency(firstRowMandatorNo);

    if (foundFirstRowDefaultCurrency) {
      // Check if more than one currencies are present on Grid
      const multipleCurrenciesOnGrid: boolean = this.bookingGrid && this.bookingGrid.rows.length > 1
        && this.bookingGrid.rows.some(e => e.currencyId !== firstRowCurrencyId);

      if (multipleCurrenciesOnGrid || firstRowCurrencyId !== foundFirstRowDefaultCurrency.id) {
        this.updateDefaultValueOfColumn('exchangeRate', 1);
      } else {
        this.updateDefaultValueOfColumn('exchangeRate', firstRowExchangeRate);
      }
    }
  }

  public updateDefaultValueOfColumn = (columnName: string, value: any) => {
    const foundColumn = this.gridConfig.columns
      .find(column => column.key === columnName);
    if (foundColumn) {
      foundColumn.editControl.defaultValue = value;
    }
  }

  public getPlusMinusValue(debitCreditType: number): string {
    return debitCreditType === 1
      ? this.settingsService.debitCredit === DEBIT_CREDIT.plusMinus
        ? '+'
        : this.wording.accounting.DEBIT[this.settingsService.language]
      : debitCreditType === -1
        ? this.settingsService.debitCredit === DEBIT_CREDIT.plusMinus
          ? '-'
          : this.wording.accounting.CREDIT[this.settingsService.language]
        : null;
  }

  private subscribeToFirstForm(): void {
    const firstForm: FormGroup = this.firstForm;
    firstForm.get('mandatorId').valueChanges
      .pipe(
        takeUntil(this.componentDestroyed$)
      ).subscribe(() => {
        this.updateCurrentBalance();
      });

    firstForm.get('bookingAccountId').valueChanges.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(() => {
      this.updateCurrentBalance();
    });
  }

  private predefineTextFromAccountChange(form: FormGroup) {
    const textColumn: NvColumnConfig = this.gridConfig.columns
      .find(column => column.key === 'text');

    const currentDefaultText: string = this.behavioursService.getDefaultColumnValue(textColumn) || '';

    if (form.value.id === this.firstForm.value.id) {
      const accountNo: number = +form.get('accountNo').value;
      const foundBookingAccount = this.bookingAccountRepo.getStreamValue()
        .find(bookingAccount => bookingAccount.no === accountNo);

      if (foundBookingAccount && foundBookingAccount.debitorCreditorType !== DEBITOR_CREDITOR.NONE) {
        const foundDebitorCreditor = this.debitorCreditorRepo.getStreamValue()
          .find(debitorCreditor => foundBookingAccount.debitorCreditorType === DEBITOR_CREDITOR.DEBITOR
            ? foundBookingAccount.id === debitorCreditor.debitorAccountId
            : foundBookingAccount.id === debitorCreditor.creditorAccountId);

        const startEndRange: IStartEndRange<DEBITOR_CREDITOR> = this.globalSettings.system.debitorCreditorRanges
          .find(range => range.type === foundBookingAccount.debitorCreditorType);

        const isValid: boolean = foundDebitorCreditor && startEndRange
          && this.isInRange(accountNo, startEndRange);

        if (isValid) {
          const foundCompany = this.companyRepo.getStreamValue()
            .find(company => company.id === foundDebitorCreditor.companyId);

          if (foundCompany) {
            this.updateDefaultValueOfColumn('text', foundCompany.companyName);
            this.bookingGrid.formsDataSubject.value.forEach(b => {
              if (!b.form.get('text').value || b.form.get('text').value === currentDefaultText) {
                b.form.get('text').setValue(foundCompany.companyName);
              }
            });
          }
        }
      }
    }
  }

  private onTextChange(form: FormGroup) {
    this.predefineTextFromTextChange(form);

    this.updateReadonlyLines();
  }

  private onMandatorChange(
    form: FormGroup,
    bookInvoiceMode: boolean = false,
    bookingId: BookingKey = null) {

    this.behavioursService.onMandatorChange(form, bookInvoiceMode, bookingId);
    this.updateReadonlyLines();
  }

  private predefineTextFromTextChange(form: FormGroup) {
    const textColumn: NvColumnConfig = this.gridConfig.columns
      .find(column => column.key === 'text');

    const currentDefaultText: string = this.behavioursService.getDefaultColumnValue(textColumn) || '';

    if (form.value.id === this.firstForm.value.id) {
      const accountNo: number = +form.get('accountNo').value;
      const foundBookingAccount = this.bookingAccountRepo.getStreamValue()
        .find(bookingAccount => bookingAccount.no === accountNo);

      if (foundBookingAccount && foundBookingAccount.debitorCreditorType === DEBITOR_CREDITOR.NONE) {

        const startEndRange = this.globalSettings.system.accountingRelationRanges
          .find(range => range.type === foundBookingAccount.accountingRelation);

        const isValid: boolean = startEndRange && this.isInRange(accountNo, startEndRange);

        if (isValid) {
          const newDefaultText: string = form.get('text').value;
          this.updateDefaultValueOfColumn('text', newDefaultText);
          this.bookingGrid.formsDataSubject.value.forEach(b => {
            if (!b.form.get('text').value || b.form.get('text').value === currentDefaultText) {
              b.form.get('text').setValue(newDefaultText);
            }
          });
        }
      }
    }
  }

  private isInRange(accountNo: number, range: IStartEndRange<any>) {
    return accountNo >= range.start && accountNo <= range.end;
  }

  private updateCurrentBalance(sendUpdateEvent: boolean = false): void {
    const firstForm: FormGroup = this.firstForm;
    this.accountBalanceMainService.updateCurrentBalance(
      +firstForm.get('mandatorId').value,
      +firstForm.get('bookingAccountId').value,
      +firstForm.get('currencyId').value,
      +this.bookingFormService.bookingFormSubject.value.get('year').value,
      this.bookingGrid.rows, sendUpdateEvent, this.cloneMode
    );
  }

  public updateDiffer = (): void => {
    this.accountBalanceMainService
      .updateDiffer(this.bookingGrid.formsDataSubject.value.map(nvForm => nvForm.form));
  }

  public get firstForm(): FormGroup {
    return this.bookingGrid && this.bookingGrid.formsDataSubject.value.length > 0
      ? this.bookingGrid.formsDataSubject.value[0].form
      : null;
  }

  public get isNotLocalCurrency(): boolean {
    const firstForm: FormGroup = this.firstForm;
    if (!firstForm?.get('currencyId')?.value) {
      return false;
    }
    const mandatorId: MandatorKey = +firstForm.get('mandatorId').value;
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === mandatorId);
    return foundMandator?.currencyId !== +firstForm.get('currencyId').value;
  }

  private setupCloneFromInvoiceLedger(invoiceLedgerRow?: IInvoiceLedgerRow) {
    const invoiceLedger = invoiceLedgerRow
      ? invoiceLedgerRow
      : {
        ...this.route.snapshot.queryParams,
        hasCredit: this.route.snapshot.queryParams.hasCredit === 'true'
      } as IInvoiceLedgerRow;
    this.invoiceLedgerId = invoiceLedger.id;
    const voucherNumber = invoiceLedger.voucherNumber.toString();
    this.mandatorAccountRepo.fetchAll({}).subscribe();

    const mandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(m => m.id === +invoiceLedger.mandatorId);

    const voucherTypeREE = this.voucherTypeRepo.getStreamValue()
      .find(v => v.abbreviation === 'REE');

    const booking: IBooking = {
      mandatorId: mandator.id,
      year: invoiceLedger.year,
      voucherTypeId: voucherTypeREE.id,
      voucherNumber: null,
      voucherDate: invoiceLedger.receiptDate,
      serialNo: voucherNumber.slice(voucherNumber.length - 5),
      period: 1,
      invoiceDate: invoiceLedger.invoiceDate,
      bookingLines: [],
      accountingType: mandator.accountingType,
      defineInvoice: false,
      leadingAccount: false,
      accrualAccounting: false,
      userName: '',
      bookingDate: null,
      id: null,
      _type: null,
    };

    const foundDebitorCreditor = this.debitorCreditorRepo.getStreamValue()
      .find(debitorCreditor => debitorCreditor.id === +invoiceLedger.debitorCreditorId);

    const foundBookingAccountFromCreditorAccountNo = this.bookingAccountRepo.getStreamValue()
      .find(bookingAccount => bookingAccount.id === foundDebitorCreditor.creditorAccountId);

    const foundMandatorAccount = this.mandatorAccountRepo.getStreamValue()
      .find(mandatorAccount => mandatorAccount.id === +invoiceLedger.mandatorAccountId);

    const foundAccount: IBookingAccount = foundMandatorAccount
      ? this.bookingAccountRepo.getStreamValue()
        .find(bookingAccount => bookingAccount.id === foundMandatorAccount.bookingAccountId)
      : null;

    const foundCurrency = this.currencyRepo.getStreamValue()
      .find(currency => currency.id === +invoiceLedger.currencyId);

    const euroCurrency = this.currencyRepo.getStreamValue()
      .find(currency => currency.isoCode === 'EUR');

    // used for first line
    const bookingCode350 = this.bookingCodeRepo.getStreamValue()
      .find(bookingCode => bookingCode.no === 350);

    // used for booking code of second line
    let bookingCodeNoToFind: number;

    if (+invoiceLedger.currencyId === euroCurrency.id && mandator.currencyId === euroCurrency.id) {
      bookingCodeNoToFind = 803;
    } else if (+invoiceLedger.currencyId !== euroCurrency.id) {
      bookingCodeNoToFind = 805;
    } else {
      bookingCodeNoToFind = 803;
    }

    const foundBookingCode = this.bookingCodeRepo.getStreamValue()
      .find(bookingCode => bookingCode.no === bookingCodeNoToFind);

    const bookingLine1: IBookingLineRow = {
      invoiceNo: invoiceLedger.invoiceNumber,
      discount: isNaN(+invoiceLedger.discount) ? null : +invoiceLedger.discount,
      paymentDate: invoiceLedger.paymentDate,
      text: mandator.name,
      amountTransaction: +invoiceLedger.amount,
      '+/-': invoiceLedger.hasCredit ? '+' : '-',
      debitCreditType: invoiceLedger.hasCredit ? 1 : -1,
      mandatorId: mandator.id,
      mandatorNo: mandator.no,
      currencyId: +invoiceLedger.currencyId,
      currencyIsoCode: foundCurrency.isoCode,
      bookingAccountId: foundBookingAccountFromCreditorAccountNo.id,
      accountNo: foundBookingAccountFromCreditorAccountNo.no,
      bookingCodeId: foundBookingCode.id,
      bookingCodeNo: foundBookingCode.no,
      voyage: invoiceLedger.voyageName,
      lineIndex: 1,
      exchangeRate: null,
      portId: null,
      bookingId: null,
      countryId: null,
      taxKeyId: null,
      amountLocal: null,
      costTypeId: null,
      costCenterId: null,
      costUnitId: null,
      accrualAccounting: false,
      _type: '',
      readOnly: false,
      id: 1,
    };

    const bookingLine2: IBookingLineRow = {
      invoiceNo: invoiceLedger.invoiceNumber,
      discount: null, // first booking line doesn't get preset value
      paymentDate: new Date().toISOString(), // preset with current date
      text: invoiceLedger.companyName,
      amountTransaction: +invoiceLedger.amount,
      '+/-': invoiceLedger.hasCredit ? '-' : '+',
      debitCreditType: invoiceLedger.hasCredit ? -1 : 1,
      mandatorId: mandator.id,
      mandatorNo: mandator.no,
      currencyId: +invoiceLedger.currencyId,
      currencyIsoCode: foundCurrency.isoCode,
      bookingAccountId: foundAccount?.id,
      accountNo: foundAccount?.no,
      bookingCodeId: bookingCode350.id,
      bookingCodeNo: bookingCode350.no,
      voyage: null,
      lineIndex: 0,
      exchangeRate: null,
      portId: null,
      bookingId: null,
      countryId: null,
      taxKeyId: null,
      amountLocal: null,
      costTypeId: null,
      costCenterId: null,
      costUnitId: null,
      accrualAccounting: false,
      _type: '',
      readOnly: false,
      id: 2,
    };


    this.currentBookingDataSubject.next({
      booking,
      mandator,
      voucherType: null,
      voucherTypes: [],
    });

    const bookingLines = [bookingLine1, bookingLine2];

    if (this.bookingGrid.rows?.length) {
      this.bookingGrid.rows.forEach(r => this.bookingGrid.unsubscribeToFormValues(r.id));
    }

    this.bookingLinesSubject.next(bookingLines);
    bookingLines.forEach(bookingLine => this.bookingGrid.createNewForm(bookingLine.id));
    const firstForm: FormGroup = this.firstForm;
    const fieldsToUpdateValueAndValidity = [
      'mandatorNo',
      'currencyId',
      'currencyIsoCode',
      'exchangeRate',
      'amountTransaction',
      'discount',
      'paymentDate',
    ];

    // needed to activate behaviours/calculations
    fieldsToUpdateValueAndValidity.forEach(fieldName => {
      firstForm.get(fieldName).updateValueAndValidity();
    });
    const secondFrom = this.bookingGrid.formsDataSubject.value[1].form;
    fieldsToUpdateValueAndValidity.forEach(fieldName => {
      secondFrom.get(fieldName).updateValueAndValidity();
    });

    markControlsAsTouched(firstForm);
    markControlsAsTouched(secondFrom);

    const bookingFormDisabledFields = ['mandatorId', 'invoiceDate', 'voucherDate', 'year', 'voucherTypeId',
      'serialNo'];
    const fieldsToDisableInTwoLines = ['+/-',
      'debitCreditType', 'currencyId', 'currencyIsoCode', 'invoiceNo', 'mandatorNo'];
    const firstFormDisabledFields = ['bookingAccountId', 'accountNo', 'voyage'];
    const secondFormDisabledFields = [];

    const bookingForm = this.bookingFormService.bookingFormSubject.value;

    bookingFormDisabledFields.forEach(field => bookingForm.get(field).disable());
    firstFormDisabledFields.forEach(field => firstForm.get(field).disable());
    secondFormDisabledFields.forEach(field => secondFrom.get(field).disable());

    fieldsToDisableInTwoLines.forEach(field => {
      firstForm.get(field).disable();
      secondFrom.get(field).disable();
    });

    this.changeMandator$.next(mandator);
    // update fields after mandator change
    fieldsToUpdateValueAndValidity.forEach(fieldName => {
      firstForm.get(fieldName).updateValueAndValidity();
    });
    fieldsToUpdateValueAndValidity.forEach(fieldName => {
      secondFrom.get(fieldName).updateValueAndValidity();
    });
  }

  private showInAccountSheet = (bookingLine: IBookingLine) => {
    const bookingForm: FormGroup = this.bookingFormService.bookingFormSubject.value;
    const year: number = +bookingForm.get('year').value;
    if (year && bookingLine.mandatorId && bookingLine.bookingAccountId) {
      let params = new HttpParams();
      params = params.append('bookingAccountId', bookingLine.bookingAccountId.toString());
      params = params.append('mandatorId', bookingLine.mandatorId.toString());
      params = params.append('year', year.toString());
      window.open(`${this.accountSheetPath}?${params.toString()}`, '_blank');
    }
  }

  private takeInvoice = (): void => {
    const form: FormGroup = this.bookingFormService.bookingFormSubject.value;
    const mandatorId: MandatorKey = form.get('mandatorId').value;
    this.incomingInvoideModalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.accounting.selectIncomingInvoice[this.settingsService.language],
      nzClosable: true,
      nzFooter: null,
      nzBodyStyle: { padding: '0', 'min-width': '400px' },
      nzContent: IncomingInvoiceComponent,
      nzClassName: 'select-incoming-invoice',
      nzComponentParams: {
        closeModal: this.closeModal,
        mandatorId
      },
    });
  }

  private closeModal = (invoice?: IInvoiceLedgerRow): void => {
    if (invoice) {
      this.bookInvoiceMode = true;
      this.bookingFormService.bookInvoiceMode = true;
      this.setupCloneFromInvoiceLedger(invoice);
      const form: FormGroup = this.bookingFormService.bookingFormSubject.value;
      form.get('mandatorId').patchValue(invoice.mandatorId);
    }
    this.incomingInvoideModalRef.destroy();
  }

  public checkValidations = (): void => {
    const booking: IBooking = this.prepareBooking();
    this.bookingRepo.checkValidations(booking)
      .subscribe((bookingValidation: IBookingValidation) => {
        this.validationErrors$.next(bookingValidation.errors || []);
        this.shouldShowNoValidationErrorMessage = this.validationErrors$.value.length === 0;
        this.updateFooterAmountErrorMarker(this.validationErrors$.value);
      });
  }

  private updateFooterAmountErrorMarker = (validationErrors: INavidoHTTPResponseError[] = []) => {
    const markAmountLocalError = validationErrors.some(
      error => ['TC-balance-LC-balance-must-be-zero',
        'LC-balance-must-be-zero'].includes(error.key));
    const markAmountTransactionError = validationErrors.some(
      error => ['TC-balance-LC-balance-must-be-zero'].includes(error.key));

    const amountLocalColumn = this.gridConfig.columns
      .find(column => column.key === 'amountLocal');

    if (amountLocalColumn) {
      amountLocalColumn.footer.markAsError = markAmountLocalError;
    }

    const amountTransactionColumn = this.gridConfig.columns
      .find(column => column.key === 'amountTransaction');

    if (amountTransactionColumn) {
      amountTransactionColumn.footer.markAsError = markAmountTransactionError;
    }

    this.bookingGrid.changeDetectorRef.detectChanges();
  }

  public onAmountLocalChange = (form: FormGroup): any => {
    this.amountAndRate.handleAmountLocalChange(form);
    this.calculatePlusMinusSignAndAmountLocal(form);
    this.updateDiffer();
    this.updateFooterAmountErrorMarker();
  }

  public onRateChange = (form: FormGroup): any => {
    this.amountAndRate.handleRateChange(form);
    this.updateDefaultExchangeRateValue();
  }

  public onAmountTransactionChange = (form: FormGroup): any => {
    if (!form.get('exchangeRate').valid && !form.get('amountLocal').valid) {
      this.behavioursService.onCurrencyChange(form);
    } else {
      this.amountAndRate.handleAmountTransactionChange(form);
    }
    this.updateFooterAmountErrorMarker();
  }

  public onTaxKeyChange = (form: FormGroup) => {
    this.behavioursService.onTaxKeyChange(form);
    this.updateReadonlyLines();
  }

  private updateReadonlyLines = () => {

    if (this.readMode) {
      return;
    }

    const lines: IBookingLineRow[] = this.bookingGrid.dataSource;

    const taxReadonlyLines = this.bookingCalculatorService.getTaxReadonlyLines(lines);

    this.bookingGrid.replaceDataSourceReadOnlyRows(taxReadonlyLines);
  }

  public onCurrencyChange(form: FormGroup) {
    this.behavioursService.onCurrencyChange(form);

    this.updateReadonlyLines();
  }

  public initDatesRatesModal(form: FormGroup, emitter: EventEmitter<any>) {
    const mandatorNo: number = this.firstForm?.get('mandatorId').value;
    const localCurrency: ICurrency = this.findLocalCurrency(mandatorNo);
    if (localCurrency) {
      const currencyId: number = form.get('currencyId').value;
      const foreignCurrency: ICurrency = !currencyId
        ? localCurrency
        : this.currencyRepo.getStreamValue()
          .find(currency => currency.id === +currencyId);

      if (foreignCurrency) {
        const date: string = moment().startOf('day').format('YYYY-MM-DD');
        this.fetchCaculatedRates(date, foreignCurrency.id, localCurrency.id)
          .pipe(
            takeUntil(this.componentDestroyed$)
          ).subscribe(rates => {
            this.showDatesRatesModal(form, emitter, rates);
          });
      } else {
        this.showDatesRatesModal(form, emitter, []);
      }
    } else {
      this.showDatesRatesModal(form, emitter, []);
    }
  }

  private showDatesRatesModal(form: FormGroup, emitter: EventEmitter<any>, rates: IDateRate[] = []) {
    this.datesRatesModalRef = this.modalService.create({
      nzFooter: null,
      nzWidth: 'fit-content',
      nzTitle: wording.general.rates[this.settingsService.language],
      nzClosable: true,
      nzContent: DatesRatesTableComponent,
      nzComponentParams: {
        rowSelected: (row: IDateRate) => row
          ? this.datesRatesRowSelected(form, row.rate, emitter)
          : this.datesRatesModalRef.destroy(),
        rates: rates || [],
        selectable: true
      },
      nzOnCancel: () => this.datesRatesModalRef.destroy(),
      nzWrapClassName: 'dates-rates-modal'
    });
  }

  private fetchCaculatedRates(date: string, currencyId: number, baseCurrencyId: number): Observable<IExchangeRateCalculated[]> {
    return this.exchangeRateRepo.fetchCalculatedRates({
      queryParams: {
        startDate: '1990-01-01',
        endDate: date,
        currencyId: currencyId,
        baseCurrencyId: baseCurrencyId,
      }
    });
  }

  private findLocalCurrency(mandatorId: number): ICurrency {
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === mandatorId);

    if (foundMandator) {
      const foundLocalCurrency: ICurrency = this.currencyRepo.getStreamValue()
        .find(currency => currency.id === +foundMandator.currencyId);
      return foundLocalCurrency;
    }
    return;
  }

  private datesRatesRowSelected = (form: FormGroup, rate: number, eventEmitter: EventEmitter<boolean>) => {
    form.get('exchangeRate').patchValue(rate);

    const multipleCurrenciesOnGrid: boolean = this.bookingGrid && this.bookingGrid.rows.length > 1
      && this.bookingGrid.rows.some(e => e.currencyIsoCode !== this.bookingGrid.rows[0].isoCode);

    if (!multipleCurrenciesOnGrid) {
      this.updateDefaultValueOfColumn('exchangeRate', rate);
    }
    this.datesRatesModalRef.destroy();
    eventEmitter.emit(true);
  }

  public emitBlur(eventEmitter: EventEmitter<FocusEvent>, event: FocusEvent): void {
    if (!this.datesRatesModalRef) {
      eventEmitter.emit(event);
    }
  }
}
