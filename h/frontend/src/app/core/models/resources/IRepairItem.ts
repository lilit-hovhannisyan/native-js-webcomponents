import { IResource } from './IResource';
import { IArticle } from './IArticle';
import { IArticleUnit } from './IArticleUnit';
import { ICurrency } from './ICurrency';
import { IWording } from './IWording';

export type ShipyardTypeKey = number;

interface ISharedProps {
  articleId: number;
  quantity: number;
  total?: number;
  pricePerUnit: number;
  stevedoreDamageRepairId: number;
}

export interface IRepairItemBase extends ISharedProps, IResource<number> { }

export interface IRepairItemCreate extends ISharedProps { }

export interface IRepairItemDTO extends IRepairItemBase { }

export interface IRepairItem extends IRepairItemDTO { }

export interface IRepairItemFull extends IRepairItem {
  article: IArticle;
  articleUnit: IArticleUnit;
  currency: ICurrency;
}

export interface IRepairItemRow extends IRepairItemFull {
  articleName: IWording;
  unitName: IWording;
  pricePerUnit: number;
  isoCode: string;
  code: string;
}
