import { IRoleDTO, IRoleCreate, RoleKey } from './../../models/resources/IRole';
import { IRole } from '../../models/resources/IRole';
import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { getLabelKeyByLanguage, sortByKey } from '../../helpers/general';
import { SettingsService } from '../settings.service';
import { HttpPatch } from '../../models/http/http-patch';
import { map, tap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';
import { IResourceCollection } from '../../models/resources/IResourceCollection';

@Injectable({
  providedIn: 'root'
})
export class RoleRepositoryService extends AbstractRepository<
IRole,
IRoleDTO,
IRoleCreate,
RepoParams<IRole>,
RoleKey
>  {
  protected resourceType = 'Role';
  public sortAsce = true;
  public sortKey = getLabelKeyByLanguage(this.settingsService.language);
  constructor(
    repositoryService: RepositoryService,
    private settingsService: SettingsService,
  ) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/roles/';
  }

  protected postProcess(item: IRoleDTO): IRole { return item; }

  public updateRange(roles: IRole[]) {
    const request = new HttpPatch(this.getBaseUrl(), roles);
    const request$ = this.repositoryService.httpClient.request(request).pipe(
      map((response: HttpResponse<IDataResponseBody<IResourceCollection<IRole>>>) => response.body.data.items),
      tap((entities: IRole[]) => this.addToStream(entities)),
      tap(() => { this.streamSubject.getValue().sort(sortByKey('id', false)); })
    );
    return request$;
  }

}
