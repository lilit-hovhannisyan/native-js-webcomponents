import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { wording } from 'src/app/core/constants/wording/wording';
import { Validators } from '../../../../core/classes/validators';
import { validateAllFormFields } from '../../../helpers/validateAllFormFields';
import { CalendarsRepositoryService } from '../../../../core/services/repositories/calendars-repository.service';
import {AuthenticationService} from '../../../../authentication/services/authentication.service';



@Component({
  selector: 'nv-calendar-modal',
  templateUrl: './calendar-modal.component.html',
  styleUrls: [ './calendar-modal.component.scss' ]
})
export class CalendarModalComponent implements OnInit {
  @Input() public closeModal: () => void;

  public wording = wording;
  public form: FormGroup;
  public isSuperAdmin = this.authService.getSessionInfo().user.isSuperAdmin;

  constructor(
    private nzModalComponent: NzModalRef,
    private fb: FormBuilder,
    private formBuilder: FormBuilder,
    private calendarsRepo: CalendarsRepositoryService,
    private authService: AuthenticationService,
  ) { }

  public ngOnInit(): void {
    this.form = this.formBuilder.group({
      labelDe: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      labelEn: [null, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      isSystemDefault: false
    });
  }

  public createCalendar(): void {
    validateAllFormFields(this.form);
    if (!this.form.valid) {
      return;
    }

    const value = this.form.value;
    this.calendarsRepo.create(value, {}).subscribe(() => this.closeModal());
  }
}
