import { IUserToken } from './IUserToken';

export interface IPasswordReset extends IUserToken {
  password: string;
}
