import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'me-table-string',
  templateUrl: './table-string.component.html',
  styleUrls: ['./table-string.component.scss']
})
export class TableStringComponent implements OnInit {
  @Input() data!: string;
  @Output() tableDataChangeEvent = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  onClick() {
    this.tableDataChangeEvent.emit();
  }
}
