import { Component, ChangeDetectionStrategy, Input, OnChanges, OnInit } from '@angular/core';
import { NvGridConfig} from 'nv-grid';
import { Observable } from 'rxjs';
import { ConfirmationService } from '../../../../../../../../core/services/confirmation.service';
import { GridConfigService } from '../../../../../../../../core/services/grid-config.service';
import { getAccountCostTypeGridConfig } from '../../../../../../../../core/constants/nv-grid-configs/account-cost-type.config';
import { AuthenticationService } from '../../../../../../../../authentication/services/authentication.service';
import { AccountCostTypeRepositoryService } from '../../../../../../../../core/services/repositories/account-cost-type-repository.service';
import { AccountCostTypeMainService } from '../../../../../../../../core/services/mainServices/account-cost-type-main.service';
import { BookingAccountKey } from '../../../../../../../../core/models/resources/IBookingAccount';
import { DefaultModalOptions } from '../../../../../../../../core/constants/modalOptions';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { SettingsService } from '../../../../../../../../core/services/settings.service';
import { wordingAccounting } from '../../../../../../../../core/constants/wording/accounting';
import { wording } from '../../../../../../../../core/constants/wording/wording';
import { setTranslationSubjects } from '../../../../../../../../shared/helpers/general';
import { CostAccountingRelationDetailViewComponent } from '../detail-view/detail-view.component';
import { ACCOUNTING_RELATION_CATEGORY } from '../../../../../../../../core/models/enums/accounting-relation-category';
import { IAccountRelatedCostType } from '../../../../../../../../core/models/resources/IAccountCostType';

@Component({
  selector: 'nv-cost-accounting-relation-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CostAccountingRelationListViewComponent implements OnInit, OnChanges {
  @Input() public bookingAccountId: BookingAccountKey;
  @Input() public accountingRelationCategory: ACCOUNTING_RELATION_CATEGORY;
  public dataSource$: Observable<IAccountRelatedCostType[]>;
  public costTypeGridConfig: NvGridConfig;
  public relatedCt: IAccountRelatedCostType[];

  private modalRef?: NzModalRef;
  public title = wordingAccounting.costAccounting;

  constructor(
    public gridConfigService: GridConfigService,
    private accountCostTypeMainService: AccountCostTypeMainService,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private modalService: NzModalService,
    private settingService: SettingsService,
  ) { }

  public ngOnInit(): void {
    this.dataSource$ = this.accountCostTypeMainService.bookingIdRelatedCostTypeList$;

    const gridActions = {
      edit: () => { },
      delete: (row: IAccountRelatedCostType) => this.deleteCostTypeRelation(row),
      add: () => this.openDetailViewModal()
    };

    const canPerform = (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation);
    this.costTypeGridConfig = getAccountCostTypeGridConfig(
      false,
      gridActions,
      canPerform
    );
  }

  public ngOnChanges(): void {
    /**
     * getStream is called in this hook, because it's dependant from 2 values
     * of parent component, which can be dynamically changed
     */
    this.accountCostTypeMainService.getStream({
      bookingAccountId: this.bookingAccountId,
      accountingRelationCategory: this.accountingRelationCategory
    }).subscribe( relCt => {
      this.relatedCt = relCt;
    });
  }

  private openDetailViewModal = (bookingAccountKey?: BookingAccountKey): void => {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wordingAccounting.costAccounting[this.settingService.language],
      nzClosable: true,
      nzContent: CostAccountingRelationDetailViewComponent,
      nzComponentParams: {
        closeModal: this.closeModal,
        bookingAccountId: this.bookingAccountId,
        accountingRelationCategory: this.accountingRelationCategory,
      },
      nzFooter: null
    });
  }

  private closeModal = (): void => {
    this.modalRef.destroy();
  }

  private deleteCostTypeRelation(accountRelatedCostType: IAccountRelatedCostType): void {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: setTranslationSubjects(
        wording.general.areYouSureYouWantToDelete,
         { subject: accountRelatedCostType.displayLabel },
      ),
    }).subscribe(confirmed => {
      if (confirmed) {
        this.accountCostTypeMainService.removeFromCostTypeRelationList(accountRelatedCostType.id);
      }
    });
  }
}
