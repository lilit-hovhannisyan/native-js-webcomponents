import { Component, OnInit, HostListener, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { SitemapNode, sitemap } from '../../../core/constants/sitemap/sitemap';
import { getCategoryByUrl, getSectionByUrl, SitemapEntry, url } from '../../../core/constants/sitemap/sitemap-entry';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd/dropdown';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { ITopic } from 'src/app/core/models/resources/ITopic';
import { BookmarkRepositoryService } from '../../../core/services/repositories/bookmark-repository.service';
import { IUser } from 'src/app/core/models/resources/IUser';
import { IBookmarkFull } from 'src/app/core/models/resources/IBookmark';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';


@Component({
  selector: 'nv-navigation-board',
  templateUrl: './navigation-board.component.html',
  styleUrls: ['./navigation-board.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationBoardComponent implements OnInit, OnDestroy {
  public url = url;
  public category: SitemapEntry;
  public sections: SitemapEntry[] = [];
  public topics: { [sectionKey: string]: ITopic[] } = {};
  public bookmarks: IBookmarkFull[] = [];
  public contextMenuOpenedForTopic: SitemapNode;
  public user: IUser;
  private componentDestroyed$ = new ReplaySubject<boolean>(1);

  /**
   * Used to hide tooltip when context menu is open
   */
  public isContextMenuOpen = false;
  public timeOutExceeded = true;
  public clickedBookmarkPath: string;
  constructor(
    private nzContextMenuService: NzContextMenuService,
    private authService: AuthenticationService,
    private router: Router,
    private bookmarkRepo: BookmarkRepositoryService,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit() {
    this.user = this.authService.getUser();
    const categoryKey = getCategoryByUrl(this.router.url);
    this.category = SitemapEntry.getByNode(sitemap[categoryKey]);

    const sectionKey = getSectionByUrl(this.router.url);
    if (sectionKey) {
      const childNodes = this.category.node.children[sectionKey];
      const childEntries = SitemapEntry.getByNode(childNodes);
      this.sections = [childEntries];
    } else {
      this.sections = Object.values(this.category.node.children)
        .map(node => SitemapEntry.getByNode(node))
        .filter(section => this.authService.canAccessEntry(section));
    }
    this.sections.forEach((section: SitemapEntry) => {
      const topics = Object.values(section.node.children)
        .filter(topic => this.authService.canAccessNode(topic));
      this.topics[section.key] = topics;
    });

    this.bookmarkRepo.getBookmarksWithNodes()
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(bookmarks => this.updateBookmarks(bookmarks));
  }

  private updateBookmarks(bookmarks: IBookmarkFull[]) {
    this.bookmarks = bookmarks;
    this.sections.forEach((section: SitemapEntry) => {
      this.topics[section.key]
        .forEach(topic => topic.bookmarked = !!bookmarks
          .find(b => {
            if (b.node && topic) {
              return this.getCompoundKeyOfNode(b.node)
                === this.getCompoundKeyOfNode(topic);
            }
          }));
    });
    this.cdr.detectChanges();
  }

  private toggleTopicRotation(path: string): void {
    if (this.timeOutExceeded) {
      this.clickedBookmarkPath = path;
      this.timeOutExceeded = false;
      setTimeout(() => {
        this.clickedBookmarkPath = null;
        this.timeOutExceeded = true;
      }, 1000);
    }
  }

  private getCompoundKeyOfNode(node: SitemapNode): string {
    return SitemapEntry.getByNode(node).getCompoundKey();
  }

  public toggleBookmark = (topic: ITopic) => {
    this.toggleTopicRotation(topic.path);
    topic.bookmarked = !topic.bookmarked;
    const node = SitemapEntry.getByNode(topic);
    const uniqueKey = node.getCompoundKey();
    if (topic.bookmarked) {
      const data = {
        topicKey: uniqueKey,
        userId: this.user.id,
      };
      this.bookmarkRepo.create(data, { loadingIndicatorHidden: true }).subscribe();
    } else {
      const bookmark = this.bookmarks.find(b => b.topicKey === uniqueKey);
      this.bookmarkRepo.delete(bookmark.id, {
        loadingIndicatorHidden: true
      }).subscribe();
    }
  }

  public getTopics = (section: SitemapNode): SitemapNode[] => {
    return Object.values(section.children)
      .filter(topic => this.authService.canAccessNode(topic));
  }

  public showContextMenu(
    $event: MouseEvent,
    menu: NzDropdownMenuComponent,
    topic: SitemapNode
  ): void {
    if (!this.authService.getUser().isAdmin) {
      return;
    }
    this.nzContextMenuService.create($event, menu);
    this.isContextMenuOpen = true;
    this.contextMenuOpenedForTopic = topic;
  }

  /**
   * Used to detect when contextMenu is hidden, to show tooltip again, as we hide it when
   * contextMenu is shown
   */
  @HostListener('document:click')
  public onContextMenuVisibilityChange() {
    if (this.isContextMenuOpen) {
      this.isContextMenuOpen = false;
    }
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
