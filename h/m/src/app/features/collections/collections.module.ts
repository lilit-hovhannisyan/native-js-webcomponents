import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeNewCollectionCardModule } from '@shared/me-components/me-new-collection-card/me-new-collection-card.module';
import { MeComponentLoadingModule } from '@shared/me-directives/me-component-loader/me-component-loader.module';
import { MeSearchBarModule } from '@shared/me-components/me-search-bar/me-search-bar.module';
import { MeMaterialCardModule } from '@shared/me-components/me-material-card/me-material-card.module';
import { MeScrollToBottomModule } from '@shared/me-directives/me-scroll-to-bottom.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MeEmptyContentModule } from '@shared/me-components/me-empty-content/me-empty-content.module';

import { CollectionsRoutingModule } from './collections-routing.module';
import { CollectionsComponent } from './collections.component';
import { MeIntroductionMessageModule } from '@shared/me-components/me-introduction-message/me-introduction-message.module';

@NgModule({
  declarations: [CollectionsComponent],
  imports: [
    CommonModule,
    CollectionsRoutingModule,
    MeNewCollectionCardModule,
    MeComponentLoadingModule,
    MeSearchBarModule,
    IconsModule,
    MeMaterialCardModule,
    MeEmptyContentModule,
    MeScrollToBottomModule,
    MeIntroductionMessageModule
  ]
})
export class CollectionsModule {}
