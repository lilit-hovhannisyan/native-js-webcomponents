import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { TimeCharterRepositoryService } from 'src/app/core/services/repositories/timeCharter.repository.service';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { VoyagesMainService } from 'src/app/core/services/mainServices/voyages-main.service';
import { TimeChartersHelperService } from './time-charters.helper.service';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';

@Component({
  templateUrl: './time-charter-detail-view.component.html',
  styleUrls: ['./time-charter-detail-view.component.scss'],
  providers: [TimeChartersHelperService],
})
export class TimeCharterDetailViewComponent implements OnInit, OnDestroy {
  private componentDestroyed$ = new ReplaySubject<boolean>(1);
  public creationMode: boolean;
  public voyageId: number;
  public title: string;
  public sitemap = sitemap;

  constructor(
    private route: ActivatedRoute,
    private timeCharterRepo: TimeCharterRepositoryService,
    private voyageRepo: VoyagesMainService,
    private router: Router,
    private timeChartersHelperService: TimeChartersHelperService,
  ) { }

  public get isCreationMode() {
    return this.route.snapshot.params.id === 'new';
  }

  public ngOnInit() {
    const timeCharterId = this.route.snapshot.params.id;

    if (this.isCreationMode) {
      this.voyageId = this.route.snapshot.queryParams.voyageId;
      this.voyageRepo.fetchOne(this.voyageId, {})
        .subscribe(voyage => this.timeChartersHelperService.generateTitle(voyage));
    } else {
      this.timeCharterRepo.fetchOne(timeCharterId, {})
        .subscribe(timeCharter => {
          this.voyageId = timeCharter.voyageId;
          this.voyageRepo.fetchOne(timeCharter.voyageId, {})
            .subscribe(voyage => this.timeChartersHelperService.generateTitle(voyage, timeCharter));
        });
    }

    this.timeChartersHelperService.editViewTitle$.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(title => {
      this.title = title;
    });
  }

  public routeBack = () => {
    this.router.navigate([
      url(sitemap.chartering.children.misc.children.voyages),
      this.voyageId,
    ]);
  }

  public tabDisabler = (node: SitemapNode) => {
    const basicNode = sitemap.chartering.children.misc.children.timeCharters.children.id.children.basic;

    // in creationMode just enable the basicTab
    return this.isCreationMode ? node === basicNode : true;
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
