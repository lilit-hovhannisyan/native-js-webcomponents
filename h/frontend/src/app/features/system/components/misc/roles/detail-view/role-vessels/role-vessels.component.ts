import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { orderBy } from 'lodash';
import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { getRoleVesselsGridConfig } from 'src/app/core/constants/nv-grid-configs/roles-vessels.config';
import { RoleKey } from 'src/app/core/models/resources/IRole';
import { IVesselWithAccountings, VesselKey } from 'src/app/core/models/resources/IVessel';
import { IVesselRole } from 'src/app/core/models/resources/IVesselRole';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { VesselRoleRepositoryService } from 'src/app/core/services/repositories/vessel-role.repository.service';

@Component({
  selector: 'nv-role-vessels',
  templateUrl: './role-vessels.component.html',
  styleUrls: ['./role-vessels.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoleVesselsComponent implements OnInit {

  public roleId: RoleKey;
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IVesselWithAccountings[]>;
  public editMode = false;
  private editMode$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private selectedVessels: VesselKey[] = [];

  constructor(
    private vesselMainService: VesselMainService,
    private vesselRoleRepo: VesselRoleRepositoryService,
    public gridConfigService: GridConfigService,
    public route: ActivatedRoute,
  ) { }

  public ngOnInit(): void {
    this.gridConfig = getRoleVesselsGridConfig();
    this.roleId = +this.route.parent.snapshot.params.id;

    const queryParams: { [key: string]: RoleKey } = { roleId: this.roleId };
    this.vesselRoleRepo.fetchAll({ queryParams }).subscribe();

    this.dataSource$ = combineLatest([
      this.vesselMainService.fetchWithRolesOnly({}),
      this.vesselRoleRepo.getStream(),
      this.editMode$
    ]).pipe(map(([
      vessels,
      vesselRoles,
      editMode,
    ]) => {
      this.selectedVessels = vesselRoles.map(vesselRole => vesselRole.vesselId);
      return orderBy(vessels
        .filter(vessel => editMode || this.vesselIsAssignedToRole(vessel, vesselRoles))
        .map(vessel => ({
          ...vessel,
          _checked: editMode && this.vesselIsAssignedToRole(vessel, vesselRoles)
        })), ['_checked', 'no'], ['desc', 'asc']);
    }));
  }

  public vesselIsAssignedToRole = (
    vessel: IVesselWithAccountings,
    vesselRoles: IVesselRole[],
  ): boolean => {
    return !!vesselRoles
      .filter(vesselRole => vesselRole.roleId === this.roleId)
      .find(vesselRole => vesselRole.vesselId === vessel.id);
  }

  public enterEditMode(): void {
    this.editMode = true;
    this.editMode$.next(true);
    this.toggleGridSelectionType();
  }

  public cancelEditing(): void {
    this.editMode = false;
    this.editMode$.next(false);
    this.toggleGridSelectionType();
  }

  public toggleGridSelectionType = (): void => {
    this.gridConfig = {
      ...this.gridConfig,
      rowSelectionType: this.editMode
        ? NvGridRowSelectionType.Checkbox
        : undefined
    };
  }

  public submit(): void {
    this.vesselRoleRepo.setVesselsForRole(this.selectedVessels, this.roleId)
      .subscribe(() => this.cancelEditing());
  }

  public rowSelectionChanged(vessels: IVesselWithAccountings[]): void {
    this.selectedVessels = vessels.map(vessel => vessel.id);
  }
}
