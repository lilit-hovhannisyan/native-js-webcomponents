/**
 * This file uses for adding fixtures
 */

export const environment = {
  production: false,
  apiHost: 'http://nav2-dev.westeurope.cloudapp.azure.com:8082',
  apiBasicAuth: 'navido:navido2_hamburg',
  PREFER_FIXTURES: true,
};
