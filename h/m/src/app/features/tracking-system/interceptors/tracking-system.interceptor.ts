import { Injectable } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';

import { HttpInterceptor, HttpRequest, HttpHandler, HttpResponse, HttpEvent } from '@angular/common/http';
import { finalize, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { LoggingService } from '../services/logging.service';
import RequestLogI from '../interfaces/request-log.interface';
import ResponseLogI from '../interfaces/response-log.interface';
import EventTypes from '../enums/event-types.enum';
import { SKIP_ME_TRACKING_SYSTEM_REQUEST } from '../constants';

@Injectable()
export class TrackingSystemInterceptor implements HttpInterceptor {
  constructor(private loggingService: LoggingService) {}

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // don't log request for data tracking system
    if (req.headers.has(SKIP_ME_TRACKING_SYSTEM_REQUEST)) {
      req = req.clone({ headers: req.headers.delete(String(SKIP_ME_TRACKING_SYSTEM_REQUEST)) });
      return next.handle(req);
    }

    if (this.loggingService.enabled) {
      const startTime = Date.now();
      let status: string;
      let data: unknown;
      // TODO add to the header
      const requestId = uuidv4();

      const log: RequestLogI = {
        eventType: EventTypes.REQUEST,
        requestId,
        data: {
          method: req.method,
          urlWithParams: req.urlWithParams,
          body: req.body || ''
        },
        userActionId: this.loggingService.getUserActionId()
      };
      this.loggingService.emitLog(log);

      const sessionId = this.loggingService.getSessionId();
      const orderNumber = this.loggingService.getOrderNumber();
      const modifiedReq = req.clone({
        headers: req.headers.set('sessionId', sessionId).set('orderNumber', String(orderNumber)).set('requestId', requestId)
      });

      return next.handle(modifiedReq).pipe(
        tap(
          (event) => {
            status = '';
            data = '';
            if (event instanceof HttpResponse) {
              status = 'succeeded';
              data = event.body;
            }
          },
          (error) => {
            status = 'failed';
            data = error;
          }
        ),
        finalize(() => {
          const elapsedTime = Date.now() - startTime;
          const responseLog: ResponseLogI = {
            eventType: EventTypes.RESPONSE,
            requestId,
            data: {
              method: req.method,
              params: req.urlWithParams,
              status,
              elapsedTime: `${elapsedTime}ms`,
              body: data
            }
          };
          this.loggingService.emitLog(responseLog);
        })
      );
    } else {
      return next.handle(req);
    }
  }
}
