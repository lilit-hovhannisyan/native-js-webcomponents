import { Component, OnInit, OnDestroy, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { SubSink } from 'subsink';
import { TranslateService } from '@ngx-translate/core';

import { BackOfficeService } from '../backoffice.web-service';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { TableViewService } from '@shared/me-components/me-table/table-view.service';
import { MeAddListPopupService } from '@shared/me-components/me-popup-add-list/me-popup-add-list.service';
import { UsersStoreService } from '@features/users/users-store.service';
import { GroupsStoreService } from '@features/users/groups-store.service';

import { MeTableActionButtonPropertiesI, MeTableColumnI, MeTableColumnOptionsI, MeTableDataSourceI } from '@shared/me-components/me-table';

import { DomainModel } from '@shared/models/domain/domain.model';
import { GroupModel } from '@shared/models/groups/group.model';
import { UserModel } from '@shared/models/user/user.model';
import { QuickSearchModel } from '@shared/models/search/quick-search.model';

import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { MeSearchBarI } from '@shared/me-components/me-search-bar/me-search-bar.interface';

import { ME_DOMAIN_APPLICATION_GROUP, ME_DOMAIN_APPLICATION_USER } from '@shared/constants';
import { TableComponentTypes } from '@shared/me-components/me-table/constants';
import { MeSearchDirectionTypes, MeSearchSortByTypes } from '@shared/me-search-sort-by-types';
import { getOid } from '@shared/utils';
import { UserActionsService } from './user-actions.service';
import { GlobalService } from '@core-services/global.service';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';

@Component({
  selector: 'me-users',
  templateUrl: './users-groups.component.html',
  styleUrls: ['./users-groups.component.scss'],
  providers: [MeAddListPopupService, UsersStoreService, GroupsStoreService, UserActionsService]
})
export class UsersGroupsComponent implements OnInit, OnDestroy {
  getCardOid = getOid;
  private readonly NUMBER_OF_ITEMS_ON_PAGE = 50;
  private subs = new SubSink();
  domain: DomainModel = new DomainModel();
  userTableItems: MeTableDataSourceI[] = [];
  groupTableItems: MeTableDataSourceI[] = [];
  tableColumns: MeTableColumnI[] = [];
  tableColumnOptions: MeTableColumnOptionsI[] = [];
  usersView = true;
  tableLoader = true;
  usersTrackForBottomReach = false;
  groupsTrackForBottomReach = false;
  isUserSearch = false;
  searchBarData!: MeSearchBarI;
  searchKeyword: string = '';

  _updateUserTableDataEventSubject!: Subscription;
  updateUserTableDataEventSubject = new Subject<void>();

  i18nStrings: { [key: string]: string } = {
    newest: '',
    oldest: '',
    searchFor: '',
    users: '',
    groups: '',
    statusChange: '',
    statusChangeSuccess: '',
    successfullyUpdated: '',
    usersInGroupUpdated: '',
    bookGroupAndRoles: '',
    createUser: ''
  };

  @ViewChild('createButton', { read: TemplateRef }) createButton!: TemplateRef<Component>;

  constructor(
    private backOfficeService: BackOfficeService,
    private definitionsStoreService: DefinitionsStoreService,
    private tableViewService: TableViewService,
    private usersStoreService: UsersStoreService,
    private groupsStoreService: GroupsStoreService,
    private translate: TranslateService,
    public userActionsService: UserActionsService,
    private globalService: GlobalService,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.globalService.activateLoader();
    this.subs.sink = this.translate.get(Object.keys(this.i18nStrings)).subscribe((translations) => {
      Object.keys(this.i18nStrings).forEach((key) => {
        this.i18nStrings[key] = translations[key];
      });

      this.setSearchBarData();
    });

    this.subs.sink = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.prepareTableColumns();
        this.getAllGroups();
        if (this.usersView) {
          this.getAllUsers();
          this.prepareUserTableData();
        } else {
          this.getAllGroups();
          this.prepareGroupTableData();
        }
      }
    });

    this._updateUserTableDataEventSubject = this.updateUserTableDataEventSubject.subscribe(() => this.getAllUsers());
  }

  ngAfterViewInit(): void {
    this.breadcrumbSupportService.breadcrumbActions = [
      {
        button: {
          id: '1',
          data: { color: 'primary', type: 'transparent' },
          text: this.i18nStrings.create,
          content: this.createButton
        },
        callBack: () => {
          this._router.navigate(['backoffice/group/create']);
        }
      }
    ];
  }

  setSearchBarData(): void {
    this.searchBarData = {
      filterEnabled: false,
      usersGroupsSelect: true,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: '',
        minCharacters: 1
      },
      usersGroups: {
        options: [
          { key: 'users', value: this.i18nStrings.users },
          { key: 'groups', value: this.i18nStrings.groups }
        ],
        selectedIndex: this.usersView ? 0 : 1
      },
      sort: {
        placeholder: 'Sort by',
        options: [
          { key: 'azAlphabetically', value: 'A-Z' },
          { key: 'zaAlphabetically', value: 'Z-A' },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      }
    };
  }

  getAllUsers(skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false) {
    this.globalService.activateLoader();

    this.subs.sink = this.backOfficeService
      .getBoUsers(skip, top)
      .pipe(finalize(() => this.globalService.deactivateLoader()))
      .subscribe((response) => {
        this.handleUsersResponse(response, append);
      });
  }

  getAllGroups(skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false) {
    this.subs.sink = this.backOfficeService.getBoGroups(skip, top).subscribe((response) => {
      if (response) {
        this.handleGroupsResponse(response, append);
      }
    });
  }

  prepareTableColumns() {
    const customColumns: MeTableColumnI[] = [];
    let columnOptions!: MeTableColumnOptionsI[];
    let domainInternalName: string;

    if (this.usersView) {
      domainInternalName = ME_DOMAIN_APPLICATION_USER;
      customColumns.push({
        field: 'numberOfGroups',
        headerName: 'Groups'
      });
      columnOptions = [{ forField: 'isActive', headerName: 'Status' }];
    } else {
      domainInternalName = ME_DOMAIN_APPLICATION_GROUP;
      columnOptions = [];
    }

    this.domain = this.definitionsStoreService.getDomain(domainInternalName);
    const { layouts, attributes } = this.domain;
    const columns = this.tableViewService.genereateColumns({
      layouts,
      attributes,
      customColumns
    });

    if (columns) {
      this.tableColumns = columns;
      this.tableColumnOptions = columnOptions;
    }
  }

  prepareUserTableData(): void {
    this.tableLoader = true;
    this.subs.sink = this.usersStoreService.userEntities$.subscribe((data) => {
      this.userTableItems = [];

      const cellComponents = this.tableViewService.genereateComponentsForCell(this.tableColumns, this.domain.attributes);
      const actionButtons: MeTableActionButtonPropertiesI[] = [
        {
          iconName: 'edit-2',
          type: 'edit'
        },
        {
          iconName: 'trash-2',
          type: 'delete'
        },
        {
          contextMenuItems: [
            {
              isActiveAttrType: true,
              key: '',
              value: ''
            },
            { key: 'manageGroups', value: 'Manage Groups' }
          ],
          type: 'selectedContextMenu'
        }
      ];
      const customComponents = data.map((d) => {
        return {
          rowOID: d.oid,
          cells: [
            {
              fieldName: 'numberOfGroups',
              componentType: TableComponentTypes.STRING,
              componentData: d.numberOfGroups || 0
            }
          ]
        };
      });
      const dataSource = this.tableViewService.attechComponentDataAndRowOIDToCell({
        entities: data,
        cellComponents,
        actionButtonProperties: actionButtons,
        customComponents: customComponents
      });

      this.userTableItems = dataSource;
      this.tableLoader = false;
    });
  }

  prepareGroupTableData(): void {
    this.tableLoader = true;
    this.subs.sink = this.groupsStoreService.groupEntities$.subscribe(async (data) => {
      const roles = await this.backOfficeService.getBoRoles().toPromise();
      const replacedComponents = data.map(({ attributes, oid }) => {
        return {
          rowOID: oid,
          cells: [
            {
              fieldName: 'roleRef',
              componentType: TableComponentTypes.SELECT,
              componentData: {
                displayName: 'Roles',
                internalName: 'role',
                value: attributes.roleRef,
                values: roles.entities.map((entitie: any) => ({ displayName: entitie.attributes.name, value: entitie.oid }))
              }
            }
          ]
        };
      });
      const cellComponents = this.tableViewService.genereateComponentsForCell(this.tableColumns, this.domain.attributes);
      const actionButtons: MeTableActionButtonPropertiesI[] = [
        {
          iconName: 'edit-2',
          type: 'edit'
        },
        {
          iconName: 'user-plus',
          type: 'add-user'
        }
      ];
      const dataSource = this.tableViewService.attechComponentDataAndRowOIDToCell({
        entities: data,
        cellComponents,
        actionButtonProperties: actionButtons,
        replaceComponentsByFieldName: replacedComponents
      });

      this.groupTableItems = dataSource;
      this.tableLoader = false;
    });
  }

  handleSearch(searchValue: string) {
    this.searchKeyword = searchValue;
    this.searchBarData = {
      ...this.searchBarData,
      search: {
        placeholder: this.usersView ? this.i18nStrings.users : this.i18nStrings.groups,
        keyword: this.searchKeyword as string
      }
    };

    let orderByAttr: string = '';
    let orderByDir: string = '';
    switch (this.searchBarData.sort?.options[this.searchBarData.sort.selectedIndex].key) {
      case 'azAlphabetically':
        orderByAttr = this.usersView ? MeSearchSortByTypes.lastName : MeSearchSortByTypes.name;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
      case 'zaAlphabetically':
        orderByAttr = this.usersView ? MeSearchSortByTypes.lastName : MeSearchSortByTypes.name;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'newest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
      case 'oldest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
    }

    const searchFilter: QuickSearchModel = {
      domain: this.usersView ? ME_DOMAIN_APPLICATION_USER : ME_DOMAIN_APPLICATION_GROUP,
      criteriaQuick: searchValue.trim(),
      collectionOid: '',
      orderByAttr,
      orderByDir
    };

    this.tableLoader = true;
    if (this.usersView) {
      this.subs.sink = this.backOfficeService.searchBoUsers(searchFilter, 0, this.NUMBER_OF_ITEMS_ON_PAGE).subscribe((response) => {
        this.handleUsersResponse(response);
      });
    } else {
      this.subs.sink = this.backOfficeService.searchBoGroups(searchFilter, 0, this.NUMBER_OF_ITEMS_ON_PAGE).subscribe((response) => {
        this.handleGroupsResponse(response);
      });
    }
  }

  handleSortByEvent(sortByValue: string): void {
    if (this.searchBarData.sort) {
      this.searchBarData.sort.selectedIndex = this.searchBarData.sort.options.findIndex((x) => x.key === sortByValue);
    }
    this.handleSearch(this.searchKeyword);
  }

  handleSelectUsersGroupsEvent(selectedValue: string): void {
    if (this.searchBarData.usersGroups && this.searchBarData.search) {
      this.searchBarData = {
        ...this.searchBarData,
        search: {
          ...this.searchBarData.search,
          placeholder: this.i18nStrings.searchFor,
          keyword: ''
        },
        usersGroups: {
          ...this.searchBarData.usersGroups,
          selectedIndex: this.searchBarData.usersGroups.options.findIndex((x) => x.key === selectedValue)
        }
      };
    }

    this.tableLoader = true;
    if (selectedValue === 'users') {
      this.usersView = true;
      this.prepareTableColumns();
      this.getAllUsers();
      this.prepareUserTableData();
    } else {
      this.usersView = false;
      this.prepareTableColumns();
      this.getAllGroups();
      this.prepareGroupTableData();
    }
  }

  searchUsers(searchValue: string, skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false) {
    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_USER,
      criteriaQuick: searchValue.trim() || undefined,
      orderByAttr: undefined,
      orderByDir: undefined
    };

    this.globalService.activateLoader();
    this.subs.sink = this.subs.sink = this.backOfficeService.searchBoUsers(searchFilter, skip, top).subscribe((response) => {
      this.handleUsersResponse(response, append);
      this.globalService.deactivateLoader();
    });
  }

  bottomReachedHandlerForAddList(): void {
    console.log('bottomReachedHandlerForAddList');
    if (this.usersTrackForBottomReach) {
      this.searchUsers(this.searchKeyword || '', this.usersStoreService.userNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
    }
  }

  private handleUsersResponse(response: ArrayResponseI<UserModel>, append: boolean = false, isSearch: boolean = false): void {
    this.usersStoreService.userNextId = response.nextID;
    if (append) {
      this.usersStoreService.appenduserEntities(response.entities);
    } else {
      this.usersStoreService.userEntities = response.entities;
      this.usersStoreService.userTotalCount = response.totalCount;
      this.isUserSearch = isSearch;
      this.usersTrackForBottomReach = true;
    }

    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.usersTrackForBottomReach = false;
    }
  }

  private handleGroupsResponse(response: ArrayResponseI<GroupModel>, append: boolean = false): void {
    const usersGroupRemoval: string = 'Users';
    response.entities = response.entities.filter(
      (item) => usersGroupRemoval.indexOf(item.attributes.name) && item.attributes.name.length !== 5
    );
    response.totalCount = response.totalCount - 1;
    this.groupsStoreService.groupNextId = response.nextID;

    if (append) {
      this.groupsStoreService.appendgroupEntities(response.entities);
    } else {
      this.groupsStoreService.groupEntities = response.entities;
      this.groupsStoreService.groupTotalCount = response.totalCount;
      this.groupsTrackForBottomReach = true;
    }

    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.groupsTrackForBottomReach = false;
    }
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
