import { AfterViewInit, Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { Subject, Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { MaterialsStoreService } from '@shared/services/materials-store.service';
import { MaterialsWebService } from '@shared/services/materials.web-service';
import { GlobalService } from '@core-services/global.service';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';

import { CollectionsStoreService } from '@features/collections/collections-store.service';
import { CollectionsWebService } from '@features/collections/collections.web-service';
import { BaseLoggerComponent } from '@features/tracking-system';

import {
  MARKETPLACE_BASE_API_URL,
  MATERIAL_ATTRIBUTE_UOM,
  ME_DOMAIN_APPLICATION_COLLECTION,
  ME_DOMAIN_APPLICATION_MATERIAL
} from '@shared/constants';
import { MeNewCollectionCardI } from '@shared/me-components/me-new-collection-card/me-new-collection-card.interface';
import { CollectionModel } from '@shared/models/collection/collection.model';
import { MeSearchBarI } from '@shared/me-components/me-search-bar/me-search-bar.interface';
import { QuickSearchModel } from '@shared/models/search/quick-search.model';
import { MaterialModel } from '@shared/models/materials/material-model';
import { MeMaterialCardI } from '@shared/me-components/me-material-card/me-material-card.interface';
import { MePopupSelectService } from '@shared/me-components/me-popup-select/me-popup-select.service';
import { MePopupSelectI } from '@shared/me-components/me-popup-select/me-popup-select.interface';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeEmptyContentI } from '@shared/me-components/me-empty-content/me-empty-content-interface';
import { MePopupStatusChangeI } from '@shared/me-components/me-popup-status-change/me-popup-status-change.interface';
import { MePopupStatusChangeService } from '@shared/me-components/me-popup-status-change/me-popup-status-change.service';
import { getVisibilityObject } from '@shared/utils';
import { StateModel } from '@shared/models/state/state.model';
import { MeSearchDirectionTypes, MeSearchSortByTypes } from '@shared/me-search-sort-by-types';
import { MeIntroductionMessageI } from '@shared/me-components/me-introduction-message/me-introduction-message.interface';
import { MeAttributeTypes } from '@shared/me-attribute-types';
import { AttributesService } from '@shared/services/attributes.service';
import { DomainModel } from '@shared/models/domain/domain.model';
import { AuthStoreService } from '@core-services/auth-store.service';
import { MePermissionTypes } from '@shared/me-permission-types';
import { KeyValue } from '@angular/common';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.scss'],
  providers: [
    CollectionsStoreService,
    CollectionsWebService,
    MaterialsStoreService,
    MaterialsWebService,
    MePopupSelectService,
    MePopupStatusChangeService,
    AttributesService
  ]
})
export class CollectionsComponent extends BaseLoggerComponent implements OnInit, OnDestroy, AfterViewInit {
  componentVersion: string = '0.0.1';
  gridItemsInUse: 'collections' | 'materials' = 'collections';

  collectionCardItems: MeNewCollectionCardI[] = [];
  selectionEnabled: boolean = false;
  areCollectionsLoading: boolean = true;
  searchKeyword: string | undefined = '';

  emptyContent!: MeEmptyContentI;
  isSearch: boolean = false;
  trackForBottomReach: boolean = false;
  searchBarData!: MeSearchBarI;

  private readonly NUMBER_OF_ITEMS_ON_PAGE = 50;
  private _selectedMap: Map<string, void> = new Map();

  private _getAllCollectionsSubscription!: Subscription;
  private _getCollectionDataForCollectionCardSubscription!: Subscription;
  private _getDefinitionsDataLoadedSubscription!: Subscription;
  private _searchCollectionSubscription!: Subscription;

  @ViewChild('createButton', { read: TemplateRef }) createButton!: TemplateRef<Component>;

  // Materials grid popup
  @ViewChild('materialsGrid', { read: TemplateRef }) materialsGridRef!: TemplateRef<Component>;
  materialsPopupSelect!: MePopupSelectI;
  selectedCollectionOid!: string;

  materialCardItems: MeMaterialCardI[] = [];
  areMaterialsLoading: boolean = true;
  materialsSearchKeyword: string | undefined = '';

  materialsEmptyContent!: MeEmptyContentI;
  materialsIsSearch: boolean = false;
  materialsTrackForBottomReach: boolean = false;
  materialsSearchBarData!: MeSearchBarI;
  introMsgData!: MeIntroductionMessageI;

  private _materialsSectedMap: Map<string, void> = new Map();

  private _getAllMaterialsSubscription!: Subscription;
  private _getMaterialDataForMaterialCardSubscription!: Subscription;
  private _searchMaterialSubscription!: Subscription;
  private _materialsPopupSubscription!: Subscription;
  private _addMaterialsToCollectionSubscriptions!: Subscription;
  private _sweetAletSubscription!: Subscription;
  private _deleteCollectionSubscription!: Subscription;
  private _getI18nStringsSubscription!: Subscription;
  private _getCollectionStates!: Subscription;
  private _changeCollectionStateSubscription!: Subscription;

  i18nStrings: MeTranslationI = {};

  isVisibilityEdit: boolean = false;

  constructor(
    injector: Injector,
    private _router: Router,
    private _collectionsWebService: CollectionsWebService,
    private _definitionsStoreService: DefinitionsStoreService,
    private _collectionsStoreService: CollectionsStoreService,
    private _materialsStoreService: MaterialsStoreService,
    private _materialsWebService: MaterialsWebService,
    private _popupSelectService: MePopupSelectService,
    private _basicAlertService: MeBasicAlertService,
    private _sweetAlertService: MeSweetAlertService,
    private _translate: MeTranslationService,
    private _globalService: GlobalService,
    private _breadcrumbSupportService: BreadcrumbSupportService,
    private _popupStatusChangeService: MePopupStatusChangeService,
    private _attributesService: AttributesService,
    private _authStoreService: AuthStoreService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._getI18nStringsSubscription = this._translate.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;
    });

    this._getDefinitionsDataLoadedSubscription = this._definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.getCollections();
        this.prepareCollectionCardData();
      }
    });

    this.searchBarData = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: '',
        minCharacters: 1
      },
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'azAlphabetically', value: this.i18nStrings.azAlphabetically },
          { key: 'zaAlphabetically', value: this.i18nStrings.zaAlphabetically },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      },
      selectButton: {
        enabled: false,
        placeholder: this.i18nStrings.select
      },
      operationButtons: [
        /*   {
          button: { type: 'regular', rounded: false, color: 'link' },
          iFeatherName: 'download',
          clickEventName: 'download',
          tooltipText: this.i18nStrings.download
        }, */
        {
          button: { type: 'regular', rounded: false, color: 'link' },
          iFeatherName: 'trash',
          clickEventName: 'delete',
          tooltipText: this.i18nStrings.delete
        }
      ]
    };

    this.emptyContent = {
      title: this.i18nStrings.createYourFirstCollection,
      comment: this.i18nStrings.addYourFirstCollectionDescription,
      buttonText: this.i18nStrings.add,
      emptyIconName: 'empty-collections'
    };

    this.materialsEmptyContent = {
      title: this.i18nStrings.createYourFirstMaterial,
      comment: this.i18nStrings.startCreatingDigitalVersionsOfYourMaterials,
      buttonText: this.i18nStrings.add,
      emptyIconName: 'empty-materials'
    };

    this.introMsgData = {
      content: 'Create collections to organize sets of materials, then attach any associated documents.',
      buttonText: this.i18nStrings.learnMore
    };

    this.isVisibilityEdit = this._authStoreService.isAllowed(MePermissionTypes.STATE_COLLECTION_CHANGE);
  }

  ngAfterViewInit(): void {
    this._breadcrumbSupportService.breadcrumbActions = [
      {
        button: {
          id: '1',
          data: { color: 'primary', type: 'transparent' },
          text: this.i18nStrings.create,
          content: this.createButton
        },
        callBack: () => {
          this._router.navigate(['collections/create']);
        }
      }
    ];
  }

  btnEventHandler(): void {
    window.open('https://5883447.hs-sites.com/knowledge/collections-suppliers', '_blank');
  }

  getCollections(skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.areCollectionsLoading = true;
    this._globalService.activateLoader();
    this._getAllCollectionsSubscription = this._collectionsWebService
      .getMeCollections(skip, top)
      .pipe(
        finalize(() => {
          this.areCollectionsLoading = false;
          this._globalService.deactivateLoader();
        })
      )
      .subscribe((response: ArrayResponseI<CollectionModel>) => {
        this.handleCollectionsResponse(response, append, false);
      });
  }

  prepareCollectionCardData(): void {
    this._getCollectionDataForCollectionCardSubscription = this._collectionsStoreService.collectionEntities$.subscribe(
      (data: CollectionModel[]) => {
        if (data) {
          const collectionStates: StateModel[] = this._definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_COLLECTION).states;
          this.collectionCardItems = data.map((collection: CollectionModel) => {
            return {
              selectionEnabled: this.selectionEnabled,
              selected: this._selectedMap.has(collection.oid),
              oid: collection.oid,
              collectionName: collection.attributes.name,
              numberOfMaterials: collection.numberOfMaterials,
              numberOfCustomers: collection.numberOfCustomers,
              numberOfTeams: collection.numberOfTeams,
              contextMenu: {
                contextMenuItems: this.prepareCollectionContextMenu()
              },
              thumbnail: collection.attributes.thumbnail
                ? `${MARKETPLACE_BASE_API_URL}/files/download/${collection.attributes.thumbnail}`
                : '',
              supplierName: collection.attributesExtended.ownerRef,
              supplierOid: collection.attributes.ownerRef,
              visibility: getVisibilityObject(
                collection.state,
                collectionStates.find((cs) => cs.internalName === collection.state)?.stateType
              )
            };
          });
        }
      }
    );
  }

  prepareCollectionContextMenu(): KeyValue<string, string>[] {
    // TODO. Menu model should have show/hide flags to avoid this kind of syntax.
    if (this.isVisibilityEdit) {
      return [
        {
          key: 'view',
          value: this.i18nStrings.view
        },
        {
          key: 'edit',
          value: this.i18nStrings.edit
        },
        {
          key: 'duplicate',
          value: this.i18nStrings.duplicate
        },
        {
          key: 'visibility',
          value: this.i18nStrings.visibility
        },
        {
          key: 'delete',
          value: this.i18nStrings.delete
        },
        {
          key: 'addMaterial',
          value: this.i18nStrings.addMaterials
        }
        // TODO: Not implemented
        // {
        //   key: 'share',
        //   value: 'Share'
        // },
        // {
        //   key: 'duplicate',
        //   value: 'Duplicate'
        // }
      ];
    } else {
      return [
        {
          key: 'view',
          value: this.i18nStrings.view
        },
        {
          key: 'edit',
          value: this.i18nStrings.edit
        },
        {
          key: 'duplicate',
          value: this.i18nStrings.duplicate
        },
        {
          key: 'delete',
          value: this.i18nStrings.delete
        },
        {
          key: 'addMaterial',
          value: this.i18nStrings.addMaterials
        }
        // TODO: Not implemented
        // {
        //   key: 'share',
        //   value: 'Share'
        // },
        // {
        //   key: 'duplicate',
        //   value: 'Duplicate'
        // }
      ];
    }
  }

  handleAddButtonClick(): void {
    if (this.gridItemsInUse === 'materials') {
      this._popupSelectService.closePopup();
    }
    this._router.navigate([this.gridItemsInUse, 'create']);
  }

  private handleCollectionsResponse(response: ArrayResponseI<CollectionModel>, append: boolean = false, isSearch: boolean = false): void {
    this._collectionsStoreService.collectionNextId = response.nextID;
    if (append) {
      this._collectionsStoreService.appendCollectionEntities(response.entities);
    } else {
      this._collectionsStoreService.collectionEntities = response.entities;
      this.isSearch = isSearch;
      this.trackForBottomReach = true;
      this._selectedMap.clear();
    }
    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.trackForBottomReach = false;
    }
  }

  handleSearch(searchValue: string, skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.searchKeyword = searchValue;

    this.searchBarData = {
      ...this.searchBarData,
      search: { placeholder: this.i18nStrings.searchFor, keyword: this.searchKeyword as string }
    };

    let orderByAttr: string = '';
    let orderByDir: string = '';
    switch (this.searchBarData.sort?.options[this.searchBarData.sort.selectedIndex].key) {
      case 'azAlphabetically':
        orderByAttr = MeSearchSortByTypes.name;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
      case 'zaAlphabetically':
        orderByAttr = MeSearchSortByTypes.name;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'newest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'oldest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
    }

    const searchCriteria: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_COLLECTION,
      criteriaQuick: this.searchKeyword.length ? this.searchKeyword : undefined,
      orderByAttr,
      orderByDir
    };

    this.areCollectionsLoading = true;
    this._globalService.activateLoader();
    this._searchCollectionSubscription = this._collectionsWebService
      .searchCollections(searchCriteria, skip, top)
      .pipe(
        finalize(() => {
          this.areCollectionsLoading = false;
          this._globalService.deactivateLoader();
        })
      )
      .subscribe((response: ArrayResponseI<CollectionModel>) => {
        this.handleCollectionsResponse(response, append, true);
      });
  }

  handleToggleSelectEvent(): void {
    this.selectionEnabled = !this.selectionEnabled;
    this.searchBarData = {
      ...this.searchBarData,
      selectButton: {
        ...this.searchBarData.selectButton,
        enabled: this.selectionEnabled,
        placeholder: this.selectionEnabled ? this.i18nStrings.cancel : this.i18nStrings.select
      }
    };
    this._selectedMap.clear();
    this.prepareCollectionCardData();
  }

  // Used in both view
  customEvent(eventName: string, payload?: string, item?: MeNewCollectionCardI | MeMaterialCardI): void {
    if (eventName === 'contextMenuItemClick') {
      switch (payload) {
        case 'view':
          this._router.navigate([this.gridItemsInUse, 'view', item?.oid]);
          if (this.gridItemsInUse === 'materials') {
            this._popupSelectService.closePopup();
          }
          break;
        case 'edit':
          this._router.navigate([this.gridItemsInUse, 'edit', item?.oid]);
          if (this.gridItemsInUse === 'materials') {
            this._popupSelectService.closePopup();
          }
          break;
        case 'addMaterial':
          if (item?.oid) {
            this.showAddMaterialsPopup(item.oid);
          }
          break;
        case 'delete':
          if (item?.oid) {
            const collectionOIDS: string[] = [item.oid];
            this.deleteCollection(collectionOIDS, (item as MeNewCollectionCardI).numberOfMaterials);
          }
          break;
        case 'visibility':
          if (item) {
            // Creating of popup data
            const popup: MePopupStatusChangeI = {
              title: this.i18nStrings.setCollectionVisibility,
              cancelButtonPlaceholder: this.i18nStrings.cancel,
              changeButtonPlaceholder: this.i18nStrings.saveChanges
            };
            // open popup
            const popUpSubs: Subject<{ eventName: string; payload?: string }> = this._popupStatusChangeService.openDialog(popup);

            // get States and add data to popup
            this._getCollectionStates = this._collectionsWebService.getCollectionStates(item.oid).subscribe((states) => {
              const collection = this._collectionsStoreService.collectionEntities.find((x) => x.oid === item.oid);
              if (collection) {
                const newPopup = {
                  ...popup,
                  currentKeyValue: collection.state,
                  radioButtons: states.entities.map((state) => {
                    const displName = this._definitionsStoreService
                      .getDomain(ME_DOMAIN_APPLICATION_MATERIAL)
                      .states.find((s) => s.internalName === state.internalName);
                    return {
                      key: state.internalName,
                      value: displName ? `${displName.displayName} - ${displName.description}` : state.internalName,
                      disabled: !state.enabled
                    };
                  })
                };
                this._popupStatusChangeService.refreshDialogData(newPopup);
                popUpSubs.subscribe((ev) => {
                  if (ev.eventName === 'change' && ev.payload && collection && ev.payload !== collection.state) {
                    this.changeCollectionState(collection.oid, ev.payload);
                  }
                });
              }
            });
          }
          break;

        case 'duplicate':
          if (item) {
            this.handleDuplicateCollection(item as MeNewCollectionCardI);
          }
          break;
        default:
          break;
      }
    }
  }

  changeCollectionState(collectionOid: string, newState: string): void {
    this._changeCollectionStateSubscription = this._collectionsWebService
      .changeCollectionState(collectionOid, newState)
      .subscribe((collection) => {
        this._collectionsStoreService.collectionEntities = this._collectionsStoreService.collectionEntities.map((coll) => {
          return coll.oid === collection.oid ? collection : coll;
        });
        this._basicAlertService.openBasicAlert({
          mode: 'success',
          title: this.i18nStrings.visibilityChanged,
          content: this.i18nStrings.collectionVisibilityChanged
        });
      });
  }

  bottomReachedHandler(): void {
    if (this.gridItemsInUse === 'collections') {
      if (this.trackForBottomReach) {
        if (this.isSearch) {
          this.handleSearch(this.searchKeyword || '', this._collectionsStoreService.collectionNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
        } else {
          this.getCollections(this._collectionsStoreService.collectionNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
        }
      }
    } else {
      if (this.materialsTrackForBottomReach) {
        if (this.materialsIsSearch) {
          this.materialsHandleSearch(
            this.materialsSearchKeyword || '',
            this._materialsStoreService.materialNextId,
            this.NUMBER_OF_ITEMS_ON_PAGE,
            true
          );
        } else {
          this.getMaterials(this._materialsStoreService.materialNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
        }
      }
    }
  }

  handleItemClick(card: MeNewCollectionCardI | MeMaterialCardI, index: number): void {
    if (this.selectionEnabled || this.gridItemsInUse === 'materials') {
      this.toggleCardItemSelection(index);
    } else {
      this._router.navigate(['collections', 'view', card.oid]);
    }
  }

  toggleCardItemSelection(index: number): void {
    const items = this.gridItemsInUse === 'collections' ? this.collectionCardItems : this.materialCardItems;
    const itemSelectionMap = this.gridItemsInUse === 'collections' ? this._selectedMap : this._materialsSectedMap;
    if (itemSelectionMap.has(items[index].oid)) {
      items[index] = { ...items[index], selected: false };
      itemSelectionMap.delete(items[index].oid);
    } else {
      items[index] = { ...items[index], selected: true };
      itemSelectionMap.set(items[index].oid);
    }
    if (this.gridItemsInUse === 'materials') {
      this.updateMaterialPopup({ selectedCount: itemSelectionMap.size });
    }
  }

  trackByOid(_index: number, item: MeNewCollectionCardI | MeMaterialCardI): string {
    return item.oid;
  }

  handleSortByEvent(sortByValue: string): void {
    const searchBar = this.gridItemsInUse === 'collections' ? this.searchBarData : this.materialsSearchBarData;
    if (searchBar.sort) {
      searchBar.sort.selectedIndex = searchBar.sort.options.findIndex((x) => x.key === sortByValue);
    }
    this.gridItemsInUse === 'collections'
      ? this.handleSearch(this.searchKeyword ? this.searchKeyword : '')
      : this.materialsHandleSearch(this.materialsSearchKeyword ? this.materialsSearchKeyword : '');
  }

  handleSearchBarEvents(event: { eventName: string; payload?: string | boolean }): void {
    switch (event.eventName) {
      case 'searchEnabledToggle':
        // TODO
        break;
      case 'edit':
        // TODO
        break;
      case 'trash':
        // TODO
        break;
      case 'copy':
        // TODO
        break;
      case 'more':
        // TODO
        break;
      case 'download':
        // TODO
        break;
      case 'delete':
        const collectionOIDS: string[] = [...this._selectedMap.keys()];
        if (collectionOIDS.length > 0) {
          this.deleteCollection(collectionOIDS);
        }
        break;
      // Materials popup grid action
      case 'info':
        // TODO
        break;
      default:
        throw new Error('No default option.');
    }
  }

  // Materials grid handler section
  showAddMaterialsPopup(collectionOid: string): void {
    this.selectedCollectionOid = collectionOid;
    this.gridItemsInUse = 'materials';
    this.materialsPopupSelect = {
      title: this.i18nStrings.addMaterialsToThisCollection,
      selectedCount: this._materialsSectedMap.size,
      showOnlySelected: false,
      showActions: true
    };
    this.getMaterials();
    this.prepareMaterialCardData();

    this.materialsSearchBarData = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: '',
        minCharacters: 1
      },
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'azAlphabetically', value: this.i18nStrings.azAlphabetically },
          { key: 'zaAlphabetically', value: this.i18nStrings.zaAlphabetically },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      },
      operationButtons: [],
      forceShowButton: true
    };

    this._materialsPopupSubscription = this._popupSelectService
      .openPopupSelect(this.materialsPopupSelect, this.materialsGridRef)
      .subscribe((event: string) => {
        switch (event) {
          case 'close':
            this.resetAfterPopupClose();
            break;
          case 'clearSelection':
            this._materialsSectedMap.clear();
            this.getMaterials();
            break;
          case 'showSelected':
            this.materialCardItems = this.materialCardItems.filter((item: MeMaterialCardI) => item.selected === true);
            this.updateMaterialPopup({
              showOnlySelected: true
            });
            break;
          case 'showAll':
            this.getMaterials();
            this.updateMaterialPopup({
              showOnlySelected: false
            });
            break;
          case 'addItems':
            this.addMaterialsToCollection();
            this.resetAfterPopupClose();
            break;
          default:
            break;
        }
      });
  }

  updateMaterialPopup(materialPopup: Partial<MePopupSelectI>): void {
    this.materialsPopupSelect = { ...this.materialsPopupSelect, ...materialPopup };
    this._popupSelectService.pushDataModelToPopup(this.materialsPopupSelect);
  }

  resetAfterPopupClose(): void {
    this.gridItemsInUse = 'collections';
    this.selectedCollectionOid = '';
    this._materialsSectedMap.clear();
  }

  addMaterialsToCollection(): void {
    this._addMaterialsToCollectionSubscriptions = this._collectionsWebService
      .addMaterialsToMeCollection(this.selectedCollectionOid, Array.from(this._materialsSectedMap.keys()))
      .subscribe(() => {
        this._basicAlertService.openBasicAlert({
          mode: 'success',
          title: this.i18nStrings.materialsAdded,
          content: this.i18nStrings.materialsSuccessfullyAdded
        });
        if (this.isSearch) {
          this.handleSearch(this.searchKeyword || '');
        } else {
          this.getCollections();
        }
      });
  }

  prepareMaterialCardData(): void {
    this._getMaterialDataForMaterialCardSubscription = this._materialsStoreService.materialEntities$.subscribe(
      (data: MaterialModel[] | null) => {
        this.materialCardItems = [];
        if (data) {
          const domain: DomainModel = this._definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_MATERIAL);
          this.materialCardItems = data.map((material: MaterialModel) => {
            let unitOfMeasure: string = '';
            if (material.attributes && material.attributes.hasOwnProperty(MATERIAL_ATTRIBUTE_UOM)) {
              unitOfMeasure = this._attributesService.getEnumDisplayNames(
                {
                  dataTypeClass: MeAttributeTypes.CHOICE,
                  displayName: '',
                  internalName: MATERIAL_ATTRIBUTE_UOM,
                  value: material.attributes.unitOfMeasure,
                  convertedValue: ''
                },
                domain
              );
            }
            return {
              contextMenuButton: { type: 'regular', color: 'link', rounded: true },
              contextMenuItems: [
                {
                  key: 'view',
                  value: this.i18nStrings.view
                },
                {
                  key: 'edit',
                  value: this.i18nStrings.edit
                }
              ],
              materialName: material.attributes.name,
              materialPrice: material.attributes.price,
              materialUOM: unitOfMeasure || material.attributes.unitOfMeasure,
              oid: material.oid,
              selected: this._materialsSectedMap.has(material.oid),
              selectionEnabled: true,
              supplierName: material.attributesExtended.owner,
              supplierOid: material.ownerOID,
              thumbnail: material.attributes.thumbnail ? `${MARKETPLACE_BASE_API_URL}/files/download/${material.attributes.thumbnail}` : ''
            };
          });
        }
      }
    );
  }

  private handleMaterialsResponse(response: ArrayResponseI<MaterialModel>, append: boolean = false, isSearch: boolean = false): void {
    this._materialsStoreService.materialNextId = response.nextID;
    if (append) {
      this._materialsStoreService.appendMaterialEntities(response.entities);
    } else {
      this.materialsIsSearch = isSearch;
      if (this.materialsIsSearch) {
        this._materialsSectedMap.clear();
      }
      this._materialsStoreService.materialEntities = response.entities;
      this.materialsTrackForBottomReach = true;
      this.updateMaterialPopup({
        selectedCount: this._materialsSectedMap.size
      });
    }
    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.materialsTrackForBottomReach = false;
    }
  }

  getMaterials(skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.areMaterialsLoading = true;
    this._getAllMaterialsSubscription = this._materialsWebService
      .getMeMaterials(skip, top)
      .pipe(
        finalize(() => {
          this.areMaterialsLoading = false;
        })
      )
      .subscribe((response: ArrayResponseI<MaterialModel>) => {
        this.handleMaterialsResponse(response, append, false);
      });
  }

  materialsHandleSearch(searchValue: string, skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.materialsSearchKeyword = searchValue;
    this.materialsSearchBarData = {
      ...this.materialsSearchBarData,
      search: { placeholder: this.i18nStrings.searchFor, keyword: this.materialsSearchKeyword as string }
    };

    let orderByAttr: string = '';
    let orderByDir: string = '';
    switch (this.materialsSearchBarData.sort?.options[this.materialsSearchBarData.sort.selectedIndex].key) {
      case 'azAlphabetically':
        orderByAttr = MeSearchSortByTypes.name;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
      case 'zaAlphabetically':
        orderByAttr = MeSearchSortByTypes.name;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'newest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'oldest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
    }

    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_MATERIAL,
      criteriaQuick: this.materialsSearchKeyword.length ? this.materialsSearchKeyword : undefined,
      orderByAttr,
      orderByDir
    };

    this.areMaterialsLoading = true;
    this._searchMaterialSubscription = this._materialsWebService
      .searchMaterials(searchFilter, skip, top)
      .pipe(
        finalize(() => {
          this.areMaterialsLoading = false;
        })
      )
      .subscribe((response: ArrayResponseI<MaterialModel>) => {
        this.handleMaterialsResponse(response, append, true);
      });
  }

  private deleteCollection(collectionsOIDS: string[], numberOfMaterials?: number): void {
    this._sweetAletSubscription = this._sweetAlertService.getDataBackFromSweetAlert().subscribe((data: MeSweetAlertI) => {
      if (data.confirmed) {
        this._deleteCollectionSubscription = this._collectionsWebService.deleteCollection(collectionsOIDS).subscribe(() => {
          this._basicAlertService.openBasicAlert({
            mode: 'success',
            content: this.i18nStrings.collectionDeleted,
            title: this.i18nStrings.success
          });
          if (this.isSearch) {
            this.handleSearch(this.searchKeyword || '');
          } else {
            this.getCollections();
          }
        });
      }
      this._sweetAletSubscription.unsubscribe();
    });

    let sweetAlertMessage = this.i18nStrings.checkCollectionForDelete;
    sweetAlertMessage += numberOfMaterials ? `${this.i18nStrings.checkCollectionForDeleteNote.replace('[#]', `${numberOfMaterials}`)}` : '';

    this._sweetAlertService.openMeSweetAlert({
      icon: 'alert-octagon',
      mode: 'danger',
      message: sweetAlertMessage,
      title: this.i18nStrings.deleteCollection,
      type: {
        name: MeSweetAlertTypeEnum.submit,
        buttons: {
          submit: this.i18nStrings.delete,
          cancel: this.i18nStrings.cancel
        }
      }
    });
  }

  private handleDuplicateCollection(collectionCard: MeNewCollectionCardI): void {
    this._router.navigate(['collections', 'duplicate', collectionCard.oid]);
  }

  ngOnDestroy(): object {
    if (this._getAllCollectionsSubscription) {
      this._getAllCollectionsSubscription.unsubscribe();
    }
    if (this._getCollectionDataForCollectionCardSubscription) {
      this._getCollectionDataForCollectionCardSubscription.unsubscribe();
    }
    if (this._getDefinitionsDataLoadedSubscription) {
      this._getDefinitionsDataLoadedSubscription.unsubscribe();
    }
    if (this._searchCollectionSubscription) {
      this._searchCollectionSubscription.unsubscribe();
    }
    if (this._getAllMaterialsSubscription) {
      this._getAllMaterialsSubscription.unsubscribe();
    }
    if (this._getMaterialDataForMaterialCardSubscription) {
      this._getMaterialDataForMaterialCardSubscription.unsubscribe();
    }
    if (this._searchMaterialSubscription) {
      this._searchMaterialSubscription.unsubscribe();
    }
    if (this._materialsPopupSubscription) {
      this._materialsPopupSubscription.unsubscribe();
    }
    if (this._addMaterialsToCollectionSubscriptions) {
      this._addMaterialsToCollectionSubscriptions.unsubscribe();
    }
    if (this._deleteCollectionSubscription) {
      this._deleteCollectionSubscription.unsubscribe();
    }
    if (this._getI18nStringsSubscription) {
      this._getI18nStringsSubscription.unsubscribe();
    }
    if (this._getCollectionStates) {
      this._getCollectionStates.unsubscribe();
    }
    if (this._changeCollectionStateSubscription) {
      this._changeCollectionStateSubscription.unsubscribe();
    }
    return super.ngOnDestroy();
  }
}
