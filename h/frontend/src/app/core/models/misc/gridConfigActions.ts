export interface IGridConfigActions {
  add?: () => void;
  edit?: (row: any) => any;
  delete?: (row: any) => any;
  select?: (row: any) => any;
  clone?: (row: any) => any;
  show?: (row: any) => any;
  request?: (row: any) => any;
  isInEditMode?: () => boolean;
}
