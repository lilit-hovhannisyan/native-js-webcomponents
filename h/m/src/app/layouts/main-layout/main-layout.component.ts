import { Component, OnDestroy, OnInit, Injector, ViewChild, TemplateRef, ChangeDetectorRef } from '@angular/core';
import { NavigationError, NavigationStart, Router } from '@angular/router';

import { Subscription, Subject, forkJoin } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AuthStoreService } from '@core-services/auth-store.service';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { DefinitionsWebService } from '@shared/services/definitions.web-service';
import { GlobalService } from '@core-services/global.service';
import { UserWebService } from '@core-services/user.web-service';

import { CompanyModel } from '@shared/models/company/company.model';
import { BaseLoggerComponent } from '@features/tracking-system';

import { MeBreadcrumbActionI } from '@shared/me-components/me-breadcrumb/me-breadcrumb-action.interface';
import { MeBreadcrumbI } from '@shared/me-components/me-breadcrumb/me-breadcrumb.interface';
import { MeInfoAlertService } from '@shared/me-components/me-info-alert/me-info-alert.service';
import { MePopupAccountService } from '@shared/me-components/me-popup-account/me-popup-account.service';
import { MePopupAccountI } from '@shared/me-components/me-popup-account/me-popup-account.interface';
import { DataImportModel } from '@shared/models/data-import/data-import.model';
import { MeDataImportService } from '@shared/me-components/me-data-import/me-data-import.service';
import { MeSidebarI } from '@shared/me-components/me-sidebar/me-sidebar.interface';
import { UserModel } from '@shared/models/user/user.model';
import { MeHeaderI } from '@shared/me-components/me-header/me-header.interface';
import { MARKETPLACE_BASE_API_URL, ME_OMP_USER_GUIDE_URL } from '@shared/constants';
import { MeUserImageI } from '@shared/me-components/me-user-image/me-user-image.interface';
import { BaseUploadModel } from '@shared/models/file-upload/file-upload.model';
import { MeFileUploadTypes } from '@shared/me-file-upload-types';
import { WindowRef } from '@core-providers/window-ref.provider';
import { MePermissionTypes } from '@shared/me-permission-types';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
  providers: [DefinitionsWebService]
})
export class MainLayoutComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  @ViewChild('popupImportData', { read: TemplateRef }) popupImportDataRef!: TemplateRef<Component>;

  componentVersion: string = '0.0.1';

  activePageName: string = 'Marketplace';
  breadcrumb: MeBreadcrumbI = {
    actions: []
  };
  private _getUserDataSubscription!: Subscription;
  private _getCompanyDataSubscription!: Subscription;
  private _userLogoutSubscription!: Subscription;
  private _infoAlertSubscription!: Subscription;

  currentUser: UserModel | null = new UserModel();
  currentCompany: CompanyModel | null = new CompanyModel();
  header: MeHeaderI = {
    user: {
      userImage: {
        type: 'initials',
        size: 'small',
        shape: 'rounded'
      }
    }
  };
  private _unsubscribeOnDestroy$: Subject<void> = new Subject();
  private _getI18nStringsSubscription!: Subscription;

  i18nStrings: MeTranslationI = {};

  private _modal!: MePopupAccountI;
  interval!: ReturnType<typeof setInterval>;
  isLoading: boolean = false;
  sidebar!: MeSidebarI;

  constructor(
    injector: Injector,
    private authStoreService: AuthStoreService,
    private userWebService: UserWebService,
    private router: Router,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private definitionsStoreService: DefinitionsStoreService,
    private definitionsWebService: DefinitionsWebService,
    private basicAlertService: MeInfoAlertService,
    private popupAccountService: MePopupAccountService,
    public meDataImportService: MeDataImportService,
    public globalService: GlobalService,
    private _translate: MeTranslationService,
    private crf: ChangeDetectorRef,
    private windowRef: WindowRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._getI18nStringsSubscription = this._translate.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;
      this.sidebar = {
        isExpanded: true,
        isCollapsible: false,
        company: {
          editButton: this.authStoreService.isAllowed(MePermissionTypes.COMPANY_MODIFY)
            ? {
                type: 'regular',
                color: 'link'
              }
            : null,
          editButtonTitle: this.i18nStrings.editDigitalProfile,
          logoUrl: '/assets/images/me-logo.png',
          title: 'Company name'
        },
        logoUrl: '/assets/images/me-logo.png',
        navItems: [
          // {
          //   id: 'dashboard',
          //   title: 'Dashboard',
          //   icon: 'pie-chart',
          //   activated: false
          // },
          {
            id: 'home',
            title: 'Home',
            icon: 'home',
            activated: false
          },
          {
            id: 'materials',
            title: 'Materials',
            icon: 'grid',
            activated: false,
            disabled: !this.authStoreService.isAllowed(MePermissionTypes.MATERIAL_VIEW)
          },
          {
            id: 'collections',
            title: 'Collections',
            icon: 'book',
            activated: false,
            disabled: !this.authStoreService.isAllowed(MePermissionTypes.COLLECTION_VIEW)
          },
          {
            id: 'documents/visuals',
            title: 'Visuals',
            icon: 'visuals',
            activated: false,
            disabled: !this.authStoreService.isAllowed(MePermissionTypes.DOCUMENT_VIEW)
          },
          {
            id: 'documents',
            title: 'Documents',
            icon: 'file-text',
            activated: false,
            disabled: !this.authStoreService.isAllowed(MePermissionTypes.DOCUMENT_VIEW)
          },
          {
            id: 'users',
            title: 'Users/Groups',
            icon: 'users',
            activated: false,
            disabled: !this.authStoreService.isAllowed(MePermissionTypes.COMPANY_USER_LIST)
          },
          {
            id: 'digital-profile',
            title: 'Digital Profile',
            icon: 'digital-profile',
            activated: false,
            disabled: !this.authStoreService.isAllowed(MePermissionTypes.COMPANY_VIEW)
          }
        ]
      };
    });
    this.getDefinitionsAndProperties();
    this.getUserData();
    this.getCompanyData();
    this.updateActiveSidebarItem(this.router.url);
    this.router.events.pipe(takeUntil(this._unsubscribeOnDestroy$)).subscribe((e) => {
      if (e instanceof NavigationStart) {
        this.updateActiveSidebarItem(e.url);
        this.breadcrumbSupportService.breadcrumbActions = [];
      } else if (e instanceof NavigationError) {
        this.breadcrumbSupportService.breadcrumbActions = [];
      }
    });
    this.breadcrumbSupportService.breadcrumbActions$.subscribe((actions: MeBreadcrumbActionI[]) => {
      this.breadcrumb = { ...this.breadcrumb, actions };
    });

    this.globalService.isActivatedLoader$.pipe(takeUntil(this._unsubscribeOnDestroy$)).subscribe((active) => {
      this.isLoading = active;
      this.crf.detectChanges();
    });

    if (this.currentCompany && !this.currentCompany.attributes.isDataImported && this.currentUser && this.currentUser.isCompanyAdmin) {
      this._infoAlertSubscription = this.basicAlertService
        .openInfoAlert({
          mode: 'regular',
          title: '',
          content: this.i18nStrings.importSettingsDescription,
          buttonText: this.i18nStrings.importSettings
        })
        .subscribe((event: string) => {
          if (event === 'btnEvent') {
            this.basicAlertService.closeInfoAlert();
            this._modal = {
              title: 'Data Import Options Form',
              content: this.popupImportDataRef
            };
            this.popupAccountService.openPopupSelect(this._modal);
          }
        });
    }
  }

  handleSendFormData(data: DataImportModel): void {
    this.meDataImportService.addSettings(data);
  }

  getDefinitionsAndProperties(): void {
    const getDefinitions = this.definitionsWebService.getAllDefinitions().pipe(takeUntil(this._unsubscribeOnDestroy$));
    const getProperties = this.definitionsWebService.getDefinitionProperties().pipe(takeUntil(this._unsubscribeOnDestroy$));

    forkJoin([getDefinitions, getProperties]).subscribe(([definitionsResponse, propertiesResponse]) => {
      if (definitionsResponse.domains) {
        this.definitionsStoreService.domains = definitionsResponse.domains;
      }
      if (definitionsResponse.enums) {
        this.definitionsStoreService.enums = definitionsResponse.enums;
      }

      const thumbnailProperties: BaseUploadModel | undefined = propertiesResponse.fileUpload.find((property) => {
        return property.type === MeFileUploadTypes.thumbnail;
      });
      if (thumbnailProperties) {
        this.definitionsStoreService.thumbnailProperties = thumbnailProperties;
      }
      const documentProperties: BaseUploadModel | undefined = propertiesResponse.fileUpload.find((property) => {
        return property.type === MeFileUploadTypes.document;
      });
      if (documentProperties) {
        this.definitionsStoreService.documentProperties = documentProperties;
      }
      const certificateProperties: BaseUploadModel | undefined = propertiesResponse.fileUpload.find((property) => {
        return property.type === MeFileUploadTypes.certificate;
      });
      if (certificateProperties) {
        this.definitionsStoreService.certificateProperties = certificateProperties;
      }
      const logoProperties: BaseUploadModel | undefined = propertiesResponse.fileUpload.find((property) => {
        return property.type === MeFileUploadTypes.logo;
      });
      if (logoProperties) {
        this.definitionsStoreService.logoProperties = logoProperties;
      }

      const coverImageProperties: BaseUploadModel | undefined = propertiesResponse.fileUpload.find((property) => {
        return property.type === MeFileUploadTypes.coverImage;
      });
      if (coverImageProperties) {
        this.definitionsStoreService.coverImageProperties = coverImageProperties;
      }

      this.definitionsStoreService.generalUploadProperties = propertiesResponse.fileUpload;

      this.definitionsStoreService.dataLoaded = true;
    });
  }

  updateActiveSidebarItem(url: string): void {
    const id: string = url.slice(1, url.length);
    this.sidebar = {
      ...this.sidebar,
      navItems: this.sidebar.navItems.map((item) => {
        return { ...item, activated: item.id === id };
      })
    };
  }

  getUserData(): void {
    this._getUserDataSubscription = this.authStoreService.user$.subscribe((userData: UserModel | null) => {
      // Try to get user image otherwise get initials from name
      let userImage: MeUserImageI;
      if (userData?.attributes.avatar) {
        userImage = {
          type: 'avatar',
          size: 'small',
          shape: 'rounded',
          imageUrl: MARKETPLACE_BASE_API_URL + '/files/download/' + userData.attributes.avatar
        };
      } else {
        userImage = {
          type: 'initials',
          size: 'small',
          shape: 'rounded',
          initials:
            this.getFirstLetterFromWord(userData?.attributes.firstName).toUpperCase() +
            this.getFirstLetterFromWord(userData?.attributes.lastName).toUpperCase()
        };
      }
      this.header = {
        user: {
          userImage,
          firstName: userData?.attributes.firstName,
          lastName: userData?.attributes.lastName,
          company: userData?.attributesExtended.company,
          email: userData?.attributes.email,
          preferredName: userData?.attributes.preferredName
        },
        placeholders: {
          logout: this.i18nStrings.logout,
          profileSettings: this.i18nStrings.profileSettings,
          userGuide: this.i18nStrings.userGuide
        },
        helpMenuItems: [
          { key: 'knowledgeBase', displayName: 'Knowledge Base' },
          { key: 'userGuide', displayName: 'User Guide' },
          { key: 'getHelp', displayName: 'Get Help' },
          { key: 'provideFeedBack', displayName: 'Provide Feedback' },
          { key: 'legal', displayName: 'Legal' },
          { key: 'about', displayName: 'About Material Exchange' }
        ]
      };
    });
  }

  getCompanyData(): void {
    this._getCompanyDataSubscription = this.authStoreService.company$.subscribe((companyData: CompanyModel | null) => {
      this.currentCompany = companyData;
      this.sidebar = {
        ...this.sidebar,
        company: {
          ...this.sidebar.company,
          title: companyData?.attributes.name as string,
          logoUrl: companyData?.attributes.logo
            ? `${MARKETPLACE_BASE_API_URL}/files/download/${companyData?.attributes.logo}`
            : this.sidebar.company.logoUrl
        }
      };
    });
  }

  getFirstLetterFromWord(word: string | undefined): string {
    if (word) {
      return word.charAt(0);
    }
    return '';
  }

  notificationsEvent(): void {
    this.router.navigate(['/notifications']);
  }

  userProfileEvent(): void {
    this.router.navigate(['/user-profile']);
  }

  userGuideEvent(): void {
    this.windowRef.openInNewWindow(ME_OMP_USER_GUIDE_URL);
  }

  logOut(): void {
    // TODO this should be in the auth service not in the user service
    this._userLogoutSubscription = this.userWebService.logOut().subscribe(() => {
      sessionStorage.removeItem('loggedIn');
      this.authStoreService.user = null;
      this.authStoreService.permissions = null;
      this.authStoreService.company = null;
      this.loggingService.removeFromTheHeaders('Authorization');
      this.router.navigate(['auth/login']);
    });
  }

  messagesEvent(): void {
    this.router.navigate(['/messages']);
  }

  goToHome(): void {
    this.router.navigate(['/']);
  }

  sidebarEvents(event: { eventName: string; payload?: string }): void {
    switch (event.eventName) {
      case 'selectedItemId':
        this.router.navigate([event.payload]);
        // TODO do something with selectedItem
        break;
      case 'clickedEditButton':
        this.router.navigate(['digital-profile/edit']);
        break;
      case 'goToDigitalProfile':
        this.router.navigate(['digital-profile/view']);
        break;
    }
  }

  toggleSidebar(): void {
    this.sidebar = { ...this.sidebar, isExpanded: !this.sidebar.isExpanded };
  }

  handleHelpMenuItemClickEvent(key: string): void {
    switch (key) {
      case 'knowledgeBase':
        this.windowRef.openInNewWindow('https://5883447.hs-sites.com/knowledge');
        break;
      case 'userGuide':
        this.userGuideEvent();
        break;
      case 'getHelp':
        this.windowRef.openInNewWindow('https://material-exchange.com/contact-us/');
        break;
      case 'provideFeedBack':
        this.windowRef.openInNewWindow('https://material-exchange.com/contact-us/');
        break;
      case 'legal':
        this.windowRef.openInNewWindow('https://material-exchange.com/legal/');
        break;
      case 'about':
        this.windowRef.openInNewWindow('https://material-exchange.com');
        break;
      default:
        this.windowRef.openInNewWindow('https://material-exchange.com/contact-us/');
    }
  }

  ngOnDestroy(): object {
    if (this._getUserDataSubscription) {
      this._getUserDataSubscription.unsubscribe();
    }
    if (this._userLogoutSubscription) {
      this._userLogoutSubscription.unsubscribe();
    }
    if (this._getCompanyDataSubscription) {
      this._getCompanyDataSubscription.unsubscribe();
    }
    if (this._infoAlertSubscription) {
      this._infoAlertSubscription.unsubscribe();
    }
    if (this._getI18nStringsSubscription) {
      this._getI18nStringsSubscription.unsubscribe();
    }
    this._unsubscribeOnDestroy$.next();
    this._unsubscribeOnDestroy$.complete();

    return super.ngOnDestroy();
  }
}
