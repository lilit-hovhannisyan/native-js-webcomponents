import { Component, Input } from '@angular/core';

/**
 * This component wraps its ng-content within a fieldset
 * and adds label tag above it.
 */
@Component({
  selector: 'nv-fieldset',
  templateUrl: './fieldset.component.html',
  styleUrls: ['./fieldset.component.scss']
})
export class FieldsetComponent {
  @Input() public label: string;
}
