import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class WizardService {

  public currentStep$ = new BehaviorSubject<number>(1);
  public stepIsValid$ = new BehaviorSubject<boolean>(false);

  public state = {};

  constructor() {
  }

  public getState() {
    return this.state;
  }

  public saveToState(formValue: any): void {
    this.state[this.getCurrentStep()] = formValue;
  }

  public getCurrentStep (): number {
    return this.currentStep$.getValue();
  }

  public setCurrentStep(stepId: number): void {
    this.currentStep$.next(stepId);
  }

  public setValidity(valid: boolean): void {
    this.stepIsValid$.next(valid);
  }
}
