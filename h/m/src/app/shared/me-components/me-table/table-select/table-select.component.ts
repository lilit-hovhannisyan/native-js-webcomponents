import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';
import { MeSelectInputComponent } from '@shared/me-components/me-attribute/me-select-input/me-select-input.component';
import { DMChoiceAttributeModel } from '@shared/models/attribute/attribute.model';
import { PropertiesModel } from '@shared/models/attribute/attribute.properties.model';
import { MeSelectInputAttribute } from '@shared/models/attribute/me-select-input-attribute.model';
import SelectComponentDataI from '../interfaces/select-component-data.interface';

@Component({
  selector: 'me-table-select',
  templateUrl: './table-select.component.html',
  styleUrls: ['./table-select.component.scss']
})
export class TableSelectComponent implements OnInit {
  @Input() data!: SelectComponentDataI;
  @Output() tableDataChangeEvent = new EventEmitter<any>();

  selectAttribute!: MeAttributeItem;
  currentValue!: string;

  constructor() {}

  ngOnInit(): void {
    const { displayName, internalName, value, values } = this.data;
    this.currentValue = value;
    const g: DMChoiceAttributeModel = {
      constraints: [],
      dataTypeClass: 'moa',
      displayName,
      internalName,
      properties: new PropertiesModel(),
      required: true,
      optRequired: false,
      unique: false,
      value,
      values,
      disabled: false
    };

    const h: MeSelectInputAttribute = new MeSelectInputAttribute();
    h.data = g;
    h.form = new FormGroup({
      [g.internalName]: new FormControl(g.value, [Validators.required])
    });

    h.form.valueChanges.subscribe((val: any) => {
      const { internalName } = this.data;

      if (val[internalName] !== this.currentValue) {
        this.currentValue = val[internalName];
        this.tableDataChangeEvent.emit({ ...val, type: 'select' });
      }
    });

    this.selectAttribute = new MeAttributeItem(MeSelectInputComponent, h);
  }
}
