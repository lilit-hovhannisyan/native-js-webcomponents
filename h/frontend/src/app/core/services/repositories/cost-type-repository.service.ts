import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { CostTypeKey, ICostType, ICostTypeCreate, ICostTypeDTO } from '../../models/resources/ICostTypes';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class CostTypeRepositoryService extends AbstractRepository<
  ICostType,
  ICostTypeDTO,
  ICostTypeCreate,
  RepoParams<ICostType>,
  CostTypeKey
> {
  protected resourceType = 'Cost Types';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/cost-types';
  }

  protected postProcess(item: ICostTypeDTO): ICostType { return item; }
}
