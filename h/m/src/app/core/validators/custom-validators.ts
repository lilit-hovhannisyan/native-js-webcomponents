import { ValidationErrors, ValidatorFn, AbstractControl, FormGroup } from '@angular/forms';

export function PatternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    if (!control.value) {
      return null;
    }

    const valid = regex.test(control.value);

    return valid ? null : error;
  };
}

export function PasswordMatchValidator(control: FormGroup): { noPassswordMatch: boolean } | null {
  let password: string;
  const controlPass = control.get('newPassword');
  controlPass ? (password = controlPass.value) : (password = '');
  let confirmPassword: string;
  const controlPassConf = control.get('confirmPassword');
  controlPassConf ? (confirmPassword = controlPassConf.value) : (confirmPassword = '');

  if (password !== confirmPassword) {
    return { noPassswordMatch: true };
  } else {
    return null;
  }
}

export function dateRangeValidator(startDateField: string, endDateField: string): ValidatorFn {
  // tslint:disable-next-line:no-any
  return (control: AbstractControl): { [key: string]: any } | null => {
    const start = control.get(startDateField)?.value;
    const end = control.get(endDateField)?.value;
    if (start !== null && end !== null && new Date(start) <= new Date(end)) {
      control.get(startDateField)?.setErrors(null);
      control.get(endDateField)?.setErrors(null);
      return null;
    } else {
      if (control.get(startDateField)?.dirty || control.get(endDateField)?.dirty) {
        control.get(startDateField)?.markAllAsTouched();
        control.get(endDateField)?.markAllAsTouched();
      }
      control.get(startDateField)?.setErrors({ incorrect: true });
      control.get(endDateField)?.setErrors({ incorrect: true });
      return { range: true };
    }
  };
}
