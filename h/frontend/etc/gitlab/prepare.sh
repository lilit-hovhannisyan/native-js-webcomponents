#!/bin/sh

# TODO: check if all (env) vars are available

# install required packages and setup ssh config
apk add --update openssh rsync

# output ssh environment variables
# https://unix.stackexchange.com/q/351725/288494
eval $(ssh-agent -s)

mkdir "${HOME}/.ssh"
echo "${SSH_PRIVATE_KEY}" > "${HOME}/.ssh/id_rsa"
chmod 700 "${HOME}/.ssh/id_rsa"
echo "${REMOTE_HOST_SSH_KEY}" > "${HOME}/.ssh/known_hosts"
