import {
  NvCellAlignment,
  NvColumnDataType,
  NvGridButtonsPosition,
  NvGridConfig,
  NvRowHeight,
  NvFilterControl,
  NvExcelExportInfo
} from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';
import { Operations } from '../../models/Operations';
import { IAccountStatementFull } from '../../models/resources/IAccount-statement';
import { formatNumberWrapper } from 'src/app/shared/helpers/general';
import { NvLocale } from '../../models/dateFormat';

export const getGridConfigBookingLines = (
  actions: IGridConfigActions,
  canPerform: (operation: number) => boolean,
  customFormat: {
    debitCredit: (debitCreit: number) => string
  },
  onExcelExport: (exportExcelInfo: NvExcelExportInfo) => void

): NvGridConfig => ({
  gridName: 'accountSheetsConfigBookingLines',
  hideAllFilters: false,
  showRowIndex: false,
  showPaging: true,
  showFooter: false,
  pinFirstRow: false,
  hideRefreshButton: true,
  rowHeight: NvRowHeight.midSize,
  rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
  clearFiltersButton: true,
  showExcelButton: true,
  onExcelExport: onExcelExport,
  columns: [
    {
      key: 'id',
      visible: false
    },
    {
      key: 'voucherNumber',
      title: wording.accounting.voucherNo,
      width: 120,
      isSortable: true,
      filter: {
        values: []
      }
    },
    {
      key: 'voucherDate',
      title: wording.accounting.voucherDate,
      dataType: NvColumnDataType.Date,
      width: 120,
      isSortable: true,
      filter: {
        controlType: NvFilterControl.Date,
        values: []
      }
    },
    {
      key: 'currencyIsoCode',
      title: wording.accounting.currency,
      width: 85,
      isSortable: true,
      filter: {
        controlType: NvFilterControl.Select,
        multiSelect: false,
        values: []
      }
    },
    {
      key: '+/-',
      title: wording.accounting.DC,
      width: 50,
      isSortable: true,
      customFormatFn: (accountStatement: IAccountStatementFull) => customFormat.debitCredit(accountStatement.debitCreditType),
      filter: {
        values: []
      },
    },
    {
      key: 'amountTransaction',
      title: wording.accounting.amountTransaction,
      dataType: NvColumnDataType.Decimal,
      width: 93,
      isSortable: true,
      filter: {
        controlType: NvFilterControl.RangeNumber,
        values: []
      },
    },
    {
      key: 'exchangeRate',
      title: wording.accounting.rate,
      width: 60,
      isSortable: true,
      dataType: NvColumnDataType.Decimal,
      decimalDigitsInfo: '1.5-5',
      filter: {
        values: []
      },
    },
    {
      key: 'amountLocal',
      title: wording.accounting.amountLocal,
      width: 93,
      isSortable: true,
      dataType: NvColumnDataType.Decimal,
      filter: {
        controlType: NvFilterControl.RangeNumber,
        values: []
      },
    },
    {
      key: 'text',
      title: wording.accounting.bookingText,
      width: 80,
      isSortable: true,
      filter: {
        values: []
      },
    },
    {
      key: 'period',
      title: wording.accounting.period,
      dataType: NvColumnDataType.Number,
      width: 75,
      isSortable: true,
      filter: {
        values: []
      },
    },
    {
      key: 'invoiceDate',
      title: wording.accounting.invoiceDate,
      dataType: NvColumnDataType.Date,
      width: 120,
      isSortable: true,
      filter: {
        controlType: NvFilterControl.Date,
        values: []
      },
    },
    {
      key: 'invoiceNo',
      title: wording.accounting.invoiceNo,
      width: 90,
      isSortable: true,
      filter: {
        values: []
      },
    },
    {
      key: 'paymentDate',
      title: wording.accounting.paymentDate,
      dataType: NvColumnDataType.Date,
      width: 120,
      isSortable: true,
      filter: {
        controlType: NvFilterControl.Date,
        values: []
      },
    },
    {
      key: 'oppositeAccountNo',
      title: wording.accounting.oppositeAccount,
      width: 135,
      isSortable: true,
      filter: {
        values: []
      },
      customTooltip: (accountStatement: IAccountStatementFull) => accountStatement.accountName,
    },
    {
      key: 'userName',
      title: wording.accounting.bookedBy,
      width: 90,
      isSortable: true,
      filter: {
        controlType: NvFilterControl.Select,
        values: []
      },
    },
    {
      key: 'bookingDate',
      title: wording.accounting.bookingDate,
      dataType: NvColumnDataType.DateTime,
      width: 120,
      isSortable: true,
      filter: {
        controlType: NvFilterControl.Date,
        values: []
      },
    },
    {
      key: 'costTypeNo',
      title: wording.accounting.ct,
      width: 80,
      isSortable: true,
      filter: {
        values: []
      },
      customTooltip: (accountStatement: IAccountStatementFull) => accountStatement.costTypeName
    },
    {
      key: 'costTypeName',
      title: wording.accounting.ct,
      visible: false
    },
    {
      key: 'costCenterNo',
      title: wording.accounting.cc,
      width: 80,
      isSortable: true,
      filter: {
        values: []
      },
      customTooltip: (accountStatement: IAccountStatementFull) => accountStatement.costCenterName
    },
    {
      key: 'costCenterName',
      visible: false
    },
    {
      key: 'costUnitNo',
      title: wording.accounting.cu,
      width: 80,
      isSortable: true,
      filter: {
        values: []
      },
      customTooltip: (accountStatement: IAccountStatementFull) => accountStatement.costUnitName
    },
    {
      key: 'costUnitName',
      visible: false
    },
    {
      key: 'tax',
      title: wording.accounting.tax,
      width: 80,
      isSortable: true,
      filter: {
        values: []
      },
    },
    {
      key: 'bookingCode',
      title: wording.accounting.bc,
      width: 80,
      isSortable: true,
      filter: {
        values: []
      },
    },
    {
      key: 'voyage',
      title: wording.accounting.voyage,
      width: 70,
      isSortable: true,
      filter: {
        values: []
      },
    },
    {
      key: 'port',
      title: wording.accounting.port,
      width: 50,
      isSortable: true,
      filter: {
        values: []
      },
    },
    {
      key: 'loc',
      title: wording.accounting.loc,
      width: 50,
      isSortable: true,
      filter: {
        values: []
      },
    },
    {
      key: 'clearingNo',
      title: wording.accounting.clearingNo,
      width: 130,
      isSortable: true,
      filter: {
        values: []
      },
    },
    {
      key: 'clearingDate',
      title: wording.accounting.clearingDate,
      width: 130,
      isSortable: true,
      filter: {
        values: []
      },
    },
    {
      key: 'clearingUser',
      title: wording.accounting.clearingUser,
      width: 130,
      isSortable: true,
      filter: {
        values: []
      },
    },
    {
      key: 'clearingType',
      title: wording.accounting.clearingType,
      width: 130,
      isSortable: true,
      filter: {
        values: []
      },
    },
    {
      key: 'clearingMethod',
      title: wording.accounting.clearingMethod,
      width: 130,
      isSortable: true,
      filter: {
        values: []
      },
    },
  ],
  contextMenuButtons: [
    {
      icon: 'edit',
      description: wording.general.edit,
      name: 'editBooking',
      tooltip: wording.general.edit,
      actOnEnter: true,
      actOnDoubleClick: true,
      hidden: !canPerform(Operations.UPDATE),
      func: (accountStatement: IAccountStatementFull) => actions.edit(accountStatement)
    },
    {
      icon: 'eye',
      description: wording.accounting.showBooking,
      name: 'showBooking',
      tooltip: wording.accounting.showBooking,
      actOnDoubleClick: false,
      func: (accountStatement: IAccountStatementFull) => actions.show(accountStatement)
    },
    {
      icon: 'copy',
      description: wording.accounting.cloneBooking,
      name: 'cloneBookingLine',
      tooltip: wording.accounting.cloneBooking,
      actOnDoubleClick: false,
      func: (accountStatement: IAccountStatementFull) => actions.clone(accountStatement)
    }
  ]
});

export const getGridConfigCalculatedData = (locale: NvLocale): NvGridConfig => ({
  gridName: 'accountSheetsCalculatedData',
  hideAllFilters: true,
  showRowIndex: false,
  showPaging: false,
  showFooter: false,
  hideToolbar: true,
  pinFirstRow: false,
  hideRefreshButton: true,
  rowHeight: NvRowHeight.midSize,
  rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
  columns: [
    {
      key: 'id',
      visible: false
    },
    {
      key: 'year',
      title: wording.accounting.bookingYear,
      width: 80,
      isSortable: true,
    },
    {
      key: 'amountBroughtForward',
      title: wording.accounting.amountBroughtForward,
      width: 190,
      isSortable: true,
      alignment: NvCellAlignment.Right,
    },
    {
      key: 'currencyId',
      visible: false,
    },
    {
      key: 'currencyIsoCode',
      title: wording.accounting.currency,
      width: 90,
      isSortable: true,
    },
    {
      key: 'debit',
      title: wording.accounting.debit,
      dataType: NvColumnDataType.Decimal,
      width: 80,
      isSortable: true,
      alignment: NvCellAlignment.Right,
    },

    {
      key: 'credit',
      title: wording.accounting.credit,
      dataType: NvColumnDataType.Decimal,
      width: 80,
      isSortable: true,
      alignment: NvCellAlignment.Right,
    },
    {
      key: 'balanceDebit',
      title: wording.accounting.balanceDebit,
      dataType: NvColumnDataType.Decimal,
      width: 120,
      isSortable: true,
      customFormatFn: row => {
        const diff: number = (row.debit - row.credit) || 0;
        return diff >= 0 ? formatNumberWrapper(diff, 1, 2, 2, locale) : formatNumberWrapper(0, 1, 2, 2, locale);
      },
      alignment: NvCellAlignment.Right,
    },

    {
      key: 'balanceCredit',
      title: wording.accounting.balanceCredit,
      dataType: NvColumnDataType.Decimal,
      width: 120,
      isSortable: true,
      customFormatFn: row => {
        const diff: number = (row.credit - row.debit) || 0;
        return diff >= 0 ? formatNumberWrapper(diff, 1, 2, 2, locale) : formatNumberWrapper(0, 1, 2, 2, locale);
      },
      alignment: NvCellAlignment.Right,
    }]
});
