import { IPaymentStepRow, IPaymentCondition } from 'src/app/core/models/resources/IPaymentConditions';
import { wording } from 'src/app/core/constants/wording/wording';
import * as moment from 'moment';

export const getPaymentDateOfCondition = (
  invoiceDate: string,
  numberOfDays: number
): string => {
  return moment.utc(invoiceDate)
    .local().add(numberOfDays, 'days').toISOString();
};

export const getDiscounts = (
  paymentCondition: IPaymentCondition,
  invoiceDate: string
): IPaymentStepRow[] => {

  const defaultPaymentNet = 14;
  const paymentSteps: IPaymentStepRow[] = [];


  const defaultPaymentStep: IPaymentStepRow = {
    step: wording.accounting.net,
    percentage: 0,
    days: defaultPaymentNet,
    paymentDate: getPaymentDateOfCondition(
      invoiceDate,
      defaultPaymentNet
    ),
  };

  if (!paymentCondition) {
    return [defaultPaymentStep];
  }

  if (paymentCondition.percentageDiscountLevel1) {
    paymentSteps.push({
      step: wording.accounting.step1,
      percentage: paymentCondition.percentageDiscountLevel1,
      days: paymentCondition.daysDiscountLevel1,
      paymentDate: getPaymentDateOfCondition(
        invoiceDate,
        paymentCondition.daysDiscountLevel1
      )
    });
  }

  if (paymentCondition.percentageDiscountLevel2) {
    paymentSteps.push({
      step: wording.accounting.step2,
      percentage: paymentCondition.percentageDiscountLevel2,
      days: paymentCondition.daysDiscountLevel2,
      paymentDate: getPaymentDateOfCondition(
        invoiceDate,
        paymentCondition.daysDiscountLevel2
      )
    });
  }

  if (paymentCondition.percentageDiscountLevel3) {
    paymentSteps.push({
      step: wording.accounting.step3,
      percentage: paymentCondition.percentageDiscountLevel3,
      days: paymentCondition.daysDiscountLevel3,
      paymentDate: getPaymentDateOfCondition(
        invoiceDate,
        paymentCondition.daysDiscountLevel3
      )
    });
  }

  return [
    ...paymentSteps,
    {
      step: wording.accounting.net,
      percentage: 0,
      days: paymentCondition.net,
      paymentDate: getPaymentDateOfCondition(
        invoiceDate,
        paymentCondition.net
      )
    }];
};
