import { Component, OnInit } from '@angular/core';
import { ICurrency, ICurrencyCreate } from 'src/app/core/models/resources/ICurrency';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { tap } from 'rxjs/operators';
import { CurreniesHelperService } from '../currenies.helper.service';

@Component({
  selector: 'nv-currency-basic',
  templateUrl: './currency-basic.component.html',
  styleUrls: ['./currency-basic.component.scss']
})
export class CurrencyBasicComponent extends ResourceDetailView<ICurrency, ICurrencyCreate> implements OnInit {
  protected repoParams = {};
  protected resourceDefault = { isActive: false } as ICurrency;

  public routeBackUrl: string;
  public currencies: ICurrency[] = [];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private currencyService: CurrencyRepositoryService,
    private curreniesHelperService: CurreniesHelperService,
    baseComponentService: BaseComponentService,
  ) {
    super(currencyService, baseComponentService);
  }

  public init(currency: ICurrency = this.resourceDefault): void {
    this.initForm(this.fb.group({
      isoCode: [currency.isoCode, Validators.required],
      labelDe: [currency.labelDe, [Validators.required, Validators.maxLength(50)]],
      labelEn: [currency.labelEn, [Validators.required, Validators.maxLength(50)]],
      isActive: [currency.isActive],
      isFavorite: [currency.isFavorite],
    }));

    if (!this.authService.getUser().isSuperAdmin) {
      this.setDisabledFields(['isActive']);
      this.disableMarkedFields();
    }
  }

  public ngOnInit(): void {
    this.resourceDefault.isActive = this.authService.getUser().isSuperAdmin;
    this.init();
    this.routeBackUrl = this.url(sitemap.system.children.misc.children.currencies);
    const { id } = this.route.parent.snapshot.params;

    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => this.resource[this.labelKey];

  public submit(): void {
    if (this.form.invalid) {
      return;
    }
    this.creationMode
      ? this.createResource({ ...this.form.value })
        .pipe(tap(() => this.currencyService.clearStream()))
        .subscribe(createdResource => {
          this.curreniesHelperService.generateTitle(createdResource);
        })
      : this.updateResource({ ...this.resource, ...this.form.value })
        .pipe(tap(() => this.currencyService.clearStream()))
        .subscribe(updatedResource => {
          this.curreniesHelperService.generateTitle(updatedResource);
        });
  }
}
