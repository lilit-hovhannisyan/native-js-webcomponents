import { SessionInfo } from 'src/app/authentication/models/sessionInfo';
import { localStorageKey } from 'src/app/authentication/services/authentication.service';
import { NvLocale } from '../models/dateFormat';

/**
 * Formats number to selected locale format with 2 fraction digits
 * @param num
 * @param locale
 */
export const fDecimal = (num: number): string => {
  const session: SessionInfo = JSON.parse(localStorage.getItem(localStorageKey));
  const locale: NvLocale = session.user.locale;

  const formatOptions: Intl.NumberFormatOptions = {
    minimumFractionDigits: 2
  };
  switch (locale) {
    case NvLocale.DE:
      return num.toLocaleString('de-DE', formatOptions);
    case NvLocale.UK:
      return num.toLocaleString('en-GB', formatOptions);
    case NvLocale.US:
      return num.toLocaleString('en-US', formatOptions);
    default:
      return num.toLocaleString();
  }
};
