import { TemplateRef } from '@angular/core';

export interface MeTabI {
  index: number;
  displayName: string;
  // Can't find anything recommended to put as TemplateRef's type
  // tslint:disable-next-line:no-any
  content: TemplateRef<any>;
  // tslint:disable-next-line:no-any
  labelContent?: TemplateRef<any>;
}
