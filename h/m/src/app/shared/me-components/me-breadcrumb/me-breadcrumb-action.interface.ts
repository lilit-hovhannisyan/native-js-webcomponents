import { TemplateRef } from '@angular/core';

import { MeButtonI } from '../me-button/me-button.interface';

export interface MeBreadcrumbActionI {
  button: {
    id: string;
    data: MeButtonI;
    text: string;
    // Can't find anything recommended to put as TemplateRef's type
    // tslint:disable-next-line:no-any
    content?: TemplateRef<any>;
  };
  callBack: () => void;
}
