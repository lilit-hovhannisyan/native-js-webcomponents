import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'me-table-email',
  templateUrl: './table-email.component.html',
  styleUrls: ['./table-email.component.scss']
})
export class TableEmailComponent implements OnInit {
  @Input() data!: string;

  constructor() {}

  ngOnInit(): void {}
}
