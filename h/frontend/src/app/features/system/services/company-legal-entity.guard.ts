import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild,
  Router,
  CanActivate
} from '@angular/router';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { map, catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { ICompany } from 'src/app/core/models/resources/ICompany';

@Injectable()
export class CompanyLegalEntityGuardService implements CanActivate {

  constructor(
    private router: Router,
    private companyRepo: CompanyRepositoryService,
  ) {}

  public async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const companyId: number = +route.parent.params.id;
    let canActivate: boolean = !!companyId;
    const exitRoute = url(sitemap.main);

    if (companyId) {
      const company: ICompany = this.companyRepo
        .getStreamValue()
        .find(c => c.id === companyId);

      const company$ = company
        ? of(company)
        : this.companyRepo.fetchOne(companyId, {});

      canActivate = await company$.pipe(
        map(c => !c.isPrivatePerson),
        catchError(() => of(false)),
      ).toPromise();
    }

    if (!canActivate) {
      this.router.navigate([exitRoute]);
    }

    return canActivate;
  }
}
