import { IWithActive } from './IWithActive';
import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';
import { IWithSystemFlag } from './IWithSystemFlag';

export type CategoryKey = number;

interface ISharedProps extends IWithLabels, IWithActive, IWithSystemFlag {
}

export interface ICategoryBase extends ISharedProps, IResource<number> { }

export interface ICategoryCreate extends ISharedProps { }

export interface ICategoryDTO extends ICategoryBase { }

export interface ICategory extends ICategoryDTO, IWithLabel { }

export interface ICategoryFull extends ICategory {
  hasConnection: boolean;
}
