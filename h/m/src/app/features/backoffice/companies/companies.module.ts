import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeTableModule } from '@shared/me-components/me-table/me-table.module';

import { CompaniesRoutingModule } from './companies-routing.module';
import { CompaniesComponent } from './companies.component';
import { MeAddListCardModule } from '@shared/me-components/me-add-list-card/me-add-list-card.module';
import { MeSearchInputModule } from '@shared/me-components/me-search-input/me-search-input.module';
import { MeSearchBarModule } from '@shared/me-components/me-search-bar/me-search-bar.module';
import { MeIconsModule } from '@shared/me-components/me-icons/me-icons.module';
@NgModule({
  declarations: [CompaniesComponent],
  imports: [CommonModule, CompaniesRoutingModule, MeTableModule, MeSearchInputModule, MeAddListCardModule, MeSearchBarModule, MeIconsModule]
})
export class CompaniesModule {}
