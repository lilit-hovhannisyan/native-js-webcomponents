import { FormGroup } from '@angular/forms';
import { MeCompositeDataI } from '@shared/me-components/me-attribute/me-composite-input/me-composite-data.interface';
import { MeCompositeInputI } from '@shared/me-components/me-attribute/me-composite-input/me-composite-input.interface';
import { MeInputErrorI } from '@shared/me-components/me-attribute/me-input-error/me-input-error.interface';
import { EnumValueModel } from '../enum/enum-value.model';
import { DMCompositeAttributeModel } from './attribute.model';

export class MeCompositeInputAttribute implements MeCompositeInputI {
  private _data!: DMCompositeAttributeModel;
  private _form!: FormGroup;
  private _inputErrors!: MeInputErrorI;

  constructor() {}

  public set data(data: DMCompositeAttributeModel) {
    this._data = data;
  }

  public get data(): DMCompositeAttributeModel {
    return this._data;
  }

  public set form(form: FormGroup) {
    this._form = form;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get displayName(): string {
    return this.data.displayName;
  }

  public get internalName(): string {
    return this.data.internalName;
  }

  public get required(): boolean {
    return this.data.required;
  }

  public get optRequired(): boolean {
    return this.data.optRequired;
  }

  public get value(): MeCompositeDataI {
    return this.data.value;
  }

  public get values(): EnumValueModel[] {
    return this.data.values;
  }

  public get disabled(): boolean {
    return this.data.disabled;
  }

  public set inputErrors(inputErrors: MeInputErrorI) {
    this._inputErrors = inputErrors;
  }

  public get inputErrors(): MeInputErrorI {
    return this._inputErrors;
  }
}
