import { IUserRole } from './../../../../../../../core/models/resources/IUserRole';
import { UserRepositoryService } from './../../../../../../../core/services/repositories/user-repository.service';
import { IUser, UserKey } from './../../../../../../../core/models/resources/IUser';
import { GridConfigService } from '../../../../../../../core/services/grid-config.service';
import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { Language } from '../../../../../../../core/models/language';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { UserRoleRepositoryService } from 'src/app/core/services/repositories/user-role.repository.service';
import { ActivatedRoute } from '@angular/router';
import { orderBy } from 'lodash';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/roles-users.config';
import { ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';

@Component({
  templateUrl: './role-users.component.html',
  styleUrls: ['./role-users.component.scss']
})
export class RoleUsersComponent implements OnInit {

  public roleId: number;

  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IUser[]>;

  public editMode = false;
  private editMode$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private selectedUsers: UserKey[] = [];
  public ACL_ACTIONS = ACL_ACTIONS;
  constructor(
    private userRepositoryService: UserRepositoryService,
    private userRoleRepo: UserRoleRepositoryService,
    public gridConfigService: GridConfigService,
    public route: ActivatedRoute,
  ) { }

  public ngOnInit(): void {
    this.gridConfig = getGridConfig();
    this.roleId = +this.route.parent.snapshot.params.id;

    const queryParams = { roleId: this.roleId };
    this.userRepositoryService.fetchAll({}).subscribe();
    this.userRoleRepo.fetchAll({ queryParams }).subscribe();

    this.dataSource$ = combineLatest([
      this.userRepositoryService.getStream(),
      this.userRoleRepo.getStream(),
      this.editMode$
    ]).pipe(map(([
      users,
      userRoles,
      editMode,
    ]) => {
      this.selectedUsers = userRoles.map(userRole => userRole.userId);

      return orderBy(users
        .filter(user => editMode || this.userIsAssignedToRole(user, userRoles))
        .map(user => ({
          ...user,
          _checked: editMode && this.userIsAssignedToRole(user, userRoles)
        })), ['_checked', user => user.username.toLowerCase()], ['desc', 'asc']);
    }));
  }

  public userIsAssignedToRole = (user: IUser, userRoles: IUserRole[]): boolean => {
    return !!userRoles
      .filter(userRole => userRole.roleId === this.roleId)
      .find(userRole => userRole.userId === user.id);
  }

  public enterEditMode() {
    this.editMode = true;
    this.editMode$.next(true);
    this.toggleGridSelectionType();
  }

  public cancelEditing() {
    this.editMode = false;
    this.editMode$.next(false);
    this.toggleGridSelectionType();
  }

  public toggleGridSelectionType = () => {
    this.gridConfig = {
      ...this.gridConfig,
      rowSelectionType: this.editMode
        ? NvGridRowSelectionType.Checkbox
        : undefined
    };
  }

  public submit() {
    this.userRoleRepo.setUsersForRole(this.selectedUsers, this.roleId)
      .subscribe(() => this.cancelEditing());
  }

  public rowSelectionChanged(users: IUser[]) {
    this.selectedUsers = users.map(user => user.id);
  }
}
