export interface IGitVersion {
  commits: { hash: string; msg: string; }[];
  tag: string;
}
