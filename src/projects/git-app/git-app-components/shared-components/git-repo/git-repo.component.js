import {BaseComponent} from '../../../../../library/core/base-component/base.component.js';
import {basePath} from '../../../data/base.path.js';


export class GitRepo extends BaseComponent {
  constructor() {
    super(basePath +  'shared-components/git-repo/git-repo.component.css');
  }

  render = () => {
    const config = {
      tagName: 'div',
      attr: {
        class: 'git-repo',
      },
      subElements: [
        {
          tagName: 'div',
          attr: {
            class: 'name'
          },
          subElements:[
            {
              tagName: 'text-component',
              attr: {
                selector: 'span',
                text: 'Name:',
              },
            },
            {
              tagName: 'text-component',
              attr: {
                selector: 'span',
                text: this.getAttribute('name'),
              },
            }
          ]
        },
        {
          tagName: 'div',
          attr: {
            class: 'url'
          },
          subElements:[
            {
              tagName: 'text-component',
              attr: {
                selector: 'span',
                text: 'Url:',
              },
            },
            {
              tagName: 'a',
              attr: {
                target: '_blank',
                href: this.getAttribute('url')
              },
              text: this.getAttribute('url')
            }
          ]
        }
      ]
    };

    const el = this.createElementWithAttributes(config);
    this.removeStaticAtributesFromElNotToRender(this, ['url', 'name', 'forks', 'watchers']);

    this.shadow.appendChild(el);
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }
}