import { Component, OnDestroy, OnInit, ChangeDetectorRef, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, NavigationEnd } from '@angular/router';
import * as moment from 'moment';
import { NvGridConfig } from 'nv-grid';
import { forkJoin, Observable, of, Subscription } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/time-charter.config';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { ITimeCharter } from 'src/app/core/models/resources/ITimeCharter';
import { IVesselAccounting } from 'src/app/core/models/resources/IVesselAccounting';
import { IVoyage, IVoyageCreate, VoyageKey } from 'src/app/core/models/resources/IVoyage';
import { IVoyageCharterName } from 'src/app/core/models/resources/IVoyageCharterName';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { TimeCharterMainService } from 'src/app/core/services/mainServices/time-charter-main.service';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';
import { VoyageCharternameRepositoryService } from 'src/app/core/services/repositories/voyage-chartername-repository.service';
import { VoyagesRepositoryService } from 'src/app/core/services/repositories/voyages-repository.service';

@Component({
  templateUrl: './voyage-basic.component.html',
  styleUrls: ['./voyage-basic.component.scss'],
})
export class VoyageBasicComponent extends ResourceDetailView<
IVoyage,
IVoyageCreate
> implements OnInit, OnDestroy {
  public repoParams = {};
  public routeBackUrl: string;
  public form: FormGroup;
  public voyageId?: VoyageKey;
  public vesselAccountingId: number;
  public timeCharters$?: Observable<ITimeCharter[]>;
  public timeCharterGridConfig?: NvGridConfig;
  public vesselAccountings: IVesselAccounting[] = [];
  public routerSubscription: Subscription;
  public charternames: IVoyageCharterName[];
  // needed to avoid on time of save do an unnecessary request
  public isInitiallyCharternamesExists: boolean;
  public readonly charternameZone = 'chartering.misc.voyages.charternames';
  public timeChartersList: ITimeCharter[] = [];

  public resourceDefault = {
    year: +moment().startOf('year').format('YYYY'),
  } as IVoyage;

  public formElement: ElementRef;

  constructor(
    public voyagesRepo: VoyagesRepositoryService,
    baseComponentService: BaseComponentService,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
    private timeCharterMainService: TimeCharterMainService,
    private route: ActivatedRoute,
    private charternameRepo: VoyageCharternameRepositoryService,
    private fb: FormBuilder,
    public authService: AuthenticationService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef
  ) {
    super(voyagesRepo, baseComponentService);
  }

  public ngOnInit(): void {
    this.vesselAccountingId = +this.route.snapshot.queryParams['vesselAccountingId'];
    this.routeBackUrl = url(sitemap.chartering.children.misc.children.voyages);

    this.setMode();

    if (this.creationMode) {
      this.routerSubscription = this.router.events
        .pipe(filter(event => event instanceof NavigationEnd))
        .subscribe(() => {
          this.setMode();
          this.routerSubscription.unsubscribe();
        });
    }

    this.vesselAccountingRepo.fetchAll(this.repoParams)
      .subscribe(vessels => this.vesselAccountings = vessels);

    this.inEditMode$.subscribe(editMode => {
      if (editMode) {
        const hasConnectedEntities = this.timeChartersList.length || this.charternames.length;
        this.changeDatesEnabledState(!hasConnectedEntities);
      }
    });
  }

  public getTitle = () =>
    `${moment(this.resource.year).format('YYYY')}-${this.resource.no.toString().padStart(3, '0')}`

  public setMode(): void {
    const { id } = this.route.snapshot.parent.params;

    if (id === 'new') {
      this.enterCreationMode();
      this.fetchCharternames();
    } else {
      this.voyageId = +id;

      this.loadResource(id).subscribe(() => {
        // `timeCharters` response should be after `voyages` response and form initialization
        // to avoid error because `setDisabledFields` function uses `this.form`
        this.fetchTimeCharters();
        this.fetchCharternames();
      });
    }
  }

  private changeDatesEnabledState(enabled = false) {
    let disabledFields = this.getDisabledFields();

    if (enabled) {
      disabledFields = disabledFields.filter(fieldName => {
        return fieldName !== 'from' && fieldName !== 'until';
      });
    } else {
      disabledFields = Array.from(new Set(['from', 'until', ...disabledFields]));
    }

    this.form.enable();
    this.setDisabledFields(disabledFields);
  }

  private fetchCharternames(): void {
    if (this.creationMode) {
      this.charternames = [];
      this.isInitiallyCharternamesExists = true;
    } else {
      this.voyagesRepo.fetchCharternames(this.voyageId).subscribe(charternames => {
        this.isInitiallyCharternamesExists = !charternames.length;
        this.charternames = charternames;
      });
    }
  }

  public init(voyage: IVoyage = this.resourceDefault): void {
    const vesselAccountingId = voyage.vesselAccountingId || this.vesselAccountingId;
    const voyageNo = voyage.no ? voyage.no.toString().padStart(3, '0') : null;
    this.initForm(this.fb.group({
      no: [voyageNo],
      from: [voyage.from, [Validators.required]],
      until: [voyage.until, [Validators.required]],
      vesselAccountingId: [vesselAccountingId, [Validators.required]],
      year: [this.yearToDate(voyage.year), [Validators.required]],
    }));

    this.setDisabledFields([...this.getDisabledFields(), 'no', 'vesselAccountingId']);
    this.routeBackUrlQueryParams = { vesselAccountingId };
  }

  private fetchTimeCharters = (): void => {
    const queryParams = { voyageId: this.voyageId };
    this.timeCharters$ = this.timeCharterMainService.fetchAll({ queryParams }).pipe(
      map(timeChartersList => this.timeCharterMainService.toRows(timeChartersList)),
      tap(timeCharters => this.timeChartersList = timeCharters),
    );

    this.timeCharterGridConfig = getGridConfig(
      { edit: this.onTimeCharterSelect },
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
    );
  }

  public onTimeCharterSelect = (timeCharterId: number) => {
    const baseUrl = url(sitemap.chartering.children.misc.children.timeCharters);
    this.router.navigate([baseUrl, timeCharterId]);
  }

  public submit() {
    if (this.form.invalid) { return; }

    const formValue = this.form.getRawValue();
    const freshResource = this.creationMode
      ? formValue
      : ({ ...formValue, id: this.resource.id });

    freshResource.year = this.getYearOfDate(formValue.year);

    if (this.creationMode) {
      this.createResource(freshResource, false)
        .subscribe(({ id }) => {
          this.voyageId = id;
          this.fetchTimeCharters();
          this.saveCharternames();
          this.router.navigate(
            [ this.url(sitemap.chartering.children.misc.children.voyages), id ],
            { skipLocationChange: false, replaceUrl: true }
          );
        });
    } else {
      this.updateResource(freshResource).subscribe(() => {
        this.saveCharternames();
      });
    }
  }

  private saveCharternames(): void {
    const charternamesFull = this.charternames.map(chartername => ({
      ...chartername,
      voyageId: this.voyageId,
    }));

    const isEmpty = !this.charternames.length;
    let request: Observable<IVoyageCharterName[]>;

    if (this.isInitiallyCharternamesExists) {
      // creation
      if (!isEmpty && this.authService.canCreate(this.charternameZone)) {
        request = this.charternameRepo.create(charternamesFull);
      }
    } else {
      if (isEmpty && this.authService.canDelete(this.charternameZone)) {
        // deletion
        request = this.charternameRepo.deleteAllByVoyageId(this.voyageId).pipe(map(() => []));
      } else if (!isEmpty && this.authService.canUpdate(this.charternameZone)) {
        // edition
        request = this.charternameRepo.update(this.voyageId, charternamesFull);
      }
    }

    request?.subscribe(charternames => {
      this.isInitiallyCharternamesExists = !charternames.length;
      this.charternames = charternames;
    });
  }

  public charternamesChange(charternames: IVoyageCharterName[]): void {
    this.charternames = charternames;

    // disable dates
    const hasConnectedEntities = this.timeChartersList?.length || charternames?.length;
    this.changeDatesEnabledState(!hasConnectedEntities);

    // make dirty to let unsaved-changes guard stop routing
    this.form.markAsDirty();
  }

  public createTimeCharter() {
    const baseUrl = url(sitemap.chartering.children.misc.children.timeCharters);
    const queryParams = { voyageId: this.voyageId };

    this.router.navigate(
      [baseUrl, 'new'],
      { queryParams },
    );
  }

  private yearToDate(year: number): Date {
    return moment().year(year).startOf('year').toDate();
  }

  private getYearOfDate(isoDate: string) {
    return new Date(isoDate).getFullYear();
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

  public ngOnDestroy(): void {
    if (this.routerSubscription) {
      this.routerSubscription.unsubscribe();
    }
  }
}
