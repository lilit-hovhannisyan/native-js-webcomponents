import {Component, EventEmitter, Input, Output} from '@angular/core';
import {RadioButtonOption} from '../radio-button/radio-button.component';

@Component({
  selector: 'lavash-radio-button-list',
  templateUrl: 'radio-button-list.component.html',
  styleUrls: ['radio-button-list.component.less']
})

export class RadioButtonListComponent {
  @Input() radioButtonOptions: RadioButtonOption[] = [
    {
      key: '1',
      value: 'Test data'
    },
    {
      key: '2',
      value: 'Another test data'
    },
    {
      key: '3',
      value: 'Final test data'
    }
  ];
  @Output() radioButtonSelectOption = new EventEmitter<RadioButtonOption[]>();


  onSelectOption(o) {
    this.radioButtonOptions.map( option => {
      option.selected = false;

      if (option.key === o.key) {
        option.selected = true;
      }
    });

    this.radioButtonSelectOption.emit(this.radioButtonOptions);
  }
}

