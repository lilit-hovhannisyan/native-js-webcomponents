import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MeTranslationService {
  private _url: string = 'assets/i18n/';
  private _languages: string[] = [];
  private _language: string = '';

  private _translationsChanged: Subject<void> = new Subject();
  private _translations: BehaviorSubject<MeTranslationI> = new BehaviorSubject({});

  private languageCache: { [key: string]: MeTranslationI } = {};
  private languageChangeSubscription: Subscription | null = null;

  constructor(private http: HttpClient) {}

  public setLanguages(languages: string[]): void {
    this._languages = languages;
  }

  public getLanguages(): string[] {
    return this._languages;
  }

  public getLanguage(): string {
    return this._language;
  }

  public get translations(): MeTranslationI {
    return this._translations.getValue();
  }

  public onLanguageChanged(): Subject<void> {
    return this._translationsChanged;
  }

  public getTranslations(): BehaviorSubject<MeTranslationI> {
    return this._translations;
  }

  public setLanguage(language: string): void {
    language = this._languages.indexOf(language) !== -1 ? language : this._languages[0];
    if (this._language !== language) {
      if (this.languageChangeSubscription) {
        this.languageChangeSubscription.unsubscribe();
        this.languageChangeSubscription = null;
      }
      if (this.languageCache[language]) {
        this._language = language;
        this._translations.next(this.languageCache[language]);
        this._translationsChanged.next();
      } else {
        this.languageChangeSubscription = this.http.get(this._url + language + '.json').subscribe((response) => {
          this._language = language;
          this.languageCache[language] = response as MeTranslationI;
          this._translations.next((response as MeTranslationI) || {});
          this._translationsChanged.next();
        });
      }
    }
  }

  public translate(key: string, params?: { [param: string]: number | string }): string {
    let translation = this.translations[key] || this.prettyCamelCase(key);
    if (params) {
      for (const param in params) {
        if (params.hasOwnProperty(param)) {
          translation = translation.replace(
            '{{' + param + '}}',
            (typeof params[param] === 'number' ? params[param] : params[param] || '').toString()
          );
        }
      }
    }
    return translation;
  }

  public getBrowserLanguage(): string {
    const language: string[] = (window.navigator.language || '').split('-');
    return language[0].toLowerCase();
  }

  public prettyCamelCase(name: string): string {
    const words = name.match(/[A-Za-z][a-z]*/g) || [];
    return words.map(this.capitalize).join(' ');
  }

  public capitalize(word: string): string {
    return word.charAt(0).toUpperCase() + word.substring(1);
  }
}
