import { Injectable } from '@angular/core';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IShipyardTypeCreate, IShipyardType, IShipyardTypeDTO, ShipyardTypeKey } from '../../models/resources/IShipyardType';

@Injectable({
  providedIn: 'root'
})
export class ShipyardTypeRepositoryService extends AbstractRepository<
  IShipyardType,
  IShipyardTypeDTO,
  IShipyardTypeCreate,
  RepoParams<IShipyardType>,
  ShipyardTypeKey
>  {
  protected resourceType = 'ShipyardType';
  protected sortKey = 'label';
  protected sortAsce = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/shipyard-types';

  protected postProcess = (item: IShipyardTypeDTO): IShipyardType => item;
}
