import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { IWording } from 'src/app/core/models/resources/IWording';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { validateAllFormFields } from '../../helpers/validateAllFormFields';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-delete-reason-modal',
  templateUrl: './delete-reason-modal.component.html',
  styleUrls: ['./delete-reason-modal.component.scss']
})
export class DeleteReasonModalComponent implements OnInit {
  @Input() public messages: IWording[] = [];
  @Input() public footerMessage: IWording;
  public form: FormGroup;
  public wording = wording;

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
  ) { }

  public ngOnInit() {
    this.form = this.fb.group({
      text: ['', [Validators.required, Validators.maxLength(100), Validators.minLength(3)]]
    });
  }

  public submit() {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
    }

    this.modalRef.close(this.form.value.text);
  }

  public cancel() {
    this.modalRef.close();
  }

}
