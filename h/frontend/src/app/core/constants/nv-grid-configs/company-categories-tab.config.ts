import { NvColumnDataType, NvFilterControl, NvGridConfig, NvGridButtonsPosition } from 'nv-grid';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { IUser } from 'src/app/core/models/resources/IUser';
import { Operations } from 'src/app/core/models/Operations';
import { wording } from '../wording/wording';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

export const getCompanyCategoriesTabGridConfig = (
  actions: IGridConfigActions,
  canPerform: (operation: Operations) => boolean
): NvGridConfig => {
  return {
    gridName: 'companyCategoriesTabGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    title: wording.system.companyCategoriesTitle,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 10
    },
    columns: [
      {
        key: 'id',
        title: 'ID',
        visible: false,
      },
      {
        key: 'displayLabel',
        title: wording.system.categories,
        width: 400,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'delete',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: (user: IUser) => actions.delete(user),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.add,
        tooltip: wording.general.add,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: actions.add
      },
    ],
    contextMenuButtons: [
      ...(canPerform(Operations.READ) && getContextMenuButtonsConfig()),
    ],
  };
};

