import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ReplaySubject, Observable } from 'rxjs';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { CompanyKey, ICompany } from 'src/app/core/models/resources/ICompany';
import { Validators } from 'src/app/core/classes/validators';
import { IWording } from 'src/app/core/models/resources/IWording';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { SettingsService } from 'src/app/core/services/settings.service';
import { ISilentPartnerRepresentative } from 'src/app/core/models/resources/ISilentPartnerRepresentative';
import { wording } from 'src/app/core/constants/wording/wording';
import { map, distinctUntilChanged, startWith } from 'rxjs/operators';
import { SilentPartnerRepositoryService } from 'src/app/core/services/repositories/silent-partner-repository.service';
import * as moment from 'moment';
import { CompaniesHelperService } from '../../../companies.helper.service';
import { LegalEntityPartnersModalComponent } from '../../../../../../../shared/legal-entity-partners-modal/legal-entity-partners-modal.component';

@Component({
  templateUrl: './silent-partners-modal.component.html',
  styleUrls: ['./silent-partners-modal.component.scss'],
})
export class SilentPartnersModalComponent implements OnInit, OnDestroy {
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  @Input() public silentPartner?: ISilentPartnerRepresentative;
  @Input() public companyId?: CompanyKey;
  public form: FormGroup;
  public isRequestPending = false;
  public wording = wording;
  public companies$: Observable<ICompany[]>;

  constructor(
    public modalRef: NzModalRef,
    private modalService: NzModalService,
    private fb: FormBuilder,
    private companyRepo: CompanyRepositoryService,
    private silentPartnerRepo: SilentPartnerRepositoryService,
    private settingsService: SettingsService,
    private companiesHelperService: CompaniesHelperService,
  ) { }

  public ngOnInit() {
    this.init(this.silentPartner);
    this.setCompaniesSource();
  }

  private init(entity?: ISilentPartnerRepresentative): void {
    const currentDate: string = moment().toISOString();

    this.form = this.fb.group({
      companyId: [entity?.companyId || this.companyId],
      contribution: [
        entity?.contribution,
        [ Validators.required, Validators.positiveNumberFloat, Validators.maxDecimalDigits(2) ],
      ],
      partnerId: [entity?.partnerId, [Validators.required]],
      currencyId: [entity?.currencyId, [Validators.required]],
      isAtypicalSilentPartner: [entity?.isAtypicalSilentPartner],
      isActive: [{
        value: entity ? entity.isActive : true,
        disabled: true,
      }],
      from: [entity?.from || currentDate, [Validators.required]],
      to: [ entity?.to ], // validators will be added dynamically
      remark: [entity?.remark],
    });

    this.setDatesValidations();
  }

  private setDatesValidations(): void {
    const { from, to, isActive } = this.form.controls;
    from.valueChanges.pipe(
      distinctUntilChanged(),
      startWith(from.value),
    ).subscribe(value => {
      to.setValidators(
        Validators.dateShouldBeAfter(
          value,
          this.settingsService.locale,
        ),
      );
      to.updateValueAndValidity();
    });

    to.valueChanges.pipe(
      distinctUntilChanged(),
      startWith(to.value),
    ).subscribe(value => {
      const currentDayStart: moment.Moment = moment().utc().startOf('d');
      const valueDayStart: moment.Moment = value ? moment(value).utc().startOf('d') : null;

      isActive.setValue(!value || currentDayStart.isSameOrBefore(valueDayStart));
    });
  }

  private setCompaniesSource(): void {
    this.companies$ = this.companyRepo.fetchAll({}).pipe(
      map((companies: ICompany[]) => {
        this.companiesHelperService.setCompany(this.companyId);
        return companies.filter(c => c.id !== this.companyId);
      }),
    );
  }

  public submit(): void {
    validateAllFormFields(this.form);
    if (this.form.invalid || this.isRequestPending) {
      return;
    }

    this.companiesHelperService.checkAndCreateLegalData(this.createSilentPartner);
  }

  private createSilentPartner = (): void => {
    const { value } = this.form;

    const freshValue: ISilentPartnerRepresentative = {
      ...this.silentPartner,
      ...value,
    };

    const request$: Observable<ISilentPartnerRepresentative> = this.silentPartner
      ? this.silentPartnerRepo.update(freshValue, {})
      : this.silentPartnerRepo.create(freshValue, {});

    this.isRequestPending = true;
    request$.subscribe(
      result => {
        this.companiesHelperService.setLegalDataState(!!result);
        this.modalRef.close(result);
      },
      () => this.isRequestPending = false,
    );
  }

  public openPartnersModal(): void {
    const partnersIds: CompanyKey[] = [this.silentPartner?.companyId || this.companyId];
    const titleWording: IWording = wording.system.silentPartner;

    this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: titleWording[this.settingsService.language],
      nzContent: LegalEntityPartnersModalComponent,
      nzComponentParams: {
        partnersIds,
        onlyCreate: true,
      },
      nzWidth: 500,
      nzClosable: true,
    }).afterClose.subscribe((companyId: CompanyKey) => {
      if (!companyId) {
        return;
      }

      this.form.controls.partnerId.setValue(companyId);
    });
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }
}
