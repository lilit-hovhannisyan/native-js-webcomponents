import { NgModule } from '@angular/core';
import { DecimalPipe, CurrencyPipe, CommonModule } from '@angular/common';
import { EnvironmentService } from './services/environment.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { GlobalErrorHandler } from './interceptors/global-error-handler';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { ResponseNotificationInterceptor } from './interceptors/response-notification-interceptor';
import { ResourceInterceptor } from './interceptors/resource-interceptor';
import { AuthInterceptor } from './interceptors/auth-interceptor';
import { LoadingInterceptor } from './interceptors/loading.interceptor';
import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';

@NgModule({
  imports: [CommonModule],
  providers: [
    EnvironmentService,
    DecimalPipe,
    CurrencyPipe,
    /**
     * This is the right order for interceptors!
     * Explanation: observation logic in interceptors that are observing the REQUEST
     *   will be run by this ordering(first in the array will be called first - like in queues).
     *   But for interceptors that are observing the RESPONSE of the request, will
     *   be in opposite order(first in the array will be called last - like in stacks).
     *   So - low(high) priority response-observing interceptors should be higher(lower)
     *   and low(high) priority request-observing interceptors should be lower(higher)
     */
    /// response interceptors
    { provide: HTTP_INTERCEPTORS, useClass: GlobalErrorHandler, multi: true }, // response-interceptor with low priority
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ResponseNotificationInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ResourceInterceptor, multi: true }, // response-interceptor with high priority
    /// request interceptors
    /**
     * AuthInterceptor is a request-response-interceptor with high priority
     * (it handles response-error with 404 status).
     */
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true }, // request-interceptor with low priority
    /// other providers
    { provide: NZ_I18N, useValue: en_US }
  ]
})
export class CoreModule { }
