import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthWebService } from './auth.web-service';
@NgModule({
  imports: [CommonModule, AuthRoutingModule],
  providers: [AuthWebService]
})
export class AuthModule {}
