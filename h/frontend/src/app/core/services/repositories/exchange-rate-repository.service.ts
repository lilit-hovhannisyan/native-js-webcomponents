import { Injectable } from '@angular/core';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { IExchangeRate, IExchangeRateBase, IExchangeRateCreate, IExchangeRateDTO, ExchangeRateKey } from '../../models/resources/IExchangeRate';
import { IExchangeRateCalculated } from '../../models/resources/IExchangeRateCalculated';
import { RepositoryService } from '../repository.service';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { DataCarrier } from '../../models/DataCarrier';
import { HttpPost } from '../../models/http/http-post';
import { map } from 'rxjs/operators';
import { HttpGet } from '../../models/http/http-get';

export interface IExchangeRateRepoParams extends RepoParams<IExchangeRate> {
  queryParams: {
    startDate: string;
    endDate: string;
    currencyId?: number;
  };
}

export interface IExchangeRateCreateDocumentParams {
  startDate: string;
  endDate: string;
  currencyId: number;
  baseCurrencyId?: number;
}

export interface IExchangeRateCalculatedRepoParams extends RepoParams<IExchangeRateCalculated> {
  queryParams: {
    startDate: string;
    endDate: string;
    currencyId?: number;
    baseCurrencyId?: number;
  };
}

@Injectable({
  providedIn: 'root'
})
export class ExchangeRateRepositoryService extends AbstractRepository<
IExchangeRate,
IExchangeRateBase,
IExchangeRateCreate,
IExchangeRateRepoParams,
ExchangeRateKey
> {

  protected resourceType = 'ExchangeRate';

  public sortKey = 'date';
  public sortAsce = false;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IExchangeRateDTO): IExchangeRate => ({
    ...item,
    date: moment(item.date, 'YYYY-MM-DD').format('YYYY-MM-DD')
  })

  public getBaseUrl = (): string => '/api/exchange-rates/';

  public createDocument(params: IExchangeRateCreateDocumentParams): Observable<void> {
    const url = this.getBaseUrl();
    const request = new HttpPost<DataCarrier>(
      `${url}/document`,
      {},
      { init: { params: params } },
    );

    return this.repositoryService.httpClient.request(request).pipe(
      map(() => undefined)
    );

  }

  public fetchCalculatedRates(params: IExchangeRateCalculatedRepoParams) {
    const request = new HttpGet(`/api/exchange-rates/calculated`, { init: { params: params.queryParams } });
    return this.repositoryService.httpClient.request(request).pipe(
      map((response: any) => response.body.data.items)
    );
  }
}
