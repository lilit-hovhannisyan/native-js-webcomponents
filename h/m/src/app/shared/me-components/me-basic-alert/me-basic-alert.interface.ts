export interface MeBasicAlertI {
  mode: 'regular' | 'error' | 'success' | 'warning' | 'light' | 'dark' | 'outlined';
  title: string;
  content: string;
  linkText?: string;
  internalLink?: string;
  externalLink?: string;
}
