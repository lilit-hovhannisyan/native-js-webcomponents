import { Component, ChangeDetectionStrategy, Input, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { NvGridConfig } from 'nv-grid';
import { getVesselTaskTemplateGridConfig } from 'src/app/core/constants/nv-grid-configs/vessel-task-template.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { IResource, ResourceKey } from 'src/app/core/models/resources/IResource';
import { IWithLabels } from 'src/app/core/models/resources/IWithLabels';
import { ITaskTemplate, TaskTemplateKey } from 'src/app/core/models/resources/Task-Templates/ITaskTemplate';

declare type TemplateData<K extends ResourceKey> = IWithLabels & IResource<K>;

/** Used for both Task and Checklist templates. */
@Component({
  templateUrl: './vessel-task-template-modal.component.html',
  styleUrls: ['./vessel-task-template-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VesselTaskTemplateModalComponent implements OnInit {
  /** Used only in template. */
  public wording = wording;
  /** Used by nv-autocomplete. */
  public templateId: TaskTemplateKey;
  /** Keeps selected template data-object for result. */
  public selectedTemplate: ITaskTemplate;

  public gridConfig: NvGridConfig;

  /** Template data source. */
  @Input() public dataSource: ITaskTemplate[];

  constructor(
    public modalRef: NzModalRef,
  ) { }

  public ngOnInit() {
    this.gridConfig = getVesselTaskTemplateGridConfig();
  }

  /** Selects template by `templateId`. */
  public selectTemplate(templateId: TaskTemplateKey): void {
    this.selectedTemplate = this.dataSource.find(item => item.id === templateId);
  }
}
