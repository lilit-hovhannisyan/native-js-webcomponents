import { Component, ChangeDetectionStrategy, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, forkJoin } from 'rxjs';

import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { ZoneRepositoryService } from 'src/app/authentication/services/zoneService/zone-repository.service';
import { RoleRepositoryService } from 'src/app/core/services/repositories/role-repository.service';
import { Operations, getIncludingOperations, getOperationName } from 'src/app/core/models/Operations';
import { IZoneRow } from 'src/app/core/models/resources/IZone';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { IRole, IRoleCreate } from 'src/app/core/models/resources/IRole';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { Validators } from 'src/app/core/classes/validators';
import { specialZones } from 'src/app/authentication/services/zoneService/specialZones';
import { IWording } from 'src/app/core/models/resources/IWording';
import { RolesHelperService } from '../roles.helper.service';

type PermissionsOperationElement = {
  label: string,
  value: any,
  checked: boolean,
  isVisible: boolean,
  disabled: boolean
};

type PermissionTreeEl = {
  zone: string,
  operations: PermissionsOperationElement[],
  fullTitle: IWording,
  description: string
};

@Component({
  templateUrl: './role-basic.component.html',
  styleUrls: ['./role-basic.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoleBasicComponent
  extends ResourceDetailView<IRole, IRoleCreate>
  implements OnInit {

  public role: IRole;

  protected resourceDefault = {} as IRole;

  public repoParams = {};
  public routeBackUrl = url(sitemap.system.children.misc.children.roles);
  public form: FormGroup;

  private zones: IZoneRow[] = [];
  public permissionsTree: PermissionTreeEl[];
  private allOperations: Operations[]; // used to iterate over in template

  // arrow functions can't be inherited via 'super' keyword before overriding,
  // that's why it is copied here for further use
  private superCancelEditing = this.cancelEditing;

  constructor(
    public route: ActivatedRoute,
    private zoneRepo: ZoneRepositoryService,
    private fb: FormBuilder,
    protected roleRepo: RoleRepositoryService,
    private rolesHelperService: RolesHelperService,
    baseComponentService: BaseComponentService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef,
  ) {
    super(roleRepo, baseComponentService);
  }

  public ngOnInit() {
    // typescript enum behaves unexpected when used as an object -> it contains key: value
    // and additionally value: key pairings... thats why we filter here:
    this.allOperations = Object.values(Operations).filter(o => typeof o === 'number') as number[];
    this.init();

    const { id } = this.route.parent.snapshot.params;
    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);

    forkJoin([
      this.zoneRepo.fetchAll({ useCache: false }),
      // to make sure all data is present, we fetch roles again here with useCache: true
      // otherwise we cant be sure loadResource has completed at this point
      !this.creationMode ? this.roleRepo.fetchOne(id, { useCache: false }) : of(undefined)
    ]).subscribe(([zones, role]) => {
      this.zones = this.zoneRepo.toRows(zones);
      this.role = role; // is undefined in creatoin mode
      this.permissionsTree = this.getPermissionsTree();
      this.cdr.detectChanges();
    });
  }

  public getTitle = () => this.resource[this.labelKey];

  public init(role: IRole = this.resourceDefault) {
    this.initForm(this.fb.group({
      labelDe: [role.labelDe, Validators.required],
      labelEn: [role.labelEn, Validators.required],
      description: [role.description, []],
      remarks: [role.remarks, []],
    }));
  }

  public getPermissionsTree(): PermissionTreeEl[] {

    const rolePermissions = this.role && this.role.permissions;

    return this.zones.map(zone => {
      const roleOperations = rolePermissions && rolePermissions[zone.id]
        ? getIncludingOperations(rolePermissions[zone.id])
        : [];

      const operations = this.allOperations.map(op => {
        const foundSpecialZone = specialZones.find(specialZone => specialZone.fullZone === zone.id);

        return {
          isVisible: !!(!foundSpecialZone || (foundSpecialZone.maxOperations & op) === op),
          label: getOperationName(op),
          value: op,
          checked: roleOperations && roleOperations.includes(op),
          disabled: !(zone.supports & op)
        };
      });

      const currentZone = this.zones.find(z => z.id.toString() === zone.id);
      const permissions: PermissionTreeEl = {
        zone: zone.id,
        fullTitle: zone.fullTitle,
        operations,
        description: currentZone && currentZone.description || '',
      };

      return permissions;
    });
  }

  public cancelEditing = (): void => {
    this.permissionsTree = this.getPermissionsTree();
    this.superCancelEditing();
  }

  public submit() {
    if (!this.form.valid) { return; }

    const permissions = {};
    this.permissionsTree.forEach(perm => {
      permissions[perm.zone] = 0;
      perm.operations.forEach(opertaion => {
        opertaion.checked && (permissions[perm.zone] += opertaion.value);
      });
    });

    const { value } = this.form;
    const role = {
      ...this.resource,
      permissions,
      ...value,
    };

    this.creationMode
      ? this.createResource(role)
      .subscribe(createdResource => {
        this.rolesHelperService.generateTitle(createdResource);
      })
      : this.updateResource(role)
      .subscribe((updatedResource) => {
        this.rolesHelperService.generateTitle(updatedResource);
        this.role = role;
      });
  }

  public setOtherPermissions(permissionOperationElements: PermissionsOperationElement[], currentOperation: PermissionsOperationElement) {
    if (currentOperation.value === Operations.READ && !currentOperation.checked) {
      permissionOperationElements.forEach(el => el.checked = false);
    } else if (currentOperation.value === Operations.UPDATE && !currentOperation.checked) {
      permissionOperationElements.forEach(el => {
        if (el.value === Operations.DELETE) {
          el.checked = false;
        }
      });
    } else if (currentOperation.value === Operations.UPDATE && currentOperation.checked) {
      permissionOperationElements.forEach(el => {
        if (el.value === Operations.READ) {
          el.checked = true;
        }
      });
    } else if (currentOperation.value === Operations.CREATE && currentOperation.checked) {
      permissionOperationElements.forEach(el => {
        if (el.value === Operations.UPDATE || el.value === Operations.READ) {
          el.checked = true;
        }
      });
    } else if (currentOperation.value === Operations.DELETE && currentOperation.checked) {
      permissionOperationElements.forEach(el => {
        if (el.value === Operations.UPDATE || el.value === Operations.READ || el.value === Operations.CREATE) {
          el.checked = true;
        }
      });
    }
  }
}



