import { CompanyKey } from 'src/app/core/models/resources/ICompany';
import { Injectable } from '@angular/core';
import { forkJoin, Observable, combineLatest } from 'rxjs';
import { BankRepositoryService } from '../repositories/bank-repository.service';
import { map, switchMap } from 'rxjs/operators';
import { IBank } from '../../models/resources/IBank';
import { BankAccountRepositoryService, BankAccountParams } from '../repositories/bank-account-repository.service';
import { CurrencyRepositoryService } from '../repositories/currency-repository.service';
import { ICurrency } from '../../models/resources/ICurrency';
import { IBankAccount, IBankAccountFull, BankAccountKey, IBankAccountRow } from '../../models/resources/IBankAccount';
import { sortBy } from 'lodash';
import { SettingsService } from '../settings.service';

@Injectable({
  providedIn: 'root'
})
export class BankAccountMainService {

  constructor(
    private bankRepo: BankRepositoryService,
    private bankAccountRepo: BankAccountRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private settingsService: SettingsService
  ) { }

  private getBankAccountFullList(
    bankAccounts: IBankAccount[],
    banks: IBank[],
    currencies: ICurrency[]
  ): IBankAccountFull[] {
    return bankAccounts.map(bankAccount => this.getBankAccountFull(bankAccount, banks, currencies));
  }

  private getBankAccountFull(
    bankAccount: IBankAccount,
    banks: IBank[],
    currencies: ICurrency[]
  ): IBankAccountFull {
    const bank = banks.find(x => x.id === bankAccount.bankId);
    const currency = currencies.find(x => x.id === bankAccount.currencyId);
    return { ...bankAccount, bank, currency, bic: bank.bic };
  }

  private getCompanyFilter(companyId): (x: IBankAccount[]) => IBankAccount[] { // This fuction returns a filter function!
    return (bankAccounts) => bankAccounts.filter(ba => ba.companyId === companyId);
  }

  public fetchAll(params: BankAccountParams): Observable<IBankAccountFull[]> {
    return forkJoin([
      this.bankAccountRepo.fetchAll(params),
      this.bankRepo.fetchAll({}),
      this.currencyRepo.fetchAll({})
    ]).pipe(map(res => this.getBankAccountFullList(...res)));
  }

  public fetchOne(bankAccountId: BankAccountKey, params: BankAccountParams) {
    return forkJoin([
      this.bankAccountRepo.fetchOne(bankAccountId, params),
      this.bankRepo.fetchAll({}),
      this.currencyRepo.fetchAll({})
    ]).pipe(map(res => this.getBankAccountFull(...res)));
  }

  public getStream(params: { companyId: CompanyKey }): Observable<IBankAccountFull[]> {
    return forkJoin([
      this.bankAccountRepo.fetchAll({}),
      this.bankRepo.fetchAll({}),
      this.currencyRepo.fetchAll({})
    ]).pipe(
      switchMap(() => combineLatest([
        this.bankAccountRepo.getStream().pipe(map(this.getCompanyFilter(params.companyId))),
        this.bankRepo.getStream(),
        this.currencyRepo.getStream()
      ])),
      map(res => this.getBankAccountFullList(...res))
    );
  }

  private toRow(bankAccount: IBankAccountFull): IBankAccountRow {
    const { currency } = bankAccount;
    return ({
      ...bankAccount,
      bankName: bankAccount.bank && bankAccount.bank.name,
      currencyLabel: currency.displayLabel
    });
  }

  public toRows(bankAccounts: IBankAccountFull[]): IBankAccountRow[] {
    return sortBy(bankAccounts.map((bankAccount) => this.toRow(bankAccount)), ['bankName']);
  }
}
