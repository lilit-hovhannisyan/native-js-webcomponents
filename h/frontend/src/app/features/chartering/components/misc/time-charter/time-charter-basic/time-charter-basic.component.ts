import { AddressMainService } from 'src/app/core/services/mainServices/address-main.service';
import { map, shareReplay, takeUntil, distinctUntilChanged, tap } from 'rxjs/operators';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { Component, OnInit, OnDestroy, ChangeDetectorRef, ElementRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { Subscription, ReplaySubject, Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { ITimeCharter, ITimeCharterCreate, ITimeCharterFull } from 'src/app/core/models/resources/ITimeCharter';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { TimeCharterRepositoryService } from 'src/app/core/services/repositories/timeCharter.repository.service';
import { IDebitorCreditorRow } from 'src/app/core/models/resources/IDebitorCreditor';
import { DebitorCreditorMainService } from 'src/app/core/services/mainServices/debitor-creditor-main.service';
import { urlConcat } from 'src/app/core/helpers/general';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NVHttpErrorResponse } from 'src/app/core/abstracts/ResourceDetailView';
import { TimeCharterMainService } from 'src/app/core/services/mainServices/time-charter-main.service';
import * as moment from 'moment';
import { AddressParams } from 'src/app/core/services/repositories/address-repository.service';
import { hasValue } from 'src/app/shared/helpers/general';
import { TimeCharterPeriodRepositoryService } from 'src/app/core/services/repositories/time-charter-period-repository.service';
import { IVoyage } from 'src/app/core/models/resources/IVoyage';
import { VoyagesMainService } from 'src/app/core/services/mainServices/voyages-main.service';
import { NvGridConfig } from 'nv-grid';
import { getGridConfig as debitorsCreditorsGridConfig } from 'src/app/core/constants/nv-grid-configs/debitors-creditors.config';
import { TimeChartersHelperService } from '../time-charters.helper.service';

@Component({
  templateUrl: './time-charter-basic.component.html',
  styleUrls: ['./time-charter-basic.component.scss']
})
export class TimeCharterBasicComponent extends ResourceDetailView<
ITimeCharter,
ITimeCharterCreate
> implements OnInit, OnDestroy {

  protected repoParams = {};
  protected resourceDefault = { isActive: true } as ITimeCharter;

  public routeBackUrl: string;
  public subscription: Subscription;
  public debitorsCreditors: IDebitorCreditorRow[] = [];
  public timeCharters: ITimeCharterFull[] = [];
  public defaultAddress?: string;
  public overrideAddress = false;
  public voyage: IVoyage;
  public voyageId: number;
  public debitorsCreditorsGridConfig: NvGridConfig = debitorsCreditorsGridConfig({}, () => false);

  private componentDestroyed$ = new ReplaySubject(1);
  private formControlSubscriptions: Subscription[] = [];
  public formElement: ElementRef;

  constructor(
    private fb: FormBuilder,
    private debitorCreditorMainService: DebitorCreditorMainService,
    private addressMainService: AddressMainService,
    private timeCharterMainService: TimeCharterMainService,
    public route: ActivatedRoute,
    private nzMessageService: NzMessageService,
    timeCharterRepo: TimeCharterRepositoryService,
    baseComponentService: BaseComponentService,
    private timeCharterPeriodRepo: TimeCharterPeriodRepositoryService,
    private voyageRepo: VoyagesMainService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef,
    private timeChartersHelperService: TimeChartersHelperService,
  ) {
    super(timeCharterRepo, baseComponentService);
  }

  public ngOnInit(): void {
    this.voyageId = +this.route.snapshot.queryParams.voyageId;

    const { id } = this.route.parent.snapshot.params;
    if (id === 'new') {
      this.loadTimeCharters();
      this.enterCreationMode();
      this.loadDebitorCreditor();
      this.loadVoyage();
    } else {
      this.loadResource(id).subscribe(() => {
        this.voyageId = this.resource.voyageId;
        this.loadVoyage();
        this.loadTimeCharters();
        this.timeCharterPeriodRepo.fetchAll({ queryParams: { timeCharterId: id } }).pipe(
          takeUntil(this.componentDestroyed$)
        ).subscribe(periods => {
          if (periods.length > 0) {
            this.setDisabledFields([...this.getDisabledFields(), 'from', 'until']);
          }
        });

        this.loadDebitorCreditor().subscribe(() => {
          // this method needs to be called when the timeCharter and debitorCreditors are loaded:
          this.loadDefaultAddress();
        });
      });
    }
  }

  public getTitle = () => '';

  public loadVoyage(): void {
    this.voyageRepo.fetchOne(this.voyageId, {})
      .subscribe(voyage => {
        this.voyage = voyage;

        const dateRangeValidator = Validators.dateShouldBeAfterBefore(
          voyage.from,
          voyage.until,
          this.settingsService.locale
        );

        this.form.get('from').setValidators([Validators.required, dateRangeValidator]);
        this.form.get('until').setValidators([dateRangeValidator]);
      });
  }

  public setRouteBackFunctionality = (): void => {
    this.routeBackUrl = urlConcat([
      this.url(sitemap.chartering.children.misc.children.voyages),
      this.voyageId,
    ]);
  }

  public loadDebitorCreditor(): Observable<IDebitorCreditorRow[]> {
    const req = this.debitorCreditorMainService.fetchAll().pipe(
      tap(dcs => {
        const dc = dcs.find(d => d.id === this.form.get('debitorCreditorId').value);
        if (!dc) { return; }
        this.browserTitleService.setTitleWithInfo(`${dc.no} ${dc.company.companyName}`);
      }),
      map(debitorsCreditors => this.debitorCreditorMainService.toRows(debitorsCreditors)),
      shareReplay(1)
    );
    req.subscribe(debitorCreditorRows => {
      this.debitorsCreditors = debitorCreditorRows;
    });

    return req;
  }

  public init(timeCharter: ITimeCharter = this.resourceDefault): void {
    this.formControlSubscriptions.forEach(sub => sub.unsubscribe());
    this.formControlSubscriptions = [];

    // `timeCharter.billingAddress` value can be empty string
    this.overrideAddress = hasValue(timeCharter.billingAddress);

    this.initForm(this.fb.group(
      {
        from: [timeCharter.from, [Validators.required]],
        until: [timeCharter.until, [
          Validators.required,
          Validators.isAfterFieldDate('from', true)
        ]],
        debitorCreditorId: [timeCharter.debitorCreditorId, [Validators.required]],
        billingAddress: [timeCharter.billingAddress, Validators.maxLength(500)],
        charterParty: [timeCharter.charterParty, [Validators.maxLength(100)]],
        charterPartyDate: [timeCharter.charterPartyDate],
        laycanFrom: [timeCharter.laycanFrom],
        laycanUntil: [timeCharter.laycanUntil, Validators.isAfterFieldDate('laycanFrom')],
        overrideAddress: [this.overrideAddress]
      }));

    if (this.overrideAddress) {
      this.form.controls.billingAddress.enable();
      this.setDisabledFields(['from', 'until']);
    } else {
      this.setDisabledFields([...this.getDisabledFields(), 'billingAddress']);
      this.form.controls.billingAddress.setValue(this.defaultAddress);
    }
    this.setFromUntilRelations();
    this.setRouteBackFunctionality();
  }

  public setFromUntilRelations(): void {
    const { from, until, laycanFrom, laycanUntil } = this.form.controls;

    const fromSub = from.valueChanges.pipe(
      takeUntil(this.componentDestroyed$),
      distinctUntilChanged(),
    ).subscribe((fromStr: string) => {
      const latestEntity = this.timeCharters[this.timeCharters.length - 1];
      const maxUntil = latestEntity && latestEntity.until;

      const isOptional = maxUntil ? moment(fromStr).diff(maxUntil) >= 0 : true;
      if (isOptional) {
        until.setValidators([Validators.isAfterFieldDate('from', true)]);
      } else {
        until.setValidators([Validators.required, Validators.isAfterFieldDate('from', true)]);
      }

      until.updateValueAndValidity();
    });

    const laycanFromSub = laycanFrom.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => laycanUntil.updateValueAndValidity());

    this.formControlSubscriptions.push(fromSub, laycanFromSub);
  }

  public loadTimeCharters = (): void => {
    const queryParams = { voyageId: this.voyageId };
    this.timeCharterMainService.fetchAll({ queryParams })
      .subscribe(timeCharters => this.timeCharters = timeCharters);
  }

  public overrideAddressChanged = (value: boolean) => {
    const control = this.form.controls.billingAddress;
    const entry = this.resource || {} as ITimeCharter;
    const dcIdValue = this.form.controls.debitorCreditorId.value;

    // in creationMode `entry.debitorCreditorId` is undefined
    // this implies that in creationMode `billingAddress` value is always `this.defaultAddress`
    const billingAddress = entry.debitorCreditorId === dcIdValue && hasValue(entry.billingAddress)
      ? entry.billingAddress
      : this.defaultAddress;

    if (value) {
      control.enable();
      control.setValue(billingAddress);
    } else {
      control.disable();
      control.setValue(this.defaultAddress);
    }
  }

  public loadDefaultAddress = (item: any = null) => {
    if (item) {
      this.form.get('debitorCreditorId').patchValue(item.id);
    }
    const debitorCreditorId = this.form.getRawValue().debitorCreditorId;
    // `charterer` field has `nzAllowClear` attr and `debitorCreditorId`'s value can be falsy
    if (!this.debitorsCreditors.length || !debitorCreditorId) { return; }
    const debitorCreditor = this.debitorsCreditors.find(d => d.id === debitorCreditorId);
    const queryParams: AddressParams = { companyId: debitorCreditor.companyId };

    this.addressMainService.fetchAll({ queryParams }).pipe(
      map(addresses => this.addressMainService.toRows(addresses))
    ).subscribe(addresses => {
      // Taking the 1. Address as an interim solution
      const addressText = this.addressMainService.rowAddressToText(addresses[0]);

      const { overrideAddress, billingAddress } = this.form.controls;
      this.defaultAddress = addressText;

      const hasOverrideAddress: boolean = hasValue(overrideAddress.value);

      if (!hasOverrideAddress) {
        this.form.patchValue({
          billingAddress: this.defaultAddress,
          overrideAddress: false,
        });
      }

      const entry = this.resource || {} as ITimeCharter;
      const overrideChecked = debitorCreditorId === entry.debitorCreditorId
        && hasValue(entry.billingAddress);
      overrideAddress.setValue(overrideChecked);

      if (!this.editMode || !overrideChecked) {
        billingAddress.disable();
      }
    });
  }

  public submit(): void {
    if (this.form.invalid) { return; }

    const freshResource = this.creationMode
      ? ({ ...this.form.value, voyageId: this.voyageId })
      : ({ ...this.resource, ...this.form.value, voyageId: this.voyageId });

    if (!this.form.value.overrideAddress) {
      freshResource.billingAddress = null;
    }

    delete freshResource.overrideAddress;

    this.creationMode
      ? this.createResource(freshResource).subscribe(createdResource => {
        this.timeChartersHelperService.generateTitle(this.voyage, createdResource);
      })
      : this.updateResource(freshResource).subscribe(updatedResource => {
        this.timeChartersHelperService.generateTitle(this.voyage, updatedResource);
      });
  }

  public handleSubmitError = (error: NVHttpErrorResponse): void => {
    ResourceDetailView.handleSubmitError(error, this.form, this.nzMessageService);
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
