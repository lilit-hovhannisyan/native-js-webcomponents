import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialCreateEditRoutingModule } from './material-create-edit-routing.module';
import { MaterialCreateEditComponent } from './material-create-edit.component';
import { MeAttributeModule } from '@shared/me-components/me-attribute/me-attribute.module';
import { MatStepperModule } from '@angular/material/stepper';
import { MeButtonModule } from '@shared/me-components/me-button/me-button.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MeUploadImageModule } from '@shared/me-components/me-upload-image/me-upload-image.module';
import { MeComponentLoadingModule } from '@shared/me-directives/me-component-loader/me-component-loader.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MeEmptyContentModule } from '@shared/me-components/me-empty-content/me-empty-content.module';
import { MeSearchBarModule } from '@shared/me-components/me-search-bar/me-search-bar.module';
import { MeDocumentCardModule } from '@shared/me-components/me-document-card/me-document-card.module';
import { MePopupSelectModule } from '@shared/me-components/me-popup-select/me-popup-select.module';
import { MeScrollToBottomModule } from '@shared/me-directives/me-scroll-to-bottom.module';

@NgModule({
  declarations: [MaterialCreateEditComponent],
  imports: [
    CommonModule,
    MaterialCreateEditRoutingModule,
    MeButtonModule,
    MeAttributeModule,
    MatStepperModule,
    IconsModule,
    MeUploadImageModule,
    MeComponentLoadingModule,
    ReactiveFormsModule,
    MeEmptyContentModule,
    MeSearchBarModule,
    MeDocumentCardModule,
    MePopupSelectModule,
    MeScrollToBottomModule
  ]
})
export class MaterialCreateEditModule {}
