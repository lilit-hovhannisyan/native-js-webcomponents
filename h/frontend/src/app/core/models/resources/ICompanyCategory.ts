import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';
import { CompanyKey } from './ICompany';
import { CategoryKey } from './ICategory';

export type CompanyCategoryKey = number;

interface ISharedProps {
  companyId: CompanyKey;
  categoryId: CategoryKey;
}

export interface ICompanyCategoryBase extends ISharedProps, IResource<number> { }

export interface ICompanyCategoryCreate extends ISharedProps { }

export interface ICompanyCategoryDTO extends ICompanyCategoryBase { }

export interface ICompanyCategory extends ICompanyCategoryDTO { }

export interface ICompanyCategoryFull extends ICompanyCategory, IWithLabel { }
