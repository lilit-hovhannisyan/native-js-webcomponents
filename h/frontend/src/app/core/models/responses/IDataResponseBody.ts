import { IResponseBody } from './IResponseBody';

export interface IDataResponseBody<T> extends IResponseBody {
  data: T;
}
