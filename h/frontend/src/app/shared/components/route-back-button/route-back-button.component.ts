import { Component } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';

/**
 * A standardly styled back button for route navigations.
 * Usually used within the paper view component.
 */
@Component({
  selector: 'nv-route-back-button',
  templateUrl: './route-back-button.component.html',
  styleUrls: ['./route-back-button.component.scss']
})
export class RouteBackButtonComponent {
  public wording = wording;
}
