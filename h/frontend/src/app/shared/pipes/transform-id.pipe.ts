import { Pipe, PipeTransform } from '@angular/core';
import { IResource } from 'src/app/core/models/resources/IResource';

@Pipe({
  name: 'transformId'
})
export class TransformIdPipe implements PipeTransform {
  public transform(entities: IResource<any>[], repositoryStream: IResource<any>[], relationKey: string): IResource<any>[] {
    return entities.map(entity => {
      const foundItem = repositoryStream.find(streamItem => streamItem.id === entity[relationKey]);
      return ({
        ...foundItem
      });
    });
  }
}

