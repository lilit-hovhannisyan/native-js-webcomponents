import { Component, OnInit } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { IArticleUnitCreate, IArticleUnit } from 'src/app/core/models/resources/IArticleUnit';
import { ArticleUnitsRepositoryService } from 'src/app/core/services/repositories/article-units-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { FormBuilder } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { ActivatedRoute } from '@angular/router';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';

@Component({
  selector: 'nv-article-units-detail-view',
  templateUrl: './article-units-detail-view.component.html',
  styleUrls: ['./article-units-detail-view.component.scss']
})
export class ArticleUnitsDetailViewComponent extends
  ResourceDetailView<IArticleUnit, IArticleUnitCreate> implements OnInit {
  protected repoParams: RepoParams<IArticleUnit> = {};
  protected resourceDefault = { isActive: true } as IArticleUnit;

  public routeBackUrl = url(this.sitemap.technicalManagement.children.main.children.articleUnits);
  constructor(
    articleUnitRepo: ArticleUnitsRepositoryService,
    bcs: BaseComponentService,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
  ) {
    super(articleUnitRepo, bcs);
  }

  public ngOnInit() {
    const id: string = this.activatedRoute.snapshot.params.id;
    this.init();

    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => `${this.resource.code} ${this.resource[this.labelKey]}`;


  public init(generalUnit: IArticleUnit = this.resourceDefault): void {
    this.initForm(this.fb.group({
      labelDe: [generalUnit.labelDe, [Validators.required, Validators.maxLength(50)]],
      labelEn: [generalUnit.labelEn, [Validators.required, Validators.maxLength(50)]],
      code: [generalUnit.code, [Validators.required, Validators.maxLength(3)]],
      decimalPlaces: [generalUnit.decimalPlaces, [Validators.required, Validators.wholeNumber]],
      isActive: [generalUnit.isActive],
      isSystemDefault: [generalUnit.isSystemDefault],
    }));
  }

  public submit() {
    if (!this.form.valid) { return; }
    const { value } = this.form;
    this.creationMode
      ? this.createResource(value)
      : this.updateResource({ ...this.resource, ...value });
  }

  public routeBack = () => this.router.navigate([this.routeBackUrl]);
}
