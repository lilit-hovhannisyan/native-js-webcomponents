import { IResource } from './IResource';
import { VesselKey } from './IVessel';
import { HttpResponse } from '@angular/common/http';
import { IDataResponseBody } from 'src/app/core/models/responses/IDataResponseBody';
import { IResourceCollection } from 'src/app/core/models/resources/IResourceCollection';
import { IWording } from 'src/app/core/models/resources/IWording';
import { VESSEL_SETTING_TYPE } from '../enums/vessel-setting-type';

export type VesselSettingKey = number;

interface ISharedProps {
  id: VesselSettingKey;
  isActive: boolean;
  serviceType: VESSEL_SETTING_TYPE;
  vesselId: VesselKey;
  from: string;
  until?: string;
}

export interface IVesselSettingBase extends ISharedProps, IResource<number> { }

export interface IVesselSettingCreate extends ISharedProps { }

export interface IVesselSettingDTO extends IVesselSettingBase { }

export interface IVesselSetting extends IVesselSettingDTO { }

export interface IVesselSettingRow extends IVesselSetting { }

export interface IVesselSettingWithLabel {
  name: IWording;
  value: VESSEL_SETTING_TYPE;
  _checked?: boolean;
}

export declare type IVesselSettingResource = HttpResponse<IDataResponseBody<IResourceCollection<IVesselSetting>>>;

