export interface Option {
    key: string;
    value: string;
    selected?: boolean;
    hover?: boolean;
}