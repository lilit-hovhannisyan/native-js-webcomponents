import { Wording } from 'src/app/core/classes/wording';
import { IWording } from 'src/app/core/models/resources/IWording';
import { system } from 'src/app/core/constants/wording/system';
import { COMPANY_LEGAL_FORM } from 'src/app/core/models/enums/company-legal-form';

export enum COMPANY_CONTRIBUTION_TYPES {
  TrustorContribution,
  SubParticipantContribution,
}

export type IMapLabels = Map<COMPANY_CONTRIBUTION_TYPES, IWording>;

export const sectionTitles: Map<COMPANY_CONTRIBUTION_TYPES, IWording> = new Map([
  [COMPANY_CONTRIBUTION_TYPES.TrustorContribution, system.trustors],
  [COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution, system.subParticipants],
]);

export const trusteePartnerLabels: Map<COMPANY_CONTRIBUTION_TYPES, IWording> = new Map([
  [COMPANY_CONTRIBUTION_TYPES.TrustorContribution, system.trustor],
  [COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution, system.subParticipant],
]);

export const createModalTitles: Map<COMPANY_CONTRIBUTION_TYPES, IWording> = new Map([
  [COMPANY_CONTRIBUTION_TYPES.TrustorContribution, system.manageParticipationOfTrustors],
  [COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution, system.manageParticipationOfSubParticipants],
]);

export const increasePartnerLabels: Map<COMPANY_CONTRIBUTION_TYPES, IWording> = new Map([
  [COMPANY_CONTRIBUTION_TYPES.TrustorContribution, Wording.concat(system.trustor, system.increase, ' ')],
  [COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution, Wording.concat(system.subParticipant, system.increase, ' ')],
]);

export const reducePartnerLabels: Map<COMPANY_CONTRIBUTION_TYPES, IWording> = new Map([
  [COMPANY_CONTRIBUTION_TYPES.TrustorContribution, Wording.concat(system.trustor, system.reduce, ' ')],
  [COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution, Wording.concat(system.subParticipant, system.reduce, ' ')],
]);

export const partnerTrusteeLabels: Map<COMPANY_CONTRIBUTION_TYPES, IWording> = new Map([
  [COMPANY_CONTRIBUTION_TYPES.TrustorContribution, system.partnerTrustee],
  [COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution, system.partner],
]);

export const shareholderTrusteeLabels: Map<COMPANY_CONTRIBUTION_TYPES, IWording> = new Map([
  [COMPANY_CONTRIBUTION_TYPES.TrustorContribution, system.shareholderTrustee],
  [COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution, system.shareholder],
]);

export const createPartnerTooltipTexts: Map<COMPANY_CONTRIBUTION_TYPES, IWording> = new Map([
  [COMPANY_CONTRIBUTION_TYPES.TrustorContribution, system.defineTrustorManually],
  [COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution, system.defineSubParticipantManually],
]);


export const partnerTrusteeLabelsByLegal: Map<COMPANY_LEGAL_FORM, IMapLabels> = new Map([
  // Shareholder
  [COMPANY_LEGAL_FORM.StockCorporation, shareholderTrusteeLabels], // AG
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, shareholderTrusteeLabels], // GmbH
  [COMPANY_LEGAL_FORM.ForeignCompany, shareholderTrusteeLabels], // ForeignCompany

  // Partner
  [COMPANY_LEGAL_FORM.LimitedPartnership, partnerTrusteeLabels], // KG
  [COMPANY_LEGAL_FORM.CivilLawPartnership, partnerTrusteeLabels], // GbR
]);
