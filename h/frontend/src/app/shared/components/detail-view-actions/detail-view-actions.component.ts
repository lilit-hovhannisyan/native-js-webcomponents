import { Component, Input, EventEmitter, Output, OnInit, OnDestroy, HostListener } from '@angular/core';
import { ACL_ACTIONS, AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { wording } from 'src/app/core/constants/wording/wording';
import { getParentScreenUrl } from 'src/app/core/helpers/makeUrl';
import { PaperViewStateService } from 'src/app/core/services/ui/paper-view-state.service';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, tap } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { scrollToFirstInvalid } from 'src/app/shared/helpers/scrollToFirstInvalid';
/**
 * This component creates action buttons, which can be used in edtiable detail
 * Detail components. The possible buttons are:
 * 1. EDIT
 * 2. SAVE
 * 3. CANCEL
 * Each button emits an event on click.
 */
@Component({
  selector: 'nv-detail-view-actions',
  templateUrl: './detail-view-actions.component.html',
  styleUrls: ['./detail-view-actions.component.scss']
})
export class DetailViewActionsComponent implements OnInit, OnDestroy {
  @Input() public inEditingMode = false;
  @Input() public largeButtons = false;
  @Input() public saveDisabled: boolean;
  @Input() public editDisabled: boolean;
  @Input() public refreshDisabled: boolean;
  @Input() public zone?: string;
  @Input() public styles: Object = {};
  @Input() public showSaveAndCancel: boolean;
  @Input() public showEditButton = true;
  @Input() public showRefresh = false;
  @Input() public form?: FormGroup;
  @Input() public formElement?: Element;
  /**
   * The emitted events can be handled by the parent component
   */
  @Output() public editClicked: EventEmitter<void> = new EventEmitter();
  @Output() public cancelClicked: EventEmitter<void> = new EventEmitter();
  @Output() public saveClicked: EventEmitter<void> = new EventEmitter();
  @Output() public refreshClicked: EventEmitter<void> = new EventEmitter();

  public loading = false;
  public ACL_ACTIONS = ACL_ACTIONS;
  public wording = wording;
  private canAddOrEdit: boolean;
  // animation makes e2e tests fail because of the delays
  public animated = !window.navigator.webdriver;
  private shortCutKeys: string[] = ['c', 's', 'e'];
  private onShortCutKey = new Subject<KeyboardEvent>();

  constructor(
    private router: Router,
    private paperViewStateService: PaperViewStateService,
    private authService: AuthenticationService,
  ) {
  }

  @HostListener('window:keydown', ['$event'])
  public keyEvent(e: KeyboardEvent): void {
    if (e.altKey) {
      e.preventDefault();
      this.onShortCutKey.next(e);
    }
  }

  private onShortcut(key: string): void {
    switch (key) {
      case 'c':
        if (this.inEditingMode) {
          this.cancelClicked.emit();
        }
        break;
      case 's':
        if (this.inEditingMode) {
          this.saveClicked.emit();
        }
        break;
      case 'e':
        if (!this.inEditingMode) {
          this.editClicked.emit();
        }
        break;
      default:
        break;
    }
  }

  public ngOnInit() {
    const zoneId = SitemapEntry.getByUrl(this.router.url).toZone();

    this.canAddOrEdit = this.authService.canUpdate(zoneId) || this.authService.canCreate(zoneId);

    if ((!this.largeButtons && this.canAddOrEdit) || this.showRefresh) {
      this.paperViewStateService.areDetailsButtonsVisibleSubject$.next(true);
    }

    this.onShortCutKey
      .pipe(
        distinctUntilChanged((e1, e2) => e1.key === e2.key),
        filter((e) => e.altKey && (this.shortCutKeys.includes(e.key))),
        debounceTime(500),
        tap((e: KeyboardEvent) => this.onShortcut(e.key))
      )
      .subscribe();
  }

  public ngOnDestroy() {
    if (!this.largeButtons && this.canAddOrEdit) {
      this.paperViewStateService.areDetailsButtonsVisibleSubject$.next(false);
    }
  }

  /**
   * This function emits a EDIT clicked event, which should either lead
   * to the editing mode or to a modal, which explains to the user,
   * that editing is currently not possible.
   */
  public edit(): void {
    this.editClicked.emit();
  }

  public refresh(): void {
    const refreshActiveBtn = document.activeElement as HTMLElement;
    refreshActiveBtn.blur();
    this.refreshClicked.emit();
  }

  public back(): void {
    const parentScreenUrl = getParentScreenUrl(this.router.url);

    this.router.navigate([parentScreenUrl]);
  }

  public onSaveclick() {
    this.form
    ? scrollToFirstInvalid(this.form, this.formElement).then(() => {
        this.saveClicked.emit();
      })
      : this.saveClicked.emit();
  }

}
