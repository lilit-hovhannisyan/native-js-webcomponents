import { IWithActive } from './IWithActive';
import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithSystemFlag } from './IWithSystemFlag';
import { IWithLabel } from './IWithLabel';

export type BookingCodeKey = number;

interface ISharedProps extends IWithLabels, IWithSystemFlag, IWithActive {
  no: number;
  isActive: boolean;
  remark: string;
  isPaymentsRelevance: boolean;
}

export interface IBookingCodeBase extends ISharedProps, IResource<BookingCodeKey> { }

export interface IBookingCodeCreate extends ISharedProps { }

export interface IBookingCodeDTO extends IBookingCodeBase { }

export interface IBookingCode extends IBookingCodeDTO, IWithLabel { }

export interface IBookingCodeRow extends IBookingCode {
}
