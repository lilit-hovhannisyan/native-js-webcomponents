export const getAccordionData = () => {
  const accordionData = [
    {
      title: 'Title1',
      body: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.`
    },
    {
      title: 'Title2',
      body: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
    Architecto necessitatibus repudiandae suscipit vitae. Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
    Cumque debitis deserunt placeat porro vitae? Animi et modi natus nisi unde.`
    },
    {
      title: 'Title3',
      body: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
    Architecto necessitatibus repudiandae suscipit vitae. Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
    Cumque debitis deserunt placeat porro vitae? Animi et modi natus nisi unde. 
    Hyyyyy
    Thord`
    }
  ];

  return accordionData;
}
