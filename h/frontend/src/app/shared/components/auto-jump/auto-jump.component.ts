import { Component, AfterViewInit, EventEmitter, Input, Output, ViewChild, ElementRef } from '@angular/core';
import { AbstractValueAccessor, makeProvider } from 'nv-grid';

@Component({
  selector: 'nv-auto-jump',
  templateUrl: './auto-jump.component.html',
  styleUrls: ['./auto-jump.component.scss'],
  providers: [makeProvider(AutoJumpComponent)]

})
export class AutoJumpComponent extends AbstractValueAccessor implements AfterViewInit {
  @Input() public model: any;
  @Input() public autoJumpValues: string[] = [];
  @Input() public autofocus = true;
  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();
  @Output() public next = new EventEmitter<boolean>();
  @ViewChild('input', { static: true }) private input: ElementRef<any>;


  constructor() {
    super();
  }

  public ngAfterViewInit() {
    if (this.autofocus) {
      setTimeout(() => {
        this.input.nativeElement.focus();
        this.input.nativeElement.select();
      }, 0);
    }
  }


  public handleTab(event: KeyboardEvent, value: any) {
    this.onChange(value);
    this.tab.emit(event);
  }

  public handleEnter(event: KeyboardEvent, value: any) {
    this.onChange(value);
    this.enter.emit(event);
  }

  public handleEscape(event: any) {
    this.escape.emit(event);
  }

  public handleBlur(event: any) {
    this.blur.emit(event);
  }

  public setDisabledState(isDisabled: boolean): void {
    this._disabled = isDisabled;
    this.input.nativeElement.disabled = isDisabled;
  }

  public onValueInput(value: string) {
    this.onChange(value);

    const acceptedInput = this.autoJumpValues.find(jumpValue => jumpValue === value);
    if (acceptedInput) {
      this.next.emit(true);
    }
  }
}
