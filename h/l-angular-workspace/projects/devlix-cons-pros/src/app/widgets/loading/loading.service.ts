import {Injectable, EventEmitter} from '@angular/core';
import {ErrorHandler} from '../../dal/error-handler';


@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  loaderState = new EventEmitter();

  constructor(private errorHandler: ErrorHandler) {
    this.errorHandler.errorData.subscribe(data => {
      if (data) {
        this.showLoader(false);
      }
    });
  }

  showLoader(state) {
    this.loaderState.emit(state);
  }

  getLoaderState() {
    return this.loaderState;
  }
}
