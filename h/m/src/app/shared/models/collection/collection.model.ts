import { Type } from 'class-transformer';

import { BaseModel } from '../base-model/base-model';
import { CollectionAttributesExtendedModel } from './collection-attributes-extended.model';

import { CollectionAttributesModel } from './collection-attributes.model';

export class CollectionModel extends BaseModel {
  @Type(() => CollectionAttributesModel)
  attributes: CollectionAttributesModel = new CollectionAttributesModel();
  @Type(() => CollectionAttributesExtendedModel)
  attributesExtended: CollectionAttributesExtendedModel = new CollectionAttributesExtendedModel();
  numberOfCustomers: number = 0;
  numberOfMaterials: number = 0;
  numberOfTeams: number = 0;
}
