import { Component, OnChanges, OnDestroy, Input, Output, EventEmitter, SimpleChange, ViewChild, ElementRef } from '@angular/core';
import { Subject } from 'rxjs';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SelectionModel } from '@angular/cdk/collections';
import { PageEvent } from '@angular/material/paginator';

import { KeyValueI } from '@core-interfaces/key-value.interface';
import SortableColumnI from './interfaces/sortable-column.interface';
import ColumnI from './interfaces/column.interface';
import DataSourceI from './interfaces/data-source.interface';
import PaginationI from './interfaces/pagination.interface';
import ColumnOptionsI from './interfaces/column-options.interface';
import RenderCellI from './interfaces/render-cell.interface';
import CellClickEventI from './interfaces/cell-click-event.interface';
import { TypeSort } from './types/sort.type';
import { PAGE_INDEX_DEFAULT_VALUE, PAGE_SIZE_DEFAULT_VALUE, PAGE_SIZE_OPTIONS_DEFAULT_VALUE, PAGINATION_TOTAL_COUNT } from './constants';

@Component({
  selector: 'me-table',
  templateUrl: './me-table.component.html',
  styleUrls: ['./me-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class MeTableComponent implements OnChanges, OnDestroy {
  @Input() dataSource!: DataSourceI[];
  @Input() columns!: ColumnI[];
  @Input() columnOptions!: ColumnOptionsI[];
  @Input() checkboxSelection = false;
  @Input() isLoadingResults = false;
  @Input() hideFooterPagination = false;
  @Input() pagination: PaginationI = {
    totalCount: PAGINATION_TOTAL_COUNT,
    pageSize: PAGE_SIZE_DEFAULT_VALUE,
    pageIndex: PAGE_INDEX_DEFAULT_VALUE,
    pageSizeOptions: PAGE_SIZE_OPTIONS_DEFAULT_VALUE
  };
  @Input() cellClickEvent!: Subject<CellClickEventI>;

  @Output() sortBy = new EventEmitter<SortableColumnI>();
  @Output() changeSelection = new EventEmitter<KeyValueI<any>>();
  @Output() changeAllSelection = new EventEmitter<KeyValueI<any>[]>();
  @Output() changePage = new EventEmitter<PageEvent>();
  @Output() openExpandedRow = new EventEmitter<string>();

  @ViewChild('tableContainer') divElementRef!: ElementRef;
  @ViewChild('table') tableRef!: ElementRef;

  selection = new SelectionModel<any>(true, []);
  sortableColumns: SortableColumnI[] = [];
  displayedColumnsKeys!: string[];
  expandedRow!: string | null;

  constructor() {}

  ngAfterViewInit() {
    // const tableContainerWidth = this.divElementRef.nativeElement.offsetWidth;
    // console.log(`tableContainerWidth`, tableContainerWidth);
    // this.divElementRef.nativeElement.style.width = `${tableContainerWidth - 20}px`;
    // this.tableRef.nativeElement.style.minWidth = '1560px';
  }

  ngOnChanges(changes: { columns: SimpleChange; columnOptions: SimpleChange }) {
    const { columns, columnOptions } = changes;

    if (columns && columns.currentValue) {
      const tableColumns = columns.currentValue as ColumnI[];

      this.displayedColumnsKeys = tableColumns.map(({ field }) => field);
    }

    if (columnOptions && columnOptions.currentValue) {
      const tableColumnOptions = columnOptions.currentValue as ColumnOptionsI[];

      tableColumnOptions.forEach((columnOption) => {
        if (columnOption.sortable) {
          this.sortableColumns.push({
            field: columnOption.forField,
            sortBy: 'asc'
          });
        }
      });
    }
  }

  sortData(column: ColumnI): void {
    const { field } = column;
    const colOptions = this.columnOptions.find(({ forField }) => forField === column.field);

    if (!colOptions) {
      return;
    }

    if (!colOptions.sortable) {
      return;
    }

    const index = this.sortableColumns.findIndex((column) => column.field === field);
    if (index > -1) {
      let newSortByVal: TypeSort = 'asc';
      const { sortBy } = this.sortableColumns[index];

      if (sortBy === 'asc') {
        newSortByVal = 'desc';
      }

      this.sortableColumns[index].sortBy = newSortByVal;
      this.sortBy.emit(this.sortableColumns[index]);
    }
  }

  getSortableArrowNameByColumnFieldName(colFieldName: string): string {
    let iconName = 'north';
    const index = this.sortableColumns.findIndex((sc) => sc.field === colFieldName);

    if (index > -1) {
      const { sortBy } = this.sortableColumns[index];

      if (sortBy === 'desc') {
        iconName = 'south';
      }
    }

    return iconName;
  }

  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.length;

    return numSelected === numRows;
  }

  isColSortable(fieldName: string): boolean {
    if (!this.columnOptions) {
      return false;
    }

    const colOption = this.getColumnOption(fieldName);
    if (colOption && colOption.sortable !== undefined) {
      return colOption.sortable;
    }

    return false;
  }

  isColHidden(fieldName: string): boolean {
    if (!this.columnOptions) {
      return false;
    }

    const colOption = this.getColumnOption(fieldName);
    if (colOption && colOption.hidden !== undefined) {
      return colOption.hidden;
    }

    return false;
  }

  getColHeaderName(fieldName: string, colIndex: number): string {
    const colOption = this.getColumnOption(fieldName);

    if (colOption && colOption.headerName !== undefined) {
      return colOption.headerName;
    }

    return this.columns[colIndex].headerName;
  }

  toggleAllSelection(): void {
    if (this.isAllSelected()) {
      this.selection.clear();
    } else {
      this.dataSource.forEach((row) => this.selection.select(row));
    }

    this.changeAllSelection.emit(this.selection.selected);
  }

  toggleRowSelection(row: any): void {
    this.selection.toggle(row);

    const isSelected = this.selection.isSelected(row);
    this.changeSelection.emit({
      isSelected,
      row
    });
  }

  getPageSizeOptionsValue(): number[] {
    const { pageSizeOptions } = this.pagination;

    if (!pageSizeOptions) {
      return PAGE_SIZE_OPTIONS_DEFAULT_VALUE;
    }

    return pageSizeOptions;
  }

  countOfEmptyCell(renderedCells: any[]) {
    const columnsCount = this.columns.length;
    const renderedCellsCount = renderedCells.length;
    const count = columnsCount - renderedCellsCount;

    if (count > 0) {
      return Array(count);
    }

    return Array(0);
  }

  onChangePage(event: PageEvent): void {
    this.changePage.emit(event);
  }

  onClickExpandedRow({ rowOID }: DataSourceI): void {
    if (this.expandedRow === rowOID) {
      this.expandedRow = null;
    } else {
      this.expandedRow = rowOID;
      this.openExpandedRow.emit(rowOID);
    }
  }

  trackByRowOid(_: number, row: DataSourceI): string {
    return row.rowOID;
  }

  trackByComponentData(_: number, row: RenderCellI): any {
    return row.componentData;
  }

  trackByField(_: number, col: ColumnI): string {
    return col.field;
  }

  private getColumnOption(fieldName: string): ColumnOptionsI | undefined {
    return this.columnOptions.find(({ forField }) => forField === fieldName);
  }

  ngOnDestroy() {
    // const tableContainerWidth = this.divElementRef.nativeElement.offsetWidth;
    // this.divElementRef.nativeElement.style.width = `${tableContainerWidth + 20}px`;
    // this.tableRef.nativeElement.style.minWidth = null;
  }
}
