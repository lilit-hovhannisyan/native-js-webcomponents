import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DigitalProfileViewRoutingModule } from './digital-profile-view-routing.module';
import { DigitalProfileViewComponent } from './digital-profile-view.component';
import { MeButtonModule } from '@shared/me-components/me-button/me-button.module';
import { MeAttributeModule } from '@shared/me-components/me-attribute/me-attribute.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';
import { MeSearchBarModule } from '@shared/me-components/me-search-bar/me-search-bar.module';
import { MeCertificateModule } from '@shared/me-components/me-certificate/me-certificate.module';
import { MeLabelModule } from '@shared/me-components/me-label/me-label.module';
import { MeNewCollectionCardModule } from '@shared/me-components/me-new-collection-card/me-new-collection-card.module';
import { MeScrollToBottomModule } from '@shared/me-directives/me-scroll-to-bottom.module';
import { MatMenuModule } from '@angular/material/menu';
import { MeEmptyContentModule } from '@shared/me-components/me-empty-content/me-empty-content.module';
import { MeLogoUploadModule } from '@shared/me-components/me-logo-upload/me-logo-upload.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MeCoverImageUploadModule } from '@shared/me-components/me-cover-image-upload/me-cover-image-upload.module';
import { MeIntroductionMessageModule } from '@shared/me-components/me-introduction-message/me-introduction-message.module';
import { CertificateCreateEditDialogModule } from '@features/collection-create-edit/certificate-create-edit-dialog/certificate-create-edit-dialog.module';
import { CertificateCreateEditDialogComponent } from '@features/collection-create-edit/certificate-create-edit-dialog/certificate-create-edit-dialog.component';

@NgModule({
  declarations: [DigitalProfileViewComponent],
  imports: [
    CommonModule,
    DigitalProfileViewRoutingModule,
    MeButtonModule,
    MeAttributeModule,
    IconsModule,
    MatButtonModule,
    MatTabsModule,
    MeIconsProviderModule,
    MeSearchBarModule,
    MeCertificateModule,
    MeLabelModule,
    MeNewCollectionCardModule,
    MeScrollToBottomModule,
    MatMenuModule,
    MeEmptyContentModule,
    MeLogoUploadModule,
    ReactiveFormsModule,
    MeCoverImageUploadModule,
    MeIntroductionMessageModule,
    CertificateCreateEditDialogModule
  ],
  entryComponents: [CertificateCreateEditDialogComponent]
})
export class DigitalProfileViewModule {}
