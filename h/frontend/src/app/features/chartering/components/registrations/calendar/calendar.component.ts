import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { sitemap } from '../../../../../core/constants/sitemap/sitemap';
import { url } from '../../../../../core/constants/sitemap/sitemap-entry';
import { wording } from '../../../../../core/constants/wording/wording';
import { Router } from '@angular/router';

@Component({
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegistartionCalendarComponent implements OnInit {
  public wording = wording;

  constructor(
    private router: Router,
  ) { }

  public ngOnInit(): void {
  }

  public routeBackUrl = () => {
    this.router.navigate([
      url(sitemap.chartering.children.registrations.children.dashboard)
    ]);
  }

}
