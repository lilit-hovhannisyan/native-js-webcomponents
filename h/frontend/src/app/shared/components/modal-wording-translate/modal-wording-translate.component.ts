import { Component, Input } from '@angular/core';
import { IWording } from 'src/app/core/models/resources/IWording';

@Component({
  templateUrl: './modal-wording-translate.component.html',
  styleUrls: ['./modal-wording-translate.component.scss']
})
export class ModalWordingTranslateComponent {
  @Input() public wording: IWording;
  @Input() public item?: IWording;
  @Input() public additional?: string;
}
