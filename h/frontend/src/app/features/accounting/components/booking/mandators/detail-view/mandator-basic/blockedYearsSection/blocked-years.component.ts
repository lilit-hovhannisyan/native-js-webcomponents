import { UserKey } from './../../../../../../../../core/models/resources/IUser';
import { IBlockedYearEntry } from './../../../../../../../../core/models/resources/IBlockedYearEntry';
import { IClosedYearEntry } from './../../../../../../../../core/models/resources/IClosedYearEntry';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { sortBy } from 'lodash';

export type MergedYearEntry = {
  year: number,
  closedPeriod?: number,
  blockedPeriod?: number,
  blockedAt?: string,
  blockedBy?: UserKey,
  closedAt?: string,
  closedBy?: UserKey,
  blockedYearId?: number,
  closedYearId?: number,
};

@Component({
  selector: 'nv-blocked-years-section',
  templateUrl: './blocked-years.component.html',
  styleUrls: ['./blocked-years.component.scss']
})
export class BlockedYearsSectionComponent implements OnInit, OnChanges {

  @Input() public closedYears: IClosedYearEntry[] = [];
  @Input() public blockedYears: IBlockedYearEntry[] = [];
  @Input() public firstBookingYear: number;
  @Input() public showClosedBookingYears: boolean;
  @Input() public editMode: boolean;
  @Input() public canUpdate: boolean;
  @Output() public blockedYearsChanged = new EventEmitter<{}[]>();
  @Output() public closedYearsChanged = new EventEmitter<{}[]>();

  public periods = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
  public mergedYears: MergedYearEntry[] = [];

  private userId: UserKey;
  public wording = wording;
  constructor(private authService: AuthenticationService) { }

  public ngOnInit() {
    this.updateMergedList();
    this.userId = this.authService.getSessionInfo().user.id;
  }

  public ngOnChanges(changes) {
    if (changes.blockedYears || changes.closedYears || changes.showClosedBookingYears) {
      this.updateMergedList();
    }
  }

  public blockedPeriodChanged(id: number, year: number, blockedPeriod: number | undefined) {
    let newBlockedYears = this.blockedYears.map(blockedYear => {
      if (blockedYear.year !== year) { return blockedYear; }
      if (!blockedPeriod) { return undefined; } // gets filtered out in next step
      return this.getBlockedEntry(id, year, blockedPeriod);
    });
    if (!this.blockedYears.find(blockedYear => blockedYear.year === year)) {
      newBlockedYears.push(this.getBlockedEntry(id, year, blockedPeriod));
    }
    newBlockedYears = newBlockedYears.filter(blockedYear => !!blockedYear); // filtering out the undefine dones
    this.blockedYearsChanged.emit(newBlockedYears);
  }

  public closedPeriodChanged(id: number, year: number, closedPeriod: number | undefined) {
    let newClosedYears = this.closedYears.map(closedYear => {
      if (closedYear.year !== year) { return closedYear; }
      if (!closedPeriod) { return undefined; } // gets filtered out in next step
      return this.getClosedEntry(id, year, closedPeriod);
    });
    if (!this.closedYears.find(closedYear => closedYear.year === year)) {
      newClosedYears.push(this.getClosedEntry(id, year, closedPeriod));
    }
    newClosedYears = newClosedYears.filter(closedYear => !!closedYear); // filtering out the undefine dones
    this.closedYearsChanged.emit(newClosedYears);
  }
  private getBlockedEntry = (id: number, year: number, period: number): IBlockedYearEntry => {
    return {
      year,
      period,
      blockedBy: this.userId,
      blockedAt: new Date().toISOString(),
      id: id,
    };
  }
  private getClosedEntry = (id: number, year: number, period: number): IClosedYearEntry => {
    return {
      year,
      period,
      closedBy: this.userId,
      closedAt: new Date().toISOString(),
      id: id,
    };
  }
  private updateMergedList = () => {
    const years = [];
    const currentYear = new Date().getFullYear();
    for (let year = this.firstBookingYear; year <= currentYear; year++) {
      years.push(year);
    }

    this.mergedYears = sortBy(years.map(year => {
      let mergedYear: any = { year };
      const foundClosedYear = this.closedYears.find(closedYear => closedYear.year === year);
      if (foundClosedYear) {
        mergedYear = {
          ...mergedYear,
          ...foundClosedYear,
          closedPeriod: foundClosedYear.period,
          closedYearId: foundClosedYear.id
        };
      }
      const foundBlockedYear = this.blockedYears.find(blockedYear => blockedYear.year === year);
      if (foundBlockedYear) {
        mergedYear = {
          ...mergedYear,
          ...foundBlockedYear,
          blockedPeriod: foundBlockedYear.period,
          blockedYearId: foundBlockedYear.id
        };
      }
      return mergedYear;
    }), ['year']).reverse();

    if (!this.showClosedBookingYears) {
      this.mergedYears = this.mergedYears.filter(mergedYear => mergedYear.closedPeriod !== 13);
    }
  }

  public isCurrentUser = (userId: UserKey) => {
    return this.authService.getSessionInfo().user.id === userId;
  }

  public hasAdminRights = () => {
    return (
      this.authService.getUser().isSuperAdmin ||
      this.authService.getUser().isAdmin
    );
  }

  public getBlockablePeriods = (closedPeriod?: number): number[] => {
    if (!closedPeriod) { return this.periods; }
    return this.periods.filter(period => period > closedPeriod);
  }

  public isBlockedPeriodEditable = (entry: MergedYearEntry) => (
    this.editMode &&
    (
      this.authService.getUser().isSuperAdmin ||
      (this.authService.canUpdate('accounting.booking.mandators.blocking') && !entry.blockedPeriod) ||
      (this.authService.canUpdate('accounting.booking.mandators.blocking') && entry.blockedBy === this.authService.getUser().id)
    )
  )

  public isClosedPeriodEditable = (entry: MergedYearEntry) => {
    return this.editMode && this.authService.getUser().isSuperAdmin;
  }
}
