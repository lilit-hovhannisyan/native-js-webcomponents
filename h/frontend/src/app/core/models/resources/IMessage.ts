import { IResource } from './IResource';
import { UserKey } from './IUser';

export type MessageKey = number;

interface ISharedProps {
  date: string;
  text: string;
  userId: UserKey;
  documentGuid: string;
}

export interface IMessageCreate extends ISharedProps { }

export interface IMessageBase extends ISharedProps, IResource<MessageKey> { }

export interface IMessageDTO extends IMessageBase { }

export interface IMessage extends IMessageDTO { }

export interface IMessageFull extends IMessage { }
