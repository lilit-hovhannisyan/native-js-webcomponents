import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'modify'
})
export class ModifyPipe implements PipeTransform {
  public transform(value: any, getLabel: (value: any) => any): any[] {
    return getLabel
      ? getLabel(value)
      : value;
  }
}
