import { Type } from 'class-transformer';

export enum RequiredConstraintState {
  draft = 'draft',
  exclusive = 'exclusive',
  public = 'public',
  internal = 'internal'
}

export class ConstraintModel {
  ruleClass: string = '';
  ruleName: string = '';
}

export class DateTimeRangeConstraintModel extends ConstraintModel {
  @Type(() => DateTimeRangeDataModel)
  data: DateTimeRangeDataModel = new DateTimeRangeDataModel();
}

export class DateTimeRangeDataModel {
  minValue: string = '';
  maxValue: string = '';
}

export class DecimalRangeConstraintModel extends ConstraintModel {
  @Type(() => DecimalRangeDataModel)
  data: DecimalRangeDataModel = new DecimalRangeDataModel();
}

export class DecimalRangeDataModel {
  min!: number;
  max!: number;
}

export class FloatRangeConstraintModel extends ConstraintModel {
  @Type(() => FloatRangeDataModel)
  data: FloatRangeDataModel = new FloatRangeDataModel();
}

export class FloatRangeDataModel {
  min!: number;
  max!: number;
}

export class ImmutableConstraintModel extends ConstraintModel {}

export class IntegerRangeConstraintModel extends ConstraintModel {
  @Type(() => IntegerRangeDataModel)
  data: IntegerRangeDataModel = new IntegerRangeDataModel();
}

export class IntegerRangeDataModel {
  min!: number;
  max!: number;
}

export class MultiValuedConstraintModel extends ConstraintModel {
  @Type(() => MultiValuedDataModel)
  data: MultiValuedDataModel = new MultiValuedDataModel();
}

export class MultiValuedDataModel {
  enum: string = '';
}

export class ObjectRefConstraintModel extends ConstraintModel {
  @Type(() => ObjectRefDataModel)
  data: ObjectRefDataModel = new ObjectRefDataModel();
}

export class ObjectRefDataModel {
  list: string[] = [];
}

export class RequiredConstraintModel extends ConstraintModel {
  data: { states: RequiredConstraintState[] } = { states: [] };
}

export class SingleValuedConstraintModel extends ConstraintModel {
  @Type(() => SingleValuedDataModel)
  data: SingleValuedDataModel = new SingleValuedDataModel();
}

export class SingleValuedDataModel {
  enum: string = '';
}

export class StringLengthConstraintModel extends ConstraintModel {
  @Type(() => StringLengthDataModel)
  data: StringLengthDataModel = new StringLengthDataModel();
}

export class StringLengthDataModel {
  from: number = 0;
  to: number = 255;
}
