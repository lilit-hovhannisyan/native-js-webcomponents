import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppDataService} from '../../dal/app-data.service';
import {Subscription} from 'rxjs';
import {Field, RouteManagementService} from 'l-builder';
import {LoadingService} from '../../widgets/loading/loading.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less']
})
export class ListComponent implements OnInit, OnDestroy {
  listData = [];
  groupId;
  userId;

  listDataSubscription = Subscription.EMPTY;

  objectKeys = Object.keys;

  fieldsConfig = {};

  contenteditable = {};
  _updateEl = {};

  constructor(private appDataService: AppDataService,
              private loadingService: LoadingService) { }

  ngOnInit() {
      this.loadingService.showLoader(true);
      this.appDataService.appData.subscribe( (res: any) => {
        this.groupId = res.groupId;
        this.userId = res.userId;

        this.listDataSubscription = this.appDataService.getList(this.groupId, this.userId)
            .subscribe( (listItems: any) => {
            this.listData = listItems;

            this.loadingService.showLoader(false);

            for (const type of Object.keys(listItems)) {

                this.fieldsConfig[type] = [
                    {
                        component: 'input',
                        name: 'listItem',
                        descriptors: {
                            placeholder: 'New ' + type.replace('s', '')
                        },
                        validationRules: {
                            required: {
                                value: true,
                                customErrMsg: 'Field is required'
                            }
                        }
                    }
                ];
            }
        });
      });
  }

  ngOnDestroy() {
      this.listDataSubscription.unsubscribe();
  }

  onIncrementList(event, key) {
     this.listData[key].push(event.listItem);

     this.appDataService.updateList(this.groupId, this.userId, this.listData).subscribe( res => {
         this.loadingService.showLoader(false);
     });
  }

  removeEl(item, key) {
      const arr = this.listData[key];
      const elIndex = arr.findIndex( i => i === item);

      arr.splice(elIndex, 1);

      this.appDataService.updateList(this.groupId, this.userId, this.listData).subscribe( res => {
          this.loadingService.showLoader(false);
      });
  }

  editEl(item, key) {
      this.contenteditable = {};

      const arr = this.listData[key];

      this.contenteditable[key] = {};
      this.contenteditable[key][item] = true;
  }

  onInput(event, item, key) {
      const arr = this.listData[key];
      const elIndex = arr.findIndex( i => i === item);

      this._updateEl = {
          key: key,
          index: elIndex,
          value: event.target.value
      };
  }

  updateEl(item, key) {
      this.listData[ this._updateEl['key']][this._updateEl['index']] = this._updateEl['value'];

      this.appDataService.updateList(this.groupId, this.userId, this.listData).subscribe( res => {
          this.loadingService.showLoader(false);
          this.contenteditable = {};
          this._updateEl = {};
      });
  }
}
