import { SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { Operations } from 'src/app/core/models/Operations';
import { wording } from '../../../core/constants/wording/wording';

const ALL = Operations.CREATE | Operations.READ | Operations.UPDATE | Operations.DELETE;

export const specialZones: SitemapNode[] = [
  {
    parentFullZone: 'technicalManagement.misc.stevedoreDamages',
    fullZone: 'technicalManagement.misc.stevedoreDamages.releaseInvoice',
    fullTitle: {
      en: 'Technical Management/Misc/Crewwork & Stevedore Damages/Release Invoice',
      de: 'Technical Management/Sonstiges/Crewwork & Stevedore Damages/Rechnung Freigeben'
    },
    title: { en: 'Release Invoice', de: 'Rechnung Freigeben' },
    path: '',
    zone: '',
    maxOperations: Operations.CREATE,
  },
  {
    parentFullZone: 'accounting.booking',
    fullZone: 'accounting.booking.balances',
    fullTitle: {
      en: 'Accounting/Booking/Balances',
      de: 'Buchhaltung/Booking/Salden',
    },
    title: { en: 'Balances', de: 'Salden' },
    path: '',
    zone: '',
    maxOperations: ALL,
  },
  {
    parentFullZone: 'accounting.booking',
    fullZone: 'accounting.booking.accountstatements',
    fullTitle: {
      en: 'Accounting/Booking/Account Statements',
      de: 'Buchhaltung/Booking/Kontoauszüge',
    },
    title: { en: 'Account Statements', de: 'Kontoauszüge' },
    path: '',
    zone: '',
    maxOperations: ALL,
  },
  {
    parentFullZone: 'administration',
    fullZone: 'administration.salutations',
    fullTitle: {
      en: 'Administration/Salutations',
      de: 'Administration/Salutations',
    },
    title: { en: 'Salutations', de: 'Salutations' },
    path: '',
    zone: '',
    maxOperations: ALL,
  },
  {
    parentFullZone: 'administration',
    fullZone: 'administration.contactsTypes',
    fullTitle: {
      en: 'Administration/Contacts Types',
      de: 'Administration/Contacts Types',
    },
    title: { en: 'Contacts Types', de: 'Contacts Types' },
    path: '',
    zone: '',
    maxOperations: ALL,
  },
  {
    parentFullZone: 'administration',
    fullZone: 'administration.contactsRoles',
    fullTitle: {
      en: 'Administration/Contacts Roles',
      de: 'Administration/Contacts Roles',
    },
    title: { en: 'Contacts Roles', de: 'Contacts Roles' },
    path: '',
    zone: '',
    maxOperations: ALL,
  },
  {
    parentFullZone: 'administration',
    fullZone: 'administration.categories',
    fullTitle: {
      en: 'Administration/Categories',
      de: 'Administration/Categories',
    },
    title: { en: 'Categories', de: 'Categories' },
    path: '',
    zone: '',
    maxOperations: ALL,
  },
  {
    parentFullZone: 'administration',
    fullZone: 'administration.addressTypes',
    fullTitle: {
      en: 'Administration/Address Types',
      de: 'Administration/Address Types',
    },
    title: { en: 'Address Types', de: 'Address Types' },
    path: '',
    zone: '',
    maxOperations: ALL,
  },
  {
    parentFullZone: 'accounting.autobank.rules',
    fullZone: 'accounting.autobank.rules.fieldTypes',
    fullTitle: {
      en: 'Accounting/Autobank/Rules/Field Types',
      de: 'Accounting/Autobank/Rules/Field Types',
    },
    title: { en: 'Field Types', de: 'Field Types' },
    path: '',
    zone: '',
    maxOperations: ALL,
  },
  {
    parentFullZone: 'accounting.autobank.rules.field',
    fullZone: 'accounting.autobank.rules.field.operationTypes',
    fullTitle: {
      en: 'Accounting/Autobank/Rules/Field/Operation Types',
      de: 'Accounting/Autobank/Rules/Field/Operation Types',
    },
    title: { en: 'Operation Types', de: 'Operation Types' },
    path: '',
    zone: '',
    maxOperations: ALL,
  },
  {
    parentFullZone: 'chartering.misc.voyages',
    fullZone: 'chartering.misc.voyages.charternames',
    fullTitle: {
      en: 'Chartering and Operating/Misc/Voyages/Charternames',
      de: 'Chartering and Operating/Misc/Voyages/Charternames',
    },
    title: { en: 'Charternames', de: 'Charternames' },
    path: '',
    zone: '',
    maxOperations: ALL,
  },
  {
    parentFullZone: 'chartering.misc.vessel',
    fullZone: 'chartering.misc.vessel.preDelayCharterNames',
    fullTitle: {
      en: 'Chartering and Operating/Misc/Vessel/Pre-Delivery & Chartername',
      de: 'Chartering and Operating/Misc/Voyages/Pre-Delivery & Chartername',
    },
    title: { en: 'Pre-Delivery & Chartername', de: 'Pre-Delivery & Chartername' },
    path: '',
    zone: '',
    maxOperations: Operations.UPDATE | Operations.READ,
  },
  {
    parentFullZone: 'accounting.booking.mandators.accounts',
    fullZone: 'accounting.booking.mandators.accounts.roles',
    fullTitle: {
      en: 'Accounting/Booking/Mandators/Accounts/Roles',
      de: 'Accounting/Booking/Mandators/Accounts/Roles',
    },
    title: { en: 'Mandator Account Roles', de: 'Mandator Account Roles' },
    path: '',
    zone: 'roles',
    maxOperations: ALL,
  },
  {
    parentFullZone: 'chartering.registrations',
    fullZone: 'system.misc.calendars',
    fullTitle: {
      en: 'Chartering and Operating/Registrations/Calendar',
      de: 'Befrachtung und Operating/Registrierungen/Kalender',
    },
    title: wording.chartering.calendar,
    path: 'calendar',
    zone: '',
    maxOperations: ALL,
  },
  {
    parentFullZone: 'chartering.registrations',
    fullZone: 'system.misc.calendarEvents',
    fullTitle: {
      en: 'Chartering and Operating/Registrations/Calendar Events',
      de: 'Befrachtung und Operating/Registrierungen/Kalender Termin',
    },
    title: { en: '', de: '' },
    path: '',
    zone: '',
    maxOperations: ALL,
  },
];

export const shareholdingsLegalEntityZone = 'system.misc.shareholdings.legalEntity';
