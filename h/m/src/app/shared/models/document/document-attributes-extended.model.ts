export class DocumentAttributesExtendedModel {
  owner: string = '';
  creator: string = '';
  thumbnail: string = '';
  primaryContent: string = '';
  extension: string = '';
}
