import { ICompany, CompanyKey } from './ICompany';
import { ICurrency, CurrencyKey } from './ICurrency';
import { IMandatorBlockedBookingYear } from './IMandatorBlockedBookingYear';
import { IMandatorClosedBookingYear } from './IMandatorClosedBookingYear';
import { IMeta } from './IMeta';
import { IResource } from './IResource';
import { IWording } from './IWording';
import { MandatorTypes } from './MandatorTypes';
import { COMPANY_LEGAL_FORM } from '../enums/company-legal-form';
import { ACCOUNTING_TYPES } from '../enums/accounting-types';
export type MandatorKey = number;

export interface IMandatorRawProps {
  no: number;
  name?: string;
  start: Date;
  isCostAccounting: boolean;
  isInvoiceWorkflow: boolean;
  accountingType: ACCOUNTING_TYPES;
  startFiscalYear: number;
  firstBookingYear: number;
  mandatorType: MandatorTypes;
  currencyId: CurrencyKey;
  companyId: CompanyKey;
  isActive?: boolean;
  legalForm: COMPANY_LEGAL_FORM;
  isAutoBankWorkflow: boolean;
  isPaymentTransfer: boolean;
}

interface ISharedProps extends IMandatorRawProps, IMeta {
  blockedUntil?: string;
  blockedAt?: string;
  blockedBy?: number;
  blockedRemark?: string;
  hasCorporateRelevance?: boolean;
  corporateAccountingReference: string;
}

export interface IMandatorLookup extends IMandatorRawProps, IResource<MandatorKey> {}

export interface IMandatorBase extends ISharedProps, IResource<MandatorKey> {}

export interface IMandatorCreate extends ISharedProps { }

export interface IMandatorDTO extends IMandatorBase { }

export interface IMandator extends IMandatorDTO { }

export interface IMandatorFullRaw {
  company: ICompany;
  currency: ICurrency;
  lastClosedYear?: IMandatorClosedBookingYear;
  lastClosedYearView: number;
  legalFormWording?: IWording;
}

export interface IMandatorFull extends IMandatorFullRaw, IMandator {
  blockedYears?: IMandatorBlockedBookingYear[];
  closedYears?: IMandatorClosedBookingYear[];
}
export interface IMandatorLookupFull extends IMandatorFullRaw, IMandatorLookup { }

export interface IMandatorRowRaw {
  companyName: string;
  currencyName: IWording;
  hgb: boolean;
  ifrs: boolean;
  usgaap: boolean;
  eBalance: boolean;
  isoCode: string;
  mandatorTypeName: IWording;
  lastClosedYearView: number;
  lastClosedPeriod?: number;
}

export interface IMandatorRow extends IMandatorRowRaw, IMandatorFull { }
export interface IMandatorLookupRow extends IMandatorRowRaw, IMandatorLookupFull { }

/**
 * Converts the date passed as argument into a fiscal calendar date.
 * The month of the fiscal calendar date is called the period
 * returns a new Date that is aligned with the fiscal year
 * @param {Date} date a (non-fiscal) calendar date
 * @param {number} startFiscalYear mandator's start fiscal year
 * @returns {Date} the fiscal year and period in form of a Date object
 *     **IMPORTANT:**
 *     The day of the date is always set to 01, since the fiscal year day
 *     is never used in comparisons (only year and period are used) and
 *     keeping the day would result in problems arising due to not
 *     all months having the same number of days
 */
export const getFiscalCalendarDate = (date: Date, startFiscalYear: number): Date => {
  if (startFiscalYear === 1) {
    // fiscal year is same as calendar year
    return new Date(date.getFullYear(), date.getMonth(), 1);
  }

  if (date.getMonth() < startFiscalYear) {
    // remaining months until new fiscal year begins
    const remainingMonths = (startFiscalYear - date.getMonth());
    return new Date(
      date.getFullYear(),
      (12 - remainingMonths + 1), // +1 no move to next month
      1);
  }

  return new Date(
    date.getFullYear() + 1,
    date.getMonth() - startFiscalYear + 1,
    1
  );
};

/**
 * Returns true if the calendar date passed as argument is in the same
 * year as the fiscal year.
 * @param {Date} date a (non-fiscal) calendar date
 * @param {number} startFiscalYear mandator's start fiscal year
 * @param {Date} today the current (non-fiscal) calendar date
 * @returns {Boolean}
 */
export const isWithinFiscalYear = (date: Date, startFiscalYear: number, today?: Date): Boolean => {
  if (!today) {
    today = new Date();
  }
  return getFiscalCalendarDate(today, startFiscalYear).getFullYear() === getFiscalCalendarDate(date, startFiscalYear).getFullYear();
};

export const mandatorNoModifier = (value: number): string | null => {
  if (value) {
    return value.toString().padStart(4, '0');
  }
  return null;
};

export const isMandatorTypeParentOrVessel = (mandatorType: MandatorTypes) =>
  mandatorType === MandatorTypes.parentCompany
  || mandatorType === MandatorTypes.multivesselCompany
  || mandatorType === MandatorTypes.vesselCompany;

export const getMandatorDescription = (mandator: IMandator | IMandatorLookup): string => {
  return mandator ? `${mandatorNoModifier(mandator.no)} ${mandator.name}` : '';
};
