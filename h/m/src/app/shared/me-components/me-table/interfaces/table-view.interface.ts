import { Type } from '@angular/core';

import { AttributeModelTypes } from '@shared/models/attribute/attribute.model';
import { LayoutModel } from '@shared/models/layout/layout.model';
import { TableActionButtonsComponent } from '../table-action-buttons/table-action-buttons.component';
import ActionButtonPropertiesI from './action-button-properties.interface';
import ActionButtonI from './action-button.interface';
import ColumnI from './column.interface';
import SelectComponentDataI from './select-component-data.interface';

export interface Entities {
  attributes: { isActive?: boolean; [key: string]: any };
  oid: string;
  [key: string]: any;
}

export interface ActionsButtonComponentI {
  forField: string;
  component: Type<TableActionButtonsComponent>;
  componentData: ActionButtonI[];
}

export default interface GenereateColumnsI {
  layouts: LayoutModel[];
  attributes: AttributeModelTypes[];
  customColumns?: ColumnI[];
}

export interface CellComponentI {
  forField: string;
  component: Type<any>;
}

export interface AttechComponentDataAndRowOIDToCellI {
  entities: Entities[];
  cellComponents: CellComponentI[];
  actionButtonProperties: ActionButtonPropertiesI[];
  replaceComponentsByFieldName?: {
    rowOID: string;
    cells: {
      fieldName: string;
      componentType: string;
      componentData: ComponentData;
    }[];
  }[];
  customComponents?: CustomComponentI[];
}

export interface CustomComponentI {
  rowOID: string;
  cells: {
    fieldName: string;
    componentType: string;
    componentData: ComponentData;
  }[];
}

export type ComponentData = SelectComponentDataI | string | number;
