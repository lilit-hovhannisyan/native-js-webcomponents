export default interface ResponseLogI {
  eventType: string;
  requestId: string;
  data: {
    method: string;
    params: string;
    status: string;
    elapsedTime: string;
    body: unknown;
  };
}
