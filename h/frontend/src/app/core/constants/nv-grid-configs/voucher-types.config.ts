import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { IVoucherType } from 'src/app/core/models/resources/IVoucherType';
import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { Operations } from 'src/app/core/models/Operations';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {

  return ({
    gridName: 'voucherTypesConfig',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 10,
      showTotalItems: true
    },
    columns: [
      {
        key: 'abbreviation',
        title: wording.accounting.abbreviation,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wording.accounting.description,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isAutomaticNumbering',
        title: wording.accounting.autoNumbering,
        width: 200,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isSystemDefault',
        title: wording.general.systemDefault,
        width: 200,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isActive',
        title: wording.accounting.active,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editVoucherType',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (voucherType: IVoucherType) => actions.edit(voucherType.id),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteVoucherType',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: (voucherType: IVoucherType) => actions.delete(voucherType),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: () => actions.add()
      }
    ],
    contextMenuButtons: canPerform(Operations.READ) ? getContextMenuButtonsConfig() : [],
  });
};
