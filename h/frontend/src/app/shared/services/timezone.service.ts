import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpGet } from '../../core/models/http/http-get';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TimezoneService {
  private apiKey = '9VRGWQWE7PCM';

  constructor(private http: HttpClient) { }

  public fetchTimezones(): Observable<any> {
    const url = `https://api.timezonedb.com/v2.1/list-time-zone?key=${this.apiKey}&format=json&fields=zoneName`;
    const request = new HttpGet(url);
    request.setNoModify();
    return this.http.request(request).pipe(map((res: HttpResponse<any>) => res.body));
  }

  public fetchTimezonesByName(zone: string): Observable<any> {
    const url = `http://api.timezonedb.com/v2.1/get-time-zone?key=${this.apiKey}&format=json&by=zone&zone=${zone}`;
    const request = new HttpGet(url);
    request.setNoModify();
    return this.http.request(request).pipe(map((res: HttpResponse<any>) => res.body));
  }

  public converTimezone(from, to): Observable<any> {
    const url = `http://api.timezonedb.com/v2.1/convert-time-zone?key=${this.apiKey}&format=json&from=${from}&to=${to}`;
    const request = new HttpGet(url);
    request.setNoModify();
    return this.http.request(request).pipe(map((res: HttpResponse<any>) => res.body));
  }
}
