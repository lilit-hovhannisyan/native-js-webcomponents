import { Component, Input } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup } from '@angular/forms';
import { BankAccountRepositoryService } from 'src/app/core/services/repositories/bank-account-repository.service';

@Component({
  selector: 'nv-bank-account-autocomplete',
  templateUrl: './bank-account-autocomplete.component.html',
  styleUrls: ['./bank-account-autocomplete.component.scss']
})
export class BankAccountAutocompleteComponent {

  public wording = wording;
  public gridConfig = null;

  @Input() public form: FormGroup;
  @Input() public controlName = 'bankAccountId';
  @Input() public filters: object;

  constructor(
    public bankAccountRepo: BankAccountRepositoryService,
  ) { }
}
