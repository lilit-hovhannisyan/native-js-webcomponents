import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';

import { MeButtonModule } from '../me-button/me-button.module';
import { MePopupBasicInputComponent } from './me-popup-basic-input.component';

import { MeIconsProviderModule } from '@shared-modules/me-icons.module';

@NgModule({
  imports: [CommonModule, MeIconsProviderModule, MatDialogModule, MeButtonModule],
  declarations: [MePopupBasicInputComponent],
  exports: [MePopupBasicInputComponent]
})
export class MePopupBasicInputModule {}
