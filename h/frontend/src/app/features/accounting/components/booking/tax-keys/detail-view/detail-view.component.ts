import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { wording } from 'src/app/core/constants/wording/wording';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { AccountService } from 'src/app/core/services/account.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { TaxKeyRepositoryService } from 'src/app/core/services/repositories/tax-key-repository.service';
import { ResourceDetailView } from '../../../../../../core/abstracts/ResourceDetailView';
import { Validators } from '../../../../../../core/classes/validators';
import { sitemap } from '../../../../../../core/constants/sitemap/sitemap';
import { CALCULATION_TYPES, ITaxKey, ITaxKeyCreate, TAX_CATEGORIES } from '../../../../../../core/models/resources/ITaxKeys';
import { BaseComponentService } from '../../../../../../core/services/base-component.service';
import { NvGridConfig } from 'nv-grid';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/booking-accounts.config';
@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class TaxKeyDetailViewComponent extends ResourceDetailView<ITaxKey, ITaxKeyCreate> implements OnInit {

  protected repoParams = {};
  protected resourceDefault = { isActive: true } as ITaxKey;

  private get isGettingDeactivated(): boolean {
    const isActive = this.form.controls.isActive.value;
    return !isActive && (this.resource && isActive !== this.resource.isActive);
  }

  public routeBackUrl: string;
  public form: FormGroup;
  public taxKey?: ITaxKey;
  public bookingAccounts$: Observable<IBookingAccount[]>;
  public taxCategories = TAX_CATEGORIES;
  public taxCalculationTypes = CALCULATION_TYPES;
  public gridConfig: NvGridConfig = getGridConfig({}, () => false);

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public accountService: AccountService,
    private bookingAccountsRepo: BookingAccountRepositoryService,
    taxKeyRepo: TaxKeyRepositoryService,
    baseComponentService: BaseComponentService
  ) {
    super(taxKeyRepo, baseComponentService);
  }


  public ngOnInit(): void {
    this.init();
    this.routeBackUrl = this.url(sitemap.accounting.children.booking.children.taxKeys);
    const { id } = this.route.parent.snapshot.params;
    this.bookingAccountsRepo.fetchAll({}).subscribe();
    this.bookingAccounts$ = this.bookingAccountsRepo.getStream();
    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => `${this.resource.code} ${this.resource[this.labelKey]}`;

  public init(taxKey: ITaxKey = this.resourceDefault): void {
    this.initForm(this.fb.group({
      labelDe: [taxKey.labelDe, [Validators.required, Validators.maxLength(100)]],
      labelEn: [taxKey.labelEn, [Validators.required, Validators.maxLength(100)]],
      code: [taxKey.code, [Validators.required, Validators.maxLength(10)]],
      isSystemDefault: [taxKey.isSystemDefault],
      percentage: [taxKey.percentage, [
        Validators.required,
        Validators.min(0),
        Validators.maxDecimalDigits(2),
        Validators.max(999.99)
      ]],
      calculationType: [taxKey.calculationType || CALCULATION_TYPES.include, [Validators.required]],
      taxCategory: [taxKey.taxCategory || TAX_CATEGORIES.inputTax, [Validators.required, Validators.min(0)]],
      bookingAccountId: [taxKey.bookingAccountId, [Validators.required]],
      isActive: [taxKey.isActive]
    }));
  }

  public submit(): void {
    if (!this.form.valid) { return; }

    if (this.isGettingDeactivated) {
      this.confirmationService.confirm({
        title: wording.accounting.taxKeyDeactivationTitle,
        wording: this.wording.accounting.taxKeyDeactivationWarning,
        okButtonWording: wording.general.deactivate,
        cancelButtonWording: wording.general.cancel,
      }).subscribe(confirmed => {
        if (confirmed) { this.updateEntity(); }
      });
    } else {
      this.updateEntity();
    }
  }

  private updateEntity(): void {
    const { value } = this.form;
    const freshTaxKey = this.creationMode
      ? { ...value, isActive: true }
      : { ...this.resource, ...value };

    this.creationMode
      ? this.createResource(freshTaxKey)
      : this.updateResource(freshTaxKey);

  }
}
