import { Type } from '@angular/core';
import { Subject } from 'rxjs';

interface RederedComponentI {
  component: Type<{ data: any; tableDataChangeEvent?: Subject<any> }>;
  componentData: any;
}

export default RederedComponentI;
