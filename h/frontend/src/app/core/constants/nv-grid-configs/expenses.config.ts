import { NvColumnDataType, NvFilterControl, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { ICharteringExpense } from 'src/app/core/models/resources/ICharteringExpense';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

export const getExpensesGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'expensesGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 25,
    },
    columns: [
      {
        key: 'shortName',
        title: wording.chartering.shortName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wording.general.name,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'typeLabel',
        title: wording.chartering.type,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isDefaultForTcVoyages',
        title: wording.chartering.defaultForTcVoyages,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.Boolean,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        }
      },
      {
        key: 'isActive',
        title: wording.general.active,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.Boolean,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        }
      },
      {
        key: 'accountLabel',
        title: wording.general.account,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'costType',
        title: wording.chartering.costType,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'bookingCodeLabel',
        title: wording.accounting.bookingCode,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'edit-expense',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (expense: ICharteringExpense) => actions.edit(expense.id),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'delete-expense',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: (expense: ICharteringExpense) => actions.delete(expense),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: () => actions.add()
      },
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
