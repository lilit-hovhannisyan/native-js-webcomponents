import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { IPaymentConditionCreate, IPaymentCondition } from 'src/app/core/models/resources/IPaymentConditions';
import { PaymentConditionsRepositoryService } from 'src/app/core/services/repositories/payment-conditions-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Validators } from 'src/app/core/classes/validators';
import { inOrder } from '../dunning-system/dunning-levels-validator';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { HttpResponseBase } from '@angular/common/http';
import { DebitorCreditorRepositoryService } from 'src/app/core/services/repositories/debitor-creditor-repository.service';
import { IDebitorCreditor } from 'src/app/core/models/resources/IDebitorCreditor';

@Component({
  selector: 'nv-conditions-of-payment',
  templateUrl: './conditions-of-payment.component.html',
  styleUrls: ['./conditions-of-payment.component.scss']
})
export class DebitorCreditorConditionsOfPaymentTabComponent extends ResourceDetailView<
IPaymentCondition,
IPaymentConditionCreate
> implements OnInit {
  public routeBackUrl = this.url(sitemap.accounting.children.booking.children.debitorCreditors);
  public daysOrderErrorKey = 'daysOrderError';
  public percentageOrderErrorKey = 'percentageOrderError';
  private dicsountLevelValidators = [];
  public formElement: ElementRef;

  protected resourceDefault: IPaymentCondition = {
    net: 14,
  } as IPaymentCondition;
  protected repoParams: RepoParams<IPaymentCondition> = {};

  private debitorCreditor: IDebitorCreditor;

  constructor(
    private paymentConditionRepo: PaymentConditionsRepositoryService,
    baseComponentService: BaseComponentService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private el: ElementRef,
    private debitorCreditorService: DebitorCreditorRepositoryService,
  ) {
    super(paymentConditionRepo, baseComponentService);
  }

  public ngOnInit() {
    const id: number = +this.route.parent.snapshot.params.id;
    this.resourceDefault = { ...this.resourceDefault, debitorCreditorId: id };

    this.repoParams = { errorHandler: this.errorHandler };
    this.init();
    this.debitorCreditorService.fetchOne(id, {})
      .pipe(
        tap(dc => this.debitorCreditor = dc),
        switchMap(() => this.paymentConditionRepo.fetchByDebitorCreditor({
          debitorCreditorId: id,
          errorHandler: (error: HttpResponseBase) => {
            if (error.status === 404) {
              return true;
            }
            return error;
          }
        })),
        map(dds => dds),
        catchError(this.errorHandler)
      ).subscribe((pc) => this.initializeResource(pc));
  }

  public getTitle = () => this.debitorCreditor.no.toString();

  public init(pc: IPaymentCondition = this.resourceDefault) {
    this.dicsountLevelValidators = [
      Validators.integerBetween(1, 99, true),
    ];
    this.initForm(this.fb.group({
      daysDiscountLevel1: [pc.daysDiscountLevel1, [Validators.number, Validators.integerBetween(1, 91)]],
      daysDiscountLevel2: [pc.daysDiscountLevel2, [Validators.number, Validators.integerBetween(1, 91)]],
      daysDiscountLevel3: [pc.daysDiscountLevel3, [Validators.number, Validators.integerBetween(1, 91)]],
      percentageDiscountLevel1: [pc.percentageDiscountLevel1, [...this.dicsountLevelValidators]],
      percentageDiscountLevel2: [pc.percentageDiscountLevel2, [...this.dicsountLevelValidators]],
      percentageDiscountLevel3: [pc.percentageDiscountLevel3, this.dicsountLevelValidators],
      customerNo: [pc.customerNo, [Validators.number]],
      termsAgreed: [pc.termsAgreed, Validators.maxLength(200)],
      net: [pc.net, Validators.required],
      debitorCreditorId: [pc.debitorCreditorId],
    }, {
      validator: [
        inOrder(
          'daysDiscountLevel1',
          'daysDiscountLevel2',
          'daysDiscountLevel3',
          'net',
          this.daysOrderErrorKey
        ),
        inOrder(
          'percentageDiscountLevel3',
          'percentageDiscountLevel2',
          'percentageDiscountLevel1',
          null,
          this.percentageOrderErrorKey,
          this.wording.system.discountPercentageMustBeInDescendingOrder
        )
      ],
    }));
  }

  public submit() {
    if (this.form.invalid) {
      return;
    }
    const { value } = this.form;
    this.resource && this.resource.id
      ? this.updateResource({ ...value, id: this.resource.id }).subscribe()
      : this.createResource(value, false).subscribe();
  }

  private errorHandler = (err) => {
    if (err && err.status === 404) {
      return of(this.resourceDefault);
    }
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

}
