import { Component, OnInit, Input } from '@angular/core';
import { IMandatorVoucherType } from 'src/app/core/models/resources/IMandatorVoucherType';
import { wording } from 'src/app/core/constants/wording/wording';
import { MandatorVoucherTypeRepositoryService } from 'src/app/core/services/repositories/mandator-voucher-type-repository.service';
import { FormGroup, FormBuilder } from '@angular/forms';


@Component({
  selector: 'nv-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.scss']
})
export class MandatorVoucherTypeEditComponent implements OnInit {
  @Input() public closeModal: () => void;
  @Input() public voucherType: IMandatorVoucherType;
  public wording = wording;
  public form: FormGroup;

  constructor(
    private mandatorVoucherTypeService: MandatorVoucherTypeRepositoryService,
    private fb: FormBuilder,
  ) { }

  public ngOnInit() {
    this.init(this.voucherType);
  }

  public init(data: IMandatorVoucherType = {} as IMandatorVoucherType) {
    this.form = this.fb.group({
      labelEn: [{ value: data.labelEn, disabled: true }],
      labelDe: [{ value: data.labelDe, disabled: true }],
      abbreviation: [{ value: data.abbreviation, disabled: true }],
      isAutomaticNumbering: [data.isAutomaticNumbering],
    });
  }

  private getVoucherTypeValue(): any {
    return {
      ... this.voucherType,
      isAutomaticNumbering: this.form.value.isAutomaticNumbering,
    };
  }

  public saveClicked(): void {
    this.mandatorVoucherTypeService.update(this.getVoucherTypeValue(), {})
      .subscribe(this.closeModal);
  }
}
