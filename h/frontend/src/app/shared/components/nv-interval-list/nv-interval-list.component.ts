import { sortBy } from 'lodash';
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { isNumber } from '../../helpers/general';
import { wording } from 'src/app/core/constants/wording/wording';

type IntervalItem = {
  from: string // iso-data
  until?: string // iso-data
  id: number
  orderNumber?: number // gets added by this component
};

// TODO: change this component structure and use NvVerticalListComponent
@Component({
  selector: 'nv-interval-list',
  templateUrl: './nv-interval-list.component.html',
  styleUrls: ['./nv-interval-list.component.scss']
})
export class NvIntervalListComponent implements OnInit {
  @Input('intervalItems')
  public set intervalItems(items: Array<IntervalItem>) {
    const itemsWithOrderNumber = items.map((item, i) => ({ ...item, orderNumber: i + 1 }));
    this.intervalItemsSorted = sortBy(itemsWithOrderNumber || [], ['from']).reverse();
  }
  @Input() public selectedId?: number;
  @Input() public withCreateOption?: boolean;
  @Input() public disabled?: boolean;
  @Input() public creationMode?: boolean; // adds an empty intreval at the top
  @Input() public withOrderNumber = false;
  @Input() public width = 230;
  @Input() public withTime = true;

  @Output() public select: EventEmitter<number> = new EventEmitter<number>();
  @Output() public create: EventEmitter<void> = new EventEmitter<void>();


  public intervalItemsSorted: IntervalItem[] = [];
  public wording = wording;
  constructor() { }

  public getNextOrderNumber = () => {
    const nextNumber = Math.max(...this.intervalItemsSorted.map(i => i.orderNumber));
    return Number.isSafeInteger(nextNumber) && isNumber(nextNumber) ? nextNumber + 1 : 1;
  }

  public ngOnInit() { }
}
