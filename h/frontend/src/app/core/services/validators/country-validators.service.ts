import { ValidationErrors, AbstractControl } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { Injectable } from '@angular/core';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { CountryRepositoryService } from 'src/app/core/services/repositories/country-repository.service';


@Injectable({
  providedIn: 'root'
})
export class CountryValidatorsService {

  constructor(private countryRepo: CountryRepositoryService) { }

  public doesCountryExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundCountry = this.countryRepo.getStreamValue()
      .find(country => country.id === control.value);

    const isValid = foundCountry ? true : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      countryNotfound: {
        message: wording.accounting.notFoundCountry
      }
    };
  }

  public isCountryActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const foundCountry = this.countryRepo.getStreamValue()
      .find(country => country.id === control.value);

    const isValid = foundCountry
      ? foundCountry.isActive
      : false;

    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      countryNotActive: {
        message: wording.accounting.notActiveCountry
      }
    };
  }
}
