import { NvGridButtonsPosition, NvGridConfig, NvFilterControl, NvColumnDataType } from 'nv-grid';
import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { wording } from 'src/app/core/constants/wording/wording';
import { IArticleUnit } from 'src/app/core/models/resources/IArticleUnit';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'generalUnitsGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'labelEn',
        title: wording.general.nameEn,
        width: 90,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'labelDe',
        title: wording.general.nameDe,
        width: 90,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'code',
        title: wording.technicalManagement.code,
        width: 75,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'decimalPlaces',
        title: wording.technicalManagement.decimalPlaces,
        width: 130,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'isSystemDefault',
        title: wording.general.systemDefault,
        width: 120,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isActive',
        title: wording.accounting.active,
        width: 80,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editPort',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (unit: IArticleUnit) => actions.edit(unit),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'generalUnits',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: (unit: IArticleUnit) => actions.delete(unit),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: () => actions.add()
      }
    ],
    contextMenuButtons: canPerform(Operations.READ) ? getContextMenuButtonsConfig() : [],
  });
};
