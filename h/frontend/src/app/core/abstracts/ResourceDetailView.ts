import { NzMessageService } from 'ng-zorro-antd/message';
import { FormGroup } from '@angular/forms';
import { IResource, ResourceKey } from 'src/app/core/models/resources/IResource';
import { Observable, BehaviorSubject, noop, of } from 'rxjs';
import { BaseComponent } from 'src/app/core/abstracts/BaseComponent';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { AbstractRepository, RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { tap, shareReplay, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { uniq } from 'lodash';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { IWording } from 'src/app/core/models/resources/IWording';

export interface NVHttpErrorItem {
  field?: string;
  key: string;
  messageWording: IWording;
}
export interface NVCustomError {
  changeFieldTo?: string;
  messageWording?: IWording;
}
export interface NVCustomErrors {
  [key: string]: NVCustomError;
}
export interface NVHttpErrorResponse extends HttpErrorResponse {
  error: {
    errors?: NVHttpErrorItem[],
  };
}

/**
 * This Class provides basic functionality to detailViews. Mainly doing:
 * - Fetching a single Resource.
 * - Managing the editMode and crationMode ( enabling / disabling the form )
 * - Updating or Creating the Resource
 */
export abstract class ResourceDetailView<
  Entity extends IResource<ResourceKey>,
  EntityCreate,
  > extends BaseComponent {

  public cloneMode = false;
  public editMode = false;
  public creationMode = false;
  public form: FormGroup;
  public resource?: Entity;
  public wordingAccounting = this.wording;
  public canRouteBack = true; // can route back is false in creationMode
  public showIsActiveCheckbox: boolean;

  public inEditMode = new BehaviorSubject(false);
  public inEditMode$ = this.inEditMode.asObservable();

  public labelKey: string;
  protected customErrors: NVCustomErrors = {};

  private titleSubject$: BehaviorSubject<string> = new BehaviorSubject('');
  public title$: Observable<string> = this.titleSubject$.asObservable();

  private disabledFields: string[] = [];

  protected abstract repoParams: RepoParams<Entity>;
  protected abstract resourceDefault: {};

  protected fieldsToClone: string[] = [];
  protected isCloningAllowed = false;

  public abstract routeBackUrl?: string; // in case of undefiend, the route-back-button will get hidden
  protected routeBackUrlQueryParams = {};

  public set title(value: string) {
    this.titleSubject$.next(value);
  }

  public static handleSubmitError(
    response: NVHttpErrorResponse,
    form: FormGroup,
    nzMessageService: NzMessageService,
    customErrors: NVCustomErrors = {},
  ) {
    const { errors } = response.error;
    let errorsFound = false;
    if (form && errors) {
      errors.forEach(error => {
        const customError = customErrors[error.key];
        const fieldControlName = customError && customError.changeFieldTo
          ? customError.changeFieldTo
          : error.field;
        const formField = form.get(fieldControlName);

        if (formField) {
          const messageWording = customError && customError.messageWording
            ? customError.messageWording
            : error.messageWording;

          formField.markAsTouched();
          formField.setErrors({
            // TODO: change in the future
            error: {
              // type of the `message` is IWording
              // otherwise will be thrown an error with by `translate` pipe in input-wrapper component
              message: messageWording
            }
          });
          errorsFound = true;
        }
      });
    }

    // We only catch field errors here. GlobalErrors are already displayed
    // as Notifications from within the Abstract Repository
    if (errorsFound) {
      nzMessageService.error('The form contains errors');
    }
  }

  public abstract init(resource: Entity): void;

  constructor(
    private repository: AbstractRepository<Entity, IResource<ResourceKey>, EntityCreate, RepoParams<Entity>, ResourceKey>,
    baseComponentService: BaseComponentService,
  ) {
    super(baseComponentService);
    this.labelKey = getLabelKeyByLanguage(this.settingsService.language);
  }

  public enterCreationMode = (): void => {
    this.creationMode = true;
    this.canRouteBack = false;
    const cloneId = this.baseComponentService.router.routerState.snapshot.root.queryParams['clone_id'];
    this.cloneMode = this.isCloningAllowed && cloneId;
    if (this.cloneMode) {
      this.loadCloneResource(cloneId).subscribe(this.enterEditMode);
    } else {
      this.enterEditMode();
    }
  }

  public enterEditMode = (): void => {
    if (this.resource && this.resource['isSystemDefault'] && !this.isSuperAdmin) {
      this.baseComponentService.commonNotificationsService.showSystemDefaultWarning();
      return;
    }

    // in creation mode form is undefined, check added so there will be no need to add
    // this.init() before this.enterCreationMode()
    !this.form && this.init(this.resource as Entity);
    this.form.enable();
    this.disableMarkedFields();
    this.editMode = true;
    this.inEditMode.next(this.editMode); // update observable
  }

  public cancelEditing = (): void => {
    if (this.creationMode) { this.routeBack(); return; }
    this.leaveEditMode();
    this.init(this.resource as Entity);
  }

  public leaveEditMode = (): void => {
    this.editMode = false;
    this.inEditMode.next(this.editMode); // update observable
    this.creationMode = false;
    this.form.disable();
  }

  public initForm = (form: FormGroup): void => {
    this.form = form;
    this.form.disable();
    this.form.contains('isSystemDefault') && this.handleSystemDefaultField();
  }

  public disableField(fieldName: string) {
    this.setDisabledFields(uniq([...this.disabledFields, fieldName]));
  }

  public setDisabledFields(fieldNames: string[]): void {
    this.disabledFields = fieldNames;
    this.disableMarkedFields();
  }

  public getDisabledFields(): string[] {
    return this.disabledFields;
  }

  public enableDisabledFields(): void {
    this.getDisabledFields().forEach(field => {
      this.form.get(field).enable();
    });
  }

  public disableMarkedFields() {
    setTimeout(() => {
      this.disabledFields.forEach(fieldName => {
        this.form.controls[fieldName].disable();
      });
    });
  }

  public routeBack = (): Promise<boolean> => this.router.navigate(
    [this.routeBackUrl],
    { queryParams: this.routeBackUrlQueryParams }
  )

  // This method should be used by the child component if there is no
  // need to load a resource. Otherwise 'loadResource' should be used.
  public initializeResource = (resource: Entity): void => {
    this.resource = resource;
    this.showIsActiveCheckbox = (this.resource && !this.cloneMode && !this.resource['isSystemDefault']) || this.isSuperAdmin;
    this.init(resource as Entity);
    const title = this.getTitle();
    if (title && !this.cloneMode) {
      this.title = title;
      this.browserTitleService.setTitleWithInfo(title);
    }
  }

  public loadResource = (id: ResourceKey): Observable<Entity> => {
    const req$ = this.repository.fetchOne(id, this.repoParams)
      .pipe(
        tap(resource => { this.initializeResource(resource); }),
        shareReplay(1)
      );
    req$.subscribe(noop);
    return req$;
  }

  private loadCloneResource = (cloneId: ResourceKey): Observable<Entity> => {
    return this.repository.fetchOne(cloneId, { cloneMode: true })
      .pipe(
        catchError(() => of({} as Entity)),
        tap(resource => {
          this.initializeResource({
            ...this.resourceDefault,
            ...this.getClonedFields(resource),
          });
        }),
      );
  }

  protected getClonedFields = (resource: Entity): Entity => {
    if (!this.fieldsToClone.length) {
      const { id, ...clonedResource } = resource;

      return clonedResource as Entity;
    }

    return this.fieldsToClone.reduce((clonedResource, field) => {
      clonedResource[field] = resource[field];

      return clonedResource;
    }, {}) as Entity;
  }


  /**
   * @param {EntityCreate} entityCreate to create new resource
   * @param {boolean} routeBack has a value `true` by default tells the function to route
   * back or not
   */
  public createResource = (entityCreate: EntityCreate, routeBack = false, changeRouteParam = true): Observable<Entity> => {
    const req$ = this.repository.create(entityCreate, this.repoParams)
      .pipe(
        tap((res) => {
          this.form.markAsPristine();
          if (routeBack) {
            this.routeBack();
          } else {
            if (changeRouteParam) {
              this.router.navigate(
                [this.getCurrentUrl().replace('new', res.id as any)],
                { queryParamsHandling: 'preserve' }
              );
            }
            this.leaveEditMode();
            this.initializeResource(res);
            this.creationMode = false;
          }
        }),
        shareReplay(1)
      );
    req$.subscribe(noop, this.handleRequestError);
    return req$;
  }

  public getCurrentUrl = (): string => {
    // strips away queryParams if present
    return this.router.url.split('?')[0];
  }

  public handleRequestError = (error: NVHttpErrorResponse): void => {
    ResourceDetailView.handleSubmitError(
      error,
      this.form,
      this.messageService,
      this.customErrors,
    );
  }

  public updateResource = (entity: Entity): Observable<Entity> => {
    const req$ = this.repository.update(entity, this.repoParams)
      .pipe(tap(freshResource => {
        this.leaveEditMode();
        this.initializeResource(freshResource);
      }),
        shareReplay(1)
      );
    req$.subscribe(noop, this.handleRequestError);
    return req$;
  }

  public handleSystemDefaultField(): void {
    // Only superAdmin while creating an entity, can set the systemDefault flag
    const systemDefaultEditable = this.creationMode && this.authService.getSessionInfo().user.isSuperAdmin;
    if (!systemDefaultEditable) { this.setDisabledFields(['isSystemDefault']); }
  }

  public abstract getTitle(): string;
}
