import { BaseLoggerComponent } from '@features/tracking-system';
import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, Injector } from '@angular/core';
import { MeMaterialCardI } from './me-material-card.interface';
@Component({
  selector: 'me-material-card',
  templateUrl: './me-material-card.component.html',
  styleUrls: ['./me-material-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeMaterialCardComponent extends BaseLoggerComponent {
  componentVersion: string = '0.0.1';
  @Input() dataModel!: MeMaterialCardI;
  @Output() eventOccurs: EventEmitter<{ eventName: string; payload: string }> = new EventEmitter();

  constructor(injector: Injector) {
    super(injector);
  }

  customEvent(eventName: string, payload: string = ''): void {
    this.eventOccurs.emit({ eventName, payload });
  }

  preventOtherEvents(e: Event): void {
    e.stopPropagation();
  }
}
