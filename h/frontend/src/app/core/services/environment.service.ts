import { environment as env } from '../../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable()
export class EnvironmentService {

  public apiHost: string = env.apiHost;
  public apiBasicAuth: string = env.apiBasicAuth;
  public production: boolean = env.production;
  public PREFER_FIXTURES: boolean = env.PREFER_FIXTURES;

  public isDevMode(): boolean {
    return !this.production;
  }
}
