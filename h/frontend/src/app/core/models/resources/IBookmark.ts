import { IResource } from './IResource';
import { SitemapNode } from '../../constants/sitemap/sitemap';

export type BookmarkKey = number;

interface ISharedProps {
  userId: number;
  topicKey: string;
}

export interface IBookmarkCreate extends ISharedProps {}

export interface IBookmarkBase extends ISharedProps, IResource<number> {}

export interface IBookmarkDTO extends IBookmarkBase {}

export interface IBookmark extends IBookmarkDTO {}

export interface IBookmarkFull extends IBookmark {
  node: SitemapNode;
}
