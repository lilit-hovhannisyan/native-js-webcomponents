import { Injectable } from '@angular/core';
import { AbstractWidgetsService } from './abstract-widgets-repository';
import { IClockWidget, IClockWidgetDTO } from 'src/app/core/models/widgets/IClockWidget';
import { RepositoryService } from '../../../../core/services/repository.service';

@Injectable({
  providedIn: 'root'
})
export class ClockWidgetService extends AbstractWidgetsService {

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl() {
    return '/api/user-account/widgets/clock';
  }

  public preProcess(w: IClockWidget): IClockWidgetDTO {
    return {
      id: w.id,
      type: w.type,
      _type: w._type,
      position: {
        x: w.x,
        y: w.y,
        width: w.cols,
        height: w.rows
      },
      timeZones: w.timeZones
    };
  }

  public updateWidget(widget: IClockWidget) {
    return this.update(this.preProcess(widget), {});
  }
}
