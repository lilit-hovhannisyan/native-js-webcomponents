import { IWithActive } from 'src/app/core/models/resources/IWithActive';
import { IWithLabels } from 'src/app/core/models/resources/IWithLabels';
import { IWithSystemFlag } from 'src/app/core/models/resources/IWithSystemFlag';
import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';
import { IArticleUnit } from './IArticleUnit';
import { IArticleClass } from './IArticleClass';
import { IArticleType } from 'src/app/core/models/resources/IArticleType';
import { ICurrency } from './ICurrency';
import { IWording } from './IWording';
import { IDebitorCreditor } from './IDebitorCreditor';

export type ArticleKey = number;

interface ISharedProps extends IWithLabels, IWithActive, IWithSystemFlag {
  no: string;
  articleUnitId: number;
  articleClassId?: number;
  articleTypeId?: number;
  description?: string;
  manufacturerManual?: string;
  debitorCreditorId?: number;
  manufacturerArticleNo?: string;
  itemType?: string;
  serialNo?: string;
  year?: number;
  salePrice?: number;
  currencyId?: number;
}

export interface IArticleCreate extends ISharedProps { }

export interface IArticleBase extends ISharedProps, IResource<ArticleKey> { }

export interface IArticleDTO extends IArticleBase { }

export interface IArticle extends IArticleDTO, IWithLabel { }

export interface IArticleFull extends IArticle {
  articleUnit: IArticleUnit;
  articleClass: IArticleClass;
  articleType: IArticleType;
  currency: ICurrency;
  debitorCreditor: IDebitorCreditor;
}

export interface IArticleRow {
  unitName: IWording;
  articleClassName: IWording;
  articleTypeName: IWording;
  isoCode: string;
  debitorCreditorName: IWording;
}
