/**
 * This class is used to transport a resource payload with additional
 * meta data to the server.
 *
 * The meta property allows you to add additional information about the
 * data or instructions what should be done with the data.
 * Note that server defines what meta is allowed or necessary for the
 * requests.
 */

export class DataCarrier {
  public data: any;
  public meta?: any;
  public constructor(
    data: {},
    meta?: {}
  ) {
    this.data = data;
    this.meta = meta;
  }
}
