import { MandatorKey } from './IMandator';
import { IResource } from './IResource';
import { UserKey } from './IUser';

export type AutobankPermissionGroupKey = number;

interface ISharedProps {
  displayLabel: string;
  mandatorsIds: MandatorKey[];
  usersIds: UserKey[];
}

export interface IAutobankPermissionGroupBase extends ISharedProps, IResource<AutobankPermissionGroupKey> { }

export interface IAutobankPermissionGroupCreate extends ISharedProps { }

export interface IAutobankPermissionGroupDTO extends IAutobankPermissionGroupBase { }

export interface IAutobankPermissionGroup extends IAutobankPermissionGroupDTO { }

