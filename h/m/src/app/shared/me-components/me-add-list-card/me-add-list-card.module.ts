import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeAddListCardComponent } from './me-add-list-card.component';
import { MeButtonModule } from '../me-button/me-button.module';

import { MeUserImageModule } from '@shared-components/me-user-image/me-user-image.module';

@NgModule({
  declarations: [MeAddListCardComponent],
  imports: [CommonModule, MeUserImageModule, MeButtonModule],
  exports: [MeAddListCardComponent]
})
export class MeAddListCardModule {}
