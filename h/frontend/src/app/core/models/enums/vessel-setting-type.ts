import { wording } from '../../constants/wording/wording';
import { IWording } from '../resources/IWording';

export enum VESSEL_SETTING_TYPE {
  None = 0,
  Registration = 1,
  TechnicalInspection = 2,
  CharteringAndOperating = 4,
  Bookkeeping = 8,
  Insurance = 16,
  Crewing = 32,
  SaleAndPurchase = 64,
}

export type VesselSettingTypeMap<T = IWording> = Map<VESSEL_SETTING_TYPE, T>;
export const vesselSettingTypeOptions: VesselSettingTypeMap = new Map([
  [
    VESSEL_SETTING_TYPE.Registration,
    wording.chartering.vesselSettingsRegistration
  ],
  [
    VESSEL_SETTING_TYPE.TechnicalInspection,
    wording.chartering.vesselSettingsTechnicalInspection
  ],
  [
    VESSEL_SETTING_TYPE.CharteringAndOperating,
    wording.chartering.vesselSettingsCharteringAndOperating
  ],
  [
    VESSEL_SETTING_TYPE.Bookkeeping,
    wording.chartering.vesselSettingsBookkeeping
  ],
  [
    VESSEL_SETTING_TYPE.Insurance,
    wording.chartering.vesselSettingsInsurance
  ],
  [
    VESSEL_SETTING_TYPE.Crewing,
    wording.chartering.vesselSettingsCrewing
  ],
  [
    VESSEL_SETTING_TYPE.SaleAndPurchase,
    wording.chartering.vesselSettingsSaleAndPurchase
  ],
]);
