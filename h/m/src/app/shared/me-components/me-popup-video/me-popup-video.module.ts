import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';

import { VgCoreModule } from 'ngx-videogular';

import { MePopupVideoComponent } from './me-popup-video.component';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';
import { MePopupVideoService } from './me-popup-video.service';

@NgModule({
  imports: [CommonModule, MeIconsProviderModule, MatDialogModule, VgCoreModule],
  declarations: [MePopupVideoComponent],
  exports: [MePopupVideoComponent],
  providers: [MePopupVideoService]
})
export class MePopupVideoModule {}
