import { Injectable } from '@angular/core';
import { IBookingLineRow } from '../models/resources/IBookingLine';
import { BookingAccountRepositoryService } from './repositories/booking-account-repository.service';
import { BookingCodeRepositoryService } from './repositories/booking-code-repository.service';
import { TaxKeyRepositoryService } from './repositories/tax-key-repository.service';

@Injectable({
  providedIn: 'root'
})
export class BookingCalculatorService {

  private taxBookingCodeNo = 324;

  constructor(
    public taxKeyRepo: TaxKeyRepositoryService,
    public bookingAccountRepo: BookingAccountRepositoryService,
    public bookingCodeRepo: BookingCodeRepositoryService,
  ) {
  }

  public getTaxReadonlyLines(bookingLines: IBookingLineRow[]): IBookingLineRow[] {
    if (!bookingLines || bookingLines.length === 0) {
      return [];
    }

    const taxBookingLines = [] as IBookingLineRow[];

    for (let i = 0; i < bookingLines.length; i++) {
      const bookingLine = bookingLines[i];

      // Lines without tax key are ignored
      if (!bookingLine.taxKeyId) {
        continue;
      }

      const taxBookingLine = this.createTaxReadonlyLine(bookingLine);

      if (!taxBookingLine) {
        continue;
      }
      // If a tax key has already a tax booking line, ignore
      if (taxBookingLines.findIndex(
        bl => bl.bookingAccountId === taxBookingLine.bookingAccountId) >= 0) {
        continue;
      }

      taxBookingLines.push(taxBookingLine);
    }

    return taxBookingLines;
  }

  public createTaxReadonlyLine(bookingLine: IBookingLineRow): IBookingLineRow {

    const foundTaxKey = this.taxKeyRepo.getStreamValue()
      .find(taxKey => taxKey.id === bookingLine.taxKeyId);

    if (!foundTaxKey) {
      return null;
    }

    const foundTaxBookingAccount = this.bookingAccountRepo.getStreamValue()
      .find(bookingAccount => bookingAccount.id === foundTaxKey.bookingAccountId);

    if (!foundTaxBookingAccount) {
      return null;
    }

    const foundTaxBookingCode = this.bookingCodeRepo.getStreamValue()
      .find(bookingCode => bookingCode.no === this.taxBookingCodeNo);

    if (!foundTaxBookingCode) {
      return null;
    }

    const taxBookingLine: IBookingLineRow = {
      id: 0,
      mandatorId: bookingLine.mandatorId,
      mandatorNo: bookingLine.mandatorNo,
      bookingAccountId: foundTaxKey.bookingAccountId,
      accountNo: foundTaxBookingAccount.no,
      bookingCodeId: foundTaxBookingCode.id,
      bookingCodeNo: foundTaxBookingCode.no,
      text: bookingLine.text,
      invoiceNo: null,
      discount: null,
      paymentDate: null,
      amountTransaction: 0,
      amountLocal: 0,
      '+/-': '+',
      debitCreditType: 1,
      currencyId: bookingLine.currencyId,
      currencyIsoCode: bookingLine.currencyIsoCode,
      voyage: null,
      lineIndex: 1,
      exchangeRate: null,
      portId: null,
      bookingId: null,
      countryId: null,
      taxKeyId: null,
      costTypeId: null,
      costCenterId: null,
      costUnitId: null,
      accrualAccounting: false,
      _type: '',
      readOnly: true
    };

    return taxBookingLine;
  }
}

