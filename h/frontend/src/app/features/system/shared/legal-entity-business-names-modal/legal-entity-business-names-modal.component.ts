import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { wording } from 'src/app/core/constants/wording/wording';
import { IBusinessName } from 'src/app/core/models/resources/IBusinessName';
import { Validators } from 'src/app/core/classes/validators';
import { SettingsService } from 'src/app/core/services/settings.service';
import { IWording } from 'src/app/core/models/resources/IWording';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import * as moment from 'moment';

@Component({
  templateUrl: './legal-entity-business-names-modal.component.html',
  styleUrls: ['./legal-entity-business-names-modal.component.scss']
})
export class LegalEntityBusinessNamesModalComponent implements OnInit {
  @Input() public businessName?: IBusinessName;
  @Input() public lastBusinessName?: IBusinessName;
  @Input() public businessNameWording: IWording = wording.system.businessName;
  @Input() public editMode: boolean;
  public form: FormGroup;
  public wording = wording;

  constructor(
    public modalRef: NzModalRef<LegalEntityBusinessNamesModalComponent, IBusinessName>,
    private settingsService: SettingsService,
    private fb: FormBuilder,
  ) { }

  public ngOnInit() {
    this.init(this.businessName);
  }

  private init(resource?: IBusinessName): void {
    // allow user to only insert date <= current date +1day
    const dateShouldBeAfterValidators: ValidatorFn[] = [
      Validators.required,
      Validators.dateShouldBeBefore(
        moment().startOf('day').add(1, 'days').format(),
        this.settingsService.locale,
        true,
        false,
        'days',
      )];

    this.form = this.fb.group({
      name: [resource?.name, [Validators.required, Validators.maxLength(150)]],
      from: [
        resource?.from || new Date(),
        [...dateShouldBeAfterValidators]
      ],
    });

    if (resource && !this.editMode) {
      this.form.disable();
    }
  }

  public submit(): void {
    validateAllFormFields(this.form);
    if (!this.form.valid) {
      return;
    }

    this.modalRef.close({
      ...this.businessName,
      ...this.form.getRawValue(),
    });
  }
}
