import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MandatorRemarksComponent } from './mandator-remarks.component';
import { IUser } from 'src/app/core/models/resources/IUser';

describe('MandatorRemarksComponent', () => {
  let component: MandatorRemarksComponent;
  let fixture: ComponentFixture<MandatorRemarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MandatorRemarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MandatorRemarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return formatted datetime and full name', () => {
    const user = {
      firstName: 'Admin',
      lastName: 'Adminyan',
    } as IUser;
    const displayValue = component['formatUserAndDateTime'](user, '2019-10-10T06:04:42');
    expect(displayValue).toBe('10.10.2019 10:04 / Admin Adminyan');
  });
});
