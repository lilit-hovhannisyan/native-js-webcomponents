/**
 * @see https://stackoverflow.com/questions/43176560/property-files-does-not-exist-on-type-eventtarget-error-in-typescript
 */
export interface IHTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}
