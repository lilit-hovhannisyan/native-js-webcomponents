import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { IWording } from 'src/app/core/models/resources/IWording';
import { wording } from 'src/app/core/constants/wording/wording';
import { ThemeTypes } from 'src/app/core/models/Theme.enum';
import { SettingsService } from 'src/app/core/services/settings.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { ICompany } from 'src/app/core/models/resources/ICompany';
import { COMPANY_LEGAL_FORM, legalEntityExclamationTooltipContributions, getContributionWordingKey } from 'src/app/core/models/enums/company-legal-form';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { ContributionMainService } from 'src/app/core/services/mainServices/company-contributions-main.service';
import { CompaniesHelperService } from '../companies.helper.service';
import { CompanyLegalEntityRepositoryService } from 'src/app/core/services/repositories/company-legal-entity-repository.service';
import { switchMap } from 'rxjs/operators';
import { ShareholdingsHelperService } from '../../../shareholdings/shareholdings.helper.service';

/**
 * TODO:
 * Change current mechanism of showing exclamation-icon with `isDefaultTab`
 * if there will be another tabs with icons.
 * For current AC preferable a small solution with less changes.
 */
export interface IVerticalTabData {
  title: IWording;
  path: string;
  isDefaultTab?: boolean;
}

@Component({
  selector: 'nv-company-legal-tab',
  templateUrl: './company-legal-tab.component.html',
  styleUrls: ['./company-legal-tab.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CompanyLegalTabComponent implements OnInit {
  public sideTabs$ = new BehaviorSubject<IVerticalTabData[]>([]);
  public theme$: Observable<ThemeTypes> = this.settingsService.themeNameSubj.asObservable();
  public showLegalEntityExclamationIcon = false;
  public showTrusteeAndSubParticipantsExclamationIcons = false;
  public wording = wording;
  private legalEntityExclamationTooltip: IWording;
  private trusteeAndSubParticipantsExclamationTooltip: IWording = wording.system.shareholderRequired;
  public isCompanyResolved: boolean;

  private company: ICompany;

  constructor(
    private settingsService: SettingsService,
    private contributionMainService: ContributionMainService,
    private companiesHelperService: CompaniesHelperService,
    private legalEntityRepo: CompanyLegalEntityRepositoryService,
    private shareholdingsHelperService: ShareholdingsHelperService,
  ) { }

  public ngOnInit(): void {
    this.shareholdingsHelperService.updateModeState(false);
    this.company = this.companiesHelperService.company;
    this.setTabs(this.company.legalForm);
    this.setLegalTabsExclamationIcons(this.company);
    this.setLegalEntityExclamationWording(this.company);
  }

  public isTabDisabled(tab: IVerticalTabData): boolean {
    return !tab.isDefaultTab && this.showLegalTabExclamationIcon(tab);
  }

  public showLegalTabExclamationIcon(tab: IVerticalTabData): boolean {
    const legalChildren: { [key: string]: SitemapNode }
      = sitemap.system.children.misc.children.companies.children.id.children.legal.children;

    switch (tab.path) {
      case legalChildren.legalEntity.path:
        return this.showLegalEntityExclamationIcon;

      case legalChildren.trustors.path:
      case legalChildren.subParticipants.path:
        return this.showTrusteeAndSubParticipantsExclamationIcons;

      default:
        return false;
    }
  }

  public getExclamationTooltip(tab: IVerticalTabData) {
    const legalNode: SitemapNode = sitemap.system.children.misc.children.companies.children.id.children.legal;

    switch (tab.path) {
      case legalNode.children.legalEntity.path:
        return this.legalEntityExclamationTooltip;

      case legalNode.children.trustors.path:
      case legalNode.children.subParticipants.path:
        return this.trusteeAndSubParticipantsExclamationTooltip;

      default:
        return legalNode.notAllowedTabMessage;
    }
  }

  private setTabs(legalForm: COMPANY_LEGAL_FORM) {
    const legalChildren: { [key: string]: SitemapNode }
      = sitemap.system.children.misc.children.companies.children.id.children.legal.children;
    const sideTabs: IVerticalTabData[] = [
      { title: wording.system.legalEntity, path: legalChildren.legalEntity.path, isDefaultTab: true },
      { title: wording.system.representatives, path: legalChildren.representatives.path },
      { title: wording.system.shareholdersPartners, path: legalChildren.shareholdersPartners.path },
      { title: wording.system.trustors, path: legalChildren.trustors.path },
      { title: wording.system.documents, path: legalChildren.documents.path },
      { title: wording.accounting.metaTab, path: legalChildren.meta.path },
    ];

    if (legalForm !== COMPANY_LEGAL_FORM.ForeignCompany) {
      sideTabs.splice(4, 0, ...[
        { title: wording.system.subParticipants, path: legalChildren.subParticipants.path },
        { title: wording.system.silentPartners, path: legalChildren.silentPartners.path },
      ]);
    }
    this.sideTabs$.next(sideTabs);
  }

  private setLegalEntityExclamationWording(company: ICompany): void {
    this.legalEntityRepo.fetchOne(company.id, {}).pipe(
      switchMap(() => this.legalEntityRepo.getStream({ companyId: company.id }))
    ).subscribe(([legalEntity]) => {
      const contributionTypeName = legalEntityExclamationTooltipContributions
        .get(getContributionWordingKey(company.legalForm, legalEntity?.capitalType));

      this.legalEntityExclamationTooltip = setTranslationSubjects(
        wording.system.contributionDeviationDetected,
        { contributionTypeName },
      );
    });
  }

  private setLegalTabsExclamationIcons(company: ICompany): void {
    this.contributionMainService.showLegalTabsWarning(company)
      .subscribe(([showLegalEntityExclamationIcon, showTrusteeAndSubParticipantsExclamationIcons]) => {
        this.showLegalEntityExclamationIcon = showLegalEntityExclamationIcon;
        this.showTrusteeAndSubParticipantsExclamationIcons = showTrusteeAndSubParticipantsExclamationIcons;
      });
  }
}
