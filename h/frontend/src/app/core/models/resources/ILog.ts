import { IResource } from './IResource';
import { IUser } from './IUser';
import { IWording } from './IWording';

export interface ILog extends IResource<number> {
  message: IWording;
  date: Date;
  subject: string;
  user: IUser;
  unseen?: boolean;
  documentGuid?: string;
}

export interface ILogs {
  lastSeenDate?: string | number;
  items: ILog[];
  maxSize?: number;
}


export interface ILogCreate {
  message: IWording;
  subject: string;
  user?: IUser;
  documentGuid?: string;
}



