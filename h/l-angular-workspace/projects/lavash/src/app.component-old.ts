import {Component, Input, OnInit} from '@angular/core';
import {libStructure} from '../l-builder.structure';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
    libStructure = libStructure;


    // -----------Scrollbar
    // parent
    @Input() containerPxWidth = 400;
    @Input() containerPxHeight = 250;
    mouseInContainer: boolean;

    // content
    @Input() contentWidthPercentage = 120;

    contentTop = 0;
    _contentTop = 0; // is used in calc function DONT REMOVE
    contentLeft = 0;
    _contentLeft = 0; // is used in calc function DONT REMOVE


    // --scrollbarS
    _defaultScrollBar = 15;
    activeScrollBarY: boolean;
    activeScrollBarX: boolean;
    mouseInScrollbarY: boolean;
    mouseInScrollbarX: boolean;

    // vertical
    hasScrollVertical: boolean; // is used in calc function DONT REMOVE
    scrollHeight = this._defaultScrollBar;
    scrollTop = 0;
    _deltaHeight = 0; // is used in calc function DONT REMOVE

    // horizontal
    hasScrollHorizontal: boolean; // is used in calc function DONT REMOVE
    scrollWidth = this._defaultScrollBar;
    scrollLeft = 0;
    _deltaWidth = 0; // is used in calc function DONT REMOVE


    triggerMouseInContainerContainer( state: boolean, contentElement?: HTMLElement) {
      this.mouseInContainer = state;

      if (state) {
          this.defineIfHasScrollBar(contentElement, 'Vertical', 'Height');
          this.defineIfHasScrollBar(contentElement, 'Horizontal', 'Width');
      }
    }

    triggerMouseInScrollbar(delta: 'Y' | 'X', state: boolean) {
      this['mouseInScrollbar' + delta] = state;
    }

    defineIfHasScrollBar(contentElement: HTMLElement, direction: 'Vertical' | 'Horizontal',  wOrH: 'Height' | 'Width') {
        const unit = wOrH.toLocaleLowerCase();
        const heightOrWidth = contentElement.getBoundingClientRect()[unit];
        this['hasScroll' + direction] = heightOrWidth > this['containerPx' + wOrH];
        const scrollbarDimension = heightOrWidth / this['containerPx' + wOrH];

        this['scroll' + wOrH] = this._defaultScrollBar * scrollbarDimension > this['containerPx' + wOrH] ?
                                this._defaultScrollBar : Math.floor(this['containerPx' + wOrH] / scrollbarDimension);

        this['_delta' + wOrH] = this['containerPx' + wOrH] / heightOrWidth;

        // TODO alert dont remove commented code
        // just in case i don't undestand my own code
        /* const height = contentElement.getBoundingClientRect().height;
        this.hasVerticalScroll = height > this.containerPxHeight;
        const scrollbarDimension = height / this.containerPxHeight;

        this.scrollHeight = this._defaultScrollBar * scrollbarDimension > this.containerPxHeight ?
            this._defaultScrollBar : Math.floor(this.containerPxHeight / scrollbarDimension);

        this._deltaHeight = this.containerPxHeight / height; */
    }

    onWheel(event, contentElement) {
        event.preventDefault();
        this.calcContentAndScrollbarsPositionAndSize(contentElement, event, 'Height', 'Top', 'Y');
        this.calcContentAndScrollbarsPositionAndSize(contentElement, event, 'Width', 'Left', 'X');

    }

    calcContentAndScrollbarsPositionAndSize(contentElement: HTMLElement,
                                            event: MouseWheelEvent,
                                            wOrH: 'Height' | 'Width',
                                            topOrLeft: 'Top' | 'Left',
                                            delta: 'Y' | 'X') {
        const measureName = wOrH.toLocaleLowerCase();
        const contentElementMeasureSide = contentElement.getBoundingClientRect()[measureName]; // width or heigh

        if ( contentElementMeasureSide > this['containerPx' + wOrH] ) {
            this['_content' + topOrLeft] += event['delta' + delta];

            const diff = contentElementMeasureSide - this['containerPx' + wOrH];
            this['_content' + topOrLeft] = this['_content' + topOrLeft] < 0 ? 0 :
                                           this['_content' + topOrLeft] > diff ? diff :
                                           this['_content' + topOrLeft];

            // --calculate content position from top/left
            this['content' + topOrLeft] = -(this['_content' + topOrLeft]);

            // --calculate scrollbar position from top/left

            // to calc diff we actually need to sum it as it is negative number
            // or. elementi pastaci height-i u depi verev gnacac masi tarberutyuny
            const diffBetweencontentPositionAndMeasureSide = contentElementMeasureSide + this['content' + topOrLeft];
            // haraberakcutyun
            const deltaRangeForScrollBar = diffBetweencontentPositionAndMeasureSide / contentElementMeasureSide;
            const scrollbarDimension = diffBetweencontentPositionAndMeasureSide / this['containerPx' + wOrH];

            const scrollposition = deltaRangeForScrollBar === this['_delta' + wOrH] ? this['containerPx' + wOrH] - this['scroll' + wOrH] :
                                   deltaRangeForScrollBar === 1 ? 0 :
                                   Math.floor(this['containerPx' + wOrH] / scrollbarDimension) - this['scroll' + wOrH];

            this['scroll' + topOrLeft] = scrollposition;

        }

        // TODO alert dont remove commented code
        // just in case i don't undestand my own code
        // it is the same as generic function does
        // just in case i dont understand this fuckin code
        /*const contentElementHeight = contentElement.getBoundingClientRect().height;

        if (contentElementHeight > this.containerHeightPx ) {

            this._contentTop += event.deltaY;

            this._contentTop = this._contentTop < 0 ? 0 :
                this._contentTop > contentElementHeight - this.containerHeightPx ? contentElementHeight - this.containerHeightPx :
                    this._contentTop;

            this.contentTop = -(this._contentTop);
        }*/
    }

    onMousedownScrollbar(event, contentElement, containerElement, delta) {
        this['activeScrollBar' + delta] = true;
        this.triggerMouseInScrollbar(delta, true);
    }

    onMouseupOnDom(event, contentElement, containerElement) {
        event.e.preventDefault();
        if (event.e.type === 'mousemove') {
            if (this.activeScrollBarY ) {
                this.onMouseHoldAndMove(event.e, contentElement, containerElement, 'Height', 'Top', 'Y');
            }

            if (this.activeScrollBarX) {
                this.onMouseHoldAndMove(event.e, contentElement, containerElement, 'Width', 'Left', 'X');
            }
        }

        if ( event.e.type === 'click') {
            this.activeScrollBarX = false;
            this.activeScrollBarY = false;
            this.mouseInScrollbarY = false;
            this.mouseInScrollbarX = false;

            this._contentTop = -this.contentTop;
        }
    }

    onMouseHoldAndMove(event, contentElement, containerElement, wOrH: 'Height' | 'Width', topOrLeft: 'Top' | 'Left', delta: 'Y' | 'X') {
        const unit_HorW = wOrH.toLocaleLowerCase();
        const unit_topOrLeft = topOrLeft.toLocaleLowerCase();

        // CONTENT
        const contentEl_HorW = contentElement.getBoundingClientRect()[unit_HorW];


        // CONTAINER
        const containerEl_topOrLeft = containerElement.getBoundingClientRect()[unit_topOrLeft];

        // coordinates of mouse on window
        const window_topOrLeft =  event['client' + delta] - 8 - containerEl_topOrLeft; // 7-8 tivy mijinacvac shexumna, erevi


        // content
        const _delta = (window_topOrLeft / this['containerPx' + wOrH]) * contentEl_HorW;
        const content = window_topOrLeft < 0 ? 0 :
                        window_topOrLeft > this['containerPx' + wOrH] ? (contentEl_HorW - this['containerPx' + wOrH]) :
                        _delta >= contentEl_HorW - this['containerPx' + wOrH] ?
                        (contentEl_HorW - this['containerPx' + wOrH]) :
                        ((window_topOrLeft / this['containerPx' + wOrH]) * contentEl_HorW);

        this['content' + topOrLeft] = -(content);

        // scrollBar
        const diffOfScrollerAndContainer =  this['containerPx' + wOrH] - this['scroll' + wOrH];
        this['scroll' + topOrLeft] = window_topOrLeft <= 0 ? 0 :
                                     window_topOrLeft >= diffOfScrollerAndContainer ? diffOfScrollerAndContainer :
                                     window_topOrLeft;
    }

    onScrollbarClick(event, contentElement, containerElement) {
        if (this.activeScrollBarY ) {
            this.onMouseHoldAndMove(event, contentElement, containerElement, 'Height', 'Top', 'Y');
        }

        if (this.activeScrollBarX) {
            this.onMouseHoldAndMove(event, contentElement, containerElement, 'Width', 'Left', 'X');
        }
    }
}
