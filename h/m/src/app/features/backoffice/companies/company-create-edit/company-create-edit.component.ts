import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { SubSink } from 'subsink';
import { MatHorizontalStepper } from '@angular/material/stepper';
import { TranslateService } from '@ngx-translate/core';

import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { GlobalService } from '@core-services/global.service';
import { InputAttributeService } from '@shared/services/input-attribute.service';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { BackOfficeService } from '@features/backoffice/backoffice.web-service';

import { DomainModel } from '@shared/models/domain/domain.model';
import { AttributeModelTypes, DMChoiceAttributeModel } from '@shared/models/attribute/attribute.model';
import { LayoutGroupModel } from '@shared/models/layout/layout-group.model';
import { LayoutModel } from '@shared/models/layout/layout.model';
import { DigitalProfileModel } from '@shared/models/digital-profile/digital-profile.model';
import { MeButtonI } from '@shared/me-components/me-button/me-button.interface';
import { EntityI } from '@core-interfaces/entity.interface';
import { ArrayRequestI } from '@core-interfaces/array-request.interface';
import { KeyValueI } from '@core-interfaces/key-value.interface';
import { ME_DOMAIN_APPLICATION_ADDRESS, ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL } from '@shared/constants';
import { MeAttributeLayoutTypes } from '@shared/me-attribute-layout-types';
import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';

@Component({
  selector: 'me-company-create-edit',
  templateUrl: './company-create-edit.component.html',
  styleUrls: ['./company-create-edit.component.scss'],
  providers: [InputAttributeService]
})
export class CompanyCreateEditComponent implements OnInit, OnDestroy {
  private subs = new SubSink();
  private companyId: string | null = '';
  company!: DigitalProfileModel;
  buttonFinish!: { label: string; data: MeButtonI };
  stepOneGroups: string[] = ['companyOverview', 'businessInformation', 'address', 'contactInformation'];
  stepTwoGroups: string[] = ['generalAttributes']; // TODO: unused one
  layouts: LayoutModel[] = []; //TODO: unused copy
  groups: LayoutGroupModel[] = [];
  attributes: AttributeModelTypes[] = [];
  companydomain: DomainModel | undefined;
  addressdomain: DomainModel | undefined;
  companylayouts: LayoutModel[] = []; //TODO: unused copy
  companyAttributes: AttributeModelTypes[] = [];
  companyInfoLayoutGroups!: LayoutGroupModel[];
  addresslayouts: LayoutModel[] = []; //TODO: unused copy
  addressAttributes: AttributeModelTypes[] = [];
  /*certificateInfoLayoutGroups!: LayoutGroupModel[];
  extendedAttributes: AttributeModelTypes[] = [];*/ //TODO: unused copy

  companyInfoAttributeForm!: FormGroup;
  comapanyInfoAttributeItemMap!: Map<string, MeAttributeItem>;
  certificateInfoAttributeForm!: FormGroup;
  certificateInfoAttributeItemMap!: Map<string, MeAttributeItem>;

  materialTypesAttribute: DMChoiceAttributeModel = new DMChoiceAttributeModel();

  i18nStrings: { [key: string]: string } = {
    company: '',
    successfullyUpdated: '',
    companyInfo: '',
    insertRequiredData: '',
    requiredFields: ''
  };

  @ViewChild('stepper') stepper!: MatHorizontalStepper; //TODO: unused copy

  constructor(
    private globalService: GlobalService,
    private definitionsStoreService: DefinitionsStoreService,
    private inputAttributeService: InputAttributeService,
    private backOfficeService: BackOfficeService,
    private basicAlertService: MeBasicAlertService,
    private translate: TranslateService,
    private _router: Router,
    private _activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.companyId = this._activeRoute.snapshot.paramMap.get('id');

    this.buttonFinish = { label: 'Finish', data: { type: 'regular', color: 'primary' } };
    this.globalService.activateLoader();

    this.subs.sink = this.translate.get(Object.keys(this.i18nStrings))
      .subscribe((translations) => {
        this.i18nStrings = translations;
    });

    this.subs.sink = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      this.companydomain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL);
      this.addressdomain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_ADDRESS);
      console.log('addressdomain', this.addressdomain)

      if (dataLoaded) {
        this.companyId
          ? this.initializeEdit()
          : this.initializeCreate();
        
      }
    });
  }

  initializeEdit() {
    this.subs.sink = this.backOfficeService.getBoCompany(encodeURI(this.companyId as string)).subscribe((response) => {
      this.company = response;
      this.genereateFormByDomainAttributes();
    });
  }

  initializeCreate(): void {
    this.genereateFormByDomainAttributes();
  }

  genereateFormByDomainAttributes(): void {
    let layoutTag = this.companyId ? MeAttributeLayoutTypes.UPDATE : MeAttributeLayoutTypes.CREATE;
    let companygroups: LayoutGroupModel[] = [];
    let addressgroups: LayoutGroupModel[] = [];

    if (this.companydomain) { //TODO veracnel es if-ery
      this.companyAttributes = this.inputAttributeService.transformAttributesArray(this.companydomain.attributes, true);
      console.log(`this.companyAttributes`, this.companyAttributes);
      this.companylayouts = this.companydomain.layouts;
      companygroups = this.inputAttributeService.getLayoutGroups(this.companydomain.layouts, layoutTag);
    }

    if (this.addressdomain) { //TODO veracnel es if-ery
      this.addressAttributes = this.inputAttributeService.transformAttributesArray(this.addressdomain.attributes);
      this.addresslayouts = this.addressdomain.layouts;
      addressgroups = this.inputAttributeService.getLayoutGroups(this.addressdomain.layouts, layoutTag);
    }
    

    this.attributes = [
      ...this.companyAttributes,
      ...this.addressAttributes
    ];
    
   /* this.layouts = [
      ...this.companylayouts,
      ...this.addresslayouts
    ];

    console.log('layouts', this.layouts)*/  //TODO: unused copy

    if (this.company) { // edit mode
      this.inputAttributeService.applyUpdateFormValues(this.attributes, this.company);
    }

    this.groups = companygroups.concat(addressgroups);
    this.groups = this.inputAttributeService.generateLayoutRows(this.groups);

    console.log('this.groups', this.groups)

    this.companyInfoLayoutGroups = this.getLayoutForGroups(this.groups, this.stepOneGroups);

    console.log('this.companyInfoLayoutGroups', this.companyInfoLayoutGroups)
    
    if (this.companyInfoLayoutGroups && this.companyInfoLayoutGroups.length > 0) {
      this.companyInfoAttributeForm = this.inputAttributeService.createFormGroup(
        this.getAttributesByLayoutGroups(this.attributes, this.companyInfoLayoutGroups)
      );

      this.comapanyInfoAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
        this.getAttributesByLayoutGroups(this.attributes, this.companyInfoLayoutGroups),
        this.companyInfoAttributeForm
      );
    }

    this.companyInfoAttributeForm.get('address')?.clearValidators(); // TODO ??
    this.globalService.deactivateLoader();
  }

  getLayoutForGroups(groups: LayoutGroupModel[], layoutGroupNames: string[]): LayoutGroupModel[] {
    const layoutGroups = groups.filter((group) => layoutGroupNames.findIndex((name) => group.internalName === name) > -1);
    return layoutGroups;
  }

  finishStep() {
    this.companyInfoAttributeForm.markAllAsTouched();

    if (!this.companyInfoAttributeForm.valid) {
      return;
    }

    const companyChanges: ArrayRequestI<EntityI> = {
      entities: []
    };
    const companyEntity = {
      domainName: ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL,
      attributes: {}
    };
    const addressEntity = {
      domainName: ME_DOMAIN_APPLICATION_ADDRESS,
      attributes: {}
    };


    // TODO: haskanal incha anum es ktory
    this.setAttributesToEntity(addressEntity.attributes, this.addressAttributes);
    this.companyId
      ? this.setAttributesToEntity(companyEntity.attributes, this.companyAttributes, ['name'])
      : this.setAttributesToEntity(companyEntity.attributes, this.companyAttributes);
    


    companyChanges.entities = [
      ...companyChanges.entities, //TODO: understad if needed
      companyEntity,
      addressEntity
    ];

    console.log('final shit', companyChanges)
    return;

    this.companyId
      ? this.updateBoCompany(companyChanges)
      : this.createBoCompany(companyChanges);
  }

  private updateBoCompany(companyChanges: any): void { // TODO type
    this.subs.sink = this.backOfficeService.updateBoCompany(this.companyId as string, companyChanges).subscribe(
      () => {
        this.basicAlertService.openBasicAlert({
          mode: 'success',
          title: this.i18nStrings.company,
          content: this.i18nStrings.successfullyUpdated
        });
        this._router.navigate([`backoffice/companies/view/${this.companyId}`]);
      },
      (error) => {
        this.basicAlertService.openBasicAlert({
          mode: 'error',
          title: error.error.title,
          content: error.error.details
        });
      }
    );
  }

  private createBoCompany(companyChanges: any): void { // TODO type
    this.subs.sink = this.backOfficeService.createBoCompany(companyChanges).subscribe(
      () => {
        this.basicAlertService.openBasicAlert({
          mode: 'success',
          title: this.i18nStrings.company,
          content: 'Successfully created'
        });
        this._router.navigate([`backoffice/companies`]);
      },
      (error) => {
        this.basicAlertService.openBasicAlert({
          mode: 'error',
          title: error.error.title,
          content: error.error.details
        });
      }
    );
  }

  private crudHelper(
    
  ): void { // todo make global helper and pick better function name

  }

  private setAttributesToEntity(entity: {}, formattributes: AttributeModelTypes[], imutablecolumn?: string[]): void {
    this.getAttributesByLayoutGroups(this.attributes, this.companyInfoLayoutGroups)
      .forEach((attribute) => {
        this.setAttributeWithValueToEntity(entity, attribute, this.companyInfoAttributeForm, formattributes, imutablecolumn);
      });
  }

  private setAttributeWithValueToEntity(
    // tslint:disable-next-line:no-any
    entity: KeyValueI<any>,
    attribute: AttributeModelTypes,
    formGroup: FormGroup,
    attr: AttributeModelTypes[],
    imutablecolumn?: string[]
  ): void {
    if (attr.indexOf(attribute) > -1) {
      const hasImmutable = imutablecolumn ? imutablecolumn.filter((x) => x === attribute.internalName).length > 0 : false;
      if (!hasImmutable) {
        if (
          formGroup.controls.hasOwnProperty(attribute.internalName) &&
          formGroup.controls[attribute.internalName].hasOwnProperty('value') &&
          formGroup.controls[attribute.internalName].value !== null
        ) {
          entity[attribute.internalName] = formGroup.controls[attribute.internalName].value;
        }
      }
    }
  }

  private getAttributesByLayoutGroups(attributes: AttributeModelTypes[], layoutGroups: LayoutGroupModel[]): AttributeModelTypes[] {
    const attributeMap: Map<string, AttributeModelTypes> = new Map();
    attributes.forEach((attribute) => {
      attributeMap.set(attribute.internalName, attribute);
    });

    const x = layoutGroups.reduce((prev, curr) => {
      return [
        ...prev,
        ...curr.attributes.map((attr) => {
          return attributeMap.get(attr.internalName);
        })
      ];
      // tslint:disable-next-line:no-any
    }, [] as any);

    console.log('xxxxxx', x)

    return x;
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}