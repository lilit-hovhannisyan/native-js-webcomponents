import { CompanyRepresentativeKey } from '../../models/resources/ICompanyRepresentative';
import { Injectable } from '@angular/core';
import { ICompanyRepresentative, ICompanyRepresentativeDTO, ICompanyRepresentativeCreate } from '../../models/resources/ICompanyRepresentative';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyRepresentativeRepositoryService extends AbstractRepository<
  ICompanyRepresentative,
  ICompanyRepresentativeDTO,
  ICompanyRepresentativeCreate,
  RepoParams<ICompanyRepresentative>,
  CompanyRepresentativeKey
>  {
  protected resourceType = 'CompanyRepresentative';
  public sortAsce = true;
  public sortKey = 'id';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/company-representatives';
  }

  protected postProcess(item: ICompanyRepresentativeDTO): ICompanyRepresentative {
    return item;
  }
}
