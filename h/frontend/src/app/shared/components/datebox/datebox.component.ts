import { AfterViewInit, Component, EventEmitter, OnInit, HostListener, ElementRef, Input, ViewChild, Output, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { StringHelper } from 'src/app/core/classes/StringHelper';
import { dateFormatTypes, timeFormatTypes, NvLocale, nzDatePickerDateFormatTypes } from 'src/app/core/models/dateFormat';
import { wording } from 'src/app/core/constants/wording/wording';
import { getDateFormat } from '../../helpers/general';
import { AbstractValueAccessor, makeProvider } from 'src/app/core/abstracts/value-accessor';
import { SettingsService } from 'src/app/core/services/settings.service';
import * as moment from 'moment';
import * as momentTimezone from 'moment-timezone';
import { IWording } from 'src/app/core/models/resources/IWording';

export type DisabledDate = (currentDate: Date) => boolean;

/**
 * This component extends nz-zorro's date-picker, to give users opportunity to write date
 * as numbers inside input(i.e. 121113 will be parsed to 12.11.2013) and also choose date
 * from calendar. It also extends ControlValueAccessor so it can interact with forms as
 * FormControl. You can see additional information about this here
 * @see https://navido.atlassian.net/browse/NAVIDO-338
 */
@Component({
  selector: 'nv-datebox',
  templateUrl: './datebox.component.html',
  styleUrls: ['./datebox.component.scss'],
  providers: [makeProvider(DateboxComponent)],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class DateboxComponent extends AbstractValueAccessor implements OnInit, AfterViewInit {
  @Input() public withTime: boolean;
  @Input() public withWeekDay: boolean;
  @Input() public _dateString: string;
  @Input() public autofocus = false;
  @Input() public propagateInvalidDate = true;
  @Input() public set dateString(value: string) {
    if (value !== undefined) {
      this._dateString = value;
      this.writeValueForDateString(this._dateString);
    }
  }
  @Input() public set disabled(value: boolean) {
    this.setDisabledState(value);
  }
  @Input() public inputFieldStyle = {};
  @Input() public nzSize = 'default';

  @Input() public disabledFrom?: string;
  @Input() public disabledUntil?: string;
  @Input() public show24thHour = false;
  @Input() public placeholder: IWording;

  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();
  @Output() public focus = new EventEmitter<FocusEvent>();
  @Output() public valueChange = new EventEmitter<string>();

  @ViewChild('input', { static: true }) private input: ElementRef<any>;

  /**
   * Nz zoro's datepicker value - it accepts only native Date object or null
   */
  public date?: Date = null;

  /**
   * Date input's value, which is also displays formatted current date as string inside input
   */
  public dateInput = '';
  public isCalendarOpen = false;
  public weekDay = '';

  /**
   * Date format to which all dates will be converted
   */
  public dateFormat: string;
  public timeFormat: string;

  /**
   * Time format to which all times will be converted
   */
  public dateTimeFormat: string;
  public validationErrorMessage = wording.general.invalidDate;
  public isDateInValid = false;
  public isDisabled: boolean;
  public locale: NvLocale;

  /**
   * nz date format differs from moment.js date format, that's why we have this
   */
  public nzDatePickerFormat: string;

  /**
   * Possible time formats that we can get from input
   */
  public timeInputFormats: string[] = [];
  /**
   * Used to check if value changed after touch to make formControl dirty or pristine
   * correctly
   */
  public previousValue: string;
  public clickedOnCalendar = false;

  public disabledDateFn: DisabledDate = (currentDate: Date): boolean => {
    const current = moment(currentDate);
    const from = moment(this.disabledFrom, moment.ISO_8601);
    const until = moment(this.disabledUntil, moment.ISO_8601);
    const isDisabled: boolean = current.isBefore(until, 'day') || current.isAfter(from, 'day');
    return isDisabled;
  }


  constructor(
    private elementRef: ElementRef,
    private changeDetectorRef: ChangeDetectorRef,
    private settingsService: SettingsService,
  ) {
    super();
  }

  public ngOnInit(): void {
    this.locale = this.settingsService.locale;
    this.dateFormat = getDateFormat(this.locale);
    this.timeFormat = timeFormatTypes[this.locale] || timeFormatTypes[NvLocale.DE];
    this.dateTimeFormat = `${this.dateFormat} ${this.timeFormat}`;
    this.nzDatePickerFormat = this.withTime
      ? `${nzDatePickerDateFormatTypes[this.locale]} ${this.timeFormat}`
      : nzDatePickerDateFormatTypes[this.locale];

    moment.locale(this.locale.toString());
    this.timeInputFormats = [
      'h',
      'hm',
      'hmm',
      'Hmm',
      'h A',
      'hm A',
      'hmm A',
      'Hmm A',
      this.timeFormat
    ];
    // will turn every 2 digit year to 2000's year
    // used 'as any' to avoid 'cannot assign readonly property' lint warning,
    // because it is not actually readonly
    // TODO: check if this can be injected globally
    (moment as any).parseTwoDigitYear = yearString => {
      return +yearString + 2000;
    };
  }

  public ngAfterViewInit() {
    if (this.autofocus) {
      setTimeout(() => {
        this.input.nativeElement.focus();
        this.input.nativeElement.select();
      }, 0);
    }
  }

  public writeValue(value: any) {
    this._value = value;

    if (!value) {
      this.date = null;
      this.dateInput = '';
      this.previousValue = '';
      return;
    }

    this.dateInput = value;
    this.parseEnteredDateString(value);
  }
  /**
   * Parses the date string for the input and date picker
   */
  public parseEnteredDateString(date: string | Date): void {
    const momentDate = this.toMomentDate(date);
    if (this.validateMomentDate(momentDate)) {
      this.dateInput = this.formatDate(momentDate);
      this.previousValue = this.dateInput;
      // as nz-date-picker parses dates in user's local timezone format, before passing
      // date to it we keep it's value the same but change only timezone from utc to
      // the user's current timezone, so there wouldn't be any difference
      this.date = momentDate.tz(momentTimezone.tz.guess(), true).toDate();
      this.withWeekDay && this.changeWeekDayName();
    }
  }

  /**
   * Gets called when the input looses focus, and parses the entered date string
   */
  public handleBlur(event: FocusEvent): void {
    if (this.isValueChanged()) {
      // if input is not empty after blur
      this.parseEnteredDateString(this.dateInput);
      this.parseToUTCAndPropagate();
    } else if (this.withWeekDay) {
      this.weekDay = '';
      this.parseToUTCAndPropagate();
    }

    if (!this.isCalendarOpen) {
      this.blur.emit(event);
    }
  }

  /** Gets called when the input's being focused. */
  public handleFocus(event: FocusEvent): void {
    this.focus.emit(event);
  }

  /* Close Datepicker after clicking on Calendar */
  public closeIfCalendarClicked() {
    if (this.clickedOnCalendar) {
      this.clickedOnCalendar = false;
      this.hideCalendar();
    }
  }

  /**
   * Gets called when calendar date changes(change happens when date fully choosed i.e year,
   * month, date and time if time is required too)
   */
  public onDateChange(date: string): void {
    const momentDate = this.toMomentDate(moment(date).utc(true).toISOString());
    if (this.validateMomentDate(momentDate)) {
      this.dateInput = this.formatDate(momentDate);
      this.withWeekDay && this.changeWeekDayName();
    }
    this.parseToUTCAndPropagate();
    this.input.nativeElement.focus();
  }

  public changeWeekDayName(): void {
    this.weekDay = moment.weekdaysMin(this.toMomentDate(this.dateInput).isoWeekday());
  }

  public toMomentDate(date: string | Date): moment.Moment {
    const dateTimeFormats = [...this.getDateTimeInputFormats(date), moment.ISO_8601];
    const dateFormats = [this.getDateInputFormat(date), moment.ISO_8601];

    // check if space exists and it is not at the end then convert numbers after it as time
    if (this.withTime && typeof date === 'string' && date.includes(' ') && !date.endsWith(' ')) {
      const parts = date.split(' ');
      const datePart = parts.shift();
      const timePart = parts.join(' ');
      // if user typed 24:00 in the datebox which doesn't allow it, we change it to 00:00
      // which means day should be changed to the next day (discussed with Levon Ibranyan)
      const momentDate = timePart.startsWith('24') && !this.show24thHour
        ? moment(datePart, dateFormats).add(1, 'days').utc(true)
        : moment(datePart, dateFormats).utc(true);
      const timeFormat = moment(timePart, this.timeInputFormats).utc(true);
      return momentDate.set({
        hour: timeFormat.get('hour'),
        minute: timeFormat.get('minute'),
      });
    }

    return this.withTime
      ? moment.utc(date, dateTimeFormats).utc(true)
      : moment.utc(date, dateFormats).utc(true);
  }

  public validateMomentDate(date: moment.Moment): boolean {
    if (date.isValid()) {
      // validation for displaying error in html
      if (this.isDateInValid) {
        this.isDateInValid = false;
      }
      return true;
    } else {
      /*
      * Validation for displaying error in html
      * The creationData().input returns entered data.
      * If it's empty, it means that date.isValid() retured false due to that
      * so no need to show error
      */
      this.isDateInValid = date.creationData().input !== '';
      return false;
    }
  }

  public formatDate(date: moment.Moment, parseToISO = false): string {
    const formattedDate = date.format(this.getDateOutputFormat());
    const isThereMidnightTime = formattedDate.includes('00:00');
    // when hours should be displayed as 24:00 instead of 00:00
    if (
      this.show24thHour
      && this.locale === NvLocale.DE
      && isThereMidnightTime && !parseToISO
    ) {
      const dateFormats = [this.getDateInputFormat(this.dateInput), moment.ISO_8601];
      const typedDay = moment(this.dateInput, [...dateFormats, moment.ISO_8601]).get('date');
      const formattedDay = date.get('date');
      // for some strange reason moment sometimes adds additional day when time typed
      // as 24:00, that's why we check what day user typed and to what day it was
      // formatted, if days don't match it means moment() changed typed day and added an
      // additional day, that's why we subtract a day.
      return typedDay === formattedDay
        ? formattedDate.replace('00:00', '24:00')
        : date.subtract(1, 'days')
          .format(this.getDateOutputFormat())
          .replace('00:00', '24:00');
    }

    return parseToISO ? date.toISOString() : formattedDate;
  }

  public getDateOutputFormat(): string {
    return this.withTime ? this.dateTimeFormat : this.dateFormat;
  }

  /**
   * Returns possible dateTime formats that we can get from input
   */
  public getDateTimeInputFormats(date: string | Date | null = null): string[] {
    const dateTimeFormat = [];
    const dateInputFormat = this.getDateInputFormat(date);
    this.timeInputFormats.forEach(t => {
      dateTimeFormat.push(`${dateInputFormat} ${t}`);
    });

    return dateTimeFormat;
  }

  /**
   * Possible date format with 2 digit or 4 digit year that we can get from input
   */
  public getDateInputFormat(date: string | Date): string {
    if (typeof date === 'string' && this.isYYFormatNeeded(date)) {
      return dateFormatTypes[this.locale].slice(0, -2);
    } else {
      return dateFormatTypes[this.locale];
    }
  }

  /**
   * Checks if date needs to be parsed with `YY` year format, if date was written with
   * 2 digit year and full time(i.e.1112191930) or date was written with one or three
   * digit years(`YYYY` format does not work for one and three digit years)
   */
  public isYYFormatNeeded(date: string): boolean {
    const trimmedDate = StringHelper.trimNoneNumericChars(date);
    const isTwoDigitYearAndTime = trimmedDate.length > 8
      && trimmedDate.length < 11
      && this.withTime;
    const isOneDigitYear = trimmedDate.length === 5;
    const isThreeDigitYear = trimmedDate.length === 7;

    return isTwoDigitYearAndTime || isOneDigitYear || isThreeDigitYear;
  }

  public showCalendar(): void {
    this.isCalendarOpen = true;
    this.changeDetectorRef.detectChanges();
  }

  public hideCalendar(): void {
    this.isCalendarOpen = false;
    this.changeDetectorRef.detectChanges();
  }

  /**
   * Since we open and close the calendar manually on click event inside this component,
   * this checks for clicks outside of the calendar when it is opened,  and
   * closes it, if there is.
   */
  @HostListener('document:mousedown', ['$event'])
  public clickOutside(event: MouseEvent): void {
    if (this.isCalendarOpen) {
      const calendars: HTMLInputElement[] = Array.from(document.querySelectorAll('.datebox-overlay'));

      const clickedOnComponent = this.elementRef.nativeElement.contains(event.target);
      const clickedOnCalendar = calendars.some(calendar => calendar.contains(event.target as Node));
      if (clickedOnCalendar) {
        this.clickedOnCalendar = true;
      }

      if (!clickedOnComponent && !clickedOnCalendar) {
        // clicked outside calendar
        this.hideCalendar();
      }


    } return null;

  }

  /**
   * Parses current date display format to ISO string and propagates change
   */
  public parseToUTCAndPropagate(): void {
    if (this.isDateInValid) {
      if (this.onChange) {
        this.onChange(this.propagateInvalidDate ? this.dateInput : undefined);
        // added to detect touched statatus correctly
        this.onTouched(this.propagateInvalidDate ? this.dateInput : undefined);
      }
      this.valueChange.emit(this.propagateInvalidDate ? this.dateInput : undefined);
    } else {
      const ISODate = this.formatDate(this.toMomentDate(this.dateInput), true);
      if (this.onChange) {
        this.onChange(ISODate);
        // added to detect touched statatus correctly
        this.onTouched(ISODate);
      }
      this.valueChange.emit(ISODate);
    }
  }

  /**
   * Writes a new value from the @Input dateString if it is passed
   */
  public writeValueForDateString(value: string): void {
    if (!value) {
      this.date = null;
      this.dateInput = '';
      this.previousValue = '';
      return;
    }

    const momentDate = moment.utc(value, moment.ISO_8601);

    if (this.validateMomentDate(momentDate)) {
      this.dateInput = value;
      const parsedISODate = momentDate.format(this.getDateOutputFormat());
      this.parseEnteredDateString(parsedISODate);
    } else if (this.propagateInvalidDate) {
      this.dateInput = value;
      this.previousValue = this.dateInput;
      this.date = new Date();
    }
  }

  /**
   * Disables date input and calendar button as FormControl disabled state
   */
  public setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  public handleTab(event: KeyboardEvent) {
    if (this.isValueChanged()) {
      this.parseEnteredDateString(this.dateInput);
      this.parseToUTCAndPropagate();
    }
    this.tab.emit(event);
  }

  public handleEnter(event: KeyboardEvent) {
    if (this.isValueChanged()) {
      this.parseEnteredDateString(this.dateInput);
      this.parseToUTCAndPropagate();
    }
    this.enter.emit(event);
    event.preventDefault();
  }

  public handleEscape(event: any) {
    this.escape.emit(event);
  }

  public isValueChanged() {
    return !this.previousValue || this.dateInput !== this.previousValue;
  }

  public preventNotAllowedCharacters(e: KeyboardEvent): boolean {
    return !(
      e.key === '/' && this.locale === NvLocale.DE
      || e.key === '.' && this.locale !== NvLocale.DE
    );
  }
}
