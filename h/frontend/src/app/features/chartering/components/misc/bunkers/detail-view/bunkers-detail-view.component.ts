import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { IBunkerCreate, IBunker } from 'src/app/core/models/resources/IBunker';
import { BunkerRepositoryService } from 'src/app/core/services/repositories/bunker-repository.service';

@Component({
  templateUrl: './bunkers-detail-view.component.html',
  styleUrls: ['./bunkers-detail-view.component.scss'],
})
export class BunkersDetailViewComponent
  extends ResourceDetailView<IBunker, IBunkerCreate>
  implements OnInit {

  public repoParams = {};
  public routeBackUrl = url(sitemap.chartering.children.misc.children.bunkers);

  protected resourceDefault = {} as IBunker;

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    bunkerRepo: BunkerRepositoryService,
    baseComponentService: BaseComponentService,
  ) {
    super(bunkerRepo, baseComponentService);
  }

  public ngOnInit() {
    this.init();
    const { id } = this.route.snapshot.params;
    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => this.resource[this.labelKey];

  public init(bunker: IBunker = this.resourceDefault) {
    this.initForm(
      this.fb.group({
        labelEn: [bunker.labelEn, [Validators.required, Validators.maxLength(50)]],
        labelDe: [bunker.labelDe, [Validators.required, Validators.maxLength(50)]],
        code: [bunker.code, [Validators.required, Validators.maxLength(15)]],
        isActive: [bunker.isActive, []],
      }));
  }

  public submit() {
    if (!this.form.valid) { return; }

    const { value } = this.form;
    const bunker = this.creationMode
      ? { ...value }
      : { ...this.resource, ...value };

    this.creationMode
      ? this.createResource(bunker)
      : this.updateResource(bunker);
  }
}
