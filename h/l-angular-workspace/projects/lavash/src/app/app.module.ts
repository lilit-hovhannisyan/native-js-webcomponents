import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {LavashAtomsModule, LavashMoleculesModule, LavashOrganismsModule} from 'l-builder';
import {RouterModule} from '@angular/router';

import {ButtonComponent} from './pages/atoms/button/button.component';
import {InputComponent} from './pages/atoms/input/input.component';
import {ImageComponent} from '../../../l-builder/src/lib/atoms/image/image.component';
import {IconComponent} from './pages/atoms/icon/icon.component';
import {CheckboxComponent} from './pages/atoms/checkbox/checkbox.component';
import {CheckboxListComponent} from './pages/atoms/checkbox-list/checkbox-list.component';
import {RadioButtonComponent} from './pages/atoms/radio-button/radio-button.component';
import {RadioButtonListComponent} from './pages/atoms/radio-button-list/radio-button-list.component';
import {MsgComponent} from './pages/atoms/msg/msg.component';
import {DatepickerComponent} from './pages/atoms/datepicker/datepicker.component';
import {FileUploadComponent} from './pages/atoms/file-upload/file-upload.component';
import {FileUploadResultsComponent} from './pages/atoms/file-upload-results/file-upload-results.component';
import {SelectAutocompleteComponent} from './pages/atoms/select-autocomplete/select-autocomplete.component';
import {LinkComponent} from './pages/atoms/link/link.component';
import {TooltipComponent} from './pages/atoms/tooltip/tooltip.component';
import {PaginationComponent} from './pages/atoms/pagination/pagination.component';
import {TextComponent} from './pages/atoms/text/text.component';
import {TextBlockComponent} from './pages/atoms/text-block/text-block.component';
import {ContainerComponent} from './pages/atoms/container/container.component';
import {EventContainerComponent} from './pages/atoms/event-container/event-container.component';
import {TextareaComponent} from './pages/atoms/textarea/textarea.component';
import {SlideBarComponent} from './pages/atoms/slide-bar/slide-bar.component';
import {LoginComponent} from './pages/molecules/login/login.component';
import {FormFieldComponent} from './pages/molecules/form-field/form-field.component';
import {AudioComponent} from './pages/molecules/audio/audio.component';
import {VideoComponent} from './pages/molecules/video/video.component';
import {ImgSliderComponent} from './pages/molecules/img-slider/img-slider.component';
import {ArticleCardComponent} from './pages/molecules/article-card/article-card.component';
import {ScrollbarContainerComponent} from './pages/molecules/scrollbar-container/scrollbar-container.component';
import {NavigationComponent} from './pages/molecules/navigation/navigation.component';
import {SidebarComponent} from './pages/molecules/sidebar/sidebar.component';
import {PopupComponent} from './pages/molecules/popup/popup.component';
import {FormBuilderComponent} from './pages/organisms/form-builder/form-builder.component';
import {GridContainerComponent} from './pages/organisms/grid-container/grid-container.component';



const appRoutes = [
    {path: '', redirectTo: 'button', pathMatch: 'full'},
    {path: 'button', component: ButtonComponent},
    {path: 'input', component: InputComponent},
    {path: 'image', component: ImageComponent},
    {path: 'icon', component: IconComponent},
    {path: 'checkbox', component: CheckboxComponent},
    {path: 'checkboxlist', component: CheckboxListComponent},
    {path: 'radiobutton', component: RadioButtonComponent},
    {path: 'radiobuttonlist', component: RadioButtonListComponent},
    {path: 'msg', component: MsgComponent},
    {path: 'datepicker', component: DatepickerComponent},
    {path: 'fileupload', component: FileUploadComponent},
    {path: 'fileuploadresults', component: FileUploadResultsComponent},
    {path: 'selectautocomplete', component: SelectAutocompleteComponent},
    {path: 'link', component: LinkComponent},
    {path: 'tooltip', component: TooltipComponent},
    {path: 'pagination', component: PaginationComponent},
    {path: 'text', component: TextComponent},
    {path: 'textblock', component: TextBlockComponent},
    {path: 'container', component: ContainerComponent},
    {path: 'eventcontainer', component: EventContainerComponent},
    {path: 'textarea', component: TextareaComponent},
    {path: 'slidebar', component: SlideBarComponent},
    {path: 'login', component: LoginComponent},
    {path: 'formfield', component: FormFieldComponent},
    {path: 'audio', component: AudioComponent},
    {path: 'video', component: VideoComponent},
    {path: 'imgslider', component: ImgSliderComponent},
    {path: 'articlecard', component: ArticleCardComponent},
    {path: 'scrollbarcontainer', component: ScrollbarContainerComponent},
    {path: 'navigation', component: NavigationComponent},
    {path: 'sidebar', component: SidebarComponent},
    {path: 'popup', component: PopupComponent},
    {path: 'formbuilder', component: FormBuilderComponent},
    {path: 'gridcontainer', component: GridContainerComponent}
];


@NgModule({
    declarations: [
        AppComponent,
        ButtonComponent,
        InputComponent,
        ImageComponent,
        IconComponent,
        CheckboxComponent,
        CheckboxListComponent,
        RadioButtonComponent,
        RadioButtonListComponent,
        MsgComponent,
        DatepickerComponent,
        FileUploadComponent,
        FileUploadResultsComponent,
        SelectAutocompleteComponent,
        LinkComponent,
        TooltipComponent,
        PaginationComponent,
        TextComponent,
        TextBlockComponent,
        ContainerComponent,
        EventContainerComponent,
        TextareaComponent,
        SlideBarComponent,

        LoginComponent,
        FormFieldComponent,
        AudioComponent,
        VideoComponent,
        ImgSliderComponent,
        ArticleCardComponent,
        ScrollbarContainerComponent,
        NavigationComponent,
        SidebarComponent,
        PopupComponent,

        FormBuilderComponent,
        GridContainerComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes),
        LavashAtomsModule,
        LavashMoleculesModule,
        LavashOrganismsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
