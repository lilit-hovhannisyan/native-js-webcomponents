import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'lavash-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.less']
})
export class InputComponent {
  @Input() type: string;
  @Input() placeholder: string;
  focusIsOnInput = false;
  @Input() placeholderType: 'hidden' | 'top' | 'bottom' | 'label' = 'hidden';
  @Input() value: string;
  @Input() readonly: boolean;
  @Input() noInput: boolean;
  @Input() noClearEvent: boolean;

  @Output() inputEvent = new EventEmitter();
  @Output() blurEvent = new EventEmitter();
  @Output() focusEvent = new EventEmitter();
  @Output() enterEvent = new EventEmitter();
  @Output() keydownEvent = new EventEmitter();
  @Output() deleteEvent = new EventEmitter();

  // custom icon OR image
  @Input() graphics?: string;
  @Input() graphicsPosition = 'left';
  @Output() graphicClick = new EventEmitter();


  onInput(event) {
    this.value = event.target.value;
    this.inputEvent.emit(this.value);

    this.onBlur(event);
  }

  onFocus(event) {
    this.focusEvent.emit(event.target.value);
    this.focusIsOnInput = true;
  }

  onKeyDown(event) {
    const keyCode = event.keyCode;

    // on tab or onEnter fire onBlur event
    if (keyCode === 9) {
        this.onBlur(event);
    }

    if (keyCode === 13) {
      this.enterEvent.emit(this.value);
    }

    this.keydownEvent.emit({keyCode: event.keyCode, value: event.target.value});
  }

  onBlur(event) {
    this.focusIsOnInput = false;
    this.blurEvent.emit(event);
  }

  onRemoveValue() {
    this.value = null;
    this.deleteEvent.emit();
    this.focusIsOnInput = false;
  }

  onGraphicClick(event) {
    this.graphicClick.emit(event);
  }
}
