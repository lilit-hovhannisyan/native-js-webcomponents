import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';

import { AuthStoreService } from '@core-services/auth-store.service';

import { AuthWebService } from '../auth.web-service';

@Component({
  selector: 'me-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ForgotPasswordComponent implements OnInit, OnDestroy, AfterViewInit {
  userNameEmail: string = '';
  userNameEmailSent: boolean = false;
  forgotPasswordSubscription!: Subscription;

  @ViewChild('userNameEmailField') userNameEmailField!: ElementRef;

  constructor(
    private router: Router,
    private authWebService: AuthWebService,
    private cdRef: ChangeDetectorRef,
    private authStoreService: AuthStoreService
  ) {}

  ngOnInit(): void {
    if (this.authStoreService.getIsForgotPasswordEmailSent()) {
      this.userNameEmailSent = this.authStoreService.getIsForgotPasswordEmailSent();
    } else {
      this.userNameEmailSent = false;
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.userNameEmailField.nativeElement.focus();
    });
  }

  performForgotPassword(): void {
    this.forgotPasswordSubscription = this.authWebService.requestPasswordReset({ userID: this.userNameEmail }).subscribe(
      () => {
        this.userNameEmailSent = true;
        this.cdRef.detectChanges();
      },
      () => {
        this.userNameEmailSent = false;
      }
    );
  }

  goToLogin(): void {
    this.router.navigate(['auth/login']);
  }

  ngOnDestroy(): void {
    if (this.forgotPasswordSubscription) {
      this.forgotPasswordSubscription.unsubscribe();
    }
  }
}
