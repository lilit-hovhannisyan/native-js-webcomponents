import { IResource } from './IResource';
import { ICommissionType } from 'src/app/core/models/resources/ICommissionType';
import { ICurrency } from './ICurrency';
import { IMandatorLookup } from './IMandator';
import { IBookingAccount } from './IBookingAccount';
import { IDebitorCreditor } from './IDebitorCreditor';
import { ITaxKey } from './ITaxKeys';
import { ISelectOption } from '../misc/selectOption';
import { wording } from '../../constants/wording/wording';
import { IWording } from './IWording';

export type CommissionKey = number;

export enum BILLING_CYCLE_TYPES {
  days = 0,
  weeks,
  months,
  semimonthly,
  quarters,
  years,
  lumpsum,
}

export enum PROCESSING_TYPES {
  information = 0,
  invoice,
  creditNote,
  automatic,
}

interface ISharedProps {
  from: string;
  until?: string;
  commissionTypeId: number;
  currencyId?: number;
  amount?: number;
  billingCycle?: number;
  billingCycleType?: BILLING_CYCLE_TYPES;
  isBillingCycleContinually?: number;
  mandatorId?: number;
  bookingAccountId: number;
  costCenter: number;
  taxKeyId: number;
  processing: PROCESSING_TYPES;
  invoiceRecipientId: number;
  invoicedUntil: Date;
  remark: string;
  vesselId: number;
}

export interface IVesselCommissionBase extends ISharedProps, IResource<number> { }

export interface IVesselCommissionCreate extends ISharedProps { }

export interface IVesselCommissionDTO extends IVesselCommissionBase { }

export interface IVesselCommission extends IVesselCommissionDTO { }

export interface IVesselCommissionFull extends IVesselCommission {
  commissionType: ICommissionType;
  currency: ICurrency;
  mandator: IMandatorLookup;
  bookingAccount: IBookingAccount;
  taxKey: ITaxKey;
  invoiceRecipient: IDebitorCreditor;
}

export interface IVesselCommissionRow extends IVesselCommissionFull {
  commissionTypeLabel: IWording;
  currencyIsoCode: string;
  mandatorNo: string;
  bookingAccountNo: string;
  invoiceRecipientNo: string;
  taxKeyCode: string;
  billingCycleTypeName: string;
  processingName: string;
}


export const billingCycleTypesOptions: ISelectOption[] = [
  { value: BILLING_CYCLE_TYPES.days, displayLabel: wording.chartering.billingCycleTypeDay },
  { value: BILLING_CYCLE_TYPES.weeks, displayLabel: wording.chartering.billingCycleTypeWeek },
  { value: BILLING_CYCLE_TYPES.months, displayLabel: wording.chartering.billingCycleTypeMonths },
  { value: BILLING_CYCLE_TYPES.semimonthly, displayLabel: wording.chartering.billingCycleTypeSemimonthly },
  { value: BILLING_CYCLE_TYPES.quarters, displayLabel: wording.chartering.billingCycleTypeQuarters },
  { value: BILLING_CYCLE_TYPES.years, displayLabel: wording.chartering.billingCycleTypeYears },
  { value: BILLING_CYCLE_TYPES.lumpsum, displayLabel: wording.chartering.billingCycleTypeLumpsum },
];


export const processingOptions: ISelectOption[] = [
  { value: PROCESSING_TYPES.automatic, displayLabel: wording.chartering.processingAutomatic },
  { value: PROCESSING_TYPES.creditNote, displayLabel: wording.chartering.processingCreditNote },
  { value: PROCESSING_TYPES.information, displayLabel: wording.chartering.processingInformation },
  { value: PROCESSING_TYPES.invoice, displayLabel: wording.chartering.processingInvoice },
];
