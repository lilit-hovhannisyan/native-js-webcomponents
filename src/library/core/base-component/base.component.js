import {
  setExternileStylesForComponent,
  createElementWithAttributes,
  traverseInShadowAndGetElement,
  removeStaticAtributesFromElNotToRender
} from '../../helpers/random.helpers.js';

let counter = 1;

export class BaseComponent extends HTMLElement {
  shadow = this.attachShadow({mode: 'open'});
  externalStylePath;

  constructor(externalStylePath = '') {
    super();
    this.externalStylePath = externalStylePath;
  }

  applyExternalStyles() {
    if (this.externalStylePath) {
      const externalStyleEl = setExternileStylesForComponent(this.externalStylePath);
      this.shadow.appendChild(externalStyleEl);
    }
  }

  replaceElementsThatMustNotBeRendered(element) {
    const commentEl = document.createComment(`${element.tagName} not rendered`);
    this.shadow.appendChild(commentEl);
  }

  // life cycles
  connectedCallback(callbackRenderFunc) {
    // set identifier attribute
    this.setAttribute('id', 'identifier' + counter++);

    try {
      // if not render, then replace element with comment
      if (this.getAttribute('render') === 'false') {
        this.replaceElementsThatMustNotBeRendered(this);
        return;
      }

      // if everything is ok then render and apply styles
      this.applyExternalStyles();
      callbackRenderFunc();
    } catch(e) {
      const log = `${this.tagName} (${ this.getAttribute('id')}) can not be built (native error: ${e})`;
      console.error( log );
    }
  }

  disconnectedCallback() {
    // браузер вызывает этот метод при удалении элемента из документа
    // (может вызываться много раз, если элемент многократно добавляется/удаляется)
  }

  static get observedAttributes() {
    return [/* массив имён атрибутов для отслеживания их изменений */];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    // вызывается при изменении одного из перечисленных выше атрибутов
  }

  // component builder funcs
  createElementWithAttributes = (currentComponentConfig) => {
    return createElementWithAttributes(currentComponentConfig);
    console.log('hey')
  };

  traverseInShadowAndGetElement = (baseNode, nodeNamToFind) => {
    return traverseInShadowAndGetElement = (baseNode, nodeNamToFind);
  };

  removeStaticAtributesFromElNotToRender = (currentComponent, attrList) => {
    removeStaticAtributesFromElNotToRender(currentComponent, attrList);
  };

  // output events to host component
  dispatchEvent = (event, currentComponent) => {
    console.log('dispatch', {event, currentComponent})
    return {event, currentComponent};
  }

}