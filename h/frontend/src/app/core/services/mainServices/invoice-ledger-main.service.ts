import { Injectable } from '@angular/core';
import { InvoiceLedgerRepositoryService, IInvoiceLedgerParams } from '../repositories/invoice-ledger-repository.service';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { IInvoiceLedgerFull, IInvoiceLedger, IInvoiceLedgerRow } from '../../models/resources/IInvoiceLedger';
import { map, switchMap } from 'rxjs/operators';
import { MandatorRepositoryService } from '../repositories/mandator-repository.service';
import { DebitorCreditorRepositoryService } from '../repositories/debitor-creditor-repository.service';
import { CurrencyRepositoryService } from '../repositories/currency-repository.service';
import { VoyagesRepositoryService } from '../repositories/voyages-repository.service';
import { UserRepositoryService } from '../repositories/user-repository.service';
import { IMandatorLookup } from '../../models/resources/IMandator';
import { IDebitorCreditor } from '../../models/resources/IDebitorCreditor';
import { ICurrency } from '../../models/resources/ICurrency';
import { IVoyage } from '../../models/resources/IVoyage';
import { IUser } from '../../models/resources/IUser';
import { IBookingAccount } from '../../models/resources/IBookingAccount';
import { BookingAccountRepositoryService } from '../repositories/booking-account-repository.service';
import { sortBy } from 'lodash';
import { CompanyRepositoryService } from '../repositories/company-repository.service';
import { ICompany } from '../../models/resources/ICompany';
import { IVesselAccounting } from '../../models/resources/IVesselAccounting';
import { VesselAccountingRepositoryService } from '../repositories/vessel-accounting-repository.service';
import { IMandatorAccount } from '../../models/resources/IMandatorAccount';
import { MandatorAccountRepositoryService } from '../repositories/mandator-account-repository.service';
import * as moment from 'moment-timezone';
import { getDateTimeFormat } from 'src/app/shared/helpers/general';
import { SettingsService } from '../settings.service';

type IEntitiesList<T> = [
  T,
  IMandatorLookup[],
  IDebitorCreditor[],
  IBookingAccount[],
  IMandatorAccount[],
  IVesselAccounting[],
  IVoyage[],
  ICurrency[],
  IUser[],
  ICompany[],
];

@Injectable({
  providedIn: 'root'
})
export class InvoiceLedgerMainService {
  constructor(
    private invoiceLedgerRepo: InvoiceLedgerRepositoryService,
    private mandatorRepository: MandatorRepositoryService,
    private debitorCreditorRepo: DebitorCreditorRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
    private voyageRepo: VoyagesRepositoryService,
    private userRepo: UserRepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private mandatorAccountService: MandatorAccountRepositoryService,
    private settingsService: SettingsService,
  ) { }

  public getStream(params: IInvoiceLedgerParams = {}): Observable<IInvoiceLedgerFull[]> {
    return forkJoin([
      this.invoiceLedgerRepo.fetchAll(params),
      this.mandatorRepository.fetchLookups({}),
      this.debitorCreditorRepo.fetchAll({}),
      this.bookingAccountRepo.fetchAll({}),
      this.mandatorAccountService.fetchAll({}),
      this.vesselAccountingRepo.fetchAll({}),
      this.voyageRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.userRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.invoiceLedgerRepo.getStream(),
        this.mandatorRepository.getLookupStream({}),
        this.debitorCreditorRepo.getStream({}),
        this.bookingAccountRepo.getStream({}),
        this.mandatorAccountService.getStream({}),
        this.vesselAccountingRepo.getStream({}),
        this.voyageRepo.getStream({}),
        this.currencyRepo.getStream({}),
        this.userRepo.getStream(),
        this.companyRepo.getStream(),
      ])),
      map((res: IEntitiesList<IInvoiceLedger[]>) => this.getInvoiceLedgerFullList(...res))
    );
  }

  private getInvoiceLedgerFullList(
    invoiceLedgers: IInvoiceLedger[],
    mandators: IMandatorLookup[],
    debitorCreditors: IDebitorCreditor[],
    bookingAccoutns: IBookingAccount[],
    mandatorAccounts: IMandatorAccount[],
    vesselAccountings: IVesselAccounting[],
    voyages: IVoyage[],
    currencies: ICurrency[],
    users: IUser[],
    companies: ICompany[],
  ): IInvoiceLedgerFull[] {
    return invoiceLedgers.map(invoiceLedger => this.getInvoiceLedgerFull(
      invoiceLedger,
      mandators,
      debitorCreditors,
      bookingAccoutns,
      mandatorAccounts,
      vesselAccountings,
      voyages,
      currencies,
      users,
      companies,
    ));
  }

  private getInvoiceLedgerFull(
    invoiceLedger: IInvoiceLedger,
    mandators: IMandatorLookup[],
    debitorCreditors: IDebitorCreditor[],
    bookingAccounts: IBookingAccount[],
    mandatorAccounts: IMandatorAccount[],
    vesselAccountings: IVesselAccounting[],
    voyages: IVoyage[],
    currencies: ICurrency[],
    users: IUser[],
    companies: ICompany[],
  ): IInvoiceLedgerFull {
    const mandator = mandators.find(m => m.id === invoiceLedger.mandatorId);
    const debitorCreditor = debitorCreditors.find(d => d.id === invoiceLedger.debitorCreditorId);
    const mandatorAccount: IMandatorAccount = invoiceLedger.mandatorAccountId
      ? mandatorAccounts.find(ma => ma.id === invoiceLedger.mandatorAccountId)
      : {} as IMandatorAccount;
    const bookingAccount = bookingAccounts.find(ba => ba.id === mandatorAccount.bookingAccountId);
    const vesselAccounting = vesselAccountings.find(v => v.id === invoiceLedger.vesselAccountingId);
    const voyage = voyages.find(v => v.id === invoiceLedger.voyageId);
    const currency = currencies.find(c => c.id === invoiceLedger.currencyId);
    const changedByUser = users.find(u => u.id === invoiceLedger.changedBy);
    const createdByUser = users.find(u => u.id === invoiceLedger.createdBy);
    const company = companies.find(c => c.id === debitorCreditor.companyId);
    let bookedView = '';

    if (invoiceLedger.booked) {
      bookedView = this.getBookedText(invoiceLedger, changedByUser);
    }

    return {
      ...invoiceLedger,
      mandator,
      debitorCreditor,
      bookingAccount,
      mandatorAccount,
      vesselAccounting,
      voyage,
      currency,
      createdByUser,
      changedByUser,
      company,
      bookedView,
    };
  }

  public toRow(invoiceLedger: IInvoiceLedgerFull): IInvoiceLedgerRow {
    const {
      mandator,
      bookingAccount,
      vesselAccounting,
      voyage,
      currency,
      createdByUser,
      changedByUser,
      company,
    } = invoiceLedger;
    return ({
      ...invoiceLedger,
      mandatorName: mandator?.name,
      companyName: company?.companyName,
      debitorCreditorNo: invoiceLedger?.debitorCreditor?.no,
      mandatorAccountName: bookingAccount?.displayLabel,
      vesselName: vesselAccounting?.vesselName,
      voyageName: voyage && `${voyage.year}-${voyage.no.toString().padStart(3, '0')}`,
      currencyLabel: currency?.isoCode,
      createdByUserFullName: createdByUser && `${createdByUser.firstName} ${createdByUser.lastName}`,
      changedByUserFullName: invoiceLedger.changedDate && changedByUser
        ? `${changedByUser.firstName} ${changedByUser.lastName}`
        : '',
    });
  }

  public toRows(invoiceLedgers: IInvoiceLedgerFull[]): IInvoiceLedgerRow[] {
    return sortBy(invoiceLedgers.map((invoiceLedger) => this.toRow(invoiceLedger)), ['invoiceNumber']);
  }

  private getBookedText(invoiceLedger: IInvoiceLedger, changedByUser: IUser): string {
    const dtFormat = getDateTimeFormat(this.settingsService.locale);
    const bookDate = moment(invoiceLedger.changedDate).format(dtFormat);
    return `${invoiceLedger.voucherNumber} ${changedByUser.firstName} ${changedByUser.lastName} ${bookDate}`;
  }
}
