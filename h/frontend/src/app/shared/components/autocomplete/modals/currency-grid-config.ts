import { NvGridConfig, NvGridRowSelectionType, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const gridConfig: NvGridConfig = {
  gridName: 'currencySelectModalGrid',
  rowSelectionType: NvGridRowSelectionType.RadioButton,
  rowButtonsPosition: null,
  hideRefreshButton: true,
  hideSettingsButton: true,
  paging: {
    pageNumber: 1,
    pageSize: 10,
    showTotalItems: true
  },
  columns: [
    {
      key: 'isoCode',
      title: wording.accounting.isoCode,
      width: 85,
      isSortable: true,
      filter: {
        values: []
      }
    }, {
      key: 'symbol',
      title: wording.accounting.symbol,
      width: 85,
      isSortable: true,
      filter: {
        values: []
      }
    },
    {
      key: 'displayLabel',
      title: wording.general.label,
      width: 250,
      isSortable: true,
      filter: {
        values: []
      }
    },
    {
      key: 'isActive',
      title: wording.accounting.active,
      width: 130,
      isSortable: true,
      filter: {
        controlType: NvFilterControl.Boolean,
        values: [],
      },
      dataType: NvColumnDataType.Boolean
    }
  ],
  sortBy: 'isoCode',
  isSortAscending: true,
};
