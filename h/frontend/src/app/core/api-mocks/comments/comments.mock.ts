export const commentsMock = [
  {
    'text': 'lorem<br>\n',
    'creationDate': '2018-10-04T12:03:52.667559+00:00',
    'editDate': 'Fri Oct 03 2018 17:21:56 GMT+0400 (Armenia Standard Time)',
    'author': {
      'id': 1,
      'firstName': 'Felix',
      'lastName': 'Küppers',
      'email': 'kueppers@avetiq.de'
    },
    'id': 0,
    _type: 'comment'
  },
  {
    'text': 'ipsum',
    'creationDate': '2018-10-04T12:03:52.667559+00:00',
    'editDate': 'Fri Oct 04 2018 17:21:56 GMT+0400 (Armenia Standard Time)',
    'author': {
      'id': 1,
      'firstName': 'Felix',
      'lastName': 'Küppers',
      'email': 'kueppers@avetiq.de'
    },
    'id': 1,
    _type: 'comment'
  },
];
