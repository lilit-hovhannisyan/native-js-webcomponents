import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, Output } from '@angular/core';
import { BaseLoggerComponent } from '@features/tracking-system';

import { MeSidebarNavItemI, MeSidebarI } from './me-sidebar.interface';

@Component({
  selector: 'me-sidebar',
  templateUrl: './me-sidebar.component.html',
  styleUrls: ['./me-sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeSidebarComponent extends BaseLoggerComponent {
  componentVersion: string = '0.0.1';
  // Input sidebar object to show desired fields
  @Input() dataModel: MeSidebarI = {
    isExpanded: true,
    isCollapsible: true,
    company: {
      editButton: null,
      editButtonTitle: 'Edit',
      logoUrl: '',
      title: ''
    },
    logoUrl: '/assets/images/me-logo.png',
    navItems: []
  };

  @Output() eventOccurs: EventEmitter<{ eventName: string; payload?: string }> = new EventEmitter();
  @Output() toggleSideBar: EventEmitter<void> = new EventEmitter();

  constructor(injector: Injector) {
    super(injector);
  }

  selectItem(item: MeSidebarNavItemI | null): void {
    if (item) {
      this.customEvent('selectedItemId', item.id);
    }
  }

  customEvent(eventName: string, payload?: string): void {
    this.eventOccurs.emit({ eventName, payload });
  }

  toggleExpandableSidebar(): void {
    this.toggleSideBar.emit();
  }
}
