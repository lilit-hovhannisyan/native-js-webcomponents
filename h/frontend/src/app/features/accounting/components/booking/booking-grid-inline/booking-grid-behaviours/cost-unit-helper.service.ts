import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors, FormGroup } from '@angular/forms';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { wording } from 'src/app/core/constants/wording/wording';
import { IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { ACCOUNTING_RELATIONS } from 'src/app/core/models/enums/accounting-relations';
import { CostUnitRepositoryService } from '../../../../../../core/services/repositories/cost-unit-repository.service';
import { CostUnitKey, ICostUnit } from '../../../../../../core/models/resources/ICostUnit';
import { BookingKey } from 'src/app/core/models/resources/IBooking';
import { wordingAutocomplete } from '../../../../../../core/constants/wording/autocomplete';

@Injectable({
  providedIn: 'root'
})
export class CostUnitHelperService {
  constructor(
    private costUnitRepo: CostUnitRepositoryService,
  ) { }

  public isCostUnitActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.parent || !control.value) {
      return null;
    }
    const costUnitId: CostUnitKey = +control.parent.get('costUnitId').value;

    const costUnit: ICostUnit = this.costUnitRepo.getStreamValue()
      .find(c => c.id === costUnitId);

    if (!costUnit || costUnit.isActive) {
      return null;
    }

    markControlAsTouched(control);
    return {
      doesNotExist: {
        message: wordingAutocomplete.notActive
      }
    };
  }

  public doesCostUnitExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.parent || !control.value) {
      return null;
    }
    const costUnitId: CostUnitKey = +control.parent.get('costUnitId').value;

    const costUnit: ICostUnit = this.costUnitRepo.getStreamValue()
      .find(c => c.id === costUnitId);

    if (costUnit) {
      return null;
    }

    markControlAsTouched(control);
    return {
      doesNotExist: {
        message: wording.general.doesNotExist
      }
    };
  }

  /**
    * field CU only active if mandator of booking line has cost accounting = true
    * AND account of booking line has cost accounting = true
  */
  public enableOrDisableCostUnits(
    form: FormGroup,
    mandator: IMandatorLookup,
    bookingAccount: IBookingAccount,
    bookingId: BookingKey
  ): void {
    const costUnitNoCtrl: AbstractControl = form.get('costUnitNo');
    /**
      * only on profit and loss(earnings) && balance accounts, that have cost accounting = true
      * field CU only active and required if mandator of booking line has cost accounting = true
     */
    if (
      mandator?.isCostAccounting &&
      bookingAccount?.isCostAccounting &&
      bookingAccount.accountingRelation !== ACCOUNTING_RELATIONS.PROFITANDLOSSEXPENSES
    ) {
      costUnitNoCtrl.setValidators([
        this.doesCostUnitExist,
        this.isCostUnitActive,
      ]);
      costUnitNoCtrl.enable();
    } else {
      costUnitNoCtrl.clearValidators();
      costUnitNoCtrl.disable();
      if (!bookingId) {
        costUnitNoCtrl.patchValue(null);
      }
    }
  }
}
