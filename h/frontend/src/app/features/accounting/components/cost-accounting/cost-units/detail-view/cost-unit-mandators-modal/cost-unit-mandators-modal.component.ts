import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IMandatorLookup, MandatorKey } from 'src/app/core/models/resources/IMandator';
import { IMandatorCostUnitRow } from 'src/app/core/models/resources/IMandatorCostUnit';
import { convertToMap } from 'src/app/shared/helpers/general';
import { gridConfig } from 'src/app/shared/components/autocomplete/modals/mandator-grid-config';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { MandatorMainService } from 'src/app/core/services/mainServices/mandator-main.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';

@Component({
  selector: 'nv-cost-unit-mandators-modal',
  templateUrl: './cost-unit-mandators-modal.component.html',
  styleUrls: ['./cost-unit-mandators-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CostUnitMandatorsModalComponent implements OnInit {
  @Input() public mandatorCostUnits: IMandatorCostUnitRow[];
  public dataStream$: Observable<IMandatorLookup[]>;
  public gridConfig: NvGridConfig = gridConfig(NvGridRowSelectionType.Checkbox);
  public selectedMandators: IMandatorLookup[] = [];

  constructor(
    private mandatorMainService: MandatorMainService,
    public nzModalRef: NzModalRef,
    public gridConfigService: GridConfigService,
  ) { }

  public ngOnInit(): void {
    this.initNvGrid();
  }

  private initNvGrid(): void {
    const mandatorCostUnitsMap: Map<MandatorKey, IMandatorCostUnitRow> = convertToMap<
        IMandatorCostUnitRow,
        MandatorKey
      >(this.mandatorCostUnits, 'mandatorId');
    this.dataStream$ = this.mandatorMainService
      .fetchLookupsWithRolesOnly({ filter: { isCostAccounting: true, isActive: true, } })
      .pipe(
        map(mandators => mandators.filter(m => !mandatorCostUnitsMap.get(m.id)))
      );
  }

  public rowSelect(rows: IMandatorLookup[]): void {
    this.selectedMandators = rows;
  }
}
