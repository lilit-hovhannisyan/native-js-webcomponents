import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './company-address-tab.component.html',
  styleUrls: ['./company-address-tab.component.scss'],
})
export class CompanyAddressTabComponent implements OnInit {

  public companyId: number;

  constructor(
    private route: ActivatedRoute
  ) {}

  public ngOnInit() {
    this.companyId = +this.route.parent.snapshot.params.id;
  }
}
