import { ACCOUNTING_RELATIONS } from '../enums/accounting-relations';
import { DEBITOR_CREDITOR } from '../enums/debitor-creditor';
import { IWithSystemFlag } from './IWithSystemFlag';

export interface IStartEndRange<T> {
  start: number;
  end: number;
  type: T;
}

export interface IEmailDomain extends IWithSystemFlag {
  domain: string;
}

export interface ISystemSettings {
  accountingRelationRanges: IStartEndRange<ACCOUNTING_RELATIONS>[];
  debitorCreditorRanges: IStartEndRange<DEBITOR_CREDITOR>[];
  emailDomains: IEmailDomain[];
}

export interface ISettings {
  system: ISystemSettings;
  defaultAccountPrepaidExpensesId: number;
  defaultAccountDeferredIncomeId: number;
}


export const hasOverlaps = (ranges: IStartEndRange<any>[]): IStartEndRange<any>[] => {
  ranges.sort((r1, r2) => +r1.start > +r2.start ? 1 : -1);
  return ranges.reduce((res, currentVal, i, arr) => {
    if (i === 0) {
      return res;
    }
    const prev = arr[i - 1];
    const overlap = prev.end >= currentVal.start;
    if (overlap) {
      res.overlappedElems.push(currentVal);
    }
    return res;
  }, { overlappedElems: [] }).overlappedElems;
};

export const minAccountingNumber = 0;
export const maxAccountingNumber = 99999999;

export const hasGaps = (ranges: IStartEndRange<any>[]): IStartEndRange<any>[] => {
  ranges.sort((r1, r2) => +r1.start > +r2.start ? 1 : -1);
  return ranges.reduce((res, currentVal, i, arr) => {
    if (i === 0) {
      if (+currentVal.start !== minAccountingNumber) {
        res.elementsWithGaps.push(currentVal);
      }

      return res;
    }

    if (i === ranges.length - 1) {
      if (+currentVal.end !== maxAccountingNumber) {
        res.elementsWithGaps.push(currentVal);
      }
    }

    const prev = arr[i - 1];
    const hasGap = +prev.end + 1 < currentVal.start;

    if (hasGap) {
      res.elementsWithGaps.push(currentVal);
    }

    return res;
  }, { elementsWithGaps: [] }).elementsWithGaps;
};
