import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { GlobalService } from '@core-services/global.service';
import { MeBreadcrumbModule } from '@shared/me-components/me-breadcrumb/me-breadcrumb.module';
import { MeComponentLoadingModule } from '@shared/me-directives/me-component-loader/me-component-loader.module';
import { MeHeaderModule } from '@shared-components/me-header/me-header.module';
import { MeNewRightSidebarModule } from '@shared/me-components/me-new-right-sidebar/me-new-right-sidebar.module';
import { MeSidebarModule } from '@shared-components/me-sidebar/me-sidebar.module';
import { SharedModule } from '@shared/shared.module';
import { MePopupAccountService } from '@shared/me-components/me-popup-account/me-popup-account.service';
import { MeDataImportModule } from '@shared/me-components/me-data-import/me-data-import.module';
import { MePopupAccountModule } from '@shared/me-components/me-popup-account/me-popup-account.module';

import { MainLayoutComponent } from './main-layout.component';
import { MeDataImportService } from '@shared/me-components/me-data-import/me-data-import.service';

@NgModule({
  declarations: [MainLayoutComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MeSidebarModule,
    MeHeaderModule,
    MeBreadcrumbModule,
    MeNewRightSidebarModule,
    MeComponentLoadingModule,
    MeDataImportModule,
    MePopupAccountModule
  ],
  providers: [GlobalService, MePopupAccountService, MeDataImportService]
})
export class MainLayoutModule {}
