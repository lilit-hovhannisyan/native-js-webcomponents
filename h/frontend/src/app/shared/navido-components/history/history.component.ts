import { Component, OnInit, OnDestroy } from '@angular/core';
import { getHistoryGridConfig } from 'src/app/core/constants/nv-grid-configs/history.config';
import { getDifferenceGridConfig } from 'src/app/core/constants/nv-grid-configs/difference.config';
import { HistoryRepositoryService } from 'src/app/core/services/repositories/history.repository.service';
import { ActivatedRoute } from '@angular/router';
import { IHistory } from 'src/app/core/models/resources/IHistory';
import { ResourceKey } from 'src/app/core/models/resources/IResource';
import { of, ReplaySubject, BehaviorSubject } from 'rxjs';
import { takeUntil, map } from 'rxjs/operators';
import { NvGridConfig } from 'nv-grid';
import { SettingsService } from 'src/app/core/services/settings.service';
import { getDateTimeFormat, getLocalDateTime } from '../../helpers/general';
import { IDifference } from 'src/app/core/models/resources/IDifference';

export interface ExpandedRowEvent {
  rowIndex: number;
  row: IHistory;
}

export interface Data {
  entityKey: string;
  entity: string;
  entityName: string;
}
@Component({
  selector: 'nv-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit, OnDestroy {
  private componentDestroyed$ = new ReplaySubject<boolean>(1);
  public historyGridConfig: NvGridConfig = getHistoryGridConfig();
  public differenceGridConfig: NvGridConfig = getDifferenceGridConfig();

  public filters: { [entityKey: string]: ResourceKey; entity: string; };
  private data: Data = {} as Data;
  private entityId: string;
  private dateTimeFormat: string;
  public dataSourceSubject: BehaviorSubject<IHistory[]> = new BehaviorSubject([]);

  constructor(
    public route: ActivatedRoute,
    public historyRepo: HistoryRepositoryService,
    private settingsService: SettingsService) { }

  public ngOnInit(): void {
    this.dateTimeFormat = getDateTimeFormat(this.settingsService.locale);
    this.route.data.subscribe((data: Data) => {
      this.fetchData(data, this.route.parent.snapshot.params.id);
    });

    this.route.params.pipe(
      takeUntil(this.componentDestroyed$),
    ).subscribe(() => {
      this.fetchData(this.data, this.route.parent.snapshot.params.id);
    });
  }

  private getHistoryFull = (history: IHistory[]): IHistory[] => {
    return history.map(entry => this.getHistoryEntry(entry));
  }

  private getHistoryEntry = (entry: IHistory): IHistory => {
    return {
      ...entry,
      changeDate: getLocalDateTime(entry.changeDate, this.dateTimeFormat)
    };
  }

  private fetchData(data: Data, entityId: string): void {
    if (
      (data.entity && data.entityName && data.entityKey && entityId)
      && (
        this.data.entity !== data.entity
        || this.data.entityName !== data.entityName
        || this.data.entityKey !== data.entityKey
        || this.entityId !== entityId
      )
    ) {
      this.entityId = entityId;
      this.data = data;

      this.filters = {

        // TODO this is a dynamic component,
        // find a way to transform to number only when the entity key is number
        // This can be done by an additional parameter
        [this.data.entityKey]: +this.entityId, // e.g mandatorId: 312
        entity: this.data.entity // e.g entity: 'Mandator'
      };

      this.historyRepo.fetchAll({
        resourceKey: this.entityId,
        entityName: this.data.entityName // e.g entityName: mandators
      }).pipe(
        takeUntil(this.componentDestroyed$),
        map(res => this.getHistoryFull(res)),
      ).subscribe(v => this.dataSourceSubject.next(v));
    }
  }

  public setExpandableComponent = (expandedRowEvent: ExpandedRowEvent) => {
    this.historyGridConfig.expandableComponentConfig.inputs = {
      dataSource$: of(this.formatExpandedRowsDates(expandedRowEvent.row.differences)),
      gridConfig: getDifferenceGridConfig()
    };
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  private formatExpandedRowsDates = (differences: IDifference[]): IDifference[] => {
    return differences.map(row => {
      const dateFormat: string = row.member === 'BookingDate'
        ? this.dateTimeFormat
        : this.dateTimeFormat.split(' ')[0];

      return {
        ...row,
        newValue: {
          de: getLocalDateTime(row.newValue.de, dateFormat),
          en: getLocalDateTime(row.newValue.en, dateFormat)
        },
        oldValue: {
          de: getLocalDateTime(row.oldValue.de, dateFormat),
          en: getLocalDateTime(row.oldValue.en, dateFormat)
        }
      };
    });
  }
}
