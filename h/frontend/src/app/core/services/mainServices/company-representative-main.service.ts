import { Injectable } from '@angular/core';
import { forkJoin, Observable, combineLatest } from 'rxjs';
import { CompanyRepresentativeRepositoryService } from '../repositories/company-representative-repository.service';
import { map, switchMap } from 'rxjs/operators';
import { CompanyRepositoryService } from '../repositories/company-repository.service';
import { ICompanyRepresentative, ICompanyRepresentativeFull, ICompanyRepresentativeRow } from '../../models/resources/ICompanyRepresentative';
import { CompanyKey, ICompany } from '../../models/resources/ICompany';
import { representativeFunctionTypeOptions } from '../../models/enums/RepresentativeFunctionType';
import { scopeOfRepresentationTypeOptions } from '../../models/enums/ScopeOfRepresentationType';
import { CompanyLegalEntityRepositoryService } from '../repositories/company-legal-entity-repository.service';
import { convertToMap } from 'src/app/shared/helpers/general';
import { ICompanyLegalEntity } from '../../models/resources/ICompanyLegalEntity';
import { COMPANY_LEGAL_FORM } from '../../models/enums/company-legal-form';
import { ContributionMainService } from './company-contributions-main.service';
import { ICompanyPartnerContribution } from '../../models/resources/ICompanyPartnerContribution';
import { LegalEntityHelperService } from '../../../features/system/components/misc/companies/detail-view/company-legal-tab/legal-entity/legal-entity.helper.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyRepresentativeMainService {

  constructor(
    private companyRepresentativeRepo: CompanyRepresentativeRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private companyLegalEntityRepo: CompanyLegalEntityRepositoryService,
    private contributionMainService: ContributionMainService,
    private legalEntityHelperService: LegalEntityHelperService,
  ) { }

  private getCompanyRepresentativeFull(
    representative: ICompanyRepresentative,
    companies: Map<CompanyKey, ICompany>,
  ): ICompanyRepresentativeFull {
    const company: ICompany = companies.get(representative.companyId);
    const privatePersonCompany: ICompany = companies.get(representative.partnerId);

    return { ...representative, company, privatePersonCompany };
  }

  private getCompanyRepresentativeFullList(
    companyId: CompanyKey,
    representatives: ICompanyRepresentative[],
    companies: Map<CompanyKey, ICompany>,
    legalEntity: ICompanyLegalEntity,
    allRepresentatives: ICompanyRepresentative[],
    contributions: ICompanyPartnerContribution[],
  ): ICompanyRepresentativeFull[] {
    const company: ICompany = companies.get(companyId);
    let representativesOfPartners: ICompanyRepresentative[];

    if (
      (
        company.legalForm === COMPANY_LEGAL_FORM.LimitedPartnership
        || company.legalForm === COMPANY_LEGAL_FORM.CivilLawPartnership
      )
      && legalEntity?.partners?.length > 0
    ) {
      const partnerList: CompanyKey[]
        = this.legalEntityHelperService.getCurrentPartnerList(contributions, company);

      representativesOfPartners = partnerList
        .filter(partnerId => partnerId !== companyId)
        .flatMap(partnerId => allRepresentatives.filter(r => r.companyId === partnerId));

      representativesOfPartners = representativesOfPartners.filter(r => r.isActive);
    }

    return (representativesOfPartners
      ? [...representativesOfPartners, ...representatives]
      : representatives)
      .map(r => this.getCompanyRepresentativeFull(r, companies));
  }

  public getStream(companyId: CompanyKey): Observable<ICompanyRepresentativeFull[]> {
    this.companyRepresentativeRepo.clearStream();
    return forkJoin([
      this.companyRepresentativeRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.companyLegalEntityRepo.fetchOne(companyId, {}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.companyRepresentativeRepo.getStream({ companyId }),

        this.companyRepo.getStream().pipe(
          map(companies => convertToMap<ICompany, CompanyKey>(companies))
        ),
        this.companyLegalEntityRepo.getStream({ companyId })
          .pipe(map(legalEntities => legalEntities[0])),
        this.companyRepresentativeRepo.getStream(),
        this.contributionMainService.getStream(),
      ])),
      map(res => this.getCompanyRepresentativeFullList(companyId, ...res)),
    );
  }


  public toRow(representative: ICompanyRepresentativeFull): ICompanyRepresentativeRow {
    const {
      privatePersonCompany,
      functionType,
      scopeOfRepresentationType,
    } = representative;

    return ({
      ...representative,
      firstName: privatePersonCompany.firstName,
      lastName: privatePersonCompany.lastName,
      functionTypeLabel: representativeFunctionTypeOptions.get(functionType),
      scopeOfRepresentationTypeLabel: scopeOfRepresentationTypeOptions.get(
        scopeOfRepresentationType
      ),
    });
  }

  public toRows = (
    representatives: ICompanyRepresentativeFull[],
  ): ICompanyRepresentativeRow[] => {
    return representatives.map((r: ICompanyRepresentativeFull) => this.toRow(r));
  }
}
