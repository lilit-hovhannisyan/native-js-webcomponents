import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';

import { CompanyCreateEditComponent } from './company-create-edit.component';
import { CompanyCreateEditRoutingModule } from './company-create-edit-routing.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MeAttributeModule } from '@shared/me-components/me-attribute/me-attribute.module';
import { MeButtonModule } from '@shared-components/me-button/me-button.module';

@NgModule({
  declarations: [CompanyCreateEditComponent],
  imports: [
    CommonModule,
    CompanyCreateEditRoutingModule,
    ReactiveFormsModule,
    MatStepperModule,
    MeAttributeModule,
    MeButtonModule,
    IconsModule
  ]
})
export class CompanyCreateEditModule {}
