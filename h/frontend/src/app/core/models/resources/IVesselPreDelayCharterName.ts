import { IWithActive } from './IWithActive';
import { IResource } from './IResource';
import { VesselKey } from './IVessel';

export type VesselPreDelayCharterNameKey = number;

interface ISharedProps extends IWithActive {
  vesselId: VesselKey;
  charterName: string;
  preDelay: string;
}

export interface IVesselPreDelayCharterNameCreate extends ISharedProps { }

export interface IVesselPreDelayCharterNameBase extends ISharedProps, IResource<number> { }

export interface IVesselPreDelayCharterNameDTO extends IVesselPreDelayCharterNameBase { }

export interface IVesselPreDelayCharterName extends IVesselPreDelayCharterNameDTO { }
