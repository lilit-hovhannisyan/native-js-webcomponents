import { Component, OnInit, ViewChild, OnDestroy, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { IStevedoreRepair, IStevedoreRepairFull } from 'src/app/core/models/resources/IStevedoreRepair';
import { IStevedoreRepairCreate } from '../../../../../../../core/models/resources/IStevedoreRepair';
import { StevedoreRepairRepositoryService } from '../../../../../../../core/services/repositories/stevedore-repair-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { FormBuilder } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { ActivatedRoute } from '@angular/router';
import { repairByOptions, StevedoreDamageReportStatus } from 'src/app/core/models/resources/IStevedoreDamageReport';
import * as moment from 'moment';
import { getDateTimeFormat } from 'src/app/shared/helpers/general';
import { getStevedoreDamageRepairItemsGridConfig } from './stevedore-repair.config';
import { NvGridConfig, GridComponent } from 'nv-grid';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { IRepairItemRow, IRepairItemFull } from '../../../../../../../core/models/resources/IRepairItem';
import { StevedoreRepairMainService } from 'src/app/core/services/mainServices/stevedore-repair-main.service';
import { Observable, combineLatest, of, ReplaySubject, forkJoin } from 'rxjs';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { startWith, map, tap, takeUntil, switchMap } from 'rxjs/operators';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { Language } from 'src/app/core/models/language';
import { wording } from 'src/app/core/constants/wording/wording';
import { RepairItemsArticlesComponent } from './repair-items-articles/repair-items-articles.component';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { RepairItemsService } from './repair-items.service';
import { IStevedoreDamageDocument } from 'src/app/core/models/resources/IStevedoreDamageDocument';
import { StevedoreDamagesHelperService } from '../stevedore-damages.helper.service';
import { StevedoreDamageReportMainService } from 'src/app/core/services/mainServices/stevedore-damage-report-main.service';
import { IStevedoreDamageReportRow } from '../../../../../../../core/models/resources/IStevedoreDamageReport';
import { StevedoreDamageReportRepositoryService } from 'src/app/core/services/repositories/stevedore-damage-report-repository.service';
import { toFloat } from 'src/app/core/helpers/general';
import { IUser, UserKey } from 'src/app/core/models/resources/IUser';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { RepairArticleViewComponent } from './repair-items-articles/repair-article-view/repair-article-view.component';
import { gridConfig as currencyGridConfig } from 'src/app/shared/components/autocomplete/modals/currency-grid-config';

@Component({
  selector: 'nv-repair-documents',
  templateUrl: './repair-documents.component.html',
  styleUrls: ['./repair-documents.component.scss']
})
export class RepairDocumentsComponent extends
  ResourceDetailView<IStevedoreRepair, IStevedoreRepairCreate>
  implements OnInit, OnDestroy {
  @ViewChild('repairItemsGrid') public repairItemsGrid: GridComponent;

  public dateNowISOString = new Date().toISOString();
  public repoParams = {};
  public resourceDefault = { isReparationFinished: false } as IStevedoreRepair;
  public routeBackUrl?: string;
  public reparationDoneByOptions = repairByOptions;
  public dateTimeFormat: string;
  public repairItemsGridConfig: NvGridConfig;
  public repairItems$: Observable<IRepairItemRow[]> = of([]);
  public currencies$: Observable<ICurrency[]>;
  public currencies: ICurrency[];
  public language: Language;
  public modalRef: NzModalRef;
  public id: number;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public documents: IStevedoreDamageDocument[] = [];
  public currencyGridConfig = currencyGridConfig;
  private stevedoreDamageReport: IStevedoreDamageReportRow;
  private fetchedUser: IUser;
  public formElement: ElementRef;

  constructor(
    stevedoreRepairRepo: StevedoreRepairRepositoryService,
    private stevedoreRepairMainService: StevedoreRepairMainService,
    baseComponentService: BaseComponentService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    public gridConfigService: GridConfigService,
    private currenciesRepo: CurrencyRepositoryService,
    public nzModalService: NzModalService,
    private repairItemsService: RepairItemsService,
    private stevedoreDamageReportService: StevedoreDamageReportMainService,
    private stevedoreDamageReportRepo: StevedoreDamageReportRepositoryService,
    private stevedoreDamagesHelperService: StevedoreDamagesHelperService,
    private userRepo: UserRepositoryService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef,
  ) {
    super(stevedoreRepairRepo, baseComponentService);
  }

  public ngOnInit() {
    this.id = +this.route.parent.snapshot.params.id;
    this.language = this.settingsService.language;
    this.dateTimeFormat = getDateTimeFormat(this.settingsService.locale);
    this.initRepairItemsGrid();
    this.fetchFullRepairItemsAndDocuments();
    this.inEditMode$.pipe(takeUntil(this.componentDestroyed$)).subscribe(inEditMode => {
      if (inEditMode
        && !this.stevedoreDamageReport.isInvoiceApproved
        && !this.stevedoreDamageReport.isInvoiceWaivedAndSetToCleared
      ) {
        this.repairItemsGridConfig.editForm.disableEditColumns = false;
      } else {
        this.repairItemsGridConfig.editForm.disableEditColumns = true;
      }
    });
  }

  public fetchFullRepairItemsAndDocuments() {
    const queryParams = { queryParams: { stevedoreDamageReportId: this.id } };
    forkJoin([
      this.stevedoreRepairMainService.fetchAll(queryParams),
      this.stevedoreDamageReportService.fetchOne(this.id, {})
    ]).pipe(
      tap(([_, stevedoreDamageReport]) => {
        this.stevedoreDamageReport = this.stevedoreDamageReportService
          .toRow(stevedoreDamageReport);
      })
    ).subscribe(([stevedoreRepair]) => {
      this.initializeResource(stevedoreRepair);
      this.documents = stevedoreRepair.stevedoreDamageDocuments
        ? [...stevedoreRepair.stevedoreDamageDocuments]
        : [];
      setTimeout(() => {
        this.initRepairItems(stevedoreRepair);
      });
    });
  }

  public getTitle = () => this.stevedoreDamageReport.sdReportNo;

  public init(stevedoreRepair: IStevedoreRepairFull = this.resourceDefault as IStevedoreRepairFull): void {
    this.initForm(this.fb.group({
      repairDate: [stevedoreRepair.repairDate, Validators.dateShouldBeInThePast],
      isReparationFinished: [stevedoreRepair.isReparationFinished],
      reparationDoneBy: [stevedoreRepair.reparationDoneBy],
      description: [stevedoreRepair.description],
      reparationFinishedDate: [stevedoreRepair.reparationFinishedDate],
      reparationFinishedBy: [stevedoreRepair.reparationFinishedBy],
      reparationFinishedByAndDate: [{ value: '', disabled: true }],
      stevedoreDamageRepairItems: [stevedoreRepair.stevedoreDamageRepairItems],
      invoiceDate: [stevedoreRepair.invoiceDate, Validators.dateShouldBeInThePast],
      amount: [stevedoreRepair.amount || 0],
      currencyId: [stevedoreRepair.currencyId, Validators.required],
      internalRemark: [stevedoreRepair.internalRemark],
      mandatorId: [
        stevedoreRepair.mandatorId
          || this.stevedoreDamageReport?.vesselAccounting?.mandatorId
      ],
      invoiceNo: [],
    }));
    this.initOnReparationFixedChange();
    this.handleOnReparationFixed();
    this.initCurrencyValueChangesSubscriber();
    this.loadCurrencies();

    if (!this.form.get('reparationDoneBy').value) {
      this.setDisabledFields(
        ['reparationFinishedByAndDate', 'isReparationFinished', 'invoiceNo']
      );
    } else {
      this.setDisabledFields(['reparationFinishedByAndDate', 'invoiceNo']);
    }

    this.handleRepairDoneByValueChanges();
  }

  public initRepairItems(stevedoreRepair: IStevedoreRepairFull) {
    const repairItems = this.repairItemsToRow(stevedoreRepair.stevedoreDamageRepairItems);

    const isThereSelectedCurrency = Object.keys(
      this.repairItemsService.selectedCurrency$.getValue()
    ).length > 0;

    if (!isThereSelectedCurrency) {
      this.repairItemsService.selectedCurrency$.next(stevedoreRepair.currency);
    }
    this.repairItemsService.init(this.repairItemsGrid, this.id);
    this.repairItemsService.repairItemsSubject$.next(repairItems);

    this.repairItems$ = this.repairItemsService.repairItems$.pipe(
      tap(repairItemsRow => {
        this.calculateAmountControlOnNewItem(repairItemsRow);
        // the grid still hasn't created rows, we can't create forms for rows
        // if there isn't any, so we need to use setTimeout to avoid errors from grid
        setTimeout(() => {
          repairItemsRow.forEach(r => this.repairItemsGrid.createNewForm(r.id));
        });
      }),
    );
  }

  public initRepairItemsGrid() {
    const gridActions = {
      delete: (repairItem: IRepairItemRow) => this.deleteRepairItem(repairItem),
    };
    this.repairItemsGridConfig = getStevedoreDamageRepairItemsGridConfig(
      gridActions,
      (id?: number) => this.openArticlesModal(id),
      () => this.calculateAmountControlOnFormValueChange(),
      () => this.isGridDisabled(),
      () => this.showRowButtons(),
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
    );
  }

  public initOnReparationFixedChange() {
    const {
      isReparationFinished,
      reparationFinishedBy,
      reparationFinishedDate,
      reparationFinishedByAndDate,
    } = this.form.controls;

    isReparationFinished.valueChanges.pipe(
      takeUntil(this.componentDestroyed$),
    ).subscribe(() => {
      if (isReparationFinished.value) {
        reparationFinishedBy.setValue(this.authService.getUser().id);
        reparationFinishedDate.setValue(moment().utcOffset(0));
      } else {
        reparationFinishedBy.setValue(null);
        reparationFinishedDate.setValue(null);
        reparationFinishedByAndDate.setValue(null);
      }
      this.handleOnReparationFixed();
    });
  }

  public handleOnReparationFixed() {
    const { reparationFinishedDate, reparationFinishedBy } = this.form.controls;
    if (!reparationFinishedBy.value && !this.fetchedUser) {
      this.fetchedUser = this.authService.getUser();
      return;
    }

    if (
      this.fetchedUser
      && this.fetchedUser.id === reparationFinishedBy.value
      && reparationFinishedDate.value
    ) {
      this.setReparationFinishedValue();
      return;
    }

    if (reparationFinishedDate.value) {
      this.fetchUser(reparationFinishedBy.value).subscribe(user => {
        this.fetchedUser = user;
        this.setReparationFinishedValue();
      });
    }

  }

  public setReparationFinishedValue() {
    const { reparationFinishedDate, reparationFinishedByAndDate } = this.form.controls;

    const userName = this.fetchedUser.username;
    const momentDate = moment(moment(reparationFinishedDate.value).utc(true).toISOString());
    const formattedDate = momentDate.isValid()
      ? momentDate.format(this.dateTimeFormat)
      : null;
    reparationFinishedByAndDate.setValue(`${formattedDate} ${userName}`);
  }

  private fetchUser(reparationFinishedBy: UserKey): Observable<IUser> {
    return this.userRepo.fetchOne(reparationFinishedBy, {});
  }

  public handleRepairDoneByValueChanges() {
    const { reparationDoneBy, isReparationFinished } = this.form.controls;

    reparationDoneBy.valueChanges.pipe(
      takeUntil(this.componentDestroyed$),
    ).subscribe((value) => {
      if (!value) {
        isReparationFinished.setValue(false);
        this.setDisabledFields(
          ['reparationFinishedByAndDate', 'isReparationFinished',  'invoiceNo']
        );
      } else {
        isReparationFinished.enable();
      }
    });
  }

  private loadCurrencies(): void {
    const currencyControl = this.form.controls.currencyId;
    this.currenciesRepo.fetchAll({ useCache: false }).subscribe();
    this.currencies$ = combineLatest([
      this.currenciesRepo.getStream(),
      currencyControl.valueChanges.pipe(startWith(currencyControl.value))
    ]).pipe(
      map(([currencies, currencyId]) => {
        return this.currenciesRepo.filterActiveOnly(currencies, currencyId);
      }),
      tap(currencies => {
        this.currencies = currencies;

        if (!currencyControl.value) {
          const defaultCurrency = currencies.find(c => c.isoCode === 'USD');
          currencyControl.setValue(defaultCurrency.id);
        }
      }),
    );
  }

  private initCurrencyValueChangesSubscriber() {
    this.form.get('currencyId').valueChanges.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(id => {
      const currency = this.currencies.find(c => c.id === id);
      this.repairItemsService.selectedCurrency$.next(currency);
    });
  }

  public openArticlesModal = (articleId?: number) => {
    if (articleId) {
      this.openArticlesDetailModal(articleId);
    } else {
      this.openArticlesListModal();
    }
  }

  public openArticlesListModal = () => {
    this.modalRef = this.nzModalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.technicalManagement.articles[this.language],
      nzClosable: true,
      nzContent: RepairItemsArticlesComponent,
      nzComponentParams: {
        closeModal: this.closeModal,
      },
    });
  }

  public openArticlesDetailModal = (articleId: number) => {
    this.modalRef = this.nzModalService.create({
      ...DefaultModalOptions,
      nzTitle: null,
      nzClosable: true,
      nzContent: RepairArticleViewComponent,
      nzComponentParams: {
        articleId,
      }
    });
  }

  private closeModal = () => {
    this.modalRef.destroy();
  }

  public getRepairItemsFromForm(): IRepairItemRow[] {
    return [...this.repairItemsGrid.rawRows];
  }

  public calculateAmountControlOnNewItem(repairItems: IRepairItemRow[]) {
    const amount = repairItems.reduce((prev, cur) => {
      return cur.total + prev;
    }, 0);
    this.form.get('amount').setValue(toFloat(amount));
  }

  public calculateAmountControlOnFormValueChange = () => {
    const amount = this.repairItemsGrid.rawRows.reduce((prev, cur) => {
      return +cur.total + prev;
    }, 0);
    this.form.get('amount').setValue(toFloat(amount));
  }

  public deleteRepairItem = (repairItem: IRepairItemRow) => {
    const filteredRepairItems = this.repairItemsGrid.rawRows
      .filter(row => row.id !== repairItem.id);
    this.repairItemsService.repairItemsSubject$.next(filteredRepairItems);
  }

  public showRowButtons = () => {
    if (this.repairItemsGrid) {
      this.repairItemsGrid.savedRowsChanges(this.repairItemsGrid.rawRows);
    }
  }

  public cancelEditing = (): void => {
    if (this.creationMode) { this.routeBack(); return; }

    this.leaveEditMode();
    this.init(this.resource as IStevedoreRepairFull);
    this.emitRepairItmes();
    this.documents = this.resource.stevedoreDamageDocuments
      ? [...this.resource.stevedoreDamageDocuments]
      : [];
  }

  public repairItemsToRow(repairItems: IRepairItemFull[]): IRepairItemRow[] {
    return repairItems.map(sr => {
      return this.stevedoreRepairMainService.toRowRepairItems(sr);
    });
  }

  public emitRepairItmes(repairItems?: IRepairItemFull[]) {
    const repairItemsToEmit = this.repairItemsToRow(repairItems
      ? repairItems
      : this.repairItemsToRow(this.resource.stevedoreDamageRepairItems as IRepairItemFull[])
    );
    this.repairItemsService.repairItemsSubject$.next(
      this.repairItemsService.makeRowsUnEdited(repairItemsToEmit)
    );
  }

  public onDocumentsChange(documents: IStevedoreDamageDocument[]) {
    this.documents = documents;
  }

  public submit(): void {
    if (!this.form.valid) {
      return;
    }

    this.calculateStatus();

    const value = {
      ...this.resource,
      ...this.form.getRawValue(),
      stevedoreDamageRepairItems: this.getRepairItemsFromForm().map(item => {
        return {
          ...item,
          id: item.id > 0 ? item.id : 0,
          stevedoreDamageRepairId: this.resource.id,
        };
      }),
      stevedoreDamageReportId: this.id,
      stevedoreDamageDocuments: this.documents
        .map(document => ({ ...document, stevedoreDamageRepairId: this.resource.id }))
    };

    if (this.resource.id) {
      this.updateResource(value).pipe(
        switchMap(() => this.stevedoreDamageReportRepo
          .update(this.stevedoreDamageReport, { isNotificationHidden: true })
        ),
      ).subscribe(() => {
        this.stevedoreDamagesHelperService.generateTitle(this.stevedoreDamageReport);
        this.fetchFullRepairItemsAndDocuments();
      });
    } else {
      this.createResource(value, false, true).pipe(
        switchMap(() => this.stevedoreDamageReportRepo
          .update(this.stevedoreDamageReport, {})
        ),
      ).subscribe(() => {
        this.stevedoreDamagesHelperService.generateTitle(this.stevedoreDamageReport);
        this.fetchFullRepairItemsAndDocuments();
      });
    }
  }

  public calculateStatus() {
    const repairItems = this.getRepairItemsFromForm();
    if (this.stevedoreDamageReport.status !== StevedoreDamageReportStatus.Cleared) {
      if (
        repairItems.length > 0
        && this.stevedoreDamageReport.status !== StevedoreDamageReportStatus.InProgress
      ) {
        this.stevedoreDamageReport = {
          ...this.stevedoreDamageReport,
          status: StevedoreDamageReportStatus.ReleaseRequired
        };
      } else if (repairItems.length === 0) {
        this.stevedoreDamageReport = {
          ...this.stevedoreDamageReport,
          status: StevedoreDamageReportStatus.RatingRequired
        };
      }
    }
  }

  public isGridDisabled = () => {
    return !this.editMode
      || this.stevedoreDamageReport.isInvoiceApproved
      || this.stevedoreDamageReport.isInvoiceWaivedAndSetToCleared;
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public checkIfReparationDoneByExist(): void {
    const { reparationDoneBy } = this.form.controls;

    if (!this.editMode) {
      return;
    }

    if (!reparationDoneBy.value) {
      this.confirmationService.warning({
        wording: wording.technicalManagement.defineWhoDidRepair,
        okButtonWording: wording.general.ok,
        cancelButtonWording: null
      }).subscribe();
      return;
    }
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

  public printInvoice() {
    this.stevedoreRepairMainService.downloadInvoice(this.resource.id)
      .subscribe();
  }
}
