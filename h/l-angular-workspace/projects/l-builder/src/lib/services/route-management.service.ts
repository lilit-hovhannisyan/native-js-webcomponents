import { Injectable } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouteManagementService {
  queryParams = {};
  constructor(private router: Router,
              private activatedRoute: ActivatedRoute) { }

  setRouteQuery(query: string, param: string, resetParams = true) {
      this.queryParams = resetParams ? {} : this.queryParams;
      this.queryParams[query] =  param;

      this.router.navigate(
          [],
          {
              relativeTo: this.activatedRoute,
              queryParams: this.queryParams,
          });
  }

  navigate(page) {
      this.router.navigate([page]);
  }
}
