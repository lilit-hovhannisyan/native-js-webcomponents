export default interface RequestLogI {
  eventType: string;
  requestId: string;
  data: {
    method: string;
    urlWithParams: string;
    body: unknown;
  };
  userActionId: string | null;
}
