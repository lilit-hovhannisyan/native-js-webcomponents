import { IWording } from './resources/IWording';

export interface INotification {
  type: NotificationType;
  title: IWording;
  message: IWording;
  duration: number;
}

export enum NotificationType {
  Blank = 'Blank',
  Error = 'Error',
  Info = 'Info',
  Success = 'Success',
  Warning = 'Warning'
}
