import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';
import { IWithLabels } from './IWithLabels';
import { IWording } from './IWording';

export type MasterStructureNodeKey = string;

export enum MASTER_STRUCTURE_NODE_TYPE {
  NODE = 1,
  BOOKING_ACCOUNT = 2,
  SUMMATION = 3,
  SWITCHING_ACCOUNT = 4,
}

interface ISharedProps extends IWithLabels {
  externalSystemCode: string;
  masterStructureId: number;
  parentNodeId?: MasterStructureNodeKey;
  childNodes: IMasterStructureNode[];
  nodeType: MASTER_STRUCTURE_NODE_TYPE;
  bookingAccountId?: number;
  level: number;
  isHighlighted: boolean;
  hasExpenseItem: boolean;
  hasResultCurrentYear: boolean;
  hasResultPreviousYear: boolean;
  position: number;
  hasSwitchingNode: boolean;
  switchingNodeId: MasterStructureNodeKey;
}

export interface IMasterStructureNodeCreate extends ISharedProps { }

export interface IMasterStructureNodeCreate extends ISharedProps { }

export interface IMasterStructureNodeBase extends ISharedProps, IResource<MasterStructureNodeKey> { }

export interface IMasterStructureNodeDTO extends IMasterStructureNodeBase { }

export interface IMasterStructureNode extends IMasterStructureNodeDTO, IWithLabel { }

export interface IMasterStructureNodeTemplate extends IMasterStructureNode {
  title: IWording;
  key: MasterStructureNodeKey;
  expanded?: boolean;
  children?: IMasterStructureNodeTemplate[];
  isLeaf?: boolean;
  origin?: any;
}

export interface IMasterStructureNodeHashTable {
  [id: string]: IMasterStructureNodeTemplate;
}
