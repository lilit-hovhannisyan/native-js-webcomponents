import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { CostTypeKey } from '../../../../../../../../core/models/resources/ICostTypes';
import { BookingAccountKey } from '../../../../../../../../core/models/resources/IBookingAccount';
import { getAccountCostTypeGridConfig } from '../../../../../../../../core/constants/nv-grid-configs/account-cost-type.config';
import { AccountCostTypeMainService } from '../../../../../../../../core/services/mainServices/account-cost-type-main.service';
import { wording } from '../../../../../../../../core/constants/wording/wording';
import { IAccountRelatedCostType } from 'src/app/core/models/resources/IAccountCostType';
import { ACCOUNTING_RELATION_CATEGORY } from '../../../../../../../../core/models/enums/accounting-relation-category';

@Component({
  selector: 'nv-cost-accounting-relation-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CostAccountingRelationDetailViewComponent implements OnInit {
  @Input() public closeModal: () => void;
  @Input() public bookingAccountId: BookingAccountKey;
  @Input() public accountingRelationCategory: ACCOUNTING_RELATION_CATEGORY;

  public selectedCostTypesIdArray: CostTypeKey[] = [];
  public dataSource$: Observable<IAccountRelatedCostType[]>;
  public costTypeGridConfig: NvGridConfig;
  public wording = wording;

  constructor(
    private accountCostTypeMainService: AccountCostTypeMainService,
  ) { }

  public ngOnInit(): void {
    this.dataSource$ = this.accountCostTypeMainService.bookingIdNotRelatedCostTypeList$;
    this.costTypeGridConfig = getAccountCostTypeGridConfig(true);
  }

  public rowSelect(rows: IAccountRelatedCostType[]): void {
    this.selectedCostTypesIdArray = rows.map(r => r.id);
  }

  public save(): void {
    this.accountCostTypeMainService.addToCostTypeRelationList(this.selectedCostTypesIdArray);
    this.closeModal();
  }
}
