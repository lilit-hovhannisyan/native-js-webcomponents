import { Type } from 'class-transformer';
import { LayoutGroupAttributeModel } from './layout-group-attribute.model';

export class LayoutRowModel {
  rowIndex: number = 1;
  @Type(() => LayoutGroupAttributeModel)
  attributes: LayoutGroupAttributeModel[] = [];
}
