import { AbstractControl, FormArray, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators as ngValidators } from '@angular/forms';
import { isValidBIC, isValidIBAN } from 'ibantools';
import * as moment from 'moment';
import { isNumber, setTranslationSubjects } from 'src/app/shared/helpers/general';
import { FOURDIGITS, LATITUDE, LONGITUDE, NUMBER, REALNUMBERS, WHOLENUMBERS, ONLY_DIGITS_AND_LETTERS } from '../constants/regexp';
import { wording } from '../constants/wording/wording';
import { fDecimal } from '../helpers/decimal-formatter';
import { getFiscalCalendarDate } from '../models/resources/IMandator';
import { IWording } from '../models/resources/IWording';
import { dateFormatTypes, NvLocale, timeFormatTypes } from './../models/dateFormat';
import { NVFormControl } from './NVFormControl';


/**
 * This class contains validators that wrap the built-in validators by angular.
 * @see https://angular.io/api/forms/Validators
 *
 * We wrap built-in validators in order to include error messages instead of always
 * adding these error messages to each field redundantly in the templates.
 */
export class Validators {

  /**
   * Core methods that will wrap default validation methods
   */

  /**
   * Wraps default Angular validators which return ValidatorFn
   */
  private static methodWrapper(methodName: string, message: IWording, argument: any): ValidatorFn {
    const _validator = ngValidators[methodName](argument);
    return (control: AbstractControl): ValidationErrors | null => {
      const error = _validator(control);
      return Validators.handleError(error, message);
    };
  }

  /**
   * Wraps default Angular validators which return ValidationErrors
   */
  private static methodWrapperError(methodName: string, message: IWording, argument: any): ValidationErrors | null {
    const error = ngValidators[methodName](argument);
    return Validators.handleError(error, message);
  }

  /**
   * Returns validation error message, if it exists
   */
  private static handleError(error: ValidationErrors, message: IWording): ValidationErrors | null {
    if (error) {
      const firstError = Object.keys(error)[0];
      return { [firstError]: { message } };
    }
    return null;
  }

  /**
   * Wrap default ngValidators
   */

  /**
   * Validator that requires the control's value to be greater than or equal to the
   * provided number.
   */
  public static min(min: number | string, customMessage?: IWording): ValidatorFn {
    const message = customMessage || setTranslationSubjects(
      wording.general.theValueShouldBeGreaterOrEqualThan,
      { subject: fDecimal(+min) },
    );
    return Validators.methodWrapper('min', message, min);
  }

  /**
   * Validator that requires the control's value to be less than or equal to the provided
   * number.
   */
  public static max(max: number, customMessage?: IWording): ValidatorFn {
    const message = customMessage || setTranslationSubjects(
      wording.general.theValueShouldBeLessOrEqualThan,
      { subject: fDecimal(max) },
    );
    return Validators.methodWrapper('max', message, max);
  }

  /**
   * Validator that requires the control have a non-empty value.
   */
  public static required(control: AbstractControl | null): ValidationErrors | null {
    const key = 'required';
    const message = wording.general.theFieldIsRequired;
    if (control.value && typeof control.value === 'string' && !control.value.trim()) {
      return { [key]: { message } };
    }
    return Validators.methodWrapperError(key, message, control);
  }

  /**
   * Validator that requires the control's value be true. This validator is commonly
   * used for required checkboxes.
   */
  public static requiredTrue(control: AbstractControl): ValidationErrors | null {
    const message = wording.general.theValueNeedsToBeTrue;
    return Validators.methodWrapperError('requiredTrue', message, control);
  }

  /**
   * Validator that requires the control's value pass an email validation test.
   */
  public static email(control: AbstractControl): ValidationErrors | null {
    const message = wording.general.invalidEmail;
    return Validators.methodWrapperError('email', message, control);
  }

  /**
   * Validator that requires the length of the control's value to be greater than or equal
   * to the provided minimum length.
   */
  public static minLength(minLength: number, msg?: string): ValidatorFn {
    return (control: AbstractControl | null) => {
      const { value } = control;
      if (!value) {
        return null;
      }
      const message = setTranslationSubjects(
        wording.general.minLengthNeedsToBe,
        { subject: minLength },
      );

      return value && value.trim().length >= minLength
        ? null
        : { error: { message: msg || message } };
    };
  }

  /**
   * Validator that requires the length of the control's value to be less than or equal
   * to the provided maximum length.
   */
  public static maxLength(max: number): ValidatorFn {
    const message = setTranslationSubjects(
      wording.general.maxLengthIs,
      { subject: max },
    );
    return Validators.methodWrapper('maxLength', message, max);
  }

  /**
   * Validator that requires the control's value to match a regex pattern.
   */
  public static pattern(
    pattern: string | RegExp,
    message: IWording = wording.general.theValueIsNotValid,
  ): ValidatorFn {
    return Validators.methodWrapper('pattern', message, pattern);
  }

  public static exactLength(length: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      let { value } = control;
      if (value && isNumber(value)) {
        value = value.toString();
      }
      const isValid = (typeof value === 'string') && (value.length === length);
      const message = setTranslationSubjects(
        wording.general.theLengthNeedsToBe,
        { subject: length },
      );
      return isValid
        ? null
        : { invalidLength: { message } };
    };
  }


  /**
   * Custom Validation Methods
   */


  /**
   * Validate IBAN.
   * Works with help of this package https://www.npmjs.com/package/ibantools
   * Use it as simple validation method like required or email.
   * @example
   * "AL35202111090000000001234567" // returns true
   * @example
   * "NL92ABNA0517164300" // returns false
   */
  public static isIban(control: AbstractControl | null): ValidationErrors | null {
    const message = wording.accounting.invalidIban;
    return isValidIBAN(control.value) || !control.value
      ? null
      : { invalidIban: { message: message } };
  }

  /**
   * Validate BIC/SWIFT.
   * Works with help of this package https://www.npmjs.com/package/ibantools
   * Use it as simple validation method like required or email.
   * @example
   * "ABNANL2A"    // returns true
   * @example
   * "NEDSZAJJXXX" // returns true
   * @example
   * "ABN4NL2A"    // returns false
   * @example
   * "ABNA NL 2A"  // returns false
   */
  public static isBic(control: AbstractControl | null): ValidationErrors | null {
    const message = wording.accounting.invalidBic;
    return !control.value || isValidBIC(control.value || '')
      ? null
      : { invalidBic: { message } };
  }

  /**
   * Checks if passwords in 2 inputs are equal
   */
  public static passwordConfirming(
    password: FormControl | AbstractControl | null,
    passwordCf: FormControl | AbstractControl | null,
    message = wording.accounting.passwordDoNotMatch
  ): ValidatorFn {
    return (): ValidationErrors | null => {
      if (password && passwordCf && password.value !== passwordCf.value) {
        const doNotMatch = { doNotMatch: { message } };
        passwordCf.setErrors(doNotMatch);
        return doNotMatch;
      }
      passwordCf.setErrors(null);
      return null;
    };
  }
  /**
   * Requires several amount of checkboxes from checkbox group
   */
  public static requiredCheckboxes(minRequired = 1, message?: IWording): ValidatorFn {
    return (formGroup: FormGroup) => {
      let checked = 0;
      const errorMessage = setTranslationSubjects(
        wording.general.youNeedToCheckMinimumCheckbox,
        { subject: minRequired },
      );
      Object.keys(formGroup.controls).forEach(key => {
        const control = formGroup.controls[key];
        if (control.value === true) {
          checked++;
        }
      });
      if (checked < minRequired) {
        return {
          requiredCheckboxes: { message: message || errorMessage },
        };
      }
      return null;
    };
  }

  public static number(c: AbstractControl | null): ValidationErrors | null {
    const message = wording.general.onlyNumbersAllowed;
    if (c.value) {
      return (NUMBER.test(c.value))
        ? null
        : { isNotDecimal: { message } };
    }
    return null;
  }

  public static wholeNumber(c: AbstractControl | null): ValidationErrors | null {
    const message = wording.general.wholeNumbersAllowed;
    if (c.value) {
      return (WHOLENUMBERS.test(c.value))
        ? null
        : { notWholeNumber: { message } };
    }
    return null;
  }

  public static positiveNumber(control: AbstractControl | null): ValidationErrors | null {
    const message = setTranslationSubjects(
      wording.general.theValueShouldBeGreaterThan,
      { subject: 0 },
    );

    if (control.value || control.value === 0) {
      return (control.value > 0)
        ? null
        : { notPositive: { message } };
    }
    return null;
  }

  public static positiveNumberFloat(control: AbstractControl | null): ValidationErrors | null {
    const message = setTranslationSubjects(
      wording.general.theValueShouldBeGreaterThan,
      { subject: '0,00' },
    );

    if (control.value || control.value === 0) {
      return (control.value > 0)
        ? null
        : { notPositive: { message } };
    }
    return null;
  }

  public static notNegative(control: AbstractControl | null): ValidationErrors | null {
    const message = setTranslationSubjects(
      wording.general.theValueShouldBeGreaterThan,
      { subject: -1 },
    );

    if (!control.value || control.value > 0) {
      return null;
    }

    return { notPositive: { message } };
  }

  public static notNegativeFloat(control: AbstractControl | null): ValidationErrors | null {
    const message = setTranslationSubjects(
      wording.general.theValueShouldBeGreaterThan,
      { subject: '-1.00' },
    );

    if (!control.value || control.value > 0) {
      return null;
    }

    return { notPositive: { message } };
  }

  public static year(control: AbstractControl): ValidationErrors | null {
    const value: string = control.value;
    const message = wording.general.invalidYear;
    if (!value) { return null; }
    return FOURDIGITS.test(value) ? null : { isValidYear: { message } };
  }
  /**
   * @param  {string} fieldName
   * @param  {boolean} canBeEqual?
   * @param  {moment.unitOfTime.Base='day'} startOf
   * @returns ValidatorFn
   */
  public static isAfterFieldDate(
    fieldName: string,
    canBeEqual?: boolean,
    startOf: moment.unitOfTime.Base = 'day',
  ): ValidatorFn {
    return (control: NVFormControl): ValidationErrors | null => {
      const minDateFormControl = control.parent && control.parent.get(fieldName) as NVFormControl;
      if (minDateFormControl) {
        const messageWording = canBeEqual
          ? wording.accounting.startMustBeLessOrEqualThanEnd
          : wording.accounting.startMustBeLessThanEnd;
        const message = setTranslationSubjects(
          messageWording,
          {
            START_LABEL: minDateFormControl.label,
            END_LABEL: control.label,
          },
        );

        const startDateValue = moment(minDateFormControl.value);
        const endDateValue = moment(control.value);

        if (minDateFormControl.value && control.value) {
          const startDateStartOf = moment(startDateValue).startOf(startOf);
          const endDateStartOf = moment(endDateValue).startOf(startOf);
          const isBefore = canBeEqual
            ? startDateStartOf.isSameOrBefore(endDateStartOf)
            : startDateStartOf.isBefore(endDateStartOf);
          if (!isBefore) {
            control.markAsTouched();
          }
          return isBefore ? null : { isAfterFieldDate: { message } };
        }
        return null;
      }
      return null;
    };
  }

  public static latitude(control: FormControl) {
    const { value } = control;
    const message = wording.general.invalidLatitude;
    return LATITUDE.test(value)
      ? null
      : { error: { message } };
  }

  public static longitude(control: FormControl) {
    const { value } = control;
    const message = wording.general.invalidLongitude;
    return LONGITUDE.test(value)
      ? null
      : { error: { message } };
  }

  public static mandatorStartDate = (formControl: AbstractControl): ValidationErrors | null => {
    if (formControl && formControl.parent) {
      const form = formControl.parent;
      const isStartDateValid = Validators.isMandatorStartDateValid(form);
      const message = wording
        .accounting
        .startDateShouldBeLowerThenFirstBookingYear;

      if (!isStartDateValid) {
        return { invalidDate: { message } };
      } else {
        return null;
      }
    }
    return null;
  }

  // Validation rule https://navido.atlassian.net/browse/NAVIDO-404
  private static isMandatorStartDateValid(form: FormGroup | FormArray): boolean {
    const firstBookinYearValue = form.get('firstBookingYear').value;
    if (!firstBookinYearValue) {
      return true;
    }
    const firstBookingYear = new Date(firstBookinYearValue).getFullYear();
    const startFiscalYear = form.get('startFiscalYear').value || 1;
    const start = new Date(form.get('start').value);
    return getFiscalCalendarDate(start, startFiscalYear).getFullYear() <= firstBookingYear;
  }

  public static intOfLength(length: number, canBeEmpty?: boolean): ValidatorFn {
    return (control: AbstractControl | null) => {
      const value = control.value == null
        ? ''
        : control.value.toString();
      const message = setTranslationSubjects(
        wording.accounting.needsToBeDigitsLong,
        { subject: length, },
      );
      return value &&
        (value.length === length && NUMBER.test(value)) ||
        value.length === 0 && canBeEmpty
        ? null
        : { invalidInteger: { message } };
    };
  }

  public static integerBetween(min: number, max: number, realNumbers = false): ValidatorFn {
    return (control: AbstractControl | null) => {
      const message = setTranslationSubjects(
        wording.accounting.needsToBeNumberBetween,
        { subject1: min, subject2: max },
      );
      const error = { invalidInteger: { message } };
      if (
        control.value === undefined
        || control.value === null
        || control.value === ''
        || isNaN(control.value)
      ) { return null; }
      const val = control.value.toString();
      if (realNumbers) {
        if (!REALNUMBERS.test(val)) { return error; }
      } else {
        if (!NUMBER.test(val)) { return error; }
      }
      const int = parseInt(val, 10);
      if (isNaN(int)) { return error; }
      if (int < min || int > max) { return error; }
      return null;
    };
  }
  /**
   * @param  {string} fieldName
   * @param  {boolean} canBeEqual?
   * @returns ValidatorFn
   */
  public static isGreaterThanFieldValue(
    fieldName: string,
    canBeEqual?: boolean,
  ): ValidatorFn {
    return (control: NVFormControl): ValidationErrors | null => {
      const minFormControl = control.parent?.get(fieldName) as NVFormControl;
      if (!minFormControl) {
        return null;
      }
      // we check for the value becuase `+null` returns `0` and we don't want that
      const lowValue = minFormControl.value ? +minFormControl.value : undefined;
      const highValue = control.value ? +control.value : undefined;
      if (!isNumber(lowValue) || !isNumber(highValue)) {
        return null;
      }
      const messageWording = canBeEqual
        ? wording.general.value1MustBeGreaterOrEqualThanValue2
        : wording.general.value1MustBeGreaterThanValue2;
      const message = setTranslationSubjects(
        messageWording,
        { VALUE_1: control.label, VALUE_2: minFormControl.label },
      );

      return lowValue < highValue || (canBeEqual && lowValue === highValue)
        ? null
        : { isAfterFieldValue: { message } };
    };
  }

  /**
   * @param  {string} fieldName
   * @param  {boolean} canBeEqual?
   * @returns ValidatorFn
   */
  public static isLessThanFieldValue(
    fieldName: string,
    canBeEqual?: boolean,
    compareWithLabel = true,
    withFloatNumber = false,
  ): ValidatorFn {
    return (control: NVFormControl): ValidationErrors | null => {
      const minFormControl = control.parent?.get(fieldName) as NVFormControl;
      if (!minFormControl) {
        return null;
      }
      // we check for the value becuase `+null` returns `0` and we don't want that
      const lowValue = control.value || `${control.value}` === '0'
        ? +control.value
        : undefined;
      const highValue = minFormControl.value || `${minFormControl.value}` === '0'
        ? +minFormControl.value
        : undefined;
      if (!isNumber(lowValue) || !isNumber(highValue)) {
        return null;
      }
      const messageWording = canBeEqual
        ? wording.accounting.startMustBeLessOrEqualThanEnd
        : wording.accounting.startMustBeLessThanEnd;
      const message = setTranslationSubjects(
        messageWording,
        {
          START_LABEL: control.label,
          END_LABEL: compareWithLabel
            ? minFormControl.label
            : (withFloatNumber
              ? fDecimal(minFormControl.value)
              : minFormControl.value
            )
        },
      );

      return lowValue < highValue || (canBeEqual && lowValue === highValue)
        ? null
        : { isAfterFieldValue: { message } };
    };
  }

  /**
   * This validator is mainly used on a datebox to show error message when date is
   * invalid or it's in wrong ISO format
   */
  public static isDateInputInvalid = (control: AbstractControl | null) => {
    if (!control.value || moment(control.value, moment.ISO_8601).isValid()) {
      return null;
    }

    return {
      invalidDate: {
        message: wording.general.invalidInput
      }
    };
  }

  public static maxDecimalDigits(maxDigits: number): ValidatorFn {
    return (control: AbstractControl | null) => {

      const error = {
        maxDecimalDigitsExceeded: {
          message: setTranslationSubjects(
            wording.general.maxDecimalDigits,
            { DIGITS: maxDigits },
          )
        }
      };

      if (!control.value || Math.floor(control.value) === control.value) {
        return null;
      }
      const decimalDigitsCount = control.value.toString().split('.')[1].length || 0;
      return decimalDigitsCount > maxDigits ? error : null;
    };
  }

  /**
   * @param  {string} after
   * @param  {string} before
   * @returns ValidatorFn
   */
  public static dateShouldBeAfterBefore(
    after: string,
    before: string,
    locale: NvLocale,
    withTime: boolean = false,
  ): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const current = new Date(control.value).getTime();
      const from = moment.utc(after).toDate().getTime();
      const until = moment.utc(before).toDate().getTime();

      if (current < from || current > until) {
        const format = withTime
          ? dateFormatTypes[locale]
          : `${dateFormatTypes[locale]} ${timeFormatTypes[locale]};`;
        return {
          disabledRange: {
            message: setTranslationSubjects(
              wording.general.dateShouldBeAfterBefore,
              {
                afterDate: moment(after).format(format),
                beforeDate: moment(before).format(format),
              },
            ),
          }
        };
      }
      return null;
    };
  }

  public static dateShouldBeAfter(
    value: string,
    locale: NvLocale,
    canBeEqual: boolean = false,
    withTimeFormat: boolean = false,
    granularity: moment.unitOfTime.StartOf = 's',
  ): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const current = moment(control.value);
      const isOut = canBeEqual
        ? current.isBefore(value, granularity)
        : current.isSameOrBefore(value, granularity);

      if (isOut) {
        const format = withTimeFormat
          ? `${dateFormatTypes[locale]} ${timeFormatTypes[locale]}`
          : dateFormatTypes[locale];

        const messageWording = canBeEqual
          ? wording.general.dateShouldBeGreaterOrEqual
          : wording.general.dateShouldBeGreater;

        return {
          disabledRange: {
            message: setTranslationSubjects(
              messageWording,
              {
                afterDate: moment(value).format(format),
              },
            ),
          }
        };
      }
      return null;
    };
  }

  public static dateShouldBeBefore(
    value: string,
    locale: NvLocale,
    canBeEqual: boolean = false,
    withTimeFormat: boolean = false,
    granularity: moment.unitOfTime.StartOf = 's',
  ): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const current = moment(control.value);
      const isOut = canBeEqual
        ? current.isAfter(value, granularity)
        : current.isSameOrAfter(value, granularity);

      if (isOut) {
        const format = withTimeFormat
          ? `${dateFormatTypes[locale]} ${timeFormatTypes[locale]}`
          : dateFormatTypes[locale];

        const messageWording = canBeEqual
          ? wording.general.dateShouldBeSmallerOrEqual
          : wording.general.dateShouldBeSmaller;

        return {
          disabledRange: {
            message: setTranslationSubjects(
              messageWording,
              {
                beforeDate: moment(value).format(format),
              },
            ),
          }
        };
      }
      return null;
    };
  }

  public static dateShouldBeInThePast: ValidatorFn = function (control: FormControl): ValidationErrors | null {
    if (moment().utc(true).isBefore(control.value)) {
      return {
        dateShouldBeInThePast: { message: wording.general.dateShouldBeInThePast }
      };
    }
    return null;
  };


  /**
   * @param keepLocalTime uses UTC +00 if `false`
   */
  public static dateShouldBeInFuture(keepLocalTime = true): ValidatorFn {
    return (control: AbstractControl | null): ValidationErrors | null => {
      if (moment().utc(keepLocalTime).isAfter(control.value)) {
        return {
          dateShouldBeInFuture: { message: wording.general.dateShouldBeInFuture }
        };
      }
      return null;
    };
  }

  /**
   * Validator for year range works with nv-datebox
   * @param from start year
   * @param to end year
   */
  public static yearRange(from: number, to: number): ValidatorFn {
    return (control: AbstractControl | null) => {
      if (!control.value) {
        return null;
      }

      const error = {
        yearRangeError: {
          message: setTranslationSubjects(
            wording.general.dateShouldBeAfterBefore,
            { afterDate: from - 1, beforeDate: to + 1 },
          ),
        }
      };

      const year = control.value;
      return year > to || year < from ? error : null;
    };
  }

  public static greaterThan(min: number): ValidatorFn {
    return (control: AbstractControl | null) => {
      const value = control.value === null || control.value === undefined
        ? ''
        : control.value.toString();
      const message = setTranslationSubjects(
        wording.general.theValueShouldBeGreaterThan,
        { subject: min },
      );
      return value && value > min
        ? null
        : { inputTooSmall: { message } };
    };
  }

  public static unique<T = any>(
    allItems: T[],
    equalityChecker?: (controlValue: any, itemValue: T) => boolean,
  ): ValidatorFn {
    return (control: AbstractControl | null) => {
      const callback = equalityChecker || ((controlValue: any, itemValue: T) => itemValue === controlValue);
      const exists = allItems.some(item => callback(control.value, item));

      return exists
        ? { unique: { message: wording.general.valueAlreadyExists } }
        : null;
    };
  }
  /**
   * The value of the control shouldn't be equal to any of values.
   * @param  {any[]} ...values
   * @returns ValidatorFn
   */
  public static valueShouldNotBeEqualTo(...values: any[]): ValidatorFn {
    return (control: AbstractControl) => {
      const index: number = values.indexOf(control.value);

      return index !== -1
        ? {
          notEqual: {
            message: setTranslationSubjects(
              wording.general.theValueShouldNotBeEqual,
              { subject: values[index] }
            ),
          },
        }
        : null;
    };
  }

  /**
   * The value of the control shouldn't be equal to any of values.
   * @param  {number[]} ...values
   * @returns ValidatorFn
   */
  public static valueShouldNotBeEqualToFloat(...values: number[]): ValidatorFn {
    return (control: AbstractControl) => {
      const index: number = values.indexOf(control.value);

      return index !== -1
        ? {
          notEqual: {
            message: setTranslationSubjects(
              wording.general.theValueShouldNotBeEqual,
              { subject: fDecimal(values[index]) }
            ),
          },
        }
        : null;
    };
  }

  public static onlyNumberAndLetter(
    control: AbstractControl | null
  ): ValidationErrors | null {
    const message: IWording = wording.general.onlyNumbersAndLetters;

    if (!control.value || ONLY_DIGITS_AND_LETTERS.test(control.value)) {
      return null;
    }

    return { isNotNumberOrLetter: { message } };
  }

  public static isDateInRangeOfFields(
    fromCtrlName: string,
    untilCtrlName: string,
  ): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      if (!control.root || !control.value) {
        return null;
      }
      const form: AbstractControl = control.root;
      const fromCtrl: AbstractControl = form.get(fromCtrlName);
      const untilCtrl: AbstractControl = form.get(untilCtrlName);
      const from: moment.Moment = moment(fromCtrl?.value);
      const until: moment.Moment = moment(untilCtrl?.value);
      const current: moment.Moment = moment(control.value);

      if (current.isBefore(from, 'day') || current.isAfter(until, 'day')) {
        return {
          disabledRange: {
            message: wording.chartering.timeRangeDoesntMatchWithPeriod
          }
        };
      }
      return null;
    };
  }
}
