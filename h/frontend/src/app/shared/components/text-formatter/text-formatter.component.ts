import { OnInit, Component, Input, SimpleChanges, OnChanges, ChangeDetectionStrategy } from '@angular/core';

/**
 * In the future can be added functionality for 'italic', 'underline' and etc.
 */

/**
 * Additional "Extensions"(ex: italic, underline or custom) can ba added
 * using string template interpolation: `(${a}|${b}|${c})`
 * Where each one(a, b, c) is a part of regexp-pattern for extension.
 */
const LEFT_ASTERISK = '((\\s\\*)|(^\\*))';
const RIGHT_ASTERISK = '((\\*\\s)|(\\*))';
const MARKED_TEXT = `(${LEFT_ASTERISK}(.|\\n)*?${RIGHT_ASTERISK})`;
const UNMARKED_TEXT = `(((?!${MARKED_TEXT})(.|\\n))+)`;

/**
 * The result of the `TEXT_SEPARATOR` is
 * `((((\s\*)|(^\*)).*?((\*\s)|(\*$)))|(((?!(((\s\*)|(^\*)).*?((\*\s)|(\*$)))).)+))`
 */
const TEXT_SEPARATOR = `(${MARKED_TEXT}|${UNMARKED_TEXT})`;

interface TextPart {
  value: string;
  isBold: boolean;
}

@Component({
  selector: 'nv-text-formatter',
  templateUrl: './text-formatter.component.html',
  styleUrls: ['./text-formatter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextFormatterComponent implements OnInit {
  /**
   * The `boldTextPattern` matches only text-snippets inside of asterisks where
   * 1. left-asterisk has to follow after blank or it has to be the start of the string
   * 2. right-asterisk has to be followed by blank or it has to be the end of the string
   * Example: `*C++ has* been created *by* Bjarne *Stroustrup*.`
   *   matches: ['*C++ has*', '*by*']
   *   // The part `*Stroustrup*` end with '.'(dot) so doesn't match
   * Additional Info: Double-asterisk('**') will be matched as well for
   * corner cases when the text after subject-replacement contains
   * double-asterisk instead of `*{subject}*` because the value of
   * the subject can be `null` or an empty string.
   */
  private readonly boldTextPattern = new RegExp(MARKED_TEXT, 'g');
  /**
   * The `boldTextSeparatorPattern` matches whole whole text bold and not-bold parts.
   * Example:
   *   text: `*Git is* a *distributed* version-*control* *system.*`
   *   textParts: `['*Git is* ', 'a', ' *distributed* ', 'version-*control*', ' *system.*']
   *   // The word `control` will not be counted as a marked(bold) text-snippet.
   */
  private readonly boldTextSeparatorPattern = new RegExp(TEXT_SEPARATOR, 'g');

  @Input() public text: string;
  public textParts: TextPart[] = [];

  constructor() { }

  public ngOnInit(): void {
    this.setTextParts();
  }

  private setTextParts(): void {
    const textParts = this.text.match(this.boldTextSeparatorPattern);

    // if there is no text, then there is no parts
    if (textParts) {
      this.textParts = textParts.map(part => {
        const regNewLine = /^((?:(?!(\n))[\s]))+|((?:(?!(\n))[\s]))+$/gm;

        // instead of trimming the string, we replace every white space character
        // at the beginning or at the end of the string with an empty character
        // except the new line character (in case a new line is at the beginning
        // or at the end of the string)
        const trimmed = part.replace(regNewLine, '');
        const isBold = this.boldTextPattern.test(trimmed);

        const withoutAsterisks = isBold
          ? trimmed.slice(1, trimmed.length - 1)
          : trimmed;

        return {
          // ends with blank to separate text-parts in view
          value: `${withoutAsterisks} `,
          isBold
        };
      });
    }
  }
}
