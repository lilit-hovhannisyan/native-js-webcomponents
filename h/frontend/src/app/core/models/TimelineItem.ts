export class TimelineItem {
  public timestamp: string;
  public activity: string;
  public initiator: any;
}
