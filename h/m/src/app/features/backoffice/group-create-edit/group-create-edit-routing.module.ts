import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupCreateEditComponent } from './group-create-edit.component';

const routes: Routes = [{ path: '', component: GroupCreateEditComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupCreateEditRoutingModule {}
