import { Injectable } from '@angular/core';
import { forkJoin, Observable, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ContactRoleRepositoryService } from '../repositories/contact-role-repository.service';
import { ContactTypeRepositoryService } from '../repositories/contact-type-repository.service';
import { IContact, ContactKey } from '../../models/resources/IContact';
import { IContactType } from '../../models/resources/IContactType';
import { IContactRole } from '../../models/resources/IContactRole';
import { ContactRepositoryService } from '../repositories/contact-repository.service';
import { CompanyKey } from '../../models/resources/ICompany';
import { RepoParams } from '../../abstracts/abstract.repository';
import { IWording } from '../../models/resources/IWording';

export interface IContactFull extends IContact {
  role: IContactRole;
  type: IContactType;
}

export interface IContactRow extends IContactFull {
  roleName: IWording;
  typeName: IWording;
}

@Injectable({
  providedIn: 'root'
})
export class ContactMainService {

  constructor(
    private contactRepo: ContactRepositoryService,
    private roleRepo: ContactRoleRepositoryService,
    private typeRepo: ContactTypeRepositoryService,
  ) { }

  private getCompanyFilter(companyId): (x: IContact[]) => IContact[] { // This fuction returns a filter function!
    return (contacts) => contacts.filter(c => c.companyId === companyId);
  }

  private getContactFull(
    contact: IContact,
    roles: IContactRole[],
    types: IContactType[]
  ): IContactFull {
    const role = roles.find(r => r.id === contact.roleId);
    const type = types.find(t => t.id === contact.typeId);
    return { ...contact, role, type };
  }

  private getContactFullList(
    contacts: IContact[],
    roles: IContactRole[],
    types: IContactType[]
  ): IContactFull[] {
    return contacts.map(contact => {
      return this.getContactFull(contact, roles, types);
    });
  }

  private composeContact(
    contact$: Observable<IContact>,
    roles$: Observable<IContactRole[]>,
    types$: Observable<IContactType[]>,
  ): Observable<IContactFull> {
    return forkJoin([contact$, roles$, types$]).pipe(
      map((res) => this.getContactFull(...res))
    );
  }

  private composeContactList(
    contacts$: Observable<IContact[]>,
    roles$: Observable<IContactRole[]>,
    types$: Observable<IContactType[]>,
  ): Observable<IContactFull[]> {
    return forkJoin([contacts$, roles$, types$]).pipe(
      map((res) => this.getContactFullList(...res))
    );
  }

  public fetchAll(params: RepoParams<IContact>): Observable<IContact[]> {
    return this.composeContactList(
      this.contactRepo.fetchAll(params),
      this.roleRepo.fetchAll({}),
      this.typeRepo.fetchAll({}),
    );
  }

  public fetchOne(id: ContactKey, params: RepoParams<IContact>): Observable<IContact> {
    return this.composeContact(
      this.contactRepo.fetchOne(id, params),
      this.roleRepo.fetchAll({}),
      this.typeRepo.fetchAll({}),
    );
  }

  public getStream(params: { companyId: CompanyKey }): Observable<IContactFull[]> {
    return forkJoin([
      this.contactRepo.fetchAll({ queryParams: params }),
      this.roleRepo.fetchAll({}),
      this.typeRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.contactRepo.getStream().pipe(map(this.getCompanyFilter(params.companyId))),
        this.roleRepo.getStream(),
        this.typeRepo.getStream()
      ])),
      map(res => this.getContactFullList(...res))
    );
  }

  public toRow(contact: IContactFull): IContactRow {
    const { role, type } = contact;
    return ({
      ...contact,
      typeName: type.displayLabel,
      roleName: role.displayLabel
    });
  }

  public toRows(contacts: IContactFull[]): IContactRow[] {
    return contacts.map((contact) => this.toRow(contact));
  }
}
