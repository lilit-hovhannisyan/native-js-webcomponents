# Material Exchange Logger

version 0.0.1

## Instructions to use

### 1

In Root module (Core or App) add this object into providers array ` { appVersion: "0.0.1", trackerEnabled: true }` where
appVersion should be the version of the frontend client app and it should be string, and boolean value for trackerEnabled if this value is set to false it will not track any logs.
Also optional you can provide `enableDebugging: boolean` which will print logs in the console
ex:

```javascript
providers: [
  {
    provide: 'tracker-config',
    useValue: { appVersion: '0.0.1', trackerEnabled: true }
  }
];
```

### 2

import `trackingSystemInterceptor` from `PATH_TO_FOLDER/interceptors/trackingSystemInterceptor` and add it to the apps interceptors array `{ provide: HTTP_INTERCEPTORS, useClass: trackingSystemInterceptor, multi: true }`

### 3

Each component you want to track should extend `BaseLoggerComponent` from `tracking-system`

### 4

Each extended component should have `componentVersion: string` property specified

### 5

Each extended component should import `Injector` from `@angular/core` provide it to it's `constructor(injector: Injector)`
and in the constructor call the super with the injector as an argument `super(injector)`

### 6

If in the extended component you have `ngOnDestroy` then you should change it's return type to `object` do your cleanup then return `super.ngOnDestroy()`
ex: `ngOnDestroy(): object { return super.ngOnDestroy()}`

### 7

if we need to add header to loggerService which will make call to data tracking system backend we should use  
`loggerService.appendToHeaders` function, which expects 2 parameters `(name: string, value: string)` if the header with that name
already exists it will be replaced
also we can delete the header with `loggingService.removeFromTheHeaders` which expects 1 parameter `(name: string)`

### 8 optional

you can provide within the component `loggingConfig: ConfigI` property `PATH_TO_FOLDER/interfaces/ConfigI` and specify the value for `skipOption` which can have as a value one of enum values specified in the `SkipOptions` import `PATH_TO_FOLDER/enums/SkipOptions`
ex. `loggingConfig: ConfigI = { skipOption: SkipOptions.DEFAULT };`
`SkipOptions.DEFAULT` is the default value so it may be omitted
other options:
`SkipOptions.SKIP_SELF` will skip that component
`SkipOptions.SKIP_TREE` will log events in that component but will skip the rest of that tree

### Data which will be sent

1. each time I will send you an array of logs
2. logs in the array may be with this 5 types
   2.1 ROUTER_LOADED_COMPONENT
   2.2 ROUTER_NAVIGATION_START
   2.3 REQUEST
   2.4 RESPONSE
   2.5 CLICK

# ROUTER_LOADED_COMPONENT will be with this type

```javascript
{
  eventType: string = "ROUTER_LOADED_COMPONENT",
  sessionId: string,
  appVersion: string,
  clientDate: string,
  serverDate: string,
  orderNumber: integer
  data: {
    componentName: string;
  };
  userActionId: string | null
}
```

# ROUTER_NAVIGATION_START will be with this type

```javascript
{
  eventType: string = "ROUTER_NAVIGATION_START",
  sessionId: string,
  appVersion: string,
  loggerClientVersion: string,
  clientDate: string,
  serverDate: string,
  orderNumber: integer
  data: {
    url: string;
  };
  userActionId: string | null;
}

```

# REQUEST will be with this type

```javascript
{
  eventType: string = "REQUEST",
  sessionId: string,
  appVersion: string,
  loggerClientVersion: string,
  clientDate: string,
  serverDate: string,
  orderNumber: integer
  requestId: string;
  data: {
    method: string;
    urlWithParams: string;
    body: unknown
  };
  userActionId: string | null;
}

```

# RESPONSE will be with this type

```javascript
{
  eventType: string = "RESPONSE",
  sessionId: string,
  appVersion: string,
  loggerClientVersion: string,
  clientDate: string, // Timestamp
  serverDate: string, // Timestamp
  orderNumber: integer
  requestId: string;
  data: {
    method: string;
    params: string;
    status: string; // "succeeded" || "failed"
    elapsedTime: string;
    body: unknown; // body of the response type is unknown
  };
}

```

# CLICK will be with this type

```javascript
{
  eventType: string = "CLICK",
  sessionId: string,
  appVersion: string,
  loggerClientVersion: string,
  clientDate: string,
  serverDate: string,
  orderNumber: integer
  componentsData: Array<{
    componentName: string;
    version: string;
    props: Array<{ key: string; value: unknown }>; // value could be any type
  }>;
  clientData: {
    screenResolution: string;
    userAgent: string;
    cookieEnabled: boolean;
  };
  eventData: {
    browserSize: {
      width: string;
      height: string;
    };
    path: string;
    innerText: string;
    cssClasses: string;
    clickCoordinates: {
      clientX: string;
      clientY: string;
    };
  };
  userActionId: string | null;
}

```
