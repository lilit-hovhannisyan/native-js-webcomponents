import { Component, OnInit, Input } from '@angular/core';
import { MandatorVoucherTypeRepositoryService } from 'src/app/core/services/repositories/mandator-voucher-type-repository.service';
import { IMandatorVoucherType } from 'src/app/core/models/resources/IMandatorVoucherType';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { IVoucherType } from 'src/app/core/models/resources/IVoucherType';
import { map } from 'rxjs/operators';
import { VoucherTypeRepositoryService } from 'src/app/core/services/repositories/voucher-type-repository.service';
import { combineLatest, Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { getConfig } from 'src/app/core/constants/nv-grid-configs/mandator-voucher-types-detail-view.config';
import { wording } from 'src/app/core/constants/wording/wording';
@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class MandatorVoucherTypesDetailViewComponent implements OnInit {

  @Input() public mandatorId: number;
  @Input() public closeModal: () => void;
  public form: FormGroup;
  public currentStep = 0;
  public dataSource$: Observable<IVoucherType[]>;
  public config: NvGridConfig;
  public wording = wording;

  constructor(
    private mandatorVoucherTypeRepo: MandatorVoucherTypeRepositoryService,
    private fb: FormBuilder,
    private voucherTypeRepo: VoucherTypeRepositoryService
  ) { }

  public ngOnInit() {
    this.initConfig();
    this.initForm();

    this.dataSource$ = combineLatest([
      this.voucherTypeRepo.fetchAll({}),
      this.mandatorVoucherTypeRepo.fetchAll({ queryParams: { mandatorId: this.mandatorId } })
    ]).pipe(
      map(([voucherTypes, _mandatorVoucherTypes]) => {

        // This filter is neccessary, as long as queryParams do not filter on server-side
        const mandatorVoucherTypes = _mandatorVoucherTypes.filter(mvt => mvt.mandatorId === this.mandatorId);

        return voucherTypes.filter(vt => {
          // only take voucherTypes that are not connected to this mandator already
          return vt.isActive && !mandatorVoucherTypes.find(mvt => mvt.voucherTypeId === vt.id);
        });
      }),
    );
  }

  private initForm(mandatorVoucherType: IMandatorVoucherType = {} as IMandatorVoucherType) {
    this.form = this.fb.group({
      mandatorId: [this.mandatorId, Validators.required],
      voucherTypeId: [mandatorVoucherType.voucherTypeId, Validators.required],
      id: [mandatorVoucherType.id],
      isAutomaticNumbering: [mandatorVoucherType.isAutomaticNumbering]
    });
  }

  public selectRow(voucherType?: IVoucherType): void {
    if (!voucherType) { return; }
    this.form.controls.voucherTypeId.patchValue(voucherType.id);
  }

  public stepBack() {
    --this.currentStep;
  }

  public stepForward() {
    ++this.currentStep;
  }

  public save() {
    const { value } = this.form;
    this.mandatorVoucherTypeRepo
      .create(value, {}).subscribe(() => this.closeModal());
  }

  public rowSelect(voucherTypes: IVoucherType[]): void {
    if (!voucherTypes.length) { return; }
    this.selectRow(voucherTypes[0]);
  }

  private initConfig() {
    const config = getConfig();
    config.rowSelectionType = 0;
    this.config = config;
  }
}
