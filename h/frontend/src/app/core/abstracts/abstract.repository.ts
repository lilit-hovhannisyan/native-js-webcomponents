import { cleanUrl, sortByKey } from './../helpers/general';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IResource, ResourceKey } from '../models/resources/IResource';
import { HttpGet } from '../models/http/http-get';
import { IDataResponseBody } from '../models/responses/IDataResponseBody';
import { IResourceCollection } from '../models/resources/IResourceCollection';
import { urlConcat } from '../helpers/general';
import { HttpPost } from '../models/http/http-post';
import { DataCarrier } from '../models/DataCarrier';
import { HttpPut } from '../models/http/http-put';
import { HttpDelete } from '../models/http/http-delete';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { map, tap, flatMap } from 'rxjs/operators';
import { ResourceTrackerService, TTL_SHORT } from 'src/app/shared/services/resource-tracker.service';
import { RepositoryService } from '../services/repository.service';
import { filter, convertToMap } from 'src/app/shared/helpers/general';
// import { filter } from 'lodash';

export interface RepoParams<Entity extends IResource<ResourceKey>> {
  cloneMode?: boolean;
  useCache?: boolean;
  baseUrl?: string;
  meta?: { [key: string]: any };
  errorHandler?: (err: HttpErrorResponse) => any;
  /**
   * pass query params to let the server filter the entities
   * included in the response.
   */
  queryParams?: {};
  /**
   * provide filter params to filter entities locally in the frontend
   * after the response has been received.
   */
  filter?: Partial<Entity>;
  /**
   * Sometimes when with one action we send several requests to the backend, we don't want
   * to see notification for each request at once, but instead we want to see only one
   * notification for an action, so we can pass this parameter as 'true', with a request
   * and hide it's notification.
   */
  isNotificationHidden?: boolean;
  loadingIndicatorHidden?: boolean;
}

export interface IGetStreamOptions {
  onlyActive: boolean;
}

/**
 * This class is parent class for all of other classes which will be used as
 * ICrudRepository It provides create, update, delete, fetch one IResource data object or
 * fetch all IResource data object methods
 */
@Injectable()
export abstract class AbstractRepository<
  Entity extends IResource<ResourceKey>,
  DTO extends IResource<ResourceKey>,
  EntityCreate,
  ARepoParams extends RepoParams<Entity>,
  Key extends ResourceKey,
  EntityLookup extends IResource<ResourceKey> = IResource<ResourceKey>,
  > {


  protected streamSubject = new BehaviorSubject<Entity[]>([]);
  protected stream$: Observable<Entity[]> = this.streamSubject.asObservable();
  protected streamLookupSubject: BehaviorSubject<EntityLookup[]>
    = new BehaviorSubject<EntityLookup[]>([]);
  protected streamLookup$: Observable<EntityLookup[]> = this.streamLookupSubject
    .asObservable();
  protected abstract resourceType: string;
  protected resourceTTL = TTL_SHORT;
  protected useCachePerDefault = false;
  protected resourceTracker: ResourceTrackerService;
  protected sortKey = 'id';
  protected sortAsce = false;

  public abstract getBaseUrl(repoParams: ARepoParams): string;

  protected abstract postProcess(data: DTO): Entity;

  constructor(
    protected repositoryService: RepositoryService,
  ) {
    this.repositoryService.authenticationService
      .isLoggedInSubject$
      .subscribe(isLoggedIn => !isLoggedIn && this.clearStream());
    this.resourceTracker = repositoryService.resourceTracker;
  }

  private getExpiryDate(): Date {
    return new Date(Date.now() + this.resourceTTL);
  }

  public fetchAll(options: ARepoParams): Observable<Entity[]> {
    const url = cleanUrl(options.baseUrl || this.getBaseUrl(options));

    let source: Observable<Entity[]>;
    if (options.useCache || (options.useCache === undefined && this.useCachePerDefault)) {
      const cacheItem = this.resourceTracker.getCollection<Entity>(this.resourceType, url);
      if (cacheItem.isHit) {
        source = of(cacheItem.value);
      }
    }

    if (!source) { // cache either not used or no hit
      const request = new HttpGet(url, {
        loadingIndicatorHidden: options.loadingIndicatorHidden,
        init: {
          params: options.queryParams,
        }
      });

      source = this.repositoryService.httpClient.request(request).pipe(
        map((response: HttpResponse<IDataResponseBody<IResourceCollection<DTO>>>) => response.body.data.items),
        map((items) => items.map((dto) => this.postProcess(dto))),
        map((items) => items.sort(sortByKey(this.sortKey, this.sortAsce))),
        tap(entities => {
          this.resourceTracker.saveCollection(this.resourceType, entities, url, this.getExpiryDate());
        }),
        tap((entities) => options.queryParams ? this.addToStream(entities) : this.updateStream(entities)),
      );
    }

    // regardless of whether the source is the API or our cache,
    // we filter the result before we return it if a filter has been provided
    return options && options.filter
      ? source.pipe(map(entities => filter(entities, options.filter)))
      : source;
  }

  public getStream(filterParams?: Partial<Entity>): Observable<Entity[]> {
    return filterParams
      ? this.stream$.pipe(map(entities => filter(entities, filterParams)))
      : this.stream$;
  }

  public getStreamValue(): Entity[] {
    return this.streamSubject.getValue();
  }

  public fetchOne(id: Key, options: ARepoParams): Observable<Entity> {
    const baseUrl = options.baseUrl || this.getBaseUrl(options);
    const url = cleanUrl(urlConcat([baseUrl, id]));

    if (options.useCache || (options.useCache === undefined && this.useCachePerDefault)) {
      const cacheItem = this.resourceTracker.getOne<Entity>({ _type: this.resourceType, id: id });
      if (cacheItem.isHit) {
        return of(cacheItem.value);
      }
    }

    const request = new HttpGet(url, {
      entityId: id.toString(),
      cloneMode: options.cloneMode,
      init: {
        params: options.queryParams,
      }
    });


    if (options.errorHandler) {
      request.setErrorHandler(options.errorHandler);
    }

    const request$ = this.repositoryService.httpClient.request(request).pipe(
      map((responseBody: HttpResponse<IDataResponseBody<DTO>>) => responseBody.body.data),
      map((dto) => this.postProcess(dto)),
      tap(resource => this.resourceTracker.saveOne(resource, this.getExpiryDate())),
      tap(entity => this.addToStream([entity])),
    );

    return request$;
  }

  // displayGlobalErrors(e: any) {
  //   if (e.error && e.error.globalErrors && e.error.globalErrors.length) {
  //     e.error.globalErrors.forEach((globalError: { message: string }) => {
  //       // TODO remove this length-check. as soon as the backend gets fixed.
  //       // currently every error includes a globalError with message: ERROR_CODE
  //       if (globalError.message.length > 3) {
  //         // TODO create interface for the globalError
  //         this.repositoryService.notificationsService.notify(
  //           NotificationType.Error,
  //           wording.general.error,
  //           // {globalError.message
  //         );
  //       }
  //     });
  //   }
  // }

  /**
   * Creates a new resource.
   *
   * **Note:** The caches for all collections are cleared since we do
   * not know how this newly added resource affects existing collections,
   * i.e. whether it is included in a collection or not.
   */
  public create(data: EntityCreate, options: ARepoParams): Observable<Entity> {
    const url = cleanUrl(options.baseUrl || this.getBaseUrl(options));
    // TODO maybe remove this line once isActive is implemented in all detailViews ( NAVIDO-558 )
    const _data = { isActive: true, ...data as Object };
    const request = new HttpPost<DataCarrier>(
      url,
      _data,
      {
        isNotificationHidden: options.isNotificationHidden,
        loadingIndicatorHidden: options.loadingIndicatorHidden,
        init: {
          params: options.queryParams,
        }
      },
    );
    // By returning false, we avoid the error
    // to bubble up, while we can still catch it within the ResourceDetailView.
    // That way we can add a custom form error handler.
    // request.setErrorHandler((e) => {
    //   this.displayGlobalErrors(e);
    //   return false;
    // });

    return this.repositoryService.httpClient.request(request).pipe(
      map((res: HttpResponse<IDataResponseBody<DTO>>) => res.body.data),
      map((dto) => this.postProcess(dto)),
      tap(dto => {
        this.resourceTracker.deleteCollectionsByType(this.resourceType);
        this.resourceTracker.saveOne(dto);
      }),
      tap(entity => this.addToStream([entity])),
    );
  }

  /**
   * Updates (= completely replaces) an existing resource.
   *
   * Note: The caches for all collections are cleared since we do
   * not know how this newly added resource affects existing collections,
   * i.e. whether it is included in a collection or not.
   */
  public update(resource: DTO, options: ARepoParams): Observable<Entity> {
    const baseUrl = options.baseUrl || this.getBaseUrl(options);
    // TODO maybe remove line below once isActive is implemented in all detailViews ( NAVIDO-558 )
    const _resource = { isActive: true, ...resource as Object } as any as DTO;
    const url = cleanUrl(urlConcat([baseUrl, _resource.id]));
    const request = new HttpPut(
      url,
      _resource,
      {
        isNotificationHidden: options.isNotificationHidden,
        loadingIndicatorHidden: options.loadingIndicatorHidden,
        init: {
          params: options.queryParams,
        }
      }
    );
    // By returning false, we avoid the error
    // to bubble up, while we can still catch it within the ResourceDetailView.
    // That way we can add a custom form error handler.
    // request.setErrorHandler((e) => {
    //   this.displayGlobalErrors(e);
    //   return false;
    // });

    return this.repositoryService.httpClient.request(request).pipe(
      map((res: HttpResponse<IDataResponseBody<DTO>>) => res.body.data),
      map((dto) => this.postProcess(dto)),
      tap(dto => {
        this.resourceTracker.deleteCollectionsByType(this.resourceType);
        this.resourceTracker.saveOne(dto);
      }),
      tap(entity => this.addToStream([entity]))
    );
  }

  /**
   * Deletes (= completely removes) an existing resource.
   *
   * Note: The caches for all collections are cleared since we do
   * not know how this newly added resource affects existing collections,
   * i.e. whether it is included in a collection or not.
   */
  public delete(id: Key, options: ARepoParams): Observable<void> {
    const baseUrl = options.baseUrl || this.getBaseUrl(options);
    const url = cleanUrl(urlConcat([baseUrl, id]));
    const request = new HttpDelete(
      url,
      {
        isNotificationHidden: options.isNotificationHidden,
        loadingIndicatorHidden: options.loadingIndicatorHidden,
        init: {
          params: options.queryParams,
        }
      }
    );
    return this.repositoryService.httpClient.request<void>(request).pipe(
      tap(() => this.removeFromStream(id)),
      tap(dto => {
        this.resourceTracker.deleteCollectionsByType(this.resourceType);
        this.resourceTracker.deleteOne({ _type: this.resourceType, id });
      }),
      map(() => undefined)
    );
  }

  protected addToStream = (entities: Entity[]): void => {
    const stream: Entity[] = this.streamSubject.getValue();
    this.streamSubject.next(this.getNewStream<Entity>(entities, stream));
  }

  protected updateStream = (entities: Entity[]): void => {
    this.streamSubject.next(entities);
  }

  public removeFromStream(itemId: Key) {
    const streamItems = this.streamSubject.getValue();
    const nextStreamItems = streamItems.filter(_item => _item.id !== itemId);
    this.streamSubject.next(nextStreamItems);
  }

  public clearStream() {
    this.streamSubject.next([]);
  }

  public getPropertyById = (id: number, property: string): string | number | undefined => {
    const foundItem = this.getStreamValue()
      .find(item => item.id === id);
    return foundItem
      ? foundItem[property]
      : id; // if not found, returns id for validations
  }

  protected addToLookupStream = (entities: EntityLookup[]): void => {
    const stream: EntityLookup[] = this.streamLookupSubject.getValue();
    this.streamLookupSubject.next(this.getNewStream<EntityLookup>(entities, stream));
  }

  protected updateLookupStream = (entities: EntityLookup[]): void => {
    this.streamLookupSubject.next(entities);
  }

  public getLookupStream(filterParams?: Partial<EntityLookup>): Observable<EntityLookup[]> {
    return filterParams
      ? this.streamLookup$.pipe(map(entities => filter(entities, filterParams)))
      : this.streamLookup$;
  }

  public getLookupStreamValue(): EntityLookup[] {
    return this.streamLookupSubject.getValue();
  }

  private getNewStream<T extends IResource<ResourceKey>>(entities: T[], stream: T[]): T[] {
    const entitiesMap: Map<ResourceKey, T> = convertToMap<T, ResourceKey>(entities, 'id');
    const unTouchedEntities: T[] = stream.filter(entity => !entitiesMap.get(entity.id));

    return [...unTouchedEntities, ...entities].sort(sortByKey(this.sortKey, this.sortAsce));
  }

  public fetchLookup(id: Key, options: ARepoParams): Observable<EntityLookup> {
    return this.fetchLookups({ ...options, queryParams: { id } }).pipe(
      flatMap(entities => of(entities.length === 1
        ? entities[0]
        : {} as EntityLookup
      ))
    );
  }

  public fetchLookups(options: ARepoParams): Observable<EntityLookup[]> {
    const url = `${cleanUrl(options.baseUrl || this.getBaseUrl(options))}/lookups`;
    const request = new HttpGet(url, { init: { params: options.queryParams } });
    const source: Observable<EntityLookup[]> = this.repositoryService.httpClient
      .request(request)
      .pipe(
        map((
          response: HttpResponse<IDataResponseBody<IResourceCollection<EntityLookup>>>
        ) => {
          return response.body.data.items;
        }),
        map((items) => items.sort(sortByKey(this.sortKey, this.sortAsce))),
        tap((entities) => options.queryParams
          ? this.addToLookupStream(entities)
          : this.updateLookupStream(entities)),
      );

    return options && options.filter
      ? source.pipe(map(entities => filter(entities, options.filter)))
      : source;
  }
}
