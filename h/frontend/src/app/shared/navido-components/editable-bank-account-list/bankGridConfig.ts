import { Operations } from './../../../core/models/Operations';
import { NvGridButtonsPosition, NvGridConfig, NvFilterControl, NvColumnDataType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IBankAccountRow } from 'src/app/core/models/resources/IBankAccount';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';


export const getBankGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean,
  customFormats: { currency: (bankAccount: IBankAccountRow) => string }
): NvGridConfig => {
  return ({
    gridName: 'bankAccountGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    showPaging: true,
    hideRefreshButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'bankName',
        title: wording.accounting.bank,
        width: 130,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        }
      },
      {
        key: 'bic',
        title: wording.accounting.bicSwift,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        },
      },
      {
        key: 'currencyLabel',
        title: wording.accounting.currency,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        },
        customFormatFn: customFormats.currency,
      },
      {
        key: 'iban',
        title: wording.accounting.iban,
        width: 100,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        }
      },
      {
        key: 'accountNo',
        title: wording.accounting.accountNo,
        width: 135,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        }
      },
      {
        key: 'forInvoices',
        title: wording.accounting.invoices,
        width: 100,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'forPayments',
        title: wording.accounting.payments,
        width: 100,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'forAccountStatements',
        title: wording.accounting.accountStatements,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'viaBank',
        title: wording.accounting.viaBank,
        width: 100,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        },
        hidden: true,
      },
      {
        key: 'viaAccount',
        title: wording.accounting.viaAccount,
        width: 100,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        },
        hidden: true,
      },
      {
        key: 'viaBic',
        title: wording.accounting.viaBic,
        width: 100,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        },
        hidden: true,
      },
      {
        key: 'viaBankCode',
        title: wording.accounting.viaBankCode,
        width: 100,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText
        },
        hidden: true,
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: 'Edit Bank',
        name: 'editBank',
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: actions.edit
      },
      {
        icon: 'delete',
        description: 'Delete Bank',
        name: 'deleteBank',
        hidden: !canPerform(Operations.DELETE),
        func: actions.delete
      }
    ],
    toolbarButtons: [
      {
        title: wording.accounting.addNewBank,
        hidden: () => !canPerform(Operations.CREATE),
        icon: 'plus-circle-o',
        func: actions.add
      }
    ],
  });
};
