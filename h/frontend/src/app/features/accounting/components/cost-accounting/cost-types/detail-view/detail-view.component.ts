import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ResourceDetailView } from '../../../../../../core/abstracts/ResourceDetailView';
import { ICostType, ICostTypeCreate, ICostTypeFull } from '../../../../../../core/models/resources/ICostTypes';
import { Validators } from '../../../../../../core/classes/validators';
import { CostTypeRepositoryService } from '../../../../../../core/services/repositories/cost-type-repository.service';
import { BaseComponentService } from '../../../../../../core/services/base-component.service';
import { url } from '../../../../../../core/constants/sitemap/sitemap-entry';
import { sitemap } from '../../../../../../core/constants/sitemap/sitemap';
import { ISelectOption } from '../../../../../../core/models/misc/selectOption';
import { opexTypesOptions } from '../../../../../../core/models/enums/opex-types';
import { CostTypeMainService } from '../../../../../../core/services/mainServices/cost-type-main.service';
import { Language } from 'src/app/core/models/language';
import { setTranslationSubjects } from '../../../../../../shared/helpers/general';
import { wording } from '../../../../../../core/constants/wording/wording';
import { AccountCostTypeMainService } from '../../../../../../core/services/mainServices/account-cost-type-main.service';
import { IAccountCostType } from '../../../../../../core/models/resources/IAccountCostType';

@Component({
  selector: 'nv-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CostTypesDetailViewComponent
  extends ResourceDetailView<ICostType, ICostTypeCreate>
  implements OnInit {

  protected repoParams = {};
  public form: FormGroup;
  protected resourceDefault = { isOpex: false, opexType: 0, isActive: true } as ICostType;
  public routeBackUrl = url(sitemap.accounting.children.costAccounting.children.costTypes);

  public accountingRelationOptions: ISelectOption[] = this.costTypeService.accountingRelationsOptions;
  public opexTypeOptions: ISelectOption[] = opexTypesOptions;
  private language: Language;
  private id: number;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private costTypeService: CostTypeMainService,
    private accountCostTypeMainService: AccountCostTypeMainService,
    costTypeRepo: CostTypeRepositoryService,
    baseComponentService: BaseComponentService,
  ) {
    super(costTypeRepo, baseComponentService);
  }

  public ngOnInit(): void {
    this.init();
    this.language = this.settingsService.language;

    const { id } = this.activatedRoute.snapshot.params;
    this.id = +id;
    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle: () => string = () => {
    const costType = this.costTypeService.toRow(this.resource as ICostTypeFull);
    return `${this.resource.no} ${costType.displayLabel[this.language]}`;
  }

  public init(costType: ICostType = this.resourceDefault): void {
    this.initForm(this.fb.group({
      no: [costType.no, [Validators.required, Validators.integerBetween(1, 99999)]],
      labelDe: [costType.labelDe, [Validators.required, Validators.maxLength(150)]],
      labelEn: [costType.labelEn, [Validators.required, Validators.maxLength(150)]],
      accountingRelation: [costType.accountingRelation, [Validators.required]],
      remark: [costType.remark],
      isOpex: [costType.isOpex],
      opexType: [costType.opexType],
      isActive: [costType.isActive],
    }));
  }

  public submit(): void {
    if (!this.form.valid || this.isCostTypeRequired()) {
      return;
    }

    const { value } = this.form;

    this.creationMode
      ? this.createResource(value)
      : this.updateResource({ ...this.resource, ...value });
  }

  public isCostTypeRequired(): boolean {
    const isOpex = this.form.get('isOpex').value;
    const opexType = this.form.get('opexType').value;

    return isOpex && !opexType;
  }

  public onIsOpexChange(checkboxState: boolean): void {
    this.form.get('opexType').patchValue(Number(checkboxState));
  }

  public onIsActiveChange(checkboxState: boolean) {
    if (checkboxState) {
      return;
    }

    this.accountCostTypeMainService.getAccountRelatedToCostType(this.id)
      .subscribe(ctRelAccounts => {
        if (ctRelAccounts.length) {
          this.showCtDeactivationConfirmationBox(ctRelAccounts);
        }
      });
  }

  private showCtDeactivationConfirmationBox(relatedAccountList: IAccountCostType[]) {
    // todo don't remove bellow comments, they will be implemented
    /**
     * show at max 3 relations
     */
    /*let relAccounts = '';
    relatedAccountList.forEach((acc, index) => {
      if ( index > 2) {
        return;
      }
      relAccounts += acc.bookingAccountId + ' ';
    });*/

    this.confirmationService.confirm({
      okButtonWording: wording.general.ok,
      cancelButtonWording: null,
      wording: setTranslationSubjects(
        wording.accounting.ctDeactivationNotPossible,
        // { subject: relAccounts },
      ),
    }).subscribe(confirmed => {
      this.form.get('isActive').patchValue(true);
    });
  }
}
