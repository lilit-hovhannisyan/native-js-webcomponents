import { Injectable } from '@angular/core';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { CostUnitRepositoryService } from '../repositories/cost-unit-repository.service';
import { ICostUnit, ICostUnitFull, ICostUnitRow } from '../../models/resources/ICostUnit';
import { convertToMap } from 'src/app/shared/helpers/general';
import { CostUnitCategoryRepositoryService } from '../repositories/cost-unit-category-repository.service';
import { CostUnitCategoryKey, ICostUnitCategory } from '../../models/resources/ICostUnitCategory';

@Injectable({
  providedIn: 'root'
})
export class CostUnitMainService {
  constructor(
    private costUnitCategoryRepo: CostUnitCategoryRepositoryService,
    private costUnitRepo: CostUnitRepositoryService,
  ) { }

  public getStream(): Observable<ICostUnitFull[]> {
    return forkJoin([
      this.costUnitRepo.fetchAll({}),
      this.costUnitCategoryRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.costUnitRepo.getStream(),
        this.costUnitCategoryRepo.getStream()
          .pipe(map(costUnitCategories =>
            convertToMap<ICostUnitCategory, CostUnitCategoryKey>(costUnitCategories))),
      ])
      ),
      map(response => this.getMandatorCostUnitsFullList(...response))
    );
  }

  private getMandatorCostUnitsFullList(
    costUnits: ICostUnit[],
    costUnitCategories: Map<CostUnitCategoryKey, ICostUnitCategory>,
  ): ICostUnitFull[] {
    return costUnits.map(costUnit => {
      return this.getMandatorCostUnitFull(
        costUnit,
        costUnitCategories,
      );
    });
  }

  private getMandatorCostUnitFull(
    costUnit: ICostUnit,
    costUnitCategories: Map<CostUnitCategoryKey, ICostUnitCategory>,
  ): ICostUnitFull {
    return {
      ...costUnit,
      costUnitCategory: costUnitCategories.get(costUnit.costUnitCategoryId),
    };
  }

  public toRows(costUnits: ICostUnitFull[]): ICostUnitRow[] {
    return costUnits.map((costUnit) => this.toRow(costUnit));
  }

  public toRow(costUnit: ICostUnitFull): ICostUnitRow {
    const { costUnitCategory } = costUnit;

    return ({
      ...costUnit,
      costUnitCategoryName: costUnitCategory.displayLabel,
    });
  }
}
