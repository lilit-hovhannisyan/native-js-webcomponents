export interface MeEmptyContentI {
  title: string;
  comment: string;
  buttonText: string;
  emptyIconName: string;
}
