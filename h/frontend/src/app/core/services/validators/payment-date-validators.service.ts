import { ValidationErrors, AbstractControl } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { Injectable } from '@angular/core';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { dateFormatTypes } from 'src/app/core/models/dateFormat';
import * as moment from 'moment';
import { SettingsService } from 'src/app/core/services/settings.service';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { ToCurrentDateFormatPipe } from 'src/app/shared/pipes/to-current-date-format.pipe';
import { BookingResourceService } from '../booking-resource.service';
import { BookingFormService } from '../booking-form.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentDateValidatorsService {

  constructor(
    private bookingFormService: BookingFormService,
    private settingsService: SettingsService,
    private bookingResourceService: BookingResourceService,
  ) { }

  public validatePaymentDate = (control: AbstractControl): ValidationErrors | null => {
    const bookingForm = this.bookingFormService.bookingFormSubject.value;
    if (!control.value || !bookingForm) {
      return null;
    }
    const isValid = bookingForm
      && moment(control.value, [dateFormatTypes[this.settingsService.locale], moment.ISO_8601])
        .isSameOrAfter(
          moment(
            bookingForm.getRawValue().invoiceDate,
            [dateFormatTypes[this.settingsService.locale], moment.ISO_8601],
          ),
          'day'
        );
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      paymentDateIsLessThenInvoiceDate: {
        message: wording.accounting.paymentDateEqualOrAfterInvoiceDate
      }
    };
  }

  public paymentDateOfDiscount = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value || !control.parent) {
      return null;
    }

    const bookingAccountId = control.parent.get('bookingAccountId').value;
    const discountControl = control.parent.get('discount');

    if (discountControl.value == null) {
      return null;
    }

    let isZeroPercent: boolean;
    const discountsOfAccount = this.bookingResourceService
      .paymentConditionsOfAccounts
      .value[bookingAccountId] || [];

    if (discountsOfAccount.length === 0) {
      return null;
    }
    const foundDiscountIndex = discountsOfAccount
      .findIndex(discountOfAccount => discountOfAccount.percentage === +discountControl.value);

    isZeroPercent = foundDiscountIndex === discountsOfAccount.length - 1;
    const foundDiscount = discountsOfAccount[foundDiscountIndex];
    const nextDiscount = discountsOfAccount[foundDiscountIndex - 1];

    const isBeforeOrSameCurrentDiscount = !isZeroPercent
      ? moment(control.value).isSameOrBefore(foundDiscount.paymentDate)
      : true;
    const isAfterNextDiscount = nextDiscount
      ? moment(control.value).isAfter(nextDiscount.paymentDate)
      : true;
    const isValid = foundDiscount
      && discountControl.enabled
      && isBeforeOrSameCurrentDiscount
      && isAfterNextDiscount;
    if (isValid) {
      return null;
    }
    markControlAsTouched(control);
    return {
      paymentDateIsNotInRange: {
        message: setTranslationSubjects(
          wording.accounting.shouldUpdateDiscountFromPaymentDate, {
          END: new ToCurrentDateFormatPipe(this.settingsService)
            .transform(foundDiscount.paymentDate),
          START: nextDiscount
            ? new ToCurrentDateFormatPipe(this.settingsService)
              .transform(nextDiscount.paymentDate)
            : '-',
        }),
      }
    };
  }
}
