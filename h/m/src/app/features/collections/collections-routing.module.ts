import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BasePermissionGuard } from '@core-guards/base-permission.guard';
import { MePermissionTypes } from '@shared/me-permission-types';

import { CollectionsComponent } from './collections.component';

const routes: Routes = [
  {
    path: '',
    component: CollectionsComponent
  },
  {
    path: 'view/:id',
    data: {
      breadcrumb: {
        alias: 'collectionName'
      },
      permission: MePermissionTypes.COLLECTION_VIEW
    },
    loadChildren: () => import('@features/collection-view/collection-view.module').then((m) => m.CollectionViewModule),
    canActivate: [BasePermissionGuard]
  },
  {
    path: 'create',
    data: {
      // This should be tested will it work when change language...
      breadcrumb: 'Create Collection',
      permission: MePermissionTypes.COLLECTION_CREATE
    },
    loadChildren: () => import('@features/collection-create-edit/collection-create-edit.module').then((m) => m.CollectionCreateEditModule),
    canActivate: [BasePermissionGuard]
  },
  {
    path: 'edit/:id',
    data: {
      // This should be tested will it work when change language...
      breadcrumb: 'Edit Collection',
      permission: MePermissionTypes.COLLECTION_MODIFY
    },
    loadChildren: () => import('@features/collection-create-edit/collection-create-edit.module').then((m) => m.CollectionCreateEditModule),
    canActivate: [BasePermissionGuard]
  },
  {
    path: 'duplicate/:id',
    data: {
      breadcrumb: 'Duplicate Collection',
      permission: MePermissionTypes.COLLECTION_CREATE
    },
    loadChildren: () => import('@features/collection-create-edit/collection-create-edit.module').then((m) => m.CollectionCreateEditModule),
    canActivate: [BasePermissionGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollectionsRoutingModule {}
