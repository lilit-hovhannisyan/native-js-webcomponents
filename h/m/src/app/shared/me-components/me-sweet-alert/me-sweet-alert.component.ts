import { AfterViewInit, Component, EventEmitter, Inject, OnDestroy, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Subscription } from 'rxjs';

import { MeButtonI } from '../me-button/me-button.interface';

import { MeSweetAlertI, MeSweetAlertTypeEnum } from './me-sweet-alert.interface';

@Component({
  selector: 'me-sweet-alert',
  templateUrl: './me-sweet-alert.component.html',
  styleUrls: ['./me-sweet-alert.component.scss']
})
export class MeSweetAlertComponent implements AfterViewInit, OnDestroy {
  @ViewChild('sweetAlertForm') sweetForm!: NgForm;
  @Output() eventOccurs: EventEmitter<{ eventName: string; payload: MeSweetAlertI }> = new EventEmitter();

  cancelButton: MeButtonI = { color: 'secondary', disabled: false, rounded: false, type: 'regular' };
  submitButton: MeButtonI = { color: 'primary', disabled: false, rounded: false, type: 'regular' };
  confirmButton: MeButtonI = { color: 'primary', disabled: false, rounded: false, type: 'regular' };

  private _statusSubscritpion!: Subscription;

  constructor(@Inject(MAT_DIALOG_DATA) public data: MeSweetAlertI) {}

  ngAfterViewInit(): void {
    if (this.data.type.name === MeSweetAlertTypeEnum.input) {
      this._statusSubscritpion = this.sweetForm.form.statusChanges.subscribe((result) => {
        if (result === 'INVALID') {
          this.submitButton.disabled = true;
        } else {
          this.submitButton.disabled = false;
        }
      });
    }
  }

  customEvent(eventName: string, payload: MeSweetAlertI): void {
    this.eventOccurs.emit({ eventName, payload });
  }

  ngOnDestroy(): void {
    if (this._statusSubscritpion) {
      this._statusSubscritpion.unsubscribe();
    }
  }
}
