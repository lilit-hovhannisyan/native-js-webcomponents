import { Component, ElementRef, OnDestroy, Optional, Self, OnInit, Injector } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { FormGroup, ControlValueAccessor, NgControl, AbstractControl, FormControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';

import { ReplaySubject, Subject, Subscription } from 'rxjs';

import { MeAttributeComponentInterface } from '../me-attribute-component.interface';
import { MeSelectInputI } from './me-select-input.interface';
import { BaseLoggerComponent } from '@features/tracking-system';
import { takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './me-select-input.component.html',
  styleUrls: ['./me-select-input.component.scss'],
  providers: [
    {
      provide: MatFormFieldControl,
      useExisting: MeSelectInputComponent
    }
  ]
})
export class MeSelectInputComponent
  extends BaseLoggerComponent
  implements MeAttributeComponentInterface, ControlValueAccessor, MatFormFieldControl<string>, OnInit, OnDestroy {
  // variables for MatFormFieldControl interface
  static nextId = 0;
  componentVersion: string = '0.0.1';
  currentValue!: string | undefined;
  name: string = '';
  data!: MeSelectInputI;
  form!: FormGroup;
  private _placeholder!: string;
  private _required = false;
  private _disabled = false;
  private _valueChangeSubscription!: Subscription | undefined;
  stateChanges = new Subject<void>();
  focused = false;
  errorState = false;
  controlType = 'me-select-input';
  id = `me-select-input-${MeSelectInputComponent.nextId++}`;
  describedBy = '';

  filteredValues: ReplaySubject<{ displayName: string; value: string }[]> = new ReplaySubject(1);

  searchFilterCtrl: FormControl = new FormControl('');

  private _onDestroy: Subject<void> = new Subject<void>();

  // tslint:disable-next-line:no-any
  onChange = (_: any) => {};
  onTouched = () => {};

  get formControl(): AbstractControl | null {
    return this.form.get(this.data.internalName);
  }

  constructor(
    injector: Injector,
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,

    @Optional() @Self() public ngControl: NgControl
  ) {
    super(injector);
    _focusMonitor.monitor(_elementRef, true).subscribe((origin) => {
      if (this.focused && !origin) {
        this.onTouched();
      }
      this.focused = !!origin;
      this.stateChanges.next();
    });

    if (this.ngControl !== null) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit(): void {
    this.form = this.data.form;
    this.name = this.data.internalName;
    this.currentValue = this.formControl?.value;
    this.disabled = this.data.disabled;
    this._valueChangeSubscription = this.formControl?.valueChanges.subscribe((val: string) => {
      this.currentValue = val;
    });

    this.filteredValues.next(this.data.values.slice());

    this.searchFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => {
      this.filterValues();
    });
  }

  ngOnDestroy(): object {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
    if (this._valueChangeSubscription) {
      this._valueChangeSubscription.unsubscribe();
    }
    this._onDestroy.next();
    this._onDestroy.complete();
    return super.ngOnDestroy();
  }

  // methods required by MatFormFieldControl interface
  get empty(): boolean {
    return false;
  }

  get shouldLabelFloat(): boolean {
    return this.focused || !this.empty;
  }

  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  get required(): boolean {
    return this._required;
  }
  set required(value: boolean) {
    this._required = value;
    this.stateChanges.next();
  }

  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = value;

    setTimeout(() => {
      if (this.form) {
        this._disabled ? this.formControl?.disable() : this.formControl?.enable();
        this.stateChanges.next();
      }
    }, 0);
  }

  get value(): string {
    return this.formControl?.value;
  }
  set value(value: string) {
    this.formControl?.setValue(value);
    this.stateChanges.next();
  }

  filterValues(): void {
    if (!this.data.values) {
      return;
    }
    // get the search keyword
    let search = this.searchFilterCtrl.value;
    if (!search) {
      this.filteredValues.next(this.data.values.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredValues.next(this.data.values.filter((value) => value.displayName.toLowerCase().indexOf(search) > -1));
  }

  setDescribedByIds(ids: string[]): void {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(): void {}

  writeValue(value: string): void {
    this.value = value;
  }
  // tslint:disable-next-line:no-any
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  // tslint:disable-next-line:no-any
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
