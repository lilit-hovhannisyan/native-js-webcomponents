import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PaperViewStateService {
  public areDetailsButtonsVisibleSubject$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public areDetailsButtonsVisible$ = this.areDetailsButtonsVisibleSubject$.asObservable().pipe(delay(0));
}
