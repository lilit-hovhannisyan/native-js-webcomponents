import { Component, OnInit } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { IArticleType, IArticleTypeCreate } from 'src/app/core/models/resources/IArticleType';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { ArticleTypeRepositoryService } from 'src/app/core/services/repositories/article-type-repository.service';
import { ActivatedRoute } from '@angular/router';
import { Validators } from 'src/app/core/classes/validators';

@Component({
  selector: 'nv-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class ArticleTypesDetailViewComponent
  extends ResourceDetailView<IArticleType, IArticleTypeCreate>
  implements OnInit {

  public repoParams = {};
  public routeBackUrl = url(sitemap.technicalManagement.children.main.children.articleTypes);
  protected resourceDefault = { isActive: true } as IArticleType;

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    articleTypeRepo: ArticleTypeRepositoryService,
    baseComponentService: BaseComponentService
  ) {
    super(articleTypeRepo, baseComponentService);
  }

  public ngOnInit(): void {
    this.init();
    const { id } = this.route.snapshot.params;

    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => this.resource.abbreviation;

  public init(articleType: IArticleType = this.resourceDefault): void {
    this.initForm(this.fb.group({
      labelDe: [articleType.labelDe, [Validators.required, Validators.maxLength(50)]],
      labelEn: [articleType.labelEn, [Validators.required, Validators.maxLength(50)]],
      abbreviation: [articleType.abbreviation, [
        Validators.required,
        Validators.maxLength(5)
      ]],
      isSystemDefault: [articleType.isSystemDefault],
      isActive: [articleType.isActive],
    }));

    if (!this.isAdmin) {
      this.disableField('isActive');
    }
  }

  public submit(): void {
    if (!this.form.valid) { return; }

    const { value } = this.form;

    this.creationMode
      ? this.createResource(value)
      : this.updateResource({...this.resource, ...value});
  }

}
