import { NvFilterControl, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { Operations } from 'src/app/core/models/Operations';
import { Router } from '@angular/router';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { ICompany } from 'src/app/core/models/resources/ICompany';
import { wording } from 'src/app/core/constants/wording/wording';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

const companiesUrl = url(sitemap.system.children.misc.children.companies);

export const getGridConfig = (
  router: Router,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'companyGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'companyName',
        title: wording.accounting.companyName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'firstName',
        title: wording.general.firstName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'lastName',
        title: wording.general.lastName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'salutationLabel',
        title: wording.accounting.salutation,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'titleLabel',
        title: wording.system.title,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'taxNo',
        title: wording.accounting.taxNumber,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Select,
          selectValues: []
        }
      },
      {
        key: 'vatId',
        title: wording.accounting.vatId,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Select,
          selectValues: []
        }
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editCompany',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (company: ICompany) => {
          router.navigate([`${companiesUrl}/${company.id}`]);
        },

      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        hidden: () => !canPerform(Operations.CREATE),
        icon: 'plus-circle-o',
        func: () => router.navigate([`${companiesUrl}/new`])
      }
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
