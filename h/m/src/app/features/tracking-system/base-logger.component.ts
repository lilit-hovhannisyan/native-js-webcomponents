import { Component, ElementRef, Injector, Input, OnDestroy } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import ConfigI from './interfaces/config.interface';
import SkipOptions from './enums/config-values.enum';
import { LoggingService } from './services/logging.service';
import { PROPERTIES_TO_EXCLUDE, VALID_TAGS_FOR_INNER_TEXT } from './constants';
import ClickLogI from './interfaces/click-log.interface';
import ComponentDataI from './interfaces/component-data.interface';
import EventTypes from './enums/event-types.enum';

// tslint:disable-next-line:class-name
class REQUIRED_SUPER {}

@Component({
  selector: 'me-base',
  template: '',
  providers: []
})
export class BaseLoggerComponent implements OnDestroy {
  private static clickEventInstance: Event;
  private static eventTimestamp: number;

  public loggingService: LoggingService;
  private elementClickEventsSubscription!: Subscription;

  @Input() loggingConfig: ConfigI = {
    skipOption: SkipOptions.DEFAULT
  };
  @Input() componentVersion: string = '';

  constructor(injector: Injector) {
    this.loggingService = injector.get<LoggingService>(LoggingService);
    const elementRef = injector.get(ElementRef);

    if (!this.loggingService.enabled) {
      return;
    }
    // create observable that emits click events
    const clickEventsSource = fromEvent<Event>(elementRef.nativeElement, 'click', {
      capture: true
    });

    // subscribe to clicks
    this.elementClickEventsSubscription = clickEventsSource.subscribe((event: Event) => {
      if (this.loggingConfig.skipOption === SkipOptions.SKIP_SELF) {
        return;
      }
      const el: HTMLElement = event.target as HTMLElement;
      const componentName: string = this.constructor.name;
      const props: Array<{
        key: string;
        value: unknown;
      }> = this.getProperties();

      if (!this.componentVersion) {
        alert(`Provide componentVersion in: ${componentName}`);
      }

      /**
       * If in the one event loop tick we are getting several events that means it's the same event and we are catching it on
       * different parts of our app as we are listening on capture phase, so we are checking if reference is the same adding the click
       * event to the stack, otherwise creating new stack, with setTimeout we will log right after this tick, but added 100ms because
       * if for example after click we will load component and make get request on ngnInit I would like that event to be with our click
       */
      if (BaseLoggerComponent.clickEventInstance === event) {
        // const log = {
        // 	eventType: EventTypes.COMPONENT_DATA_WHEN_CLICKED,
        // 	data: {
        // 		componentName,
        // 		props
        // 	},
        // 	userActionId: this.loggingService.getUserActionId()
        // };
        // this.loggingService.emitLog(log);

        const data: ComponentDataI = {
          componentName,
          version: this.componentVersion,
          props
        };
        const userActionId: string | null = this.loggingService.getUserActionId();

        this.loggingService.addComponentDataToEventLog(userActionId, data);

        return;
      } else {
        // it's a new event

        /**
         * determine if click was user initiated or for example label emitted 2 user click events
         */
        const currentTimestamp = Date.now();
        if (!BaseLoggerComponent.eventTimestamp) {
          BaseLoggerComponent.eventTimestamp = currentTimestamp;
        } else {
          if (currentTimestamp - BaseLoggerComponent.eventTimestamp <= 100) {
            /**
             * this means it's click on label for example which emitted to events (same) with 2 targets so we will
             * get only first one and discard the rest
             */
            BaseLoggerComponent.eventTimestamp = currentTimestamp;
            return;
          }
          BaseLoggerComponent.eventTimestamp = currentTimestamp;
        }

        BaseLoggerComponent.clickEventInstance = event;
        this.loggingService.setUserActionId();

        /**
         * as it's new event gether tags array, innerText and classList
         */
        let innerText: string = '';
        if (VALID_TAGS_FOR_INNER_TEXT.includes(el.tagName)) {
          innerText = el.innerText || '';
        }
        const classList: DOMTokenList = el.classList;
        const path = this.getTagsPath(el);
        const tagsPath: string = path.reverse().join(' > ');

        // browser width and height
        const screenResolution: string =
          `${window.screen.width * window.devicePixelRatio}x${window.screen.height * window.devicePixelRatio}` || '';

        // browser width and height
        const browserSize = {
          width: `${window.innerWidth || document.body.clientWidth}px`,
          height: `${window.innerHeight || document.body.clientHeight}px`
        };

        const log: ClickLogI = {
          eventType: EventTypes.CLICK,
          componentsData: [
            {
              componentName,
              version: this.componentVersion,
              props
            }
          ],
          clientData: {
            screenResolution,
            userAgent: window.navigator.userAgent,
            cookieEnabled: window.navigator.cookieEnabled
          },
          eventData: {
            path: tagsPath,
            innerText,
            browserSize,

            cssClasses: classList?.value || '',
            clickCoordinates: {
              clientX: String((event as MouseEvent).clientX || '' + 'px'),
              clientY: String((event as MouseEvent).clientY || '' + 'px')
            }
          },
          userActionId: this.loggingService.getUserActionId()
        };

        this.loggingService.emitLog(log);

        // const componentLog = {
        // 	eventType: EventTypes.COMPONENT_DATA_WHEN_CLICKED,
        // 	data: {
        // 		componentName,
        // 		props
        // 	},
        // 	userActionId: this.loggingService.getUserActionId()
        // };
        // this.loggingService.emitLog(componentLog);

        /**
         * we are setting timeout in order to fix all events which will happen on one tic of capturing phase
         */
        setTimeout(() => {
          this.loggingService.deleteUserActionId();
        });
      }

      /**
       * log this event end stop propagation in order to not log further in the tree
       */
      if (this.loggingConfig.skipOption === SkipOptions.SKIP_TREE) {
        event.stopPropagation();
      }
      return;
    });
  }

  private getProperties(): Array<{ key: string; value: unknown }> {
    const props: Array<{ key: string; value: unknown }> = [];
    for (const key in this) {
      if (!PROPERTIES_TO_EXCLUDE.includes(key) && !this.checkIfPropertyIsADependency(this[key])) {
        try {
          const logEntry = {
            key,
            value: this[key]
          };
          JSON.stringify(logEntry);
          props.push(logEntry);
        } catch (err) {
          if (this.loggingService.enableDebugging) {
            console.warn('This is not valid property to track: ', this[key]);
          }
        }
      }
    }

    return props;
  }

  // tslint:disable-next-line:no-any
  private checkIfPropertyIsADependency(property: any): boolean {
    if (property?.constructor) {
      if (Object.keys(property.constructor).includes('decorators')) {
        return true;
      }
    }

    return false;
  }

  private getTagsPath(el: HTMLElement): Array<string> {
    let element: HTMLElement = el;
    const path: Array<string> = [element.tagName || 'no tagName'];

    while ((element.parentNode as HTMLElement)?.tagName) {
      element = element.parentNode as HTMLElement;
      path.push(element.tagName);
    }

    return path;
  }

  ngOnDestroy(): REQUIRED_SUPER {
    if (this.elementClickEventsSubscription) {
      this.elementClickEventsSubscription.unsubscribe();
    }
    return new REQUIRED_SUPER();
  }
}
