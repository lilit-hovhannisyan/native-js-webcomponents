import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { IShipyardType } from 'src/app/core/models/resources/IShipyardType';
import { ShipyardTypeRepositoryService } from 'src/app/core/services/repositories/shipyard-type-repository.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/shipyard-types.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';

@Component({
  templateUrl: './shipyard-types-list-view.component.html',
  styleUrls: ['./shipyard-types-list-view.component.scss']
})
export class ShipyardTypesListViewComponent implements OnInit {
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IShipyardType[]>;

  constructor(
    private shipyardTypeRepo: ShipyardTypeRepositoryService,
    private confirmationService: ConfirmationService,
    private authService: AuthenticationService,
    private router: Router,
    public gridConfigService: GridConfigService,
  ) { }
  public ngOnInit() {
    this.shipyardTypeRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.shipyardTypeRepo.getStream();

    const actions = {
      edit: (id) => this.router.navigate([`${this.router.url}/${id}`]),
      delete: this.deleteClicked,
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    this.gridConfig = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  public deleteClicked = (vessel: IShipyardType) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: vessel.label },
    }).subscribe(confirmed => confirmed && this.shipyardTypeRepo.delete(vessel.id, {}).subscribe());

  }

}
