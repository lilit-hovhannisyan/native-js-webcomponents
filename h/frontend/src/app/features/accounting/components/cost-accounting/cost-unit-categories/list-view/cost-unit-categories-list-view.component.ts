import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { wording } from 'src/app/core/constants/wording/wording';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { CostUnitCategoryRepositoryService } from 'src/app/core/services/repositories/cost-unit-category-repository.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { ICostUnitCategory } from 'src/app/core/models/resources/ICostUnitCategory';
import { getCostUnitCategoriesGridConfig } from 'src/app/core/constants/nv-grid-configs/cost-unit-categories.config';
import { CostUnitRepositoryService } from '../../../../../../core/services/repositories/cost-unit-repository.service';
import { ICostUnit } from 'src/app/core/models/resources/ICostUnit';

@Component({
  selector: 'nv-cost-unit-categories-list-view',
  templateUrl: './cost-unit-categories-list-view.component.html',
  styleUrls: ['./cost-unit-categories-list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CostUnitCategoriesListViewComponent implements OnInit {
  public dataSource$: Observable<ICostUnitCategory[]>;
  public gridConfig: NvGridConfig;
  private costUnits: ICostUnit[] = [];

  constructor(
    public gridConfigService: GridConfigService,
    private costUnitCategoryRepo: CostUnitCategoryRepositoryService,
    private router: Router,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private costUnitRepo: CostUnitRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.initNvGrid();
  }

  private initNvGrid(): void {
    const gridActions: IGridConfigActions = {
      edit: (row: ICostUnitCategory) => {
        this.router.navigate([`${this.router.url}/${row.id}`]);
      },
      delete: (row: ICostUnitCategory) => this.delete(row),
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    const canPerform: (operation: number) => boolean = (operation: number) => {
      return this.authService.canPerformOperationOnCurrentUrl(operation);
    };
    this.costUnitRepo.fetchAll({}).subscribe();
    this.costUnitRepo.getStream().subscribe(costUnits => {
      this.costUnits = costUnits;
      this.gridConfig = getCostUnitCategoriesGridConfig(
        gridActions,
        canPerform,
      );
    });

    this.costUnitCategoryRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.costUnitCategoryRepo.getStream();
  }

  private delete: (costUnitCategory: ICostUnitCategory) => void = (
    costUnitCategory: ICostUnitCategory
  ): void => {
    const isRelatedToCostUnit: boolean = !!this.costUnits
      .find(cu => cu.costUnitCategoryId === costUnitCategory.id);

    if (isRelatedToCostUnit) {
      this.confirmationService.confirm({
        okButtonWording: wording.general.ok,
        cancelButtonWording: null,
        wording: wording.accounting.deletionNotPossibleRelatedToCostUnits,
      }).subscribe();
      return;
    }

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: setTranslationSubjects(
        wording.general.areYouSureYouWantToDelete,
        { subject: costUnitCategory.displayLabel },
      ),
    }).subscribe(confirmed => {
      if (confirmed) {
        this.costUnitCategoryRepo.delete(costUnitCategory.id, {}).subscribe();
      }
    });
  }
}
