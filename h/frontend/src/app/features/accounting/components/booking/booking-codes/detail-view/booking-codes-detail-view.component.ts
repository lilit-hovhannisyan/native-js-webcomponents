import { wording } from 'src/app/core/constants/wording/wording';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { IBookingCode, IBookingCodeCreate } from 'src/app/core/models/resources/IBookingCode';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { Validators } from 'src/app/core/classes/validators';


/**
 * https://navido.atlassian.net/browse/NAVIDO-630
 */
@Component({
  selector: 'nv-booking-codes-detail-view',
  templateUrl: './booking-codes-detail-view.component.html',
  styleUrls: ['./booking-codes-detail-view.component.scss']
})
export class BookingCodesDetailViewComponent
  extends ResourceDetailView<IBookingCode, IBookingCodeCreate>
  implements OnInit {

  public repoParams = {};
  public routeBackUrl = url(sitemap.accounting.children.booking.children.bookingCodes);
  public form: FormGroup;

  protected resourceDefault = { isActive: true } as IBookingCode;

  private get isGettingDeactivated(): boolean {
    const isActive = this.form.controls.isActive.value;
    return !isActive && (this.resource && isActive !== this.resource.isActive);
  }

  public formElement: ElementRef = this.el.nativeElement.querySelector('form');

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    public authService: AuthenticationService,
    bookingCodeRepo: BookingCodeRepositoryService,
    baseComponentService: BaseComponentService,
    public el: ElementRef,
    private cdr: ChangeDetectorRef
  ) {
    super(bookingCodeRepo, baseComponentService);
  }

  public ngOnInit(): void {
    const { id } = this.route.snapshot.params;

    const isNew = id === 'new';
    isNew
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => `${this.resource.no.toString().padStart(3, '0')} ${this.resource[this.labelKey]}`;

  public init(bookingCode: IBookingCode = this.resourceDefault): void {
    this.initForm(this.fb.group({
      no: [bookingCode.no, [Validators.min(0), Validators.required]],
      isSystemDefault: [bookingCode.isSystemDefault],
      isPaymentsRelevance: [bookingCode.isPaymentsRelevance],
      remark: [bookingCode.remark, [Validators.maxLength(1000)]],
      isActive: [this.creationMode ? true : bookingCode.isActive],
      labelDe: [bookingCode.labelDe, [Validators.maxLength(50), Validators.required]],
      labelEn: [bookingCode.labelEn, [Validators.maxLength(50), Validators.required]],
    }));
  }

  public submit(): void {
    if (!this.form.valid) { return; }

    if (this.isGettingDeactivated) {
      this.confirmationService.confirm({
        title: wording.accounting.bookingCodeDeactivationTitle,
        wording: wording.accounting.bookingCodeDeactivationWarning,
        okButtonWording: wording.general.deactivate,
        cancelButtonWording: wording.general.cancel,
      }).subscribe(confirmed => {
        if (confirmed) { this.updateEntity(); }
      });
    } else {
      this.updateEntity();
    }
  }

  private updateEntity(): void {
    const bookingCodeEdited = this.creationMode
      ? { ...this.resource, ...this.form.value }
      : { ...this.resource, ...this.form.value, id: +this.route.snapshot.params.id };

    this.creationMode
      ? this.createResource(bookingCodeEdited)
      : this.updateResource(bookingCodeEdited);
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
