import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';

import { GroupCreateEditComponent } from './group-create-edit.component';
import { GroupCreateEditRoutingModule } from './group-create-edit-routing.module';
import { MeAttributeModule } from '@shared/me-components/me-attribute/me-attribute.module';
import { MeButtonModule } from '@shared/me-components/me-button/me-button.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MeSearchInputModule } from '@shared/me-components/me-search-input/me-search-input.module';
import { MeAddListCardModule } from '@shared/me-components/me-add-list-card/me-add-list-card.module';
import { MeTableModule } from '@shared/me-components/me-table/me-table.module';

@NgModule({
  declarations: [GroupCreateEditComponent],
  imports: [
    CommonModule,
    GroupCreateEditRoutingModule,
    MatStepperModule,
    MatButtonModule,
    MeAttributeModule,
    ReactiveFormsModule,
    MeButtonModule,
    MeSearchInputModule,
    MeAddListCardModule,
    MeTableModule,
    IconsModule
  ]
})
export class GroupCreateEditModule {}
