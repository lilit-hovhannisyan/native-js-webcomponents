import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { BunkerRepositoryService } from 'src/app/core/services/repositories/bunker-repository.service';
import { IBunker } from 'src/app/core/models/resources/IBunker';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/bunkers.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-bunkers-list-view',
  templateUrl: './bunkers-list-view.component.html',
  styleUrls: ['./bunkers-list-view.component.scss']
})
export class BunkersListViewComponent implements OnInit {
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IBunker[]>;

  constructor(
    private bunkerRepo: BunkerRepositoryService,
    private confirmationService: ConfirmationService,
    private authService: AuthenticationService,
    private router: Router,
    public gridConfigService: GridConfigService,
  ) { }
  public ngOnInit() {
    this.bunkerRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.bunkerRepo.getStream();

    const actions = {
      edit: (id) => this.router.navigate([`${this.router.url}/${id}`]),
      delete: this.deleteClicked,
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    this.gridConfig = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  public deleteClicked = (bunker: IBunker) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: bunker.displayLabel },
    }).subscribe(confirmed => confirmed && this.bunkerRepo.delete(bunker.id, {}).subscribe());
  }
}
