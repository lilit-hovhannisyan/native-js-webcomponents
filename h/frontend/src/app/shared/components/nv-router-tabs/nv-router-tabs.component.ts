import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';
import { AuthenticationService } from './../../../authentication/services/authentication.service';
import { SitemapNode } from './../../../core/constants/sitemap/sitemap';
import { SitemapEntry } from './../../../core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-router-tabs',
  templateUrl: './nv-router-tabs.component.html',
  styleUrls: ['./nv-router-tabs.component.scss']
})
export class NvRouterTabsComponent implements OnInit, OnDestroy {
  @Input() public sitemapNode: SitemapNode;
  @Input() public isDataMissing?: (sitemapNode: SitemapNode) => boolean | Observable<boolean>;
  @Input() public tabDisabler?: (SitemapNode: SitemapNode) => boolean;
  @Input() public resetDisabledNodes: Subject<void>;
  public disabledNodes: Array<SitemapNode> = [];

  public wording = wording;
  public tabNodes: Array<SitemapNode>;
  public selectedTabIndex: number;
  private currentTabNodeSubject = new BehaviorSubject<SitemapNode>(this.getCurrentNodeByUrl());
  public currentTabNode: Observable<SitemapNode> = this.currentTabNodeSubject.asObservable();
  private componentDestroyed$ = new Subject();

  constructor(
    private router: Router,
    private authService: AuthenticationService,
  ) { }

  public ngOnInit(): void {
    this.resetDisabledNodes && this.resetDisabledNodes.subscribe(() => {
      if (this.tabNodes?.length) {
        this.setDisabledNodes();
      }
    });
    this.filterAccessNodes();

    this.router.events.pipe(
      takeUntil(this.componentDestroyed$),
      filter(routerEvent => routerEvent instanceof NavigationEnd),
    )
    .subscribe(() => {
      this.setCurrentNodeAndIndexByUrl();
      this.setDisabledNodes();
    });
  }

  private filterAccessNodes(): void {
    // setTimeout is used to avoid 'ExpressionChangedAfterItHasBeenCheckedError', until the real reason of
    // the error is found
    setTimeout(() => {
      this.tabNodes = Object.values(this.sitemapNode.children)
        .filter(node => this.authService.canAccessNode(node));

      this.setDisabledNodes();

      const currentNode = this.getCurrentNodeByUrl();
      this.setSelectedTabIndex(this.tabNodes.indexOf(currentNode));
    });
  }

  private getCurrentNodeByUrl() {
    return SitemapEntry.getByUrl(this.router.url).node;
  }

  public tabClicked(node: SitemapNode): void {
    const urlParams = SitemapEntry.getUrlParams(this.router.url);
    const url = SitemapEntry.getByNode(node).toUrl(urlParams);

    this.router.navigate([url]);
  }

  private setSelectedTabIndex(index: number): void {
    this.selectedTabIndex = index;
  }

  private setCurrentNodeAndIndexByUrl(): void {
    const currentNode = this.getCurrentNodeByUrl();
    this.setCurrentTabNode(currentNode);
    /**
     * TODO: improve this part later
     * Optional chaining added to avoid error.
     * Page-url: /base/accounting/autobank/autobank-permissions/1/history
     */
    this.setSelectedTabIndex(this.tabNodes?.indexOf(currentNode));
  }

  private setCurrentTabNode(node: SitemapNode) {
    this.currentTabNodeSubject.next(node);
  }

  private setDisabledNodes(): void {
    if (this.tabDisabler) {
      /**
       * TODO: improve this part later
       * Optional chaining added to avoid error.
       * Page-url: /base/accounting/autobank/autobank-permissions/1/history
       */

      this.disabledNodes = this.tabNodes?.filter(tabNode => !this.tabDisabler(tabNode));
    }
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }
}
