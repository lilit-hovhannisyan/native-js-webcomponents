import { IUserRole } from '../../../../../../../core/models/resources/IUserRole';
import { UserRoleRepositoryService } from '../../../../../../../core/services/repositories/user-role.repository.service';
import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { Language } from '../../../../../../../core/models/language';
import { RoleRepositoryService } from '../../../../../../../core/services/repositories/role-repository.service';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { IRole, RoleKey } from '../../../../../../../core/models/resources/IRole';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { orderBy } from 'lodash';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { getUserRolesGridConfig } from 'src/app/core/constants/nv-grid-configs/user-roles.config';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  templateUrl: './user-roles.component.html',
  styleUrls: ['./user-roles.component.scss']
})
export class UserRolesComponent implements OnInit {

  public userId: number;
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IRole[]>;

  public editMode = false;
  private editMode$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private selectedRoles: RoleKey[] = [];

  constructor(
    public route: ActivatedRoute,
    private roleRepo: RoleRepositoryService,
    private userRoleRepo: UserRoleRepositoryService,
    private settingsService: SettingsService
  ) { }

  public ngOnInit(): void {
    this.gridConfig = getUserRolesGridConfig();
    this.userId = +this.route.parent.snapshot.params.id;

    const queryParams = { userId: this.userId };

    this.roleRepo.fetchAll({}).subscribe();
    this.userRoleRepo.fetchAll({ queryParams }).subscribe();

    this.dataSource$ = combineLatest([
      this.roleRepo.getStream(),
      this.userRoleRepo.getStream(),
      this.editMode$
    ]).pipe(map(([
      roles,
      userRoles,
      editMode,
    ]) => {
      this.selectedRoles = userRoles.map(userRole => userRole.roleId);

      return orderBy(roles
        .filter(role => editMode || this.roleIsAssignedToUser(role, userRoles))
        .map(role => ({
          ...role,
          _checked: editMode && this.roleIsAssignedToUser(role, userRoles)
        })), ['_checked', (user) => user[getLabelKeyByLanguage(this.settingsService.language)].toLowerCase()], ['desc', 'asc']);
    }));
  }

  public roleIsAssignedToUser = (role: IRole, userRoles: IUserRole[]): boolean => {
    return !!userRoles
      .filter(userRole => userRole.userId === this.userId)
      .find(userRole => userRole.roleId === role.id);
  }

  public enterEditMode() {
    this.editMode = true;
    this.editMode$.next(true);
    this.toggleGridSelectionType();
  }

  public cancelEditing() {
    this.editMode = false;
    this.editMode$.next(false);
    this.toggleGridSelectionType();
  }

  public toggleGridSelectionType = () => {
    this.gridConfig = {
      ...this.gridConfig,
      rowSelectionType: this.editMode
        ? NvGridRowSelectionType.Checkbox
        : undefined
    };
  }

  public submit() {
    this.userRoleRepo.setRolesForUser(this.userId, this.selectedRoles)
      .subscribe(() => this.cancelEditing());
  }

  public rowSelectionChanged(roles: IRole[]) {
    this.selectedRoles = roles.map(role => role.id);
  }
}
