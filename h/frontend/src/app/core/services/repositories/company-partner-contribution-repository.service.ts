import { Injectable } from '@angular/core';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { ICompanyPartnerContribution, CompanyPartnerContributionKey, ICompanyPartnerContributionDTO, ICompanyPartnerContributionCreate } from '../../models/resources/ICompanyPartnerContribution';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { CompanyKey } from 'src/app/core/models/resources/ICompany';


export interface ICompanyContributionRepoParams extends RepoParams<ICompanyPartnerContribution> {
  queryParams?: {
    cid: CompanyKey; // companyId
  };
}

@Injectable({
  providedIn: 'root'
})
export class CompanyContributionsRepositoryService extends AbstractRepository<
ICompanyPartnerContribution,
ICompanyPartnerContributionDTO,
ICompanyPartnerContributionCreate,
ICompanyContributionRepoParams,
CompanyPartnerContributionKey
>  {
  protected resourceType = 'CompanyPartnerContribution';
  protected sortKey = 'label';
  protected sortAsce = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/company-partner-contributions';

  protected postProcess = (item: ICompanyPartnerContributionDTO): ICompanyPartnerContribution => item;

  public fetchCompanyContributions(companyId: CompanyKey): Observable<ICompanyPartnerContribution[]> {
    return this.fetchAll({ queryParams: { cid: companyId } }).pipe(
      map((contributions: ICompanyPartnerContribution[]) => {
        return contributions.filter(c => c.companyId === companyId);
      })
    );
  }
}
