import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { IAutobankSetting } from '../../../../../../core/models/resources/IAutobankSetting';
import { AutobankSettingRepositoryService } from '../../../../../../core/services/repositories/autobank-setting.repository.service';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { forkJoin } from 'rxjs';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { AccountValidatorsService } from 'src/app/core/services/validators/account-validators.service';
import { CodeValidatorsService } from 'src/app/core/services/validators/code-validators.service';

@Component({
  templateUrl: './autobank-general-settings.component.html',
  styleUrls: ['./autobank-general-settings.component.scss']
})
export class AutobankGeneralSettingsComponent implements OnInit {
  protected resourceDefault: IAutobankSetting = {} as IAutobankSetting;
  public routeBackUrl = url(
    sitemap.accounting.children.autobank.children.autobankSettings
  );
  public form: FormGroup;
  public editMode: boolean;
  public wording = wording;

  public formElement: ElementRef;

  constructor(
    private autobankSettingRepo: AutobankSettingRepositoryService,
    private fb: FormBuilder,
    private bookingCodeRepo: BookingCodeRepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    private accountValidatorsService: AccountValidatorsService,
    private bookingCodeValidatorsService: CodeValidatorsService,
    private el: ElementRef,
    private cdr: ChangeDetectorRef
  ) {}

  public ngOnInit(): void {
    forkJoin([
      this.autobankSettingRepo.fetchAll(),
      this.bookingCodeRepo.fetchAll({}),
      this.bookingAccountRepo.fetchAll({}),
    ]).subscribe(([settings]) => {
      this.resourceDefault = settings[0] || {} as IAutobankSetting;
      this.init(this.resourceDefault);
    });
  }

  public init(autobankSetting: IAutobankSetting = this.resourceDefault): void {
    this.form = this.fb.group({
      bookingCodeBankAccountBookingId: [
        autobankSetting.bookingCodeBankAccountBookingId,
        [Validators.required, this.bookingCodeValidatorsService.isBookingCodeActive],
      ],
      bookingCodeBankForeignIncomingPaymentId: [
        autobankSetting.bookingCodeBankForeignIncomingPaymentId,
        [Validators.required, this.bookingCodeValidatorsService.isBookingCodeActive],
      ],
      matchingAccountIncomingPaymentId: [
        autobankSetting.matchingAccountIncomingPaymentId,
        [Validators.required, this.accountValidatorsService.isBookingAccountActive],
      ],
      matchingAccountOutgoingPaymentId: [
        autobankSetting.matchingAccountOutgoingPaymentId,
        [Validators.required, this.accountValidatorsService.isBookingAccountActive],
      ],
      bankFeesAccountId: [autobankSetting.bankFeesAccountId, [Validators.required]],
      exchangeRateProfitAccountId: [
        autobankSetting.exchangeRateProfitAccountId,
        [Validators.required, this.accountValidatorsService.isBookingAccountActive],
      ],
      exchangeRateLossAccountId: [
        autobankSetting.exchangeRateLossAccountId,
        [Validators.required, this.accountValidatorsService.isBookingAccountActive],
      ],
      path: [autobankSetting.path, [Validators.required]],
    });

    this.form.disable({ emitEvent: false });
    this.form.markAsPristine();
  }

  public enterEditMode(): void {
    this.editMode = true;
    this.form.enable({ emitEvent: false });
  }

  public cancelEditing(): void {
    this.editMode = false;
    this.init();
    // need a little delay to disable autocomplete fields
    setTimeout(() => this.form.disable());
  }

  public submit(): void {
    if (!this.form.valid) {
      return;
    }

    this.autobankSettingRepo.update(this.form.value).subscribe(settings => {
      this.resourceDefault = settings;
      this.cancelEditing();
    });
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
