import { Injectable } from '@angular/core';
import { combineLatest, Observable, forkJoin } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { SilentPartnerRepositoryService } from '../repositories/silent-partner-repository.service';
import { CurrencyRepositoryService } from '../repositories/currency-repository.service';
import { CompanyRepositoryService } from '../repositories/company-repository.service';
import { CompanyKey, ICompany } from '../../models/resources/ICompany';
import { ICurrency, CurrencyKey } from '../../models/resources/ICurrency';
import { ISilentPartnerRaw } from '../../models/resources/ISilentPartnerRepresentative';
import { convertToMap } from 'src/app/shared/helpers/general';

@Injectable({ providedIn: 'root' })
export class SilentPartnerMainService {
  constructor(
    private silentPartnersRepo: SilentPartnerRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private companyRepo: CompanyRepositoryService,
  ) { }


  public getStream(companyId: CompanyKey): Observable<ISilentPartnerRaw[]> {
    return forkJoin([
      this.silentPartnersRepo.fetchAll({ queryParams: { cid: companyId } }),
      this.currencyRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.silentPartnersRepo.getStream({ companyId }),
        this.currencyRepo.getStream().pipe(
          map(this.currencyRepo.filterActiveOnly),
          map(currencies => convertToMap<ICurrency, CurrencyKey>(currencies)),
        ),
        this.companyRepo.getStream().pipe(
          map(companies => convertToMap<ICompany, CompanyKey>(companies)),
        ),
      ])),
      map(([silentPartners, currenciesMap, companiesMap]) => silentPartners.map(s => ({
        ...s,
        currencyIsoCode: currenciesMap.get(s.currencyId)?.isoCode,
        partnerCompanyName: companiesMap.get(s.partnerId)?.companyName,
      }))),
    );
  }
}
