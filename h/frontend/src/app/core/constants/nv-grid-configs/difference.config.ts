import { NvGridConfig } from 'nv-grid';
import { wordingGeneral } from 'src/app/core/constants/wording/general';


export const getDifferenceGridConfig = (): NvGridConfig => {
  return ({
    gridName: 'DifferenceGrid',
    rowButtonsPosition: null,
    hideRefreshButton: true,
    showFooter: false,
    hideToolbar: true,
    paging: {
      pageNumber: 1,
      pageSize: 10,
      showTotalItems: true
    },
    columns: [{
      key: 'id',
      visible: false,
    },
    {
      key: 'member',
      title: wordingGeneral.member,
      width: 250,
      isSortable: true,
      filter: {
        values: []
      }
    }, {
      key: 'oldValue',
      title: wordingGeneral.oldValue,
      width: 250,
      isSortable: true,
      filter: {
        values: []
      }
    },
    {
      key: 'newValue',
      title: wordingGeneral.newValue,
      width: 250,
      isSortable: true,
      filter: {
        values: []
      }
    }]
  });
};
