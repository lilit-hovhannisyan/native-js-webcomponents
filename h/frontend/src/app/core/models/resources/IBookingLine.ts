import { IResource } from './IResource';
import { IBookingAccount } from './IBookingAccount';
import { CostTypeKey } from './ICostTypes';
import { CostCenterKey } from './ICostCenter';
import { CostUnitKey } from './ICostUnit';

interface ISharedProps extends IResource<number> {
  invoiceNo: string;
  text: string;
  debitCreditType: number;
  amountLocal?: number;
  amountTransaction?: number;
  discount: number;
  paymentDate: string;
  exchangeRate: number;
  lineIndex: number;
  mandatorId: number;
  portId: number;
  bookingId: number;
  currencyId: number;
  countryId: number;
  bookingAccountId: number;
  taxKeyId: number;
  bookingCodeId: number;
  costTypeId: CostTypeKey;
  costCenterId: CostCenterKey;
  costUnitId?: CostUnitKey;
  accrualAccounting: boolean;
  readOnly: boolean;
}

export interface IBookingLineCreate extends ISharedProps { }

export interface IBookingLineBase extends ISharedProps, IResource<number> { }

export interface IBookingLineDTO extends IBookingLineBase { }

export interface IBookingLine extends IBookingLineDTO { }

export interface IBookingLineFull extends IBookingLine {
  bookingAccount: IBookingAccount;
}

// IBookingLineRow interface is being used only in the inline-editing booking form
export interface IBookingLineRow extends IBookingLine {
  /*mandatorNo (-> mandatorId) represents the input of mandatorNo by user in the booking form */
  mandatorNo?: number;
  /*accountName represents the custom tooltip of accountNo in the booking form */
  accountName?: string;
  /*accountNo (-> bookingAccountId) represents the input of accountNo by user in the booking form */
  accountNo?: number;
  /*tax (-> taxId)  represents the input of tax by user in the booking form */
  tax?: string;
  /*currencyIsoCode (-> currencyId) epresents the input of currencyIsoCode by user in the booking form */
  currencyIsoCode?: string;
  /*bookingCodeNo (-> bookingCodeId) represents the input of bookingCodeNo by user in the booking form */
  bookingCodeNo?: number;
  /*+/- (-> debitCreditType 1, -1) represents the input of + or - by user in the booking form */
  '+/-'?: string;
  /* TODO: Move voyage to bookingLine interface once backend starts sending it */
  voyage?: string;
  // For tooltip
  costTypeName?: string;
  // For display
  costTypeNo?: number;
  costCenterName?: string;
  costCenterNo?: number;
  costUnitNo?: string;
  costUnitName?: string;
}
