import { Component, Input } from '@angular/core';
import { gridConfig } from 'src/app/shared/components/autocomplete/modals/currency-grid-config';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup } from '@angular/forms';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { IWording } from 'src/app/core/models/resources/IWording';

@Component({
  selector: 'nv-currency-autocomplete',
  templateUrl: './currency-autocomplete.component.html',
  styleUrls: ['./currency-autocomplete.component.scss']
})
export class CurrencyAutocompleteComponent {

  public wording = wording;
  public gridConfig = gridConfig;

  @Input() public form: FormGroup;
  @Input() public controlName = 'currencyId';
  @Input() public filters?: object;
  @Input() public label: IWording = wording.accounting.currency;
  @Input() public setNullIfNotFound = false;

  constructor(public currencyRepo: CurrencyRepositoryService) { }
}
