import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { MeBreadcrumbActionI } from '@shared/me-components/me-breadcrumb/me-breadcrumb-action.interface';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbSupportService {
  private readonly _breadcrumbActions = new BehaviorSubject<MeBreadcrumbActionI[]>([]);
  readonly breadcrumbActions$ = this._breadcrumbActions.asObservable();

  get breadcrumbActions(): MeBreadcrumbActionI[] {
    return this._breadcrumbActions.getValue();
  }

  set breadcrumbActions(data: MeBreadcrumbActionI[]) {
    this._breadcrumbActions.next(data);
  }
}
