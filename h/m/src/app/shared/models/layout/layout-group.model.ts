import { Type } from 'class-transformer';
import { LayoutGroupAttributeModel } from './layout-group-attribute.model';
import { LayoutRowModel } from './layout-row.model';

export class LayoutGroupModel {
  internalName: string = '';
  displayName: string = '';
  placement: number = 0;
  @Type(() => LayoutGroupAttributeModel)
  attributes: LayoutGroupAttributeModel[] = [];
  @Type(() => LayoutRowModel)
  rows: LayoutRowModel[] = [];
}
