import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'me-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.scss']
})
export class TableListComponent implements OnInit {
  @Input() data!: string[];

  constructor() {}

  ngOnInit(): void {}
}
