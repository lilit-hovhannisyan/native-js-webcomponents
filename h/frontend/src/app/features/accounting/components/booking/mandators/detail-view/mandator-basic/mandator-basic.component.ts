import { AuthenticationService } from './../../../../../../../authentication/services/authentication.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, AbstractControl } from '@angular/forms';
import { ICompany, CompanyKey } from '../../../../../../../core/models/resources/ICompany';
import { Validators } from 'src/app/core/classes/validators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { startWith, takeUntil, map, distinctUntilChanged, take, switchMap } from 'rxjs/operators';
import { Observable, combineLatest, Subject, of } from 'rxjs';
import { BaseComponentService } from '../../../../../../../core/services/base-component.service';
import { CompanyRepositoryService } from '../../../../../../../core/services/repositories/company-repository.service';
import { ResourceDetailView } from '../../../../../../../core/abstracts/ResourceDetailView';
import { IMandator, IMandatorCreate, MandatorKey } from '../../../../../../../core/models/resources/IMandator';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { ICurrency } from '../../../../../../../core/models/resources/ICurrency';
import { BookingRepositoryService } from 'src/app/core/services/repositories/booking-repository.service';
import { IBooking } from 'src/app/core/models/resources/IBooking';
import * as moment from 'moment';
import { wording } from 'src/app/core/constants/wording/wording';
import { MandatorClosedYearsRepositoryService } from 'src/app/core/services/repositories/mandator-closed-year-repository.service';
import { ACCOUNTING_TYPES } from 'src/app/core/models/enums/accounting-types';
import { IMandatorClosedBookingYear } from 'src/app/core/models/resources/IMandatorClosedBookingYear';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { mandatorTypesOptions } from 'src/app/core/services/mainServices/mandator-main.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/companies.config';
import { NvGridConfig } from 'nv-grid';
import { gridConfig } from 'src/app/shared/components/autocomplete/modals/currency-grid-config';
import { getCompanyLegalForm } from 'src/app/core/models/enums/company-legal-form';

@Component({
  selector: 'nv-basic',
  templateUrl: './mandator-basic.component.html',
  styleUrls: ['./mandator-basic.component.scss']
})
export class MandatorBasicComponent extends ResourceDetailView<
IMandator,
IMandatorCreate
> implements OnInit, OnDestroy {

  protected repoParams = {};
  protected resourceDefault = {
    isActive: true,
    firstBookingYear: moment().startOf('year').format('YYYY')
  } as any as IMandator;

  public mandatorId?: MandatorKey;
  public mandator?: IMandator;
  public routeBackUrl = this.url(sitemap.accounting.children.booking.children.mandators);
  public companies: ICompany[] = [];
  public currencies$: Observable<ICurrency[]>;
  public accountingType: number;
  public ACL_ACTIONS = ACL_ACTIONS;
  public ACCOUNTING_TYPES = ACCOUNTING_TYPES;
  public legalFormOptions = getCompanyLegalForm();
  public closedYears = [];
  public blockedYears = [];
  public blockedYears_editable = [];
  public closedYears_editable = [];
  public companyGridConfig = getGridConfig(this.router, () => false);

  public mandatorTypes: ISelectOption[] = mandatorTypesOptions;

  private destroy$: Subject<void> = new Subject();
  private isBlockedYearsEdited = false;
  private isClosedYearsEdited = false;
  public showClosedBookingYears = false;
  public currencyGridConfig: NvGridConfig = gridConfig;
  public companyLogoId: string;
  public formElement: ElementRef;
  constructor(
    private currencyRepo: CurrencyRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private bookingRepo: BookingRepositoryService,
    private fb: FormBuilder,
    private mandatorRepo: MandatorRepositoryService,
    private mandatorClosedYearRepo: MandatorClosedYearsRepositoryService,
    baseComponentService: BaseComponentService,
    public route: ActivatedRoute,
    public authService: AuthenticationService,
    public el: ElementRef,
    private cdr: ChangeDetectorRef
  ) {
    super(mandatorRepo, baseComponentService);
  }

  public ngOnInit(): void {
    const { id } = this.route.parent.snapshot.params;

    if (id === 'new') {
      this.fetchCompanies();
      this.enterCreationMode();
    } else {
      this.loadResource(id).subscribe(mandator => this.initializeMandator(mandator));
    }

    this.currencies$ = this.currencyRepo.fetchAll({ filter: { isAllowedLocal: true } })
      .pipe(map(currencies => this.currencyRepo.filterActiveOnly(currencies)));
  }

  public getTitle = () => `${this.resource.no} ${this.resource.name}`;

  public initializeMandator(mandator: IMandator) {
    this.mandator = mandator;
    this.fetchBlockedYearsData(mandator.id);

    combineLatest([
      this.bookingRepo.getBookingsByMandator(mandator.id),
      this.mandatorClosedYearRepo.fetchByMandators([mandator.id])
    ]).subscribe(([bookings, lastClosedYear]) => {
      this.setFirstBookingYearStatus(bookings, lastClosedYear);
    });

    this.mandatorRepo.getBookingLines(mandator.id).subscribe(bookingLines => {
      if (bookingLines.length > 0) {
        this.setDisabledFields([...this.getDisabledFields(), 'currencyId']);
      }
    });

    this.fetchCompanies();
  }

  public init(mandator: IMandator = this.resourceDefault): void {
    this.initForm(this.fb.group({
      no: [mandator.no, [Validators.required, Validators.integerBetween(1, 9999)]],
      name: [mandator.name, [Validators.required, Validators.maxLength(50)]],
      start: [mandator.start, [Validators.required, Validators.mandatorStartDate]],
      isCostAccounting: [mandator.isCostAccounting],
      isInvoiceWorkflow: [mandator.isInvoiceWorkflow],
      startFiscalYear: [mandator.startFiscalYear || 1, [Validators.required, Validators.max(12), Validators.min(1)]],
      firstBookingYear: [this.yearToDate(mandator.firstBookingYear), [Validators.required]],
      mandatorType: [mandator.mandatorType, [Validators.required]],
      currencyId: [mandator.currencyId, [Validators.required]],
      companyId: [mandator.companyId, [Validators.required]],
      isActive: [mandator.isActive],
      remarks: [mandator.remarks],
      isAutoBankWorkflow: [mandator.isAutoBankWorkflow],
      isPaymentTransfer: [mandator.isPaymentTransfer],
      accountingCheckboxGroup: this.fb.group(
        {
          isHgbAccounting: [this.isAccountingTypeEnabled(ACCOUNTING_TYPES.HGB)],
          isIfrsAccounting: [this.isAccountingTypeEnabled(ACCOUNTING_TYPES.IFRS)],
          isUsgaapAccounting: [this.isAccountingTypeEnabled(ACCOUNTING_TYPES.USGAAP)],
          eBalanceAccounting: [this.isAccountingTypeEnabled(ACCOUNTING_TYPES.EBalance)],
        },
        { validator: Validators.requiredCheckboxes(1, wording.accounting.youNeedToCheckAcccountingType) }
      ),
      // view field
      legalForm: [mandator.legalForm, Validators.number],
    }));

    this.setDisabledFields(['legalForm']);
    this.accountingType = mandator.accountingType;
    this.initFiscalYearChangeListener();
    // discarding unsaved changes of blockedYears / closedYears
    this.blockedYears_editable = [...this.blockedYears];
    this.closedYears_editable = [...this.closedYears];

    this.companyChangeHandler();
  }

  private fetchCompanies(): void {
    /**
     * On creation mod there is no selected company, so we have to
     * fetch only companies which has no connection with mandators.
     */
    const selectedCompanyRequest = this.mandator
      ? this.companyRepo.fetchOne(this.mandator.companyId, {})
      : of(null);

    /**
     * The `fetchAllWithoutMandator` method gets all companies which are not connected
     * to any mandator - even current one. So needed to separately get company which
     * is connected to current mandator.
     */
    combineLatest([
      this.companyRepo.fetchAllWithoutMandator(),
      selectedCompanyRequest,
    ]).pipe(take(1)).subscribe(([freeCompanies, mandatorCompany]) => {
      this.companies = freeCompanies;

      /**
       * On creation mode mandatorCompany will be `null`, but on edition
       * mode it will be an instance of company entity.
       */
      if (mandatorCompany) {
        this.companies.push(mandatorCompany);
      }

      /**
       * Getting companies can be delayed and after form initialization
       * `this.companyLogoId` can be set incorrect value such as `null`.
       */
      const companyId: CompanyKey = this.form?.controls.companyId.value;
      this.setCompanyLogo(companyId);
      this.setLegalForm(companyId);
    });
  }

  private companyChangeHandler(): void {
    this.form.controls.companyId.valueChanges.pipe(
      distinctUntilChanged(),
      startWith(this.form.controls.companyId.value),
    ).subscribe(companyId => {
      this.setCompanyLogo(companyId);
      this.setLegalForm(companyId);
    });
  }

  private setLegalForm(companyId: CompanyKey) {
    const company: ICompany = this.companies.find(c => c.id === companyId);
    const legalFormCtrl: AbstractControl = this.form.get('legalForm');
    // When it's zero we want to patch `null`
    legalFormCtrl.patchValue(company?.legalForm || null);
  }

  /**
   * If `companyId` is falsy or doesn't exists in companies list,
   * then `this.companyLogoId` will be set falsy value.
   */
  private setCompanyLogo(companyId?: number): void {
    const company = this.companies.find(item => item.id === companyId);
    this.companyLogoId = company?.logoId;
  }

  private isAccountingTypeEnabled(accountingBitmask: number): boolean {
    return (this.resource && (this.resource.accountingType & accountingBitmask)) === accountingBitmask;
  }

  public accountingTypeChange(values: number[]): void {
    if (values.length > 0) {
      this.accountingType = values.reduce((acc, cur) => acc | cur);
    }
  }

  public submit(): void {
    if (this.form.invalid) { return; }

    this.showAutobankWorkflowWarning().pipe(
      switchMap(confirmed => {
        /**
         * if autobankWorkflow warning confirmed - then show paymentTransfer warning.
         */
        return confirmed ? this.showPaymentTransferWarning() : of(false);
      }),
    ).subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      const formValue = this.form.getRawValue();
      const freshResource = this.creationMode
        ? formValue
        : ({ ...formValue, id: this.resource.id });

      freshResource.accountingType = this.accountingType;
      freshResource.firstBookingYear = this.getYearOfDate(formValue.firstBookingYear);

      delete freshResource.accountingCheckboxGroup;
      const blockedYearsPayload = [...this.blockedYears_editable];
      const closedYearsPayload = [...this.closedYears_editable];

      if (this.creationMode) {
        this.createResource(freshResource).subscribe();
      } else {
        this.updateResource(freshResource).subscribe(() => {
          this.updateBlockedYears(blockedYearsPayload);
          this.updateClosedYears(closedYearsPayload);
        });
      }
    });
  }

  private showAutobankWorkflowWarning() {
    if (this.creationMode
      || this.form.value.isAutoBankWorkflow
      || !this.resource?.isAutoBankWorkflow) {
      return of(true);
    }
    return this.confirmationService.confirm({
      wording: this.wording.accounting.mandatorWorkflowRemoveAutobankPermissionWarning,
      okButtonWording: this.wording.accounting.saveAndDeleteFromAutobank,
      cancelButtonWording: this.wording.general.cancel,
    });
  }
  private showPaymentTransferWarning() {
    if (this.creationMode
      || this.form.value.isPaymentTransfer
      || !this.resource?.isPaymentTransfer) {
      return of(true);
    }
    return this.confirmationService.confirm({
      wording: this.wording.accounting.mandatorPaymentTransferCheckboxWarningWarning,
      okButtonWording: this.wording.accounting.saveAndDeleteFromPaymentsPermissions,
      cancelButtonWording: this.wording.general.cancel,
    });
  }


  private initFiscalYearChangeListener() {
    const { start, firstBookingYear, startFiscalYear } = this.form.controls;

    combineLatest([
      startFiscalYear.valueChanges.pipe(startWith(startFiscalYear.value)),
      firstBookingYear.valueChanges.pipe(startWith(firstBookingYear.value))
    ])
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => (startFiscalYear.value > 0) && start.updateValueAndValidity());
  }

  private setFirstBookingYearStatus(
    bookings: IBooking[],
    lastClosedYear?: IMandatorClosedBookingYear[]
  ): void {
    if (bookings && bookings.length > 0 || lastClosedYear) {
      this.setDisabledFields([...this.getDisabledFields(), 'firstBookingYear']);
    }
  }

  private yearToDate(year: number): Date {
    return moment().year(year).startOf('year').toDate();
  }

  private getYearOfDate(isoDate: string) {
    return new Date(isoDate).getFullYear();
  }

  public blockedYearsChanged = (blockedYears) => {
    this.isBlockedYearsEdited = true;
    this.blockedYears_editable = blockedYears;
  }

  public closedYearsChanged = (closedYears) => {
    this.isClosedYearsEdited = true;
    this.closedYears_editable = closedYears;
  }

  private updateBlockedYears = (blockedYearsPayload) => {
    if (!this.isBlockedYearsEdited) { return; } // no need to write to db, when nothing was edited
    this.mandatorRepo.setBlockedYears(this.mandator.id, blockedYearsPayload)
      .subscribe(blockedYears => {
        this.blockedYears = blockedYears;
        this.blockedYears_editable = [...blockedYears];
      });
  }

  private updateClosedYears = (closedYearsPayload) => {
    if (!this.isClosedYearsEdited) { return; } // no need to write to db, when nothing was edited
    this.mandatorRepo.setClosedYears(this.mandator.id, closedYearsPayload)
      .subscribe(closedYears => {
        this.closedYears = closedYears;
        this.closedYears_editable = [...closedYears];
      });
  }

  private fetchBlockedYearsData = (mandatorId: MandatorKey) => {
    this.mandatorRepo.getClosedYears(mandatorId).subscribe(closedYears => {
      this.closedYears = closedYears;
      this.closedYears_editable = [...this.closedYears];

    });

    this.mandatorRepo.getBlockedYears([mandatorId]).subscribe(blockedYears => {
      this.blockedYears = blockedYears;
      this.blockedYears_editable = [...this.blockedYears];
    });
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
