import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MandatorKey, IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { IUser, UserKey } from 'src/app/core/models/resources/IUser';
import { IPermissionGroup, IPermissionGroupUserMandatorConnection, IPermissionsGroupsMandatorsUsersLists, PermissionKey, IPermissionRadioSelectItem } from '../../models/resources/IPermissionGroup';
import { IResource } from '../../models/resources/IResource';

export type ConnectionsHashKey = string;

@Injectable()
export class PermissionsGroupsHelperService<
  C extends IPermissionGroupUserMandatorConnection,
  P extends (IPermissionGroup<C> & IResource<PermissionKey>),
> {
  public mandators$ = new BehaviorSubject<IMandatorLookup[]>([]);
  public selectedMandators$: BehaviorSubject<IMandatorLookup[]> = new BehaviorSubject<IMandatorLookup[]>([]);
  public users$ = new BehaviorSubject<IUser[]>([]);
  public selectedUsers$ = new BehaviorSubject<IUser[]>([]);
  public selectedGroup$ = new BehaviorSubject<P>(null);

  public inCreateMode$ = new BehaviorSubject<boolean>(false);
  public inEditMode$ = new BehaviorSubject<boolean>(false);
  public editMode = false;
  public createMode = false;

  public hasChanges$ = new BehaviorSubject<boolean>(false);

  constructor() {
    this.inEditMode$.subscribe(editMode => {
      this.editMode = editMode;
    });
    this.inCreateMode$.subscribe(createMode => this.createMode = createMode);
  }

  public connections2Ids(
    connections: C[],
  ): IPermissionsGroupsMandatorsUsersLists {
    const usersSet = new Set<UserKey>();
    const mandatorsSet = new Set<MandatorKey>();

    connections.forEach(c => {
      usersSet.add(c.userId);
      mandatorsSet.add(c.mandatorId);
    });

    return {
      usersIds: Array.from(usersSet),
      mandatorsIds: Array.from(mandatorsSet),
    };
  }

  public entities2connections(
    users: IUser[],
    mandators: IMandatorLookup[],
    permission: P,
    permissionIdProp: string,
  ): C[] {
    const connectionsHashTable = this.connectionsToKeyHashTable(
      permission.connections,
      permissionIdProp,
    );
    const connectionsArrays: C[][] = users.map(userItem => {
      return mandators.map(m => {
        const key: ConnectionsHashKey = this.getConnectionHashKey(
          permission.id,
          m.id,
          userItem.id
        );

        return connectionsHashTable.get(key) || {
          mandatorId: m.id,
          userId: userItem.id,
          paymentPermissionId: permission.id,
          autobankPermissionId: permission.id,
        } as unknown as C;
      });
    });

    return [].concat(...connectionsArrays);
  }

  private connectionsToKeyHashTable(
    connections: C[],
    permissionIdProp: string,
  ): Map<string, C> {
    const hashTable = new Map<string, C>();
    if (!connections?.length) {
      return hashTable;
    }

    connections.forEach(c => {
      const key: ConnectionsHashKey = this.getConnectionHashKey(
        c[permissionIdProp],
        c.mandatorId,
        c.userId
      );
      hashTable.set(key, c);
    });

    return hashTable;
  }

  private getConnectionHashKey(
    pId: PermissionKey,
    mId: MandatorKey,
    uId: UserKey
  ): ConnectionsHashKey {
    return `pid:${pId}-mid:${mId}-uid:${uId}`;
  }

  public permission2radioSelectItem(
    permission: P
  ): IPermissionRadioSelectItem<C> {
    return {
      ...permission,
      displayLabel: permission.name,
    };
  }

  public map2RadioSelectItems(permissions: P[]): IPermissionRadioSelectItem<C>[] {
    return permissions.map(p => this.permission2radioSelectItem(p));
  }
}
