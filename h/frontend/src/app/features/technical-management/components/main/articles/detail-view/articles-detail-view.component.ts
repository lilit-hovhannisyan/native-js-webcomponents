import { Component, OnInit, Input, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { IArticle, IArticleCreate } from 'src/app/core/models/resources/IArticle';
import { IArticleClass } from 'src/app/core/models/resources/IArticleClass';
import { IArticleType } from 'src/app/core/models/resources/IArticleType';
import { IArticleUnit } from 'src/app/core/models/resources/IArticleUnit';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { IDebitorCreditorFull } from 'src/app/core/models/resources/IDebitorCreditor';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { DebitorCreditorMainService } from 'src/app/core/services/mainServices/debitor-creditor-main.service';
import { ArticleClassRepositoryService } from 'src/app/core/services/repositories/article-class-repository.service';
import { ArticleRepositoryService } from 'src/app/core/services/repositories/article-repository.service';
import { ArticleTypeRepositoryService } from 'src/app/core/services/repositories/article-type-repository.service';
import { ArticleUnitsRepositoryService } from 'src/app/core/services/repositories/article-units-repository.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { NvGridConfig } from 'nv-grid';
import { getGridConfig as getArticleUnitsGridConfig } from 'src/app/core/constants/nv-grid-configs/article-units.config';
import { getArticleClassesGridConfig } from 'src/app/core/constants/nv-grid-configs/article-classes.config';
import { getArticleTypesGridConfig } from 'src/app/core/constants/nv-grid-configs/article-types.config';
import { getGridConfig as getDebitorCreditorsGridConfig } from 'src/app/core/constants/nv-grid-configs/debitors-creditors.config';

@Component({
  templateUrl: './articles-detail-view.component.html',
  styleUrls: ['./articles-detail-view.component.scss']
})
export class ArticlesDetailViewComponent
  extends ResourceDetailView<IArticle, IArticleCreate>
  implements OnInit {

  @Input() public articleId: number;
  public repoParams = {};
  public routeBackUrl = url(sitemap.technicalManagement.children.main.children.articles);
  protected resourceDefault = { isActive: true } as IArticle;

  public articleUnits$: Observable<IArticleUnit[]>;
  public articleClasses$: Observable<IArticleClass[]>;
  public articleTypes$: Observable<IArticleType[]>;
  public debitorCreditors$: Observable<IDebitorCreditorFull[]>;
  public currencies$: Observable<ICurrency[]>;

  public articleUnitsGridConfig: NvGridConfig = getArticleUnitsGridConfig({}, () => false);

  public articleClassesGridConfig: NvGridConfig = getArticleClassesGridConfig({}, () => false);

  public articleTypesGridConfig: NvGridConfig = getArticleTypesGridConfig({}, () => false);

  public debitorCreditorsGridConfig: NvGridConfig = getDebitorCreditorsGridConfig({}, () => false);
  public formElement: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private articleUnitRepo: ArticleUnitsRepositoryService,
    private articleClassRepo: ArticleClassRepositoryService,
    private articleTypeRepo: ArticleTypeRepositoryService,
    private debitorCreditorMainService: DebitorCreditorMainService,
    private currenciesRepo: CurrencyRepositoryService,
    articleRepo: ArticleRepositoryService,
    baseComponentService: BaseComponentService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef,
  ) {
    super(articleRepo, baseComponentService);
  }

  private static yearToDate(year: number): Date {
    return year ? new Date(`${year}`) : null;
  }

  private static getYearOfDate(isoDate: string): number | null {
    return isoDate ? new Date(isoDate).getFullYear() : null;
  }

  public ngOnInit() {
    this.articleUnits$ = this.articleUnitRepo.fetchAll({});
    this.articleTypes$ = this.articleTypeRepo.fetchAll({});
    this.articleClasses$ = this.articleClassRepo.fetchAll({});
    this.debitorCreditors$ = this.debitorCreditorMainService.fetchAll()
      .pipe(
        map(res => this.debitorCreditorMainService.toRows(res))
      );
    this.currenciesRepo.fetchAll({}).subscribe();

    const id = this.articleId || this.route.snapshot.params.id;

    if (id === 'new') {
      this.enterCreationMode();
    } else {
      this.loadResource(id);
    }
  }

  public getTitle = () => `${this.resource.no} ${this.resource[this.labelKey]}`;

  public init(article: IArticle = this.resourceDefault): void {
    this.initForm(this.fb.group({
      no: [article.no, [Validators.required, Validators.maxLength(10)]],
      labelEn: [article.labelEn, [Validators.required, Validators.maxLength(100)]],
      labelDe: [article.labelDe, [Validators.required, Validators.maxLength(100)]],
      articleUnitId: [article.articleUnitId, [Validators.required]],
      articleClassId: [article.articleClassId],
      articleTypeId: [article.articleTypeId],
      description: [article.description, [Validators.maxLength(4000)]],
      manufacturerManual: [article.manufacturerManual, [Validators.maxLength(70)]],
      manufacturerArticleNo: [article.manufacturerArticleNo, [Validators.maxLength(50)]],
      debitorCreditorId: [article.debitorCreditorId],
      itemType: [article.itemType, [Validators.maxLength(100)]],
      serialNo: [article.serialNo, [Validators.maxLength(50)]],
      year: [ArticlesDetailViewComponent.yearToDate(article.year)],
      salePrice: [article.salePrice, [Validators.maxDecimalDigits(2)]],
      currencyId: [article.currencyId],
      isSystemDefault: [article.isSystemDefault],
      isActive: [article.isActive],
    }));
    this.loadCurrencies();

    if (!this.isAdmin) {
      this.disableField('isActive');
    }
  }

  private loadCurrencies(): void {
    const currencyControl = this.form.controls.currencyId;
    this.currencies$ = combineLatest([
      this.currenciesRepo.getStream(),
      currencyControl.valueChanges.pipe(startWith(currencyControl.value))
    ]).pipe(
      map(([currencies, currencyId]) => this.currenciesRepo.filterActiveOnly(currencies, currencyId))
    );
  }


  public submit(): void {
    if (!this.form.valid) {
      return;
    }

    const freshResource = {
      ...this.form.value,
      year: ArticlesDetailViewComponent.getYearOfDate(this.form.value.year),
    };
    if (this.creationMode) {
      this.createResource(freshResource);
    } else {
      this.updateResource({ ...this.resource, ...freshResource });
    }
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

}
