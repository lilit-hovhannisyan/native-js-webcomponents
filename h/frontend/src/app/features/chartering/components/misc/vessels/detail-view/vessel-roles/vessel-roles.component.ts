import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { orderBy } from 'lodash';
import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { getVesselRolesGridConfig } from 'src/app/core/constants/nv-grid-configs/vessel-roles.config';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { IRole, RoleKey } from 'src/app/core/models/resources/IRole';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { IVesselRole } from 'src/app/core/models/resources/IVesselRole';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { RoleRepositoryService } from 'src/app/core/services/repositories/role-repository.service';
import { VesselRoleRepositoryService } from 'src/app/core/services/repositories/vessel-role.repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  selector: 'nv-vessel-roles',
  templateUrl: './vessel-roles.component.html',
  styleUrls: ['./vessel-roles.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VesselRolesComponent implements OnInit {

  public vesselId: VesselKey;
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IRole[]>;

  public editMode = false;
  private editMode$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private selectedRoles: RoleKey[] = [];

  constructor(
    private roleRepo: RoleRepositoryService,
    private vesselRoleRepo: VesselRoleRepositoryService,
    public gridConfigService: GridConfigService,
    public route: ActivatedRoute,
    private settingsLanguage: SettingsService
  ) { }

  public ngOnInit(): void {
    this.vesselId = +this.route.parent.snapshot.params.id;
    this.gridConfig = getVesselRolesGridConfig();

    const queryParams: { [key: string]: number } = { vesselId: this.vesselId };
    this.roleRepo.fetchAll({}).subscribe();
    this.vesselRoleRepo.fetchAll({ queryParams }).subscribe();

    this.dataSource$ = combineLatest([
      this.roleRepo.getStream(),
      this.vesselRoleRepo.getStream(),
      this.editMode$,
    ]).pipe(map(([
      roles,
      vesselRoles,
      editMode,
    ]) => {
      this.selectedRoles = vesselRoles.map(vesselRole => vesselRole.roleId);

      return orderBy(roles
        .filter(role => editMode || this.roleIsAssignedToVessel(role, vesselRoles))
        .map(role => ({
          ...role,
          _checked: editMode && this.roleIsAssignedToVessel(role, vesselRoles)
        })),
        ['_checked', role => role[getLabelKeyByLanguage(this.settingsLanguage.language)]
          .toLowerCase()], ['desc', 'asc']);
    }));
  }

  public roleIsAssignedToVessel = (role: IRole, vesselRoles: IVesselRole[]): boolean => {
    return !!vesselRoles
      .filter(vesselRole => vesselRole.vesselId === this.vesselId)
      .find(vesselRole => vesselRole.roleId === role.id);
  }

  public enterEditMode() {
    this.editMode = true;
    this.editMode$.next(true);
    this.toggleGridSelectionType();
  }

  public cancelEditing() {
    this.editMode = false;
    this.editMode$.next(false);
    this.toggleGridSelectionType();
  }

  public toggleGridSelectionType = () => {
    this.gridConfig = {
      ...this.gridConfig,
      rowSelectionType: this.editMode
        ? NvGridRowSelectionType.Checkbox
        : undefined
    };
  }

  public submit() {
    this.vesselRoleRepo.setRolesForVessel(this.selectedRoles, this.vesselId)
      .subscribe(() => this.cancelEditing());
  }

  public rowSelectionChanged(roles: IRole[]) {
    this.selectedRoles = roles.map(role => role.id);
  }
}
