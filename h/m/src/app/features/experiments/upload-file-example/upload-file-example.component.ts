import { Component, OnInit } from '@angular/core';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { FileWebService } from '@shared/services/file.web-service';
import { MeUploadFileI } from '@shared/me-components/me-upload-file/me-upload-file.interface';
import { FileUploadResponseModel } from '@shared/models/file-upload/file-upload-response.model';
import { FileUploadType } from '@shared/models/file-upload/file-upload.model';
import { bytesToSize } from '@shared/utils';

@Component({
  selector: 'me-upload-file-example',
  templateUrl: './upload-file-example.component.html',
  styleUrls: ['./upload-file-example.component.scss'],
  providers: [FileWebService]
})
export class UploadFileExampleComponent implements OnInit {
  uploadFileDataInterface!: MeUploadFileI;
  // thumbnailProperties!: ThumbnailDocument;
  supportedFiles: string = '';
  acceptExtensions: string = '';
  maxUploadSize: string = '';
  metaDataToSend: FileUploadType;

  constructor(private definitionsStoreService: DefinitionsStoreService, private fileWebService: FileWebService) {
    this.metaDataToSend = {
      type: 'THUMBNAIL'
    };
  }

  ngOnInit(): void {
    this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        if (this.definitionsStoreService.thumbnailProperties) {
          this.supportedFiles = this.definitionsStoreService.thumbnailProperties.supportedFiles.replace(/,/g, ', ');
          this.maxUploadSize = bytesToSize(this.definitionsStoreService.thumbnailProperties.maxSize);

          const splitString = this.definitionsStoreService.thumbnailProperties.supportedFiles.split(',');
          this.acceptExtensions = `.${splitString.join(',.')}`;
        }

        this.loadInitialImageData();
      }
    });
  }

  loadInitialImageData(): void {
    this.uploadFileDataInterface = {
      fileName: '',
      isStatusInvalid: false,
      acceptFileExtensions: this.acceptExtensions,
      supportedFiles: this.supportedFiles,
      maxUploadSize: this.maxUploadSize
    };
  }

  openFormData(recievedFile: File): void {
    const formData = new FormData();
    formData.append('meta', JSON.stringify(this.metaDataToSend));
    formData.append('file', recievedFile, recievedFile.name);

    this.fileWebService.uploadFile(formData, FileUploadResponseModel).subscribe(
      (response) => {
        this.uploadFileDataInterface = {
          fileName: response.attributes.name,
          showProgress: false,
          acceptFileExtensions: this.acceptExtensions,
          supportedFiles: this.supportedFiles,
          maxUploadSize: this.maxUploadSize
        };
        // this.globalService.setReferenceID(response.oid);
      },
      (error: ErrorResponseI) => {
        console.error(error);
        this.uploadFileDataInterface = {
          fileName: undefined,
          isStatusInvalid: true,
          acceptFileExtensions: this.acceptExtensions,
          supportedFiles: this.supportedFiles,
          maxUploadSize: this.maxUploadSize
        };
        /* const basicAlertData = new BasicAlertModel();
        basicAlertData.mode = 'error';
        basicAlertData.title = error.error.title;
        basicAlertData.content = error.error.details;
        this.alertsService.openBasicAlert(basicAlertData);

        this.globalService.setThumbnailStatusInvalid(true); */
      }
    );
  }
}
