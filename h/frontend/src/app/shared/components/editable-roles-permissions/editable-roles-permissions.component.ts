import { Component, OnInit, Input } from '@angular/core';
import { addTabsFullZoneAndTitle } from 'src/app/core/constants/sitemap/sitemap-entry';
import { IRole } from 'src/app/core/models/resources/IRole';
import { RoleRepositoryService } from 'src/app/core/services/repositories/role-repository.service';
import { Operations, getOperationName } from 'src/app/core/models/Operations';
import { SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { wording } from 'src/app/core/constants/wording/wording';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { specialZones } from 'src/app/authentication/services/zoneService/specialZones';


interface PermissionsOperationElement {
  label: string;
  value: any;
  checked: boolean;
}

interface IRoleWithOperations extends IRole {
  selectedTopicOperations: PermissionsOperationElement[];
}

@Component({
  selector: 'nv-editable-roles-permissions',
  templateUrl: './editable-roles-permissions.component.html',
  styleUrls: ['./editable-roles-permissions.component.scss']
})
export class EditableRolesPermissionsComponent implements OnInit {

  @Input() public selectedTopic: SitemapNode;
  public roles: IRole[] = [];

  public Operations = Operations;

  /**
   * Used to check if any permission is changed to disable/enable "Apply" button
   */
  public isDirty: boolean;

  /**
   * Typescript enum behaves unexpected when used as an object -> it contains key: value
   * and additionally value: key pairings... thats why we filter here:
   */
  public allOperations = Object.values(Operations).filter(o => typeof o === 'number') as number[];
  public wording = wording;
  public tabs = [];

  constructor(
    private roleRepository: RoleRepositoryService,
    private nzModalRef: NzModalRef,
    private authService: AuthenticationService,
  ) { }

  public ngOnInit() {
    this.roleRepository.fetchAll({}).subscribe(roles => {
      this.roles = roles;
      this.initTabs(this.selectedTopic);
      specialZones.forEach(zone => this.initTabs(zone));
    });
  }

  public initTabs(sitemapNode: SitemapNode) {
    this.tabs = addTabsFullZoneAndTitle(sitemapNode, this.tabs)
      .map((tab) => this.addRolesWithOperations(tab));
  }

  public addRolesWithOperations(tab: SitemapNode) {
    const rolesWithOperations = this.getRolesWithOperations(this.roles, tab.fullZone)
      .filter(role => this.authService.getSessionInfo().user.isSuperAdmin
        || !role.isSystemDefault);

    return {
      ...tab,
      rolesWithOperations,
    };
  }

  public getRolesWithOperations(
    roles: IRoleWithOperations[] | IRole[],
    selectedZone: string,
  ): IRoleWithOperations[] {
    return (roles as IRoleWithOperations[])
      .map(r => this.getRoleWithOperations(r, selectedZone));
  }

  public getRoleWithOperations(
    role: IRole | IRoleWithOperations,
    selectedZone: string,
  ): IRoleWithOperations {
    const permissions = role.permissions[selectedZone];
    const operations = this.allOperations.map(operation => {
      const foundSpecialZone = specialZones.find(specialZone => specialZone.fullZone === selectedZone);

      return {
        isVisible: !!(!foundSpecialZone || (foundSpecialZone.maxOperations & operation) === operation),
        label: getOperationName(operation),
        value: operation,
        checked: (permissions & operation) === operation
      };
    });

    return {
      ...role,
      selectedTopicOperations: operations,
    };
  }

  public setDirtyStatus() {
    if (!this.isDirty) {
      this.isDirty = true;
    }
  }

  public closeModal() {
    this.nzModalRef.close();
  }

  public submit(shouldCloseModal = true) {
    if (!this.isDirty) {
      this.closeModal();
      return;
    }

    const rolesToSend = this.getRolesToSend();

    this.roleRepository.updateRange(rolesToSend).subscribe(() => {
      if (shouldCloseModal) {
        this.closeModal();
      } else {
        this.isDirty = false;
      }
    });
  }

  public getRolesToSend() {
    let rolesToSend = [];

    const changedRoles = this.tabs
      .map(tab => tab.rolesWithOperations
        .map(role => ({
          ...role,
          permissions: {
            ...role.permissions,
            [tab.fullZone]: role.selectedTopicOperations.reduce(
              (permissions, operation) => operation.checked
                ? (permissions | operation.value)
                : permissions, 0)
          },
          changedZone: tab.fullZone
        }))
        .filter((role, index) => role.permissions[tab.fullZone]
          !== this.roles[index].permissions[tab.fullZone])
      );

    changedRoles.forEach(role => {
      role.forEach(r => {
        const index = rolesToSend.findIndex(roleToSend => roleToSend.id === r.id);

        if (index !== -1) {
          rolesToSend[index] = {
            ...rolesToSend[index],
            permissions: {
              ...rolesToSend[index].permissions,
              [r.changedZone]: r.permissions[r.changedZone]
            },
          };
        } else {
          rolesToSend = [...rolesToSend, r];
        }
      });
    });

    return rolesToSend;
  }

  public setOtherPermissions(
    tabIndex: number,
    roleIndex: number,
    currentOperation: PermissionsOperationElement,
  ) {
    this.setDirtyStatus();
    if (currentOperation.value === Operations.READ && !currentOperation.checked) {
      this.setOtherPermissionsOnRead(tabIndex, roleIndex);
    } else if (currentOperation.value === Operations.UPDATE && !currentOperation.checked) {
      this.setOtherPermissionsOnUpdateUncheck(tabIndex, roleIndex);
    } else if (currentOperation.value === Operations.UPDATE && currentOperation.checked) {
      this.setOtherPermissionsOnUpdate(tabIndex, roleIndex);
    } else if (currentOperation.value === Operations.CREATE && currentOperation.checked) {
      this.setOtherPermissionsOnCreate(tabIndex, roleIndex);
    } else if (currentOperation.value === Operations.DELETE && currentOperation.checked) {
      this.setOtherPermissionsOnDelete(tabIndex, roleIndex);
    }
  }

  public setOtherPermissionsOnRead(tabIndex: number, roleIndex: number) {
    this.tabs[tabIndex].rolesWithOperations[roleIndex].selectedTopicOperations
      .forEach(el => el.checked = false);
  }

  public setOtherPermissionsOnUpdateUncheck(tabIndex: number, roleIndex: number) {
    this.tabs[tabIndex].rolesWithOperations[roleIndex].selectedTopicOperations
      .forEach(el => {
        if (el.value === Operations.DELETE) {
          el.checked = false;
        }
      });
  }

  public setOtherPermissionsOnUpdate(tabIndex: number, roleIndex: number) {
    this.tabs[tabIndex].rolesWithOperations[roleIndex].selectedTopicOperations
      .forEach(el => {
        if (el.value === Operations.READ) {
          el.checked = true;
        }
      });
  }

  public setOtherPermissionsOnCreate(tabIndex: number, roleIndex: number) {
    this.tabs[tabIndex].rolesWithOperations[roleIndex].selectedTopicOperations
      .forEach(el => {
        if (el.value === Operations.UPDATE || el.value === Operations.READ) {
          el.checked = true;
        }
      });
  }

  public setOtherPermissionsOnDelete(tabIndex: number, roleIndex: number) {
    this.tabs[tabIndex].rolesWithOperations[roleIndex].selectedTopicOperations
      .forEach(el => {
        if (
          el.value === Operations.UPDATE
          || el.value === Operations.READ
          || el.value === Operations.CREATE
        ) {
          el.checked = true;
        }
      });
  }
}
