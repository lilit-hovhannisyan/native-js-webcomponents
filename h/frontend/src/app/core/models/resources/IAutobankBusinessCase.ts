import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';


export type AutobankBusinessCaseKey = number;

interface ISharedProps extends IWithLabels, IWithLabel {
  gvc: number;
}

export interface IAutobankBusinessCaseBase extends ISharedProps, IResource<AutobankBusinessCaseKey> { }

export interface IAutobankBusinessCaseCreate extends ISharedProps { }

export interface IAutobankBusinessCaseDTO extends IAutobankBusinessCaseBase { }

export interface IAutobankBusinessCase extends IAutobankBusinessCaseDTO { }


