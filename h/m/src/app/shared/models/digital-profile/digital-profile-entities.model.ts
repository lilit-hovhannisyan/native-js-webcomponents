import { Type } from 'class-transformer';
// import { DomainModel } from '../domain/domain.model';
import { DigitalProfileModel } from './digital-profile.model';
// import { DigitalProfileModel } from './digital-profile.model';

export class DigitalProfileEntitesModel {
  @Type(() => DigitalProfileModel)
  entities: DigitalProfileModel[] = []; //DomainModel
}
