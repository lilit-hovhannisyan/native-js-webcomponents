import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IAutobankRule, IAutobankRuleDTO, IAutobankRuleCreate, AutobankRuleKey } from '../../models/resources/IAutobankRule';
@Injectable({
  providedIn: 'root'
})
export class AutobankRuleRepositoryService extends AbstractRepository<
IAutobankRule,
IAutobankRuleDTO,
IAutobankRuleCreate,
RepoParams<IAutobankRule>,
AutobankRuleKey
>  {
  protected resourceType = 'Autobank';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IAutobankRuleDTO): IAutobankRule => item;

  public getBaseUrl = () => '/api/autobank/rules';
}
