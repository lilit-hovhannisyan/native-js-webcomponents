import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { Operations } from '../../models/Operations';
import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IBookingCode } from 'src/app/core/models/resources/IBookingCode';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {

  return ({
    gridName: 'bookingCodeGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'no',
        title: wording.accounting.no,
        width: 90,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isActive',
        title: wording.accounting.active,
        width: 230,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isSystemDefault',
        title: wording.general.systemDefault,
        width: 90,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isPaymentsRelevance',
        title: wording.accounting.paymentsRelevance,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'displayLabel',
        title: wording.general.name,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editBookingCode',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (bookingCode: IBookingCode) => {
          actions.edit(bookingCode.id);
        }
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteBookingCode',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: actions.delete,
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: () => actions.add()
      }
    ],
    contextMenuButtons: canPerform(Operations.READ) ? getContextMenuButtonsConfig() : [],
  });
};
