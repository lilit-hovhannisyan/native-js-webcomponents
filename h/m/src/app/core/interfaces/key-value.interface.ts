export interface KeyValueI<T> {
  [key: string]: T;
}
