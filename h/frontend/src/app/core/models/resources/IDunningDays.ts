import { IWithLabels } from './IWithLabels';
import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';

interface ISharedProps extends IWithLabels {
  daysLevel1: number;
  daysLevel2: number;
  daysLevel3: number;
  daysCollectionProcedure: number;
  lateFee: number;
  currencyId: number;
  debitorCreditorId: number;
  isActivated: boolean;
}

export interface IDunningDaysBase extends ISharedProps, IResource<number> { }

export interface IDunningDaysCreate extends ISharedProps { }

export interface IDunningDaysDTO extends IDunningDaysBase { }

export interface IDunningDays extends IDunningDaysDTO, IWithLabel { }
