import { Injectable } from '@angular/core';
import { IUser } from 'src/app/core/models/resources/IUser';
import { Observable, BehaviorSubject } from 'rxjs';
import { FileUploaderService } from './file-uploader.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
/**
 * Used only to get basic data of current user profile
 * and keep track of changes.
 */
@Injectable({
  providedIn: 'root'
})
export class CurrentUserService {
  private user = new BehaviorSubject<IUser>(null);

  constructor(
    private uploadService: FileUploaderService,
    private authService: AuthenticationService,
  ) {
    this.authService.isLoggedInSubject$.subscribe(isLoggedIn => {
      if (isLoggedIn) {
        this.user.next(this.authService.getUser());
        this.avatarChanged();
      }
    });
  }

  public getCurrentUser(): Observable<IUser> {
    return this.user.asObservable();
  }

  public avatarChanged() {
    const avatarId = this.authService.getUser().avatarId;
    if (avatarId) {
      this.uploadService.getUpload(avatarId)
        .subscribe(avatar => this.user.next({ ...this.user.value, avatar }));
    }
  }

  public configsUpdated(config) {
    this.user.next({ ...this.user.value, ...config });
  }
}
