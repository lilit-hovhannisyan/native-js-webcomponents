import { IResource } from './IResource';
import { NvGridPin } from 'nv-grid';

export interface IGridConfigBase {
  gridName: string;
  rowHeight: number;
  pin: NvGridPin;
  columns: { key: string; width: number, hidden: boolean }[];
  version: string;
}

export interface IGridConfigCreate extends IGridConfigBase, IResource<string> { }

export interface IGridConfigDTO extends IGridConfigBase { }

export interface IGridConfigDTO extends IGridConfigBase { }

export interface IGridConfig extends IGridConfigDTO { }
