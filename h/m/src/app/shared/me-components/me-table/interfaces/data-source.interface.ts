import RenderCellI from './render-cell.interface';
import RederedComponentI from './rendered-component.interface';

interface DataSourceI {
  hasChildren?: boolean;
  children?: {
    rowOID: string;
    renderedCells: RederedComponentI[];
  }[];
  renderedCells: RenderCellI[];
  rowOID: string;
}

export default DataSourceI;
