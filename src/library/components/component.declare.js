import {DataBindingComponent} from './data-binding-component/data-binding.component.js';
import {AccordionItemComponent} from './accordion-item-component/accordion-item.component.js';
import {AccordionComponent} from './accordion-component/accordion.component.js';
import {IconComponent} from './icon-component/icon.component.js';
import {TextComponent} from './text-component/text.component.js';
import {ButtonComponent} from './button-component/button.component.js';
import {InputComponent} from './input-component/input.component.js';
import {RouteManager} from '../core/route-manager/route.manager.js';

export const declareComponents = () => {
  customElements.define('route-manager', RouteManager);
  customElements.define('icon-component', IconComponent);
  customElements.define('text-component', TextComponent);
  customElements.define('button-component', ButtonComponent);
  customElements.define('input-component', InputComponent);
  customElements.define('accordion-item-component', AccordionItemComponent);
  customElements.define('accordion-component', AccordionComponent);
  customElements.define('data-binding-component', DataBindingComponent);
}