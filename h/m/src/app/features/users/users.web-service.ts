import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseWebService } from '../../core/services/base.web-service';

import { ArrayRequestI } from '@core-interfaces/array-request.interface';
import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { EntityI } from '@core-interfaces/entity.interface';

import { GroupModel } from '@shared/models/groups/group.model';
import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { UserCreateModel } from '@shared/models/user/user-create.model';
import { UserModel } from '@shared/models/user/user.model';
import { constructUrl } from '@shared/utils';
import { BaseSearchModel } from '@shared/models/search/search-base.model';
@Injectable()
export class UsersWebService {
  constructor(private baseWebService: BaseWebService) {}

  getMeUsers(from?: number, to?: number): Observable<ArrayResponseI<UserModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/users`, from, to);
    return this.baseWebService.getRequestForArray<UserModel>(url, UserModel);
  }

  getMeUsersGroups(userOID: string): Observable<ArrayResponseI<GroupModel>> {
    return this.baseWebService.getRequestForArray<GroupModel>(`${MARKETPLACE_BASE_API_URL}/users/${encodeURI(userOID)}/groups`, GroupModel);
  }

  createMeUser(data: ArrayRequestI<UserCreateModel>): Observable<ArrayRequestI<UserModel>> {
    return this.baseWebService.postRequestForArray<UserModel, ArrayRequestI<UserCreateModel>>(
      `${MARKETPLACE_BASE_API_URL}/users/`,
      data,
      UserModel
    );
  }

  inviteMeUser(data: object): Observable<UserModel> {
    return this.baseWebService.postRequest<UserModel, object>(`${MARKETPLACE_BASE_API_URL}/users/invite`, data);
  }

  updateMeUser(userOID: string, data: ArrayRequestI<EntityI>): Observable<UserModel> {
    return this.baseWebService.putRequest<UserModel, ArrayRequestI<EntityI>>(
      `${MARKETPLACE_BASE_API_URL}/users/${encodeURI(userOID)}`,
      data,
      UserModel
    );
  }

  searchUsers(data: BaseSearchModel, skip?: number, top?: number): Observable<ArrayResponseI<UserModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/users/search`, skip, top);
    return this.baseWebService.postRequestForArray<UserModel, BaseSearchModel>(url, data, UserModel);
  }

  deleteMeUser(userOID: string): Observable<void> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/users/${encodeURI(userOID)}`);
    return this.baseWebService.deleteRequest<void>(url);
  }
}
