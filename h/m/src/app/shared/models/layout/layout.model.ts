import { Type } from 'class-transformer';
import { LayoutGroupModel } from './layout-group.model';

export class LayoutModel {
  tag: string = '';
  @Type(() => LayoutGroupModel)
  groups: LayoutGroupModel[] = [];
}
