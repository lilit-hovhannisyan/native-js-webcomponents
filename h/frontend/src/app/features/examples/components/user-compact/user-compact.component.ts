import { Component, OnInit } from '@angular/core';
import { IUser } from 'src/app/core/models/resources/IUser';
import { Language } from 'src/app/core/models/language';

@Component({
  templateUrl: './user-compact.component.html',
  styleUrls: ['./user-compact.component.scss']
})
export class UserCompactComponent implements OnInit {
  public user: IUser;
  public userId: string;
  constructor() {}

  public ngOnInit() {
    // for example the data is hardcoded
    this.user = {
      avatarId: 'fa38a74c321f448f8e80f5e415f54c8d',
      displayName: 'Admin',
      email: 'admin@navido.de',
      firstName: 'Admin',
      id: 1,
      isActive: true,
      isSuperAdmin: false,
      language: Language.EN,
      lastName: 'Adminyan',
      username: 'Admin',
      isAdmin: true,
      tel: '',
      _type: 'User',
      hasEmail: true,
    };
    this.userId = '2';
  }

}
