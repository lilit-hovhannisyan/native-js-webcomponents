import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild,
  Router
} from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';

/**
 * This guard uses for almost every route, to check if user has permissions to view it
 */
@Injectable({
  providedIn: 'root'
})
export class AccessControlGuard implements CanActivateChild {

  constructor(
    private authService: AuthenticationService,
    private router: Router,
  ) {}

  public canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const canActivate = this.authService.canAccessUrl(state.url);
    const exitRoute = url(sitemap.main);

    if (this.authService.isLoggedIn() && !canActivate) {
      this.router.navigate([exitRoute]);
    }
    return canActivate;
  }
}
