import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IFieldOperationType, IFieldOperationTypeCreate, IFieldOperationTypeDTO } from '../../models/resources/IFieldOperationType';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class FieldOperationTypeRepositoryService extends AbstractRepository<
IFieldOperationType,
IFieldOperationTypeDTO,
IFieldOperationTypeCreate,
RepoParams<IFieldOperationType>,
number
> {
  protected resourceType = 'AutobankRuleFieldTypeToOperationType';
  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/autobank/rule-field-type-operation-types';
  }

  protected postProcess(item: IFieldOperationTypeDTO): IFieldOperationType { return item; }
}
