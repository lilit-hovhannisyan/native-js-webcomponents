import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { Observable } from 'rxjs';
import { IVesselAccountingListItem } from 'src/app/core/models/resources/IVesselAccounting';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup } from '@angular/forms';
import { IWording } from 'src/app/core/models/resources/IWording';
import { getVesselAccountingGridConfig } from 'src/app/core/constants/nv-grid-configs/vessel-accounting.config';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'nv-vessel-autocomplete',
  templateUrl: './vessel-autocomplete.component.html',
  styleUrls: ['./vessel-autocomplete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VesselAutocompleteComponent implements OnInit {
  public wording = wording;
  public gridConfig = getVesselAccountingGridConfig({}, () => false);

  @Input() public form: FormGroup;
  @Input() public controlName = 'vesselId';
  @Input() public label: IWording = wording.accounting.vessel;
  @Input() public dataSource$: Observable<IVesselAccountingListItem[]>
    = this.vesselMainService.fetchVesselsWithClosestAccounting(true);
  @Input() public useVesselAccountingId: boolean;

  @Output() public select = new EventEmitter<IVesselAccountingListItem>();

  constructor(
    private vesselMainService: VesselMainService,
  ) { }

  public ngOnInit(): void {
    if (this.useVesselAccountingId) {
      this.dataSource$ = this.dataSource$.pipe(startWith([]));
    } else {
      this.dataSource$ = this.dataSource$.pipe(
        startWith([]),
        map((vesselAccountings: IVesselAccountingListItem[]) => (
          vesselAccountings.map(v => {
            return {
              ...v,
              vesselAccountingId: v.id,
              id: v.vesselId,
            };
          })
        ))
      );
    }
  }

}
