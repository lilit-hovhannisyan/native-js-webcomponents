import { IVoucherType } from 'src/app/core/models/resources/IVoucherType';
import { VoucherTypeRepositoryService } from './../voucher-type-repository.service';
import { BookingRepositoryService, BookingRepoParams } from './../booking-repository.service';
import { MandatorRepositoryService } from './../mandator-repository.service';
import { IBooking, BookingKey } from './../../../models/resources/IBooking';
import { Injectable } from '@angular/core';
import { forkJoin, Observable, combineLatest } from 'rxjs';
import { map, filter } from 'rxjs/operators';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { SettingsService } from '../../settings.service';
import { IMandatorLookup } from 'src/app/core/models/resources/IMandator';

export interface IBookingFull extends IBooking {
  mandator: IMandatorLookup;
  voucherType: IVoucherType;
}

export interface IBookingRow extends IBookingFull {
  mandatorName: string;
  voucherTypeLabel: string;
}

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(
    private bookingRepo: BookingRepositoryService,
    private mandatorRepo: MandatorRepositoryService,
    private voucherTypeRepo: VoucherTypeRepositoryService,
    private settingsService: SettingsService
  ) { }

  private getBookingFullList(
    bookings: IBooking[],
    mandator: IMandatorLookup[],
    voucherTypes: IVoucherType[]
  ): IBookingFull[] {
    return bookings.map(booking => this.getBookingFull(booking, mandator, voucherTypes));
  }

  private getBookingFull(
    booking: IBooking,
    mandators: IMandatorLookup[],
    voucherTypes: IVoucherType[]
  ): IBookingFull {
    const mandator = mandators.find(x => x.id === booking.mandatorId);
    const voucherType = voucherTypes.find(x => x.id === booking.voucherTypeId);
    return { ...booking, mandator, voucherType };
  }

  public fetchAll(params: BookingRepoParams): Observable<IBookingFull[]> {
    return forkJoin([
      this.bookingRepo.fetchAll(params),
      this.mandatorRepo.fetchLookups({}),
      this.voucherTypeRepo.fetchAll({})
    ]).pipe(map(res => this.getBookingFullList(...res)));
  }

  public fetchOne(
    bookingId: BookingKey,
    params: BookingRepoParams
  ): Observable<IBookingFull> {
    return forkJoin([
      this.bookingRepo.fetchOne(bookingId, params),
      this.mandatorRepo.fetchLookups({}),
      this.voucherTypeRepo.fetchAll({})
    ]).pipe(map(res => this.getBookingFull(...res)));
  }

  public getStream(): Observable<IBookingFull[]> {
    return combineLatest([
      this.bookingRepo.getStream(),
      this.mandatorRepo.getLookupStream(),
      this.voucherTypeRepo.getStream()
    ]).pipe(
      // stop here if mandator or voucherTypes are empty
      filter(([bookings, mandator, voucherTypes]) => !!(mandator.length && voucherTypes.length)),
      map(res => this.getBookingFullList(...res))
    );
  }

  toRow(booking: IBookingFull): IBookingRow {
    const { voucherType, mandator } = booking;
    const labelKey = getLabelKeyByLanguage(this.settingsService.language);
    return ({
      ...booking,
      mandatorName: mandator.name,
      voucherTypeLabel: voucherType && voucherType[labelKey]
    });
  }

  toRows(bookings: IBookingFull[]): IBookingRow[] {
    return bookings.map((booking) => this.toRow(booking));
  }
}
