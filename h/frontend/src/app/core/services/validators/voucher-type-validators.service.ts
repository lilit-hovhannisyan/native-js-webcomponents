
import { ValidationErrors, AbstractControl } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { Injectable } from '@angular/core';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { VoucherTypeRepositoryService } from 'src/app/core/services/repositories/voucher-type-repository.service';
import { MandatorVoucherTypeRepositoryService } from 'src/app/core/services/repositories/mandator-voucher-type-repository.service';

@Injectable({
  providedIn: 'root'
})
export class VoucherTypeValidatorsService {

  constructor(
    private voucherTypeRepo: VoucherTypeRepositoryService,
    private mandatorVoucherTypeRepo: MandatorVoucherTypeRepositoryService
  ) { }

  public doesVoucherTypeExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundVoucherType = this.voucherTypeRepo.getStreamValue()
      .find(voucherType => voucherType.id === control.value);

    const isValid = foundVoucherType ? true : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      voucherNotFound: {
        message: wording.accounting.notFoundVoucherType
      }
    };
  }

  public isVoucherTypeActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const foundVoucherType = this.voucherTypeRepo.getStreamValue()
      .find(voucherType => voucherType.id === control.value);

    const isValid = foundVoucherType ? foundVoucherType.isActive : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      voucherTypeNotActive: {
        message: wording.accounting.notActiveVoucherType
      }
    };
  }

  public isVoucherTypeRelatedToMandator = (control: AbstractControl): ValidationErrors | null => {
    if (
      !control.value
      || !control.parent
      || !control.parent.get('mandatorId').value
    ) {
      return null;
    }

    const mandatorId = control.parent.get('mandatorId').value;
    const foundVoucherType = this.mandatorVoucherTypeRepo.getStreamValue()
      .find(mandatorVoucherType =>
        mandatorVoucherType.voucherTypeId === control.value
        && mandatorVoucherType.mandatorId === mandatorId);

    if (foundVoucherType) {
      return null;
    }

    markControlAsTouched(control);
    return {
      notRelatedToMandator: {
        message: wording.general.notRelatedToMandator
      }
    };
  }
}
