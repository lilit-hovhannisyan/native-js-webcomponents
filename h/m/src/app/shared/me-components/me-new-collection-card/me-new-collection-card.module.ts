import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';

import { IconsModule } from '@shared-modules/icons.module';

import { MeContextMenuModule } from '../me-context-menu/me-context-menu.module';
import { MeEllipsisRowModule } from '../me-ellipsis-row/me-ellipsis-row.module';

import { MeNewCollectionCardComponent } from './me-new-collection-card.component';
import { MeIconsModule } from '../me-icons/me-icons.module';

@NgModule({
  imports: [CommonModule, IconsModule, FormsModule, MatCheckboxModule, MeEllipsisRowModule, MeContextMenuModule, MeIconsModule],
  declarations: [MeNewCollectionCardComponent],
  exports: [MeNewCollectionCardComponent]
})
export class MeNewCollectionCardModule {}
