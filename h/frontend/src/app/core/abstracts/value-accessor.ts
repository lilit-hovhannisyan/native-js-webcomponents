import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { forwardRef, Type } from '@angular/core';

export abstract class AbstractValueAccessor<T = any> implements ControlValueAccessor {
  public onChange: (value: T) => void;
  public onTouched: (value: any) => void;

  public _value: T = '' as any;
  public _disabled = false;

  public get value(): T {
    return this._value;
  }

  public set value(value: T) {
    if (value !== this._value) {
      this._value = value;
      this.onChange(value);
    }
  }

  public get disabled(): boolean {
    return this._disabled;
  }

  public set disabled(isDisabled: boolean) {
    this._disabled = isDisabled;
  }

  public setDisabledState?(isDisabled: boolean): void {
    this._disabled = isDisabled;
  }

  public writeValue(value: T) {
    this._value = value;
  }

  public registerOnChange(fn: (_: T) => void): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }
}

export function makeProvider(type: Type<any>) {
  return {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => type),
    multi: true
  };
}
