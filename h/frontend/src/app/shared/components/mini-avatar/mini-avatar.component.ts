import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { IUser } from 'src/app/core/models/resources/IUser';

@Component({
  selector: 'nv-mini-avatar',
  templateUrl: './mini-avatar.component.html',
  styleUrls: ['./mini-avatar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MiniAvatarComponent {
  @Input() public user: IUser;
  @Input() public width = '30px';
  @Input() public height = '30px';
  @Input() public shape?: 'square' | 'circle';
}
