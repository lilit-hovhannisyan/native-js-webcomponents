import { Injectable } from '@angular/core';
import { ZoneKey, IZoneRow } from 'src/app/core/models/resources/IZone';
import { IZoneDTO, IZoneCreate, IZone } from 'src/app/core/models/resources/IZone';
import { AbstractRepository, RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { RepositoryService } from 'src/app/core/services/repository.service';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { specialZones } from 'src/app/authentication/services/zoneService/specialZones';
import { IWording } from 'src/app/core/models/resources/IWording';
import { SitemapNode } from 'src/app/core/constants/sitemap/sitemap';

@Injectable({
  providedIn: 'root'
})
export class ZoneRepositoryService extends AbstractRepository<
  IZone,
  IZoneDTO,
  IZoneCreate,
  RepoParams<IZone>,
  ZoneKey
>  {

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected resourceType = 'Zone';

  public getBaseUrl(): string {
    return '/api/zones/';
  }

  protected postProcess(item: IZoneDTO): IZone { return item; }

  public toRow(zone: IZone): IZoneRow {
    const specialZone: SitemapNode = specialZones.find(s => s.fullZone === zone.id);

    if (specialZone) {
      return {
        ...zone,
        fullTitle: specialZone.fullTitle,
      };
    }

    const sitemapEntry = SitemapEntry.getByZoneId(zone.id);

    if (sitemapEntry) {
      const fullTitle: IWording = sitemapEntry.toTitle();
      if (fullTitle) {
        return {
          ...zone,
          fullTitle,
        };
      }
    }

    return zone;
  }

  public toRows(zones: IZone[]) {
    return zones.map(this.toRow) as IZoneRow[];
  }
}
