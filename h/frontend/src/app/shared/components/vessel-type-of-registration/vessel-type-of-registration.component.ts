import {Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import { RegistrationTypes } from 'src/app/core/models/resources/IVesselRegister';
import { wording } from '../../../core/constants/wording/wording';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'nv-vessel-type-of-registration',
  templateUrl: './vessel-type-of-registration.component.html',
  styleUrls: ['./vessel-type-of-registration.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VesselTypeOfRegistrationComponent implements OnInit {
  @Input() public form: FormGroup;
  @Input() public readOnly: boolean;

  public RegistrationTypes = RegistrationTypes;
  public wording = wording;

  constructor(
  ) { }

  public ngOnInit(): void {
  }
}
