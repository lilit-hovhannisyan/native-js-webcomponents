import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { IAutobankBusinessCase } from 'src/app/core/models/resources/IAutobankBusinessCase';
import { AutobankBusinessCaseRepositoryService } from 'src/app/core/services/repositories/autobank-business-case-repository.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-autobank-business-cases-detail-view',
  templateUrl: './autobank-business-cases-detail-view.component.html',
  styleUrls: ['./autobank-business-cases-detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutobankBusinessCasesDetailViewComponent implements OnInit {

  @Input() public closeModal: () => void;
  public form: FormGroup;
  public dataSource$: Observable<IAutobankBusinessCase[]>;
  public wording = wording;

  public repoParams = {};

  protected resourceDefault = {} as IAutobankBusinessCase;

  constructor(
    private fb: FormBuilder,
    private autobankBusinessCaseRepo: AutobankBusinessCaseRepositoryService,
  ) { }

  public ngOnInit() {
    this.initForm();
  }


  private initForm(autobankBusinessCase: IAutobankBusinessCase = this.resourceDefault) {
    this.form = this.fb.group({
      labelEn: [autobankBusinessCase.labelEn, [Validators.required, Validators.maxLength(150)]],
      labelDe: [autobankBusinessCase.labelDe, [Validators.required, Validators.maxLength(150)]],
      gvc: [autobankBusinessCase.gvc, [Validators.required, Validators.max(999), Validators.min(1)]],
    });
  }

  public save() {
    const { value } = this.form;
    this.autobankBusinessCaseRepo
      .create(value, {}).subscribe(() => this.closeModal());
  }
}
