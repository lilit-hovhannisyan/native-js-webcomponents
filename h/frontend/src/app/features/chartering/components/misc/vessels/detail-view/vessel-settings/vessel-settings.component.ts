import { ActivatedRoute } from '@angular/router';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/vessel-settings.config';
import { IVesselSetting, IVesselSettingWithLabel, VesselSettingKey } from 'src/app/core/models/resources/IVesselSetting';
import { VesselSettingsMainService } from 'src/app/core/services/mainServices/vessel-settings-main.service';
import { VesselsHelperService } from '../vessels.helper.service';
import { vesselSettingTypeOptions } from 'src/app/core/models/enums/vessel-setting-type';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { wording } from 'src/app/core/constants/wording/wording';
import { VesselSettingsRepositoryService } from 'src/app/core/services/repositories/vessel-settings-repository.service';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { getDefaultIntervalListItem, getDateTimeFormat, fetchFormattedIntervals, fetchFormattedFormValue } from 'src/app/shared/helpers/general';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  selector: 'nv-vessel-settings-list-view',
  templateUrl: './vessel-settings.component.html',
  styleUrls: ['./vessel-settings.component.scss']
})
export class VesselSettingsListViewComponent implements OnInit {
  public node: SitemapNode = sitemap.chartering.children.misc.children.vessels
    .children.id.children.settings;
  public zone: string = SitemapEntry.getByNode(this.node).toZone();
  private vesselId: VesselKey;
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IVesselSettingWithLabel[]> = of([]);

  public editMode = false;
  private editMode$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public inCreationMode: boolean;
  public selectedSetting: IVesselSetting;
  public settings: IVesselSetting[] = [];
  public settingTypes: IVesselSettingWithLabel[] = Array
    .from(vesselSettingTypeOptions, ([name, value]) => ({ name: value, value: name }));
  public form: FormGroup;
  public wording = wording;
  public dateTimeFormat: string;

  constructor(
    private vesselSettingsMainService: VesselSettingsMainService,
    private vesselSettingsRepo: VesselSettingsRepositoryService,
    private vesselsHelperService: VesselsHelperService,
    public gridConfigService: GridConfigService,
    public route: ActivatedRoute,
    public authService: AuthenticationService,
    private fb: FormBuilder,
    private confirmationService: ConfirmationService,
    private cdr: ChangeDetectorRef,
    private settingsService: SettingsService,
  ) { }

  public ngOnInit() {
    this.dateTimeFormat = getDateTimeFormat(this.settingsService.locale);
    this.vesselId = parseInt(this.route.parent.snapshot.params.id, 10);
    this.initForm();
    this.fetchSettings();
    this.initData();
    this.initConfig();
  }

  private fetchSettings(selectIdToSelect: number = null): void {
    this.vesselSettingsMainService.fetchAllByVesselId(this.vesselId)
      .pipe(
        map(settings => fetchFormattedIntervals(settings))
      )
      .subscribe(settings => {
        this.settings = settings;
        if (settings.length) {
          const vesselSetting: IVesselSetting = selectIdToSelect
            ? settings.find(r => r.id === +selectIdToSelect)
            : getDefaultIntervalListItem(settings);
          this.selectSetting(vesselSetting || settings[settings.length - 1]);
        }
      });
  }

  private initData(): void {
    this.dataSource$ = this.editMode$.asObservable().pipe(
      map(editMode => {
        const settingsToDisplay: IVesselSettingWithLabel[] = editMode
          ? this.settingTypes
            .map(s => ({
              ...s,
              _checked: this.isSettingTypeEnabled(
                s.value,
                this.selectedSetting?.serviceType
              )
            }))
          : this.settingTypes.filter(s => this.isSettingTypeEnabled(
            s.value,
            this.selectedSetting?.serviceType)
          );
        return settingsToDisplay;
      }));
  }

  private isSettingTypeEnabled(
    settingBitmask: number,
    settingType: number,
  ): boolean {
    return (settingBitmask && (settingType & settingBitmask)) === settingBitmask;
  }

  private initForm(setting: IVesselSetting = {} as IVesselSetting): void {
    this.form = this.fb.group({
      from: [setting.from, Validators.required],
      until: [setting.until, Validators.isAfterFieldDate('from')],
    });

    // the form needs a little delay to set a new status correctly
    setTimeout(() => {
      this.form.disable();
      this.form.markAsPristine();
    });
  }

  public enterEditMode() {
    this.editMode = true;
    this.editMode$.next(true);
    this.toggleGridSelectionType();
    // the form needs a little delay to set a new status correctly
    setTimeout(() => { this.form.enable(); });
  }

  public cancelEditing() {
    this.editMode = false;
    this.inCreationMode = false;
    this.editMode$.next(false);
    this.toggleGridSelectionType();
    this.initForm(this.selectedSetting);
    // the form needs a little delay to set a new status correctly
    setTimeout(() => {
      this.form.disable();
      if (this.selectedSetting.id) {
        this.fetchSettings(this.selectedSetting.id);
      }
      this.selectedSetting = {} as IVesselSetting;
      this.initData();
    });
  }

  public toggleGridSelectionType = (): void => {
    this.gridConfig = {
      ...this.gridConfig,
      rowSelectionType: this.editMode
        ? NvGridRowSelectionType.Checkbox
        : undefined
    };
  }

  public submit(): void {
    if (this.form.invalid) {
      return;
    }

    if (!this.selectedSetting.serviceType) {
      this.confirmationService.warning({
        title: wording.general.warning,
        wording: wording.chartering.youNeedToCheckServiceType,
        okButtonWording: wording.general.ok,
        cancelButtonWording: null,
      });
      return;
    }
    const settingIdToSelect: VesselSettingKey | null = this.inCreationMode
      ? null
      : this.selectedSetting.id;

    const value: IVesselSetting = fetchFormattedFormValue({
      ...this.form.value,
      vesselId: this.vesselId,
    });

    const payload: IVesselSetting = this.inCreationMode
      ? { serviceType: 0, ...this.selectedSetting, ...value, id: null }
      : { ...this.selectedSetting, ...value };

    const request$: Observable<IVesselSetting> = this.inCreationMode
      ? this.vesselSettingsRepo.create(payload, {})
      : this.vesselSettingsRepo.update(payload, {});

    request$.subscribe(() => {
      this.cancelEditing();
      this.fetchSettings(settingIdToSelect);
      this.initData();
      this.vesselsHelperService.isSettingsMissing$.next(
        !this.settings[this.settings.length - 1]?.serviceType
      );
    });
  }

  public rowSelectionChanged(activeServices: IVesselSettingWithLabel[]) {
    if (this.editMode) {
      if (activeServices.length > 0) {
        this.selectedSetting.serviceType = activeServices
          .reduce((acc, cur) => acc | cur.value, activeServices[0].value);
      } else {
        this.selectedSetting.serviceType = 0;
      }
    }
  }

  private initConfig(): void {
    this.gridConfig = getGridConfig();
  }

  public selectSetting = (setting: IVesselSetting) => {
    this.selectedSetting = setting;
    this.initForm(setting);
    this.initData();
    // used to avoid ExpressionChangedAfterItHasBeenCheckedError
    this.cdr.detectChanges();
  }

  public intervalClicked = (settingId: VesselSettingKey) => {
    const setting: IVesselSetting = this.settings.find(s => s.id === settingId);
    this.selectSetting(setting);
  }

  public addIntervalClicked = () => {
    this.inCreationMode = true;

    const latestEntry: IVesselSetting = this.settings[this.settings.length - 1];
    const clonedSetting: IVesselSetting = latestEntry
      ? {
        ...latestEntry,
        from: latestEntry.until,
        until: undefined,
      }
      : {} as IVesselSetting;

    this.selectedSetting = { serviceType: latestEntry?.serviceType } as IVesselSetting;

    delete clonedSetting.id; // we don't need the id in creation mode
    this.initForm(clonedSetting); // cloning the most recent one
    this.enterEditMode();
  }
}
