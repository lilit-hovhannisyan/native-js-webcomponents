import { MeAttributeTypes } from '@shared/me-attribute-types';

export const PAGINATION_TOTAL_COUNT = 0;
export const PAGE_SIZE_DEFAULT_VALUE = 50;
export const PAGE_INDEX_DEFAULT_VALUE = 0;
export const PAGE_SIZE_OPTIONS_DEFAULT_VALUE = [5, 10, 15, 100];
export const TableComponentTypes = {
  ...MeAttributeTypes,
  SELECT: 'select',
  STRING: 'string',
  IMAGE_WITH_TITLE: 'imageWithTitle'
};
