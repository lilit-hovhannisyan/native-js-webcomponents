import { MeUserImageI } from '../me-user-image/me-user-image.interface';

export interface MeHeaderI {
  user: {
    userImage: MeUserImageI;
    firstName?: string;
    lastName?: string;
    company?: string;
    email?: string;
    preferredName?: string;
  };
  placeholders?: {
    profileSettings?: string;
    userGuide?: string;
    logout?: string;
  };
  helpMenuItems?: {
    key: string;
    displayName: string;
  }[];
}
