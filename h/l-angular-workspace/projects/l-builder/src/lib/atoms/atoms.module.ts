import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';

import {TranslateService} from '@ngx-translate/core';


// components
import {ButtonComponent} from './button/button.component';

import {InputComponent} from './input/input.component';
import {ImageComponent} from './image/image.component';
import {IconComponent} from './icon/icon.component';
import {CheckboxComponent} from './checkbox/checkbox.component';
import {CheckboxListComponent} from './checkbox-list/checkbox-list.component';
import {RadioButtonComponent} from './radio-button/radio-button.component';
import {RadioButtonListComponent} from './radio-button-list/radio-button-list.component';
import {MsgComponent} from './msg/msg.component';
import {DatepickerComponent} from './datepicker/datepicker.component';
import {FileUploadComponent} from './file-upload/file-upload.component';
import {FileUploadResultsComponent} from './file-upload-results/file-upload-results.component';
import {SelectAutocompleteComponent} from './select-autocomplete/select-autocomplete.component';
import {LinkComponent} from './link/link.component';
import {TooltipComponent} from './tooltip/tooltip.component';
import {PaginationComponent} from './pagination/pagination.component';
import {TextComponent} from './text/text.component';
import {ContainerComponent} from './container/container.component';
import {TextBlockComponent} from './text-block/text-block.component';
import {EventContainerComponent} from './event-container/event-container.component';
import {SlideBarComponent} from './slide-bar/slide-bar.component';
import {TextareaComponent} from './textarea/textarea.component';


// services
import {ValidationService} from '../services/validation.service';


// components not to export
import {GraphicsComponent} from './graphics/graphics.component';


@NgModule({
  declarations: [
    GraphicsComponent,

    ButtonComponent,
    InputComponent,
    ImageComponent,
    IconComponent,
    CheckboxComponent,
    CheckboxListComponent,
    RadioButtonComponent,
    RadioButtonListComponent,
    MsgComponent,
    DatepickerComponent,
    FileUploadComponent,
    FileUploadResultsComponent,
    SelectAutocompleteComponent,
    LinkComponent,
    TooltipComponent,
    PaginationComponent,
    TextComponent,
    TextBlockComponent,
    ContainerComponent,
    EventContainerComponent,
    TextareaComponent,

    SlideBarComponent
  ],
  imports: [
    CommonModule,
      RouterModule
    // TranslateModule
  ],
  providers: [
    // TranslateService
      ValidationService
  ],
  exports: [
    ButtonComponent,
    InputComponent,
    ImageComponent,
    IconComponent,
    CheckboxComponent,
    CheckboxListComponent,
    RadioButtonComponent,
    RadioButtonListComponent,
    MsgComponent,
    DatepickerComponent,
    FileUploadComponent,
    FileUploadResultsComponent,
    SelectAutocompleteComponent,
    LinkComponent,
    TooltipComponent,
    PaginationComponent,
    TextComponent,
    TextBlockComponent,
    ContainerComponent,
    EventContainerComponent,
    TextareaComponent,

    SlideBarComponent
  ]
})
export class LavashAtomsModule {
}
