import { Component, TemplateRef, Type, Input, ChangeDetectionStrategy } from '@angular/core';
import {
  ReadableRolesPermissionsComponent
} from 'src/app/shared/components/readable-roles-permissions/readable-roles-permissions.component';
import { EditableRolesPermissionsComponent } from '../editable-roles-permissions/editable-roles-permissions.component';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { NzModalService } from 'ng-zorro-antd/modal';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-topic-context-menu',
  templateUrl: './topic-context-menu.component.html',
  styleUrls: ['./topic-context-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopicContextMenuComponent {
  public wording = wording;
  @Input() public selectedTopic: SitemapNode;

  constructor(private nzModalService: NzModalService) {
  }

  public openModal<T>(content: string | TemplateRef<{}> | Type<T>): void {
    this.nzModalService.create(
      {
        ...DefaultModalOptions,
        nzContent: content,
        nzClosable: false,
        nzMaskClosable: true,
        nzComponentParams: {
          selectedTopic: this.selectedTopic
        } as any,
      }
    );
  }

  public openEditablePermissions(): void {
    this.openModal<EditableRolesPermissionsComponent>(EditableRolesPermissionsComponent);
  }

  public openReadablePermissions(): void {
    this.openModal<ReadableRolesPermissionsComponent>(ReadableRolesPermissionsComponent);
  }
}
