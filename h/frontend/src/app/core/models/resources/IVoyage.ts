import { IResource } from './IResource';
import { IVesselAccounting } from './IVesselAccounting';

export type VoyageKey = number;

interface ISharedProps {
  from: string;
  no?: number;
  until: string;
  vesselAccountingId: number;
  year: number;
}

export interface IVoyageCreate extends ISharedProps { }

export interface IVoyageBase extends ISharedProps, IResource<VoyageKey> { }

export interface IVoyageDTO extends IVoyageBase { }

export interface IVoyage extends IVoyageDTO { }

export interface IVoyageFull extends IVoyage {
  vesselAccounting?: IVesselAccounting;
}

export interface IVoyageRow extends IVoyageFull {
  yearAndNo: string;
}

