import { IUser, ITokenResource } from '../../core/models/resources/IUser';
import { IRole } from 'src/app/core/models/resources/IRole';


export interface SessionInfo {
  user: IUser;
  tokenResource: ITokenResource;
  roles: IRole[];
}
