import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseWebService } from '../../core/services/base.web-service';

import { ArrayResponseI } from '@core-interfaces/array-response.interface';

import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { MaterialModel } from '@shared-models/materials/material-model';
import { BaseSearchModel } from '@shared/models/search/search-base.model';
import { constructUrl } from '@shared/utils';
import { MeDocumentModel } from '@shared/models/document/document.model';

@Injectable()
export class MaterialsWebService {
  constructor(private baseWebService: BaseWebService) {}

  getMeMaterialById(materialOID: string): Observable<MaterialModel> {
    return this.baseWebService.getRequest<MaterialModel>(`${MARKETPLACE_BASE_API_URL}/materials/${encodeURI(materialOID)}`, MaterialModel);
  }

  getMeMaterials(skip: number, top: number): Observable<ArrayResponseI<MaterialModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/materials`, skip, top);
    return this.baseWebService.getRequestForArray<MaterialModel>(url, MaterialModel);
  }

  deleteMaterials(materialOids: string[]): Observable<void> {
    return this.baseWebService.deleteRequest<void>(`${MARKETPLACE_BASE_API_URL}/materials`, materialOids);
  }

  deleteMeMaterialDocuments(materialOID: string, documentsOIDs: string[]): Observable<string> {
    return this.baseWebService.deleteRequest<string>(
      `${MARKETPLACE_BASE_API_URL}/materials/${encodeURI(materialOID)}/documents`,
      documentsOIDs
    );
  }

  getMeMaterialDocuments(materialOID: string): Observable<ArrayResponseI<MeDocumentModel>> {
    return this.baseWebService.getRequestForArray<MeDocumentModel>(
      `${MARKETPLACE_BASE_API_URL}/materials/${encodeURI(materialOID)}/documents`,
      MeDocumentModel
    );
  }

  addMeMaterialDocuments(materialOID: string, data: FormData): Observable<unknown> {
    return this.baseWebService.postRequestForArray<MaterialModel, FormData>(
      `${MARKETPLACE_BASE_API_URL}/materials/${encodeURI(materialOID)}/documents`,
      data,
      MaterialModel
    );
  }

  searchMaterials(data: BaseSearchModel, skip?: number, top?: number): Observable<ArrayResponseI<MaterialModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/materials/search`, skip, top);
    return this.baseWebService.postRequestForArray<MaterialModel, BaseSearchModel>(url, data, MaterialModel);
  }

  getMaterialStates(materialOID: string): Observable<ArrayResponseI<MaterialState>> {
    return this.baseWebService.getRequestForArray<MaterialState>(
      `${MARKETPLACE_BASE_API_URL}/materials/${encodeURI(materialOID)}/states`,
      MaterialState
    );
  }

  changeMaterialState(materialOID: string, newState: string): Observable<MaterialModel> {
    return this.baseWebService.postRequest<MaterialModel, { state: string }>(
      `${MARKETPLACE_BASE_API_URL}/materials/${encodeURI(materialOID)}/state`,
      { state: newState },
      MaterialModel
    );
  }
}

class MaterialState {
  public internalName!: string;
  public enabled!: boolean;
}
