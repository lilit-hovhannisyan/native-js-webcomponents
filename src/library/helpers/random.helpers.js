export const externalStyleBasePath = './src/library/components/';

function stringContainsUppercase(str) {
  return /[A-Z]/.test(str);
}

function convertToSnakeCase(str) {
  let snakeCaseStr = '';

  const [...strArr] = str;

  strArr.map(i => {
    if (stringContainsUppercase(i)) {
      snakeCaseStr += '-' + i.toLowerCase();
    } else {
      snakeCaseStr += i;
    }
  });

  return snakeCaseStr;
}

const setAtributesOnElement = (el, attribbuteListObj) => {
  if (!attribbuteListObj) { return; }


  for (let [key, value] of Object.entries(attribbuteListObj)) {
    let keyName = key;

    if (stringContainsUppercase(keyName)) {
      keyName = convertToSnakeCase(keyName);
    }
    el.setAttribute(keyName, value);
  }
}

const setInnerText = (el, text) => {
  if (!text) { return; }
  el.innerText = text;
}

const setInnerHtml = (el, html) => {
  if (!html) { return; }

  el.innerHTML = html;
}

const setEvents = (el, eventList) => {
  if (!eventList) { return; }

  for (let [key, value] of Object.entries(eventList)) {
    el.addEventListener(key, value);
  }
}

export const createElementWithAttributes = (elConfig) => {
  const tagName = elConfig.tagName;

  const el = document.createElement(tagName);
  setAtributesOnElement(el, elConfig?.attr);
  setInnerText(el, elConfig?.text);
  setInnerHtml(el, elConfig?.html);
  setEvents(el, elConfig?.events);

  if (!elConfig.subElements) {
    return el;
  }

  for (let subEl of elConfig.subElements) {
    const childEl = createElementWithAttributes(subEl);
    el.appendChild(childEl);
  }

  return el;
}

export const setInlineStylesForComponent = (stylesSource) => {
  const style = document.createElement('style');
  style.textContent = stylesSource;

  return style;
}

export const setExternileStylesForComponent = (stylesSource) => {
  const linkElem = document.createElement('link');
  linkElem.setAttribute('rel', 'stylesheet');
  linkElem.setAttribute('href', stylesSource);

  return linkElem;
}

export const canElementBeRendered = (value) => {
  return (!!value && value !== 'null');
}

export const removeStaticAtributesFromElNotToRender = (el, attrList) => {
  for (let attr of attrList) {
    el.removeAttribute(attr);
  }
}

export function traverseInShadowAndGetElement(baseNode, nodeNamToFind) {
  let foundNode;
  const nestedDom = baseNode.shadowRoot ? baseNode.shadowRoot : baseNode;

  if (!nestedDom) {
    return;
  }

  const childNodes = Array.from(nestedDom.childNodes);

  foundNode = childNodes.find(node => node.nodeName.toLowerCase() === nodeNamToFind);

  if (!foundNode) {
    const x = childNodes.filter(n => n.children)
      x.forEach(i => {
        if (!foundNode) {
          foundNode = traverseInShadowAndGetElement(i, nodeNamToFind);
        }
      });
  }

  //console.log('foundNode', foundNode)
  return foundNode;
}

export const getParentNodeByTagName = (child, parentNodeName) => {
  let parentNode;

  if (child.parentNode.nodeName === parentNodeName) {
    parentNode = child.parentNode;
  } else {
    parentNode = getParentNode(child.parentNode, parentNodeName);
  }

  return parentNode;
}

export const updateHistoryAndDocumentTitle = (title, url) => {
  document.title = title || document.title;
  history.pushState({}, title, url);
}
