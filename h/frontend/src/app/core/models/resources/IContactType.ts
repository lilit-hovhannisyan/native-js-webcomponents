import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';

export type ContactKey = number;

interface ISharedProps extends IWithLabels { }

export interface IContactTypeCreate extends ISharedProps { }

export interface IContactTypeBase extends ISharedProps, IResource<number> { }

export interface IContactTypeDTO extends IContactTypeBase { }

export interface IContactType extends IContactTypeDTO, IWithLabel { }




