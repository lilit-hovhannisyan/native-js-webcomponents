import { VesselKey } from './IVessel';
import { IResource } from './IResource';
import { RoleKey } from './IRole';

export type VesselRoleKey = number;

interface ISharedProps {
  vesselId: VesselKey;
  roleId: RoleKey;
}

export interface IVesselRoleBase extends ISharedProps, IResource<VesselRoleKey> { }

export interface IVesselRoleCreate extends ISharedProps { }

export interface IVesselRoleDTO extends IVesselRoleBase { }

export interface IVesselRole extends IVesselRoleDTO { }
