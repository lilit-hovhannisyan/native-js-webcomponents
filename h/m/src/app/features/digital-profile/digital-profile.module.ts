import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DigitalProfileRoutingModule } from './digital-profile-routing.module';
import { DigitalProfileStoreService } from '@features/digital-profile/digital-profile-store.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, DigitalProfileRoutingModule],
  providers: [DigitalProfileStoreService]
})
export class DigitalProfileModule {}
