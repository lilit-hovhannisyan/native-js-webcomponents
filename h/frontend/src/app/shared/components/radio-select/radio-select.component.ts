import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractValueAccessor, makeProvider } from 'src/app/core/abstracts/value-accessor';
import { Language } from 'src/app/core/models/language';
import { ResourceKey } from 'src/app/core/models/resources/IResource';
import { IWording } from 'src/app/core/models/resources/IWording';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { ActivatedRoute, Router } from '@angular/router';

export interface IRadioSelectItem<K extends ResourceKey> {
  id: K;
  displayLabel: string;
}

@Component({
  selector: 'nv-radio-select',
  templateUrl: './radio-select.component.html',
  styleUrls: ['./radio-select.component.scss'],
  providers: [ makeProvider(RadioSelectComponent) ],
})
export class RadioSelectComponent extends AbstractValueAccessor implements OnChanges {
  @Input() public placeholder?: IWording;
  @Input() public sourceArray: IRadioSelectItem<ResourceKey>[];
  @Input() public isDisabled = false;
  @Input() public withRouterLink = false;
  public searchValue: string;
  public searchResult = [];
  public selectedItemId: ResourceKey;
  public language: Language = this.baseComponentService.settingsService.language;
  public wording = this.baseComponentService.wordingService.wording;

  constructor(
    private baseComponentService: BaseComponentService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    super();
  }

  public ngOnChanges(changes: SimpleChanges) {
    const { sourceArray } = changes;
    if (sourceArray) {
      this.onSearch();
    }
  }

  public onSearch(): void {
    this.searchResult = this.sourceArray?.filter((item) => {
      return item?.displayLabel.toLowerCase().includes(this.searchValue?.toLowerCase() || '');
    });
  }

  public onSelect(itemId: ResourceKey): void {
    if (!this.onChange) {
      return;
    }

    this.value = itemId;

    /**
     * the nz-radio-group component prevents event, so the anchor tag can't work
     */
    if (this.withRouterLink) {
      const parentUrl = '/' + this.activatedRoute.pathFromRoot.map(r => r.snapshot.url).filter(f => !!f[0]).map(([f]) => f.path).join('/');
      const fullUrl = this.router.url;
      const restUrl = fullUrl.replace(parentUrl, '').split('/').filter(str => !!str);
      restUrl.shift();
      const url = `${parentUrl}/${itemId}/${restUrl.join('/')}`;

      this.router.navigate([url]);
    }
  }
}
