import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ActivityLogsService } from '../../../../core/services/activity-logs.service';
import { ILogCreate } from 'src/app/core/models/resources/ILog';

@Component({
  selector: 'nv-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogsComponent {

  constructor(private logsService: ActivityLogsService) { }

  public addLog = () => {
    const logInfo: ILogCreate = {
      message: {
        en: `this is log ${this.logsService.logsSubject.value.length + 1}`,
        de: `this is log ${this.logsService.logsSubject.value.length + 1}`
      },
      subject: 'Aenean commodo',
    };
    this.logsService.addLog(logInfo);
  }
}
