import { IResource } from './IResource';
import { CostTypeKey, ICostType } from './ICostTypes';
import { BookingAccountKey } from './IBookingAccount';

export type AccountCostTypeKey = number;

interface ISharedProps {
  costTypeId: CostTypeKey;
  bookingAccountId: BookingAccountKey;
}

export interface IAccountCostTypeCreate extends ISharedProps { }

export interface IAccountCostTypeBase extends ISharedProps, IResource<AccountCostTypeKey> { }

export interface IAccountCostTypeDTO extends IAccountCostTypeBase { }

export interface IAccountCostType extends IAccountCostTypeDTO { }

export interface IAccountRelatedCostType extends ICostType {
  relationId?: AccountCostTypeKey;
}
