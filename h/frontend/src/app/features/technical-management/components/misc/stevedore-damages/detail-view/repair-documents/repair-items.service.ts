import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { IRepairItem, IRepairItemFull, IRepairItemRow } from '../../../../../../../core/models/resources/IRepairItem';
import { IArticleFull } from 'src/app/core/models/resources/IArticle';
import { StevedoreRepairMainService } from 'src/app/core/services/mainServices/stevedore-repair-main.service';
import { map, timestamp } from 'rxjs/operators';
import * as moment from 'moment';
import { ExchangeRateService } from 'src/app/core/services/mainServices/exchange-rate.service';
import { IExchangeRate, IExchangeRateFull } from 'src/app/core/models/resources/IExchangeRate';
import { GridComponent } from 'nv-grid';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { toFloat } from 'src/app/core/helpers/general';

@Injectable({
  providedIn: 'root'
})
export class RepairItemsService {
  public selectedCurrency$: BehaviorSubject<ICurrency> = new BehaviorSubject({} as ICurrency);
  public repairItemsSubject$: BehaviorSubject<IRepairItemRow[]> = new BehaviorSubject([]);
  public repairItems$: Observable<IRepairItemRow[]>;
  private gridComponent: GridComponent;
  private stevedoreReportId: number;
  private id = -1;

  constructor(
    private stevedoreRepairService: StevedoreRepairMainService,
    private exchangeRateService: ExchangeRateService
  ) { }

  public init(gridComponent: GridComponent, stevedoreReportId: number) {
    this.gridComponent = gridComponent;

    this.stevedoreReportId = stevedoreReportId;
    const today = moment().toISOString();
    const queryParams = {
      startDate: today,
      endDate: today,
    };
    this.repairItems$ = combineLatest([
      this.exchangeRateService.fetchAll({ queryParams }).pipe(timestamp()),
      this.repairItemsSubject$.asObservable().pipe(timestamp()),
      this.selectedCurrency$.asObservable().pipe(timestamp()),
    ]).pipe(
      map(([exchangeRates, repairItems, selectedCurrency]) => {
        const formRepairItems = this.gridComponent.rawRows;
        return this.calculateRepairItems(
          exchangeRates.value,
          this.exchangeRateService.getRate(selectedCurrency.value.id),
          repairItems.timestamp >= selectedCurrency.timestamp
            ? repairItems.value
            : formRepairItems
        );
      })
    );
  }

  public convertArticleToRepairItem(article: IArticleFull): IRepairItemRow {
    const repairItem = {
      articleId: article.id,
      quantity: 1,
      pricePerUnit: article.salePrice || 0,
      stevedoreDamageRepairId: this.stevedoreReportId,
      total: article.salePrice || 0,
      id: --this.id, // we pass negative id's for new DTO's so those could be edited in the inline-grid
    } as IRepairItem;
    return this.stevedoreRepairService
      .toRowRepairItems(this.getFullRepairItem(repairItem, article));
  }

  public getFullRepairItem(repairItem: IRepairItem, article: IArticleFull): IRepairItemFull {
    return {
      ...repairItem,
      article: article,
      articleUnit: article.articleUnit,
      currency: article.currency,
    };
  }

  public addNewRepairItems(newRepairItems: IRepairItemRow[]) {
    const existingRepairItems = this.gridComponent.rawRows;
    newRepairItems.forEach((newRepairItem) => {
      const foundRepairItemIndex = existingRepairItems
        .findIndex(r => r.articleId === newRepairItem.articleId);
      if (foundRepairItemIndex !== -1) {
        const foundRepairItem = existingRepairItems[foundRepairItemIndex];

        existingRepairItems[foundRepairItemIndex] = {
          ...foundRepairItem,
          quantity: +foundRepairItem.quantity + 1,
          total: toFloat((+foundRepairItem.quantity + 1) * foundRepairItem.pricePerUnit),
        };

      } else {
        existingRepairItems.push(newRepairItem);
      }

    });

    this.repairItemsSubject$.next(this.makeRowsUnEdited(existingRepairItems));
  }

  public makeRowsUnEdited(repairItems: IRepairItemRow[]): IRepairItemRow[] {
    const repairItemsToChange = repairItems;
    repairItemsToChange.forEach((v, i) => {
      // grid adds "_edited: true" property to the row which was edited, and when we send
      // new dataStream the property stays there and discard button does not disappear,
      // thats why we need to remove that property before getting new dataStream
     const currentElement = repairItemsToChange[i] as any;
     if (currentElement._edited) {
       currentElement._edited = false;
     }
   });

   return repairItemsToChange;
  }

  public calculateRepairItems(
    exchangeRates: IExchangeRateFull[],
    transactionExchangeRate: IExchangeRate,
    repairItems: IRepairItemRow[],
  ): IRepairItemRow[] {
    return repairItems.map(item => {
      return this.calculateRepairItem(exchangeRates, transactionExchangeRate, item);
    });
  }

  public calculateRepairItem(
    exchangeRates: IExchangeRateFull[],
    transactionExchangeRate: IExchangeRate,
    repairItem: IRepairItemRow
  ): IRepairItemRow {
    const itemCurrency = repairItem.currency || exchangeRates.find(e => e.isoCode === 'USD');
    const itemExchangeRate = this.exchangeRateService.getRate(itemCurrency.id);
    const exchangeRateFrom = itemExchangeRate
      ? itemExchangeRate.rate || 1
      : 1;
    const exchangeRateTo = transactionExchangeRate
      ? transactionExchangeRate.rate || 1
      : 1;
    const exchangeRateFinal = exchangeRateFrom / exchangeRateTo;
    const pricePerUnit = toFloat(repairItem.pricePerUnit / exchangeRateFinal);
    const transactionCurrency = this.selectedCurrency$.getValue();
    return {
      ...repairItem,
      pricePerUnit,
      total: toFloat(pricePerUnit * repairItem.quantity),
      currency: transactionCurrency,
      isoCode: transactionCurrency.isoCode
    };
  }
}
