import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { wording } from 'src/app/core/constants/wording/wording';
import { Validators } from '../../../../core/classes/validators';
import { validateAllFormFields } from '../../../helpers/validateAllFormFields';
import { AuthenticationService } from '../../../../authentication/services/authentication.service';
import { ICalendar } from '../../../../core/models/resources/ICalendar';
import { EventInput } from '@fullcalendar/angular';
import { ICalendarEvent } from '../../../../core/models/resources/ICalendarEvent';
import { CalendarEventsRepositoryService } from '../../../../core/services/repositories/calendar-events-repository.service';
import * as moment from 'moment';
import { ColorEvent } from 'ngx-color';

@Component({
  selector: 'nv-calendar-event-modal',
  templateUrl: './calendar-event-modal.component.html',
  styleUrls: [ './calendar-event-modal.component.scss' ]
})
export class CalendarEventModalComponent implements OnInit {
  @Input() public closeModalAndUpdateData: () => void;
  @Input() public calendars: ICalendar[];
  @Input() public data: EventInput;
  @Input() public startDate: string;
  @Input() public isDisabled: boolean;
  public isColorPickerOpened = false;

  public wording = wording;
  public form: FormGroup;
  public isSuperAdmin = this.authService.getSessionInfo().user.isSuperAdmin;

  constructor(
    private nzModalComponent: NzModalRef,
    private fb: FormBuilder,
    private formBuilder: FormBuilder,
    private calendarEventsRepo: CalendarEventsRepositoryService,
    private authService: AuthenticationService,
  ) { }

  public ngOnInit(): void {
    const event: ICalendarEvent = this.data?.event ? this.data.event.extendedProps : {} as ICalendarEvent;
    this.form = this.formBuilder.group({
      id: [event.id],
      labelDe: [event.labelDe, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      labelEn: [event.labelEn, [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      calendarId: [event.calendarId, [Validators.required]],
      start: [event.start || this.startDate, [Validators.required]],
      end: [event.end],
      remark: [event.remark],
      color: [event.color || '#c44747'],
      isWholeDayEvent: [event.isWholeDayEvent],
      isSystemDefault: [event.isSystemDefault]
    });

    this.onToggleIsWholeDay();
  }

  public onToggleIsWholeDay(): void {
    const control: AbstractControl = this.form.get('end');

    if (this.form.get('isWholeDayEvent').value) {
      control.patchValue(null);
      control.clearValidators();
    } else {
      control.setValidators(Validators.required);
    }
  }

  public handleColorPickerHover(hovered: boolean): void {
    this.isColorPickerOpened = hovered;
  }

  public handleColorChange(event: ColorEvent): void {
    this.form.get('color').setValue(event.color.hex);
  }

  public submitCalendarEvent(): void {
    validateAllFormFields(this.form);

    if (!this.form.valid) {
      return;
    }

    const value = this.form.value;

    value.id
      ? this.calendarEventsRepo.update(value, {}).subscribe(() => this.closeModalAndUpdateData())
      : this.calendarEventsRepo.create(value, {}).subscribe(() => this.closeModalAndUpdateData());
  }
}
