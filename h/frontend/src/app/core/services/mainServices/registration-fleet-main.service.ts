import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { convertToMap } from 'src/app/shared/helpers/general';
import { wording } from '../../constants/wording/wording';
import { findClosestWithFromUntil } from '../../helpers/general';
import { IRegistrationFleet } from '../../models/IRegistrationFleet';
import { CompanyKey, ICompany } from '../../models/resources/ICompany';
import { CountryKey, ICountry } from '../../models/resources/ICountry';
import { DebitorCreditorKey, IDebitorCreditor } from '../../models/resources/IDebitorCreditor';
import { IVessel } from '../../models/resources/IVessel';
import { IVesselAccounting } from '../../models/resources/IVesselAccounting';
import { IVesselPreDelayCharterName } from '../../models/resources/IVesselPreDelayCharterName';
import { IVesselRegister, RegistrationTypes } from '../../models/resources/IVesselRegister';
import { IVesselSetting } from '../../models/resources/IVesselSetting';
import { IVesselSpecification } from '../../models/resources/IVesselSpecification';
import { IVoyageFull } from '../../models/resources/IVoyage';
import { IVoyageCharterName } from '../../models/resources/IVoyageCharterName';
import { IWording } from '../../models/resources/IWording';
import { CompanyRepositoryService } from '../repositories/company-repository.service';
import { CountryRepositoryService } from '../repositories/country-repository.service';
import { DebitorCreditorRepositoryService } from '../repositories/debitor-creditor-repository.service';
import { VesselAccountingRepositoryService } from '../repositories/vessel-accounting-repository.service';
import { VesselRegisterRepositoryService } from '../repositories/vessel-registeries-repository.service';
import { VesselRepositoryService } from '../repositories/vessel-repository.service';
import { VesselSettingsRepositoryService } from '../repositories/vessel-settings-repository.service';
import { VesselSpecificationRepositoryService, vesselTypes } from '../repositories/vessel-specification-repository.service';
import { VoyageCharternameRepositoryService } from '../repositories/voyage-chartername-repository.service';
import { VesselMainService } from './vessel-main.service';
import { VoyagesMainService } from './voyages-main.service';
import { setTranslationSubjects } from '../../../shared/helpers/general';

type IEntitiesList<T> = [
  T,
  IVesselAccounting[],
  IVessel[],
  IVesselSpecification[],
  IVoyageFull[],
  IVesselPreDelayCharterName[],
  IVoyageCharterName[],
  ICompany[],
  IDebitorCreditor[],
  ICountry[],
  IVesselSetting[],
];

@Injectable({
  providedIn: 'root'
})
export class RegistrationFleetMainService {

  constructor(
    private vesselRegisterRepo: VesselRegisterRepositoryService,
    private vesselAccountingService: VesselAccountingRepositoryService,
    private vesselService: VesselRepositoryService,
    private vesselSpecificationService: VesselSpecificationRepositoryService,
    private vesselMainService: VesselMainService,
    private voyagesMainService: VoyagesMainService,
    private charternameRepo: VoyageCharternameRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private debitorCreditorRepo: DebitorCreditorRepositoryService,
    private countriesRepo: CountryRepositoryService,
    private vesselSettings: VesselSettingsRepositoryService,
  ) { }


  private composeRegistrationFleetList(
    vesselRegister$: Observable<IVesselRegister[]>,
    vesselAccountings$: Observable<IVesselAccounting[]>,
    vessels$: Observable<IVessel[]>,
    vesselSpecifications$: Observable<IVesselSpecification[]>,
    voyages$: Observable<IVoyageFull[]>,
    vesselPreDelayCharterNames$: Observable<IVesselPreDelayCharterName[]>,
    charterNames$: Observable<IVoyageCharterName[]>,
    companies$: Observable<ICompany[]>,
    debitorsCreditors$: Observable<IDebitorCreditor[]>,
    countries$: Observable<ICountry[]>,
    settings$: Observable<IVesselSetting[]>,
  ): Observable<IRegistrationFleet[]> {
    return forkJoin([
      vesselRegister$,
      vesselAccountings$,
      vessels$,
      vesselSpecifications$,
      voyages$,
      vesselPreDelayCharterNames$,
      charterNames$,
      companies$,
      debitorsCreditors$,
      countries$,
      settings$,
    ]).pipe(
      map((res: IEntitiesList<IVesselRegister[]>) => {
        return this.getRegistrationFleetFullList(...res);
      })
    );
  }

  public fetchAll(): Observable<IRegistrationFleet[]> {
    return this.composeRegistrationFleetList(
      this.vesselRegisterRepo.fetchAll({}),
      this.vesselAccountingService.fetchAll({}),
      this.vesselService.fetchAll({}),
      this.vesselSpecificationService.fetchAll({}),
      this.voyagesMainService.fetchAll({}),
      this.charternameRepo.fetchAllPreDeliveryCharternames(),
      this.charternameRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
      this.countriesRepo.fetchAll({}),
      this.vesselSettings.fetchAll({}),
    );
  }

  public getStream(): Observable<IRegistrationFleet[]> {
    return forkJoin([
      this.vesselRegisterRepo.fetchAll({}),
      this.vesselAccountingService.fetchAll({}),
      this.vesselService.fetchAll({}),
      this.vesselSpecificationService.fetchAll({}),
      this.voyagesMainService.fetchAll({}),
      this.charternameRepo.fetchAllPreDeliveryCharternames(),
      this.charternameRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
      this.countriesRepo.fetchAll({}),
      this.vesselSettings.fetchAll({}),
    ]).pipe(
      switchMap(([
        vesselRegisters,
        vesselAccountings,
        vessels,
        vesselSpecifications,
        voyages,
        vesselPreDelayCharterNames,
        charterNames,
        companies,
        debitorsCreditors,
        countries,
        settings,
      ]) => combineLatest([
        this.vesselRegisterRepo.getStream({}),
        this.vesselAccountingService.getStream({}),
        this.vesselService.getStream({}),
        this.vesselSpecificationService.getStream({}),
        this.voyagesMainService.getStream(),
        of(vesselPreDelayCharterNames),
        of(charterNames),
        this.companyRepo.getStream(),
        this.debitorCreditorRepo.getStream(),
        this.countriesRepo.getStream(),
        this.vesselSettings.getStream(),
      ])),
      map((res: IEntitiesList<IVesselRegister[]>) => {
        return this.getRegistrationFleetFullList(...res);
      })
    );
  }

  private getRegistrationFleetFull(
    vessel: IVessel,
    vesselAccountings: IVesselAccounting[],
    vesselRegisters: IVesselRegister[],
    vesselSpecifications: IVesselSpecification[],
    voyages: IVoyageFull[],
    vesselPreDelayCharterNames: IVesselPreDelayCharterName[],
    charterNames: IVoyageCharterName[],
    companiesMap: Map<CompanyKey, ICompany>,
    debitorsCreditorsMap: Map<DebitorCreditorKey, IDebitorCreditor>,
    countriesMap: Map<CountryKey, ICountry>,
    settings: IVesselSetting[],
  ): IRegistrationFleet {
    const closestAccounting: IVesselAccounting = findClosestWithFromUntil(
      vesselAccountings.filter(va => va.vesselId === vessel.id)
    );

    const currentVesselVoyages: IVoyageFull[] = closestAccounting
      ? voyages
        .filter(v => v.vesselAccountingId === closestAccounting.id)
      : [];

    const currentVesselCharterNames: IVoyageCharterName[] = charterNames
      .filter(c => {
        return currentVesselVoyages.find(v => v.id === c.voyageId);
      });

    const vesselPreDelivery: IVesselPreDelayCharterName = vesselPreDelayCharterNames
      .find(v => v.vesselId === vessel.id);

    const closestCharterName: IVoyageCharterName = this.vesselMainService
      .findClosestChartername(currentVesselCharterNames, vesselPreDelivery);

    const specificationsOfVessel: IVesselSpecification[] = vesselSpecifications
      .filter(s => s.vesselId === vessel.id);

    const latestSpecification: IVesselSpecification = specificationsOfVessel?.length
      ? findClosestWithFromUntil(specificationsOfVessel)
      : null;

    const vesselTypeName: IWording = latestSpecification?.vesselType
      ? vesselTypes.find(v => v.value === latestSpecification.vesselType).displayLabel
      : null;

    const currentVesselRegisters: IVesselRegister[] = vesselRegisters
      .filter(vr => vr.vesselId === vessel.id);

    const foundVesselRegister: IVesselRegister = this.vesselMainService
      .findClosestWithFromUntil(currentVesselRegisters);

    const futureVesselRegister: IVesselRegister = this.vesselMainService
      .findClosestWithFromUntil(currentVesselRegisters, true);

    const vesselRegister: IVesselRegister = foundVesselRegister
      ? foundVesselRegister
      : {} as IVesselRegister;

    const typeOfRegistrationName: IWording =
      vesselRegister.type === RegistrationTypes.Bareboat
        ? wording.chartering.bareboat
        : wording.chartering.permanent;

    const registeredOwnerDebitorCreditor: IDebitorCreditor = debitorsCreditorsMap
      .get(closestAccounting?.owningCompanyId);
    const bbChartererDebitorCreditor: IDebitorCreditor = debitorsCreditorsMap
      .get(closestAccounting?.bareboatCompanyId);

    const registeredOwner: string = companiesMap
      .get(registeredOwnerDebitorCreditor?.companyId)?.companyName;

    const bbCharterer: string = companiesMap
      .get(bbChartererDebitorCreditor?.companyId)?.companyName;


    const flag: IVesselRegister = vesselRegisters
      .filter(vesselRegisterFlag => {
        return vesselRegisterFlag.vesselId === vessel.id;
      })
      .reverse()
      .find(vesselRegisterFlag => new Date(vesselRegisterFlag.from) <= new Date());

    const flagCountryId: CountryKey | undefined = flag?.secondaryCountryId
      || flag?.primaryCountryId;
    const flagPath: string = flagCountryId
      ? countriesMap.get(flagCountryId).countryFlagPath
      : '';

    return {
      vesselId: vessel.id,
      imo: vessel.imo,
      officialNo: vessel.officialNumber,
      vesselAccountingId: closestAccounting?.id,
      vesselName: closestAccounting?.vesselName,
      charterName: closestCharterName?.charterName,
      vesselType: latestSpecification?.vesselType,
      vesselTypeName,
      vesselRegisterId: vesselRegister.id,
      client: vesselRegister?.client,
      comments: vesselRegister?.remarks,
      periodFrom: vesselRegister.from,
      periodUntil: vesselRegister.until,
      typeOfRegistration: vesselRegister.type,
      ssrNo: vesselRegister.ssrNo,
      callSign: vesselRegister.secondaryCallSign || vesselRegister.primaryCallSign,
      typeOfRegistrationName,
      registeredOwner,
      bbCharterer,
      flagPath,
      settings: settings.filter(s => s.vesselId === vessel.id),
      doesPeriodStartWithinSixtyDays: this.doesPeriodStartWithinSixtyDays(
        futureVesselRegister
      ),
      nextPeriodStartsWording: setTranslationSubjects(
        wording.chartering.nextRegistrationPeriodStarts,
        // should include additional (the latest) day too, thats why  `+ 1` part is added
        { DAYS: moment(futureVesselRegister?.from)
            .diff(moment().startOf('day'), 'days') + 1
        },
      ),
    };
  }

  private getRegistrationFleetFullList(
    vesselRegisters: IVesselRegister[],
    vesselAccountings: IVesselAccounting[],
    vessels: IVessel[],
    vesselSpecifications: IVesselSpecification[],
    voyages: IVoyageFull[],
    vesselPreDelayCharterNames: IVesselPreDelayCharterName[],
    charterNames: IVoyageCharterName[],
    companies: ICompany[],
    debitorsCreditors: IDebitorCreditor[],
    countries: ICountry[],
    settings: IVesselSetting[],
  ): IRegistrationFleet[] {
    const debitorsCreditorsMap: Map<DebitorCreditorKey, IDebitorCreditor>
      = convertToMap(debitorsCreditors);
    const companiesMap: Map<CompanyKey, ICompany>
      = convertToMap(companies);
    const countriesMap: Map<CountryKey, ICountry>
      = convertToMap(countries);

    return vessels.map(vessel => {
      return this.getRegistrationFleetFull(
        vessel,
        vesselAccountings,
        vesselRegisters,
        vesselSpecifications,
        voyages,
        vesselPreDelayCharterNames,
        charterNames,
        companiesMap,
        debitorsCreditorsMap,
        countriesMap,
        settings,
      );
    });
  }

  private doesPeriodStartWithinSixtyDays(
    registration: IVesselRegister
  ): boolean {
    if (registration === undefined) {
      return false;
    }

    const startDate: moment.Moment = moment(registration.from);
    const untilDate: moment.Moment = moment().add(60, 'days');

    return startDate.isBetween(moment(), untilDate, 'days');
  }
}
