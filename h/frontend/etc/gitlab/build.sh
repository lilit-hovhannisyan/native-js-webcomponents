#!/bin/bash

source .env
docker login -u ${DOCKER_USER} -p ${CI_JOB_TOKEN} ${CI_REGISTRY}

# build and push main image
docker build \
  --pull \
  --tag ${MAIN_IMAGE} \
  --file ${MAIN_DOCKERFILE} .
docker push ${MAIN_IMAGE}
