import { Component, OnInit, Input, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { NvGridConfig } from 'nv-grid';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';

import { IUpload } from 'src/app/core/models/resources/IUpload';
import { wording } from 'src/app/core/constants/wording/wording';
import { IStevedoreDamageDocument } from 'src/app/core/models/resources/IStevedoreDamageDocument';
import { FileUploaderService } from 'src/app/shared/services/file-uploader.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { NotificationType } from 'src/app/core/models/INotification';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { getStevedoreDamageDocumentGridConfig } from './upload-documents.config';
import { StevedoreDamegeDocumentEditComponent } from './edit-modal/edit-modal.component';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';

@Component({
  selector: 'nv-upload-documents',
  templateUrl: './upload-documents.component.html',
  styleUrls: ['./upload-documents.component.scss']
})

export class UploadDocumentsComponent implements OnInit {
  public _documents: IStevedoreDamageDocument[];
  @Input() public set documents(value: IStevedoreDamageDocument[]) {
    if (!this._documents || value.length !== this._documents.length) {
      this._documents = value;
      this.fillUploadInfo();
    }
  }
  @Input() public inEditingMode$: Observable<boolean>;
  @Output() public documentsChange = new EventEmitter<IStevedoreDamageDocument[]>();
  @ViewChild('fileInput', { static: true }) public fileInput: ElementRef;
  public gridConfig: NvGridConfig;
  public wording = wording;
  private modalRef?: NzModalRef;
  public dataSource$ = new BehaviorSubject<IStevedoreDamageDocument[]>([]);
  private file: File;
  private maxFileSize = 10; // in MB
  private isFormValid = true;
  private inEditMode = true;
  protected resourceDefault = {
    isAttached: true,
    id: null,
    uploadId: null,
    filename: '',
    remark: '',
  } as IStevedoreDamageDocument;

  constructor(
    public gridConfigService: GridConfigService,
    private fileUploaderService: FileUploaderService,
    private confirmService: ConfirmationService,
    private authService: AuthenticationService,
    private modalService: NzModalService,
    private settingsService: SettingsService,
    private notificationsService: NotificationsService,
  ) { }

  public ngOnInit() {
    const gridActions: IGridConfigActions = {
      add: this.onUpload,
      edit: this.onEdit,
      delete: this.onDelete,
      request: this.onRequest,
      isInEditMode: this.isInEditMode,
    };

    this.gridConfig = getStevedoreDamageDocumentGridConfig(
      gridActions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
    );

    this.inEditingMode$.subscribe(inEditingMode => {
      this.inEditMode = !inEditingMode;
      this.gridConfig = {
        ...this.gridConfig,
        buttons: this.gridConfig.buttons.map((button) => {
          if (button.name === 'downloadStevedoreDocument') {
            return button;
          }

          return { ...button, disabled: !inEditingMode };
        })
      };
    });
  }

  private fillUploadInfo() {
    if (this._documents.length === 0) {
      this.dataSource$.next([]);
      return;
    }
    const uploadIds = this._documents.map(document => document.uploadId);

    this.fileUploaderService.getUploads(uploadIds).subscribe(uploads => {
      this._documents = uploads.map(upload => {
        const document = this.getDocumentByUloadId(upload.id.toString());

        return {
          ...document,
          filename: upload.name,
          uploadedOn: upload.createdDate,
        };
      });

      this.dataSource$.next(this._documents);
    });
  }

  public isInEditMode = () => {
    return this.inEditMode;
  }

  public onUpload = () => {
    this.fileInput.nativeElement.click();
  }

  public onFileChange(event: EventTarget): void {
    const eventObj: MSInputMethodContext = <MSInputMethodContext>event;
    const target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    const files: FileList = target.files;
    const maxFileSizeInBytes = this.maxFileSize * 1024 * 1024;

    this.file = files[0];

    if (this.file.size > maxFileSizeInBytes) {
      const message = setTranslationSubjects(
        wording.technicalManagement.fileTooBig,
        { SIZE: `${this.maxFileSize}MB` },
      );

      return this.notificationsService.notify(
        NotificationType.Warning,
        wording.general.notAllowed,
        message
      );
    }

    this.openEditModal({
      ...this.resourceDefault,
      filename: this.file.name,
    });
  }

  public upload = (): Observable<IUpload> => {
    const formData = new FormData();

    formData.append('files', this.file, this.file.name);
    return this.fileUploaderService.upload(formData, 'files');
  }

  private onRequest = (document: IStevedoreDamageDocument) => {
    this.fileUploaderService.download(document.uploadId, document.filename).subscribe();
  }

  private onDelete = (document: IStevedoreDamageDocument) => {
    this.confirmService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: document.filename },
    }).subscribe(confirmed => confirmed && this.delete(document.uploadId));
  }

  private delete = (uploadId: string) => {
    const index = this.getDocumentIndexByUploadId(uploadId);

    this._documents.splice(index, 1);
    this.dataSource$.next(this._documents);
    this.documentsChange.emit(this._documents);
  }

  private onEdit = (document: IStevedoreDamageDocument) => {
    const index = this.getDocumentIndexByUploadId(document.uploadId);

    this.openEditModal(this._documents[index]);
  }

  private openEditModal = (document?: IStevedoreDamageDocument) => {
    this.modalRef = this.modalService.create({
      nzTitle: document.filename,
      nzClosable: true,
      nzContent: StevedoreDamegeDocumentEditComponent,
      nzComponentParams: {
        closeModal: this.closeModal,
        document,
        formChange: this.formChange
      },
      nzOkText: wording.general.save[this.settingsService.language],
      nzOnCancel: this.closeModal,

      nzFooter: [
        {
          label: 'Close',
          onClick: this.closeModal,
        },
        {
          label: wording.general.save[this.settingsService.language],
          type: 'primary',
          onClick: this.saveClicked,
          disabled: this.isModalButtonDisabled
        }
      ],
    });
  }

  public formChange = (status: 'VALID' | 'INVALID') => {
    this.isFormValid = status === 'VALID';
  }

  public isModalButtonDisabled = () => {
    return !this.isFormValid;
  }

  public saveClicked = (event: StevedoreDamegeDocumentEditComponent): void => {
    const { form, document } = event;

    if (!this.file) {
      this.saveDocument({
        ...document,
        ...form.value,
      });

      this.closeModal();
      return;
    }

    this.upload().pipe(
      tap(upload => {
        form.value.uploadId = upload.id;
        form.value.uploadedOn = upload.createdDate;
        this.saveDocument({ ...form.value, filename: this.file.name });
        this.closeModal();
      }),
      map(() => null)).subscribe();
  }

  public saveDocument(document: IStevedoreDamageDocument) {
    const index = this.getDocumentIndexByUploadId(document.uploadId);

    if (index === -1) {
      this._documents.push(document);
    } else {
      this._documents[index] = document;
    }

    this.dataSource$.next(this._documents);
    this.documentsChange.emit(this._documents);
  }

  private closeModal = () => {
    this.modalRef.destroy();
    this.file = null;
    this.fileInput.nativeElement.value = '';
  }

  private getDocumentIndexByUploadId(uploadId: string): number {
    return this._documents.findIndex((item) => item.uploadId === uploadId);
  }

  private getDocumentByUloadId(id: string) {
    return this._documents.find(({ uploadId }) => uploadId === id);
  }
}
