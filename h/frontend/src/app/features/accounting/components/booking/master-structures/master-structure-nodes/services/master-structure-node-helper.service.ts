import { Injectable } from '@angular/core';
import { NzTreeNode, NzFormatEmitEvent, NzTreeNodeOptions } from 'ng-zorro-antd/tree';
import { IMasterStructureNode, IMasterStructureNodeTemplate, MASTER_STRUCTURE_NODE_TYPE } from 'src/app/core/models/resources/IMasterStructureNode';
import { v4 as uuidv4 } from 'uuid';
import { IMasterStructure } from 'src/app/core/models/resources/IMasterStructure';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { Language } from 'src/app/core/models/language';
import { MASTER_STRUCTURE_TYPES } from 'src/app/core/models/enums/master-structure-type';

@Injectable({
  providedIn: 'root'
})
export class MasterStructureNodeHelperService {

  private currentStructure: IMasterStructure;
  private language: Language;

  constructor() { }

  public initCurrentStructure(structure: IMasterStructure): void {
    this.currentStructure = structure;
  }

  public setLanguage(language: Language): void {
    this.language = language;
  }

  public buildNodeTree(arr: IMasterStructureNode[]): IMasterStructureNodeTemplate[] {
    const tree: IMasterStructureNodeTemplate[] = [];
    const mappedArr: { [key: string]: IMasterStructureNodeTemplate } = {};
    const length: number = arr.length;

    for (let i = 0; i < length; i++) {
      const element: IMasterStructureNodeTemplate
        = arr[i] as IMasterStructureNodeTemplate;
      mappedArr[element.id] = element;
      mappedArr[element.id].children = [];
    }

    for (const id in mappedArr) {
      if (mappedArr.hasOwnProperty(id)) {
        const element: IMasterStructureNodeTemplate = mappedArr[id];
        if (element.parentNodeId && element.level !== 1) {
          mappedArr[element.parentNodeId].children
            .push(this.transformToNodeTemplate(element));
          mappedArr[element.parentNodeId].childNodes
            .push(this.transformToNodeTemplate(element));
        } else {
          tree.push(this.transformToNodeTemplate(element));
        }
      }
    }
    return tree;
  }

  /**
   * This method is equiavalent of Array.prototype.map for tree, you can pass your custom
   * callback to it, which will work for each node
   */
  public mapNodes(
    nodes: IMasterStructureNode[] | IMasterStructureNode | NzTreeNode[] | NzTreeNode,
    callback: (
      node: IMasterStructureNode | NzTreeNode,
      index?: number,
      parent?: IMasterStructureNode,
      firstParent?: IMasterStructureNode | NzTreeNode,
    ) => void | boolean,
    childrenKey = 'childNodes',
    index = 0,
    parent?: IMasterStructureNode,
    firstParent?: IMasterStructureNode,
  ): IMasterStructureNodeTemplate[] {
    if (Array.isArray(nodes)) {
      for (let i = 0; i < nodes.length; i++) {
        const node: IMasterStructureNodeTemplate
           = nodes[i] as IMasterStructureNodeTemplate;
        this.mapNodes(
          node,
          callback,
          childrenKey,
          i,
          parent,
          node.level === 1
            ? node
            : firstParent,
        );
      }
    } else {
      const result: void | boolean = callback(nodes, index, parent, firstParent);
      if (result) {
        return;
      }
    }

    const children: IMasterStructureNode[] | NzTreeNode[]
      = typeof nodes[childrenKey] === 'function'
      ? nodes[childrenKey]()
      : nodes[childrenKey];

    if (!Array.isArray(nodes) && children?.length > 0) {
      this.mapNodes(
        children,
        callback,
        childrenKey,
        index,
        nodes as IMasterStructureNode,
        firstParent,
      );
    }
    return nodes as IMasterStructureNodeTemplate[];
  }

  /**
   * In a lot of cases we need to lose all references, and get totally new tree,
   * without messing the old treem if we want to rollback our changes later or do some
   * actions with nodes
   */
  public copyNodes(nodes: IMasterStructureNode[]): IMasterStructureNode[] {
    return JSON.parse(JSON.stringify(nodes));
  }

  /**
   * This is used to transform IMasterStructureNode to IMasterStructureNodeTemplate,
   * because ng Zorro requires other properties to be in a node to work with it correctly
   */
  public transformToNodeTemplate(
    node: IMasterStructureNode,
  ): IMasterStructureNodeTemplate {
    const newId: string = uuidv4();
    return {
      title: { en: node.labelEn, de: node.labelDe },
      key: (node.id || newId).toString(),
      expanded: false,
      id: newId,
      children: [],
      ...node,
      masterStructureId: this.currentStructure.id,
      childNodes: node.childNodes || [],
      isLeaf: node.nodeType !== MASTER_STRUCTURE_NODE_TYPE.NODE,
    };
  }

  public sortChildNodes(
    targetNode: IMasterStructureNode | NzTreeNode,
    childrenKey = 'childNodes',
  ): void {
    const label: string = getLabelKeyByLanguage(this.language);
    targetNode[childrenKey]
      .sort((a: IMasterStructureNodeTemplate, b: IMasterStructureNodeTemplate) => {
        // if node isn't booking account it should be after booking accounts
        if (!(a.bookingAccountId || a.origin?.bookingAccountId)) {
          return 1;
        }

        // if node isn't booking account it should be after booking accounts
        if (!(b.bookingAccountId || b.origin?.bookingAccountId)) {
          return -1;
        }

        // if nodes are booking accounts we should compare them by
        // title(which includes account no at the beginning) and order by ascending order
        return (a[label] || a.origin[label]).localeCompare(b[label] || b.origin[label]);
      });
  }

  public changeNode(
    nodes: IMasterStructureNodeTemplate[],
    selectedNode: NzTreeNode | IMasterStructureNodeTemplate,
    makeChange: (node: IMasterStructureNodeTemplate) => void,
  ): void {
    this.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        if (node.id === selectedNode.key) {
          makeChange(node);
          return true;
        }
      }
    );
  }

  public closeTree(nodes: IMasterStructureNodeTemplate[]): void {
    this.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        node.expanded = false;
      }
    );
  }

  public openTree(nodes: IMasterStructureNodeTemplate[]): void {
    this.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        node.expanded = true;
      }
    );
  }

  public onExpandedChange(
    event: NzFormatEmitEvent,
    nodes: IMasterStructureNodeTemplate[],
  ): void {
    this.mapNodes(
      nodes,
      (node: IMasterStructureNodeTemplate) => {
        if (node.key === event.node.key) {
          node.expanded = event.node.isExpanded;
        }
      }
    );
  }

  public addFirstLevelNode(newNode: IMasterStructureNode): IMasterStructureNodeTemplate {
    const addedNode: IMasterStructureNodeTemplate = this.transformToNodeTemplate({
      ...newNode,
      parentNodeId: null,
      nodeType: newNode.nodeType || MASTER_STRUCTURE_NODE_TYPE.NODE,
      level: 1,
      isHighlighted: true,
    });

    return addedNode;
  }

  public addChildNode(
    nodes: IMasterStructureNodeTemplate[],
    selectedNode: IMasterStructureNodeTemplate | NzTreeNodeOptions,
    newNode: IMasterStructureNode,
  ): IMasterStructureNodeTemplate {
    let addedNode: IMasterStructureNodeTemplate;

    this.changeNode(
      nodes,
      selectedNode as IMasterStructureNodeTemplate,
      node => {
        addedNode = this.transformToNodeTemplate(
          {
            ...newNode,
            parentNodeId: node.id,
            nodeType: newNode.nodeType || MASTER_STRUCTURE_NODE_TYPE.NODE,
            level: selectedNode.origin
              ? selectedNode.origin.level + 1
              : selectedNode.level + 1,
            hasExpenseItem: (selectedNode.origin?.hasExpenseItem
              || selectedNode.hasExpenseItem)
              && this.currentStructure.masterStructureType
              === MASTER_STRUCTURE_TYPES.BUSINESS_MANAGEMENT_EVALUATION,
          }
        );
        node.expanded = true;
        node.childNodes.push(addedNode);

        if (addedNode.bookingAccountId) {
          this.sortChildNodes(node);
        }
      }
    );

    return addedNode;
  }

  public finalizeNodes(
    nodes: IMasterStructureNodeTemplate[]
  ): IMasterStructureNodeTemplate[] {
    return this.mapNodes(
      this.copyNodes(nodes),
      (
        node: IMasterStructureNode,
        index: number,
        parent: IMasterStructureNodeTemplate,
      ) => {
        if (node.nodeType === MASTER_STRUCTURE_NODE_TYPE.SUMMATION) {
          node.childNodes = [];
        }
        node.position = index + 1;
        if (node.level !== 1 && node.bookingAccountId) {
          node.hasExpenseItem = parent?.hasExpenseItem;
        }
      }
    );
  }
}
