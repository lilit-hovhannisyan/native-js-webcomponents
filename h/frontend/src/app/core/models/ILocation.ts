export interface ILocation {
  host:	string;
  pathname:	string;
  scheme:	string;
  href:	string; // host and pathname
}
