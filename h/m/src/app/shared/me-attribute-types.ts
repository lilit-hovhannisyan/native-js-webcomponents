export enum MeAttributeTypes {
  BOOLEAN = 'core.fc.datatype.DMBoolean',
  CHOICE = 'core.fc.datatype.DMChoice',
  COMPOSITE = 'core.fc.datatype.DMComposite',
  DATE_TIME = 'core.fc.datatype.DMDateTime',
  DECIMAL = 'core.fc.datatype.DMDecimal',
  FLOAT = 'core.fc.datatype.DMFloat',
  INTEGER = 'core.fc.datatype.DMInteger',
  LONG = 'core.fc.datatype.DMLong',
  MOA_LIST = 'core.fc.datatype.DMMOAList',
  OBJECT_REF = 'core.fc.datatype.DMObjectRef',
  OBJECT_REF_LIST = 'core.fc.datatype.DMObjectRefList',
  STRING = 'core.fc.datatype.DMString',
  TEXT = 'core.fc.datatype.DMText',
  URL = 'core.fc.datatype.DMUrl'
}
