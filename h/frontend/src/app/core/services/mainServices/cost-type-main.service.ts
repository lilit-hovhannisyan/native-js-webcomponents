import { Injectable } from '@angular/core';
import { ICostType, ICostTypeRow } from '../../models/resources/ICostTypes';
import { opexTypesOptions } from '../../models/enums/opex-types';
import { ISelectOption } from '../../models/misc/selectOption';
import { accountingRelationCategoryOptions } from '../../models/enums/accounting-relation-category';

@Injectable({
  providedIn: 'root'
})
export class CostTypeMainService {
  public accountingRelationsOptions: ISelectOption[] = accountingRelationCategoryOptions;

  constructor(
  ) {}

  public toRow(costType: ICostType): ICostTypeRow {
    const opexTypeName = opexTypesOptions
      .find(option => option.value === costType.opexType)?.displayLabel;
    const accountingRelationName = this.accountingRelationsOptions
      .find(option => option.value === costType.accountingRelation)?.displayLabel;

    return ({
      ...costType,
      opexTypeName,
      accountingRelationName,
    });
  }

  public toRows (costTypes: ICostType[]): ICostTypeRow[] {
    return costTypes.map((costType) => this.toRow(costType));
  }
}
