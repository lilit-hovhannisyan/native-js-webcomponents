import { Language } from 'src/app/core/models/language';
import { ITaskTemplateCategory } from 'src/app/core/models/resources/Task-Templates/ITaskTemplateCategory';

export function checkForCategoryName(
  category: ITaskTemplateCategory,
  query: string,
  language: Language
): boolean {
  const usrInput: string = query.toLowerCase().trim();
  const categoryName: string = language === Language.EN
    ? category.labelEn.toLowerCase()
    : category.labelDe.toLowerCase();

  return categoryName.startsWith(usrInput);
}
