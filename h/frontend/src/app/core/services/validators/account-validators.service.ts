import { ValidationErrors, AbstractControl, FormGroup } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { Injectable } from '@angular/core';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { MandatorAccountRoleRepositoryService } from 'src/app/core/services/repositories/mandator-account-role.repository.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { IMandatorAccount } from 'src/app/core/models/resources/IMandatorAccount';
import { MandatorKey } from 'src/app/core/models/resources/IMandator';
import { IMandatorAccountRole } from 'src/app/core/models/resources/IMandatorAccountRole';
import { ACCOUNT_TYPE_ACCRUALS } from 'src/app/core/models/enums/account-type-accruals';
import { accountNoModifier, BookingAccountKey, IBookingAccount } from '../../models/resources/IBookingAccount';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { ACCOUNTING_TYPES } from '../../models/enums/accounting-types';
import { BookingFormService } from '../booking-form.service';
import { isAccountingTypeValid } from './validators';

@Injectable({
  providedIn: 'root'
})
export class AccountValidatorsService {

  constructor(
    private bookingFormService: BookingFormService,
    private mandatorAccountRepo: MandatorAccountRepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    private mandatorAccountRoleRepo: MandatorAccountRoleRepositoryService,
    private authService: AuthenticationService,
  ) { }

  public doesBookingAccountExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundBookingAccount: IBookingAccount = this.bookingAccountRepo.getStreamValue()
      .find(bookingAccount => bookingAccount.id === control.value);

    const isValid: boolean = foundBookingAccount ? true : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      bookingAccountNotFound: {
        message: wording.accounting.notFoundAccount
      }
    };
  }

  public doesBookingAccountNoExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const accountNo: number = +control.value;
    const foundBookingAccount: IBookingAccount = this.bookingAccountRepo.getStreamValue()
      .find(bookingAccount => bookingAccount.no === accountNo);

    if (foundBookingAccount) {
      return null;
    }

    markControlAsTouched(control);
    return {
      bookingAccountNotFound: {
        message: wording.accounting.notFoundAccount
      }
    };
  }

  public isBookingAccountActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const foundBookingAccount: IBookingAccount = this.bookingAccountRepo.getStreamValue()
      .find(bookingAccount => bookingAccount.id === control.value);

    const isValid: boolean = foundBookingAccount
      ? foundBookingAccount.isActive
      : false;

    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      bookingAccountNotActive: {
        message: wording.accounting.notActiveAccount
      }
    };
  }

  public isBookingAccountNoActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const accountNo: number = +control.value;
    const foundBookingAccount: IBookingAccount = this.bookingAccountRepo.getStreamValue()
      .find(bookingAccount => bookingAccount.no === accountNo);

    const isValid: boolean = foundBookingAccount
      ? foundBookingAccount.isActive
      : false;

    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      bookingAccountNotActive: {
        message: wording.accounting.notActiveAccount
      }
    };
  }

  public isBookingAccountNoRelatedToMandator = (control: AbstractControl): ValidationErrors | null => {
    if (
      !control.value
      || !control.parent
      || !control.parent.get('mandatorId').value
    ) {
      return null;
    }

    const foundBookingAccount: IBookingAccount = this.bookingAccountRepo.getStreamValue()
      .find(bookingAccount =>
        bookingAccount.no === control.value);

    let foundMandatorbookingAccount: IMandatorAccount;
    if (foundBookingAccount) {
      const mandatorId: MandatorKey = control.parent.get('mandatorId').value;
      const bookingAccountId: BookingAccountKey = foundBookingAccount.id;

      foundMandatorbookingAccount = this.mandatorAccountRepo.getStreamValue()
        .find(mandatorBookingAccount =>
          mandatorBookingAccount.bookingAccountId === bookingAccountId
          && mandatorBookingAccount.mandatorId === mandatorId);
    }
    if (foundMandatorbookingAccount) {
      return null;
    }

    markControlAsTouched(control);
    return {
      bookingAccountNotRelatedToMandator: {
        message: wording.general.bookingAccountNotRelatedToMandator
      }
    };
  }

  public isBookingAccountRelatedToMandator = (control: AbstractControl): ValidationErrors | null => {
    if (
      !control.value
      || !control.parent
      || !control.parent.get('mandatorId').value
    ) {
      return null;
    }

    const mandatorId: MandatorKey = control.parent.get('mandatorId').value;
    const bookingAccountId: BookingAccountKey = control.parent.get('bookingAccountId').value;

    const foundMandatorAccount: IMandatorAccount = this.mandatorAccountRepo.getStreamValue()
      .find(mandatorBookingAccount =>
        mandatorBookingAccount.bookingAccountId === bookingAccountId
        && mandatorBookingAccount.mandatorId === mandatorId);

    if (foundMandatorAccount) {
      return null;
    }

    markControlAsTouched(control);
    return {
      notRelatedToMandator: {
        message: wording.general.notRelatedToMandator
      }
    };
  }

  public isBookingAccountRelatedToBookingMandatorId = (control: AbstractControl): ValidationErrors | null => {
    if (
      !control.value
      || !control.parent
      || !control.parent.get('bookingMandatorId').value
    ) {
      return null;
    }

    const bookingMandatorId: BookingAccountKey = control.parent.get('bookingMandatorId').value;
    const mandatorBookingAccount: IMandatorAccount = this.mandatorAccountRepo.getStreamValue()
      .find(m =>
        m.bookingAccountId === control.value
        && m.mandatorId === bookingMandatorId);

    if (mandatorBookingAccount) {
      return null;
    }

    markControlAsTouched(control);
    return {
      notRelatedToMandator: {
        message: wording.general.notRelatedToMandator
      }
    };
  }

  public doesBookingMandatorHasRoleOnAccount = (control: AbstractControl): ValidationErrors | null => {
    if (
      !control.value
      || !control.parent
      || !control.parent.get('bookingMandatorId').value
    ) {
      return null;
    }

    const mandatorId: MandatorKey = control.parent.get('bookingMandatorId').value;
    const foundMandatorAccount: IMandatorAccount = this.mandatorAccountRepo.getStreamValue()
      .find(m =>
        m.bookingAccountId === control.value
        && m.mandatorId === mandatorId);

    if (!foundMandatorAccount) {
      return null;
    }

    const mandatorAccountRoles: IMandatorAccountRole[] = this.mandatorAccountRoleRepo.getStreamValue()
      .filter(m => m.mandatorAccountId === foundMandatorAccount.id);

    const isValid: boolean = !foundMandatorAccount.needsAccountPermission
      || this.authService
        .getRolesIds()
        .some(userRole => mandatorAccountRoles
          .some(mandatorAccountRole => mandatorAccountRole.roleId === userRole
          ));


    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      mandatorHasNoRoles: {
        message: wording.accounting.mandatorHasNoRoles
      }
    };
  }

  public isAccountPrepaidExpenses = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundBookingAccount: IBookingAccount = this.bookingAccountRepo.getStreamValue()
      .find(bookingAccount => bookingAccount.id === control.value);

    const isARAP: boolean = foundBookingAccount
      ? foundBookingAccount.accountTypeAccruals === ACCOUNT_TYPE_ACCRUALS.AccountPrepaidExpenses
        && foundBookingAccount.isAccruedAndDeferredItem
      : false;

    if (isARAP) {
      return null;
    }

    markControlAsTouched(control);
    return {
      bookingAccountNotPrepaidExpenses: {
        message: wording.accounting.accruedAccountPrepaidExpenses
      }
    };
  }

  public isAccountDeferredIncome = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundBookingAccount: IBookingAccount = this.bookingAccountRepo.getStreamValue()
      .find(bookingAccount => bookingAccount.id === control.value);

    const isPRAP: boolean = foundBookingAccount
      ? foundBookingAccount.accountTypeAccruals === ACCOUNT_TYPE_ACCRUALS.AccountDeferredIncome
        && foundBookingAccount.isAccruedAndDeferredItem
      : false;

    if (isPRAP) {
      return null;
    }

    markControlAsTouched(control);
    return {
      bookingAccountNotDeferredIncome: {
        message: wording.accounting.accruedAccountDeferredIncome
      }
    };
  }

  public validateAccountNoAccountType = (control: AbstractControl): ValidationErrors | null => {
    const bookingForm: FormGroup = this.bookingFormService.bookingFormSubject.value;
    if (!control.value || !bookingForm) {
      return null;
    }
    const bookingAccountNo: number = +control.value;
    const foundBookingAccount: IBookingAccount = this.bookingAccountRepo.getStreamValue()
      .find(bookingAccount => bookingAccount.no === +bookingAccountNo);

    const currentAccountingType: ACCOUNTING_TYPES = bookingForm.get('accountingType').value;

    const isValid: boolean = foundBookingAccount
      ? isAccountingTypeValid(
        currentAccountingType,
        foundBookingAccount.accountingType
      )
      : false;

    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      notValidAccountingType: {
        message: setTranslationSubjects(
          wording.accounting.notValidAccountingType,
          {
            SUBJECT: 'Account',
            NUMBER: accountNoModifier(bookingAccountNo),
            NAME: ACCOUNTING_TYPES[currentAccountingType]
          },
        )
      }
    };
  }
}
