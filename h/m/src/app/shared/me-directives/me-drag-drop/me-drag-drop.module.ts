import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeDragDropDirective } from './me-drag-drop.directive';
@NgModule({
  imports: [CommonModule],
  declarations: [MeDragDropDirective],
  exports: [MeDragDropDirective]
})
export class MeDragDropModule {}
