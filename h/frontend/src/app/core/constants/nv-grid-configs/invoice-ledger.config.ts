import { NvColumnDataType, NvGridButtonsPosition, NvGridConfig, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { IInvoiceLedgerRow } from '../../models/resources/IInvoiceLedger';

export const getLedgerGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'invoiceLedger',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    sortBy: 'voucherNumber',
    isSortAscending: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 25,
    },
    columns: [
      {
        key: 'mandatorName',
        title: wording.accounting.mandator,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'companyName',
        title: wording.accounting.companyName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'orderNumber',
        title: wording.accounting.orderNumber,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'year',
        title: wording.accounting.year,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'voucherNumber',
        title: wording.accounting.voucherNo,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'invoiceDate',
        title: wording.accounting.invoiceDate,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
        },
        dataType: NvColumnDataType.Date,
      },
      {
        key: 'invoiceNumber',
        title: wording.accounting.invoiceNo,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'discount',
        title: wording.accounting.discount,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        dataType: NvColumnDataType.Decimal
      },
      {
        key: 'paymentDate',
        title: wording.accounting.ledgerPaymentDate,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
        },
        dataType: NvColumnDataType.Date,
      },
      {
        key: 'currencyLabel',
        title: wording.accounting.isoCode,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'amount',
        title: wording.accounting.amount,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        dataType: NvColumnDataType.Decimal
      },
      {
        key: 'hasCredit',
        title: wording.accounting.creditNote,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        dataType: NvColumnDataType.Boolean,
      },
      {
        key: 'mandatorAccountName',
        title: wording.accounting.account,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'taxKey',
        title: wording.accounting.taxKey,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'vesselName',
        title: wording.accounting.vessel,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'voyageName',
        title: wording.accounting.ledgerVoyage,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'remark',
        title: wording.accounting.remarks,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'barcode',
        title: wording.accounting.barcode,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'receiptDate',
        title: wording.accounting.receiptDate,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
        },
        dataType: NvColumnDataType.DateTime,
      },
      {
        key: 'createdByUserFullName',
        title: wording.general.createdBy,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'creationDate',
        title: wording.general.createdBy,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        dataType: NvColumnDataType.Date,
      },
      {
        key: 'changedByUserFullName',
        title: wording.general.changedBy,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
        },
        dataType: NvColumnDataType.Date,
      },
      {
        key: 'changedDate',
        title: wording.general.changeDate,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
        },
        dataType: NvColumnDataType.Date,
      },
      {
        key: 'bookedView',
        title: wording.accounting.booked,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isDeleted',
        title: wording.general.deleted,
        width: 150,
        isSortable: true,
        filter: {
          values: ['0'],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean,
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'edit-invoice',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: (il: IInvoiceLedgerRow) => {
          return !canPerform(Operations.UPDATE) || !!il?.booked;
        },
        func: (invoiceLedger: IInvoiceLedgerRow) => actions.edit(invoiceLedger.id),
      },
      {
        icon: 'eye',
        description: wording.general.view,
        name: 'show-invoice',
        tooltip: wording.general.view,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: (il: IInvoiceLedgerRow) => {
          return !canPerform(Operations.READ) || !il?.booked;
        },
        func: (invoiceLedger: IInvoiceLedgerRow) => actions.edit(invoiceLedger.id),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'delete-invoice',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: (invoiceLedger: IInvoiceLedgerRow) => {
          return !canPerform(Operations.DELETE) || invoiceLedger && invoiceLedger.isDeleted;
        },
        func: (invoiceLedger: IInvoiceLedgerRow) => actions.delete(invoiceLedger),
        disabled: (il: IInvoiceLedgerRow) => !!il?.booked,
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: () => actions.add()
      },
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? [
        ...getContextMenuButtonsConfig(),
        {
          icon: 'copy',
          description: wording.accounting.copyInBookingForm,
          func: (invoiceLedger: IInvoiceLedgerRow) => actions.clone(invoiceLedger),
          hidden: (invoiceLedger: IInvoiceLedgerRow) => !!invoiceLedger.booked,
        },
        {
          icon: 'eye',
          description: wording.accounting.showBooking,
          name: 'showBooking',
          tooltip: wording.accounting.showBooking,
          actOnDoubleClick: false,
          func: (invoiceLedger: IInvoiceLedgerRow) => actions.show(invoiceLedger),
          hidden: (invoiceLedger: IInvoiceLedgerRow) => !invoiceLedger.booked,
        },
      ]
      : [],
  });
};
