import { Injectable } from '@angular/core';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { IMandatorBlockedBookingYear, IMandatorBlockedBookingYearDTO, IMandatorBlockedBookingYearCreate, MandatorBlockedBookingYearKey } from '../../models/resources/IMandatorBlockedBookingYear';
import { Observable, forkJoin } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { IMandator } from '../../models/resources/IMandator';
import { HttpGet } from '../../models/http/http-get';
import { RepositoryService } from '../repository.service';
import { UserRepositoryService } from './user-repository.service';

export interface IMandatorBlockedBookingYearParams extends RepoParams<IMandator> {
  mandatorIds?: number[];
  onlyLast?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class MandatorBlockedYearsRepositoryService extends AbstractRepository<
IMandatorBlockedBookingYear,
IMandatorBlockedBookingYearDTO,
IMandatorBlockedBookingYearCreate,
RepoParams<IMandatorBlockedBookingYear>,
MandatorBlockedBookingYearKey
> {
  protected resourceType = 'MandatorBlockedBookingYear';

  constructor(
    public repositoryService: RepositoryService,
    private userRepo: UserRepositoryService) {
    super(repositoryService);
  }

  /**
   *
   * @param mandatorIds ids of mandators you want the BlockedYear
   * @param onlyLast
   */
  public fetchAllOfMandators(mandatorIds: number[] = [], onlyLast = false): Observable<IMandatorBlockedBookingYear[]> {
    const request = new HttpGet(this.getBaseUrl(), { init: { params: { mandatorIds } } });
    return this.repositoryService.httpClient.request(request).pipe(
      map((response: any) => response.body.data.items as IMandatorBlockedBookingYear[]),
      shareReplay(1)
    );
  }

  public fetchAllWithUsers(mandatorIds: number[] = [], onlyLast = false): Observable<IMandatorBlockedBookingYear[]> {
    return forkJoin([
      this.fetchAllOfMandators(mandatorIds, onlyLast),
      this.userRepo.fetchAll({})
    ]).pipe(map(([blockedYears, users]) => {
      const blockedYearsWithUserName = blockedYears.map(blockedYear => {
        const foundUser = users.find(user => user.id === blockedYear.blockedBy);
        return foundUser ? { ...blockedYear, userName: foundUser.firstName } : blockedYear;
      });
      return blockedYearsWithUserName;
    }));
  }

  protected postProcess(item: IMandatorBlockedBookingYearDTO): IMandatorBlockedBookingYear { return item; }

  public getBaseUrl = () => '/api/mandator-blocked-years';
}
