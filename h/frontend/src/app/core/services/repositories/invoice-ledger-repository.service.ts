import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IInvoiceLedger, IInvoiceLedgerDTO, IInvoiceLedgerCreate, InvoiceLedgerKey } from '../../models/resources/IInvoiceLedger';
import { RepositoryService } from '../repository.service';
import { HttpGet } from '../../models/http/http-get';
import { map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';
import { IBooking } from '../../models/resources/IBooking';
import { Observable } from 'rxjs';

export interface IInvoiceLedgerParams extends RepoParams<IInvoiceLedger> { }

@Injectable({
  providedIn: 'root'
})
export class InvoiceLedgerRepositoryService extends AbstractRepository<
  IInvoiceLedger,
  IInvoiceLedgerDTO,
  IInvoiceLedgerCreate,
  IInvoiceLedgerParams,
  InvoiceLedgerKey
>  {
  protected resourceType = 'InvoiceLedger';
  public sortAsce = true;
  public sortKey = 'voucherNumber';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IInvoiceLedgerDTO): IInvoiceLedger => item;

  public getBaseUrl = () => '/api/invoice-ledgers';

  public fetchBooking(invoiceLedgerId: InvoiceLedgerKey): Observable<IBooking> {
    const req = new HttpGet(`${this.getBaseUrl()}/${invoiceLedgerId}/booking`);
    return this.repositoryService.httpClient.request(req)
      .pipe(map((res: HttpResponse<IDataResponseBody<IBooking>>) => res.body.data));
  }
}
