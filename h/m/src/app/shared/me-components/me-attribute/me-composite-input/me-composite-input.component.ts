import { Component, ElementRef, ViewChild, OnDestroy, Optional, Self, OnInit, Injector, AfterViewInit } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { FormGroup, ControlValueAccessor, NgControl, AbstractControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';
import { MatSelect } from '@angular/material/select';

import { Subject, Subscription } from 'rxjs';

import { MeCompositeInputI } from './me-composite-input.interface';
import { MeButtonI } from '@shared/me-components/me-button/me-button.interface';
import { MeCompositeDataI, MeCompositeItemI } from './me-composite-data.interface';
import { BaseLoggerComponent } from '@features/tracking-system';
@Component({
  templateUrl: './me-composite-input.component.html',
  styleUrls: ['./me-composite-input.component.scss'],
  providers: [
    {
      provide: MatFormFieldControl,
      useExisting: MeCompositeInputComponent
    }
  ]
})
export class MeCompositeInputComponent
  extends BaseLoggerComponent
  implements ControlValueAccessor, MatFormFieldControl<MeCompositeDataI>, OnInit, AfterViewInit, OnDestroy {
  // variables for MatFormFieldControl interface
  static nextId = 0;
  componentVersion: string = '0.0.1';
  currentValue!: MeCompositeDataI | undefined;
  name: string = '';
  data!: MeCompositeInputI;
  form!: FormGroup;

  @ViewChild('compositeSelect') compositeSelect!: MatSelect;
  @ViewChild('compositePercentage') compositePercentage!: ElementRef;

  components: MeCompositeItemI[] = [];
  sum: number = 0;
  sumLeftOver: number = 100;

  private _placeholder!: string;
  private _required = false;
  private _disabled = false;
  private _valueChangeSubscription!: Subscription | undefined;
  stateChanges = new Subject<void>();
  focused = false;
  errorState = false;
  controlType = 'me-composite-input';
  id = `me-composite-input-${MeCompositeInputComponent.nextId++}`;
  describedBy = '';
  addButton: MeButtonI = {
    type: 'regular',
    color: 'primary',
    disabled: false
  };

  // tslint:disable-next-line:no-any
  onChange = (_: any) => {};
  onTouched = () => {};

  get formControl(): AbstractControl | null {
    return this.form.get(this.data.internalName);
  }

  constructor(
    injector: Injector,
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,

    @Optional() @Self() public ngControl: NgControl
  ) {
    super(injector);
    _focusMonitor.monitor(_elementRef, true).subscribe((origin) => {
      if (this.focused && !origin) {
        this.onTouched();
      }
      this.focused = !!origin;
      this.stateChanges.next();
    });

    if (this.ngControl !== null) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit(): void {
    this.form = this.data.form;
    this.name = this.data.internalName;
    this.currentValue = this.formControl?.value;
    this.disabled = this.data.disabled;
    this.addButton.disabled = this.data.disabled;
    this._valueChangeSubscription = this.formControl?.valueChanges.subscribe((val: MeCompositeDataI) => {
      this.currentValue = val;
    });
    if (this.value) {
      this.components = [...this.value.components];
      this.updateSum();
    }
  }

  ngAfterViewInit(): void {
    this.compositeSelect.disabled = this.data.disabled;
    this.compositePercentage.nativeElement.disabled = this.data.disabled;
  }

  // methods required by MatFormFieldControl interface
  get empty(): boolean {
    return false;
  }

  get shouldLabelFloat(): boolean {
    return this.focused || !this.empty;
  }

  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  get required(): boolean {
    return this._required;
  }
  set required(value: boolean) {
    this._required = value;
    this.stateChanges.next();
  }

  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = value;

    setTimeout(() => {
      if (this.form) {
        this._disabled ? this.formControl?.disable() : this.formControl?.enable();
        this.stateChanges.next();
      }
    }, 0);
  }

  get value(): MeCompositeDataI {
    return this.formControl?.value;
  }
  set value(value: MeCompositeDataI) {
    this.formControl?.setValue(value);
    this.stateChanges.next();
  }

  setDescribedByIds(ids: string[]): void {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(): void {}

  writeValue(value: MeCompositeDataI): void {
    this.value = value;
  }

  // tslint:disable-next-line:no-any
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  // tslint:disable-next-line:no-any
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  // component methods
  getComponentLabel(componentName: string): string | undefined {
    const component = this.data.values.find((value) => value.value === componentName);
    return component ? component.displayName : undefined;
  }

  handleAddButtonClick(): void {
    const percentageValue: number = parseFloat(this.compositePercentage.nativeElement.value);

    const newComponent: MeCompositeItemI = {
      name: this.compositeSelect.value,
      percentage: parseFloat(percentageValue.toFixed(2)) as number
    };

    if (
      this.compositeSelect.value &&
      percentageValue > 0 &&
      percentageValue <= this.sumLeftOver &&
      this.sumLeftOver !== 0 &&
      !this.components.some((component) => component.name === newComponent.name)
    ) {
      this.components.push(newComponent);

      this.updateFormValue();
      this.updateSum();
      this.compositeSelect.writeValue(undefined);
    }
  }

  removeComponent(component: MeCompositeItemI): void {
    if (this.data.disabled) {
      return;
    }

    this.components = this.components.filter((compItem) => compItem !== component);

    this.updateFormValue();
    this.updateSum();
  }

  private updateFormValue(): void {
    if (this.components.length) {
      this.formControl?.setValue({
        components: this.components
      });

      this.formControl?.markAsDirty();
    } else {
      this.formControl?.setValue(null);
      this.formControl?.markAsPristine();
    }
  }

  private updateSum(): void {
    let newSum: number = 0;
    const maxSum: number = 100;

    this.components.forEach((component) => {
      newSum += component.percentage;
    });

    this.sum = +newSum.toFixed(2);

    this.sumLeftOver = +(maxSum - newSum).toFixed(2);

    if (newSum >= 100) {
      this.formControl?.setErrors(null);
    } else {
      this.formControl?.setErrors({ invalid: true });
    }

    this.updateErrorState();
  }

  updateErrorState(): void {
    if (this.formControl?.value === null) {
      this.errorState = true;
    } else {
      this.errorState = false;
    }

    this.stateChanges.next();
  }

  ngOnDestroy(): object {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
    if (this._valueChangeSubscription) {
      this._valueChangeSubscription.unsubscribe();
    }
    return super.ngOnDestroy();
  }
}
