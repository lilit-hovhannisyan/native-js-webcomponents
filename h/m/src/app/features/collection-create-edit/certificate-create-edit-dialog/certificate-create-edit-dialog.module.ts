import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificateCreateEditDialogComponent } from './certificate-create-edit-dialog.component';
import { DigitalProfileEditRoutingModule } from '@features/digital-profile-edit/digital-profile-edit-routing.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MeAttributeModule } from '@shared/me-components/me-attribute/me-attribute.module';
import { MeButtonModule } from '@shared/me-components/me-button/me-button.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MeEmptyContentModule } from '@shared/me-components/me-empty-content/me-empty-content.module';
import { MatStepperModule } from '@angular/material/stepper';
import { MeUploadFileModule } from '@shared/me-components/me-upload-file/me-upload-file.module';
import { MeComponentLoadingModule } from '@shared/me-directives/me-component-loader/me-component-loader.module';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';
import { MeDocumentLineModule } from '@shared/me-components/me-document-line/me-document-line.module';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [CertificateCreateEditDialogComponent],
  imports: [
    CommonModule,
    DigitalProfileEditRoutingModule,
    IconsModule,
    MeAttributeModule,
    MeButtonModule,
    MatStepperModule,
    ReactiveFormsModule,
    MeEmptyContentModule,
    MeUploadFileModule,
    MeComponentLoadingModule,
    MeIconsProviderModule,
    MeDocumentLineModule,
    MatDialogModule,
    MeComponentLoadingModule
  ],
  exports: [CertificateCreateEditDialogComponent]
})
export class CertificateCreateEditDialogModule {}
