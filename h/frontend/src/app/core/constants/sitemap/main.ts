import { SitemapNode } from './sitemap';

export const main: SitemapNode = {
  path: 'main',
  description: { en: '', de: '' },
  title: { en: 'Dashboard', de: 'Dashboard' },
  icon: 'global',
  hidden: true,
  zone: 'main',
  freeAccess: true,
  children: {
    settings: {
      path: 'settings',
      title: { en: 'Settings', de: 'Einstellungen' },
      description: { en: 'Description...', de: 'Beschreibung...' },
      icon: 'assets/icons/88-512.png',
      zone: 'settings',
      freeAccess: true,
      children: {
        accountSettings: {
          path: 'account-settings',
          description: { en: '', de: '' },
          title: { en: 'Account Settings', de: 'Account Einstellungen' },
          hidden: true,
          zone: 'accountSettings',
          freeAccess: true,
        },
      }
    }
  }
};
