import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors, FormGroup } from '@angular/forms';
import { CostCenterKey, ICostCenter } from 'src/app/core/models/resources/ICostCenter';
import { CostCenterRepositoryService } from 'src/app/core/services/repositories/cost-center-repository.service';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { wording } from 'src/app/core/constants/wording/wording';
import { IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { ACCOUNTING_RELATIONS } from 'src/app/core/models/enums/accounting-relations';
import { Validators } from 'src/app/core/classes/validators';
import { wordingAutocomplete } from 'src/app/core/constants/wording/autocomplete';
import { BookingKey } from 'src/app/core/models/resources/IBooking';

@Injectable({
  providedIn: 'root'
})
export class CostCenterHelperService {

  constructor(
    private costCenterRepo: CostCenterRepositoryService,
  ) { }

  public isCostCenterActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.parent || !control.value) {
      return null;
    }
    const costCenterId: CostCenterKey = +control.parent.get('costCenterId').value;

    const costCenter: ICostCenter = this.costCenterRepo.getStreamValue()
      .find(c => c.id === costCenterId);

    if (!costCenter || costCenter.isActive) {
      return null;
    }

    markControlAsTouched(control);
    return {
      doesNotExist: {
        message: wordingAutocomplete.notActive
      }
    };
  }


  public doesCostCenterExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.parent || !control.value) {
      return null;
    }
    const costCenterId: CostCenterKey = +control.parent.get('costCenterId').value;

    const costCenter: ICostCenter = this.costCenterRepo.getStreamValue()
      .find(c => c.id === costCenterId);

    if (costCenter) {
      return null;
    }

    markControlAsTouched(control);
    return {
      doesNotExist: {
        message: wording.general.doesNotExist
      }
    };
  }

  /**
    * field CC only active if mandator of booking line has cost accounting = true
    * AND account of booking line has cost accounting = true
  */
  public enableOrDisableCostCenters(
    form: FormGroup,
    mandator: IMandatorLookup,
    bookingAccount: IBookingAccount,
    bookingId: BookingKey
  ): void {
    const costCenterNoCtrl: AbstractControl = form.get('costCenterNo');
    /**
      * only on profit and loss accounts, that have cost accounting = true
      * field CC only active and required if mandator of booking line has cost accounting = true
     */
    if (
      mandator?.isCostAccounting &&
      bookingAccount?.isCostAccounting &&
      bookingAccount.accountingRelation !== ACCOUNTING_RELATIONS.BALANCE
    ) {
      costCenterNoCtrl.setValidators([
        Validators.required,
        this.doesCostCenterExist,
        this.isCostCenterActive,
      ]);
      costCenterNoCtrl.enable();
    } else {
      costCenterNoCtrl.clearValidators();
      costCenterNoCtrl.disable();
      if (!bookingId) {
        costCenterNoCtrl.patchValue(null);
      }
    }
  }

}
