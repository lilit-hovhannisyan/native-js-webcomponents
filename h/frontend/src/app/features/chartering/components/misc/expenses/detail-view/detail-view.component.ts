import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { Language } from 'src/app/core/models/language';
import { ExpensesRepositoryService } from 'src/app/core/services/repositories/expenses-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { ExpensesHelperService } from './expenses.helper.service';

@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
  providers: [ExpensesHelperService]
})
export class ExpensesDetailViewComponent implements OnInit, OnDestroy {
  private componentDestroyed$ = new ReplaySubject<boolean>(1);
  public language: Language;
  public title: string;
  public sitemap = sitemap;

  public get isCreationMode(): boolean {
    return this.route.snapshot.params.id === 'new';
  }

  constructor(
    private expensesRepo: ExpensesRepositoryService,
    private router: Router,
    private route: ActivatedRoute,
    private settingsService: SettingsService,
    private expensesHelperService: ExpensesHelperService,
  ) { }

  public ngOnInit() {
    this.language = this.settingsService.language;
    const expenseId = +this.route.snapshot.params.id;

    if (this.isCreationMode) {
      this.expensesHelperService.generateTitle();
    } else {
      this.expensesRepo.fetchOne(expenseId, {})
        .pipe(
          takeUntil(this.componentDestroyed$),
        )
        .subscribe(expense => {
          this.expensesHelperService.generateTitle(expense);
        });
    }

    this.expensesHelperService.editViewTitle$.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(title => {
      this.title = title;
    });
  }

  public routeBack = () => {
    this.router.navigate([url(sitemap.chartering.children.misc.children.expenses)]);
  }

  public tabDisabler = (node: SitemapNode) => {
    const basicNode = sitemap.chartering.children.misc.children.expenses.children.id.children.basic;
    return this.isCreationMode ? node === basicNode : true;
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
