import { Type } from '@angular/core';
import { MeAttributeComponentInterface } from './me-attribute-component.interface';

export class MeAttributeItem implements MeAttributeComponentInterface {
  // tslint:disable-next-line:no-any
  constructor(public component: Type<MeAttributeComponentInterface>, public data: any) {}
}
