import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { ICostUnit, CostUnitKey, ICostUnitCreate, ICostUnitDTO } from '../../models/resources/ICostUnit';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class CostUnitRepositoryService extends AbstractRepository<
  ICostUnit,
  ICostUnitDTO,
  ICostUnitCreate,
  RepoParams<ICostUnit>,
  CostUnitKey
> {
  protected resourceType = 'CostUnits';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/cost-units';
  }

  protected postProcess(item: ICostUnitDTO): ICostUnit { return item; }

}
