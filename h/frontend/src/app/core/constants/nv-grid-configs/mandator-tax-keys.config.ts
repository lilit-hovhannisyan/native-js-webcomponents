import { NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { wording } from '../wording/wording';
import { Operations } from '../../models/Operations';

const wordingTaxKeys = wording.accounting;
const wordingGeneral = wording.general;

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    title: wordingTaxKeys.taxRelCodes,
    gridName: 'MandatorTaxKeysGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'code',
        title: wordingTaxKeys.taxKeysLabelCode,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'name',
        title: wordingGeneral.name,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'percentage',
        title: wordingTaxKeys.taxKeysLabelPercentage,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          selectValues: []
        }
      },
      {
        key: 'accountNo',
        title: wordingTaxKeys.taxKeysLabelAccountNo,
        width: 120,
        isSortable: true,
        filter: {
          values: [],
        },
      },
      {
        key: 'accountLabel',
        title: wordingTaxKeys.accountName,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
        },
      }
    ],
    buttons: [
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteTaxKey',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        func: actions.delete,
        hidden: !canPerform(Operations.DELETE),
      },
      {
        // Note: hidden button is added to enable double click on a row
        icon: '',
        description: '',
        actOnDoubleClick: true,
        func: (data) => actions.show(data),
        hidden: true,
      },
    ],
    toolbarButtons: [
      {
        title: wordingTaxKeys.buttonDescriptionAddTaxKey,
        tooltip: wordingTaxKeys.tooltipTextAddTaxKey,
        icon: 'plus-circle-o',
        func: actions.add,
        hidden: () => !canPerform(Operations.CREATE),
      }
    ]
  });
};
