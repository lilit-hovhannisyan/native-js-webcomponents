import { Component, Injectable, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MePopupSelectComponent } from './me-popup-select.component';
import { MePopupSelectI } from './me-popup-select.interface';

@Injectable()
export class MePopupSelectService {
  private _takeUntilObservable$: Subject<void> = new Subject<void>();
  private _currentInstance: MePopupSelectComponent | undefined;

  constructor(private dialog: MatDialog) {}

  openPopupSelect(dataModel: MePopupSelectI, content?: TemplateRef<Component>): Subject<string> {
    const subjectToReturn = new Subject<string>();
    const dialogRef = this.dialog.open(MePopupSelectComponent, { data: { dataModel, content }, panelClass: 'no-padding-popup' });

    this._currentInstance = dialogRef.componentInstance;
    this._currentInstance.popupSelectEvent
      .pipe(takeUntil(this._takeUntilObservable$))
      .subscribe((event: { eventName: string; items?: [] }) => {
        if (event.eventName === 'close') {
          dialogRef.close();
        } else if (event.eventName === 'addItems') {
          subjectToReturn.next(event.eventName);
          dialogRef.close();
        } else {
          subjectToReturn.next(event.eventName);
        }
      });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this._takeUntilObservable$))
      .subscribe(() => {
        subjectToReturn.next('close');
        this._takeUntilObservable$.next();
        this._takeUntilObservable$.complete();
        this._currentInstance = undefined;
        subjectToReturn.complete();
      });

    return subjectToReturn;
  }

  pushDataModelToPopup(data: MePopupSelectI): void {
    if (this._currentInstance) {
      this._currentInstance.dataModel = data;
    }
  }

  closePopup(): void {
    if (this._currentInstance) {
      this._currentInstance.customEvent('close');
    }
  }
}
