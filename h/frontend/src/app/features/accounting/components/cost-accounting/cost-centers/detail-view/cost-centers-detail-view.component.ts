import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { ICostCenter, ICostCenterCreate } from 'src/app/core/models/resources/ICostCenter';
import { FormGroup, FormBuilder } from '@angular/forms';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { CostCenterRepositoryService } from 'src/app/core/services/repositories/cost-center-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { Validators } from 'src/app/core/classes/validators';
import { ActivatedRoute } from '@angular/router';
import { getCostCenterTypesMap, COST_CENTER_TYPE } from 'src/app/core/models/enums/cost-center-type';
import { IWording } from 'src/app/core/models/resources/IWording';

@Component({
  selector: 'nv-cost-centers-detail-view',
  templateUrl: './cost-centers-detail-view.component.html',
  styleUrls: ['./cost-centers-detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CostCentersDetailViewComponent extends ResourceDetailView<ICostCenter, ICostCenterCreate>
  implements OnInit {

  protected repoParams = {};
  public form: FormGroup;
  protected resourceDefault = { isActive: true } as ICostCenter;
  public routeBackUrl = url(sitemap.accounting.children.costAccounting.children.costCenters);

  public costCenterTypes: Map<COST_CENTER_TYPE, IWording> = getCostCenterTypesMap();

  constructor(
    private fb: FormBuilder,
    costCenterRepo: CostCenterRepositoryService,
    baseComponentService: BaseComponentService,
    private route: ActivatedRoute
  ) {
    super(costCenterRepo, baseComponentService);
  }

  public getTitle = () => {
    return `${this.resource.no} ${this.resource.displayLabel[this.settingsService.language]}`;
  }


  public ngOnInit(): void {
    this.init();

    const { id } = this.route.snapshot.params;

    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public init(costType: ICostCenter = this.resourceDefault): void {
    this.initForm(this.fb.group({
      no: [costType.no, [Validators.required, Validators.integerBetween(1, 99999)]],
      labelDe: [costType.labelDe, [Validators.required, Validators.maxLength(150)]],
      labelEn: [costType.labelEn, [Validators.required, Validators.maxLength(150)]],
      type: [costType.type, [Validators.required]],
      remark: [costType.remark],
      isActive: [costType.isActive],
    }));
  }

  public submit(): void {
    if (!this.form.valid) {
      return;
    }

    const { value } = this.form;

    this.creationMode
      ? this.createResource(value)
      : this.updateResource({ ...this.resource, ...value });
  }
}
