import { IWidgetDTO, IWidgetBase } from './IWidget';
import { GridsterItem } from 'angular-gridster2';

export interface IClockWidget extends GridsterItem, IWidgetDTO {
  timeZones: string;
}

export interface IClockWidgetDTO extends IWidgetDTO {
  timeZones?: string;
}
