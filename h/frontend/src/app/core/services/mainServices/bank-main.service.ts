import { Injectable } from '@angular/core';
import { forkJoin, Observable, combineLatest } from 'rxjs';
import { BankRepositoryService } from '../repositories/bank-repository.service';
import { map, switchMap } from 'rxjs/operators';
import { CountryRepositoryService, MAX_COUNTRIES } from '../repositories/country-repository.service';
import { IBank } from '../../models/resources/IBank';
import { ICountry } from '../../models/resources/ICountry';
import { RepoParams } from '../../abstracts/abstract.repository';
import { SettingsService } from '../settings.service';
import { IWording } from '../../models/resources/IWording';

export interface IBankFull extends IBank {
  country: ICountry;
}

export interface IBankRow extends IBankFull {
  countryName: IWording;
}

@Injectable({
  providedIn: 'root'
})
export class BankMainService {

  constructor(
    private bankRepo: BankRepositoryService,
    private countryRepo: CountryRepositoryService,
    private settingsService: SettingsService
  ) { }

  private getBankFull(bank: IBank, countries: ICountry[]): IBankFull {
    const country = countries.find(c => c.id === bank.countryId);
    return { ...bank, country };
  }

  private getBankFullList(banks: IBank[], countries: ICountry[]): IBankFull[] {
    return banks.map(bank => this.getBankFull(bank, countries));
  }

  private composeBank(
    bank$: Observable<IBank>,
    countries$: Observable<ICountry[]>
  ): Observable<IBankFull> {

    return forkJoin([bank$, countries$]).pipe(
      map(([bank, countries]) => this.getBankFull(bank, countries))
    );
  }

  private composeBankList(
    banks$: Observable<IBank[]>,
    countries$: Observable<ICountry[]>
  ): Observable<IBankFull[]> {

    return forkJoin([banks$, countries$]).pipe(
      map(([banks, countries]) => {
        return banks.map((bank: IBank) => this.getBankFull(bank, countries));
      })
    );
  }

  public fetchAll(params: RepoParams<IBank>) {
    return this.composeBankList(
      this.bankRepo.fetchAll(params),
      this.countryRepo.fetchAll({ queryParams: { limit: MAX_COUNTRIES } })
    );
  }

  public fetchOne(id: number, params: RepoParams<IBank>) {
    return this.composeBank(
      this.bankRepo.fetchOne(id, params),
      this.countryRepo.fetchAll({ queryParams: { limit: MAX_COUNTRIES } })
    );
  }

  public getStream(params = {}): Observable<IBankFull[]> {
    return forkJoin([
      this.bankRepo.fetchAll(params),
      this.countryRepo.fetchAll({})
    ]).pipe(
      switchMap(() => combineLatest([
        this.bankRepo.getStream(),
        this.countryRepo.getStream(),
      ])),
      map(res => this.getBankFullList(...res)),
    );
  }


  public toRow(bank: IBankFull): IBankRow {
    const { country } = bank;
    return ({
      ...bank,
      countryName: country.displayLabel
    });
  }

  public toRows = (banks: IBankFull[]): IBankRow[] =>
    banks.map((bank) => this.toRow(bank))
}
