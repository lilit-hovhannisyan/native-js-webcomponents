import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeCertificateComponent } from './me-certificate.component';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';
import { MeLabelModule } from '../me-label/me-label.module';
import { MeContextMenuModule } from '../me-context-menu/me-context-menu.module';

@NgModule({
  declarations: [MeCertificateComponent],
  imports: [CommonModule, MeIconsProviderModule, MeLabelModule, MeContextMenuModule],
  exports: [MeCertificateComponent]
})
export class MeCertificateModule {}
