import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeDataImportComponent } from './me-data-import.component';
import { MePopupAccountModule } from '../me-popup-account/me-popup-account.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [MeDataImportComponent],
  imports: [CommonModule, MePopupAccountModule, IconsModule, MatButtonModule, MatSlideToggleModule, ReactiveFormsModule, FormsModule],
  exports: [MeDataImportComponent]
})
export class MeDataImportModule {}
