import { FormGroup, FormControlDirective } from '@angular/forms';
import { scrollToFirstInvalidControl } from './form-fields-error-scroll';

/**
 * This helper function goes through all controls inside
 * a form and tries to validate these. Usually it is used
 * before doing a submit.
 */
export const validateAllFormFields = (
  formGroup: FormGroup,
  opts: { onlySelf?: boolean, emitEvent?: boolean } = {},
  element?: HTMLElement,
  scrollElementSelector = 'form'
): void => {
  for (const control in formGroup.controls) {
    if (formGroup.controls.hasOwnProperty(control)) {
      const formControl = formGroup.controls[control];
      if (!formControl.valid) {
        formControl.markAsDirty(opts);
        formControl.markAsTouched(opts);
        formControl.updateValueAndValidity(opts);
      }
    }
  }
  if (!formGroup.valid && element) {
    // skip a cycle until dom is updated (.invalid class is added)
    setTimeout(() => {
      scrollToFirstInvalidControl(element, scrollElementSelector);
    });
  }
};


