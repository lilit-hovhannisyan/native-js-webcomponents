import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersTableComponent } from './users-table.component';
import { MeTableModule } from '@shared/me-components/me-table/me-table.module';
import { UserActionsService } from '@features/backoffice/users-groups/user-actions.service';
import { MeAddListCardModule } from '@shared/me-components/me-add-list-card/me-add-list-card.module';
import { MeAddListPopupService } from '@shared/me-components/me-popup-add-list/me-popup-add-list.service';

@NgModule({
  declarations: [UsersTableComponent],
  imports: [CommonModule, MeTableModule, MeAddListCardModule],
  exports: [UsersTableComponent],
  providers: [UserActionsService, MeAddListPopupService]
})
export class UsersTableModule {}
