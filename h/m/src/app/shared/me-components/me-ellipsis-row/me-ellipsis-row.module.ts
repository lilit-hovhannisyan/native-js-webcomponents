import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeEllipsisRowComponent } from './me-ellipsis-row.component';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  declarations: [MeEllipsisRowComponent],
  imports: [MatTooltipModule, CommonModule],
  exports: [MeEllipsisRowComponent]
})
export class MeEllipsisRowModule {}
