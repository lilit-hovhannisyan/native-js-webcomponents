import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { ReplaySubject, Observable, combineLatest } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { getLegalEntityBusinessNamesGridConfig } from './legal-entity-business-names.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { IWording } from 'src/app/core/models/resources/IWording';
import { wording } from 'src/app/core/constants/wording/wording';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { NzModalService } from 'ng-zorro-antd/modal';
import { SettingsService } from 'src/app/core/services/settings.service';
import { IBusinessName } from 'src/app/core/models/resources/IBusinessName';
import { map, tap, startWith, takeUntil } from 'rxjs/operators';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { LegalEntityBusinessNamesModalComponent } from 'src/app/features/system/shared/legal-entity-business-names-modal/legal-entity-business-names-modal.component';
import { FormControl } from '@angular/forms';
import { orderBy } from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'nv-legal-entity-business-names',
  templateUrl: './business-names.component.html',
  styleUrls: ['./business-names.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LegalEntityBusinessNamesComponent implements OnInit, OnChanges, OnDestroy {
  @Input() public editable: boolean;
  @Input() public businessNamesCtrl: FormControl;
  @Output() public businessNameAdd = new EventEmitter<IBusinessName>();
  @Output() public businessNameDelete = new EventEmitter<IBusinessName>();
  public businessNameWording: IWording = wording.system.businessName;
  public gridConfig: NvGridConfig;
  public businessNames$: Observable<IBusinessName[]>;
  public allBusinessNames$: Observable<IBusinessName[]>;
  public wording = wording;
  public showAllCtrl = new FormControl(false);

  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  private lastBusinessName?: IBusinessName;

  constructor(
    private authService: AuthenticationService,
    private modalService: NzModalService,
    private settingsService: SettingsService,
    private confirmationService: ConfirmationService,
  ) { }

  public ngOnInit(): void {
    this.initData();
  }

  public ngOnChanges({ editable }: SimpleChanges): void {
    if (editable) {
      this.initGrid();
      this.initData();
    }
  }

  public initData(): void {
    this.allBusinessNames$ = this.businessNamesCtrl.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed$),
        startWith(this.businessNamesCtrl.value),
        map((names: IBusinessName[]) => names),
      );
    this.businessNames$ =
      combineLatest([
        this.businessNamesCtrl.valueChanges
          .pipe(
            startWith(this.businessNamesCtrl.value),
            map((names: IBusinessName[]) => names)
          ),
        this.showAllCtrl.valueChanges.pipe(startWith(this.showAllCtrl.value))
      ]).pipe(
        takeUntil(this.componentDestroyed$),
        map(([businessNames, showAll]) => {
          if (!businessNames?.length) {
            return [];
          }

          if (!showAll) {
            return [orderBy([...businessNames], [name => moment(name.from)], 'desc')[0]];
          }
          return [...businessNames];
        }),
        map(names => orderBy(names, [name => moment(name.from)], 'desc')),
        tap(names => {
          this.lastBusinessName = names[0];
          this.initGrid();
        }),
      );
  }

  private initGrid(): void {
    const gridActions: IGridConfigActions = {
      delete: this.deleteBusinessName,
      add: this.openBusinessNameDetailViewModal,
      show: this.openBusinessNameDetailViewModal,
      edit: (businessName: IBusinessName) =>
        this.openBusinessNameDetailViewModal(businessName, true),
    };
    this.gridConfig = getLegalEntityBusinessNamesGridConfig(
      gridActions,
      () => !this.editable,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
      this.settingsService,
      this.businessNameWording,
      this.lastBusinessName,
    );
  }

  private deleteBusinessName = (businessName: IBusinessName): void => {
    this.confirmationService.confirm({
      cancelButtonWording: wording.general.cancel,
      okButtonWording: wording.general.delete,
      wording: setTranslationSubjects(
        wording.system.businessNameDeleteWarning,
        { subject: businessName.name },
      ),
    }).subscribe(confirmed => {
      if (confirmed) {
        this.businessNameDelete.emit(businessName);
      }
    });
  }

  private openBusinessNameDetailViewModal = (
    resource?: IBusinessName,
    editMode = false
  ): void => {
    this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: this.businessNameWording[this.settingsService.language],
      nzContent: LegalEntityBusinessNamesModalComponent,
      nzComponentParams: {
        businessName: resource,
        lastBusinessName: this.lastBusinessName,
        businessNameWording: this.businessNameWording,
        editMode,
      },
      nzWidth: 500,
      nzClosable: true,
    }).afterClose.subscribe((businessName: IBusinessName) => {
      if (!businessName) {
        return;
      }

      this.businessNameAdd.emit(businessName);
    });
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

}
