/* tslint:disable:max-line-length*/

import { wording } from '../wording/wording';
import { SitemapNode } from './sitemap';

export const accounting: SitemapNode = {
  path: 'accounting',
  description: { en: 'Description...', de: 'Beschreibung...' },
  title: {
    en: 'Accounting',
    de: 'Buchhaltung'
  },
  icon: 'schedule',
  zone: 'accounting',
  children: {
    booking: {
      path: 'booking',
      title: { en: 'Booking', de: 'Booking' },
      description: { en: 'Description...', de: 'Beschreibung...' },
      icon: 'assets/icons/88-512.png',
      zone: 'booking',
      children: {
        voucherTypes: {
          path: 'voucher-types',
          children: {
            id: {
              path: ':voucherTypeId',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: wording.accounting.basic,
                },
                history: {
                  path: 'history',
                  zone: '*',
                  title: wording.accounting.historyTab,
                },
                meta: {
                  path: 'meta',
                  zone: '*',
                  title: wording.accounting.metaTab,
                }
              }
            },
          },
          description: {
            en: 'Read, Edit and add voucher types here. You can define the settings of custom vouchertypes.',
            de: 'Rufen Sie hier die Belegarten auf, bearbeiten Sie individuelle Belegarten und fügen Sie neue hinzu.'
          },
          keywords: [
            'Voucher',
            'Voucher number',
            'VoucherNo',
            'Abbreviation',
            'Business',
            'Case',
            'unique',
            'Belege',
            'Belegnummern',
            'Belegnummer',
            'Kürzel',
            'Belegkürzel',
            'Geschäftsvorfall',
            'Eindeutig'
          ],
          title: { en: 'Voucher Types', de: 'Belegarten' },
          zone: 'voucherTypes',
        },
        mandators: {
          path: 'mandators',
          description: {
            en: 'Open mandators and update values and settings here. You can create new mandators.',
            de: 'Rufen Sie hier die Mandanten auf, bearbeiten Sie selbige und legen Sie bei Bedarf neue Mandanten an.',
          },
          title: wording.accounting.mandators,
          zone: 'mandators',
          keywords: [
            'Maindata',
            'Companies',
            'Company',
            'Company Code',
            'Code',
            'Block',
            'Differentiating',
            'financial',
            'year',
            'Stammdaten',
            'Gesellschaften',
            'Gesellschaft',
            'Buchungskreis',
            'Blockieren',
            'Periode',
            'Abweichendes',
            'Wirtschaftsjahr',
          ],
          children: {
            id: {
              path: ':mandatorId',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: wording.accounting.basic,
                },
                company: {
                  path: 'company',
                  zone: 'company',
                  title: { en: 'Company & address', de: 'Company & address' }
                },
                voucherTypes: {
                  path: 'voucherTypes',
                  zone: 'voucherTypes',
                  title: { en: 'Voucher Types', de: 'Voucher Types' }
                },
                taxKeys: {
                  path: 'taxKeys',
                  zone: 'taxKeys',
                  title: { en: 'Taxes', de: 'Steuern' }
                },
                accounts: {
                  path: 'accounts',
                  zone: 'accounts',
                  title: { en: 'Accounts', de: 'Konten' }
                },
                bookingCodes: {
                  path: 'bookingCodes',
                  zone: 'bookingCodes',
                  title: { en: 'Booking Codes', de: 'Booking Codes' }
                },
                roles: {
                  path: 'roles',
                  zone: 'roles',
                  title: { en: 'Roles', de: 'Rollen' }
                },
                corporate: {
                  path: 'corporate',
                  zone: '*',
                  title: { en: 'Corporate', de: 'InterCompany' }
                },
                history: {
                  path: 'history',
                  zone: '*',
                  title: wording.accounting.historyTab,
                },
                meta: {
                  path: 'meta',
                  zone: '*',
                  title: wording.accounting.metaTab,
                }
              }
            }
          }
        },
        debitorCreditors: {
          path: 'debtors-and-creditors',
          description: {
            en: 'See all business partners here.',
            de: 'Sehen Sie hier die Stammdaten Ihrer Geschäftspartner'
          },
          title: wording.accounting.debtorsCreditors,
          zone: 'debitorCreditor',
          keywords: [
            'Debtor',
            'Creditor',
            'Customer',
            'Business partner',
            'Supplier',
            'Debtor',
            'Debitor',
            'Kreditor',
            'Kunde',
            'Geschäftspartner',
            'Lieferant',
          ],
          children: {
            id: {
              path: ':id',
              zone: '*',
              title: wording.accounting.debtorsCreditors,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: wording.accounting.basic
                },
                dunningSystem: {
                  path: 'dunning-system',
                  zone: 'dunning',
                  title: wording.system.dunningSystem
                },
                conditionsOfPayment: {
                  path: 'condition-of-payment',
                  zone: 'paymentConditions',
                  title: wording.system.conditionOfPayments
                },
                remarks: {
                  path: 'remarks',
                  zone: '*',
                  title: wording.system.remarks
                },
              }
            }
          }
        },
        bookingAccounts: {
          path: 'booking-accounts',
          description: {
            en: 'List all master accounts and add new accounts if needed.',
            de: 'Rufen Sie den Masterkontenrahmen auf und legen Sie bei Bedarf neue Masterkonten an.'
          },
          title: { en: 'Chart of Accounts', de: 'Masterkontenrahmen' },
          keywords: [
            'Master',
            'Account',
            'Main',
            'Master',
            'Konto',
            'Kontenrahmen',
            'Konten',
          ],
          zone: 'bookingAccounts',
        },
        taxKeys: {
          path: 'tax-keys',
          children: {
            id: {
              path: ':taxKeyId',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: wording.accounting.basic,
                },
                history: {
                  path: 'history',
                  zone: '*',
                  title: wording.accounting.historyTab,
                },
                meta: {
                  path: 'meta',
                  zone: '*',
                  title: wording.accounting.metaTab,
                }
              }
            },
          },
          description: {
            en: 'Define needed tax keys and specify the related rules for postings.',
            de: 'Definieren Sie die zu verwendenden Steuerschlüssel und legen Sie deren buchhalterische Verwendung fest.'
          },
          title: { en: 'Tax Keys', de: 'Steuerschlüssel' },
          keywords: [
            'VAT',
            'Value added',
            'Sales',
            'Mehrwertsteuer',
            'Vorsteuer',
            'Mwst',
            'Umsatzsteuer',
          ],
          zone: 'taxKeys',
        },
        bookingCodes: {
          path: 'booking-codes',
          description: {
            en: 'Obtain information for booking codes, adjust same and create new ones if needed',
            de: 'Informieren Sie sich über die Einstellungen einzelner Buchungscodes, passen Sie selbige an oder legen Sie bei Bedarf neue an.'
          },
          title: { en: 'Booking Codes', de: 'Buchungscodes' },
          keywords: [
            'BC',
            'Code',
            'Flag',
            'Mark',
            'Key',
            'Payment relevant',
            'Journey',
            'Book',
            'Post',
            'VGS',
            'Vorgang',
            'Buchungskennzeichen',
            'Kennzeichen',
            'Schlüssel',
            'Vorgangschlüssel',
            'Zahlungsrelevant',
            'Reise',
            'Buchung',
          ],
          zone: 'bookingCodes',
        },
        bookings: {
          path: 'bookings',
          description: {
            en: 'Post business operations with the navido booking form and benefit from several automatic validations to reduce human mistakes.',
            de: 'Buchen Sie erfolgte Geschäftsvorfälle über die Navido Buchungsmaske und profitieren Sie von den zahlreichen automatischen Plausibilitätsprüfungen, welche Sie optimal dabei unterstützen Benutzerfehler zu vermeiden.',
          },
          title: { en: 'Create Bookings', de: 'Buchung erfassen' },
          keywords: [
            'Book',
            'Post',
            'Create',
            'Record',
            'Enter',
            'Buchen',
            'Buchungsmaske',
            'Buchung eingeben',
            'Verbuchen',
          ],
          zone: 'booking',
        },
        accountSheets: {
          path: 'account-sheets',
          description: {
            en: 'View all bookings on a defineable Account of a chooseable mandator of a specified year, edit bookings, see open items and create new bookings.',
            de: 'Rufen Sie die Buchhaltungskonten auf, sichten, bearbeiten und finden Sie die Buchungen, sehen Sie die offenen Posten ein und führen Sie Buchungen aus.',
          },
          title: wording.accounting.accountSheets,
          keywords: [
            'statement',
            'open item',
            'offsetting entry',
            'counter entry',
            'voucher',
            'Cancellation; Reversal',
            'balance',
            'Overview',
            'Booking',
            'Kontenblatt',
            'Kontoauszug',
            'OP',
            'offene Posten',
            'Buchhaltungskonto',
            'Gegenbuchung',
            'Buchbeleg',
            'Storno',
            'Buchung bearbeiten',
            'Saldo',
            'Saldovortrag',
            'Übersicht',
            'Buchung'
          ],
          zone: 'accountSheets'
        },
        openItemAccountSheets: {
          path: 'open-item-account-sheets',
          description: {
            en: 'View all open items on a customer account, manage open items and save comments on the status of the open item.',
            de: 'Rufen Sie die OP-Konten auf, verwaten Sie die offenen Posten und hinterlegen Kommentare für die einzelnen OP\'s .',
          },
          title: wording.accounting.openItemAccountSheets,
          keywords: [
            'open item',
            'offsetting entry',
            'counter entry',
            'voucher',
            'cancellation',
            'reversal',
            'balance',
            'overview',
            'booking',
            'comment',
            'remark accounts revceivables',
            'AR',
            'dunning letter',
            'dunning process debtor',
            'Kontenblatt',
            'Kontoauszug',
            'OP',
            'offene Posten',
            'Buchhaltungskonto',
            'Gegenbuchung',
            'Buchbeleg',
            'Storno',
            'Buchung bearbeiten',
            'Saldo',
            'Saldovortrag',
            'Übersicht',
            'Buchung',
            'Kommentar',
            'Mahnung',
            'Mahnwesen'
          ],
          zone: 'openItems'
        },
        invoiceLedger: {
          path: 'invoice-ledger',
          description: {
            en: 'Manage and find incoming invoices here. You can book and cancel your invoices and check their status.',
            de: 'Organisieren Sie hier Ihre Eingangsrechnungen. Sie können diese von hier sowohl Buchen als auch Stornieren.'
          },
          keywords: [
            'Incoming',
            'invoice',
            'create',
            'ledger',
            'order',
            'costs',
            'booking',
            'posting',
            'overview',
            'open',
            'booked',
            'Rechnungen',
            'Rechnungseingang',
            'Eingangsrechnungen',
            'Eingang',
            'Bestellung',
            'Bestellnummer',
            'buchen',
            'Übersicht',
            'offen',
            'gebucht',
          ],
          title: wording.accounting.purchaseInvoices,
          zone: 'invoiceLedgers',
        },
        openItemAccounts: {
          path: 'open-item-accounts',
          description: {
            en: 'Find all open item accounts of a mandator. You can define new open item accounts for mandators here.',
            de: 'Lassen Sie sich hier die OP-Konten der Mandanten anzeigen und definieren Sie bei Bedarf weitere OP-Konten ',
          },
          keywords: [
            'Maindata',
            'open',
            'open item',
            'Mandator',
            'OP',
            'offen',
            'Offene Posten',
            'OP-Konten',
            'accounts',
            'Stammdaten',
            'Mandant',
            'Konto',
            'Konten',
          ],
          title: wording.accounting.openItemAccounts,
          zone: 'bookingAccounts',
        },
        masterStructures: {
          path: 'master-structures',
          description: {
            en: '',
            de: ''
          },
          keywords: [
          ],
          title: wording.accounting.masterStructures,
          zone: 'masterStructures',
        },
      }
    },

    autobank: {
      path: 'autobank',
      title: { en: 'Autobank', de: 'Autobank' },
      description: { en: 'Autobank...', de: 'Autobank...' },
      icon: 'assets/icons/88-512.png',
      zone: 'autobank',
      children: {
        rules: {
          path: 'autobank-rules',
          description: {
            en: 'Define rules and parameters as templates for automatic processing of bank statements',
            de: 'Definieren Sie Regeln und Parameter als Vorlage zur automatischen Verarbeitung der Bankauszüge',
          },
          title: { en: 'Autobank Rules', de: 'Autobank Regeln' },
          keywords: [
            'Bankauszüge',
            'Filter',
            'Autobank',
            'import',
            'Regel',
            'account statements',
            'filters',
            'rules',
          ],
          zone: 'rules',
          children: {
            id: {
              path: ':id',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: wording.accounting.basic,
                },
                meta: {
                  path: 'meta',
                  zone: '*',
                  title: wording.accounting.metaTab,
                },
              }
            }
          },
        },
        accountingRelations: {
          path: 'accounting-relations',
          description: {
            en: 'Define the corresponding bookkeeping accounts per bank account for automatic posting of the bank statements to be imported',
            de: 'Definieren Sie die zugehörigen Buchhaltungskonten je Bankkonto zur automatischen Buchung der zu importierenden Bankauszüge',
          },
          title: { en: 'Accounting Relations', de: 'Autobank Bankkontenverwaltung' },
          keywords: [
            'Bankkonto',
            'Buchhaltung',
            'Konto',
            'Autobank',
            'Einstellungen',
            'Settings',
            'bank',
            'account',
            'accounts',
            'bookkeeping',
            'accounting',
          ],
          zone: 'rules',
        },
        autobankPermissions: {
          path: 'autobank-permissions',
          description: { en: 'Description...', de: 'Beschreibung...' },
          title: { en: 'Autobank Permissions', de: 'Autobank Berechtigungen' },
          keywords: [
            'Autobank',
          ],
          zone: 'permissions',
          children: {
            id: {
              path: ':id',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: wording.accounting.basic,
                },
                history: {
                  path: 'history',
                  zone: '*',
                  title: wording.general.history
                },
              }
            }
          }
        },
        autobankSettings: {
          path: 'autobank-settings',
          description: {
            en: 'Define system settings for autobank processing and call up statistics.',
            de: 'Definieren Sie Systemeinstellungen zur Autobank-Verarbeitung und rufen Sie Statistiken ab.'
          },
          title: { en: 'Autobank Settings', de: 'Autobank Einstellungen' },
          keywords: [
            'Statistik',
            'Autobank',
            'statistic',
            'settings',
            'Statistik',
            'Einstellungen',
          ],
          zone: 'settings',
          children: {
            settings: {
              path: 'settings',
              zone: '*',
              title: wording.accounting.settings,
            },
            businessCases: {
              path: 'businessCases',
              zone: '*',
              title: wording.accounting.businessCases,
            },
          }
        },
      },
    },
    costAccounting: {
      path: 'cost-acounting',
      title: { en: 'Cost Accounting', de: 'Kostenrechnung' },
      description: { en: 'Cost Accounting...', de: 'Kostenrechnung...' },
      icon: 'assets/icons/88-512.png',
      zone: 'costAccounting',
      children: {
        costTypes: {
          path: 'cost-types',
          description: {
            en: 'Find and edit detailed information related to each cost type',
            de: 'Suchen und bearbeiten Sie die detaillierten Informationen zu jeder Kostenart',
          },
          title: wording.accounting.costType,
          keywords: [
            'cost type',
            'cost accounting',
            'OPEX',
            'relation',
            'CT',
            'KA',
            'Kostenart',
            'Kosten',
            'KLRm Kostenrechnung',
            'KORE',
            'Verknüpfung',
            'Relation',
          ],
          zone: 'costTypes',
        },
        costCenters: {
          path: 'cost-centers',
          description: {
            en: 'Find and edit detailed information related to each cost center',
            de: 'Suchen und bearbeiten Sie die detaillierten Informationen zu jeder Kostenstelle.',
          },
          title: wording.accounting.costCenters,
          keywords: [
            'cost center',
            'cost accounting',
            'OPEX',
            'relation',
            'CC',
            'KST',
            'Kostenstelle',
            'Kosten',
            'KLR',
            'Kostenrechnung',
            'KORE',
            'Verknüpfung',
            'Relation',
          ],
          zone: 'costCenters',
          children: {
            id: {
              path: ':id',
              zone: '*',
              title: wording.accounting.basic,
            }
          },
        },
        costUnits: {
          path: 'cost-units',
          description: {
            en: 'Find and edit detailed information related to each cost unit',
            de: 'Suchen und bearbeiten Sie die detaillierten Informationen zu jedem Kostenträger.',
          },
          title: wording.accounting.costUnits,
          keywords: [
            'cost unit',
            'cost accounting',
            'OPEX',
            'relation',
            'CU',
            'KT',
            'Kostenträger',
            'Kosten',
            'KLR',
            'Kostenrechnung',
            'KORE',
            'Verknüpfung',
            'Relation',
          ],
          zone: 'costUnits',
          children: {
            id: {
              path: ':id',
              zone: '*',
              title: wording.accounting.basic,
            }
          },
        },
        costUnitCategories: {
          path: 'cost-unit-categories',
          description: {
            en: 'Find, add and edit types of cost units',
            de: 'Suchen, bearbeiten und fügen Sie Kostenträgerarten hinzu.',
          },
          title: wording.accounting.costUnitCategories,
          keywords: [
            'cost unit',
            'categories',
            'catagory',
            'CU',
            'group',
            'KT',
            'Kostenträger',
            'KLR',
            'Kostenrechnung',
            'KORE',
            'Gruppe',
            'Kategorie',
          ],
          zone: 'costUnitCategories',
          children: {
            id: {
              path: ':id',
              zone: '*',
              title: wording.accounting.basic,
            }
          },
        },
      },
    },
  },
};
