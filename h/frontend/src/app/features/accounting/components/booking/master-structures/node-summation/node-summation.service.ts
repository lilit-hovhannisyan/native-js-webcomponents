import { Injectable } from '@angular/core';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { NzTreeNode, NzFormatBeforeDropEvent } from 'ng-zorro-antd/tree';
import { wording } from 'src/app/core/constants/wording/wording';
import { IMasterStructureNodeTemplate, IMasterStructureNode, MASTER_STRUCTURE_NODE_TYPE, IMasterStructureNodeHashTable } from 'src/app/core/models/resources/IMasterStructureNode';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { NodeSummationComponent } from './node-summation.component';
import { IMasterStructure } from 'src/app/core/models/resources/IMasterStructure';
import { MASTER_STRUCTURE_TYPES } from 'src/app/core/models/enums/master-structure-type';

@Injectable({
  providedIn: 'root'
})
export class NodeSummationService {
  private currentStructure: IMasterStructure;

  constructor(
    private confirmationService: ConfirmationService,
    private modalService: NzModalService,
  ) { }

  public initCurrentStructure(structure: IMasterStructure): void {
    this.currentStructure = structure;
  }

  public showNodeUsedInSummationWarning(): void {
    this.confirmationService.error({
      title: wording.accounting.nodeUsedInSummationCannotBeDeleted,
      cancelButtonWording: null,
      okButtonWording: wording.general.ok
    }).subscribe();
  }

  public removeSummationNode(
    nodes: IMasterStructureNodeTemplate[],
    foundNode: IMasterStructureNode,
  ): IMasterStructureNodeTemplate[] {
    return nodes.map(n => ({
      ...n,
      parentNodeId: n.parentNodeId === foundNode.id ? null : n.parentNodeId
    }));
  }

  public openSummationModal(
    nodes: IMasterStructureNodeTemplate[],
    selectedNode: NzTreeNode,
    refreshNodes: () => void,
  ): void {
    const modalRef: NzModalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: null,
      nzClosable: true,
      nzContent: NodeSummationComponent,
      nzComponentParams: {
        nodes: nodes,
        selectedNode: selectedNode,
      },
      nzOnOk: componentInstance => {
        const assignedNodesHashTable: IMasterStructureNodeHashTable = componentInstance
          .getAssignedNodesHashTable();
        const nodeIndex: number = nodes
          .findIndex(n => selectedNode.origin.id === n.id);
        nodes[nodeIndex].nodeType = MASTER_STRUCTURE_NODE_TYPE.SUMMATION;
        nodes[nodeIndex].isLeaf = true;
        nodes.forEach(node => {
          if (assignedNodesHashTable[node.id]) {
            node.parentNodeId = nodes[nodeIndex].id;
          } else if (node.parentNodeId === nodes[nodeIndex].id) {
            node.parentNodeId = null;
          }
        });
        refreshNodes();
        modalRef.close();
      },
      nzOnCancel: () => {
        modalRef.close();
      },
    });
  }

  public isInvalidDrop(confirm: NzFormatBeforeDropEvent): boolean {
    const dragNode: NzTreeNode = confirm.dragNode;
    const targetNode: NzTreeNode = confirm.node;
    const isSummation: boolean = dragNode.origin.nodeType === MASTER_STRUCTURE_NODE_TYPE.SUMMATION;
    return (isSummation && targetNode.level > 0)
      || (isSummation && confirm.pos === 0);
  }

  public checkIfSummationIsVisible(selectedNode?: NzTreeNode): boolean {
    if (!selectedNode) {
      return false;
    }

    return (
      this.currentStructure
        .masterStructureType === MASTER_STRUCTURE_TYPES.BUSINESS_MANAGEMENT_EVALUATION
      || this.currentStructure
        .masterStructureType === MASTER_STRUCTURE_TYPES.PROFIT_AND_LOSS_STATEMENT
      )
      && selectedNode?.origin.childNodes?.length === 0
      && selectedNode?.origin.level === 1
      && selectedNode?.level === 0;
  }
}
