import { Component, OnInit, forwardRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { WizardStep } from '../../../../../shared/components/wizard/WizardStep.abstract';
import { WizardService } from '../../../../../shared/services/wizard.service';

@Component({
  selector: 'nv-form1',
  templateUrl: './index.html',
  styleUrls: ['./index.scss'],
  providers: [{provide: WizardStep, useExisting: forwardRef(() => Form1Component)}],
})
export class Form1Component extends WizardStep implements OnInit {

  constructor(
    wizardService: WizardService,
    private fb: FormBuilder
  ) {
    super(wizardService);
  }

  public ngOnInit() {
    this.initForm();
  }

  public initForm() {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }
}
