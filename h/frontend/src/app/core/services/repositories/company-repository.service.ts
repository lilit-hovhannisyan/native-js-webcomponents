import { CompanyKey } from './../../models/resources/ICompany';
import { Injectable } from '@angular/core';
import { HttpResponse, HttpClient } from '@angular/common/http';
import { map, switchMap } from 'rxjs/operators';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { ICompany, ICompanyDTO, ICompanyCreate } from '../../models/resources/ICompany';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';
import { IResourceCollection } from '../../models/resources/IResourceCollection';
import { RepositoryService } from '../repository.service';
import { HttpGet } from '../../models/http/http-get';
import { Observable, forkJoin } from 'rxjs';
import { MandatorRepositoryService } from './mandator-repository.service';
import { DebitorCreditorRepositoryService } from './debitor-creditor-repository.service';
import { IDebitorCreditor } from '../../models/resources/IDebitorCreditor';
import { IMandatorLookup } from '../../models/resources/IMandator';
import { cleanUrl, urlConcat } from '../../helpers/general';
import { CompanyLegalEntityKey, ICompanyLegalEntity } from '../../models/resources/ICompanyLegalEntity';

export interface CompanyRelations {
  mandator?: IMandatorLookup;
  debitorCreditor?: IDebitorCreditor;
}

@Injectable({
  providedIn: 'root'
})
export class CompanyRepositoryService extends AbstractRepository<
ICompany,
ICompanyDTO,
ICompanyCreate,
RepoParams<ICompany>,
CompanyKey
> {
  protected resourceType = 'Company';
  public sortAsce = true;
  public sortKey = 'companyName';

  constructor(
    repositoryService: RepositoryService,
    private mandatorRepo: MandatorRepositoryService,
    private debitorCreditorRepo: DebitorCreditorRepositoryService,
    private http: HttpClient,
  ) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/companies';
  }

  public getVatIdValidationUrl(vatId: string): string {
    return `/api/companies/validateVatId/${vatId}`;
  }

  protected postProcess(item: ICompanyDTO): ICompany {
    return {
      ...item,
      legalForm: item.legalForm ? item.legalForm : null, // backend replaces `null` values with 0
      legalFormType: item.legalFormType ? item.legalFormType : null,
    };
  }

  public fetchAllWithoutMandator() {
    const request = new HttpGet(
      this.getBaseUrl(),
      { init: { params: { withoutMandator: true } } },
    );
    const request$ = this.repositoryService.httpClient.request(request).pipe(
      map((response: HttpResponse<IDataResponseBody<IResourceCollection<ICompany>>>) => response.body.data.items));
    return request$;
  }

  public fetchAllWithoutDebitorCreditor() {
    const request = new HttpGet(this.getBaseUrl(),
      {
        init: { params: { withoutDebitorCreditor: true } }
      }
    );
    const request$ = this.repositoryService.httpClient.request(request).pipe(
      map((response: HttpResponse<IDataResponseBody<IResourceCollection<ICompany>>>) => response.body.data.items));
    return request$;
  }

  public validateVatId(vatId: string): Observable<boolean> {
    const request = new HttpGet<boolean>(this.getVatIdValidationUrl(vatId));
    return this.http.request(request).pipe(
      map((response: HttpResponse<IDataResponseBody<boolean>>) => response.body.data)
    );
  }

  public fetchRelations = (id: CompanyKey): Observable<CompanyRelations> => {
    return forkJoin([
      this.mandatorRepo.fetchLookups({}),
      this.debitorCreditorRepo.fetchAll({}),
    ]).pipe(
      map(([mandators, debitorCreditors]) => ({
        mandator: mandators.find((m) => m.companyId === id),
        debitorCreditor: debitorCreditors.find((d) => d.companyId === id),
      }))
    );
  }

  public fetchByShareholdingId(id: CompanyLegalEntityKey): Observable<ICompany> {
    const baseUrl = '/api/company-legals';
    const url = cleanUrl(urlConcat([baseUrl, id]));

    const request = new HttpGet(url, {
      entityId: id.toString(),
    });

    const req$ = this.repositoryService.httpClient.request(request).pipe(
      map(
        (responseBody: HttpResponse<IDataResponseBody<ICompanyLegalEntity>>) =>
          responseBody.body.data
      ),
    );

    return req$
      .pipe(switchMap(legalEntity => this.fetchOne(legalEntity.companyId, {})));
  }
}
