import { TemplateRef } from '@angular/core';
export enum MeSweetAlertTypeEnum {
  input = 'input',
  confirm = 'confirm',
  submit = 'submit',
  custom = 'custom'
}

type MeSweetAlertTypeInput = {
  name: MeSweetAlertTypeEnum.input;
  buttons: {
    submit: string;
  };
};
type MeSweetAlertTypeConfirm = {
  name: MeSweetAlertTypeEnum.confirm;
  buttons: {
    confirm: string;
  };
};
type MeSweetAlertTypeSubmit = {
  name: MeSweetAlertTypeEnum.submit;
  buttons: {
    submit: string;
    cancel: string;
  };
};
type MeSweetAlertTypeCustom = {
  name: MeSweetAlertTypeEnum.custom;
  customTemplate: TemplateRef<unknown>;
};
export interface MeSweetAlertI {
  mode: 'primary' | 'success' | 'warning' | 'danger';
  type: MeSweetAlertTypeInput | MeSweetAlertTypeSubmit | MeSweetAlertTypeConfirm | MeSweetAlertTypeCustom;
  icon: string;
  title: string;
  message: string;
  userInput?: string;
  inputLabel?: string;
  confirmed?: boolean;
}
