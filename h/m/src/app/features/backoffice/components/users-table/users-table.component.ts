import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { SubSink } from 'subsink';

import { UserActionsService } from '@features/backoffice/users-groups/user-actions.service';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeAddListPopupService } from '@shared/me-components/me-popup-add-list/me-popup-add-list.service';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { BackOfficeService } from '@features/backoffice/backoffice.web-service';
import { MeAddListCardI } from '@shared/me-components/me-add-list-card/me-add-list-card.interface';
import ColumnOptionsI from '@shared/me-components/me-table/interfaces/column-options.interface';
import DataSourceI from '@shared/me-components/me-table/interfaces/data-source.interface';
import ColumnI from '@shared/me-components/me-table/interfaces/column.interface';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { MeTableCellClickEventI } from '@shared/me-components/me-table';
import { UserModel } from '@shared/models/user/user.model';
import { getOid } from '@shared/utils';
import { MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';

@Component({
  selector: 'me-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnInit {
  @Input() items: DataSourceI[] = [];
  @Input() columns: ColumnI[] = [];
  @Input() columnOptions: ColumnOptionsI[] = [];
  @Input() loader = false;
  @Input() updateTableData!: Subject<void>;

  private subs = new SubSink();
  getCardOid = getOid;
  addGroupListCardItems: MeAddListCardI[] = [];

  _cellClickEventSubject!: Subscription;
  cellClickEventSubject = new Subject<MeTableCellClickEventI>();

  i18nStrings: { [key: string]: string } = {
    statusChange: '',
    statusChangeSuccess: '',
    users: '',
    usersInGroupUpdated: '',
    deleteUser: '',
    areYouSureYouWantToDeleteUser: ''
  };

  @ViewChild('addGroupsList', { read: TemplateRef }) addGroupsList!: TemplateRef<Component>;

  constructor(
    public userActionsService: UserActionsService,
    private basicAlertService: MeBasicAlertService,
    private translate: TranslateService,
    private meAddListPopupService: MeAddListPopupService,
    private meSweetAlertService: MeSweetAlertService,
    private backOfficeService: BackOfficeService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.subs.sink = this.translate.get(Object.keys(this.i18nStrings)).subscribe((translations) => {
      Object.keys(this.i18nStrings).forEach((key) => {
        this.i18nStrings[key] = translations[key];
      });
    });

    this._cellClickEventSubject = this.cellClickEventSubject.subscribe(({ data, rowOID }) => {
      switch (data.type) {
        case 'selectedContextMenu':
          let userActivateOrDeactivateObs!: Observable<UserModel>;

          switch (data.key) {
            case 'activate':
              userActivateOrDeactivateObs = this.userActionsService.activateUser(rowOID);
              break;
            case 'deactivate':
              userActivateOrDeactivateObs = this.userActionsService.deActivateUser(rowOID);
              break;
            case 'manageGroups':
              this.openGroupsListPopUp(rowOID);
              this.userActionsService.prepareAddListGroupCardData(rowOID).then((response) => {
                this.addGroupListCardItems = response;
              });
          }

          if (userActivateOrDeactivateObs) {
            this.subs.sink = userActivateOrDeactivateObs.subscribe(
              () => {
                this.updateTableData.next();
                this.basicAlertService.openBasicAlert({
                  mode: 'success',
                  title: this.i18nStrings.statusChange,
                  content: this.i18nStrings.statusChangeSuccess
                });
              },
              (error: ErrorResponseI) => {
                this.basicAlertService.openBasicAlert({
                  mode: 'error',
                  title: error.error.title,
                  content: error.error.details
                });
              }
            );
          }
          break;
        case 'edit':
          this._router.navigate(['backoffice/user/edit', rowOID]);
          break;
        case 'delete':
          this.deleteUser(rowOID);
          break;
      }
    });
  }

  openGroupsListPopUp(userOID: string) {
    this.subs.sink = this.meAddListPopupService.openPopup({ title: 'Add to Group' }, this.addGroupsList).subscribe((popUpEvent: string) => {
      if (popUpEvent === 'addUsersAndClose') {
        this.userActionsService.removeOrAddUsersFromGroup(userOID).subscribe(() => {
          this.userActionsService.addUserListCardItemsToSend = [];
          this.userActionsService.removeUserListCardItemsToSend = [];

          this.updateTableData.next();
          this.basicAlertService.openBasicAlert({
            mode: 'success',
            title: this.i18nStrings.users,
            content: this.i18nStrings.usersInGroupUpdated
          });
        });
      }
    });
  }

  deleteUser(userOID: string) {
    this.meSweetAlertService.openMeSweetAlert({
      mode: 'danger',
      icon: 'x-octagon',
      type: {
        name: MeSweetAlertTypeEnum.submit,
        buttons: {
          submit: 'Delete',
          cancel: 'Cancel'
        }
      },
      title: this.i18nStrings.deleteUser,
      message: this.i18nStrings.areYouSureYouWantToDeleteUser
    });

    this.subs.sink = this.meSweetAlertService
      .getDataBackFromSweetAlert()
      .pipe(take(1))
      .subscribe((data) => {
        if (data.confirmed) {
          this.subs.sink = this.backOfficeService.deleteBoUser(userOID).subscribe(() => {
            this.updateTableData.next();
          });
        }
      });
  }
}
