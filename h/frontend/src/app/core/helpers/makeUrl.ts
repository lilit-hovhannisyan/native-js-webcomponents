/**
 * Creates a URL from a url string
 * note: firefox needs some kind of host that looks valid
 */
export const makeURL = (url: string, host?: string): URL => new URL(url, host || 'http://foo.bar/');

export const getParentScreenUrl = (url: string) => {
  const parentUrl = url.substr(0, url.lastIndexOf('/'));
  const lastPart = parentUrl.substr(parentUrl.lastIndexOf('/') + 1, parentUrl.length);
  const isNumber = !isNaN(+lastPart);

  if (isNumber) {
    return getParentScreenUrl(parentUrl);
  }

  return parentUrl;
};
