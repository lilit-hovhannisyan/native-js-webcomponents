import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Language } from '../models/language';
import { NvLocale } from '../models/dateFormat';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { DEBIT_CREDIT } from '../models/enums/debitCredit';
import { IAccountConfigGeneral } from '../models/resources/IAccountConfigGeneral';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Theme, ThemeTypes } from '../models/Theme.enum';
import { map } from 'rxjs/operators';
import { IDataResponseBody } from '../models/responses/IDataResponseBody';
import { HttpPost } from '../models/http/http-post';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  public languageSubject: BehaviorSubject<Language> =
    new BehaviorSubject(this.authenticationService.getSessionInfo().user.language || Language.EN);

  public localeSubject: BehaviorSubject<NvLocale> =
    new BehaviorSubject(this.authenticationService.getSessionInfo().user.locale || NvLocale.UK);

  public debitCreditSubject: BehaviorSubject<DEBIT_CREDIT> =
    new BehaviorSubject(this.authenticationService.getSessionInfo().user.debitCredit || DEBIT_CREDIT.plusMinus);

  public themeSubject = new BehaviorSubject<Theme>(this.getInitialTheme());
  public themeNameSubj = new BehaviorSubject<ThemeTypes>(this.getThemeName(this.theme));

  constructor(
    private authenticationService: AuthenticationService,
    private http: HttpClient
  ) { }

  public get language(): Language {
    return this.languageSubject.value;
  }

  public get locale(): NvLocale {
    return this.localeSubject.value;
  }

  public get debitCredit(): DEBIT_CREDIT {
    return this.debitCreditSubject.value;
  }

  public set language(language: Language) {
    this.languageSubject.next(language);
  }

  public set debitCredit(debitCredit: DEBIT_CREDIT) {
    this.debitCreditSubject.next(debitCredit);
  }

  public set locale(locale: NvLocale) {
    this.localeSubject.next(locale);
  }

  public save(config: IAccountConfigGeneral): Observable<IAccountConfigGeneral> {
    const request = new HttpPost('/api/user-account/general/config', config);
    return this.http.request(request).pipe(
      map((res: HttpResponse<IDataResponseBody<IAccountConfigGeneral>>) => res.body.data),
    );
  }

  public set theme(theme: Theme) {
    const themeName: ThemeTypes = this.getThemeName(theme);
    this.setThemeAttr(themeName);
    this.themeSubject.next(theme);
    this.themeNameSubj.next(themeName);
  }

  public get theme() {
    return this.themeSubject.value;
  }

  /**
   * returns real theme 'dark' or 'light' without auto
   */
  private getInitialTheme(): Theme.dark | Theme.light {
    let theme = this.authenticationService.getSessionInfo().user.theme
      || Theme.light;
    if (Theme.auto === theme) {
      theme = window.matchMedia('(prefers-color-scheme: dark)').matches
        ? Theme.dark
        : Theme.light;
    }
    return theme;
  }

  private setThemeAttr = (theme: ThemeTypes) => {
    document.querySelector('html').setAttribute('data-theme', theme);
  }

  private getThemeName(theme: Theme): ThemeTypes {
    let themeName: ThemeTypes;
    if (theme === Theme.auto && window.matchMedia) {
      // Set the theme
      themeName = window.matchMedia('(prefers-color-scheme: dark)').matches
        ? ThemeTypes.dark
        : ThemeTypes.light;
      // add event listner
      window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', e => {
        if (e.matches) {
          themeName = ThemeTypes.dark;
        } else {
          themeName = ThemeTypes.light;
        }
      });
    } else {
      // if theme passed !== auto
      themeName = Theme.dark === theme ? ThemeTypes.dark : ThemeTypes.light;
    }
    return themeName;
  }
}
