import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersGroupsComponent } from './users-groups.component';

import { UsersGroupsRoutingModule } from './users-groups-routing.module';
import { MeSearchBarModule } from '@shared/me-components/me-search-bar/me-search-bar.module';
import { MeScrollToBottomModule } from '@shared/me-directives/me-scroll-to-bottom.module';
import { UsersTableModule } from '../components/users-table/users-table.module';
import { GroupsTableModule } from '../components/groups-table/groups-table.module';
import { MeIconsModule } from '@shared/me-components/me-icons/me-icons.module';

@NgModule({
  declarations: [UsersGroupsComponent],
  imports: [
    CommonModule,
    UsersGroupsRoutingModule,
    MeSearchBarModule,
    MeScrollToBottomModule,
    MeIconsModule,
    UsersTableModule,
    GroupsTableModule
  ]
})
export class UsersGroupsModule {}
