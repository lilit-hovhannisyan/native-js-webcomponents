import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { CompanyGroupKey, ICompanyGroup, ICompanyGroupCreate, ICompanyGroupDTO } from '../../models/resources/ICompanyGroup';

@Injectable({
  providedIn: 'root'
})
export class CompanyGroupRepositoryService extends AbstractRepository<
ICompanyGroup,
ICompanyGroupDTO,
ICompanyGroupCreate,
RepoParams<ICompanyGroup>,
CompanyGroupKey
>  {
  protected resourceType = 'GroupOfCompany';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: ICompanyGroupDTO): ICompanyGroup => item;

  public getBaseUrl = (): string => '/api/group-of-companies/';
}
