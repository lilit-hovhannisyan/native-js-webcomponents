import {BaseComponent} from '../../core/base-component/base.component.js';
import {
  externalStyleBasePath,
  canElementBeRendered,
} from '../../helpers/random.helpers.js';


export class InputComponent extends BaseComponent {
  constructor() {
    super(externalStyleBasePath + 'input-component/input.component.css');
  }

  render = () => {
    this.icon = this.getAttribute('icon') || '';
    this.type = this.getAttribute('type') || 'text';
    this.value = this.getAttribute('value') || '';
    this.placeholder = this.getAttribute('placeholder') || '';

    const inputConfig = {
      tagName: 'div',
      attr: {
        class: 'l-input',
      },
      subElements: [
        {
          tagName: 'icon-component',
          attr: {
            icon: this.icon,
            render: canElementBeRendered(this.icon)
          }
        },
        {
          tagName: 'input',
          attr: {
            type: this.type,
            value: this.value,
            placeholder: this.placeholder
          },
        }
      ],
    };

    const inputEl = this.createElementWithAttributes(inputConfig);

    this.removeStaticAtributesFromElNotToRender(this, ['icon', 'type', 'value', 'placeholder']);
    this.shadow.appendChild(inputEl.cloneNode(true));
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }

  onInput = () => {
    console.log('blin')
  }
}