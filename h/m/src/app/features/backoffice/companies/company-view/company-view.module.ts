import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';

import { CompanyViewComponent } from './company-view.component';
import { CompanyViewRoutingModule } from './company-view-routing.module';
import { MeCoverImageUploadModule } from '@shared/me-components/me-cover-image-upload/me-cover-image-upload.module';
import { MeLogoUploadModule } from '@shared/me-components/me-logo-upload/me-logo-upload.module';
import { MeIconsModule } from '@shared/me-components/me-icons/me-icons.module';
import { MeLabelModule } from '@shared/me-components/me-label/me-label.module';
import { MeTableModule } from '@shared/me-components/me-table/me-table.module';
import { MeAddListCardModule } from '@shared/me-components/me-add-list-card/me-add-list-card.module';
import { CompanyUsersComponent } from '../company-users/company-users.component';
import { CompanyGroupsComponent } from '../company-groups/company-groups.component';
import { MeSearchInputModule } from '@shared/me-components/me-search-input/me-search-input.module';
import { MeScrollToBottomModule } from '@shared/me-directives/me-scroll-to-bottom.module';
import { IconsModule } from '@shared/modules/icons.module';
import { UsersTableModule } from '@features/backoffice/components/users-table/users-table.module';
import { GroupsTableModule } from '@features/backoffice/components/groups-table/groups-table.module';

@NgModule({
  declarations: [CompanyViewComponent, CompanyUsersComponent, CompanyGroupsComponent],
  imports: [
    CommonModule,
    CompanyViewRoutingModule,
    MatTabsModule,
    MatButtonModule,
    IconsModule,
    MeCoverImageUploadModule,
    MeLogoUploadModule,
    MeIconsModule,
    MeLabelModule,
    MeTableModule,
    MeAddListCardModule,
    MeSearchInputModule,
    MeScrollToBottomModule,
    UsersTableModule,
    GroupsTableModule
  ]
})
export class CompanyViewModule {}
