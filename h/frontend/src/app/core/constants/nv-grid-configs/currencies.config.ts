import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (operation: Operations) => boolean,
  isFavoritesGrid = false,
): NvGridConfig => {

  return ({
    gridName: isFavoritesGrid ? 'favoriteCurrencyListGrid' : 'currencyListGrid',
    title: isFavoritesGrid ? wording.system.favorites : wording.system.allCurrencies,
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    showPaging: !isFavoritesGrid,
    sortBy: 'isoCode',
    isSortAscending: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 50
    },
    columns: [
      {
        key: 'isoCode',
        title: wording.accounting.isoCode,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
        }
      },
      {
        key: 'displayLabel',
        title: wording.general.label,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
        }
      },
      {
        key: 'issuingDate',
        title: wording.accounting.lastRateFrom,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.Date,
        filter: {
          values: [],
        },
        // TODO: Change this in future to a better solution
        hidden: !canPerform,
      },
      {
        key: 'rate',
        title: 'EUR / ISO',
        width: 150,
        dataType: NvColumnDataType.Decimal,
        isSortable: true,
        filter: {
          values: [],
        },
        decimalDigitsInfo: '1.5-5',
        hidden: !canPerform,
      },
      {
        key: 'rateUSD',
        title: 'USD / ISO',
        width: 150,
        dataType: NvColumnDataType.Decimal,
        isSortable: true,
        filter: {
          values: [],
        },
        decimalDigitsInfo: '1.5-5',
        hidden: !canPerform,
      },
      {
        key: 'isActive',
        title: wording.accounting.active,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: ['1'],
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isFavorite',
        title: wording.general.favorite,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: [],
        },
        dataType: NvColumnDataType.Boolean,
        hidden: true, // this column could be made visible if needed later on
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: wording.general.edit,
        tooltip: wording.general.edit,
        actOnEnter: !!canPerform,
        actOnDoubleClick: !!canPerform,
        func: (row) => actions.edit(row.id),
        hidden: canPerform ? !canPerform(Operations.UPDATE) : true,
      },
      {
        actOnDoubleClick: true,
        func: row => actions.select(row),
        description: wording.autocomplete.selectRow,
        icon: 'select',
        actOnEnter: true,
        hidden: actions.select === undefined,
        tooltip: wording.general.select,
        name: 'select'
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        func: actions.add,
        hidden: () => canPerform ? !canPerform(Operations.CREATE) : true,
      }
    ],
    contextMenuButtons: canPerform && canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
