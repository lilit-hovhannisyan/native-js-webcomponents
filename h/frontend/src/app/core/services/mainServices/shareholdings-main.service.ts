import { Injectable } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { CurrencyRepositoryService } from '../repositories/currency-repository.service';
import { CompanyLegalEntityRepositoryService } from '../repositories/company-legal-entity-repository.service';
import { ICompanyLegalEntity } from '../../models/resources/ICompanyLegalEntity';
import { ICurrency } from '../../models/resources/ICurrency';
import { CompanyRepositoryService } from '../repositories/company-repository.service';
import { CompanyKey, ICompany } from '../../models/resources/ICompany';
import { COMPANY_LEGAL_FORM } from '../../models/enums/company-legal-form';
import { ContributionMainService } from './company-contributions-main.service';
import { ICompanyPartnerContributionFull } from '../../models/resources/ICompanyPartnerContribution';

export interface IShareholding extends ICompany {
  shareCapitalOption: number;
  currency: string;
  contributions: ICompanyPartnerContributionFull[];
  businessName: string;
  legalEntity?: ICompanyLegalEntity;
  isDeleted?: boolean;
  isSold?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ShareholdingsMainService {
  constructor(
    private companyLegalEntityRepo: CompanyLegalEntityRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private contributionMainService: ContributionMainService,
  ) { }

  public fetchAllWithLegalDepartmentReposible(): Observable<IShareholding[]> {
    return forkJoin([
      this.companyLegalEntityRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.contributionMainService.fetchAll(),
    ]).pipe(
      map(res => this.getShareholdings(...res)),
      map(shareholdings => shareholdings.filter(s => s.isLegalDepartmentResponsible))
    );
  }

  public getStream(): Observable<IShareholding[]> {
    return forkJoin([
      this.companyLegalEntityRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.contributionMainService.fetchAll(),
    ]).pipe(
      switchMap(() => combineLatest([
        this.companyLegalEntityRepo.getStream(),
        this.currencyRepo.getStream(),
        this.companyRepo.getStream(),
        this.contributionMainService.getStream(),
      ])),
      map(res => this.getShareholdings(...res)),
      map(shareholdings => shareholdings.filter(s => s.isLegalDepartmentResponsible))
    );
  }

  private getShareholdings(
    legalEntities: ICompanyLegalEntity[],
    currencies: ICurrency[],
    companies: ICompany[],
    allContributions: ICompanyPartnerContributionFull[],
  ): IShareholding[] {
    return companies.map(company => {
      const legalEntity: ICompanyLegalEntity = legalEntities
        .find(l => company.id === l.companyId);
      const currency: ICurrency = currencies.find(c => c.id === legalEntity?.currencyId);
      const contributions: ICompanyPartnerContributionFull[] = allContributions
        .filter(c => c.companyId === company.id);

      const currencyISOCode: string = company.legalForm === COMPANY_LEGAL_FORM.CompanyWithLimitedLiability
        || company.legalForm === COMPANY_LEGAL_FORM.StockCorporation
        ? 'EUR'
        : currency?.isoCode;

      return {
        ...company,
        shareCapitalOption: legalEntity?.shareCapital || legalEntity?.sumOfContributions,
        currency: currencyISOCode,
        contributions,
        businessName: company.companyName,
        legalEntity,
        isDeleted: legalEntity ? legalEntity.isDeleted : false,
        isSold: legalEntity ? legalEntity.isSold : false,
      };
    });
  }

  public deactivateCompanyResponsibleForLegalData(company: ICompany): void {
    company.isLegalDepartmentResponsible = false;
    this.companyRepo.update(company, {}).subscribe();
  }

  public getShareholdingById(shareholdingId: CompanyKey): Observable<IShareholding> {
    return this.getStream().pipe(
      map(shareholdings => shareholdings.find(s => s.id === shareholdingId))
    );
  }
}
