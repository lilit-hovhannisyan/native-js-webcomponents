import { Injectable } from '@angular/core';
import { IShipyard, IShipyardDTO, IShipyardCreate, ShipyardKey } from '../../models/resources/IShipyard';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class ShipyardRepositoryService extends AbstractRepository<
  IShipyard,
  IShipyardDTO,
  IShipyardCreate,
  RepoParams<IShipyard>,
  ShipyardKey
>  {
  protected resourceType = 'Shipyard';
  protected sortKey = 'label';
  protected sortAsce = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/shipyards';

  protected postProcess = (item: IShipyardDTO): IShipyard => item;
}
