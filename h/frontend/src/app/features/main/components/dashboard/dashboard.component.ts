import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { GridsterConfig, GridsterComponent } from 'angular-gridster2';
import { options } from './dashboard-options';
import { widgetsList } from './widgets-list';
import { Observable } from 'rxjs';
import { IWidget } from 'src/app/core/models/widgets/IWidget';
import { AbstractWidgetsService } from '../../../examples/services/widgets/abstract-widgets-repository';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { NotificationType } from 'src/app/core/models/INotification';
import { wording } from 'src/app/core/constants/wording/wording';
import { WidgetType } from 'src/app/core/models/widgets/WidgetType';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardComponent implements OnInit {
  @ViewChild('grid', { static: true }) public grid: GridsterComponent;
  public options: GridsterConfig;
  public dashboardWidgetItems: Observable<IWidget[]>;
  public gridEditEnabled: boolean;
  public widgetsList = widgetsList;

  public ngOnInit() {
    this.initDashboard();
  }

  constructor(
    private widgetsService: AbstractWidgetsService,
    private notificationService: NotificationsService,
  ) { }

  private initDashboard() {
    this.dashboardWidgetItems = this.widgetsService.getDashboardWidgets();
    this.options = options;
    this.widgetsService.fetchAll({})
      .subscribe((widgets: IWidget[]) => this.widgetsService.setDashboardWidgets(widgets));
  }

  public addWidget(type: WidgetType) {
    const newItem = this.options.api.getFirstPossiblePosition({
      cols: 1,
      rows: 1,
      y: 0,
      x: 0
    });
    if (this.grid.checkCollision(newItem)) {
      this.notificationService.notify(
        NotificationType.Warning,
        wording.general.warning,
        wording.general.cantAddMore
      );
    } else {
      const newWidget: IWidget = {
        cols: 1,
        rows: 1,
        y: newItem.y,
        x: newItem.x,
        type,
        _type: 'Widget', // just needed here to satisfy interface
        id: 0
      };
      this.widgetsService.addWidget(newWidget).subscribe();
    }
  }

  public toggleGrid() {
    this.gridEditEnabled = !this.gridEditEnabled;
    this.options.resizable.enabled = !this.options.resizable.enabled;
    this.options.draggable.enabled = !this.options.draggable.enabled;
    this.options.api.optionsChanged();
    if (!this.gridEditEnabled) {
      this.widgetsService.saveWidgets().subscribe();
    }
  }

  public onWidgetDelete(id: number, index: number) {
    this.widgetsService.deleteWidget(id, index)
      .subscribe(() => (<any>document.querySelector('gridster-preview')).style.display = 'none');
  }
}

