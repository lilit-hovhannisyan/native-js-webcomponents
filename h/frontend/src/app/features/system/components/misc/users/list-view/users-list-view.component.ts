import { Component, OnInit } from '@angular/core';
import { IUser } from '../../../../../../core/models/resources/IUser';
import { NvGridConfig } from 'nv-grid';
import { GridConfigService } from '../../../../../../core/services/grid-config.service';
import { UserRepositoryService } from '../../../../../../core/services/repositories/user-repository.service';
import { Observable } from 'rxjs';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/users.config';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { AccountService } from 'src/app/core/services/account.service';

@Component({
  templateUrl: './users-list-view.component.html',
  styleUrls: ['./users-list-view.component.scss']
})
export class UserListViewComponent implements OnInit {
  public dataSource$: Observable<IUser[]>;
  public userGridConfig: NvGridConfig;

  constructor(
    public gridConfigService: GridConfigService,
    private userRepositoryService: UserRepositoryService,
    private confirmationService: ConfirmationService,
    private authService: AuthenticationService,
    private router: Router,
    private accountService: AccountService
  ) { }

  public ngOnInit(): void {
    this.userRepositoryService.fetchAll({}).subscribe();
    this.dataSource$ = this.userRepositoryService.getStream();

    const actions: IGridConfigActions = {
      add: () => this.router.navigate([`${this.router.url}/new`]),
      edit: (user: IUser) => this.router.navigate([`${this.router.url}/${user.id}`]),
      delete: (user: IUser) => {
        this.confirmationService.confirm({
          okButtonWording: wording.general.delete,
          cancelButtonWording: wording.general.cancel,
          wording: wording.general.areYouSureYouWantToDelete,
          subjects: { subject: user.username },
        }).subscribe(confirmed => confirmed && this.userRepositoryService.delete(user.id, {}).subscribe());
      },
      request: (user: IUser) => this.accountService.resetPassword(user)
    };

    this.userGridConfig = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }
}
