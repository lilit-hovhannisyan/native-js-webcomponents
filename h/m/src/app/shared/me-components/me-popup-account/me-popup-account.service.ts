import { MePopupAccountComponent } from './me-popup-account.component';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { MePopupAccountI } from './me-popup-account.interface';
import { takeUntil } from 'rxjs/operators';

@Injectable()
export class MePopupAccountService {
  private _takeUntilObservable$: Subject<void> = new Subject<void>();
  private _currentInstance: MePopupAccountComponent | undefined;

  constructor(private dialog: MatDialog) {}

  openPopupSelect(dataModel: MePopupAccountI): Subject<string> {
    const subjectToReturn = new Subject<string>();
    const dialogRef = this.dialog.open(MePopupAccountComponent, {
      data: { dataModel },
      panelClass: 'popup-account-container',
      backdropClass: 'popup-account-backdrop',
      disableClose: true
    });
    this._currentInstance = dialogRef.componentInstance;
    this._currentInstance.popupSelectEvent
      .pipe(takeUntil(this._takeUntilObservable$))
      .subscribe((event: { eventName: string; items?: [] }) => {
        if (event.eventName === 'close') {
          dialogRef.close();
        } else if (event.eventName === 'next') {
          subjectToReturn.next(event.eventName);
          dialogRef.close();
        } else {
          subjectToReturn.next(event.eventName);
        }
      });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this._takeUntilObservable$))
      .subscribe(() => {
        this._currentInstance = undefined;
        subjectToReturn.next('close');
        this._takeUntilObservable$.next();
        this._takeUntilObservable$.complete();
        subjectToReturn.complete();
      });

    return subjectToReturn;
  }

  pushDataModelToPopup(data: MePopupAccountI): void {
    if (this._currentInstance) {
      this._currentInstance.dataModel = data;
    }
  }

  closePopup(): void {
    if (this._currentInstance) {
      this._currentInstance.customEvent('close');
    }
  }
}
