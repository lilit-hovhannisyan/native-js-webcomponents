import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { HttpGet } from '../../models/http/http-get';
import { HttpPut } from '../../models/http/http-put';
import { IResourceCollection } from '../../models/resources/IResourceCollection';
import { IVessel, IVesselCreate, IVesselDTO, VesselKey } from '../../models/resources/IVessel';
import { IVesselAccountingDTO } from '../../models/resources/IVesselAccounting';
import { IVesselPreDelayCharterName } from '../../models/resources/IVesselPreDelayCharterName';
import { IVesselSpecificationDTO } from '../../models/resources/IVesselSpecification';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';
import { RepositoryService } from '../repository.service';
import { IVesselRegistrationMortgage } from '../../models/resources/IVesselRegistrationMortgage';
import { HttpDelete } from '../../models/http/http-delete';

export interface IVesselRepoParams extends RepoParams<IVessel> { }
declare type IPreDeliveryResource = HttpResponse<IDataResponseBody<IVesselPreDelayCharterName>>;
declare type IVesselRegistrationMortgageResource = HttpResponse<IDataResponseBody<IVesselRegistrationMortgage>>;

@Injectable({
  providedIn: 'root'
})
export class VesselRepositoryService extends AbstractRepository<
IVessel,
IVesselDTO,
IVesselCreate,
IVesselRepoParams,
VesselKey
> {
  protected resourceType = 'Vessel';
  private readonly vesselRegistriesUrlSlug = 'registries';
  public sortAsce = true;
  public sortKey = 'imo';
  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/vessels';
  }

  protected postProcess(item: IVesselDTO): IVessel { return item; }

  public fetchSpecificationsByVesselId(vesselId: number) {
    const url = `${this.getBaseUrl()}/${vesselId}/specifications`;
    const req = new HttpGet(url);
    return this.repositoryService.httpClient.request(req)
      .pipe(
        map((res: HttpResponse<IDataResponseBody<IResourceCollection<IVesselSpecificationDTO>>>) =>
          res.body.data.items));
  }

  public fetchAccountingsByVesselId(vesselId: number) {
    const url = `${this.getBaseUrl()}/${vesselId}/accountings`;
    const req = new HttpGet(url);
    return this.repositoryService.httpClient.request(req)
      .pipe(
        map((res: HttpResponse<IDataResponseBody<IResourceCollection<IVesselAccountingDTO>>>) =>
          res.body.data.items));
  }

  public fetchPreDeliveryChartername(vesselId: VesselKey): Observable<IVesselPreDelayCharterName> {
    const url = `${this.getBaseUrl()}/${vesselId}/chartername`;

    return this.repositoryService.httpClient.request(new HttpGet(url)).pipe(
      map((result: IPreDeliveryResource) => result.body.data),
    );
  }

  public updatePreDeliveryChartername(
    vesselId: VesselKey,
    body: IVesselPreDelayCharterName,
  ): Observable<IVesselPreDelayCharterName> {
    const url = `${this.getBaseUrl()}/${vesselId}/chartername`;
    const request = new HttpPut(url, body);

    return this.repositoryService.httpClient.request(request).pipe(
      map((result: IPreDeliveryResource) => result.body.data),
    );
  }

  public deleteRegistrationSetting(vesselId: VesselKey): Observable<void> {
    const url = `${this.getBaseUrl()}/${vesselId}/registration-setting`;
    const request = new HttpDelete(url);

    return this.repositoryService.httpClient.request(request)
      .pipe(map(() => void(0)));
  }
}
