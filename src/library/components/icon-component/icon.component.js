import {BaseComponent} from '../../core/base-component/base.component.js';
import {icomoonIcons} from './icomoon-icons.js';
import {
  externalStyleBasePath,
  createElementWithAttributes,
  canElementBeRendered,
  removeStaticAtributesFromElNotToRender,
  setInlineStylesForComponent,
} from '../../helpers/random.helpers.js';




export class IconComponent extends BaseComponent {
  constructor() {
    super(externalStyleBasePath + 'icon-component/icon-component.css');
  }

  setIconsStyleSheet(iconName) {
    const iconStyles = setInlineStylesForComponent(iconName);
    this.shadow.appendChild(iconStyles);
  }

  render = () => {
    const iconClassName = this.getAttribute('icon');

    if (!iconClassName) {
      throw error();
    }

    const iconConfig = {
      tagName: 'i',
      attr: {
        class: 'l-icon icon ' + iconClassName,
        render: canElementBeRendered(iconClassName)
      },
    };

    const iconEl = createElementWithAttributes(iconConfig);
    this.shadow.appendChild(iconEl);
  }

  connectedCallback() {
    if (this.getAttribute('render') !== 'false' ) {
      this.setIconsStyleSheet(icomoonIcons);
    }
    super.connectedCallback(this.render);
  }
}