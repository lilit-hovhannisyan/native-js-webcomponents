import { version, build_number, copyright_year } from '../../package.json';

export const environment = {
  production: false,
  VERSION: version,
  BUILD_NUMBER: build_number,
  COPYRIGHT_YEAR: copyright_year,
  BASE_API_URL: 'http://mate-test-business.materialexchange.se/webapi/v1',
  TRACKING_BASE_API_URL: 'http://tracker-dev.materialexchange.se/mate-tracker/api/v1.0'
};
