import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { IBookingCode } from 'src/app/core/models/resources/IBookingCode';
import {
  charteringExpenseTypeOptions,
  CharteringExpenseType,
  ICharteringExpense,
  ICharteringExpenseCreate,
} from 'src/app/core/models/resources/ICharteringExpense';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { ExpensesRepositoryService } from 'src/app/core/services/repositories/expenses-repository.service';
import { ExpensesHelperService } from '../expenses.helper.service';

@Component({
  templateUrl: './expenses-basic.component.html',
  styleUrls: ['./expenses-basic.component.scss'],
})
export class ExpensesBasicComponent extends ResourceDetailView<ICharteringExpense, ICharteringExpenseCreate> implements OnInit {
  protected repoParams = {};
  protected resourceDefault = { isActive: true } as ICharteringExpense;
  public routeBackUrl = this.url(sitemap.chartering.children.misc.children.expenses);
  public expensesTypes = charteringExpenseTypeOptions;
  public bookingCodes: IBookingCode[];
  public bookingAccounts: IBookingAccount[];
  public formElement: ElementRef;

  constructor(
    private bookingAccountRepo: BookingAccountRepositoryService,
    private bookingCodeRepo: BookingCodeRepositoryService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private expensesHelperService: ExpensesHelperService,
    expensesRepo: ExpensesRepositoryService,
    baseComponentService: BaseComponentService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef
  ) {
    super(expensesRepo, baseComponentService);
  }

  public ngOnInit() {
    const { id } = this.route.parent.snapshot.params;
    this.bookingAccountRepo.fetchAll({}).subscribe(allBookingAccounts => {
      this.bookingAccounts = allBookingAccounts;
    });
    this.bookingCodeRepo.fetchAll({}).subscribe(allBookingCodes => {
      this.bookingCodes = allBookingCodes;
    });

    if (id === 'new') {
      this.enterCreationMode();
    } else {
      this.loadResource(+id);
    }
  }

  public init(expense: ICharteringExpense = this.resourceDefault): void {
    this.initForm(this.fb.group({
      shortName: [expense.shortName, [Validators.required, Validators.maxLength(50)]],
      labelEn: [expense.labelEn, [Validators.required, Validators.maxLength(100)]],
      labelDe: [expense.labelDe, [Validators.required, Validators.maxLength(100)]],
      type: [expense.type || CharteringExpenseType.Lumpsum],
      isDefaultForTcVoyages: [expense.isDefaultForTcVoyages],
      bookingAccountId: [expense.bookingAccountId, Validators.required],
      costType: [expense.costType, Validators.maxLength(50)],
      bookingCodeId: [expense.bookingCodeId, Validators.required],
      isActive: [expense.isActive],
    }));

    if (!this.isAdmin) {
      this.disableField('isActive');
    }
  }

  public submit(): void {
    if (!this.form.valid) {
      return;
    }

    this.creationMode
      ? this.createResource(this.form.value)
        .subscribe(createdResource => {
          this.expensesHelperService.generateTitle(createdResource);
        })
      : this.updateResource({ ...this.resource, ...this.form.value })
        .subscribe(updatedResource => {
          this.expensesHelperService.generateTitle(updatedResource);
        });

  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

  public getTitle = () => this.resource.shortName;
}
