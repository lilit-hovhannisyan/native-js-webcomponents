import { Component, Input } from '@angular/core';
import {RouteManagementService} from 'l-builder';

@Component ({
  selector: 'app-logo-component',
  templateUrl : './logo.component.html',
  styleUrls : ['./logo.component.less']

})

export class LogoComponent {
  @Input() lense: false;

  constructor(private routeManagementService: RouteManagementService) {}

  goToMainPage() {
      this.routeManagementService.navigate('/');
  }
}

