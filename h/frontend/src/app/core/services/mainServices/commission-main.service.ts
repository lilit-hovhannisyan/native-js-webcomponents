import { Injectable } from '@angular/core';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { IVesselCommissionFull, IVesselCommission, IVesselCommissionRow, BILLING_CYCLE_TYPES, PROCESSING_TYPES } from 'src/app/core/models/resources/IVesselCommission';
import { map, switchMap } from 'rxjs/operators';
import { CommissionRepositoryService } from '../repositories/commission-repository.service';
import { sortBy } from 'lodash';
import { BookingAccountRepositoryService } from '../repositories/booking-account-repository.service';
import { CommissionTypeRepositoryService } from '../repositories/commission-type-repository.service';
import { CurrencyRepositoryService } from '../repositories/currency-repository.service';
import { MandatorRepositoryService } from '../repositories/mandator-repository.service';
import { TaxKeyRepositoryService } from '../repositories/tax-key-repository.service';
import { ICommissionType } from '../../models/resources/ICommissionType';
import { IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { IDebitorCreditor } from '../../models/resources/IDebitorCreditor';
import { DebitorCreditorRepositoryService } from '../repositories/debitor-creditor-repository.service';
import { ITaxKey } from '../../models/resources/ITaxKeys';
import { ICurrency } from '../../models/resources/ICurrency';

/**
 * This type is needed because when combineLatest or forkJoin operators have more than
 * 6 arguments they throw type error, so we need to provide types of response callback's
 * arguments to avoid it, generic type is added to give us opportunity to use these types
 * with IVesselCommission or IVesselCommission[] types.
 */
type IEntitiesList<T> = [
  T,
  ICommissionType[],
  ICurrency[],
  IMandatorLookup[],
  IBookingAccount[],
  ITaxKey[],
  IDebitorCreditor[]
];

@Injectable({
  providedIn: 'root'
})
export class CommissionMainService {
  constructor(
    private commissionRepo: CommissionRepositoryService,
    private commissionTypeRepo: CommissionTypeRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private mandatorRepo: MandatorRepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    private taxKeyRepo: TaxKeyRepositoryService,
    private debitorCreditorRepo: DebitorCreditorRepositoryService
  ) { }

  public fetchAll(): Observable<IVesselCommissionFull[]> {
    return combineLatest([
      this.commissionRepo.fetchAll({}),
      this.commissionTypeRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.mandatorRepo.fetchLookups({}),
      this.bookingAccountRepo.fetchAll({}),
      this.taxKeyRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
    ]).pipe(map((res: IEntitiesList<IVesselCommission[]>) =>
      this.getCommissionsFullList(...res))
    );
  }

  public fetchOne(id: number, params = {}): Observable<IVesselCommissionFull> {
    return this.composeCommissionFull(
      this.commissionRepo.fetchOne(id, params),
      this.commissionTypeRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.mandatorRepo.fetchLookups({}),
      this.bookingAccountRepo.fetchAll({}),
      this.taxKeyRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
    );
  }

  private composeCommissionFull(
    commission$: Observable<IVesselCommission>,
    commissionTypes$: Observable<ICommissionType[]>,
    currencies$: Observable<ICurrency[]>,
    mandators$: Observable<IMandatorLookup[]>,
    bookingAccounts$: Observable<IBookingAccount[]>,
    taxKeys$: Observable<ITaxKey[]>,
    debitorCreditors$: Observable<IDebitorCreditor[]>,
  ): Observable<IVesselCommissionFull> {
    return forkJoin([
      commission$,
      commissionTypes$,
      currencies$,
      mandators$,
      bookingAccounts$,
      taxKeys$,
      debitorCreditors$,
    ] as any).pipe(
      map((res: IEntitiesList<IVesselCommission>) => this.getCommissionFull(...res))
    );
  }

  private getCommissionsFullList(
    commissions: IVesselCommission[],
    commissionTypes: ICommissionType[],
    currencies: ICurrency[],
    mandators: IMandatorLookup[],
    bookingAccounts: IBookingAccount[],
    taxKeys: ITaxKey[],
    debitorCreditors: IDebitorCreditor[],
  ): IVesselCommissionFull[] {
    return commissions.map(c => {
      return this.getCommissionFull(
        c,
        commissionTypes,
        currencies,
        mandators,
        bookingAccounts,
        taxKeys,
        debitorCreditors,
      );
    });
  }

  private getCommissionFull(
    commission: IVesselCommission,
    commissionTypes: ICommissionType[],
    currencies: ICurrency[],
    mandators: IMandatorLookup[],
    bookingAccounts: IBookingAccount[],
    taxKeys: ITaxKey[],
    debitorCreditors: IDebitorCreditor[],
  ): IVesselCommissionFull {
    const commissionType = commissionTypes.find(c => c.id === commission.commissionTypeId);
    const currency = currencies.find(c => c.id === commission.currencyId);
    const mandator = mandators.find(m => m.id === commission.mandatorId);
    const bookingAccount = bookingAccounts.find(b => b.id === commission.bookingAccountId);
    const taxKey = taxKeys.find(t => t.id === commission.taxKeyId);
    const invoiceRecipient = debitorCreditors.find(d => d.id === commission.invoiceRecipientId);
    return {
      ...commission,
      commissionType,
      currency,
      mandator,
      bookingAccount,
      taxKey,
      invoiceRecipient,
    };
  }

  public getStream(): Observable<IVesselCommissionFull[]> {
    return forkJoin([
      this.commissionRepo.fetchAll({}),
      this.commissionTypeRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.mandatorRepo.fetchLookups({}),
      this.bookingAccountRepo.fetchAll({}),
      this.taxKeyRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.commissionRepo.getStream(),
        this.commissionTypeRepo.getStream(),
        this.currencyRepo.getStream(),
        this.mandatorRepo.getLookupStream(),
        this.bookingAccountRepo.getStream(),
        this.taxKeyRepo.getStream(),
        this.debitorCreditorRepo.getStream(),
      ])),
      map((res: IEntitiesList<IVesselCommission[]>) => this.getCommissionsFullList(...res))
    );
  }

  public toRow(commission: IVesselCommissionFull): IVesselCommissionRow {
    const commissionTypeLabel = commission.commissionType.displayLabel;
    const currencyIsoCode = commission.currency ? commission.currency.isoCode : '';
    const mandatorNo = commission.mandator ? commission.mandator.no.toString() : '';
    const bookingAccountNo = commission.bookingAccount ? commission.bookingAccount.no.toString() : '';
    const invoiceRecipientNo = commission.invoiceRecipient ? commission.invoiceRecipient.no.toString() : '';
    const taxKeyCode = commission.taxKey ? commission.taxKey.code : '';
    const billingCycleTypeName = BILLING_CYCLE_TYPES[commission.billingCycleType];
    const processingName = PROCESSING_TYPES[commission.processing];

    return ({
      ...commission,
      commissionTypeLabel,
      currencyIsoCode,
      mandatorNo,
      bookingAccountNo,
      invoiceRecipientNo,
      taxKeyCode,
      billingCycleTypeName,
      processingName,
    });
  }

  public toRows(commissions: IVesselCommissionFull[]): IVesselCommissionRow[] {
    return sortBy(commissions.map(c => this.toRow(c)), ['id']);
  }
}
