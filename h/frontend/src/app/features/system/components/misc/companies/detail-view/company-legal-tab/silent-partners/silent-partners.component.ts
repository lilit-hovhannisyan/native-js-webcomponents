import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Input } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { NvGridConfig } from 'nv-grid';
import { getLegalEntitySilentPartnersGridConfig } from 'src/app/core/constants/nv-grid-configs/legal-entity-silent-partners.config';
import { Operations } from 'src/app/core/models/Operations';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { SilentPartnerRepositoryService } from 'src/app/core/services/repositories/silent-partner-repository.service';
import { ISilentPartnerRepresentative, ISilentPartnerRaw } from 'src/app/core/models/resources/ISilentPartnerRepresentative';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { SettingsService } from 'src/app/core/services/settings.service';
import { SilentPartnersModalComponent } from './silent-partners-modal/silent-partners-modal.component';
import { CompanyKey } from 'src/app/core/models/resources/ICompany';
import { ActivatedRoute } from '@angular/router';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { SilentPartnerMainService } from 'src/app/core/services/mainServices/silent-partners-main.service';

@Component({
  selector: 'nv-silent-partners',
  templateUrl: './silent-partners.component.html',
  styleUrls: ['./silent-partners.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SilentPartnersComponent implements OnInit {
  @Input() public companyId: CompanyKey;

  public wording = wording;
  public gridConfig: NvGridConfig;
  public silentPartnersList: ISilentPartnerRaw[] = [];

  constructor(
    private authService: AuthenticationService,
    private silentPartnersRepo: SilentPartnerRepositoryService,
    private modalService: NzModalService,
    private settingsService: SettingsService,
    private route: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef,
    private confirmationService: ConfirmationService,
    private silentPartnerMainService: SilentPartnerMainService,
  ) { }

  public ngOnInit(): void {
    this.companyId = this.companyId || +this.route.parent.parent.snapshot.params.id;
    this.setGridConfig();
    this.setGridData();
  }

  private setGridData(): void {
    this.silentPartnerMainService.getStream(this.companyId)
      .subscribe((silentPartners: ISilentPartnerRaw[]) => {
        this.silentPartnersList = silentPartners;
        this.changeDetectorRef.detectChanges();
      });
  }

  private setGridConfig(): void {
    this.gridConfig = getLegalEntitySilentPartnersGridConfig(
      {
        add: () => this.openSilentPartnersModal(),
        edit: (row: ISilentPartnerRaw) => this.openSilentPartnersModal(row),
        delete: (row: ISilentPartnerRaw) => this.delete(row),
      },
      (operation: Operations) => this.authService.canPerformOperationOnCurrentUrl(operation),
      this.settingsService,
    );
  }

  private openSilentPartnersModal(silentPartner?: ISilentPartnerRepresentative): void {
    this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.system.silentPartner[this.settingsService.language],
      nzContent: SilentPartnersModalComponent,
      nzComponentParams: {
        silentPartner,
        companyId: this.companyId,
      },
      nzWidth: 600,
    });
  }

  private delete(silentPartner: ISilentPartnerRepresentative): void {
    this.confirmationService.confirm({
      title: wording.system.areYouSureYouWantToDeleteSilentPartner,
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
    }).subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      this.silentPartnersRepo.delete(silentPartner.id, {}).subscribe();
    });
  }
}
