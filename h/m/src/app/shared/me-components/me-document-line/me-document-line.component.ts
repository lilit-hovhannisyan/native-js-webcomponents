import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnDestroy, Output } from '@angular/core';

import { BaseLoggerComponent } from '@features/tracking-system';

import { MeDocumentLineI } from './me-document-line.interface';

@Component({
  selector: 'me-document-line',
  templateUrl: './me-document-line.component.html',
  styleUrls: ['./me-document-line.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeDocumentLineComponent extends BaseLoggerComponent implements OnDestroy {
  componentVersion: string = '0.0.1';

  @Input() dataModel!: MeDocumentLineI;
  @Output() eventOccurs: EventEmitter<{ eventName: string; payload?: string }> = new EventEmitter();

  constructor(injector: Injector) {
    super(injector);
  }

  customEvent(eventName: string, payload?: string): void {
    this.eventOccurs.emit({ eventName, payload });
  }

  ngOnDestroy(): object {
    return super.ngOnDestroy();
  }
}
