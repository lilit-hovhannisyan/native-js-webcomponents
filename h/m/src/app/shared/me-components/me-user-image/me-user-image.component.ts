import { Component, Input, OnInit } from '@angular/core';

import { MeUserImageI } from './me-user-image.interface';

@Component({
  selector: 'me-user-image',
  templateUrl: './me-user-image.component.html',
  styleUrls: ['./me-user-image.component.scss']
})
export class MeUserImageComponent implements OnInit {
  @Input() imageModel: MeUserImageI = {
    type: 'placeholder',
    size: 'medium',
    shape: 'rounded',
    initials: '',
    imageUrl: '',
    alt: ''
  };
  randomColor = ['primary', 'success', 'danger', 'warning', 'info'];
  randomItem = 0;

  constructor() {}

  ngOnInit(): void {
    if (this.imageModel.initials) {
      const charCode = this.imageModel.initials.charCodeAt(0);
      this.randomItem = charCode % this.randomColor.length;
    } else {
      this.randomItem = Math.floor(Math.random() * this.randomColor.length);
    }
  }
}
