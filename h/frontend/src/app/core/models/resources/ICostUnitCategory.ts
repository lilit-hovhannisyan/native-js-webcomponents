import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';

export type CostUnitCategoryKey = number;

interface ISharedProps extends IWithLabels {
  abbreviation: string;
}

export interface ICostUnitCategoryCreate extends ISharedProps { }

export interface ICostUnitCategoryBase
  extends ISharedProps, IResource<CostUnitCategoryKey> { }

export interface ICostUnitCategoryDTO extends ICostUnitCategoryBase { }

export interface ICostUnitCategory extends ICostUnitCategoryDTO, IWithLabel { }
