import { ACCOUNTING_RELATIONS } from 'src/app/core/models/enums/accounting-relations';
import { DEBITOR_CREDITOR } from 'src/app/core/models/enums/debitor-creditor';
import { wording } from 'src/app/core/constants/wording/wording';

export const getAccountingRelationTypesOptions = () => [
  {
    displayLabel: wording.accounting.balanceAccount,
    value: ACCOUNTING_RELATIONS.BALANCE
  },
  {
    displayLabel: wording.accounting.profitAndLossEarnings,
    value: ACCOUNTING_RELATIONS.PROFITANDLOSSEARNINGS,
  },
  {
    displayLabel: wording.accounting.profitAndLossExpenses,
    value: ACCOUNTING_RELATIONS.PROFITANDLOSSEXPENSES,
  },
];

export const getCreditorDebitorOptions = () => [
  {
    displayLabel: wording.accounting.creditorAccount,
    value: DEBITOR_CREDITOR.CREDITOR
  },
  {
    displayLabel: wording.accounting.debitorAccount,
    value: DEBITOR_CREDITOR.DEBITOR
  },
];

export const getCreditorDebitorWithNoneOptions = () => [
  {
    displayLabel: wording.general.none,
    value: DEBITOR_CREDITOR.NONE
  },
  {
    displayLabel: wording.accounting.creditorAccount,
    value: DEBITOR_CREDITOR.CREDITOR
  },
  {
    displayLabel: wording.accounting.debitorAccount,
    value: DEBITOR_CREDITOR.DEBITOR
  },
];
