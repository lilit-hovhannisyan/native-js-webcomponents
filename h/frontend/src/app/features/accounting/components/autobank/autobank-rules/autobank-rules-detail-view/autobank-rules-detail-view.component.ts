import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef, ElementRef } from '@angular/core';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { mandatorNoModifier, IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { autobankFilterConfig } from 'src/app/core/constants/nv-grid-configs/autobank-filter.config';
import { BankAccountRepositoryService } from 'src/app/core/services/repositories/bank-account-repository.service';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { MandatorBookingCodeRepositoryService } from 'src/app/core/services/repositories/mandator-booking-code-repository.service';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { MandatorBookingCodeMainService } from 'src/app/core/services/mainServices/mandator-booking-code-main.service';
import { DebitorCreditorRepositoryService } from 'src/app/core/services/repositories/debitor-creditor-repository.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { CountryRepositoryService } from 'src/app/core/services/repositories/country-repository.service';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { AutobankRuleRepositoryService } from 'src/app/core/services/repositories/autobank-rule-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { IAutobankRule, IAutobankRuleCreate, IAutobankRuleFilter } from 'src/app/core/models/resources/IAutobankRule';
import { PaymentType } from 'src/app/core/models/enums/payment-type';
import { Validity } from 'src/app/core/models/enums/validity';
import { ActivatedRoute } from '@angular/router';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { GridComponent, NvGridConfig } from 'nv-grid';
import { forkJoin } from 'rxjs';
import { Validators } from 'src/app/core/classes/validators';
import { AutobankFieldTypeRepositoryService } from 'src/app/core/services/repositories/autobank-field-type.repository.service';
import { AutobankOperationTypeRepositoryService } from 'src/app/core/services/repositories/autobank-operation-type.repository.service';
import { gridConfig as mandatorGridConfig } from 'src/app/shared/components/autocomplete/modals/mandator-grid-config';
import { gridConfig as accountGridConfig } from 'src/app/shared/components/autocomplete/modals/account-grid-config';
import { gridConfig as codeGridConfig } from 'src/app/shared/components/autocomplete/modals/booking-code-grid-config';
import { MandatorRoleRepositoryService } from 'src/app/core/services/repositories/mandator-role.repository.service';
import { FieldOperationTypeRepositoryService } from 'src/app/core/services/repositories/field-operation-type-repository.service';
import { AutobankBusinessCaseRepositoryService } from 'src/app/core/services/repositories/autobank-business-case-repository.service';
import { AutobankFilterValueValidatorsService } from '../../../../../../core/services/validators/autobank-filter-value-validators.service';
import { AutobankFieldTypes } from 'src/app/core/models/enums/autobank-field-types';
import { AccountValidatorsService } from 'src/app/core/services/validators/account-validators.service';
import { BankAccountValidatorsService } from 'src/app/core/services/validators/bank-account-validators.service';
import { CodeValidatorsService } from 'src/app/core/services/validators/code-validators.service';
import { CountryValidatorsService } from 'src/app/core/services/validators/country-validators.service';
import { DebitorCreditorValidatorsService } from 'src/app/core/services/validators/debitor-creditor-validators.service';
import { MandatorValidatorsService } from 'src/app/core/services/validators/mandator-validators.service';
import { OperationTypeValidatorsService } from 'src/app/core/services/validators/operation-type-validators.service';
import { scrollToFirstInvalid } from 'src/app/shared/helpers/scrollToFirstInvalid';

@Component({
  selector: 'nv-autobank-detail-view',
  templateUrl: './autobank-rules-detail-view.component.html',
  styleUrls: ['./autobank-rules-detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutobankRulesDetailViewComponent
  extends ResourceDetailView<IAutobankRule, IAutobankRuleCreate>
  implements OnInit {
  @ViewChild('autobankFilterGrid') public autobankFilterGrid: GridComponent;

  protected repoParams = {};
  public routeBackUrl = url(sitemap.accounting.children.autobank.children.rules);
  public PaymentType = PaymentType;
  public Validity = Validity;
  public autobankFilterConfig: NvGridConfig;
  public mandatorNoModifier = mandatorNoModifier;

  public dataSource: IAutobankRuleFilter[];
  protected resourceDefault: IAutobankRule = {
    id: null,
    _type: 'Autobank',
    isActive: true,
    mandatorId: null,
    companyId: null,
    filterName: null,
    isValidForAllBankAccounts: true,
    bankAccountId: null,
    autobankPaymentType: PaymentType.BOTH,
    autobankRuleFields: [],
    bookingMandatorId: null,
    bookingAccountId: null,
    debitorCreditorId: null,
    bookingCodeId: null,
    costCenter: null,
    countryId: null,
    bookingText: null,
    remarks: null,
  };

  public mandatorGridConfig = mandatorGridConfig();
  public codeGridConfig = codeGridConfig;
  public accountrGridConfig = accountGridConfig;
  public formElement: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    public mandatorRepo: MandatorRepositoryService,
    public bookingCodeRepo: BookingCodeRepositoryService,
    public bankAccountRepo: BankAccountRepositoryService,
    public mandatorBookingCodeMainService: MandatorBookingCodeMainService,
    public debitorCreditorRepo: DebitorCreditorRepositoryService,
    public bookingAccountRepo: BookingAccountRepositoryService,
    public mandatorBookingCodeRepo: MandatorBookingCodeRepositoryService,
    public mandatorBookingAccountRepo: MandatorAccountRepositoryService,
    public countryRepo: CountryRepositoryService,
    private route: ActivatedRoute,
    autobankRuleRepo: AutobankRuleRepositoryService,
    baseComponentService: BaseComponentService,
    private mandatorValidatorsService: MandatorValidatorsService,
    private accountValidatorsService: AccountValidatorsService,
    private codeValidatorsService: CodeValidatorsService,
    private countryValidatorsService: CountryValidatorsService,
    private debitorCreditorValidatorsService: DebitorCreditorValidatorsService,
    public autobankFieldTypeRepo: AutobankFieldTypeRepositoryService,
    public autobankOperationTypeRepo: AutobankOperationTypeRepositoryService,
    public fieldOperationTypeRepo: FieldOperationTypeRepositoryService,
    public autobankBusinessCaseRepo: AutobankBusinessCaseRepositoryService,
    private bankAccountValidatorsService: BankAccountValidatorsService,
    public mandatorRoleRepo: MandatorRoleRepositoryService,
    private operationTypeValidatorsService: OperationTypeValidatorsService,
    private autobankFilterValueValidatorsService: AutobankFilterValueValidatorsService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef
  ) {
    super(autobankRuleRepo, baseComponentService);
  }

  public enterCreationMode = (): void => {
    this.creationMode = true;
    this.canRouteBack = false;
    this.enterEditMode();
  }

  public getTitle = () => {
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === this.resource.mandatorId);
    return `${mandatorNoModifier(foundMandator.no)} ${foundMandator.name} ${this.resource.filterName}`;
  }


  public ngOnInit() {
    const { id } = this.route.parent.snapshot.params;
    this.autobankFilterConfig = autobankFilterConfig(
      {
        field: this.autobankFieldTypeRepo.getPropertyById,
        operation: this.autobankOperationTypeRepo.getPropertyById
      },
      {
        doesOperationTypeExist: this.operationTypeValidatorsService.doesExist,
        isRelated: this.operationTypeValidatorsService.isRelatedToFieldType,
        hasValue: this.autobankFilterValueValidatorsService.hasValue
      },
      {
        delete: (deletedRow: IAutobankRuleFilter) => {
          if (this.editMode) {
            this.dataSource = this.dataSource.filter(row => row.id !== deletedRow.id);
          }
        }
      },
      this.onFieldTypeChange
    );

    this.inEditMode
      .subscribe(value => {
        this.autobankFilterConfig.editForm.disableEditColumns = !value;
      }
      );

    forkJoin([
      this.mandatorRoleRepo.fetchAll({}),
      this.mandatorRepo.fetchLookups({}),
      this.bookingCodeRepo.fetchAll({}),
      this.bookingAccountRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
      this.bankAccountRepo.fetchAll({}),
      this.countryRepo.fetchAll({}),
      this.autobankFieldTypeRepo.fetchAll({}),
      this.autobankOperationTypeRepo.fetchAll({}),
      this.fieldOperationTypeRepo.fetchAll({}),
      this.autobankBusinessCaseRepo.fetchAll({}),
    ]).subscribe(() => {
      if (id === 'new') {
        this.enterCreationMode();
        this.cdr.markForCheck();
      } else {
        this.loadResource(id).subscribe((resource) => {
          this.cdr.markForCheck();
        });
      }
    });
  }

  public onFieldTypeChange(form: FormGroup): any {
    form.get('value').patchValue(null);
  }

  public updateCompanyId(value: number): void {
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === value);

    if (foundMandator) {
      this.form.get('companyId').patchValue(
        foundMandator
          ? foundMandator.companyId
          : null
      );
    }
  }

  public createItem(rule?: IAutobankRuleFilter): FormGroup {
    return this.formBuilder.group({
      id: !this.creationMode && Number(rule.id) ? rule.id : null,
      operationTypeId: rule.operationTypeId,
      autobankRuleId: rule.autobankRuleId,
      fieldTypeId: rule.fieldTypeId,
      // For field type GVC: store value in autoBankBusinessCaseId field
      value: rule.fieldTypeId === AutobankFieldTypes.GVC ? null : rule.value,
      autobankBusinessCaseId: rule.fieldTypeId === AutobankFieldTypes.GVC ? +rule.value : null,
    });
  }

  public get rulesContainer() {
    return this.form.get('autobankRuleFields') as FormArray;
  }


  public updateGridDatasource() {
    this.dataSource = this.resource
      // TODO: replace this json parse stringify with deep clone of an array
      ? JSON.parse(JSON.stringify(this.resource.autobankRuleFields))
      : [];
  }

  public init(autobankRule: IAutobankRule = this.resourceDefault): void {
    this.initForm(this.formBuilder.group({
      isActive: [autobankRule.isActive],
      mandatorId: [autobankRule.mandatorId, [
        Validators.required,
        this.mandatorValidatorsService.doesMandatorExist,
        this.mandatorValidatorsService.isMandatorActive,
        this.mandatorValidatorsService.doesMandatorHaveAnyRole,
        this.mandatorValidatorsService.isAutoBankWorkflow
      ]],
      companyId: [autobankRule.companyId, [Validators.required]],
      filterName: [autobankRule.filterName, [Validators.required]],
      isValidForAllBankAccounts: [
        autobankRule.isValidForAllBankAccounts,
        [Validators.required]
      ],
      bankAccountId: [{
        value: autobankRule.bankAccountId,
        disabled: autobankRule.isValidForAllBankAccounts
      }, [
        Validators.required,
        this.bankAccountValidatorsService.doesBankAccountExist,
        this.bankAccountValidatorsService.isBankAccountRelatedToCompany
      ]],
      autobankPaymentType: [autobankRule.autobankPaymentType],
      autobankRuleFields: this.formBuilder.array(autobankRule.autobankRuleFields),
      bookingMandatorId: [autobankRule.bookingMandatorId, [
        Validators.required,
        this.mandatorValidatorsService.doesMandatorExist,
        this.mandatorValidatorsService.isMandatorActive
      ]],
      bookingAccountId: [autobankRule.bookingAccountId, [
        Validators.required,
        this.accountValidatorsService.doesBookingAccountExist,
        this.accountValidatorsService.isBookingAccountActive,
        this.accountValidatorsService.isBookingAccountRelatedToBookingMandatorId,
      ]],
      debitorCreditorId: [autobankRule.debitorCreditorId, [
        this.debitorCreditorValidatorsService.doesDdebitorCreditorExist,
        this.debitorCreditorValidatorsService.isDebitorCreditorActive
      ]],
      bookingCodeId: [autobankRule.bookingCodeId, [
        this.codeValidatorsService.doesBookingCodeExist,
        this.codeValidatorsService.isBookingCodeActive,
        this.codeValidatorsService.isBookingCodeRelatedToBookingMandatorId,
      ]],
      costCenter: [autobankRule.costCenter],
      countryId: [autobankRule.countryId, [
        this.countryValidatorsService.doesCountryExist,
        this.countryValidatorsService.isCountryActive
      ]],
      bookingText: [autobankRule.bookingText],
      remarks: [autobankRule.remarks],
    }));

    this.updateCompanyId(autobankRule.mandatorId);

    // For AutoBankFieldTypes.GVC: store autoBankBusinessCaseId in value field
    // so it can be displayed in grid column
    if (this.resource) {
      this.resource.autobankRuleFields.forEach(filter => {
        if (filter.fieldTypeId === AutobankFieldTypes.GVC) {
          filter.value = filter.autobankBusinessCaseId;
          filter.autobankBusinessCaseId = null;
        }
      });
    }

    this.updateGridDatasource();

    if (autobankRule.isValidForAllBankAccounts) {
      this.setDisabledFields(['bankAccountId']);
    }

    const {
      mandatorId,
      companyId,
      isValidForAllBankAccounts,
      bankAccountId,
      bookingMandatorId,
      bookingCodeId,
      bookingAccountId
    } = this.form.controls;

    mandatorId.valueChanges.subscribe(value => {
      if (value) {
        const foundMandator: IMandatorLookup = this.mandatorRepo
          .getLookupStreamValue()
          .find(mandator => mandator.id === value);

        if (foundMandator) {
          companyId.patchValue(
            foundMandator
              ? foundMandator.companyId
              : null
          );
          this.bankAccountRepo.fetchAll({
            queryParams: { companyId: foundMandator.companyId }
          }).subscribe(() => bankAccountId.updateValueAndValidity());
        }
      }
    });

    isValidForAllBankAccounts
      .valueChanges
      .subscribe(value => value === false
        ? bankAccountId.enable()
        : (
          bankAccountId.setValue(null),
          bankAccountId.disable()
        )
      );

    bookingMandatorId.valueChanges.subscribe(value => {
      if (value) {
        const foundMandator: IMandatorLookup = this.mandatorRepo
          .getLookupStreamValue()
          .find(mandator => mandator.id === value);

        if (foundMandator) {
          forkJoin([
            this.mandatorBookingCodeRepo
              .fetchAll({ queryParams: { mandatorId: value } }),
            this.mandatorBookingAccountRepo
              .fetchAll({ queryParams: { mandatorId: value } })
          ]).subscribe(() => {
            bookingCodeId.updateValueAndValidity();
            bookingAccountId.updateValueAndValidity();
          });
        }
      }
    });
  }

  private areAllFormsValid(): boolean {
    return this.autobankFilterGrid.formsDataSubject.value
      ? this.autobankFilterGrid.formsDataSubject
        .value
        .every(nvform => nvform.form.valid)
      : false;
  }

  private areAllFormsUndirty(): boolean {
    return this.autobankFilterGrid.formsDataSubject.value
      ? this.autobankFilterGrid.formsDataSubject
        .value
        .every(nvform => !nvform.form.dirty)
      : true;
  }

  public submit() {
    if (!this.form.valid) { return; }

    this.autobankFilterGrid.formsDataSubject.value.forEach(
      nvForm => scrollToFirstInvalid(nvForm.form, this.el.nativeElement.querySelector('nv-grid'), true)
    );

    // Check if not more than one GVC filter option is selected
    if (this.autobankFilterGrid.rows.filter(row =>
      row.fieldTypeId === AutobankFieldTypes.GVC).length > 1) {
      this.confirmationService.error({
        title: this.wording.accounting.notMoreThanOneGVCFilterOptionIsAllowed,
        cancelButtonWording: null,
        okButtonWording: this.wording.general.ok
      }).subscribe();
      return;
    }

    this.form.setControl('autobankRuleFields', new FormArray([]));

    this.autobankFilterGrid.rawRows
      .forEach((row: IAutobankRuleFilter) => this.rulesContainer.push(
        this.createItem(row)));

    if (!this.form.valid || !this.areAllFormsValid()) {
      return;
    }

    if (this.creationMode) {
      this.updateGridDatasource();
    }

    const value = this.form.getRawValue();
    const autobank = this.creationMode
      ? { ...value }
      : { ...this.resource, ...value };

    this.creationMode
      ? this.createResource(autobank)
      : this.updateResource(autobank);
  }

  public jumpToGrid() {
    this.autobankFilterGrid.focusFirstCellOfFirstRow();
  }

  public customValidation = (): boolean => {
    return this.form
      ? !this.form.dirty && this.areAllFormsUndirty()
      : true;
  }

  public isGVC = (data: any): boolean => {
    if (data && data.rowIndex > -1) {
      const row = this.autobankFilterGrid.rows[data.rowIndex];
      return row['fieldTypeId'] === AutobankFieldTypes.GVC;
    }

    return false;
  }

  public getAutobankFilterDescription = (cellValue: any, rowIndex: number): string => {
    let value = cellValue;
    const row = this.autobankFilterGrid && this.autobankFilterGrid.rows[rowIndex];
    if (row && row['fieldTypeId'] === AutobankFieldTypes.GVC) {
      const businessCaseId = cellValue as number;
      if (businessCaseId) {
        const businessCase = this.autobankBusinessCaseRepo.getStreamValue().find(bc => bc.id === businessCaseId);
        value = businessCase ? businessCase.gvc + ' ' + businessCase[this.labelKey] : '';
      }
    }

    return value;
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}

