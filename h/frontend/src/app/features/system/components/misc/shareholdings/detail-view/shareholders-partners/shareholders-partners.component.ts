import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompanyKey } from '../../../../../../../core/models/resources/ICompany';
import { ShareholdingsMainService } from '../../../../../../../core/services/mainServices/shareholdings-main.service';

@Component({
  templateUrl: './shareholders-partners.component.html',
  styleUrls: ['./shareholders-partners.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareholdingsShareholdersPartnersComponent implements OnInit {
  public companyId: CompanyKey;

  constructor(
    private activatedRoute: ActivatedRoute,
    private shareholdingsMainService: ShareholdingsMainService,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    const shareholdingId: CompanyKey = +this.activatedRoute.parent.snapshot.params.id;

    this.shareholdingsMainService.getShareholdingById(shareholdingId).subscribe(shareholding => {
      if (shareholding) {
        this.companyId = shareholding.id;
        this.cdr.markForCheck();
      }
    });
  }

}
