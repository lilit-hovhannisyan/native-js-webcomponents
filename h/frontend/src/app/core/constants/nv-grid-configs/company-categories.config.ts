import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import {
  NvFilterControl, NvGridButtonsPosition, NvGridConfig, NvColumnDataType
} from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { Operations } from 'src/app/core/models/Operations';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (operation: Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'companyCategoriesGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    sortBy: 'id',
    isSortAscending: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 10
    },
    columns: [
      {
        key: 'id',
        hidden: true,
      },
      {
        key: 'displayLabel',
        title: wording.system.category,
        width: 250,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'isSystemDefault',
        title: wording.general.systemDefault,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isActive',
        title: wording.accounting.active,
        width: 100,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: wording.general.edit,
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        func: actions.edit,
        hidden: !canPerform(Operations.UPDATE)
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: wording.general.delete,
        tooltip: wording.general.delete,
        func: actions.delete,
        hidden: !canPerform(Operations.DELETE)
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        func: actions.add,
        hidden: () => !canPerform(Operations.CREATE)
      }
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
