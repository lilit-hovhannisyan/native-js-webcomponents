export default interface BackendConfigI {
  serverTime: {
    time: number;
    timezone: string;
    datetime: string;
    offset: number;
  };
  maxEventsInBatch: number;
}
