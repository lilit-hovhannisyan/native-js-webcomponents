import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MePopupUserInfoComponent } from './me-popup-user-info.component';

import { MeContextMenuModule } from '../me-context-menu/me-context-menu.module';
import { MeUserImageModule } from '@shared-components/me-user-image/me-user-image.module';
import { MeLabelModule } from '@shared-components/me-label/me-label.module';

import { MeIconsProviderModule } from '@shared-modules/me-icons.module';

@NgModule({
  declarations: [MePopupUserInfoComponent],
  imports: [CommonModule, MeContextMenuModule, MeUserImageModule, MeLabelModule, MeIconsProviderModule],
  exports: [MePopupUserInfoComponent]
})
export class MePopupUserInfoModule {}
