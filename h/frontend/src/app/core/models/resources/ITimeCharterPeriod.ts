import { CHARTER_DURATION_UNIT } from './../enums/CharterDurationUnit';
import { CHARTER_DURATION_TYPE } from './../enums/CharterDurationType';
import { CHARTER_PERIOD_TYPE } from './../enums/CharterPeriodType';
import { IResource } from './IResource';
import { TimeCharterKey } from './ITimeCharter';
import { CHARTER_RATE_TYPE } from '../enums/CharterRateType';

export type TimeCharterPeriodKey = number;

export interface ITimeChartererRate {
  from: string;
  charterRate: number;
}

interface ISharedProps {
  from: string;
  until: string;
  timeCharterId: TimeCharterKey;
  periodType: CHARTER_PERIOD_TYPE;
  begin: string;
  endApprox: string;
  endActual?: string;
  durationType: CHARTER_DURATION_TYPE;
  duration?: number;
  durationUnit: CHARTER_DURATION_UNIT;
  minPeriod?: number;
  maxPeriod?: number;
  minDate?: number;
  maxDate?: number;
  plusDays?: number;
  minusDays?: number;
  additionalOffHire?: number;
  declarationToBe?: string;
  declared?: string;
  addendumDated?: string;
  addendumNo?: string;
  currencyId: number;
  charterRateType: CHARTER_RATE_TYPE;
  timeChartererRates?: ITimeChartererRate[];
}

export interface ITimeCharterPeriodBase extends ISharedProps, IResource<number> { }

export interface ITimeCharterPeriodCreate extends ISharedProps { }

export interface ITimeCharterPeriodDTO extends ITimeCharterPeriodBase { }

export interface ITimeCharterPeriod extends ITimeCharterPeriodDTO { }
