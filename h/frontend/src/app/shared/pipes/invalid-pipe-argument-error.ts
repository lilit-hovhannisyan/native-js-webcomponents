/***
 * Copied from https://github.com/angular/angular/blob/master/packages/common/src/pipes/invalid_pipe_argument_error.ts
 */
import {Type, ɵstringify as stringify} from '@angular/core';

export function invalidPipeArgumentError(type: Type<any>, value: Object) {
  /* passed JSON.stringify of value, because value can be object type */
  return Error(`InvalidPipeArgument: '${JSON.stringify(value)}' for pipe '${stringify(type)}'`);
}
