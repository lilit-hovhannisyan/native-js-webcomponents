import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { dateFormatTypes, timeFormatTypes } from 'src/app/core/models/dateFormat';
import { SettingsService } from 'src/app/core/services/settings.service';

@Pipe({
  name: 'toCurrentDateFormat'
})
export class ToCurrentDateFormatPipe implements PipeTransform {

  constructor(private settingsService: SettingsService) { }
  public transform(
    value: any,
    timeDisplay?: { withTime?: boolean, justTime?: boolean },
    separateChar = ' '
  ): string | null {
    if (!value) {
      return null;
    }

    if (timeDisplay && timeDisplay.withTime) {
      return moment(value).format(
        `${dateFormatTypes[this.settingsService.locale]}` +
        `${separateChar}` +
        `${timeFormatTypes[this.settingsService.locale]}`
      );
    }

    if (timeDisplay && timeDisplay.justTime) {
      return moment(value).format(
        `${timeFormatTypes[this.settingsService.locale]}`
      );
    }

    // per default just diplay date, no time
    return moment(value).format(dateFormatTypes[this.settingsService.locale]);
  }
}
