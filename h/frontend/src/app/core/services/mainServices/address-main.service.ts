import { CompanyKey } from './../../models/resources/ICompany';
import { Injectable } from '@angular/core';
import { forkJoin, Observable, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ICountry } from '../../models/resources/ICountry';
import { IAddressType } from '../../models/resources/IAddressType';
import { AddressRepositoryService, AddressParams } from '../repositories/address-repository.service';
import { IAddress, AddressKey } from '../../models/resources/IAddress';
import { CountryRepositoryService, MAX_COUNTRIES } from '../repositories/country-repository.service';
import { AddressTypeRepositoryService } from '../repositories/address-type-repository.service';
import { sortBy } from 'lodash';
import { SettingsService } from '../settings.service';
import { IWording } from '../../models/resources/IWording';

export interface IAddressFull extends IAddress {
  country: ICountry;
  type: IAddressType;
}

export interface IAddressRow extends IAddressFull {
  countryName: IWording;
  typeName: IWording;
  _checked: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AddressMainService {

  constructor(
    private addressRepo: AddressRepositoryService,
    private countryRepo: CountryRepositoryService,
    private addressTypeRepo: AddressTypeRepositoryService,
    private settingsService: SettingsService
  ) { }

  private getAddressFullList(
    addresses: IAddress[],
    country: ICountry[],
    addressTypes: IAddressType[]
  ): IAddressFull[] {
    return addresses.map(address => this.getAddressFull(address, country, addressTypes));
  }

  private getAddressFull(
    address: IAddress,
    countries: ICountry[],
    addressTypes: IAddressType[]
  ): IAddressFull {
    const foundCountry = countries.find(country => country.id === address.countryId);
    const foundAdressType = addressTypes.find(addressType => addressType.id === address.addressTypeId);
    return {
      ...address,
      country: foundCountry,
      type: foundAdressType
    };
  }

  private getCompanyFilter(companyId: CompanyKey): (x: IAddress[]) => IAddress[] {
    return (address) => address.filter(ba => ba.companyId === companyId);
  }

  public fetchAll(params: AddressParams): Observable<IAddressFull[]> {
    return forkJoin([
      this.addressRepo.fetchAll(params),
      this.countryRepo.fetchAll({ queryParams: { limit: MAX_COUNTRIES } }),
      this.addressTypeRepo.fetchAll({})
    ]).pipe(map(res => this.getAddressFullList(...res)));
  }

  public fetchOne(addressId: AddressKey, params: AddressParams) {
    return forkJoin([
      this.addressRepo.fetchOne(addressId, params),
      this.countryRepo.fetchAll({ queryParams: { limit: MAX_COUNTRIES } }),
      this.addressTypeRepo.fetchAll({})
    ]).pipe(map(res => this.getAddressFull(...res)));
  }

  public getStream(params: { companyId: CompanyKey }): Observable<IAddressFull[]> {
    return forkJoin([
      this.addressRepo.fetchAll({ queryParams: params }),
      this.countryRepo.fetchAll({ queryParams: { limit: MAX_COUNTRIES } }),
      this.addressTypeRepo.fetchAll({})
    ]).pipe(
      switchMap(() => combineLatest([
        this.addressRepo.getStream().pipe(map(this.getCompanyFilter(params.companyId))),
        this.countryRepo.getStream(),
        this.addressTypeRepo.getStream()
      ])),
      map(collection => this.getAddressFullList(...collection))
    );
  }

  public toRow(address: IAddressFull): IAddressRow {
    const { type, country } = address;
    return ({
      ...address,
      countryName: country ? country.displayLabel : null,
      typeName: type ? type.displayLabel : null,
      _checked: false,
    });
  }

  public toRows(addresses: IAddressFull[]): IAddressRow[] {
    return sortBy(addresses.map((address) => this.toRow(address)), ['countryName']);
  }

  public rowAddressToText(rowAddress: IAddressRow): string {
    const address = rowAddress; // Taking the 1. Address as an interim solution
    const countryName = address && address.countryName
      ? address.countryName[this.settingsService.language]
      : '';
    return address
      ? `${address.street}\n${address.zipCode}\n${address.city}\n${countryName}`
      : '';
  }
}
