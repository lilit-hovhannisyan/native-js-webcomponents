import { NvGridConfig, NvGridButtonsPosition, NvColumnDataType } from 'nv-grid';
import { wording } from '../wording/wording';
import { Validators } from '../../classes/validators';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';
import { AbstractControl, FormGroup, ValidationErrors } from '@angular/forms';

export const getMortgageGridConfig = (
  actions: IGridConfigActions,
  setCurrencyId: (formGroup: FormGroup) => void,
  validators: {
    doesCurrencyExist: (control: AbstractControl) => ValidationErrors | null,
    isCurrencyActive: (control: AbstractControl) => ValidationErrors | null,
  },
  readOnly: boolean,
): NvGridConfig => ({
  gridName: 'MortgageGridFilter',
  hideToolbar: true,
  hideAllFilters: true,
  showFooter: false,
  disableDragAndDrop: true,
  disableHideColumns: true,
  clearFiltersButton: true,
  rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
  paging: {
    pageNumber: 1,
    pageSize: readOnly ? 3 : 10
  },
  editForm: {
    allowCreateNewRow: true,
    preventDiscardingFirstRow: false,
    showDiscardChangesButton: true,
  },
  columns: [
    {
      key: 'id',
      visible: false
    },
    {
      key: 'mortgagee',
      title: wording.chartering.mortgagee,
      width: 150,
      isSortable: true,
      editControl: {
        editable: true,
        validation: {
          validators: [Validators.required],
        },
      },
    },
    {
      key: 'mortgageAmount',
      title: wording.chartering.mortgageAmount,
      width: 150,
      isSortable: true,
      dataType: NvColumnDataType.Decimal,
      editControl: {
        editable: true,
        validation: {
          validators: [Validators.required],
        },
      },
    },
    {
      key: 'currencyName',
      title: wording.accounting.currency,
      width: 150,
      isSortable: true,
      editControl: {
        editable: true,
        validation: {
          validators: [
            Validators.required,
            validators.doesCurrencyExist,
            validators.isCurrencyActive
          ],
        },
        onValueChange: (form: FormGroup) => {
          setCurrencyId(form);
        }
      },
    },
    {
      key: 'currencyId',
      visible: false,
    },
  ],
  buttons: readOnly
    ? []
    : [
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteFilter',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        func: actions.delete,
      },
    ]
});
