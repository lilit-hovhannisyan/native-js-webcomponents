import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';
import { CalendarKey } from './ICalendar';

export type CalendarEventKey = number;

interface ISharedProps extends IWithLabels {
  isSystemDefault: boolean;
  calendarId: CalendarKey;
  isWholeDayEvent: boolean;
  start: string;
  end: string;
  remark: string;
  color: string;
}

export interface ICalendarEventCreate extends ISharedProps { }

export interface ICalendarEventBase extends ISharedProps, IResource<CalendarEventKey> { }

export interface ICalendarEventDTO extends ICalendarEventBase { }

export interface ICalendarEvent extends ICalendarEventDTO, IWithLabel { }
