import { IResource } from './IResource';
import { IAutobankMandatorUserConnection } from './IAutobankMandatorUserConnection';
import { UserKey } from './IUser';
import { MandatorKey } from './IMandator';
import { IPermissionGroup, PermissionKey } from './IPermissionGroup';

export type AutobankPermissionKey = PermissionKey;

interface ISharedProps extends IPermissionGroup<IAutobankMandatorUserConnection> {
}

export interface IAutobankPermissionBase extends ISharedProps, IResource<AutobankPermissionKey> { }

export interface IAutobankPermissionCreate extends ISharedProps { }

export interface IAutobankPermissionDTO extends IAutobankPermissionBase { }

export interface IAutobankPermission extends IAutobankPermissionDTO { }

export interface IAutobankMandatorsUsersLists {
  usersIds: UserKey[];
  mandatorsIds: MandatorKey[];
}
