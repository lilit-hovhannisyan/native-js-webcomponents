import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IWording } from 'src/app/core/models/resources/IWording';

@Component({
  selector: 'nv-tab',
  templateUrl: './nv-tab.component.html',
  styleUrls: ['./nv-tab.component.scss']
})
export class NvTabComponent {
  @Input() public title = '';
  @Input() public path = '';
  @Input() public disabled = false;
  @Input() public selected = false;
  @Input() public isDataMissing = false;
  @Input() public missingDataWording: IWording;

  @Output() public nvClick = new EventEmitter<void>();

}
