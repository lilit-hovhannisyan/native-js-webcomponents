import { Injectable } from '@angular/core';
import { ILog, ILogCreate } from '../models/resources/ILog';
import { Observable, BehaviorSubject } from 'rxjs';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { MessageRepositoryService } from './repositories/message-repository.service';
import { IMessage } from '../models/resources/IMessage';
import { IDocumentBlobInfo } from 'src/app/core/models/IDocumentBlobInfo';
import { StoredDocumentService } from 'src/app/core/services/stored-document.service';
import { ExcelHelperService } from 'src/app/shared/services/excel-helper.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class ActivityLogsService {
  public logsSubject = new BehaviorSubject<ILog[]>([]);
  public logs$: Observable<ILog[]> = this.logsSubject.asObservable();

  constructor(
    private authService: AuthenticationService,
    private messageRepositoryService: MessageRepositoryService,
    private storedDocumentService: StoredDocumentService,
    private excelHelperService: ExcelHelperService
  ) { }

  private get logs() {
    return this.logsSubject.value;
  }

  private publishLogs = (newLogs: ILog[]): void => {
    this.logsSubject.next(newLogs);
  }

  public addLog(logsInfo: ILogCreate): void {
    const log: ILog = {
      message: logsInfo.message,
      subject: logsInfo.subject,
      user: logsInfo.user ? logsInfo.user : this.authService.getUser(),
      date: new Date(),
      unseen: true,
      id: this.logs.length + 1,
      _type: 'log'
    };
    this.publishLogs([log, ...this.logs]);
  }

  public markAsSeen = (): void => {
    this.publishLogs(this.logs.map(log => ({ ...log, unseen: false })));
  }

  public refresh() {
    // add messages to log
    this.messageRepositoryService.fetchAll({})
      .subscribe(messages => {
        messages.reverse().forEach(m => this.addLogForMessage(m));
      });
  }

  private addLogForMessage(message: IMessage) {
    const hasLog = this.logs.some(l => l.message.en === message.text &&
      l.documentGuid === message.documentGuid);

    if (hasLog) {
      return;
    }

    const log: ILog = {
      message: { en: message.text, de: message.text },
      subject: 'Document ready',
      documentGuid: message.documentGuid,
      user: this.authService.getUser(),
      date: moment(message.date).toDate(),
      unseen: true,
      id: this.logs.length + 1,
      _type: 'log'
    };
    this.publishLogs([log, ...this.logs]);
  }

  public downloadDocument(documentGuid: string) {
    this.storedDocumentService.fetchDocument(documentGuid)
      .subscribe((blobInfo: IDocumentBlobInfo) => {
        this.excelHelperService.saveBlobAsFile(blobInfo.blob, blobInfo.filename);
      });
  }
}
