import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IVoucherType, IVoucherTypeCreate, IVoucherTypeDTO, VoucherTypeKey } from './../../models/resources/IVoucherType';

export interface VoucherTypeParams extends RepoParams<IVoucherType> {
  mandatorId?: number;
}

@Injectable({
  providedIn: 'root'
})
export class VoucherTypeRepositoryService extends AbstractRepository<
  IVoucherType,
  IVoucherTypeDTO,
  IVoucherTypeCreate,
  VoucherTypeParams,
  VoucherTypeKey
> {
  protected resourceType = 'VoucherType';
  public sortAsce = true;
  public sortKey = 'abbreviation';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/voucher-types/';
  }

  protected postProcess(item: IVoucherTypeDTO): IVoucherType { return item; }
}
