import { Component, ChangeDetectionStrategy, HostBinding, HostListener, Input, ChangeDetectorRef } from '@angular/core';
import { AbstractValueAccessor, makeProvider } from 'src/app/core/abstracts/value-accessor';
import { ITaskStatus, taskStatusMap, TaskStatusState, VesselTaskStatus } from 'src/app/core/models/enums/vessel-registration-tasks';
import { IWording } from 'src/app/core/models/resources/IWording';

@Component({
  selector: 'nv-vessel-task-status-picker',
  templateUrl: './vessel-task-status-picker.component.html',
  styleUrls: ['./vessel-task-status-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [makeProvider(VesselTaskStatusPickerComponent)]
})
export class VesselTaskStatusPickerComponent extends AbstractValueAccessor<VesselTaskStatus> {
  /**
   * Overridden from {@Link AbstractValueAccessor}
   * Needed to have disabled `Input` and `.disabled` css-class.
   */
  // tslint:disable-next-line: no-input-rename
  @Input('disabled')
  @HostBinding('attr.disabled')
  public _disabled: boolean;

  /**
   * Needed to set html-attributes to component-tag for color changes.
   * Can be used as selectors in the tests.
   */
  @HostBinding('attr.data-task-status')
  private statusState: TaskStatusState = 'not-started';

  /**
   * Used only in template to show current status in selected language.
   */
  public tooltipWording: IWording;

  constructor(
    private cdr: ChangeDetectorRef,
  ) {
    super();
  }

  public writeValue(value: VesselTaskStatus = VesselTaskStatus.NotStarted): void {
    super.writeValue(value);
    this.setStatus(value);
  }

  /**
   * Automatically changes to next status if not disabled.
   */
  @HostListener('click')
  private changeStatus(): void {
    if (this.disabled) {
      return;
    }

    const current: ITaskStatus = taskStatusMap.get(this.value);
    this.value = current.next;
    this.setStatus(this.value);
  }

  /**
   * Sets new state and tooltips new text(wording).
   */
  private setStatus(statusValue: VesselTaskStatus): void {
    const status: ITaskStatus = taskStatusMap.get(statusValue);
    this.statusState = status.state;
    this.tooltipWording = status.wording;
    this.cdr.detectChanges();
  }
}
