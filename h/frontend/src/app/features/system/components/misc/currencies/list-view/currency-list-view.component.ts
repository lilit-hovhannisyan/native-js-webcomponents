import { Operations } from 'src/app/core/models/Operations';
import { Component, OnInit } from '@angular/core';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { AccountService } from 'src/app/core/services/account.service';
import { Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/currencies.config';
import { ExchangeRateService } from 'src/app/core/services/mainServices/exchange-rate.service';
import { IExchangeRateFull } from 'src/app/core/models/resources/IExchangeRate';
import * as moment from 'moment';
import { map, shareReplay } from 'rxjs/operators';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'nv-currency-list-view',
  templateUrl: './currency-list-view.component.html',
  styleUrls: ['./currency-list-view.component.scss']
})
export class CurrencyListViewComponent implements OnInit {
  public currenciesConfig: NvGridConfig;
  public favoriteCurrenciesConfig: NvGridConfig;
  public favoritesDataSource$: Observable<IExchangeRateFull[]>;
  public dataSource$: Observable<IExchangeRateFull[]>;

  constructor(
    private exchangeRateService: ExchangeRateService,
    public gridConfigService: GridConfigService,
    public accountService: AccountService,
    private authService: AuthenticationService,
    private router: Router
  ) { }

  public ngOnInit() {
    const today = moment().format('YYYY-MM-DD');
    const queryParams = { startDate: today, endDate: today };
    this.dataSource$ = this.exchangeRateService.fetchAll({ queryParams })
      .pipe(
        shareReplay(1)
      );


    this.favoritesDataSource$ = this.dataSource$.pipe(
      map(currencies => currencies.filter(c => c.isFavorite)),
    );

    const canPerform = (operation: Operations) => this.authService.canPerformOperationOnCurrentUrl(operation);
    const actions: IGridConfigActions = {
      edit: this.onEdit,
      add: this.onCreate,
    };

    this.favoriteCurrenciesConfig = getGridConfig(actions, canPerform, true);
    this.currenciesConfig = getGridConfig(actions, canPerform);
  }

  private onEdit = (id: number) => this.router.navigate([`${this.router.url}/${id}`]);

  private onCreate = () => this.router.navigate([this.router.url, 'new']);
}
