import { ACL_ACTIONS } from './../../authentication/services/authentication.service';


































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































import { sitemap } from '../../core/constants/sitemap/sitemap';
import { Operations } from '../../core/models/Operations';
import { BaseComponentService } from '../services/base-component.service';
import { Router } from '@angular/router';
import { url, SitemapEntry } from '../../core/constants/sitemap/sitemap-entry';

/**
 * The base component for all components of project. All components should extend it,
 * because it contains general properties and methods that we use almost in every
 * component.
 */
export abstract class BaseComponent {

  public url = url;
  protected router: Router;
  // not typed because: Typing it destroys autocomplete for the object-keys
  public wording = this.baseComponentService.wordingService.wording;
  public authService = this.baseComponentService.authService;
  public nzModalService = this.baseComponentService.nzModalService;
  public confirmationService = this.baseComponentService.confirmationService;
  public settingsService = this.baseComponentService.settingsService;
  public browserTitleService = this.baseComponentService.browserTitleService;
  public wordingService = this.baseComponentService.wordingService;
  public accountService = this.baseComponentService.accountService;
  public messageService = this.baseComponentService.messageService;


  public isSuperAdmin: boolean;
  public isAdmin: boolean;
  public ACL_ACTIONS = ACL_ACTIONS;

  /**
   * ACL operations
   */
  public readonly CREATE = Operations.CREATE;   // 0001
  public readonly READ = Operations.READ;       // 0010
  public readonly UPDATE = Operations.UPDATE;   // 0100
  public readonly DELETE = Operations.DELETE;   // 1000

  public sitemap = sitemap;

  constructor(
    protected baseComponentService: BaseComponentService
  ) {
    this.router = baseComponentService.router;
    this.isAdmin = this.authService.getSessionInfo().user.isAdmin;
    this.isSuperAdmin = this.authService.getSessionInfo().user.isSuperAdmin;
  }

  /**
   * returns the zone of the current URl
   */
  protected getCurrentZone(): string {
    return SitemapEntry.getByUrl(this.router.url).toZone();
  }
}
