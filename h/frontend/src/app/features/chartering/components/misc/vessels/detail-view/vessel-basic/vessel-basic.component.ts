import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NvGridConfig } from 'nv-grid';
import { forkJoin, merge, Observable, of, Subscription } from 'rxjs';
import { catchError, distinctUntilChanged, map, tap } from 'rxjs/operators';
import { NVCustomErrors } from 'src/app/core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { IVesselAccounting } from 'src/app/core/models/resources/IVesselAccounting';
import { IVesselPreDelayCharterName } from 'src/app/core/models/resources/IVesselPreDelayCharterName';
import { IVoyage } from 'src/app/core/models/resources/IVoyage';
import { IVoyageCharterName } from 'src/app/core/models/resources/IVoyageCharterName';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { VesselRepositoryService } from 'src/app/core/services/repositories/vessel-repository.service';
import { VoyageCharternameRepositoryService } from 'src/app/core/services/repositories/voyage-chartername-repository.service';
import { VoyagesRepositoryService } from 'src/app/core/services/repositories/voyages-repository.service';
import { ResourceDetailView } from '../../../../../../../core/abstracts/ResourceDetailView';
import { IVessel, IVesselCreate, VesselKey } from '../../../../../../../core/models/resources/IVessel';
import { AccountService } from '../../../../../../../core/services/account.service';
import { BaseComponentService } from '../../../../../../../core/services/base-component.service';
import { VesselsHelperService } from '../vessels.helper.service';
import { getGridConfig } from './accountings-grid.config';
import * as moment from 'moment';
import { Wording } from 'src/app/core/classes/wording';
import { ICompany } from 'src/app/core/models/resources/ICompany';
import { getGridConfig as getCompanyGridConfig } from 'src/app/core/constants/nv-grid-configs/companies.config';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { CountryKey, ICountry } from 'src/app/core/models/resources/ICountry';
import { CountryRepositoryService } from 'src/app/core/services/repositories/country-repository.service';
import { findClosestWithFromUntil } from 'src/app/core/helpers/general';
import { VesselRegisterRepositoryService } from 'src/app/core/services/repositories/vessel-registeries-repository.service';
import { IVesselRegister } from 'src/app/core/models/resources/IVesselRegister';

@Component({
  templateUrl: './vessel-basic.component.html',
  styleUrls: ['./vessel-basic.component.scss']
})
export class VesselBasicComponent extends ResourceDetailView<
IVessel,
IVesselCreate
> implements OnInit {
  protected repoParams = {};
  protected resourceDefault = { isActive: true } as IVessel;
  protected customErrors: NVCustomErrors = {
    'db-update': {
      changeFieldTo: 'imo',
      messageWording: this.wording.chartering.duplicateImo,

    },
  };
  private disabledFieldsArr = ['vesselAccountingNo', 'vesselAccountingName', 'foreignCountry'];

  public routeBackUrl = this.url(sitemap.chartering.children.misc.children.vessels);
  public subscription: Subscription;
  public charternameForm: FormGroup;
  public vesselCharternames: IVoyageCharterName[];
  public voyages: IVoyage[] = [];
  public companies: ICompany[] = [];
  public companyGridConfig: NvGridConfig = getCompanyGridConfig(this.router, () => false);

  /**
   * If previous accounting has the same name and no with current
   * item - then the current is skipped. Thus the length of the
   * data array smaller or equal than all connected accountings.
   */
  public accountingsDataSource$: Observable<IVesselAccounting[]>;
  public accountingsGridConfig: NvGridConfig;
  // Needed for name and no of the latest vessel accounting
  public closestAccounting: IVesselAccounting;
  public vesselPreDelivery: IVesselPreDelayCharterName;
  public readonly charternameZone = 'chartering.misc.voyages.charternames';
  public readonly preDelayZone = 'chartering.misc.vessel.preDelayCharterNames';
  public currentCountryISO: string;
  public latestVesselRegister: IVesselRegister;
  private vesselRegisters: IVesselRegister[] = [];
  public countries: ICountry[] = [];
  public selectedCountryFlag: string;

  public get canUpdatePreDeliveryFields(): boolean {
    return this.authService.canUpdate(this.preDelayZone);
  }
  public formElement: ElementRef;

  constructor(
    private fb: FormBuilder,
    public accountService: AccountService,
    protected vesselRepo: VesselRepositoryService,
    public route: ActivatedRoute,
    public gridConfigService: GridConfigService,
    private vesselMainService: VesselMainService,
    private charternameRepo: VoyageCharternameRepositoryService,
    private voyageRepo: VoyagesRepositoryService,
    private vesselsHelperService: VesselsHelperService,
    baseComponentService: BaseComponentService,
    private companyRepo: CompanyRepositoryService,
    private countryRepo: CountryRepositoryService,
    private vesselRegisterRepo: VesselRegisterRepositoryService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef
  ) {
    super(vesselRepo, baseComponentService);
  }

  public ngOnInit(): void {
    const idParam = this.route.parent.snapshot.params.id;
    this.fetchCompanies().subscribe();
    if (idParam === 'new') {
      this.enterCreationMode();
    } else {
      const id = parseInt(idParam, 10);

      // needed to have vesselAccountings before init call
      this.accountingsDataSource$ = this.fetchVesselAccountings(id);
      this.accountingsGridConfig = getGridConfig();
      forkJoin([
        this.accountingsDataSource$,
        this.fetchPreDeliveryAndCharternames(id),
        this.fetchCharternames(id),
        this.fetchVoyages(id),
        this.fetchRegistries(id),
        this.countryRepo.fetchAll({ useCache: true })
          .pipe(tap(countries => this.countries = countries)),
      ]).subscribe(() => {
        this.loadResource(id);
      });
    }

    if (!this.canUpdatePreDeliveryFields) {
      this.disabledFieldsArr.push('preDelay', 'charterName');
    }
  }

  private fetchRegistries(id: VesselKey): Observable<IVesselRegister[]> {
    return this.vesselRegisterRepo.fetchAll({ queryParams: { vesselId: id } })
      .pipe(
        tap(vesselRegisters => this.vesselRegisters = vesselRegisters)
      );
  }

  private fetchCompanies(): Observable<ICompany[]> {
    return this.companyRepo.fetchAll({})
      .pipe(
        tap(companies => this.companies = companies)
      );
  }

  public getTitle = () => this.resource.imo.toString();

  public init(vessel: IVessel = this.resourceDefault): void {
    this.latestVesselRegister = findClosestWithFromUntil<IVesselRegister>(
      this.vesselRegisters
    );
    const currentFlagId: CountryKey = this.latestVesselRegister?.secondaryCountryId
      || this.latestVesselRegister?.primaryCountryId;

    if (currentFlagId) {
      const foundCountry = this.countryRepo.getStreamValue()
        .find(c => c.id === currentFlagId);
      this.currentCountryISO = foundCountry ? foundCountry.isoAlpha3.toLowerCase() : '';
    }

    this.initForm(this.fb.group({
      imo: [vessel.imo, [Validators.required, Validators.exactLength(7), Validators.number]],
      deliveryDate: [vessel.deliveryDate, [Validators.required]],
      isIntendedSale: [vessel.isIntendedSale],
      isActive: [vessel.isActive],
      officialNumber: [vessel.officialNumber, [Validators.maxLength(20)]],
      mmsi: [vessel.mmsi, [Validators.maxLength(20)]],
      // the latest accounting connected to current vessel
      vesselAccountingNo: [this.closestAccounting?.vesselNo],
      vesselAccountingName: [this.closestAccounting?.vesselName],
      preDelay: [this.vesselPreDelivery?.preDelay],
      charterName: [this.vesselPreDelivery?.charterName],
      charternames: [this.vesselCharternames],
      // readonly display value
      foreignCountry: [currentFlagId],
    }));
    this.setDisabledFields(this.disabledFieldsArr);
    this.setPreDelayValidation();
  }

  private savePreDelivery(vesselId: VesselKey, formValue: IVesselPreDelayCharterName): void {
    if (!this.canUpdatePreDeliveryFields) {
      return;
    }
    const { charterName, preDelay } = formValue;
    const { preDelay: prevPreDelay, charterName: prevCharterName } = this.vesselPreDelivery || {};

    /**
     * if previous and current values are falsy
     * or previous values are equal to current values(no changes at all)
     * then it's required to not send a request
     */
    if (
      charterName === prevCharterName
      && preDelay === prevPreDelay
      || !charterName && !preDelay && !prevCharterName && !prevPreDelay
    ) {
      return;
    }

    const freshValue = {
      ...this.vesselPreDelivery,
      charterName,
      preDelay,
      vesselId,
    };

    this.vesselRepo.updatePreDeliveryChartername(vesselId, freshValue).subscribe(preDelivery => {
      this.vesselsHelperService.generateTitle(vesselId);
      this.vesselPreDelivery = preDelivery;
      this.setCharternameFields(preDelivery);
    });
  }

  public submit(): void {
    if (this.form.invalid) { return; }

    const formValue = this.form.value;
    const payload = this.creationMode
      ? this.form.value
      : ({ ...this.resource, ...this.form.value });
    if (this.creationMode) {
      this.createResource(payload, false)
        .subscribe(({ id }) => {
          this.vesselsHelperService.generateTitle(id);
          this.router.navigate(
            [this.url(sitemap.chartering.children.misc.children.vessels), id],
            { skipLocationChange: false, replaceUrl: true }
          );

          // set grid configs to show grid without data
          this.accountingsGridConfig = getGridConfig();
          this.accountingsDataSource$ = of([]);
          this.savePreDelivery(id, formValue);
        });
    } else {
      if (this.voyages.length) {
        const voyageWithTodaysDateInRange = this.voyages.find(v => {
          const isInRange = moment().isBetween(v.from, v.until);
          return isInRange;
        });

        if (voyageWithTodaysDateInRange && moment(payload.preDelay).isBefore(voyageWithTodaysDateInRange.from)) {
          this.confirmationService.warning({
            title: this.wording.general.warning,
            okButtonWording: this.wording.general.save,
            cancelButtonWording: this.wording.general.cancel,
            wording: Wording.concat(
              this.wording.chartering.preDelivery,
              this.wording.chartering.unexpectedDate,
              ': '
            ),
          }).subscribe(confirmed => {
            if (confirmed) {
              this.updateVoyage(payload, formValue);
            }
          });
        } else {
          this.updateVoyage(payload, formValue);
        }
      } else {
        this.updateVoyage(payload, formValue);
      }
    }
  }

  private updateVoyage(payload, formValue) {
    this.updateResource(payload)
      .subscribe(({ id }) => {
        this.vesselsHelperService.generateTitle(id);
        this.savePreDelivery(id, formValue);
      });
  }

  private setCharternameFields(entity: IVesselPreDelayCharterName): void {
    const { charterName, preDelay } = entity;
    const formValue = {
      ...this.form?.value,
      charterName,
      preDelay,
    };
    this.form.setValue(formValue);
  }

  private fetchPreDeliveryAndCharternames(vesselId: VesselKey): Observable<IVesselPreDelayCharterName> {
    if (!this.authService.canRead(this.preDelayZone)) {
      return of(null);
    }
    return this.vesselRepo.fetchPreDeliveryChartername(vesselId).pipe(
      // first time vesselPreDelivery doesn't exist and will throw an error
      catchError(() => of({} as IVesselPreDelayCharterName)),
      tap(vesselPreDelivery => this.vesselPreDelivery = vesselPreDelivery),
    );
  }

  private fetchVoyages(vesselId: VesselKey): Observable<IVoyage[]> {
    return this.voyageRepo.fetchAll({ queryParams: { vesselId } })
      .pipe(
        tap(voyages => {
          this.voyages = voyages;
        })
      );
  }

  private fetchCharternames(vesselId: VesselKey): Observable<IVoyageCharterName[]> {
    if (!this.authService.canRead(this.preDelayZone)) {
      return of(null);
    }

    return this.charternameRepo.fetchAll({ queryParams: { vesselId } }).pipe(
      tap(charternames => {
        this.vesselCharternames = charternames;
      }),
    );
  }

  private setPreDelayValidation(): void {
    const { preDelay, charterName } = this.form.controls;
    const { locale } = this.settingsService;

    const voyagesLength: number = this.voyages?.length || 0;
    const dateValidators = [];

    const firstFrom = this.voyages[voyagesLength - 1]?.from;
    const secondFrom = this.voyages[voyagesLength - 2]?.from;

    if (voyagesLength && firstFrom) {
      dateValidators.push(
        Validators.dateShouldBeBefore(firstFrom, locale, true, false, 'd'),
      );
    }
    if (voyagesLength && secondFrom) {
      dateValidators.push(
        Validators.dateShouldBeAfter(secondFrom, locale, false, false, 'd'),
      );
    }

    merge(
      preDelay.valueChanges.pipe(distinctUntilChanged()),
      charterName.valueChanges.pipe(distinctUntilChanged()),
    ).subscribe(() => {
      /**
       * If preDelivery fields had value, or on time of change
       * one of them has value, then the both fields are required.
       */
      if (
        preDelay.value
        || charterName.value
        || this.vesselPreDelivery?.charterName
        || this.vesselPreDelivery?.preDelay
      ) {
        preDelay.setValidators(Validators.required);
        charterName.setValidators(Validators.required);
      } else {
        charterName.setValidators([]);
      }
      preDelay.updateValueAndValidity();
      charterName.updateValueAndValidity();
    });
  }

  private fetchVesselAccountings(vesselId: number): Observable<IVesselAccounting[]> {
    return this.vesselRepo.fetchAccountingsByVesselId(vesselId).pipe(
      tap(vesselAccountings => {
        this.closestAccounting = findClosestWithFromUntil(vesselAccountings);
      }),
      map(vesselAccountings => {
        /**
         * If the current item has same vesselName and vesselNo as the previous one,
         * then it has to be skipped.
         */
        return vesselAccountings.filter((item, index, array) => {
          const prevItem = array[index - 1];
          return prevItem?.vesselName !== item.vesselName || prevItem?.vesselNo !== item.vesselNo;
        });
      }),
    );
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

}
