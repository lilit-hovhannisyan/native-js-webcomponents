import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ME_COPYRIGHT_YEAR, ME_FDRA_SITE_URL, ME_JOIN_MATERIAL_EXCHANGE, ME_USFIA_SITE_URL } from '@shared/constants';
@Component({
  selector: 'me-auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthLayoutComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  get copyrightYear(): number {
    return ME_COPYRIGHT_YEAR;
  }

  get fdraSiteUrl(): string {
    return ME_FDRA_SITE_URL;
  }

  get usfiaSiteUrl(): string {
    return ME_USFIA_SITE_URL;
  }

  get joinMaterialExchangeUrl(): string {
    return ME_JOIN_MATERIAL_EXCHANGE;
  }
}
