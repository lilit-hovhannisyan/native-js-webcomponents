import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NvGridConfig } from 'nv-grid';
import { Observable, ReplaySubject } from 'rxjs';
import { debounceTime, distinctUntilChanged, skipWhile, startWith, switchMap, takeUntil } from 'rxjs/operators';
import { Validators } from 'src/app/core/classes/validators';
import { wording } from 'src/app/core/constants/wording/wording';
import { ACCOUNTING_TYPES } from 'src/app/core/models/enums/accounting-types';
import { IBooking } from 'src/app/core/models/resources/IBooking';
import { getFiscalCalendarDate, mandatorNoModifier, IMandatorLookup, IMandatorLookupRow } from 'src/app/core/models/resources/IMandator';
import { IVoucherType } from 'src/app/core/models/resources/IVoucherType';
import { ExchangeRateService } from 'src/app/core/services/mainServices/exchange-rate.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { MandatorBlockedYearsRepositoryService } from 'src/app/core/services/repositories/mandator-blocked-years-repository.service';
import { MandatorClosedYearsRepositoryService } from 'src/app/core/services/repositories/mandator-closed-year-repository.service';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { MandatorVoucherTypeRepositoryService } from 'src/app/core/services/repositories/mandator-voucher-type-repository.service';
import { VoucherTypeRepositoryService } from 'src/app/core/services/repositories/voucher-type-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { RangeCalendarComponent } from 'src/app/shared/components/range-calendar/range-calendar.component';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { gridConfig as voucherTypeGridConfig } from 'src/app/shared/components/autocomplete/modals/voucher-type-grid-config';
import { BookingFieldsData } from '../booking-grid.component';
import { IBookingFormState } from './booking-form-helper';
import { MandatorAutocompleteComponent } from 'src/app/shared/components/auto-completes/mandator-autocomplete/mandator-autocomplete.component';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { MandatorValidatorsService } from 'src/app/core/services/validators/mandator-validators.service';
import { VoucherTypeValidatorsService } from 'src/app/core/services/validators/voucher-type-validators.service';
import { BookingFormService } from 'src/app/core/services/booking-form.service';

@Component({
  selector: 'nv-booking-form-fields-inline',
  templateUrl: './booking-form-fields.component.html',
  styleUrls: ['./booking-form-fields.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookingFormFieldsInlineComponent implements OnInit, OnChanges, OnDestroy {
  @Input() public currentBookingData$: Observable<BookingFieldsData>;
  @Input() public createMode: boolean;
  @Input() public cloneMode: boolean;
  @Input() public readMode: boolean;
  @Input() public changeMandator$: Observable<IMandatorLookup>;

  @Output() private jumpToGridEmitter = new EventEmitter<void>();
  @Output() private mandatorValueChanged = new EventEmitter<IMandatorLookup>();
  @Output() private invoiceDateValueChanged = new EventEmitter<Date>();
  @Output() private accountingTypeValueChanged = new EventEmitter<number>();
  @Output() private yearValueChanged = new EventEmitter<number>();
  @Output() private periodValueChanged = new EventEmitter<number>();
  @Output() private leadingAccountChanged = new EventEmitter<boolean>();
  @Output() private accrualAccountingChanged = new EventEmitter<boolean>();

  @ViewChild('mandatorInput', { static: true }) public mandatorInput: MandatorAutocompleteComponent;

  private modalRef: NzModalRef;
  public wording = wording;
  public form: FormGroup;
  public ACCOUNTING_TYPES = ACCOUNTING_TYPES;
  private enabledAccountingTypes: number[];
  public periods: number[] = [];
  public currentPeriods = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  public blockedPeriods: number[] = [];
  public closedPeriods: number[] = [];
  public voucherTypeGridConfig: NvGridConfig;
  public predefinedVoucherType = { abbreviation: null };
  public selectedBooking: IBooking;
  public selectedMandator: IMandatorLookup;
  public selectedVoucherType: IVoucherType;
  public serialNoValidators: ValidatorFn[];

  public mandatorNoModifier = mandatorNoModifier;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  private bookInvoiceMode: boolean;

  constructor(
    private formBuilder: FormBuilder,
    public mandatorRepo: MandatorRepositoryService,
    private bookingFormServce: BookingFormService,
    private exchangeRateService: ExchangeRateService,
    private mandatorValidatorsService: MandatorValidatorsService,
    private modalService: NzModalService,
    public settingsService: SettingsService,
    private currencyRepo: CurrencyRepositoryService,
    public voucherTypeRepo: VoucherTypeRepositoryService,
    public mandatorVoucherRepo: MandatorVoucherTypeRepositoryService,
    private voucherTypeValidatorsService: VoucherTypeValidatorsService,
    private mandatorClosedYearsRepo: MandatorClosedYearsRepositoryService,
    private mandatorBlockedYearsRepo: MandatorBlockedYearsRepositoryService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit() {
    this.voucherTypeGridConfig = voucherTypeGridConfig;
    this.bookInvoiceMode = !!this.route.snapshot.queryParams['book-invoice'];
    this.changeMandator$.subscribe(mandator => this.onMandatorChange(mandator));
    this.initForm();
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.currentBookingData$) {
      this.currentBookingData$.
        pipe(
          takeUntil(this.componentDestroyed$)
        ).subscribe(bookingData => {
          this.selectedVoucherType = bookingData.voucherType;
          this.selectedMandator = bookingData.mandator;
          this.selectedBooking = bookingData.booking;
          this.setFormValue(bookingData);
        });
    }
  }

  private initDate(): string {
    return new Date(moment().format('YYYY-MM-DD')).toISOString();
  }

  private getInitMonth(): number {
    return new Date().getMonth() + 1;
  }

  private setFormValue(bookingData: BookingFieldsData) {
    const state: IBookingFormState = {
      mandatorId: bookingData.mandator ? bookingData.mandator.id : null,
      mandatorNoDisabled: bookingData.mandator ? this.mandatorNoModifier(bookingData.mandator.no) : null,
      yearDisabled: bookingData.booking ? moment(bookingData.booking.year, 'YYYY').format('YY') : null,
      voucherTypeId: bookingData.booking ? bookingData.booking.voucherTypeId : null,
      year: bookingData.booking ? bookingData.booking.year.toString() : new Date().getFullYear().toString(),
      voucherDate: bookingData.booking && !this.cloneMode ? bookingData.booking.voucherDate : this.initDate(),
      invoiceDate: bookingData.booking && !this.cloneMode ? bookingData.booking.invoiceDate : this.initDate(),
      serialNo: bookingData.booking ? bookingData.booking.serialNo : null,
      period: bookingData.booking ? bookingData.booking.period : this.getInitMonth(),
      accountingType: bookingData.booking ? bookingData.booking.accountingType : null,
      defineInvoice: bookingData.booking ? bookingData.booking.defineInvoice : null,
      leadingAccount: bookingData.booking ? bookingData.booking.leadingAccount : null,
      accrualAccounting: bookingData.booking?.accrualAccounting || false,
    };
    this.updateFormValue(state);
  }


  private initForm() {
    this.form = this.formBuilder.group({
      mandatorId: [null, [
        Validators.required,
        this.mandatorValidatorsService.doesMandatorExist,
        this.mandatorValidatorsService.isMandatorActive
      ]],
      mandatorNoDisabled: [{ value: null, disabled: true }],
      yearDisabled: [{ value: null, disabled: true }],
      voucherTypeId: [null, [
        Validators.required,
        this.voucherTypeValidatorsService.doesVoucherTypeExist,
        this.voucherTypeValidatorsService.isVoucherTypeActive,
        this.voucherTypeValidatorsService.isVoucherTypeRelatedToMandator
      ]],
      year: [new Date().getFullYear(), [
        Validators.required,
        Validators.year,
        this.yearValidator,
        this.mandatorValidatorsService.isYearClosedForMandator,
        this.mandatorValidatorsService.isYearBlockedForMandator
      ]],
      voucherDate: [this.initDate(), [
        Validators.required,
        this.voucherDateValidator
      ]],
      invoiceDate: [this.initDate(), [Validators.required]],
      serialNo: [null, [this.isSerialNumberRequired]],
      period: [this.getInitMonth(), [
        Validators.required,
        this.mandatorValidatorsService.isPeriodClosedForMandator,
        this.mandatorValidatorsService.isPeriodBlockedForMandator,
      ]],
      accountingType: [null, [Validators.required]],
      defineInvoice: [{ value: false, disabled: true }],
      leadingAccount: [{ value: false, disabled: !this.createMode }],
      accrualAccounting: [{ value: false, disabled: !this.createMode }]
    });

    if (this.readMode) {
      this.form.disable({ emitEvent: false });
      this.bookingFormServce.bookingFormSubject.next(this.form);
    } else {
      this.bookingFormServce.bookingFormSubject.next(this.form);
      this.mandatorIdChanges(this.form);
      this.yearChanges(this.form);
      this.voucherTypeIdChanges(this.form);
      this.form.controls.accountingType.valueChanges.pipe(
        takeUntil(this.componentDestroyed$)
      ).subscribe((value) => this.accountingTypeValueChanged.emit(value));

      this.form.controls.invoiceDate.valueChanges.pipe(
        takeUntil(this.componentDestroyed$)
      ).subscribe((value) => this.invoiceDateValueChanged.emit(value));

      this.form.controls.period.valueChanges.pipe(
        takeUntil(this.componentDestroyed$)
      ).subscribe((value) => this.periodValueChanged.emit(value));

      this.form.controls.leadingAccount.valueChanges.pipe(
        takeUntil(this.componentDestroyed$)
      ).subscribe((value) => this.leadingAccountChanged.emit(value));

      this.form.controls.accrualAccounting.valueChanges.pipe(
        takeUntil(this.componentDestroyed$)
      ).subscribe((value) => this.accrualAccountingChanged.emit(value));

      this.initSerialNoChangeListener();
      this.invoiceDateChanges(this.form);
    }
  }

  private updateFormValue(state: IBookingFormState) {
    if (state) {
      this.form.setValue(state);
    }
  }

  private voucherTypeIdChanges = (form: FormGroup) => {
    form.controls.voucherTypeId.valueChanges.subscribe((value) => {
      const foundVoucherType = this.voucherTypeRepo.getStreamValue().find(voucherType => voucherType.id === +value);
      this.selectedVoucherType = foundVoucherType;

      this.form.controls.serialNo.updateValueAndValidity();
    });
  }

  private mandatorIdChanges(form: FormGroup) {
    form.controls.mandatorId.valueChanges.subscribe((value) => {
      const foundMandator: IMandatorLookup = this.mandatorRepo
        .getLookupStreamValue()
        .find(mandator => mandator.id === value);
      if (foundMandator) {
        this.onMandatorChange(foundMandator);
      }
      this.mandatorValueChanged.emit(foundMandator);
    });
  }

  private getChosenMandator(): IMandatorLookup {
    return this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === this.form.controls.mandatorId.value);
  }
  private yearChanges(form: FormGroup) {
    form.controls.year.valueChanges.subscribe(() => {
      this.periods = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
      const yearControl = form.get('year');
      this.yearValueChanged.emit(yearControl.value);
      const chosenMandator = this.getChosenMandator();
      const yearDisabledCtrl: AbstractControl = form.get('yearDisabled');
      if (yearControl.valid && chosenMandator) {
        const yearYYFormat = moment(+yearControl.value, 'YYYY').format('YY');
        yearDisabledCtrl.patchValue(yearYYFormat);
        const foundClosedYear = this.mandatorClosedYearsRepo.getStreamValue()
          .find(closedYear => closedYear.year === +yearControl.value
            && closedYear.mandatorId === chosenMandator.id);
        const foundBlockedYear = this.mandatorBlockedYearsRepo.getStreamValue()
          .find(blockedYear => blockedYear.year === +yearControl.value
            && blockedYear.mandatorId === blockedYear.id);

        if (foundClosedYear || foundBlockedYear) {
          if (
            foundClosedYear
            && foundClosedYear.period === 13
            || foundBlockedYear
            && foundBlockedYear.period === 13
          ) {
            // When period === 13 whole year is to be taken as closed
            this.currentPeriods = [];
          } else if (foundClosedYear && foundClosedYear.period === 12) {
            this.periods.push(13);
          } else {
            const fiscalYearPeriod = getFiscalCalendarDate(
              new Date(),
              chosenMandator.startFiscalYear
            ).getMonth() + 1;
            this.currentPeriods = this.periods.slice(0, fiscalYearPeriod);
          }
          this.blockedPeriods = foundBlockedYear
            ? this.periods.slice(0, foundBlockedYear.period)
            : [];
          this.closedPeriods = foundClosedYear
            ? this.periods.slice(0, foundClosedYear.period)
            : [];
        }
        if (+yearControl.value === new Date().getFullYear()) {
          this.currentPeriods = this.periods.slice(
            0,
            getFiscalCalendarDate(
              new Date(),
              chosenMandator.startFiscalYear).getMonth() + 1
          );
        } else {
          this.currentPeriods = this.periods;
        }
        const period = this.currentPeriods[this.currentPeriods.length - 1];
        this.form.controls.period.patchValue(period);
        form.get('period').updateValueAndValidity({ emitEvent: false });
        form.get('period').markAsTouched();
        return;
      }

      if (this.bookInvoiceMode || this.bookingFormServce.bookInvoiceMode) {
        return;
      }
      yearDisabledCtrl.patchValue(null);
    });
  }

  private yearValidator = (control: FormControl): ValidationErrors | null => {
    if (
      !control.value
      || !control.parent
      || !control.parent.get('mandatorId')
      || !control.parent.get('mandatorId').value
    ) {
      return null;
    }

    const year = control.value;
    const mandatorId = control.parent.get('mandatorId').value;

    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === mandatorId);

    if (!foundMandator) {
      return null;
    }
    const fiscalCalendarYear = getFiscalCalendarDate(
      new Date(),
      foundMandator.startFiscalYear
    ).getFullYear();
    return year >= foundMandator.firstBookingYear && year <= fiscalCalendarYear
      ? null
      : {
        invalidBookingYear: {
          message: setTranslationSubjects(
            wording.accounting.invalidBookingYear,
            { FIRST: foundMandator.firstBookingYear, SECOND: fiscalCalendarYear },
          ),
        }
      };
  }

  public onMandatorChange(mandator: IMandatorLookup): void {
    // Validation rule 1
    // https://navido.atlassian.net/wiki/spaces/TE/pages/376274945/Booking+form
    this.form.get('mandatorNoDisabled')
      .patchValue(this.mandatorNoModifier(mandator.no));

    // Workaround: Clear mandatorvouchertypes collection because it might be outdated
    // (e.g. might contain items already deleted by a different user or in a different tab)
    this.mandatorVoucherRepo.clearStream();
    this.mandatorVoucherRepo.fetchAll({ queryParams: { mandatorId: mandator.id } })
      .pipe(
        takeUntil(this.componentDestroyed$)
      ).subscribe(voucherTypes => {
        // Validation rule 5
        // https://navido.atlassian.net/wiki/spaces/TE/pages/376274945/Booking+form

        if (!this.bookInvoiceMode) {
          const voucherTypeIdControl = this.form.get('voucherTypeId');
          const predefinedVoucherType = this.voucherTypeRepo.getStreamValue()
            .find(voucherType => voucherType.abbreviation === 'ALL');

          if (predefinedVoucherType) {
            const isPredefinedRelatedToMandator = this.mandatorVoucherRepo.getStreamValue()
              .find(mandatorVoucherType => mandatorVoucherType.mandatorId === mandator.id
                && mandatorVoucherType.voucherTypeId === predefinedVoucherType.id);
            if (isPredefinedRelatedToMandator && !voucherTypeIdControl.value) {
              voucherTypeIdControl.patchValue(predefinedVoucherType.id);
            }
          }
          voucherTypeIdControl.updateValueAndValidity();
        }
      });

    const activeAccountingType = this.getMandatorActiveAccountingType(mandator.accountingType);
    this.form.get('accountingType').patchValue(activeAccountingType);

    if (!this.bookInvoiceMode) {
      mandator.startFiscalYear > 1
        ? this.form.controls.year.patchValue(new Date().getFullYear() + 1)
        : this.form.controls.year.patchValue(new Date().getFullYear());

    }
    this.form.get('year').updateValueAndValidity();
    this.form.get('year').markAsTouched();
  }

  // Validation rule 1
  // https://navido.atlassian.net/wiki/spaces/TE/pages/376274945/Booking+form
  private getMandatorActiveAccountingType(mandatorAccountingTypeBitmask): number {
    if (mandatorAccountingTypeBitmask) {
      this.updateMandatorEnabledAccountingTypes(mandatorAccountingTypeBitmask);
      const activeAccountingType = Math.min(
        ...this.enabledAccountingTypes
      );
      return activeAccountingType;
    }
    return null;
    // as ACCOUNTING_TYPES has this priority HGB, IFRS, USGAAP(1,2,4), this will get from
    // mandator's enabled accountingTypes minimum value, i.e highest priority
  }

  private updateMandatorEnabledAccountingTypes(accountingType: number): void {
    const accountingTypesBitmasks = [
      accountingType & ACCOUNTING_TYPES.HGB,
      accountingType & ACCOUNTING_TYPES.IFRS,
      accountingType & ACCOUNTING_TYPES.USGAAP,
      accountingType & ACCOUNTING_TYPES.EBalance,
    ];
    this.enabledAccountingTypes = accountingTypesBitmasks.filter(
      b => Object.values(ACCOUNTING_TYPES).includes(b)
    );
  }

  public isAccountingTypeDisabled(accountingType: number): boolean {
    const value = this.form.get('accountingType').value;
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === this.form.get('mandatorId').value);

    if (foundMandator) {
      this.updateMandatorEnabledAccountingTypes(foundMandator.accountingType);
      return value !== accountingType
        && !this.enabledAccountingTypes.includes(accountingType);
    }
    return true;
  }


  // Validation rule 3
  // https://navido.atlassian.net/wiki/spaces/TE/pages/376274945/Booking+form
  public isDefineInvoiceDisabled(): boolean {
    return false;
    // return !this.bookingLines.some(bl => bl.bookingAccount.accountingRelation === 2);
  }

  private voucherDateValidator = (control: FormControl): ValidationErrors | null => {
    const voucherDate: Date = control.value;
    const mandator = this.selectedMandator;
    if (!mandator || !voucherDate) { return null; }
    if (moment(voucherDate).isAfter(moment(new Date))) {
      return {
        invalidVoucherDate: {
          message: wording.accounting.voucherDateLessOrEqualCurrentDate
        }
      };
    }

    const fiscalCalendarDate = getFiscalCalendarDate(
      new Date(voucherDate)
      , mandator.startFiscalYear
    ).getFullYear();

    if (moment(mandator.firstBookingYear).isAfter(fiscalCalendarDate)) {
      return {
        invalidVoucherDate: {
          message:
            `${wording.accounting.voucherDateGreaterOrEqualFirstBookingYear[this.settingsService.language]}
            (${mandator.firstBookingYear})`
        }
      };
    }

    return null;
  }

  public jumpToGrid(event: KeyboardEvent) {
    if (event instanceof KeyboardEvent) {
      event.preventDefault();
    }
    if (this.form.valid) {
      this.jumpToGridEmitter.emit();
    } else {
      validateAllFormFields(this.form, { emitEvent: false });
    }
  }

  public jumpToCell(event: KeyboardEvent) {
    const inputElementsArray: HTMLInputElement[] = Array.from(document.querySelectorAll('input'));

    const currentIndex = inputElementsArray.findIndex(element => element === event.target);
    const nextElement: HTMLInputElement =
      inputElementsArray.find((element, index) => !element.disabled && index > currentIndex);

    if (nextElement) {
      nextElement.focus();
    }
  }

  public jump(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      this.jumpToCell(event);
    }
  }

  /**
  * This function watches for value changes on invoice date control
  * fetches list of exchage rates and caches it in exchangeRateservice
  * later it can be used in booking lines
  * @param {FormGroup} form
  */
  private invoiceDateChanges = (form: FormGroup) => {
    form.controls.invoiceDate.valueChanges
      .pipe(
        startWith(form.controls.invoiceDate.value),
        skipWhile(date => !date),
        debounceTime(1000),
        distinctUntilChanged(),
        switchMap(date => {
          const endDate = moment(date).format('YYYY-MM-DD');
          const startDate = moment(date).format('YYYY-MM-DD');
          return this.exchangeRateService.fetchAll({ queryParams: { startDate, endDate } });
        })
      ).subscribe();
  }

  public openRatesCalendar = () => {
    const mandatorId = this.form.get('mandatorId').value;
    let foundMandator: IMandatorLookup;
    if (mandatorId) {
      foundMandator = this.mandatorRepo
        .getLookupStreamValue()
        .find(mandator => mandator.id === mandatorId);
    }
    if (foundMandator && foundMandator.currencyId) {
      this.modalRef = this.modalService.create({
        nzWidth: 'min-content',
        nzTitle: wording.general.midrateCalculation[this.settingsService.language],
        nzClosable: true,
        nzContent: RangeCalendarComponent,
        nzComponentParams: {
          localCurrencyId: foundMandator.currencyId,
          currencies: this.currencyRepo.filterActiveOnly(this.currencyRepo.getStreamValue()),
        },
        nzFooter: null,
        nzOnCancel: () => this.modalRef.destroy(),
      });
    }
  }

  public padStartParser = (value: number) => {
    if (value && !isNaN(value) && value > -1) {
      return parseInt(value.toString(), 10)
        .toString()
        .padStart(5, '0')
        .slice(0, 5);
    }
    return null;
  }

  public initSerialNoChangeListener(): void {
    const { serialNo } = this.form.controls;

    serialNo.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(value => {
        serialNo.setValue(this.padStartParser(value), { emitEvent: false });
      });
  }

  private isSerialNumberRequired = (control: FormControl): ValidationErrors | null => {
    if (
      !control.parent
      || !control.parent.get('voucherTypeId')
    ) {
      return null;
    }

    const voucherTypeId = +control.parent.get('voucherTypeId').value;
    const foundVoucherType = this.voucherTypeRepo.getStreamValue()
      .find(voucherType => voucherType.id === voucherTypeId);
    const isValid = foundVoucherType ? foundVoucherType.isAutomaticNumbering : false;
    if (isValid) {
      return null;
    }
    if (control.value) {
      control.setValidators([
        this.isSerialNumberRequired,
        Validators.min(1),
        Validators.max(99999)
      ]);
      return null;
    }
    markControlAsTouched(control);

    return {
      required: {
        message: wording.general.theFieldIsRequired
      }
    };
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public resetForm(): void {
    const mandatorId = this.form.get('mandatorId').value;

    this.initForm();

    this.form.markAsPristine();

    if (mandatorId) {
      this.form.get('mandatorId').setValue(mandatorId);
    }

    this.cdr.detectChanges();

    setTimeout(() => {
      this.focusMandatorInput();
    });
  }

  private focusMandatorInput(): void {
    this.mandatorInput.focus();
  }
}
