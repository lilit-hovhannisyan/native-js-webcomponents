
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { wording } from 'src/app/core/constants/wording/wording';
import { SettingsService } from 'src/app/core/services/settings.service';
import { IUser } from 'src/app/core/models/resources/IUser';

@Injectable()
export class UsersHelperService {
  public editViewTitleSubject: BehaviorSubject<string> = new BehaviorSubject('');
  public editViewTitle$ = this.editViewTitleSubject.asObservable();
  constructor(
    public settingsService: SettingsService,
  ) { }

  public generateTitle = (user?: IUser) => {
    let title = wording.general.create[this.settingsService.language];

    if (user) {
      title = user.username;
    }

    this.editViewTitleSubject.next(title);
  }
}
