import { Component, OnInit } from '@angular/core';
import { RoutingHistoryService } from './routing/services/routing-history.service';
import { Language } from './core/models/language';
import { SettingsService } from './core/services/settings.service';
import { NotificationsService } from './core/services/notification.service';

@Component({
  selector: 'nv-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  constructor(
    private routingHistory: RoutingHistoryService,
    private notificationsService: NotificationsService,
    private settingsService: SettingsService,
  ) { }

  public ngOnInit(): void {
    this.setDefaultLanguage();
    this.notificationsService.initNotifications();
    this.routingHistory.loadRouting();
  }

  private setDefaultLanguage() {
    this.settingsService.language = Language.EN;
  }
}
