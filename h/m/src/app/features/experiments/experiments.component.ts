import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

import { MeBasicAlertService } from '@shared-components/me-basic-alert/me-basic-alert.service';
import { MeButtonI } from '@shared-components/me-button/me-button.interface';
import { MeMaterialCardI } from '@shared-components/me-material-card/me-material-card.interface';
import { MeCollectionCardI } from '@shared/me-components/me-collection-card/me-collection-card.interface';
import { MeDocumentCardI } from '@shared/me-components/me-document-card/me-document-card.interface';
import { MeSearchBarI } from '@shared/me-components/me-search-bar/me-search-bar.interface';
import { MeEmptyContentI } from '@shared/me-components/me-empty-content/me-empty-content-interface';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from 'src/app/shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { MeRightSidebarI, TabTypeEnum } from '@shared/me-components/me-right-sidebar/me-right-sidebar.interface';
import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';
import { MeBooleanInputComponent } from '@shared/me-components/me-attribute/me-boolean-input/me-boolean-input.component';
import {
  DMBooleanAttributeModel,
  DMChoiceAttributeModel,
  DMDateTimeAttributeModel,
  DMMOAListAttributeModel,
  DMStringAttributeModel,
  DMTextAttributeModel
} from '@shared/models/attribute/attribute.model';
import { PropertiesModel } from '@shared/models/attribute/attribute.properties.model';
import { MeBooleanInputAttribute } from '@shared/models/attribute/me-boolean-input-attribute.model';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MeTextInputAttribute } from '@shared/models/attribute/me-text-input-attribute.model';
import { MeTextInputComponent } from '@shared/me-components/me-attribute/me-text-input/me-text-input.component';
import { MeMultiSelectInputAttribute } from '@shared/models/attribute/me-multi-select-input-attribute.model';
import { MeMultiSelectInputComponent } from '@shared/me-components/me-attribute/me-multi-select-input/me-multi-select-input.component';
import { MeSelectInputAttribute } from '@shared/models/attribute/me-select-input-attribute.model';
import { MeSelectInputComponent } from '@shared/me-components/me-attribute/me-select-input/me-select-input.component';
import { MeTextAreaInputAttribute } from '@shared/models/attribute/me-text-area-input-attribute.model';
import { MeTextAreaInputComponent } from '@shared/me-components/me-attribute/me-text-area-input/me-text-area-input.component';

import { MeUserGroupCardI } from '@shared-components/me-user-group-card/me-user-group-card.interface';

import { MePopupSelectService } from '@shared-components/me-popup-select/me-popup-select.service';
import { MePopupSelectI } from '@shared-components/me-popup-select/me-popup-select.interface';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';
import { MeAddListCardI } from '@shared-components/me-add-list-card/me-add-list-card.interface';
import { MeTabGroupI } from '@shared/me-components/me-tab-group/me-tab-group.interface';
import { RightSidebarSupportService } from '@core-services/right-sidebar-support.service';
import { MeInfoCardI } from '@shared/me-components/me-info-card/me-info-card.interface';
import { MeMaterialTypeCardI } from '@shared/me-components/me-material-type-card/me-material-type-card.interface';
import { MeAdvancedFilterI } from '@shared/me-components/me-advanced-filter/me-advanced-filter.interface';
import { DeactivatableComponent } from '@core-interfaces/deactivatable-component.interface';
import { RouterStoreService } from '@core-services/router-store.service';
import { Router } from '@angular/router';
import { MeDateTimeInputAttribute } from '@shared/models/attribute/me-date-time-input-attribute.model';
import { MeDateTimeInputComponent } from '@shared/me-components/me-attribute/me-date-time-input/me-date-time-input.component';
import { mapExtensionToIcon } from '@shared/utils';
import { MePopupVideoService } from '@shared/me-components/me-popup-video/me-popup-video.service';

@Component({
  selector: 'me-experiments',
  templateUrl: './experiments.component.html',
  styleUrls: ['./experiments.component.scss']
})
export class ExperimentsComponent implements OnInit, AfterViewInit, DeactivatableComponent {
  openBasicAlerts: boolean = false;
  isLoading: boolean = true;

  applyCheck: boolean = false;
  leaveWarningModel!: MeSweetAlertI;

  @ViewChild('popupDocumentGrid', { read: TemplateRef }) popupDocumentGridRef!: TemplateRef<Component>;
  @ViewChild('createButton', { read: TemplateRef }) createButton!: TemplateRef<Component>;
  @ViewChild('infoButton', { read: TemplateRef }) infoButton!: TemplateRef<Component>;
  @ViewChild('filterButton', { read: TemplateRef }) filterButton!: TemplateRef<Component>;
  private _selectedMap: Map<string, void> = new Map();
  documentPopupData!: MePopupSelectI;

  buttonDataModel: MeButtonI = { type: 'regular', color: 'primary' };
  materialDataModel: MeMaterialCardI = {
    contextMenuItems: [{ key: 'open', value: 'Open' }],
    materialName: 'Test material name reallllyyyy realllyyyy long',
    selectionEnabled: false,
    materialUOM: 'm',
    thumbnail: 'https://baconmockup.com/500/500/',
    selected: false,
    materialPrice: '$5',
    oid: '123',
    supplierOid: '12345',
    supplierName: 'Company name 123'
  };

  userGroupCardDataModel: MeUserGroupCardI = {
    contextMenuButton: { type: 'transparent', color: 'warning' },
    contextMenuItems: [
      { key: 'edit', value: 'Edit' },
      { key: 'manageGroups', value: 'Manage Groups' }
    ],
    firstName: 'Firstname',
    lastName: 'Lastname',
    status: true,
    groups: 2,
    oid: '123',
    selected: false,
    selectionEnabled: false,
    thumbnail: 'https://baconmockup.com/500/500/',
    type: 'user',
    statusLabel: { type: 'inversed', color: 'primary', margin: 'none' }
  };

  addListCardDataModelUser: MeAddListCardI = {
    firstName: 'Zoran',
    lastName: 'Zoranovic',
    uid: 'zoki',
    oid: '123',
    thumbnail: 'https://baconmockup.com/500/500/',
    type: 'user',
    showButton: true
  };

  addListCardDataModelGroup: MeAddListCardI = {
    groupName: 'Admins',
    groupRole: 'Sys Admin',
    description: 'Network management',
    oid: '123',
    thumbnail: 'https://baconmockup.com/500/500/',
    type: 'group',
    showButton: false
  };

  attributeItem!: MeAttributeItem;
  attributeItem2!: MeAttributeItem;
  attributeItem3!: MeAttributeItem;
  attributeItem4!: MeAttributeItem;
  attributeItem5!: MeAttributeItem;
  attributeItem6!: MeAttributeItem;

  advancedFilter: MeAdvancedFilterI = {
    active: true,
    icon: 'filter',
    resetMessage: 'Reset all filters'
  };

  componentHeader: MeSearchBarI = {
    filterEnabled: true,
    search: {
      placeholder: 'Search for...',
      keyword: ''
    },
    sort: {
      placeholder: 'Sort by',
      options: [
        { key: 'newest', value: 'Newest' },
        { key: 'transparent', value: 'Transparent' }
      ],
      selectedIndex: 0
    },
    selectButton: {
      enabled: true,
      placeholder: 'Select'
    },
    operationButtons: [
      {
        button: { type: 'regular', rounded: false, color: 'link' },
        iFeatherName: 'edit',
        clickEventName: 'edit',
        tooltipText: 'Edit'
      },
      {
        button: { type: 'regular', rounded: false, color: 'link' },
        iFeatherName: 'trash',
        clickEventName: 'trash',
        tooltipText: 'Trash'
      },
      {
        button: { type: 'regular', rounded: false, color: 'link' },
        iFeatherName: 'copy',
        clickEventName: 'copy',
        tooltipText: 'Copy'
      },
      {
        button: { type: 'regular', rounded: false, color: 'link' },
        iFeatherName: 'more-vertical',
        clickEventName: 'more',
        tooltipText: 'More options'
      }
    ],
    view: 'grid'
  };

  collection: MeCollectionCardI = {
    selectionEnabled: false,
    contextMenu: {
      contextMenuItems: [
        { key: 'first', value: 'First option' },
        { key: 'second', value: 'Second option' }
      ]
    },

    oid: 'string',
    userProfile: {
      type: 'initials',
      size: 'small',
      shape: 'circled',
      initials: 'AV',
      imageUrl: '/assets/images/mev-black-logo.png',
      alt: 'Nicht arbeit'
    },
    creator: 'Creator',
    name: 'Name',
    numberOfMaterials: 11,
    numberOfCustomers: 22,
    numberOfTeams: 33,
    restMaterialsNumber: 44,
    firstThreeMaterialsThumbnailUrl: [
      '/assets/images/mev-black-logo.png',
      '/assets/images/mev-black-logo.png',
      '/assets/images/mev-black-logo.png'
    ],
    lastUpdatedPlaceholder: 'Last updated:',
    dateDiff: '01.01.2000',
    selected: false
  };

  typeCards: MeMaterialTypeCardI[] = [
    { title: 'Leather', type: 'Leather', icon: 'leather-material-type', selected: false },
    { title: 'Synthetic', type: 'Leather', icon: 'synthetic-material-type', selected: false },
    { title: 'Leather', type: 'Leather', icon: 'plus', selected: true },
    { title: 'Leather', type: 'Leather', icon: 'leather-material-type', selected: true }
  ];

  documents: MeDocumentCardI[] = [
    {
      contextMenu: {
        contextMenuItems: [
          { key: 'first', value: 'First option' },
          { key: 'second', value: 'Second option' }
        ]
      },
      creationDate: '22/11/2021',
      documentExtensionIcon: mapExtensionToIcon('3dm'),
      documentName: 'somedocumentname.png',
      selected: false,
      selectionEnabled: true,
      oid: '1'
    },
    {
      contextMenu: {
        contextMenuItems: [
          { key: 'first', value: 'First option' },
          { key: 'second', value: 'Second option' }
        ]
      },
      creationDate: '22/11/2021',
      documentExtensionIcon: mapExtensionToIcon('docx'),
      documentName: 'somedocumentname.png',
      selected: false,
      selectionEnabled: true,
      oid: '2'
    },
    {
      contextMenu: {
        contextMenuItems: [
          { key: 'first', value: 'First option' },
          { key: 'second', value: 'Second option' }
        ]
      },
      creationDate: '22/11/2021',
      documentExtensionIcon: mapExtensionToIcon('jpg'),
      documentName: 'somedocumentname.png',
      selected: false,
      selectionEnabled: true,
      oid: '4'
    },
    {
      contextMenu: {
        contextMenuItems: [
          { key: 'first', value: 'First option' },
          { key: 'second', value: 'Second option' }
        ]
      },
      creationDate: '22/11/2021',
      documentExtensionIcon: mapExtensionToIcon('pdf'),
      documentName: 'somedocumentname.png',
      selected: false,
      selectionEnabled: true,
      oid: '3'
    },
    {
      contextMenu: {
        contextMenuItems: [
          { key: 'first', value: 'First option' },
          { key: 'second', value: 'Second option' }
        ]
      },
      creationDate: '22/11/2021',
      documentExtensionIcon: mapExtensionToIcon('png'),
      documentName: 'somedocumentname.png',
      selected: false,
      selectionEnabled: true,
      oid: '5'
    },
    {
      contextMenu: {
        contextMenuItems: [
          { key: 'first', value: 'First option' },
          { key: 'second', value: 'Second option' }
        ]
      },
      creationDate: '22/11/2021',
      documentExtensionIcon: mapExtensionToIcon('png'),
      documentName: 'somedocumentname.png',
      selected: false,
      selectionEnabled: true,
      oid: '6'
    },
    {
      contextMenu: {
        contextMenuItems: [
          { key: 'first', value: 'First option' },
          { key: 'second', value: 'Second option' }
        ]
      },
      creationDate: '22/11/2021',
      documentExtensionIcon: mapExtensionToIcon('xlsx'),
      documentName: 'somedocumentname asdasdasd.png',
      selected: false,
      selectionEnabled: true,
      oid: '7'
    },
    {
      contextMenu: {
        contextMenuItems: [
          { key: 'first', value: 'First option' },
          { key: 'second', value: 'Second option' }
        ]
      },
      creationDate: '22/11/2021',
      documentName: 'somedocumentname.png',
      documentThumbnail: 'https://baconmockup.com/500/500/',
      selected: false,
      selectionEnabled: true,
      oid: '8'
    }
  ];
  emptyContent: MeEmptyContentI = {
    title: 'Some title that is weeeeeeeeeeeeeeery long',
    comment: 'Here comes some comment Here comes some comment Here comes some comment Here comes some comment Here comes some comment',
    buttonText: 'Add',
    emptyIconName: 'empty-colleagues'
  };
  rightSidebar: MeRightSidebarI = {
    tabs: [
      {
        label: 'Prvi tab',
        active: true,
        type: {
          id: TabTypeEnum.overview,
          content: {
            thumbnailUrl: 'https://baconmockup.com/500/500/',
            title: 'Naslov',
            subtitle: 'Create by Scepan Jankovic',
            status: 'string',
            contextMenu: {
              contextMenuItems: [
                { key: 'prvi', value: 'Prviii' },
                { key: 'drugi', value: 'Druuugi' }
              ]
            },
            paragraph:
              'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum ',
            video: { url: '', type: 'video/mp4', alt: 'Video nicht arbeit' }
          }
        }
      },
      {
        label: 'Drugi tab',
        active: true,
        type: {
          id: TabTypeEnum.documents,
          content: {
            search: {
              keyword: 'string',
              placeholder: 'Search for...'
            },
            allFilesPlaceholder: 'All files',
            extensions: [
              {
                id: 'pdf',
                displayValue: 'pdf'
              },
              {
                id: 'jpg',
                displayValue: 'jpg'
              },
              {
                id: 'jpeg',
                displayValue: 'jpeg'
              }
            ],
            documentCards: [
              {
                id: '111',
                oid: '111',
                contextMenu: {
                  contextMenuItems: [
                    { key: 'first', value: 'First option' },
                    { key: 'second', value: 'Second option' }
                  ]
                },
                creationDate: '22/11/2021',
                documentExtensionIcon: mapExtensionToIcon('3dm'),
                documentName: 'somedocumentname.png',
                selected: false,
                selectionEnabled: true
              },
              {
                id: '222',
                oid: '222',
                contextMenu: {
                  contextMenuItems: [
                    { key: 'first', value: 'First option' },
                    { key: 'second', value: 'Second option' }
                  ]
                },
                creationDate: '22/11/2021',
                documentExtensionIcon: mapExtensionToIcon('jpg'),
                documentName: 'somedocumentname.png',
                selected: false,
                selectionEnabled: true
              },
              {
                id: '333',
                oid: '333',
                contextMenu: {
                  contextMenuItems: [
                    { key: 'first', value: 'First option' },
                    { key: 'second', value: 'Second option' }
                  ]
                },
                creationDate: '22/11/2021',
                documentExtensionIcon: mapExtensionToIcon('pdf'),
                documentName: 'somedocumentname.png',
                selected: false,
                selectionEnabled: true
              },
              {
                id: '444',
                oid: '444',
                contextMenu: {
                  contextMenuItems: [
                    { key: 'first', value: 'First option' },
                    { key: 'second', value: 'Second option' }
                  ]
                },
                creationDate: '22/11/2021',
                documentExtensionIcon: mapExtensionToIcon('png'),
                documentName: 'somedocumentname.png',
                selected: false,
                selectionEnabled: true
              },
              {
                id: '555',
                oid: '555',
                contextMenu: {
                  contextMenuItems: [
                    { key: 'first', value: 'First option' },
                    { key: 'second', value: 'Second option' }
                  ]
                },
                creationDate: '22/11/2021',
                documentExtensionIcon: mapExtensionToIcon('png'),
                documentName: 'somedocumentname.png',
                selected: false,
                selectionEnabled: true
              },
              {
                id: '666',
                oid: '666',
                contextMenu: {
                  contextMenuItems: [
                    { key: 'first', value: 'First option' },
                    { key: 'second', value: 'Second option' }
                  ]
                },
                creationDate: '22/11/2021',
                documentName: 'somedocumentname.png',
                documentThumbnail: 'https://baconmockup.com/500/500/',
                selected: false,
                selectionEnabled: true
              }
            ]
          }
        }
      }
    ]
  };

  @ViewChild('tabOne', { read: TemplateRef }) tabOne!: TemplateRef<Component>;
  @ViewChild('tabTwo', { read: TemplateRef }) tabTwo!: TemplateRef<Component>;
  @ViewChild('labelOne', { read: TemplateRef }) labelOne!: TemplateRef<Component>;
  @ViewChild('labelTwo', { read: TemplateRef }) labelTwo!: TemplateRef<Component>;
  meTabGroup: MeTabGroupI = {
    tabs: []
  };

  meInfoCardOne: MeInfoCardI = {
    thumbnail: 'https://baconmockup.com/500/500/',
    title: 'Collection N_1',
    subtitle: 'Created by Alex Smirnov',
    description: 'Public',
    contextMenu: {
      contextMenuItems: [
        {
          key: 'some',
          value: 'Some'
        }
      ]
    },
    label: {
      color: 'success',
      type: 'regular',
      margin: 'none'
    }
  };

  meInfoCardTwo: MeInfoCardI = {
    thumbnail: 'https://baconmockup.com/500/500/',
    title: 'WRAP_Certificate.pdfsssss',
    subtitle: 'Created by Alex Smirnov',
    description: '07/10/20',
    contextMenu: {
      contextMenuItems: [
        {
          key: 'some',
          value: 'Some'
        }
      ]
    }
  };

  @ViewChild('rightSidebarContent', { read: TemplateRef }) rightSidebarContent!: TemplateRef<Component>;

  constructor(
    private router: Router,
    private basicAlertService: MeBasicAlertService,
    private sweetAlertService: MeSweetAlertService,
    private popupSelectService: MePopupSelectService,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private rightSidebarSupportService: RightSidebarSupportService,
    private routerStoreService: RouterStoreService,
    private videoPopupService: MePopupVideoService
  ) {
    const a: DMBooleanAttributeModel = {
      constraints: [],
      dataTypeClass: 'boolean',
      displayName: 'Test input',
      internalName: 'testInput',
      properties: new PropertiesModel(),
      required: true,
      optRequired: false,
      value: false,
      disabled: false,
      unique: false
    };
    const b: MeBooleanInputAttribute = new MeBooleanInputAttribute();
    b.data = a;
    b.form = new FormGroup({ [a.internalName]: new FormControl(a.value, Validators.required) });
    this.attributeItem = new MeAttributeItem(MeBooleanInputComponent, b);

    const c: DMStringAttributeModel = {
      constraints: [],
      dataTypeClass: 'string',
      displayName: 'Test input',
      internalName: 'testInput',
      properties: new PropertiesModel(),
      required: true,
      optRequired: false,
      value: 'BorkoBorkoBorkoBorkoBorko',
      minLength: 10,
      maxLength: 20,
      disabled: false,
      unique: false
    };
    const d: MeTextInputAttribute = new MeTextInputAttribute();
    d.data = c;
    d.form = new FormGroup({
      [c.internalName]: new FormControl(c.value, [
        Validators.required,
        Validators.minLength(c.minLength),
        Validators.maxLength(c.maxLength)
      ])
    });
    d.inputErrors = {
      formControl: d.form.get(c.internalName) as AbstractControl,
      errors: [
        { key: 'required', value: 'this is required' },
        { key: 'minlength', value: 'min length is 10' },
        { key: 'maxlength', value: 'max length is 20' }
      ]
    };
    this.attributeItem2 = new MeAttributeItem(MeTextInputComponent, d);

    const e: DMMOAListAttributeModel = {
      constraints: [],
      dataTypeClass: 'moa',
      displayName: 'Test input',
      internalName: 'testInput',
      properties: new PropertiesModel(),
      required: true,
      optRequired: false,
      value: ['test1'],
      values: [
        { displayName: 'test 1', value: 'test1' },
        { displayName: 'test 2', value: 'test2' }
      ],
      disabled: false,
      unique: false
    };

    const f: MeMultiSelectInputAttribute = new MeMultiSelectInputAttribute();
    f.data = e;
    f.form = new FormGroup({
      [e.internalName]: new FormControl(e.value, [Validators.required])
    });

    this.attributeItem3 = new MeAttributeItem(MeMultiSelectInputComponent, f);

    const g: DMChoiceAttributeModel = {
      constraints: [],
      dataTypeClass: 'moa',
      displayName: 'Test input',
      internalName: 'testInput',
      properties: new PropertiesModel(),
      required: true,
      optRequired: false,
      value: 'test1',
      values: [
        { displayName: 'test 1', value: 'test1' },
        { displayName: 'test 2', value: 'test2' }
      ],
      disabled: false,
      unique: false
    };

    const h: MeSelectInputAttribute = new MeSelectInputAttribute();
    h.data = g;
    h.form = new FormGroup({
      [g.internalName]: new FormControl(g.value, [Validators.required])
    });

    this.attributeItem4 = new MeAttributeItem(MeSelectInputComponent, h);

    const i: DMTextAttributeModel = {
      constraints: [],
      dataTypeClass: 'moa',
      displayName: 'Test input',
      internalName: 'testInput',
      properties: new PropertiesModel(),
      required: true,
      optRequired: false,
      value: 'test1',
      minLength: 0,
      maxLength: 100,
      disabled: false,
      unique: false
    };

    const j: MeTextAreaInputAttribute = new MeTextAreaInputAttribute();
    j.data = i;
    j.form = new FormGroup({
      [i.internalName]: new FormControl(i.value, [Validators.required])
    });

    this.attributeItem5 = new MeAttributeItem(MeTextAreaInputComponent, j);

    const dateAttrM: DMDateTimeAttributeModel = {
      constraints: [],
      dataTypeClass: 'date',
      displayName: 'Date input',
      internalName: 'dateInput',
      properties: new PropertiesModel(),
      required: true,
      optRequired: false,
      value: '2021-02-17T09:14:35.995657Z',
      disabled: false,
      unique: false
    };

    const date: MeDateTimeInputAttribute = new MeDateTimeInputAttribute();
    date.data = dateAttrM;
    date.form = new FormGroup({
      [dateAttrM.internalName]: new FormControl(dateAttrM.value, [Validators.required])
    });

    this.attributeItem6 = new MeAttributeItem(MeDateTimeInputComponent, date);
  }

  ngOnInit(): void {
    // testing on push change detenction
    setTimeout(() => {
      this.buttonDataModel = { type: 'regular', color: 'warning' };
      this.materialDataModel = {
        contextMenuItems: [
          { key: 'open', value: 'Open' },
          { key: 'duplicate', value: 'Duplicate' }
        ],
        materialName: 'Short material name',
        selectionEnabled: false,
        materialUOM: 'km',
        thumbnail: 'https://baconmockup.com/500/500/',
        selected: false,
        materialPrice: '$500',
        oid: '123',
        supplierOid: '12345',
        supplierName: 'Company name 123'
      };
    }, 5000);

    setTimeout(() => {
      this.isLoading = false;
    }, 1000);

    if (this.openBasicAlerts) {
      this.basicAlertService.openBasicAlert({
        content: 'test content for alert',
        mode: 'error',
        title: 'test title 1'
      });

      this.sweetAlertService.openMeSweetAlert({
        icon: 'grid',
        message: 'Test sweet message',
        mode: 'warning',
        title: 'Test sweet title',
        type: { name: MeSweetAlertTypeEnum.confirm, buttons: { confirm: 'Confirm' } }
      });
    }

    setTimeout(() => {
      this.componentHeader = { ...this.componentHeader, search: { placeholder: 'Search for...', keyword: 'new keyword' } };
    }, 3000);

    this.leaveWarningModel = {
      icon: 'alert-triangle',
      mode: 'warning',
      title: 'Test sweet title',
      message: 'Test sweet message',
      type: { name: MeSweetAlertTypeEnum.submit, buttons: { submit: 'Leave', cancel: 'Stay on page' } }
    };
  }

  ngAfterViewInit(): void {
    this.documentPopupData = { selectedCount: 0, title: 'Add documents' };
    // Put in comment this line of code to stop producing popup
    // this.popupSelectService.openPopupSelect(this.documentPopupData, this.popupDocumentGridRef);

    this.breadcrumbSupportService.breadcrumbActions = [
      {
        button: {
          id: '1',
          data: { color: 'primary', type: 'transparent' },
          text: 'Create new',
          content: this.createButton
        },
        callBack: () => {
          this.basicAlertService.openBasicAlert({
            title: 'Create new button work',
            content: 'This button work',
            mode: 'success'
          });
        }
      },
      {
        button: {
          id: '3',
          data: { color: 'secondary', type: 'regular' },
          text: 'Open filter',
          content: this.infoButton
        },
        callBack: () => {
          if (!this.rightSidebarSupportService.rightSidebarContent) {
            this.rightSidebarSupportService.rightSidebarContent = this.rightSidebarContent;
          } else {
            this.rightSidebarSupportService.rightSidebarContent = null;
          }
        }
      },
      {
        button: {
          id: '2',
          data: { color: 'secondary', type: 'regular' },
          text: 'Info button',
          content: this.filterButton
        },
        callBack: () => {
          this.toggleFilter({ eventName: 'close' });
        }
      },
      {
        button: {
          id: '4',
          data: { color: 'primary', type: 'transparent' },
          text: 'Activate leave guard'
        },
        callBack: () => {
          // first click artivate leave guard
          if (!this.applyCheck) {
            this.applyCheck = true;
          } else {
            // second click work as cancel
            // NOTE: u have to check is there previous route!!!!!
            const path = this.routerStoreService.getPreviousUrl();
            if (path === '/experiments') {
              this.router.navigate(['/']);
            } else {
              this.router.navigate([path]);
            }
          }
        }
      },
      {
        button: {
          id: '5',
          data: { color: 'primary', type: 'transparent' },
          text: 'Open video popup'
        },
        callBack: () => {
          this.videoPopupService.openVideoPopupAlert({
            videoPath: 'http://localhost:4200/webapi/v1/marketplace/files/download/OR:MTFileStore:37880'
          });
        }
      }
    ];

    this.meTabGroup = {
      tabs: [
        { index: 0, displayName: 'Tab 1', content: this.tabOne, labelContent: this.labelOne },
        { index: 1, displayName: 'Tab 2', content: this.tabTwo, labelContent: this.labelTwo }
      ]
    };
  }

  toggleFilter(event: { eventName: string }): void {
    if (event.eventName === 'close') {
      this.advancedFilter = {
        ...this.advancedFilter,
        active: !this.advancedFilter.active
      };
    }
  }

  documentClickHandler(index: number): void {
    if (this._selectedMap.has(this.documents[index].oid)) {
      this.documents[index] = { ...this.documents[index], selected: false };
      this._selectedMap.delete(this.documents[index].oid);
    } else {
      this.documents[index] = { ...this.documents[index], selected: true };
      this._selectedMap.set(this.documents[index].oid);
    }
    this.popupSelectService.pushDataModelToPopup({ ...this.documentPopupData, selectedCount: this._selectedMap.size });
  }

  documentOid(_index: number, item: MeDocumentCardI): string {
    return item.oid;
  }

  componentHeaderEventHandler(event: { eventName: string; payload?: string | boolean }): void {
    switch (event.eventName) {
      case 'searchEnabledToggle':
      // TODO
      case 'searchKeyword':
        // TODO
        console.log(event);
        this.componentHeader = { ...this.componentHeader, search: { placeholder: 'Search for...', keyword: event.payload as string } };
        break;
      case 'sortBy':
        // TODO
        if (this.componentHeader.sort) {
          this.componentHeader.sort.selectedIndex = this.componentHeader.sort.options.findIndex((x) => x.key === event.payload);
        }
        break;
      case 'select':
        // TODO
        if (this.componentHeader.selectButton) {
          this.componentHeader.selectButton.enabled = false;
        }
        break;
      case 'download':
        // TODO
        break;
      case 'view':
        // TODO
        this.componentHeader.view === 'grid' ? (this.componentHeader.view = 'list') : (this.componentHeader.view = 'grid');
        break;
    }
  }

  event(event: { eventName: string; payload?: string | undefined }): void {
    console.log(event);
  }

  clickHandler(): void {
    // TODO
    console.log('empty content button clicked');
  }
}
