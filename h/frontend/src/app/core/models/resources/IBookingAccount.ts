import { ACCOUNTING_RELATIONS } from '../enums/accounting-relations';
import { ACCOUNTING_TYPES } from '../enums/accounting-types';
import { DEBITOR_CREDITOR } from '../enums/debitor-creditor';
import { Language } from '../language';
import { IResource } from './IResource';
import { IWithActive } from './IWithActive';
import { IWithLabel } from './IWithLabel';
import { IWithLabels } from './IWithLabels';
import { IWording } from './IWording';

export type BookingAccountKey = number;

interface ISharedProps extends IWithLabels, IWithActive {
  no: number;
  isCostAccounting: boolean;
  isOpenItem: boolean;
  isOpex: boolean;
  isActive: boolean;
  isSwitchingAccount: boolean;
  isAccruedAndDeferredItem: boolean;
  accountTypeAccruals: number;
  isAccrualDefaultPrepaid: boolean;
  isAccrualDefaultDeferred: boolean;
  remark: string;
  accountingType: ACCOUNTING_TYPES;
  currencyId: number;
  assetAccount: boolean;
  changeIsOpenForMandators: boolean;
  changeIsOpexForMandators: boolean;
  changeRestrictedCurrencyForMandators: boolean;
  labelDe: string;
  labelEn: string;
  accountingRelation: ACCOUNTING_RELATIONS;
  debitorCreditorType: DEBITOR_CREDITOR;
  accrualBookingAccountId: number;
}

export interface IBookingAccountBase extends ISharedProps, IResource<number> { }

export interface IBookingAccountCreate extends ISharedProps { }

export interface IBookingAccountDTO extends IBookingAccountBase { }

export interface IBookingAccount extends IBookingAccountDTO, IWithLabel { }

export interface IBookingAccountWithCurrency extends IBookingAccount {
  currencyCode: IWording;
}
export interface IBookingAccountRow extends IBookingAccount {
  isRelatedToMandator?: boolean;
}


export const accountNoModifier = (value: number): string | null => {
  if (value) {
    return value.toString().padStart(8, '0');
  }
  return null;
};

export const getBookingAccountDescription = (bookingAccount: IBookingAccount, language: Language): string => {
  return bookingAccount ? `${accountNoModifier(bookingAccount.no)} ${bookingAccount.displayLabel[language]}` : '';
};
