import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Observable, combineLatest } from 'rxjs';
import { switchMap, startWith, map } from 'rxjs/operators';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { wording } from 'src/app/core/constants/wording/wording';
import { taskTypes, TaskType } from 'src/app/core/models/enums/vessel-registration-tasks';
import { Language } from 'src/app/core/models/language';
import { IWording } from 'src/app/core/models/resources/IWording';
import { ITaskTemplateCategory, TaskTemplateCategoryKey } from 'src/app/core/models/resources/Task-Templates/ITaskTemplateCategory';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { TaskTemplateCategoryRepositoryService } from 'src/app/core/services/repositories/task-templates/task-template-category-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { CreateCategoryComponent } from './create-category/create-category.component';
import { TaskTemplatesHelperService } from '../task-templates-helper.service';
import { checkForCategoryName } from '../task-templates.helper';

@Component({
  selector: 'nv-task-categories',
  templateUrl: './task-categories.component.html',
  styleUrls: ['./task-categories.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskCategoriesComponent implements OnInit {
  public wording = wording;
  public taskCategories$: Observable<ITaskTemplateCategory[]>;
  public searchCtrl = new FormControl();
  public categoryCtrl = new FormControl();
  public categoryCreateForm: FormGroup;
  public simpleVesselTaskCtrl = new FormControl(true);
  public registrationVesselTaskCtrl = new FormControl(true);
  public taskTypes = taskTypes;
  public language: Language;
  public Language = Language;

  private modalRef: NzModalRef;
  constructor(
    private taskTemplateCategoryRepositoryService: TaskTemplateCategoryRepositoryService,
    private modalService: NzModalService,
    private settingsService: SettingsService,
    private confirmationService: ConfirmationService,
    private taskTemplatesHelperService: TaskTemplatesHelperService,
  ) { }

  public ngOnInit(): void {
    this.language = this.settingsService.language;
    this.initData();
  }

  private initData(): void {
    // to let other components know about selected category
    this.categoryCtrl.valueChanges
      .pipe(startWith(this.categoryCtrl.value))
      .subscribe((selectedCategoryId: TaskTemplateCategoryKey) => {
        this.taskTemplatesHelperService.selectedCategory.next(selectedCategoryId);
      });
    this.taskTemplateCategoryRepositoryService.fetchAll({}).subscribe();
    this.taskCategories$ = this.taskTemplateCategoryRepositoryService
      .getStream()
      .pipe(
        switchMap(categoryItems => {
          return combineLatest([
            this.searchCtrl.valueChanges.pipe(startWith('')),
            this.simpleVesselTaskCtrl.valueChanges
              .pipe(startWith(this.simpleVesselTaskCtrl.value)),
            this.registrationVesselTaskCtrl.valueChanges
              .pipe(startWith(this.registrationVesselTaskCtrl.value)),
          ])
            .pipe(
              map(([searchText, simpleVesselTask, registrationVesselTask]) => {
                return categoryItems.filter(item => {
                  if (item.type === TaskType.RegistrationVesselTask && !registrationVesselTask) {
                    return;
                  }

                  if (item.type === TaskType.SimpleVesselTask && !simpleVesselTask) {
                    return;
                  }

                  return checkForCategoryName(item, searchText, this.settingsService.language);
                });
              }));
        })
      );
  }

  public onCategoryCreateOrEdit = (category?: ITaskTemplateCategory): void => {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzWidth: 600,
      nzTitle: wording.system.createCatgory[this.language],
      nzClosable: true,
      nzContent: CreateCategoryComponent,
      nzComponentParams: {
        closeModal: this.closeModal,
        category,
      }
    });
  }

  private closeModal = () => {
    this.modalRef.destroy();
  }

  public onDelete(category: ITaskTemplateCategory): void {
    if (category && !this.taskTemplatesHelperService.checkForSystemDefault(category)) {
      return;
    }
    const subject: IWording = {
      en: category.labelEn,
      de: category.labelDe,
    };

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject },
    }).subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.taskTemplateCategoryRepositoryService.delete(category.id, {})
          .subscribe();
      }
    });
  }
}
