import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';

import { IconsModule } from '@shared/modules/icons.module';
import { MeUserImageModule } from '@shared-components/me-user-image/me-user-image.module';
import { MeLabelModule } from '@shared-components/me-label/me-label.module';
import { MeAttributeModule } from '../me-attribute/me-attribute.module';
import { MeTableComponent } from './me-table.component';
import { MeTableDirective } from './me-table.directive';
import { TableButtonWithIconComponent } from './table-button-with-icon/table-button-with-icon.component';
import { TableStringComponent } from './table-string/table-string.component';
import { TableImageWithTitleComponent } from './table-image-with-title/table-image-with-title.component';
import { TableLabelComponent } from './table-label/table-label.component';
import { TableContextMenuComponent } from './table-context-menu/table-context-menu.component';
import { MeContextMenuModule } from '../me-context-menu/me-context-menu.module';
import { TableActionButtonsComponent } from './table-action-buttons/table-action-buttons.component';
import { TableActionButtonDirective } from './table-action-buttons/table-action-buttons.directive';
import { TableImageComponent } from './table-image/table-image.component';
import { TableEmailComponent } from './table-email/table-email.component';
import { TableDateComponent } from './table-date/table-date.component';
import { TableListComponent } from './table-list/table-list.component';
import { TableSelectComponent } from './table-select/table-select.component';

@NgModule({
  declarations: [
    MeTableComponent,
    MeTableDirective,
    TableActionButtonDirective,
    TableActionButtonsComponent,
    TableButtonWithIconComponent,
    TableStringComponent,
    TableImageWithTitleComponent,
    TableLabelComponent,
    TableContextMenuComponent,
    TableImageComponent,
    TableEmailComponent,
    TableDateComponent,
    TableListComponent,
    TableSelectComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    MatButtonModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MeContextMenuModule,
    MatPaginatorModule,
    IconsModule,
    MeUserImageModule,
    MeLabelModule,
    MeAttributeModule
  ],
  exports: [MeTableComponent],
  entryComponents: [
    TableActionButtonsComponent,
    TableButtonWithIconComponent,
    TableStringComponent,
    TableImageWithTitleComponent,
    TableLabelComponent,
    TableContextMenuComponent,
    TableImageComponent,
    TableEmailComponent,
    TableDateComponent,
    TableListComponent
  ]
})
export class MeTableModule {}
