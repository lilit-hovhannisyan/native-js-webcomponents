import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';

import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { ICompanyRow } from 'src/app/core/models/resources/ICompany';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/companies.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { CompanyMainService } from 'src/app/core/services/mainServices/company-main.service';

@Component({
  selector: 'nv-company-list-view',
  templateUrl: './company-list-view.component.html',
  styleUrls: ['./company-list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompanyListViewComponent implements OnInit {

  public companyGridConfig: NvGridConfig;
  public dataSource$: Observable<ICompanyRow[]>;

  constructor(
    private companyService: CompanyMainService,
    private authService: AuthenticationService,
    private router: Router,
    public gridConfigService: GridConfigService
  ) { }

  public ngOnInit(): void {
    this.dataSource$ = this.companyService.getStream();
    this.companyGridConfig = getGridConfig(
      this.router,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }
}
