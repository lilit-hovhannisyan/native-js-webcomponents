import { Directive, AfterViewInit, ElementRef, Input } from '@angular/core';

/**
 * This directive will set the default auto focus of the user
 * to the chosen element, when the view is being rendered.
 */
@Directive({
  selector: '[nvAutofocus]'
})
export class AutofocusDirective implements AfterViewInit {

  @Input() public nvAutofocus = true;

  constructor(private el: ElementRef) {
  }

  public ngAfterViewInit() {
    if (this.nvAutofocus !== false) {
      setTimeout(() => this.el.nativeElement.focus());
    }
  }
}
