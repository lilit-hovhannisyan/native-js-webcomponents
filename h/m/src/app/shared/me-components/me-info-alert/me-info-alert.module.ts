import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';

import { IconsModule } from '@shared-modules/icons.module';

import { MeInfoAlertService } from './me-info-alert.service';
import { MeInfoAlertComponent } from './me-info-alert.component';
@NgModule({
  imports: [CommonModule, IconsModule, MatSnackBarModule, MatButtonModule],
  exports: [MeInfoAlertComponent],
  declarations: [MeInfoAlertComponent],
  providers: [MeInfoAlertService]
})
export class MeInfoAlertModule {}
