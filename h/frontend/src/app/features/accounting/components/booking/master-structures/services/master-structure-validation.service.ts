import { Injectable } from '@angular/core';
import { IMasterStructureNode, MASTER_STRUCTURE_NODE_TYPE, IMasterStructureNodeTemplate } from 'src/app/core/models/resources/IMasterStructureNode';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { IMasterStructure } from 'src/app/core/models/resources/IMasterStructure';
import { MASTER_STRUCTURE_TYPES } from 'src/app/core/models/enums/master-structure-type';
import { ACCOUNTING_TYPES } from 'src/app/core/models/enums/accounting-types';
import { IWording } from 'src/app/core/models/resources/IWording';
import { AbstractControl } from '@angular/forms';
import { MasterStructureBookingAccountService } from '../master-structure-booking-accounts/master-structure-booking-account.service';
import { ACL_ACTIONS, AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { MasterStructureNodeHelperService } from '../master-structure-nodes/services/master-structure-node-helper.service';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';

@Injectable({
  providedIn: 'root'
})
export class MasterStructureValidationService {
  constructor(
    private nodesHelperService: MasterStructureNodeHelperService,
    private confirmationService: ConfirmationService,
    private bookingAccountsService: MasterStructureBookingAccountService,
    private authService: AuthenticationService,
  ) { }

  /**
   * This method changes structure's type or relation back to the previous after warning
   * modal is closed, if there is a linked booking account to the structure which
   * doesn't match with the structure's account or relation
   */
  public validateStructureRelationOrTypeChange(
    title: IWording,
    formControl: AbstractControl,
    field: 'accountingType' | 'masterStructureType',
    isValid: boolean,
    usedBookingAccounts: IBookingAccount[],
    currentStructure: IMasterStructure,
    value?: ACCOUNTING_TYPES | MASTER_STRUCTURE_TYPES,
  ): boolean {
    if (usedBookingAccounts.length === 0 && isValid) {
      formControl.setValue(value);
      return true;
    }

    if (isValid) {
      formControl.setValue(value);
      return true;
    } else {
      this.confirmationService.error({
        title,
        cancelButtonWording: null,
        okButtonWording: wording.general.ok,
      }).subscribe(() =>  currentStructure[field as string] = formControl.value);
      return false;
    }
  }

  public hasTheSameValue(
    field: 'accountingType' | 'masterStructureType',
    value: MASTER_STRUCTURE_TYPES | ACCOUNTING_TYPES,
    usedBookingAccounts: IBookingAccount[],
  ): boolean {
    const hasTheSameRelationOrType: boolean = usedBookingAccounts
      .every(account => (field === 'accountingType'
        ? this.bookingAccountsService
            .hasTheSameAccountingType(account, value as ACCOUNTING_TYPES)
        : this.bookingAccountsService
            .hasTheSameMasterStructureType(account, value as MASTER_STRUCTURE_TYPES)
      ));
    return hasTheSameRelationOrType;
  }

  public hasUsedFunctionsForStructureType(
    structureType: MASTER_STRUCTURE_TYPES,
    nodes: IMasterStructureNodeTemplate[],
  ): boolean {
    let hasUsedFunctions = false;
    this.nodesHelperService.mapNodes(
      nodes,
      (node: IMasterStructureNode) => {
        const isBalanceSheet: boolean = structureType
          === MASTER_STRUCTURE_TYPES.BALANCE_SHEET;
        const isBusinessEvaluation: boolean = structureType
          === MASTER_STRUCTURE_TYPES.BUSINESS_MANAGEMENT_EVALUATION;
        const hasSummation: boolean = isBalanceSheet
          && node.nodeType === MASTER_STRUCTURE_NODE_TYPE.SUMMATION;
        const hasExpenseItem: boolean = !isBusinessEvaluation && node.hasExpenseItem;
        const hasCurrentYearResult: boolean = !isBalanceSheet
          && node.hasResultCurrentYear;
        const hasPreviousYearResult: boolean = !isBalanceSheet
          && node.hasResultPreviousYear;
        const hasSwitchingNodes: boolean = !!!(isBalanceSheet && node.switchingNodeId);

        if (
          hasSummation
          || hasExpenseItem
          || hasCurrentYearResult
          || hasPreviousYearResult
          || hasSwitchingNodes
        ) {
          hasUsedFunctions = true;
          return true;
        }
      }
    );
    return hasUsedFunctions;
  }

  public hasAccess(operation: ACL_ACTIONS, url: string): boolean {
    const zoneOfRoute: string = SitemapEntry.getByUrl(url).toZone();
    return this.authService.can(operation, zoneOfRoute);
  }

  public onStructureTypeChange(
    relation: MASTER_STRUCTURE_TYPES,
    masterStructureType: AbstractControl,
    usedBookingAccounts: IBookingAccount[],
    currentStructure: IMasterStructure,
    nodes: IMasterStructureNodeTemplate[],
  ): void {
    const hasTheSameStructureType: boolean = this
      .hasTheSameValue('masterStructureType', relation, usedBookingAccounts);

    if (hasTheSameStructureType) {
      this.validateStructureRelationOrTypeChange(
        wording.accounting.severalFunctionsWereUsedForStructureType,
        masterStructureType,
        'masterStructureType',
        !this.hasUsedFunctionsForStructureType(
          currentStructure.masterStructureType,
          nodes,
        ),
        usedBookingAccounts,
        currentStructure,
        relation,
      );
    } else {
      this.validateStructureRelationOrTypeChange(
        wording.accounting.structureTypeCannotBeChanged,
        masterStructureType,
        'masterStructureType',
        hasTheSameStructureType,
        usedBookingAccounts,
        currentStructure,
        relation,
      );
    }
    this.bookingAccountsService.setMasterStructure({
      ...currentStructure,
      masterStructureType: masterStructureType.value,
    });
  }

  public onAccountingNormChange(
    type: ACCOUNTING_TYPES,
    accountingType: AbstractControl,
    usedBookingAccounts: IBookingAccount[],
    currentStructure: IMasterStructure,
  ): any {
    this.validateStructureRelationOrTypeChange(
      wording.accounting.structureAccountNormCannotBeChanged,
      accountingType,
      'accountingType',
      this.hasTheSameValue('accountingType', type, usedBookingAccounts),
      usedBookingAccounts,
      currentStructure,
      type,
    );
    this.bookingAccountsService.setMasterStructure({
      ...currentStructure,
      accountingType: accountingType.value,
    });
  }

}
