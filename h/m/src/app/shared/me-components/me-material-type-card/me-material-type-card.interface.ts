export interface MeMaterialTypeCardI {
  title: string;
  type: string;
  icon: string;
  selected?: boolean;
}
