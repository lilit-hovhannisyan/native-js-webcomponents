import { Component, OnInit, HostListener, ViewChild, ElementRef, OnDestroy, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { SitemapNode, sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { Subject, BehaviorSubject, Subscription, timer } from 'rxjs';
import { debounce } from 'rxjs/operators';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { Router } from '@angular/router';
import { wording } from 'src/app/core/constants/wording/wording';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';

interface SitemapNodeWithSiblings extends SitemapNode {
  previous?: SitemapNodeWithSiblings;
  next?: SitemapNodeWithSiblings;
  topic?: SitemapNodeWithSiblings[];
  section?: SitemapNodeWithSiblings[];
  url?: string;
}

interface IFocusedTopic {
  category: SitemapNodeWithSiblings;
  section: SitemapNodeWithSiblings;
  topic: SitemapNodeWithSiblings;
  url: string;
}

@Component({
  selector: 'nv-topic-search-content',
  templateUrl: './topic-search-content.component.html',
  styleUrls: ['./topic-search-content.component.scss']
})
export class TopicSearchContentComponent implements OnInit, AfterViewInit, OnDestroy {
  public input$ = new Subject();
  public sitemapArr = new BehaviorSubject<any[]>([]);
  public sitemapArr$ = this.sitemapArr.asObservable();
  public subscription: Subscription;
  public isClearButtonVisible = false;
  public focusedTopic: IFocusedTopic = {} as IFocusedTopic;
  public wording = wording;

  @ViewChild('searchInput') public searchInput: ElementRef;

  @Output() public navigationHappened = new EventEmitter<void>();

  constructor(
    private router: Router,
    private authService: AuthenticationService,
  ) { }

  public ngOnInit() {
    this.subscription = this.input$
      .pipe(debounce(() => timer(100)))
      .subscribe((searchString: string) => {
        this.shouldClearButtonBeShown();
        if (!searchString) { // exit here if the input is empty
          this.sitemapArr.next([]);
          return;
        }

        this.focusedTopic = {} as IFocusedTopic;
        this.sitemapArr.next(
          this.filterAndMakeLinkedCategories(searchString)
        );
    });
  }

  public ngAfterViewInit() {
    setTimeout(() => {
      this.searchInput.nativeElement.focus();
    }, 300);
  }

  /**
   * The sitemap is a nested obj of categories -> sections -> topics. This function
   * generates a nested array -> obj -> array -> obj -> array -> obj, containing only
   * categorys, sections, topics that match the searchString. It also adds siblings to
   * categorys, sections and topics so it would be easy to navigate between them in the
   * list with arrow keys in onArrowUp() and onArrowDown() methods.
   * Relevant for the matching are just topic-keywords, topic-title, topic-description.
   * Filtering starts in filterAndMakeLinkedTopics() then continues in
   * filterAndMakeLinkedSections() and ends finally here.
   */
  public filterAndMakeLinkedCategories(searchString: string): SitemapNodeWithSiblings[] {
    const categories = Object.values(sitemap as { [key: string]: SitemapNodeWithSiblings })
      .map(c => {
        c.section = this.filterAndMakeLinkedSections(c.children, searchString);
        return c;
      })
      .filter(category => !!category.section.length);
    categories.forEach((category, categoryIndex) => {
      categories[categoryIndex] = {
        ...category,
        previous: categories[categoryIndex - 1],
      };

      if (categories[categoryIndex - 1] !== undefined) {
        categories[categoryIndex - 1].next = categories[categoryIndex];
      }
    });
    return [...categories];
  }

  /**
   * The section is a nested obj of sections -> topics. This function
   * generates a nested -> array -> obj -> array -> obj, containing only
   * categorys, sections, topics that match the searchString. It also adds siblings to
   * categorys, sections and topics so it would be easy to navigate between them in the
   * list with arrow keys in onArrowUp() and onArrowDown() methods.
   * Relevant for the matching are just topic-keywords, topic-title, topic-description.
   * Filtering starts in filterAndMakeLinkedTopics() then continues here and ends finally
   * in filterAndMakeLinkedCategories().
   */
  public filterAndMakeLinkedSections(
    rawSections: { [key: string]: SitemapNodeWithSiblings },
    searchString: string,
  ): SitemapNodeWithSiblings[]  {
    const sections = Object.values(rawSections)
      .map(s => {
        s.topic = this.filterAndMakeLinkedTopics(s.children, searchString);
        return s;
      })
      .filter(section => !!section.topic.length);
    sections.forEach((section, sectionIndex) => {
      sections[sectionIndex] = {
        ...section,
        previous: sections[sectionIndex - 1],
      };

      if (sections[sectionIndex - 1] !== undefined) {
        sections[sectionIndex - 1].next = sections[sectionIndex];
      }
    });
    return [...sections];
  }

  /**
   * The topic is a nested obj of topics. This function generates a nested
   * -> array -> obj, containing only categorys, sections, topics that match
   * the searchString. It also adds siblings to categorys, sections and topics so it would
   * be easy to navigate between them in the list with arrow keys in onArrowUp() and
   * onArrowDown() methods. Relevant for the matching are just topic-keywords,
   * topic-title, topic-description. Filtering starts here then continues in
   * filterAndMakeLinkedSections() and ends finally in filterAndMakeLinkedCategories().
   */
  public filterAndMakeLinkedTopics(
    rawTopics: { [key: string]: SitemapNodeWithSiblings },
    searchString: string,
  ): SitemapNodeWithSiblings[] {
    const topics = Object.values(rawTopics)
      .filter((topic: SitemapNode) => this.topicMatches(topic, searchString)
        && this.authService.canAccessNode(topic));
    topics.forEach((topic, topicIndex) => {
      topics[topicIndex] = {
        ...topic,
        previous: topics[topicIndex - 1],
        url: this.getTopicUrl(topic)
      };

      if (topics[topicIndex - 1] !== undefined) {
        topics[topicIndex - 1].next = topics[topicIndex];
      }
    });
    return topics;
  }

  public getTopicUrl = (topic) => SitemapEntry.getByNode(topic).toUrl();

  public topicMatches = (t: SitemapNode, searchString) => {
    const keywords = t.keywords || [];
    const title = (t.title && t.title.en) || '';
    const description = (t.description || '');
    const lookupString = [ ...keywords, title, description].join('-/-').toLowerCase();
    return lookupString.includes((searchString || '').toLowerCase());
  }

  public onChange(event) {
    this.input$.next(event.target.value);
  }

  public navigateTo(url: string) {
    this.navigationHappened.emit();
    this.clearSearchInput();
    this.router.navigate([url]);
  }


  public shouldClearButtonBeShown() {
    this.isClearButtonVisible = this.searchInput.nativeElement.value !== '';
  }

  public clearSearchInput() {
    this.searchInput.nativeElement.value = '';
    this.isClearButtonVisible = false;
    this.focusedTopic = {} as IFocusedTopic;
    this.sitemapArr.next([]);
  }

  /**
   * Sets current focusedTopic, which contains various information about it.
   */
  public setFocusedTopic(
    topic?: SitemapNodeWithSiblings,
    section?: SitemapNodeWithSiblings,
    category?: SitemapNodeWithSiblings
  ) {
    this.focusedTopic = {
      category: category || this.focusedTopic.category,
      section: section || this.focusedTopic.section,
      topic: topic || this.focusedTopic.topic,
      url: (topic || this.focusedTopic.topic).url,
    };

    this.scrollToTheFocusedTopic();
  }

  /**
   * If list is big, this is needed so user could see focused topic
   */
  public scrollToTheFocusedTopic() {
    document.querySelector(`[data-topic-url='${this.focusedTopic.url}']`)
      .scrollIntoView({ behavior: 'smooth', block: 'center' });
  }

  /**
   * Returns true if there isn't focused topic or :
   * the first topic is focused and arrow key up fires (`topicDirection: 'previous'`)
   * the last topic is focused and arrow key down fires (`topicDirection: 'next'`)
   */
  private shouldTopicBeFocused(topicDirection: 'next' | 'previous'): boolean {
    return Object.entries(this.focusedTopic).length === 0
      || (
        !this.focusedTopic.topic[topicDirection]
        && !this.focusedTopic.section[topicDirection]
        && !this.focusedTopic.category[topicDirection]
      );
  }

  // todo: create IRoute resource, and apply it here, and everywhere in the project needed
  private getItemIndex(obj, path: string, arrowDirection: 'up' | 'down'): number {
    return arrowDirection === 'up' ? obj[path].length - 1 : 0;
  }

  public onArrowAction(arrowDirection: 'up' | 'down', topicDirection: 'previous' | 'next'): void {
    /**
     * as onArrowUp && onArrowDown functions are same, I combine them in this function
     * arrowDirection: 'up' -> topicDirection: 'previous'
     * arrowDirection: 'down' -> topicDirection: 'next'
     */
    const categories = this.sitemapArr.getValue();

    if (!categories.length) {
      return;
    }

    // if there isn't focused topic or the first/last topic is focused
    if (this.shouldTopicBeFocused(topicDirection)) {
      const categoryIndex = arrowDirection === 'up' ? categories.length - 1 : 0;

      const category = categories[categoryIndex];
      const section = category.section[this.getItemIndex(category, 'section', arrowDirection)];
      const topic = section.topic[this.getItemIndex(section, 'topic', arrowDirection)];

      this.setFocusedTopic(topic, section, category);
      return;
    }

    // if there is a previous/next topic
    if (this.focusedTopic.topic[topicDirection]) {
      this.setFocusedTopic(this.focusedTopic.topic[topicDirection]);
      return;
    }

    // if there isn't previous/next topic we check if there is previous/next section block with topics
    if (!this.focusedTopic.topic[topicDirection] && this.focusedTopic.section[topicDirection]) {
      const section = this.focusedTopic.section[topicDirection];
      const topic = section.topic[this.getItemIndex(section, 'topic', arrowDirection)];
      this.setFocusedTopic(topic, section);
      return;
    }

    // if there isn't previous/next topic and section
    //  we check if there is previous/next category block with sections and topics
    if (
      !this.focusedTopic.topic[topicDirection]
      && !this.focusedTopic.section[topicDirection]
      && this.focusedTopic.category[topicDirection]
    ) {
      const category = this.focusedTopic.category[topicDirection];
      const section = category.section[this.getItemIndex(category, 'section', arrowDirection)];
      const topic = section.topic[this.getItemIndex(section, 'topic', arrowDirection)];
      this.setFocusedTopic(topic, section, category);
    }
  }

  @HostListener('document:keyup.arrowup')
  public onArrowUp(): void {
    this.onArrowAction('up', 'previous');
  }

  @HostListener('document:keyup.arrowdown')
  public onArrowDown(): void {
    this.onArrowAction('down', 'next');
  }

  /**
   * If there is a focused topic and enter key fires, navigate to that topic
   */
  @HostListener('document:keyup.enter')
  public onEnter() {
    if (this.focusedTopic.url) {
      this.navigateTo(this.focusedTopic.url);
    }
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
