import { IResource } from './IResource';
import { CostUnitKey, ICostUnit } from './ICostUnit';
import { MandatorKey, IMandatorLookup } from './IMandator';

export type MandatorCostUnitKey = number;

interface ISharedProps {
  costUnitId: CostUnitKey;
  mandatorId: MandatorKey;
}

export interface IMandatorCostUnitCreate extends ISharedProps { }

export interface IMandatorCostUnitBase extends ISharedProps, IResource<MandatorCostUnitKey> { }

export interface IMandatorCostUnitDTO extends IMandatorCostUnitBase { }

export interface IMandatorCostUnit extends IMandatorCostUnitDTO { }

export interface IMandatorCostUnitFull extends IMandatorCostUnit {
  mandator: IMandatorLookup;
  costUnit: ICostUnit;
}

export interface IMandatorCostUnitRow extends IMandatorCostUnitFull {
  no: string;
  shortName: string;
}
