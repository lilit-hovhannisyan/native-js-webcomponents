import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { map, share } from 'rxjs/operators';
import { Observable, interval } from 'rxjs';
import * as moment from 'moment-timezone';
import { IClockWidget } from 'src/app/core/models/widgets/IClockWidget';
import { ClockWidgetService } from '../../../../../examples/services/widgets/clock-widget-repository.service';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { ClockDetailViewComponent } from './detail-view/detail-view.component';
import { wording } from 'src/app/core/constants/wording/wording';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  selector: 'nv-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ClockComponent implements OnInit {
  @Input() public widget: IClockWidget;
  public clocks: { date: Observable<string>; timeZone: string; }[] = [];
  public addClockModalVisible: boolean;
  private modalRef?: NzModalRef;
  public wording = wording;

  constructor(
    private cdr: ChangeDetectorRef,
    private clockWidgetService: ClockWidgetService,
    private modalService: NzModalService,
    private settingsService: SettingsService
  ) { }

  public ngOnInit() {
    this.initInputClocks();
  }

  private initInputClocks() {
    let timezones: string[];
    if (this.widget.timeZones) {
      timezones = this.widget.timeZones.split(',');
      this.clocks = timezones.map(timeZone => ({ date: this.getDate(timeZone), timeZone }));
    } else {
      timezones = [];
      this.widget.timeZones = '';
    }
  }

  public addClock = (timeZone: string) => {
    this.clocks.push({
      date: this.getDate(timeZone),
      timeZone
    });
    this.widget.timeZones += `,${timeZone}`;
    this.updateWidget();
  }

  public delete(index: number) {
    this.clocks.splice(index, 1);
    this.widget.timeZones = this.clocks.map(clock => clock.timeZone).join(',');
    this.updateWidget();
  }

  private updateWidget(): void {
    this.widget.timeZones = this.widget.timeZones.replace(/(^,)|(,$)/g, '');
    this.clockWidgetService.updateWidget(this.widget)
      .subscribe(() => this.cdr.detectChanges());
  }

  public getDate(zoneName: string): Observable<string> {
    return interval(1000)
      .pipe(
        map(() => moment().tz(zoneName).format('h:mm:ss A')),
        share()
      );
  }

  public openDetailViewModal = () => {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzWidth: 530,
      nzTitle: wording.general.addClock[this.settingsService.language],
      nzClosable: true,
      nzContent: ClockDetailViewComponent,
      nzComponentParams: {
        closeModal: () => this.modalRef.destroy(),
        submit: (timezone: string) => { this.modalRef.destroy(); this.addClock(timezone); }
      },
    });
  }

}
