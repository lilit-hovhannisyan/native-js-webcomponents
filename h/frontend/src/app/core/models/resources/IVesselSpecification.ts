import { VesselKey } from './IVessel';
import { IResource } from './IResource';
import { VESSEL_TYPE } from '../enums/vessel-type';

export type VesselSpecificationKey = number;

interface ISharedProps {
  from: string;
  until: string;
  vesselType?: VESSEL_TYPE;
  shipyardId: number;
  shipyardTypeId: number;
  hullNo: string;
  hatches: number;
  vesselId: VesselKey;
  gear: string;
  class: string;

  dwats: number;
  dwatw: number;
  dwccw: number;
  dwccs: number;
  grt: number;
  nrt: number;
  // The following are shown for vessel type "bulkcarrier"
  cbftGrain: number;
  cbftBale: number;
  cbmGrain: number;
  cbmBale: number;
  // and the following are shown for "container" and "ConRo"
  container20ft: number;
  container40ft: number;
  homog14ts: number;
  reefer: number;
}

export interface IVesselSpecificationCreate extends ISharedProps { }

export interface IVesselSpecificationBase extends ISharedProps, IResource<number> { }

export interface IVesselSpecificationDTO extends IVesselSpecificationBase { }

export interface IVesselSpecification extends IVesselSpecificationDTO { }









