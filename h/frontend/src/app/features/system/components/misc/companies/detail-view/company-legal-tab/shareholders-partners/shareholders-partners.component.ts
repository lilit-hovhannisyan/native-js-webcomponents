import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, Input} from '@angular/core';
import { ICompanyPartnerContribution, ICompanyPartnerContributionFull, PartnerType } from 'src/app/core/models/resources/ICompanyPartnerContribution';
import { wording } from 'src/app/core/constants/wording/wording';
import { Router, ActivatedRoute } from '@angular/router';
import { tap, map, takeUntil, switchMap } from 'rxjs/operators';
import { Observable, ReplaySubject } from 'rxjs';
import { ContributionMainService } from 'src/app/core/services/mainServices/company-contributions-main.service';
import * as moment from 'moment';
import { CompanyKey } from 'src/app/core/models/resources/ICompany';
import { CompaniesHelperService } from '../../companies.helper.service';
import { CompanyLegalEntityRepositoryService } from '../../../../../../../../core/services/repositories/company-legal-entity-repository.service';

@Component({
  selector: 'nv-shareholders-partners',
  templateUrl: './shareholders-partners.component.html',
  styleUrls: ['./shareholders-partners.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ShareholdersPartnersComponent implements OnInit, OnDestroy {
  @Input() public companyId: CompanyKey;

  public wording = wording;
  public selectedFrom: string;
  public allContributions: ICompanyPartnerContributionFull[];
  public contributionsUniqueDates: string[];
  public nearestDate: string;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

  constructor(
    private contributionMainService: ContributionMainService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectionRef: ChangeDetectorRef,
    private companiesHelperService: CompaniesHelperService,
    private companyLegalEntityRepo: CompanyLegalEntityRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.companyId = this.companyId || this.companiesHelperService.company.id;

    this.getContributionsStream()
      .pipe(
        takeUntil(this.componentDestroyed$),
        tap(() => {
          this.nearestDate = this.getNearestDate();
          this.navigate2NearestMovementWhenUnselected();
        }),
        switchMap(() => {
          return this.activatedRoute.queryParams.pipe(
            tap(params => {
              this.selectedFrom = params.from;
              this.changeDetectionRef.detectChanges();
            })
          );
        }),
      ).subscribe();
  }

  /**
   * If `selectedFrom` is invalid or doesn't exist - check first movement.
   */
  private async navigate2NearestMovementWhenUnselected(): Promise<boolean> {
    if (
      !this.contributionsUniqueDates?.length
      || this.selectedFrom
      && this.contributionsUniqueDates.indexOf(this.selectedFrom) !== -1
    ) {
      return Promise.resolve(false);
    }

    this.selectedFrom = this.getNearestDate();
    const navigationDone = await this.router.navigate([], {
      queryParams: { from: this.selectedFrom },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });

    if (navigationDone) {
      /**
       * The ChangeDetection isn't detecting changes automatically
       * after navigation.
       */
      this.changeDetectionRef.detectChanges();
    }
    return navigationDone;
  }

  // add and edit
  public movementChangeHandler(contribution: ICompanyPartnerContribution): void {
    this.selectMovement(contribution.from);
    this.navigate2NearestMovementWhenUnselected();
  }

  public movementDeleteHandler(contribution: ICompanyPartnerContribution): void {
    this.navigate2NearestMovementWhenUnselected();
  }

  private async selectMovement(from: string): Promise<boolean> {
    const clearedFrom = from;

    if (this.contributionsUniqueDates.indexOf(clearedFrom) === -1) {
      return Promise.resolve(false);
    }

    const navigationDone: boolean = await this.router.navigate([], {
      queryParams: { from: clearedFrom },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });

    if (navigationDone) {
      this.selectedFrom = clearedFrom;
      this.changeDetectionRef.detectChanges();
    }
    return Promise.resolve(navigationDone);
  }

  private getContributionsStream(): Observable<ICompanyPartnerContributionFull[]> {
    return this.contributionMainService.getStream({ queryParams: { cid: this.companyId } })
      .pipe(
        map(contributions => this.contributionMainService.sortByDate(contributions)),
        tap(allContributions => {
          this.allContributions = allContributions;
          this.contributionsUniqueDates = allContributions?.length
            ? this.getUniqueDates(this.allContributions)
            : [moment().utc().startOf('day').toISOString()];
        }),
      );
  }


  private getUniqueDates(contributions: ICompanyPartnerContribution[]): string[] {
    const dates = contributions.map(s => {
      const from = s.from;
      /**
       * The backend gives date with 'Z' at the end on time of creation
       * and with fetch gives us the same dates without 'Z'.
       * Since we are using getStream which is inserting the new created entity
       * (which was given by backend(POST response)) on stream data, we need to have
       * all dates in same format.
       */
      return from[from.length - 1] === 'Z' ? from : `${from}Z`;
    });
    const datesSet = new Set<string>(dates);

    return Array.from(datesSet).sort((a, b) => moment(a).isAfter(b) ? -1 : +1);
  }

  /**
   * According to AC: Should select nearest date to today.
   * 1. Select current date(today) if it exists
   * 2. Otherwise select past-nearest date if it exists.
   * 3. Otherwise select future-nearest date.
   */
  private getNearestDate(): string | null {
    if (!this.contributionsUniqueDates?.length) {
      return null;
    }

    return this.contributionsUniqueDates.reduce((result, date) => {
      if (moment().isSame(result, 'd')) {
        return result;
      }
      if (moment().isSame(date, 'd')) {
        return date;
      }

      const resultDiff: number = moment().diff(result);
      const dateDiff: number = moment().diff(date);
      if (resultDiff < 0 && dateDiff > 0) {
        return date;
      }
      if (resultDiff > 0 && dateDiff < 0) {
        return result;
      }
      return Math.abs(resultDiff) < Math.abs(dateDiff) ? result : date;
    });
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
