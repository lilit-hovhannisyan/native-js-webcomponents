import { ActivatedRoute } from '@angular/router';
import { MandatorMainService } from '../../../../../../../core/services/mainServices/mandator-main.service';
import { Component, OnInit } from '@angular/core';
import { ICompany } from '../../../../../../../core/models/resources/ICompany';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { AccountService } from 'src/app/core/services/account.service';
import { Validators } from 'src/app/core/classes/validators';
import { IAddressRow } from 'src/app/core/services/mainServices/address-main.service';
import { MandatorKey } from 'src/app/core/models/resources/IMandator';
import { IWording } from 'src/app/core/models/resources/IWording';
import { SettingsService } from 'src/app/core/services/settings.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';

@Component({
  selector: 'nv-company-and-address',
  templateUrl: './company-and-address.component.html',
  styleUrls: ['./company-and-address.component.scss']
})
export class CompanyAndAddressComponent implements OnInit {
  private companyNode = sitemap.system.children.misc.children.companies;

  public company: ICompany;
  public form: FormGroup;
  public mandatorId: MandatorKey;
  public addressType: IWording = null;
  public fullAddress: string;
  public editMode = false;
  public wording = wording;
  public hasAccessCompanies = false;
  public companyPath: string;

  constructor(
    private fb: FormBuilder,
    private mainService: MandatorMainService,
    public companyRepo: CompanyRepositoryService,
    public accountService: AccountService,
    public route: ActivatedRoute,
    private settingsService: SettingsService,
    private authService: AuthenticationService,
  ) { }

  public ngOnInit(): void {
    this.hasAccessCompanies = this.authService.canAccessNode(this.companyNode);
    this.mandatorId = parseInt(this.route.parent.snapshot.params.id, 10);
    this.mainService.fetchOne(this.mandatorId, {}).subscribe(mandator => {
      this.company = mandator.company;
      this.init(this.company);
    });
  }

  public init(company = {} as ICompany): void {
    this.form = this.fb.group({
      companyName: [company.companyName, [Validators.required, Validators.maxLength(100)]],
      taxNo: [company.taxNo, [Validators.required, Validators.maxLength(15)]],
      vatId: [company.vatId, [Validators.required, Validators.maxLength(20)]],
      debitorCreditorNo: [5454874]
    });
    this.form.disable();
    this.companyPath = `${url(this.companyNode)}/${company.id}`;
  }

  public onAddressRowSelectionChange(address: IAddressRow | null) {
    if (address) {
      this.addressType = address.typeName;
      this.fullAddress =
        `${address.street}\n${address.zipCode} ${address.city}\n${address.countryName[this.settingsService.language]}`;
    } else {
      this.addressType = null;
      this.fullAddress = null;
    }
  }

  public enterEditMode() {
    this.editMode = true;
    this.form.enable();
  }

  public cancelEditing() {
    this.leaveEditMode();
    this.init(this.company);
  }

  public leaveEditMode() {
    this.editMode = false;
    this.form.disable();
  }

  public submit() {
    if (!this.form.valid) {
      return;
    }
    this.companyRepo.update({ ...this.company, ...this.form.value }, {})
      .subscribe(() => {
        this.leaveEditMode();
        this.form.markAsPristine();
      });
  }

  public copyToClipboard(input: HTMLTextAreaElement): void {
    if (this.fullAddress) {
      input.select();
      document.execCommand('copy');
      input.setSelectionRange(0, 0);
    }
  }
}
