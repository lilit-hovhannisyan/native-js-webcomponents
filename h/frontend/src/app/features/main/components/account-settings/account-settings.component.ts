import { Component, OnInit } from '@angular/core';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { Language } from 'src/app/core/models/language';
import { NvLocale } from 'src/app/core/models/dateFormat';
import { IAccountConfigGeneral } from 'src/app/core/models/resources/IAccountConfigGeneral';
import { DEBIT_CREDIT } from 'src/app/core/models/enums/debitCredit';
import { SettingsService } from 'src/app/core/services/settings.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { NvGridI18nService } from 'nv-grid';
import { nvLocalMapper } from 'src/app/shared/pipes/localDecimal';
import { wording } from 'src/app/core/constants/wording/wording';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { Theme } from 'src/app/core/models/Theme.enum';
import { AccountService } from 'src/app/core/services/account.service';
import { CurrentUserService } from 'src/app/shared/services/current-user.service';

@Component({
  selector: 'account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {
  // Store reference to the `AccountLocale` enum
  public NvLocale = NvLocale;
  public DEBIT_CREDIT = DEBIT_CREDIT;
  public wording = wording;
  public sitemap = sitemap;
  public theme: Theme;
  public Theme = Theme;
  public themes = [
    { label: wording.general.auto, value: Theme.auto },
    { label: wording.general.light, value: Theme.light },
    { label: wording.general.dark, value: Theme.dark },
  ];
  constructor(
    private authService: AuthenticationService,
    public userRepository: UserRepositoryService,
    public settingsService: SettingsService,
    private gridI18nService: NvGridI18nService,
    private accountService: AccountService,
    private currentUserService: CurrentUserService,
  ) { }

  public ngOnInit() {
    this.theme = this.settingsService.theme;
  }

  public onDebitCreditFormatChange(debitCredit: DEBIT_CREDIT) {
    const config: IAccountConfigGeneral = {
      language: this.settingsService.language,
      locale: this.settingsService.locale,
      debitCredit: debitCredit,
      avatarId: this.authService.getSessionInfo().user.avatarId,
      theme: this.settingsService.theme,
    };

    this.settingsService.save(config).subscribe(() => {
      this.settingsService.debitCredit = debitCredit;
      this.authService.storeUserConfigInfo(config);
    });
  }

  public onLanguageChange(language: Language) {
    const config: IAccountConfigGeneral = {
      language,
      locale: this.settingsService.locale,
      debitCredit: this.settingsService.debitCredit,
      avatarId: this.authService.getSessionInfo().user.avatarId,
      theme: this.settingsService.theme,
    };
    // Just a fast way to save the language in the SettingsService
    // This needs to get refactored

    this.settingsService.save(config).subscribe(() => {
      this.settingsService.language = language;
      this.authService.storeUserConfigInfo(config);
      this.updateNvGridConfig();
      this.currentUserService.configsUpdated(config);
    });
  }

  public onLocaleChange(locale: NvLocale): void {
    const config: IAccountConfigGeneral = {
      language: this.settingsService.language,
      locale,
      debitCredit: this.settingsService.debitCredit,
      avatarId: this.authService.getSessionInfo().user.avatarId,
      theme: this.settingsService.theme,
    };
    this.settingsService.save(config).subscribe(() => {
      this.settingsService.locale = locale;
      this.authService.storeUserConfigInfo(config);
      this.updateNvGridConfig();
    });
  }

  private updateNvGridConfig() {
    this.gridI18nService.setConfiguration({
      gridLanguage: this.settingsService.language,
      gridLocale: nvLocalMapper[this.settingsService.locale]
    });
  }

  public onThemeChange(theme: Theme) {
    const config: IAccountConfigGeneral = {
      language: this.settingsService.language,
      locale: this.settingsService.locale,
      debitCredit: this.settingsService.debitCredit,
      avatarId: this.authService.getSessionInfo().user.avatarId,
      theme,
    };
    this.settingsService.save(config).subscribe(() => {
      this.settingsService.theme = theme;
      this.authService.storeUserConfigInfo(config);
    });
  }

  public resetPassword = () => {
    this.accountService.resetPassword(this.authService.getUser());
  }
}
