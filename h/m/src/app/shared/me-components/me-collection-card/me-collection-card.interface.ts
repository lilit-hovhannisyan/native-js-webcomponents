import { MeContextMenuI } from '../me-context-menu/me-context-menu-inteface';

import { MeUserImageI } from '../me-user-image/me-user-image.interface';

export interface MeCollectionCardI {
  selectionEnabled: boolean;
  contextMenu: MeContextMenuI;

  oid: string;
  userProfile: MeUserImageI;
  creator: string;
  name: string;
  numberOfMaterials: number;
  numberOfCustomers: number;
  numberOfTeams: number;
  restMaterialsNumber: number;
  firstThreeMaterialsThumbnailUrl: string[];
  lastUpdatedPlaceholder: string;
  dateDiff: string;
  selected: boolean;
}
