import { FormGroup } from '@angular/forms';
import { MeInputErrorI } from '../me-input-error/me-input-error.interface';

export interface MeDateTimeInputI {
  form: FormGroup;
  inputErrors: MeInputErrorI;
  internalName: string;
  displayName: string;
  required: boolean;
  value: string;
  disabled: boolean;
}
