import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { distinctUntilChanged } from 'rxjs/operators';
import { wording } from 'src/app/core/constants/wording/wording';
import { IVessel, IVesselWithAccountings } from 'src/app/core/models/resources/IVessel';
import { IVesselAccountingFormattedDates } from 'src/app/core/models/resources/IVesselAccounting';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';

export interface IVesselValues {
  vesselId: number;
  vesselAccountingId: number;
  vesselName: string;
}

@Component({
  selector: 'nv-vessel-select-modal',
  templateUrl: './vessel-select-modal.component.html',
  styleUrls: [ './vessel-select-modal.component.scss' ]
})
export class VesselSelectModalComponent implements OnInit {
  public wording = wording;
  public form: FormGroup;
  public vessels: IVesselWithAccountings[] = [];
  public vesselAccountings: IVesselAccountingFormattedDates[] = [];
  @Input() public submit: (vessel: IVessel, vesselAccounting: IVesselAccountingFormattedDates) => void;
  @Input() public cancel: () => void;
  @Input() public value: IVesselValues;

  constructor(
    private nzModalComponent: NzModalRef,
    private fb: FormBuilder,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
    private vesselMainService: VesselMainService,
  ) { }

  public ngOnInit(): void {
    this.fetchVessels();
    const { vesselId, vesselAccountingId } = this.value || {} as IVesselValues;
    this.form = this.fb.group({
      vesselId: [ vesselId ],
      vesselAccountingId: [ vesselAccountingId ],
    });
    this.handleVesselChange();
  }

  public fetchVessels(): void {
    this.vesselMainService.fetchWithRolesOnly()
      .subscribe(vessels => this.vessels = vessels);
  }

  public setVesselAccountings(vessel: IVesselWithAccountings): void {
    const vesselAccountingCtrl = this.form.get('vesselAccountingId');
    if (!vessel) {
      this.vesselAccountings = [];
      vesselAccountingCtrl.setValue(null);
      return;
    }

    const accountingId = this.vesselAccountings.length ? null : vesselAccountingCtrl.value;
    this.vesselAccountings = this.vesselAccountingRepo.formatArrayFromUntil(vessel.vesselAccountings);
    vesselAccountingCtrl.setValue(accountingId);
  }

  public handleVesselChange(): void {
    this.form.controls.vesselId.valueChanges.pipe(distinctUntilChanged())
      .subscribe(vesselId => {
        const vessel = this.vessels.find(item => item.id === vesselId);
        this.setVesselAccountings(vessel);
      });
  }

  public handleSubmit(): void {
    const { vesselId, vesselAccountingId } = this.form.value;

    const vessel = this.vessels.find(elem => elem.id === vesselId);
    const vesselAccounting = this.vesselAccountings.find(elem => elem.id === vesselAccountingId);
    this.submit(vessel, vesselAccounting);
  }

  public handleCancel(): void {
    this.cancel();
    this.nzModalComponent.close();
  }
}
