import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatMenuModule } from '@angular/material/menu';

import { MeHeaderComponent } from './me-header.component';

import { IconsModule } from '@shared/modules/icons.module';
import { MeUserImageModule } from '@shared-components/me-user-image/me-user-image.module';
@NgModule({
  declarations: [MeHeaderComponent],
  imports: [CommonModule, IconsModule, MeUserImageModule, MatMenuModule],
  exports: [MeHeaderComponent]
})
export class MeHeaderModule {}
