import { Injectable } from '@angular/core';
import { SettingsService } from 'src/app/core/services/settings.service';
import { getDateTimeFormat } from 'src/app/shared/helpers/general';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { VesselKey } from '../../models/resources/IVessel';
import { RepositoryService } from '../repository.service';
import {
  IVesselAccounting,
  IVesselAccountingDTO,
  IVesselAccountingCreate,
  IVesselAccountingFormattedDates
} from '../../models/resources/IVesselAccounting';
import * as moment from 'moment';

export interface VesselAccountingRepoParams extends RepoParams<IVesselAccounting> {
  queryParams?: {
    date?: string;
    vesselId?: VesselKey;
  };
}

@Injectable({
  providedIn: 'root'
})
export class VesselAccountingRepositoryService extends AbstractRepository<IVesselAccounting,
  IVesselAccountingDTO,
  IVesselAccountingCreate,
  VesselAccountingRepoParams,
  VesselKey> {

  protected resourceType = 'VesselAccounting';
  public sortKey = 'from';
  public sortAsce = true;

  constructor(
    repositoryService: RepositoryService,
    private settingsService: SettingsService,
  ) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/vessel-accountings';

  public formatFromUntil(vesselAccounting: IVesselAccounting): IVesselAccountingFormattedDates {
    const {from, until} = vesselAccounting;
    const dateFormat = getDateTimeFormat(this.settingsService.locale);
    return {
      ...vesselAccounting,
      formattedFrom: from && moment(from).format(dateFormat),
      formattedUntil: until && moment(until).format(dateFormat),
    };
  }

  public formatArrayFromUntil(vesselAccountings: IVesselAccounting[]): IVesselAccountingFormattedDates[] {
    return vesselAccountings.map(accounting => this.formatFromUntil(accounting));
  }

  protected postProcess(item: IVesselAccountingDTO): IVesselAccounting { return item; }
}
