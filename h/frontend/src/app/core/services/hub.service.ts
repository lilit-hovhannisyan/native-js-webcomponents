import { Injectable } from '@angular/core';
import { IHttpConnectionOptions, HubConnection, HubConnectionBuilder, LogLevel } from '@microsoft/signalr';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { IUserInfo, IMessage } from 'src/app/features/examples/components/chat-room/real-time-chat/real-time-chat.component';
import { EnvironmentService } from './environment.service';

@Injectable({
  providedIn: 'root'
})
export class HubService {
  public connection: HubConnection;
  public currentUser: IUserInfo;
  public onlineUsers: IUserInfo[] = [];
  public messages: {[key: string]: IMessage[]} = {};

  constructor(
    private authService: AuthenticationService,
    private environmentService: EnvironmentService,
  ) { }

  public initConnection() {
    const session = this.authService.getSessionInfo();
    const options: IHttpConnectionOptions = {
      accessTokenFactory: () => {
        return session.tokenResource.accessToken;
      }
    };

    this.connection = new HubConnectionBuilder()
      .configureLogging(LogLevel.Information)
      .withUrl(`${this.environmentService.apiHost}/hub`, options)
      .build();
  }

  public addListener(methodName: string, callback: (...data: any) => void) {
    this.connection.on(methodName, (...data) => {
      callback(...data);
    });
  }

  public invokeMethod(methodName: string, ...parameters: any[]) {
    this.connection.invoke(methodName, ...parameters).catch(e => console.log(e));
  }

  public startListening(): Promise<void> {
    return this.connection.start().catch(err => console.log(err));
  }

  public stopListening(): Promise<void> {
    return this.connection.stop().catch(err => console.log(err));
  }

}
