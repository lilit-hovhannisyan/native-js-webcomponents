import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { HttpGet } from 'src/app/core/models/http/http-get';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private apiKey = '22d6a4a3a03e74228f4983a42f0049d4';
  constructor(
    private http: HttpClient
  ) { }

  /**
   * API call:
    api.openweathermap.org/data/2.5/weather?q={city name}

    api.openweathermap.org/data/2.5/weather?q={city name},{country code}

    Parameters:
    q city name and country code divided by comma, use ISO 3166 country codes
   */

  public fetchWeatherByCityName(name: string): Observable<any> {
    const url = `https://api.openweathermap.org/data/2.5/weather?APPID=${this.apiKey}&q=${name}&units=metric`;
    const request = new HttpGet(url);
    request.setNoModify();
    return this.http.request(request).pipe(map((res: HttpResponse<any>) => res.body));
  }

  public fetchWeatherByGeographicCoordinates(lat: number, lon: number): Observable<any> {
    const url = `https://api.openweathermap.org/data/2.5/weather?APPID=${this.apiKey}&lat=${lat}&lon=${lon}&units=metric`;
    const request = new HttpGet(url);
    request.setNoModify();
     return this.http.request(request).pipe(map((res: HttpResponse<any>) => res.body));
  }

  public fetchWeatherByZipCode(zip: number): Observable<any> {
    const url = `https://api.openweathermap.org/data/2.5/weather?APPID=${this.apiKey}&zip=${zip}&units=metric`;
    const request = new HttpGet(url);
    request.setNoModify();
     return this.http.request(request).pipe(map((res: HttpResponse<any>) => res.body));
  }

  public fetchWeatherByCityID(id: number): Observable<any> {
    const url = `https://api.openweathermap.org/data/2.5/weather?APPID=${this.apiKey}&id=${id}&units=metric`;
    const request = new HttpGet(url);
    request.setNoModify();
     return this.http.request(request).pipe(map((res: HttpResponse<any>) => res.body));
  }
}
