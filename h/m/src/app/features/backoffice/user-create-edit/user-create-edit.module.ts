import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { UserCreateEditComponent } from './user-create-edit.component';
import { UserCreateEditRoutingModule } from './user-create-edit-routing.module';
import { MeAttributeModule } from '@shared/me-components/me-attribute/me-attribute.module';
import { MeUploadImageModule } from '@shared/me-components/me-upload-image/me-upload-image.module';
import { MeButtonModule } from '@shared/me-components/me-button/me-button.module';

@NgModule({
  declarations: [UserCreateEditComponent],
  imports: [CommonModule, UserCreateEditRoutingModule, MeAttributeModule, MeUploadImageModule, ReactiveFormsModule, MeButtonModule]
})
export class UserCreateEditModule {}
