export default interface ComponentDataI {
  componentName: string;
  version: string;
  props: Array<{ key: string; value: unknown }>;
}
