import { Component, OnInit, ChangeDetectorRef, ViewChild, Input } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { FormGroup, FormBuilder, FormControl, FormGroupDirective } from '@angular/forms';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { Language } from 'src/app/core/models/language';
import { forkJoin, Observable, ReplaySubject } from 'rxjs';
import { map, switchMap, takeUntil, tap } from 'rxjs/operators';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { ActivatedRoute } from '@angular/router';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Validators } from 'src/app/core/classes/validators';
import { IVesselRegistrationMortgage, IVesselRegistrationMortgageCreate, VesselRegistrationMortgageKey } from 'src/app/core/models/resources/IVesselRegistrationMortgage';
import { VesselRegistrationMortgageRepositoryService } from 'src/app/core/services/repositories/vessel-registration-mortgage-repository.service';
import { GridComponent, NvGridConfig } from 'nv-grid';
import { getMortgageGridConfig } from 'src/app/core/constants/nv-grid-configs/mortgage.config';
import { CurrencyKey, ICurrency } from 'src/app/core/models/resources/ICurrency';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { gridConfig as currencyGridConfig } from 'src/app/shared/components/autocomplete/modals/currency-grid-config';
import { VesselRegisterRepositoryService } from 'src/app/core/services/repositories/vessel-registeries-repository.service';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { CurrencyValidatorsService } from 'src/app/core/services/validators/currency-validators.service';
import { RegistrationFleetHelperService } from '../registrations-fleet-helper.service';
export interface IMortgageGridRow {
  mortgagee: string;
  mortgageAmount: number;
  currencyId: CurrencyKey;
  currencyName?: string;
  id?: VesselRegistrationMortgageKey;
}

@Component({
  selector: 'nv-registration-fleet-mortgage',
  templateUrl: './registration-fleet-mortgage.component.html',
  styleUrls: ['./registration-fleet-mortgage.component.scss'],
  providers: [FormGroupDirective]
})
export class RegistrationFleetMortgageComponent extends ResourceDetailView<
IVesselRegistrationMortgage,
IVesselRegistrationMortgageCreate
>
  implements OnInit {
  @Input() public readOnly: boolean;
  protected repoParams: RepoParams<IVesselRegistrationMortgage> = {};
  public form: FormGroup;
  protected resourceDefault: IVesselRegistrationMortgage = {} as IVesselRegistrationMortgage;
  public routeBackUrl: string = url(
    sitemap.chartering.children.registrations.children.registrationsFleet
  );
  private language: Language = this.settingsService.language;
  public isHandset$: Observable<boolean> = this.breakpointObserver
    .observe('(max-width: 1024px)')
    .pipe(map(result => result.matches));
  @ViewChild('mortgageGridComponent') public mortgageGridComponent: GridComponent;
  public mortgageGridConfig: NvGridConfig;
  public dataSource: IMortgageGridRow[] = [];
  private componentDestroyed$ = new ReplaySubject<boolean>(1);
  public currencyGridConfig: NvGridConfig;
  public remarkCtrl: FormControl = new FormControl('', [Validators.maxLength(3000)]);
  private vesselId: VesselKey;
  private vesselRegistrationMortgageListId: VesselRegistrationMortgageKey;

  constructor(
    private fb: FormBuilder,
    private vesselRegistrationMortgageRepositoryService: VesselRegistrationMortgageRepositoryService,
    baseComponentService: BaseComponentService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private breakpointObserver: BreakpointObserver,
    public currencyRepo: CurrencyRepositoryService,
    private currencyValidatorsService: CurrencyValidatorsService,
    private helperService: RegistrationFleetHelperService,
  ) {
    super(vesselRegistrationMortgageRepositoryService, baseComponentService);
  }


  public getTitle: () => string = (): string =>
    this.wording.chartering.registration[this.language]

  public ngOnInit(): void {
    this.creationMode = false;
    this.vesselId = +this.route.snapshot.parent.params.id;
    this.initGridConfig();
    this.fetchData();
  }

  private getEmptyRow(): IMortgageGridRow {
    return {
      currencyId: null,
      mortgageAmount: null,
      mortgagee: null,
      id: -1,
    };
  }

  private fetchData(): void {
    forkJoin([
      this.vesselRegistrationMortgageRepositoryService.fetchAll({
        queryParams: { vesselId: this.vesselId }
      }),
      this.currencyRepo.fetchAll({}),
    ]).pipe(
      map(([vesselRegistrationMortgageList]) => vesselRegistrationMortgageList[0]),
      tap(vesselRegistrationMortgageList => {
        this.remarkCtrl.disable();
        if (vesselRegistrationMortgageList) {
          this.vesselRegistrationMortgageListId = vesselRegistrationMortgageList.id;
          this.remarkCtrl.patchValue(vesselRegistrationMortgageList.remark);
          this.dataSource = this.getGridRows({
            ...vesselRegistrationMortgageList,
            entries: vesselRegistrationMortgageList.entries.length
              ? vesselRegistrationMortgageList.entries
              : [this.getEmptyRow()]
          });
        } else {
          this.dataSource = [this.getEmptyRow()];
        }
      })
    ).subscribe();
  }

  private getGridRows(items: IVesselRegistrationMortgage): IMortgageGridRow[] {
    // id used for index
    return items.entries.map((i, id) => ({
      currencyName: this.currencyRepo.getStreamValue()
        .find(c => c.id === i.currencyId)?.isoCode,
      currencyId: i.currencyId,
      mortgagee: i.mortgagee,
      mortgageAmount: i.mortgageAmount,
      id: id + 1, // id shouldn't be 0 (nv-grid)
    }));
  }

  public enterCreationMode = (): void => {
    this.enterEditMode();
    this.cdr.markForCheck();
    this.helperService.registersSliderDisabledSubject.next(this.editMode);
  }

  private initGridConfig(): void {
    this.currencyGridConfig = currencyGridConfig;
    this.mortgageGridConfig = getMortgageGridConfig(
      {
        delete: (deletedRow: any) => {
          if (this.editMode) {
            this.dataSource = this.dataSource.filter(row => row.id !== deletedRow.id);
          }
        },
      },
      (form: FormGroup) => {
        this.setCurrencyId(form);
      },
      {
        doesCurrencyExist: this.currencyValidatorsService.doesCurrencyIsoExist,
        isCurrencyActive: this.currencyValidatorsService.isCurrencyIsoActive,
      },
      this.readOnly
    );

    this.inEditMode
      .pipe(
        takeUntil(this.componentDestroyed$),
      ).subscribe(inEditMode => {
        this.mortgageGridConfig.editForm.disableEditColumns = !inEditMode;
        if (inEditMode) {
          this.remarkCtrl.enable();
        } else {
          this.remarkCtrl.disable();
        }
      });
  }

  // not used, just here becase it's an abstract method
  public init(
    fleetRegistration: IVesselRegistrationMortgage = this.resourceDefault
  ): void {
    this.initForm(this.fb.group({}));
  }

  public submit() {
    // we need to have at least one empty row to be able to create a new row
    const hasOneEmptyRow: boolean = this.hasOneEmptyRow();
    if (!this.areAllRowsValid() && !hasOneEmptyRow) {
      return;
    }
    const remark: string = this.remarkCtrl.value;
    const rows: IMortgageGridRow[] = hasOneEmptyRow
      ? []
      : this.mortgageGridComponent.rawRows;
    const payload: IVesselRegistrationMortgageCreate = {
      entries: rows.map(r => ({
        mortgagee: r.mortgagee,
        mortgageAmount: r.mortgageAmount,
        currencyId: r.currencyId,
      })),
      remark,
      vesselId: this.vesselId,
    };
    this.vesselRegistrationMortgageListId
      ? this.vesselRegistrationMortgageRepositoryService.update(
        {
          ...payload,
          id: this.vesselRegistrationMortgageListId,
        } as IVesselRegistrationMortgage,
        {}).subscribe(() => {
          this.leaveEditMode();
          this.fetchData();
        })
      : this.vesselRegistrationMortgageRepositoryService.create(payload, {})
        .subscribe(() => {
          this.leaveEditMode();
          this.fetchData();
        });
  }
  public setCurrencyId = (form: FormGroup): void => {
    const isoCode = form.get('currencyName').value;
    const currency: ICurrency = this.currencyRepo.getStreamValue()
      .find(c => c.isoCode === isoCode);
    form.get('currencyId').patchValue(currency ? currency.id : null);
  }

  private areAllRowsValid = (): boolean => {
    const rows: IMortgageGridRow[] = this.mortgageGridComponent.rawRows;
    return !rows.find(r => {
      return !(r.currencyId && r.mortgageAmount && r.mortgagee);
    });
  }

  private hasOneEmptyRow(): boolean {
    const rows: IMortgageGridRow[] = this.mortgageGridComponent.rawRows;
    const firstRow: IMortgageGridRow = rows[0];
    return rows.length === 1 &&
      (!firstRow.currencyId && !firstRow.mortgageAmount && !firstRow.mortgagee);
  }

  public onCancel(): void {
    this.fetchData();
    this.cancelEditing();
    this.helperService.registersSliderDisabledSubject.next(this.editMode);
  }

  public ngOnDestroy(): void {
    this.helperService.registersSliderDisabledSubject.next(false);
  }
}
