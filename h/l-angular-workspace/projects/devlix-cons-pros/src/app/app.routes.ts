import { Routes } from '@angular/router';

import { ListComponent,
    SelectGroupComponent,
    SelectUserComponent
} from './pages';

export const appRoutes: Routes = [
    {path: 'select-group', component: SelectGroupComponent },
    {path: 'select-user', component: SelectUserComponent },
    {path: 'list', component: ListComponent },
    {path: '', redirectTo: 'select-group', pathMatch: 'full'},
];



