import { AfterViewInit, Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';

import { SubSink } from 'subsink';
import { finalize } from 'rxjs/operators';
import { forkJoin, Subscription, Observable } from 'rxjs';

import { ArrayRequestI } from '@core-interfaces/array-request.interface';
import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { EntityI } from '@core-interfaces/entity.interface';
import { GlobalService } from '@core-services/global.service';

import { BaseLoggerComponent } from '@features/tracking-system';
import { GroupsStoreService } from '@features/users/groups-store.service';
import { GroupsWebService } from '@features/users/groups.web-service';
import { UsersStoreService } from '@features/users/users-store.service';
import { UsersWebService } from '@features/users/users.web-service';

import { DomainModel } from '@shared/models/domain/domain.model';
import { GroupModel } from '@shared/models/groups/group.model';
import {
  ME_DOMAIN_APPLICATION_USER,
  ME_DOMAIN_APPLICATION_GROUP,
  MARKETPLACE_BASE_API_URL,
  DEFAULT_USERS_GROUP,
  ME_USER_EMAIL_REGEX,
  DEFAULT_INVITED_USER
} from '@shared/constants';
import { MeAddListCardI } from '@shared/me-components/me-add-list-card/me-add-list-card.interface';
import { MeAddListPopupService } from '@shared/me-components/me-popup-add-list/me-popup-add-list.service';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeBasicInputPopupService } from '@shared/me-components/me-popup-basic-input/me-popup-basic-input.service';
import { MeEmptyContentI } from '@shared/me-components/me-empty-content/me-empty-content-interface';
import { MePopupAddListI } from '@shared/me-components/me-popup-add-list/me-popup-add-list.interface';
import { MePopupBasicInputI } from '@shared/me-components/me-popup-basic-input/me-popup-basic-input.interface';
import { MePopupSelectI } from '@shared/me-components/me-popup-select/me-popup-select.interface';
import { MePopupSelectService } from '@shared/me-components/me-popup-select/me-popup-select.service';
import { MePopupUserInfoI } from '@shared/me-components/me-popup-user-info/me-popup-user-info.interface';
import { MePopupUserInfoService } from '@shared/me-components/me-popup-user-info/me-popup-user-info.service';
import { MeSearchBarI } from '@shared/me-components/me-search-bar/me-search-bar.interface';
import { MeSearchInputI } from '@shared/me-components/me-search-input/me-search-input-interface';
import { MeSearchSortByTypes, MeSearchDirectionTypes } from '@shared/me-search-sort-by-types';
import { MeUserGroupCardI } from '@shared/me-components/me-user-group-card/me-user-group-card.interface';
import { QuickSearchModel } from '@shared/models/search/quick-search.model';
import { UserModel } from '@shared/models/user/user.model';
import { getOid, getUserInitials } from '@shared/utils';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeIntroductionMessageI } from '@shared/me-components/me-introduction-message/me-introduction-message.interface';
import { AuthStoreService } from '@core-services/auth-store.service';
import { MePermissionTypes } from '@shared/me-permission-types';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [
    GroupsStoreService,
    GroupsWebService,
    MeAddListPopupService,
    MeBasicInputPopupService,
    MePopupSelectService,
    MePopupUserInfoService,
    UsersStoreService,
    UsersWebService
  ]
})
export class UsersComponent extends BaseLoggerComponent implements OnInit, AfterViewInit, OnDestroy {
  componentVersion: string = '0.0.1';
  private subs = new SubSink();

  usersView: boolean = true;
  searchBarData!: MeSearchBarI;
  searchBarDataForGroupMembersPopup!: MeSearchBarI;
  usersSearchKeyword: string | undefined = '';
  groupsSearchKeyword: string | undefined = '';
  groupMembersSearchKeyword: string | undefined = '';
  basicInputPopupEmail!: MeSearchInputI;
  basicInputPopupNickname!: MeSearchInputI;
  searchUsersFromAddListPopup!: MeSearchInputI;
  basicPopupInputEmailValue: string | undefined = '';
  selectionEnabled: boolean = false;
  private selectedUsersMap: Map<string, void> = new Map();
  private selectedGroupsMap: Map<string, void> = new Map();
  private usersIntersectionMap: Map<string, void> = new Map();
  areUsersLoading: boolean = true;
  areGroupsLoading: boolean = true;
  userCardItems: MeUserGroupCardI[] = [];
  groupMembersCardItems: MeUserGroupCardI[] = [];
  groupCardItems: MeUserGroupCardI[] = [];
  addUserListCardItems: MeAddListCardI[] = [];
  addGroupListCardItems: MeAddListCardI[] = [];
  userGroupsListCardItems: MeAddListCardI[] = [];
  addUserListCardItemsToSend: string[] = [];
  removeUserListCardItemsToSend: string[] = [];
  domain: DomainModel = new DomainModel();

  addUsersToGroupPopupData!: MePopupSelectI;
  userInfoPopupData!: MePopupUserInfoI;
  addUsersListPopupData!: MePopupAddListI;
  clickedGroupCardItem!: MeUserGroupCardI;

  // @ViewChild('addUserToGroupsList', { read: TemplateRef }) addUserToGroupsList!: TemplateRef<Component>;
  @ViewChild('addUsersList', { read: TemplateRef }) addUsersList!: TemplateRef<Component>;
  @ViewChild('createButton', { read: TemplateRef }) createButton!: TemplateRef<Component>;
  @ViewChild('groupMembersGrid', { read: TemplateRef }) groupMembersGrid!: TemplateRef<Component>;
  @ViewChild('popupBasicInput', { read: TemplateRef }) popupBasicInput!: TemplateRef<Component>;
  @ViewChild('userInfoGroups', { read: TemplateRef }) userInfoGroups!: TemplateRef<Component>;

  createUserPopupData!: MePopupBasicInputI;

  isUserSearch: boolean = false;
  isGroupSearch: boolean = false;
  isGroupMembersSearch: boolean = false;
  usersTrackForBottomReach: boolean = false;
  groupsTrackForBottomReach: boolean = false;
  groupMembersTrackForBottomReach: boolean = false;
  areUsersInPopupLoading: boolean = false;

  emptyUserContent!: MeEmptyContentI;
  emptyGroupContent!: MeEmptyContentI;
  introMsgData!: MeIntroductionMessageI;

  i18nStrings: MeTranslationI = {};

  private readonly NUMBER_OF_ITEMS_ON_PAGE = 50;
  getCardOid = getOid;

  sortByValue: string = '';
  sortByDirection: '' | 'ASC' | 'DESC' = '';

  sortGroupMembersByValue: string = '';
  sortGroupMembersByDirection: '' | 'ASC' | 'DESC' = '';

  _deleteUserSweetAlert!: Subscription;
  _removeUserSweetAlert!: Subscription;

  isAddUser: boolean = false;
  isModifyUser: boolean = false;
  isDeleteUser: boolean = false;

  constructor(
    injector: Injector,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private definitionsStoreService: DefinitionsStoreService,
    private groupsStoreService: GroupsStoreService,
    private groupsWebService: GroupsWebService,
    private meAddListPopupService: MeAddListPopupService,
    private meBasicInputPopupService: MeBasicInputPopupService,
    private mePopupSelectService: MePopupSelectService,
    private translate: MeTranslationService,
    private usersStoreService: UsersStoreService,
    private usersWebService: UsersWebService,
    private basicAlertService: MeBasicAlertService,
    private mePopupUserInfoService: MePopupUserInfoService,
    private meSweetAlertService: MeSweetAlertService,
    private globalService: GlobalService,
    private authStoreService: AuthStoreService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isAddUser =
      this.authStoreService.isAllowed(MePermissionTypes.COMPANY_USER_ANY) ||
      this.authStoreService.isAllowed(MePermissionTypes.COMPANY_USER_CREATE);
    this.isModifyUser =
      this.authStoreService.isAllowed(MePermissionTypes.COMPANY_USER_ANY) ||
      this.authStoreService.isAllowed(MePermissionTypes.COMPANY_USER_MODIFY);
    this.isDeleteUser =
      this.authStoreService.isAllowed(MePermissionTypes.COMPANY_USER_ANY) ||
      this.authStoreService.isAllowed(MePermissionTypes.COMPANY_USER_DELETE);

    this.getTranslations();
    this.setSearchBarData();
    this.setSearhBarDataForGroupMembersPopup();
    this.setEmptyContentData();
    this.setBasicInputSearch();
    this.setIntroductionMessage();

    this.subs.sink = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        if (this.usersView) {
          this.getAllUsers();
          this.prepareUserCardData();
          this.prepareAddListGroupCardData();
        } else {
          this.getAllGroups();
          this.prepareGroupCardData();
          this.prepareAddListUserCardData();
        }
      }
    });
  }

  ngAfterViewInit(): void {
    if (this.isAddUser) {
      this.breadcrumbSupportService.breadcrumbActions = [
        {
          button: {
            id: '1',
            data: { color: 'primary', type: 'transparent' },
            text: this.i18nStrings.create,
            content: this.createButton
          },
          callBack: () => {
            this.createInviteUser();
          }
        }
      ];
    }
  }

  getTranslations(): void {
    this.subs.sink = this.translate.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;
    });
  }

  getAllUsers(skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.globalService.activateLoader();
    this.areUsersLoading = true;
    this.subs.sink = this.usersWebService
      .getMeUsers(skip, top)
      .pipe(
        finalize(() => {
          this.areUsersLoading = false;
          this.globalService.deactivateLoader();
        })
      )
      .subscribe((response: ArrayResponseI<UserModel>) => {
        this.handleUsersResponse(response, append, false);
      });
  }

  createInviteUser(): void {
    this.createUserPopupData = { inputEmailValue: false, title: this.i18nStrings.inviteUser };
    this.subs.sink = this.meBasicInputPopupService.openPopup(this.createUserPopupData, this.popupBasicInput).subscribe((event: string) => {
      if (event === 'invite') {
        if (this.basicPopupInputEmailValue) {
          const inviteUser = {
            userEmail: this.basicPopupInputEmailValue
          };
          this.subs.sink = this.usersWebService.inviteMeUser(inviteUser).subscribe((response: UserModel) => {
            if (response) {
              const userID: string = response.oid;
              // In background add user to Material Coordinators group (finding of this group is hard-coded),
              // and just show pop-up about successfully inviting user.
              const groupID: string | undefined = this.addGroupListCardItems.find((group) => group.groupName === DEFAULT_USERS_GROUP)?.oid;
              if (groupID) {
                this.subs.sink = this.groupsWebService.addMembersToMeGroup(groupID, [userID]).subscribe(() => {
                  this.getAllGroups();
                  this.prepareGroupCardData();
                  this.prepareAddListUserCardData();
                  const sweetAlertModel: MeSweetAlertI = {
                    mode: 'success',
                    icon: 'check',
                    type: {
                      name: MeSweetAlertTypeEnum.confirm,
                      buttons: {
                        confirm: this.i18nStrings.close
                      }
                    },
                    title: this.i18nStrings.usersAdded,
                    message: this.i18nStrings.usersAddedSuccess
                  };
                  this.meSweetAlertService.openMeSweetAlert(sweetAlertModel);
                });
              }
            }
          });
        }
      }
    });
  }

  private handleUsersResponse(response: ArrayResponseI<UserModel>, append: boolean = false, isSearch: boolean = false): void {
    this.usersStoreService.userNextId = response.nextID;
    if (append) {
      this.usersStoreService.appenduserEntities(response.entities);
    } else {
      this.usersStoreService.userEntities = response.entities;
      this.usersStoreService.userTotalCount = response.totalCount;
      this.isUserSearch = isSearch;
      this.usersTrackForBottomReach = true;
      this.selectedUsersMap.clear();
    }

    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.usersTrackForBottomReach = false;
    }
  }

  prepareUserCardData(): void {
    this.subs.sink = this.usersStoreService.userEntities$.subscribe((data: UserModel[] | null) => {
      this.userCardItems = [];
      if (data) {
        this.userCardItems = data.map((user: UserModel) => {
          return {
            contextMenuButton: { type: 'transparent', color: 'warning', rounded: true },
            contextMenuItems: [
              {
                key: user.attributes.isActive ? 'deactivate' : 'activate',
                value: user.attributes.isActive ? this.i18nStrings.deactivate : this.i18nStrings.activate,
                disabled: !this.isModifyUser || this.authStoreService.user?.oid === user.oid
              },
              {
                key: 'delete',
                value: this.i18nStrings.delete,
                disabled: !this.isDeleteUser || this.authStoreService.user?.oid === user.oid
              }
            ],
            firstName: user.attributes.firstName,
            lastName: user.attributes.lastName === DEFAULT_INVITED_USER ? undefined : user.attributes.lastName,
            invitedEmail: user.attributes.firstName === DEFAULT_INVITED_USER ? user.attributes.email : undefined,
            userInitials:
              user.attributes.firstName === DEFAULT_INVITED_USER
                ? 'IU'
                : getUserInitials(user.attributes.firstName + ' ' + user.attributes.lastName),
            status: user.attributes.isActive,
            groups: user.numberOfGroups ? user.numberOfGroups - 1 : 0,
            oid: user.oid,
            selected: this.selectedUsersMap.has(user.oid),
            selectionEnabled: this.selectionEnabled,
            thumbnail: user.attributes.avatar ? `${MARKETPLACE_BASE_API_URL}/files/download/${user.attributes.avatar}` : '',
            type: 'user',
            statusLabel: { type: user.attributes.isActive ? 'regular' : 'inversed', color: 'primary', margin: 'none' }
          };
        });
      }
    });
  }

  getAllGroups(skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.globalService.activateLoader();
    this.areGroupsLoading = true;
    this.subs.sink = this.groupsWebService
      .getMeGroups(skip, top)
      .pipe(
        finalize(() => {
          this.areGroupsLoading = false;
          this.globalService.deactivateLoader();
        })
      )
      .subscribe((response: ArrayResponseI<GroupModel>) => {
        this.handleGroupsResponse(response, append, false);
      });
  }

  private handleGroupsResponse(response: ArrayResponseI<GroupModel>, append: boolean = false, isSearch: boolean = false): void {
    // Remove users groups
    const usersGroupRemoval: string = 'Users';
    response.entities = response.entities.filter(
      (item) => usersGroupRemoval.indexOf(item.attributes.name) && item.attributes.name.length !== 5
    );
    response.totalCount = response.totalCount - 1;

    this.groupsStoreService.groupNextId = response.nextID;
    if (append) {
      this.groupsStoreService.appendgroupEntities(response.entities);
    } else {
      this.groupsStoreService.groupEntities = response.entities;
      this.groupsStoreService.groupTotalCount = response.totalCount;
      this.isGroupSearch = isSearch;
      this.groupsTrackForBottomReach = true;
      this.selectedGroupsMap.clear();
    }
    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.groupsTrackForBottomReach = false;
    }
  }

  private handleGroupMembersResponse(response: ArrayResponseI<UserModel>, append: boolean = false, isSearch: boolean = false): void {
    this.groupsStoreService.groupMembersNextId = response.nextID;
    if (append) {
      this.groupsStoreService.appendgroupMembersEntities(response.entities);
    } else {
      this.groupsStoreService.groupMembersEntities = response.entities;
      this.groupsStoreService.groupMembersTotalCount = response.totalCount;
      this.isGroupMembersSearch = isSearch;
      this.groupMembersTrackForBottomReach = true;
    }

    // If reached end of pagination unsubscribe from scroll
    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.groupMembersTrackForBottomReach = false;
    }
  }

  prepareGroupCardData(): void {
    this.subs.sink = this.groupsStoreService.groupEntities$.subscribe((data: GroupModel[] | null) => {
      this.groupCardItems = [];
      if (data) {
        this.groupCardItems = data.map((group: GroupModel) => {
          return {
            contextMenuButton: { type: 'transparent', color: 'warning', rounded: true },
            contextMenuItems: [
              { key: 'addRemoveUsers', value: this.i18nStrings.addOrRemoveUsers, disabled: !(this.isAddUser && this.isDeleteUser) }
            ],
            groupName: group.attributes.name,
            userInitials: getUserInitials(group.attributes.name),
            groupDescription: group.attributes.description,
            groupRole: group.attributesExtended.roleRef,
            users: group.numberOfUsers,
            oid: group.oid,
            selected: this.selectedGroupsMap.has(group.oid),
            selectionEnabled: this.selectionEnabled,
            thumbnail: '',
            type: 'group',
            statusLabel: { type: 'inversed', color: 'primary', margin: 'none' }
          };
        });
      }
    });
  }

  // Get data for Add list user popup
  prepareAddListUserCardData(): void {
    this.subs.sink = this.usersStoreService.userEntities$.subscribe((data: UserModel[] | null) => {
      this.addUserListCardItems = [];
      if (data) {
        this.addUserListCardItems = data.map((user: UserModel) => {
          return {
            firstName: user.attributes.firstName,
            lastName: user.attributes.lastName === DEFAULT_INVITED_USER ? undefined : user.attributes.lastName,
            invitedEmail: user.attributes.firstName === DEFAULT_INVITED_USER ? user.attributes.email : undefined,
            userInitials:
              user.attributes.firstName === DEFAULT_INVITED_USER
                ? 'IU'
                : getUserInitials(user.attributes.firstName + ' ' + user.attributes.lastName),
            uid: user.attributes.uid,
            oid: user.oid,
            thumbnail: user.attributes.avatar ? `${MARKETPLACE_BASE_API_URL}/files/download/${user.attributes.avatar}` : '',
            type: 'user',
            showButton: true,
            isInGroup: this.usersIntersectionMap.has(user.oid)
          };
        });
      }
    });
  }

  prepareAddListGroupCardData(): void {
    this.getAllGroups();
    this.subs.sink = this.groupsStoreService.groupEntities$.subscribe((data: GroupModel[] | null) => {
      this.addGroupListCardItems = [];
      if (data) {
        this.addGroupListCardItems = data.map((group: GroupModel) => {
          return {
            groupName: group.attributes.name,
            initials: getUserInitials(group.attributes.name),
            groupRole: group.attributesExtended.roleRef,
            description: group.attributes.description,
            oid: group.oid,
            thumbnail: '',
            type: 'group',
            showButton: true
          };
        });
      }
    });
  }

  prepareAddListGroupsCardData(): void {
    // Waiting for BE
  }

  // Set search bar for USERS/GROUPS
  setSearchBarData(): void {
    this.searchBarData = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: '',
        minCharacters: 1
      },
      usersGroupsSelect: true,
      usersGroups: {
        options: [
          { key: 'users', value: this.i18nStrings.users },
          { key: 'groups', value: this.i18nStrings.groups }
        ],
        selectedIndex: 0
      },
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'azAlphabetically', value: 'A - Z' },
          { key: 'zaAlphabetically', value: 'Z - A' },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      }
    };
  }

  // Set search bar from group members popup
  setSearhBarDataForGroupMembersPopup(): void {
    this.searchBarDataForGroupMembersPopup = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: '',
        minCharacters: 1
      },
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'azAlphabetically', value: 'A - Z' },
          { key: 'zaAlphabetically', value: 'Z - A' },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      },
      addButton:
        this.isAddUser && this.isDeleteUser
          ? {
              placeholder: this.i18nStrings.addOrRemoveUsers
            }
          : undefined
    };
  }

  setEmptyContentData(): void {
    this.emptyUserContent = {
      title: this.i18nStrings.inviteYourColleagueTitle,
      comment: this.i18nStrings.inviteYourColleagueDescription,
      buttonText: this.i18nStrings.inviteUser,
      emptyIconName: 'empty-colleagues'
    };

    this.emptyGroupContent = {
      title: 'Groups Title',
      comment: 'Groups Description',
      buttonText: 'Create/Add Groups',
      emptyIconName: 'empty-materials'
    };
  }

  // Set various search inputs
  setBasicInputSearch(): void {
    this.basicInputPopupEmail = {
      placeholder: this.i18nStrings.email,
      keyword: ''
    };
    this.searchUsersFromAddListPopup = {
      placeholder: this.i18nStrings.users,
      keyword: ''
    };
  }

  // Set introduction message data
  setIntroductionMessage(): void {
    this.introMsgData = {
      content: 'Invite and manage your team members. Add users to one or more groups to set roles and permissions.',
      buttonText: this.i18nStrings.learnMore
    };
  }

  btnEventHandler(): void {
    window.open('https://5883447.hs-sites.com/knowledge/managing-users-groups-suppliers', '_blank');
  }

  // Get EMAIL from create new user popup
  handleBasicInputEmailEvent(inputValue: string): void {
    this.basicPopupInputEmailValue = inputValue;
    const validEmail = ME_USER_EMAIL_REGEX.test(this.basicPopupInputEmailValue);
    if (validEmail) {
      this.createUserPopupData = { ...this.createUserPopupData, inputEmailValue: true };
    } else {
      this.createUserPopupData = { ...this.createUserPopupData, inputEmailValue: false };
    }

    this.meBasicInputPopupService.pushDataModelToPopup(this.createUserPopupData);
  }

  handleSearchInputEvent(searchValue: string, skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    if (this.usersView) {
      this.usersSearchKeyword = searchValue;
    } else {
      this.groupsSearchKeyword = searchValue;
    }

    this.searchBarData = {
      ...this.searchBarData,
      search: { placeholder: this.i18nStrings.searchFor, keyword: searchValue as string }
    };

    const searchFilter: QuickSearchModel = {
      domain: this.usersView ? ME_DOMAIN_APPLICATION_USER : ME_DOMAIN_APPLICATION_GROUP,
      criteriaQuick: searchValue.trim() || undefined,
      orderByAttr: this.sortByValue.trim() || undefined,
      orderByDir: this.sortByDirection.trim() || undefined
    };

    this.globalService.activateLoader();
    if (this.usersView) {
      this.subs.sink = this.usersWebService
        .searchUsers(searchFilter, skip, top)
        .pipe(
          finalize(() => {
            if (this.usersView) {
              this.isUserSearch = true;
            } else {
              this.isGroupSearch = true;
            }
            this.globalService.deactivateLoader();
          })
        )
        .subscribe((response: ArrayResponseI<UserModel>) => {
          this.handleUsersResponse(response, append, true);
        });
    } else {
      this.subs.sink = this.groupsWebService
        .searchGroups(searchFilter, skip, top)
        .pipe(
          finalize(() => {
            this.globalService.deactivateLoader();
          })
        )
        .subscribe((response: ArrayResponseI<GroupModel>) => {
          this.handleGroupsResponse(response, append, true);
        });
    }
  }

  handleSortByEvent(sortValue: string): void {
    if (this.searchBarData.sort) {
      this.searchBarData = {
        ...this.searchBarData,
        sort: {
          ...this.searchBarData.sort,
          selectedIndex: this.searchBarData.sort?.options.findIndex((x) => x.key === sortValue)
        }
      };
    }

    switch (sortValue) {
      case 'azAlphabetically':
        if (this.usersView) {
          this.sortByValue = MeSearchSortByTypes.lastName;
          this.sortByDirection = MeSearchDirectionTypes.ascending;
        } else {
          this.sortByValue = MeSearchSortByTypes.name;
          this.sortByDirection = MeSearchDirectionTypes.ascending;
        }
        this.handleSearchInputEvent('');
        break;
      case 'zaAlphabetically':
        if (this.usersView) {
          this.sortByValue = MeSearchSortByTypes.lastName;
          this.sortByDirection = MeSearchDirectionTypes.descending;
        } else {
          this.sortByValue = MeSearchSortByTypes.name;
          this.sortByDirection = MeSearchDirectionTypes.descending;
        }
        this.handleSearchInputEvent('');
        break;

      case 'newest':
        this.sortByValue = MeSearchSortByTypes.created;
        this.sortByDirection = MeSearchDirectionTypes.descending;
        this.handleSearchInputEvent('');
        break;
      case 'oldest':
        this.sortByValue = MeSearchSortByTypes.created;
        this.sortByDirection = MeSearchDirectionTypes.ascending;
        this.handleSearchInputEvent('');
        break;
      default:
        throw new Error('No default option.');
    }
  }

  handleSortByGroupMembersEvent(sortValue: string): void {
    if (this.searchBarDataForGroupMembersPopup.sort) {
      this.searchBarDataForGroupMembersPopup = {
        ...this.searchBarDataForGroupMembersPopup,
        sort: {
          ...this.searchBarDataForGroupMembersPopup.sort,
          selectedIndex: this.searchBarDataForGroupMembersPopup.sort?.options.findIndex((x) => x.key === sortValue)
        }
      };
    }

    switch (sortValue) {
      case 'azAlphabetically':
        this.sortGroupMembersByValue = MeSearchSortByTypes.lastName;
        this.sortGroupMembersByDirection = MeSearchDirectionTypes.ascending;
        this.handleSearchInputGroupMembersEvent('');
        break;
      case 'zaAlphabetically':
        this.sortGroupMembersByValue = MeSearchSortByTypes.lastName;
        this.sortGroupMembersByDirection = MeSearchDirectionTypes.descending;
        this.handleSearchInputGroupMembersEvent('');
        break;
      case 'newest':
        this.sortGroupMembersByValue = MeSearchSortByTypes.created;
        this.sortGroupMembersByDirection = MeSearchDirectionTypes.descending;
        this.handleSearchInputGroupMembersEvent('');
        break;
      case 'oldest':
        this.sortGroupMembersByValue = MeSearchSortByTypes.created;
        this.sortGroupMembersByDirection = MeSearchDirectionTypes.ascending;
        this.handleSearchInputGroupMembersEvent('');
        break;
      default:
        throw new Error('No default option.');
    }
  }

  handleSearchInputGroupMembersEvent(
    searchValue: string,
    skip: number = 0,
    top: number = this.NUMBER_OF_ITEMS_ON_PAGE,
    append: boolean = false
  ): void {
    this.groupMembersSearchKeyword = searchValue;

    this.searchBarDataForGroupMembersPopup = {
      ...this.searchBarDataForGroupMembersPopup,
      search: { placeholder: this.i18nStrings.searchFor, keyword: searchValue as string }
    };

    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_USER,
      criteriaQuick: searchValue.trim() || undefined,
      orderByAttr: this.sortGroupMembersByValue.trim() || undefined,
      orderByDir: this.sortGroupMembersByDirection.trim() || undefined
    };

    this.globalService.activateLoader();
    this.subs.sink = this.groupsWebService
      .searchMembersInGroups(this.clickedGroupCardItem.oid, searchFilter, skip, top)
      .pipe(
        finalize(() => {
          this.globalService.deactivateLoader();
        })
      )
      .subscribe((response: ArrayResponseI<UserModel>) => {
        this.handleGroupMembersResponse(response, append, true);
      });
  }

  handleSelectUsersGroupsEvent(selectedValue: string): void {
    if (this.searchBarData.usersGroups) {
      this.searchBarData = {
        ...this.searchBarData,
        usersGroups: {
          ...this.searchBarData.usersGroups,
          selectedIndex: this.searchBarData.usersGroups.options.findIndex((x) => x.key === selectedValue)
        }
      };
    }

    // Reset search bar when changing views
    /* this.selectionEnabled = false;
    this.searchBarData = {
      ...this.searchBarData,
      selectButton: {
        ...this.searchBarData.selectButton,
        enabled: false,
        placeholder: this.i18nStrings.select
      }
    }; */

    if (selectedValue === 'users') {
      this.usersView = true;
      this.getAllUsers();
      this.prepareUserCardData();
    } else {
      this.usersView = false;
      this.getAllGroups();
      this.prepareGroupCardData();
      this.prepareAddListUserCardData();
    }
  }

  handleToggleSelectEvent(): void {
    this.selectionEnabled = !this.selectionEnabled;
    this.searchBarData = {
      ...this.searchBarData,
      selectButton: {
        ...this.searchBarData.selectButton,
        enabled: this.selectionEnabled,
        placeholder: this.selectionEnabled ? this.i18nStrings.cancel : this.i18nStrings.select
      }
    };

    // Prepare cards for selection based on view
    if (this.usersView) {
      this.prepareUserCardData();
    } else {
      this.prepareGroupCardData();
      this.prepareAddListUserCardData();
    }
  }

  handleViewTypeEvent(): void {
    this.searchBarData = { ...this.searchBarData, view: this.searchBarData.view === 'grid' ? 'list' : 'grid' };
  }

  handleSearchBarEvents(event: { eventName: string; payload?: string | boolean }): void {
    switch (event.eventName) {
      case 'searchEnabledToggle':
        // TODO
        break;
      case 'edit':
        // TODO
        break;
      case 'trash':
        // TODO
        break;
      case 'copy':
        // TODO
        break;
      case 'more':
        // TODO
        break;
      case 'download':
        // TODO
        break;
    }
  }

  // Triggering adding users popup from group info popup
  addingUsersFromGroupInfoPopup(): void {
    if (this.clickedGroupCardItem) {
      this.prepareAddUserListData(this.clickedGroupCardItem.oid);
    }
  }

  // Prepare data for add list popup
  prepareAddUserListData(groupCardOID: string, isCardEvent: boolean = false): void {
    // update add list data cards with is in group
    this.prepareAddListUserCardData();

    // Trigger opening popup
    this.addUsersListPopupData = { title: this.i18nStrings.addUser };
    this.subs.sink = this.meAddListPopupService.openPopup(this.addUsersListPopupData, this.addUsersList).subscribe((popUpEvent: string) => {
      if (popUpEvent === 'addUsersAndClose') {
        const fjObject: { [key: string]: Observable<boolean> } = {};
        if (this.addUserListCardItemsToSend && this.addUserListCardItemsToSend.length > 0) {
          fjObject.add = this.groupsWebService.addMembersToMeGroup(groupCardOID, this.addUserListCardItemsToSend);
        }
        if (this.removeUserListCardItemsToSend && this.removeUserListCardItemsToSend.length > 0) {
          fjObject.remove = this.groupsWebService.removeMembersFromMeGroup(groupCardOID, this.removeUserListCardItemsToSend);
        }
        if (Object.keys(fjObject).length > 0) {
          this.subs.sink = forkJoin(fjObject).subscribe(() => {
            this.addUserListCardItemsToSend = [];
            this.removeUserListCardItemsToSend = [];

            this.basicAlertService.openBasicAlert({
              mode: 'success',
              title: this.i18nStrings.successfullyUpdated,
              content: this.i18nStrings.usersInGroupUpdated
            });
            if (isCardEvent) {
              this.getAllGroups();
            } else {
              this.getAllGroupMembers(groupCardOID);
            }
          });
        }
      }
    });
  }

  bottomReachedHandler(): void {
    if (this.usersView) {
      if (this.usersTrackForBottomReach) {
        if (this.isUserSearch) {
          this.handleSearchInputEvent(this.usersSearchKeyword || '', this.usersStoreService.userNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
        } else {
          this.getAllUsers(this.usersStoreService.userNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
        }
      }
    } else {
      if (this.groupsTrackForBottomReach) {
        if (this.isGroupSearch) {
          this.handleSearchInputEvent(
            this.groupsSearchKeyword || '',
            this.groupsStoreService.groupNextId,
            this.NUMBER_OF_ITEMS_ON_PAGE,
            true
          );
        } else {
          this.getAllGroups(this.groupsStoreService.groupNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
        }
      } else if (this.groupMembersTrackForBottomReach) {
        if (this.isGroupMembersSearch) {
          this.handleSearchInputGroupMembersEvent(
            this.groupMembersSearchKeyword || '',
            this.groupsStoreService.groupMembersNextId,
            this.NUMBER_OF_ITEMS_ON_PAGE,
            true
          );
        } else {
          this.getAllGroupMembers(
            this.clickedGroupCardItem.oid,
            this.groupsStoreService.groupMembersNextId,
            this.NUMBER_OF_ITEMS_ON_PAGE,
            true
          );
        }
      }
    }
  }

  bottomReachedHandlerForAddList(): void {
    if (this.usersTrackForBottomReach) {
      if (this.isUserSearch) {
        this.handleSearchInputEvent(this.usersSearchKeyword || '', this.usersStoreService.userNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
      } else {
        this.getAllUsers(this.usersStoreService.userNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
      }
    }
  }

  // Toggle user card selection
  toggleUserCardItemSelection(index: number): void {
    if (this.selectedUsersMap.has(this.userCardItems[index].oid)) {
      this.userCardItems[index] = { ...this.userCardItems[index], selected: false };
      this.selectedUsersMap.delete(this.userCardItems[index].oid);
    } else {
      this.userCardItems[index] = { ...this.userCardItems[index], selected: true };
      this.selectedUsersMap.set(this.userCardItems[index].oid);
    }
  }

  // Toggle group card selection
  toggleGroupCardItemSelection(index: number): void {
    if (this.selectedGroupsMap.has(this.groupCardItems[index].oid)) {
      this.groupCardItems[index] = { ...this.groupCardItems[index], selected: false };
      this.selectedGroupsMap.delete(this.groupCardItems[index].oid);
    } else {
      this.groupCardItems[index] = { ...this.groupCardItems[index], selected: true };
      this.selectedGroupsMap.set(this.groupCardItems[index].oid);
    }
  }

  getUserGroups(userOID: string): void {
    this.subs.sink = this.usersWebService.getMeUsersGroups(userOID).subscribe((response: ArrayResponseI<GroupModel>) => {
      this.userGroupsListCardItems = [];
      if (response) {
        // Remove users groups
        const usersGroupRemoval: string = 'Users';
        response.entities = response.entities.filter(
          (item) => usersGroupRemoval.indexOf(item.attributes.name) && item.attributes.name.length !== 5
        );
        response.totalCount = response.totalCount - 1;

        this.usersStoreService.userGroupsEntities = response.entities;
        this.usersStoreService.userGroupsTotalCount = response.totalCount;
        this.usersStoreService.userGroupsNextId = response.nextID;

        this.userGroupsListCardItems = response.entities.map((group: GroupModel) => {
          return {
            groupName: group.attributes.name,
            initials: getUserInitials(group.attributes.name),
            description: group.attributes.description,
            groupRole: group.attributesExtended.roleRef,
            oid: group.oid,
            thumbnail: '',
            type: 'group',
            showButton: false
          };
        });
      }
    });
  }

  userCardActions(cardIdem: MeUserGroupCardI): void {
    this.prepareAddListGroupsCardData();

    for (const user of this.usersStoreService.userEntities) {
      if (user.oid === cardIdem.oid) {
        this.userInfoPopupData = {
          firstName: user.attributes.firstName,
          lastName: user.attributes.lastName === DEFAULT_INVITED_USER ? undefined : user.attributes.lastName,
          userInitials:
            user.attributes.firstName === DEFAULT_INVITED_USER
              ? 'IU'
              : getUserInitials(user.attributes.firstName + ' ' + user.attributes.lastName),
          email: user.attributes.email,
          status: user.attributes.isActive,
          uid: user.attributes.uid,
          oid: user.oid,
          thumbnail: user.attributes.avatar ? `${MARKETPLACE_BASE_API_URL}/files/download/${user.attributes.avatar}` : ''
        };
      }
    }

    this.getUserGroups(cardIdem.oid);

    this.mePopupUserInfoService.openPopup(this.userInfoPopupData, this.userInfoGroups);
  }

  memberCardActions(index: number, cardIdem: MeUserGroupCardI): void {
    console.log(index);
    console.log(cardIdem);
  }

  // Trigger popup for showing group members and get group members from groups store
  openGroupInfoPopup(cardItem: MeUserGroupCardI): void {
    this.clickedGroupCardItem = cardItem;
    this.getAllGroupMembers(cardItem.oid);

    // Get group members from groups store
    this.subs.sink = this.groupsStoreService.groupMembersEntities$.subscribe((data: UserModel[] | null) => {
      this.groupMembersCardItems = [];
      if (data) {
        // Fill map with group members oid
        this.usersIntersectionMap.clear();
        for (const d of data) {
          this.usersIntersectionMap.set(d.oid);
        }

        this.groupMembersCardItems = data.map((user: UserModel) => {
          return {
            contextMenuButton: { type: 'transparent', color: 'warning', rounded: true },
            contextMenuItems: [{ key: 'remove', value: this.i18nStrings.remove, disabled: !this.isDeleteUser }],
            firstName: user.attributes.firstName,
            lastName: user.attributes.lastName === DEFAULT_INVITED_USER ? undefined : user.attributes.lastName,
            invitedEmail: user.attributes.firstName === DEFAULT_INVITED_USER ? user.attributes.email : undefined,
            userInitials:
              user.attributes.firstName === DEFAULT_INVITED_USER
                ? 'IU'
                : getUserInitials(user.attributes.firstName + ' ' + user.attributes.lastName),
            status: user.attributes.isActive,
            groups: user.numberOfGroups ? user.numberOfGroups - 1 : 0,
            oid: user.oid,
            selected: this.selectedUsersMap.has(user.oid),
            selectionEnabled: this.selectionEnabled,
            thumbnail: user.attributes.avatar ? `${MARKETPLACE_BASE_API_URL}/files/download/${user.attributes.avatar}` : '',
            type: 'user',
            statusLabel: { type: user.attributes.isActive ? 'regular' : 'inversed', color: 'primary', margin: 'none' }
          };
        });
      }
    });

    const popupTitle: string = cardItem.groupName + ' - ' + this.i18nStrings.users;

    // Trigger opening of popup
    this.addUsersToGroupPopupData = { selectedCount: 0, title: popupTitle, showActions: false };
    this.mePopupSelectService.openPopupSelect(this.addUsersToGroupPopupData, this.groupMembersGrid);
  }

  // Get all group members from group
  getAllGroupMembers(
    groupOID: string,
    skip: number = 0,
    top: number = this.NUMBER_OF_ITEMS_ON_PAGE,
    append: boolean = false
  ): Promise<unknown> {
    this.areUsersInPopupLoading = true;
    const promise = new Promise<void>((resolve, reject) => {
      this.groupsWebService
        .getMeGroupsMembers(groupOID, skip, top)
        .pipe(
          finalize(() => {
            this.areUsersInPopupLoading = false;
          })
        )
        .toPromise()
        .then(
          (response: ArrayResponseI<UserModel>) => {
            this.handleGroupMembersResponse(response, append, false);
            resolve();
          },
          (msg) => {
            reject(msg);
          }
        );
    });

    return promise;
  }

  // Update status of a user
  updateMeUserStatus(cardOID: string, statusChange: ArrayRequestI<EntityI>): void {
    this.subs.sink = this.usersWebService.updateMeUser(cardOID, statusChange).subscribe((response: UserModel) => {
      if (response) {
        this.getAllUsers();
        this.basicAlertService.openBasicAlert({
          mode: 'success',
          title: this.i18nStrings.statusChange,
          content: this.i18nStrings.statusChangeSuccess
        });
      }
    });
  }

  handleUserCardOutputEvent(event: { eventName: string; payload: unknown }, cardItem: MeUserGroupCardI): void {
    switch (event.payload) {
      case 'activate':
        // Set active status for user
        const activateUser: ArrayRequestI<EntityI> = {
          entities: [
            {
              domainName: ME_DOMAIN_APPLICATION_USER,
              attributes: {
                isActive: true
              }
            }
          ]
        };
        this.updateMeUserStatus(cardItem.oid, activateUser);
        break;
      case 'deactivate':
        // Set inactive status for user
        const deactivateUser: ArrayRequestI<EntityI> = {
          entities: [
            {
              domainName: ME_DOMAIN_APPLICATION_USER,
              attributes: {
                isActive: false
              }
            }
          ]
        };
        this.updateMeUserStatus(cardItem.oid, deactivateUser);
        break;
      case 'delete':
        this._deleteUserSweetAlert = this.meSweetAlertService.getDataBackFromSweetAlert().subscribe((data) => {
          if (data && data.confirmed) {
            this.subs.sink = this.usersWebService.deleteMeUser(cardItem.oid).subscribe(() => {
              if (this.usersView) {
                this.getAllUsers();
              } else {
                this.getAllGroupMembers(this.clickedGroupCardItem.oid);
              }
            });
          }
          this._deleteUserSweetAlert.unsubscribe();
        });
        const sweetAlertModel: MeSweetAlertI = {
          mode: 'danger',
          icon: 'alert-triangle',
          type: {
            name: MeSweetAlertTypeEnum.submit,
            buttons: {
              submit: this.i18nStrings.delete,
              cancel: this.i18nStrings.cancel
            }
          },
          title: this.i18nStrings.deleteUser,
          message: this.i18nStrings.areYouSureYouWantToDeleteUser
        };
        this.meSweetAlertService.openMeSweetAlert(sweetAlertModel);
        break;
      default:
        throw new Error('No default option.');
    }
  }

  handleGroupUserCardOutputEvent(event: { eventName: string; payload: unknown }, cardItem: MeUserGroupCardI): void {
    switch (event.payload) {
      case 'remove':
        this._removeUserSweetAlert = this.meSweetAlertService.getDataBackFromSweetAlert().subscribe((data) => {
          if (data && data.confirmed) {
            this.subs.sink = this.groupsWebService.removeMembersFromMeGroup(this.clickedGroupCardItem.oid, [cardItem.oid]).subscribe(() => {
              this.getAllGroupMembers(this.clickedGroupCardItem.oid);
            });
          }
          this._removeUserSweetAlert.unsubscribe();
        });
        const sweetAlertModel: MeSweetAlertI = {
          mode: 'danger',
          icon: 'alert-triangle',
          type: {
            name: MeSweetAlertTypeEnum.submit,
            buttons: {
              submit: this.i18nStrings.remove,
              cancel: this.i18nStrings.cancel
            }
          },
          title: this.i18nStrings.removeUser,
          message: this.i18nStrings.areYouSureYouWantToRemoveUser
        };
        this.meSweetAlertService.openMeSweetAlert(sweetAlertModel);
        break;
      default:
        throw new Error('No default option.');
    }
  }

  // Handle group card context menu events
  handleGroupCardOutputEvent(event: { eventName: string; payload: unknown }, cardItem: MeUserGroupCardI): void {
    switch (event.payload) {
      case 'addRemoveUsers':
        this.clickedGroupCardItem = cardItem;
        this.getAllGroupMembers(cardItem.oid).then(() => {
          this.subs.sink = this.groupsStoreService.groupMembersEntities$.subscribe((data: UserModel[]) => {
            this.usersIntersectionMap.clear();
            for (const d of data) {
              this.usersIntersectionMap.set(d.oid);
            }
          });
          this.prepareAddUserListData(cardItem.oid, true);
        });
        break;
      default:
        throw new Error('No default option.');
    }
  }

  handleSearchUsersFromAddListPopup(
    searchValue: string,
    skip: number = 0,
    top: number = this.NUMBER_OF_ITEMS_ON_PAGE,
    append: boolean = false
  ): void {
    this.usersSearchKeyword = searchValue;

    this.searchUsersFromAddListPopup = {
      ...this.searchUsersFromAddListPopup,

      placeholder: this.i18nStrings.searchFor,
      keyword: searchValue as string
    };

    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_USER,
      criteriaQuick: searchValue.trim() || undefined
    };

    this.subs.sink = this.usersWebService.searchUsers(searchFilter, skip, top).subscribe((response: ArrayResponseI<UserModel>) => {
      this.handleUsersResponse(response, append, true);
    });
  }

  handleAddListCardEvent(event: { eventName: string; payload: MeAddListCardI }, index: number, cardList: MeAddListCardI[]): void {
    switch (event.eventName) {
      case 'add':
        if (event.payload && event.payload.oid) {
          if (!this.addUserListCardItemsToSend.includes(event.payload.oid)) {
            this.addUserListCardItemsToSend.push(event.payload.oid);
          }
        }
        break;
      case 'remove':
        if (event.payload && event.payload.oid) {
          if (
            !this.removeUserListCardItemsToSend.includes(event.payload.oid) &&
            !this.addUserListCardItemsToSend.includes(event.payload.oid)
          ) {
            this.removeUserListCardItemsToSend.push(event.payload.oid);
          } else {
            this.addUserListCardItemsToSend = this.addUserListCardItemsToSend.filter((card) => card !== event.payload.oid);
          }
        }
        break;
    }
    cardList[index] = {
      ...cardList[index],
      isInGroup: this.addUserListCardItemsToSend.includes(event.payload.oid)
    };
  }

  handleUserGroupsEvent(event: { eventName: string; payload: MeAddListCardI }): void {
    console.log(event);
    switch (event.eventName) {
      case 'add':
        break;
      case 'remove':
        break;
    }
  }

  handleGroupListCardEvent(event: { eventName: string; payload: MeAddListCardI }): void {
    switch (event.eventName) {
      case 'add':
        // TODO
        break;
      case 'remove':
        // TODO
        break;
    }
  }

  handleEmptyContentClick(): void {
    this.createInviteUser();
  }

  ngOnDestroy(): object {
    if (this._deleteUserSweetAlert) {
      this._deleteUserSweetAlert.unsubscribe();
    }
    if (this._removeUserSweetAlert) {
      this._removeUserSweetAlert.unsubscribe();
    }
    this.subs.unsubscribe();
    return super.ngOnDestroy();
  }
}
