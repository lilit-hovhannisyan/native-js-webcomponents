import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ElementRef, OnDestroy } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { ActivatedRoute } from '@angular/router';
import { RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { Language } from 'src/app/core/models/language';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { Observable } from 'rxjs';
import { BreakpointObserver } from '@angular/cdk/layout';
import { map, tap } from 'rxjs/operators';
import { VesselSpecificationRepositoryService, vesselTypes } from 'src/app/core/services/repositories/vessel-specification-repository.service';
import { VESSEL_TYPE } from 'src/app/core/models/enums/vessel-type';
import { IVesselSpecification, IVesselSpecificationCreate } from 'src/app/core/models/resources/IVesselSpecification';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { NotificationType } from 'src/app/core/models/INotification';
import { findClosestWithFromUntil } from 'src/app/core/helpers/general';
import { RegistrationFleetHelperService } from '../registrations-fleet-helper.service';

@Component({
  selector: 'nv-registration-fleet-technical',
  templateUrl: './registration-fleet-technical.component.html',
  styleUrls: ['./registration-fleet-technical.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegistrationFleetTechnicalComponent extends ResourceDetailView<IVesselSpecification, IVesselSpecificationCreate>
  implements OnInit, OnDestroy {
  protected repoParams: RepoParams<IVesselSpecification> = {};
  public form: FormGroup;
  protected resourceDefault: IVesselSpecification = {} as IVesselSpecification;
  public routeBackUrl: string = url(
    sitemap.chartering.children.registrations.children.registrationsFleet
  );
  private id: VesselKey;
  private language: Language = this.settingsService.language;
  public isHandset$: Observable<boolean> = this.breakpointObserver
    .observe('(max-width: 1024px)')
    .pipe(map(result => result.matches));
  public vesselTypes = vesselTypes;
  public specification: IVesselSpecification;
  public formElement: ElementRef;

  constructor(
    private fb: FormBuilder,
    private vesselSpecificationRepo: VesselSpecificationRepositoryService,
    baseComponentService: BaseComponentService,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private breakpointObserver: BreakpointObserver,
    private vesselMainService: VesselMainService,
    private notificationService: NotificationsService,
    private helperService: RegistrationFleetHelperService,
    private el: ElementRef
  ) {
    super(vesselSpecificationRepo, baseComponentService);
  }

  public getTitle: () => string = (): string =>
    this.wording.chartering.registration[this.language]

  public ngOnInit(): void {
    this.handleEditModeChange();
    this.id = +this.route.snapshot.parent.params.id;
    this.vesselSpecificationRepo
      .fetchAll({ queryParams: { vesselId: this.id } })
      .pipe(
        tap(vesselSpecifications => {
          if (!vesselSpecifications.length) {
            this.notificationService.notify(
              NotificationType.Warning,
              this.wording.chartering.noSpecification,
              this.wording.chartering.noSpecification,
            );
            return;
          }
          this.specification = findClosestWithFromUntil(vesselSpecifications);
          this.initializeResource(this.specification);
          this.cdr.markForCheck();
        })
      ).subscribe();
  }

  public handleEditModeChange(): void {
    this.inEditMode.subscribe(isEditMode => {
      this.helperService.registersSliderDisabledSubject.next(isEditMode);
    });
  }

  public init(fleetRegistration: IVesselSpecification = this.resourceDefault): void {
    this.initForm(this.fb.group({
      vesselType: [fleetRegistration.vesselType],
    }));
  }

  public submit() {
    if (!this.form.valid) {
      return;
    }

    const vesselType: VESSEL_TYPE = this.form.value.vesselType;

    this.updateResource({ ...this.resource, vesselType }).subscribe();
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

  public ngOnDestroy(): void {
    this.helperService.registersSliderDisabledSubject.next(false);
  }
}
