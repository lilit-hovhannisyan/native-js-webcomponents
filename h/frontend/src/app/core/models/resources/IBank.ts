import { IResource } from './IResource';
import { ICountryBase } from './ICountry';

export type BankKey = number;

interface ISharedProps {
  name: string;
  code: string;
  bic: string;
  zipCode: string;
  city: string;
  street: string;
  differentAccountHolder: string;
  countryId: number;
  isActive: boolean;
}

export interface IBankCreate extends ISharedProps { }

export interface IBankCreate extends ISharedProps { }

export interface IBankBase extends ISharedProps, IResource<number> { }

export interface IBankDTO extends IBankBase { }

export interface IBank extends IBankDTO { }








