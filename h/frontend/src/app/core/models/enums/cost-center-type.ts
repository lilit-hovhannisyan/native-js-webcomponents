import { ISelectOption } from '../misc/selectOption';
import { wording } from '../../constants/wording/wording';
import { IWording } from '../resources/IWording';

export enum COST_CENTER_TYPE {
  MAIN_COST_CENTER = 1,
  PRE_COST_CENTER = 2
}

export const getCostCenterTypesMap = (): Map<COST_CENTER_TYPE, IWording> => {
  return new Map<COST_CENTER_TYPE, IWording>([
    [
      COST_CENTER_TYPE.MAIN_COST_CENTER,
      wording.accounting.mainCostCenter
    ],
    [
      COST_CENTER_TYPE.PRE_COST_CENTER,
      wording.accounting.preCostCenter
    ]
  ]);
};
