import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { SubSink } from 'subsink';
import { TranslateService } from '@ngx-translate/core';

import { GlobalService } from '@core-services/global.service';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { InputAttributeService } from '@shared/services/input-attribute.service';
import { FileWebService } from '@shared/services/file.web-service';
import { BackOfficeService } from '../backoffice.web-service';

import { ArrayRequestI } from '@core-interfaces/array-request.interface';
import { EntityI } from '@core-interfaces/entity.interface';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { AttributeModelTypes } from '@shared/models/attribute/attribute.model';
import { DomainModel } from '@shared/models/domain/domain.model';
import { LayoutGroupModel } from '@shared/models/layout/layout-group.model';
import { UserModel } from '@shared/models/user/user.model';
import { FileUploadResponseModel } from '@shared/models/file-upload/file-upload-response.model';
import { RequiredConstraintState } from '@shared/models/attribute/attribute.constraint.model';
import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';
import { MeUploadImageI } from '@shared/me-components/me-upload-image/me-upload-image.interface';
import { MeButtonI } from '@shared/me-components/me-button/me-button.interface';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeAttributeLayoutTypes } from '@shared/me-attribute-layout-types';
import { bytesToSize } from '@shared/utils';
import { MARKETPLACE_BASE_API_URL, ME_DOMAIN_APPLICATION_USER } from '@shared/constants';

@Component({
  selector: 'me-user-create-edit',
  templateUrl: './user-create-edit.component.html',
  styleUrls: ['./user-create-edit.component.scss'],
  providers: [InputAttributeService, FileWebService]
})
export class UserCreateEditComponent implements OnInit, OnDestroy {
  private subs = new SubSink();
  private userId: string | null = '';
  private thumbnailAttributeName = 'avatar';
  private appliedState = RequiredConstraintState.draft;
  domain: DomainModel | undefined;
  groups: LayoutGroupModel[] = [];
  attributes: AttributeModelTypes[] = [];
  generalInfoLayoutGroups!: LayoutGroupModel[];
  layoutGroups = ['generalInformation'];
  uploadImageData!: MeUploadImageI;
  user!: UserModel;
  button!: { label: string; data: MeButtonI };

  userInfoAttributeForm!: FormGroup;
  userInfoAttributeItemMap!: Map<string, MeAttributeItem>;

  i18nStrings: { [key: string]: string } = {
    update: '',
    successfullyUpdated: ''
  };

  constructor(
    private globalService: GlobalService,
    private definitionsStoreService: DefinitionsStoreService,
    private inputAttributeService: InputAttributeService,
    private backOfficeService: BackOfficeService,
    private fileWebService: FileWebService,
    private meBasicAlertService: MeBasicAlertService,
    private translate: TranslateService,
    private _activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    let buttonLabel = '';
    this.userId = this._activeRoute.snapshot.paramMap.get('id');

    this.globalService.activateLoader();
    this.subs.sink = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        if (!this.userId) {
          buttonLabel = 'Create';
          this.initializeCreate();
        } else {
          buttonLabel = 'Edit';
          this.initializeEdit();
        }

        this.button = { data: { type: 'regular', color: 'primary' }, label: buttonLabel };
      }
    });

    this.subs.sink = this.translate.get(Object.keys(this.i18nStrings)).subscribe((translations) => {
      Object.keys(this.i18nStrings).forEach((key) => {
        this.i18nStrings[key] = translations[key];
      });
    });
  }

  initializeCreate(): void {
    this.setUploadImageData();
    this.domain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_USER);
    this.genereateFormByDomainAttributes();
    this.globalService.deactivateLoader();
  }

  initializeEdit(): void {
    this.subs.sink = this.backOfficeService.getBoUser(encodeURI(this.userId as string)).subscribe((response) => {
      this.user = response;
      this.setUploadImageData();
      this.domain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_USER);
      this.genereateFormByDomainAttributes();
    });

    this.globalService.deactivateLoader();
  }

  getLayoutForGroups(layoutGroupNames: string[]): LayoutGroupModel[] {
    const layoutGroups = this.groups.filter((group) => layoutGroupNames.findIndex((name) => group.internalName === name) > -1);
    return layoutGroups;
  }

  setUploadImageData(): void {
    if (this.definitionsStoreService.thumbnailProperties) {
      const splitString = this.definitionsStoreService.thumbnailProperties.supportedFiles.split(',');
      let thumbnail = '';

      if (this.user) {
        thumbnail = this.user.attributes.avatar ? `${MARKETPLACE_BASE_API_URL}/files/download/${this.user.attributes.avatar}` : '';
      }

      this.uploadImageData = {
        title: 'User Avatar',
        thumbnail,
        thumbnailProperties: this.definitionsStoreService.thumbnailProperties,
        uploadImageButton: { type: 'regular', color: 'primary' },
        isThumbnailStatusInvalid: false,
        acceptPhotoExtensions: `.${splitString.join(',.')}`,
        supportedThumbnailFiles: this.definitionsStoreService.thumbnailProperties.supportedFiles.replace(/,/g, ', '),
        maxUploadSize: bytesToSize(this.definitionsStoreService.thumbnailProperties.maxSize)
      };
    }
  }

  genereateFormByDomainAttributes(): void {
    if (this.domain) {
      let layoutTag = MeAttributeLayoutTypes.CREATE;
      let formState = null;
      this.attributes = this.inputAttributeService.transformAttributesArray(this.domain.attributes);

      if (this.user) {
        this.inputAttributeService.applyUpdateFormValues(this.attributes, this.user);
        layoutTag = MeAttributeLayoutTypes.UPDATE;
        formState = this.user.attributes.avatar;
      }

      this.groups = this.inputAttributeService.getLayoutGroups(this.domain.layouts, layoutTag);
      this.groups = this.inputAttributeService.generateLayoutRows(this.groups);

      this.generalInfoLayoutGroups = this.getLayoutForGroups(this.layoutGroups);
      if (this.generalInfoLayoutGroups && this.generalInfoLayoutGroups.length) {
        this.userInfoAttributeForm = this.inputAttributeService.createFormGroup(
          this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups),
          this.appliedState
        );

        const isRequired: boolean = this.inputAttributeService.isAttributeRequired(
          this.attributes,
          this.thumbnailAttributeName,
          this.appliedState
        );
        const validators: ValidatorFn[] = [];

        if (isRequired) {
          validators.push(Validators.required);
        }

        this.userInfoAttributeForm.addControl(this.thumbnailAttributeName, new FormControl(formState, validators));
        this.userInfoAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
          this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups),
          this.userInfoAttributeForm,
          this.appliedState
        );
      }
    }
  }

  handleImageFormData(recievedFile: File): void {
    const formData = new FormData();

    formData.append(
      'meta',
      JSON.stringify({
        type: this.definitionsStoreService.thumbnailProperties?.type
      })
    );
    formData.append('file', recievedFile, recievedFile.name);

    this.subs.sink = this.fileWebService.uploadFile(formData, FileUploadResponseModel).subscribe(
      (response) => {
        this.uploadImageData = {
          thumbnail: `${MARKETPLACE_BASE_API_URL}/files/download/${response.oid}`,
          showProgress: false
        };
        if (response.oid) {
          this.updateThumbnail(response.oid);
        } else {
          this.updateThumbnail(null);
        }
      },
      (_error: ErrorResponseI) => {
        this.uploadImageData = { ...this.uploadImageData, isThumbnailStatusInvalid: true };
      }
    );
  }

  updateThumbnail(thumbnailOR: string | null): void {
    if (this.userInfoAttributeForm) {
      this.userInfoAttributeForm.controls[this.thumbnailAttributeName].setValue(thumbnailOR);
      this.userInfoAttributeForm.markAsDirty();
    }
  }

  onSubmit(form: FormGroup): void {
    const { valid, value } = form;

    if (!valid) {
      return;
    }

    const updatedUser: ArrayRequestI<EntityI> = {
      entities: [
        {
          domainName: ME_DOMAIN_APPLICATION_USER,
          attributes: {
            ...value
          }
        }
      ]
    };

    form.markAsUntouched();
    this.globalService.activateLoader();
    if (this.user) {
      this.updateUser(updatedUser);
    }
  }

  updateUser(updatedUser: ArrayRequestI<EntityI>) {
    this.subs.sink = this.backOfficeService.updateBoUser(this.user.oid, updatedUser).subscribe(() => {
      this.globalService.deactivateLoader();
      this.meBasicAlertService.openBasicAlert({
        mode: 'success',
        title: this.i18nStrings.update,
        content: this.i18nStrings.successfullyUpdated
      });
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
