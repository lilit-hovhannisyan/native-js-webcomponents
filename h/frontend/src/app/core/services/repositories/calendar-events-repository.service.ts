import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { CalendarEventKey, ICalendarEvent, ICalendarEventCreate, ICalendarEventDTO } from '../../models/resources/ICalendarEvent';

@Injectable({
  providedIn: 'root'
})
export class CalendarEventsRepositoryService extends AbstractRepository<
  ICalendarEvent,
  ICalendarEventDTO,
  ICalendarEventCreate,
  RepoParams<ICalendarEvent>,
  CalendarEventKey
> {
  protected resourceType = 'CalendarEvent';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/calendar-events';
  }

  protected postProcess(item: ICalendarEventDTO): ICalendarEvent { return item; }

}
