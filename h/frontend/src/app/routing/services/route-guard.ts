import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { AuthenticationService } from '../../authentication/services/authentication.service';
import { Observable } from 'rxjs';

@Injectable()
export class RouteGuard implements CanLoad, CanActivate {

  constructor(
    private authService: AuthenticationService,
  ) {
  }
  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.authService.isLoggedIn()) {
      return true;
    }
    this.authService.logout({redirectUrl: state.url});
    return false;
  }


  /**
   * No module can be loaded until the user has been authenticated.
   * The auth section is already loaded, so this won't be a problem.
   */
  public canLoad(route: Route): boolean {
    return this.authService.isLoggedIn();
  }

  public canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.canActivate(route, state);
  }
}
