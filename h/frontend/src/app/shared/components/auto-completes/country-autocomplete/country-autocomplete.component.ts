import { Component, Input, OnInit } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup } from '@angular/forms';
import { CountryRepositoryService } from 'src/app/core/services/repositories/country-repository.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/countries.config';
import { IWording } from 'src/app/core/models/resources/IWording';
@Component({
  selector: 'nv-country-autocomplete',
  templateUrl: './country-autocomplete.component.html',
  styleUrls: ['./country-autocomplete.component.scss']
})
export class CountryAutocompleteComponent implements OnInit {
  public wording = wording;
  public gridConfig = getGridConfig({}, () => false);

  @Input() public form: FormGroup;
  @Input() public controlName = 'countryId';
  @Input() public filters: object;
  @Input() public label: IWording = wording.accounting.country;
  @Input() public withFlag: boolean;

  constructor(public countryRepo: CountryRepositoryService) { }

  public ngOnInit() {
    this.gridConfig.columns = this.gridConfig.columns.filter(c => c.key !== 'countryFlagPath');
    this.countryRepo.fetchAll({}).subscribe();
  }
}
