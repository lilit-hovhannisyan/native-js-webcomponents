import { MeContextMenuI } from '../me-context-menu/me-context-menu-inteface';

export interface MeDocumentCardI {
  selectionEnabled: boolean;
  selected: boolean;

  oid: string;
  documentName: string;
  creationDate: string;
  documentThumbnail?: string;
  primaryContent?: string;
  documentExtensionIcon?: string;

  contextMenu: MeContextMenuI;
}
