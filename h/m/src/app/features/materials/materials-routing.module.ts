import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BasePermissionGuard } from '@core-guards/base-permission.guard';
import { MePermissionTypes } from '@shared/me-permission-types';

import { MaterialsComponent } from './materials.component';

const routes: Routes = [
  {
    path: '',
    component: MaterialsComponent
  },
  {
    path: 'view/:id',
    data: {
      breadcrumb: {
        alias: 'materialName'
      },
      permission: MePermissionTypes.MATERIAL_VIEW
    },
    loadChildren: () => import('@features/material-view/material-view.module').then((m) => m.MaterialViewModule),
    canActivate: [BasePermissionGuard]
  },
  {
    path: 'create',
    data: {
      // This should be tested will it work when change language...
      breadcrumb: 'Create Material',
      permission: MePermissionTypes.MATERIAL_CREATE
    },
    loadChildren: () => import('@features/material-create-edit/material-create-edit.module').then((m) => m.MaterialCreateEditModule),
    canActivate: [BasePermissionGuard]
  },
  {
    path: 'edit/:id',
    data: {
      // This should be tested will it work when change language...
      breadcrumb: 'Edit Material',
      permission: MePermissionTypes.MATERIAL_MODIFY
    },
    loadChildren: () => import('@features/material-create-edit/material-create-edit.module').then((m) => m.MaterialCreateEditModule),
    canActivate: [BasePermissionGuard]
  },
  {
    path: 'duplicate/:id',
    data: {
      breadcrumb: 'Duplicate Material',
      permission: MePermissionTypes.MATERIAL_CREATE
    },
    loadChildren: () => import('@features/material-create-edit/material-create-edit.module').then((m) => m.MaterialCreateEditModule),
    canActivate: [BasePermissionGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaterialsRoutingModule {}
