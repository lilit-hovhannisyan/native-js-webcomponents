import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { ISilentPartnerRepresentative, ISilentPartnerRepresentativeDTO, ISilentPartnerRepresentativeCreate, SilentPartnerRepresentativeKey } from '../../models/resources/ISilentPartnerRepresentative';
import { CompanyKey } from '../../models/resources/ICompany';

export interface ISilentPartnerRepresentativeParams extends RepoParams<ISilentPartnerRepresentative> {
  queryParams?: {
    cid?: CompanyKey;
  };
}

@Injectable({
  providedIn: 'root'
})
export class SilentPartnerRepositoryService extends AbstractRepository<
  ISilentPartnerRepresentative,
  ISilentPartnerRepresentativeDTO,
  ISilentPartnerRepresentativeCreate,
  ISilentPartnerRepresentativeParams,
  SilentPartnerRepresentativeKey
>  {
  protected resourceType = 'SilentPartnerRepresentative';
  public sortAsce = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: ISilentPartnerRepresentativeDTO): ISilentPartnerRepresentative => item;

  public getBaseUrl = () => '/api/silent-partner-representatives';
}
