import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnDestroy, Output } from '@angular/core';

import { BaseLoggerComponent } from '@features/tracking-system';

import { MeDocumentCardI } from './me-document-card.interface';

@Component({
  selector: 'me-document-card',
  templateUrl: './me-document-card.component.html',
  styleUrls: ['./me-document-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeDocumentCardComponent extends BaseLoggerComponent implements OnDestroy {
  componentVersion: string = '0.0.1';

  @Input() dataModel!: MeDocumentCardI;
  @Output() eventOccurs: EventEmitter<{ eventName: string; payload?: string }> = new EventEmitter();

  constructor(injector: Injector) {
    super(injector);
  }

  customEvent(eventName: string, payload?: string): void {
    this.eventOccurs.emit({ eventName, payload });
  }

  ngOnDestroy(): object {
    return super.ngOnDestroy();
  }
}
