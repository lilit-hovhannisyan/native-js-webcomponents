import { NvColumnDataType, NvGridButtonsPosition, NvGridConfig, NvRowHeight, NvFilterControl } from 'nv-grid';
import { FormGroup, AbstractControl, ValidationErrors } from '@angular/forms';
import { DEBITCREDIT } from 'src/app/core/constants/regexp';
import { Validators } from 'src/app/core/classes/validators';
import * as moment from 'moment';
import { wording } from 'src/app/core/constants/wording/wording';
import { TemplateRef } from '@angular/core';
import { IBookingLine, IBookingLineRow } from '../../models/resources/IBookingLine';
import { BehaviorSubject } from 'rxjs';
import { mandatorNoModifier } from '../../models/resources/IMandator';
import { accountNoModifier } from '../../models/resources/IBookingAccount';
import { NvAction } from 'nv-grid/lib/components/nv-button/nv-button.component';

export interface IBookingGridConfigActions {
  updateDefaultMandatorNo: (form: FormGroup) => any;
  setMandatorId: (form: FormGroup) => any;
  onMandatorChange: (form: FormGroup) => any;
  onPlusMinusChange: (form: FormGroup) => any;
  setBookingAccount: (form: FormGroup) => any;
  onBookingAccountChange: (form: FormGroup) => any;
  onAmountLocalChange: (form: FormGroup) => any;
  calculatePlusMinusSignAndAmountLocal: (form: FormGroup) => any;
  onAmountTransactionChange: (form: FormGroup) => any;
  onRateChange: (form: FormGroup) => any;
  setBookingCodeId: (form: FormGroup) => any;
  setTaxKeyId: (form: FormGroup) => any;
  setDefaultCurrencyValue: (form: FormGroup) => any;
  setCurrencyId: (form: FormGroup) => any;
  onCurrencyChange: (form: FormGroup) => any;
  onTaxKeyChange: (form: FormGroup) => any;
  isBookingSaveButtonDisabled: () => boolean;
  saveBooking: () => NvAction;
  onDebitCreditTypeChange: (form: FormGroup) => any;
  onDiscountChange: (form: FormGroup) => any;
  updateDiffer: () => any;
  showInAccountSheet: (bookingLine: IBookingLine) => void;
  bookingFormSubject: BehaviorSubject<FormGroup>;
  setCostTypeId: (form: FormGroup) => void;
  setCostCenterId: (form: FormGroup) => void;
  setCostUnitId: (form: FormGroup) => void;
  takeInvoice: () => void;
  predefineTextFromAccountChange: (form: FormGroup) => void;
  onTextChange: (form: FormGroup) => void;
}

export const getBookinglineGridConfig = (
  actions: IBookingGridConfigActions,
  validators: {
    validatePaymentDate: (control: AbstractControl) => ValidationErrors | null,
    paymentDateOfDiscount: (control: AbstractControl) => ValidationErrors | null,
    validateMandatorAccountingType: (control: AbstractControl) => ValidationErrors | null,
    doesMandatorExist: (control: AbstractControl) => ValidationErrors | null,
    isMandatorActive: (control: AbstractControl) => ValidationErrors | null,
    doesBookingAccountExist: (control: AbstractControl) => ValidationErrors | null,
    isBookingAccountActive: (control: AbstractControl) => ValidationErrors | null,
    doesBookingCodeExist: (control: AbstractControl) => ValidationErrors | null,
    isBookingCodeActive: (control: AbstractControl) => ValidationErrors | null,
    doesCurrencyExist: (control: AbstractControl) => ValidationErrors | null,
    isCurrencyActive: (control: AbstractControl) => ValidationErrors | null,
    isYearClosedForMandator: (control: AbstractControl) => ValidationErrors | null,
    isPeriodClosedForMandator: (control: AbstractControl) => ValidationErrors | null,
    validateFirstBookingYear: (control: AbstractControl) => ValidationErrors | null,
    isPeriodBlockedForMandator: (control: AbstractControl) => ValidationErrors | null,
    validateAccountNoAccountType: (control: AbstractControl) => ValidationErrors | null,
    isBookingAccountRelatedToMandator: (control: AbstractControl) => ValidationErrors | null,
    isBookingCodeRelatedToMandator: (control: AbstractControl) => ValidationErrors | null,
    isCostTypeRelatedToBookingAccount: (control: AbstractControl) => ValidationErrors | null,
    doesCostTypeExist: (control: AbstractControl) => ValidationErrors | null,
    doesCostCenterExist: (control: AbstractControl) => ValidationErrors | null,
    isCostCenterActive: (control: AbstractControl) => ValidationErrors | null,
    doesCostUnitExist: (control: AbstractControl) => ValidationErrors | null,
  },
  template: {
    accountNoIsNotRelatedTemplate: TemplateRef<any>,
    bookingCodeIsNotRelatedTemplate: TemplateRef<any>,
    discountDiffersWithPaymentDateTemplate: TemplateRef<any>,
  },
  customFormat: {
    debitCredit: (debitCreit: number) => string
    discount: (row: IBookingLine) => string

  }
): NvGridConfig => ({
  gridName: 'createBookingLinesGrid',
  hideAllFilters: true,
  showRowIndex: true,
  showPaging: false,
  showFooter: true,
  disableDragAndDrop: true,
  disableHideColumns: true,
  disableSortColumns: true,
  pinFirstRow: true,
  hideRefreshButton: true,
  rowHeight: NvRowHeight.midSize,
  editForm: {
    createFirstFormIfRowExist: true,
    disableFirstCell: true,
    allowCreateNewRow: true,
    enableLocalStorageService: false,
    showCreateNewRowButton: false,
    preventDiscardingFirstRow: true,
    showDiscardChangesButton: true,
    markBackgroundAsEdited: false,
    showSaveChangesButton: false,
    allowJumpToNextCellIfInvalid: true,
    allowJumpToNextRowIfInvalid: false
  },
  rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
  columns: [
    {
      key: 'id',
      visible: false
    },
    {
      key: 'mandatorId',
      visible: false,
      editControl: {
        onValueChange: (form: FormGroup) => {
          actions.onMandatorChange(form);
        }
      }
    },
    {
      key: 'mandatorNo',
      title: wording.accounting.mandatorNo,
      width: 80,
      isSortable: true,
      editControl: {
        editable: true,
        validation: {
          validators: [
            Validators.required,
            validators.doesMandatorExist,
            validators.isMandatorActive,
            validators.validateMandatorAccountingType,
            validators.validateFirstBookingYear,
            validators.isYearClosedForMandator,
            validators.isPeriodClosedForMandator,
            validators.isPeriodBlockedForMandator
          ]
        },
        onValueChange: (form: FormGroup) => {
          actions.setMandatorId(form);
          actions.updateDefaultMandatorNo(form);
        }
      },
      customFormatFn: (bookingLineRow: IBookingLineRow) => mandatorNoModifier(bookingLineRow.mandatorNo)
    },
    {
      key: 'bookingAccountId',
      visible: false,
      editControl: {
        onValueChange: (form: FormGroup) => {
          actions.onBookingAccountChange(form);
          actions.updateDiffer();
        }
      }
    },
    {
      key: 'accountNo',
      title: wording.accounting.accountNo,
      width: 150,
      isSortable: true,
      editControl: {
        editable: true,
        validation: {
          validators: [
            Validators.required,
            validators.doesBookingAccountExist,
            validators.isBookingAccountActive,
            validators.validateAccountNoAccountType,
            validators.isBookingAccountRelatedToMandator
          ]
        },
        errorPopoverContentTemplate: (form: FormGroup) => {
          return form.get('accountNo').errors
            && Object.keys(form.get('accountNo').errors)[0] === 'bookingAccountNotRelatedToMandator'
            ? template.accountNoIsNotRelatedTemplate
            : null;
        },
        onValueChange: (form: FormGroup) => {
          actions.setBookingAccount(form);
          if (form.get('accountNo').valid) {
            actions.predefineTextFromAccountChange(form);
          }
        }
      },
      customTooltip: (row: IBookingLineRow) => row.accountName,
      customFormatFn: (row: IBookingLineRow) => accountNoModifier(row.accountNo)
    },
    {
      key: 'accountName',
      title: wording.accounting.accountName,
      visible: false,
    },
    {
      key: 'bookingCodeId',
      visible: false,
    },
    {
      key: 'bookingCodeNo',
      title: wording.accounting.bookingCode,
      width: 90,
      isSortable: true,
      editControl: {
        editable: true,
        validation: {
          validators: [
            Validators.required,
            validators.doesBookingCodeExist,
            validators.isBookingCodeActive,
            validators.isBookingCodeRelatedToMandator
          ]
        },
        errorPopoverContentTemplate: (form: FormGroup) => {
          return form.get('bookingCodeNo').errors
            && Object.keys(form.get('bookingCodeNo').errors)[0] === 'bookingCodeNotRelatedToMandator'
            ? template.bookingCodeIsNotRelatedTemplate
            : null;
        },
        onValueChange: (form: FormGroup) => actions.setBookingCodeId(form)
      }
    },
    {
      key: 'text',
      title: wording.accounting.bookingText,
      width: 150,
      isSortable: true,
      editControl: {
        editable: true,
        validation: {
          // https://navido.atlassian.net/browse/NAVIDO-603
          validators: [Validators.required, Validators.minLength(3), Validators.maxLength(1500)]
        },
        onValueChange: (form: FormGroup) => {
          if (form.get('accountNo').valid && form.get('text').valid) {
            actions.onTextChange(form);
          }
        }
      },
      footer: {
        label: wording.accounting.amountBalance
      }
    },
    {
      key: 'currencyId',
      visible: false,
      editControl: {
        onValueChange: (form: FormGroup) => {
          actions.onCurrencyChange(form);
          actions.updateDiffer();
        }
      }
    },
    {
      key: 'currencyIsoCode',
      title: wording.accounting.currency,
      width: 74,
      isSortable: true,
      editControl: {
        editable: true,
        validation: {
          validators: [
            Validators.required,
            validators.doesCurrencyExist,
            validators.isCurrencyActive
          ],
        },
        onValueChange: (form: FormGroup) => {
          actions.setCurrencyId(form);
          actions.setDefaultCurrencyValue(form);
        }
      },
    },
    {
      key: 'debitCreditType',
      visible: false,
      editControl: {
        editable: true,
        onValueChange: (form: FormGroup) => actions.onDebitCreditTypeChange(form)
      }
    },
    {
      key: '+/-',
      title: wording.accounting.DC,
      width: 50,
      isSortable: true,
      editControl: {
        editable: true,
        validation: {
          validators: [Validators.required, Validators.maxLength(1), Validators.pattern(DEBITCREDIT)],
        },
        onValueChange: (form: FormGroup) => {
          actions.onPlusMinusChange(form);
          actions.updateDiffer();
        }
      },
      customFormatFn: (row: IBookingLine) => {
        return customFormat.debitCredit(row.debitCreditType);
      },
      footer: {
        operation: (bookingLines: IBookingLine[]) => {
          const rows = bookingLines.map(bookingLine => bookingLine.debitCreditType * bookingLine.amountTransaction);
          const amountTransactionSum = rows.reduce((prev, cur) => +prev + +cur, 0);

          return customFormat.debitCredit(
            amountTransactionSum
              ? -1
              : amountTransactionSum < 0
                ? 1
                : null
          );
        },
        disableCustomFormat: true
      }
    },
    {
      key: 'amountTransaction',
      title: wording.accounting.amountTransaction,
      dataType: NvColumnDataType.Decimal,
      width: 94,
      isSortable: true,
      editControl: {
        defaultValue: 0,
        editable: true,
        validation: {
          validators: [
            Validators.required,
            Validators.min(0)
          ]
        },
        onValueChange: (form: FormGroup) => {
          actions.onAmountTransactionChange(form);
        }
      },
      footer: {
        operation: (bookingLines: IBookingLine[]) => {
          const currencyIds = bookingLines.map(bookingLine => bookingLine.currencyId);
          const rows = bookingLines.map(bookingLine => bookingLine.debitCreditType * bookingLine.amountTransaction);
          return currencyIds.every(curencyId => curencyId === currencyIds[0])
            ? Math.abs(rows.reduce((prev, cur) => +prev + +cur, 0))
            : null;
        }
      }
    },
    {
      key: 'exchangeRate',
      title: wording.accounting.rate,
      width: 70,
      isSortable: true,
      dataType: NvColumnDataType.Decimal,
      decimalDigitsInfo: '1.5-5',
      editControl: {
        defaultValue: 1,
        editable: true,
        validation: {
          validators: [
            Validators.required,
            Validators.positiveNumber
          ]
        },
        onValueChange: (form: FormGroup) => {
          actions.onRateChange(form);
        }
      }
    },
    {
      key: 'amountLocal',
      title: wording.accounting.amountLocal,
      width: 94,
      isSortable: true,
      dataType: NvColumnDataType.Decimal,
      editControl: {
        defaultValue: 0,
        editable: true,
        validation: {
          validators: [
            Validators.required,
            Validators.min(0)
          ]
        },
        onValueChange: (form: FormGroup) => {
          actions.onAmountLocalChange(form);
        }
      },
      footer: {
        operation: (bookingLines: IBookingLine[]) => {
          const rows = bookingLines.map(bookingLine => bookingLine.debitCreditType * bookingLine.amountLocal);
          actions.updateDiffer();
          return Math.abs(rows.reduce((prev, cur) => +prev + +cur, 0));
        }
      }
    },
    {
      key: 'invoiceNo',
      title: wording.accounting.invoiceNo,
      width: 100,
      isSortable: true,
      editControl: {
        editable: true,
        disabled: true,
        validation: {
          validators: [
            Validators.maxLength(20)
          ]
        },
      },
    },
    {
      key: 'discount',
      title: wording.accounting.discount,
      width: 125,
      isSortable: true,
      dataType: NvColumnDataType.Decimal,
      editControl: {
        editable: true,
        disabled: true,
        validation: {
          validators: [
            Validators.required,
            Validators.min(0)
          ],
        },
        onValueChange: (form: FormGroup) => {
          actions.onDiscountChange(form);
        }
      },
      customFormatFn: (row: IBookingLine) => customFormat.discount(row)
    },
    {
      key: 'paymentDate',
      title: wording.accounting.paymentDate,
      width: 110,
      isSortable: true,
      editControl: {
        editable: true,
        defaultValue: moment.utc(new Date()).toISOString(),
        validation: {
          validators: [
            Validators.required,
            Validators.isDateInputInvalid,
            validators.validatePaymentDate,
            validators.paymentDateOfDiscount,
          ]
        },
        errorPopoverContentTemplate: (form: FormGroup) => {
          const validationErrors = form.get('paymentDate').errors;
          return validationErrors
            && Object.keys(validationErrors)[0] === 'shouldUpdateDiscount'
            ? template.discountDiffersWithPaymentDateTemplate
            : null;
        }
      },
      dataType: NvColumnDataType.Date
    },
    {
      key: 'taxKeyId',
      visible: false,
      editControl: {
        onValueChange: (form: FormGroup) => {
          actions.onTaxKeyChange(form);
        }
      }
    },
    {
      key: 'tax',
      title: wording.accounting.tax,
      editControl: {
        editable: true,
        onValueChange: (form: FormGroup) => actions.setTaxKeyId(form)
      }
    },
    {
      key: 'costTypeName',
      title: wording.accounting.costType,
      visible: false,
    },
    {
      key: 'costTypeId',
      visible: false,
    },
    {
      key: 'costTypeNo',
      title: wording.accounting.ct,
      width: 70,
      isSortable: true,
      editControl: {
        disabled: true,
        editable: true,
        validation: {
          validators: [
            Validators.required,
            validators.isCostTypeRelatedToBookingAccount,
            validators.doesCostTypeExist
          ]
        },
        onValueChange: (form: FormGroup) => actions.setCostTypeId(form),
      },
      customTooltip: (row: IBookingLineRow) => row.costTypeName,
    },
    {
      key: 'costCenterName',
      title: wording.accounting.costCenterName,
      visible: false,
    },
    {
      key: 'costCenterId',
      visible: false,
    },
    {
      key: 'costCenterNo',
      title: wording.accounting.cc,
      width: 70,
      isSortable: true,
      editControl: {
        disabled: true,
        editable: true,
        validation: {
          validators: [
            Validators.required,
            validators.doesCostTypeExist
          ]
        },
        onValueChange: (form: FormGroup) => actions.setCostCenterId(form),
      },
      customTooltip: (row: IBookingLineRow) => row.costCenterName,
    },
    {
      key: 'costUnitName',
      title: wording.accounting.costUnitName,
      visible: false,
    },
    {
      key: 'costUnitId',
      visible: false,
    },
    {
      key: 'costUnitNo',
      title: wording.accounting.cu,
      width: 70,
      isSortable: true,
      editControl: {
        disabled: true,
        editable: true,
        validation: {
          validators: [
            validators.doesCostUnitExist
          ]
        },
        onValueChange: (form: FormGroup) => actions.setCostUnitId(form),
      },
      customTooltip: (row: IBookingLineRow) => row.costUnitName,
    },
    {
      key: 'voyage',
      title: wording.accounting.voyage,
      width: 60,
      isSortable: true,
      editControl: {
        editable: true
      }
    },
    {
      key: 'port',
      title: wording.accounting.port,
      width: 50,
      isSortable: true,
      editControl: {
        editable: true
      }
    },
    {
      key: 'loc',
      title: 'Loc',
      width: 50,
      isSortable: true,
      editControl: {
        editable: true
      },
      customFormatFn: (form: FormGroup) => {
        actions.setDefaultCurrencyValue(form);
        actions.calculatePlusMinusSignAndAmountLocal(null);
      }
    },
    {
      key: 'accrualAccounting',
      title: 'A',
      width: 30,
      visible: false,
      isSortable: false,
      editControl: {
        defaultValue: false,
        editable: true,
        disabled: false
      },
      dataType: NvColumnDataType.Boolean
    }
  ],
  toolbarButtons: [
    {
      tooltip: wording.accounting.takeInvoice,
      icon: 'book',
      title: wording.accounting.takeInvoice,
      func: () => actions.takeInvoice()
    },
    {
      tooltip: wording.general.save,
      icon: 'save',
      title: wording.general.save,
      disabled: actions.isBookingSaveButtonDisabled,
      func: actions.saveBooking()
    },
  ],
  contextMenuButtons: [
    {
      icon: 'eye',
      description: wording.accounting.showAccountSheet,
      func: (bookingLine: IBookingLine) => actions.showInAccountSheet(bookingLine),
      hidden: (bookingLine: IBookingLine) => {
        const yearControl: AbstractControl = actions.bookingFormSubject.value.get('year');
        const year = +yearControl.value;
        const isHidden: boolean = !(
          bookingLine.mandatorId
          && bookingLine.bookingAccountId
          && (yearControl.valid || yearControl.disabled)
          && !isNaN(year)
          && year
        );
        return isHidden;
      }
    },
  ],
});
