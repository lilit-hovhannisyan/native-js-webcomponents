import { Pipe, PipeTransform } from '@angular/core';
import { invalidPipeArgumentError } from 'src/app/shared/pipes/invalid-pipe-argument-error';
import { IWording } from '../../core/models/resources/IWording';
import { SettingsService } from 'src/app/core/services/settings.service';

/**
 * This pipe turns wording-objects to strings, of the currently selected language.
 */
@Pipe({
  name: 'translate',
  pure: false
})
export class TranslatePipe implements PipeTransform {

  constructor(private settingsService: SettingsService) {
  }

  /**
   * When `safeTranslation` is `true` and system selected language
   * translation doesn't exist, then returns opposite translation.
   */
  public transform(wording: IWording, safeTranslation?: boolean): string | null {
    if (!wording) { return null; }

    if (typeof wording !== 'object'
      || typeof wording.en !== 'string'
      ||  typeof wording.de !== 'string'
    ) {
      throw invalidPipeArgumentError(TranslatePipe, wording);
    }

    const translation: string = wording[this.settingsService.language];

    return safeTranslation
      ? translation || wording.en || wording.de
      : translation;
  }
}
