import { IWidgetListItem } from 'src/app/core/models/widgets/IWidgetList';
import { WidgetType } from 'src/app/core/models/widgets/WidgetType';

export const widgetsList: IWidgetListItem[] = [
  { icon: 'cloud', name: 'Weather', type: WidgetType.Weather },
  { icon: 'clock-circle-o', name: 'Clock', type: WidgetType.Clock },
  { icon: 'book', name: 'Bookmark', type: WidgetType.Bookmark },
  { icon: 'unordered-list', name: 'Todo', type: WidgetType.Todo },
  { icon: 'dollar', name: 'Currency', type: WidgetType.Currency },
];
