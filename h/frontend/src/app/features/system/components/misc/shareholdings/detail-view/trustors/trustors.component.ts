import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { CompanyKey } from '../../../../../../../core/models/resources/ICompany';
import { ActivatedRoute } from '@angular/router';
import { ShareholdingsMainService } from '../../../../../../../core/services/mainServices/shareholdings-main.service';
import { COMPANY_CONTRIBUTION_TYPES } from '../../../../../shared/trustee-partner-contributions/enums/company-contributions';

@Component({
  templateUrl: './trustors.component.html',
  styleUrls: ['./trustors.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareholdingsTrustorsComponent implements OnInit {
  public contributorType: COMPANY_CONTRIBUTION_TYPES = COMPANY_CONTRIBUTION_TYPES.TrustorContribution;
  public companyId: CompanyKey;

  constructor(
    private activatedRoute: ActivatedRoute,
    private shareholdingsMainService: ShareholdingsMainService,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    const shareholdingId: CompanyKey = +this.activatedRoute.parent.snapshot.params.id;

    this.shareholdingsMainService.getShareholdingById(shareholdingId).subscribe(shareholding => {
      if (shareholding) {
        this.companyId = +shareholding.id;
        this.cdr.markForCheck();
      }
    });
  }
}
