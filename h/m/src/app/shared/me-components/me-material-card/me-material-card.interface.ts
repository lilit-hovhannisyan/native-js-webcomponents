import { KeyValue } from '@angular/common';
import { MeButtonI } from '../me-button/me-button.interface';

export interface MeMaterialCardI {
  contextMenuButton?: MeButtonI;
  contextMenuItems: KeyValue<string, string>[];
  materialName: string;
  materialPrice: string;
  materialUOM?: string;
  oid: string;
  selected: boolean;
  selectionEnabled?: boolean;
  supplierName?: string;
  supplierOid: string | undefined;
  thumbnail: string;
  visibility?: MeVisibilityI;
  state?: string;
}

export interface MeVisibilityI {
  label: string;
  fontColorHex: string;
  backgroundColorHex: string;
}
