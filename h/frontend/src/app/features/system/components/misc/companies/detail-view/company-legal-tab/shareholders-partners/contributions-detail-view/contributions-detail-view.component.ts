import { Component, OnInit, Input, EventEmitter, Output, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NvGridConfig } from 'nv-grid';
import { ContributionMainService } from 'src/app/core/services/mainServices/company-contributions-main.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { CompanyKey, ICompany } from 'src/app/core/models/resources/ICompany';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { ContributionModalComponent } from '../contribution-modal/contribution-modal.component';
import { ICompanyPartnerContributionFull, ICompanyPartnerContribution, AmountChangeType } from 'src/app/core/models/resources/ICompanyPartnerContribution';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { getContributionsGridConfig } from './contributions-grid.config';
import { SettingsService } from 'src/app/core/services/settings.service';
import { CompanyLegalEntityRepositoryService } from 'src/app/core/services/repositories/company-legal-entity-repository.service';
import { ICurrency, CurrencyKey } from 'src/app/core/models/resources/ICurrency';
import { Observable, BehaviorSubject, forkJoin, combineLatest } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { ICompanyLegalEntity } from 'src/app/core/models/resources/ICompanyLegalEntity';
import { ContributionEditModalComponent } from '../contribution-edit-modal/contribution-edit-modal.component';
import { contributionModalTitles, contributionsFieldsetTitle } from 'src/app/core/models/enums/company-legal-form';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { CompanyContributionsRepositoryService } from 'src/app/core/services/repositories/company-partner-contribution-repository.service';
import { IWording } from 'src/app/core/models/resources/IWording';
import { CompaniesHelperService } from '../../../companies.helper.service';
import { CompanyRepositoryService } from '../../../../../../../../../core/services/repositories/company-repository.service';
import { sortByKey } from 'src/app/core/helpers/general';
import { orderBy } from 'lodash';
@Component({
  selector: 'nv-contributions-detail-view',
  templateUrl: './contributions-detail-view.component.html',
  styleUrls: ['./contributions-detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContributionsDetailViewComponent implements OnInit {
  public wording = wording;
  public fieldsetTitleWording: IWording;
  public form: FormGroup;
  public resultGridConfig: NvGridConfig;
  public historyGridConfig: NvGridConfig;
  private company: ICompany;
  private legalEntity: ICompanyLegalEntity;
  private currency: ICurrency;
  private currencyEuro: ICurrency;
  private selectedDateSubject: BehaviorSubject<string> = new BehaviorSubject('');
  private contributionsSubject: BehaviorSubject<ICompanyPartnerContributionFull[]>
    = new BehaviorSubject<ICompanyPartnerContributionFull[]>([]);

  public latestContributionsMap: Map<CompanyKey, ICompanyPartnerContributionFull>;
  public filteredContributions: ICompanyPartnerContributionFull[];
  public resultData: ICompanyPartnerContributionFull[];
  public historyData: ICompanyPartnerContributionFull[];

  @Input() public companyId: CompanyKey;

  @Input() public set contributions(value: ICompanyPartnerContributionFull[]) {
    this.contributionsSubject.next(value);
  }
  public get contributions(): ICompanyPartnerContributionFull[] {
    return this.contributionsSubject.value;
  }

  @Input()
  public set selectedDate(value: string) {
    this.selectedDateSubject.next(value);
  }
  public get selectedDate(): string {
    return this.selectedDateSubject.getValue();
  }

  @Output() public add: EventEmitter<ICompanyPartnerContribution> = new EventEmitter<ICompanyPartnerContribution>();
  @Output() public delete: EventEmitter<ICompanyPartnerContribution> = new EventEmitter<ICompanyPartnerContribution>();
  @Output() public edit: EventEmitter<ICompanyPartnerContribution> = new EventEmitter<ICompanyPartnerContribution>();

  constructor(
    private modalService: NzModalService,
    private fb: FormBuilder,
    private contributionsMainService: ContributionMainService,
    private contributionsRepo: CompanyContributionsRepositoryService,
    private authService: AuthenticationService,
    private settingsService: SettingsService,
    private legalEntityRepo: CompanyLegalEntityRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private confirmationService: ConfirmationService,
    private changeDetectionRef: ChangeDetectorRef,
    private companiesHelperService: CompaniesHelperService,
    private companyRepository: CompanyRepositoryService,
  ) { }

  public ngOnInit(): void {
    forkJoin([
      this.companyRepository.fetchOne(this.companyId, {}),
      this.fetchCurrency(),
      this.selectedDateSubject.pipe(take(1)),
    ]).subscribe(([company]) => {
      this.company = company;

      this.fieldsetTitleWording = contributionsFieldsetTitle.get(this.company.legalForm);
      this.init();
    });
  }

  private fetchCurrency(): Observable<void> {
    return forkJoin([
      this.legalEntityRepo.fetchOne(this.companyId, {}),
      this.currencyRepo.fetchAll({}),
    ]).pipe(
      map(([legalEntity, currencies]: [ICompanyLegalEntity, ICurrency[]]) => {
        this.legalEntity = legalEntity;
        const currencyId: CurrencyKey = legalEntity?.currencyId;

        currencies.some(c => {
          if (!c.isActive) {
            return;
          }

          if (c.isoCode === 'EUR') {
            this.currencyEuro = c;
          }
          if (c.id === currencyId) {
            this.currency = c;
          }
          // break loop when both are found
          return this.currency && this.currencyEuro;
        });

        // if currency of legalEntity has not set, use EURO
        if (!currencyId) {
          this.currency = this.currencyEuro;
        }
      })
    );
  }

  private init(): void {
    this.form = this.fb.group({
      date: [{ value: this.selectedDate, disabled: true }],
      showHistoricalData: [false],
    });

    this.changeDetectionRef.detectChanges();

    combineLatest([
      this.selectedDateSubject,
      this.contributionsSubject,
    ]).subscribe(() => {
      this.setGridsData();
      this.initGrids();
      this.form.controls.date.patchValue(this.selectedDate);
    });
  }

  private initGrids(): void {
    this.resultGridConfig = getContributionsGridConfig(
      this.settingsService,
      this.company,
      this.currency,
      this.currencyEuro,
      this.legalEntity,
      this.latestContributionsMap,
      this.contributionsMainService.getTotalContributionAmount(this.resultData),
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
    );

    this.historyGridConfig = getContributionsGridConfig(
      this.settingsService,
      this.company,
      this.currency,
      this.currencyEuro,
      this.legalEntity,
      this.latestContributionsMap,
      this.contributionsMainService.getTotalContributionAmount(this.resultData),
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
      {
        edit: this.openEditModal,
        delete: this.deleteContribution,
      },
    );
  }

  public openContributionsModal(): void {
    this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: contributionModalTitles.get(this.company.legalForm)[this.settingsService.language],
      nzContent: ContributionModalComponent,
      nzWidth: 800,
      nzClosable: true,
      nzComponentParams: {
        latestContributions: this.latestContributionsMap,
        contributions: this.contributions,
        companyId: this.companyId,
      },
    }).afterClose.subscribe((data) => {
      if (!data?.length) {
        return;
      }

      // uses getStream
      this.add.emit(data[0]);
    });
  }

  private setGridsData(): void {
    this.latestContributionsMap = this.contributionsMainService
      .getCompaniesLatestContributions(this.contributions);
    const filteredContributions: ICompanyPartnerContributionFull[] = this.contributionsMainService
      .filterContributions(this.contributions, this.selectedDate);
    const latestContributions: Map<CompanyKey, ICompanyPartnerContributionFull> = this.contributionsMainService
      .getCompaniesLatestContributions(filteredContributions);

    /**
     * Only show partners with percentage > 0
     * Or "Zero" contributions with Increase type
     */
    this.resultData = orderBy(Array.from(latestContributions.values())
      .filter(contribution => {
        return (
          !!contribution.contributionAmount
          || (
            contribution.contributionIsZero
            && contribution.amountChangeType === AmountChangeType.Increase
          )
        );
      }), [c => c.companyName]);

    this.historyData = this.contributionsMainService
      .filterContributions(this.contributions, this.selectedDate)
      .sort(sortByKey('id'));
  }

  private openEditModal = (entity: ICompanyPartnerContribution): void => {
    this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.general.edit[this.settingsService.language],
      nzContent: ContributionEditModalComponent,
      nzWidth: 600,
      nzClosable: true,
      nzComponentParams: {
        company: this.company,
        legalEntity: this.legalEntity,
        contribution: entity,
        currency: this.currency,
        currencyEuro: this.currencyEuro,
        partnersTotalContributionsMap: this.latestContributionsMap,
        allContributions: this.contributions,
      },
    }).afterClose.subscribe((data) => {
      if (!data) {
        return;
      }
      const freshData: ICompanyPartnerContributionFull = {
        ...entity,
        ...data,
      };
      this.contributionsRepo.update(freshData, {}).subscribe(updatedEntity => {
        // uses getStream
        this.edit.emit(updatedEntity);
      });
    });
  }
  private deleteContribution = (entity: ICompanyPartnerContribution): void => {
    this.confirmationService.confirm({
      cancelButtonWording: wording.general.cancel,
      okButtonWording: wording.general.delete,
      wording: wording.system.areYouSureYouWantToDeleteChange,
    }).subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      this.contributionsRepo.delete(entity.id, {}).subscribe(() => {
        // uses getStream
        this.delete.emit(entity);
      });
    });
  }
}
