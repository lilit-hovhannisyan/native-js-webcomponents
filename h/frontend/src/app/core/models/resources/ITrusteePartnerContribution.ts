import { IResource } from './IResource';
import { CompanyKey, ICompany } from './ICompany';
import { ICompanyPartnerContributionFull, AmountChangeType } from 'src/app/core/models/resources/ICompanyPartnerContribution';

export type TrusteePartnerContributionKey = number;

interface ISharedProps {
  from: string;
  contributionAmount: number;
  companyId: CompanyKey;
  partnerId: CompanyKey;
  shareholderId: CompanyKey;
  remark?: string;
  contributionIsZero?: boolean;
  amountChangeType?: AmountChangeType;
}

export interface ITrusteePartnerContributionBase extends ISharedProps, IResource<TrusteePartnerContributionKey> { }

export interface ITrusteePartnerContributionCreate extends ISharedProps { }

export interface ITrusteePartnerContributionDTO extends ITrusteePartnerContributionBase { }

export interface ITrusteePartnerContribution extends ITrusteePartnerContributionDTO { }

export interface ITrusteePartnerContributionFull extends ITrusteePartnerContribution, ICompany {
  trusteePartner: ICompanyPartnerContributionFull;
  trusteePartnerName: string;
}
