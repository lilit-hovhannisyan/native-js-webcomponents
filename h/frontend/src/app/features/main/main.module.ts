import { DECLARATIONS, MainRoutingModule } from './main-routing.module';
import { GridsterModule } from 'angular-gridster2';
import { FileUploaderService } from '../../shared/services/file-uploader.service';
import { WriteProtectionService } from '../../core/services/ui/write-protection.service';
import { SharedModule } from '../../shared/shared.module';
import { NgModule } from '@angular/core';
import { WidgetsContainerComponent } from './components/dashboard/widgets/widgets-container/widgets-container.component';
import { WeatherComponent } from './components/dashboard/widgets/weather/weather.component';
import { ClockComponent } from './components/dashboard/widgets/clock/clock.component';
import { BookmarkComponent } from './components/dashboard/widgets/bookmark/bookmark.component';
import { ClockDetailViewComponent } from './components/dashboard/widgets/clock/detail-view/detail-view.component';
import { TodoWidgetComponent } from './components/dashboard/widgets/todo/todo-widget.component';
import { CurrencyComponent } from './components/dashboard/widgets/currency/currency.component';

@NgModule({
  imports: [
    MainRoutingModule,
    SharedModule,
    GridsterModule
  ],
  declarations: [
    DECLARATIONS,
    WeatherComponent,
    ClockComponent,
    TodoWidgetComponent,
    WidgetsContainerComponent,
    BookmarkComponent,
    ClockDetailViewComponent,
    CurrencyComponent,
  ],
  providers: [
    FileUploaderService,
    WriteProtectionService,
  ],
  entryComponents: [
    ClockDetailViewComponent,
  ]
})
export class MainModule {
}
