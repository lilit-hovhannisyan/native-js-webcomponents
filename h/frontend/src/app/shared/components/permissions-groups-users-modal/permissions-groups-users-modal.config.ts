import { NvColumnDataType, NvFilterControl, NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getPermissionsGroupsUsersModalGridConfig = (
  isAutobankPage = true,
): NvGridConfig => {
  return {
    gridName: `${isAutobankPage ? 'autobank' : 'payments'}PermissionGroupUsersModalGrid`,
    title: wording.system.users,
    sortBy: 'displayName',
    isSortAscending: true,
    disableHideColumns: true,
    hideRefreshButton: true,
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    rowSelectionType: NvGridRowSelectionType.Checkbox,
    paging: {
      pageNumber: 1,
      pageSize: 10
    },
    columns: [
      {
        key: 'displayName',
        title: wording.general.displayName,
        width: 150,
        filter: {
          values: []
        }
      },
      {
        key: 'lastLoggedIn',
        title: wording.general.lastLoggedIn,
        dataType: NvColumnDataType.DateTime,
        filter: {
          values: []
        },
        width: 170,
      },
      {
        key: 'isActive',
        title: wording.general.isActive,
        width: 80,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
  };
};

