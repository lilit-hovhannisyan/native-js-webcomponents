import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';

import { IconsModule } from '@shared/modules/icons.module';

import { MeButtonModule } from '../me-button/me-button.module';
import { MePopupSelectComponent } from './me-popup-select.component';

@NgModule({
  imports: [CommonModule, IconsModule, MatDialogModule, MeButtonModule],
  declarations: [MePopupSelectComponent],
  exports: [MePopupSelectComponent]
})
export class MePopupSelectModule {}
