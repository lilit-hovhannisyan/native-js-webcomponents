import { wording } from 'src/app/core/constants/wording/wording';
import { NvLocale } from 'src/app/core/models/dateFormat';
import { Language } from 'src/app/core/models/language';
import { IResource } from 'src/app/core/models/resources/IResource';
import { isNumber } from 'src/app/shared/helpers/general';
import { COUNTRY_FLAG_EMOJI } from '../constants/regexp';
import * as moment from 'moment';
import { IUser } from '../models/resources/IUser';
import { NvButton } from 'nv-grid';
import { IWithFromUntil } from '../models/resources/IWithFromUntil';

// concats url-segments, taking care of adding slashes if neccessary
export const urlConcat = (urlSegments: (string | number)[]): string => {
  let finalUrl = '';

  urlSegments.forEach(_segment => {
    const segment = _segment.toString(); // so numbers can be valid arguments
    const toAdd = segment.slice(-1) === '/' ? segment : segment + '/';
    finalUrl += toAdd;
  });

  return finalUrl;
};

export const withTrailingSlash = (url: string) => {
  if (url.substr(url.length - 1) === '/') { return url; }
  return url += '/';
};

// removes trailing slash, or doubble slashes from an url-string
export const cleanUrl = (_url: string): string => {
  let url = _url.slice(-1) === '/' ? _url.slice(0, -1) : _url;
  url = url.replace(/\/\//g, '/');
  return url;
};

// Ads an entity to an entity-list if its not in there yet, otherwise
// replaces the old version old version of it in the list.
export const getUpdatedEntityList = <T extends IResource<any>>(entityList: T[], entity: T): T[] => {
  const isNew = !entityList.find(ad => ad.id === entity.id);
  return isNew
    ? [...entityList, entity]
    : entityList.map(e => (e.id === entity.id ? entity : e));
};

// Chooses label key depending on the current language
export const getLabelKeyByLanguage = (language: string): 'labelEn' | 'labelDe' => {
  return language === Language.EN ? 'labelEn' : 'labelDe';
};

export const sortByKey = (key: string, isAscending?: boolean): (a: any, b: any) => number => {
  const isAscendingNum = isAscending ? 1 : -1;
  return (a: any, b: any): number => {
    const first = (a[key] || a[key] === 0) ? a[key] : '';
    const second = (b[key] || b[key] === 0) ? b[key] : '';

    return isNumber(first)
      ? isAscendingNum * (+(first > second) - +(second > first))
      : isAscendingNum * (+(first.toLowerCase() > second.toLowerCase()) - +(second.toLowerCase() > first.toLowerCase()));
  };
};

/**
 *For rounding and cropping fractional part
 * of the number to keep requirement `Decimal(m, n)`.
 * In which `n` is `fractionalPartLength`.
 */
export const roundFractionalPart = (value: number, fractionalPartLength: number = 0): number => {
  return Math.round(value * 10 ** fractionalPartLength) / (10 ** fractionalPartLength);
};

export const copyToClipboard = (value: string) => {
  const selBox = document.createElement('textarea');
  selBox.style.position = 'fixed';
  selBox.style.left = '0';
  selBox.style.top = '0';
  selBox.style.opacity = '0';
  selBox.value = value;
  document.body.appendChild(selBox);
  selBox.focus();
  selBox.select();
  document.execCommand('copy');
  document.body.removeChild(selBox);
};

export const getDecimalLanguageSaperator = (locale: NvLocale) => {
  return locale === NvLocale.DE ? ',' : '.';
};

export const toFloat = (number: number, numbersAfterSeparator = 2): number => {
  return parseFloat(number.toFixed(numbersAfterSeparator));
};

export const getContextMenuButtonsConfig = (url?: string): NvButton[] => {
  const { origin, pathname } = window.location;
  const _url = url || `${origin}${pathname}`;

  return [
    {
      icon: 'form',
      description: wording.general.openInNewTab,
      func: (row) => window.open(`${_url}/${row.id}`),
    },
    {
      icon: 'form',
      description: wording.general.openInNewWindow,
      func: (row) => window.open(
        `${_url}/${row.id}`,
        '_blank',
        `location=1, width=${screen.availWidth}, height=${screen.availHeight}`
      ),
    },
  ];
};

export const isUpperCase = (str: string) => {
  return str === str.toUpperCase();
};

export const toUpperSnakeCase = (str: string) => {
  if (str === str.toUpperCase()) {
    return str;
  }

  return str
    .split(/(?=[A-Z])/)
    .join('_')
    .toUpperCase();
};

export function getUniqueId(length?: number, radix = 16): string {
  const value = Math.random().toString(radix);
  return value.slice(3, length || value.length - 3);
}

export function arrayToKeyHashTable<T extends { id: number }>(
  arr: Array<T>,
  key: keyof T = 'id'
) {
  return arr.reduce((obj, el) => {
    obj[el[key]] = el;

    return obj;
  }, {} as any);
}

/**
 * returns emoji of country flag:
 * example: 'AM' => 🇦🇲
 * @param isoAlpha2 iso alpha 2 code of country
 */
export const isoAlpha2ToFlagEmoji = (isoAlpha2: string): string => {
  const emoji = String.fromCodePoint(...[...isoAlpha2.toUpperCase()]
    .map(char => char.charCodeAt(0) + 127397));

  return COUNTRY_FLAG_EMOJI.test(emoji)
    ? emoji
    : '';
};

export const openNewTab = (url: string): void => {
  window.open(url, '_blank');
};

export const getGreaterDate = (date1: string, date2: string): string => {
  return moment(date1).isSameOrAfter(moment(date2))
    ? date1
    : date2;
};

export const getFullName = (user: IUser): string => {
  if (!user) {
    return '';
  }
  return `${user.firstName} ${user.lastName}`;
};

export function findClosestWithFromUntil<T extends IWithFromUntil>(data: T[]): T {
  if (!data?.length) {
    return undefined;
  }

  /**
   * On particular cases, the backend can give not sorted array.
   * For example seeded data can be not sorted.
   */
  const sortedData: T[] = data.sort((a: T, b: T) => {
    return moment(a.from).isAfter(b.from) ? 1 : -1;
  });

  const pastItems: T[] = sortedData.filter(accounting => {
    return moment().diff(accounting.from, 'minutes') >= 0;
  });

  // If there are past items return last one
  if (pastItems.length) {
    return pastItems[pastItems.length - 1];
  }

  const futureItems = sortedData.filter(item => {
    return moment().diff(item.from, 'minutes') < 0;
  });

  // If there are future items return first one
  if (futureItems.length) {
    return futureItems[0];
  }

  // In case there are accountings with wrong date (from)
  return sortedData[sortedData.length - 1];
}
