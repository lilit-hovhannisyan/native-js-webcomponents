import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/tex-keys.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { NotificationType } from 'src/app/core/models/INotification';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { CALCULATION_TYPES, ITaxKey, ITaxKeyRow, TAX_CATEGORIES } from 'src/app/core/models/resources/ITaxKeys';
import { TaxKeyRepositoryService } from 'src/app/core/services/repositories/tax-key-repository.service';

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
})
export class TaxKeyListViewComponent implements OnInit {
  private isSuperAdmin: boolean;
  public taxKeysGridConfig: NvGridConfig;
  public dataSource$: Observable<ITaxKeyRow[]>;

  constructor(
    private taxKeyRepository: TaxKeyRepositoryService,
    public gridConfigService: GridConfigService,
    private confirmationService: ConfirmationService,
    private notificationService: NotificationsService,
    private router: Router,
    private authService: AuthenticationService
  ) { }

  public ngOnInit() {
    this.isSuperAdmin = this.authService.getSessionInfo().user.isSuperAdmin;
    this.taxKeyRepository.fetchAll({}).subscribe();
    this.dataSource$ = this.taxKeyRepository.getStream().pipe(
      map(taxes => taxes.map(tax => {
        return {
          ...tax,
          taxCategoryLabel: tax.taxCategory === TAX_CATEGORIES.inputTax
            ? wording.accounting.inputTax
            : wording.accounting.valueAddedTax,
          calculationTypeLabel: tax.calculationType === CALCULATION_TYPES.include
            ? wording.accounting.taxKeysLabelCalculationInclude
            : wording.accounting.taxKeysLabelCalculationExclude,
        } as ITaxKeyRow;
      }))
    );


    const actions = {
      edit: this.editClicked,
      add: () => this.router.navigate([`${this.router.url}/new`]),
      delete: this.deleteClicked,
    };

    this.taxKeysGridConfig = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  private editClicked = (taxKey: ITaxKey) => {
    this.router.navigate([`${this.router.url}/${taxKey.id}`]);
  }

  private deleteClicked = (taxKey: ITaxKey) => {
    if (taxKey.isSystemDefault && !this.isSuperAdmin) {
      return this.notificationService.notify(
        NotificationType.Warning,
        wording.general.notAllowed,
        wording.accounting.systemDefaultTaxKeyCannotBeDeleted
      );
    }

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: taxKey.displayLabel },
    }).subscribe(confirmed => confirmed && this.taxKeyRepository.delete(taxKey.id, {}).subscribe());
  }
}
