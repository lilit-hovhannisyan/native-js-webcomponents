import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { PaymentsRoutingModule, ROUTING_DECLARATIONS } from './payments-routing.module';


const ENTRY_COMPONENTS = [
];

@NgModule({
  imports: [
    PaymentsRoutingModule,
    SharedModule,
  ],
  declarations: [
    ...ROUTING_DECLARATIONS,
    ...ENTRY_COMPONENTS,
  ],
  entryComponents: ENTRY_COMPONENTS,
})
export class PaymentsModule {
}
