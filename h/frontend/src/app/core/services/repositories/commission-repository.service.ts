import { Injectable } from '@angular/core';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IVesselCommissionCreate, IVesselCommission, IVesselCommissionDTO, CommissionKey } from '../../models/resources/IVesselCommission';

@Injectable({
  providedIn: 'root'
})
export class CommissionRepositoryService extends AbstractRepository<
  IVesselCommission,
  IVesselCommissionDTO,
  IVesselCommissionCreate,
  RepoParams<IVesselCommission>,
  CommissionKey
>  {
  protected resourceType = 'Commission';
  public sortAsce = true;
  public sortKey = 'id';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/vessel-commissions';

  protected postProcess = (item: IVesselCommissionDTO): IVesselCommission => item;
}
