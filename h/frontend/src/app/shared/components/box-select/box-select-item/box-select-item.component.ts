import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';

@Component({
  selector: 'nv-box-select-item',
  templateUrl: './box-select-item.component.html',
  styleUrls: [ './box-select-item.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BoxSelectItemComponent {
  @Input() public value: number;
  @Output() public clickEmitter = new EventEmitter<number>();

  @HostBinding('class.selected') public selected = false;

  @HostListener('click')
  public handleClick(): void {
    this.clickEmitter.emit(this.value);
  }
}
