import {BaseComponent} from '../../../../../library/core/base-component/base.component.js';
import {basePath} from '../../../data/base.path.js';

import {setCache} from '../../../data/git.data.js';

export class GitAvatar extends BaseComponent {
  constructor() {
    super(basePath + 'shared-components/git-avatar/git-avatar.component.css');
  }

  render = () => {
    const userLogin = this.getAttribute('user');
    const userId = this.getAttribute('userid');

    if (userLogin) {
      let url = 'https://api.github.com/users/' + userLogin;

      fetch(url)
        .then(respone => {
          console.log('zaeazeaz', response.status);
        })
        .then(response => response.json())
        .then(user => {
          setCache(user);
          this.initAvatar(user);
        })
        .catch( e => {
          const randomWholeNumber = Math.floor(Math.random() * 60);

          const user = {
            name: 'dummy user ' + userId,
            avatar_url: 'https://i.pravatar.cc/150?img=' + randomWholeNumber,
            id: userId,
          };

          setCache(user);
          this.initAvatar(user);

          console.log('catch')
        })

    } else {
      const user = {
        name: this.getAttribute('name'),
        avatar_url: this.getAttribute('avatar'),
        id: this.getAttribute('userid'),
      };

      this.initAvatar(user);
    }
  }

  initAvatar(user) {
    const config = {
      tagName: 'div',
      attr: {
        class: 'git-avatar',
        gituserid: user.id,
      },
      subElements: [
        {
          tagName: 'img',
          attr: {
            class: 'git-avatar-img',
            src: user.avatar_url,
            alt: 'gitAvatar'
          }
        },
        {
          tagName: 'text-component',
          attr: {
            class: 'git-avatar-name',
            text: user.name,
          }
        }
      ]
    };

    const el = this.createElementWithAttributes(config);

    this.shadow.appendChild(el);
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }
}