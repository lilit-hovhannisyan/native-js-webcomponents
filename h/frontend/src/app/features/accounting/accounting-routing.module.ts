import { NgModule, Type } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { UnsavedChangesGuard } from 'src/app/routing/services/unsaved-changes.guard';
import { HistoryComponent } from 'src/app/shared/navido-components/history/history.component';
import { MetaComponent } from 'src/app/shared/navido-components/meta/meta.component';
import { NavigationBoardComponent } from '../../shared/components/navigation-board/navigation-board.component';
import { PageNotFoundComponent } from '../../shared/components/page-not-found/page-not-found.component';
import { DebitorCreditorBasicTabComponent } from '../system/components/misc/debitor-and-creditors/detial-view/basic/basic.component';
import { DebitorCreditorConditionsOfPaymentTabComponent } from '../system/components/misc/debitor-and-creditors/detial-view/conditions-of-payment/conditions-of-payment.component';
import { DebitorAndCreditorDetialViewComponent } from '../system/components/misc/debitor-and-creditors/detial-view/debitor-and-creditor-detial-view.component';
import { DebitorCreditorDunningSystemTabComponent } from '../system/components/misc/debitor-and-creditors/detial-view/dunning-system/dunning-system.component';
import { DebitorCreditorRemarksTabComponent } from '../system/components/misc/debitor-and-creditors/detial-view/remarks/remarks.component';
import { DebitorAndCreditorListViewComponent } from '../system/components/misc/debitor-and-creditors/list-view/debitor-and-creditor-list-view.component';
import { AccountingRelationsComponent } from './components/autobank/accounting-relations/accounting-relations.component';
import { AutobankPermissionsComponent } from './components/autobank/autobank-permissions/autobank-permissions.component';
import { AutobankRulesDetailViewComponent } from './components/autobank/autobank-rules/autobank-rules-detail-view/autobank-rules-detail-view.component';
import { AutobankRulesViewListComponent } from './components/autobank/autobank-rules/autobank-rules-view-list/autobank-rules-view-list.component';
import { AccountSheetsComponent } from './components/booking/account-sheets/account-sheets.component';
import { OpenItemAccountSheetsComponent } from './components/booking/open-item-account-sheets/open-item-account-sheets.component';
import { BookingAccountsDetailViewComponent } from './components/booking/booking-accounts/detail-view/detail-view.component';
import { BookingAccountsListViewComponent } from './components/booking/booking-accounts/list-view/list-view.component';
import { BookingCodesDetailViewComponent } from './components/booking/booking-codes/detail-view/booking-codes-detail-view.component';
import { BookingCodesListViewComponent } from './components/booking/booking-codes/list-view/booking-codes-list-view.component';
import { BookingGridComponent } from './components/booking/booking-grid-inline/booking-grid.component';
import { InvoiceLedgerDetailViewComponent } from './components/booking/invoice-ledger/detail-view/invoice-ledger-detail-view.component';
import { InvoiceLedgerListViewComponent } from './components/booking/invoice-ledger/list-view/invoice-ledger-list-view.component';
import { CompanyAndAddressComponent } from './components/booking/mandators/detail-view/company-and-address/company-and-address.component';
import { MandatorAccountListViewComponent } from './components/booking/mandators/detail-view/mandator-account/list-view/mandator-account-list-view.component';
import { MandatorBasicComponent } from './components/booking/mandators/detail-view/mandator-basic/mandator-basic.component';
import { MandatorBookingCodesListViewComponent } from './components/booking/mandators/detail-view/mandator-booking-codes/list-view/list-view.component';
import { MandatorCorporateComponent } from './components/booking/mandators/detail-view/mandator-corporate/mandator-corporate.component';
import { MandatorRemarksComponent } from './components/booking/mandators/detail-view/mandator-remarks/mandator-remarks.component';
import { MandatorRolesComponent } from './components/booking/mandators/detail-view/mandator-roles/mandator-roles.component';
import { MandatorTaxKeyListViewComponent } from './components/booking/mandators/detail-view/mandator-tax-keys/list-view/list-view.component';
import { MandatorVoucherTypesListViewComponent } from './components/booking/mandators/detail-view/voucher-types/list-view/list-view.component';
import { MandatorListViewComponent } from './components/booking/mandators/list-view/mandator-list-view.component';
import { MasterStructuresComponent } from './components/booking/master-structures/master-structure.component';
import { OpenItemAccountsListViewComponent } from './components/booking/open-item-accounts/list-view/open-item-accounts-list-view.component';
import { TaxKeyDetailViewComponent } from './components/booking/tax-keys/detail-view/detail-view.component';
import { TaxKeyListViewComponent } from './components/booking/tax-keys/list-view/list-view.component';
import { VoucherTypeDetailViewComponent } from './components/booking/voucher-types/detail-view/detail-view.component';
import { VoucherTypesListViewComponent } from './components/booking/voucher-types/list-view/list-view.component';
import { TabHandlerComponent } from 'src/app/shared/components/tab-handler/tab-handler.component';
import { AutobankRuleRepositoryService } from 'src/app/core/services/repositories/autobank-rule-repository.service';
import { MandatorsHelperService } from './components/booking/mandators/detail-view/mandators.helper.service';
import { AutobankPermissionsBasicComponent } from './components/autobank/autobank-permissions/autobank-permissions-basic/autobank-permissions-basic.component';
import { AutobankGeneralSettingsComponent } from './components/autobank/autobank-settings/autobank-general-settings/autobank-general-settings.component';
import { AutobankSettingDetailViewComponent } from './components/autobank/autobank-settings/autobank-setting-detail-view.component';
import { CostTypesListViewComponent } from './components/cost-accounting/cost-types/list-view/list-view.component';
import { CostTypesDetailViewComponent } from './components/cost-accounting/cost-types/detail-view/detail-view.component';
import { CostCentersListViewComponent } from './components/cost-accounting/cost-centers/list-view/cost-centers-list-view.component';
import { CostCentersDetailViewComponent } from './components/cost-accounting/cost-centers/detail-view/cost-centers-detail-view.component';
import { PermissionsHistoryComponent } from 'src/app/shared/components/permissions-history/permissions-history.component';
import { CostUnitsListViewComponent } from './components/cost-accounting/cost-units/list-view/cost-units-list-view.component';
import { CostUnitsDetailViewComponent } from './components/cost-accounting/cost-units/detail-view/cost-units-detail-view.component';
import { CostUnitCategoriesListViewComponent } from './components/cost-accounting/cost-unit-categories/list-view/cost-unit-categories-list-view.component';
import { CostUnitCategoriesDetailViewComponent } from './components/cost-accounting/cost-unit-categories/detail-view/cost-unit-categories-detail-view.component';
import { AutobankBusinessCasesViewListComponent } from './components/autobank/autobank-settings/autobank-settings-business-cases/autobank-business-cases-view-list/autobank-business-cases-view-list.component';
import { AutobankBusinessCasesDetailViewComponent } from './components/autobank/autobank-settings/autobank-settings-business-cases/autobank-business-cases-detail-view/autobank-business-cases-detail-view.component';
import { AutobankBusinessCasesEditModalComponent } from './components/autobank/autobank-settings/autobank-settings-business-cases/autobank-business-cases-edit-modal/autobank-business-cases-edit-modal.component';
import { VoucherTypeRepositoryService } from 'src/app/core/services/repositories/voucher-type-repository.service';
import { TaxKeyRepositoryService } from '../../core/services/repositories/tax-key-repository.service';

export const routes: Routes = [
  {
    path: '',
    component: NavigationBoardComponent,
    pathMatch: 'full'
  },
  {
    path: sitemap.accounting.children.booking.path,
    children: [
      {
        path: '',
        component: NavigationBoardComponent,
        pathMatch: 'full'
      },
      {
        path: sitemap.accounting.children.booking.children.voucherTypes.path,
        children: [
          {
            path: '',
            component: VoucherTypesListViewComponent,
          },
          {
            path: ':id',
            component: TabHandlerComponent,
            data: {
              parentNode: sitemap.accounting.children.booking.children.voucherTypes
            },
            children: [
              {
                path: sitemap.accounting.children.booking.children.voucherTypes.children.id.children.basic.path,
                component: VoucherTypeDetailViewComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.accounting.children.booking.children.voucherTypes.children.id.children.meta.path,
                component: MetaComponent,
                data: {
                  service: VoucherTypeRepositoryService
                },
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.accounting.children.booking.children.voucherTypes.children.id.children.history.path,
                component: HistoryComponent,
                data: {
                  entityKey: 'id',
                  entity: 'VoucherType',
                  entityName: 'voucher-types'
                }
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.accounting.children.booking.children.voucherTypes.children.id.children.basic.path,
              }
            ],
          }
        ]
      },
      {
        path: sitemap.accounting.children.booking.children.mandators.path,
        children: [
          {
            path: '',
            component: MandatorListViewComponent,
          },
          {
            path: ':id',
            component: TabHandlerComponent,
            data: {
              parentNode: sitemap.accounting.children.booking.children.mandators,
              helperService: MandatorsHelperService
            },
            children: [
              {
                path: sitemap.accounting.children.booking.children.mandators.children.id.children.basic.path,
                component: MandatorBasicComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.accounting.children.booking.children.mandators.children.id.children.company.path,
                component: CompanyAndAddressComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.accounting.children.booking.children.mandators.children.id.children.voucherTypes.path,
                component: MandatorVoucherTypesListViewComponent
              },
              {
                path: sitemap.accounting.children.booking.children.mandators.children.id.children.bookingCodes.path,
                component: MandatorBookingCodesListViewComponent
              },
              {
                path: sitemap.accounting.children.booking.children.mandators.children.id.children.accounts.path,
                component: MandatorAccountListViewComponent
              },
              {
                path: sitemap.accounting.children.booking.children.mandators.children.id.children.taxKeys.path,
                component: MandatorTaxKeyListViewComponent
              },
              {
                path: sitemap.accounting.children.booking.children.mandators.children.id.children.roles.path,
                component: MandatorRolesComponent
              },
              {
                path: sitemap.accounting.children.booking.children.mandators.children.id.children.corporate.path,
                component: MandatorCorporateComponent
              },
              {
                path: sitemap.accounting.children.booking.children.mandators.children.id.children.meta.path,
                component: MetaComponent,
                data: {
                  service: MandatorRepositoryService
                },
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.accounting.children.booking.children.mandators.children.id.children.history.path,
                component: HistoryComponent,
                data: {
                  entityKey: 'id',
                  entity: 'Mandator',
                  entityName: 'mandators'
                }
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.accounting.children.booking.children.mandators.children.id.children.basic.path,
              }
            ]
          }]
      },
      {
        path: sitemap.accounting.children.booking.children.debitorCreditors.path,
        children: [
          {
            path: '',
            component: DebitorAndCreditorListViewComponent,
          },
          {
            path: ':id',
            component: DebitorAndCreditorDetialViewComponent,
            children: [
              {
                path: sitemap.accounting.children.booking.children.debitorCreditors.children.id.children.basic.path,
                component: DebitorCreditorBasicTabComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.accounting.children.booking.children.debitorCreditors.children.id.children.dunningSystem.path,
                component: DebitorCreditorDunningSystemTabComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.accounting.children.booking.children.debitorCreditors.children.id.children.basic.path,
              },
              {
                path: sitemap.accounting.children.booking.children.debitorCreditors.children.id.children.conditionsOfPayment.path,
                component: DebitorCreditorConditionsOfPaymentTabComponent,
                canDeactivate: [UnsavedChangesGuard]

              },
              {
                path: sitemap.accounting.children.booking.children.debitorCreditors.children.id.children.remarks.path,
                component: DebitorCreditorRemarksTabComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
            ]
          },
        ]
      },
      {
        path: sitemap.accounting.children.booking.children.taxKeys.path,
        children: [
          {
            path: '',
            component: TaxKeyListViewComponent,
          },
          {
            path: ':id',
            component: TabHandlerComponent,
            data: {
              parentNode: sitemap.accounting.children.booking.children.taxKeys
            },
            children: [
              {
                path: sitemap.accounting.children.booking.children.taxKeys.children.id.children.basic.path,
                component: TaxKeyDetailViewComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.accounting.children.booking.children.taxKeys.children.id.children.meta.path,
                component: MetaComponent,
                data: {
                  service: TaxKeyRepositoryService
                },
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.accounting.children.booking.children.taxKeys.children.id.children.history.path,
                component: HistoryComponent,
                data: {
                  entityKey: 'id',
                  entity: 'TaxKey',
                  entityName: 'tax-keys'
                }
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.accounting.children.booking.children.taxKeys.children.id.children.basic.path,
              }
            ],
          }
        ]
      },
      {
        path: sitemap.accounting.children.booking.children.bookingAccounts.path,
        children: [
          {
            path: '',
            component: BookingAccountsListViewComponent,
          },
          {
            path: ':id',
            component: BookingAccountsDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          }
        ]
      },
      {
        path: sitemap.accounting.children.booking.children.bookingCodes.path,
        children: [
          {
            path: '',
            component: BookingCodesListViewComponent,
          },
          {
            path: ':id',
            component: BookingCodesDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          }
        ]
      },
      {
        path: sitemap.accounting.children.booking.children.bookings.path,
        children: [
          {
            path: '',
            redirectTo: 'new',
            pathMatch: 'full'
          },
          {
            path: ':id',
            component: BookingGridComponent,
            canDeactivate: [UnsavedChangesGuard]
          }
        ],
      },
      {
        path: sitemap.accounting.children.booking.children.accountSheets.path,
        children: [
          {
            path: '',
            component: AccountSheetsComponent,
          }
        ],
      },
      {
        path: sitemap.accounting.children.booking.children.openItemAccountSheets.path,
        children: [
          {
            path: '',
            component: OpenItemAccountSheetsComponent,
          }
        ],
      },
      {
        path: sitemap.accounting.children.booking.children.invoiceLedger.path,
        children: [
          {
            path: '',
            component: InvoiceLedgerListViewComponent,
          },
          {
            path: ':id',
            component: InvoiceLedgerDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          }
        ],
      },
      {
        path: sitemap.accounting.children.booking.children.openItemAccounts.path,
        children: [
          {
            path: '',
            component: OpenItemAccountsListViewComponent,
          }
        ]
      },
      {
        path: sitemap.accounting.children.booking.children.masterStructures.path,
        children: [
          {
            path: '',
            component: MasterStructuresComponent,
          },
        ],
      },
    ],
  },
  {
    path: sitemap.accounting.children.autobank.path,
    children: [
      {
        path: '',
        component: NavigationBoardComponent,
        pathMatch: 'full'
      },
      {
        path: sitemap.accounting.children.autobank.children.rules.path,
        children: [
          {
            path: '',
            component: AutobankRulesViewListComponent,
          },
          {
            path: ':id',
            component: TabHandlerComponent,
            data: {
              parentNode: sitemap.accounting.children.autobank.children.rules,
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.accounting.children.autobank.children.rules.children.id.children.basic.path,
              },
              {
                path: sitemap.accounting.children.autobank.children.rules.children.id.children.basic.path,
                component: AutobankRulesDetailViewComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.accounting.children.autobank.children.rules.children.id.children.meta.path,
                component: MetaComponent,
                data: {
                  service: AutobankRuleRepositoryService
                },
                canDeactivate: [UnsavedChangesGuard]
              },
            ]
          },
        ]
      },
      {
        path: sitemap.accounting.children.autobank.children.accountingRelations.path,
        children: [
          {
            path: '',
            component: AccountingRelationsComponent,
            canDeactivate: [UnsavedChangesGuard]
          },
        ]
      },
      {
        path: sitemap.accounting.children.autobank.children.autobankPermissions.path,
        children: [
          {
            path: '',
            component: AutobankPermissionsComponent,
            children: [
              {
                path: sitemap.accounting.children.autobank.children.autobankPermissions.children.id.path,
                canDeactivate: [UnsavedChangesGuard],
                children: [
                  {
                    path: sitemap.accounting.children.autobank.children.autobankPermissions.children.id.children.basic.path,
                    component: AutobankPermissionsBasicComponent,
                    canDeactivate: [UnsavedChangesGuard],
                  },
                  {
                    path: sitemap.accounting.children.autobank.children.autobankPermissions.children.id.children.history.path,
                    component: PermissionsHistoryComponent,
                    data: {
                      entityKey: 'id',
                      entity: 'AutobankPermission',
                      entityName: 'autobank/permissions',
                    },
                  },
                  {
                    path: '',
                    pathMatch: 'full',
                    redirectTo: sitemap.accounting.children.autobank.children.autobankPermissions.children.id.children.basic.path,
                  },
                ],
              }
            ]
          },
        ]
      },
      {
        path: sitemap.accounting.children.autobank.children.autobankSettings.path,
        component: AutobankSettingDetailViewComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: sitemap.accounting.children.autobank.children.autobankSettings.children.settings.path,
          },
          {
            path: sitemap.accounting.children.autobank.children.autobankSettings.children.settings.path,
            component: AutobankGeneralSettingsComponent,
            canDeactivate: [UnsavedChangesGuard]
          },
          {
            path: sitemap.accounting.children.autobank.children.autobankSettings.children.businessCases.path,
            component: AutobankBusinessCasesViewListComponent,
          },
        ]
      },
    ]
  },
  {
    path: sitemap.accounting.children.costAccounting.path,
    children: [
      {
        path: '',
        component: NavigationBoardComponent,
        pathMatch: 'full'
      },
      {
        path: sitemap.accounting.children.costAccounting.children.costTypes.path,
        children: [
          {
            path: '',
            component: CostTypesListViewComponent,
          },
          {
            path: ':id',
            component: CostTypesDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          }
        ]
      },
      {
        path: sitemap.accounting.children.costAccounting.children.costCenters.path,
        children: [
          {
            path: '',
            component: CostCentersListViewComponent,
          },
          {
            path: ':id',
            component: CostCentersDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          }
        ]
      },
      {
        path: sitemap.accounting.children.costAccounting.children.costUnits.path,
        children: [
          {
            path: '',
            component: CostUnitsListViewComponent,
          },
          {
            path: ':id',
            component: CostUnitsDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          }
        ]
      },
      {
        path: sitemap.accounting.children.costAccounting.children.costUnitCategories.path,
        children: [
          {
            path: '',
            component: CostUnitCategoriesListViewComponent,
          },
          {
            path: ':id',
            component: CostUnitCategoriesDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          }
        ]
      },
    ]
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  }
];

export const DECLARATIONS: Array<Type<any> | any[]> = [
  BookingAccountsDetailViewComponent,
  BookingAccountsListViewComponent,
  BookingCodesListViewComponent,
  BookingCodesDetailViewComponent,
  MandatorVoucherTypesListViewComponent,
  MandatorBasicComponent,
  CompanyAndAddressComponent,
  MandatorBookingCodesListViewComponent,
  MandatorAccountListViewComponent,
  MandatorTaxKeyListViewComponent,
  MandatorRolesComponent,
  VoucherTypesListViewComponent,
  VoucherTypeDetailViewComponent,
  MandatorListViewComponent,
  TaxKeyDetailViewComponent,
  TaxKeyListViewComponent,
  BookingGridComponent,
  DebitorAndCreditorListViewComponent,
  DebitorAndCreditorDetialViewComponent,
  DebitorCreditorBasicTabComponent,
  DebitorCreditorDunningSystemTabComponent,
  DebitorCreditorConditionsOfPaymentTabComponent,
  DebitorCreditorRemarksTabComponent,
  MandatorRemarksComponent,
  MandatorCorporateComponent,
  InvoiceLedgerDetailViewComponent,
  InvoiceLedgerListViewComponent,
  OpenItemAccountSheetsComponent,
  OpenItemAccountsListViewComponent,
  MasterStructuresComponent,
  AutobankPermissionsComponent,
  AutobankPermissionsBasicComponent,
  AutobankGeneralSettingsComponent,
  AutobankSettingDetailViewComponent,
  CostTypesListViewComponent,
  CostTypesDetailViewComponent,
  CostCentersListViewComponent,
  CostCentersDetailViewComponent,
  CostUnitsDetailViewComponent,
  CostUnitsListViewComponent,
  CostUnitCategoriesDetailViewComponent,
  CostUnitCategoriesListViewComponent,
  AutobankBusinessCasesViewListComponent,
  AutobankBusinessCasesDetailViewComponent,
  AutobankBusinessCasesEditModalComponent,
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountingRoutingModule {
}

