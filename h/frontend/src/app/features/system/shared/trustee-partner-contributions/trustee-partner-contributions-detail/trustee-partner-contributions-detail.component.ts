import { Component, OnInit, Input, EventEmitter, Output, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NvGridConfig } from 'nv-grid';
import { TrusteePartnerContributionMainService, LatestContributionsMap } from 'src/app/core/services/mainServices/trustee-partner-contributions-main.service';
import { ActivatedRoute } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { CompanyKey, ICompany } from 'src/app/core/models/resources/ICompany';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { TrusteePartnerContributionModalComponent } from '../trustee-partner-contribution-modal/trustee-partner-contribution-modal.component';
import { ITrusteePartnerContributionFull, ITrusteePartnerContribution } from 'src/app/core/models/resources/ITrusteePartnerContribution';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { getContributionsGridConfig } from './trustee-partner-contributions-grid.config';
import { SettingsService } from 'src/app/core/services/settings.service';
import { CompanyLegalEntityRepositoryService } from 'src/app/core/services/repositories/company-legal-entity-repository.service';
import { ICurrency, CurrencyKey } from 'src/app/core/models/resources/ICurrency';
import { Observable, BehaviorSubject, forkJoin, combineLatest } from 'rxjs';
import { tap, take, map } from 'rxjs/operators';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { ICompanyLegalEntity } from 'src/app/core/models/resources/ICompanyLegalEntity';
import { TrusteePartnerContributionEditModalComponent } from '../trustee-partner-contribution-edit-modal/trustee-partner-contribution-edit-modal.component';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { IWording } from 'src/app/core/models/resources/IWording';
import {COMPANY_CONTRIBUTION_TYPES, createModalTitles, sectionTitles} from '../enums/company-contributions';
import { TrusteePartnerContributionsRepositoryService } from 'src/app/core/services/repositories/trustee-partner-contribution-repository.service';
import { COMPANY_LEGAL_FORM, CAPITAL_TYPE } from 'src/app/core/models/enums/company-legal-form';


@Component({
  selector: 'nv-trustee-partner-contributions-detail',
  templateUrl: './trustee-partner-contributions-detail.component.html',
  styleUrls: ['./trustee-partner-contributions-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrusteePartnerContributionsDetailComponent implements OnInit {
  @Input() public contributorType: COMPANY_CONTRIBUTION_TYPES;
  @Input() public companyId: CompanyKey;

  public wording = wording;
  public sectionTitleWording: IWording;
  public form: FormGroup;
  public resultGridConfig: NvGridConfig;
  public historyGridConfig: NvGridConfig;
  private company: ICompany;
  private legalEntity: ICompanyLegalEntity;
  private isNonParValueShare = false;
  private currency: ICurrency;
  private currencyEuro: ICurrency;
  private selectedDateSubject: BehaviorSubject<string> = new BehaviorSubject('');
  private contributionsSubject: BehaviorSubject<ITrusteePartnerContributionFull[]>
    = new BehaviorSubject<ITrusteePartnerContributionFull[]>([]);

  public latestContributionsMap: LatestContributionsMap;
  public filteredContributions: ITrusteePartnerContributionFull[];
  public resultData: ITrusteePartnerContributionFull[];
  public historyData: ITrusteePartnerContributionFull[];


  @Input() public set contributions(value: ITrusteePartnerContributionFull[]) {
    this.contributionsSubject.next(value);
  }
  public get contributions(): ITrusteePartnerContributionFull[] {
    return this.contributionsSubject.value;
  }

  @Input()
  public set selectedDate(value: string) {
    this.selectedDateSubject.next(value);
  }
  public get selectedDate(): string {
    return this.selectedDateSubject.getValue();
  }

  @Output() public add: EventEmitter<ITrusteePartnerContribution> = new EventEmitter<ITrusteePartnerContribution>();
  @Output() public delete: EventEmitter<ITrusteePartnerContribution> = new EventEmitter<ITrusteePartnerContribution>();
  @Output() public edit: EventEmitter<ITrusteePartnerContribution> = new EventEmitter<ITrusteePartnerContribution>();

  constructor(
    private modalService: NzModalService,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private trusteeContributionsMainService: TrusteePartnerContributionMainService,
    private contributionsRepo: TrusteePartnerContributionsRepositoryService,
    private authService: AuthenticationService,
    private settingsService: SettingsService,
    private legalEntityRepo: CompanyLegalEntityRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private confirmationService: ConfirmationService,
    private changeDetectionRef: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    this.sectionTitleWording = sectionTitles.get(this.contributorType);

    forkJoin([
      this.fetchCompany(),
      this.fetchCurrency(),
      this.selectedDateSubject.pipe(take(1)),
    ]).subscribe(() => this.init());
  }

  private fetchCurrency(): Observable<void> {
    return forkJoin([
      this.legalEntityRepo.fetchOne(this.companyId, {}),
      this.currencyRepo.fetchAll({}),
    ]).pipe(
      map(([legalEntity, currencies]: [ICompanyLegalEntity, ICurrency[]]) => {
        this.legalEntity = legalEntity;
        const currencyId: CurrencyKey = legalEntity?.currencyId;

        currencies.some(c => {
          if (!c.isActive) {
            return;
          }

          if (c.isoCode === 'EUR') {
            this.currencyEuro = c;
          }
          if (c.id === currencyId) {
            this.currency = c;
          }
          // break loop when both are found
          return this.currency && this.currencyEuro;
        });

        // if currency of legalEntity has not set, use EURO
        if (!currencyId) {
          this.currency = this.currencyEuro;
        }
      })
    );
  }

  private fetchCompany(): Observable<ICompany> {
    return this.companyRepo.fetchOne(this.companyId, {}).pipe(
      tap((company) => {
        this.company = company;
      }),
    );
  }

  private init(): void {
    this.isNonParValueShare = this.company.legalForm === COMPANY_LEGAL_FORM.StockCorporation
      && this.legalEntity.capitalType === CAPITAL_TYPE.NonParValueShare;

    this.form = this.fb.group({
      date: [{ value: this.selectedDate, disabled: true }],
      showHistoricalData: [false],
    });

    this.changeDetectionRef.detectChanges();

    combineLatest([
      this.selectedDateSubject,
      this.contributionsSubject,
    ]).subscribe(() => {
      this.setGridsData();
      this.initGrids();
      this.form.controls.date.patchValue(this.selectedDate);
    });
  }

  private initGrids(): void {
    const totalContributionAmount: number
      = this.trusteeContributionsMainService.getTotalContributionAmount(this.resultData);

    this.resultGridConfig = getContributionsGridConfig(
      this.isNonParValueShare,
      this.settingsService,
      this.contributorType,
      this.company.legalForm,
      this.currency,
      this.latestContributionsMap,
      totalContributionAmount,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
    );

    this.historyGridConfig = getContributionsGridConfig(
      this.isNonParValueShare,
      this.settingsService,
      this.contributorType,
      this.company.legalForm,
      this.currency,
      this.latestContributionsMap,
      totalContributionAmount,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
      {
        edit: this.openEditModal,
        delete: this.deleteContribution,
      },
    );
  }

  public openContributionsModal(): void {
    this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: createModalTitles.get(this.contributorType)[this.settingsService.language],
      nzContent: TrusteePartnerContributionModalComponent,
      nzWidth: 800,
      nzClosable: true,
      nzComponentParams: {
        isNonParValueShare: this.isNonParValueShare,
        contributorType: this.contributorType,
        companyId: this.companyId,
        latestContributionsMap: this.latestContributionsMap,
        allContributions: this.contributions,
      },
    }).afterClose.subscribe((data) => {
      if (!data?.length) {
        return;
      }

      // uses getStream
      this.add.emit(data[0]);
    });
  }

  private setGridsData(): void {
    this.latestContributionsMap
      = this.trusteeContributionsMainService.getCompaniesLatestContributions(this.contributions);
    const filteredContributions: ITrusteePartnerContributionFull[]
      = this.trusteeContributionsMainService.filterContributions(this.contributions, this.selectedDate);
    const latestContributions: LatestContributionsMap
      = this.trusteeContributionsMainService.getCompaniesLatestContributions(filteredContributions);

    /**
     * Only show partners with percentage > 0
     */
    this.resultData = Array.from(latestContributions.values())
      .filter(contribution => !!contribution.contributionAmount);

    this.historyData = this.trusteeContributionsMainService
      .filterContributions(this.contributions, this.selectedDate);
  }

  private openEditModal = (entity: ITrusteePartnerContribution): void => {
    this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.general.edit[this.settingsService.language],
      nzContent: TrusteePartnerContributionEditModalComponent,
      nzWidth: 600,
      nzClosable: true,
      nzComponentParams: {
        isNonParValueShare: this.isNonParValueShare,
        company: this.company,
        legalEntity: this.legalEntity,
        contribution: entity,
        currency: this.currency,
        currencyEuro: this.currencyEuro,
        latestContributionsMap: this.latestContributionsMap,
        allContributions: this.contributions,
        contributorType: this.contributorType,
      },
    }).afterClose.subscribe((data) => {
      if (!data) {
        return;
      }
      const freshData: ITrusteePartnerContributionFull = {
        ...entity,
        ...data,
      };
      this.contributionsRepo.update(freshData, { contributorType: this.contributorType })
        .subscribe(updatedEntity => {
          // uses getStream
          this.edit.emit(updatedEntity);
        });
    });
  }
  private deleteContribution = (entity: ITrusteePartnerContribution): void => {
    this.confirmationService.confirm({
      cancelButtonWording: wording.general.cancel,
      okButtonWording: wording.general.delete,
      wording: wording.system.areYouSureYouWantToDeleteChange,
    }).subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      this.contributionsRepo.delete(entity.id, { contributorType: this.contributorType })
        .subscribe(() => {
          // uses getStream
          this.delete.emit(entity);
        });
    });
  }
}
