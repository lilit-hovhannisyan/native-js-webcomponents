export enum MeSearchSortByTypes {
  created = 'CreatedOn',
  modified = 'ModifiedOn',
  name = 'name',
  lastName = 'lastName',
  title = 'title'
}

export enum MeSearchDirectionTypes {
  ascending = 'ASC',
  descending = 'DESC'
}
