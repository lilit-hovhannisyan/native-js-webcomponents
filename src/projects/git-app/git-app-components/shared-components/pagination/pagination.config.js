export const paginationComponentContentConfig = {
  users: {
    default: 'No users data',
    fetchUrl: 'https://api.github.com/search/users?q=${id}',
    fetchDataIdentifier: 'items',
    nestedHtml: `<avatar-block class="avatar-block">
                   <git-avatar user="$login" userid="$id"></git-avatar>
                   <actions-block class="actions">
                     <button-component text="Details in popup" click="*openInPopup" user="$id"></button-component>
                     <button-component text="Details as page" click="*openInNewPage" user="$id"></button-component>
                   </actions-block>
                 </avatar-block>`
  },
  repository: {
    default: 'No repository data',
    fetchUrl: 'https://api.github.com/search/repositories?q=${id}',
    fetchDataIdentifier: 'items',
    nestedHtml: `<git-repo name="$name" url="$svn_url"></git-repo>`
  },
  userRepository: {
    default: 'No user repository data',
    fetchUrl: 'https://api.github.com/users/${id}/repos',
    nestedHtml: `<git-repo name="$name" url="$svn_url"></git-repo>`
  }
};