import { Component, Input } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import {
  gridConfig
} from 'src/app/shared/components/autocomplete/modals/booking-code-grid-config';
import { FormGroup } from '@angular/forms';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { MandatorBookingCodeRepositoryService } from 'src/app/core/services/repositories/mandator-booking-code-repository.service';
import { IWording } from 'src/app/core/models/resources/IWording';

@Component({
  selector: 'nv-booking-code-autocomplete',
  templateUrl: './booking-code-autocomplete.component.html',
  styleUrls: ['./booking-code-autocomplete.component.scss']
})
export class BookingCodeAutocompleteComponent {
  public wording = wording;
  public gridConfig = gridConfig;

  @Input() public form: FormGroup;
  @Input() public controlName = 'bookingCodeId';
  @Input() public filters: object;
  @Input() public onlyOfMandator: boolean;
  @Input() public mandatorControlName = 'mandatorId';
  @Input() public label: IWording;

  constructor(
    public bookingCodeRepo: BookingCodeRepositoryService,
    public mandatorBookingCodeRepo: MandatorBookingCodeRepositoryService
  ) { }
}
