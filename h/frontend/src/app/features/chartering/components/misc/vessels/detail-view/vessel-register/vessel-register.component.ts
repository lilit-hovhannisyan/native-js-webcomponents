import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { VesselKey } from 'src/app/core/models/resources/IVessel';

@Component({
  selector: 'nv-vessel-register',
  templateUrl: './vessel-register.component.html',
  styleUrls: ['./vessel-register.component.scss']
})
export class VesselRegisterComponent implements OnInit {
  public node: SitemapNode = sitemap.chartering.children.misc.children.vessels.children.id
    .children.registrations;
  public vesselId: VesselKey;

  constructor(
    private route: ActivatedRoute,
  ) { }

  public ngOnInit(): void {
    this.vesselId = +this.route.parent.snapshot.params.id;
  }

}
