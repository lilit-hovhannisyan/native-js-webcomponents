import { Injectable } from '@angular/core';
import { IBunker, IBunkerDTO, IBunkerCreate, BunkerKey } from '../../models/resources/IBunker';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class BunkerRepositoryService extends AbstractRepository<
  IBunker,
  IBunkerDTO,
  IBunkerCreate,
  RepoParams<IBunker>,
  BunkerKey
>  {
  protected resourceType = 'Bunker';
  protected sortKey = 'code';
  protected sortAsce = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/bunkers';

  protected postProcess = (item: IBunkerDTO): IBunker => item;
}
