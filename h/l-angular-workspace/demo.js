const x = [1,2,3];

const y = x.reduce( (result, item) => {
    result['ff' + item] = item;
    return result;
}, {});

const z = new Array(1,2,3,4)

const last = (arr) => {
    return arr[arr.length - 1]
}

Array.prototype._last = function () => {
    console.log(this)
}

Array.__last = (arr) => {
    return arr[arr.length - 1];
}

console.log (x);
console.log (JSON.stringify(y, {}, 2));
console.log (last(z));
console.log (z._last());
console.log ('last', Array.__last(z));