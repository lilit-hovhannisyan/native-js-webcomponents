import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from 'src/app/core/abstracts/abstract.repository';
import {
  IStevedoreDamageReport,
  IStevedoreDamageReportCreate,
  IStevedoreDamageReportDTO,
  StevedoreDamageReportKey
} from 'src/app/core/models/resources/IStevedoreDamageReport';
import { RepositoryService } from 'src/app/core/services/repository.service';
​

@Injectable({
  providedIn: 'root',
})
export class StevedoreDamageReportRepositoryService extends AbstractRepository<
  IStevedoreDamageReport,
  IStevedoreDamageReportDTO,
  IStevedoreDamageReportCreate,
  RepoParams<IStevedoreDamageReport>,
  StevedoreDamageReportKey
> {
  protected resourceType = 'StevedoreDamageReport';
  public sortAsce = true;
​
  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IStevedoreDamageReportDTO): IStevedoreDamageReport => item;
  public getBaseUrl = () => '/api/stevedore-damage-reports';
}
