import { KeyValue } from '@angular/common';
import { AbstractControl } from '@angular/forms';

export type MeInputErrorType = 'required' | 'min' | 'max' | 'minlength' | 'maxlength' | 'pattern' | 'unique';
export enum MeInputErrorTypeEnum {
  required = 'required',
  min = 'min',
  max = 'max',
  minlength = 'minlength',
  maxlength = 'maxlength',
  pattern = 'pattern',
  unique = 'unique'
}

export interface MeInputErrorI {
  formControl: AbstractControl;
  errors: KeyValue<MeInputErrorType, string>[];
}
