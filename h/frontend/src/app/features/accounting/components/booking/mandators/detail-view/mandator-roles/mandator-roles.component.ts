import { ActivatedRoute } from '@angular/router';
import { IMandatorRole } from '../../../../../../../core/models/resources/IMandatorRole';
import { MandatorRoleRepositoryService } from '../../../../../../../core/services/repositories/mandator-role.repository.service';
import { MandatorKey } from '../../../../../../../core/models/resources/IMandator';
import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { Language } from '../../../../../../../core/models/language';
import { RoleRepositoryService } from '../../../../../../../core/services/repositories/role-repository.service';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { IRole, RoleKey } from '../../../../../../../core/models/resources/IRole';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { orderBy } from 'lodash';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/mandator-roles.config';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  templateUrl: './mandator-roles.component.html',
  styleUrls: ['./mandator-roles.component.scss']
})
export class MandatorRolesComponent implements OnInit {

  public mandatorId: MandatorKey;
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IRole[]>;

  public editMode = false;
  private editMode$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private selectedRoles: RoleKey[] = [];

  constructor(
    private roleRepo: RoleRepositoryService,
    private mandatorRoleRepo: MandatorRoleRepositoryService,
    public gridConfigService: GridConfigService,
    public route: ActivatedRoute,
    private settingsLanguage: SettingsService
  ) { }

  public ngOnInit(): void {
    this.mandatorId = parseInt(this.route.parent.snapshot.params.id, 10);
    this.gridConfig = getGridConfig();

    const queryParams = { mandatorId: this.mandatorId };
    this.roleRepo.fetchAll({}).subscribe();
    this.mandatorRoleRepo.fetchAll({ queryParams }).subscribe();

    this.dataSource$ = combineLatest([
      this.roleRepo.getStream(),
      this.mandatorRoleRepo.getStream(),
      this.editMode$
    ]).pipe(map(([
      roles,
      mandatorRoles,
      editMode,
    ]) => {
      this.selectedRoles = mandatorRoles.map(mandatorRole => mandatorRole.roleId);

      return orderBy(roles
        .filter(role => editMode || this.roleIsAssignedToMandator(role, mandatorRoles))
        .map(role => ({
          ...role,
          _checked: editMode && this.roleIsAssignedToMandator(role, mandatorRoles)
        })),
        ['_checked', role => role[getLabelKeyByLanguage(this.settingsLanguage.language)].toLowerCase()], ['desc', 'asc']);
    }));
  }

  public roleIsAssignedToMandator = (role: IRole, mandatorRoles: IMandatorRole[]): boolean => {
    return !!mandatorRoles
      .filter(mandatorRole => mandatorRole.mandatorId === this.mandatorId)
      .find(mandatorRole => mandatorRole.roleId === role.id);
  }

  public enterEditMode() {
    this.editMode = true;
    this.editMode$.next(true);
    this.toggleGridSelectionType();
  }

  public cancelEditing() {
    this.editMode = false;
    this.editMode$.next(false);
    this.toggleGridSelectionType();
  }

  public toggleGridSelectionType = () => {
    this.gridConfig = {
      ...this.gridConfig,
      rowSelectionType: this.editMode
        ? NvGridRowSelectionType.Checkbox
        : undefined
    };
  }

  public submit() {
    this.mandatorRoleRepo.setRolesForMandator(this.selectedRoles, this.mandatorId)
      .subscribe(() => this.cancelEditing());
  }

  public rowSelectionChanged(roles: IRole[]) {
    this.selectedRoles = roles.map(role => role.id);
  }
}
