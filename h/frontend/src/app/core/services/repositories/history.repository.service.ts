import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IHistory, IHistoryDTO, IHistoryCreate, HistoryKey } from '../../models/resources/IHistory';
import { ResourceKey } from '../../models/resources/IResource';

export interface HistoryParams extends RepoParams<IHistory> {
  resourceKey: ResourceKey;
  entityName: string;
}

@Injectable({
  providedIn: 'root'
})
export class HistoryRepositoryService extends AbstractRepository<
IHistory,
IHistoryDTO,
IHistoryCreate,
HistoryParams,
HistoryKey
>  {
  protected resourceType = 'History';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IHistoryDTO): IHistory => item;

  public getBaseUrl(params: HistoryParams): string {
    return `/api/${params.entityName}/${params.resourceKey}/history`;
  }
}
