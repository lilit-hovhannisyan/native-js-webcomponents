import { Component} from '@angular/core';
import {videos} from '../../../../../shared-layer/l-hovhanni/dal/portfolio.model';
import {RouteManagementService} from 'l-builder';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.less']
})
export class VideosComponent {
  columns = 3;
  videos = videos;

  constructor(public routeManagementService: RouteManagementService) {
  }
}
