import {Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild} from '@angular/core';
import {ValidationService} from '../../services/validation.service';

@Component({
  selector: 'lavash-grid-container',
  templateUrl: './grid-container.component.html',
  styleUrls: ['./grid-container.component.less']
})
export class GridContainerComponent implements OnInit, OnChanges {
    @Input() columns = 4;
    @Input() items = [];
    @Output() itemClickEvent = new EventEmitter();
    @Output() itemHoverEvent = new EventEmitter();
    @Output() cardBtnClickEvent = new EventEmitter();
    @Output() windowResizeEvent = new EventEmitter();
    @Output() pageScrollEvent = new EventEmitter();
    @Output() imageLoadEvent = new EventEmitter();

    columnsArr;
    itemsPerColumn = [];

    @Input() articleImgPath = '';
    @Input() imageImgPath = '';
    @Input() videoImgPath = '';
    @Input() videoPosterPath = '';

    dynamicItemStyles = {
        top: 0,
        left: 0,
        width: 0,
        height: 0,
        opacity: 0,
        pointerEvents: 'none'
    };
    dynamicItemData;


    @ViewChild('gridContainer')
    gridContainer: ElementRef;

    constructor(private validationService: ValidationService) {}

    ngOnInit() {
        const top = this.gridContainer.nativeElement.getBoundingClientRect().top;
        const left = this.gridContainer.nativeElement.getBoundingClientRect().left;
        const width = this.gridContainer.nativeElement.getBoundingClientRect().width;

        this.dynamicItemStyles.top = top;
        this.dynamicItemStyles.left = left;
        this.dynamicItemStyles.width = width / this.columns;

        this.onResize({windowWidth: window.innerWidth});
    }

    ngOnChanges() {
        this.itemsPerColumn = [];
        this.columnsArr = [];

        if ( this.items.length >= 1 && this.items.length < this.columns ) {
            this.generateColumns();
            this.columnsArr.map( (col, index) => {
                if (this.items[index]) {
                    this.itemsPerColumn.push([this.items[index]]);
                }
            });
            return;
        }

        this.generateColumns(true);
    }

    generateColumns(constructNewArray?: boolean) {
        let counter = 1;
        let iterationCount = this.columns;
        while (iterationCount > 0 ) {
            this.columnsArr.push(counter);

            if (constructNewArray) {
                this.itemsPerColumn.push(this.getItemsPerColumns(this.items, counter - 1));
            }

            iterationCount--;
            counter++;
        }
    }

    getItemsPerColumns(arr, colIndex) {
        const itemsPerCol =  Math.round(arr.length / this.columns);


        const start = colIndex * itemsPerCol;
        let end = start + itemsPerCol;

        const rest = arr.length - end;

        // todo use this logic OR the comented one underneeth
        // end = rest < itemsPerCol || colIndex === this.columns - 1 ? end + rest : end; // todo bacel es logicy ete if-erov marazmy traqi
        const group = arr.slice(start,  end);
        if (rest < itemsPerCol || colIndex === this.columns - 1) {
            if (rest > 1) {
                arr.slice(end).map( (i, index) => {

                    this.itemsPerColumn[index] ?  this.itemsPerColumn[index].push(i) :
                        group.push(i);
                });
            } else {
                end = end + rest;
            }
        }

        return group;
    }

    defineSourceType(_type, src) {
        if ( _type) {
            return _type;
        }

        const type = this.validationService.validationRules.fileFormatImage(src) ? 'img' :
            this.validationService.validationRules.fileFormatVideo(src) ? 'video' : 'card';

        return type;
    }

    onItemClick(item) {
       this.itemClickEvent.emit(item);
    }

    onCardBtnClick(cardItem) {
        this.cardBtnClickEvent.emit(cardItem);
    }

    onItemMouseenter(item, overElement) {
        const animate = () => {
            this.dynamicItemStyles.top = overElement.getBoundingClientRect().top;
            this.dynamicItemStyles.left = overElement.getBoundingClientRect().left;
            this.dynamicItemStyles.width = overElement.getBoundingClientRect().width;
            this.dynamicItemStyles.height = overElement.getBoundingClientRect().height;
            this.dynamicItemStyles.opacity = 1;
            this.dynamicItemStyles.pointerEvents = 'all';

            this.dynamicItemData = item;
        };

        animate();

        this.itemHoverEvent.emit({
            dynamicItemData: this.dynamicItemData,
            type: this.defineSourceType(this.dynamicItemData.type, this.dynamicItemData.src)
        });
    }

    onItemMouseleave() {
        this.dynamicItemStyles['opacity'] = 0;
        this.dynamicItemStyles['pointerEvents'] = 'none';
        this.dynamicItemData = null;
    }

    onscroll($event) {
        if (Object.keys(this.dynamicItemStyles).length) {
            this.onItemMouseleave();
        }
    }

    onResize(event = {windowWidth: 801}) {
        if (event.windowWidth <= 600) {
            this.columns = 1;
        } else if (event.windowWidth <= 800) {
            this.columns = 2;
        } else {
            this.columns = 4;
        }

        this.windowResizeEvent.emit({
           windowWidth:  event.windowWidth
        });
    }

    onImageLoad(event) {
        this.imageLoadEvent.emit(event);
    }
}
