import { NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';
import { getContextMenuButtonsConfig } from '../../helpers/general';
import { wordingGeneral } from '../wording/general';
import { Operations } from '../../models/Operations';

export const getCostUnitCategoriesGridConfig: (
  actions: IGridConfigActions,
  canPerform: (operations: Operations) => boolean,
) => NvGridConfig = (
  actions: IGridConfigActions,
  canPerform: (operations: Operations) => boolean,
): NvGridConfig => {
  return ({
    gridName: 'costUnitCategoriesGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    rowSelectionType: actions ? null : NvGridRowSelectionType.RadioButton,
    hideRefreshButton: true,
    isSortAscending: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 50
    },
    columns: [
      {
        key: 'labelEn',
        title: wordingGeneral.nameEn,
        width: 250,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'labelDe',
        title: wordingGeneral.nameDe,
        width: 250,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: actions
      ? [
          {
            icon: 'edit',
            description: wordingGeneral.edit,
            name: 'editCostUnitCategory',
            tooltip: wordingGeneral.edit,
            actOnDoubleClick: true,
            func: actions.edit,
            hidden: !canPerform(Operations.UPDATE)
          },
          {
            icon: 'delete',
            description: wordingGeneral.delete,
            name: 'deleteCostUnitCategory',
            tooltip: wordingGeneral.delete,
            actOnDoubleClick: false,
            func: actions.delete,
            hidden: !canPerform(Operations.DELETE),
          }
        ]
      : [],
    toolbarButtons: actions
      ? [
          {
            title: wordingGeneral.create,
            tooltip: wordingGeneral.create,
            icon: 'plus-circle-o',
            func: actions.add,
            hidden: () => !canPerform(Operations.CREATE)
          }
        ]
      : [],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};

