import { MandatorKey } from './IMandator';
import { UserKey } from './IUser';
import { IRadioSelectItem } from 'src/app/shared/components/radio-select/radio-select.component';
import { IResource } from './IResource';
import { IWithSystemFlag } from './IWithSystemFlag';

export type PermissionKey = number;

export interface IPermissionGroupUserMandatorConnection extends IResource<PermissionKey> {
  mandatorId: MandatorKey;
  userId: UserKey;
}

export interface IPermissionGroup<T extends IPermissionGroupUserMandatorConnection>
extends IResource<PermissionKey>, IWithSystemFlag {
  name: string;
  connections: T[];
}

export interface IPermissionsGroupsMandatorsUsersLists {
  usersIds: UserKey[];
  mandatorsIds: MandatorKey[];
}

export interface IPermissionRadioSelectItem<P extends IPermissionGroupUserMandatorConnection> extends
  IPermissionGroup<P>,
  IRadioSelectItem<PermissionKey> {}
