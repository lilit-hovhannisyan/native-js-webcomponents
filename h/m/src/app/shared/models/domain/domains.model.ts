import { Type } from 'class-transformer';

import { DomainModel } from './domain.model';
import { EnumModel } from '../enum/enum.model';

export class DomainsModel {
  @Type(() => DomainModel)
  domains: DomainModel[] = [];
  @Type(() => EnumModel)
  enums: EnumModel[] = [];
}
