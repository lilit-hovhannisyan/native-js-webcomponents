import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeCoverImageUploadComponent } from './me-cover-image-upload.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MeButtonModule } from '@shared/me-components/me-button/me-button.module';
import { MeDragDropModule } from '@shared/me-directives/me-drag-drop/me-drag-drop.module';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';

@NgModule({
  declarations: [MeCoverImageUploadComponent],
  imports: [CommonModule, MatProgressBarModule, MeButtonModule, MeDragDropModule, MeIconsProviderModule],
  exports: [MeCoverImageUploadComponent]
})
export class MeCoverImageUploadModule {}
