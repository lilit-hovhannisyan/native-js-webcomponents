import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'me-menu',
  templateUrl: './me-menu.component.html',
  styleUrls: ['./me-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeMenuComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
