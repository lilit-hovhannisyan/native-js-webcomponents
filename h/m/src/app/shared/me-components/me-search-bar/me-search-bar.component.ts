import { BaseLoggerComponent } from './../../../features/tracking-system/base-logger.component';
import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, Output } from '@angular/core';

import { MeSearchBarI } from './me-search-bar.interface';
import { MeButtonI } from '../me-button/me-button.interface';

@Component({
  selector: 'me-search-bar',
  templateUrl: './me-search-bar.component.html',
  styleUrls: ['./me-search-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeSearchBarComponent extends BaseLoggerComponent {
  componentVersion: string = '0.0.1';
  @Input() dataModel!: MeSearchBarI;

  @Output() searchBarActions: EventEmitter<{ eventName: string; payload?: string | boolean }> = new EventEmitter();
  @Output() searchInput: EventEmitter<string> = new EventEmitter();
  @Output() sortBy: EventEmitter<string> = new EventEmitter();
  @Output() selectedUsersGroups: EventEmitter<string> = new EventEmitter();
  @Output() toggleSelect: EventEmitter<void> = new EventEmitter();
  @Output() changeViewType: EventEmitter<void> = new EventEmitter();
  @Output() addButton: EventEmitter<void> = new EventEmitter();

  noBorderButton: MeButtonI = {
    type: 'regular',
    color: 'link',
    rounded: false
  };

  primaryButton: MeButtonI = {
    color: 'primary',
    type: 'regular',
    rounded: false
  };

  constructor(injector: Injector) {
    super(injector);
  }

  customEvent(eventName: string, payload?: string | boolean): void {
    this.searchBarActions.emit({ eventName, payload });
  }

  searchInputEvent(searchValue: string): void {
    this.searchInput.emit(searchValue);
  }

  sortByEvent(sortByValue: string): void {
    this.sortBy.emit(sortByValue);
  }

  selectUsersGroupsEvent(selectedValue: string): void {
    this.selectedUsersGroups.emit(selectedValue);
  }

  toggleSelectEvent(): void {
    this.toggleSelect.emit();
  }

  changeViewTypeEvent(): void {
    this.changeViewType.emit();
  }

  addButtonEvent(): void {
    this.addButton.emit();
  }
}
