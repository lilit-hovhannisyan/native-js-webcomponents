import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

// interface

@Component({
  selector: 'lavash-text',
  templateUrl: './crossing-text.component.html',
  styleUrls: ['./crossing-text.component.less']
})
export class CrossingTextComponent implements OnInit {
  @Input() textBlocksMap: any;
  textBlocksCount = 2;
  @Input() spliter = '!ee!';

  wrapperContainerType = 'flex flex-wrap col-12';

  ngOnInit() {
     this.textBlocksCount = Object.keys(this.textBlocksMap).length;

     if (this.textBlocksCount < 2) {
         console.error('L-builder-error: textBlocksCount must have at least 2 properties.');
     } else {

     }
  }
}
