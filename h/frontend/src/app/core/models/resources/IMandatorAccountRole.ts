import { IResource } from './IResource';
import { RoleKey } from './IRole';
import { MandatorAccountKey } from './IMandatorAccount';

export type MandatorAccountRoleKey = number;

interface ISharedProps {
  mandatorAccountId: MandatorAccountKey;
  roleId: RoleKey;
}

export interface IMandatorAccountRoleBase extends ISharedProps, IResource<MandatorAccountRoleKey> { }

export interface IMandatorAccountRoleCreate extends ISharedProps { }

export interface IMandatorAccountRoleDTO extends IMandatorAccountRoleBase { }

export interface IMandatorAccountRole extends IMandatorAccountRoleDTO { }
