import { KeyValueI } from '@core-interfaces/key-value.interface';

export interface CollectionEntityI {
  domainName: string;
  attributes: KeyValueI<string | number | boolean | string[]>;
}
