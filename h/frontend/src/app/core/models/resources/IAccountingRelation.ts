import { BankKey } from './IBank';
import { BankAccountKey, IBankAccountRelation } from './IBankAccount';
import { BookingAccountKey } from './IBookingAccount';
import { MandatorKey } from './IMandator';
import { IResource } from './IResource';

export type AccountingRelationKey = number;

interface ISharedProps {
  bankAccountId: BankAccountKey;
  bookingAccountId: BookingAccountKey;
  bankAccountNo: string;
  mandatorId: MandatorKey;
}

export interface IAccountingRelationBase extends ISharedProps, IResource<AccountingRelationKey> { }

export interface IAccountingRelationCreate extends ISharedProps { }

export interface IAccountingRelationDTO extends IAccountingRelationBase { }

export interface IAccountingRelation extends IAccountingRelationDTO { }


