export enum WidgetType {
  Clock = 0,
  Weather = 1,
  Bookmark = 2,
  Todo = 3,
  Currency = 4
}
