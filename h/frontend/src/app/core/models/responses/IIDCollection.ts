import { IResponseBody } from './IResponseBody';

export interface IIdCollection<T> extends IResponseBody {
  items: T[];
  count: number;
}
