import { IResource } from './IResource';

export type TimeCharterSplitRateKey = number;

interface ISharedProps {
  from: string;
  until: string;
  charterRate: number;
  timeCharterPeriodId: number;
}

export interface ITimeCharterSplitRateBase extends ISharedProps, IResource<TimeCharterSplitRateKey> { }

export interface ITimeCharterSplitRateCreate extends ISharedProps { }

export interface ITimeCharterSplitRateDTO extends ITimeCharterSplitRateBase { }

export interface ITimeCharterSplitRate extends ITimeCharterSplitRateDTO { }
