import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  EventEmitter,
  Output
} from '@angular/core';
import { SelectType } from 'src/app/core/models/SelectType';
import { wording } from 'src/app/core/constants/wording/wording';


@Component({
  selector: 'nv-list-select',
  templateUrl: './list-select.component.html',
  styleUrls: ['./list-select.component.scss']
})

export class ListSelectComponent implements OnInit, OnChanges {
  @Input() public resource: any[] = [];
  @Input() public properties: any[] = [];
  @Input() public selectType: SelectType = SelectType.Checkbox;
  @Input() public allChecked = false;
  @Input() public getLabel: (value: any) => any;
  @Input() public propertyToReturn: string;

  @Output() public selectedId = new EventEmitter<number>();
  @Output() public checkedIds = new EventEmitter<number[]>();
  @Output() public checkedAll = new EventEmitter<boolean>();

  public SelectType = SelectType;
  public original: any[] = [];
  public checkedIdsList: number[];
  public inputValue: string;
  public indeterminate = false;
  public selectedItemId: number;
  public wording = wording;
  constructor() { }

  public ngOnInit() { }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.resource) {
      this.original = this.resource;
    }
  }

  public onItemChecked() {
    this.updateStatus();
  }

  public filter() {
    if (this.inputValue) {
      const value = this.inputValue
        .toString()
        .toLowerCase();

      this.resource = this.original
        .filter(item => {
          const firstPropertyValue = this.getLabel
            ? this.getLabel(item[this.properties[0]])
            : item[this.properties[0]];
          return firstPropertyValue
            .toString()
            .toLowerCase()
            .toLowerCase().includes(value)
            || item[this.properties[1]]
              .toString()
              .toLowerCase().includes(value);
        });
    } else {
      this.resource = this.original;
    }
    this.updateStatus();
  }

  public updateAllChecked(value: boolean) {
    this.indeterminate = false;
    this.resource.forEach(item => item['checked'] = value);
    this.filter();
  }

  private updateStatus() {
    if (this.resource.every(item => !item.checked)) {
      this.allChecked = false;
      this.indeterminate = false;
    } else if (this.resource.every(item => item.checked)) {
      this.allChecked = true;
      this.indeterminate = false;
    } else {
      this.indeterminate = true;
    }
  }

  public onItemSelected(item: any) {
    this.selectedId.emit(
      this.propertyToReturn
        ? {
          id: item.id,
          [this.propertyToReturn]: item[this.propertyToReturn]
        }
        : item.id
    );
  }
}
