export interface MePopupStatusChangeI {
  title?: string;
  currentKeyValue?: string;
  radioButtons?: {
    key: string;
    value: string;
    disabled?: boolean;
  }[];
  cancelButtonPlaceholder?: string;
  changeButtonPlaceholder?: string;
}
