import { Component, ChangeDetectionStrategy } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-vertical-list',
  templateUrl: './nv-vertical-list.component.html',
  styleUrls: ['./nv-vertical-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NvVerticalListComponent {
  public wording = wording;
}
