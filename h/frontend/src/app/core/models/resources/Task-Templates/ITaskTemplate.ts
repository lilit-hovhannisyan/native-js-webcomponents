import { IResource } from '../IResource';
import { TaskLanguageType } from '../IVesselTask';
import { IWithLabels } from '../IWithLabels';
import { TaskTemplateCategoryKey } from './ITaskTemplateCategory';
import { ITaskTemplateTodoItem } from './ITaskTemplateTodoItem';

export type TaskTemplateKey = number;

interface ISharedProps extends IWithLabels {
  /**
   * extends
   * labelDe: string;
   * labelEn: string;
   */
  categoryId: TaskTemplateCategoryKey;
  useStartDate: boolean;
  color: string; // example: #fff
  isSystemDefault: boolean;
  todos: ITaskTemplateTodoItem[];
  languageType: TaskLanguageType;
}

export interface ITaskTemplateCreate extends ISharedProps { }

export interface ITaskTemplateBase extends ISharedProps, IResource<TaskTemplateKey> { }

export interface ITaskTemplateDTO extends ITaskTemplateBase { }

export interface ITaskTemplate extends ITaskTemplateDTO { }

