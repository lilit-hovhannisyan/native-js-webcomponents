import { Component, Injectable, TemplateRef } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RightSidebarSupportService {
  private readonly _rightSidebarContent = new BehaviorSubject<TemplateRef<Component> | null>(null);
  readonly rightSidebarContent$ = this._rightSidebarContent.asObservable();

  constructor(private _router: Router) {
    this._router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.rightSidebarContent = null;
      }
    });
  }

  get rightSidebarContent(): TemplateRef<Component> | null {
    return this._rightSidebarContent.getValue();
  }

  set rightSidebarContent(data: TemplateRef<Component> | null) {
    this._rightSidebarContent.next(data);
  }
}
