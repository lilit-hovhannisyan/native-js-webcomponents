import { Injectable } from '@angular/core';
import { Observable, forkJoin, combineLatest, of } from 'rxjs';
import { map, switchMap, startWith } from 'rxjs/operators';
import { ICompany, CompanyKey } from '../../models/resources/ICompany';
import { MandatorRepositoryService } from '../repositories/mandator-repository.service';
import { CurrencyRepositoryService } from '../repositories/currency-repository.service';
import { IMandator, IMandatorFull, IMandatorRow, MandatorKey, IMandatorLookup, IMandatorLookupFull, IMandatorLookupRow } from '../../models/resources/IMandator';
import { ICurrency, CurrencyKey } from '../../models/resources/ICurrency';
import { CompanyRepositoryService } from '../repositories/company-repository.service';
import { sortByKey } from '../../helpers/general';
import { ACCOUNTING_TYPES } from '../../models/enums/accounting-types';
import { MandatorTypes } from '../../models/resources/MandatorTypes';
import { MandatorClosedYearsRepositoryService } from '../repositories/mandator-closed-year-repository.service';
import { IMandatorClosedBookingYear, MandatorClosedBookingYearKey } from '../../models/resources/IMandatorClosedBookingYear';
import { MandatorBlockedYearsRepositoryService } from '../repositories/mandator-blocked-years-repository.service';
import { IMandatorBlockedBookingYear } from '../../models/resources/IMandatorBlockedBookingYear';
import { ISelectOption } from '../../models/misc/selectOption';
import { wording } from '../../constants/wording/wording';
import { RepoParams } from '../../abstracts/abstract.repository';
import { IMandatorRole } from '../../models/resources/IMandatorRole';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { MandatorRoleRepositoryService } from '../repositories/mandator-role.repository.service';
import { sortBy } from 'lodash';
import { getCompanyLegalForm, COMPANY_LEGAL_FORM } from '../../models/enums/company-legal-form';
import { IWording } from '../../models/resources/IWording';
import { convertToMap } from 'src/app/shared/helpers/general';

export const mandatorTypesOptions: ISelectOption[] = [
  { value: MandatorTypes.parentCompany, displayLabel: wording.accounting.mandatorTypeParentCompany },
  { value: MandatorTypes.vesselCompany, displayLabel: wording.accounting.mandatorTypeVesselCompany },
  { value: MandatorTypes.multivesselCompany, displayLabel: wording.accounting.mandatorTypeMultivesselCompany },
  { value: MandatorTypes.administrativeCompany, displayLabel: wording.accounting.mandatorTypeAdministrativeCompany },
];

@Injectable({
  providedIn: 'root'
})
export class MandatorMainService {
  constructor(
    private mandatorRepo: MandatorRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private mandatorClosedYearRepo: MandatorClosedYearsRepositoryService,
    private mandatorBlockedYearRepo: MandatorBlockedYearsRepositoryService,
    private authService: AuthenticationService,
    private mandatorRoleRepo: MandatorRoleRepositoryService,
  ) { }

  public fetchAll(params: RepoParams<IMandator> = {}): Observable<IMandatorFull[]> {
    return forkJoin([
      this.mandatorRepo.fetchAll(params),
      this.companyRepo.fetchAll({}),
      this.currencyRepo.fetchAll({ useCache: false })
    ]).pipe(
      switchMap(([mandators, companies, currencies]) =>
        combineLatest([
          this.fetchMandatorLastClosedYear(mandators).pipe(
            map(lastClosedYears => convertToMap<
              IMandatorClosedBookingYear,
              MandatorKey
            >(lastClosedYears, 'mandatorId')),
          ),
          this.fetchMandatorClosedYears(mandators),
          this.fetchMandatorBlockedYears(mandators),
        ]).pipe(map(response => ({ mandators, companies, currencies, response })))
      ),
      map(data =>
        this.getMandatorsFullList(
          data.mandators,
          convertToMap<ICompany, CompanyKey>(data.companies),
          convertToMap<ICurrency, CurrencyKey>(data.currencies),
          ...data.response,
        )
      ),
      map(res => res.sort(sortByKey('name', true))),
    );
  }

  public fetchOne(id: MandatorKey, params: RepoParams<IMandator>): Observable<IMandatorFull> {
    return this.composeMandator(
      this.mandatorRepo.fetchOne(id, params),
      this.companyRepo.fetchAll({})
        .pipe(map(companies => convertToMap<ICompany, CompanyKey>(companies))),
      this.currencyRepo.fetchAll({ useCache: false })
        .pipe(map(currencies => convertToMap<ICurrency, CurrencyKey>(currencies))),
      this.mandatorClosedYearRepo.fetchByMandators([id], true).pipe(
        map(lastClosedYears => convertToMap<
          IMandatorClosedBookingYear,
          MandatorKey
        >(lastClosedYears, 'mandatorId')),
      ), // get LastClosedYear
      this.mandatorClosedYearRepo.fetchByMandators([id]), // get all ClosedYears
      this.mandatorBlockedYearRepo.fetchAllOfMandators([id]),
    );
  }

  private getMandatorsFullList(
    mandators: IMandator[],
    companies: Map<CompanyKey, ICompany>,
    currencies: Map<CurrencyKey, ICurrency>,
    lastClosedYears: Map<MandatorKey, IMandatorClosedBookingYear>,
    closedYears: IMandatorClosedBookingYear[],
    blockedYears: IMandatorBlockedBookingYear[],
  ): IMandatorFull[] {
    return mandators.map(mandator => {
      return this.getMandatorsFull(
        mandator,
        companies,
        currencies,
        lastClosedYears,
        closedYears,
        blockedYears,
      );
    });
  }

  private getMandatorsFull(
    mandator: IMandator,
    companies: Map<CompanyKey, ICompany>,
    currencies: Map<CurrencyKey, ICurrency>,
    lastClosedYears: Map<MandatorKey, IMandatorClosedBookingYear>,
    closedYears: IMandatorClosedBookingYear[],
    blockedYears: IMandatorBlockedBookingYear[],
  ): IMandatorFull {
    const foundCompany = companies.get(mandator.companyId);
    const foundCurrency = currencies.get(mandator.currencyId);
    const foundLastClosedYear = lastClosedYears.get(mandator.id);
    const mandatorClosedYears = closedYears.filter(closedYear => closedYear.mandatorId === mandator.id);
    const mandatorBlockedYears = blockedYears.filter(blockedYear => blockedYear.mandatorId === mandator.id);
    return {
      ...mandator,
      company: foundCompany,
      currency: foundCurrency,
      lastClosedYear: foundLastClosedYear,
      closedYears: mandatorClosedYears,
      blockedYears: mandatorBlockedYears,
      lastClosedYearView: foundLastClosedYear && foundLastClosedYear.year,
    };
  }

  private toRow(
    mandator: IMandatorFull | IMandatorLookupFull,
    companyLegalFormMap: Map<COMPANY_LEGAL_FORM, IWording>
  ): IMandatorRow | IMandatorLookupRow {
    const { company, currency } = mandator;
    const mandatorOption = mandatorTypesOptions.find(opt => opt.value === mandator.mandatorType);
    const mandatorTypeName = mandatorOption ? mandatorOption.displayLabel : null;
    const legalFormWording: IWording = companyLegalFormMap.get(company.legalForm);

    return ({
      ...mandator,
      companyName: company && company.companyName,
      currencyName: currency && currency.displayLabel,
      hgb: Boolean(mandator.accountingType & ACCOUNTING_TYPES.HGB),
      ifrs: Boolean(mandator.accountingType & ACCOUNTING_TYPES.IFRS),
      usgaap: Boolean(mandator.accountingType & ACCOUNTING_TYPES.USGAAP),
      eBalance: Boolean(mandator.accountingType & ACCOUNTING_TYPES.EBalance),
      isoCode: currency && currency.isoCode,
      mandatorTypeName,
      lastClosedYearView: mandator.lastClosedYear && mandator.lastClosedYear.year,
      lastClosedPeriod: mandator.lastClosedYear && mandator.lastClosedYear.period,
      legalFormWording,
    });
  }

  public toRows(mandators: IMandatorFull[]): IMandatorRow[] {
    const legalForm: Map<COMPANY_LEGAL_FORM, IWording> = this.getLegalForm();
    return mandators.map(mandator => this.toRow(mandator, legalForm)) as IMandatorRow[];
  }

  private getLegalForm(): Map<COMPANY_LEGAL_FORM, IWording> {
    const companyLegalForms: ISelectOption<COMPANY_LEGAL_FORM>[] = getCompanyLegalForm();
    const companyLegalFormMap: Map<COMPANY_LEGAL_FORM, IWording> = new Map([]);
    companyLegalForms.forEach(c => companyLegalFormMap.set(c.value, c.displayLabel));
    return companyLegalFormMap;
  }

  private composeMandator(
    mandator$: Observable<IMandator>,
    companies$: Observable<Map<CompanyKey, ICompany>>,
    currencies$: Observable<Map<CurrencyKey, ICurrency>>,
    lastClosedYears$: Observable<Map<MandatorKey, IMandatorClosedBookingYear>>,
    closedYears$: Observable<IMandatorClosedBookingYear[]>,
    blockedYears$: Observable<IMandatorBlockedBookingYear[]>,
  ): Observable<IMandatorFull> {
    return forkJoin([
      mandator$,
      companies$,
      currencies$,
      lastClosedYears$,
      closedYears$,
      blockedYears$,
    ]).pipe(map(res => this.getMandatorsFull(...res)));
  }

  public getStream(): Observable<IMandatorFull[]> {
    return forkJoin([
      this.mandatorRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.currencyRepo.fetchAll({ useCache: false }),
    ]).pipe(
      switchMap(() => combineLatest([
        this.mandatorRepo.getStream(),
        this.companyRepo.getStream()
          .pipe(map(companies => convertToMap<ICompany, CompanyKey>(companies))),
        this.currencyRepo.getStream()
          .pipe(map(currencies => convertToMap<ICurrency, CurrencyKey>(currencies))),
        this.mandatorRepo.getStream()
          .pipe(
            switchMap(mandators => this.fetchMandatorLastClosedYear(mandators)),
            map(lastClosedYears => convertToMap<
              IMandatorClosedBookingYear,
              MandatorKey
            >(lastClosedYears, 'mandatorId')),
          ),
        this.mandatorRepo.getStream()
          .pipe(switchMap(mandators => this.fetchMandatorClosedYears(mandators))),
        this.mandatorRepo.getLookupStream()
          .pipe(switchMap(mandators => this.mandatorBlockedYearRepo
          .fetchAllWithUsers(mandators.map(m => m.id), false))),
      ])
      ),
      map(response => this.getMandatorsFullList(...response))
    );
  }

  public getLookupStream(
    filterParams?: Partial<IMandatorLookup>
  ): Observable<IMandatorLookupRow[]> {
    const legalForm: Map<COMPANY_LEGAL_FORM, IWording> = this.getLegalForm();

    return forkJoin([
      this.mandatorRepo.fetchLookups({}),
      this.companyRepo.fetchAll({}),
      this.currencyRepo.fetchAll({ useCache: false }),
    ]).pipe(
      switchMap(() => combineLatest([
        this.mandatorRepo.getLookupStream(filterParams),
        this.companyRepo.getStream()
          .pipe(map(companies => convertToMap<ICompany, CompanyKey>(companies))),
        this.currencyRepo.getStream()
          .pipe(map(currencies => convertToMap<ICurrency, CurrencyKey>(currencies))),
        this.mandatorRepo.getStream()
          .pipe(
            switchMap(mandators => this.fetchMandatorLastClosedYear(mandators)),
            map(lastClosedYears => convertToMap<
              IMandatorClosedBookingYear,
              MandatorKey
            >(lastClosedYears, 'mandatorId')),
          ),
      ])),
      map(res => this.getMandatorsLookupFullList(...res)),
      map(mandators => mandators.map(m => this.toRow(m, legalForm))),
       // TODO: sometimes this returns Observable<null> at the beginning,
       // investigate the reason and remove startWith if possible
      startWith([]),
    );
  }

  private getMandatorsLookupFullList(
    mandators: IMandatorLookup[],
    companies: Map<CompanyKey, ICompany>,
    currencies: Map<CurrencyKey, ICurrency>,
    lastClosedYears: Map<MandatorClosedBookingYearKey, IMandatorClosedBookingYear>,
  ): IMandatorLookupFull[] {
    return mandators.map(mandator => {
      const foundCompany = companies.get(mandator.companyId);
      const foundCurrency = currencies.get(mandator.currencyId);
      const foundLastClosedYear = lastClosedYears.get(mandator.id);
      return {
        ...mandator,
        company: foundCompany,
        currency: foundCurrency,
        lastClosedYear: foundLastClosedYear,
        lastClosedYearView: foundLastClosedYear && foundLastClosedYear.year,
      };
    });
  }

  private fetchMandatorLastClosedYear(
    mandators: IMandator[]
  ): Observable<IMandatorClosedBookingYear[]> {
    const mandatorIds = mandators.map(mandator => mandator.id);
    return this.mandatorClosedYearRepo.fetchByMandators(mandatorIds, true);
  }

  private fetchMandatorClosedYears(
    mandators: IMandator[]
  ): Observable<IMandatorClosedBookingYear[]> {
    const mandatorIds = mandators.map(mandator => mandator.id);
    return this.mandatorClosedYearRepo.fetchByMandators(mandatorIds, false);
  }

  private fetchMandatorBlockedYears(
    mandators: IMandator[]
  ): Observable<IMandatorBlockedBookingYear[]> {
    const mandatorIds = mandators.map(mandator => mandator.id);
    return this.mandatorBlockedYearRepo.fetchAllWithUsers(mandatorIds, false);
  }

  /**
   * Checks if user has permission(Role) for a mandator
   */
  public mandatorIsAssignedToRole = (
    mandatorId: MandatorKey,
    mandatorRoles: IMandatorRole[]
  ): boolean => {
    const user = this.authService.getUser();
    const isSuperAdmin = user.isSuperAdmin;
    const isAdmin = user.isAdmin;
    const userRoles: number[] = this.authService.getRolesIds();
    return (isSuperAdmin || isAdmin)
      || !!mandatorRoles
        .filter(mandatorRole => userRoles.includes(mandatorRole.roleId))
        .find(mandatorRole => mandatorRole.mandatorId === mandatorId);
  }

  public fetchLookupsWithRolesOnly(
    params: RepoParams<IMandator> = {},
  ): Observable<IMandatorLookupRow[]>   {
    return this.fetchWithRolesOnly(true, params);
  }

  public fetchMandatorsWithRolesOnly(
    params: RepoParams<IMandator> = {},
  ): Observable<IMandatorRow[]> {
    return this.fetchWithRolesOnly(false, params) as Observable<IMandatorRow[]>;
  }

  private fetchWithRolesOnly(
    shouldFetchLookups: boolean,
    params: RepoParams<IMandator> = {},
  ): Observable<IMandatorLookupRow[] | IMandatorRow[]> {
    const legalForm: Map<COMPANY_LEGAL_FORM, IWording> = this.getLegalForm();
    const mandators$ = shouldFetchLookups
      ? this.mandatorRepo.fetchLookups(params)
      : this.mandatorRepo.fetchAll(params);

    return forkJoin([
      this.mandatorRoleRepo.fetchAll({}),
      mandators$,
      this.companyRepo.fetchAll({}),
      this.currencyRepo.fetchAll({ useCache: false }),
    ]).pipe(
        switchMap(([mandatorRoles, mandators, companies, currencies]) => {
          return forkJoin([
            of(mandatorRoles),
            of(mandators),
            of(convertToMap<ICompany, CompanyKey>(companies)),
            of(convertToMap<ICurrency, CurrencyKey>(currencies)),
            this.mandatorClosedYearRepo
              .fetchByMandators(mandators.map(m => m.id), false)
              .pipe(
                map(lastClosedYears => convertToMap<
                  IMandatorClosedBookingYear,
                  MandatorKey
                >(lastClosedYears, 'mandatorId')),
              ), // get LastClosedYear
          ]);
        }),
        map(([roles, ...res]) =>
          sortBy(
            this.getMandatorsLookupFullList(...res)
              .filter(m => this.mandatorIsAssignedToRole(m.id, roles)),
            ['no']
          )
        ),
        map(mandators => mandators.map(m => this.toRow(m, legalForm))),
        startWith([]),
      );
  }
}
