import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IVesselAccountingListItem } from 'src/app/core/models/resources/IVesselAccounting';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup } from '@angular/forms';
import { IWording } from 'src/app/core/models/resources/IWording';
import { getVesselRegisterAutocompleteGridConfig } from 'src/app/core/constants/nv-grid-configs/vessel-register-autocomplete.config';
import { VesselRegisterRepositoryService } from 'src/app/core/services/repositories/vessel-registeries-repository.service';
import { IVesselRegister } from 'src/app/core/models/resources/IVesselRegister';
import { SettingsService } from 'src/app/core/services/settings.service';
import { NvGridConfig } from 'nv-grid';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { dateFormatTypes } from 'src/app/core/models/dateFormat';

/** Same IVesselRegister with additional helper properties. */
interface IRegisterFull extends IVesselRegister {
  /** Same `from` only with system-selected date-format. */
  formattedFrom?: string;
  /** Same `until` only with system-selected date-format. */
  formattedUntil?: string;
  /** Formation formula: `${formattedFrom} - ${formattedFrom || open}`. */
  fromUntil?: string;
}

@Component({
  selector: 'nv-vessel-register-autocomplete',
  templateUrl: './vessel-register-autocomplete.component.html',
  styleUrls: ['./vessel-register-autocomplete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VesselRegisterAutocompleteComponent implements OnInit, OnChanges {
  public wording = wording;
  public gridConfig: NvGridConfig = getVesselRegisterAutocompleteGridConfig();

  @Input() public form: FormGroup;
  @Input() public controlName = 'vesselRegistrationId';
  @Input() public label: IWording = wording.chartering.registrationPeriod;
  @Input() private dataSource$: Observable<IVesselRegister[]> = this.vesselRegisterRepo.getStream();
  @Input() public fetchResource = true;
  @Output() public select = new EventEmitter<IVesselAccountingListItem>();

  /** Same `dataSource$` mapped to IRegisterFull. */
  public formattedDataSource$: Observable<IRegisterFull[]>;

  constructor(
    private vesselRegisterRepo: VesselRegisterRepositoryService,
    private settingsService: SettingsService,
  ) { }

  public ngOnInit(): void {
    if (this.fetchResource) {
      this.vesselRegisterRepo.fetchAll({}).subscribe();
    }
    this.setFormattedData();
  }

  public ngOnChanges({dataSource$}: SimpleChanges): void {
    if (dataSource$) {
      this.setFormattedData();
    }
  }

  /** Sets `formattedDataSource$` value. */
  private setFormattedData(): void {
    if (!this.dataSource$) {
      this.formattedDataSource$ = of([]);
      return;
    }

    this.formattedDataSource$ = this.dataSource$.pipe(
      map(vesselRegisters => vesselRegisters.map(register => {
        const formattedFrom: string = this.formatDate(register.from);
        const formattedUntil: string = this.formatDate(register.until);

        return {
          ...register,
          formattedFrom,
          formattedUntil,
          fromUntil: `${formattedFrom} - ${formattedUntil}`,
        };
      }))
    );
  }

  /** Returns @param date formatted with system-selected configs.  */
  private formatDate(date?: string): string {
    if (!date) {
      return this.wording.general.open[this.settingsService.language];
    }

    return moment(date).format(dateFormatTypes[this.settingsService.locale]);
  }
}

