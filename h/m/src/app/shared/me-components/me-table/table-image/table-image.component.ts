import { Component, Input, OnInit } from '@angular/core';
import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { MeUserImageI } from '@shared/me-components/me-user-image/me-user-image.interface';

@Component({
  selector: 'me-table-image',
  templateUrl: './table-image.component.html',
  styleUrls: ['./table-image.component.scss']
})
export class TableImageComponent implements OnInit {
  userImage!: MeUserImageI;
  baseUrl = MARKETPLACE_BASE_API_URL;
  @Input() data: string = '';

  constructor() {
    this.userImage = {
      type: 'avatar',
      size: 'small',
      shape: 'rounded',
      initials: 'FB',
      imageUrl: this.data,
      alt: ''
    };
  }

  ngOnInit(): void {}
}
