import { wording } from '../wording/wording';
import { SitemapNode } from './sitemap';

export const chartering: SitemapNode = {
  path: 'chartering',
  title: { en: 'Chartering and Operating', de: 'Befrachtung und Operating' },
  description: { en: 'Chartering and Operating...', de: 'Befrachtung und Operating...' },
  icon: 'pic-center',
  zone: 'chartering',
  children: {
    misc: {
      path: 'misc',
      title: { en: 'Misc', de: 'Misc' },
      description: { en: 'Misc...', de: 'Misc...' },
      icon: 'assets/icons/88-512.png',
      zone: 'misc',
      children: {
        ports: {
          path: 'ports',
          description: {
            en: 'See, edit and add ports here.',
            de: 'Sehen Sie welche Häfen im System vorhanden sind, passen Sie die Daten an oder fügen Sie neue Häfen hinzu.'
          },
          title: { en: 'Ports', de: 'Häfen' },
          keywords: [
            'Harbour',
            'Hafen'
          ],
          zone: 'ports',
        },
        vessels: {
          path: 'vessels',
          description: {
            en: 'Manage vessel main data and define the parameters for using the vessel in automated processes of navido.',
            // tslint:disable-next-line:max-line-length
            de: 'Pflegen und Überprüfen Sie die Schiffstammdaten und definieren Sie Parameter zur Verwendung des Schiffes in den Systemautomatismen'
          },
          title: { en: 'Vessels', de: 'Schiffe' },
          keywords: [
            'Maindata',
            'Ship',
            'Ships',
            'Stammdaten',
            'Schiffstamm',
            'Schiffstammdaten',
          ],
          zone: 'vessel',
          children: {
            id: {
              path: ':vesselId',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: { en: 'Basic', de: 'Allgemein' }
                },
                specification: {
                  path: 'specifications',
                  zone: 'specifications',
                  title: { en: 'Specification', de: 'Technik' }
                },
                accounting: {
                  path: 'billing',
                  zone: 'accountings',
                  title: { en: 'Accounting', de: 'Buchhaltung' }
                },
                commissions: {
                  path: 'commissions',
                  zone: 'commissions',
                  title: { en: 'Commissions', de: 'Kommissionen' }
                },
                registrations: {
                  path: 'registrations',
                  zone: 'registrations',
                  title: wording.chartering.registration,
                },
                settings: {
                  path: 'settings',
                  zone: 'settings',
                  title: { en: 'Settings', de: 'Einstellungen' },
                },
                roles: {
                  path: 'roles',
                  zone: 'roles',
                  title: wording.general.roles,
                },
              },
            }
          }
        },
        shipyards: {
          path: 'shipyards',
          description: {
            en: 'See and add yards in navido.',
            de: 'Finden Sie hier die Stammdaten der Werften im System, fügen Sie bei Bedarf neue Werften hinzu.',
          },
          title: { en: 'Shipyards', de: 'Werften' },
          keywords: [
            'Shipyard',
            'dockyard',
            'Trockendock',
          ],
          zone: 'shipyards',
        },
        shipyardTypes: {
          path: 'shipyard-types',
          description: {
            en: 'See and add vessel types related to yards.',
            de: 'Sehen Sie hier die Liste der Werft-Schiffstypen und fügen Sie bei bedarf weitere hinzu'
          },
          title: { en: 'Shipyard Types', de: 'Werft-Schiffstypen' },
          keywords: [
            'Shipyard',
            'dockyard',
            'Trockendock',
            'Schiffstyp',
          ],
          zone: 'shipyardType',
        },
        commissionTypes: {
          path: 'commission-types',
          description: {
            en: 'List all available commission types, add new commissions and define settings for automated postings.',
            // tslint:disable-next-line:max-line-length
            de: 'Sehen Sie welche Kommissionsarten im System verfügbar sind, fügen Sie neue hinzu oder definieren Sie die Einstellungen zu buchhalterischen Verwendung.',
          },
          title: { en: 'Commission Types', de: 'Kommissionstypen' },
          keywords: [
            'Booking Code',
            'Buchungscode',
            'Code',
          ],
          zone: 'commissionTypes',
        },
        voyages: {
          path: 'voyages',
          description: {
            // tslint:disable-next-line:max-line-length
            en: 'Find information to all fixtures of your vessels, see the defined periods and manage settings for Invoicing and commissions.',
            // tslint:disable-next-line:max-line-length
            de: 'Legen Sie hier alle Informationen zu den Reisen und Beschäftigungen Ihrer Schiffe an, Informieren Sie sich über Zeiträume und steuern Sie die Abrechnung mit dem Charterer, sowie die Abwicklung etwaiger Kommissionen.'
          },
          title: { en: 'Voyages', de: 'Reisen' },
          keywords: [
            'Charter',
            'Timecharter',
            'Positioning',
            'Automatic',
            'Hire',
            'Invoices',
            'Reisebuch',
            'Freie',
            'TC',
            'Zeitcharter',
            'By-pass',
            'Positionierung',
            'Automatische',
            'Rechnungsschreibung',
          ],
          zone: 'voyages',
          children: {
            id: {
              path: ':voyageId',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: { en: 'Basic', de: 'Allgemein' }
                },
                offHire: {
                  path: 'off-hire',
                  zone: '*',
                  title: { en: 'Off Hire', de: 'Off Hire' }
                }
              }
            }
          }
        },
        timeCharters: {
          path: 'time-charters',
          description: { en: 'Time Charters', de: 'Zeitchartern' },
          title: { en: 'Time Charters', de: 'Zeitchartern' },
          zone: 'voyages',
          hidden: true,
          children: {
            id: {
              path: ':timeCharterId',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: { en: 'Basic', de: 'Allgemein' }
                },
                periods: {
                  path: 'periods',
                  zone: '*',
                  title: { en: 'Periods', de: 'Periods' }
                },
                billing: {
                  path: 'billing',
                  zone: '*',
                  title: { en: 'Billing', de: 'Billing' }
                },
              },
            }
          }

        },
        expenses: {
          path: 'expenses',
          description: { en: 'Expenses', de: 'Kosten' },
          title: wording.chartering.expenses,
          zone: 'expenses',
          children: {
            id: {
              path: ':expenseId',
              zone: '*',
              title: wording.general.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: wording.general.basic,
                },
                remarks: {
                  path: 'remarks',
                  zone: '*',
                  title: wording.chartering.remarks,
                },
              },
            },
          },
        },
        bunkers: {
          path: 'bunkers',
          description: {
            en: 'Bunkers',
            de: 'Bunkers',
          },
          title: { en: 'Bunkers', de: 'Bunkers' },
          keywords: [
            'Bunkers',
          ],
          zone: 'bunkers',
        },
      },
    },
    registrations: {
      path: 'registrations',
      title: wording.chartering.registrations,
      description: wording.chartering.registrations,
      icon: 'assets/icons/88-512.png',
      zone: 'registrations',
      children: {
        dashboard: {
          path: 'dashboard',
          description: {
            en: 'View and edit upcoming registration tasks on the dashboard.',
            de: 'Sehen und bearbeiten Sie anstehende Aufgaben der Registrierung auf dem Dashboard.',
          },
          title: wording.chartering.dashboardRegistration,
          keywords: [
            'Dashboard',
            'Registration',
            'Registrations',
            'Registrierungen',
            'Registrierungen',
          ],
          zone: 'dashboard',
        },
        registrationsFleet: {
          path: 'fleet-registrations',
          description: {
            en: 'Document information concerning registration and flag of vessels.',
            de: 'Dokumentieren Sie Informationen zu der Registrierung und Flagge eines Schiffes.'
          },
          title: wording.chartering.registrationsFleet,
          keywords: [
            'registrations', 'flag', 'fleet', 'call', 'sign', 'SSR', 'registry', // en
            'Registrierung', 'Flagge', 'Flotte', 'Rufzeichen', 'Register' // de
          ],
          zone: 'fleetRegistrations',
          children: {
            id: {
              path: ':id',
              zone: '*',
              title: wording.accounting.basic,
              children: {
                basic: {
                  path: 'basic',
                  zone: '*',
                  title: wording.accounting.basic,
                },
                register: {
                  path: 'register',
                  zone: '*',
                  title: wording.chartering.register,
                },
                technical: {
                  path: 'technical',
                  zone: 'technical',
                  title: wording.chartering.technical
                },
                mortgage: {
                  path: 'mortgage',
                  zone: 'mortgage',
                  title: wording.chartering.mortgage,
                  fullZone: 'chartering.misc.vessel.registrations'
                },
                tasks: {
                  path: 'tasks',
                  zone: 'tasks',
                  title: wording.chartering.tasks
                },
              },
            }
          }
        },
        calendar: {
          path: 'calendar',
          hidden: true,
          title: wording.chartering.calendar,
          zone: 'calendars',
          fullZone: 'system.misc.calendars',
        },
      },
    },
  },
};
