import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';

interface ISharedProps {
  mandatorId: number;
  taxKeyId: number;
}

export interface IMandatorTaxKeyBase extends ISharedProps, IResource<number> { }

export interface IMandatorTaxKeyCreate extends ISharedProps { }

export interface IMandatorTaxKeyDTO extends IMandatorTaxKeyBase { }

export interface IMandatorTaxKey extends IMandatorTaxKeyDTO { }
