export interface LoginData {
  userIdentifier: string;
  password: string;
  rememberMe?: boolean;
}
