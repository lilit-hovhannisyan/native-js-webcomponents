import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { ILog } from '../../../../../core/models/resources/ILog';
import { ActivityLogsService } from '../../../../../core/services/activity-logs.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-activity-logs',
  templateUrl: './activity-logs.component.html',
  styleUrls: ['./activity-logs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActivityLogsComponent implements OnInit {
  public logs: Observable<ILog[]> = this.activityLogsService.logs$;
  public unseenCountSubject = new BehaviorSubject<number>(0);
  public unseenCount$: Observable<number> = this.unseenCountSubject.asObservable();
  public visible: boolean;
  public wording = wording;

  constructor(private activityLogsService: ActivityLogsService) { }

  public ngOnInit(): void {
    this.activityLogsService
      .logs$
      .subscribe(activityLogs => this.unseenCountSubject.next(
        activityLogs.filter(log => log.unseen).length
      ));

    this.activityLogsService.refresh();
  }

  public popoverVisibilityChanged(isOpened: boolean): void {
    if (isOpened) {
      this.activityLogsService.refresh();
    } else {
      this.activityLogsService.markAsSeen();
    }
  }

  public navigateToTarget(log: ILog): void {
    this.visible = false;
  }

  public downloadClicked(documentGuid: string) {
    if (documentGuid) {
      this.activityLogsService.downloadDocument(documentGuid);
    }
  }
}
