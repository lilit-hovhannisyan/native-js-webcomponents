import { NvButton, NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType, NvToolbarButton } from 'nv-grid';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';
import { getContextMenuButtonsConfig } from '../../helpers/general';
import { wordingAccounting } from '../wording/accounting';
import { wordingGeneral } from '../wording/general';
import { Operations } from '../../models/Operations';

interface IActionConfigs {
  buttons?: NvButton[];
  toolbarButtons?: NvToolbarButton[];
  contextMenuButtons?: NvButton[];
}

export const getAccountCostTypeGridConfig = (
  _rowSelectionType: boolean = false,
  actions?: IGridConfigActions,
  canPerform?: (operations: Operations) => boolean,
): NvGridConfig => {
  let actionConfig: IActionConfigs = {};
  const rowSelectionType = _rowSelectionType ? NvGridRowSelectionType.Checkbox : null;

  if (actions && canPerform) {
    actionConfig = {
      buttons: [
        {
          icon: 'delete',
          description: wordingGeneral.delete,
          name: 'deleteAccountCostType',
          tooltip: wordingGeneral.delete,
          actOnDoubleClick: false,
          func: actions?.delete,
          hidden: !canPerform(Operations.DELETE)
        }
      ],
      toolbarButtons: [
        {
          title: wordingGeneral.add,
          tooltip: wordingGeneral.add,
          icon: 'plus-circle-o',
          func: actions?.add,
          hidden: () => !canPerform(Operations.CREATE)
        }
      ],
      contextMenuButtons: canPerform(Operations.READ)
        ? getContextMenuButtonsConfig()
        : [],
    };
  }

  return ({
    gridName: 'accountCostTypeGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    sortBy: 'no',
    isSortAscending: true,
    rowSelectionType,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 25
    },
    columns: [
      {
        key: 'no',
        title: wordingAccounting.no,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wordingAccounting.ctName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    ...actionConfig,
  });
};

