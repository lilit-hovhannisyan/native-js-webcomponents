// tslint:disable: max-line-length
export const wordingChartering = {
  vessel: { en: 'Vessel', de: 'Schiff' },
  vessels: { en: 'Vessels', de: 'Schiffe' },
  imoNumber: { en: 'IMO Number', de: 'IMO Nummer' },
  deliveredFromShipyard: { en: 'Delivered from Shipyard', de: 'Ablieferung ex Bauwerft' },
  vesselType: { en: 'Vessel Type', de: 'Schiffstyp' },
  endOfTermReason: { en: 'End of Term Reason', de: 'Ende des Zeitraums' },
  vesselNo: { en: 'Vessel No.', de: 'Schiffs-Nr.' },
  vesselName: { en: 'Vessel Name', de: 'Schiffs-Name' },
  accountingMandator: { en: 'Accounting Mandator', de: 'Buchhaltungsmandant' },
  owningCompany: { en: 'Owning Company', de: 'Eigentumsgesellschaft' },
  bareboatCompany: { en: 'Bareboat Company', de: 'Bareboat Gesellschaft' },
  commercialManager: { en: 'Commercial Manager', de: 'Kommerzieller Manager' },
  technicalManager: { en: 'Technical Manager', de: 'Technischer Manager' },
  owningCompanyForBookkeepingPurposes: { en: 'Owning Company for Bookkeeping Purposes', de: 'Gesellschaft für Buchhaltungszwecke' },
  crewingCompany: { en: 'Crewing Company', de: 'Crewing Gesellschaft' },
  insolvencyAdministrator: { en: 'Insolvency Administrator', de: 'Insolvenzverwalter' },
  hullNo: { en: 'Hull No', de: 'Hull No' },
  class: { en: 'Class', de: 'Klasse' },
  gear: { en: 'Gear', de: 'Geschirr' },
  hatches: { en: 'Hatches', de: 'Luken' },
  shipyard: { en: 'Shipyard', de: 'Werft' },
  shipyardTypes: { en: 'Shipyard Types', de: 'Werfttyp' },
  untilDateNeedsToBeGreaterThanFromDate: {
    en: 'until-date needs to be greater than from-date',
    de: 'Das end-datum muss größer sein als start-datum',
  },
  yearAndNo: { en: 'Year & No', de: 'Jahr and Nr.' },
  year: { en: 'Year', de: 'Jahr' },
  timeCharter: { en: 'Time Charter', de: 'Zeitchartern' },
  charterer: { en: 'Charterer', de: 'Charterer' },
  charterParty: { en: 'Charterparty', de: 'BASIS CHARTERPARTY' },
  charterPartyDate: { en: 'CP date', de: 'CP Datum' },
  billingAddress: { en: 'Billing Address', de: 'Rechnungsanschrift' },
  laycanFrom: { en: 'Laycan From', de: 'Liegezeit von' },
  laycanUntil: { en: 'Laycan Until', de: 'Liegezeit bis' },
  noVesselSelected: { en: 'No Vessel Selected', de: 'No Vessel Selected' },
  selectVessel: { en: '👈🏻 Select Vessel to list voyages', de: '👈🏻 Select Vessel to list voyages' },
  voyage: { en: 'Voyage', de: 'Reise' },
  voyages: { en: 'Voyages', de: 'Reisen' },
  commission: { en: 'Commission', de: 'Vergütungen' },
  commissionType: { en: 'Commission Type', de: 'Vergütungsart' },
  billingCycle: { en: 'Billing Cycle', de: 'Intervall' },
  billingCycleType: { en: 'Billing Cycle Type', de: 'Intervall Typ' },
  billingCycleContinually: { en: 'Billing Cycle Continually', de: 'Intervall Kontinuierlich' },
  costCenter: { en: 'Cost Center', de: 'Kostenstelle' },
  processing: { en: 'Processing', de: 'Verarbeitung' },
  invoiceRecipient: { en: 'Invoice Recipient', de: 'Rechnungsempfänger' },
  invoicedUntil: { en: 'Invoiced Until', de: 'Abgerechnet Bis' },

  processingInformation: { en: 'Information', de: 'Information' },
  processingInvoice: { en: 'Invoice', de: 'Invoice' },
  processingCreditNote: { en: 'Credit Note', de: 'Credit Note' },
  processingAutomatic: { en: 'Automatic', de: 'Automatic' },
  billingCycleTypeDay: { en: 'Days', de: 'Days' },
  billingCycleTypeWeek: { en: 'Weeks', de: 'Weeks' },
  billingCycleTypeMonths: { en: 'Months', de: 'Months' },
  billingCycleTypeSemimonthly: { en: 'Semimonthly', de: 'Semimonthly' },
  billingCycleTypeQuarters: { en: 'Quarters', de: 'Quarters' },
  billingCycleTypeYears: { en: 'Years', de: 'Years' },
  billingCycleTypeLumpsum: { en: 'Lumpsum', de: 'Lumpsum' },
  billingActive: { en: 'Billing active', de: 'Rechnung aktiv' },

  overrideBillingAddress: {
    en: 'OVERRIDE BILLING ADDRESS',
    de: 'RECHNUNGSEMPFÄNGER ABWEICHEND',
  },

  period: { en: 'Period', de: 'Period' },
  option: { en: 'Option', de: 'Option' },
  optedPeriod: { en: 'Opted Period', de: 'Opted Period' },
  nonDeclaredOption: { en: 'Non-Declared Option', de: 'Non-Declared Option' },
  extension: { en: 'Extension', de: 'Extension' },

  redeliveryDate: { en: 'Redelivery Date', de: 'Redelivery Date' },
  fixedPeriod: { en: 'Fixed Period', de: 'Fixed Period' },
  minMaxPeriod: { en: 'Min/Max Period', de: 'Min/Max Period' },
  minMaxDates: { en: 'Min/Max Dates', de: 'Min/Max Dates' },

  days: { en: 'Days', de: 'Tage' },
  months: { en: 'Months', de: 'Monate' },
  month: { en: 'Month', de: 'Monat' },

  periodType: { en: 'Period Type', de: 'Periodentyp' },
  durationType: { en: 'Duration Type', de: 'Dauertyp' },

  deliveryRedelivery: { en: 'Delivery / Redelivery', de: 'Delivery / Redelivery' },

  begin: { en: 'Begin', de: 'Beginn' },
  endApprox: { en: 'End (Approx)', de: 'ENDE (AVISIERT)' },
  endActual: { en: 'End (Actual)', de: 'Ende (Tatsächlich)' },
  duration: { en: 'Duration', de: 'Dauer' },
  durationUnit: { en: 'Duration Unit', de: 'Dauer Einheit' },

  declarationOfOption: { en: 'Declaration Of Option', de: 'Declaration Of Option' },

  declarationToBe: { en: 'Declaration to be', de: 'Erklärung bis' },
  declared: { en: 'Declared', de: 'Erklärt' },

  addendum: { en: 'Addendum', de: 'Addendum' },
  addendumDated: { en: 'Addendum Dated', de: 'Zusatz Vom' },
  addendumNo: { en: 'Addendum No.', de: 'Zusatz Nr.' },
  charterRate: { en: 'Charterrate', de: 'Charterrate' },
  charterRateType: { en: 'Charterrate Type', de: 'Charterrate typ' },
  charterRateFrom: { en: 'Charterrate From', de: 'Charterrate ab' },
  countsFrom: { en: 'Counts From', de: 'Zählt Ab' },
  countsUntil: { en: 'Counts Until', de: 'Zählt bis' },
  fixed: { en: 'Fixed', de: 'Fixed' },
  variable: { en: 'Variable', de: 'Variable' },
  additionalOffHire: { en: 'Additional off Hire', de: 'ANGEHÄNGTE OFF-HIRE TAGE' },
  minusDays: { en: 'Minus Days', de: 'Minus Tage' },
  plusDays: { en: 'Plus Days', de: 'Plus Tage' },
  minDate: { en: 'Min Date', de: 'Min Datum' },
  maxDate: { en: 'Max Date', de: 'Max Datum' },
  minPeriod: { en: 'Min Period', de: 'Min Periode' },
  maxPeriod: { en: 'Max Period', de: 'Max Periode' },
  vesselTypeNone: { en: 'None', de: 'None' },
  vesselTypeBulkcarrier: { en: 'Bulkcarrier', de: 'Bulkcarrier' },
  vesselTypeContainer: { en: 'Container', de: 'Container' },
  vesselTypeRoRo: { en: 'RoRo', de: 'RoRo' },
  vesselTypeConRo: { en: 'ConRo', de: 'ConRo' },
  vesselTypeMPP: { en: 'MPP', de: 'MPP' },
  commissionsTypeGroupChartering: { en: 'Chartering', de: 'Chartering' },
  commissionsTypeGroupBroker: { en: 'Broker', de: 'Broker' },
  commissionsTypeGroupAddress: { en: 'Address', de: 'Address' },
  commissionsTypeGroupOther: { en: 'Other', de: 'Other' },
  endOfTermReasonsSolid: { en: 'Solid', de: 'Solid' },
  endOfTermReasonsPoolExit: { en: 'Pool Exit', de: 'Pool Exit' },
  endOfTermReasonsChangeOfOwner: { en: 'Change of Owner', de: 'Change of Owner' },
  endOfTermReasonsContractTermination: { en: 'Contract Termination', de: 'Contract Termination' },
  endOfTermReasonsLiquidation: { en: 'Liquidation', de: 'Liquidation' },
  toEditRemoveTimeCharters: {
    en: 'To edit, remove all Time Charters and Charternames',
    de: 'To edit, remove all Time Charters and Charternames'
  },
  toEditRemovePeriods: { en: 'To edit, remove all Periods', de: 'To edit, remove all Periods' },
  continually: { en: 'Continually', de: 'Intervall Kontinuierlich' },
  dueOnDayOfMonth: { en: 'Due on Day of Month', de: 'Fällig Am' },
  issueInvoiceDaysBeforeDue: { en: 'Issue Invoice ... Days before Due', de: 'Ausstellung … Tage vor Fälligkeit' },
  issueInvoicesInAdvance: { en: 'Issue ... Invoices in Advance', de: 'Rechnungen im Vorraus erstellen' },
  hire: { en: 'Hire', de: 'Miete' },
  offHire: { en: 'Off Hire', de: 'Off Hire' },
  lumpsumExpenses: { en: 'Lumpsum Expenses', de: 'Lumpsum Expenses' },
  atCostExpenses: { en: 'At Cost Expenses', de: 'At Cost Expenses' },
  onHire: { en: 'On Hire', de: 'On Hire' },
  percentToCount: { en: '% to Count', de: 'Anteilig' },
  cause: { en: 'Cause', de: 'Ursache' },
  remarks: { en: 'Remarks', de: 'Bemerkungen' },
  credit: { en: 'Credit note', de: 'Gutschrift' },
  totalDays: { en: 'Total Days', de: 'Total Days' },
  daysToCount: { en: 'Days to Count', de: 'Days to Count' },
  location: { en: 'Location', de: 'Ort' },
  expenses: { en: 'Expenses', de: 'Expenses' },
  type: { en: 'Type', de: 'Typ' },
  defaultForTcVoyages: { en: 'Default for TC-voyages', de: 'Standard bei Zeitcharterreisen' },
  costType: { en: 'Costtype', de: 'Kostenart' },
  expenseTypeLupsum: { en: 'Lumpsum', de: 'Lumpsum' },
  expenseTypeAtCost: { en: 'at cost', de: 'nach Aufwand' },
  shortName: { en: 'Short Name', de: 'Abkürzung' },
  lastChange: { en: 'Last change', de: 'Letzte Änderung' },
  change: { en: 'Change', de: 'Änderung' },
  timeCharters: { en: 'Time Charters', de: 'Zeitchartern' },
  vesselNoDisabledPopup: {
    en: 'Change of number only possible for systemadmin',
    de: 'Änderung der Nummer nur durch Systemadmin möglich',
  },
  vesselNameDisabledPopup: {
    en: 'Change of name only possible for administrators',
    de: 'Änderung des Namens nur durch Administratoren möglich',
  },
  preDelivery: { en: 'Pre-Delivery', de: 'Vor Anlieferung' },
  duplicateImo: {
    en: 'IMO number already exists. Please provide a new IMO number',
    de: 'IMO nummer wird bereits verwendet. Bitte eine neue IMO nummer eingeben',
  },
  unexpectedDate: {
    en: 'Unexpected date.',
    de: 'Unerwartetes Datum'
  },
  callSign: { en: 'Call sign', de: 'Rufzeichen' },
  officialNumber: { en: 'Official number', de: 'Official number' },
  mmsi: { en: 'MMSI', de: 'MMSI' },
  foreignCompany: { en: 'Foreign company', de: 'Auslandsgesellschaft' },
  register: { en: 'Register', de: 'Register' },
  registers: { en: 'Registers', de: 'Registers' },
  flagHistory: { en: 'Flag history', de: 'Flaggenhistorie' },
  periodFromUntil: { en: 'Period from / until', de: 'Laufzeit von / bis' },
  portOfRegistry: { en: 'Port of registry', de: 'Heimathafen' },
  training: { en: 'Training', de: 'Ausbildung' },
  intendedSale: { en: 'Intended sale', de: 'Geplanter Verkauf' },
  portDoesntBelongToFlagstate: {
    en: 'The chosen port does not belong to chosen Flagstate.',
    de: 'Der Heimathafen gehört nicht zum gewählten Flaggenstaat. '
  },
  invalidFlagHistoryValue: {
    en: 'Chosen dates include parts of other flag periode, please check flag history and use meaningful values.',
    de: 'Der gewählte Zeitraum überlappt mit anderen Flaggen-Perioden, bitte prüfen Sie die Flaggenhistorie und passen Sie Ihre Daten an.'
  },
  registration: {
    en: 'Registration',
    de: 'Registrierung'
  },
  vesselSettingsRegistration: {
    en: 'Registration',
    de: 'Registrierung'
  },
  vesselSettingsTechnicalInspection: {
    en: 'Technical Inspection',
    de: 'Technische Bereederung'
  },
  vesselSettingsCharteringAndOperating: {
    en: 'Chartering & Operating',
    de: 'Befrachtung & Operating'
  },
  vesselSettingsBookkeeping: {
    en: 'Bookkeeping',
    de: 'Buchhaltung'
  },
  vesselSettingsInsurance: {
    en: 'Insurance',
    de: 'Versicherung'
  },
  vesselSettingsCrewing: {
    en: 'Crewing',
    de: 'Crewing'
  },
  vesselSettingsSaleAndPurchase: {
    en: 'Sale & Purchase',
    de: 'An- und Verkauf'
  },
  typesOfMaritimeServices: {
    en: 'Types of maritime services',
    de: 'Arten der maritimen Dienstleistungen',
  },
  registrations: { en: 'Registrations', de: 'Registrierungen' },
  registrationsFleet: { en: 'Registrations Fleet', de: 'Registrierungen Flotte' },
  chartername: { en: 'Chartername', de: 'Chartername' },
  client: { en: 'Client', de: 'Kunde' },
  comments: { en: 'Comments', de: 'Anmerkungen' },
  flag: { en: 'Flag', de: 'Flagge' },
  periodFrom: { en: 'Period from', de: 'Laufzeit von' },
  periodTill: { en: 'Period till', de: 'Laufzeit bis' },
  typeOfRegistration: { en: 'Type of registration', de: 'Art der Registrierung' },
  ssrNo: { en: 'SSR no.', de: 'SSR-Nummer' },
  officialNo: { en: 'Official no.', de: 'Official Number' },
  imoNo: { en: 'IMO no.', de: 'IMO-Nr.' },
  registeredOwner: { en: 'Registered owner', de: 'Eigentümer' },
  bbCharterer: { en: 'bb-charterer', de: 'Bareboatcharterer' },
  mortgagee: { en: 'Mortgagee', de: 'Gläubiger' },
  general: { en: 'General', de: 'Allgemein' },
  germanRegister: { en: 'German Register', de: 'Deutsches Register' },
  foreignRegister: { en: 'Foreign Register', de: 'Auslandsregister' },
  vesselNameFleet: { en: 'Vessel name', de: 'Schiffsname' },
  bareboatCharterer: { en: 'Bareboatcharterer', de: 'Bareboatcharterer' },
  bareboat: { en: 'Bareboat', de: 'Bareboat' },
  permanent: { en: 'Permanent', de: 'Permanent' },
  technical: { en: 'Technical', de: 'Technik' },
  mortgage: { en: 'Mortgage', de: 'Hypothek' },
  tasks: { en: 'Tasks', de: 'Aufgaben' },
  vesselRegistrationTasks: { en: 'Vessel registration tasks', de: 'Aufgaben Schiffsregistrierung' },
  technicalDetails: { en: 'Technical Details', de: 'Technische Details' },
  debitorAndCreditorAreOpenItems: {
    en: 'Debtor and creditor accounts are open item accounts by default. The open item account flag cannot be removed.',
    de: 'Debitor - und Kreditorkonten sind standardmäßig als OP-Konten definiert. Die OP-Verwaltung kann nicht entfernt werden.'
  },
  ssr: { en: 'SSR', de: 'SSR' },
  registerCourt: { en: 'Register Court', de: 'Registergericht' },
  primaryRegister: { en: 'Primary Register', de: 'Primär-Register' },
  secondaryRegister: { en: 'Secondary Register', de: 'Sekundär-Register' },
  fromUntil: { en: 'From / Until', de: 'Von / Bis' },
  timeRangeDoesntMatchWithPeriod: {
    en: 'Time range doesn’t match with register period',
    de: 'Zeitrahmen passt nicht zu Registerperiode',
  },
  officialNoAbbr: { en: 'Official No.', de: 'Offizielle Nr.' },
  company: { en: 'Company', de: 'Gesellschaft' },
  youNeedToCheckServiceType: {
    en: 'You need to check at least 1 service type',
    de: 'Geben Sie bitte mindestens eine Dienstleistung an'
  },
  mortgageAmount: { en: 'Mortgage amount', de: 'Hypothek' },
  showDoneTasks: { en: 'show done tasks', de: 'erledigte Aufgaben anzeigen' },
  doneDue: { en: 'Done / Due', de: 'Erledigt / fällig' },
  fleetRegistrationDeleteConfirm: {
    en: 'Do you really want to remove the vessel from registration module? ',
    de: 'Wollen Sie das Schiff wirklich nicht mehr im Registrierungsmodul anzeigen lassen?'
  },
  removeRegistration: { en: 'Remove registration', de: 'Anzeige entfernen' },
  notStarted: { en: 'not started', de: 'nicht begonnen' },
  open: { en: 'open', de: 'offen' },
  inProgress: { en: 'in progress', de: 'In Bearbeitung' },
  done: { en: 'done', de: 'erledigt' },
  showOnDoneDueDate: { en: 'show on Done / Due date', de: 'Zum Erledigt / Fällig Datum anzeigen' },
  showOnStartDate: { en: 'show on Start date', de: 'Zum Start Datum anzeigen' },
  areYouSureYouWantToDeleteTask: {
    en: 'Are you sure you want to delete task {subject}?',
    de: 'Möchten Sie die Aufgabe {subject} wirklich löschen?',
  },
  dashboardRegistration: {
    en: 'Dashboard Registration',
    de: 'Dashboard Registrierungen'
  },
  dates: {
    en: 'Dates',
    de: 'Termine'
  },
  activeTasks: { en: 'Active tasks', de: 'Aktive Aufgaben' },
  calendar: { en: 'Calendar', de: 'Kalender' },
  todo: { en: 'ToDo', de: 'Tätigkeiten' },
  statistics: { en: 'Statistics', de: 'Statistik' },
  overview: { en: 'Overview', de: 'Übersicht' },
  reports: { en: 'Reports', de: 'Berichte' },
  noSpecification: {
    en: 'Vessel does not have specification',
    de: 'Vessel does not have specification'
  },
  addNewTask: {
    en: 'Add new task',
    de: 'Aufgabe hinzufügen'
  },
  addNewTodo: {
    en: 'Add new todo',
    de: 'Tätigkeit hinzufügen'
  },
  addNewEvent: {
    en: 'Add new event',
    de: 'Neuen Termin hinzufügen'
  },
  showTaskDetails: {
    en: 'Show task details',
    de: 'Aufgabe anzeigen'
  },
  editTask: {
    en: 'Edit task',
    de: 'Aufgabe bearbeiten'
  },
  deleteTask: {
    en: 'Delete task',
    de: 'Aufgabe löschen'
  },
  showTodoDetails: {
    en: 'Show Todo details',
    de: 'Tätigkeit anzeigen'
  },
  editToDo: {
    en: 'Edit ToDo',
    de: 'Tätigkeit bearbeiten'
  },
  deleteToDo: {
    en: 'Delete ToDo',
    de: 'ToDo löschen'
  },
  showEventDetails: {
    en: 'Show Event details',
    de: 'Zeige Termin-Details'
  },
  editEvent: {
    en: 'Edit Event',
    de: 'Termin bearbeiten'
  },
  deleteEvent: {
    en: 'Delete Event',
    de: 'Termin löschen'
  },
  showDoneEntries: {
    en: 'show DONE entries',
    de: 'erledigte Einträge anzeigen'
  },
  calendarListViewBtnText: {
    en: 'show event list for a week',
    de: 'terminliste (wochenbasiert) anzeigen'
  },
  today: {
    en: 'Today',
    de: 'Heute'
  },
  searchCalendar: {
    en: 'Search task, event', // , todos
    de: 'Suche Auftrag, Termin' // , Aufgabe
  },
  createCalendar: { en: 'Create calendar', de: 'Erstellen kaleder' },
  registrationPeriod: { en: 'Registration Period', de: 'Registrierungsperiode' },
  event: {
    en: 'Event',
    de: 'Termin'
  },
  taskCantBeModifiedDueToDateOrder: {
    en: 'Start date({STARTDATE}) is older then end date({ENDDATE})',
    de: 'Start date({STARTDATE}) is older then end date({ENDDATE})'
  },
  nextRegistrationPeriodStarts: {
    en: 'Next registration period starts in {DAYS} day(s)',
    de: 'Nächste Registrierungsperiode startet in {DAYS} Tag(en)'
  },
  registerAndFlag: {
    en: 'Register / Flag',
    de: 'Register / Flagge'
  },
};
