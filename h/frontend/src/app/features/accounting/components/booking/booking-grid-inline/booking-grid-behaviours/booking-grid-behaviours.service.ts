import { Injectable } from '@angular/core';
import { FormGroup, AbstractControl } from '@angular/forms';
import { IBookingAccount, BookingAccountKey } from 'src/app/core/models/resources/IBookingAccount';
import { Language } from 'src/app/core/models/language';
import { ACCOUNTING_RELATIONS } from 'src/app/core/models/enums/accounting-relations';
import { enableOrDisableAmountLocal } from '../helpers/amounts-and-rate';
import { ExchangeRateService } from 'src/app/core/services/mainServices/exchange-rate.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { DebitorCreditorPaymentConditionsService } from 'src/app/core/services/mainServices/debitor-creditor-payment-conditions.service';
import { combineLatest } from 'rxjs';
import { DEBITOR_CREDITOR } from 'src/app/core/models/enums/debitor-creditor';
import { NvColumnConfig } from 'nv-grid';
import { SettingsService } from 'src/app/core/services/settings.service';
import { DEBIT_CREDIT } from 'src/app/core/models/enums/debitCredit';
import { wording } from 'src/app/core/constants/wording/wording';
import { IPaymentCondition, IPaymentStepRow } from 'src/app/core/models/resources/IPaymentConditions';
import { ToCurrentDateFormatPipe } from 'src/app/shared/pipes/to-current-date-format.pipe';
import { TaxKeyRepositoryService } from 'src/app/core/services/repositories/tax-key-repository.service';
import { getDiscounts } from 'src/app/features/accounting/helpers/general';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { MandatorBookingCodeRepositoryService } from 'src/app/core/services/repositories/mandator-booking-code-repository.service';
import { MandatorTaxKeysRepositoryService } from 'src/app/core/services/repositories/mandator-tax-keys-repository.service';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { DebitorCreditorRepositoryService } from 'src/app/core/services/repositories/debitor-creditor-repository.service';
import { skipWhile, take } from 'rxjs/operators';
import { MandatorKey, IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { IMandatorAccount } from 'src/app/core/models/resources/IMandatorAccount';
import { enableOrDisableCostType } from '../helpers/booking-lines-cost-type';
import { CostTypeRepositoryService } from 'src/app/core/services/repositories/cost-type-repository.service';
import { ICostType } from 'src/app/core/models/resources/ICostTypes';
import { AccountCostTypeRepositoryService } from 'src/app/core/services/repositories/account-cost-type-repository.service';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { CostCenterRepositoryService } from 'src/app/core/services/repositories/cost-center-repository.service';
import { ICostCenter } from 'src/app/core/models/resources/ICostCenter';
import { CostCenterHelperService } from './cost-center-helper.service';
import { CostUnitHelperService } from './cost-unit-helper.service';
import { BookingKey } from 'src/app/core/models/resources/IBooking';
import { CurrencyKey, ICurrency } from 'src/app/core/models/resources/ICurrency';
import { ActivatedRoute, Router } from '@angular/router';
import { CostTypeValidatorsService } from 'src/app/core/services/validators/cost-type-validators.service';
import { ICostUnit } from 'src/app/core/models/resources/ICostUnit';
import { BookingFormService } from 'src/app/core/services/booking-form.service';
import { BookingResourceService } from 'src/app/core/services/booking-resource.service';
import { CostUnitRepositoryService } from 'src/app/core/services/repositories/cost-unit-repository.service';

@Injectable({
  providedIn: 'root'
})
export class BookingGridBehavioursService {
  constructor(
    private bookingFormService: BookingFormService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    private debitorCreditorRepo: DebitorCreditorRepositoryService,
    private mandatorAccountRepo: MandatorAccountRepositoryService,
    private mandatorCodeRepo: MandatorBookingCodeRepositoryService,
    private mandatorTaxRepo: MandatorTaxKeysRepositoryService,
    private bookingResourceService: BookingResourceService,
    private bookingCodeRepo: BookingCodeRepositoryService,
    private exchangeRateService: ExchangeRateService,
    private debitorCreditorPaymentConditionsService: DebitorCreditorPaymentConditionsService,
    private settingsService: SettingsService,
    private taxKeyRepo: TaxKeyRepositoryService,
    private mandatorRepo: MandatorRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private costTypeRepo: CostTypeRepositoryService,
    private accountCostTypeRepo: AccountCostTypeRepositoryService,
    private costTypeValidatorsService: CostTypeValidatorsService,
    private costCenterRepo: CostCenterRepositoryService,
    private costCenterHelperService: CostCenterHelperService,
    private costUnitRepo: CostUnitRepositoryService,
    private costUnitHelperService: CostUnitHelperService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }
  public setMandatorId = (form: FormGroup) => {
    const mandatorNoControl = form.get('mandatorNo');
    const mandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(m => m.no === +mandatorNoControl.value);
    const mandatorId: MandatorKey | null = mandator && !mandatorNoControl.invalid
      ? mandator.id
      : null;

    form.get('mandatorId').patchValue(mandatorId);
  }

  public setCurrencyId = (form: FormGroup) => {
    const isoCode = form.get('currencyIsoCode').value;
    const foundCurrency = this.currencyRepo.getStreamValue()
      .find(currency => currency.isoCode === isoCode);
    form.get('currencyId').patchValue(foundCurrency ? foundCurrency.id : null);
  }

  public onMandatorChange = (form: FormGroup, bookInvoiceMode: boolean = false, bookingId: BookingKey = null) => {
    // Validation 22 to 29_01
    // https://navido.atlassian.net/wiki/spaces/TE/pages/376274945/Booking+form
    const mandatorCtrl: AbstractControl = form.get('mandatorId');
    const mandatorId: MandatorKey = +mandatorCtrl.value;
    const mandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(m => m.id === mandatorId);
    const { accountNo, currencyIsoCode, bookingCodeNo } = form.controls;
    if (mandator) {
      const bookingAccountCtrl: AbstractControl = form.get('bookingAccountId');
      const bookingAccountId: BookingAccountKey = +bookingAccountCtrl.value;
      const bookingAccount: IBookingAccount = bookingAccountId
        ? this.bookingAccountRepo.getStreamValue().find(ba => ba.id === bookingAccountId)
        : null;
      if (mandatorCtrl.valid && bookingAccountCtrl.valid) {
        this.validateCT_CC_CU(form, mandator, bookingAccount, bookingId);
      }
      // https://navido.atlassian.net/browse/NAVIDO-604

      combineLatest([
        this.mandatorAccountRepo.fetchAll({ queryParams: { mandatorId: mandator.id } }),
        this.mandatorCodeRepo.fetchAll({ queryParams: { mandatorId: mandator.id } }),
        this.mandatorTaxRepo.fetchAll({ queryParams: { mandatorId: mandator.id } })
      ]).subscribe(() => {
        accountNo.updateValueAndValidity();
        bookingCodeNo.updateValueAndValidity();
      });

      /**
        we don't want to set mandator's currency when we are in
        read, clone and edit modes
       */
      if (
        !bookInvoiceMode
        && !this.isBookingInReadOnlyMode()
        && !this.isBookingInCloneMode()
        && !this.isBookingInEditMode()
      ) {
        const foundCurrency: ICurrency = this.currencyRepo.getStreamValue()
          .find(currency => currency.id === mandator.currencyId);
        if (foundCurrency) {
          currencyIsoCode.patchValue(foundCurrency.isoCode);
        }
      }
    }
  }

  public initAccrualAccountingValue(form: FormGroup, bookingAccount: IBookingAccount) {
    // Accrual accounting should be set to true for PL accounts
    const useAccrualAccountingForRow = bookingAccount &&
      [ACCOUNTING_RELATIONS.PROFITANDLOSSEARNINGS,
      ACCOUNTING_RELATIONS.PROFITANDLOSSEXPENSES]
        .includes(bookingAccount.accountingRelation);
    form.get('accrualAccounting').patchValue(useAccrualAccountingForRow);
  }

  private validateCT_CC_CU(
    form: FormGroup,
    mandator: IMandatorLookup,
    bookingAccount: IBookingAccount,
    bookingId: BookingKey
  ): void {
    enableOrDisableCostType(
      form,
      mandator,
      bookingAccount,
      this.accountCostTypeRepo.getStreamValue(),
      this.costTypeRepo.getStreamValue(),
      this.costTypeValidatorsService,
      bookingId
    );
    this.costCenterHelperService.enableOrDisableCostCenters(
      form,
      mandator,
      bookingAccount,
      bookingId
    );
    this.costUnitHelperService.enableOrDisableCostUnits(
      form,
      mandator,
      bookingAccount,
      bookingId
    );
  }

  public setDefaultColumnValue = (value: any, column: NvColumnConfig) => {
    if (column) {
      column.editControl.defaultValue = value;
    }
  }

  public getDefaultColumnValue = (column: NvColumnConfig) => {
    if (column) {
      return column.editControl?.defaultValue;
    }
  }

  public onTaxKeyChange = (form: FormGroup) => {
    const taxKeyId = form.get('taxKeyId').value;
    const taxKeyControl = form.get('tax');
    const foundTaxKey = this.taxKeyRepo.getStreamValue()
      .find(taxKey => taxKey.id === +taxKeyId);
    if (foundTaxKey) {
      if (taxKeyControl.value !== foundTaxKey.code) {
        taxKeyControl.patchValue(foundTaxKey.code);
      }
    }
  }

  public onCurrencyChange = (form: FormGroup) => {
    const currencyId: CurrencyKey = form.get('currencyId').value;

    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === +(form.get('mandatorId').value));
    if (foundMandator && currencyId) {
      this.setRate(currencyId, foundMandator.currencyId, form.get('exchangeRate'));
      const leadingAccount: boolean = this.bookingFormService.getLeadingAccountValue();

      enableOrDisableAmountLocal(
        currencyId,
        foundMandator.currencyId,
        form.get('amountLocal'),
        leadingAccount
      );
    }
  }

  public setBookingAccount = (form: FormGroup) => {
    const accountNo = form.get('accountNo').value;
    const foundBookingAccount = this.bookingAccountRepo.getStreamValue()
      .find(bookingAccount => bookingAccount.no === +accountNo);
    if (foundBookingAccount) {
      form.get('bookingAccountId').patchValue(+foundBookingAccount.id);
      form.get('accountName').patchValue(
        this.settingsService.language === Language.EN
          ? foundBookingAccount.labelEn
          : foundBookingAccount.labelDe
      );

    } else {
      form.get('bookingAccountId').patchValue(null);
      form.get('accountName').patchValue('');
    }
  }

  public setTaxKeyId = (form: FormGroup) => {
    const taxKeyCode = form.get('tax').value;
    const foundTaxKey = this.taxKeyRepo.getStreamValue()
      .find(taxKey => taxKey.code === taxKeyCode);

    form.get('taxKeyId').patchValue(
      foundTaxKey
        ? foundTaxKey.id
        : null
    );
  }

  public setBookingCodeId = (form: FormGroup) => {
    const bookingCodeNo = form.get('bookingCodeNo').value;
    const foundBookingCode = this.bookingCodeRepo.getStreamValue()
      .find(bookingCode => bookingCode.no === +bookingCodeNo);

    form.get('bookingCodeId').patchValue(
      foundBookingCode
        ? foundBookingCode.id
        : null
    );
  }

  public setCostTypeId = (form: FormGroup): void => {
    const language: Language = this.settingsService.language;
    const costTypeNoCtrl: AbstractControl = form.get('costTypeNo');
    const costTypeNo: number = +costTypeNoCtrl.value;
    const costType: ICostType = this.costTypeRepo.getStreamValue()
      .find(c => c.no === costTypeNo);
    const costTypeIdCtrl: AbstractControl = form.get('costTypeId');
    const costTypeNameCtrl: AbstractControl = form.get('costTypeName');

    if (costType) {
      costTypeIdCtrl.patchValue(costType.id);
      const displayLabel: string = costType[getLabelKeyByLanguage(language)];
      costTypeNameCtrl.patchValue(displayLabel);
    } else {
      costTypeIdCtrl.patchValue(null);
      costTypeNameCtrl.patchValue('');
    }
    costTypeNoCtrl.updateValueAndValidity({ emitEvent: false });
  }

  public setCostCenterId = (form: FormGroup): void => {
    const language: Language = this.settingsService.language;
    const costCenterNoCtrl: AbstractControl = form.get('costCenterNo');
    const costCenterNo: number = +costCenterNoCtrl.value;
    const costCenter: ICostCenter = this.costCenterRepo.getStreamValue()
      .find(c => c.no === costCenterNo);
    const costCenterIdCtrl: AbstractControl = form.get('costCenterId');
    const costCenterNameCtrl: AbstractControl = form.get('costCenterName');

    if (costCenter) {
      costCenterIdCtrl.patchValue(costCenter.id);
      const displayLabel: string = costCenter[getLabelKeyByLanguage(language)];
      costCenterNameCtrl.patchValue(displayLabel);
    } else {
      costCenterIdCtrl.patchValue(null);
      costCenterNameCtrl.patchValue('');
    }
    costCenterNoCtrl.updateValueAndValidity({ emitEvent: false });
  }

  public setCostUnitId = (form: FormGroup): void => {
    const language: Language = this.settingsService.language;
    const costUnitNoCtrl: AbstractControl = form.get('costUnitNo');
    const costUnitNo: string = costUnitNoCtrl.value;
    const costUnit: ICostUnit = this.costUnitRepo.getStreamValue()
      .find(c => c.no === costUnitNo);
    const costUnitIdCtrl: AbstractControl = form.get('costUnitId');
    const costUnitNameCtrl: AbstractControl = form.get('costUnitName');

    if (costUnit) {
      costUnitIdCtrl.patchValue(costUnit.id);
      const displayLabel: string = costUnit[getLabelKeyByLanguage(language)];
      costUnitNameCtrl.patchValue(displayLabel);
    } else {
      costUnitIdCtrl.patchValue(null);
      costUnitNameCtrl.patchValue('');
    }
    costUnitNoCtrl.updateValueAndValidity({ emitEvent: false });
  }

  /**
 * @param {FormGroup} form booking account's from
 * @param {boolean} bookInvoiceMode false by default, when true; skips doing changes
 * to the currency and discount fields
 */
  public onBookingAccountChange = (
    form: FormGroup,
    bookInvoiceMode: boolean = false,
    bookingId: BookingKey = null
  ) => {
    const bookingAccountCtrl: AbstractControl = form.get('bookingAccountId');
    const bookingAccountId: BookingAccountKey = +bookingAccountCtrl.value;

    const bookingAccount: IBookingAccount = bookingAccountId
      ? this.bookingAccountRepo.getStreamValue().find(ba => ba.id === bookingAccountId)
      : null;

    const currencyIsoCodeControl: AbstractControl = form.get('currencyIsoCode');

    if (bookingAccount) {
      const mandatorCtrl: AbstractControl = form.get('mandatorId');
      const mandatorId: MandatorKey = +mandatorCtrl.value;
      const mandator: IMandatorLookup = this.mandatorRepo
        .getLookupStreamValue()
        .find(m => m.id === mandatorId);

      if (mandatorCtrl.valid && bookingAccountCtrl.valid) {
        this.validateCT_CC_CU(form, mandator, bookingAccount, bookingId);
      }

      const mandatorBookingAccount: IMandatorAccount = this.mandatorAccountRepo
        .getStreamValue()
        .filter(mandatorAccount => mandatorAccount.mandatorId === mandatorId)
        .find(mandatorAccount => mandatorAccount.bookingAccountId === bookingAccount.id);

      const restrictedCurrency = mandatorBookingAccount
        && this.currencyRepo.getStreamValue().find(currency => {
          return currency.id === mandatorBookingAccount.restrictedCurrencyId;
        });

      if (!bookInvoiceMode) {
        if (restrictedCurrency) {
          currencyIsoCodeControl.patchValue(restrictedCurrency.isoCode);
          currencyIsoCodeControl.disable({ emitEvent: false });
        } else {
          currencyIsoCodeControl.enable({ emitEvent: false });
        }
      }

      const bookingForm: FormGroup = this.bookingFormService
        .bookingFormSubject
        .value;

      const isProfitOrLoss: boolean =
        bookingAccount.accountingRelation === ACCOUNTING_RELATIONS.PROFITANDLOSSEARNINGS
        || bookingAccount.accountingRelation === ACCOUNTING_RELATIONS.PROFITANDLOSSEXPENSES;

      if (isProfitOrLoss) {
        bookingForm?.get('defineInvoice').enable({ emitEvent: false });
      } else {
        bookingForm?.get('defineInvoice').disable({ emitEvent: false });
      }

      const discountControl: AbstractControl = form.get('discount');

      bookingAccount.debitorCreditorType === DEBITOR_CREDITOR.CREDITOR
        ? (
          !bookInvoiceMode && discountControl.patchValue(0),
          discountControl.enable({ emitEvent: false })
        )
        : (
          discountControl.disable({ emitEvent: false }),
          !bookInvoiceMode && discountControl.patchValue(null)
        );

      // https://navido.atlassian.net/wiki/spaces/TE/pages/376274945/Booking+form
      // rule No 62
      if (!bookInvoiceMode) {
        const invoiceNoCtrl: AbstractControl = form.get('invoiceNo');
        bookingAccount.debitorCreditorType === DEBITOR_CREDITOR.NONE
          ? invoiceNoCtrl.disable({ emitEvent: false })
          : invoiceNoCtrl.enable({ emitEvent: false });
      }

      this.initDiscountAndPaymentDate(form, bookingAccount, bookInvoiceMode);

      this.initAccrualAccountingValue(form, bookingAccount);

      const useAccrualAccountingForRow = bookingAccount &&
        [ACCOUNTING_RELATIONS.PROFITANDLOSSEARNINGS,
        ACCOUNTING_RELATIONS.PROFITANDLOSSEXPENSES]
          .includes(bookingAccount.accountingRelation);

      useAccrualAccountingForRow ?
        form.get('accrualAccounting').enable() :
        form.get('accrualAccounting').disable();
    } else {
      if (currencyIsoCodeControl.disabled && !bookInvoiceMode) {
        currencyIsoCodeControl.enable({ emitEvent: false });
      }
    }
  }

  // https://navido.atlassian.net/browse/NAVIDO-784
  // For Creditor account: Check related company cash discount period and automatically set payment date to last date of period
  private initDiscountAndPaymentDate(form: FormGroup, bookingAccount: IBookingAccount, bookInvoiceMode: boolean) {
    if (bookingAccount && bookingAccount.debitorCreditorType === DEBITOR_CREDITOR.CREDITOR) {
      const bookingForm: FormGroup = this.bookingFormService.bookingFormSubject.value;
      const invoiceDate = bookingForm?.get('invoiceDate').value;

      const foundDebitorCreditor = this.debitorCreditorRepo.getStreamValue()
        .find(debitorCreditor => debitorCreditor.creditorAccountId === bookingAccount.id);
      if (foundDebitorCreditor) {
        const foundPaymentCondition = this.bookingResourceService
          .paymentConditions
          .value
          .find(paymentCondition => paymentCondition.debitorCreditorId === foundDebitorCreditor.id);
        if (foundPaymentCondition) {
          this.updatePaymentConditionsOfAccount(bookingAccount.id, foundPaymentCondition);
          const suggestedPaymentDate = this.debitorCreditorPaymentConditionsService
            .getSuggestedPaymentDate(foundPaymentCondition, invoiceDate);
          if (suggestedPaymentDate) {
            const suggestedPaymentConditionLevel = this.debitorCreditorPaymentConditionsService
              .getPaymentConditionLevelForPaymentDate(
                foundPaymentCondition,
                invoiceDate,
                suggestedPaymentDate
              );

            const currentDiscountControl = form.get('discount');

            if (!currentDiscountControl.value && currentDiscountControl.value !== 0) {
              currentDiscountControl.patchValue(+suggestedPaymentConditionLevel?.percentageDiscount || 0);
            }

            if (!bookInvoiceMode) {
              const paymentDateControl = form.get('paymentDate');
              paymentDateControl.patchValue(suggestedPaymentDate);
            }
          }
        } else {
          this.updatePaymentConditionsOfAccount(bookingAccount.id, null);
        }
      }
    }
  }

  /**
   * sets rate control's value to 1 if current currency id is same as mandator currency id
   * @param {number} currencyId selected currency id
   * @param {number} mandatorCurrencyId current mandator currency id
   * @param {AbstractControl} rateControl current mandator currency id
 */
  public setRate = (
    currencyId: number,
    mandatorCurrencyId: number,
    rateControl: AbstractControl
  ): void => {
    if (currencyId === mandatorCurrencyId) {
      rateControl.patchValue(1);
      rateControl.disable({ emitEvent: false });
    } else {
      rateControl.enable({ emitEvent: false });
      this.exchangeRateService.exchangeRates
        .pipe(
          skipWhile((res) => !res.length),
          take(1),
        )
        .subscribe(() => {
          const exchangeRateUserCurr = this.exchangeRateService.getRate(currencyId);
          let exchangeRateLC = 1;
          const rateLC = this.exchangeRateService.getRate(mandatorCurrencyId);
          if (rateLC) {
            exchangeRateLC = rateLC.rate;
          }
          if (exchangeRateUserCurr && exchangeRateLC) {
            let rate: number;
            if (rateLC && rateLC.isoCode === 'EUR') {
              rate = (exchangeRateUserCurr.rate * exchangeRateLC);
            } else {
              rate = (exchangeRateUserCurr.rate / exchangeRateLC);
            }
            rateControl.patchValue(rate);
            rateControl.parent.get('amountTransaction').updateValueAndValidity();
            rateControl.updateValueAndValidity({ emitEvent: false });
          }
        });
    }
  }

  public updatePaymentConditionsOfAccount(bookingAccountId: number, paymentCondition: IPaymentCondition) {
    const invoiceDate = this.bookingFormService
      .bookingFormSubject
      .value
      ?.get('invoiceDate').value;
    const discounts: IPaymentStepRow[] = getDiscounts(paymentCondition, invoiceDate);

    discounts.forEach(discount =>
      discount.paymentDateLabel = new ToCurrentDateFormatPipe(this.settingsService)
        .transform(discount.paymentDate));

    this.bookingResourceService.paymentConditionsOfAccounts.next(
      {
        ...this.bookingResourceService.paymentConditionsOfAccounts.value,
        [bookingAccountId]: discounts
      });
  }

  public isAccountingTypeValid = (validAccountingType: number, toCheckAccountingType: number) => {
    return validAccountingType === (validAccountingType & toCheckAccountingType);
  }

  public onPlusMinusChange = (form: FormGroup) => {
    const plusMinus = form.get('+/-').value;
    let debitCreditTypeNewValue: 1 | -1;
    const currentFormat = this.settingsService.debitCredit;
    let correctValue: string;
    const debitValues = ['+', 'S', 's', 'D', 'd'];
    const creditValues = ['-', 'H', 'h', 'C', 'c'];

    if (debitValues.includes(plusMinus)) {
      correctValue = currentFormat === DEBIT_CREDIT.plusMinus
        ? '+'
        : wording.accounting.DEBIT[this.settingsService.language];
      debitCreditTypeNewValue = 1;
    } else if (creditValues.includes(plusMinus)) {
      correctValue = currentFormat === DEBIT_CREDIT.plusMinus
        ? '-'
        : wording.accounting.CREDIT[this.settingsService.language];
      debitCreditTypeNewValue = -1;
    } else {
      correctValue = null;
      debitCreditTypeNewValue = null;
    }

    if (plusMinus !== correctValue) {
      return form.get('+/-').patchValue(correctValue);
    }
    return form.get('debitCreditType').patchValue(debitCreditTypeNewValue);
  }

  public onDiscountChange = (form: FormGroup) => {
    const bookingAccountId = form.get('bookingAccountId').value;
    const discountControl = form.get('discount');
    const discountsOfAccount = this.bookingResourceService
      .paymentConditionsOfAccounts
      .value[bookingAccountId] || [];

    const foundDiscount = discountsOfAccount
      .find(discountOfAccount => discountOfAccount.percentage === +discountControl.value);
    if (foundDiscount && discountControl.enabled) {
      form.get('paymentDate').patchValue(foundDiscount.paymentDate);
    } else if (discountControl.value !== 0 && discountControl.enabled) {
      discountControl.patchValue(0);
    }
  }

  private isBookingInReadOnlyMode(): boolean {
    return !!this.route.snapshot.queryParamMap.get('readOnly');
  }

  private isBookingInCloneMode(): boolean {
    return !!this.route.snapshot.queryParamMap.get('clones');
  }

  private isBookingInEditMode(): boolean {
    const id: BookingKey = +this.router.url.split('/').pop();
    return !isNaN(id);
  }
}
