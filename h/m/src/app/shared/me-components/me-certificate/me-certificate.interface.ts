import { MeContextMenuI } from '../me-context-menu/me-context-menu-inteface';

export interface MeCertificateI {
  certDocument: string;
  certificateOid: string;
  certDocumentExtension: string;
  contextMenu: MeContextMenuI;
  description: string;
  expirationDate: string;
  group: string;
  issuedDate: string;
  link: string;
  urn: string;
  logoDefault: string;
  mediaFiles: string;
  name: string;
  oid: string;
  active: boolean;
  companyCertificateName: string;
}
