import { NvLocale } from '../dateFormat';
import { Language } from '../language';
import { DEBIT_CREDIT } from '../enums/debitCredit';
import { Theme } from '../Theme.enum';

interface ISharedProps {
  language: Language;
  locale: NvLocale;
  debitCredit: DEBIT_CREDIT;
  avatarId: string;
  theme: Theme;
}

export interface IAccountConfigGeneralCreate extends ISharedProps { }

export interface IAccountConfigGeneralBase extends ISharedProps { }

export interface IAccountConfigGeneralDTO extends IAccountConfigGeneralBase { }

export interface IAccountConfigGeneral extends IAccountConfigGeneralDTO { }
