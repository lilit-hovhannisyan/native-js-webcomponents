import { Pipe, PipeTransform } from '@angular/core';
import { MeTranslationService } from '@core-services/me-translation.service';

@Pipe({
  name: 'translate'
})
export class TranslatePipe implements PipeTransform {
  constructor(private meTranslationService: MeTranslationService) {}

  transform(key: string, params?: { [param: string]: number | string }): string {
    return this.meTranslationService.translate(key, params);
  }
}
