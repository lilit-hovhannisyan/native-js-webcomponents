import { Component, OnInit, Input } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup } from '@angular/forms';
import { IDebitorCreditorRow } from 'src/app/core/models/resources/IDebitorCreditor';
import { DebitorCreditorMainService } from 'src/app/core/services/mainServices/debitor-creditor-main.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'nv-debitor-creditor-autocomplete',
  templateUrl: './debitor-creditor-autocomplete.component.html',
  styleUrls: ['./debitor-creditor-autocomplete.component.scss']
})
export class DebitorCreditorAutocompleteComponent implements OnInit {
  public wording = wording;
  public gridConfig = null;

  @Input() public form: FormGroup;
  @Input() public controlName = 'debitorCreditorId';
  @Input() public filters: object;
  public stream$: Observable<IDebitorCreditorRow[]>;
  constructor(
    public debitorCreditorMainService: DebitorCreditorMainService,
  ) { }

  public ngOnInit() {
    this.stream$ = this.debitorCreditorMainService.fetchAllDebitorsCreditorsRows();
  }
}
