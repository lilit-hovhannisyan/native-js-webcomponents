import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';

export type BunkerKey = number;

interface ISharedProps extends IWithLabels, IWithLabel {
  code: string;
  isActive: boolean;
}

export interface IBunkerBase extends ISharedProps, IResource<number> { }

export interface IBunkerCreate extends ISharedProps { }

export interface IBunkerDTO extends IBunkerBase { }

export interface IBunker extends IBunkerDTO { }

export interface IBunkerRow extends IBunker { }
