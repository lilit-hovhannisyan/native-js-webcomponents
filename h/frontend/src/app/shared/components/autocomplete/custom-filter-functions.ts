import { IAutocompleteCustomOptionsFilter } from './autocomplete.component';
import { IPaymentStepRow } from 'src/app/core/models/resources/IPaymentConditions';

const discoutDisplayValuePattern: RegExp = /(^\d{1,2} \d{1,2}.\d{1,2}.\d{4}$)/;
const discountOnlyPattern: RegExp = /(^\d{0,2})$/;

/**
 * Credits: https://stackoverflow.com/a/36566052
 */
const editDistance = (str1: string, str2: string): number => {
  str1 = str1.toLowerCase();
  str2 = str2.toLowerCase();

  const rates: number[] = [];
  for (let i = 0; i <= str1.length; i++) {
    let lastValue = i;
    for (let j = 0; j <= str2.length; j++) {
      if (i === 0) {
        rates[j] = j;
      } else {
        if (j > 0) {
          let newValue = rates[j - 1];
          if (str1.charAt(i - 1) !== str2.charAt(j - 1)) {
            newValue = Math.min(Math.min(newValue, lastValue), rates[j]) + 1;
          }
          rates[j - 1] = lastValue;
          lastValue = newValue;
        }
      }
    }
    if (i > 0) {
      rates[str2.length] = lastValue;
    }
  }
  return rates[str2.length];
};

/**
 * Credits: https://stackoverflow.com/a/36566052
 */
const similarity = (searchText: string, target: string): number => {
  let longer = searchText;
  let shorter = target;
  if (searchText.length < target.length) {
    longer = target;
    shorter = searchText;
  }
  const longerLength = longer.length;
  if (longerLength === 0) {
    return 1.0;
  }
  return (longerLength - editDistance(longer, shorter)) / parseFloat(longerLength.toFixed(4));
};

const findClosestAvailableKey = (map: Map<number, IPaymentStepRow>, key: number): number => {
  while (true) {
    key -= 0.01;
    if (!map.has(key)) {
      return key;
    }
  }
};

/**
 * returns discount rates sorted by best match
 * @param searchText
 * @param paymentRows
 */
const sortByBestMatch = (searchText: string, paymentRows: IPaymentStepRow[]): IPaymentStepRow[] => {
  const discountsMap = new Map<number, IPaymentStepRow>([]);
  paymentRows.forEach(row => {
    const percentageString = row.percentage.toString();
    let similarityRate: number = similarity(searchText, percentageString);
    // we want to skip items with 0 rate ⛔
    if (similarityRate) {
      // If key already exists we need to get the closest key
      if (discountsMap.has(similarityRate)) {
        similarityRate = findClosestAvailableKey(discountsMap, similarityRate);
      }
      discountsMap.set(similarityRate, row);
    }
  });
  const sortedMap = new Map([...discountsMap.entries()].sort().reverse());
  return [...sortedMap.values()];
};

/**
 * Filters discount options for autocomplete based on user input
 * @param paymentRows
 * @param searchValue
 */
export const autocompleteDiscountOptionsFilter: IAutocompleteCustomOptionsFilter = (
  paymentRows: IPaymentStepRow[],
  searchValue: string | number
): IPaymentStepRow[] => {
  const searchValueTrimmed = searchValue.toString().trim();

  // If we have a valid exact display value we find and return it
  // example: `9 20.06.2020`
  if (!!searchValueTrimmed.match(discoutDisplayValuePattern)) {
    const exactMatch = paymentRows.find(row => {
      const rowFormatted = `${row.percentage} ${row.paymentDateLabel}`;
      return rowFormatted === searchValue;
    });
    if (exactMatch) {
      return [exactMatch];
    }
  }

  // If user is searching/typing a discount we return all matches sorted by best match
  // checks discount inputs only `x` or `xy` where x is a positive integer
  if (!!searchValueTrimmed.match(discountOnlyPattern)) {
    return sortByBestMatch(searchValueTrimmed, paymentRows);
  }

  return [];
};
