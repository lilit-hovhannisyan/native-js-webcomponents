import {GitUserSingle} from './page-components/git-user-single-page/git-user-single-page.component.js';
import {GitRepo} from './shared-components/git-repo/git-repo.component.js';
import {GitAvatar} from './shared-components/git-avatar/git-avatar.component.js';
import {GitRepos} from './page-components/git-repos-page/git-repos-page.component.js';
import {GitUsers} from './page-components/git-users-page/git-users-page.component.js';
import {AppRoot} from './app-base-components/app-root/app-root.component.js';
import {Body} from './app-base-components/body/body.component.js';
import {Header} from './app-base-components/header/header.component.js';
import {Navigation} from './shared-components/navigation/navigation.component.js';
import {Pagination} from './shared-components/pagination/pagination.component.js';
import {PopupManagaer} from './shared-components/popup-manager/popup-manager.component.js';

export const declareGitAppComponents = () => {
  customElements.define('git-app-pagination', Pagination);
  customElements.define('git-app-navigation', Navigation);
  customElements.define('git-app-header', Header);
  customElements.define('git-app-body', Body);
  customElements.define('git-app-root', AppRoot);
  customElements.define('git-users', GitUsers);
  customElements.define('git-user-single', GitUserSingle);
  customElements.define('git-repos', GitRepos);
  customElements.define('git-avatar', GitAvatar);
  customElements.define('git-repo', GitRepo);
  customElements.define('git-popup-manager', PopupManagaer);
}