import {Component, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {dateFormat} from '../../services/dateFormat.service';
import {Import} from '@angular/compiler-cli/src/ngtsc/host';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'lavash-media-control',
  templateUrl: './media-control.component.html',
  styleUrls: ['./media-control.component.less']
})
export class MediaControlComponent {
  @Input() mediaType;
  @Output() playEvent = new EventEmitter();
  @Output() pauseEvent = new EventEmitter();
  @Output() stopEvent = new EventEmitter();

  // progress
  _progress;
  progressPercentage;
  dateFormat = dateFormat;
  playBtnActive = true;


  // --voice
  showVoiceSlider: boolean;
  voiceProgressPercentage = 100;


  // --fullscreen
  fullscreen = false;
  fullScreenProportion;

  // speed
  @Input() speedText = 'Speed:';
  @Input() hasSpeedControl;
  speedOptions = [.5, .6, .7, .8, .9, 1, 1.2, 1.4, 1.6, 1.8, 2];
  showSpeedSlider: boolean;
  hoverSpeed = 1;
  _currentSpeed;

  getMediaType() {
      return (this.mediaType instanceof Audio) ? 'audio' : 'video';
  }

  calcDuration(duartionTimeStamp) {
      // todo mi hat nayel jamy vor poxancum es inchi a mish 04 veradarznum
      return dateFormat(duartionTimeStamp, 'MM:SS');
  }

  onPlay(mediaType, fromCurrentTime?: number, equilizer?) {
      if (fromCurrentTime) {
          console.log('media up', fromCurrentTime)
          // mediaType.currentTime = fromCurrentTime * event / 100;
      }
      this.playBtnActive = false;
      mediaType.play();

      this._progress = setInterval(() => {
          const progress = mediaType.currentTime;
          this.progressPercentage = progress * 100 / mediaType.duration;

          if (this.progressPercentage >= 100) {
              clearInterval(this._progress);
              this.progressPercentage = 100;
              this.playBtnActive = true;
              this.stopEvent.emit();
          }

      }, 200);

      this.playEvent.emit();
  }

  onPause(mediaType, stop = false) {
      this.playBtnActive = true;
      mediaType.pause();

      if (stop) {
          mediaType.currentTime = 0;
          this.progressPercentage = 0;
          this.stopEvent.emit();
      }

      this.pauseEvent.emit();
  }

  onProgressChange(event, mediaType) {
      mediaType.currentTime = mediaType.duration * event / 100;
      this.onPlay(mediaType);
  }

  // --voice && speed
  onShowSlider(sliderNameState, state?: boolean) {
    this[sliderNameState] = state ? state : !this[sliderNameState];
  }

  // --voice
  controlVoice(mediaType) {
      mediaType.muted = !mediaType.muted;
      mediaType.volume = mediaType.muted ? 0 : 1;
      this.voiceProgressPercentage = mediaType.muted ? 0 : 100;
  }

  onVoiceProgressChange(event, mediaType) {
      mediaType.volume = event <= 8 ? 0 : event / 100;
      this.voiceProgressPercentage = event <= 8 ? 0 : this.voiceProgressPercentage;
      mediaType.volume = mediaType.volume < 0 ? 0 : mediaType.volume > 1 ? 1 : mediaType.volume;
      this.mediaType.muted = !Boolean(mediaType.volume);
  }

  // --speed
  onSpeedHover(speed, reverse?: boolean) {
      if (reverse) {
          this.hoverSpeed = this._currentSpeed;
      } else {
          this._currentSpeed = this.hoverSpeed;
          this.hoverSpeed = speed;
      }
  }

  onSpeedClick(speed) {
      this.hoverSpeed = speed;
      this._currentSpeed = speed;
      this.mediaType.playbackRate = speed;
  }

  // fullscreen
  requestFullScreen(mediaContainer) {
      // this.mediaTypeContainer.requestFullscreen();
      // request fullscreen for container not the video not to loose custom controls
      const requestFullscreenPrefixList = [
          'requestFullscreen',
          'msRequestFullscreen',
          'mozRequestFullscreen',
          'webkitRequestFullscreen']; // browser prefixes

      const elem = mediaContainer;

      for (const requestFullscreenPrefix of requestFullscreenPrefixList) {
          if ( elem[requestFullscreenPrefix]) {
              elem[requestFullscreenPrefix]();
              break;
          }
      }

      this.fullScreenProportion = this.mediaType.offsetWidth >= this.mediaType.offsetHeight ? 'l-width-strech' : 'l-height-strech';
  }

  cancelFullScreen(mediaContainer) {
      const exitFullscreenPrefixList = [
          'exitFullscreen',
          'mozCancelFullScreen',
          'webkitExitFullscreen',
          'msExitFullscreen']; // browser prefixes

      const elem = document;
      for (const exitFullscreenPrefix of exitFullscreenPrefixList) {
          if ( elem[exitFullscreenPrefix]) {
              elem[exitFullscreenPrefix]();
              break;
          }
      }
  }

  escapeFullScreen(event) {
      this.fullscreen = !this.fullscreen;
  }

  onArrowToggle(event) {
      if (this.fullscreen && event.e.keyCode === 39) {
          console.log(event)
          const currentTime = this.mediaType.currentTime + 10 > this.mediaType.duration ?
                              this.mediaType.duration :
                              this.mediaType.currentTime + 10;

          this.mediaType.currentTime = currentTime;
          this.progressPercentage = currentTime * 100 / this.mediaType.duration;
      }

      if (this.fullscreen && event.e.keyCode === 37) {
          const currentTime = this.mediaType.currentTime - 10 < 0 ?
                              0 :
                              this.mediaType.currentTime - 10;

          this.mediaType.currentTime = currentTime;
          this.progressPercentage = currentTime * 100 / this.mediaType.duration;
      }
  }
}




function FakePromiseFunction( callback ) {
    this.value = undefined;
    this.status = 'pending';

    const updatePromise = (value, status) => {
        setTimeout( () => {
            if ( this.status !== 'resolved' ) {
                this.value = value;
                this.status = status;
            }
        }, 0);
    }

    const resolve = (data) => {
        const value = FakePromiseFunction.resolve(data);
        if (value) updatePromise(value, 'resolved');
    }

    const reject = (data) => {
        const value = FakePromiseFunction.reject(data);
        updatePromise(value, 'rejected');
    }

    callback(resolve, reject);
}

FakePromiseFunction.prototype = {
    then( functionSuccess, functionError ) {
        setTimeout ( () => {
            if (this.status !== 'resolved' && !functionError) return this;
            const callback = this.status === 'resolved' ? functionSuccess : functionError;

            setTimeout(callback(this.value), 0);
        }, 0);

        return this;
    },

    catch(functionError) {
        setTimeout ( () => {
            if (this.status === 'rejected') functionError(this.value);
        }, 0);

        return this;
    },

    finally(functionFinally) {
        setTimeout ( () => {
            functionFinally();
        }, 0);
    },
}

FakePromiseFunction.resolve = function(fakePromiseDataProducer){
    return fakePromiseDataProducer;
}

FakePromiseFunction.reject = function(fakePromiseDataProducer) {
    return fakePromiseDataProducer;
}


var fakePromiseFunction = new FakePromiseFunction( (a, b) => {
    const activate = false;
    if (activate === true) {
        a("I'm successful");
    } else if (activate === false) {
        b("I'm a failure.");
    } else {
        throw new Error("Oh this is not good.");
    }
})

console.log('hey', fakePromiseFunction)

fakePromiseFunction
    .then(
        value => console.log('fake func: then success', value),
        err => console.log('fake func: then error', err)
    )
    .catch( err => {
        console.log('fake func: cathc', err)
    })
    .finally( () => {
        console.log('fake func: finally')
    });