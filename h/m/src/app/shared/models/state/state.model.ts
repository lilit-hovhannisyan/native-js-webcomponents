export class StateModel {
  defaultState: boolean = false;
  displayName: string = '';
  internalName: string = '';
  stateType: string = '';
  description: string = '';
}
