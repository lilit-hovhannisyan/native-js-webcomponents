import { NvColumnDataType, NvFilterControl, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { IResource } from 'src/app/core/models/resources/IResource';
import { StevedoreDamageReportStatus } from 'src/app/core/models/resources/IStevedoreDamageReport';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { NvAction } from 'nv-grid/lib/components/nv-button/nv-button.component';

export interface IStevedoreDamageReportGridConfig extends NvGridConfig {
  toolbarButtons: {
    title?: Object | string;
    tooltip?: Object | string;
    icon: string;
    func?: NvAction | ((any: any) => any);
    disabled?: () => boolean;
    hidden?: () => boolean;
    class?: string;
    status?: StevedoreDamageReportStatus;
  }[];
}

export const getStevedoreDamageReportGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations: Operations) => boolean
): IStevedoreDamageReportGridConfig => {
  return ({
    gridName: 'StevedoreReportGridConfig',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    showExcelButton: canPerform(Operations.UPDATE),
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'vesselName',
        title: wording.technicalManagement.vesselName,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'vesselNo',
        title: wording.technicalManagement.vesselNo,
        width: 116,
        dataType: NvColumnDataType.String,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'statusLabel',
        title: wording.general.status,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'reportNo',
        title: wording.technicalManagement.reportNo,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'reportTypeLabel',
        title: wording.technicalManagement.reportType,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'reportDate',
        title: wording.technicalManagement.reportDate,
        width: 116,
        isSortable: true,
        dataType: NvColumnDataType.Date,
        filter: {
          values: []
        },
      },
      {
        key: 'incidentLabel',
        title: wording.technicalManagement.typeOfDamage,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'charterer',
        title: wording.technicalManagement.charterer,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'voyageNo',
        title: wording.technicalManagement.voyageNo,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'repairByLabel',
        title: wording.technicalManagement.repairBy,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'expenses',
        title: wording.technicalManagement.expenses,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        },
        dataType: NvColumnDataType.Decimal
      },
      {
        key: 'iso',
        title: wording.technicalManagement.ISO,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'isInvoiceWaivedAndSetToCleared',
        title: wording.technicalManagement.invoiceWaived,
        width: 116,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean,
      },
      {
        key: 'voyageEnd',
        title: wording.technicalManagement.voyageEnd,
        width: 116,
        isSortable: true,
        dataType: NvColumnDataType.Date,
        filter: {
          values: []
        },
      },
      {
        key: 'invoiceNo',
        title: wording.technicalManagement.invoiceNo,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'paymentDate',
        title: wording.technicalManagement.paymentDate,
        width: 116,
        isSortable: true,
        dataType: NvColumnDataType.Date,
        filter: {
          values: []
        }
      },
      {
        key: 'resubmissionDate',
        title: wording.technicalManagement.resubmissionDate,
        width: 116,
        isSortable: true,
        dataType: NvColumnDataType.Date,
        filter: {
          values: []
        }
      },
      {
        key: 'portName',
        title: wording.accounting.port,
        width: 120,
        isSortable: true,
        dataType: NvColumnDataType.String,
        filter: {
          values: []
        }
      },
      {
        key: 'invoiceApprovedOn',
        title: wording.technicalManagement.releaseDate,
        width: 116,
        isSortable: true,
        dataType: NvColumnDataType.DateTime,
        filter: {
          values: []
        },
      },
      {
        key: 'invoiceApprovedByUserFullName',
        title: wording.technicalManagement.releasedBy,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'invoicePaidOn',
        title: wording.technicalManagement.clearedDate,
        width: 120,
        isSortable: true,
        filter: {
          values: []
        },
        dataType: NvColumnDataType.DateTime
      },
      {
        key: 'createdOn',
        title: wording.technicalManagement.createdOn,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        },
        dataType: NvColumnDataType.DateTime
      },
      {
        key: 'createdByUserFullName',
        title: wording.technicalManagement.createdBy,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'startOfTimeCharter',
        title: wording.technicalManagement.startOfTimeCharter,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'approximateDateOfRedelivery',
        title: wording.technicalManagement.approximateDateOfRedelivery,
        width: 116,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editStevedoreReport',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (entity: IResource<number>) => actions.edit(entity.id),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteStevedoreReport',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: (entity: IResource<number>) => actions.delete(entity),
      }
    ],
    toolbarButtons: [
      {
        class: 'toolbar-button-rating-required toolbar-red-button',
        title: wording.technicalManagement.ratingRequired,
        icon: '',
        func: () => actions.select(StevedoreDamageReportStatus.RatingRequired),
        status: StevedoreDamageReportStatus.RatingRequired,
      },
      {
        class: 'toolbar-button-release-required toolbar-red-button',
        title: wording.technicalManagement.approvalRequired,
        icon: '',
        func: () => actions.select(StevedoreDamageReportStatus.ReleaseRequired),
        status: StevedoreDamageReportStatus.ReleaseRequired,
      },
      {
        class: 'toolbar-button-in-progress toolbar-red-button',
        title: wording.technicalManagement.invoiceRequired,
        icon: '',
        func: () => actions.select(StevedoreDamageReportStatus.InProgress),
        status: StevedoreDamageReportStatus.InProgress,
      },
      {
        class: 'toolbar-button-invoice-created toolbar-red-button',
        title: wording.technicalManagement.administratorTabInvoiceCreated,
        icon: '',
        func: () => actions.select(StevedoreDamageReportStatus.InvoiceCreated),
        status: StevedoreDamageReportStatus.InvoiceCreated,
      },
      {
        class: 'toolbar-button-cleared toolbar-red-button',
        title: wording.technicalManagement.invoiceCleared,
        icon: '',
        func: () => actions.select(StevedoreDamageReportStatus.Cleared),
        status: StevedoreDamageReportStatus.Cleared,
      },
      {
        class: 'toolbar-button-create',
        title: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: () => actions.add(),
        status: null,
      }
    ],
    contextMenuButtons: canPerform(Operations.READ) ? getContextMenuButtonsConfig() : [],
  });
};
