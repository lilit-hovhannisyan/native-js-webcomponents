import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { NgModule } from '@angular/core';
import { MatRadioModule } from '@angular/material/radio';

import { IconsModule } from '@shared/modules/icons.module';
import { MeComponentLoadingModule } from '@shared/me-directives/me-component-loader/me-component-loader.module';

import { MeButtonModule } from '../me-button/me-button.module';
import { MePopupStatusChangeComponent } from './me-popup-status-change.component';

@NgModule({
  declarations: [MePopupStatusChangeComponent],
  imports: [CommonModule, MeButtonModule, MatDialogModule, MatRadioModule, IconsModule, FormsModule, MeComponentLoadingModule],
  exports: [MePopupStatusChangeComponent]
})
export class MePopupStatusChangeModule {}
