import { Type } from 'class-transformer';
import { UserModel } from '@shared-models/user/user.model';
import { CompanyModel } from '@shared-models/company/company.model';
import { PermissionModel } from '../permission/permission.model';

export class UserProfileModel {
  @Type(() => UserModel)
  user: UserModel = new UserModel();
  @Type(() => CompanyModel)
  company: CompanyModel = new CompanyModel();
  permissions: PermissionModel[] = [];
}
