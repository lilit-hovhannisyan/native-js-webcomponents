import { Input, OnInit, Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { AddressKey, IAddress } from '../../../core/models/resources/IAddress';
import { getAddressGridConfig } from './addressGridConfig';
import { AddressDetailViewComponent } from './detail-view/address-detail-view.component';
import { DefaultModalOptions } from '../../../core/constants/modalOptions';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { AddressRepositoryService } from '../../../core/services/repositories/address-repository.service';
import { IAddressRow, AddressMainService } from '../../../core/services/mainServices/address-main.service';
import { NvGridConfig, GridComponent } from 'nv-grid';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  selector: 'nv-editable-address-list',
  templateUrl: './editable-address-list.component.html',
  styleUrls: ['./editable-address-list.component.scss'],
})
export class EditableAddressListComponent implements OnInit {

  @Input() public companyId: number;
  @Input() public shouldFirstRowBeSelected: boolean;
  @Input() public isSelectable: boolean;
  @Output() public addressRowSelectionChanged: EventEmitter<IAddressRow | null> = new EventEmitter<IAddressRow | null>();
  @ViewChild('gridComponent', { static: true }) public gridComponent: GridComponent;

  public addressesGridConfig: NvGridConfig;
  private addressesSubject: ReplaySubject<IAddressRow[]> = new ReplaySubject<IAddressRow[]>(1);
  public dataSource$: Observable<IAddressRow[]> = this.addressesSubject.asObservable();
  private allAddresses: IAddress[];
  private modalRef?: NzModalRef;

  constructor(
    private mainService: AddressMainService,
    private addressRepo: AddressRepositoryService,
    private modalService: NzModalService,
    public gridConfigService: GridConfigService,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private settingsService: SettingsService,
  ) { }

  public ngOnInit(): void {
    const params = { companyId: this.companyId };

    this.mainService.getStream(params)
      .pipe(
        map((addersses) => this.mainService.toRows(addersses)),
      ).subscribe(addressesRows => {
      if (
        this.shouldFirstRowBeSelected
        && addressesRows.length > 0
      ) {
        addressesRows[0]._checked = true;
      }
      this.addressesSubject.next(addressesRows);
      this.allAddresses = addressesRows;
    });

    const actions = {
      edit: (row: IAddressRow) => this.openDetailViewModal(row.id),
      delete: (row: IAddressRow) => this.deleteRowClicked(row),
      add: () => this.openDetailViewModal(),
    };

    const addressNode = sitemap.system.children.misc.children.companies.children.id.children.addresses;
    this.addressesGridConfig = getAddressGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnNode(operation, addressNode),
      this.isSelectable,
    );
  }

  public openDetailViewModal(addressId?: AddressKey): void {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.general.addresses[this.settingsService.language],
      nzClosable: false,
      nzContent: AddressDetailViewComponent,
      nzComponentParams: {
        addressId: addressId,
        companyId: this.companyId,
        closeModal: (saveClicked) => {
          this.modalRef.destroy();
          if (saveClicked) {
            this.addressRepo.fetchAll({
              queryParams: { companyId: this.companyId },
            }).subscribe();
          }
        },
      },
      nzFooter: null,
    });
  }

  public deleteRowClicked = (address: IAddress): void => {
    const allOfType = this.allAddresses.filter(addr => addr.addressTypeId === address.addressTypeId);
    if (allOfType.length > 2 && address.isTypeDefault) {
      this.confirmationService.warning({
        wording: wording.system.deleteDefaultAddressMessage,
        cancelButtonWording: null,
        okButtonWording: wording.general.ok,
      });
    } else {
      this.confirmationService.confirm({
        wording: wording.system.addressDeleteConfirmationMessage,
      }).subscribe(confirmed => {
        if (!confirmed) { return; }
        /**
         * 1. Every not-default address can be deleted.
         * 2. When there are exactly 2 addresses, which belong to the same type
         * - then on time of deletion the default-address, another one has to be set default.
         * 3. When there are more addresses than 2, which belong to the same type
         * - then that default address can't be deleted.
         */
        if (!address.isTypeDefault || allOfType.length === 1) {
          this.delete(address.id);
        } else if (allOfType.length === 2) {
          const notDefaultAddress = allOfType.find(addr => addr.id !== address.id);
          this.addressRepo.update({
            ...notDefaultAddress,
            isTypeDefault: true,
          }, {}).subscribe(() => this.delete(address.id));
        }
      });
    }
  }

  public delete(addressId: AddressKey): void {
    this.addressRepo.delete(addressId, {}).subscribe();
  }

  public rowSelectionChanged(addresses: IAddressRow[]): void {
    this.addressRowSelectionChanged.emit(addresses[0]);
  }
}
