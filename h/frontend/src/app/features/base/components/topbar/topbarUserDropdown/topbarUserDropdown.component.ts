import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { IUser } from '../../../../../core/models/resources/IUser';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { wording } from 'src/app/core/constants/wording/wording';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { IGitVersion } from 'src/app/core/models/IGitVersion';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { AboutComponent } from 'src/app/shared/components/about/about.component';
import { SettingsService } from 'src/app/core/services/settings.service';
import { TranslatePipe } from 'src/app/shared/pipes/translate.pipe';
import { GitInfoService } from 'src/app/core/services/git-info.service';

@Component({
  selector: 'nv-topbar-user-dropdown',
  templateUrl: './topbarUserDropdown.component.html',
  styleUrls: ['./topbarUserDropdown.component.scss']
})
export class TopbarUserDropdownComponent implements OnInit {
  @Input() public user: IUser;
  @Output() public logout = new EventEmitter<void>();

  public wording = wording;
  public sitemap = sitemap;
  public url = url;
  public version: IGitVersion;
  private modalRef: NzModalRef;
  private translatePipe: TranslatePipe;

  constructor(
    private modalService: NzModalService,
    private settingsService: SettingsService,
    private gitInfoService: GitInfoService,
  ) { }

  public ngOnInit() {
    this.translatePipe = new TranslatePipe(this.settingsService);
    this.version = this.gitInfoService.gitInfo;
  }

  public showVersionInfo() {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzWidth: 'fit-content',
      nzTitle: `${this.translatePipe.transform(wording.general.about)} NAVIDO 2.0`,
      nzClosable: true,
      nzContent: AboutComponent,
      nzComponentParams: {
        closeModal: () => this.modalRef.destroy(),
        version: this.version,
      },
      nzWrapClassName: 'about-modal'
    });
  }
}
