import { IResource } from './IResource';
import { MandatorKey, IMandator } from './IMandator';

export type MandatorRemarkKey = number;

interface ISharedProps {
  mandatorId: MandatorKey;
  text: string;
  createdBy: number;
  createdAt: string; // ISO String
  editedBy: number;
  editedAt: string; // ISO String
  mandator: IMandator;
}

export interface IMandatorRemarkBase extends ISharedProps, IResource<MandatorRemarkKey> { }

export interface IMandatorRemarkCreate {
  mandatorId: MandatorKey;
  text: string;
}

export interface IMandatorRemarkDTO extends IMandatorRemarkBase { }

export interface IMandatorRemark extends IMandatorRemarkDTO { }

export interface IMandatorRemarkFull extends IMandatorRemark {
  // these include time, firstname + lastname
  createdByFull: string;
  editedByFull: string;
}
