import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeScrollToBottomDirective } from './me-scroll-to-bottom.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [MeScrollToBottomDirective],
  exports: [MeScrollToBottomDirective]
})
export class MeScrollToBottomModule {}
