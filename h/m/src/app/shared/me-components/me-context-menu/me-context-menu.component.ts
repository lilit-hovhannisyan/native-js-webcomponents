import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { MeButtonI } from '../me-button/me-button.interface';

import { MeContextMenuI } from './me-context-menu-inteface';
import { BaseLoggerComponent } from '@features/tracking-system/base-logger.component';

@Component({
  selector: 'me-context-menu',
  templateUrl: './me-context-menu.component.html',
  styleUrls: ['./me-context-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeContextMenuComponent extends BaseLoggerComponent implements OnInit {
  componentVersion: string = '0.0.1';

  @Input() dataModel!: MeContextMenuI;
  @Output() itemClick: EventEmitter<string> = new EventEmitter();
  contextMenuButton: MeButtonI = { type: 'transparent', color: 'secondary', rounded: true };

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {}

  onClick(key: string): void {
    this.itemClick.emit(key);
  }

  itemKey(_index: number, item: { key: string }): string {
    return item.key;
  }
}
