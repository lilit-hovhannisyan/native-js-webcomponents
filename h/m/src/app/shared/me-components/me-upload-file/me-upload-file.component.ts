import { Component, EventEmitter, Injector, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { BaseLoggerComponent } from '@features/tracking-system';
import { MeUploadFileI } from './me-upload-file.interface';

@Component({
  selector: 'me-upload-file',
  templateUrl: './me-upload-file.component.html',
  styleUrls: ['./me-upload-file.component.scss']
})
export class MeUploadFileComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  componentVersion: string = '0.0.1';

  @Input() uploadData!: MeUploadFileI;
  @Output() returnFormData: EventEmitter<File> = new EventEmitter();

  fileToUpload!: File | null;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    // disable drag & drop files on page, except on element that has a css class 'droparea'
    window.addEventListener('dragover', this.disableHandleEvent, false);
    window.addEventListener('drop', this.disableHandleEvent, false);
  }

  handleFileInput(event: Event): void {
    if (event.type === 'change') {
      const element = event.currentTarget as HTMLInputElement;
      const files: FileList | null = element.files;

      if (files) {
        if (this.uploadData.fileName) {
          this.fileToUpload = null;
        }

        this.fileToUpload = files.item(0);

        if (this.fileToUpload) {
          this.returnFormData.emit(this.fileToUpload);
        }
      }
    } else if (event.type === 'drop') {
      const element = event as DragEvent;
      const files: FileList | undefined = element.dataTransfer?.files;

      if (files) {
        if (this.uploadData.fileName) {
          this.fileToUpload = null;
        }

        this.fileToUpload = files.item(0);

        if (this.fileToUpload) {
          this.returnFormData.emit(this.fileToUpload);
        }
      }
    }
  }

  onDropped(event: Event): void {
    this.handleFileInput(event);
  }

  private disableHandleEvent(event: Event): boolean | void {
    const element = event.target as HTMLElement;
    return !element.classList.contains('droparea') && event && event.preventDefault();
  }

  ngOnDestroy(): object {
    window.removeEventListener('dragover', this.disableHandleEvent, false);
    window.removeEventListener('drop', this.disableHandleEvent, false);

    return super.ngOnDestroy();
  }
}
