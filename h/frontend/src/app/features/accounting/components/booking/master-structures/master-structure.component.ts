import { Component, OnInit, TemplateRef, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { forkJoin, ReplaySubject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';
import { BaseComponent } from 'src/app/core/abstracts/BaseComponent';
import { Validators } from 'src/app/core/classes/validators';
import { accountingTypeOptions, ACCOUNTING_TYPES } from 'src/app/core/models/enums/accounting-types';
import { masterStructureTypesOptions, MASTER_STRUCTURE_TYPES } from 'src/app/core/models/enums/master-structure-type';
import { Language } from 'src/app/core/models/language';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { IMasterStructure, StructureKey, IMasterStructureCreate } from 'src/app/core/models/resources/IMasterStructure';
import { IMasterStructureNodeTemplate } from 'src/app/core/models/resources/IMasterStructureNode';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { IMasterStructureFull } from '../../../../../core/models/resources/IMasterStructure';
import { StructureRepositoryService } from '../../../../../core/services/repositories/structure-repository.service';
import { MasterStructureBookingAccountService } from './master-structure-booking-accounts/master-structure-booking-account.service';
import { MasterStructureNodeComponent } from './master-structure-nodes/master-structure-node.component';
import { MasterStructureExportService } from './services/master-structure-export.service';
import { MasterStructureValidationService } from './services/master-structure-validation.service';
import { MasterStructureNodeHelperService } from './master-structure-nodes/services/master-structure-node-helper.service';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';

@Component({
  selector: 'nv-master-structures',
  templateUrl: './master-structure.component.html',
  styleUrls: ['./master-structure.component.scss']
})
export class MasterStructuresComponent extends BaseComponent implements OnInit, OnDestroy {
  @ViewChild('nodes') public nodesComponent: MasterStructureNodeComponent;
  @ViewChild('structureNameField') public structureNameField: ElementRef;

  public Language = Language;
  private language: Language;
  /**
   * This works only for MasterStructureNodes and have nothing to do with the general
   * language
   */
  public selectedLanguage: Language;
  /**
   * This is usually used for search to have access to all entities
   */
  private defaultStructures: IMasterStructureFull[] = [];
  public structures: IMasterStructureFull[] = [];
  public currentStructure: IMasterStructureFull;
  /**
   * We keep current structure id in a separate property to make two-way data binding more
   * easy with radio buttons group
   */
  public currentStructureId: number;
  public searchValue: string;
  public editMode: boolean;
  public creationMode: boolean;
  public changedNodes: IMasterStructureNodeTemplate[];
  public ACL_ACTIONS = ACL_ACTIONS;
  public canRead: boolean;
  public canCreate: boolean;
  public canDelete: boolean;
  public canUpdate: boolean;
  public form: FormGroup;
  public accountingTypeOptions: ISelectOption<ACCOUNTING_TYPES>[] = accountingTypeOptions;
  public masterStructureTypeOptions: ISelectOption<MASTER_STRUCTURE_TYPES>[] = masterStructureTypesOptions;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  private usedBookingAccounts: IBookingAccount[] = [];
  public unusedBookingAccounts: IBookingAccount[] = [];
  public isInActiveTooltipVisible: boolean;
  public exportWithAccounts = false;
  public downloadAs: 'excel' | 'pdf';
  public formElement: ElementRef;

  constructor(
    baseComponentService: BaseComponentService,
    private structureRepo: StructureRepositoryService,
    private fb: FormBuilder,
    private bookingAccountsService: MasterStructureBookingAccountService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    private nodesHelperService: MasterStructureNodeHelperService,
    private structureExportService: MasterStructureExportService,
    private structureValidationService: MasterStructureValidationService,
    private el: ElementRef
  ) {
    super(baseComponentService);
  }

  public ngOnInit(): void {
    this.language = this.settingsService.language;
    this.selectedLanguage = this.settingsService.language;
    this.onLanguageChange(this.selectedLanguage);

    this.definePermissions();
    this.initForm();
    this.fetchStructures();
    this.bookingAccountsService.fetchBookingAccounts()
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe();
    this.listenToBookingAccountChanges();
    this.listenToUsedBookingAccounts();
  }

  private definePermissions(): void {
    const url: string = this.router.url;
    this.canRead = this.structureValidationService.hasAccess(ACL_ACTIONS.READ, url);
    this.canCreate = this.structureValidationService.hasAccess(ACL_ACTIONS.CREATE, url);
    this.canDelete = this.structureValidationService.hasAccess(ACL_ACTIONS.DELETE, url);
    this.canUpdate = this.structureValidationService.hasAccess(ACL_ACTIONS.UPDATE, url);
  }

  private fetchStructures(): void {
    forkJoin([
      this.bookingAccountRepo.fetchAll({}),
      this.structureRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => this.structureRepo.getStream({})),
      takeUntil(this.componentDestroyed$)
    ).subscribe(structures => {
      if (structures.length > 0) {
        this.defaultStructures = this.defineStructures(structures);
        // we need to keep separate defaultStructure from structures, to have clean,
        // version of structures in case we need to rollback changes in editMode,
        // as structures does not have any methods inside and only contains array and
        // objects, JSON.parse and JSON.stringify fits well in this case
        this.structures = JSON.parse(JSON.stringify(this.defaultStructures));
        if (!this.currentStructureId) {
          this.currentStructure = { ...this.structures[0] };
          this.currentStructureId = this.currentStructure.id;
          this.confirmMessageOfRemovedAccounts();
        } else {
          this.currentStructure = this.structures
            .find(s => s.id === this.currentStructureId);
        }
      } else {
        this.defaultStructures = [];
        this.structures = [];
      }
      this.countBookingAccountNodes();
    });
  }

  private defineStructures(structures: IMasterStructure[]): IMasterStructureFull[] {
    return structures.map(structure => this.getStructureBookingAccounts(structure));
  }

  /**
   * This method gets used and unused bookingAccounts inside structure, so we could keep
   * track if structure is complete or not, usually we do it by showing an icon near to
   * the structure name in the template
   */
  private getStructureBookingAccounts(structure: IMasterStructure): IMasterStructureFull {
    const usedBookingAccounts: IBookingAccount[] = this.bookingAccountsService
      .getStructureUsedBookingAccounts(structure);
    const unusedBookingAccounts: IBookingAccount[] = this.bookingAccountsService
      .getStructureUnusedBookingAccounts(structure, usedBookingAccounts);
    return {
      ...structure,
      usedBookingAccounts,
      unusedBookingAccounts,
      fullTitle: this.buildStructureTitle(structure),
      titleWithoutStructureType: this.buildStructureTitle(structure, false)
    };
  }

  private buildStructureTitle(structure: IMasterStructure, withStructureType: boolean = true): string {
    const structureType: string = masterStructureTypesOptions
      .find(o => o.value === structure.masterStructureType).displayLabel[this.language];
    const accountingType: string = accountingTypeOptions
      .find(o => o.value === structure.accountingType).displayLabel[this.language];
    return withStructureType
      ? `${structure.name}
        (${structureType})
        (${accountingType})
        `
      : `${structure.name}
        (${accountingType})
        `;
  }

  public onLanguageChange(value: Language): void {
    this.language = value;
    this.structureExportService.setLanguage(this.language);
  }

  /**
   * isActive checkbox only can be true when structure is complete(i.e: all
   * bookingAccounts are linked to it), if structure is not complete we need to disable
   * isActive checkbox, that's why we listen here to bookingAccounts changes
   */
  private listenToBookingAccountChanges(): void {
    this.bookingAccountsService.bookingAccounts$
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => this.disableIsActiveCheckbox());
  }

  /**
   * We need to have the updated list of used bookingAccounts in edit mode, to be able to
   * add validation for accountingType and masterStructureType form fields changes,
   * check @method onStructureTypeChange() and @method onAccountingNormChange() .
   * Also on submit we update structure used and unused bookingAccounts with the new ones,
   * for which this helps too.
   */
  private listenToUsedBookingAccounts(): void {
    this.bookingAccountsService.usedBookingAccounts$
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(bookingAccounts => {
        this.usedBookingAccounts = Object.values(bookingAccounts);
      });
  }

  private disableIsActiveCheckbox(): void {
    const { isActive } = this.form.controls;
    this.unusedBookingAccounts = this.bookingAccountsService
      .getUnusedBookingAccounts();
    this.isInActiveTooltipVisible = this.unusedBookingAccounts.length > 0;
    if (this.unusedBookingAccounts.length > 0) {
      isActive.disable();
      if (isActive.value === true) {
        isActive.setValue(false);
      }
      if (this.currentStructure && this.editMode) {
        this.currentStructure.isActive = false;
      }
    } else {
      isActive.enable();
    }
  }

  /**
   * We need this method to filter bookingAccounts that are not linked to the current
   * structure, to show those in the modal
   */
  private countBookingAccountNodes(): void {
    if (this.currentStructure) {
      this.bookingAccountsService.setMasterStructure(this.currentStructure);
      this.bookingAccountsService.usedBookingAccountIdsSubject$.next([]);
      this.bookingAccountsService.addUsedBookingAccount(
        this.bookingAccountsService
          .getStructureUsedBookingAccounts(this.currentStructure)
          .map(structure => structure.id)
      );
    } else {
      this.bookingAccountsService.usedBookingAccountIdsSubject$.next([]);
    }
  }

  private initForm(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(60)]],
      isActive: [false],
      accountingType: [ACCOUNTING_TYPES.HGB],
      masterStructureType: [MASTER_STRUCTURE_TYPES.BUSINESS_MANAGEMENT_EVALUATION],
    });

    this.form.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(changes => {
        if (!this.editMode) {
          return;
        }

        this.currentStructure = {
          ...this.currentStructure,
          ...changes
        };
        this.currentStructure.fullTitle = this.buildStructureTitle(this.currentStructure);
        this.bookingAccountsService.setMasterStructure(this.currentStructure);
      });
  }

  public onStructureChange(id: StructureKey): void {
    this.currentStructure = { ...this.structures.find(s => s.id === id) };
    this.currentStructureId = this.currentStructureId;
    this.confirmMessageOfRemovedAccounts();
    this.countBookingAccountNodes();
  }

  private confirmMessageOfRemovedAccounts(): void {
    if (this.currentStructure.hasRemovedAccounts) {
      this.confirmationService.info({
        wording: this.wording.accounting.accountsHaveBeenRemoved,
        okButtonWording: this.wording.general.ok,
        cancelButtonWording: null,
      }).subscribe(() => {
        const payload: IMasterStructure = {
          ...this.currentStructure,
          hasRemovedAccounts: false,
          nodes: this.changedNodes,
        };
        this.structureRepo.update(payload, {}).subscribe();
      });
    }
  }

  public onSearch(): void {
    this.structures = this.defaultStructures
      .filter(s => s.name.toLowerCase().includes(this.searchValue.toLowerCase()));
  }

  public onNodeChange(nodes: IMasterStructureNodeTemplate[]): void {
    this.changedNodes = this.nodesHelperService
      .copyNodes(nodes) as IMasterStructureNodeTemplate[];
  }

  public enterCreationMode(): void {
    this.creationMode = true;
    this.currentStructure = {
      name: '',
      nodes: [],
      id: 0,
      isActive: false,
      accountingType: ACCOUNTING_TYPES.HGB,
      masterStructureType: MASTER_STRUCTURE_TYPES.BUSINESS_MANAGEMENT_EVALUATION,
      usedBookingAccounts: [],
      unusedBookingAccounts: [],
    } as IMasterStructureFull;
    this.countBookingAccountNodes();
    this.changedNodes = [];
    this.enterEditMode();
  }

  public enterEditMode(): void {
    this.searchValue = '';
    this.onSearch();
    this.setFormValues();
    this.editMode = true;
    this.disableIsActiveCheckbox();

    // this.structureNameField is getting created when this.editMode is true, so we need
    // to wait a bit until it is ready, thats why setTimeout is used
    setTimeout(() => {
      this.form.markAsPristine();
      this.structureNameField.nativeElement.focus();
      this.formElement = this.el.nativeElement.querySelector('form');
    });
  }

  public cancelEditing(): void {
    this.editMode = false;
    this.creationMode = false;
    this.currentStructure = this.defaultStructures
      .find(s => s.id === this.currentStructureId);
    this.countBookingAccountNodes();
  }

  private setFormValues(): void {
    const { name, isActive, accountingType, masterStructureType } = this.form.controls;
    name.setValue(this.currentStructure.name);
    isActive.setValue(this.currentStructure.isActive);
    accountingType.setValue(this.currentStructure.accountingType);
    masterStructureType.setValue(this.currentStructure.masterStructureType);
  }

  public onStructureTypeChange(relation: MASTER_STRUCTURE_TYPES): void {
    this.nodesComponent.closeContextmenu();

    const { masterStructureType } = this.form.controls;
    this.structureValidationService.onStructureTypeChange(
      relation,
      masterStructureType,
      this.usedBookingAccounts,
      this.currentStructure,
      this.nodesComponent.nodes,
    );
  }

  public onAccountingNormChange(type: ACCOUNTING_TYPES): any {
    const { accountingType } = this.form.controls;
    this.nodesComponent.closeContextmenu();
    this.structureValidationService.onAccountingNormChange(
      type,
      accountingType,
      this.usedBookingAccounts,
      this.currentStructure,
    );
  }

  private createStructure(payload: IMasterStructureCreate): void {
    this.structureRepo.create(payload, {}).subscribe(newStructure => {
      this.currentStructureId = newStructure.id;
      this.cancelEditing();
    });
  }

  public download(template: TemplateRef<any>, type: 'excel' | 'pdf'): void {
    this.downloadAs = type;
    this.structureExportService.download(template, this.currentStructure);
  }

  public closeDownloadModal(): void {
    this.structureExportService.closeDownloadModal();
  }

  public startDownloading(): void {
    this.structureExportService.startDownloading(
      this.currentStructure,
      this.nodesComponent.nodes,
      this.exportWithAccounts,
      this.downloadAs,
    );
  }

  public copy(): void {
    const newStructure: IMasterStructure = this.structureExportService
      .copyStructure(this.currentStructure, this.nodesComponent.nodes);
    this.createStructure(newStructure);
  }

  public delete(): void {
    this.confirmationService.confirm({
      wording: setTranslationSubjects(
        this.wording.general.areYouSureYouWantToDelete,
        { subject: this.currentStructure.name },
      ),
      okButtonWording: this.wording.general.delete,
      cancelButtonWording: this.wording.general.cancel
    }).subscribe(isConfirmed => {
      if (isConfirmed) {
        this.structureRepo.delete(this.currentStructureId, {}).subscribe(() => {
          this.currentStructure = undefined;
          this.currentStructureId = undefined;
        });
      }
    });
  }

  public submit(): void {
    validateAllFormFields(this.form);
    if (this.form.invalid) {
      return;
    }
    const payload: IMasterStructure = {
      ...this.currentStructure,
      ...this.form.value,
      nodes: this.changedNodes
    };

    if (this.creationMode) {
      this.createStructure(payload);
    } else {
      this.structureRepo.update(payload, {}).subscribe(() => {
        this.cancelEditing();
      });
    }
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
