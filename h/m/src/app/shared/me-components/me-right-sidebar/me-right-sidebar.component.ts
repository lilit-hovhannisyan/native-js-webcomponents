import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MeRightSidebarI } from './me-right-sidebar.interface';

@Component({
  selector: 'me-right-sidebar',
  templateUrl: './me-right-sidebar.component.html',
  styleUrls: ['./me-right-sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeRightSidebarComponent implements OnInit {
  @Input() dataModel!: MeRightSidebarI;
  files = new FormControl();

  @Output() eventOccurs: EventEmitter<{ eventName: string; payload?: string }> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  customEvent(eventName: string, payload?: string): void {
    this.eventOccurs.emit({ eventName, payload });
  }
}
