import { Component, OnDestroy, OnInit, ChangeDetectorRef, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { sortBy } from 'lodash';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NvGridConfig } from 'nv-grid';
import { Observable, Subscription } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { NVHttpErrorResponse, ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { getGridConfig as debitorsCreditorsGridConfig } from 'src/app/core/constants/nv-grid-configs/debitors-creditors.config';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/mandator.config';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { IDebitorCreditorRow } from 'src/app/core/models/resources/IDebitorCreditor';
import { isMandatorTypeParentOrVessel, IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { IUser } from 'src/app/core/models/resources/IUser';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { IVesselAccounting } from 'src/app/core/models/resources/IVesselAccounting';
import { AccountService } from 'src/app/core/services/account.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { DebitorCreditorMainService } from 'src/app/core/services/mainServices/debitor-creditor-main.service';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';
import { VesselRepositoryService } from 'src/app/core/services/repositories/vessel-repository.service';
import { VesselsHelperService } from '../vessels.helper.service';
import { endOfTermReasonsOptions } from './end-of-term-reasons-options';
import { getDefaultIntervalListItem, fetchFormattedIntervals, fetchFormattedFormValue } from 'src/app/shared/helpers/general';
import { MandatorValidatorsService } from 'src/app/core/services/validators/mandator-validators.service';

@Component({
  selector: 'nv-vessel-accounting',
  templateUrl: './vessel-accounting.component.html',
  styleUrls: ['./vessel-accounting.component.scss']
})
export class VesselAccountingComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public vesselId: VesselKey;
  public accountings: Array<IVesselAccounting> = [];
  public selectedAccounting?: IVesselAccounting;
  public inEditMode = false;
  public inCreationMode = false;
  public node = sitemap.chartering.children.misc.children.vessels.children.id.children.specification;
  public zone = SitemapEntry.getByNode(this.node).toZone();
  public debitorsCreditors$: Observable<IDebitorCreditorRow[]>;
  public endOfTermReasons: ISelectOption[] = endOfTermReasonsOptions;
  public getGridConfig = getGridConfig;
  private subscriptions: Subscription[] = [];
  public wording = wording;
  public debitorsCreditorsGridConfig: NvGridConfig = debitorsCreditorsGridConfig({}, () => false);
  private user: IUser;

  public mandatorsFilter: object = {
    isActive: true,
    checkType: (mandator: IMandatorLookup) => isMandatorTypeParentOrVessel(mandator.mandatorType),
  };

  public get vesselNoDisabled(): boolean {
    // vesselNo can be edited only by superAdmin
    // trial-change: if there is no accounting, then it's enabled also for admin.
    const isSuperAdmin = this.user?.isSuperAdmin;
    return this.accountings?.length ? !isSuperAdmin : !this.user?.isAdmin && !isSuperAdmin;
  }
  public get vesselNameDisabled(): boolean {
    // vesselName can be edited only by admin or superAdmin
    return !this.user?.isSuperAdmin && !this.user?.isAdmin;
  }

  public formElement: ElementRef;

  constructor(
    private fb: FormBuilder,
    private confirmationService: ConfirmationService,
    public accountService: AccountService,
    public route: ActivatedRoute,
    private vesselsAccountingRepo: VesselAccountingRepositoryService,
    private debitorsCreditorsService: DebitorCreditorMainService,
    private vesselsRepo: VesselRepositoryService,
    private nzMessageService: NzMessageService,
    public authService: AuthenticationService,
    private mandatorValidatorsService: MandatorValidatorsService,
    private vesselsHelperService: VesselsHelperService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef
  ) { }

  public ngOnInit(): void {
    this.user = this.authService.getSessionInfo().user;
    const { id } = this.route.parent.snapshot.params;
    this.vesselId = parseInt(id, 10);
    this.init();
    this.debitorsCreditors$ = this.debitorsCreditorsService.fetchAllDebitorsCreditorsRows()
      .pipe(shareReplay());
    this.fetchAccountings();
  }

  private fetchAccountings = (idToSelect?: number) => {
    this.vesselsRepo.fetchAccountingsByVesselId(this.vesselId)
    .pipe(
      map(accountings =>  fetchFormattedIntervals(accountings))
    ).subscribe(accountings => {
      this.accountings = accountings || [];
      this.accountings = sortBy(this.accountings, ['from']);

      if (this.accountings.length) {
        const specificationToSelect = idToSelect
          ? this.accountings.find(s => s.id === +idToSelect)
          : getDefaultIntervalListItem(this.accountings);
        this.selectSpecification(specificationToSelect
           || this.accountings[this.accountings.length - 1]);
      }
    });
  }

  private init(accountingTerm: IVesselAccounting = { vesselId: this.vesselId } as any) {
    this.form = this.fb.group({
      from: [accountingTerm.from, [Validators.required]],
      until: [accountingTerm.until, [Validators.isAfterFieldDate('from')]],
      endOfTermReason: [accountingTerm.endOfTermReason],
      vesselNo: [accountingTerm.vesselNo, [Validators.required, Validators.min(1), Validators.max(9999), Validators.wholeNumber]],
      vesselName: [accountingTerm.vesselName, [Validators.required, Validators.maxLength(100)]],
      mandatorId: [accountingTerm.mandatorId, [this.mandatorValidatorsService.doesMandatorHaveVesselRelation]],
      owningCompanyId: [accountingTerm.owningCompanyId],
      bareboatCompanyId: [accountingTerm.bareboatCompanyId],
      commercialManagerId: [accountingTerm.commercialManagerId],
      technicalManagerId: [accountingTerm.technicalManagerId],
      bookkeepingCompanyId: [accountingTerm.bookkeepingCompanyId],
      crewingCompanyId: [accountingTerm.crewingCompanyId],
      insolvencyAdministrator: [accountingTerm.insolvencyAdministrator, [Validators.maxLength(300)]],
    });
    this.form.disable();
    this.form.markAsPristine();
    this.updateFromFieldOnUntilChange();
  }

  public updateFromFieldOnUntilChange() {
    this.subscriptions = [
      this.form.get('from').valueChanges.subscribe(() => {
        this.form.get('until').updateValueAndValidity();
      }),
      ...this.subscriptions
    ];
  }

  public intervalClicked = (accountingId: number) => {
    const accounting = this.accountings.find(s => s.id === accountingId);
    this.selectSpecification(accounting);
  }

  private selectSpecification = (accounting: IVesselAccounting) => {
    this.selectedAccounting = accounting;
    this.init(accounting);
  }

  public enterEditMode = () => {
    this.inEditMode = true;
    // for some strange reason, form needs to be enabled in next event-cycle.
    // otherwise it stays disabled
    setTimeout(() => {
      this.form.enable();

      if (this.vesselNoDisabled) {
        this.form.controls.vesselNo.disable();
      }
      if (this.vesselNameDisabled) {
        this.form.controls.vesselName.disable();
      }
    });
  }

  public cancelEditing = () => {
    this.inEditMode = false;
    this.inCreationMode = false;
    this.init(this.selectedAccounting);
    // for some strange reason, form needs to be disabled in next event-cycle.
    // otherwise it stays enabled
    setTimeout(() => this.form.disable());

  }

  public addIntervalClicked = () => {
    this.inCreationMode = true;
    this.selectedAccounting = {} as IVesselAccounting;
    const latestEntry = this.accountings[this.accountings.length - 1];
    const clonedAcccounting = latestEntry
      ? { ...latestEntry, from: latestEntry.until, until: undefined }
      : {} as IVesselAccounting;
    delete clonedAcccounting.id; // we don't need the id in creation mode
    this.init(clonedAcccounting); // cloning the most recent one
    this.enterEditMode();
  }

  public deleteClicked = () => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: this.selectedAccounting.vesselName },
    }).subscribe(confirmed => {
      if (confirmed) {
        this.vesselsAccountingRepo.delete(this.selectedAccounting.id, {})
          .subscribe(() => {
            this.vesselsHelperService.generateTitle(this.vesselId);
          });
      }
    });
  }

  public saveClicked = () => {
    if (this.form.invalid) { return; }

    const value: IVesselAccounting = fetchFormattedFormValue(this.form.getRawValue());

    const freshResource = this.selectedAccounting.id
      ? { ...this.selectedAccounting, ...value }
      : { ...value, vesselId: this.vesselId };

    this.selectedAccounting.id
      ? this.vesselsAccountingRepo.update(freshResource, {}).subscribe(spec => {
        this.cancelEditing();
        this.vesselsHelperService.generateTitle(this.vesselId);
        this.fetchAccountings(spec.id);
      }, this.handleSubmitError)

      : this.vesselsAccountingRepo.create(freshResource, {}).subscribe(spec => {
        this.cancelEditing();
        this.vesselsHelperService.generateTitle(this.vesselId);
        this.fetchAccountings(spec.id);
        this.vesselsHelperService.isAccountingsMissing$.next(false);
      }, this.handleSubmitError);
  }

  public handleSubmitError = (error: NVHttpErrorResponse) => {
    ResourceDetailView.handleSubmitError(error, this.form, this.nzMessageService);
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
