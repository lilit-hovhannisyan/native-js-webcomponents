import { Directive } from '@angular/core';

export interface MeAttributeComponentInterface extends Directive {
  // tslint:disable-next-line:no-any
  data: any;
}
