import { Component, OnInit } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { DebitorCreditorRepositoryService } from 'src/app/core/services/repositories/debitor-creditor-repository.service';
import { IDebitorCreditor, IDebitorCreditorCreate } from 'src/app/core/models/resources/IDebitorCreditor';
import { Validators } from 'src/app/core/classes/validators';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'nv-remarks',
  templateUrl: './remarks.component.html',
  styleUrls: ['./remarks.component.scss']
})
export class DebitorCreditorRemarksTabComponent extends ResourceDetailView<
IDebitorCreditor,
IDebitorCreditorCreate
> implements OnInit {
  public routeBackUrl = this.url(sitemap.accounting.children.booking.children.debitorCreditors);
  public logoId: string;


  protected resourceDefault = {} as unknown as IDebitorCreditor;
  protected repoParams = {};

  constructor(
    baseComponentService: BaseComponentService,
    debitorCreditorRepo: DebitorCreditorRepositoryService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
  ) {
    super(debitorCreditorRepo, baseComponentService);
  }

  public ngOnInit() {
    const { id } = this.route.parent.snapshot.params;
    this.loadResource(id);
  }

  public getTitle = () => this.resource.no.toString();

  public init(dc: IDebitorCreditor = this.resourceDefault) {
    this.initForm(this.fb.group({
      remark: [dc.remark, Validators.maxLength(500)],
    }));
  }

  public submit() {
    validateAllFormFields(this.form);
    if (this.form.invalid) { return; }

    const formValue = this.form.getRawValue();
    const resource = { ...this.resource, remark: formValue.remark };

    this.updateResource(resource).subscribe();
  }

}
