import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { markControlAsTouched } from 'src/app/core/helpers/form';

@Injectable({
  providedIn: 'root'
})
export class AutobankFilterValueValidatorsService {

  constructor(
  ) { }

  public hasValue = (control: AbstractControl): ValidationErrors | null => {

    if (!control || !control.parent) {
      return null;
    }

    if (control.value) {
      return null;
    }

    markControlAsTouched(control);

    return {
      valueRequired: {
        message: wording.errors.required
      }
    };
  }
}
