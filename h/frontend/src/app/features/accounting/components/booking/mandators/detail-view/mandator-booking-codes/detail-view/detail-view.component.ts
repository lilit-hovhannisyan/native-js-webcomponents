import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable, forkJoin } from 'rxjs';
import { IMandatorBookingCodeFull } from 'src/app/core/models/resources/IMandatorBookingCode';
import { MandatorBookingCodeRepositoryService } from 'src/app/core/services/repositories/mandator-booking-code-repository.service';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { Validators } from 'src/app/core/classes/validators';
import { startWith, map } from 'rxjs/operators';
import { sortBy } from 'lodash';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { SettingsService } from 'src/app/core/services/settings.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { IBookingCode } from 'src/app/core/models/resources/IBookingCode';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/booking-codes.config';
@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class MandatorBookingCodesDetailViewComponent implements OnInit {

  @Input() public id: number;
  @Input() public mandatorId: number;
  @Input() public closeModal: (IMandatorAccountCompact?) => void;

  public bookingCodes$: Observable<IBookingCode[]>;

  public creationMode: boolean;
  public form: FormGroup;
  public isValid$: Observable<boolean>;
  public mandatorBookingCode: IMandatorBookingCodeFull;
  public wording = wording;
  public gridConfig = getGridConfig({}, () => false);

  constructor(
    private fb: FormBuilder,
    public mandatorBookingCodeRepo: MandatorBookingCodeRepositoryService,
    public bookingCodeRepo: BookingCodeRepositoryService,
    private settingsService: SettingsService
  ) { }

  public ngOnInit(): void {
    this.creationMode = !this.id;
    this.initForm();

    this.bookingCodes$ = forkJoin([
      this.bookingCodeRepo.fetchAll({}),
      this.mandatorBookingCodeRepo.fetchAll({ queryParams: { mandatorId: this.mandatorId } }),
    ]).pipe(
      map(([bookingCodes, _mandatorBookingCodes]) => {
        // This filter is neccessary, as long as queryParams do not filter on server-side
        const mandatorBookingCodes = _mandatorBookingCodes.filter(m => m.mandatorId === this.mandatorId);
        return bookingCodes.filter(bc => {
          // show only bookingCodes that are not already conected to the mandator
          return bc.isActive && !mandatorBookingCodes.find(mbCode => mbCode.bookingCodeId === bc.id);
        });
      }),
      map(res => sortBy(res, [r => r[getLabelKeyByLanguage(this.settingsService.language)].toLowerCase()]))
    );
  }

  public initForm(mandatorBookingCode = {} as IMandatorBookingCodeFull): void {
    this.form = this.fb.group({
      mandatorId: [this.mandatorId],
      bookingCodeId: [mandatorBookingCode.bookingCodeId, Validators.required],
    });

    this.isValid$ = this.form.valueChanges.pipe(
      startWith([null]),
      map(() => this.form.valid)
    );
  }

  public saveClicked(): void {
    this.mandatorBookingCodeRepo.create(this.form.value, {})
      .subscribe(() => { this.closeModal(); });
  }
}
