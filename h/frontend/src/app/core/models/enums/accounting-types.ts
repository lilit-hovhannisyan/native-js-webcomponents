import { wording } from '../../constants/wording/wording';
import { ISelectOption } from '../misc/selectOption';

export enum ACCOUNTING_TYPES {
  HGB = 1,   // 0001
  IFRS = 2, // 0010
  USGAAP = 4, // 0100
  EBalance = 8, // 1000
}

export const accountingTypeOptions: ISelectOption<ACCOUNTING_TYPES>[] = [
  { value: ACCOUNTING_TYPES.HGB, displayLabel: wording.accounting.hgb },
  { value: ACCOUNTING_TYPES.IFRS, displayLabel: wording.accounting.ifrs },
  { value: ACCOUNTING_TYPES.USGAAP, displayLabel: wording.accounting.usgaap },
];

export const accountingTypeOptionsFull: ISelectOption<ACCOUNTING_TYPES>[] = [
  ...accountingTypeOptions,
  { value: ACCOUNTING_TYPES.EBalance, displayLabel: wording.accounting.eBalance },
];
