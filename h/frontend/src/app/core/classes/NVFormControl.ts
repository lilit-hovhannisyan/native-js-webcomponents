import { FormControl } from '@angular/forms';

export class NVFormControl extends FormControl {
  public label: string;
}
