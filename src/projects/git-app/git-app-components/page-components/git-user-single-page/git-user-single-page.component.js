import {BaseComponent} from '../../../../../library/core/base-component/base.component.js';
import {basePath} from '../../../data/base.path.js';
import {gitUsers} from '../../../data/git.data.js';

export class GitUserSingle extends BaseComponent {
  constructor() {
    super(basePath + 'page-components/git-user-single-page/git-user-single-page.component.css');
  }

  render = () => {
    const gitUserId = this.getAttribute('userid') || location.pathname.substr(location.pathname.lastIndexOf('/') + 1);
    const user = gitUsers.get(gitUserId);
    const userId = user?.login;


    const html = `<git-app-pagination component="userRepository" requestid="${userId}" itemsperpage="3"></git-app-pagination>`;
    const config = {
      tagName: 'div',
      subElements: [
        {
          tagName: 'div',
          attr: {
            class: 'user-general',
          },
          subElements: [
            {
              tagName: 'git-avatar',
              attr: {
                name: user.name,
                avatar: user.avatar_url,
                userid: user['id'],
              }
            },
            {
              tagName: 'div',
              attr: {
                class: 'user-ifo',
              },
              text: user.bio
            },
          ]
        },
        {
          tagName: 'div',
          attr: {
            class: 'user-repos',
          },
          html
        }
      ]
    };

    const el = this.createElementWithAttributes(config);
    this.shadow.appendChild(el);
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }
}