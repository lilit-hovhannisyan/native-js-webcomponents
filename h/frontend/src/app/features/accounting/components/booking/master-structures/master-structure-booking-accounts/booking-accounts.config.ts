import { NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getMasterStructureBookingAccountsGridConfig = (
  isSwitchingNode: boolean,
): NvGridConfig => {
  return ({
    gridName: 'bookingAccountsGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    rowSelectionType: NvGridRowSelectionType.Checkbox,
    hideRefreshButton: true,
    sortBy: 'no',
    isSortAscending: true,
    paging: {
      pageNumber: 1,
      pageSize: 10
    },
    columns: [
      {
        key: 'isSwitchingAccount',
        title: wording.accounting.switchingAccount,
        width: 200,
        isSortable: true,
        dataType: NvColumnDataType.Boolean,
        hidden: !isSwitchingNode,
        visible: isSwitchingNode,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: [],
        },
      },
      {
        key: 'no',
        title: wording.accounting.no,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'labelEn',
        title: wording.accounting.accountNameEn,
        width: 400,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'labelDe',
        title: wording.accounting.accountNameDe,
        width: 400,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
  });
};
