import SkipOptions from '../enums/config-values.enum';

interface ConfigI {
  skipOption: SkipOptions;
}

export default ConfigI;
