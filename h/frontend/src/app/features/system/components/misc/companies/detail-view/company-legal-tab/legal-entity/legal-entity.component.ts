import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef, Input } from '@angular/core';
import { CompanyLegalEntityRepositoryService } from 'src/app/core/services/repositories/company-legal-entity-repository.service';
import { ICompanyLegalEntity, ICompanyLegalEntityCreate } from 'src/app/core/models/resources/ICompanyLegalEntity';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { FormBuilder, AbstractControl, FormControl } from '@angular/forms';
import { ICompany, CompanyKey, ICompanyRow } from 'src/app/core/models/resources/ICompany';
import { NvGridConfig } from 'nv-grid';
import { getLegalEntityPartnersGridConfig } from './legal-entity-partners.config';
import { Observable, combineLatest, BehaviorSubject, ReplaySubject, forkJoin, of } from 'rxjs';
import { getGridConfig as getCurrenciesGridConfig } from 'src/app/core/constants/nv-grid-configs/currencies.config';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { map, startWith, tap, takeUntil, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { wording } from 'src/app/core/constants/wording/wording';
import { COMPANY_LEGAL_FORM, CAPITAL_TYPE } from 'src/app/core/models/enums/company-legal-form';
import { Validators } from 'src/app/core/classes/validators';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { legalEntityFields, LegalEntityHelperService } from './legal-entity.helper.service';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { MAX_INT32, DECIMAL_14_2 } from 'src/app/core/constants/min-max-values';
import { ContributionMainService } from 'src/app/core/services/mainServices/company-contributions-main.service';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { CompaniesHelperService } from '../../companies.helper.service';
import { SilentPartnerRepositoryService } from 'src/app/core/services/repositories/silent-partner-repository.service';
import { ISilentPartnerRepresentative } from 'src/app/core/models/resources/ISilentPartnerRepresentative';
import { TrusteePartnerContributionsRepositoryService } from 'src/app/core/services/repositories/trustee-partner-contribution-repository.service';
import { Language } from 'src/app/core/models/language';
import { ICompanyGroup } from 'src/app/core/models/resources/ICompanyGroup';
import { CompanyGroupRepositoryService } from 'src/app/core/services/repositories/company-group-repository.service';
import { ICompanyContributionRepoParams } from 'src/app/core/services/repositories/company-partner-contribution-repository.service';
import { CompanyPartnerContributionKey, ICompanyPartnerContributionFull } from 'src/app/core/models/resources/ICompanyPartnerContribution';
import { ShareholdingsHelperService } from '../../../../shareholdings/shareholdings.helper.service';
import { ITrusteePartnerContribution } from 'src/app/core/models/resources/ITrusteePartnerContribution';
import { IBusinessName } from 'src/app/core/models/resources/IBusinessName';
import { COMPANY_CONTRIBUTION_TYPES } from '../../../../../../shared/trustee-partner-contributions/enums/company-contributions';
import { shareholdingsLegalEntityZone } from 'src/app/authentication/services/zoneService/specialZones';
import { uniqueId } from 'lodash';

@Component({
  selector: 'nv-legal-entity',
  templateUrl: './legal-entity.component.html',
  styleUrls: ['./legal-entity.component.scss'],
  providers: [LegalEntityHelperService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LegalEntityComponent extends ResourceDetailView<
ICompanyLegalEntity,
ICompanyLegalEntityCreate
> implements OnInit, OnDestroy {
  @Input() public companyId: CompanyKey;

  /**
   * to be reusable we have to enhance current components with these concepts, where
   * `customPreAction` controls whether some action must be done before final submission
   * `context` is to provide clue to backend which component exactlty calls the api
   *
   * Depending on the context, we will use different validations.
   */

  @Input() public customPreAction: boolean;
  @Input() public context: string; // pass information for backend validations

  protected repoParams: ICompanyContributionRepoParams = {};
  protected resourceDefault = {} as ICompanyLegalEntity;
  public routeBackUrl?: string;
  public partnersGridConfig: NvGridConfig;
  public partnersSubject$: BehaviorSubject<ICompanyRow[]> = new BehaviorSubject([]);
  public gridConfig: NvGridConfig = getCurrenciesGridConfig({}, () => false);
  public currencies: ICurrency[];
  public currencies$: Observable<ICurrency[]>;
  public isFieldAvailable: (fieldName: string) => boolean;
  public capitalType = CAPITAL_TYPE;
  public hasAnySilentPartner = false;
  public hasTrusteeContribution = false;
  public hasSubParticipantContribution = false;
  public companyGroups$: Observable<ICompanyGroup[]>;
  public countryPath = this.url(this.sitemap.system.children.misc.children.companies);
  public wording = wording;
  public businessNamesCtrl = new FormControl([], [Validators.required]);
  public businessNameHasError: boolean;
  public isInShareholdingsLegalEntity: boolean;

  private company: ICompany;
  private componentDestroyed$ = new ReplaySubject(1);
  private hasContribution = false;
  private subsequentCompanyLegalEntity: ICompanyLegalEntity;

  constructor(
    private companyLegalEntityRepo: CompanyLegalEntityRepositoryService,
    private fb: FormBuilder,
    public baseComponentService: BaseComponentService,
    private currencyRepo: CurrencyRepositoryService,
    private silentPartnerRepo: SilentPartnerRepositoryService,
    public legalEntityHelperService: LegalEntityHelperService,
    private companyRepo: CompanyRepositoryService,
    private contributionMainService: ContributionMainService,
    private companiesHelperService: CompaniesHelperService,
    private trusteeContributionsRepo: TrusteePartnerContributionsRepositoryService,
    private companyGroupRepo: CompanyGroupRepositoryService,
    private cdr: ChangeDetectorRef,
    private shareholdingsHelperService: ShareholdingsHelperService,
  ) {
    super(companyLegalEntityRepo, baseComponentService);
  }

  public getTitle = () => '';

  public ngOnInit(): void {
    this.isInShareholdingsLegalEntity = this.context === shareholdingsLegalEntityZone;
    this.isFieldAvailable = this.legalEntityHelperService.isFieldAvailable;
    this.companyId = this.companyId || this.companiesHelperService.company.id;
    this.repoParams = { queryParams: { cid: this.companyId } };

    forkJoin([
      this.companyRepo.fetchOne(this.companyId, {}),
      this.contributionMainService.fetchAll(this.repoParams),
      this.trusteeContributionsRepo.fetchAll({
        ...this.repoParams,
        contributorType: COMPANY_CONTRIBUTION_TYPES.TrustorContribution,
      }),
      this.trusteeContributionsRepo.fetchAll({
        ...this.repoParams,
        contributorType: COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution,
      }),
      this.currencyRepo.fetchAll({ useCache: false }),
      this.setHesAnySilentPartner(),
      this.companyGroupRepo.fetchAll({})
    ]).subscribe(([
      company,
      contributions,
      trusteeContributions,
      subParticipantContributions,
    ]: [
        ICompany,
        ICompanyPartnerContributionFull[],
        ITrusteePartnerContribution[],
        ITrusteePartnerContribution[],
      ]) => {
      this.company = company;

      if (!this.customPreAction) {
        this.businessNamesCtrl.patchValue(company.businessNames);
      }

      this.init();
      this.companiesHelperService.setCompany(this.companyId);
      this.legalEntityHelperService.subscribeToLegalForm();
      this.hasContribution = contributions.length > 0;
      this.hasTrusteeContribution = trusteeContributions.length > 0;
      this.hasSubParticipantContribution = subParticipantContributions.length > 0;
      this.companyGroups$ = this.companyGroupRepo.getStream().pipe(
        map(groups => {
          return groups.filter(g => g.isActive)
            .sort((g1, g2) => {
              const language: Language = this.settingsService.language;
              const opt1: string = g1.displayLabel[language].toLowerCase();
              const opt2: string = g2.displayLabel[language].toLowerCase();
              if (opt1 < opt2) { return -1; }
              if (opt1 > opt2) { return 1; }
              return 0;
            });
        })
      );
      this.loadResource(this.companyId).subscribe(() => {
        this.initPartnersSubject();
        this.initLegalEntityPartnersGrid();
        this.legalEntityHelperService.legalForm$.next(this.company.legalForm);
        this.inEditMode.subscribe(() => {
          this.initPartnersSubject();
          // to toggle disabled actions of grid
          this.initLegalEntityPartnersGrid();
          this.cdr.detectChanges();
        });

        if (this.shareholdingsHelperService.isInCreationMode$.value) {
          this.enterEditMode();
        }
      });
    });
  }

  private setHesAnySilentPartner(): Observable<ISilentPartnerRepresentative[]> {
    return this.silentPartnerRepo.fetchAll({ queryParams: { cid: this.companyId } }).pipe(
      tap(silentPartners => this.hasAnySilentPartner = !!silentPartners.length),
    );
  }

  public init(legalEntity = {} as ICompanyLegalEntity): void {
    this.subsequentCompanyLegalEntity = this.companyLegalEntityRepo.getStreamValue()
      .find(c => c.formerCompanyId === this.resource?.companyId);

    const fieldsMap = {
      // Commercial Register
      id: [legalEntity.id],
      companyId: [legalEntity.companyId],
      hra: [legalEntity.hra, [Validators.number]],
      hrb: [legalEntity.hrb, [Validators.number]],
      commercialRegister: [legalEntity.commercialRegister, [Validators.maxLength(100)]],
      dateOfEntry: [legalEntity.dateOfEntry],
      sumOfContributions: [legalEntity.sumOfContributions, [Validators.max(DECIMAL_14_2)]],
      shareCapital: [legalEntity.shareCapital, [Validators.max(DECIMAL_14_2)]],
      currencyId: [legalEntity.currencyId],
      capitalType: [legalEntity.capitalType],
      sumOfNonParValueShares: [legalEntity.sumOfNonParValueShares, [
        Validators.max(MAX_INT32),
        Validators.positiveNumber,
      ]],
      companyNumber: [legalEntity.companyNumber, [Validators.maxLength(100)]],

      // Classification
      groupOfCompanyId: [legalEntity.groupOfCompanyId],
      isShellCompany: [legalEntity.isShellCompany],
      isDormantCompany: [legalEntity.isDormantCompany],
      isUnitaryLimitedPartnership: [legalEntity.isUnitaryLimitedPartnership],

      // Remark
      remark: [legalEntity.remark],

      // Status
      isPreliminaryInsolvencyProceeding: [legalEntity.isPreliminaryInsolvencyProceeding],
      preliminaryInsolvencyProceedingStartDate: [legalEntity.preliminaryInsolvencyProceedingStartDate],
      preliminaryInsolvencyAdministrator: [
        legalEntity.preliminaryInsolvencyAdministrator,
        [Validators.maxLength(200)],
      ],

      isInsolvencyProceeding: [legalEntity.isInsolvencyProceeding],
      insolvencyProceedingStartDate: [legalEntity.insolvencyProceedingStartDate],

      isInLiquidation: [legalEntity.isInLiquidation],
      liquidationStartDate: [legalEntity.liquidationStartDate],
      liquidator: [legalEntity.liquidator, [Validators.maxLength(200)]],

      isDeleted: [legalEntity.isDeleted],
      deletedStartDate: [legalEntity.deletedStartDate],

      isSold: [legalEntity.isSold],
      soldStartDate: [legalEntity.soldStartDate],
      buyer: [legalEntity.buyer, Validators.maxLength(200)],
      partners: [legalEntity.partners],
      formerCompanyId: [legalEntity.formerCompanyId],

      subSequentCompanyId: [this.subsequentCompanyLegalEntity?.companyId], // view field, not in DTO
    };

    this.initForm(this.fb.group(fieldsMap));

    if (this.context === shareholdingsLegalEntityZone) {
      const fieldNames = ['hra', 'hrb', 'commercialRegister', 'dateOfEntry', 'sumOfContributions'];
      const controls = this.form.controls;
      const { groupOfCompanyId } = controls;

      for (const field of fieldNames) {
        if (fieldNames.includes(field) && legalEntityFields[this.company.legalForm][field]) {
          controls[field].setValidators(Validators.required);
        }
      }

      groupOfCompanyId.setValidators(Validators.required);
    }

    if (this.hasContribution) {
      this.disableField('capitalType');
    }

    this.loadCurrencies();
    this.initFormerCompanyField();
    this.cdr.detectChanges();
  }

  /**
   * if subsequent company has value then we need to disable former company field
   */
  private initFormerCompanyField() {
    this.form.get('subSequentCompanyId').valueChanges
      .pipe(
        tap(id => {
          const formerCompanyCtrl = this.form.get('formerCompanyId');
          if (id) {
            formerCompanyCtrl.disable();
          } else {
            if (this.form.enabled) {
              formerCompanyCtrl.enable();
            }
          }
        })
      )
      .subscribe();
  }

  private initPartnersSubject(): void {
    const { partners } = this.form.controls;

    combineLatest([
      this.companyRepo.getStream(),
      this.contributionMainService.getStream(this.repoParams),
      this.form.controls.partners.valueChanges.pipe(
        startWith(partners.value),
      ),
    ]).pipe(takeUntil(this.componentDestroyed$)).subscribe(([companies, contributions]) => {
      let currentContributionIds: CompanyPartnerContributionKey[] = partners.value;

      currentContributionIds
        = this.legalEntityHelperService.getCurrentPartnerList(contributions, this.company);

      const partnersList: ICompany[] = companies.filter((c: ICompany) => currentContributionIds.includes(c.id));
      this.partnersSubject$.next(partnersList);
    });
  }

  private initLegalEntityPartnersGrid(): void {
    this.partnersGridConfig = getLegalEntityPartnersGridConfig(
      this.legalEntityHelperService.legalForm,
    );
  }

  public deletePartner = (company: ICompany): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: setTranslationSubjects(
        wording.general.areYouSureYouWantToDelete,
        { subject: company.companyName }
      ),
    }).subscribe(confirmed => {
      if (confirmed) {
        const { partners } = this.form.controls;
        const partnersIds: CompanyKey[] = partners.value;
        const index = partnersIds.findIndex(id => id === company.id);

        if (index === -1) {
          return;
        }

        partnersIds.splice(index, 1);
        partners.setValue(partnersIds);
      }
    });
  }

  public isGridDisabled = (): boolean => {
    return !this.editMode;
  }

  private loadCurrencies(): void {
    const currencyControl: AbstractControl = this.form.controls.currencyId;

    this.currencies$ = combineLatest([
      this.currencyRepo.getStream(),
      currencyControl.valueChanges.pipe(startWith(currencyControl.value))
    ]).pipe(
      distinctUntilChanged(),
      map(([currencies, currencyId]) => {
        return this.currencyRepo.filterActiveOnly(currencies, currencyId);
      }),
      tap((currencies: ICurrency[]) => {
        this.currencies = currencies;

        if (!currencyControl.value) {
          const currency: ICurrency = currencies.find(c => c.isoCode === 'EUR');
          currencyControl.setValue(currency.id);
        }

        const isAcceptOnlyEuro: boolean = this.legalEntityHelperService.legalForm ===
          COMPANY_LEGAL_FORM.CompanyWithLimitedLiability
          || this.legalEntityHelperService.legalForm ===
          COMPANY_LEGAL_FORM.StockCorporation;

        if ((isAcceptOnlyEuro || this.hasContribution) && !currencyControl.disabled) {
          this.disableField('currencyId');
        }
      }),
    );
  }

  public submit(): void {
    if (!this.businessNamesCtrl.value?.length) {
      this.businessNameHasError = true;
      return;
    }

    validateAllFormFields(this.form);

    if (this.form.invalid) { return; }

    this.checkConditionalFields();
    // formerCompanyId is disabled in some cases and we don't want to lose its value
    const value = this.form.getRawValue();

    const subSequentCompanyId: CompanyKey = value.subSequentCompanyId;

    const { capitalType } = this.form.controls;
    const newResource: ICompanyLegalEntity = {
      ...value,
      capitalType: capitalType.value,
    } as ICompanyLegalEntity;

    this.generalSubmitAction(newResource, subSequentCompanyId);
  }

  private generalSubmitAction(
    resource: ICompanyLegalEntity,
    subSequentCompanyId: CompanyKey
  ): void {
    if (this.isInShareholdingsLegalEntity) {
      resource = {
        ...resource,
        _meta: this.context,
      };
    }

    if (this.companiesHelperService.companyHasLegalData) {
      resource.id
        ? this.updateResource(resource)
          .pipe(
            switchMap((companyLegalEntity: ICompanyLegalEntity) => {
              return this.onCompanyLegalEntitySave(companyLegalEntity, subSequentCompanyId);
            })
          ).subscribe()
        : this.createResource(resource, false, false)
          .pipe(
            tap(res => this.navigateToDetailViewWithCompanyId(res)),
            switchMap((companyLegalEntity: ICompanyLegalEntity) => {
              return this.onCompanyLegalEntitySave(companyLegalEntity, subSequentCompanyId);
            })
          ).subscribe();

      return;
    }

    this.companiesHelperService.confirmThatLegalFormIsNoLongerEditable()
      .subscribe(confirmed => {
        if (confirmed) {
          this.createResource(resource, false, false)
            .pipe(
              tap(res => this.navigateToDetailViewWithCompanyId(res)),
              switchMap((companyLegalEntity: ICompanyLegalEntity) => {
                return this.onCompanyLegalEntitySave(companyLegalEntity, subSequentCompanyId);
              })
            ).subscribe(legalEntityData => {
              this.companiesHelperService.setLegalDataState(!!legalEntityData);
            });
        }
      });
  }

  private navigateToDetailViewWithCompanyId(res: ICompanyLegalEntity): void {
    this.router.navigate(
      [this.getCurrentUrl().replace('new', res.companyId.toString())],
      { queryParamsHandling: 'preserve' }
    );
  }

  /**
   * When there's a subSequentCompenyId set in the UI we need to update that company's previousCompanyId to
   * this companyLegalEntity's id, if that company doesn't have legal entity we need to create it too 🙃
   * aaaand we need to make API call to remove subsequent company if it's removed from
   * the field 😁
   */
  private onCompanyLegalEntitySave(
    companyLegalEntity: ICompanyLegalEntity,
    subSequentCompanyId: CompanyKey
  ): Observable<ICompanyLegalEntity> {
    // ID is removed from this.subseq... so we need to do this to get subSeq companyId
    // in case there was one
    const subsequentCompanyLegalEntity: ICompanyLegalEntity = this.companyLegalEntityRepo
      .getStreamValue()
      .find(c => c.formerCompanyId === this.resource?.companyId);
    if ((!subSequentCompanyId
      || this.subsequentCompanyLegalEntity?.id === subSequentCompanyId)
      && subsequentCompanyLegalEntity
    ) {
      return this.companyLegalEntityRepo.fetchOne(subsequentCompanyLegalEntity.companyId, {})
        .pipe(
          switchMap((companyLegalEntityB: ICompanyLegalEntity) => {
            return this.companyLegalEntityRepo.update({
              ...companyLegalEntityB,
              formerCompanyId: null,
            }, {});
          }),
          switchMap(() => this.companyLegalEntityRepo.fetchAll({})),
          tap(() => this.init(this.resource)),
          map(() => companyLegalEntity),
        );
    } else if (subSequentCompanyId) {
      return this.companyLegalEntityRepo.fetchOne(subSequentCompanyId, {})
        .pipe(
          switchMap(formerCompanyLegalEntity => {
            // Maybe company doesn't have legal entity yet, we call .create for that
            if (formerCompanyLegalEntity.id) {
              return this.companyLegalEntityRepo.update({
                ...formerCompanyLegalEntity,
                formerCompanyId: companyLegalEntity.companyId,
              }, {});
            }
            return this.companyLegalEntityRepo.create({
              ...formerCompanyLegalEntity,
              formerCompanyId: companyLegalEntity.companyId,
            }, {});
          }),
          switchMap(() => this.companyLegalEntityRepo.fetchAll({})),
          tap(() => this.init(this.resource)),
          map(() => companyLegalEntity),
        );
    }

    if (this.isInShareholdingsLegalEntity) {
      const businessNames: IBusinessName[] = this.businessNamesCtrl.value
        .map((bn: IBusinessName) => {
          if (!bn.companyId) {
            return {
              ...bn,
              id: null
            };
          }
          return bn;
        }).map(b => ({ ...b, companyId: this.companyId }));
      this.companyRepo.update({
        ...this.company,
        businessNames
      }, { isNotificationHidden: true }).subscribe(company => {
        this.customPreAction = false;
        this.shareholdingsHelperService.setCompany(this.company);
        this.shareholdingsHelperService.updateModeState(false);

      });
      return of(undefined);
    }

    return of(companyLegalEntity);
  }

  private checkConditionalFields(): void {
    const {
      isPreliminaryInsolvencyProceeding,
      isInsolvencyProceeding,
      isInLiquidation,
      isDeleted,
      isSold,
      capitalType,
    } = this.form.controls;

    if (!isPreliminaryInsolvencyProceeding.value) {
      this.form.patchValue({
        preliminaryInsolvencyProceedingStartDate: '',
        preliminaryInsolvencyAdministrator: '',
      });
    }

    if (!isInsolvencyProceeding.value) {
      this.form.patchValue({
        insolvencyProceedingStartDate: '',
      });
    }

    if (!isInLiquidation.value) {
      this.form.patchValue({
        liquidationStartDate: '',
        liquidator: '',
      });
    }

    if (!isDeleted.value) {
      this.form.patchValue({
        deletedStartDate: '',
      });
    }

    if (!isSold.value) {
      this.form.patchValue({
        soldStartDate: '',
        buyer: '',
      });
    }

    if (!!capitalType.value) {
      this.form.patchValue({
        sumOfNonParValueShares: '',
      });
    }
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();

    if (this.customPreAction) {
      this.companyRepo.delete(this.companyId, {}).subscribe();
    }
  }

  public onBusinessNameAdd(businessName: IBusinessName): void {
    const businessNamesCtrl = this.businessNamesCtrl;
    const businessNames: IBusinessName[] = [...businessNamesCtrl.value];
    // edited
    if (businessName.id) {
      const editedBusinessNameIndex = businessNames
        .findIndex(bn => bn.id === businessName.id);
      businessNames.splice(editedBusinessNameIndex, 1, businessName);
      businessNamesCtrl.patchValue(businessNames);
    } else {
      // new business name
      const newBusinessName: IBusinessName = {
        name: businessName.name,
        from: businessName.from,
        id: uniqueId('temp') as any as number
      } as IBusinessName;
      businessNamesCtrl.patchValue([
        ...businessNames,
        newBusinessName,
      ]);
    }
    this.cdr.detectChanges();
    this.businessNameHasError = false;
  }

  public onBusinessNameDelete(businessName: IBusinessName): void {
    const businessNames: IBusinessName[] = this.businessNamesCtrl.value;
    const newBusinessNames: IBusinessName[] = businessNames
      .filter(bn => bn.id !== businessName.id);
    this.businessNamesCtrl.patchValue(newBusinessNames);
  }
}
