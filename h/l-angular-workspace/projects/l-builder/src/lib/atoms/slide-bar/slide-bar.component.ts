import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'lavash-slide-bar',
  templateUrl: './slide-bar.component.html',
  styleUrls: ['./slide-bar.component.less']
})
export class SlideBarComponent {
  @Input() progressPercentage = 0;
  @Output() progressChange = new EventEmitter();
  // @Output() mouseDownEvent = new EventEmitter();
  // @Output() mouseUpEvent = new EventEmitter();

  containerWidth;
  containerHeight;

  toggleActive = false;

  @ViewChild('container')
  container: ElementRef;

  Math = Math;

  onChangeProggressPosition(event) {
    this.containerWidth = this.container.nativeElement.offsetWidth;
    this.progressPercentage = event.offsetX * 100 / this.containerWidth;
    this.progressChange.emit(this.progressPercentage);
  }

  /*onMousedown(event) {
    this.toggleActive = true;
    this.mouseDownEvent.emit();
  }

  onMouseup(event) {
    this.toggleActive = false;
    this.mouseUpEvent.emit(this.progressPercentage);
  }

  onMouseMove(event) {
    if (this.toggleActive) {
        console.log(event)
        this.onChangeProggressPosition(event);
    }
  }*/

}
