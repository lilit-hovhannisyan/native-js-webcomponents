import { NgModule } from '@angular/core';
import { BaseRoutingModule } from './base-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { BaseModuleComponent } from './components/base-module.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { ActivityLogsComponent } from './components/topbar/activity-logs/activity-logs.component';
import { TopbarUserDropdownComponent } from './components/topbar/topbarUserDropdown/topbarUserDropdown.component';
import { BreadCrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';


@NgModule({
  imports: [
    BaseRoutingModule,
    SharedModule
  ],
  declarations: [
    BaseModuleComponent,
    TopbarComponent,
    ActivityLogsComponent,
    TopbarUserDropdownComponent,
    BreadCrumbsComponent,
    LoadingSpinnerComponent
  ]
})
export class BaseModule {
}
