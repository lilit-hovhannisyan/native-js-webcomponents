import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'lavash-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.less']
})
export class ButtonComponent {
  _libClassName = ['outlined', 'rounded', 'no-background', 'disabled', 'no-padding', 'animated', 'opacity', 'default'];
  @Input() text: string;
  @Input() className = 'default';
  @Output() clickEvent = new EventEmitter();


  // custom icon OR image
  @Input() graphics?: string;
  @Input() graphicsPosition = 'left';

  onClick(event) {
    this.clickEvent.emit();
  }

  getClassName() {
    let _class = '';
    this.className.split(' ').map(_className => {
        _class += this._libClassName.includes(_className) ?
                  'l-' + _className + ' ' : _className + ' ';
    });

    return _class;
  }
}
