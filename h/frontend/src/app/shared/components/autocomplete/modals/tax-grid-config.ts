import { NvGridRowSelectionType, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

const wordingTaxKeys = wording.accounting;
const wordingGeneral = wording.general;

export const gridConfig: NvGridConfig = {
  gridName: 'taxSelectModalGrid',
  rowSelectionType: NvGridRowSelectionType.RadioButton,
  hideRefreshButton: true,
  hideSettingsButton: true,
  paging: {
    pageNumber: 1,
    pageSize: 10,
    showTotalItems: true
  },
  columns: [
    {
      key: 'code',
      title: wordingTaxKeys.taxKeysLabelCode,
      width: 150,
      isSortable: true,
      filter: {
        values: []
      }
    },
    {
      key: 'displayLabel',
      title: wordingGeneral.name,
      width: 150,
      isSortable: true,
      filter: {
        values: []
      }
    },
    {
      key: 'percentage',
      title: wordingTaxKeys.taxKeysLabelPercentage,
      width: 150,
      isSortable: true,
      filter: {
        values: [],
        selectValues: []
      }
    },
  ],
};
