import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpErrorResponse, HttpEventType, HttpEvent } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject, of } from 'rxjs';
import { catchError, filter, take, switchMap, skipWhile } from 'rxjs/operators';
import { EHttpRequest } from '../models/http/EHttpRequest';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { InternetConnectionService } from 'src/app/shared/services/internet-connection.service';
import { UNAUTHORIZED, NOT_FOUND, SERVICE_UNAVAILABLE } from 'http-status-codes';
import { MaintenanceService } from 'src/app/authentication/services/maintenance.service';
import { INavidoHTTPResponseError } from './http-error-handler';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { NotificationType } from 'src/app/core/models/INotification';
import { wording } from 'src/app/core/constants/wording/wording';

/**
 * Logs out on unauthenticated exception
 *
 * Redirects to NOT FOUND page when HTTP_METHOD == 'GET'
 *
 * Handles maintanance mode when HTTP statuscode == `SERVICE_UNAVAILABLE`
 *
 * Resends HTTP Requests when connection is restored
 */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  private isInternetRetrievedSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private internetConnectionService: InternetConnectionService,
    private maintenanceService: MaintenanceService,
    private notificationService: NotificationsService,
  ) {
    this.internetConnectionService.onlineSubject
      .subscribe(isOnline => this.isInternetRetrievedSubject.next(isOnline));
  }

  public intercept(request: EHttpRequest<any>, next: HttpHandler): Observable<any> {
    return next.handle(request).pipe(
      catchError((response: HttpErrorResponse) => {

        if (this.isUnauthenticatedException(response)) {
          this.authService.logout();
          return of({ type: HttpEventType.Sent });
        }

        if (response.status === SERVICE_UNAVAILABLE) {
          const maintenanceError: INavidoHTTPResponseError = response.error.errors
            .find((e: INavidoHTTPResponseError) => e.key === 'maintenance');
          const estimatedEndTime = maintenanceError.args.estimatedEndTime;

          this.maintenanceService.setNextMaintenanceDate(estimatedEndTime);

          const isAdmin: boolean = this.authService.getUser().isAdmin;
          if (!isAdmin) {
            this.authService.logout();
            this.maintenanceService.inMaintenanceMode.next(true);
            return of({ type: HttpEventType.Sent });
          }
        }

        const entityId = request.getEntityId && request.getEntityId();
        const urlWithoutQueryParams = request.url.split('?')[0];
        const notFoundEntity = request.method === 'GET'
          && entityId
          && urlWithoutQueryParams.endsWith(entityId);

        if (response.status === NOT_FOUND && notFoundEntity) {
          if (request.options.cloneMode) {
            this.notificationService.notify(
              NotificationType.Error,
              wording.general.error,
              wording.general.clonedEntityNotFound
            );
          } else {
            this.router.navigate(['base/main/404'], { skipLocationChange: true });
          }
        }

        if (response.error instanceof ProgressEvent) {
          if (!this.internetConnectionService.isOnline) {
            return this.isInternetRetrievedSubject.pipe(
              filter(isOnline => isOnline),
              take(1),
              switchMap(() => next.handle(request))
            );
          }
        }

        return throwError(response);
      }),
      skipWhile((event: HttpEvent<any>) => event.type === HttpEventType.Sent),
    );
  }

  private isUnauthenticatedException(response: HttpErrorResponse) {
    return !!(
      response.status === UNAUTHORIZED
      // TODO: remove the "response.error === null" check once back-end fixed
      && (response.error === null ||
        (Array.isArray(response.error.errors)
          && response.error.errors.find(error => error.key === 'unauthenticated')
        )
      )
    );
  }
}
