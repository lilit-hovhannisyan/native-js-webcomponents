import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';

import { ReactiveFormsModule } from '@angular/forms';

import { IconsModule } from '@shared/modules/icons.module';

import { MeSearchInputComponent } from './me-search-input.component';

@NgModule({
  declarations: [MeSearchInputComponent],
  imports: [CommonModule, MatInputModule, ReactiveFormsModule, MatButtonModule, MatIconModule, MatFormFieldModule, IconsModule],
  exports: [MeSearchInputComponent]
})
export class MeSearchInputModule {}
