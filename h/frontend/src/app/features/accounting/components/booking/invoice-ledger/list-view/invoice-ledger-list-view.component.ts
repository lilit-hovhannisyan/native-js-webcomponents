import { Component, OnInit, OnDestroy } from '@angular/core';
import { BaseComponent } from 'src/app/core/abstracts/BaseComponent';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { NvGridConfig } from 'nv-grid';
import { Observable, BehaviorSubject, ReplaySubject, Subscription, forkJoin } from 'rxjs';
import { getLedgerGridConfig } from 'src/app/core/constants/nv-grid-configs/invoice-ledger.config';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { IInvoiceLedgerRow, InvoiceLedgerKey } from 'src/app/core/models/resources/IInvoiceLedger';
import { InvoiceLedgerMainService } from 'src/app/core/services/mainServices/invoice-ledger-main.service';
import { InvoiceLedgerRepositoryService } from 'src/app/core/services/repositories/invoice-ledger-repository.service';
import { map, tap, switchMap, skipWhile, takeUntil, filter } from 'rxjs/operators';
import { IWording } from 'src/app/core/models/resources/IWording';
import { IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { MandatorMainService } from 'src/app/core/services/mainServices/mandator-main.service';
import { MandatorRoleRepositoryService } from 'src/app/core/services/repositories/mandator-role.repository.service';
import { uniqBy } from 'lodash';
import { wording } from 'src/app/core/constants/wording/wording';
import { DeleteReasonModalComponent } from 'src/app/shared/components/delete-reason-modal/delete-reason-modal.component';
import { Wording } from 'src/app/core/classes/wording';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { HttpParams } from '@angular/common/http';
import { BoradcastService } from 'src/app/core/services/boradcast.service';
import { VesselMainService } from '../../../../../../core/services/mainServices/vessel-main.service';
import { VesselRoleRepositoryService } from '../../../../../../core/services/repositories/vessel-role.repository.service';

@Component({
  selector: 'nv-invoice-ledger-list-view',
  templateUrl: './invoice-ledger-list-view.component.html',
  styleUrls: ['./invoice-ledger-list-view.component.scss']
})
export class InvoiceLedgerListViewComponent extends BaseComponent implements OnInit, OnDestroy {
  public gridConfig: NvGridConfig;
  public dataSourceSubj = new BehaviorSubject<IInvoiceLedgerRow[]>([]);
  public dataSource$: Observable<IInvoiceLedgerRow[]> = this.dataSourceSubj.asObservable();
  public availableMandators: IMandatorLookup[] = [];

  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  private invoiceLedgers: IInvoiceLedgerRow[] = [];
  private subscription: Subscription;

  constructor(
    bcs: BaseComponentService,
    public gridConfigService: GridConfigService,
    private invoiceLedgerMainService: InvoiceLedgerMainService,
    private invoiceLedgerRepo: InvoiceLedgerRepositoryService,
    private mandatorMainService: MandatorMainService,
    private mandatorRoleRepo: MandatorRoleRepositoryService,
    private broadcastService: BoradcastService,
    private vesselMainService: VesselMainService,
    private vesselRoleRepo: VesselRoleRepositoryService,
  ) {
    super(bcs);
  }

  public ngOnInit() {
    this.initGridConfig();
    this.initData();
  }

  private initGridConfig() {
    const actions = {
      edit: this.editClicked,
      add: () => this.router.navigate([`${this.router.url}/new`]),
      delete: this.deleteClicked,
      clone: this.cloneToBookingForm,
      show: this.showInBookingForm
    };

    this.gridConfig = getLedgerGridConfig(actions, (operation: number) =>
      this.authService.canPerformOperationOnCurrentUrl(operation));
  }

  private initData() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.subscription = forkJoin([
      this.mandatorRoleRepo.fetchAll({ useCache: true }),
      this.vesselRoleRepo.fetchAll({ useCache: true }),
    ])
      .pipe(
        switchMap(([mandatorRoles, vesselRoles]) => {
          return this.invoiceLedgerMainService.getStream()
            .pipe(
              takeUntil(this.componentDestroyed$),
              skipWhile(invoiceLedgers => !invoiceLedgers.length),
              map(invoiceLedgers => ({ invoiceLedgers, mandatorRoles, vesselRoles }))
            );
        }),
        map(({ invoiceLedgers, mandatorRoles, vesselRoles }) => {
          return this.invoiceLedgerMainService.toRows(
            invoiceLedgers.filter(i => {
              return i.mandator.isActive
                && (this.mandatorMainService
                  .mandatorIsAssignedToRole(i.mandatorId, mandatorRoles)
                )
                && (
                  !i.vesselAccounting?.vesselId || this.vesselMainService
                  .vesselIsAssignedToRole(i.vesselAccounting?.vesselId, vesselRoles));
            }));
        }),
        tap(invoiceLedgers => {
          this.availableMandators = uniqBy(invoiceLedgers.map(i => i.mandator), 'id');
          this.invoiceLedgers = invoiceLedgers;
          this.dataSourceSubj.next(this.invoiceLedgers);
        }),
      ).subscribe();
  }

  private editClicked = (id: InvoiceLedgerKey) => {
    this.router.navigate([`${this.router.url}/${id}`]);
  }

  private deleteClicked = (invoiceLedger: IInvoiceLedgerRow) => {
    const subject: IWording = {
      en: `${invoiceLedger.voucherNumber}(${invoiceLedger.invoiceNumber})`,
      de: `${invoiceLedger.voucherNumber}(${invoiceLedger.invoiceNumber})`,
    };

    this.nzModalService.warning({
      nzClassName: `nv-invoiceledger-confirm-modal`,
      nzWidth: 'fit-content',
      nzClosable: true,
      nzContent: DeleteReasonModalComponent,
      nzComponentParams: {
        messages: [
          Wording.replace(wording.general.areYouSureYouWantToDelete, '{subject}', subject),
          wording.accounting.invoiceLedgerDeleteMessage,
        ],
        footerMessage: wording.accounting.invoiceLedgerDeleteFooterMessage,
      },
      nzFooter: null,
      nzOkText: null
    }).afterClose.subscribe(deletionReason => {
      if (deletionReason) {
        this.invoiceLedgerRepo.update(
          {
            ...invoiceLedger,
            deletionReason,
            isDeleted: true
          }, {}).subscribe();
      }
    });
  }

  public onMandatorChecked({ ids, invoiceLedgerYears }: { ids: number[], invoiceLedgerYears: number[] }) {
    this.dataSourceSubj.next(
      this.invoiceLedgers.filter(i => ids.includes(i.mandatorId)
        && invoiceLedgerYears.includes(i.year)
      )
    );
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  private cloneToBookingForm = (invoiceLedger: IInvoiceLedgerRow) => {
    const keysToClone = [
      'mandatorId',
      'year',
      'voucherNumber',
      'debitorCreditorId',
      'invoiceNumber',
      'invoiceDate',
      'receiptDate',
      'discount',
      'paymentDate',
      'currencyId',
      'amount',
      'hasCredit',
      'voyageId',
      'mandatorAccountId',
      'voyageName', // remove this after booking form has autocompelte for voyage
      'companyName',
      'id',
    ];
    let params = new HttpParams();
    keysToClone.forEach(key => {
      if (invoiceLedger[key] !== undefined) {
        params = params.append(key, invoiceLedger[key]);
      }
    });
    params = params.append('book-invoice', 'true');
    const bookingFormPath = sitemap.accounting.children.booking.children.bookings;
    this.broadcastService.listenToBroadcastMessages()
      .pipe(
        filter(msg => msg === 'booked'),
      ).subscribe(() => {
        this.initData();
      });
    window.open(`${url(bookingFormPath)}/new?${params.toString()}`, '_blank');
  }

  private showInBookingForm = (invoiceLedger: IInvoiceLedgerRow) => {
    const bookingPath = url(sitemap.accounting.children.booking.children.bookings);
    this.invoiceLedgerRepo.fetchBooking(invoiceLedger.id)
      .subscribe(booking => {
        window.open(`${bookingPath}/${booking.id}?readOnly=true`, '_blank');
      });
  }
}
