import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthStoreService } from '@core-services/auth-store.service';
import { MeBasicAlertI } from '@shared/me-components/me-basic-alert/me-basic-alert.interface';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class BasePermissionGuard {
  constructor(private authStoreService: AuthStoreService, private basicAlertService: MeBasicAlertService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authStoreService.permissions && this.authStoreService.isAllowed(route.data.permission)) {
      return true;
    } else {
      const alertData: MeBasicAlertI = {
        mode: 'error',
        title: 'Not Allowed',
        content: `You don't have access`
      };
      this.basicAlertService.openBasicAlert(alertData);
      this.router.navigate(['/']);
      return false;
    }
  }
}
