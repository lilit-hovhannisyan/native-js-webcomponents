import { Component, ChangeDetectionStrategy, Input, HostBinding, OnDestroy, Output, EventEmitter, ElementRef, OnInit, ChangeDetectorRef, OnChanges, SimpleChanges, AfterViewInit, ViewChildren, QueryList } from '@angular/core';
import { FormGroup, FormBuilder, ValidatorFn } from '@angular/forms';
import { IRegistrationVesselTask } from 'src/app/core/models/resources/IRegistrationVesselTask';
import { wording } from 'src/app/core/constants/wording/wording';
import { takeUntil, distinctUntilChanged, skip } from 'rxjs/operators';
import { Validators } from 'src/app/core/classes/validators';
import { ReplaySubject } from 'rxjs';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { IVesselTaskTodoItem } from 'src/app/core/models/resources/IVesselTaskTodoItem';
import { isEqual, isNil } from 'lodash';
import { VesselTaskStatus } from 'src/app/core/models/enums/vessel-registration-tasks';
import { VesselTaskHelperService } from '../vessel-task/vessel-task-helper.service';
import { TaskLanguageType } from 'src/app/core/models/resources/IVesselTask';

@Component({
  selector: 'nv-vessel-task-todo-item',
  templateUrl: './vessel-task-todo-item.component.html',
  styleUrls: ['./vessel-task-todo-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VesselTaskTodoItemComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  public wording = wording;

  /** For firing event when component destroys. */
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  /** Visible name labels input elements. */
  @ViewChildren('todoName', {read: ElementRef})
  private inputElements: QueryList<ElementRef>;

  /** Sets [disabled] html-attribute. */
  @HostBinding('attr.disabled')
  @Input()
  public disabled = false;

  /**
   * When `true` - `Name EN` and `Name DE` texts are hidden
   * and if checklist items label with current system language is missing
   * will be shown the value of opposite label when disabled.
   */
  @Input() public showOppositeLangueIfFieldEmpty = false;

  /** Language type from task. By default is system language. */
  @Input() public languageType: TaskLanguageType;

  /** Todo-item's value */
  @Input() public value: IVesselTaskTodoItem = {} as IVesselTaskTodoItem;

  /** Emits event when `form` is being changed. */
  @Output() private valueChange: EventEmitter<IVesselTaskTodoItem>
    = new EventEmitter<IVesselTaskTodoItem>(true);

  /** Emits event when delete-icon being clicked. */
  @Output() public delete: EventEmitter<IVesselTaskTodoItem> = new EventEmitter(true);
  /** Emits event when add-icon being clicked. */
  @Output() public add: EventEmitter<IVesselTaskTodoItem> = new EventEmitter(true);
  /** Emits event when some of inputs got focused. */
  @Output() public focus: EventEmitter<FocusEvent> = new EventEmitter(true);
  /** Emits event when some of inputs got blurred. */
  @Output() public blur: EventEmitter<FocusEvent> = new EventEmitter(true);

  /** Emits event only when status changes. */
  @Output() private statusChange: EventEmitter<VesselTaskStatus> = new EventEmitter(true);

  /** Returns `true` when form is not valid. */
  public get isInvalid(): boolean {
    return this.form ? this.form.invalid : false;
  }

  public get isDisabled(): boolean {
    return this.form?.disabled || false;
  }

  /** True if selected language is `EN`, otherwise - false. */
  public isSystemEnglish = false;

  private get labelEnValue(): string {
    return this.form?.controls.labelEn.value || '';
  }
  private get labelDeValue(): string {
    return this.form?.controls.labelDe.value || '';
  }

  /**
   * If `EN/DE` checkbox in the task component is unchecked - should be
   * shown only label which matches with chosen system language.
   */
  public get showLabelEn(): boolean {
    const whenDisabled: boolean = this.isSystemEnglish
      ? !!this.labelEnValue
      : !this.labelDeValue;
    const show: boolean = (this.isDisabled && this.showOppositeLangueIfFieldEmpty)
      ?  whenDisabled
      : this.isSystemEnglish;
    return show || this.languageType === TaskLanguageType.All;
  }
  public get showLabelDe(): boolean {
    const whenDisabled: boolean = this.isSystemEnglish
      ? !!this.labelDeValue && !this.labelEnValue
      : !!this.labelDeValue;
    const show: boolean = (this.isDisabled && this.showOppositeLangueIfFieldEmpty)
      ? whenDisabled
      : !this.isSystemEnglish;
    return show || this.languageType === TaskLanguageType.All;
  }

  public get showLabelEnWarning(): boolean {
    return this.languageType !== TaskLanguageType.All
      && this.isSystemEnglish
      && !this.labelEnValue
      && !!this.labelDeValue;
  }
  public get showLabelDeWarning(): boolean {
    return this.languageType !== TaskLanguageType.All
      && !this.isSystemEnglish
      && !this.labelDeValue
      && !!this.labelEnValue;
  }

  /** Todo-item's fields form. */
  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private elRef: ElementRef<HTMLElement>,
    private helperService: VesselTaskHelperService,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    this.handleIsEnglishChange();
    this.init(this.value);
  }

  public ngOnChanges({languageType, disabled}: SimpleChanges): void {
    if (languageType && !languageType.isFirstChange) {
      this.setLabelsValidators();
    }
    if (disabled && !disabled.firstChange) {
      /** The initial state will be set in `onInit`. */
      this.setFormDisabledState();
    }
  }

  public ngAfterViewInit(): void {
    /** Automatically should be focused only the label of new todo. */
    if (!(this.value.labelEn && this.value.labelDe)) {
      this.focusFirstLabel();
    }
  }

  /** Inits form-fields of current todo-item. */
  private init(resource: IVesselTaskTodoItem = {} as IVesselTaskTodoItem): void {
    this.form = this.fb.group({
      labelEn: [resource.labelEn || '', this.getLabelValidators()],
      labelDe: [resource.labelDe || '', this.getLabelValidators(false)],
      status: [resource.status || VesselTaskStatus.NotStarted, [Validators.required]],
      started: [resource.started, [Validators.isDateInputInvalid]],
      completed: [resource.completed, [Validators.isDateInputInvalid]],
    });

    this.handleFormValueChange();
    this.handleDateboxesChanges();
    this.handleStatusChange();

    /** Sets initial disable state. */
    this.setFormDisabledState();
    this.cdr.detectChanges();
  }

  /** Returns corresponding validators for label(En/De) field. */
  private getLabelValidators(isEn = true): ValidatorFn[] {
    const matches: boolean = isEn
      ? this.languageType === TaskLanguageType.En || this.languageType === TaskLanguageType.All
      : this.languageType === TaskLanguageType.De || this.languageType === TaskLanguageType.All;
    return matches
      ? [Validators.required, Validators.maxLength(500)]
      : [];
  }

  /** Sets corresponding validators and updates validity of label fields. */
  private setLabelsValidators(): void {
    const { labelEn, labelDe } = this.form.controls;

    labelEn.setValidators(this.getLabelValidators());
    labelDe.setValidators(this.getLabelValidators(false));
    labelEn.updateValueAndValidity();
    labelDe.updateValueAndValidity();
  }

  /** Disables/enables the `form` .*/
  public setFormDisabledState(isDisabled = this.disabled): void {
    if (!this.form) {
      return;
    }

    if (isDisabled) {
      this.form?.disable();
    } else {
      this.form?.enable();
    }
    this.cdr.detectChanges();
  }

  /** Keeps `isEnglish` value. */
  private handleIsEnglishChange(): void {
    this.helperService.isEnglish
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(isEnglish => {
        this.isSystemEnglish = isEnglish;
        this.cdr.detectChanges();
      });
  }

  /** Listens all values changes and emits the value. */
  private handleFormValueChange(): void {
    this.form.valueChanges.pipe(
      takeUntil(this.componentDestroyed$),
      distinctUntilChanged((x, y) => isEqual(x, y)),
      /** Strongly necessary to skip first change which is being emitted with init. */
      skip(1),
    ).subscribe(() => {
      const value: IRegistrationVesselTask = this.form.getRawValue();
      // not needed to change the reference of this.value
      Object.keys(value).forEach(k => {
        this.value[k] = value[k];

        /**
         * Needed to not have Nils in result to prevent unnecessary differences between
         * initial data and current, because backend sends data only with existed values.
         */
        if (isNil(this.value[k])) {
          delete this.value[k];
        }
      });

      this.valueChange.emit(this.value);
    });
  }

  /** Removes form's dateboxes values when dates are invalid. */
  private handleDateboxesChanges(): void {
    const { started, completed } = this.form.controls;

    [started, completed].forEach(control => control.valueChanges.pipe(
      takeUntil(this.componentDestroyed$),
      distinctUntilChanged(),
    ).subscribe(() => {
      if (!completed.errors) {
        return;
      }

      // needed to set valid value to remove red border
      completed.setValue(1);
      completed.setValue(null);
    }));
  }

  private handleStatusChange(): void {
    const { status } = this.form.controls;
    status.valueChanges.pipe(
      takeUntil(this.componentDestroyed$),
      distinctUntilChanged(),
    ).subscribe(() => this.statusChange.emit(status.value));
  }

  /** Scrolls todo-item into view. */
  public scrollIntoView(
    arg: boolean | ScrollIntoViewOptions = { behavior: 'smooth', block: 'center' }
  ): void {
    /**
     * vessel-autocomplete fetches data when card is being opened and loading-spinner
     * is being appeared which is stopping already started but not finished scrolling.
     * setTimeout used to delay the start of scrolling.
     */
    setTimeout(() => this.elRef.nativeElement.scrollIntoView(arg));
  }

  /** Enables form and validates all fields. */
  public validate(): void {
    this.form.enable();
    validateAllFormFields(this.form);
  }

  /** Focuses first visible name label input element. */
  public focusFirstLabel(): void {
    if (!this.inputElements.length) {
      return;
    }
    this.inputElements.first.nativeElement.focus();
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
