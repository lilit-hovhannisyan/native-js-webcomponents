import { RepairBy } from './IStevedoreDamageReport';
import { IResource } from './IResource';
import { IRepairItem, IRepairItemFull } from './IRepairItem';
import { ICurrency } from './ICurrency';
import { IStevedoreDamageDocument } from './IStevedoreDamageDocument';
import { MandatorKey } from './IMandator';

export type StevedoreRepairKey = number;

interface ISharedProps {
  repairDate?: string;
  isReparationFinished: boolean;
  reparationDoneBy?: RepairBy;
  description?: string;
  reparationFinishedDate?: string;
  reparationFinishedBy?: number;
  stevedoreDamageRepairItems?: IRepairItem[];
  stevedoreDamageDocuments?: IStevedoreDamageDocument[];
  stevedoreDamageReportId: number;
  amount?: number;
  invoiceDate?: string;
  currencyId: number;
  internalRemark: string;
  mandatorId: MandatorKey;
}

export interface IStevedoreRepairBase extends ISharedProps, IResource<number> { }

export interface IStevedoreRepairCreate extends ISharedProps { }

export interface IStevedoreRepairDTO extends IStevedoreRepairBase { }

export interface IStevedoreRepair extends IStevedoreRepairDTO { }

export interface IStevedoreRepairFull extends IStevedoreRepair {
  stevedoreDamageRepairItems: IRepairItemFull[];
  currency: ICurrency;
}

