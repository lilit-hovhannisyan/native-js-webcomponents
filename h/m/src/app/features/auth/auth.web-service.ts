import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseWebService } from '../../core/services/base.web-service';

import { LOGIN_FORM_CONTENT_TYPE, MARKETPLACE_BASE_API_URL, MATE_BASE_API_URL } from '@shared/constants';
import { AuthModel } from '@shared-models/auth/auth.model';
import { UserProfileModel } from '@shared-models/user-profile/user-profile.model';
import { AccountModel } from '@shared/models/account/account.model';
@Injectable()
export class AuthWebService {
  constructor(private baseWebService: BaseWebService) {}

  login(data: string): Observable<AuthModel> {
    return this.baseWebService.postRequest<AuthModel, string>(`${MATE_BASE_API_URL}/auth/login`, data, AuthModel, LOGIN_FORM_CONTENT_TYPE);
  }

  getCurrentUser(): Observable<UserProfileModel> {
    return this.baseWebService.getRequest<UserProfileModel>(`${MARKETPLACE_BASE_API_URL}/users/profile`, UserProfileModel);
  }

  requestPasswordReset(data: { userID: string }): Observable<void> {
    return this.baseWebService.postRequest<void, { userID: string }>(`${MATE_BASE_API_URL}/auth/password-reset`, data);
  }

  requestPasswordResetWithToken(data: { newPassword: string; token: string }): Observable<void> {
    return this.baseWebService.postRequest<void, { newPassword: string; token: string }>(
      `${MATE_BASE_API_URL}/auth/password-reset-token`,
      data
    );
  }

  requestChangePassword(data: { oldPassword: string; newPassword: string }): Observable<void> {
    return this.baseWebService.postRequest<void, { oldPassword: string; newPassword: string }>(`${MATE_BASE_API_URL}/auth/password`, data);
  }

  createAccount(data: AccountModel): Observable<void> {
    return this.baseWebService.postRequest<void, AccountModel>(`${MARKETPLACE_BASE_API_URL}/users/user-invite-token`, data, AccountModel);
  }
}
