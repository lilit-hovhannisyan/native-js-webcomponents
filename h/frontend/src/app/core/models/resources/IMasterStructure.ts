import { IResource } from './IResource';
import { IMasterStructureNode } from './IMasterStructureNode';
import { ACCOUNTING_TYPES } from '../enums/accounting-types';
import { IBookingAccount } from './IBookingAccount';
import { MASTER_STRUCTURE_TYPES } from '../enums/master-structure-type';

export type StructureKey = number;

interface ISharedProps {
  name: string;
  nodes: IMasterStructureNode[];
  accountingType: ACCOUNTING_TYPES;
  masterStructureType: MASTER_STRUCTURE_TYPES;
  isActive: boolean;
  hasRemovedAccounts: boolean;
}

export interface IMasterStructureCreate extends ISharedProps { }

export interface IMasterStructureCreate extends ISharedProps { }

export interface IMasterStructureBase extends ISharedProps, IResource<number> { }

export interface IMasterStructureDTO extends IMasterStructureBase { }

export interface IMasterStructure extends IMasterStructureDTO { }

export interface IMasterStructureFull extends IMasterStructure {
  unusedBookingAccounts: IBookingAccount[];
  usedBookingAccounts: IBookingAccount[];
  fullTitle: string;
  titleWithoutStructureType: string;
}








