import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeImageLoaderDirective } from './me-image-loader.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [MeImageLoaderDirective],
  exports: [MeImageLoaderDirective]
})
export class MeImageLoaderModule {}
