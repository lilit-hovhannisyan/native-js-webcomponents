import { TaskType } from '../enums/vessel-registration-tasks';
import { IResource } from './IResource';
import { VesselRegisterKey } from './IVesselRegister';
import { IVesselTask, VesselTaskKey } from './IVesselTask';

interface ISharedProps extends IVesselTask {
  sortIndex: number;
  vesselRegistrationId: VesselRegisterKey;
}

export interface IRegistrationVesselTaskCreate extends ISharedProps { }

export interface IRegistrationVesselTaskBase extends ISharedProps, IResource<VesselTaskKey> { }

export interface IRegistrationVesselTaskDTO extends IRegistrationVesselTaskBase { }

export interface IRegistrationVesselTask extends IRegistrationVesselTaskDTO { }

/**
 * Type-guard: Returns true if @param task is IRegistrationVesselTask.
 * Necessary for separation IVesselTask and IRegistrationVesselTask.
 */
export const isRegistrationTask = (task: Partial<IVesselTask>): task is IRegistrationVesselTask => {
  if (!task) {
    return false;
  }

  return task.type === TaskType.RegistrationVesselTask;
};

/** Type-guard: Returns true if all items of @param tasks is IRegistrationVesselTask. */
export const isRegistrationTasksArray = (
  tasks: IVesselTask[],
): tasks is IRegistrationVesselTask[] => {
  if (!tasks?.length) {
    return false;
  }

  return tasks.every(t => isRegistrationTask(t));
};
