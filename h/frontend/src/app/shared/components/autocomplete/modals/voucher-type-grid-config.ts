import { NvColumnDataType, NvGridConfig, NvGridRowSelectionType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';


export const gridConfig: NvGridConfig = {
  gridName: 'voucherTypesSelectModalGrid',
  rowSelectionType: NvGridRowSelectionType.RadioButton,
  rowButtonsPosition: null,
  hideRefreshButton: true,
  hideSettingsButton: true,
  paging: {
    pageNumber: 1,
    pageSize: 10,
    showTotalItems: true
  },

  columns: [
    {
      key: 'abbreviation',
      title: wording.accounting.abbreviation,
      width: 150,
      isSortable: true,
      filter: {
        values: []
      }
    },
    {
      key: 'displayLabel',
      title: wording.general.name,
      width: 150,
      isSortable: true,
      filter: {
        values: []
      }
    },
    {
      key: 'isAutomaticNumbering',
      title: wording.accounting.autoNumbering,
      width: 200,
      isSortable: true,
      dataType: NvColumnDataType.Boolean,
      filter: {
        controlType: NvFilterControl.Boolean,
        values: []
      }
    },
    {
      key: 'isSystemDefault',
      title: wording.general.systemDefault,
      width: 200,
      isSortable: true,
      dataType: NvColumnDataType.Boolean,
      filter: {
        controlType: NvFilterControl.Boolean,
        values: []
      }
    },
    {
      key: 'isActive',
      title: wording.accounting.active,
      width: 150,
      isSortable: true,
      dataType: NvColumnDataType.Boolean,
      filter: {
        controlType: NvFilterControl.Boolean,
        values: []
      }
    },
  ]
};
