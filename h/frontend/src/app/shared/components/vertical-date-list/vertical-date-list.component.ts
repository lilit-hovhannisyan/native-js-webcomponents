import { Component, OnInit, ChangeDetectionStrategy, Input, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { distinctUntilChanged, takeUntil, tap } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import { wording } from 'src/app/core/constants/wording/wording';
import * as moment from 'moment';
import { orderBy } from 'lodash';
import { Router } from '@angular/router';
import { Validators } from 'src/app/core/classes/validators';

export interface IDiffDate {
  date: string;
  diff: number;
}

@Component({
  selector: 'nv-vertical-date-list',
  templateUrl: './vertical-date-list.component.html',
  styleUrls: ['./vertical-date-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VerticalDateListComponent implements OnInit, OnDestroy {
  @Input() public dates: string[];
  @Input() public defaultSelectedDate: string;

  public control: FormControl = new FormControl('',
    [Validators.isDateInputInvalid]
  );
  public wording = wording;

  private componentDestroyed$ = new ReplaySubject<boolean>(1);

  constructor(
    private router: Router,
  ) { }

  public ngOnInit(): void {
    this.control.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed$),
        distinctUntilChanged(),
        tap(str => this.highlight(str))
      )
      .subscribe();
  }

  private highlight(str: string): void {
    if (this.control.valid && str === null) {
      this.router.navigate([], {
        queryParams: {
          from: this.defaultSelectedDate
        },
        queryParamsHandling: 'merge'
      });
      return;
    }

    if (!str?.length) {
      this.router.navigate([]);
      return;
    }

    const searchDate: moment.Moment = moment(str);

    const diffDates: IDiffDate[] = this.dates.map(date => {
      const diff: number = searchDate.diff(date);
      return {
        diff,
        date,
      };
    });

    const sortedDates: IDiffDate[] = orderBy(diffDates, d => Math.abs(d.diff));

    const closestDate: string = sortedDates
      .find(d => {
        return moment(d.date).isSameOrBefore(searchDate);
      })?.date;

    if (closestDate) {
      this.router.navigate([], {
        queryParams: {
          from: closestDate
        },
        queryParamsHandling: 'merge'
      });
    }
  }

  public onClick(): void {
    // https://navido.atlassian.net/browse/NAVIDO-2803
    // If user selects a timeslice manually, clear
    this.control.patchValue('', { emitEvent: false });
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

}
