import { Directive, Input, ElementRef } from '@angular/core';
import { CdkDropList } from '@angular/cdk/drag-drop';

@Directive({
  selector: '[cdkDropList][dropAutoScrollContainer]',
})
export class CdkDropListAutoScrollDirective {
  @Input() private dropAutoScrollContainer: string;
  private originalElement: ElementRef<HTMLElement>;

  constructor(cdkDropList: CdkDropList) {
    cdkDropList._dropListRef.beforeStarted.subscribe( () => {
      if (!this.originalElement) {
        this.originalElement = cdkDropList.element;
      }

      if (this.dropAutoScrollContainer) {
        const element = this.originalElement.nativeElement.closest(this.dropAutoScrollContainer) as HTMLElement;
        cdkDropList.element = new ElementRef<HTMLElement>(element);
        cdkDropList._dropListRef.element = element;
      } else {
        cdkDropList.element = this.originalElement;
        cdkDropList._dropListRef.element = cdkDropList.element.nativeElement;
      }
    });
  }
}
