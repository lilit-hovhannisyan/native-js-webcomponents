import { Type } from 'class-transformer';
import { UserCreateAttributesModel } from './user-create-attributes.model';

export class UserCreateModel {
  domainName: string = '';
  @Type(() => UserCreateAttributesModel)
  attributes: UserCreateAttributesModel = new UserCreateAttributesModel();
}
