import { AbstractControl } from '@angular/forms';

export interface IInvoiceLedgerControls {
  mandatorId: AbstractControl;
  debitorCreditorId: AbstractControl;
  mandatorAccountId: AbstractControl;
  voyageId: AbstractControl;
  currencyId: AbstractControl;
  voucherNumber: AbstractControl;
  orderNumber: AbstractControl;
  year: AbstractControl;
  invoiceDate: AbstractControl;
  invoiceNumber: AbstractControl;
  discount: AbstractControl;
  paymentDate: AbstractControl;
  amount: AbstractControl;
  hasCredit: AbstractControl;
  taxKey: AbstractControl;
  remark: AbstractControl;
  barcode: AbstractControl;
  receiptDate: AbstractControl;
  creationDate: AbstractControl;
  changedDate: AbstractControl;
  createdBy: AbstractControl;
  changedBy: AbstractControl;
  booked: AbstractControl;
  vesselAccountingId: AbstractControl;

  /**
   * View Control
   */
  changedByChangeDate: AbstractControl;
  /**
   * View Control
   */
  createdByCreateDate: AbstractControl;
  /**
   * View Control
   */
  voucherNumberAbbr: AbstractControl;
  /**
   * View Control
   */
  mandatorDisplayValue: AbstractControl;
  /**
   * View Control
   */
  voucherYear: AbstractControl;
  /**
   * View Control
   */
  deletionReason: AbstractControl;
}

export const invoiceLedgerControls: string[] = [
  'mandatorId',
  'debitorCreditorId',
  'mandatorAccountId',
  'voyageId',
  'currencyId',
  'voucherNumber',
  'orderNumber',
  'year',
  'invoiceDate',
  'invoiceNumber',
  'discount',
  'paymentDate',
  'amount',
  'hasCredit',
  'taxKey',
  'remark',
  'barcode',
  'receiptDate',
  'creationDate',
  'changedDate',
  'createdBy',
  'changedBy',
  'booked',
  'vesselAccountingId',
  'changedByChangeDate',
  'createdByCreateDate',
  'voucherNumberAbbr',
  'mandatorDisplayValue',
  'voucherYear',
  'deletionReason',
];
