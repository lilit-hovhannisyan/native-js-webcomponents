import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeMenuComponent } from './me-menu.component';

@NgModule({
  declarations: [MeMenuComponent],
  imports: [CommonModule],
  exports: [MeMenuComponent]
})
export class MeMenuModule {}
