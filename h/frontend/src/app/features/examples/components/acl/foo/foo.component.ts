import { Component } from '@angular/core';

@Component({
  selector: 'nv-foo',
  templateUrl: './foo.component.html',
  styleUrls: ['./foo.component.scss']
})
export class FooComponent { }
