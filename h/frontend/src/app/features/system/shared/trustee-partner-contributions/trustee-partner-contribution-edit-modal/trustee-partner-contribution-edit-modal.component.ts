import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn } from '@angular/forms';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { map, take, withLatestFrom, startWith, distinctUntilChanged } from 'rxjs/operators';
import { NzModalRef } from 'ng-zorro-antd/modal';
import * as moment from 'moment';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { ICompany, CompanyKey } from 'src/app/core/models/resources/ICompany';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { wording } from 'src/app/core/constants/wording/wording';
import { Validators } from 'src/app/core/classes/validators';
import { ITrusteePartnerContribution, ITrusteePartnerContributionFull } from 'src/app/core/models/resources/ITrusteePartnerContribution';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { IWording } from 'src/app/core/models/resources/IWording';
import { COMPANY_LEGAL_FORM, contributionGridCompanyName, contributionGridContributionAmount, getContributionWordingKey } from 'src/app/core/models/enums/company-legal-form';
import { ICompanyLegalEntity } from 'src/app/core/models/resources/ICompanyLegalEntity';
import { TrusteePartnerContributionMainService, LatestContributionsMap } from 'src/app/core/services/mainServices/trustee-partner-contributions-main.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { CompanyContributionsRepositoryService } from 'src/app/core/services/repositories/company-partner-contribution-repository.service';
import { convertToMap } from 'src/app/shared/helpers/general';
import { ICompanyPartnerContribution } from 'src/app/core/models/resources/ICompanyPartnerContribution';
import { ContributionMainService } from 'src/app/core/services/mainServices/company-contributions-main.service';
import { COMPANY_CONTRIBUTION_TYPES, partnerTrusteeLabelsByLegal, trusteePartnerLabels } from '../enums/company-contributions';


@Component({
  templateUrl: './trustee-partner-contribution-edit-modal.component.html',
  styleUrls: ['../trustee-partner-contribution-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrusteePartnerContributionEditModalComponent implements OnInit {
  @Input() public contribution: ITrusteePartnerContribution;
  @Input() public company: ICompany;
  @Input() public legalEntity: ICompanyLegalEntity;
  @Input() public currency: ICurrency;
  @Input() public currencyEuro: ICurrency;
  @Input() public latestContributionsMap: LatestContributionsMap;
  @Input() public allContributions: ITrusteePartnerContributionFull[];
  @Input() public contributorType: COMPANY_CONTRIBUTION_TYPES;
  @Input() public isNonParValueShare: boolean;
  private otherContributionsMap: LatestContributionsMap;
  public shareholderLatestContributionsMap: Map<CompanyKey, ICompanyPartnerContribution>;

  private amountDiff: number;
  public companyNameWording: IWording;
  public contributionAmountWording: IWording;
  public partnerLabel: IWording;
  public trusteePartnerLabel: IWording;
  public contributionLabel: IWording;

  public form: FormGroup;
  public wording = wording;
  public companyLegalFormTypes = COMPANY_LEGAL_FORM;
  public companiesObservable: Observable<ICompany[]>;
  public partners: ICompany[] = [];

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private companyRepo: CompanyRepositoryService,
    private trusteePartnerContributionMainService: TrusteePartnerContributionMainService,
    private settingsService: SettingsService,
    private contributionsRepo: CompanyContributionsRepositoryService,
    private changeDetectorRef: ChangeDetectorRef,
    private shareholderContributionsMainService: ContributionMainService,
  ) { }

  public ngOnInit() {
    this.setLabels();
    this.amountDiff = this.company.legalForm === COMPANY_LEGAL_FORM.LimitedPartnership ? 1 : 0;
    this.companyNameWording = contributionGridCompanyName.get(this.company.legalForm);
    this.contributionAmountWording = contributionGridContributionAmount.get(
      getContributionWordingKey(this.company.legalForm, this.legalEntity.capitalType)
    );

    this.setOtherContributions();

    forkJoin([
      this.setCompanies().pipe(take(1)),
      this.setPartnerCompanies().pipe(take(1)),
    ]).subscribe(() => this.init());
  }

  private init(): void {
    this.form = this.fb.group({
      from: [
        this.contribution.from,
        [Validators.required], // Other validators will be added dynamically
      ],
      partnerId: [this.contribution.partnerId, [Validators.required]],
      shareholderId: [this.contribution.shareholderId, [Validators.required]],
      companyId: [this.company.id, [Validators.required]],
      contributionAmount: [
        this.contribution.contributionAmount,
        [Validators.required, Validators.maxDecimalDigits(2)], // Other validators will be added dynamically
      ],
      currency: [{ value: this.currency.isoCode, disabled: true }],
      remark: [this.contribution.remark],
    });

    this.partnerChangeHandler();
    this.fromChangeHandler();
    this.changeDetectorRef.detectChanges();
  }

  private getContributionAmountValidators(): ValidatorFn[] {
    return [
      Validators.required,
      Validators.maxDecimalDigits(2),
      Validators.min(this.getMinAmount() + this.amountDiff),
      Validators.max(this.getMaxAmount()),
      Validators.valueShouldNotBeEqualTo(0),
    ];
  }

  private getMaxAmount(): number {
    /**
     * To access form value(getRawValue) used optional chaining, because this method can be
     * called on time of initialization of the form when it doesn't exist yet.
     * Used `getRawValue().partnerId` because `form.value.partnerId` is being changed later.
     */
    const { partnerId, shareholderId } = this.form?.getRawValue();
    const shareholderContributionAmount: number = this.shareholderLatestContributionsMap
      .get(shareholderId || this.contribution.shareholderId).contributionAmount;

    return shareholderContributionAmount;
  }

  private getMinAmount(): number {
    /**
     * To access form value(getRawValue) used optional chaining, because this method can be
     * called on time of initialization of the form when it doesn't exist yet.
     * Used `getRawValue().partnerId` because `form.value.partnerId` is being changed later.
     */
    const { partnerId, shareholderId } = this.form?.getRawValue();
    const contribution: ITrusteePartnerContribution = this.latestContributionsMap.get(
      partnerId || this.contribution.partnerId,
      shareholderId || this.contribution.shareholderId,
    );

    const amount: number = contribution?.contributionAmount || 0;
    // needed to exclude the amount of the current contribution
    const minAmount: number = partnerId === this.contribution.partnerId
      ? amount - this.contribution.contributionAmount
      : amount;
    return minAmount <= 0 ? 0 : -minAmount;
  }

  private setOtherContributions(): void {
    const otherContributions: ITrusteePartnerContributionFull[]
      = this.allContributions.filter(c => c.id !== this.contribution.id);
    this.otherContributionsMap = this.trusteePartnerContributionMainService
      .getCompaniesLatestContributions(otherContributions);
  }

  private setMinDate(): void {
    const { partnerId, shareholderId, from } = this.form.controls;
    const latestContribution: ITrusteePartnerContribution
      = this.otherContributionsMap.get(partnerId.value, shareholderId.value);

    const minDate: string = latestContribution?.from
      && moment(latestContribution.from).startOf('day').utc().toISOString();

    const minDateValidatorArr: ValidatorFn[] = minDate
      ? [Validators.dateShouldBeAfter(minDate, this.settingsService.locale, true)]
      : [];

    from.setValidators([
      Validators.required,
      ...minDateValidatorArr,
    ]);
    from.updateValueAndValidity();
  }

  private partnerChangeHandler(): void {
    const { partnerId, shareholderId, contributionAmount } = this.form.controls;

    combineLatest([
      partnerId.valueChanges.pipe(startWith(partnerId.value)),
      shareholderId.valueChanges.pipe(startWith(partnerId.value))
    ]).subscribe(() => {
      // needed to calculate and set validators one more time
      contributionAmount.setValidators(this.getContributionAmountValidators());
      contributionAmount.markAsDirty();
      contributionAmount.updateValueAndValidity();
      this.setMinDate();
    });
  }

  private fromChangeHandler(): void {
    const { from } = this.form.controls;

    from.valueChanges.pipe(
      startWith(from.value),
      distinctUntilChanged(),
    ).subscribe((date) => {
      // needed to calculate and set validators one more time
      this.resetPartners(date);
    });
  }

  private resetPartners(date: string): void {
    const finalContributions = this.shareholderContributionsMainService.getCompaniesLatestContributions(
      this.shareholderContributionsMainService.filterContributions(
        this.contributionsRepo.getStreamValue(),
        date,
      ),
    );

    this.partners = this.companyRepo.getStreamValue()
      .filter(c => finalContributions.get(c.id)?.contributionAmount > 0 && c.id !== this.company.id);
  }

  private setCompanies(): Observable<ICompany[]> {
    return this.companiesObservable = this.companyRepo.getStream().pipe(
      map((companies: ICompany[]) => {
        return companies.filter(c => c.id !== this.company.id);
      }),
    );
  }

  private setPartnerCompanies(): Observable<ICompany[]> {
    return this.contributionsRepo.fetchCompanyContributions(this.company.id).pipe(
      withLatestFrom(this.companiesObservable),
      map(([shareholdersContributions, companies]) => {
        this.shareholderLatestContributionsMap = this.shareholderContributionsMainService
          .getCompaniesLatestContributions(shareholdersContributions);
        const partnersMap: Map<CompanyKey, ICompanyPartnerContribution>
          = convertToMap<ICompanyPartnerContribution, CompanyKey>(shareholdersContributions, 'partnerId');


        return this.partners = companies.filter(company => !!partnersMap.get(company.id));
      })
    );
  }

  private setLabels(): void {
    this.partnerLabel = partnerTrusteeLabelsByLegal.get(this.company.legalForm).get(this.contributorType);
    this.trusteePartnerLabel = trusteePartnerLabels.get(this.contributorType);
    this.contributionLabel = this.isNonParValueShare
      ? wording.system.numberOfNonParValueShares
      : wording.system.contributionChangeAmount;
  }

  public submit(): void {
    validateAllFormFields(this.form);
    if (this.form.invalid) { return; }

    this.modalRef.close(this.form.getRawValue());
  }

  public cancel(): void {
    this.modalRef.close();
  }
}
