import { Injectable } from '@angular/core';
import { FormControl, FormGroup, AbstractControl, Validators, ValidatorFn } from '@angular/forms';
import { plainToClass } from 'class-transformer';

import { DefinitionsStoreService } from '../../core/services/definitions-store.service';

import { MeAttributeTypes } from '@shared/me-attribute-types';
import { MeAttributeConstraintTypes } from '@shared/me-attribute-constraint-types';

import { ME_URL_REGEX } from '@shared/constants';

import { AttributeGroupModel } from '@shared-models/attribute/attribute.group.model';
import { SingleAttributeGroupModel } from '@shared-models/attribute/single-attribute.group.model';
import { MaterialAttributesModel } from '@shared/models/materials/material-attributes.model';

import { EnumModel } from '@shared-models/enum/enum.model';
import { EnumValueModel } from '@shared-models/enum/enum-value.model';

import { LayoutModel } from '@shared-models/layout/layout.model';
import { LayoutRowModel } from '@shared-models/layout/layout-row.model';
import { LayoutGroupAttributeModel } from '@shared-models/layout/layout-group-attribute.model';
import { LayoutGroupModel } from '@shared-models/layout/layout-group.model';
import {
  AttributeModel,
  AttributeModelTypes,
  DMBooleanAttributeModel,
  DMChoiceAttributeModel,
  DMCompositeAttributeModel,
  DMDateTimeAttributeModel,
  DMDecimalAttributeModel,
  DMFloatAttributeModel,
  DMIntegerAttributeModel,
  DMLongAttributeModel,
  DMMOAListAttributeModel,
  DMObjectRefAttributeModel,
  DMObjectRefListAttributeModel,
  DMStringAttributeModel,
  DMTextAttributeModel,
  DMUrlAttributeModel
} from '@shared-models/attribute/attribute.model';

import {
  ConstraintModel,
  DateTimeRangeConstraintModel,
  DecimalRangeConstraintModel,
  FloatRangeConstraintModel,
  ImmutableConstraintModel,
  IntegerRangeConstraintModel,
  MultiValuedConstraintModel,
  ObjectRefConstraintModel,
  RequiredConstraintModel,
  SingleValuedConstraintModel,
  StringLengthConstraintModel
} from '@shared-models/attribute/attribute.constraint.model';
import { DomainModel } from '@shared/models/domain/domain.model';
@Injectable()
export class AttributesService {
  constructor(private definitionsStoreService: DefinitionsStoreService) {}

  /**
   * Transforms attributes array objects to some inheritance AttributeModel class
   *
   * @param attributes Array of attributes that are going to be transformed
   *
   * @returns Array of attributes that are based on inheritance of AttributeModel class
   */
  transformAttributesArray(attributes: AttributeModel<unknown>[]): AttributeModel<unknown>[] {
    attributes.forEach((attribute, index) => {
      switch (attribute.dataTypeClass) {
        case MeAttributeTypes.BOOLEAN:
          attributes[index] = plainToClass(DMBooleanAttributeModel, attribute);
          break;

        case MeAttributeTypes.CHOICE:
          attributes[index] = plainToClass(DMChoiceAttributeModel, attribute);
          break;

        case MeAttributeTypes.COMPOSITE:
          attributes[index] = plainToClass(DMCompositeAttributeModel, attribute);
          break;

        case MeAttributeTypes.DATE_TIME:
          attributes[index] = plainToClass(DMDateTimeAttributeModel, attribute);
          break;

        case MeAttributeTypes.DECIMAL:
          attributes[index] = plainToClass(DMDecimalAttributeModel, attribute);
          break;

        case MeAttributeTypes.FLOAT:
          attributes[index] = plainToClass(DMFloatAttributeModel, attribute);
          break;

        case MeAttributeTypes.INTEGER:
          attributes[index] = plainToClass(DMIntegerAttributeModel, attribute);
          break;

        case MeAttributeTypes.LONG:
          attributes[index] = plainToClass(DMLongAttributeModel, attribute);
          break;

        case MeAttributeTypes.MOA_LIST:
          attributes[index] = plainToClass(DMMOAListAttributeModel, attribute);
          break;

        case MeAttributeTypes.OBJECT_REF:
          attributes[index] = plainToClass(DMObjectRefAttributeModel, attribute);
          break;

        case MeAttributeTypes.OBJECT_REF_LIST:
          attributes[index] = plainToClass(DMObjectRefListAttributeModel, attribute);
          break;

        case MeAttributeTypes.STRING:
          attributes[index] = plainToClass(DMStringAttributeModel, attribute);
          break;

        case MeAttributeTypes.TEXT:
          attributes[index] = plainToClass(DMTextAttributeModel, attribute);
          break;

        case MeAttributeTypes.URL:
          attributes[index] = plainToClass(DMUrlAttributeModel, attribute);
          break;

        default:
          attributes[index] = plainToClass(AttributeModel, attribute);
      }

      // if constraints exist transform array of constraint objects to some inheritance of Constraint model class
      if (attribute.constraints && attribute.constraints.length > 0) {
        attributes[index].constraints = this.transformConstraints(attribute.constraints);
      }
    });

    return attributes;
  }

  /**
   * Transforms constraints array objects to some inheritance of ConstraintModel class
   *
   * @param constraints Array of constraints that are going to be transformed
   *
   * @returns Array of constraints that are based on inheritance of ConstraintModel class
   */
  private transformConstraints(constraints: ConstraintModel[]): ConstraintModel[] {
    constraints.forEach((constraint, index) => {
      switch (constraint.ruleClass) {
        case MeAttributeConstraintTypes.DATE_TIME_RANGE:
          constraints[index] = plainToClass(DateTimeRangeConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.DECIMAL_RANGE:
          constraints[index] = plainToClass(DecimalRangeConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.FLOAT_RANGE:
          constraints[index] = plainToClass(FloatRangeConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.IMMUTABLE:
          constraints[index] = plainToClass(ImmutableConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.INTEGER_RANGE:
          constraints[index] = plainToClass(IntegerRangeConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.MULTI_VALUED:
          constraints[index] = plainToClass(MultiValuedConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.OBJECT_REF:
          constraints[index] = plainToClass(ObjectRefConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.REQUIRED:
          constraints[index] = plainToClass(RequiredConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.SINGLE_VALUED:
          constraints[index] = plainToClass(SingleValuedConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.STRING_LENGTH:
          constraints[index] = plainToClass(StringLengthConstraintModel, constraint);
          break;

        default:
          constraints[index] = plainToClass(ConstraintModel, constraint);
      }
    });

    return constraints;
  }

  /**
   * Creates Form Group with Form Controls (array of AttributeModel objects)
   *
   * @param attributes Array of AttributeModel attributes
   * @param layoutType layout mode which tell us what validators should be added (SEARCH or VIEW)
   *
   * @returns Created form group
   */
  createFormGroup(attributes: AttributeModel<unknown>[], layoutType: string): FormGroup {
    const formGroup: { [key: string]: AbstractControl } = {};

    // create Form Control for each attribute and add validators if applicable
    attributes.forEach((attribute) => {
      formGroup[attribute.internalName] = new FormControl(attribute.value, this.applyConstraints(attribute, layoutType));
    });

    return new FormGroup(formGroup);
  }

  /**
   * Adds Validators to Form Control, also adds options from enum list
   *
   * @param attribute Attribute that has information about what validators (constraints) should be applied
   * @param layoutType  layout mode which tell us what validators should be added (SEARCH or VIEW)
   *
   * @return Array of Validators
   */
  applyConstraints(attribute: AttributeModel<unknown>, layoutType: string): ValidatorFn[] {
    const validatorsArray: ValidatorFn[] = [];

    attribute.constraints.forEach((constraint: ConstraintModel) => {
      if (constraint instanceof DateTimeRangeConstraintModel) {
        // console.log("TODO: DateTimeRangeConstraintModel");
      }

      if (constraint instanceof DecimalRangeConstraintModel && attribute instanceof DMDecimalAttributeModel) {
        if (constraint.data.min) {
          validatorsArray.push(Validators.min(constraint.data.min));
          attribute.minValue = constraint.data.min;
        }
        if (constraint.data.max) {
          validatorsArray.push(Validators.max(constraint.data.max));
          attribute.maxValue = constraint.data.max;
        }
      }

      if (constraint instanceof FloatRangeConstraintModel && attribute instanceof DMFloatAttributeModel) {
        if (constraint.data.min) {
          validatorsArray.push(Validators.min(constraint.data.min));
          attribute.minValue = constraint.data.min;
        }
        if (constraint.data.max) {
          validatorsArray.push(Validators.max(constraint.data.max));
          attribute.maxValue = constraint.data.max;
        }
      }

      if (constraint instanceof ImmutableConstraintModel) {
        // console.log("TODO: ImmutableConstraintModel");
      }

      if (
        constraint instanceof IntegerRangeConstraintModel &&
        (attribute instanceof DMIntegerAttributeModel || attribute instanceof DMLongAttributeModel)
      ) {
        if (constraint.data.min) {
          validatorsArray.push(Validators.min(constraint.data.min));
          attribute.minValue = constraint.data.min;
        }
        if (constraint.data.max) {
          validatorsArray.push(Validators.max(constraint.data.max));
          attribute.maxValue = constraint.data.max;
        }
      }

      if (
        constraint instanceof MultiValuedConstraintModel &&
        (attribute instanceof DMMOAListAttributeModel || attribute instanceof DMCompositeAttributeModel)
      ) {
        attribute.values = this.getEnumValues(constraint.data.enum);
      }

      if (constraint instanceof ObjectRefConstraintModel) {
        // console.log("TODO: ObjectRefConstraintModel");
      }

      if (constraint instanceof RequiredConstraintModel) {
        if (layoutType /*=== this.constantsService.LAYOUT_VIEW*/) {
          validatorsArray.push(Validators.required);
          attribute.required = true;
        }
      }

      if (constraint instanceof SingleValuedConstraintModel && attribute instanceof DMChoiceAttributeModel) {
        attribute.values = this.getEnumValues(constraint.data.enum);
      }

      if (
        constraint instanceof StringLengthConstraintModel &&
        (attribute instanceof DMStringAttributeModel ||
          attribute instanceof DMUrlAttributeModel ||
          attribute instanceof DMTextAttributeModel)
      ) {
        validatorsArray.push(Validators.minLength(constraint.data.from));
        validatorsArray.push(Validators.maxLength(constraint.data.to));

        attribute.minLength = constraint.data.from;
        attribute.maxLength = constraint.data.to;
      }
    });

    if (attribute instanceof DMUrlAttributeModel) {
      validatorsArray.push(Validators.pattern(ME_URL_REGEX));
    }

    return validatorsArray;
  }

  /**
   * Reads enum objects from store
   * and returns list of Enum values (dropdown options) for desired enum
   *
   * @param enumName enum name for which we want to return values
   *
   * @returns list of Enum values (dropdown options)
   */
  private getEnumValues(enumName: string): EnumValueModel[] {
    const enums: EnumModel[] = this.definitionsStoreService.enums;
    const enumObject: EnumModel | undefined = enums.find((enumObj) => enumObj.name === enumName);

    return enumObject ? enumObject.values : [];
  }

  /**
   * Returns layout groups based on layout type.
   *
   * @param layoutsArray Array of all layouts
   * @param layoutTag tag which tell us which type of layout it is
   *
   * @returns Layout groups
   */
  getLayoutGroups(layoutsArray: LayoutModel[], layoutTag: string): LayoutGroupModel[] {
    const foundLayout: LayoutModel | undefined = layoutsArray.find((layout) => layout.tag === layoutTag);
    return foundLayout ? foundLayout.groups : [];
  }

  /**
   * Returns modified array of LayoutGroupModel class
   * Base values stay the same, only thing that is added is row objects
   * Frontend sorts attributes by row/column and generates those row objects
   * @param layoutGroups layout groups that we receive from backend
   *
   * @returns Modified LayoutGroupModel object that contains rows
   */
  generateLayoutRows(layoutGroups: LayoutGroupModel[]): LayoutGroupModel[] {
    layoutGroups.forEach((layoutGroup, index) => {
      // sort attributes by row and colum properties
      const sortedLayoutGroupAttributes: LayoutGroupAttributeModel[] = layoutGroup.attributes.sort((a, b) => {
        return a[`row`] - b[`row`] || a[`column`] - b[`column`];
      });

      const rowArray: LayoutRowModel[] = [];
      // get how many unique row indexes are in attributes array
      const uniqueRows = [...new Set(layoutGroup.attributes.map((item) => item.row))];

      // for each index generate new row with attributes
      uniqueRows.forEach((row) => {
        const newRow: LayoutRowModel = new LayoutRowModel();
        newRow.rowIndex = row;

        // go through group attributes and place them in correct row
        newRow.attributes = sortedLayoutGroupAttributes.filter((attribute) => {
          return attribute.row === row;
        });

        newRow.attributes.forEach((attribute) => {
          if (attribute.width === '0%') {
            attribute.width = `${Math.floor(100 / newRow.attributes.length)}%`;
          }
        });

        rowArray.push(newRow);
      });

      layoutGroups[index].rows = rowArray;
    });

    return layoutGroups;
  }

  /**
   * Returns attribute object of AttributeModel subclass
   *
   * @param attributes Array of all attributes
   * @param attributeObj Attribute object from layout group
   *
   * @returns Attribut Model object
   */
  getAttributeFromArray(
    attributes: AttributeModel<unknown>[],
    attributeObj: LayoutGroupAttributeModel
  ): AttributeModel<unknown> | undefined {
    return attributes.find((attribute) => attribute.internalName === attributeObj.internalName);
  }

  /**
   * Returns Layout group by internal name
   *
   * @param layoutGroups Array of layout groups
   * @param internalName Layout group internal name
   *
   * @returns Layout group by internal name
   */
  getLayoutGroupByInternalName(layoutGroups: LayoutGroupModel[], internalName: string): LayoutGroupModel | undefined {
    return layoutGroups.find((layoutGroup) => layoutGroup.internalName === internalName);
  }

  /**
   * Returns Layout groups except general info
   *
   * @param layoutGroups Array of layout groups
   * @param internalName Layout group internal name
   *
   * @returns Layout groups except general info
   */
  getLayoutGroupsExceptGeneral(layoutGroups: LayoutGroupModel[], internalName: string): LayoutGroupModel[] | undefined {
    let restOfGroups: LayoutGroupModel[] = [];
    restOfGroups = layoutGroups.filter((group) => group.internalName !== internalName);
    return restOfGroups;
  }

  /**
   * Returns ready data model of layout group with sorted attributes
   *
   * @param layoutGroup Array of layout groups
   * @param layoutAttributes Array of layout Attributes
   * @param materialAttributes Object of material Attributes
   *
   * @returns Ready data model of layout group with sorted attributes
   */
  getAttributesLayoutByGroup(
    layoutGroup: LayoutGroupModel | undefined,
    layoutAttributes: AttributeModel<unknown>[],
    materialAttributes: MaterialAttributesModel
  ): AttributeGroupModel {
    const groupAttributes: AttributeModel<unknown>[] = [];

    if (layoutGroup) {
      for (const groupAttribute of layoutGroup.attributes) {
        const attributeFromArray = this.getAttributeFromArray(layoutAttributes, groupAttribute);
        if (attributeFromArray) {
          groupAttributes.push(attributeFromArray);
        }
      }
    }

    const sortedAttributes = this.getSortedLayoutGroupAttributes(groupAttributes, materialAttributes);

    return {
      attributes: sortedAttributes,
      displayName: layoutGroup?.displayName,
      internalName: layoutGroup?.internalName,
      placement: layoutGroup?.placement
    };
  }

  /**
   * Returns sorted array of attributes for Layout group
   *
   * @param groupAttributes Array of attribute Model objects
   * @param materialAttributes Object of material Attributes
   *
   * @returns Sorted array of attributes for Layout group
   */
  getSortedLayoutGroupAttributes(
    groupAttributes: AttributeModel<unknown>[],
    materialAttributes: MaterialAttributesModel
  ): SingleAttributeGroupModel[] {
    const singleAttributeModelArray: SingleAttributeGroupModel[] = [];

    for (const groupAttribute of groupAttributes) {
      for (const [key, value] of Object.entries(materialAttributes)) {
        if (groupAttribute?.internalName === key) {
          const setSingleAttribute: SingleAttributeGroupModel = new SingleAttributeGroupModel();
          setSingleAttribute.internalName = groupAttribute.internalName;
          setSingleAttribute.displayName = groupAttribute.displayName;
          setSingleAttribute.dataTypeClass = groupAttribute.dataTypeClass;
          setSingleAttribute.value = value;
          singleAttributeModelArray.push(setSingleAttribute);
        }
      }
    }

    return singleAttributeModelArray;
  }

  convertAttributesForDisplaying(groups: AttributeGroupModel[], domain: DomainModel): AttributeGroupModel[] {
    return groups.map((group) => {
      return {
        ...group,
        attributes: group.attributes.map((attr) => {
          let convertedValue: string = '';
          switch (attr.dataTypeClass) {
            case MeAttributeTypes.BOOLEAN:
              convertedValue = attr.value ? 'YES' : 'NO';
              break;

            case MeAttributeTypes.CHOICE:
            case MeAttributeTypes.COMPOSITE:
            case MeAttributeTypes.MOA_LIST:
              convertedValue = this.getEnumDisplayNames(attr, domain);
              break;

            case MeAttributeTypes.DATE_TIME:
            case MeAttributeTypes.DECIMAL:
            case MeAttributeTypes.FLOAT:
            case MeAttributeTypes.INTEGER:
            case MeAttributeTypes.LONG:
            case MeAttributeTypes.STRING:
            case MeAttributeTypes.TEXT:
            case MeAttributeTypes.URL:
              convertedValue = attr.value;
              break;

            default:
              convertedValue = attr.value;
          }
          const convertedAttrModel: SingleAttributeGroupModel = plainToClass(SingleAttributeGroupModel, { ...attr, convertedValue });
          return convertedAttrModel;
        })
      };
    });
  }

  getEnumDisplayNames(attr: SingleAttributeGroupModel, domain: DomainModel): string {
    let ret: string = '';
    const keys: { [key: string]: string | number } = {};
    if (attr.dataTypeClass === MeAttributeTypes.MOA_LIST) {
      attr.value.forEach((val: string) => {
        keys[val] = '';
      });
    } else if (attr.dataTypeClass === MeAttributeTypes.COMPOSITE) {
      attr.value.components.forEach((component: { name: string; percentage: number }) => {
        keys[component.name] = component.percentage;
      });
    } else if (attr.dataTypeClass === MeAttributeTypes.CHOICE) {
      [attr.value].forEach((val: string) => {
        keys[val] = '';
      });
    }

    const domainAttributeModel: AttributeModelTypes | undefined = domain.attributes.find((domainAttr) => {
      return domainAttr.internalName === attr.internalName;
    });

    if (domainAttributeModel) {
      const constraintModel: ConstraintModel | undefined = domainAttributeModel.constraints.find((constraint) => {
        return (
          constraint.ruleClass === MeAttributeConstraintTypes.SINGLE_VALUED ||
          constraint.ruleClass === MeAttributeConstraintTypes.MULTI_VALUED
        );
      });
      if (constraintModel) {
        const multiValuedConstraintModel: MultiValuedConstraintModel = constraintModel as MultiValuedConstraintModel;
        const enumValuesArr: EnumValueModel[] = this.getEnumValues(multiValuedConstraintModel.data.enum);
        enumValuesArr.forEach((evm) => {
          if (keys.hasOwnProperty(evm.value)) {
            if (attr.dataTypeClass === MeAttributeTypes.COMPOSITE) {
              ret = ret === '' ? `${evm.displayName} ${keys[evm.value]}%` : `${ret}, ${evm.displayName} ${keys[evm.value]}%`;
            } else {
              ret = ret === '' ? evm.displayName : `${ret}, ${evm.displayName}`;
            }
          }
        });
      }
    }

    return ret;
  }
}
