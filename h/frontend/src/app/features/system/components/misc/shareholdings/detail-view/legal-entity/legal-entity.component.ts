import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ShareholdingsMainService } from '../../../../../../../core/services/mainServices/shareholdings-main.service';
import { CompanyKey, ICompany } from '../../../../../../../core/models/resources/ICompany';
import { CompanyRepositoryService } from '../../../../../../../core/services/repositories/company-repository.service';
import { ShareholdingsHelperService } from '../../shareholdings.helper.service';
import { shareholdingsLegalEntityZone } from '../../../../../../../authentication/services/zoneService/specialZones';

@Component({
  templateUrl: './legal-entity.component.html',
  styleUrls: ['./legal-entity.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareholdingsLegalEntityComponent implements OnInit {
  public companyId: CompanyKey;
  public customPreAction: boolean;
  public context = shareholdingsLegalEntityZone;

  constructor(
    private activatedRoute: ActivatedRoute,
    private shareholdingsMainService: ShareholdingsMainService,
    private cdr: ChangeDetectorRef,
    private companyService: CompanyRepositoryService,
    private shareholdingsHelperService: ShareholdingsHelperService,
    private router: Router,
  ) { }

  public ngOnInit(): void {
    const shareholdingId: CompanyKey | string = this.activatedRoute.parent.snapshot.params.id;

    if (shareholdingId === 'new') {

      if (!this.shareholdingsHelperService.legalData) {
        this.router.navigate(['base/system/misc/shareholdings']);
        return;
      }

      const newCompany: ICompany = {
        businessNames: [{ name: 'temp', from: new Date().toISOString() }],
        legalForm: this.shareholdingsHelperService.legalData.legalForm,
        legalFormType: this.shareholdingsHelperService.legalData.legalFormType,
        isLegalDepartmentResponsible: true,
        isActive: true,
      } as ICompany;

      this.companyService.create(newCompany, {}).subscribe(company => {
        this.companyId = company.id;
        this.customPreAction = true;
        this.cdr.markForCheck();
        this.shareholdingsHelperService.setLegalData({}); // drop prev settings
      });
      return;
    }

    this.shareholdingsMainService.getShareholdingById(+shareholdingId).subscribe(shareholding => {
      if (shareholding) {
        this.companyId = shareholding.id;
        this.cdr.markForCheck();
      }
    });
  }
}
