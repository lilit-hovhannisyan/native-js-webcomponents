import { IResource } from './IResource';
import { VoyageKey } from './IVoyage';
import { HttpResponse } from '@angular/common/http';
import { IDataResponseBody } from '../responses/IDataResponseBody';
import { IResourceCollection } from './IResourceCollection';

export type VoyageCharterNameKey = number;

interface ISharedProps {
  voyageId: VoyageKey;
  charterName: string;
  from: string;
}

export interface IVoyageCharterNameCreate extends ISharedProps { }

export interface IVoyageCharterNameBase extends ISharedProps, IResource<number> { }

export interface IVoyageCharterNameDTO extends IVoyageCharterNameBase { }

export interface IVoyageCharterName extends IVoyageCharterNameDTO { }

export declare type IVoyageCharterNameResource = HttpResponse<IDataResponseBody<IResourceCollection<IVoyageCharterName>>>;

export interface IVoyageCharterNameFull extends IVoyageCharterName {
  until?: string;
}
