import { NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wordingAccounting } from 'src/app/core/constants/wording/accounting';

export const getMandatorAccountCreateViewGridConfig = (): NvGridConfig => {
  return ({
    gridName: 'mandatorAccountCreateViewGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    rowSelectionType: NvGridRowSelectionType.Checkbox,
    hideRefreshButton: true,
    clearFiltersButton: true,
    columns: [
      {
        key: 'no',
        title: wordingAccounting.accountNo,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wordingAccounting.accountName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isOpenItem',
        title: wordingAccounting.openItem,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isOpex',
        title: wordingAccounting.opex,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'currencyCode',
        title: wordingAccounting.restrictedCurrency,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      }
    ],
    buttons: [],
    toolbarButtons: []
  });
};
