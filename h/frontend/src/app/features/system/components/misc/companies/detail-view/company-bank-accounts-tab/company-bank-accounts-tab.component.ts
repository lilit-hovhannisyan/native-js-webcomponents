import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './company-bank-accounts-tab.component.html',
  styleUrls: ['./company-bank-accounts-tab.component.scss'],
})
export class CompanyBankAccountsTabComponent implements OnInit {
  public companyId: number;

  constructor(
    private route: ActivatedRoute
  ) {}

  public ngOnInit() {
    this.companyId = +this.route.parent.snapshot.params.id;
  }

}
