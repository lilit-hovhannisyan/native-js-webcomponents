import { Component, OnInit, Input } from '@angular/core';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { forkJoin, Observable } from 'rxjs';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { NvGridConfig } from 'nv-grid';
import { ICountry, CountryKey } from 'src/app/core/models/resources/ICountry';
import { CountryRepositoryService } from 'src/app/core/services/repositories/country-repository.service';
import { getFlagHistoryConfig } from 'src/app/core/constants/nv-grid-configs/flag-history.config';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { CompanyKey, ICompany } from 'src/app/core/models/resources/ICompany';
import { PortRepositoryService } from 'src/app/core/services/repositories/port-repository.service';
import { PortKey, IPort } from 'src/app/core/models/resources/IPort';
import { VesselRegisterRepositoryService } from 'src/app/core/services/repositories/vessel-registeries-repository.service';
import { map, tap } from 'rxjs/operators';
import { IVesselRegister } from '../../../core/models/resources/IVesselRegister';

@Component({
  selector: 'nv-flag-history',
  templateUrl: './flag-history.component.html',
  styleUrls: ['./flag-history.component.scss']
})
export class FlagHistoryComponent implements OnInit {
  @Input() public vesselId: VesselKey;
  public flagHistory$: Observable<IVesselRegister[]>;
  public gridConfig: NvGridConfig;
  private countriesMap = new Map<CountryKey, ICountry>();
  private companiesMap = new Map<CompanyKey, ICompany>();
  private portsMap = new Map<PortKey, IPort>();

  constructor(
    public gridConfigService: GridConfigService,
    private countryRepo: CountryRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private portRepo: PortRepositoryService,
    private vesselRegisterRepo: VesselRegisterRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.gridConfig = getFlagHistoryConfig();
    this.fetchDataSource();
  }

  public fetchDataSource(): void {
    this.flagHistory$ = forkJoin([
      this.vesselRegisterRepo.fetchAll({ queryParams: { vesselId: this.vesselId } }),
      this.countryRepo.fetchAll({ useCache: true }),
      this.companyRepo.fetchAll({ useCache: true }),
      this.portRepo.fetchAll({ useCache: true }),
    ]).pipe(
      tap(([registers, countries, companies, ports]) => {
        countries.forEach(c => this.countriesMap.set(c.id, c));
        companies.forEach(c => this.companiesMap.set(c.id, c));
        ports.forEach(p => this.portsMap.set(p.id, p));
      }),
      map(([registers]) => registers.map(r => {
        const flagCountryId: CountryKey = r.secondaryCountryId || r.primaryCountryId;
        const flagCountry: ICountry = this.countriesMap.get(flagCountryId);
        const country: ICountry = this.countriesMap.get(flagCountryId);
        const company: ICompany = this.companiesMap.get(r.secondaryCountryId
          ? r.secondaryCompanyId
          : r.primaryCompanyId);
        const port: IPort = this.portsMap.get(r?.secondaryCountryId
          ? r?.secondaryPortId
          : r?.primaryPortId
        );
        const callSign: string = r?.secondaryCountryId
          ? r?.secondaryCallSign
          : r?.primaryCallSign;

        return {
          ...r,
          flagPath: flagCountry.countryFlagPath,
          countryName: flagCountry.displayLabel,
          portOfRegistry: port && country && `${port.shortName} (${country.isoAlpha3})`,
          callSign,
          company: company?.companyName,
          register: country?.displayLabel,
        };
      }))
    );
  }
}
