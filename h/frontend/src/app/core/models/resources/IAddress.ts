import { IResource } from './IResource';

export type AddressKey = number;

interface ISharedProps {
  city: string;
  countryId: number;
  street: string;
  postalCode: string;
  zipCode: string;
  addressTypeId: number;
  province: string;
  companyId: number;
  isTypeDefault: boolean;
}

export interface IAddressCreate extends ISharedProps { }

export interface IAddressBase extends ISharedProps, IResource<AddressKey> { }

export interface IAddressDTO extends IAddressBase { }

export interface IAddress extends IAddressDTO { }



