import { Injectable } from '@angular/core';
import {
  AbstractRepository,
  RepoParams
} from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import {
  IMaintenanceSchedule,
  IMaintenanceScheduleDTO,
  IMaintenanceScheduleCreate,
  MaintenanceScheduleKey
} from '../../models/resources/IMaintenanceSchedule';
import { map, switchMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { NotificationType } from '../../models/INotification';
import { wordingGeneral } from '../../constants/wording/general';

@Injectable({
  providedIn: 'root'
})
export class MaintenanceScheduleRepository extends AbstractRepository<
IMaintenanceSchedule,
IMaintenanceScheduleDTO,
IMaintenanceScheduleCreate,
RepoParams<IMaintenanceSchedule>,
MaintenanceScheduleKey
> {
  protected resourceType = 'MaintenanceSchedule';

  constructor(
    repositoryService: RepositoryService,
  ) {
    super(repositoryService);
  }

  public getBaseUrl = (): string => '/api/administration/maintenance-schedules';

  public postProcess = (item: IMaintenanceScheduleDTO): IMaintenanceSchedule => item;

  public deactivate(id: MaintenanceScheduleKey): Observable<void> {
    return this.delete(id, { isNotificationHidden: true }).pipe(
      switchMap(() => this.fetchOne(id, {})),
      map(() => {
        this.repositoryService.notificationsService.notify(
          NotificationType.Success,
          wordingGeneral.success,
          wordingGeneral.deactivated,
        );
        return null;
      })
    );
  }
}
