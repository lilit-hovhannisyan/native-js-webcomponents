import { Component, ElementRef, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { sortBy } from 'lodash';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NvGridConfig } from 'nv-grid';
import { combineLatest, Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { NVHttpErrorResponse, ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { getGridConfig as bookingAccountGridConfig, getGridConfig } from 'src/app/core/constants/nv-grid-configs/booking-accounts.config';
import { getGridConfig as commissionTypeGridConfig } from 'src/app/core/constants/nv-grid-configs/commission-types.config';
import { getGridConfig as debitorsCreditorsGridConfig } from 'src/app/core/constants/nv-grid-configs/debitors-creditors.config';
import { getGridConfig as taxKeysGridConfig } from 'src/app/core/constants/nv-grid-configs/mandator-tax-keys.config';
import { getGridConfig as getMandatorGridConfig } from 'src/app/core/constants/nv-grid-configs/mandator.config';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { Language } from 'src/app/core/models/language';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { IBookingAccountRow } from 'src/app/core/models/resources/IBookingAccount';
import { ICommissionType } from 'src/app/core/models/resources/ICommissionType';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { IDebitorCreditor } from 'src/app/core/models/resources/IDebitorCreditor';
import { MandatorKey, IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { billingCycleTypesOptions, IVesselCommission, processingOptions } from 'src/app/core/models/resources/IVesselCommission';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { DebitorCreditorMainService } from 'src/app/core/services/mainServices/debitor-creditor-main.service';
import { IMandatorTaxKeyFull, MandatorTaxKeysMainService } from 'src/app/core/services/mainServices/mandator-tax-keys-main.service';
import { CommissionRepositoryService } from 'src/app/core/services/repositories/commission-repository.service';
import { CommissionTypeRepositoryService } from 'src/app/core/services/repositories/commission-type-repository.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { VesselsHelperService } from '../vessels.helper.service';
import { getDefaultIntervalListItem, fetchFormattedFormValue, fetchFormattedIntervals } from 'src/app/shared/helpers/general';
import { MandatorAccountMainService } from 'src/app/core/services/mainServices/mandator-account-main.service';
import { IMandatorAccount } from 'src/app/core/models/resources/IMandatorAccount';
import { BookingAccountAutocompleteComponent } from '../../../../../../../shared/components/auto-completes/booking-account-autocomplete/booking-account-autocomplete.component';

@Component({
  templateUrl: './vessel-commission.component.html',
  styleUrls: ['./vessel-commission.component.scss']
})
export class VesselCommissionComponent implements OnInit {
  @ViewChild('bookingAccountAutocomplete')
  public bookingAccountAutocomplete: BookingAccountAutocompleteComponent;
  public wording = wording;
  public commissionTypeGridConfig: NvGridConfig = commissionTypeGridConfig({}, () => false);

  // in case we have more than one forms, we could pass the reference to the form element
  // we need with e.g. @ViewChild('form') public formElement: ElementRef;
  public formElement: ElementRef = this.el.nativeElement.querySelector('form');

  constructor(
    private commissionsRepo: CommissionRepositoryService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private commissionTypeRepo: CommissionTypeRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private debitorCreditorService: DebitorCreditorMainService,
    private mandatorTaxKeyService: MandatorTaxKeysMainService,
    private nzMessageService: NzMessageService,
    private settingsService: SettingsService,
    public authService: AuthenticationService,
    private confirmService: ConfirmationService,
    private mandatorAccountRepo: MandatorAccountRepositoryService,
    private vesselsHelperService: VesselsHelperService,
    private mandatorAccountMainService: MandatorAccountMainService,
    private el: ElementRef,
    private cdr: ChangeDetectorRef,
  ) { }

  public processingOptions: ISelectOption[] = processingOptions;
  public billingCycleTypesOptions: ISelectOption[] = billingCycleTypesOptions;
  public debitorsCreditorsGridConfig: NvGridConfig = debitorsCreditorsGridConfig({}, () => false);

  public language: Language;
  public commissionTypes$: Observable<ICommissionType[]>;
  public currencies$: Observable<ICurrency[]>;
  public debitorCreditors$: Observable<IDebitorCreditor[]>;
  public mandatorTaxKeys$: Observable<IMandatorTaxKeyFull[]>;
  public mandatorId: number;
  public inCreationMode: boolean;
  public vesselId: VesselKey;
  public commissions: IVesselCommission[];
  public selectedCommission: IVesselCommission;
  public inEditMode = false;
  public form: FormGroup;
  public disabledFields: string[] = [];
  public node = sitemap.chartering.children.misc.children.vessels.children.id.children.commissions;
  public zone = SitemapEntry.getByNode(this.node).toZone();
  public getGridConfig = getGridConfig;
  public getMandatorGridConfig = getMandatorGridConfig;
  public bookingAccountGridConfig: NvGridConfig = bookingAccountGridConfig({}, () => false);
  public taxKeysGridConfig: NvGridConfig = taxKeysGridConfig({}, () => false);

  public getLabel = (bookingAccount: IBookingAccountRow) => `${bookingAccount.no} ${bookingAccount[getLabelKeyByLanguage(this.language)]}`;
  public getMandatorLabel = (m: IMandatorLookup) => `${m.no} ${m.name}`;

  public ngOnInit() {
    this.language = this.settingsService.language;
    this.commissionTypes$ = this.commissionTypeRepo.fetchAll({}).pipe(map(types => {
      return types.filter(type => type.isActive);
    }));
    this.currencyRepo.fetchAll({}).subscribe();
    this.debitorCreditors$ = this.debitorCreditorService.fetchAll()
      .pipe(map(res => this.debitorCreditorService.toRows(res)));

    const { id } = this.route.parent.snapshot.params;
    this.inCreationMode = id === 'new';
    this.vesselId = id;

    this.init();
    this.fetchCommissions();
  }

  private loadCurrencies() {
    const currencyControl = this.form.get('currencyId');
    this.currencies$ = combineLatest([
      this.currencyRepo.getStream(),
      currencyControl.valueChanges.pipe(startWith(currencyControl.value))
    ]).pipe(
      map(([currencies, currencyId]) =>
        this.currencyRepo.filterActiveOnly(currencies, currencyId))
    );
  }

  public fetchCommissions = (idToSelect?: number) => {
    this.commissionsRepo.fetchAll({ queryParams: { vesselId: this.vesselId } })
      .pipe(
        map(commissions => fetchFormattedIntervals(commissions))
      )
      .subscribe(commissions => {
        this.commissions = commissions || [];
        this.commissions = sortBy(this.commissions, ['from']);

        if (this.commissions.length) {
          const commissionToSelect = idToSelect
            ? this.commissions.find(s => s.id === +idToSelect)
            : getDefaultIntervalListItem(this.commissions);
          this.selectCommission(commissionToSelect
            || this.commissions[this.commissions.length - 1]);
        }
      });
  }

  public selectCommission = (commission: IVesselCommission) => {
    this.selectedCommission = commission;
    this.init(commission);
    this.bookingAccountAutocomplete?.init();
  }

  public intervalClicked = (commissionId: number) => {
    const commision = this.commissions.find(c => c.id === commissionId);
    this.selectCommission(commision);
  }

  public addIntervalClicked = () => {
    this.inCreationMode = true;
    this.selectedCommission = {} as IVesselCommission;
    const latestEntry = this.commissions[this.commissions.length - 1];
    const mandatorAccount: IMandatorAccount = this.mandatorAccountRepo
      .getStreamValue()
      .find(ma => ma.bookingAccountId === latestEntry?.bookingAccountId
        && ma.mandatorId === latestEntry?.mandatorId);

    this.currencyRepo.fetchAll({ useCache: true }).subscribe(currencies => {
      const lastEntryCurrency = latestEntry
        ? currencies.find(c => c.id === latestEntry.currencyId)
        : null;
      const clonedCommission = latestEntry
        ? {
          ...latestEntry,
          from: latestEntry.until,
          until: undefined,
          currencyId: lastEntryCurrency && lastEntryCurrency.isActive
            ? latestEntry.currencyId
            : null,
          bookingAccountId: this.mandatorAccountMainService
            .hasAccessToMandatorAccount(mandatorAccount)
            ? latestEntry.bookingAccountId
            : undefined
        }
        : {} as IVesselCommission;

      delete clonedCommission.id; // we don't need the id in creation mode
      this.init(clonedCommission); // cloning the latest most recent one
      this.enterEditMode();
      this.bookingAccountAutocomplete?.init();
    });
  }

  public enableAccountAndTaxKeyFields(mandatorId: MandatorKey) {
    this.enableDisabledFields();
    this.mandatorTaxKeys$ = this.mandatorTaxKeyService.getStream({ mandatorId: this.mandatorId })
      .pipe(map((mandatorTaxKeys) => this.mandatorTaxKeyService.toRows(mandatorTaxKeys)));
    this.mandatorAccountRepo
      .fetchAll({ queryParams: { mandatorId: this.mandatorId } })
      .subscribe();
  }

  public disableAccountAndTaxKeyFields(): void {
    this.setDisabledFields([
      ...this.getDisabledFields(),
      'bookingAccountId',
      'taxKeyId',
    ]);
  }

  public enterEditMode = () => {
    this.inEditMode = true;
    // for some strange reason, form needs to be enabled in next event-cycle.
    // otherwise it stays disabled
    setTimeout(() => {
      this.form.enable();
      let lastCommission;
      if (this.inCreationMode) {
        lastCommission = this.commissions[this.commissions.length - 1];
      }
      if (
        this.selectedCommission && this.selectedCommission.mandatorId
        || lastCommission && lastCommission.mandatorId
      ) {
        this.enableAccountAndTaxKeyFields(this.selectedCommission.mandatorId || lastCommission.mandatorId);
      } else {
        this.disableAccountAndTaxKeyFields();
      }
    });
  }

  public cancelEditing = () => {
    this.inEditMode = false;
    this.inCreationMode = false;
    this.init(this.selectedCommission);
    // for some strange reason, form needs to be disabled in next event-cycle.
    // otherwise it stays enabled
    setTimeout(() => {
      this.form.disable();
    });
  }

  public init(commission: IVesselCommission = {} as IVesselCommission) {
    this.form = this.fb.group({
      from: [commission.from, [Validators.required]],
      until: [commission.until, [Validators.isAfterFieldDate('from')]],
      commissionTypeId: [commission.commissionTypeId, Validators.required],
      currencyId: [commission.currencyId, []],
      amount: [commission.amount],
      billingCycle: [commission.billingCycle, [Validators.min(1), Validators.max(999)]],
      billingCycleType: [commission.billingCycleType],
      isBillingCycleContinually: [commission.isBillingCycleContinually],
      mandatorId: [commission.mandatorId],
      bookingAccountId: [commission.bookingAccountId],
      costCenter: [commission.costCenter, [Validators.min(1), Validators.max(999999)]],
      taxKeyId: [commission.taxKeyId],
      processing: [commission.processing],
      invoiceRecipientId: [commission.invoiceRecipientId],
      invoicedUntil: [commission.invoicedUntil],
      remark: [commission.remark],
    });
    this.loadCurrencies();
    this.form.disable();
    this.form.markAsPristine();
    this.updateFromFieldOnUntilChange();
    if (commission.mandatorId) {
      this.onMandatorChange(commission.mandatorId);
    }
    this.form.get('mandatorId').valueChanges.subscribe(id => this.onMandatorChange(id));
  }

  public updateFromFieldOnUntilChange() {
    this.form.get('from').valueChanges.subscribe(() => {
      this.form.get('until').updateValueAndValidity();
    });
  }

  public onMandatorChange(id: number | null) {
    this.mandatorId = id;
    if (this.mandatorId !== undefined && this.mandatorId !== null) {
      this.enableDisabledFields();
      this.mandatorTaxKeyService.fetchAll({ mandatorId: this.mandatorId }).subscribe();
      this.mandatorTaxKeys$ = this.mandatorTaxKeyService.getStream({ mandatorId: this.mandatorId })
        .pipe(map((mandatorTaxKeys) => this.mandatorTaxKeyService.toRows(mandatorTaxKeys)));
      this.mandatorAccountRepo
        .fetchAll({ queryParams: { mandatorId: this.mandatorId } })
        .subscribe();
    } else {
      this.disableAccountAndTaxKeyFields();
      this.form.get('taxKeyId').reset();
      this.form.get('bookingAccountId').reset();
    }
  }

  public deleteClicked = () => {
    this.confirmService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: '' },
    }).subscribe((confirmed) => {
      if (confirmed) {
        this.commissionsRepo.delete(this.selectedCommission.id, {}).subscribe(() => {
          this.selectedCommission = {} as IVesselCommission;
          this.fetchCommissions();
          this.cancelEditing();
        });
      }
    });
  }

  public saveClicked = () => {
    if (this.form.invalid) {
      return;
    }

    const value = fetchFormattedFormValue(this.form.getRawValue());

    const freshResource = this.selectedCommission.id
      ? { ...this.selectedCommission, ...value }
      : { ...value, vesselId: this.vesselId };

    this.selectedCommission.id
      ? this.commissionsRepo.update(freshResource, {}).subscribe(spec => {
        this.cancelEditing();
        this.fetchCommissions(spec.id);
      }, this.handleSubmitError)

      : this.commissionsRepo.create(freshResource, {}).subscribe(spec => {
        this.cancelEditing();
        this.fetchCommissions(spec.id);
        this.vesselsHelperService.isCommissionsMissing$.next(false);
      }, this.handleSubmitError);
  }

  public handleSubmitError = (error: NVHttpErrorResponse) => {
    ResourceDetailView.handleSubmitError(error, this.form, this.nzMessageService);
  }

  public setDisabledFields(fieldNames: string[]) {
    this.disabledFields = fieldNames;
    this.disableMarkedFields();
  }

  public getDisabledFields(): string[] {
    return this.disabledFields;
  }

  public enableDisabledFields(): void {
    this.getDisabledFields().forEach(field => {
      this.form.get(field).enable();
    });
  }

  public disableMarkedFields() {
    setTimeout(() => {
      this.disabledFields.forEach(fieldName => {
        this.form.controls[fieldName].disable();
        // following line is neccessary, to be able to disable radio-buttons. ( nz-radaio )
        this.form.controls[fieldName].setValue(this.form.controls[fieldName].value);
      });
    });
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
