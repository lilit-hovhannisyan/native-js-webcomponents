import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { EnumModel } from '@shared-models/enum/enum.model';
import { DomainModel } from '@shared-models/domain/domain.model';
import {
  CertificateDocumentModel,
  LogoModel,
  DocumentModel,
  ThreeDdocumentModel,
  ThumbnailDocument,
  CoverImageModel,
  BaseUploadModel
} from '@shared-models/file-upload/file-upload.model';
import { plainToClass } from 'class-transformer';
@Injectable({
  providedIn: 'root'
})
export class DefinitionsStoreService {
  // flag that indicates if we have received data from web service
  private readonly _dataLoaded = new BehaviorSubject<boolean>(false);
  readonly dataLoaded$ = this._dataLoaded.asObservable();

  private readonly _domains = new BehaviorSubject<DomainModel[]>([]);
  readonly domains$ = this._domains.asObservable();

  private readonly _enums = new BehaviorSubject<EnumModel[]>([]);

  private readonly _thumbnailProperties = new BehaviorSubject<ThumbnailDocument | null>(null);
  readonly thumbnailProperties$ = this._thumbnailProperties.asObservable();

  private readonly _documentProperties = new BehaviorSubject<DocumentModel | null>(null);
  readonly documentProperties$ = this._documentProperties.asObservable();

  private readonly _threeDdocumentProperties = new BehaviorSubject<ThumbnailDocument | null>(null);
  readonly threeDdocumentProperties$ = this._threeDdocumentProperties.asObservable();

  private readonly _logoProperties = new BehaviorSubject<LogoModel | null>(null);
  readonly logoProperties$ = this._logoProperties.asObservable();

  private readonly _coverImageProperties = new BehaviorSubject<CoverImageModel | null>(null);
  readonly coverImageProperties$ = this._coverImageProperties.asObservable();

  private readonly _certificateProperties = new BehaviorSubject<CertificateDocumentModel | null>(null);
  readonly certificateProperties$ = this._certificateProperties.asObservable();

  private readonly _generalUploadProperties = new BehaviorSubject<BaseUploadModel[] | null>(null);
  readonly generalUploadProperties$ = this._generalUploadProperties.asObservable();

  private readonly _domainsMap: Map<string, DomainModel> = new Map();

  // data loaded methods
  get dataLoaded(): boolean {
    return this._dataLoaded.getValue();
  }

  set dataLoaded(dataLoaded: boolean) {
    this._dataLoaded.next(dataLoaded);
  }

  // domains methods
  get domains(): DomainModel[] {
    return this._domains.getValue();
  }

  set domains(domains: DomainModel[]) {
    this._domains.next(domains);
    this.populateDomainsMap(domains);
  }

  getDomain(domain: string): DomainModel {
    return this._domainsMap.get(domain) || new DomainModel();
  }

  getDescendantDomain(rootDomainName: string, descendantDomainName: string): DomainModel | undefined {
    const descendant: DomainModel | undefined = this._domains
      .getValue()
      .find((domain) => domain.internalName === rootDomainName)
      ?.descendants.find((domain) => domain.internalName === descendantDomainName);
    return descendant;
  }

  // enums methods
  get enums(): EnumModel[] {
    return this._enums.getValue();
  }

  set enums(enums: EnumModel[]) {
    this._enums.next(enums);
  }

  // thumbnail properties  methods
  get thumbnailProperties(): ThumbnailDocument | null {
    return this._thumbnailProperties.getValue();
  }

  set thumbnailProperties(data: ThumbnailDocument | null) {
    this._thumbnailProperties.next(data);
  }

  // document properties  methods
  get documentProperties(): DocumentModel | null {
    return this._documentProperties.getValue();
  }

  set documentProperties(data: DocumentModel | null) {
    this._documentProperties.next(data);
  }

  // three d properties  methods
  get threeDdocumentProperties(): ThreeDdocumentModel | null {
    return this._threeDdocumentProperties.getValue();
  }

  set threeDdocumentProperties(data: ThreeDdocumentModel | null) {
    this._threeDdocumentProperties.next(data);
  }

  // logo properties  methods
  get logoProperties(): LogoModel | null {
    return this._logoProperties.getValue();
  }

  set logoProperties(data: LogoModel | null) {
    this._logoProperties.next(data);
  }

  // cover image properties methods
  get coverImageProperties(): CoverImageModel | null {
    return this._coverImageProperties.getValue();
  }

  set coverImageProperties(data: CoverImageModel | null) {
    this._coverImageProperties.next(data);
  }

  // certificate properties methods
  get certificateProperties(): CertificateDocumentModel | null {
    return this._certificateProperties.getValue();
  }

  set certificateProperties(data: CertificateDocumentModel | null) {
    this._certificateProperties.next(data);
  }

  // general upload properties methods
  get generalUploadProperties(): BaseUploadModel[] | null {
    return this._generalUploadProperties.getValue();
  }

  set generalUploadProperties(data: BaseUploadModel[] | null) {
    this._generalUploadProperties.next(data);
  }

  private populateDomainsMap(domains: DomainModel[]): void {
    domains.forEach((domain) => {
      this._domainsMap.set(domain.internalName, plainToClass(DomainModel, domain));
      this.populateDomainsMap(domain.descendants);
    });
  }
}
