import { Component, OnInit, Input, OnDestroy, OnChanges } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { getGridConfig as getPortsGridConfig } from 'src/app/core/constants/nv-grid-configs/ports.config';
import { NvGridConfig } from 'nv-grid';
import { IPort, IPortFull, PortKey } from 'src/app/core/models/resources/IPort';
import { PortMainService } from 'src/app/core/services/mainServices/port-main.service';
import { ICountry } from 'src/app/core/models/resources/ICountry';
import { takeUntil } from 'rxjs/operators';
import { combineLatest, forkJoin, ReplaySubject } from 'rxjs';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { CountryRepositoryService } from 'src/app/core/services/repositories/country-repository.service';
import { VesselRegisterRepositoryService } from 'src/app/core/services/repositories/vessel-registeries-repository.service';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { ICompany } from 'src/app/core/models/resources/ICompany';
import { VesselKey } from 'src/app/core/models/resources/IVessel';


const primaryFields: string[] = [
  'primaryPortId',
  'primaryCallSign',
  'ssrNo',
  'registerCourt',
  'primaryCompanyId',
  'secondaryCountryId',
];

const secondaryFields: string[] = [
  'secondaryPortId',
  'secondaryCallSign',
  'secondaryCompanyId',
];


const readOnlyFields: string[] = [
  'primaryPortId',
  'secondaryPortId',
  'primaryCountryId',
  'secondaryCountryId',
  'primaryCallSign',
  'secondaryCallSign',
  'primaryCompanyId',
  'secondaryCompanyId',
  'registerCourt',
  'ssrNo',
];

@Component({
  selector: 'nv-vessel-primary-secondary-register',
  templateUrl: './vessel-primary-secondary-register.component.html',
  styleUrls: ['./vessel-primary-secondary-register.component.scss'],
})
export class VesselPrimaryAndSecondaryRegisterComponent implements  OnInit, OnChanges, OnDestroy {
  @Input() public vesselId: VesselKey;
  @Input() public inEditMode: boolean;
  @Input() public isReadOnly: boolean;
  @Input() public set formGroup(form: FormGroup) {
    this.form = form;
    this.initFormListeners();
  }

  public isPrimaryGermany: boolean;
  public isSecondaryGermany: boolean;

  public wording = wording;
  private componentDestroyed$ = new ReplaySubject<boolean>(1);

  public primaryCountryFilter: { checkSecondaryCountry: (country: ICountry) => boolean };
  public secondaryCountryFilter: { checkPrimaryCountry: (country: ICountry) => boolean };

  public ports: IPortFull[] = [];
  public portsGridConfig: NvGridConfig = getPortsGridConfig({}, () => false);
  public portsMap = new Map<PortKey, IPortFull>();

  public companies: ICompany[] = [];
  public form: FormGroup;

  constructor(
    private portMainService: PortMainService,
    private countryRepo: CountryRepositoryService,
    private vesselRegistriesRepo: VesselRegisterRepositoryService,
    private companyRepo: CompanyRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.initData();

    if (this.isReadOnly) {
      this.setDisabledFields(readOnlyFields);
    }
  }

  private initData(): void {

    forkJoin([
      this.vesselRegistriesRepo.fetchAll({ queryParams: { vesselId: this.vesselId } }),
      this.portMainService.fetchAll(),
      this.companyRepo.fetchAll({}),
    ]).subscribe(([vesselRegisters, ports, companies]) => {
      this.ports = ports.map(p => ({
        ...p,
        countryIsoAlpha3: `(${p.countryIsoAlpha3})`,
      }));
      this.ports.forEach(p => this.portsMap.set(p.id, p));
      this.companies = companies;
    });
  }

  public initFormListeners(): void {
    this.initPrimaryCountryChangeListener();
    this.initSecondaryCountryChangeListener();
    this.initPrimaryPortAndCountryChangeListener();
    this.initSecondaryPortAndCountryChangeListener();
    this.changePrimaryFieldsStatus();
    this.changeSecondaryFieldsStatus();
    this.updateUntilFieldOnFromChange();
  }

  public ngOnChanges(): void {
    if (this.isReadOnly) {
      this.setDisabledFields(readOnlyFields);
    }
  }

  private setDisabledFields(fieldNames: string[]): void {
    fieldNames.forEach(fieldName => {
      this.form.controls[fieldName].disable();
    });
  }

  private initPrimaryCountryChangeListener(): void {
    const { primaryCountryId } = this.form.controls;

    primaryCountryId.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(value => {
        this.secondaryCountryFilter = {
          checkPrimaryCountry: (country: ICountry) => country.id !== value
        };
        this.changePrimaryFieldsStatus();
      });
  }

  private initSecondaryCountryChangeListener(): void {
    const { secondaryCountryId } = this.form.controls;

    secondaryCountryId.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(value => {
        this.primaryCountryFilter = {
          checkSecondaryCountry: (country: ICountry) => country.id !== value
        };
        this.changeSecondaryFieldsStatus();
      });
  }

  private initPrimaryPortAndCountryChangeListener(): void {
    const { primaryCountryId, primaryPortId } = this.form.controls;

    combineLatest([
      primaryCountryId.valueChanges,
      primaryPortId.valueChanges
    ]).pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => this.validatePrimaryPort());
  }

  private initSecondaryPortAndCountryChangeListener(): void {
    const { secondaryCountryId, secondaryPortId } = this.form.controls;

    combineLatest([
      secondaryCountryId.valueChanges,
      secondaryPortId.valueChanges
    ]).pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => this.validateSecondaryPort());
  }

  public changePrimaryFieldsStatus(): void {
    this.changeFieldsStatus('primaryCountryId', primaryFields, 'isPrimaryGermany');
  }

  public changeSecondaryFieldsStatus(): void {
    this.changeFieldsStatus('secondaryCountryId', secondaryFields, 'isSecondaryGermany');
  }

  private changeFieldsStatus(controlName: string, fields: string[], countryIsGermanyIdentifier: string): void {
    const control: AbstractControl = this.form.get(controlName);

    if (!control.value) { return; }

    this.countryRepo.fetchOne(control.value, { useCache: true })
      .subscribe(country => {
        this[countryIsGermanyIdentifier] = country?.isoAlpha3 === 'DEU';

        if (control.value && this.inEditMode) {
          if (!this.isReadOnly) {
            fields.forEach(field => this.form.get(field).enable());
          }
        } else {
          fields.forEach(field => {
            this.disableAndResetControl(field);
          });
        }
      });
  }

  private updateUntilFieldOnFromChange(): void {
    const { from, until } = this.form.controls;

    from.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => {
        until.updateValueAndValidity();
      });
  }

  private validatePrimaryPort(): void {
    const { primaryCountryId, primaryPortId } = this.form.controls;

    if (!primaryPortId.value || !primaryCountryId.value) {
      return null;
    }

    const port: IPort = this.portsMap.get(primaryPortId.value);

    if (primaryCountryId.value === port?.countryId) {
      return primaryPortId.setErrors(null);
    }

    markControlAsTouched(primaryPortId);
    primaryPortId.setErrors({
      invalidPort: {
        message: wording.chartering.portDoesntBelongToFlagstate
      }
    });
  }

  private validateSecondaryPort(): void {
    const { secondaryCountryId, secondaryPortId } = this.form.controls;

    if (!secondaryPortId.value || !secondaryCountryId.value) {
      return null;
    }

    const port: IPort = this.portsMap.get(secondaryPortId.value);

    if (secondaryCountryId.value === port?.countryId) {
      return secondaryPortId.setErrors(null);
    }

    markControlAsTouched(secondaryPortId);
    secondaryPortId.setErrors({
      invalidPort: {
        message: wording.chartering.portDoesntBelongToFlagstate
      }
    });
  }

  private disableAndResetControl(controlName: string): void {
    const control: AbstractControl = this.form.get(controlName);

    control.disable();
    if (this.inEditMode) {
      control.reset();
    }
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

}
