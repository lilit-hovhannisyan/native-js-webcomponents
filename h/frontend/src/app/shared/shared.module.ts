import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule, Provider, Type } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NvButtonModule } from 'nv-button';
import { NvGridModule } from 'nv-grid';
import { BoxSelectItemComponent } from 'src/app/shared/components/box-select/box-select-item/box-select-item.component';
import { VesselSelectModalComponent } from 'src/app/shared/components/vessel-select-modal/vessel-select-modal.component';
import { AutocompleteComponent } from './components/autocomplete/autocomplete.component';
import { UnsavedChangesGuard } from '../routing/services/unsaved-changes.guard';
import { AboutComponent } from './components/about/about.component';
import { AccessControlComponent } from './components/access-control/access-control.component';
import { AddressFormComponent } from './components/address-form/address-form.component';
import { BankAccountAutocompleteComponent } from './components/auto-completes/bank-account-autocomplete/bank-account-autocomplete.component';
import { BookingAccountAutocompleteComponent } from './components/auto-completes/booking-account-autocomplete/booking-account-autocomplete.component';
import { BookingCodeAutocompleteComponent } from './components/auto-completes/booking-code-autocomplete/booking-code-autocomplete.component';
import { CountryAutocompleteComponent } from './components/auto-completes/country-autocomplete/country-autocomplete.component';
import { CurrencyAutocompleteComponent } from './components/auto-completes/currency-autocomplete/currency-autocomplete.component';
import { DebitorCreditorAutocompleteComponent } from './components/auto-completes/debitor-creditor-autocomplete/debitor-creditor-autocomplete.component';
import { MandatorAutocompleteComponent } from './components/auto-completes/mandator-autocomplete/mandator-autocomplete.component';
import { OperationTypeAutocompleteComponent } from './components/auto-completes/operation-type-autocomplete/operation-type-autocomplete.component';
import { StreamAutocompleteInputWrapperComponent } from './components/auto-completes/stream-autocomplete-input-wrapper/stream-autocomplete-input-wrapper.component';
import { StreamAutocompleteComponent } from './components/auto-completes/stream-autocomplete/stream-autocomplete.component';
import { VoucherTypeAutocompleteComponent } from './components/auto-completes/voucher-type-autocomplete/voucher-type-autocomplete.component';
import { BankAccountRelationGridComponent } from './components/bank-account-relation-grid/bank-account-relation-grid.component';
import { BookingAccountSelectFieldComponent } from './components/booking-account-select-field/booking-account-select-field.component';
import { BoxSelectComponent } from './components/box-select/box-select.component';
import { CharternameDetailViewModalComponent } from './components/chartername-detail-view-modal/chartername-detail-view-modal.component';
import { CharternameGridComponent } from './components/chartername-grid/chartername-grid.component';
import { CurrencySelectComponent } from './components/currency-select/currency-select.component';
import { DateboxComponent } from './components/datebox/datebox.component';
import { DatesRatesTableComponent } from './components/dates-rates-table/dates-rates-table.component';
import { DebitorCreditorSelectFieldComponent } from './components/debitor-creditor-select-field/debitor-creditor-select-field.component';
import { DeleteReasonModalComponent } from './components/delete-reason-modal/delete-reason-modal.component';
import { DetailViewActionsComponent } from './components/detail-view-actions/detail-view-actions.component';
import { EditableRolesPermissionsComponent } from './components/editable-roles-permissions/editable-roles-permissions.component';
import { EmailDomainModalComponent } from './components/email-domain-detail-view-modal/email-domain-modal.component';
import { FieldsetComponent } from './components/fieldset/fieldset.component';
import { ImageCropperComponent } from './components/image-cropper/image-cropper.component';
import { ImageUploaderComponent } from './components/image-uploader/image-uploader.component';
import { InactiveEntitiesCheckboxComponent } from './components/inactive-entities-checkbox/inactive-entities-checkbox.component';
import { InputWrapperComponent } from './components/input-wrapper/input-wrapper.component';
import { ListSelectComponent } from './components/list-select/list-select.component';
import { MaintenanceModeComponent } from './components/maintenance-mode/maintenance-mode.component';
import { MandatorBookingAccountSelectFieldComponent } from './components/mandator-booking-account-select-field/mandator-booking-account-select-field.component';
import { MiniAvatarComponent } from './components/mini-avatar/mini-avatar.component';
import { ModalConfirmationContentComponent } from './components/modal-confirmation-content/modal-confirmation-content.component';
import { ModalFooterComponent } from './components/modal-footer/modal-footer.component';
import { ModalWordingTranslateComponent } from './components/modal-wording-translate/modal-wording-translate.component';
import { NavigationBoardComponent } from './components/navigation-board/navigation-board.component';
import { NvDecimalInputComponent } from './components/nv-decimal-input/nv-decimal-input.component';
import { NvIntervalListComponent } from './components/nv-interval-list/nv-interval-list.component';
import { NvRouterTabsComponent } from './components/nv-router-tabs/nv-router-tabs.component';
import { NvTabComponent } from './components/nv-tab/nv-tab.component';
import { NvTabsetComponent } from './components/nv-tabset/nv-tabset.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PaperViewComponent } from './components/paper-view/paper-view.component';
import { RadioSelectComponent } from './components/radio-select/radio-select.component';
import { RangeCalendarComponent } from './components/range-calendar/range-calendar.component';
import { ReadableRolesPermissionsComponent } from './components/readable-roles-permissions/readable-roles-permissions.component';
import { ResourceSelectComponent } from './components/resource-select/resource-select.component';
import { RouteBackButtonComponent } from './components/route-back-button/route-back-button.component';
import { TabManagerComponent } from './components/tab-manager/tab-manager.component';
import { TextFormatterComponent } from './components/text-formatter/text-formatter.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { TopicContextMenuComponent } from './components/topic-context-menu/topic-context-menu.component';
import { TopicSearchContentComponent } from './components/topic-search/topic-search-content/topic-search-content.component';
import { TopicSearchComponent } from './components/topic-search/topic-search.component';
import { VesselAccountingSelectFieldComponent } from './components/vessel-accounting-select-field/vessel-accounting-select-field.component';
import { VesselSelectFieldComponent } from './components/vessel-select-field/vessel-select-field.component';
import { VoyageSelectFieldComponent } from './components/voyage-select-field/voyage-select-field.component';
import { WizardComponent } from './components/wizard/component/wizard-component';
import { AccessControlDirective } from './directives/access-control.directive';
import { AutofocusDirective } from './directives/autofocus.directive';
import { GooglePlacesDirective } from './directives/google-places.directive';
import { AddressDetailViewComponent } from './navido-components/editable-address-list/detail-view/address-detail-view.component';
import { EditableAddressListComponent } from './navido-components/editable-address-list/editable-address-list.component';
import { BankAccountDetailViewComponent } from './navido-components/editable-bank-account-list/detail-view/bank-account-detail-view.component';
import { EditableBankAccountListComponent } from './navido-components/editable-bank-account-list/editable-bank-account-list.component';
import { ContactDetailViewComponent } from './navido-components/editable-contact-list/detail-view/contact-detail-view.component';
import { EditableContactListComponent } from './navido-components/editable-contact-list/editable-contact-list.component';
import { GridDataSelectComponent } from './navido-components/grid-data-select/grid-data-select.component';
import { HistoryComponent } from './navido-components/history/history.component';
import { MetaComponent } from './navido-components/meta/meta.component';
import { UserCompactComponent } from './navido-components/user-compact/user-compact.component';
import { BitmaskNamePipe } from './pipes/bitmask-name.pipe';
import { CurrentDateFormatPipe } from './pipes/current-date-format.pipe';
import { FilterStreamPipe } from './pipes/filter-stream.pipe';
import { ImgSrc } from './pipes/imgSrc.pipe';
import { LabelKeyPipe } from './pipes/label.pipe';
import { ModifyPipe } from './pipes/modify.pipe';
import { ObjectFirstElPipe } from './pipes/object-first-el.pipe';
import { RateDatePipe } from './pipes/rate-date.pipe';
import { SafeHTMLPipe } from './pipes/safe-html.pipe';
import { SortPipe } from './pipes/sort.pipe';
import { TextReplacePipe } from './pipes/text-replace.pipe';
import { ToCurrentDateFormatPipe } from './pipes/to-current-date-format.pipe';
import { TransformIdPipe } from './pipes/transform-id.pipe';
import { TranslatePipe } from './pipes/translate.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';
import { WizardService } from './services/wizard.service';
import { TabHandlerComponent } from './components/tab-handler/tab-handler.component';
import { ImgPreloadDirective } from './directives/img-preload.directive';
import { NvVerticalListItemComponent } from './components/nv-vertical-list/nv-vertical-list-item/nv-vertical-list-item.component';
import { NvVerticalListComponent } from './components/nv-vertical-list/nv-vertical-list.component';
import { ngZorroImports } from './ng-zorro';
import { CompanyAutocompleteComponent } from './components/auto-completes/company-autocomplete/company-autocomplete.component';
import { CostTypeAutocompleteComponent } from './components/auto-completes/cost-type-autocomplete/cost-type-autocomplete.component';
import { PermissionsGroupsUserModalComponent } from './components/permissions-groups-users-modal/permissions-groups-users-modal.component';
import { PermissionsGroupsMandatorModalComponent } from './components/permissions-groups-mandators-modal/permissions-groups-mandators-modal.component';
import { CostCenterAutocompleteComponent } from './components/auto-completes/cost-center-autocomplete/cost-center-autocomplete.component';
import { PermissionsHistoryComponent } from './components/permissions-history/permissions-history.component';
import { CostUnitAutocompleteComponent } from './components/auto-completes/cost-unit-autocomplete/cost-unit-autocomplete.component';
import { VerticalDateListComponent } from './components/vertical-date-list/vertical-date-list.component';
import { VesselAutocompleteComponent } from './components/auto-completes/vessel-autocomplete/vessel-autocomplete.component';
import { UpdateOnJumpComponent } from './components/update-on-jump/update-on-jump.component';
import { VoucherNumberComponent } from './components/auto-completes/voucher-number/voucher-number.component';
import { AutobankBusinessCaseAutocompleteComponent } from './components/auto-completes/autobank-business-case-autocomplete/autobank-business-case-autocomplete.component';
import { VesselTypeOfRegistrationComponent } from './components/vessel-type-of-registration/vessel-type-of-registration.component';
import { FlagHistoryComponent } from './components/flag-history/flag-history.component';
import { SystemDatePipe } from './pipes/system-date.pipe';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { VesselIntervalRegisterComponent } from './components/vessel-interval-register/vessel-interval-register.component';
import { RegistrationFleetMortgageComponent } from '../features/chartering/components/registrations/registrations-fleet/registrations-fleet-detail-view/registration-fleet-mortgage/registration-fleet-mortgage.component';
import { CdkDropListAutoScrollDirective } from './directives/drag-auto-scroll.directive';
import { ColorSketchModule } from 'ngx-color/sketch';
import { AutoFocusComponent } from './components/auto-focus/auto-focus.component';
import { AutoJumpComponent } from './components/auto-jump/auto-jump.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { VesselTaskComponent } from './components/vessel-task-components/vessel-task/vessel-task.component';
import { VesselTaskListComponent } from './components/vessel-task-components/vessel-task-list/vessel-task-list.component';
import { VesselTaskStatusPickerComponent } from './components/vessel-task-components/vessel-task-status-picker/vessel-task-status-picker.component';
import { VesselTaskTodoItemComponent } from './components/vessel-task-components/vessel-task-todo-item/vessel-task-todo-item.component';
import { VesselTaskTodoListComponent } from './components/vessel-task-components/vessel-task-todo-list/vessel-task-todo-list.component';
import { LabelTranslatePipe } from './pipes/label-translate.pipe';
import { SpyElementDirective } from './directives/spy-element.directive';
import { CalendarModalComponent } from './components/calendar/calendar-modal/calendar-modal.component';
import { CalendarEventModalComponent } from './components/calendar/calendar-event-modal/calendar-event-modal.component';
import { VesselTaskTemplateModalComponent } from './components/vessel-task-components/vessel-task-template-modal/vessel-task-template-modal.component';
import { OneStepSliderComponent } from './components/one-step-slider/one-step-slider.component';
import { NZ_DATE_CONFIG } from 'ng-zorro-antd/i18n';
import { VesselRegisterAutocompleteComponent } from './components/auto-completes/vessel-register-autocomplete/vessel-register-autocomplete.component';

const sharedModules: Array<Type<any> | ModuleWithProviders<{}> | any[]> = [
  CommonModule,
  ReactiveFormsModule,
  FormsModule,
  RouterModule,
  NvButtonModule,
  DragDropModule,
  ColorSketchModule,
  FullCalendarModule,
  ...ngZorroImports,
];

const entryComponents: Array<Type<any> | any[]> = [
  AddressDetailViewComponent,
  ContactDetailViewComponent,
  BankAccountDetailViewComponent,
  GridDataSelectComponent,
  RangeCalendarComponent,
  DatesRatesTableComponent,
  EditableRolesPermissionsComponent,
  ReadableRolesPermissionsComponent,
  ModalWordingTranslateComponent,
  AboutComponent,
  VesselSelectModalComponent,
  CurrencySelectComponent,
  DebitorCreditorSelectFieldComponent,
  VesselAccountingSelectFieldComponent,
  VoyageSelectFieldComponent,
  MandatorBookingAccountSelectFieldComponent,
  ListSelectComponent,
  ModalConfirmationContentComponent,
  BankAccountRelationGridComponent,
  DeleteReasonModalComponent,
  EmailDomainModalComponent,
  CharternameDetailViewModalComponent,
  PermissionsGroupsUserModalComponent,
  PermissionsGroupsMandatorModalComponent,
  VesselTaskComponent,
  VesselTaskTemplateModalComponent,
  CalendarModalComponent,
  CalendarEventModalComponent,
];

const declarations: Array<Type<any> | any[]> = [
  ...entryComponents,
  InputWrapperComponent,
  TimelineComponent,
  WizardComponent,
  ObjectFirstElPipe,
  BitmaskNamePipe,
  SystemDatePipe,
  TruncatePipe,
  ImgSrc,
  GooglePlacesDirective,
  SafeHTMLPipe,
  MiniAvatarComponent,
  PageNotFoundComponent,
  AccessControlDirective,
  CdkDropListAutoScrollDirective,
  AccessControlComponent,
  TabManagerComponent,
  AddressFormComponent,
  ImageUploaderComponent,
  TranslatePipe,
  FilterStreamPipe,
  ModifyPipe,
  TransformIdPipe,
  NavigationBoardComponent,
  PaperViewComponent,
  ModalFooterComponent,
  RouteBackButtonComponent,
  AutofocusDirective,
  DetailViewActionsComponent,
  SortPipe,
  RateDatePipe,
  ImageCropperComponent,
  FieldsetComponent,
  DateboxComponent,
  EditableAddressListComponent,
  AddressDetailViewComponent,
  EditableContactListComponent,
  ContactDetailViewComponent,
  BankAccountDetailViewComponent,
  EditableBankAccountListComponent,
  InactiveEntitiesCheckboxComponent,
  NvIntervalListComponent,
  UserCompactComponent,
  ToCurrentDateFormatPipe,
  CurrentDateFormatPipe,
  LabelKeyPipe,
  NvDecimalInputComponent,
  NvRouterTabsComponent,
  TextReplacePipe,
  NvTabsetComponent,
  NvTabComponent,
  GridDataSelectComponent,
  ResourceSelectComponent,
  TopicSearchComponent,
  TopicSearchContentComponent,
  RangeCalendarComponent,
  DatesRatesTableComponent,
  AutocompleteComponent,
  TopicContextMenuComponent,
  EditableRolesPermissionsComponent,
  ReadableRolesPermissionsComponent,
  ModalWordingTranslateComponent,
  BoxSelectComponent,
  BoxSelectItemComponent,
  VesselSelectFieldComponent,
  AboutComponent,
  VesselSelectModalComponent,
  BookingAccountSelectFieldComponent,
  CurrencySelectComponent,
  DebitorCreditorSelectFieldComponent,
  VesselAccountingSelectFieldComponent,
  VoyageSelectFieldComponent,
  StreamAutocompleteComponent,
  StreamAutocompleteInputWrapperComponent,
  MandatorBookingAccountSelectFieldComponent,
  ListSelectComponent,
  MandatorAutocompleteComponent,
  BookingAccountAutocompleteComponent,
  BookingCodeAutocompleteComponent,
  VoucherTypeAutocompleteComponent,
  DebitorCreditorAutocompleteComponent,
  CountryAutocompleteComponent,
  BankAccountAutocompleteComponent,
  AutobankBusinessCaseAutocompleteComponent,
  CurrencyAutocompleteComponent,
  ModalConfirmationContentComponent,
  TextFormatterComponent,
  BankAccountRelationGridComponent,
  DeleteReasonModalComponent,
  EmailDomainModalComponent,
  HistoryComponent,
  MaintenanceModeComponent,
  CharternameGridComponent,
  CharternameDetailViewModalComponent,
  OperationTypeAutocompleteComponent,
  MetaComponent,
  RadioSelectComponent,
  TabHandlerComponent,
  ImgPreloadDirective,
  NvVerticalListComponent,
  NvVerticalListItemComponent,
  CompanyAutocompleteComponent,
  CostTypeAutocompleteComponent,
  CostCenterAutocompleteComponent,
  CostUnitAutocompleteComponent,
  PermissionsHistoryComponent,
  VerticalDateListComponent,
  VesselAutocompleteComponent,
  UpdateOnJumpComponent,
  // TODO: Move these to chartering if not used outside chartering
  VoucherNumberComponent,
  VesselTypeOfRegistrationComponent,
  FlagHistoryComponent,
  VesselIntervalRegisterComponent,
  RegistrationFleetMortgageComponent,
  AutoFocusComponent,
  AutoJumpComponent,
  CalendarComponent,
  VesselTaskTodoItemComponent,
  VesselTaskStatusPickerComponent,
  VesselTaskTodoListComponent,
  VesselTaskListComponent,
  LabelTranslatePipe,
  SpyElementDirective,
  OneStepSliderComponent,
  VesselRegisterAutocompleteComponent,
];

const providers: Provider[] = [
  WizardService,
  UnsavedChangesGuard,
  {
    provide: NZ_DATE_CONFIG,
    useValue: {
      firstDayOfWeek: 1
    }
  }
];

@NgModule({
  imports: [
    ...sharedModules,
    NvGridModule.forRoot({}),
  ],
  declarations,
  entryComponents: [
    ...entryComponents,
    NvGridModule
  ],
  exports: [
    sharedModules,
    declarations,
    NvGridModule
  ],
})
export class SharedModule {
  public static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers
    };
  }
}

