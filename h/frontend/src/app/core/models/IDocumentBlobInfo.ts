export interface IDocumentBlobInfo {
  blob: Blob;
  filename: string;
}

