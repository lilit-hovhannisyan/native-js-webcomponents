import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { NotificationType } from 'src/app/core/models/INotification';
import { IUser } from 'src/app/core/models/resources/IUser';
import { TaskTemplateCategoryKey } from 'src/app/core/models/resources/Task-Templates/ITaskTemplateCategory';
import { NotificationsService } from 'src/app/core/services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class TaskTemplatesHelperService {
  public selectedCategory = new BehaviorSubject<TaskTemplateCategoryKey>(null);
  public selectedCategory$ = this.selectedCategory.asObservable();

  constructor(
    private authService: AuthenticationService,
    private notificationService: NotificationsService,
  ) { }


  /**
   * returns true if user has permission
   * shows warning and returns false if not
   * @param item
   */
  public checkForSystemDefault(item: { isSystemDefault: boolean }): boolean {
    const user: IUser = this.authService.getUser();
    if (item.isSystemDefault && !user.isSuperAdmin) {
      this.notificationService.notify(
        NotificationType.Warning,
        wording.general.notAllowed,
        wording.general.cannotModifySystemDefaultItems
      );
      return false;
    }
    return true;
  }
}
