import { AppException } from './AppException';

/**
 * Thrown if argument is conflicting or not supported
 */
export class InvalidArgumentException extends AppException {
}
