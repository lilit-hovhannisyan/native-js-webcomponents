import { Component, ElementRef, Injector, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AuthStoreService } from '@core-services/auth-store.service';
import { BaseLoggerComponent } from '@features/tracking-system';
import { ME_OMP_INTRO_VIDEO_URL } from '@shared/constants';
import { MeBasicAlertI } from '@shared/me-components/me-basic-alert/me-basic-alert.interface';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';

import { MeDataImportService } from '@shared/me-components/me-data-import/me-data-import.service';
import { MeIntroductionMessageI } from '@shared/me-components/me-introduction-message/me-introduction-message.interface';
import { MePopupAccountI } from '@shared/me-components/me-popup-account/me-popup-account.interface';
import { MePopupAccountService } from '@shared/me-components/me-popup-account/me-popup-account.service';
import { MePermissionTypes } from '@shared/me-permission-types';
import { MePopupVideoService } from '@shared/me-components/me-popup-video/me-popup-video.service';
import { DataImportModel } from '@shared/models/data-import/data-import.model';
import { Subscription } from 'rxjs';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [MePopupAccountService, MeDataImportService]
})
export class HomeComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  componentVersion: string = '0.0.1';

  @ViewChild('popupImportData', { read: TemplateRef }) popupImportDataRef!: TemplateRef<Component>;
  @ViewChild('inviteForm', { read: TemplateRef }) inviteFormRef!: TemplateRef<Component>;
  @ViewChild('inviteCustomerTextArea') inviteCustomerTextArea!: ElementRef;

  introMsgData!: MeIntroductionMessageI;
  private _modal!: MePopupAccountI;

  i18nStrings: MeTranslationI = {};

  private _getI18nStringsSubscription!: Subscription;

  isImport: boolean = false;
  isCompanyProfileView: boolean = false;
  isCompanyProfileEdit: boolean = false;
  isUserList: boolean = false;
  isMaterialsView: boolean = false;
  isCollectionsView: boolean = false;

  constructor(
    injector: Injector,
    private popupAccountService: MePopupAccountService,
    private meDataImportService: MeDataImportService,
    private basicAlertService: MeBasicAlertService,
    private translate: MeTranslationService,
    private authStoreService: AuthStoreService,
    private videoPopupService: MePopupVideoService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._getI18nStringsSubscription = this.translate.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;
      this.isImport = this.authStoreService.isAllowed(MePermissionTypes.IMPORT);
      this.isCompanyProfileEdit = this.authStoreService.isAllowed(MePermissionTypes.COMPANY_MODIFY);
      this.isCompanyProfileView = this.authStoreService.isAllowed(MePermissionTypes.COMPANY_ANY);
      this.isMaterialsView = this.authStoreService.isAllowed(MePermissionTypes.MATERIAL_VIEW);
      this.isCollectionsView = this.authStoreService.isAllowed(MePermissionTypes.COLLECTION_VIEW);
      this.isUserList = this.authStoreService.isAllowed(MePermissionTypes.COMPANY_USER_CREATE);
    });

    this.introMsgData = {
      content: 'Choose your import settings, then automatically import all your materials, books, and documents from the Library platform.',
      buttonText: 'Import Settings'
    };
  }

  playVideo(): void {
    const FILE_URL: string = ME_OMP_INTRO_VIDEO_URL;
    this.videoPopupService.openVideoPopupAlert({ videoPath: FILE_URL });
  }

  btnEventHandler(): void {
    this._modal = {
      title: 'Data Import Options Form',
      content: this.popupImportDataRef
    };
    this.popupAccountService.openPopupSelect(this._modal);
  }

  handleSendFormData(data: DataImportModel): void {
    this.meDataImportService.addSettings(data);
  }

  openInviteForm(): void {
    this._modal = {
      title: 'Invite your Customers',
      content: this.inviteFormRef
    };
    this.popupAccountService.openPopupSelect(this._modal);
  }

  copyinviteUrl(): void {
    try {
      if (document.queryCommandSupported('copy')) {
        this.inviteCustomerTextArea.nativeElement.focus();
        this.inviteCustomerTextArea.nativeElement.select();
        const successful: boolean = document.execCommand('copy');
        if (successful) {
          const basicAlertData: MeBasicAlertI = {
            mode: 'success',
            title: this.i18nStrings.copied,
            content: this.i18nStrings.theContentIsCopiedSuccessfully
          };
          this.basicAlertService.openBasicAlert(basicAlertData);
          this.popupAccountService.closePopup();
        }
      }
    } catch {
      throw new Error('copy not supported');
    }
  }

  ngOnDestroy(): object {
    if (this._getI18nStringsSubscription) {
      this._getI18nStringsSubscription.unsubscribe();
    }
    return super.ngOnDestroy();
  }
}
