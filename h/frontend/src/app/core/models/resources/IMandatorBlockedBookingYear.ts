import { IResource } from './IResource';

export type MandatorBlockedBookingYearKey = number;

interface ISharedProps {
  mandatorId: number;
  year: number;
  period: number;
  blockedBy: number;
  blockedAt?: string;
}

export interface IMandatorBlockedBookingYearBase extends ISharedProps, IResource<MandatorBlockedBookingYearKey> { }

export interface IMandatorBlockedBookingYearCreate extends ISharedProps { }

export interface IMandatorBlockedBookingYearDTO extends IMandatorBlockedBookingYearBase { }

export interface IMandatorBlockedBookingYear extends IMandatorBlockedBookingYearDTO { }


