import { distinctUntilChanged } from 'rxjs/operators';
import { sortBy } from 'lodash';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NVHttpErrorResponse } from './../../../../../../../core/abstracts/ResourceDetailView';
import { IOffHire, OffHireKey } from './../../../../../../../core/models/resources/IOffHire';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { VoyageKey } from 'src/app/core/models/resources/IVoyage';
import { Validators } from 'src/app/core/classes/validators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { OffHireRepositoryService } from 'src/app/core/services/repositories/off-hire-repository.service';
import { Subscription, merge } from 'rxjs';
import * as moment from 'moment';
import { roundFractionalPart } from 'src/app/core/helpers/general';
import { wording } from 'src/app/core/constants/wording/wording';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { getDefaultIntervalListItem } from 'src/app/shared/helpers/general';

// 1 Day equal to 86400 seconds
const DAY_IN_SECONDS = 86400;

@Component({
  templateUrl: './voyage-off-hire.component.html',
  styleUrls: ['./voyage-off-hire.component.scss'],
})
export class VoyageOffHireComponent implements OnInit {
  public form: FormGroup;
  public offHires: IOffHire[] = [];
  public selectedOffHire: IOffHire;
  public voyageId: VoyageKey;

  public inEditMode = false;
  public inCreationMode = false;
  private disabledFields: string[] = [];
  private formFieldsSubscriptions: Subscription[] = [];

  public node = sitemap.chartering.children.misc.children.voyages.children.id.children.offHire;
  public zone: string = SitemapEntry.getByNode(this.node).toZone();
  public wording = wording;
  public formElement: ElementRef;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private offHireRepo: OffHireRepositoryService,
    private nzMessageService: NzMessageService,
    public authService: AuthenticationService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef
  ) { }

  public ngOnInit(): void {
    const { id } = this.route.parent.snapshot.params;
    this.voyageId = +id;
    this.init();
    this.fetchOffHires();
  }

  public init(offHire: IOffHire = {} as IOffHire): void {
    this.form = this.fb.group({
      from: [offHire.from, [Validators.required, Validators.isDateInputInvalid]],
      until: [offHire.until, [
        Validators.required,
        Validators.isDateInputInvalid,
        Validators.isAfterFieldDate('from', false, 'seconds'),
      ]],
      factor: [offHire.factor, [Validators.required, Validators.max(100), Validators.maxDecimalDigits(5)]],
      location: [offHire.location, [Validators.required, Validators.maxLength(50)]],
      cause: [offHire.cause, [Validators.maxLength(4000)]],
      isCredit: [offHire.isCredit || false, [Validators.required]],
      remark: [offHire.remark, [Validators.maxLength(4000)]],
      totalDays: [],
      daysToCount: [],
    });
    this.calculateTotalDays();
    this.calculateDaysToCount();

    this.disabledFields = ['totalDays', 'daysToCount'];
    this.formFieldsSubscriptions.forEach(s => s.unsubscribe());
    this.formFieldsSubscriptions = [];
    this.setFormRelations();
    this.form.disable();
    this.form.markAsPristine();
  }

  public setFormRelations(): void {
    const { from, until, factor, totalDays } = this.form.controls;
    const fromSub = from.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
      until.updateValueAndValidity();
      this.calculateTotalDays();
    });
    const fromUntilSub = until.valueChanges.pipe(distinctUntilChanged()).subscribe(() => {
      this.calculateTotalDays();
    });

    const daysToCountSub = merge(
      factor.valueChanges.pipe(distinctUntilChanged()),
      totalDays.valueChanges.pipe(distinctUntilChanged()),
    ).subscribe(() => this.calculateDaysToCount());

    this.formFieldsSubscriptions.push(fromUntilSub, daysToCountSub, fromSub);
  }

  public calculateTotalDays(): void {
    const { from, until, totalDays } = this.form.controls;
    const diff = this.secondsToDays(moment(until.value).diff(from.value, 'seconds'));
    totalDays.setValue(diff > 0 ? roundFractionalPart(diff, 5) : null);
  }

  public calculateDaysToCount(): void {
    const { factor, totalDays, daysToCount } = this.form.controls;
    const value = factor.value * totalDays.value;
    daysToCount.setValue(roundFractionalPart(value, 5) || null);
  }

  public fetchOffHires(idToSelect?: OffHireKey): void {
    const queryParams = { voyageId: this.voyageId };
    this.offHireRepo.fetchAll({ queryParams }).subscribe(offHires => {
      this.offHires = sortBy(offHires || [], ['from']);

      if (this.offHires.length) {
        const toSelect = idToSelect
          ? this.offHires.find(elem => elem.id === +idToSelect)
          : getDefaultIntervalListItem(this.offHires);
          this.selectOffHire(toSelect || this.offHires[this.offHires.length - 1]);
      }
    });
  }

  public selectOffHire(offHire: IOffHire): void {
    if (offHire.voyageId) {
      this.voyageId = offHire.voyageId;
    }
    this.selectedOffHire = offHire;
    this.init(offHire);
  }

  public intervalClicked(offHireId: OffHireKey): void {
    const offHire = this.offHires.find(elem => elem.id === offHireId);
    this.selectOffHire(offHire);
  }

  public addIntervalClicked(): void {
    this.inCreationMode = true;
    this.selectedOffHire = {} as IOffHire;
    this.init();
    this.enterEditMode();
  }

  public enterEditMode(): void {
    this.inEditMode = true;
    // form needs to be enabled in next event-cycle. otherwise it stays disabled
    setTimeout(() => {
      this.form.enable();
      this.disableMarkedFields();
    });
  }

  public cancelEditing(): void {
    this.inEditMode = false;
    this.inCreationMode = false;
    this.init(this.selectedOffHire);
    // form needs to be disabled in next event-cycle. otherwise it stays enabled
    setTimeout(() => this.form.disable());
  }

  public saveClicked(): void {
    if (this.form.invalid) { return; }

    const freshResource = {
      ...this.form.value,
      voyageId: this.voyageId,
      id: this.selectedOffHire.id,
    };

    this.selectedOffHire.id
      ? this.offHireRepo.update(freshResource, {})
        .subscribe((offHire: IOffHire) => {
          this.cancelEditing();
          this.fetchOffHires(offHire.id);
        }, this.handleSubmitError)
      : this.offHireRepo.create(freshResource, {})
        .subscribe((offHire: IOffHire) => {
          this.cancelEditing();
          this.fetchOffHires(offHire.id);
        }, this.handleSubmitError);
  }

  private secondsToDays = (seconds: number): number => {
    return seconds / DAY_IN_SECONDS;
  }

  private handleSubmitError = (error: NVHttpErrorResponse) => {
    ResourceDetailView.handleSubmitError(error, this.form, this.nzMessageService);
  }

  private disableMarkedFields(): void {
    this.disabledFields.forEach(f => this.form.get(f).disable());
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
