import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AccountBalanceRepositoryService } from '../repositories/account-balance-reopsitory.service';
import { map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { CurrencyKey } from '../../models/resources/ICurrency';
import { IBookingLineRow } from '../../models/resources/IBookingLine';

@Injectable({
  providedIn: 'root'
})
export class AccountBalanceMainService {
  public amounts = {
    amountLocal: { current: 0, differ: 0, init: 0 },
    amountTransaction: { current: 0, differ: 0, init: 0 }
  };

  private updatedSubject = new BehaviorSubject({});

  constructor(private accountBalanceRepositoryService: AccountBalanceRepositoryService) { }

  public getUpdatedSubject(): Observable<any> {
    return this.updatedSubject.asObservable();
  }

  public updateCurrentBalance(
    mandatorId: number,
    bookingAccountId: number,
    currencyId: number,
    year: number,
    initialGridData: IBookingLineRow[] = [],
    sendUpdateEvent: boolean = false,
    isCloneMode: boolean = false) {
    if (mandatorId && bookingAccountId && currencyId && year) {
      this.accountBalanceRepositoryService.fetchAll(
        {
          queryParams: {
            mandatorId,
            bookingAccountId,
            year,
            currencyId
          }
        }).pipe(
          map(accountBalances => accountBalances[0])
        ).subscribe(accountBalance =>
          this.updateCurrent(
            accountBalance.balanceLocal,
            accountBalance.balance,
            initialGridData,
            sendUpdateEvent,
            isCloneMode));
    }
  }

  public updateCurrent(
    balanceLocal: number,
    balance: number,
    initialGridData: IBookingLineRow[] = [],
    sendUpdateEvent: boolean = false,
    isCloneMode: boolean = false) {
    this.amounts.amountLocal.current = balanceLocal;
    this.amounts.amountTransaction.current = balance;
    this.initialGridAmounts(initialGridData, isCloneMode);
    if (sendUpdateEvent) {
      this.updatedSubject.next(this.amounts);
    }
  }

  public initialGridAmounts = (initData: any[] = [], isCloneMode: boolean = false) => {
    this.amounts.amountLocal.init = 0;
    this.amounts.amountTransaction.init = 0;
    let amountLocalGrid = 0;
    let amountTransactionGrid = 0;

    if (initData.length > 0) {
      const firstFormBookingAccountId = +initData[0].bookingAccountId;
      const firstFormCurrencyId = +initData[0].currencyId;

      initData
        .filter(data => +data.bookingAccountId === firstFormBookingAccountId)
        .forEach(data => {
          amountLocalGrid +=
            data.debitCreditType === 1 ? +data.amountLocal :
              data.debitCreditType === -1 ? -data.amountLocal : 0;

          amountTransactionGrid += +data.currencyId === firstFormCurrencyId ?
            data.debitCreditType === 1 ? +data.amountTransaction :
              data.debitCreditType === -1 ? -data.amountTransaction : 0
            : 0;
        });

      if (isCloneMode) {
        this.amounts.amountLocal.init = this.amounts.amountLocal.current;
        this.amounts.amountTransaction.init = this.amounts.amountTransaction.current;
      } else {
        this.amounts.amountLocal.init = this.amounts.amountLocal.current - amountLocalGrid;
        this.amounts.amountTransaction.init = this.amounts.amountTransaction.current - amountTransactionGrid;
      }

    }
  }

  public updateDiffer = (forms: FormGroup[]) => {
    this.amounts.amountLocal.differ = 0;
    this.amounts.amountTransaction.differ = 0;

    if (forms.length > 0) {
      const firstFormBookingAccountId = +forms[0].get('bookingAccountId').value;
      const firstFormCurrencyId: CurrencyKey = +forms[0].get('currencyId').value;

      forms
        .filter(form => +form.get('bookingAccountId').value === firstFormBookingAccountId)
        .forEach(form => {
          const debitCreditType: number = form.get('debitCreditType').value;
          const amountLocal: number = form.get('amountLocal').value;
          const amountTransaction: number = form.get('amountTransaction').value;

          this.amounts.amountLocal.differ +=
            debitCreditType === 1 ? +amountLocal :
              debitCreditType === -1 ? -amountLocal : 0;

          this.amounts.amountTransaction.differ += +form.get('currencyId').value === firstFormCurrencyId ?
            debitCreditType === 1 ? +amountTransaction :
              debitCreditType === -1 ? -amountTransaction : 0
            : 0;

        });
    }
  }

  public reset() {
    this.amounts.amountLocal.init = 0;
    this.amounts.amountTransaction.init = 0;
    this.updateCurrent(0, 0);
    this.updateDiffer([]);
  }
}
