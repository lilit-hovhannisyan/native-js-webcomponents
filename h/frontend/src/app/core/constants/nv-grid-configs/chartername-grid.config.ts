import { NvColumnDataType, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { IVoyageCharterName } from 'src/app/core/models/resources/IVoyageCharterName';

export const getCharternameGridConfig = (
  actions: IGridConfigActions,
  canPerform: (o: Operations) => boolean,
  isEditable = false,
  isActionsDisabled: (row: IVoyageCharterName) => boolean,
  disabled: boolean,
): NvGridConfig => ({
  gridName: 'VoyagesCharternameGrid',
  hideToolbar: !isEditable || !canPerform(Operations.CREATE),
  rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
  hideAllFilters: true,
  hideRefreshButton: true,
  showPaging: false,
  showFooter: false,
  sortBy: 'from',
  disableDragAndDrop: true,
  disableHideColumns: true,
  disableSortColumns: true,
  hideSettingsButton: true,
  columns: [
    {
      key: 'id',
      visible: false,
    },
    {
      key: 'from',
      title: wording.general.from,
      width: 200,
      isSortable: true,
      dataType: NvColumnDataType.Date,
    },
    {
      key: 'charterName',
      title: wording.technicalManagement.charterName,
      width: 350,
      isSortable: true,
    },
  ],
  buttons: [
    {
      icon: 'edit',
      description: wording.general.edit,
      name: 'editChartername',
      tooltip: wording.general.edit,
      actOnEnter: true,
      actOnDoubleClick: isEditable,
      disabled: (row) => disabled || !isActionsDisabled(row),
      hidden: !isEditable || !canPerform(Operations.UPDATE),
      func: actions.edit,
    },
    {
      icon: 'delete',
      description: wording.general.delete,
      name: 'deleteChartername',
      tooltip: wording.general.delete,
      actOnDoubleClick: false,
      disabled: (raw) => disabled || !isActionsDisabled(raw),
      hidden: !isEditable || !canPerform(Operations.DELETE),
      func: actions.delete,
    }
  ],
  toolbarButtons: [
    {
      title: wording.general.create,
      tooltip: wording.general.create,
      icon: 'plus-circle-o',
      hidden: () => !canPerform(Operations.CREATE),
      func: actions.add,
      disabled: () => disabled,
    }
  ],
});
