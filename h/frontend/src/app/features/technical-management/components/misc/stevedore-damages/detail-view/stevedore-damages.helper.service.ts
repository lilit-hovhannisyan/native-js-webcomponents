
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IStevedoreDamageReportRow, StevedoreDamageReportType, stevedoreDamageStatusOptions } from 'src/app/core/models/resources/IStevedoreDamageReport';
import { SettingsService } from 'src/app/core/services/settings.service';
import { wording } from 'src/app/core/constants/wording/wording';

@Injectable({
  providedIn: 'root'
})
export class StevedoreDamagesHelperService {
  public editViewTitleSubject: BehaviorSubject<string> = new BehaviorSubject('');
  public editViewTitle$ = this.editViewTitleSubject.asObservable();

  constructor(
    private settingsService: SettingsService
  ) { }

  public generateTitle(sdrRow: IStevedoreDamageReportRow) {
    const language = this.settingsService.language;
    const vesselInfo = sdrRow.vesselAccounting
      ? `${sdrRow.vesselAccounting.vesselNo} ${sdrRow.vesselAccounting.vesselName}`
      : sdrRow.vessel.imo;
    const voyageInfo = sdrRow.voyage
      ? `- ${sdrRow.voyage.year}-${sdrRow.voyageNo}`
      : '';
    const damageType = sdrRow.reportType === StevedoreDamageReportType.CrewWork
      ? sdrRow.crewReportNo
      : sdrRow.sdReportNo;
    const statusOption = stevedoreDamageStatusOptions
        .find(option => option.value === sdrRow.status);
    const status = statusOption
      ? statusOption.displayLabel
      : '';
    const reportType = sdrRow.reportType === StevedoreDamageReportType.CrewWork
      ? `CW ${wording.technicalManagement.report[language]}:`
      : `SDD ${wording.technicalManagement.report[language]}:`;
    const title = `
      ${reportType}
      <span>${vesselInfo} ${voyageInfo}</span>
      <span class="damage-type-and-status">
        ${damageType} - ${status[language]} - ${sdrRow.charterer}
      </span>
    `;

    this.editViewTitleSubject.next(title);
  }

}
