import { MeContextMenuI } from '../me-context-menu/me-context-menu-inteface';

export interface MeNewCollectionCardI {
  selectionEnabled: boolean;
  selected: boolean;
  contextMenu: MeContextMenuI;

  oid: string;
  collectionName: string;
  numberOfMaterials: number;
  numberOfCustomers: number;
  numberOfTeams: number;
  thumbnail: string;

  supplierName: string;
  supplierOid: string;
  visibility?: MeVisibilityI;
}

export interface MeVisibilityI {
  label: string;
  fontColorHex: string;
  backgroundColorHex: string;
}
