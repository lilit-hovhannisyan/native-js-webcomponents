import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators } from '../../../core/classes/validators';
import { validateAllFormFields } from '../../../shared/helpers/validateAllFormFields';
import { ResetPassword } from '../../models/reset-password';
import { HttpPost } from 'src/app/core/models/http/http-post';

@Component({
  templateUrl: './reset-password-view.component.html',
  styleUrls: ['./reset-password-view.component.scss']
})
export class ResetPasswordViewComponent implements OnInit {
  public form: FormGroup;
  private queryParams?: { user: string, token: string };

  constructor(
    private httpClient: HttpClient,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  public ngOnInit(): void {

    this.route.queryParams.subscribe((params: any) => {
      this.queryParams = params;
      if (!params.userId || !params.code) {
        throw new Error('params need to contain code and userId');
      } else {
        this.initializeForm(params.userId, params.code);
      }
    });
  }

  public initializeForm(userId: number = null, confirmationCode: string = null): void {
    this.form = this.fb.group({
      newPassword: ['', [Validators.required, Validators.minLength(12)]],
      newPasswordConfirm: ['', [Validators.required, Validators.minLength(12)]],
      userId: [userId, Validators.required],
      confirmationCode: [confirmationCode, Validators.required],
    });
    this.form.setValidators(Validators.passwordConfirming(this.form.get('newPassword'), this.form.get('newPasswordConfirm')));
  }

  public onSubmit(): void {
    if (!this.form.valid) {
      validateAllFormFields(this.form); // show validation error messages after submit button hit
      return;
    }
    this.changePassword();
  }

  public changePassword(): void {
    if (!this.queryParams) {
      throw new Error('params are undefined');
    }
    const payload: ResetPassword = this.form.getRawValue();

    const request = new HttpPost('/api/user-account/password/reset', payload);
    this.httpClient.request(request).subscribe(() => {
      this.router.navigate(['/auth/login']);
    });
  }
}
