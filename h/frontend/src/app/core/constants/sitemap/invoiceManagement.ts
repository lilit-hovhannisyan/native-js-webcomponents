// TODO: add SitemapNode and fix errors
export const invoiceManagement = {
  path: 'invoiceManagement',
  title: {en: 'Invoice Management', de: 'Invoice Management'},
  description: { en: 'Description...', de: 'Beschreibung...' },
  icon: 'assets/icons/88-512.png',
  children: {
    debtorManagement: {
      path: 'debtorManagement',
      title: {en: 'Debtor Management', de: 'Debtor Management'},
      description: { en: 'Description...', de: 'Beschreibung...' },
      url: '',
      icon: 'assets/icons/88-512.png',
      children: {
        issueInvoice: {
          path: 'issueInvoice',
          description: {
            en: 'Use pre-defined templates to issue and automatically process invoices.',
            de: 'Erstellen und buchen Sie Rechnungen und Gutschriften an Hand vordefinierter Vorlagen'
          },
          title: {en: 'Issue Invoice / Credit Note', de: 'Rechnungen/Gutschriften erstellen'},

        },
        manageInvoices: {
          path: 'manageInvoices',
          description: {
            en: 'Manage, track and cancel invoices.',
            de: 'Verwalten und stornieren Sie alle geschriebenen Ausgangsrechnungen'
          },
          title: {en: 'Manage Invoices', de: 'Rechnungen verwalten'},

        },
        monthlyCompensation: {
          path: 'monthlyCompensation',
          description: {
            en: 'Issue monthly operating or pool compensation invoices',
            de: 'Erstellen und buchen Sie monatliche Rechnungen für Bereederungs- und Poolvergütungen '
          },
          title: {en: 'Monthly Compensation Invoicing', de: 'Bereederungs-/Poolvergütung'},

        },
      }
    },
    creditorManagement: {
      path: 'creditorManagement',
      title: {en: 'Creditor Management', de: 'Creditor Management'},
      description: { en: 'Description...', de: 'Beschreibung...' },
      url: '',
      icon: 'assets/icons/88-512.png',
      children: {
        incomingInvoices: {
          path: 'incomingInvoices',
          description: {
            en: 'Manage, track and retrieve incoming invoices.',
            de: 'Verwalten und bearbeiten Sie alle Eingangsrechnungen'
          },
          title: {en: 'Incoming Invoices', de: 'Eingangsrechnungen'},

        },
        printIncomingInvoices: {
          path: 'printIncomingInvoices',
          description: {
            en: '',
            de: ''
          },
          title: {en: 'Print Incoming Invoices', de: 'Druck Eingangsrechnungen'},

        },
      }
    },
    outstandingItems: {
      path: 'outstandingItems',
      title: { en: 'Outstanding Items', de: 'Outstanding Items' },
      description: { en: 'Description...', de: 'Beschreibung...' },
      url: '',
      icon: 'assets/icons/88-512.png',
      children: {
        allocatedOpenItems: {
          path: 'allocatedOpenItems',
          description: {
            en: 'Manage and retrieve allocated open items',
            de: 'Verwalten und stellen Sie ausgezifferte OP\'s wieder her'
          },
          title: {
            en: 'Allocated Open Items Management',
            de: 'Verwaltung gelöschte OP'
          },

        },
        openByVesselCustomer: {
          path: 'openByVesselCustomer',
          description: {
            en: 'View open items per vessel and customer-number',
            de: 'Rufen Sie offene Posten pro Schiff und Kunden-Numer ab'
          },
          title: { en: 'Open Item List Vessel/Customer', de: 'OP Schiff/Lieferant' },

        },
        openItems: {
          path: 'openItems',
          description: {
            en: 'View open items',
            de: 'Verschaffen Sie sich einen Überblick über die offenen Posten'
          },
          title: {
            en: 'Open Item List',
            de: 'Offene Posten anzeigen'
          },

        },
        openPrints: {
          path: 'openPrints',
          description: {
            en: '',
            de: ''
          },
          title: {en: '', de: 'Druck Offene Posten'},

        },
        claimsManagement: {
          path: 'claimsManagement',
          description: {
            en: 'Track and manage claims against debtors using individual parameters.',
            de: 'Rufen Sie offene Forderungen pro Schiff, Charterer oder Kundengruppen ab'
          },
          title: {en: 'Claims Management', de: 'Forderungsmanagement'},

        },
      }
    },
    masterData: {
      path: 'masterData',
      title: {en: 'Master Data', de: 'Master Data'},
      description: { en: 'Description...', de: 'Beschreibung...' },
      url: '',
      icon: 'assets/icons/88-512.png',
      children: {
        masterTemplates: {
          path: 'masterTemplates',
          description: {
            en: 'Set up master templates for invoicing',
            de: 'Richten Sie Vorlagen für Rechnungen und Gutschriften ein'
          },
          title: {
            en: 'Master Templates',
            de: 'Vorlagen'
          },

        },
        invoiceTemplates: {
          path: 'invoiceTemplates',
          description: {
            en: 'Define layout and accounting rules for invoice templates.',
            de: 'Definieren Sie Layout und Buchungsregeln für Rechnungsformulare'
          },
          title: {en: 'Invoice Templates', de: 'Formulare'},

        },
        accountMapping: {
          path: 'accountMapping',
          description: {
            en: 'Assign booking keys to accounts.',
            de: 'Definieren Sie die Zuordnung von Vorgangsschlüsseln zu Buchungskonten'
          },
          title: {en: 'Account Mapping', de: 'Zuordnung Konten'},

        },
        openAccounts: {
          path: 'openAccounts',
          description: {
            en: 'Define open items accounts on company level',
            de: 'Definieren Sie die OP-Konten auf Mandanten-Ebene'
          },
          title: {en: 'Open Item Accounts (Company)', de: 'OP-Konten (Mandant)'},

        },
        debtorCreditor: {
          path: 'debtorCreditor',
          description: {
            en: 'Manage debtor & creditor master data (addresses, discounts, etc.)',
            de: 'Bearbeiten Sie die Kunden-und Lieferantenstammdaten (Adressen, Zahlulngsmodalitäten usw.)'
          },
          title: {en: 'Debtor & Creditor Master ', de: 'Kunden & Lieferanten'},

        },
        debtManangement: {
          path: 'debtManangement',
          description: {
            en: 'Set up and and manage customer groups for claims management.',
            de: 'Definieren und bearbeiten Sie Kundengruppen für das Forderungsmanagement '
          },
          title: {en: 'Debt Manangement Client Groups', de: 'Forderungsmanagement Kundengruppen'},
        }
      }
    }
  }
};
