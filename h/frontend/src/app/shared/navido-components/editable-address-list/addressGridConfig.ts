import { Operations } from './../../../core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { NvGridButtonsPosition, NvFilterControl, NvGridConfig, NvGridRowSelectionType, NvColumnDataType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getAddressGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean,
  isSelectable?: boolean,
): NvGridConfig => {
  return ({
  gridName: 'addressesGridComponent',
  rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
  rowSelectionType: isSelectable ? NvGridRowSelectionType.RadioButton : null,
  showPaging: true,
  hideRefreshButton: true,
  paging: {
    pageNumber: 1,
    pageSize: 100
  },
  columns: [
    {
      key: 'countryName',
      title: wording.general.country,
      width: 150,
      isSortable: true,
      filter: {
        values: [],
        controlType: NvFilterControl.FreeText
      }
    },
    {
      key: 'city',
      title: wording.general.city,
      width: 150,
      isSortable: true,
      filter: {
        values: [],
        controlType: NvFilterControl.FreeText
      }
    },
    {
      key: 'street',
      title: wording.general.street,
      width: 150,
      isSortable: true,
      filter: {
        values: [],
        controlType: NvFilterControl.FreeText
      }
    },
    {
      key: 'zipCode',
      title: wording.general.zipCode,
      width: 150,
      isSortable: true,
      filter: {
        values: [],
        controlType: NvFilterControl.FreeText
      }
    },
    {
      key: 'typeName',
      title: wording.general.addressType,
      width: 150,
      isSortable: true,
      filter: {
        values: [],
        controlType: NvFilterControl.FreeText
      }
    },
    {
      key: 'isTypeDefault',
      title: wording.system.addressTypeDefault,
      width: 150,
      isSortable: true,
      filter: {
        values: [],
        controlType: NvFilterControl.Boolean,
      },
      dataType: NvColumnDataType.Boolean
    }
  ],
  buttons: [
    {
      icon: 'edit',
      description: wording.general.edit,
      name: 'editAddress',
      tooltip: wording.general.edit,
      actOnEnter: true,
      actOnDoubleClick: true,
      hidden: !canPerform(Operations.UPDATE),
      func: actions.edit
    },
    {
      icon: 'delete',
      description: wording.general.delete,
      name: 'deleteAddress',
      tooltip: wording.general.delete,
      actOnDoubleClick: true,
      hidden: !canPerform(Operations.DELETE),
      func: actions.delete,
    }
  ],
  toolbarButtons: [
    {
      title: wording.general.create,
      tooltip: wording.general.create,
      hidden: () => !canPerform(Operations.CREATE),
      icon: 'plus-circle-o',
      func: actions.add
    }
  ]
  });
};
