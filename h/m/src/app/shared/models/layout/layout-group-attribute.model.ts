export class LayoutGroupAttributeModel {
  internalName: string = '';
  row: number = 1;
  column: number = 1;
  width: string = '0%';
}
