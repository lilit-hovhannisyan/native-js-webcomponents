import { NgModule, Type } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { AccountingRoutingModule, DECLARATIONS } from './accounting-routing.module';
import { AccountingRelationsComponent } from './components/autobank/accounting-relations/accounting-relations.component';
import { AutobankRulesDetailViewComponent } from './components/autobank/autobank-rules/autobank-rules-detail-view/autobank-rules-detail-view.component';
import { AutobankRulesViewListComponent } from './components/autobank/autobank-rules/autobank-rules-view-list/autobank-rules-view-list.component';
import { AccountSheetsComponent } from './components/booking/account-sheets/account-sheets.component';
import { AccountBalanceComponent } from './components/booking/booking-grid-inline/account-balance/account-balance.component';
import { BookingFormFieldsInlineComponent } from './components/booking/booking-grid-inline/booking-form-fields/booking-form-fields.component';
import { SidebarFilterComponent } from './components/booking/invoice-ledger/list-view/sidebar-filter/sidebar-filter.component';
import { CompanyAndAddressComponent } from './components/booking/mandators/detail-view/company-and-address/company-and-address.component';
import { MandatorAccountCreateViewComponent } from './components/booking/mandators/detail-view/mandator-account/detail-view/mandator-account-create-view/mandator-account-create-view.component';
import { MandatorAccountEditViewComponent } from './components/booking/mandators/detail-view/mandator-account/detail-view/mandator-account-edit-view/mandator-account-edit-view.component';
import { BlockedYearsSectionComponent } from './components/booking/mandators/detail-view/mandator-basic/blockedYearsSection/blocked-years.component';
import { MandatorBasicComponent } from './components/booking/mandators/detail-view/mandator-basic/mandator-basic.component';
import { MandatorBookingCodesDetailViewComponent } from './components/booking/mandators/detail-view/mandator-booking-codes/detail-view/detail-view.component';
import { MandatorCorporateComponent } from './components/booking/mandators/detail-view/mandator-corporate/mandator-corporate.component';
import { MandatorTaxKeyDetailViewComponent } from './components/booking/mandators/detail-view/mandator-tax-keys/detail-view/detail-view.component';
import { MandatorTaxKeyDetailsComponent } from './components/booking/mandators/detail-view/mandator-tax-keys/list-view/details/details.component';
import { MandatorTaxKeyListViewComponent } from './components/booking/mandators/detail-view/mandator-tax-keys/list-view/list-view.component';
import { MandatorTaxRelInfoComponent } from './components/booking/mandators/detail-view/mandator-tax-keys/list-view/tax-rel-info/tax-rel-info.component';
import { MandatorVoucherTypesDetailViewComponent } from './components/booking/mandators/detail-view/voucher-types/detail-view/detail-view.component';
import { MandatorVoucherTypeEditComponent } from './components/booking/mandators/detail-view/voucher-types/edit-modal/edit-modal.component';
import { MasterStructureBookingAccountsComponent } from './components/booking/master-structures/master-structure-booking-accounts/master-structure-booking-accounts.component';
import { MasterStructureNodeComponent } from './components/booking/master-structures/master-structure-nodes/master-structure-node.component';
import { NodeSummationComponent } from './components/booking/master-structures/node-summation/node-summation.component';
import { SwitchingNodesComponent } from './components/booking/master-structures/switching-nodes/switching-nodes.component';
import { CostAccountingRelationListViewComponent } from './components/booking/booking-accounts/detail-view/cost-accounting-relation/list-view/list-view.component';
import { CostAccountingRelationDetailViewComponent } from './components/booking/booking-accounts/detail-view/cost-accounting-relation/detail-view/detail-view.component';
import { CostUnitMandatorsModalComponent } from './components/cost-accounting/cost-units/detail-view/cost-unit-mandators-modal/cost-unit-mandators-modal.component';
import { IncomingInvoiceComponent } from './components/booking/booking-grid-inline/incoming-invoice/incoming-invoice.component';

const entryComponents: Array<Type<any> | any[]> = [
  MandatorVoucherTypesDetailViewComponent,
  MandatorBasicComponent,
  MandatorAccountEditViewComponent,
  MandatorTaxKeyDetailViewComponent,
  MandatorTaxKeyDetailsComponent,
  MandatorTaxKeyListViewComponent,
  MandatorBookingCodesDetailViewComponent,
  CompanyAndAddressComponent,
  MandatorAccountCreateViewComponent,
  MandatorVoucherTypeEditComponent,
  MasterStructureBookingAccountsComponent,
  NodeSummationComponent,
  SwitchingNodesComponent
];

@NgModule({
  imports: [
    AccountingRoutingModule,
    SharedModule
  ],
  declarations: [
    ...DECLARATIONS,
    ...entryComponents,
    MandatorTaxRelInfoComponent,
    BookingFormFieldsInlineComponent,
    BlockedYearsSectionComponent,
    AccountSheetsComponent,
    MandatorCorporateComponent,
    AccountBalanceComponent,
    SidebarFilterComponent,
    AutobankRulesViewListComponent,
    AutobankRulesDetailViewComponent,
    AccountingRelationsComponent,
    MasterStructureNodeComponent,
    CostAccountingRelationListViewComponent,
    CostAccountingRelationDetailViewComponent,
    CostUnitMandatorsModalComponent,
    IncomingInvoiceComponent,
  ],
  entryComponents,
})
export class AccountingModule {
}
