import { EHttpRequest, RequestOptions, getHttpPrams, getHttpHeaders } from './EHttpRequest';

export class HttpDelete<T> extends EHttpRequest<T> {
  constructor(url: string, options: RequestOptions = {}) {
    const init = {
      ...options.init,
      params: getHttpPrams(options.init ? options.init.params : {}),
      headers: getHttpHeaders(options.init ? options.init.headers : {}),
    };
    super('DELETE', url, init );
    this.setAdditionalHeaders(options.init ? options.init.headers : {});
    this.initOptions(options);
  }
}
