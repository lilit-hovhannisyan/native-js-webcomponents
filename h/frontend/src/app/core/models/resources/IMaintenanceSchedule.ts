import { IResource } from './IResource';
import { IWithActive } from './IWithActive';

export type MaintenanceScheduleKey = number;

interface ISharedProps {
  message: string;
  startTime: string; // date time
  estimatedEndTime: string; // date-time
  actualEndTime: string; // date-time
}

export interface IMaintenanceScheduleBase extends ISharedProps, IResource<MaintenanceScheduleKey>, IWithActive { }

export interface IMaintenanceScheduleCreate extends ISharedProps { }

export interface IMaintenanceScheduleDTO extends IMaintenanceScheduleBase { }

export interface IMaintenanceSchedule extends IMaintenanceScheduleDTO { }
