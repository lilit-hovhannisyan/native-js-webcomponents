import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup } from '@angular/forms';
import { IMandatorAccount } from 'src/app/core/models/resources/IMandatorAccount';
import { IWording } from 'src/app/core/models/resources/IWording';
import { CostTypeRepositoryService } from 'src/app/core/services/repositories/cost-type-repository.service';
import { AccountCostTypeRepositoryService } from 'src/app/core/services/repositories/account-cost-type-repository.service';
import { getAccountCostTypeGridConfig } from 'src/app/core/constants/nv-grid-configs/account-cost-type.config';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'nv-cost-type-autocomplete',
  templateUrl: './cost-type-autocomplete.component.html',
  styleUrls: ['./cost-type-autocomplete.component.scss'],
})
export class CostTypeAutocompleteComponent implements OnInit {
  public wording = wording;
  public gridConfig = getAccountCostTypeGridConfig(
    false,
    {},
    () => false
  );

  @Input() public predefineIfOnlyField: boolean;
  @Input() public form: FormGroup;
  @Input() public controlName = 'costTypeId';
  @Input() public filters: object;
  @Input() public onlyOfBookingAccount: boolean;
  @Input() public bookingAccountControlName = 'bookingAccountId';
  @Input() public showLabel = true;
  @Input() public label: IWording;

  @Input() public setId = true;
  @Input() public displayOneProperty: boolean;
  @Input() public openListOnInit: boolean;
  @Input() public autofocus: boolean;

  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();

  public onlyMandatorFilter: { checkRole: (mandatorAccount: IMandatorAccount) => boolean };

  constructor(
    public accountCostTypeRepo: AccountCostTypeRepositoryService,
    public costTypeRepo: CostTypeRepositoryService,
  ) { }

  public ngOnInit() {
    forkJoin([
      this.accountCostTypeRepo.fetchAll({}),
      this.costTypeRepo.fetchAll({})
    ]).subscribe();
  }
}
