import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { END_OF_TERM_REASONS } from 'src/app/core/models/resources/IVesselAccounting';
import { wording } from 'src/app/core/constants/wording/wording';

export const endOfTermReasonsOptions: ISelectOption[] = [
  { value: END_OF_TERM_REASONS.solid, displayLabel: wording.chartering.endOfTermReasonsSolid },
  { value: END_OF_TERM_REASONS.poolExit, displayLabel: wording.chartering.endOfTermReasonsPoolExit },
  { value: END_OF_TERM_REASONS.changeOfOwner, displayLabel: wording.chartering.endOfTermReasonsChangeOfOwner },
  { value: END_OF_TERM_REASONS.contractTermination, displayLabel: wording.chartering.endOfTermReasonsContractTermination },
  { value: END_OF_TERM_REASONS.liquidation, displayLabel: wording.chartering.endOfTermReasonsLiquidation },
];
