import {
  MandatorBookingCodeMainService
} from 'src/app/core/services/mainServices/mandator-booking-code-main.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { IMandatorBookingCode } from '../../../../../../../../core/models/resources/IMandatorBookingCode';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { MandatorBookingCodesDetailViewComponent } from '../detail-view/detail-view.component';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { MandatorBookingCodeRepositoryService } from 'src/app/core/services/repositories/mandator-booking-code-repository.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/mandator-booking-codes.config';
import { Language } from 'src/app/core/models/language';
import { SettingsService } from 'src/app/core/services/settings.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';

@Component({
  selector: 'mandator-booking-codes-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class MandatorBookingCodesListViewComponent implements OnInit {

  public language: Language;
  public mandatorId: number;
  public bookingCodeGridConfig: NvGridConfig;
  public dataSource$: Observable<IMandatorBookingCode[]>;
  private modalRef?: NzModalRef;

  public config: NvGridConfig;

  constructor(
    public gridConfigService: GridConfigService,
    public route: ActivatedRoute,
    private modalService: NzModalService,
    private mandatorBookingCodeRepo: MandatorBookingCodeRepositoryService,
    private mainService: MandatorBookingCodeMainService,
    private settingsService: SettingsService,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService
  ) {
  }

  public ngOnInit(): void {
    this.mandatorId = parseInt(this.route.parent.snapshot.params.id, 10);
    this.language = this.settingsService.language;
    const params = { mandatorId: this.mandatorId };

    this.dataSource$ = this.mainService.getStream(params);


    const canPerform = (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation);
    const actions = {
      delete: this.delete,
      edit: () => { },
      add: () => this.openDetailViewModal(),
    };

    this.bookingCodeGridConfig = getGridConfig(actions, canPerform);
  }

  public openDetailViewModal(mandatorBookingCodeId?: number): void {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.accounting.bookingCodes[this.language],
      nzClosable: false,
      nzContent: MandatorBookingCodesDetailViewComponent,
      nzComponentParams: {
        id: mandatorBookingCodeId,
        mandatorId: this.mandatorId,
        closeModal: () => { this.modalRef.destroy(); }
      },
      nzFooter: null
    });
  }

  public delete = (mandatorBookingCode: IMandatorBookingCode) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: mandatorBookingCode.displayLabel },
    }).subscribe(confirmed => confirmed && this.mandatorBookingCodeRepo.delete(mandatorBookingCode.id, {}).subscribe());
  }
}
