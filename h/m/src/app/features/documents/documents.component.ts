import { AfterViewInit, Component, Injector, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

import { Subject } from 'rxjs';
import { debounceTime, finalize, skip, take, takeUntil } from 'rxjs/operators';

import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';
import { RightSidebarSupportService } from '@core-services/right-sidebar-support.service';
import { GlobalService } from '@core-services/global.service';
import { WindowRef } from '@core-providers/window-ref.provider';

import { DocumentStoreService } from '@features/documents/documents-store.service';
import { DocumentsWebService } from '@features/documents/documents.web-service';
import { BaseLoggerComponent } from '@features/tracking-system';

import { MeDocumentCardI } from '@shared/me-components/me-document-card/me-document-card.interface';
import { MeSearchBarI } from '@shared/me-components/me-search-bar/me-search-bar.interface';
import {
  MARKETPLACE_BASE_API_URL,
  ME_DOMAIN_APPLICATION_DOCUMENT,
  ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_GENERIC,
  ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL,
  ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL,
  FORCE_DOWNLOAD,
  ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL_VIDEO,
  ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_CERTIFICATE
} from '@shared/constants';
import { QuickSearchModel } from '@shared/models/search/quick-search.model';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeEmptyContentI } from '@shared/me-components/me-empty-content/me-empty-content-interface';
import { getFormatedDate, mapExtensionToIcon } from '@shared/utils';
import { MeTabGroupI } from '@shared/me-components/me-tab-group/me-tab-group.interface';
import { MeInfoCardI } from '@shared/me-components/me-info-card/me-info-card.interface';
import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';
import { DMChoiceAttributeModel } from '@shared/models/attribute/attribute.model';
import { PropertiesModel } from '@shared/models/attribute/attribute.properties.model';
import { MeSelectInputAttribute } from '@shared/models/attribute/me-select-input-attribute.model';
import { MeSelectInputComponent } from '@shared/me-components/me-attribute/me-select-input/me-select-input.component';
import { DomainModel } from '@shared/models/domain/domain.model';
import { EnumValueModel } from '@shared/models/enum/enum-value.model';
import { MeSearchDirectionTypes, MeSearchSortByTypes } from '@shared/me-search-sort-by-types';
import { MeIntroductionMessageI } from '@shared/me-components/me-introduction-message/me-introduction-message.interface';
import { MePopupVideoService } from '@shared/me-components/me-popup-video/me-popup-video.service';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss'],
  providers: [DocumentsWebService, DocumentStoreService]
})
export class DocumentsComponent extends BaseLoggerComponent implements OnInit, OnDestroy, AfterViewInit {
  componentVersion: string = '0.0.1';

  isVisuals: boolean = false;
  private VISUALS_URL: string = '/documents/visuals';

  documentCardItems: MeDocumentCardI[] = [];
  selectionEnabled: boolean = false;
  areDocumentsLoading: boolean = true;
  searchKeyword: string | undefined = '';
  emptyContent!: MeEmptyContentI;

  private _takeUntilDestroy$: Subject<void> = new Subject();

  documentDomain?: DomainModel;
  selectedDocumentType: string = ME_DOMAIN_APPLICATION_DOCUMENT;
  isSearch: boolean = false;
  trackForBottomReach: boolean = false;
  searchBarData!: MeSearchBarI;
  sortByValue: MeSearchSortByTypes = MeSearchSortByTypes.title;
  sortByDirection: MeSearchDirectionTypes = MeSearchDirectionTypes.ascending;
  filterDelay: number = 500;
  filterChanged: Subject<void> = new Subject();
  introMsgData!: MeIntroductionMessageI;

  private readonly NUMBER_OF_ITEMS_ON_PAGE = 50;
  private _selectedMap: Map<string, void> = new Map();

  i18nStrings: MeTranslationI = {};

  selectedDocument: MeDocumentModel | null = null;

  @ViewChild('rightSidebarContent', { read: TemplateRef }) rightSidebarContent!: TemplateRef<Component>;
  @ViewChild('create', { read: TemplateRef }) create!: TemplateRef<Component>;
  @ViewChild('info', { read: TemplateRef }) info!: TemplateRef<Component>;

  tabGroupData?: MeTabGroupI;
  @ViewChild('tabOne', { read: TemplateRef }) tabOne!: TemplateRef<Component>;

  // Tab one
  documentInfoCard: MeInfoCardI | null = null;

  constructor(
    injector: Injector,
    private _translate: MeTranslationService,
    private _documentsWebService: DocumentsWebService,
    private _documentStoreService: DocumentStoreService,
    private _definitionsStoreService: DefinitionsStoreService,
    private _router: Router,
    private _sweetAlertService: MeSweetAlertService,
    private _basicAlertService: MeBasicAlertService,
    private _breadcrumbSupportService: BreadcrumbSupportService,
    private _rightSidebarSupportService: RightSidebarSupportService,
    private _globalService: GlobalService,
    private _windowRef: WindowRef,
    private _videoPopupService: MePopupVideoService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isVisuals = this._router.url === this.VISUALS_URL;
    this._translate
      .getTranslations()
      .pipe(takeUntil(this._takeUntilDestroy$))
      .subscribe((translations) => {
        this.i18nStrings = translations;
      });

    this.searchBarData = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: '',
        minCharacters: 1
      },
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'azAlphabetically', value: this.i18nStrings.azAlphabetically },
          { key: 'zaAlphabetically', value: this.i18nStrings.zaAlphabetically },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      },
      operationButtons: [
        {
          button: { type: 'regular', rounded: false, color: 'link' },
          iFeatherName: 'trash',
          clickEventName: 'delete',
          tooltipText: this.i18nStrings.delete
        }
      ],
      selectButton: {
        enabled: false,
        placeholder: this.i18nStrings.select
      }
    };

    this._definitionsStoreService.dataLoaded$.pipe(takeUntil(this._takeUntilDestroy$)).subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.prepareFilterData();
        this.handleSearch(this.searchKeyword || '');
        this.prepareDocumentData();
      }
    });

    this.emptyContent = {
      title: this.i18nStrings.addYourFirstDocument,
      comment: this.i18nStrings.addYourFirstDocumentDescription,
      buttonText: this.i18nStrings.add,
      emptyIconName: 'empty-documents'
    };

    this.introMsgData = {
      content: this.isVisuals
        ? 'Visuals are uploaded image files and 3D assets that can be attached to one or more materials. Customers can preview and download visuals that are attached to materials.'
        : 'Documents are uploaded files (like PDFs and Word Documents). These might be catalogs, brochures, certificates, test reports, and more. Documents are attached to one or more materials or collections. Certificates are also displayed automatically on your company profile.',
      buttonText: this.i18nStrings.learnMore
    };

    // Listen for any sort/filter criteria
    this.filterChanged.pipe(debounceTime(this.filterDelay), takeUntil(this._takeUntilDestroy$)).subscribe(() => {
      this.handleSearch(this.searchKeyword || '');
    });
  }

  ngAfterViewInit(): void {
    this.tabGroupData = {
      tabs: [
        {
          index: 0,
          displayName: this.i18nStrings.overview,
          content: this.tabOne
        }
      ]
    };

    this._breadcrumbSupportService.breadcrumbActions = [
      {
        button: {
          id: '0',
          data: {
            color: 'primary',
            type: 'transparent'
          },
          text: this.i18nStrings.create,
          content: this.create
        },
        callBack: () => {
          this._router.navigate(['documents', 'create']);
        }
      },
      {
        button: {
          id: '1',
          data: {
            color: 'secondary',
            type: 'regular'
          },
          text: this.i18nStrings.info,
          content: this.info
        },
        callBack: () => {
          if (this.selectedDocument) {
            this.toggleInfoRightPanel();
          }
        }
      }
    ];
  }

  btnEventHandler(): void {
    this.isVisuals
      ? window.open('https://5883447.hs-sites.com/knowledge/visuals-suppliers', '_blank')
      : window.open('https://5883447.hs-sites.com/knowledge/documents-suppliers', '_blank');
  }

  private prepareFilterData(): void {
    this.selectedDocumentType = this.isVisuals ? ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL : ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL;
    this.documentDomain = this._definitionsStoreService.getDomain(this.selectedDocumentType);

    // Override document type dispaly value with 'All documents'
    const documentTypes = this.getAllDomainsEnum(this.documentDomain).filter(
      // THIS IS HARD CODED - remove Certificate document type from the list of Document types for search
      (item) => item.value !== ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_CERTIFICATE
    );
    const allDocumentsType = documentTypes.find((d) => d.value === this.documentDomain?.internalName);
    if (allDocumentsType) {
      allDocumentsType.displayName = this.isVisuals ? this.i18nStrings.allVisuals : this.i18nStrings.allDocuments;
    }

    const dmDocumentTypeFilterAttribute: DMChoiceAttributeModel = {
      constraints: [],
      dataTypeClass: 'moa',
      displayName: this.i18nStrings.documentType,
      internalName: 'documentType',
      properties: new PropertiesModel(),
      required: false,
      optRequired: false,
      value: this.selectedDocumentType,
      values: documentTypes,
      disabled: false,
      unique: false
    };

    const meDocumentTypeFilterAttribute: MeSelectInputAttribute = new MeSelectInputAttribute();
    meDocumentTypeFilterAttribute.data = dmDocumentTypeFilterAttribute;
    meDocumentTypeFilterAttribute.form = new FormGroup({
      [dmDocumentTypeFilterAttribute.internalName]: new FormControl(dmDocumentTypeFilterAttribute.value)
    });

    this.searchBarData = {
      ...this.searchBarData,
      singleValueFilter: new MeAttributeItem(MeSelectInputComponent, meDocumentTypeFilterAttribute)
    };

    meDocumentTypeFilterAttribute.form.controls.documentType.valueChanges
      .pipe(skip(1), takeUntil(this._takeUntilDestroy$))
      .subscribe((value) => {
        this.selectedDocumentType = value;
        this.filterChanged.next();
      });
  }

  private getAllDomainsEnum(documentDomain: DomainModel): EnumValueModel[] {
    const types: EnumValueModel[] = [];
    types.push({
      displayName: documentDomain.displayName,
      value: documentDomain.internalName
    });
    documentDomain.descendants?.forEach((desc) => types.push(...this.getAllDomainsEnum(desc)));
    return types;
  }

  handleAddButtonClick(): void {
    this._router.navigate(['documents', 'create']);
  }

  toggleInfoRightPanel(): void {
    if (this._rightSidebarSupportService.rightSidebarContent !== null) {
      this._rightSidebarSupportService.rightSidebarContent = null;
    } else {
      this._rightSidebarSupportService.rightSidebarContent = this.rightSidebarContent;
    }

    const infoButtonAction = this._breadcrumbSupportService.breadcrumbActions[1];
    const filteredActionsButtons = this._breadcrumbSupportService.breadcrumbActions.filter((a) => a !== infoButtonAction);
    if (infoButtonAction) {
      this._breadcrumbSupportService.breadcrumbActions = [
        ...filteredActionsButtons,
        {
          ...infoButtonAction,
          button: {
            ...infoButtonAction.button,
            data: {
              color: this._rightSidebarSupportService.rightSidebarContent === null ? 'secondary' : 'primary',
              type: this._rightSidebarSupportService.rightSidebarContent === null ? 'regular' : 'transparent'
            }
          }
        }
      ];
    }
  }

  trackByDocumentOid(_index: number, item: MeDocumentCardI): string {
    return item.oid;
  }

  prepareDocumentData(): void {
    this._documentStoreService.documentEntities$.pipe(takeUntil(this._takeUntilDestroy$)).subscribe((data: MeDocumentModel[]) => {
      if (data) {
        this.documentCardItems = data.map((document: MeDocumentModel) => {
          const docCard = {
            contextMenu: {
              contextMenuItems: [
                {
                  key: 'edit',
                  value: this.i18nStrings.edit
                },
                {
                  key: 'view',
                  value: this.i18nStrings.view
                },
                {
                  key: 'download',
                  value: this.i18nStrings.download
                },
                {
                  key: 'delete',
                  value: this.i18nStrings.delete
                }
              ]
            },
            creationDate: getFormatedDate(document.createdOn),
            documentName: document.attributes.title,
            oid: document.oid,
            selected: this._selectedMap.has(document.oid),
            selectionEnabled: this.selectionEnabled,
            documentThumbnail: document.attributes.thumbnail
              ? `${MARKETPLACE_BASE_API_URL}/files/download/${document.attributes.thumbnail}`
              : undefined,
            documentExtensionIcon: mapExtensionToIcon(document.attributesExtended.extension)
          };
          if (document.domainName === ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_GENERIC) {
            docCard.contextMenu.contextMenuItems.push({
              key: 'changeType',
              value: this.i18nStrings.changeType
            });
          }
          return docCard;
        });
      }
    });
  }

  handleItemClick(card: MeDocumentCardI, index: number): void {
    if (this.selectionEnabled) {
      this.toggleCardItemSelection(index);
    } else {
      this.handleDocumentSelection(card.oid);
    }
  }

  handleDocumentSelection(oid: string): void {
    this.selectedDocument = this._documentStoreService.documentEntities.find((doc: MeDocumentModel) => doc.oid === oid) || null;
    if (this.selectedDocument) {
      const docCard = {
        title: this.selectedDocument.attributes.title,
        subtitle: `${this.i18nStrings.createdBy} ${this.selectedDocument.attributesExtended.creator}`,
        description: getFormatedDate(this.selectedDocument.createdOn),
        contextMenu: {
          contextMenuItems: [
            {
              key: 'edit',
              value: this.i18nStrings.edit
            },
            {
              key: 'download',
              value: this.i18nStrings.download
            },
            {
              key: 'delete',
              value: this.i18nStrings.delete
            },
            {
              key: 'view',
              value: this.i18nStrings.view
            }
          ]
        },
        thumbnail: this.selectedDocument.attributes.thumbnail
          ? `${MARKETPLACE_BASE_API_URL}/files/download/${this.selectedDocument.attributes.thumbnail}`
          : undefined,
        icon: mapExtensionToIcon(this.selectedDocument.attributesExtended.extension)
      };
      if (this.selectedDocument.domainName === ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_GENERIC) {
        docCard.contextMenu.contextMenuItems.push({
          key: 'changeType',
          value: this.i18nStrings.changeType
        });
      }
      this.documentInfoCard = docCard;
    }
    if (this._rightSidebarSupportService.rightSidebarContent === null) {
      this.toggleInfoRightPanel();
    }
  }

  toggleCardItemSelection(index: number): void {
    if (this._selectedMap.has(this.documentCardItems[index].oid)) {
      this.documentCardItems[index] = { ...this.documentCardItems[index], selected: false };
      this._selectedMap.delete(this.documentCardItems[index].oid);
    } else {
      this.documentCardItems[index] = { ...this.documentCardItems[index], selected: true };
      this._selectedMap.set(this.documentCardItems[index].oid);
    }
  }

  customEvent(eventName: string, payload?: string, item?: MeDocumentCardI | MeDocumentModel): void {
    if (['infoCardContextMenuClick', 'contextMenuItemClick'].includes(eventName)) {
      switch (payload) {
        case 'edit':
          this._router.navigate(['documents', 'edit', item?.oid]);
          break;
        case 'download':
          const file = this._documentStoreService.documentEntities.find((d) => d.oid === item?.oid);
          if (file && file.attributes.primaryContent) {
            const FILE_URL: string = `${MARKETPLACE_BASE_API_URL}/files/download/${file.attributes.primaryContent}${FORCE_DOWNLOAD}`;
            this._windowRef.openInNewWindow(FILE_URL);
          } else {
            this._basicAlertService.openBasicAlert({
              mode: 'error',
              content: this.i18nStrings.noFile,
              title: this.i18nStrings.emptyDocument
            });
          }
          break;
        case 'view':
          const fileToView = this._documentStoreService.documentEntities.find((d) => d.oid === item?.oid);
          if (fileToView && fileToView.attributes.primaryContent) {
            const FILE_URL: string = `${MARKETPLACE_BASE_API_URL}/files/download/${fileToView.attributes.primaryContent}`;
            if (item?.oid.includes(ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL_VIDEO)) {
              this._videoPopupService.openVideoPopupAlert({ videoPath: FILE_URL });
            } else {
              this._windowRef.openInNewWindow(FILE_URL);
            }
          } else {
            this._basicAlertService.openBasicAlert({
              mode: 'error',
              content: this.i18nStrings.noFile,
              title: this.i18nStrings.emptyDocument
            });
          }
          break;
        case 'delete':
          console.log(item);
          if (item?.oid) {
            this.deleteDocument([item.oid]);
          }
          break;
        case 'changeType':
          this._router.navigate(['documents', 'edit', item?.oid], { queryParams: { isTypeChange: true } });
          break;
        default:
          break;
      }
    }
  }

  bottomReachedHandler(): void {
    if (this.trackForBottomReach) {
      this.handleSearch(this.searchKeyword || '', this._documentStoreService.documentNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
    }
  }

  handleSearch(searchValue: string, from: number = 0, to: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.searchKeyword = searchValue;
    this.searchBarData = {
      ...this.searchBarData,
      search: { placeholder: this.i18nStrings.searchFor, keyword: this.searchKeyword as string }
    };

    const searchCriteria: QuickSearchModel = {
      domain: this.selectedDocumentType,
      criteriaQuick: this.searchKeyword.trim() || undefined,
      orderByAttr: this.sortByValue.trim() || undefined,
      orderByDir: this.sortByDirection.trim() || undefined
    };

    this.areDocumentsLoading = true;
    this._globalService.activateLoader();
    this._documentsWebService
      .searchDocuments(searchCriteria, from, to)
      .pipe(
        takeUntil(this._takeUntilDestroy$),
        finalize(() => {
          this.areDocumentsLoading = false;
          this._globalService.deactivateLoader();
        })
      )
      .subscribe((response: ArrayResponseI<MeDocumentModel>) => {
        this._documentStoreService.documentNextId = response.nextID;
        if (append) {
          this._documentStoreService.appendDocumentEntities(response.entities);
        } else {
          this._documentStoreService.documentEntities = response.entities;
          this.isSearch = true;
          this.trackForBottomReach = true;
        }
        // If reached end of pagination unsubscribe from scroll
        if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
          this.trackForBottomReach = false;
        }
      });
  }

  handleSortByEvent(sortByValue: string): void {
    if (this.searchBarData.sort) {
      this.searchBarData = {
        ...this.searchBarData,
        sort: {
          ...this.searchBarData.sort,
          selectedIndex: this.searchBarData.sort.options.findIndex((x) => x.key === sortByValue) || 0
        }
      };
      switch (sortByValue) {
        case 'azAlphabetically':
          this.sortByValue = MeSearchSortByTypes.title;
          this.sortByDirection = MeSearchDirectionTypes.ascending;
          break;
        case 'zaAlphabetically':
          this.sortByValue = MeSearchSortByTypes.title;
          this.sortByDirection = MeSearchDirectionTypes.descending;
          break;
        case 'newest':
          this.sortByValue = MeSearchSortByTypes.created;
          this.sortByDirection = MeSearchDirectionTypes.descending;
          break;
        case 'oldest':
          this.sortByValue = MeSearchSortByTypes.created;
          this.sortByDirection = MeSearchDirectionTypes.ascending;
          break;
      }
      this.filterChanged.next();
    }
  }

  handleToggleSelectEvent(): void {
    this.selectionEnabled = !this.selectionEnabled;
    this.searchBarData = {
      ...this.searchBarData,
      selectButton: {
        ...this.searchBarData.selectButton,
        enabled: this.selectionEnabled,
        placeholder: this.selectionEnabled ? this.i18nStrings.cancel : this.i18nStrings.select
      }
    };
    this._selectedMap.clear();
    this.prepareDocumentData();
  }

  handleSearchBarEvents(event: { eventName: string; payload?: string | boolean }): void {
    switch (event.eventName) {
      case 'edit':
        // TODO
        break;
      case 'delete':
        const documentsOid: string[] = [...this._selectedMap.keys()];
        if (documentsOid.length > 0) {
          this.deleteDocument(documentsOid);
        }
        break;
      case 'copy':
        // TODO
        break;
      case 'more':
        // TODO
        break;
    }
  }

  private deleteDocument(oids: string[]): void {
    this._sweetAlertService
      .getDataBackFromSweetAlert()
      .pipe(take(1), takeUntil(this._takeUntilDestroy$))
      .subscribe((data: MeSweetAlertI) => {
        if (data.confirmed) {
          this._documentsWebService
            .deleteDocument(oids)
            .pipe(takeUntil(this._takeUntilDestroy$))
            .subscribe(() => {
              this._basicAlertService.openBasicAlert({
                mode: 'success',
                content: this.i18nStrings.documentDeleted,
                title: this.i18nStrings.success
              });
              this.handleSearch(this.searchKeyword || '');
            });
        }
      });

    this._sweetAlertService.openMeSweetAlert({
      icon: 'alert-octagon',
      mode: 'danger',
      message: this.i18nStrings.checkDocumentForDelete,
      title: this.i18nStrings.deleteDocument,
      type: {
        name: MeSweetAlertTypeEnum.submit,
        buttons: {
          submit: this.i18nStrings.delete,
          cancel: this.i18nStrings.cancel
        }
      }
    });
  }

  ngOnDestroy(): object {
    this._takeUntilDestroy$.next();
    this._takeUntilDestroy$.complete();
    return super.ngOnDestroy();
  }
}
