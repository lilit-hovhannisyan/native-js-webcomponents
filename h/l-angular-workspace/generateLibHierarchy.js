// filesystem
var fs = require('fs');

// run command
const { exec } = require("child_process");

const pathPrefix = 'projects/l-builder/src/lib/';
const lavashModules = [];
const lavashRoutes = [];
const modules = ['Atoms', 'Molecules', 'Organisms'];

generateLibStructure();

function generateLibStructure() {
    for (const m of modules) {
        const module = m.toLowerCase();
        const pathToModuleDir = pathPrefix + module + '/';
        const moduleDirName = module.toLowerCase();
        var pathToModule = pathToModuleDir + module + '.module.ts';

        const moduleData = fs.readFileSync(pathToModule).toString();

        const components = moduleData.substring(moduleData.lastIndexOf('[') + 1, moduleData.lastIndexOf(']')).replace(/\s+/g, '').split(',');
        const componentsMetadata = [];

        for (const componentName of components) {
            // get compoonent path from module file
            var componentPath = moduleData.substring(moduleData.indexOf(componentName));
            componentPath = componentPath.substring(0, componentPath.indexOf(';'));
            componentPath = componentPath.substring(componentPath.indexOf('\''));

            // get component selector from component file
            const _componentPathRelative = pathToModuleDir + componentPath.replace('./', '')
                .replace('\'', '').replace('\'', '') + '.ts';

            const componentData = fs.readFileSync(_componentPathRelative).toString();

            var selector = componentData.substring(componentData.indexOf('selector: '));
            selector = selector.substring(0, selector.indexOf('\',')).replace('selector: \'', '').replace(/\s+/g, '');// .;

            // call ng g component
            const componentRoutePath = generatePageInLavashApp(module, componentName);

            // metadata array
            componentsMetadata.push({componentName, componentPath, selector, componentRoutePath});
        }

        lavashModules.push({
            moduleName: 'Lavash' + m + 'Module',
            moduleDirName,
            components,
            componentsMetadata
        });
    }

    fs.writeFile('projects/lavash/l-builder.structure.ts',
        'export const libStructure =' + JSON.stringify(lavashModules) + ';' +
        'export const routes =' + JSON.stringify(lavashRoutes) ,
        (err) => {});

    console.log('successfully generated');
}

function executeCommand(command, callback, callbackParam) {
    exec(command, (error, stdout, stderr) => {
        if (error) {
            // console.log(`error: ${command}: ${error.message}`);
            return;
        }
        if (stderr) {
            // console.log(`stderr: ${command}: ${stderr}`);
            return;
        }
        // console.log(`stdout: ${command}: ${stdout}`);
        callback(callbackParam);
    });
}

function generatePageInLavashApp(moduleName, componentName) {
    // executeCommand('ng g c pages/hhh --spec=false --project lavash');

    // remove Component not to create --componentComponent
    const componentFileName = componentName.replace('Component', '');

    const lavashPagePath = 'pages/' + moduleName + '/' + componentFileName;

    const componentPathInLavash = 'pages/' + moduleName + '/' + componentFileName + '/' + componentFileName + '.component.';

    // if (!fs.existsSync(componentPathInLavash + 'html')) {
        const commandStr = 'ng g c ' + lavashPagePath + ' --spec=false --project lavash';

        executeCommand(commandStr, writeInLavashFile, componentPathInLavash);
    // }

    lavashRoutes.push({
        path: componentFileName.toLowerCase(),
        component: componentName
    });

    return componentFileName.toLowerCase();
}

function writeInLavashFile(path) {
   /* const pathToModuleDirectory = pathPrefix + module + '/';
    var pathToModule = pathToModuleDirectory + module + '.module.ts';

    fs.writeFile('projects/lavash/src/app/' + path + 'html',
        'hello',
        (err) => {
        //console.log('err', err)
    });*/
}


