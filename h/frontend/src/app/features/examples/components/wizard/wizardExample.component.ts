import { Component } from '@angular/core';

@Component({
  templateUrl: './wizardExample.component.html',
  styleUrls: ['./wizardExample.component.scss']
})
export class WizardExampleComponent {

  public stepsCompleted = (formData) => {
    console.log('COMPLETE: ', formData);
  }
}

