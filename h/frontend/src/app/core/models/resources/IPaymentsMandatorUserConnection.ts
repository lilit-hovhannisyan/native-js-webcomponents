import { IResource } from './IResource';
import { PaymentPermissionKey } from './IPaymentsPermission';
import { IPermissionGroupUserMandatorConnection } from './IPermissionGroup';

export type PaymentMandatorUserConnectionKey = number;

interface ISharedProps extends IPermissionGroupUserMandatorConnection {
  paymentPermissionId: PaymentPermissionKey;
}

export interface IPaymentMandatorUserConnectionBase extends ISharedProps, IResource<PaymentMandatorUserConnectionKey> { }

export interface IPaymentMandatorUserConnectionCreate extends ISharedProps { }

export interface IPaymentMandatorUserConnectionDTO extends IPaymentMandatorUserConnectionBase { }

export interface IPaymentMandatorUserConnection extends IPaymentMandatorUserConnectionDTO { }
