import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigationBoardComponent } from '../../shared/components/navigation-board/navigation-board.component';
import { sitemap } from '../../core/constants/sitemap/sitemap';
import { WizardExampleComponent } from './components/wizard/wizardExample.component';
import { TimelineExampleComponent } from './components/timeline/timeline.component';
import { LogsComponent } from './components/logs/logs.component';
import { AclComponent } from './components/acl/acl.component';
import { PageNotFoundComponent } from '../../shared/components/page-not-found/page-not-found.component';
import { FormWithFrameComponent } from './components/form-with-frame/form-with-frame.component';
import { DateboxComponent } from './components/datebox/datebox.component';
import { UserCompactComponent } from './components/user-compact/user-compact.component';
import { NumberInputFormatComponent } from './components/number-input-format/number-input-format.component';
import { UnsavedChangesGuard } from 'src/app/routing/services/unsaved-changes.guard';
import { ChatRoomComponent } from './components/chat-room/chat-room.component';


export const routes: Routes = [
  {
    path: '',
    component: NavigationBoardComponent,
    pathMatch: 'full'
  },
  {
    path: sitemap.examples.children.mixed.path,
    children: [
      {
        path: '',
        component: NavigationBoardComponent,
        pathMatch: 'full'
      },
      {
        path: sitemap.examples.children.mixed.children.wizard.path,
        component: WizardExampleComponent,
      },
      {
        path: sitemap.examples.children.mixed.children.timeline.path,
        component: TimelineExampleComponent,
      },
      {
        path: sitemap.examples.children.mixed.children.logs.path,
        component: LogsComponent
      },
      {
        path: sitemap.examples.children.mixed.children.acl.path,
        component: AclComponent
      },
      {
        path: sitemap.examples.children.mixed.children.formFrame.path,
        component: FormWithFrameComponent,
        canDeactivate: [UnsavedChangesGuard]
      },
      {
        path: sitemap.examples.children.mixed.children.datebox.path,
        component: DateboxComponent
      },
      {
        path: sitemap.examples.children.mixed.children.userCompact.path,
        component: UserCompactComponent
      },
      {
        path: sitemap.examples.children.mixed.children.numberInputFormat.path,
        component: NumberInputFormatComponent
      },
      // TODO: uncomment this when cors issue with signalR fixed in azure
      // {
      //   path: sitemap.examples.children.mixed.children.signalR.path,
      //   component: ChatRoomComponent
      // },
    ]
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  }
];

export const DECLARATIONS = [
  WizardExampleComponent,
  AclComponent,
  LogsComponent,
  TimelineExampleComponent,
  UserCompactComponent,
  NumberInputFormatComponent,
  ChatRoomComponent,
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamplesRoutingModule { }
