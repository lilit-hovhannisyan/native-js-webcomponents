import {
  Component,
  Input,
  ViewChild,
  ViewContainerRef,
  ComponentRef,
  ComponentFactoryResolver,
  OnDestroy,
  AfterViewInit,
  HostBinding,
  OnInit,
  ChangeDetectorRef,
  Injector
} from '@angular/core';
import { WindowRef } from '@core-providers/window-ref.provider';
import { fromEvent, Subscription } from 'rxjs';
import { MeAttributeComponentInterface } from './me-attribute-component.interface';
import { MeAttributeItem } from './me-attribute-item';
import { BaseLoggerComponent } from '@features/tracking-system/base-logger.component';

@Component({
  selector: 'me-attribute',
  template: `<ng-template #attributeTarget></ng-template>`,
  styleUrls: ['./me-attribute.component.scss']
})
export class MeAttributeComponent extends BaseLoggerComponent implements OnInit, OnDestroy, AfterViewInit {
  componentVersion: string = '0.0.1';
  @ViewChild('attributeTarget', { read: ViewContainerRef }) target!: ViewContainerRef;
  @Input() attWidth: string = '100%';
  @HostBinding('style.width') width = this.attWidth;
  @Input() attributeItem!: MeAttributeItem | undefined;
  cmpRef!: ComponentRef<MeAttributeComponentInterface>;
  resizeSubscription!: Subscription;

  constructor(
    injector: Injector,
    private componentFactoryResolver: ComponentFactoryResolver,
    private windowRef: WindowRef,
    private cdr: ChangeDetectorRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.adjustWidth();
    this.resizeSubscription = fromEvent(this.windowRef.nativeWindow, 'resize').subscribe(() => this.adjustWidth());
  }
  ngAfterViewInit(): void {
    if (this.cmpRef) {
      this.cmpRef.destroy();
    }
    if (this.attributeItem) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.attributeItem.component);
      this.cmpRef = this.target.createComponent(componentFactory);
      (this.cmpRef.instance as MeAttributeComponentInterface).data = this.attributeItem.data;
    }
    this.cdr.detectChanges();
  }

  adjustWidth(): void {
    if (!this.windowRef.isMobile()) {
      this.width = `calc(${this.attWidth} - 20px)`; // 20px is left + right padding
    } else {
      this.width = `calc(100% - 20px)`;
    }
  }

  ngOnDestroy(): object {
    if (this.cmpRef) {
      this.cmpRef.destroy();
    }
    this.resizeSubscription.unsubscribe();
    return super.ngOnDestroy();
  }
}
