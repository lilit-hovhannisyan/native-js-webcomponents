import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeSearchBarModule } from '@shared/me-components/me-search-bar/me-search-bar.module';
import { MeMaterialCardModule } from '@shared/me-components/me-material-card/me-material-card.module';
import { MeComponentLoadingModule } from '@shared/me-directives/me-component-loader/me-component-loader.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MeTabGroupModuel } from '@shared/me-components/me-tab-group/me-tab-group.module';
import { MeDocumentLineModule } from '@shared/me-components/me-document-line/me-document-line.module';
import { MeScrollToBottomModule } from '@shared/me-directives/me-scroll-to-bottom.module';
import { MeEmptyContentModule } from '@shared/me-components/me-empty-content/me-empty-content.module';
import { MeInfoCardModule } from '@shared/me-components/me-info-card/me-info-card.module';
import { MePopupVideoModule } from '@shared/me-components/me-popup-video/me-popup-video.module';

import { CollectionViewRoutingModule } from './collection-view-routing.module';
import { CollectionViewComponent } from './collection-view.component';

@NgModule({
  declarations: [CollectionViewComponent],
  imports: [
    CommonModule,
    CollectionViewRoutingModule,
    MeMaterialCardModule,
    MeSearchBarModule,
    MeComponentLoadingModule,
    MeEmptyContentModule,
    IconsModule,
    MeTabGroupModuel,
    MeInfoCardModule,
    MeDocumentLineModule,
    MeScrollToBottomModule,
    MePopupVideoModule
  ]
})
export class CollectionViewModule {}
