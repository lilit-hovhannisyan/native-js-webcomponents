import { IResource } from './IResource';
import { IUser } from './IUser';

interface ISharedProps {
  name: string;
  size: number;
  createdDate: string;
  userId: number;
  contentType: string;
  location: ILocation;
}

export interface IUploadCreate extends ISharedProps { }

export interface IUploadBase extends ISharedProps, IResource<number> { }

export interface IUploadDTO extends IUploadBase { }

export interface IUpload extends IUploadDTO { }

export interface ILocation {
  host: string;
  pathname: string;
  scheme: string;
  href: string;
}
