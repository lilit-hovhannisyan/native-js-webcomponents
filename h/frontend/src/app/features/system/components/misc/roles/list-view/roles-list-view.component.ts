import { RoleRepositoryService } from '../../../../../../core/services/repositories/role-repository.service';
import {
  ChangeDetectionStrategy,
  Component,
  OnInit
} from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { NzModalService } from 'ng-zorro-antd/modal';
import { IRole } from '../../../../../../core/models/resources/IRole';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { getGridConfig } from '../../../../../../core/constants/nv-grid-configs/roles.config';
import { Observable } from 'rxjs';
import { Language } from 'src/app/core/models/language';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  templateUrl: './roles-list-view.component.html',
  styleUrls: ['./roles-list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RolesListViewComponent implements OnInit {

  public rolessGridConfig: NvGridConfig;
  public dataSource$: Observable<IRole[]>;

  constructor(
    private roleRepo: RoleRepositoryService,
    private modalService: NzModalService,
    public gridConfigService: GridConfigService,
    private authService: AuthenticationService,
    private router: Router,
    private confirmationService: ConfirmationService,
    private settingsService: SettingsService
  ) { }

  public ngOnInit() {

    this.roleRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.roleRepo.getStream();

    const actions = {
      edit: (id) => this.router.navigate([`${this.router.url}/${id}`]),
      delete: this.deleteRoleClicked,
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    this.rolessGridConfig = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  public deleteRoleClicked = (role: IRole) => {
    if (role.isSystemDefault && !this.authService.getUser().isSuperAdmin) {
      this.modalService.warning({
        nzTitle: wording.general.systemTypesCanNotBeDeleted[this.settingsService.language],
      });
      return;
    }

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: role.displayLabel },
    }).subscribe(confirmed => confirmed && this.roleRepo.delete(role.id, {}).subscribe());
  }
}
