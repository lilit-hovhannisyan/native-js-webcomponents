import { NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IMasterStructureNodeTemplate } from 'src/app/core/models/resources/IMasterStructureNode';

export const getSwitchingNodesGridConfig = (actions: {
  triggerOk: (row?: IMasterStructureNodeTemplate) => void,
}): NvGridConfig => {
  return ({
    gridName: 'switchingNodesGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 10
    },
    columns: [
      {
        key: 'labelEn',
        title: wording.accounting.switchingNodeNameEn,
        width: 400,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'labelDe',
        title: wording.accounting.switchingNodeNameDe,
        width: 400,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'save',
        description: wording.general.save,
        name: 'saveSwitchingNode',
        tooltip: wording.general.save,
        actOnEnter: true,
        actOnDoubleClick: true,
        func: (row) => actions.triggerOk(row),
      },
    ]
  });
};
