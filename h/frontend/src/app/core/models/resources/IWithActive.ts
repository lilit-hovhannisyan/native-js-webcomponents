export interface IWithActive {
  isActive: boolean;
}
