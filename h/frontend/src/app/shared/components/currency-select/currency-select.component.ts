import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/currencies.config';
import { Observable } from 'rxjs';
import { ICurrency, CurrencyKey } from 'src/app/core/models/resources/ICurrency';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { makeProvider, AbstractValueAccessor } from 'src/app/core/abstracts/value-accessor';
import { map, tap } from 'rxjs/operators';
import { SettingsService } from 'src/app/core/services/settings.service';
@Component({
  selector: 'nv-currency-select',
  templateUrl: './currency-select.component.html',
  styleUrls: ['./currency-select.component.scss'],
  providers: [makeProvider(CurrencySelectComponent)],
})
export class CurrencySelectComponent extends AbstractValueAccessor implements OnInit {

  public getGridConfig = getGridConfig;
  public resourceFormControl = new FormControl({ value: null });

  public currencies$: Observable<ICurrency[]>;
  private currencies: ICurrency[] = [];
  @Input() public queryParams = {};
  @Input() public defaultCurrencyId: CurrencyKey;

  @Output() public change = new EventEmitter<ICurrency>(null);

  public getLabel = (currency: ICurrency) =>
    `${currency.isoCode} / ${currency.displayLabel[this.settingsService.language]}`

  constructor(
    private currenciesService: CurrencyRepositoryService,
    private settingsService: SettingsService,
  ) {
    super();
  }

  public ngOnInit(): void {
    this.currencies$ = this.currenciesService.fetchAll(this.queryParams)
      .pipe(
        tap(currencies => this.currencies = currencies),
        map(currencies => currencies
          .filter(c => c.isActive || c.id === this.defaultCurrencyId)));
  }

  public onSelect(value: number): void {
    this.onCurrencySelect(value);
    this.onChange(value);
  }

  public writeValue(value: number): void {
    this.onCurrencySelect(value);
    this.resourceFormControl.setValue(value);
  }

  public setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.resourceFormControl.disable();
    } else {
      this.resourceFormControl.enable();
    }
  }

  private onCurrencySelect(currencyId: CurrencyKey) {
    const currency = this.currencies.find(c => c.id === currencyId);
    this.change.emit(currency);
  }

}
