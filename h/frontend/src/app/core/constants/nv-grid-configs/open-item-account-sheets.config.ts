import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { NvCellAlignment, NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl, NvRowHeight, NvExcelExportInfo } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { formatNumberWrapper } from 'src/app/shared/helpers/general';
import { NvLocale } from '../../models/dateFormat';
import { IBookingLine } from 'src/app/core/models/resources/IBookingLine';
import { IOpenItemAccountSheetsFull } from '../../models/resources/IOpenItemAccountSheets';

export const getMainGridConfig = (
  actions: IGridConfigActions,
  canPerform: (operation: number) => boolean,
  customFormat: {
    debitCredit: (debitCredit: number) => string
  },
  onExcelExport: (exportExcelInfo: NvExcelExportInfo) => void
): NvGridConfig => {
  return ({
    gridName: 'openItemAccountSheetsGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    showExcelButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    editForm: {
      allowCreateNewRow: false
    },
    onExcelExport: onExcelExport,
    columns: [
      {
        key: 'id',
        visible: false
      },
      {
        key: 'voucherNumber',
        title: wording.accounting.voucherNo,
        width: 120,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'voucherDate',
        title: wording.accounting.voucherDate,
        dataType: NvColumnDataType.Date,
        width: 120,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Date,
          values: []
        }
      },
      {
        key: 'currencyIsoCode',
        title: wording.accounting.currency,
        width: 85,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Select,
          multiSelect: false,
          values: []
        }
      },
      {
        key: '+/-',
        title: wording.accounting.DC,
        width: 50,
        isSortable: true,
        customFormatFn: (openItem: IOpenItemAccountSheetsFull) => customFormat.debitCredit(openItem.debitCreditType),
        filter: {
          values: []
        },
      },
      {
        key: 'amountTransaction',
        title: wording.accounting.amountTransaction,
        dataType: NvColumnDataType.Decimal,
        width: 93,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.RangeNumber,
          values: []
        },
      },
      {
        key: 'exchangeRate',
        title: wording.accounting.rate,
        width: 60,
        isSortable: true,
        dataType: NvColumnDataType.Decimal,
        decimalDigitsInfo: '1.5-5',
        filter: {
          values: []
        },
      },
      {
        key: 'amountLocal',
        title: wording.accounting.amountLocal,
        width: 93,
        isSortable: true,
        dataType: NvColumnDataType.Decimal,
        filter: {
          controlType: NvFilterControl.RangeNumber,
          values: []
        },
      },
      {
        key: 'text',
        title: wording.accounting.bookingText,
        width: 80,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'period',
        title: wording.accounting.period,
        dataType: NvColumnDataType.Number,
        width: 75,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'invoiceDate',
        title: wording.accounting.invoiceDate,
        dataType: NvColumnDataType.Date,
        width: 120,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Date,
          values: []
        },
      },
      {
        key: 'invoiceNo',
        title: wording.accounting.invoiceNo,
        width: 90,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'paymentDate',
        title: wording.accounting.paymentDate,
        dataType: NvColumnDataType.Date,
        width: 120,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Date,
          values: []
        },
      },
      {
        key: 'oppositeAccountNo',
        title: wording.accounting.oppositeAccount,
        width: 135,
        isSortable: true,
        filter: {
          values: []
        },
        customTooltip: (openItem: IOpenItemAccountSheetsFull) => openItem.accountName,
      },
      {
        key: 'userName',
        title: wording.accounting.bookedBy,
        width: 90,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Select,
          values: []
        },
      },
      {
        key: 'bookingDate',
        title: wording.accounting.bookingDate,
        dataType: NvColumnDataType.DateTime,
        width: 120,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Date,
          values: []
        },
      },
      {
        key: 'remarks',
        title: wording.accounting.remarks,
        width: 80,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'voyage',
        title: wording.accounting.voyage,
        width: 70,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'port',
        title: wording.accounting.port,
        width: 50,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'loc',
        title: wording.accounting.loc,
        width: 50,
        isSortable: true,
        filter: {
          values: []
        },
      },
    ],
    contextMenuButtons: [
      {
        icon: 'eye',
        description: wording.accounting.showBooking,
        name: 'showBooking',
        tooltip: wording.accounting.showBooking,
        actOnDoubleClick: false,
        func: (bookingLine: IBookingLine) => actions.show(bookingLine)
      }
    ]
  });
};


export const getSideGridConfig = (locale: NvLocale): NvGridConfig => ({
  gridName: 'openItemAccountSheetsCalculatedData',
  hideAllFilters: true,
  showRowIndex: false,
  showPaging: false,
  showFooter: false,
  hideToolbar: true,
  pinFirstRow: false,
  hideRefreshButton: true,
  rowHeight: NvRowHeight.midSize,
  rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
  columns: [
    {
      key: 'id',
      visible: false
    },
    {
      key: 'currencyId',
      visible: false,
    },
    {
      key: 'currencyIsoCode',
      title: wording.accounting.currency,
      width: 90,
      isSortable: true,
    },
    {
      key: 'debit',
      title: wording.accounting.debit,
      dataType: NvColumnDataType.Decimal,
      width: 80,
      isSortable: true,
      alignment: NvCellAlignment.Right,
    },
    {
      key: 'credit',
      title: wording.accounting.credit,
      dataType: NvColumnDataType.Decimal,
      width: 80,
      isSortable: true,
      alignment: NvCellAlignment.Right,
    },
    {
      key: 'balanceDebit',
      title: wording.accounting.balanceDebit,
      dataType: NvColumnDataType.Decimal,
      width: 120,
      isSortable: true,
      customFormatFn: row => {
        const diff: number = (row.debit - row.credit) || 0;
        return diff >= 0 ? formatNumberWrapper(diff, 1, 2, 2, locale) : null;
      },
      alignment: NvCellAlignment.Right,
    },
    {
      key: 'balanceCredit',
      title: wording.accounting.balanceCredit,
      dataType: NvColumnDataType.Decimal,
      width: 120,
      isSortable: true,
      customFormatFn: row => row.credit > row.debit
        ? formatNumberWrapper(row.credit - row.debit, 1, 2, 2, locale)
        : null,
      alignment: NvCellAlignment.Right,
    }]
});

