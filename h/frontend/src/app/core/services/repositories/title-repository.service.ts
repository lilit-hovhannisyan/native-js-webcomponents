import { TitleKey } from '../../models/resources/ITitle';
import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { ITitle, ITitleDTO, ITitleCreate } from '../../models/resources/ITitle';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class TitleRepositoryService extends AbstractRepository<
  ITitle,
  ITitleDTO,
  ITitleCreate,
  RepoParams<ITitle>,
  TitleKey
> {
  protected resourceType = 'Title';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/titles';
  }

  protected postProcess(item: ITitleDTO): ITitle { return item; }
}
