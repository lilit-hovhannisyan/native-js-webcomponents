import { Injectable } from '@angular/core';
import { combineLatest, forkJoin, Observable, Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { IBookingCode } from 'src/app/core/models/resources/IBookingCode';
import {
  charteringExpenseTypeOptions,
  ICharteringExpense,
  ICharteringExpenseFull,
  ICharteringExpenseRow,
} from 'src/app/core/models/resources/ICharteringExpense';
import { IWording } from 'src/app/core/models/resources/IWording';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { ExpensesRepositoryService } from 'src/app/core/services/repositories/expenses-repository.service';

type EntitiesList<T> = [
  T,
  IBookingAccount[],
  IBookingCode[],
];

@Injectable({
  providedIn: 'root',
})
export class ExpensesMainService {

  constructor(
    private expensesRepo: ExpensesRepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    private bookingCodeRepo: BookingCodeRepositoryService,

  ) { }

  public fetchAll(params: RepoParams<ICharteringExpense>): Observable<ICharteringExpenseFull[]> {
    return combineLatest([
      this.expensesRepo.fetchAll(params),
      this.bookingAccountRepo.fetchAll({}),
      this.bookingCodeRepo.fetchAll({}),
    ]).pipe(
      map((entities: EntitiesList<ICharteringExpense[]>) => this.toFullList(...entities))
    );
  }

  public getStream(params: RepoParams<ICharteringExpense>): Observable<ICharteringExpenseFull[]> {
    return combineLatest([
      this.expensesRepo.fetchAll(params),
      this.bookingAccountRepo.fetchAll({}),
      this.bookingCodeRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.expensesRepo.getStream(),
        this.bookingAccountRepo.getStream({}),
        this.bookingCodeRepo.getStream({}),
      ])),
      map((entities: EntitiesList<ICharteringExpense[]>) => this.toFullList(...entities))
    );
  }

  public toFullList(
    expenses: ICharteringExpense[],
    bookingAccounts: IBookingAccount[],
    bookingCodes: IBookingCode[]
  ): ICharteringExpenseFull[] {
    return expenses.map(expense => ({
      ...expense,
      bookingAccount: bookingAccounts.find(ba => ba.id === expense.bookingAccountId),
      bookingCode: bookingCodes.find(bc => bc.id === expense.bookingCodeId),
    }));
  }

  public toRow(expense: ICharteringExpenseFull): ICharteringExpenseRow {
    const { bookingAccount, bookingCode, type } = expense;
    const typeOption = charteringExpenseTypeOptions.find(option => option.value === type);

    return {
      ...expense,
      typeLabel: typeOption && typeOption.displayLabel,
      accountLabel: bookingAccount && this.addPrefixToWording(bookingAccount.no, bookingAccount.displayLabel),
      bookingCodeLabel: bookingCode && this.addPrefixToWording(bookingCode.no, bookingCode.displayLabel),
    };
  }

  public toRows(expenses: ICharteringExpenseFull[]): ICharteringExpenseRow[] {
    return expenses.map(expense => this.toRow(expense));
  }

  private addPrefixToWording(prefix: number, wording: IWording): IWording {
    return {
      en: `${prefix} ${ wording ? wording.en : '' }`,
      de: `${prefix} ${ wording ? wording.de : '' }`,
    };
  }
}
