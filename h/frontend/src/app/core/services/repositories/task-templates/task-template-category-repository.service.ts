import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { ITaskTemplateCategory, ITaskTemplateCategoryCreate, ITaskTemplateCategoryDTO, TaskTemplateCategoryKey } from 'src/app/core/models/resources/Task-Templates/ITaskTemplateCategory';
import { RepositoryService } from '../../repository.service';

@Injectable({
  providedIn: 'root'
})
export class TaskTemplateCategoryRepositoryService extends AbstractRepository<
ITaskTemplateCategory,
ITaskTemplateCategoryDTO,
ITaskTemplateCategoryCreate,
RepoParams<ITaskTemplateCategory>,
TaskTemplateCategoryKey
> {
  protected resourceType = 'TaskTemplateCategory';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: ITaskTemplateCategoryDTO): ITaskTemplateCategory => item;

  public getBaseUrl = () => '/api/task-template-categories';
}
