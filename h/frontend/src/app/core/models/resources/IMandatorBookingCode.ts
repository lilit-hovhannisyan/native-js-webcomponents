import { IResource } from './IResource';
import { IBookingCode } from './IBookingCode';
import { IWithLabel } from './IWithLabel';

export type MandatorBookingCodeKey = number;

interface ISharedProps {
  mandatorId: number;
  bookingCodeId: number;
}

export interface IMandatorBookingCodeBase extends ISharedProps, IResource<MandatorBookingCodeKey> { }

export interface IMandatorBookingCodeCreate extends ISharedProps { }

export interface IMandatorBookingCodeDTO extends IMandatorBookingCodeBase { }

export interface IMandatorBookingCode extends IMandatorBookingCodeDTO, IWithLabel { }

export interface IMandatorBookingCodeFull extends IMandatorBookingCode, IBookingCode { }

export interface IMandatorBookingCodeRow extends IMandatorBookingCodeFull { }
