import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeIconsProviderModule } from '@shared/modules/me-icons.module';

import { MeEllipsisRowModule } from '../me-ellipsis-row/me-ellipsis-row.module';
import { MeContextMenuModule } from '../me-context-menu/me-context-menu.module';
import { MeLabelModule } from '../me-label/me-label.module';

import { MeInfoCardComponent } from './me-info-card.component';

@NgModule({
  imports: [CommonModule, MeEllipsisRowModule, MeContextMenuModule, MeLabelModule, MeIconsProviderModule],
  declarations: [MeInfoCardComponent],
  exports: [MeInfoCardComponent]
})
export class MeInfoCardModule {}
