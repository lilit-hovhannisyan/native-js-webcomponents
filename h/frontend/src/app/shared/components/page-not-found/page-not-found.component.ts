import { Component } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent {
  public wording = wording;
 }
