import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountSettingsComponent } from './components/account-settings/account-settings.component';
import { NavigationBoardComponent } from '../../shared/components/navigation-board/navigation-board.component';
import { sitemap } from '../../core/constants/sitemap/sitemap';
import { PageNotFoundComponent } from '../../shared/components/page-not-found/page-not-found.component';


export const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    pathMatch: 'full'
  },
  {
    path: sitemap.main.children.settings.path,
    children: [
      {
        path: '',
        component: NavigationBoardComponent,
        pathMatch: 'full'
      },
      {
        path: sitemap.main.children.settings.children.accountSettings.path,
        component: AccountSettingsComponent,
      },
    ]
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  }
];

export const DECLARATIONS = [
  AccountSettingsComponent,
  DashboardComponent
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
