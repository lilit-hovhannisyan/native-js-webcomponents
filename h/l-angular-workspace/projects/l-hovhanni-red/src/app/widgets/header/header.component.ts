import {Component, OnInit} from '@angular/core';
import {SidebarService} from 'l-builder';
import {ProjectsService} from '../../services/projects.service';
import {Subscription} from 'rxjs';

@Component({
  selector : 'app-header-component',
  templateUrl : './header.component.html',
  styleUrls : ['./header.component.less']
})

export class HeaderComponent implements OnInit {
  //_currentProject = Subscription.EMPTY;
  currentProject;
  sidebarVisible;
  constructor(private sidebarService: SidebarService,
              private projectsService: ProjectsService) {}

  ngOnInit() {
      this.projectsService.getCurrentProject();

      this.projectsService.currentProject.subscribe( project => {
          this.currentProject = project;
      });

      this.sidebarService.sidebarState.subscribe( observer => {
        this.sidebarVisible = observer;
      });
  }
}
