import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'companies',
    pathMatch: 'full'
  },
  {
    path: 'users-groups',
    data: {
      breadcrumb: 'users/groups'
    },
    loadChildren: () => import('./users-groups/users-groups.module').then((m) => m.UsersGroupsModule)
  },
  {
    path: 'user/create',
    loadChildren: () => import('./user-create-edit/user-create-edit.module').then((m) => m.UserCreateEditModule)
  },
  {
    path: 'user/edit/:id',
    data: {
      breadcrumb: 'Edit User'
    },
    loadChildren: () => import('./user-create-edit/user-create-edit.module').then((m) => m.UserCreateEditModule)
  },
  {
    path: 'group/create',
    loadChildren: () => import('./group-create-edit/group-create-edit.module').then((m) => m.GroupCreateEditModule)
  },
  {
    path: 'group/edit/:id',
    data: {
      breadcrumb: 'Edit Group'
    },
    loadChildren: () => import('./group-create-edit/group-create-edit.module').then((m) => m.GroupCreateEditModule)
  },
  {
    path: 'companies',
    loadChildren: () => import('./companies/companies.module').then((m) => m.CompaniesModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackofficeRoutingModule {}
