import { IWording } from 'src/app/core/models/resources/IWording';

export interface ISelectOption<T = string | number> {
  displayLabel: IWording;
  value: T;
  default?: boolean;
  disabled?: boolean;
}
