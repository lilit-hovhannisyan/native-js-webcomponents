import { NvGridButtonsPosition, NvGridConfig, NvFilterControl, NvGridRowSelectionType, NvColumnDataType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IVesselWithAccountings } from '../../models/resources/IVessel';

export const getRoleVesselsGridConfig = (): NvGridConfig => {
  return ({
    gridName: 'roleVesselsGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        customFormatFn: ((
          row: IVesselWithAccountings,
        ) => row.lastVesselAccounting?.vesselNo),
        key: 'lastVesselAccounting',
        title: wording.general.number,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        customFormatFn: ((
          row: IVesselWithAccountings,
        ) => row.lastVesselAccounting?.vesselName),
        key: 'lastVesselAccounting',
        title: wording.general.name,
        width: 300,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'imo',
        title: wording.chartering.imoNumber,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        customFormatFn: ((
          row: IVesselWithAccountings,
        ) => row.lastVesselChartername?.charterName),
        key: 'lastVesselChartername',
        title: wording.technicalManagement.charterName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'deliveryDate',
        title: wording.chartering.deliveredFromShipyard,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.Date,
        filter: {
          values: []
        }
      },
    ]
  });
};
