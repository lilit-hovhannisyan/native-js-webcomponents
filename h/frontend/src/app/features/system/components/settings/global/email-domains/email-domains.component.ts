import { Component, OnInit, HostBinding, Input } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { Observable, of } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { getEmailDomainsConfig } from 'src/app/core/constants/nv-grid-configs/global-settings-email-domains.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { EmailDomainModalComponent } from 'src/app/shared/components/email-domain-detail-view-modal/email-domain-modal.component';
import { SettingsService } from 'src/app/core/services/settings.service';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { IEmailDomain } from 'src/app/core/models/resources/ISettings';
import { AbstractValueAccessor, makeProvider } from 'src/app/core/abstracts/value-accessor';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { IUser } from 'src/app/core/models/resources/IUser';

export interface IEmailDomainWithConnections extends IEmailDomain {
  hasActiveUser: boolean;
  hasInactiveUser: boolean;
}

@Component({
  selector: 'nv-email-domains',
  templateUrl: './email-domains.component.html',
  styleUrls: ['./email-domains.component.scss'],
  providers: [makeProvider(EmailDomainsComponent)],
})
export class EmailDomainsComponent
extends AbstractValueAccessor<IEmailDomain[]>
implements OnInit {
  public wording = wording;
  public dataSource$: Observable<IEmailDomainWithConnections[]> = of([]);
  public gridConfig: NvGridConfig;
  public allUsers: IUser[] = [];
  public isSuperAdmin = false;

  public actions = {
    edit: (entity: IEmailDomainWithConnections) => this.openDetailView(entity),
    delete: (entity: IEmailDomainWithConnections) => this.openDeleteDomainModal(entity),
    add: () => this.openDetailView(),
  };

  constructor(
    public gridConfigService: GridConfigService,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private modalService: NzModalService,
    private settingsService: SettingsService,
    private usersService: UserRepositoryService,
  ) {
    super();
  }

  public writeValue(value?: IEmailDomain[]) {
    const mappedValue = value && value.map(item => this.domainUsedInfo(item));
    this.dataSource$ = of(mappedValue || []);
    super.writeValue(mappedValue || []);
  }

  public setDisabledState(isDisabled: boolean) {
    super.setDisabledState(isDisabled);

    /**
     * Because of some implicit reasons in some cases the nv-grid
     * can't disable the grid actions without setTimeout.
     * Case example: when clicking on the cancel button.
     */
    setTimeout(() => {
      this.setGridConfig();
    });
  }

  public ngOnInit() {
    this.isSuperAdmin = this.authService.getSessionInfo().user.isSuperAdmin;

    this.setGridConfig();
    this.usersService.fetchAll({}).subscribe(users => {
      this.allUsers = users;
      this.writeValue(this.value);
    });
  }

  private setGridConfig(): void {
    this.gridConfig = getEmailDomainsConfig(
      this.actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
      this.disabled,
    );
  }

  private openDetailView (entity?: IEmailDomainWithConnections): void {
    const { domain, hasActiveUser, hasInactiveUser, isSystemDefault } = entity || {} as IEmailDomainWithConnections;

    if (isSystemDefault && !this.isSuperAdmin) {
      this.showSystemDefaultEditWarning();
      return;
    }
    if (hasActiveUser) {
      this.showNotAllowedToEditWarning();
      return;
    }

    const editionWarningModalObs = !hasInactiveUser
      ? of(true)
      : this.confirmationService.warning({
        wording: wording.system.domainHasConnectedInactiveUser,
        okButtonWording: wording.general.edit,
        cancelButtonWording: wording.general.cancel,
      });

    editionWarningModalObs.subscribe(confirmed => {
      if (confirmed) {
        this.openCreateEditModal(entity, domain);
      }
    });
  }

  private openCreateEditModal(entity: IEmailDomain, domain: string): void {
    const editWord = wording.general.edit[this.settingsService.language];
    const addWord = wording.general.add[this.settingsService.language];
    const title = entity ? `${editWord} ${domain}` : addWord;

    this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: title,
      nzClosable: true,
      nzContent: EmailDomainModalComponent,
      nzComponentParams: {
        emailDomain: entity,
        allDomains: this.value,
        isSuperAdmin: this.isSuperAdmin,
      },
      nzWidth: 450,
    }).afterClose.subscribe((result?: IEmailDomain) => {
      if (!result) {
        return;
      }

      if (entity) {
        const index = this.value.findIndex(item => item === entity);
        if (index < 0) {
          return;
        }

        this.value.splice(index, 1, result);
      } else {
        this.value.push(result);
      }
      this.writeValue(this.value);
      this.onChange(this.value);
    });
  }

  private openDeleteDomainModal (entity: IEmailDomainWithConnections): void {
    const { hasInactiveUser, hasActiveUser, isSystemDefault } = entity;
    if (isSystemDefault && !this.isSuperAdmin) {
      this.showSystemDefaultDeleteWarning();
      return;
    }
    if (hasActiveUser) {
      this.showNotAllowedToDeleteWarning();
      return;
    }

    if (hasInactiveUser) {
      this.confirmationService.warning({
        wording: wording.system.domainHasConnectedInactiveUser,
        okButtonWording: wording.general.delete,
        cancelButtonWording: wording.general.cancel,
      }).subscribe(confirmed => confirmed && this.deleteDomain(entity));
      return;
    }

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: entity.domain },
    }).subscribe(confirmed => confirmed && this.deleteDomain(entity));
  }

  private deleteDomain(entity: IEmailDomain): void {
    const index = this.value.findIndex(item => item === entity);
    if (index < 0) {
      return;
    }

    this.value.splice(index, 1);
    this.writeValue(this.value);
    this.onChange(this.value);
  }

  private domainUsedInfo(emailDomain: IEmailDomain): IEmailDomainWithConnections {
    let hasActiveUser = false;
    let hasInactiveUser = false;

    for (const user of this.allUsers) {
      if (hasActiveUser && hasInactiveUser) {
        break;
      }
      if (!user.hasEmail) {
        continue;
      }

      const emailParts = user.email.split('@');
      if (emailParts[1] !== emailDomain.domain) {
        continue;
      }

      hasActiveUser = hasActiveUser || user.isActive;
      hasInactiveUser = hasInactiveUser || !user.isActive;
    }

    return { ...emailDomain, hasActiveUser, hasInactiveUser };
  }

  private showSystemDefaultEditWarning(): void {
    this.confirmationService.warning({
      title: this.wording.accounting.canNotEditSystemDefaultTypes,
      okButtonWording: this.wording.general.ok,
      cancelButtonWording: null,
    });
  }

  private showNotAllowedToEditWarning(): void {
    this.confirmationService.warning({
      title: this.wording.system.notAllowedToEditDomain,
      okButtonWording: this.wording.general.ok,
      cancelButtonWording: null,
    });
  }
  private showNotAllowedToDeleteWarning(): void {
    this.confirmationService.warning({
      title: this.wording.system.notAllowedToDeleteDomain,
      okButtonWording: this.wording.general.ok,
      cancelButtonWording: null,
    });
  }

  private showSystemDefaultDeleteWarning(): void {
    this.confirmationService.warning({
      title: this.wording.general.systemTypesCanNotBeDeleted,
      okButtonWording: this.wording.general.ok,
      cancelButtonWording: null,
    });
  }
}
