import { NvColumnDataType, NvGridButtonsPosition, NvGridConfig, NvRowHeight } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { IVoyageRow } from '../../models/resources/IVoyage';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'voyageListView',
    showRowIndex: true,
    showPaging: true,
    showFooter: true,
    rowHeight: NvRowHeight.midSize,
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    sortBy: 'yearAndNo',
    isSortAscending: false,
    clearFiltersButton: true,
    columns: [
      {
        key: 'yearAndNo',
        title: wording.chartering.yearAndNo,
        isSortable: true,
        width: 100,
        filter: {
          values: []
        }
      },
      {
        key: 'from',
        title: wording.general.from,
        width: 210,
        isSortable: true,
        dataType: NvColumnDataType.DateTime,
        filter: {
          values: []
        }
      },
      {
        key: 'until',
        title: wording.general.until,
        isSortable: true,
        dataType: NvColumnDataType.DateTime,
        width: 210,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editBookingAccount',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: canPerform ? !canPerform(Operations.UPDATE) : true,
        func: (row: IVoyageRow) => actions.edit(row),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteBookingAccount',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: canPerform ? !canPerform(Operations.DELETE) : true,
        func: actions.delete,
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.add,
        tooltip: wording.general.add,
        icon: 'plus-circle-o',
        func: () => actions.add(),
        hidden: () => canPerform ? !canPerform(Operations.CREATE) : true,
      }
    ],
    contextMenuButtons: canPerform && canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};


