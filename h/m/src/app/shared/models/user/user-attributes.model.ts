export class UserAttributesModel {
  company: string = '';
  email: string = '';
  firstName: string = '';
  isSysAdmin: boolean = false;
  isActive: boolean = false;
  lastName: string = '';
  shippingAddress: string = '';
  uid: string = '';
  preferredName: string = '';
  jobTitle: string = '';
  language: string = 'en';
  avatar: string = '';
}
