import { Injectable } from '@angular/core';
import { IGitVersion } from 'src/app/core/models/IGitVersion';

@Injectable({
  providedIn: 'root'
})
export class GitInfoService {

  public get gitInfo(): IGitVersion {
    try {
      const gitInfo = require('../../../git-version.json');
      if (!gitInfo) {
        throw new Error('Git info was not found');
      }
      return gitInfo;
    } catch (e) {
      console.error(e);
      return { commits: [], tag: '' };
    }
  }
}
