import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, ElementRef } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { IInvoiceLedger, IInvoiceLedgerCreate } from 'src/app/core/models/resources/IInvoiceLedger';
import { FormGroup, FormBuilder, AbstractControl, ValidationErrors } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { InvoiceLedgerRepositoryService } from 'src/app/core/services/repositories/invoice-ledger-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { Validators } from 'src/app/core/classes/validators';
import { MandatorKey, mandatorNoModifier, IMandatorLookupRow } from 'src/app/core/models/resources/IMandator';
import { MandatorClosedYearsRepositoryService } from 'src/app/core/services/repositories/mandator-closed-year-repository.service';
import { IMandatorClosedBookingYear } from 'src/app/core/models/resources/IMandatorClosedBookingYear';
import { IDebitorCreditorFull, DebitorCreditorKey, IDebitorCreditorRow } from 'src/app/core/models/resources/IDebitorCreditor';
import { distinctUntilChanged, takeUntil, tap, map, switchMap, startWith, filter, skip } from 'rxjs/operators';
import * as moment from 'moment';
import { convertToMap, getDateFormat, getDateTimeFormat, setTranslationSubjects } from 'src/app/shared/helpers/general';
import { NvGridConfig } from 'nv-grid';
import { gridConfig } from 'src/app/shared/components/autocomplete/modals/currency-grid-config';
import { IUser, UserKey } from 'src/app/core/models/resources/IUser';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { IVoucherType } from 'src/app/core/models/resources/IVoucherType';
import { VoucherTypeRepositoryService } from 'src/app/core/services/repositories/voucher-type-repository.service';
import { BehaviorSubject, forkJoin, ReplaySubject, Subscription, combineLatest, Observable, of } from 'rxjs';
import { VoyageKey, IVoyage } from 'src/app/core/models/resources/IVoyage';
import { DebitorCreditorMainService } from 'src/app/core/services/mainServices/debitor-creditor-main.service';
import { dateFormatTypes } from 'src/app/core/models/dateFormat';
import { PaymentConditionsRepositoryService } from 'src/app/core/services/repositories/payment-conditions-repository.service';
import { IPaymentCondition, IPaymentStepRow } from 'src/app/core/models/resources/IPaymentConditions';
import { ToCurrentDateFormatPipe } from 'src/app/shared/pipes/to-current-date-format.pipe';
import { getDiscounts, getPaymentDateOfCondition } from 'src/app/features/accounting/helpers/general';
import { gridConfig as discountGridConfig } from 'src/app/core/constants/nv-grid-configs/discount-grid.config';
import { ICurrency, CurrencyKey } from 'src/app/core/models/resources/ICurrency';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { gridConfig as mandatorGridConfig } from 'src/app/shared/components/autocomplete/modals/mandator-grid-config';
import { MandatorMainService } from 'src/app/core/services/mainServices/mandator-main.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { getGridConfig as debitorCreditorGridConfig } from 'src/app/core/constants/nv-grid-configs/debitors-creditors.config';
import { IVesselAccounting } from 'src/app/core/models/resources/IVesselAccounting';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';
import { getVesselAccountingGridConfig } from 'src/app/core/constants/nv-grid-configs/vessel-accounting.config';
import { VoyagesMainService } from 'src/app/core/services/mainServices/voyages-main.service';
import { getGridConfig as voyageGridConfig } from 'src/app/core/constants/nv-grid-configs/voyages.config';
import { getGridConfig as bookingACcountGridConfig } from 'src/app/core/constants/nv-grid-configs/booking-accounts.config';
import { IMandatorAccount, MandatorAccountKey } from 'src/app/core/models/resources/IMandatorAccount';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { BookingAccountMainService } from 'src/app/core/services/mainServices/booking-account-main.service';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { AddressRepositoryService } from 'src/app/core/services/repositories/address-repository.service';
import { AddressTypeRepositoryService } from 'src/app/core/services/repositories/address-type-repository.service';
import { IAddressType } from 'src/app/core/models/resources/IAddressType';
import { CountryRepositoryService } from 'src/app/core/services/repositories/country-repository.service';
import { MandatorAccountRoleRepositoryService } from 'src/app/core/services/repositories/mandator-account-role.repository.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { autocompleteDiscountOptionsFilter } from 'src/app/shared/components/autocomplete/custom-filter-functions';
import { IAutocompleteCustomOptionsFilter } from 'src/app/shared/components/autocomplete/autocomplete.component';
import { IInvoiceLedgerControls, invoiceLedgerControls } from './invoice-ledger-detail-view.helper';
import { VesselMainService } from '../../../../../../core/services/mainServices/vessel-main.service';
import { IVesselWithAccountings, VesselKey } from 'src/app/core/models/resources/IVessel';

type IEntitiesList = [
  IMandatorLookupRow[],
  IMandatorClosedBookingYear[],
  IVoucherType[],
  IDebitorCreditorFull[],
  IPaymentCondition[],
  ICurrency[],
  IAddressType[],
];

@Component({
  selector: 'nv-invoice-ledger-detail-view',
  templateUrl: './invoice-ledger-detail-view.component.html',
  styleUrls: ['./invoice-ledger-detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InvoiceLedgerDetailViewComponent extends ResourceDetailView<IInvoiceLedger, IInvoiceLedgerCreate>
  implements OnInit, OnDestroy {

  protected repoParams = {};
  protected resourceDefault = {
    invoiceDate: new Date().toISOString(),
    receiptDate: moment().utc(true).toISOString(),
  } as IInvoiceLedger;

  private subscriptions: Subscription[] = [];
  private mandatorClosedYears: IMandatorClosedBookingYear[] = [];
  private dtFormat: string;
  private currentUser: IUser;
  private paymentConditionsOfAccounts: IPaymentCondition[];
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  private currentConditionOfPayment: IPaymentCondition;
  private voucherTypes: IVoucherType[] = [];
  private companyAddressType: IAddressType;
  private controls: IInvoiceLedgerControls = {} as IInvoiceLedgerControls;

  public customTitle: string;
  public routeBackUrl: string;
  public form: FormGroup;
  // ng-grid configs
  public currencyGridConfig: NvGridConfig = gridConfig;
  public discountGridConfig: NvGridConfig = discountGridConfig;
  public mandatorGridConfig: NvGridConfig = mandatorGridConfig();
  public vesselAccountingGridConfig: NvGridConfig = getVesselAccountingGridConfig({}, () => false);
  public debitorCreditorGridConfig: NvGridConfig = debitorCreditorGridConfig({}, () => false);
  public voyageGridConfig: NvGridConfig = voyageGridConfig({}, () => false);
  public bookingACcountGridConfig: NvGridConfig = bookingACcountGridConfig({}, () => false);
  public mandatorNoModifier = mandatorNoModifier;
  public autocompleteDiscountOptionsFilter: IAutocompleteCustomOptionsFilter = autocompleteDiscountOptionsFilter;

  public mandatorId$ = new BehaviorSubject<MandatorKey>(null);
  public vesselAccountingId$ = new BehaviorSubject<VoyageKey>(null);
  public discounts$ = new BehaviorSubject<IPaymentStepRow[]>([]);
  public mandators$ = new BehaviorSubject<IMandatorLookupRow[]>([]);
  public currencies$ = new BehaviorSubject<ICurrency[]>([]);
  public debitorCreditor$ = new BehaviorSubject<IDebitorCreditorRow[]>([]);
  public vesselAccountings$: Observable<IVesselAccounting[]>;
  public voyages$: Observable<IVoyage[]>;
  public mandatorBookingAccounts$ = new BehaviorSubject<IMandatorAccount[]>([]);
  public formElement: ElementRef = this.el.nativeElement.querySelector('form');

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    invoiceLedgerRepo: InvoiceLedgerRepositoryService,
    baseComponentService: BaseComponentService,
    private mandatorClosedYear: MandatorClosedYearsRepositoryService,
    private userRepo: UserRepositoryService,
    private voucherTypesRepo: VoucherTypeRepositoryService,
    private debitorCreditorService: DebitorCreditorMainService,
    private paymentOfConditionsService: PaymentConditionsRepositoryService,
    private mandatorMainService: MandatorMainService,
    private currencyRepo: CurrencyRepositoryService,
    private vesselAccountingService: VesselAccountingRepositoryService,
    private voyagesService: VoyagesMainService,
    private bookingAccountService: BookingAccountMainService,
    private mandatorAccounts: MandatorAccountRepositoryService,
    private addressRepo: AddressRepositoryService,
    private addressTypeRepo: AddressTypeRepositoryService,
    private countryRepo: CountryRepositoryService,
    public mandatorAccountRoleRepo: MandatorAccountRoleRepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    private vesselMainService: VesselMainService,
    public el: ElementRef,
    private cdr: ChangeDetectorRef,
  ) {
    super(invoiceLedgerRepo, baseComponentService);
  }

  public ngOnInit() {
    this.routeBackUrl = this.url(this.sitemap.accounting.children.booking.children.invoiceLedger);
    const { id } = this.route.snapshot.params;
    this.dtFormat = getDateTimeFormat(this.settingsService.locale);
    this.currentUser = this.authService.getUser();
    forkJoin([
      this.mandatorMainService.fetchLookupsWithRolesOnly(),
      this.mandatorClosedYear.fetchByMandators(),
      this.voucherTypesRepo.fetchAll({}),
      this.debitorCreditorService.fetchAll(),
      this.paymentOfConditionsService.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.addressTypeRepo.fetchAll({}),
      this.mandatorAccounts.fetchAll({}),
    ]).subscribe(res => {
      const [
        mandators,
        mandatorClosedYears,
        voucherTypes,
        debitorCreditors,
        paymentConditionsOfAccounts,
        currencies,
        addressTypes,
      ] = res as IEntitiesList;
      // TODO: Change this when we have enums for address types
      this.companyAddressType = addressTypes.find(a => a.id === 1);
      this.mandatorClosedYears = mandatorClosedYears;
      this.voucherTypes = voucherTypes;
      this.paymentConditionsOfAccounts = paymentConditionsOfAccounts;
      this.currencies$.next(currencies);
      this.mandators$.next(mandators);
      this.debitorCreditor$.next(this.debitorCreditorService.toRows(debitorCreditors));
      id === 'new'
        ? this.enterCreationMode()
        : this.loadResource(id);
    });
  }

  public getTitle = () => this.customTitle;

  public init(invoiceLedger: IInvoiceLedger = this.resourceDefault): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
    if (invoiceLedger.voucherNumber) {
      this.customTitle = `${invoiceLedger.voucherNumber.toString()}(${invoiceLedger.invoiceNumber})`;
    }

    const currentYear = new Date().getFullYear();

    this.initForm(this.fb.group({
      mandatorId: [invoiceLedger.mandatorId, [Validators.required]],
      debitorCreditorId: [invoiceLedger.debitorCreditorId, [Validators.required]], // supplier
      mandatorAccountId: [invoiceLedger.mandatorAccountId],
      voyageId: [invoiceLedger.voyageId],
      currencyId: [invoiceLedger.currencyId, [Validators.required]],
      voucherNumber: [invoiceLedger.voucherNumber, [Validators.required]],
      orderNumber: [invoiceLedger.orderNumber, [Validators.maxLength(20)]],
      year: [invoiceLedger.year, [
        Validators.required,
        Validators.number,
        Validators.yearRange(currentYear - 10, currentYear),
      ]],
      invoiceDate: [invoiceLedger.invoiceDate, [
        Validators.required,
        Validators.isDateInputInvalid,
        this.invoiceDateYearValidator,
      ]],
      invoiceNumber: [invoiceLedger.invoiceNumber, [
        Validators.required,
        Validators.maxLength(20),
      ]],
      discount: [invoiceLedger.discount, [Validators.required]],
      paymentDate: [invoiceLedger.paymentDate,
      [
        Validators.required,
        Validators.isDateInputInvalid,
        this.validatePaymentDate,
        this.paymentDateOfDiscount
      ]],
      amount: [invoiceLedger.amount, [
        Validators.required,
        Validators.greaterThan(0)
      ]],
      hasCredit: [invoiceLedger.hasCredit],
      taxKey: [invoiceLedger.taxKey, [Validators.maxLength(20)]],
      remark: [invoiceLedger.remark, [Validators.maxLength(100)]],
      barcode: [invoiceLedger.barcode, [Validators.maxLength(15)]],
      receiptDate: [invoiceLedger.receiptDate, [
        Validators.required,
        this.validateReceiptDate
      ]],
      creationDate: [invoiceLedger.creationDate],
      changedDate: [invoiceLedger.changedDate],
      createdBy: [invoiceLedger.createdBy],
      changedBy: [invoiceLedger.changedBy],
      booked: [invoiceLedger.booked, [Validators.maxLength(50)]],
      vesselAccountingId: [invoiceLedger.vesselAccountingId],
      // view controls
      changedByChangeDate: [],
      createdByCreateDate: [],
      voucherNumberAbbr: [],
      mandatorDisplayValue: [],
      voucherYear: [],
      deletionReason: [invoiceLedger.deletionReason],
    }));

    invoiceLedgerControls.forEach(controlName => {
      this.controls[controlName] = this.form.get(controlName);
    });
    this.initFieldsListnersAndPredefine();
    this.disableViewFields();
  }

  private disableViewFields() {
    const disabledFields: string[] = [
      'changedByChangeDate', 'createdByCreateDate',
      'voucherNumberAbbr', 'mandatorDisplayValue',
      'voucherYear', 'deletionReason',
      'booked'
    ];
    if (this.resource && this.resource.id) {
      disabledFields.push('year');
      disabledFields.push('mandatorId');
      if (!this.isCurrencyEnabled()) {
        disabledFields.push('currencyId');
      }
    }
    this.setDisabledFields(disabledFields);
    this.disableMarkedFields();
  }

  private isCurrencyEnabled(): boolean {
    const mandatorAccountId: MandatorAccountKey = this.controls.mandatorAccountId.value;
    if (!mandatorAccountId) {
      return true;
    }
    const mandatorAccount = this.mandatorAccounts.getStreamValue()
      .find(ma => ma.id === mandatorAccountId);
    const bookingAccount = this.bookingAccountRepo.getStreamValue()
      .find(ba => ba.id === mandatorAccount.bookingAccountId);
    return bookingAccount.currencyId !== this.controls.currencyId.value;
  }

  private initFieldsListnersAndPredefine() {
    const mandatorId = this.resource && this.resource.mandatorId;
    const currencyId = this.resource && this.resource.currencyId;
    const debitorCreditorId = this.resource && this.resource.debitorCreditorId;
    const mandatorAccountId = this.resource?.mandatorAccountId;

    /**
     * Filter only actives and in edit mode in addition to actives only
     * ones we have already selected
     * */
    if (this.creationMode || (!this.creationMode && this.resource && this.resource.id)) {

      this.mandators$.next(this.mandators$.value
        .filter(m => m.isActive || m.id === mandatorId));

      this.currencies$.next(this.currencies$.value
        .filter(c => c.isActive || c.id === currencyId));

      this.debitorCreditor$.next(this.debitorCreditor$.value
        .filter(dc => dc.isActive || dc.id === debitorCreditorId));
    }

    /**
     * Form Control listners
     */
    const yearControl: AbstractControl = this.controls.year;
    this.subscriptions.push(
      yearControl.valueChanges
        .pipe(
          distinctUntilChanged(),
          takeUntil(this.componentDestroyed$),
          filter(() => yearControl.valid)
        ).subscribe(() => this.predefineVoucherYear())
    );

    this.vesselAccountings$ = combineLatest([
      this.mandatorId$,
      this.vesselAccountingService.fetchAll({}),
      this.vesselMainService.fetchWithRolesOnly(),
    ]).pipe(
      map(([mid, vesselaccountings, vessels]) => {
        const vesselMap: Map<VesselKey, IVesselWithAccountings> = convertToMap<
          IVesselWithAccountings,
          VesselKey
        >(vessels);
        return mid
          ? vesselaccountings
            .filter(v => v.mandatorId === mid && vesselMap.has(v.vesselId))
          : [];
      }
      ),
      tap(vesselaccountings => {
        const vesselAccountingId: AbstractControl = this.controls.vesselAccountingId;
        if (vesselaccountings.length > 0 && !vesselAccountingId.value) {
          vesselAccountingId.patchValue(this.resource?.vesselAccountingId);
        } else if (!vesselaccountings.length && vesselAccountingId.value) {
          vesselAccountingId.patchValue(null);
        }
      })
    );

    this.voyages$ = combineLatest([
      this.controls.vesselAccountingId.valueChanges
        .pipe(
          startWith(this.resource && this.resource.vesselAccountingId),
        ),
      this.voyagesService.fetchAll({}),
    ]).pipe(
      map(([vesselAccountingId, voyages]) =>
        this.voyagesService
          .toRows(voyages
            .filter(voyage => voyage.vesselAccountingId === vesselAccountingId))
      ),
    );


    if (!this.mandatorBookingAccounts$.value.length) {
      this.mandatorAccounts.fetchAll({}).pipe(
        switchMap((mandatorAccounts) => {
          return combineLatest([
            this.mandatorId$,
            this.bookingAccountService.fetchAll(),
          ]).pipe(
            map(([mid, bookingAccounts]) => {
              if (!mid) {
                return [];
              }
              const defaultMandatorAccountId = this.resource
                && this.resource.mandatorAccountId;
              const currentMandatorAccounts = mandatorAccounts
                .filter(ma => ma.mandatorId === mid);

              return bookingAccounts.filter(ba => {
                return !!currentMandatorAccounts.find(m => m.bookingAccountId === ba.id);
              }).map(ba => {
                const mandatorAccount = currentMandatorAccounts
                  .find(ma => ma.bookingAccountId === ba.id);
                return { ...ba, ...mandatorAccount };
              }).filter(ma => ma.isActive || ma.id === defaultMandatorAccountId);
            }),
            tap(accounts => this.mandatorBookingAccounts$.next(accounts))
          );
        })).subscribe();
    }

    this.subscriptions.push(this.controls.vesselAccountingId.valueChanges
      .pipe(
        distinctUntilChanged(),
        takeUntil(this.componentDestroyed$),
        tap(() => this.enableOrDisableVoyage())
      ).subscribe(v => this.vesselAccountingId$.next(v)));

    const debitorCreditorControl: AbstractControl = this.controls.debitorCreditorId;
    this.subscriptions.push(debitorCreditorControl.valueChanges
      .pipe(
        distinctUntilChanged(),
        takeUntil(this.componentDestroyed$),
        filter(() => debitorCreditorControl.enabled),
        skip(1), // skip the first change that happens when form gets enabled
      ).subscribe(() => this.onDebitorCreditorChange()));

    this.controls.discount.valueChanges.pipe(
      distinctUntilChanged(),
      takeUntil(this.componentDestroyed$)
    ).subscribe(() => this.onDiscountOrInvoiceChange());

    this.controls.invoiceDate.valueChanges.pipe(
      distinctUntilChanged(),
      takeUntil(this.componentDestroyed$)
    ).subscribe(() => this.onDiscountOrInvoiceChange());

    this.subscriptions.push(this.controls.mandatorId.valueChanges
      .pipe(
        distinctUntilChanged(),
        takeUntil(this.componentDestroyed$),
        tap(() => {
          if (this.form.dirty) {
            this.controls.vesselAccountingId.patchValue(null);
            this.controls.mandatorAccountId.patchValue(null);
            this.predefineYear();
            this.presetCurrencyField(debitorCreditorControl.value);
          }
        })
      ).subscribe(v => this.mandatorId$.next(v)));

    const voucherType = this.voucherTypes.find(v => v.abbreviation === 'REE');
    this.controls.voucherNumberAbbr.patchValue(voucherType.abbreviation);
    this.controls.voucherNumber.patchValue(voucherType.id);

    if (this.resource) {
      if (this.resource.vesselAccountingId) {
        this.mandatorId$.next(this.resource.mandatorId);
        this.vesselAccountingId$.next(this.resource.vesselAccountingId);
      }
      this.setChangeByCreateByFields();
    }
    this.onDebitorCreditorChange();
    const mandator: IMandatorLookupRow = this.mandators$.value
      .find(m => m.id === this.controls.mandatorId.value);

    this.subscriptions.push(this.controls.mandatorAccountId.valueChanges
      .pipe(
        startWith(mandatorAccountId),
        distinctUntilChanged(),
        takeUntil(this.componentDestroyed$),
      ).subscribe(() => {
        const id = this.controls.debitorCreditorId.value;
        this.presetCurrencyField(id);
      }));

    this.onMandatorChange(mandator);
    this.cdr.detectChanges();
  }

  private onDiscountOrInvoiceChange() {
    if (this.form.disabled) {
      return;
    }
    const invoiceDateCtrl: AbstractControl = this.controls.invoiceDate;
    const debitorCreditorCtrl: AbstractControl = this.controls.debitorCreditorId;
    if (!invoiceDateCtrl.value || !debitorCreditorCtrl.value) {
      return;
    }
    const paymentDateCtrl: AbstractControl = this.controls.paymentDate;
    const days: number = this.getDiscountLevelDays();
    const paymentDate: string = moment.utc(invoiceDateCtrl.value)
      .local().add(days, 'days').toISOString();

    if (paymentDateCtrl.enabled) {
      paymentDateCtrl.patchValue(paymentDate);
    }
  }

  private getDiscountLevelDays(): number {
    const discount: string | number = this.controls.discount.value;
    if (this.currentConditionOfPayment) {
      if (+this.currentConditionOfPayment.percentageDiscountLevel1 === +discount) {
        return this.currentConditionOfPayment.daysDiscountLevel1;
      } else if (+this.currentConditionOfPayment.percentageDiscountLevel2 === +discount) {
        return this.currentConditionOfPayment.daysDiscountLevel2;
      } else if (+this.currentConditionOfPayment.percentageDiscountLevel3 === +discount) {
        return this.currentConditionOfPayment.daysDiscountLevel3;
      } else if (+discount === 0) {
        return this.currentConditionOfPayment.net;
      }
    } else {
      return 14;
    }
  }

  /**
   * The highest priorty has booking account's restricted currency
   * fallsback to debitor or creditor's address's company currency
   * fallsback to mandator local currency
   * @param debitorCreditorId
   */
  private presetCurrencyField(debitorCreditorId: DebitorCreditorKey) {
    if (this.form.disabled) {
      return;
    }
    const mandatorAccountId: MandatorAccountKey = this.controls.mandatorAccountId.value;
    const currencyCtrl: AbstractControl = this.controls.currencyId;
    const mandatorAccount = this.mandatorAccounts.getStreamValue()
      .find(ma => ma.id === mandatorAccountId);
    const debitorCreditor = this.debitorCreditor$.value
      .find(db => db.id === debitorCreditorId);
    const bookingAccount = mandatorAccount
      ? this.bookingAccountRepo.getStreamValue()
        .find(ba => ba.id === mandatorAccount.bookingAccountId)
      : null;

    if (bookingAccount) {
      if (bookingAccount.currencyId) {
        const bookingAccountRestrictedCurrncy = this.currencies$.value
          .find(c => c.id === bookingAccount.currencyId);
        currencyCtrl.patchValue(bookingAccountRestrictedCurrncy.id);
        currencyCtrl.disable();
      } else {
        currencyCtrl.enable();
      }
      return;
    }

    if (!this.form.disabled) {
      currencyCtrl.enable();
    }

    if (debitorCreditor && debitorCreditor.companyId) {
      this.addressRepo.fetchAll({ queryParams: { companyId: debitorCreditor.companyId } })
        .pipe(
          map(addresses => {
            const companyAddresses = addresses
              .filter(a => {
                // Filter: addresstype “company address”
                return a.addressTypeId === this.companyAddressType.id;
              });
            // if there are more than one address of this type,
            // take default address of this type
            if (companyAddresses.length > 1) {
              return companyAddresses.find(ca => ca.isTypeDefault);
            }
            return companyAddresses[0];
          }),
          switchMap(companyAddress => {
            const mandator: IMandatorLookupRow = this.mandators$.value
              .find(m => this.controls.mandatorId.value === m.id);
            if (companyAddress) {
              return this.countryRepo.fetchOne(companyAddress.countryId, {})
                .pipe(map(country => {
                  //  If a company has its location in a country
                  // that has Sepa-Participant = true, preset to EUR,
                  if (country.sepaParticipant) {
                    const euroCurrency = this.currencies$.value
                      .find(c => c.isoCode === 'EUR');
                    return euroCurrency && euroCurrency.id;
                  }
                  const foundCurrency = this.currencies$.value
                    .find(c => c.isoCode === country.currencyIsoCode);
                  return foundCurrency
                    ? foundCurrency.id
                    : mandator.currencyId;
                }));
            } else {
              // If no such address type or value is present
              // preset currency to mandators local currency
              return of(mandator && mandator.currencyId);
            }
          }),
          tap((currencyId: CurrencyKey) => {
            if (currencyId) {
              currencyCtrl.patchValue(currencyId);
            }
          })
        )
        .subscribe();
    } else if (this.controls.mandatorId.value) {
      // If no such address type or value is present
      // preset currency to mandators local currency
      const mandator: IMandatorLookupRow = this.mandators$.value
        .find(m => this.controls.mandatorId.value === m.id);
      if (mandator && mandator.currencyId) {
        currencyCtrl.patchValue(mandator.currencyId);
      }
    }
  }

  private onDebitorCreditorChange() {
    const debitorCreditorId: DebitorCreditorKey = this.controls.debitorCreditorId.value;

    if (this.creationMode) {
      this.presetCurrencyField(debitorCreditorId);
    }
    const discountCtrl: AbstractControl = this.controls.discount;
    if (debitorCreditorId) {
      const conditionOfPayment = this.paymentConditionsOfAccounts
        .find(p => p.debitorCreditorId === debitorCreditorId);
      if (conditionOfPayment) {
        this.currentConditionOfPayment = conditionOfPayment;
        const invoiceDate: string = this.controls.invoiceDate.value;
        const discountSteps: IPaymentStepRow[] = getDiscounts(conditionOfPayment, invoiceDate)
          .filter(d => d.percentage || d.percentage === 0);

        discountSteps.forEach(discount =>
          discount.paymentDateLabel = new ToCurrentDateFormatPipe(this.settingsService)
            .transform(discount.paymentDate)
        );
        this.discounts$.next(discountSteps);
        const percentages: number[] = discountSteps.map(d => d.percentage)
          .filter(d => d || d === 0);

        if (!this.form.disabled) {
          discountCtrl.patchValue(Math.max(...percentages));
          discountCtrl.enable();
        }
      } else {
        this.onNoConditionOfPayment();
        const invoiceDate: string = this.controls.invoiceDate.value;
        const paymentDate: string = getPaymentDateOfCondition(
          invoiceDate,
          14
        );
        this.discounts$.next([
          {
            step: this.wording.accounting.net,
            percentage: 0,
            days: 14,
            paymentDate,
            paymentDateLabel: new ToCurrentDateFormatPipe(this.settingsService)
              .transform(paymentDate)
          }
        ]);
        discountCtrl.setValue(0);
      }
    } else {
      this.onNoConditionOfPayment();
    }
    this.cdr.detectChanges();
  }

  private onNoConditionOfPayment() {
    const discountCtrl: AbstractControl = this.controls.discount;
    this.currentConditionOfPayment = null;
    discountCtrl.patchValue(null);
    discountCtrl.disable();
  }

  private enableOrDisableVoyage() {
    const { vesselAccountingId, voyageId } = this.form.controls;
    if (vesselAccountingId.value) {
      if (this.form.enabled) {
        voyageId.enable();
      }
    } else {
      voyageId.disable();
      voyageId.setValue(null);
      this.cdr.detectChanges();
    }
  }

  private setChangeByCreateByFields() {
    const createDate: string = moment.utc(this.controls.creationDate.value)
      .local()
      .format(this.dtFormat);
    const changeDate: string = moment.utc(this.controls.changedDate.value)
      .local()
      .format(this.dtFormat);

    const createdByUserId: UserKey = this.controls.createdBy.value;
    const changedByUserId: UserKey = this.controls.changedBy.value;
    let createdByUser: string;
    let changedByUser: string;

    if (this.controls.creationDate.value) {
      if (createdByUserId === this.currentUser.id) {
        createdByUser = `${this.currentUser.firstName} ${this.currentUser.lastName}`;
        this.controls.createdByCreateDate.patchValue(`${createDate} ${createdByUser}`);
      } else if (createdByUserId) {
        this.userRepo.fetchOne(createdByUserId, {}).subscribe(user => {
          createdByUser = `${user.firstName} ${user.lastName}`;
          this.controls.createdByCreateDate.patchValue(`${createDate} ${createdByUser}`);
        });
      }
    }

    if (this.controls.changedDate.value) {
      if (changedByUserId === this.currentUser.id) {
        changedByUser = `${this.currentUser.firstName} ${this.currentUser.lastName}`;
        this.controls.changedByChangeDate.patchValue(`${changeDate} ${changedByUser}`);
      } else if (changedByUserId) {
        this.userRepo.fetchOne(changedByUserId, {}).subscribe(user => {
          changedByUser = `${user.firstName} ${user.lastName}`;
          this.controls.changedByChangeDate.patchValue(`${changeDate} ${changedByUser}`);
        });
      }

      if (this.resource.booked) {
        this.controls.booked.patchValue(`${this.resource.voucherNumber} ${changedByUser} ${changeDate}`);
      }
    }

  }

  private invoiceDateYearValidator = (ctrl: AbstractControl) => {
    if (!ctrl.value) {
      return null;
    }
    const currentYear = new Date().getFullYear() + 1;
    const yearValue = new Date(ctrl.value).getFullYear();
    const nextDay = moment.utc().local().add(1, 'day').format();

    if (moment(ctrl.value).isAfter(nextDay) || yearValue < currentYear - 10) {
      const afterDate = moment().subtract(10, 'years').startOf('year')
        .format(getDateFormat(this.settingsService.locale));
      const beforeDate = moment.utc().local().add(1, 'day')
        .format(getDateFormat(this.settingsService.locale));

      return {
        error: {
          message:
            setTranslationSubjects(
              this.wording.general.dateShouldBeAfterBefore,
              { afterDate, beforeDate },
            ),
        }
      };
    } else {
      return null;
    }
  }


  public submit() {
    if (!this.form.valid) {
      return;
    }

    if (!this.checkForInactiveEntities()) {
      return;
    }

    let value: IInvoiceLedger = this.form.value;

    if (this.resourceDefault.receiptDate === value.receiptDate) {
      this.controls.receiptDate.patchValue(moment().utc(true).toISOString());
    }

    value = this.form.getRawValue();

    if (this.creationMode) {
      this.createResource(value).subscribe();
    } else {
      this.updateResource({
        id: this.resource.id,
        ...value,
      }).subscribe();
    }
  }

  /**
   * returns `true` if ok
   */
  private checkForInactiveEntities(): boolean {
    const currentCurrency: ICurrency = this.currencies$.value
      .find(c => this.controls.currencyId.value === c.id);

    const debitorCreditorId: DebitorCreditorKey = this.controls.debitorCreditorId.value;
    const currentDebitorCreditor: IDebitorCreditorFull = this.debitorCreditor$.value
      .find(dc => dc.id === debitorCreditorId);

    const currentMandator: IMandatorLookupRow = this.mandators$.value
      .find(m => m.id === this.controls.mandatorId.value);

    const currentBookingAccountId: MandatorAccountKey = this.controls.mandatorAccountId.value;

    const currentBookingAccount: IBookingAccount = this.mandatorBookingAccounts$.value
      .find(ba => ba.id === currentBookingAccountId) as any as IBookingAccount;

    const labelKey = getLabelKeyByLanguage(this.settingsService.language);
    if (currentMandator && !currentMandator.isActive) {
      return this.showInactiveWarningModal(currentMandator.name);
    } else if (currentBookingAccount && !currentBookingAccount.isActive) {
      return this.showInactiveWarningModal(currentBookingAccount[labelKey]);
    } else if (!currentCurrency.isActive) {
      return this.showInactiveWarningModal(currentCurrency[labelKey]);
    } else if (!currentDebitorCreditor.isActive) {
      return this.showInactiveWarningModal(currentDebitorCreditor.company.companyName);
    } else {
      return true;
    }
  }

  private showInactiveWarningModal(name: string) {
    this.confirmationService.warning({
      wording: this.wording.general.notActiveWarning,
      subjects: { subject: name },
      okButtonWording: this.wording.general.ok,
      cancelButtonWording: this.wording.general.discard,
    }).subscribe(ok => {
      if (!ok) {
        this.leaveEditMode();
      }
    });
    return false;
  }

  private predefineYear() {
    const mandatorCtrl: AbstractControl = this.controls.mandatorId;
    const mandatorId: MandatorKey = mandatorCtrl.value;
    if (mandatorId) {
      const currentMandatorsClosedYears: number[] = this.mandatorClosedYears
        .filter(m => m.mandatorId === mandatorId)
        .map(m => m.year).sort();

      const currentYear = new Date().getFullYear();

      const invoiceDateyear = new Date(this.controls.invoiceDate.value).getFullYear();
      if (!isNaN(invoiceDateyear)) {
        if (currentMandatorsClosedYears.includes(invoiceDateyear)) {
          // pick the highest
          if (currentMandatorsClosedYears[currentMandatorsClosedYears.length - 1] !== currentYear) {
            this.controls.year.patchValue(invoiceDateyear + 1);
          }
        } else {
          this.controls.year.patchValue(invoiceDateyear);
        }
      }
    }
  }

  public onMandatorChange(mandator: IMandatorLookupRow): void {
    if (mandator) {
      this.controls.mandatorDisplayValue.patchValue(mandatorNoModifier(mandator.no));
      this.predefineVoucherYear();
    } else {
      this.controls.mandatorDisplayValue.patchValue('');
      this.controls.voucherYear.patchValue('');
    }
  }

  private predefineVoucherYear() {
    const year: number = +this.controls.year.value;
    this.controls.voucherYear.patchValue(moment(year, 'YYYY').format('YY'));
  }

  private validatePaymentDate = (control: AbstractControl) => {
    if (!control.value || !this.form) {
      return null;
    }
    const dateTimeFormat = dateFormatTypes[this.settingsService.locale];
    const isValid = moment(control.value, [dateTimeFormat, moment.ISO_8601])
      .isSameOrAfter(
        moment(this.controls.invoiceDate.value, [dateTimeFormat, moment.ISO_8601]),
        'day'
      );
    if (isValid) {
      return null;
    }
    return {
      paymentDateIsLessThenInvoiceDate: {
        message: this.wording.accounting.paymentDateEqualOrAfterInvoiceDate
      }
    };
  }

  private validateReceiptDate = (ctrl: AbstractControl) => {
    if (!ctrl.value || !this.form) {
      return null;
    }

    const dateTimeFormat = dateFormatTypes[this.settingsService.locale];

    const isDateValid = moment(ctrl.value, [dateTimeFormat, moment.ISO_8601])
      .isAfter(
        moment(this.controls.invoiceDate.value,
          [dateTimeFormat, moment.ISO_8601]),
        'day'
      );
    const isPredefinedDate = moment(ctrl.value)
      .startOf('day')
      .isSame(moment(this.resourceDefault.receiptDate)
        .startOf('day'))
      || (this.resource && moment(ctrl.value)
        .startOf('day')
        .isSame(moment(this.resource.receiptDate)
          .startOf('day')));
    if (isDateValid || isPredefinedDate) {
      return null;
    }
    return {
      paymentDateIsLessThenInvoiceDate: {
        message: this.wording.accounting.receiptDateAfterInvoiceDate
      }
    };
  }

  public ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public editClicked() {
    if (this.resource.isDeleted) {
      this.nzModalService.warning({
        nzTitle: this.wording.general.cannotEditDeletedItem[this.settingsService.language],
      });
    } else {
      this.enterEditMode();
    }
  }

  public paymentDateOfDiscount = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value || !this.form) {
      return null;
    }

    const discountControl: AbstractControl = this.controls.discount;

    if (discountControl.value === null || this.discounts$.value.length === 1) {
      return null;
    }

    const foundDiscountIndex = this.discounts$.value
      .findIndex(discountOfAccount => discountOfAccount.percentage === +discountControl.value);

    const isZeroPercent: boolean = foundDiscountIndex === this.discounts$.value.length - 1;
    const foundDiscount = this.discounts$.value[foundDiscountIndex];
    const nextDiscount = this.discounts$.value[foundDiscountIndex - 1];

    const isBeforeOrSameCurrentDiscount = !isZeroPercent
      ? moment(control.value).isSameOrBefore(foundDiscount.paymentDate)
      : true;

    const isAfterNextDiscount = nextDiscount
      ? moment(control.value).isAfter(nextDiscount.paymentDate)
      : true;

    const isValid = foundDiscount
      && discountControl.enabled
      && isBeforeOrSameCurrentDiscount
      && isAfterNextDiscount;

    if (isValid) {
      return null;
    }

    markControlAsTouched(control);

    return {
      paymentDateIsNotInRange: {
        message: setTranslationSubjects(
          this.wording.accounting.shouldUpdateDiscountFromPaymentDate, {
          END: new ToCurrentDateFormatPipe(this.settingsService)
            .transform(foundDiscount.paymentDate),
          START: nextDiscount
            ? new ToCurrentDateFormatPipe(this.settingsService)
              .transform(nextDiscount.paymentDate)
            : '-',
        }),
      }
    };
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
