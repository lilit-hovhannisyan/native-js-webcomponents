import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterStoreService } from '@core-services/router-store.service';

import { MainRoutingModule } from './main-routing.module';

@NgModule({
  imports: [CommonModule, MainRoutingModule]
})
export class MainModule {
  constructor(_routerStoreService: RouterStoreService) {}
}
