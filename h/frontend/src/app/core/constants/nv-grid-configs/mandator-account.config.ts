import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'mandatorAccountGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'no',
        title: wording.accounting.no,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wording.accounting.accountName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isOpenItem',
        title: wording.accounting.openItem,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isOpex',
        title: wording.accounting.opex,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'restrictedCurrency',
        title: wording.accounting.restrictedCurrency,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editBookingAccount',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        func: actions.edit,
        hidden: !canPerform(Operations.UPDATE),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteBookingAccount',
        tooltip: wording.general.delete,
        func: actions.delete,
        hidden: !canPerform(Operations.DELETE),
      },
    ],
    toolbarButtons: [
      {
        title: wording.general.add,
        tooltip: wording.general.add,
        icon: 'plus-circle-o',
        func: actions.add,
        hidden: () => !canPerform(Operations.CREATE),
      }
    ]
  });
};
