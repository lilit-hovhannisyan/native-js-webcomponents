import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { BookmarkRepositoryService } from '../../../../../../core/services/repositories/bookmark-repository.service';
import { IBookmarkFull } from 'src/app/core/models/resources/IBookmark';

@Component({
  selector: 'nv-bookmark',
  templateUrl: './bookmark.component.html',
  styleUrls: ['./bookmark.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookmarkComponent implements OnInit {
  public bookmarks$: Observable<IBookmarkFull[]>;
  public url = url;
  public wording = wording;

  constructor(
    private bookmarkRepo: BookmarkRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.bookmarks$ = this.bookmarkRepo.getBookmarksWithNodes();
  }

  public removeBookmark(bookmark: IBookmarkFull): void {
    this.bookmarkRepo.delete(bookmark.id, {}).subscribe();
  }
}
