import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { ICostCenter, CostCenterKey, ICostCenterCreate, ICostCenterDTO, ICostCenterRow } from '../../models/resources/ICostCenter';
import { RepositoryService } from '../repository.service';
import { getCostCenterTypesMap, COST_CENTER_TYPE } from '../../models/enums/cost-center-type';
import { IWording } from '../../models/resources/IWording';

@Injectable({
  providedIn: 'root'
})
export class CostCenterRepositoryService extends AbstractRepository<
ICostCenter,
ICostCenterDTO,
ICostCenterCreate,
RepoParams<ICostCenter>,
CostCenterKey
> {
  protected resourceType = 'Cost_Center';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/cost-centers';
  }

  protected postProcess(item: ICostCenterDTO): ICostCenter { return item; }


  public toRows(costCenters: ICostCenter[]): ICostCenterRow[] {
    const costCenterTypes: Map<COST_CENTER_TYPE, IWording> = getCostCenterTypesMap();
    return costCenters.map(c => this.toRow(c, costCenterTypes));
  }

  private toRow(
    costCenter: ICostCenter,
    costCenterTypes: Map<COST_CENTER_TYPE, IWording>
  ): ICostCenterRow {
    return {
      ...costCenter,
      typeLabel: costCenterTypes.get(costCenter.type),
    };
  }

}
