import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { TaskTemplatesHelperService } from './task-templates-helper.service';

// wrapper component
@Component({
  selector: 'nv-task-templates',
  templateUrl: './task-templates.component.html',
  styleUrls: ['./task-templates.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskTemplatesComponent implements OnInit {
  public wording = wording;

  constructor(
    public taskTemplatesHelperService: TaskTemplatesHelperService,
  ) { }

  public ngOnInit(): void {
  }


}
