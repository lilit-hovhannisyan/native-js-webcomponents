import { IResource } from './IResource';
import { CompanyKey, ICompany } from './ICompany';
import { IWording } from './IWording';
import { wording } from '../../constants/wording/wording';

export type CompanyPartnerContributionKey = number;

export enum PartnerType {
  None = 0,
  General = 1,
  Limited = 2,
}

export enum AmountChangeType {
  Increase = 1,
  Decrease = 2,
}

interface ISharedProps {
  from: string;
  contributionAmount: number;
  companyId: CompanyKey;
  partnerId: CompanyKey;
  partnerType?: PartnerType;
  liabilitySum?: number;
  remark?: string;
  contributionIsZero?: boolean;
  amountChangeType: AmountChangeType;
}

export interface ICompanyPartnerContributionBase extends ISharedProps, IResource<CompanyPartnerContributionKey> { }

export interface ICompanyPartnerContributionCreate extends ISharedProps { }

export interface ICompanyPartnerContributionDTO extends ICompanyPartnerContributionBase { }

export interface ICompanyPartnerContribution extends ICompanyPartnerContributionDTO { }

export interface ICompanyPartnerContributionFull extends ICompanyPartnerContribution, ICompany {
}

/**
 * Enum options for displaying in views
 */
export const typePartnerOptions: Map<number, IWording> = new Map<number, IWording>([
  [PartnerType.General, wording.system.generalPartner],
  [PartnerType.Limited, wording.system.limitedPartner],
]);
