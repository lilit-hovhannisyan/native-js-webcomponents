import { Directive, EventEmitter, Output, AfterViewInit, ElementRef } from '@angular/core';

@Directive({selector: '[nvSpyElement]'})

/**
 * This directive detects and sends an event after an element
 * is being added to the DOM ( usually needed for elements added
 * dynamically and/or conditionally ).
 */

export class SpyElementDirective implements AfterViewInit {
  @Output() public initialized: EventEmitter<ElementRef> = new EventEmitter();

  constructor(private el: ElementRef) {
  }

  public ngAfterViewInit() {
    this.initialized.emit(this.el.nativeElement);
  }
}
