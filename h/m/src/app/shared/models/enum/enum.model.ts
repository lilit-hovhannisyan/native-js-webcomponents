import { Type } from 'class-transformer';
import { EnumValueModel } from './enum-value.model';

export class EnumModel {
  name: string = '';
  displayName: string = '';
  @Type(() => EnumValueModel)
  values: EnumValueModel[] = [];
}
