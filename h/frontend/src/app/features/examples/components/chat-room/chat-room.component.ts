import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nv-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.scss']
})
export class ChatRoomComponent implements OnInit {

  public newOnlineUser: string;
  public disconnectedUser: string;

  constructor(
  ) { }

  public ngOnInit() {
  }

  public onNewOnlineUser(user) {
    this.newOnlineUser = user;
    this.disconnectedUser = '';

    setTimeout(() => {
      this.newOnlineUser = '';
    }, 5000);
  }

  public onDisconnectedUser(user) {
    this.newOnlineUser = '';
    this.disconnectedUser = user;

    setTimeout(() => {
      this.disconnectedUser = '';
    }, 5000);
  }
}
