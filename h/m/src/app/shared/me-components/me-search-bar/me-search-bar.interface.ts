import { KeyValue } from '@angular/common';
import { MeAttributeItem } from '../me-attribute/me-attribute-item';
import { MeButtonI } from '../me-button/me-button.interface';
import { MeSearchInputI } from '../me-search-input/me-search-input-interface';

type view = 'grid' | 'list';
export interface MeSearchBarI {
  filterEnabled: boolean;
  search?: MeSearchInputI;
  usersGroupsSelect?: boolean;
  usersGroups?: {
    options: KeyValue<string, string>[];
    selectedIndex: number;
  };
  sort?: {
    placeholder: string;
    options: KeyValue<string, string>[];
    selectedIndex: number;
  };
  selectButton?: {
    enabled: boolean;
    placeholder: string;
  };
  operationButtons?: {
    button: MeButtonI;
    iFeatherName: string;
    clickEventName: string;
    tooltipText?: string;
  }[];
  forceShowButton?: boolean;
  view?: view;
  singleValueFilter?: MeAttributeItem;
  addButton?: {
    placeholder: string;
  };
}
