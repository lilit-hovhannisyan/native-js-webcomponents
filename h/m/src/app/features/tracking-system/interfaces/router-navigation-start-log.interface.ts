export default interface RouterNavigationStartLogI {
  eventType: string;
  data: {
    url: string;
  };
  userActionId: string | null;
}
