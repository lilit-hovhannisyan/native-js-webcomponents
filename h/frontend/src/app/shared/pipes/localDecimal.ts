import { NvLocale } from './../../core/models/dateFormat';
export type locale = 'de-DE' | 'en-US' | 'en-GB';
export const nvLocalMapper = {
  [NvLocale.US]: 'en-US' as locale,
  [NvLocale.UK]: 'en-GB' as locale,
  [NvLocale.DE]: 'de-DE' as locale
};
