import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { ArticleClassRepositoryService } from 'src/app/core/services/repositories/article-class-repository.service';
import { IArticleClass } from 'src/app/core/models/resources/IArticleClass';
import { getArticleClassesGridConfig } from 'src/app/core/constants/nv-grid-configs/article-classes.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ArticleClassesListViewComponent implements OnInit {
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IArticleClass[]>;

  constructor(
    private articleClassRepo: ArticleClassRepositoryService,
    public gridConfigService: GridConfigService,
    private confirmationService: ConfirmationService,
    private authService: AuthenticationService,
    private router: Router
  ) { }

  public ngOnInit(): void {
    this.articleClassRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.articleClassRepo.getStream();

    const gridActions = {
      edit: (id: number) => this.router.navigate([this.router.url, id]),
      delete: this.deleteClicked,
      add: () => this.router.navigate([this.router.url, 'new']),
    };

    this.gridConfig = getArticleClassesGridConfig(
      gridActions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
    );
  }

  private deleteClicked = (article: IArticleClass) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: article.displayLabel },
    }).subscribe(confirmed => confirmed && this.articleClassRepo.delete(article.id, {}).subscribe());
  }
}
