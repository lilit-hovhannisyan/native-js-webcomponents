import { NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IMandatorLookup, mandatorNoModifier } from 'src/app/core/models/resources/IMandator';

export const getPermissionsGroupsMandatorsModalGridConfig = (
  isAutobankPage = true,
): NvGridConfig => {
  return {
    gridName: `${isAutobankPage ? 'autobank' : 'payment'}PermissionGroupMandatorsModalGrid`,
    title: wording.accounting.mandators,
    sortBy: 'no',
    isSortAscending: true,
    disableHideColumns: true,
    hideRefreshButton: true,
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    rowSelectionType: NvGridRowSelectionType.Checkbox,
    paging: {
      pageNumber: 1,
      pageSize: 10
    },
    columns: [
      {
        key: 'no',
        title: wording.general.numberNo,
        width: 80,
        customFormatFn: (mandator: IMandatorLookup) => mandatorNoModifier(mandator.no),
        filter: {
          values: []
        }
      },
      {
        key: 'name',
        title: wording.general.name,
        width: 300,
        filter: {
          values: []
        }
      },
    ],
  };
};

