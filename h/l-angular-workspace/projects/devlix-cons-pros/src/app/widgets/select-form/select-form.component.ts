import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AppDataService} from '../../dal/app-data.service';
import {Field} from 'l-builder';
import {Router} from '@angular/router';
import {LoadingService} from '../loading/loading.service';

@Component({
  selector: 'app-select-form',
  templateUrl: './select-form.component.html',
  styleUrls: ['./select-form.component.less']
})
export class SelectFormComponent {
    @Input() formTitle: string;
    @Input() submitBtnText = 'Select';
    @Input() fieldsConfig: Field[];
    @Input() lookupKey: string; // pass field name
    @Input() lookupMethod: string; // pass method name
    @Input() nextRouteName: string; // pass route string


    @Output() submitEvent = new EventEmitter();


    constructor(private appDataService: AppDataService,
                private router: Router,
                private loadingService: LoadingService) { }

    onSubmit(event) {
        if (event.formErrorCount === 0) {
            this.loadingService.showLoader(true);

            if (this.lookupKey) {
                const id = event.formdata[this.lookupKey].trim().replace(/\s+/g, '_');
                this.appDataService[this.lookupMethod](id, this.nextRouteName);
            }

            this.submitEvent.emit(event.formdata);
        }
    }
}
