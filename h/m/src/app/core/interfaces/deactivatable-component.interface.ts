import { MeSweetAlertI } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';

export interface DeactivatableComponent {
  applyCheck: boolean;
  leaveWarningModel: MeSweetAlertI;
}
