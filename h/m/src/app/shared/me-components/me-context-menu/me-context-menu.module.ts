import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeContextMenuComponent } from './me-context-menu.component';
import { MatMenuModule } from '@angular/material/menu';
import { MeButtonModule } from '../me-button/me-button.module';
import { IconsModule } from '@shared-modules/icons.module';

@NgModule({
  declarations: [MeContextMenuComponent],
  imports: [CommonModule, MatMenuModule, MeButtonModule, IconsModule],
  exports: [MeContextMenuComponent]
})
export class MeContextMenuModule {}
