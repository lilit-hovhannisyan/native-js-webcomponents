import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IVoyage, IVoyageDTO, IVoyageCreate, VoyageKey } from '../../models/resources/IVoyage';
import { HttpGet } from '../../models/http/http-get';
import { IVoyageCharterName, IVoyageCharterNameResource } from '../../models/resources/IVoyageCharterName';
import { IResourceCollection } from '../../models/resources/IResourceCollection';
import { HttpResponse } from '@angular/common/http';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

export interface IVoyageRepoParams extends RepoParams<IVoyage> {
  queryParams?: {
    vesselAccountingId?: number;
    date?: string;
    vesselId?: number;
  };
}

@Injectable({
  providedIn: 'root'
})
export class VoyagesRepositoryService extends AbstractRepository<
  IVoyage,
  IVoyageDTO,
  IVoyageCreate,
  IVoyageRepoParams,
  VoyageKey
>  {
  protected resourceType = 'Voyages';
  protected sortKey = 'from';
  protected sortAsce = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/voyages';

  protected postProcess = (item: IVoyageDTO): IVoyage => item;

  public fetchCharternames(voyageId: VoyageKey): Observable<IVoyageCharterName[]> {
    const url = `${this.getBaseUrl()}/${voyageId}/charternames`;
    const request = new HttpGet(url);

    return this.repositoryService.httpClient.request(request).pipe(
      map((res: IVoyageCharterNameResource) => res.body.data.items),
    );
  }
}
