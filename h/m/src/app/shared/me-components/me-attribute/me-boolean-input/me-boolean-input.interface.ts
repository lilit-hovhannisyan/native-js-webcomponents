import { FormGroup } from '@angular/forms';

export interface MeBooleanInputI {
  form: FormGroup;
  internalName: string;
  displayName: string;
  required: boolean;
  value: boolean;
  disabled: boolean;
}
