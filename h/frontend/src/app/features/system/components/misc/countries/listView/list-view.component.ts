import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { ICountry } from '../../../../../../core/models/resources/ICountry';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { CountryRepositoryService } from '../../../../../../core/services/repositories/country-repository.service';
import { Observable } from 'rxjs';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/countries.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
})
export class CountryListViewComponent implements OnInit {
  public countries: ICountry[];
  public canCreateCountry = true;

  public countriesGridConfig: NvGridConfig;

  public dataSource$: Observable<ICountry[]>;

  constructor(
    private countryRepository: CountryRepositoryService,
    public gridConfigService: GridConfigService,
    private confirmationService: ConfirmationService,
    private authService: AuthenticationService,
    private router: Router
  ) {
  }

  public ngOnInit() {
    this.countryRepository.fetchAll({}).subscribe();
    this.dataSource$ = this.countryRepository.getStream();


    const gridActions = {
      add: () => this.router.navigate([`${this.router.url}/new`]),
      edit: (c) => this.router.navigate([`${this.router.url}/${c.id}`]),
      delete: this.deleteClicked
    };

    this.countriesGridConfig = getGridConfig(
      gridActions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  public deleteClicked = (country: ICountry) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: country.displayLabel },
    }).subscribe(confirmed => confirmed && this.countryRepository.delete(country.id, {}).subscribe());
  }
}
