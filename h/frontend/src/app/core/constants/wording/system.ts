// tslint:disable: max-line-length
export const system = {
  overlappedRangesFound: { en: 'Overlapped Ranges Found', de: 'Überlappende Bereiche gefunden' },
  gapsRangesFound: { en: 'There must be no gaps between ranges', de: 'Es darf keine Lücken zwischen den Ranges geben' },
  overlappedRange: { en: 'Overlapped Range', de: 'Überlappender Bereich' },
  exchangeRates: { en: 'Exchange Rates', de: 'Exchange Rates' },
  dunningSystem: { en: 'Dunning system', de: 'Mahnwesen' },
  conditionOfPayments: { en: 'Conditions of payment', de: 'Zahlungskonditionen' },
  remarks: { en: 'Remarks', de: 'Bemerkungen' },
  debitorCreditorNumber: { en: 'Debtor/Creditor Number', de: 'Debitor/Kreditor Nummer' },
  customersGroup: { en: 'Customers Group', de: 'Kundengruppe' },
  debitorAccoutnNo: { en: 'Debtor Account No.', de: 'Debitor Kontonummer' },
  creditorAccoutnNo: { en: 'Creditor Account No.', de: 'Kreditor Kontonummer' },
  conditions: { en: 'Conditions', de: 'Bedingungen' },
  dunningIsActivated: { en: 'Dunning is activated', de: 'Mahnwesen aktiv' },
  dunningLevelNAfter: { en: 'Dunning Level {LEVEL_INDEX} after', de: 'Mahnstufe {LEVEL_INDEX} nach' },
  collectionProcedureAfter: { en: 'Collection Procedure After', de: 'Abholvorgang danach' },
  lateFee: { en: 'late fee:', de: 'Mahngebühr:' },
  lowerLevelAMustBeLessThanHigherLevel: {
    en: '{LOWER_LEVEL} must be less than {HIGHER_LEVEL}',
    de: '{LOWER_LEVEL} muss kleiner als {HIGHER_LEVEL} sein'
  },
  globalSettings: { en: 'Global Settings', de: 'Globale Einstellungen' },
  valuesMustBeInCorrectOrder: {
    en: 'Values must be in correct order',
    de: 'Werte müssen in richtiger Reihenfolge sein'
  },
  paymentTerms: { en: 'Payment Terms', de: 'Zahlungsbedingungen' },
  discountLevel: { en: 'Discount Level {LEVEL}', de: 'Rabattstufe {LEVEL}' },
  net: { en: 'Net', de: 'Netz' },
  customerNoSupplierSystem: { en: 'Customer No Supplier-System', de: 'KundenNr Lieferanten-System' },
  termsAgreed: { en: 'Terms agreed', de: 'Vereinbarungen' },
  relation: { en: 'Relation', de: 'Beziehung' },
  terms: { en: 'Terms', de: 'Begriffe' },
  percentDeductionAfter: { en: '% - deduction after', de: '% - Abzug danach' },
  discountPercentageMustBeInDescendingOrder: {
    en: 'Discount percentage must be in descending order',
    de: 'Rabattprozentsatz muss in absteigender Reihenfolge sein'
  },
  bookingCode: { en: 'Booking Code', de: 'Vorgangsschlüssel' },
  code: { en: 'Code', de: 'Code' },
  invoiceTextEnglish: { en: 'Invoice Text English', de: 'Rechnungstext (Englisch)' },
  invoiceTextGerman: { en: 'Invoice Text German', de: 'Rechnungstext (Deutsch)' },
  allCurrencies: { en: 'All Currencies', de: 'All Currencies' },
  favorites: { en: 'Favorites', de: 'Favorites' },
  pleaseEnterIbanOrAccountNumber: {
    en: 'Please enter IBAN or account number ',
    de: 'Bitte geben Sie eine IBAN oder Kontonummer an'
  },
  addressTypeDefault: { en: 'Addresstype Default', de: 'Standard für Adresstyp' },
  newDefaultAddressMessage: {
    en: 'You added a new address. Shall it be the new default address for this address type and chosen company?',
    de: 'Sie haben eine neue Adresse hinzugefügt. Soll diese als Standard für den gewählten Adresstyp bei dem Geschäftspartner gelten?',
  },
  changeDefaultAddressMessage: {
    en: 'You set this address as default. Do you want to keep this decision and change this address to be the new default?',
    de: 'Sie haben eine neue Adresse als Standard definiert. Soll dies in dieser Form übernommen werden?',
  },
  deleteDefaultAddressMessage: {
    en: 'Please unset address as default for this address type before deleting.',
    de: 'Bitte definieren Sie eine andere Standard Adresse für diesen Adresstypen.',
  },
  addressDeleteConfirmationMessage: {
    en: 'Do you really want to delete this address?',
    de: 'Möchten Sie die Adresse tatsächlich löschen?',
  },
  untilDate: {
    en: 'Until Date', de: 'Bis Datum',
  },
  userCantBeActivatedWithCurrentEmail: {
    en: 'This user cannot be activated with current email domain. Please use a valid domain to reactivate the user.',
    de: 'Dieser Benutzer kann mit der aktuellen E-Mail Domäne nicht aktiviert werden. Bitte nutzen Sie eine zulässige Domäne um den Benutzer anzulegen.',
  },
  notAcceptedEmailDomain: {
    en: 'Email domain is not accepted by system, User will be saved as “inactive”.',
    de: 'Die Email Domäne ist nicht zulässig, der User wird als “inaktiv” gespeichert.',
  },
  domainHasConnectedInactiveUser: {
    en: 'This domain is still used in inactive users. On deleting this domain, those users will not be able to get reactivated with their current mail addresses.',
    de: 'Diese Domäne ist noch bei inaktiven Users in Verwendung. Das Löschen dieser Domäne wird das Reaktivieren dieser User ohne Anpassung der aktuellen E-mail Adresse nicht zulassen.',
  },
  createUserWithoutEmail: {
    en: 'Create user without email',
    de: 'Benutzer ohne Email erstellen'
  },
  sendPassword: {
    en: 'Send password',
    de: 'Passwort senden',
  },
  resetCurrentPasswordMessage: {
    en: 'Do you want to reset the current password of the user by sending a new initialization mail?',
    de: 'Möchten Sie das aktuelle Passwort des Benutzers zurücksetzen, indem Sie eine neue Initialisierungsmail senden?',
  },
  notAllowedToDeleteDomain: {
    en: 'Not allowed to delete due to existence of active users.',
    de: 'Löschen nicht möglich, da aktive Benutzer für diese Domäne vorhanden sind.',
  },
  notAllowedToEditDomain: {
    en: 'Not allowed to edit due to existence of active users.',
    de: 'Keine Bearbeitung möglich, da aktive Benutzer für diese Domäne vorhanden sind.',
  },
  users: { en: 'Users', de: 'Benutzer' },
  debitorCreditor: { en: 'Debtor / Creditor', de: 'Kunde/Lieferant' },
  companyCategoriesTitle: { en: 'Company Categories', de: 'Geschäftspartnerkategorien' },
  companyGroupsTitle: { en: 'Groups of Company', de: 'Konzerngruppen' },
  category: { en: 'Category', de: 'Kategorie' },
  categories: { en: 'Categories', de: 'Kategorien' },
  categoryEn: { en: 'Category EN', de: 'Kategorie EN' },
  categoryDe: { en: 'Category DE', de: 'Kategorie DE' },
  usedCompanyCategoryDeleteWarning: {
    en: 'A category needs to be set to inactive first in order to be able to delete this category.',
    de: 'Eine Kategorie muss zuerst auf inaktiv gesetzt werden, um sie endgültig zu löschen.',
  },
  companyCategoryDeactivationWarning: {
    en: 'You have set this category to inactive. The connection between this category and all linked companies will be deleted. Do you really want to save your changes and set this category to inactive?',
    de: 'Sie haben die Kategorie deaktiviert. Die Verbindung zwischen dieser Kategorie und allen verknüpften Geschäftsbeziehungen wird entfernt. Wollen Sie Ihre Änderungen speichern und die Kategorie auf inaktiv setzen?',
  },
  saveAndDeactivateCategory: {
    en: 'Save and set this category to inactive',
    de: 'Änderung speichern und Kategorie inaktiv setzen',
  },
  legal: { en: 'Legal', de: 'Rechtliches' },
  legalEntity: { en: 'Legal Entity', de: 'Gesellschaft' },
  representatives: { en: 'Representatives', de: 'Vertretungsberechtigte' },
  shareholdersPartners: { en: 'Shareholders/Partners', de: 'Gesellschafter' },
  trustors: { en: 'Trustors', de: 'Treugeber' },
  subParticipants: { en: 'Sub-Participants ', de: 'Unterbeteiligte' },
  silentPartners: { en: 'Silent Partners', de: 'Stille Gesellschafter' },
  documents: { en: 'Documents', de: 'Dokumente' },
  history: { en: 'History', de: 'Historie' },
  companyLegalFormKG: { en: 'KG (Limited Partnership)', de: 'Kommanditgesellschaft (KG)' },
  companyLegalFormGmbH: { en: 'GmbH (Company with Limited Liability)', de: 'Gesellschaft mit beschränkter Haftung (GmbH)' },
  companyLegalFormAG: { en: 'AG (Stock Corporation)', de: 'Aktiengesellschaft (AG)' },
  companyLegalFormGbR: { en: 'GbR (Civil Law Partnership)', de: 'Gesellschaft bürgerlichen Rechts (GbR)' },
  companyLegalFormForeignCompany: { en: 'Auslandsgesellschaft (Foreign Company)', de: 'Auslandsgesellschaft' },
  companyLegalFormTypeOneShipLimitedPartnership: { en: 'One-Ship Limited Partnership', de: 'Einschiffsgesellschaft' },
  companyLegalFormTypeMultishipLimitedPartnership: { en: 'Multi-Ship Limited Partnership', de: 'Mehrschiffsgesellschaft' },
  companyLegalFormTypeOtherLimitedPartnership: { en: '(Other) Limited Partnership', de: '(Sonstige) Kommanditgesellschaft' },
  companyLegalFormTypeGeneralPartnerCompany: { en: 'Komplementär-GmbH (General Partner Company)', de: 'Komplementär-GmbH' },
  companyLegalFormTypeOtherCompany: { en: '(Other) Company', de: '(Sonstige) GmbH' },
  legalFormType: { en: 'Legal Form Type', de: 'Rechtsformtyp' },
  legalDepartmentReponsibleForLegalData: { en: 'Legal department responsible for legal data', de: 'Rechtsabteilung für die rechtliche Datenverwaltung zuständig' },
  legalFormIsMissing: { en: 'Legal form is missing', de: 'Die Rechtsform fehlt' },
  commercialRegister: { en: 'Commercial Register', de: 'Handelsregistereintrag' },
  businessName: { en: 'Business Name', de: 'Firma der Gesellschaft' },
  businessName_v2: { en: 'Business Name', de: 'Name der Gesellschaft' },
  generalPartner: { en: 'General Partner', de: 'Komplementär' },
  limitedPartner: { en: 'Limited Partner', de: 'Kommanditist' },
  commercialRegisterPlace: { en: 'Commercial Register (Place)', de: 'Handelsregister (Ort)' },
  dateOfEntry: { en: 'Date of Entry', de: 'Eintragungsdatum ' },
  sumOfContributions: { en: 'Sum of Contributions', de: 'Summe Pflichteinlagen' },
  currency: { en: 'Currency', de: 'Währung' },
  classification: { en: 'Classification', de: 'Zuordnung' },
  groupOfCompany: { en: 'Group of Company', de: 'Konzerngruppe' },
  shellCompany: { en: 'Shell Company', de: 'Vorrats-/Mantelgesellschaft' },
  unitaryLimitedPartnership: { en: 'Unitary Limited Partnership', de: 'Einheitsgesellschaft' },
  trustorSubParticipantSilentPartner: { en: 'Trustor/ Sub-Participant/ Silent Partner', de: 'Treugeber/ Unterbeteiligter/ Stiller Gesellschafter' },
  trustor: { en: 'Trustor', de: 'Treugeber' },
  subParticipant: { en: 'Sub-Participant', de: 'Unterbeteiligter' },
  silentPartner: { en: 'Silent Partner', de: 'Stiller Gesellschafter' },
  preliminaryInsolvencyProceedings: { en: 'Preliminary Insolvency Proceedings', de: 'Vorläufiges Insolvenzverfahren' },
  insolvencyProceedings: { en: 'Insolvency Proceedings', de: 'Insolvenzverfahren' },
  inLiquidation: { en: 'In Liquidation', de: 'In Liquidation' },
  deleted: { en: 'Deleted', de: 'Erloschen' },
  sold: { en: 'Sold', de: 'Verkauft' },
  start: { en: 'Start', de: 'Beginn' },
  preliminaryInsolvencyAdministrator: { en: 'Preliminary Insolvency Administrator', de: '(Vorläufiger) Insolvenzverwalter' },
  liquidator: { en: 'Liquidator', de: 'Liquidator' },
  buyer: { en: 'Buyer', de: 'Käufer' },
  managingPartners: { en: 'Managing Partner', de: 'Geschäftsführender Gesellschafter' },
  defineManagingPartnerManually: { en: 'Define Managing Partner manually', de: 'Geschäftsführenden Gesellschafter anlegen' },
  defineGeneralPartnerManually: { en: 'Define General Partner manually', de: 'Komplementär anlegen' },
  businessPartner: { en: 'Business Partner', de: 'Geschäftsbeziehung' },
  privatePerson: { en: 'Private Person', de: 'Privatperson' },
  company: { en: 'Company', de: 'Firma' },
  managingPartnersBusinessName: { en: 'Business Name', de: 'Firmenname' },
  hra: { en: 'HRA', de: 'HRA' },
  hrb: { en: 'HRB', de: 'HRB' },
  businessPartnerDeletionWarning: {
    en: 'Your input of the business partner will be deleted. Do you really want to save your changes?',
    de: 'Ihre Eingaben zu dem Geschäftspartner werden gelöscht. Wollen Sie Ihre Änderung speichern?',
  },
  remark: { en: 'Remark', de: 'Bemerkung' },
  other: { en: 'Other', de: 'Sonstige' },
  shareCapital: { en: 'Share Capital', de: 'Grundkapital' },
  shareCapital_v2: { en: 'Share Capital', de: 'Stammkapital' },
  shareCapital_v3: { en: 'Share Capital', de: 'Gesellschaftskapital' },
  shareCapital_v4: { en: 'Share Capital', de: 'Einlagen' },
  stockType: { en: 'Type of Stock', de: 'Aktienform' },
  parValueShare: { en: 'Par Value Share', de: 'Nennbetragsaktie' },
  nonParValueShare: { en: 'Non-Par Value Share', de: 'Stückaktie' },
  sumOfNonParValueShares: { en: 'Sum of Non-Par Value Shares', de: 'Summe der Stückaktien' },
  sumOfNonParValueShares_v2: { en: 'Sum of Non-Par Value Shares', de: 'Anzahl Stückaktien ' },
  companyNumber: { en: 'Company Number', de: 'Registrierungsnummer' },
  dormantCompany: { en: 'Dormant Company', de: 'Vorratsgesellschaft' },
  sumOfContributions_v2: { en: 'Sum of Contributions', de: 'Summe Einlagen' },
  show: { en: 'Show', de: 'Anzeigen' },
  from: { en: 'From', de: 'Ab' },
  shareholderIncrease: { en: 'Shareholder (increase)', de: 'Gesellschafter (Einlage)' },
  shareholderIncrease_v2: { en: 'Shareholder (increase)', de: 'Aktionär (Einlage)' },
  shareholderReduce: { en: 'Shareholder (reduce)', de: 'Gesellschafter (Entnahme)' },
  shareholderReduce_v2: { en: 'Shareholder (reduce)', de: 'Aktionär (Entnahme)' },
  shareholder: { en: 'Shareholder', de: 'Gesellschafter' },
  shareholder_v2: { en: 'Shareholder', de: 'Aktionär' },
  partnerIncrease: { en: 'Partner (increase)', de: 'Gesellschafter (Einlage)' },
  partnerReduce: { en: 'Partner (reduce)', de: 'Gesellschafter (Entnahme)' },
  partner: { en: 'Partner', de: 'Gesellschafter' },
  defineShareholderManually: { en: 'Define Shareholder manually', de: 'Gesellschafter anlegen' },
  defineShareholderManually_v2: { en: 'Define Shareholder manually', de: 'Aktionär anlegen' },
  definePartnerManually: { en: 'Define partner manually', de: 'Gesellschafter anlegen' },
  parValueShare_v2: { en: 'Par Value Share', de: 'Nennbetrag Gesellschaftsanteil' },
  parValueShareNew_v2: { en: 'Par Value Share (new)', de: 'Nennbetrag Gesellschaftsanteil (neu)' },
  nominalValue: { en: 'Nominal Value', de: 'Nennbetrag' },
  numberOfNonParValueShares: { en: 'Number of Non-Par Value Shares', de: 'Anzahl der Stückaktien' },
  nominalValueNew: { en: 'Nominal Value (new)', de: 'Nennbetrag (neu)' },
  numberOfNonParValueSharesNew: { en: 'Number of Non-Par Value Shares (new)', de: 'Anzahl der Stückaktien (neu)' },
  contributionAmount: { en: 'Contribution amount', de: 'Pflichteinlage' },
  contributionAmount_v2: { en: 'Contribution amount', de: 'Einlage' },
  contributionAmountNew: { en: 'Contribution amount (new)', de: 'Pflichteinlage (neu)' },
  contributionAmountNew_v2: { en: 'Contribution amount (new)', de: 'Einlage (neu)' },
  parValueShareChange: { en: 'Par Value Share Change', de: 'Veränderung Nennbetrag Gesellschaftsanteil' },
  contributionChangeAmount: { en: 'Contribution change amount', de: 'Veränderung Einlage' },
  nominalValueChange: { en: 'Nominal Value Change', de: 'Veränderung Nennbetrag' },
  numberOfNonParValueSharesChange: { en: 'Number of Non-Par Value Shares Change', de: 'Veränderung Anzahl der Stückaktien' },
  sumNonParValueShares: { en: 'Sum (Non-Par Value Shares)', de: 'Summe (der Stückaktien)' },
  partners: { en: 'Partners', de: 'Gesellschafter' },
  showHistoricalData: { en: 'Show historical data', de: 'Historie anzeigen' },
  contribution_v1: { en: 'Contribution', de: 'Einlage' },
  percentage: { en: 'Percentage', de: 'Prozent' },
  sumContributions_v1: { en: 'Sum (Contributions)', de: 'Summe (Einlagen)' },
  sumContributions_v2: { en: 'Sum (Contributions)', de: 'Summe (Pflichteinlagen)' },
  sumShareCapital_v1: { en: 'Sum (Share Capital)', de: 'Summe (Gesellschaftskapital)' },
  sumShareCapital_v2: { en: 'Sum (Share Capital)', de: 'Summe (Grundkapital)' },
  managePartnersParticipation: { en: 'Manage participation of partners', de: 'Beteiligung der Gesellschafter verwalten' },
  manageShareholdersParticipation: { en: 'Manage participation of shareholders', de: 'Beteiligung der Aktionäre verwalten' },
  manageShareholdersParticipation_v2: { en: 'Manage participation of shareholders', de: 'Beteiligung der Gesellschafter verwalten' },
  shareholders_v1: { en: 'Shareholders', de: 'Gesellschafter' },
  shareholders_v2: { en: 'Shareholders', de: 'Aktionäre' },
  contribution_v2: { en: 'Contribution', de: 'Pflichteinlage' },
  contribution_v3: { en: 'Contribution', de: 'Einlagen' },
  contribution_v4: { en: 'Contribution', de: 'Pflichteinlagen' },
  typeOfStock: { en: 'Type of Stock', de: 'Aktienform' },
  liabilitySum: { en: 'Liability Sum', de: 'Haftsumme' },
  typePartner: { en: 'Type Partner', de: 'Typ Gesellschafter' },
  managingDirectorGeneralPartner: {
    en: 'Managing Director General Partner',
    de: 'Geschäftsführer phG',
  },
  managingPartner: {
    en: 'Managing Partner',
    de: 'Geschäftsführender Gesellschafter'
  },
  authorizedLimitedPartnership: {
    en: 'Authorized Representative Limited Partnership',
    de: 'Prokurist KG'
  },
  authorizedGeneralPartnership: {
    en: 'Authorized Representative General Partner',
    de: 'Prokurist phG'
  },
  insolvencyAdministrator: {
    en: 'Insolvency Administrator',
    de: 'Insolvenzverwalter'
  },
  managingDirector: {
    en: 'Managing Director',
    de: 'Geschäftsführer'
  },
  authorizedRepresentative: {
    en: 'Authorized Representative',
    de: 'Prokurist'
  },
  memberOfManagementBoard: {
    en: 'Member of the Management Board',
    de: 'Vorstandsmitglied'
  },
  memberOfSupervisoryBoard: {
    en: 'Member of the Supervisory Board',
    de: 'Aufsichtsratsmitglied'
  },
  managingDirectorManagingPartner: {
    en: 'Managing Director Managing Partner',
    de: 'Geschäftsführer geschäftsf. Gesellschafter'
  },
  authorizedManagingPartner: {
    en: 'Authorized Representative Managing Partner',
    de: 'Prokurist geschäftsf. Gesellschafter'
  },
  director: {
    en: 'Director',
    de: 'Director'
  },
  companySecretary: {
    en: 'Company Secretary',
    de: 'Company Secretary'
  },
  solePowerOfRepresentation: {
    en: 'Sole Power of Representation',
    de: 'Einzelvertretungsberechtigung'
  },
  jointPowerOfRepresentation: {
    en: 'Joint Power of Representation',
    de: 'Gesamtvertretungsberechtigung'
  },
  representative: {
    en: 'representative',
    de: 'den Vertretungsberechtigten',
  },
  function: {
    en: 'Function',
    de: 'Funktion'
  },
  scopeOfRepresentation: {
    en: 'Scope of Representation',
    de: 'Vertretungsumfang'
  },
  releaseSec181_1: {
    en: 'Release sec. 181 1. alt. Civil Code',
    de: 'Befreiung § 181 1. Alt. BGB'
  },
  releaseSec181_2: {
    en: 'Release sec. 181 2. alt. Civil Code',
    de: 'Befreiung § 181 2. Alt. BGB'
  },
  managingPartnersAndRepresentatives: {
    en: 'Managing Partners and Authorized Representatives',
    de: 'Geschäftsführer und Prokuristen'
  },
  membersOfManagementBoard: {
    en: 'Members of the Management Board',
    de: 'Vorstandsmitglieder'
  },
  directorsAndSecretaries: {
    en: 'Directors and Secretaries',
    de: 'Vertretungsberechtigte'
  },
  areYouSureYouWantToDeleteChange: {
    en: 'Are you sure you want to delete this change?',
    de: 'Möchten Sie diese Änderung tatsächlich löschen?',
  },
  defineManagingPartner: {
    en: 'Define Managing Partner / Authorized Representative manually',
    de: 'Geschäftsführer / Prokurist anlegen'
  },
  managingPartnerAndRepresentative: {
    en: 'Managing Partner and Authorized Representative',
    de: 'Geschäftsführer und Prokurist'
  },
  managingPartnerOrRepresentative: {
    en: 'Managing Partner / Authorized Representative',
    de: 'Geschäftsführer / Prokurist'
  },
  directorAndSecretary: {
    en: 'Director and Secretary',
    de: 'Vertretungsberechtigter'
  },
  directorOrSecretary: {
    en: 'Director / Secretary',
    de: 'Vertretungsberechtigter'
  },
  defineManagingPartnerAndRepresentative: {
    en: 'Define Managing Partner / Authorized Representative',
    de: 'Geschäftsführer / Prokuristen anlegen'
  },
  defineMemberOfManagementBoard: {
    en: 'Define Member of the Management Board',
    de: 'Vorstandsmitglied anlegen'
  },
  defineDirectorAndSecretary: {
    en: 'Define Director / Secretary',
    de: 'Vertretungsberechtigten anlegen'
  },
  contributionDeviationDetected: {
    en: '{contributionTypeName} deviation detected',
    de: 'Abweichung {contributionTypeName} festgestellt'
  },
  contributionDeviationDetected_2: {
    en: 'Contribution deviation detected',
    de: 'Abweichung pflichteinlagen festgestellt'
  },
  shareholdings: { en: 'Shareholdings', de: 'Beteiligungen' },
  selectLegalType: {
    en: 'Please select legal form of company you want to create:',
    de: 'Bitte wählen Sie die Rechtsform der Gesellschaft aus, die Sie anlegen möchten:'
  },
  deleteShareholding: {
    en: 'Do you really want to delete *{subject}* from shareholdings?',
    de: 'Möchten Sie *{subject}* tatsächlich aus den Beteiligungen löschen? '
  },
  defineSilentPartnerManually: {
    en: 'Define Silent Partner manually',
    de: 'Stillen Gesellschafter anlegen'
  },
  atypicalSilentPartner: {
    en: 'Atypical Silent Partner',
    de: 'Atypischer Stiller Gesellschafter',
  },
  dateFromTo: {
    en: 'Date from/to',
    de: 'Zeitraum von/bis',
  },
  atypical: { en: 'Atypical', de: 'Atypisch' },
  typical: { en: 'Typical', de: 'Typisch' },
  areYouSureYouWantToDeleteSilentPartner: {
    en: 'Are you sure you want to delete the silent partner?',
    de: 'Möchten Sie den stillen Gesellschafter wirklich löschen ?'
  },
  shareholderTrustee: {
    en: 'Shareholder/ Trustee',
    de: 'Gesellschafter/ Treuhänder',
  },
  partnerTrustee: {
    en: 'Partner/ Trustee',
    de: 'Gesellschafter/ Treuhänder',
  },
  manageParticipationOfTrustors: {
    en: 'Manage participation of trustors',
    de: 'Beteiligung der Treugeber verwalten',
  },
  manageParticipationOfSubParticipants: {
    en: 'Manage participation of sub-participants',
    de: 'Beteiligung der Unterbeteiligten verwalten',
  },
  increase: {
    en: '(Increase)',
    de: '(Einlage)',
  },
  reduce: {
    en: '(Reduce)',
    de: '(Entnahme)',
  },
  defineTrustorManually: {
    en: 'Define Trustor manually',
    de: 'Define Trustor manually',
  },
  defineSubParticipantManually: {
    en: 'Define Sub-Participant manually',
    de: 'Define Sub-Participant manually',
  },
  shareholderRequired: {
    en: 'Shareholder required',
    de: 'Gesellschafter notwendig',
  },
  legalDataIsNoLongerEditable: {
    en: 'The legal form of this company is a *{subject}.* \nAfter saving the changes, the legal form of this company cannot be edited anymore. Do you really want to save your changes?',
    de: 'Der Gesellschaft ist die Rechtsform *{subject}* hinterlegt. \nNach dem Abspeichern der Änderung lässt sich die Rechtsform nicht mehr ändern. Wollen Sie die Eingabe trotzdem speichern?',
  },
  viewFormerBusinessNames: {
    en: 'View former Business Names',
    de: 'Vorherige Firmenbezeichnungen anzeigen',
  },
  businessNameDeleteWarning: {
    en: 'Do you really want to delete *{subject}* as business name?',
    de: 'Möchten Sie *{subject}* als Firmenbezeichnung der Gesellschaft tatsächlich löschen?',
  },
  changeOfLegalForm: { en: 'Change of legal form', de: 'Formwechsel' },
  formerCompany: { en: 'Former Company', de: 'Vorherige Gesellschaft' },
  subsequentCompany: { en: 'Subsequent Company', de: 'Nachfolgende Gesellschaft' },
  representativesOf: {
    en: 'Representatives of {subject}',
    de: 'Vertretungsberechtigte der {subject}'
  },
  theValueShouldNotBeGreaterDueToExistingContributions: {
    en: 'Value should not be greater than {subject} due to already existing trustors or sub-participants of selected shareholder',
    de: 'Der Wert darf aufgrund von bestehenden Treugebern oder Unterbeteiligten des Gesellschafters nicht größer als {subject} sein',
  },
  theValueShouldBeGreaterThanContributionsSum: {
    en: 'Shareholder contribution should be greater than sum of contributions of corresponding trustors and sub-participants ({subject})',
    de: 'Gesellschaftereinlage muss größer als Einlagesumme der dazugehörigen Treugeber und Unterbeteiligten sein ({subject})',
  },
  partnerWithdrawWithZeroValue: { en: 'Partner withdrawal without contribution', de: 'Gesellschafteraustritt ohne Einlage' },
  partnerEntryWithZeroValue: { en: 'Partner entry without contribution', de: 'Gesellschaftereintritt ohne Einlage' },
  shareholderWithdrawWithZeroValue: { en: 'Shareholder withdrawal without contribution', de: 'Gesellschafteraustritt ohne Einlage' },
  shareholderEntryWithZeroValue: { en: 'Shareholder entry without contribution', de: 'Gesellschaftereintritt ohne Einlage' },
  taskTemplates: { en: 'Task Templates', de: 'Vorlagen Aufgaben' },
  taskCategories: { en: 'Task Categories', de: 'Aufgabenkategorien' },
  noChecklistItemsYet: {
    en: 'This checklist template does not have any checklist items yet.',
    de: 'Dieses Checklistentemplate hat noch keine Einträge.'
  },
  checklistTemplate: { en: 'Checklist Template', de: 'Vorlagen Checklisten' },
  emptyTaskTemplate: {
    en: 'This task template is not associated with any checklists yet. Please select a checklist from the right side and associate it.',
    de: 'Dieses Template wurde noch nicht mit einer Checkliste verknüpft. Bitte wählen Sie eine der Checklisten von der rechten Seite aus und fügen Sie diese dann hinzu.'
  },
  taskChecklistTemplates: { en: 'Task/Checklist Templates', de: 'Vorlagen Aufgaben/ Checklisten' },
  // TODO: Add DE translation
  taskGenerator: { en: 'Task Generator', de: 'Task Generator' },
  checklistItems: { en: 'Checklist Items', de: 'Checklist Items' },
  createCatgory: { en: 'Create Category', de: 'Create Category' },
  name: { en: 'Name', de: 'Name' },
  simpleVesselTask: { en: 'Simple Vessel Task', de: 'Simple Vessel Task' },
  registrationVesselTask: { en: 'Registration Vessel Task', de: 'Registration Vessel Task' },
  vesselTaskType: { en: 'Vessel Task Type', de: 'Vessel Task Type' },
  createTaskTemplate: { en: 'Create Task Template', de: 'Create Task Template' },
  editTaskTemplate: { en: 'Edit Task Template', de: 'Edit Task Template' },
  useStartDate: { en: 'Use Start Date', de: 'Use Start Date' },
  editTask: { en: 'Edit Task', de: 'Edit Task' },
  deleteTask: { en: 'Delete Task', de: 'Delete Task' },
  createChecklist: { en: 'Create Checklist', de: 'Create Checklist' },
  editChecklist: { en: 'Edit Checklist', de: 'Edit Checklist' },
  deleteChecklist: { en: 'Delete Checklist', de: 'Delete Checklist' },
  addTodo: { en: 'Add Todo', de: 'Add Todo' },
  editTodo: { en: 'Edit Todo', de: 'Edit Todo' },
  deleteTodo: { en: 'Delete Todo', de: 'Delete Todo' },
  associateChecklist: { en: 'Associate Checklist', de: 'Associate Checklist' },
  checklistAlreadyAssociated: { en: 'Checklist Already Associated', de: 'Checklist Already Associated' },
  dissociateChecklist: { en: 'Dissociate checklist', de: 'Dissociate checlist' },
  intercompany: { en: 'Intercompany', de: 'Intercompany'},
  intercompanyRelevance: { en: 'Intercompany Relevance', de: 'Intercompany Relevanz'},
  hasNoIntercompanyRelevance: { en: 'Debtor/Creditor has no Intercompany relevance', de: 'Keine Intercompany Relevanz'},
  hasIntercompanyRelevance: { en: 'Intercompany Debtor/Creditor', de: 'Intercompany Relevanz liegt vor'},
  title: { en: 'Title', de: 'Titel'},
  checklist: { en: 'Checklist', de: 'Checklist' },
};
