import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { Observable, forkJoin } from 'rxjs';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/company-categories.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { CategoryRepositoryService } from 'src/app/core/services/repositories/category-repository.service';
import { ICategoryFull, ICategory } from 'src/app/core/models/resources/ICategory';
import { switchMap, map, tap } from 'rxjs/operators';

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
})
export class CompanyCategoriesListViewComponent implements OnInit {
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<ICategoryFull[]>;

  constructor(
    private categoriesRepo: CategoryRepositoryService,
    public gridConfigService: GridConfigService,
    private confirmationService: ConfirmationService,
    private authService: AuthenticationService,
    private router: Router
  ) { }

  public ngOnInit() {
    this.dataSource$ = this.getCategoriesFull();
    const gridActions = {
      add: () => this.router.navigate([`${this.router.url}/new`]),
      edit: (category: ICategory) => this.router.navigate([`${this.router.url}/${category.id}`]),
      delete: this.deleteClicked
    };

    this.gridConfig = getGridConfig(
      gridActions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  private getCategoriesFull(): Observable<ICategoryFull[]> {
    return forkJoin([
      this.categoriesRepo.fetchAll({ queryParams: { withCompanyConnection: true }}),
      this.categoriesRepo.fetchAll({}), // to update a stream$
    ]).pipe(
      switchMap(([usedCategories]: ICategory[][]) => {
        const usedIdsSet = new Set<number>(usedCategories.map(c => c.id));

        return this.categoriesRepo.getStream().pipe(map(allCategories => {
          return allCategories.map<ICategoryFull>(category => ({
            ...category,
            hasConnection: category.isActive && usedIdsSet.has(category.id),
          }));
        }));
      }),
    );
  }

  public deleteClicked = (category: ICategoryFull) => {
    if (category.isSystemDefault && !this.authService.getUser().isSuperAdmin) {
      this.confirmationService.warning({
        title: wording.general.systemTypesCanNotBeDeleted,
        okButtonWording: wording.general.ok,
        cancelButtonWording: null,
      });
      return;
    }

    if (category.hasConnection) {
      this.confirmationService.warning({
        title: wording.system.usedCompanyCategoryDeleteWarning,
        okButtonWording: wording.general.ok,
        cancelButtonWording: null,
      });
      return;
    }

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: category.displayLabel },
    }).subscribe(confirmed => confirmed && this.categoriesRepo.delete(category.id, {}).subscribe());
  }
}
