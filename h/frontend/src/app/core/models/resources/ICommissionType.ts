import { IResource } from './IResource';
import { IBookingCode } from './IBookingCode';
import { IWithLabel } from './IWithLabel';
import { IWording } from './IWording';

export type CommissionTypeKey = number;

export enum COMMISSIONTYPES_GROUP {
  Chartering = 0,
  Broker = 1,
  Address = 2,
  Other = 3,
}

interface ISharedProps {
  bookingCodeId: number;
  code: string;
  definition?: string;
  invoiceTextEn?: string;
  invoiceTextDe?: string;
  isActive?: boolean;
  group: COMMISSIONTYPES_GROUP;
  labelDe?: string;
  labelEn: string;
}

export interface ICommissionTypeBase extends ISharedProps, IResource<number> { }

export interface ICommissionTypeCreate extends ISharedProps { }

export interface ICommissionTypeDTO extends ICommissionTypeBase { }

export interface ICommissionType extends ICommissionTypeDTO, IWithLabel { }

export interface ICommissionTypeFull extends ICommissionType {
  bookingCode: IWording;
}

export interface ICommissionTypeRow extends ICommissionTypeFull {
  bookingCodeName: IWording;
  groupName: IWording;
}
