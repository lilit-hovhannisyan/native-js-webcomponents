import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { wordingAccounting } from 'src/app/core/constants/wording/accounting';
import { wordingGeneral } from 'src/app/core/constants/wording/general';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'banksGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'name',
        title: wordingAccounting.bankName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'code',
        title: wordingAccounting.bankCode,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'bic',
        title: wordingAccounting.bic,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'zipCode',
        title: wordingGeneral.zipCode,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'city',
        title: wordingGeneral.city,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'street',
        title: wordingGeneral.street,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'countryName',
        title: wordingGeneral.country,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'differentAccountHolder',
        title: wordingAccounting.differentAccountHolder,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isActive',
        title: wordingAccounting.active,
        width: 75,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wordingGeneral.edit,
        name: 'editBank',
        tooltip: wordingGeneral.edit,
        actOnDoubleClick: true,
        func: actions.edit,
        hidden: !canPerform(Operations.UPDATE)
      },
      {
        icon: 'delete',
        description: wordingGeneral.delete,
        name: 'deleteBank',
        tooltip: wordingGeneral.delete,
        actOnDoubleClick: false,
        func: actions.delete,
        hidden: !canPerform(Operations.DELETE)
      }
    ],
    toolbarButtons: [
      {
        title: wordingGeneral.create,
        tooltip: wordingGeneral.create,
        icon: 'plus-circle-o',
        func: actions.add,
        hidden: () => !canPerform(Operations.CREATE)
      }
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
