import { Observable, fromEvent, BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { NotificationType } from 'src/app/core/models/INotification';
import { wording } from 'src/app/core/constants/wording/wording';
import { skip } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InternetConnectionService {
  public onlineSubject = new BehaviorSubject<boolean>(true);
  public online$: Observable<boolean> = this.onlineSubject.asObservable();

  constructor(
    private notificationService: NotificationsService,
  ) {
    fromEvent(window, 'online').subscribe(() => this.onlineSubject.next(true));
    fromEvent(window, 'offline').subscribe(() => this.onlineSubject.next(false));
    this.onlineSubject
      .pipe(skip(1))
      .subscribe(isOnline =>
        isOnline
          ? this.notificationService.notify(
              NotificationType.Success,
              wording.general.online,
              wording.general.connectionRetrieved
            )
          : this.notificationService.notify(
              NotificationType.Warning,
              wording.general.offline,
              wording.general.connectionLost
            )
      );
  }

  public get isOnline(): boolean {
    return this.onlineSubject.value;
  }
}
