import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseComponent } from 'src/app/core/abstracts/BaseComponent';
import { MandatorKey, mandatorNoModifier, IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { Observable } from 'rxjs';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { LoadingService } from 'src/app/core/services/loading.service';
import { IInvoiceLedger, InvoiceLedgerKey } from 'src/app/core/models/resources/IInvoiceLedger';
import { getLedgerGridConfig } from 'src/app/core/constants/nv-grid-configs/invoice-ledger.config';
import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { FormBuilder, FormGroup } from '@angular/forms';

export interface IMandatorIdMap {
  [id: number]: boolean;
}

export interface IMandatorFilterView extends IMandatorLookup {
  noView: string;
}

@Component({
  selector: 'nv-mandator-sidebar-filter',
  templateUrl: './sidebar-filter.component.html',
  styleUrls: ['./sidebar-filter.component.scss']
})
export class SidebarFilterComponent extends BaseComponent implements OnInit {
  @Input() public availableMandators: IMandatorLookup[] = [];
  @Input() public invoiceLedger$: Observable<IInvoiceLedger[]>;
  @Output() public mandatorsChecked = new EventEmitter<{
    ids: MandatorKey[];
    invoiceLedgerYears: number[];
  }>();
  public invoiceLedgerGridConfig: NvGridConfig;
  public mandatorAutocompleteOptions: IInvoiceLedger[] = [];
  public invoiceLedgers: IInvoiceLedger[] = [];
  public allSelected: boolean;
  public allCleared: boolean;
  public mandatorSearchInputValue: string;
  public invoiceLedgerYearInputValue: string;
  public autoCompleteSearchInputValue: string;
  public isLoading$: Observable<boolean>;
  public mandatorsList: IMandatorFilterView[] = [];
  public bookingYears: number[] = [];
  public checkedMandators: IMandatorIdMap = {};
  public checkedYears: IMandatorIdMap = {};
  public form: FormGroup;

  private availableMandatorsFilterView: IMandatorFilterView[] = [];
  private allBookingYears: number[] = [];

  constructor(
    bcs: BaseComponentService,
    private loadingService: LoadingService,
    private fb: FormBuilder,
  ) {
    super(bcs);
  }

  public ngOnInit() {
    this.isLoading$ = this.loadingService.isLoading$;
    this.initForm();
    this.invoiceLedgerGridConfig = {
      ...getLedgerGridConfig({
        select: this.invoiceLedgerSelected
      }, () => false),
      rowSelectionType: NvGridRowSelectionType.RadioButton,
    };
    this.invoiceLedger$.subscribe(il => this.invoiceLedgers = il);
    this.availableMandatorsFilterView = this.availableMandators
      .map(mandator => ({ ...mandator, noView: mandatorNoModifier(mandator.no) }));
    this.allBookingYears = this.bookingYears =
      [...new Set(this.invoiceLedgers.map(i => i.year))];
    this.onAllSelected();
    this.onAllBookingYearsChecked();
  }

  private initForm() {
    this.form = this.fb.group({
      invoiceLedgerId: [''],
    });
    this.form.get('invoiceLedgerId').valueChanges.subscribe(this.invoiceLedgerSelected);
  }

  private invoiceLedgerSelected = (invoiceLedgerId: InvoiceLedgerKey) => {
    this.router.navigate([`${this.router.url}/${invoiceLedgerId}`]);
  }

  public onAllSelected() {
    this.availableMandators.forEach(v => this.checkedMandators[v.id] = true);
    this.mandatorsList = this.availableMandatorsFilterView;
    this.onMandatorsChecked();
  }

  public onAllCleared() {
    this.checkedMandators = {};
    this.onMandatorsChecked();
  }

  public onMandatorSeach() {
    if (!this.mandatorSearchInputValue) {
      return this.mandatorsList = this.availableMandatorsFilterView;
    }
    this.mandatorsList = this.availableMandatorsFilterView.filter(m => {
      const usrInput = this.mandatorSearchInputValue.toLowerCase().trim();

      const mandatorName = m.name.toLowerCase();
      const mandatorNo = m.noView;

      return mandatorName.includes(usrInput) || mandatorNo.includes(usrInput);
    });
  }

  public onBookingYearSeach() {
    if (!this.invoiceLedgerYearInputValue) {
      return this.bookingYears = this.allBookingYears;
    }
    this.bookingYears = this.allBookingYears.filter(year => {
      const usrInput = this.invoiceLedgerYearInputValue.toLowerCase().trim();
      return year.toString().includes(usrInput);
    });
  }

  public onMandatorsChecked() {
    const ids: MandatorKey[] = [];
    for (const i in this.checkedMandators) {
      if (this.checkedMandators[i]) {
        ids.push(+i);
      }
    }

    const invoiceLedgerYears: number[] = [];
    for (const i in this.checkedYears) {
      if (this.checkedYears[i]) {
        invoiceLedgerYears.push(+i);
      }
    }
    this.mandatorsChecked.emit({ ids, invoiceLedgerYears });
  }

  public onAllBookingYearsCleared() {
    this.checkedYears = {};
    this.onMandatorsChecked();
  }

  public onAllBookingYearsChecked() {
    this.allBookingYears.map(b => this.checkedYears[b] = true);
    this.onMandatorsChecked();
  }

  public onAutoCompleteSeach() {
    if (!this.autoCompleteSearchInputValue) {
      this.mandatorAutocompleteOptions = [];
    }
    const usrInput = this.autoCompleteSearchInputValue.toLowerCase();
    this.mandatorAutocompleteOptions = this.invoiceLedgers.filter(il => {
      return il.voucherNumber.toString().toLowerCase().includes(usrInput)
        || il.invoiceNumber.toLowerCase().includes(usrInput);
    });
  }

  public onAutoCompleteSelect(selectedValue: string) {
    const foundInvoiceLedgers = this.invoiceLedgers.find(il =>
      `${il.invoiceNumber} ${il.voucherNumber}` === selectedValue);
    if (foundInvoiceLedgers) {
      this.router.navigate([`${this.router.url}/${foundInvoiceLedgers.id}`]);
    }
  }
}
