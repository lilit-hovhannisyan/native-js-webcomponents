import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { IDebitorCreditorRow } from '../../models/resources/IDebitorCreditor';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'debitorCreditorListGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'no',
        title: wording.accounting.no,
        width: 65,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'companyName',
        title: wording.general.name,
        width: 185,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'customersGroup',
        title: wording.system.customersGroup,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isActive',
        title: wording.accounting.active,
        width: 65,
        isSortable: true,
        dataType: NvColumnDataType.Boolean,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: !actions.select,
        func: (row: IDebitorCreditorRow) => actions.edit(row.id),
        hidden: canPerform ? !canPerform(Operations.UPDATE) : true,
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteDebitorCreditor',
        actOnDoubleClick: false,
        func: actions.delete,
        hidden: canPerform ? !canPerform(Operations.DELETE) : true,
      },
      {
        actOnDoubleClick: true,
        func: row => actions.select(row),
        description: wording.autocomplete.selectRow,
        icon: 'select',
        actOnEnter: true,
        hidden: actions.select === undefined,
        tooltip: wording.general.select,
        name: 'select'
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        icon: 'plus-circle-o',
        func: actions.add,
        hidden: () => canPerform ? !canPerform(Operations.CREATE) : true,
      }
    ],
    contextMenuButtons: canPerform && canPerform(Operations.READ) ? getContextMenuButtonsConfig() : [],
  });
};
