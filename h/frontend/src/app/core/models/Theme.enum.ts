export enum Theme {
  dark = 1,
  light,
  auto,
}

export enum ThemeTypes {
  light = 'light',
  dark = 'dark',
}
