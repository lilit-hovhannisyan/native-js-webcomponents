import { IWidgetDTO, IWidgetBase } from './IWidget';
import { GridsterItem } from 'angular-gridster2';

export interface IWeatherWidget extends GridsterItem, IWidgetDTO {
  geoLocation: string;
}

export interface IWeatherWidgetDTO extends IWidgetDTO {
  geoLocation?: string;
}
