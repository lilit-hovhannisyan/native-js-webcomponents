import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { MaterialModel } from '@shared/models/materials/material-model';

@Injectable()
export class MaterialsStoreService {
  // Material Entities
  private readonly _materialEntities = new BehaviorSubject<MaterialModel[]>([]);
  readonly materialEntities$ = this._materialEntities.asObservable();

  private readonly _materialNextId = new BehaviorSubject<number>(0);
  readonly materialNextId$ = this._materialNextId.asObservable();

  get materialEntities(): MaterialModel[] {
    return this._materialEntities.getValue();
  }

  set materialEntities(data: MaterialModel[]) {
    this._materialEntities.next(data);
  }

  appendMaterialEntities(data: MaterialModel[]): void {
    this.materialEntities = [...this.materialEntities, ...data];
  }

  get materialNextId(): number {
    return this._materialNextId.getValue();
  }

  set materialNextId(data: number) {
    this._materialNextId.next(data);
  }

  updateMaterialsState(updatedMaterial: MaterialModel): void {
    const materials: MaterialModel[] = this.materialEntities.map((material) => {
      return material.oid === updatedMaterial.oid ? updatedMaterial : material;
    });
    this._materialEntities.next(materials);
  }

  removeMaterialEntities(materialOIDs: string[]): void {
    const materials = this.materialEntities.filter((item) => !materialOIDs.includes(item.oid));
    this._materialEntities.next(materials);
  }
}
