import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, ViewChild } from '@angular/core';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';
import { RegistrationFleetHelperService } from '../registrations-fleet-helper.service';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { RegistrationVesselTasksMainService } from 'src/app/core/services/mainServices/registration-vessel-tasks-main.service';
import { ActivatedRoute } from '@angular/router';
import { VesselTaskListComponent } from 'src/app/shared/components/vessel-task-components/vessel-task-list/vessel-task-list.component';
import { IVesselTaskFull } from 'src/app/core/models/resources/IVesselTask';
import { TaskType } from 'src/app/core/models/enums/vessel-registration-tasks';
import { combineLatest, ReplaySubject } from 'rxjs';
import { VesselRegisterKey } from 'src/app/core/models/resources/IVesselRegister';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'nv-registration-vessel-tasks',
  templateUrl: './registration-vessel-tasks.component.html',
  styleUrls: ['./registration-vessel-tasks.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegistrationVesselTasksComponent implements OnInit, OnDestroy {
  public ACL_ACTIONS = ACL_ACTIONS;
  public wording = wording;
  public routeBackUrl: string = url(
    sitemap.chartering.children.registrations.children.registrationsFleet
  );
  public taskType = TaskType.RegistrationVesselTask;
  /** Only emits the next value and completes when component destroyed. */
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  /** Vessel id from registration page. */
  public vesselId: VesselKey;
  /** All tasks which `vesselId` matches with `this.vesselId`. */
  public tasksList: IVesselTaskFull[] = [];
  /** Page edit-mode */
  public isEditMode = false;
  /** Selected registration in title-bar. */
  public vesselRegistrationId: VesselRegisterKey;

  @ViewChild(VesselTaskListComponent, {read: VesselTaskListComponent})
  public tasksListComponent: VesselTaskListComponent;

  constructor(
    public helperService: RegistrationFleetHelperService,
    private cdr: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private registrationVesselTaskMainService: RegistrationVesselTasksMainService,
  ) {
    /** In non-edit-mode page header has 3 buttons: back, edit and create. */
    this.helperService.buttonsCount = 3;
  }

  public ngOnInit(): void {
    this.vesselId = +this.activatedRoute.parent.snapshot.params.id;
    this.getResource();
    this.setRegistrationId();
  }

  /** Gets all registration-vessel-tasks by vesselId and selected registerId. */
  private getResource(): void {
    // registration-fleet-detail-view is doing the fetch request
    combineLatest([
      this.registrationVesselTaskMainService.getStream(this.vesselId),
      this.helperService.selectedRegisterSubject,
    ]).subscribe(([registrationVesselTasks, register]) => {
        this.tasksList = registrationVesselTasks
          .filter(t => t.vesselRegistrationId === register.id);

        this.cdr.detectChanges();
      });
  }

  /** Handles registration change. */
  private setRegistrationId(): void {
    this.helperService.selectedRegisterSubject.pipe(
      takeUntil(this.componentDestroyed$),
    ).subscribe(register => this.vesselRegistrationId = register?.id);
  }

  /** Enters page edit-mode. */
  public enterEditMode(): void {
    this.isEditMode = true;
    this.helperService.registersSliderDisabledSubject.next(true);

    /** In edit-mode we have only 2 header-buttons: cancel and save. */
    this.helperService.buttonsCount = 2;
  }

  /** Cancels editing and reverts all changes to initial state of tasks. */
  public cancelEditing(): void {
    this.tasksListComponent.cancelEditing();

    // In non-edit mode we have 3 buttons.
    this.helperService.buttonsCount = 3;
    this.isEditMode = false;
    this.helperService.registersSliderDisabledSubject.next(false);
    this.cdr.detectChanges();
  }

  public submit(): void {
    this.tasksListComponent.submit();
    this.cancelEditing();
  }

  /** Page custom validation to prevent navigation when unsaved-changes exist. */
  public customValidation = (): boolean => !this.tasksListComponent.hasChanges;

  public ngOnDestroy(): void {
    /** Other tabs have 2 buttons in general. */
    this.helperService.buttonsCount = 2;
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
