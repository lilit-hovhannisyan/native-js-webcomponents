import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseWebService } from '../../core/services/base.web-service';

import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { DefinitionPropertiesModel } from '@shared-models/definition-properties/definition-properties.model';
import { DefinitionsModel } from '@shared-models/definitions/definitions.model';
@Injectable()
export class DefinitionsWebService {
  constructor(private baseWebService: BaseWebService) {}

  getAllDefinitions(): Observable<DefinitionsModel> {
    return this.baseWebService.getRequest<DefinitionsModel>(`${MARKETPLACE_BASE_API_URL}/definitions`, DefinitionsModel);
  }

  getDefinitionProperties(): Observable<DefinitionPropertiesModel> {
    return this.baseWebService.getRequest<DefinitionPropertiesModel>(
      `${MARKETPLACE_BASE_API_URL}/definitions/properties`,
      DefinitionPropertiesModel
    );
  }
}
