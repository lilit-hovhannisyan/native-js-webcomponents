import { IResource } from './IResource';
import { VoyageKey } from './IVoyage';

export type OffHireKey = number;

interface ISharedProps {
  from: string;
  until: string;
  factor: number;
  location: string;
  voyageId: VoyageKey;
  isCredit: boolean;
  cause?: string;
  remark?: string;
}

export interface IOffHireCreate extends ISharedProps { }

export interface IOffHireBase extends ISharedProps, IResource<OffHireKey> { }

export interface IOffHireDTO extends IOffHireBase { }

export interface IOffHire extends IOffHireDTO { }

export interface IOffHireFull extends IOffHire { }
