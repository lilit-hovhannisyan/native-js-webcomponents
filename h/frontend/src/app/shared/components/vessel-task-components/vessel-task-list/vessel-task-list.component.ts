import { Component, ChangeDetectionStrategy, Input, HostBinding, Output, EventEmitter, ViewChildren, QueryList, ChangeDetectorRef, OnChanges, SimpleChanges, OnInit, OnDestroy } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { VesselTaskComponent } from '../vessel-task/vessel-task.component';
import { IRegistrationVesselTask, isRegistrationTask, isRegistrationTasksArray } from 'src/app/core/models/resources/IRegistrationVesselTask';
import { IWording } from 'src/app/core/models/resources/IWording';
import { label2Wording } from 'src/app/shared/helpers/general';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { TaskType, VesselTaskStatus } from 'src/app/core/models/enums/vessel-registration-tasks';
import { VesselTaskHelperService } from '../vessel-task/vessel-task-helper.service';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { IVesselTask, IVesselTaskFull, VesselTaskKey } from 'src/app/core/models/resources/IVesselTask';
import { NzModalService } from 'ng-zorro-antd/modal';
import { getVesselTaskModalConfig, ITaskComponentParams } from '../vessel-task/vessel-task.component.config';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { cloneDeep, differenceBy, differenceWith, isEqual } from 'lodash';
import { forkJoin, Observable, of, ReplaySubject } from 'rxjs';
import { RegistrationVesselTasksRepositoryService } from 'src/app/core/services/repositories/registration-vessel-tasks-repository.service';
import { map, switchMap, takeUntil } from 'rxjs/operators';
import { ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';
import { VesselTasksRepositoryService } from 'src/app/core/services/repositories/vessel-tasks-repository.service';
import { VesselRegisterRepositoryService } from 'src/app/core/services/repositories/vessel-registeries-repository.service';
import { VesselRegisterKey } from 'src/app/core/models/resources/IVesselRegister';
import { CountryRepositoryService } from 'src/app/core/services/repositories/country-repository.service';

/**
 * Values are related to pages where the component is used.
 * Values are used also in CSS - be careful while changing.
 */
export type TaskListContext = 'dashboard' | 'registration-fleet';

/** Shortcut type alias. */
type T = IVesselTask;

@Component({
  selector: 'nv-vessel-task-list',
  templateUrl: './vessel-task-list.component.html',
  styleUrls: ['./vessel-task-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VesselTaskListComponent implements OnInit, OnChanges, OnDestroy {
  /** Reference to the project's translation main object. Used in HTML. */
  public wording = wording;
  /** Reference to CRUD-enum. Used in HTML with `nvAccessControl` directive. */
  public ACL_ACTIONS = ACL_ACTIONS;
  /** Only emits the next value and completes when component destroyed. */
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  /** Used only in template to filter not-done tasks. */
  public statusEnum = VesselTaskStatus;
  /** Initial state clone of `value`. */
  private initialValue: T[] = [];
  /** Keeps initial sortIndices of tasks. */
  private initialSortIndices: number[] = [];
  /** Needed for new indexes. */
  public allTasksLength = 0;
  /**
   * Both properties are used to fold the dragged component
   * when dragging starts and unfold while finished.
   */
  public draggedComponent: VesselTaskComponent;
  /** Keeps dragged component's opened state before dragging. */
  public dragComponentOpenedState = false;

  /** If `true` - fetches necessary resources for tasks. */
  @Input() public fetchResources = true;
  /** If `true` - fetches necessary resources for each tasks. */
  @Input() public fetchResourcesInEachTask = false;
  /** Passed tasks value. */
  @Input() public value: T[] = [];
  /**
   * While
   * * true: all tasks are shown
   * * false: only not-done tasks are shown
   * Can be set the initial state.
   */
  @Input() public showDoneTasksValue = false;
  /** Sets tasks `editable` input. */
  @Input() public isEditable = false;
  /** Stretches all task components and its todos to 100% width. */
  @Input() public stretchTasksFully = false;
  /** Disables vessel field in all tasks. */
  @Input() public disableVesselField = false;
  /** Hides edit and add buttons. */
  @Input() public hideActionsButtons = false;
  /** Task type for all tasks in the list. */
  @Input() public taskType: TaskType;
  /** `vesselId` for all tasks in the list. */
  @Input() public vesselId?: VesselKey;
  /** Current selected register id.*/
  @Input() public vesselRegistrationId?: VesselRegisterKey;
  /** Adds corresponding page-related context-attribute to component. */
  @HostBinding('attr.context')
  @Input() public context: TaskListContext;

  /** Emits all value changes. */
  @Output() private valueChange: EventEmitter<T[]> = new EventEmitter();
  /** Emits when leaveEditMode is called. */
  @Output() private leave: EventEmitter<void> = new EventEmitter();
  /** Emits when cancelEditing is called. */
  @Output() private cancel: EventEmitter<void> = new EventEmitter();
  /** Emits when task is deleted. */
  @Output() public delete: EventEmitter<T> = new EventEmitter();

  @ViewChildren(VesselTaskComponent, {read: VesselTaskComponent})
  private tasksComponents: QueryList<VesselTaskComponent>;

  /** Returns true if initialValue and value aren't identical. */
  public get hasChanges(): boolean {
    return !isEqual(this.value, this.initialValue);
  }

  constructor (
    private cdr: ChangeDetectorRef,
    private confirmationService: ConfirmationService,
    private helperService: VesselTaskHelperService,
    private vesselMainService: VesselMainService,
    private modalService: NzModalService,
    private registrationVesselTasksRepo: RegistrationVesselTasksRepositoryService,
    private vesselTaskRepo: VesselTasksRepositoryService,
    private vesselRegisterRepo: VesselRegisterRepositoryService,
    private countryRepo: CountryRepositoryService,
  ) {}

  public ngOnInit(): void {
    if (this.fetchResources) {
      this.fetchTasksResources().subscribe();
    }
    this.handleAllTasksLengthChange();
  }

  public ngOnChanges({ isEditable, value, context }: SimpleChanges): void {
    if (!isEditable?.firstChange) {
      // Fold(close) task when `editable` changes.
      this.setAllTasksOpenState(false);
    }
    if (value) {
      this.initValue();
    }
    if (context) {
      this.setContextRelatedConfigs();
    }
  }

  /** Sets some configuring properties if context exists. */
  private setContextRelatedConfigs(): void {
    this.taskType = this.taskType || this.getTypeByContext();

    switch (this.context) {
      case 'registration-fleet':
        this.hideActionsButtons = true;
        this.disableVesselField = true;
        break;
      case 'dashboard':
      default:
    }
  }

  /** Returns corresponding TaskType for given(passed) context. */
  private getTypeByContext(): TaskType {
    switch (this.context) {
      case 'registration-fleet':
      case 'dashboard':
        return TaskType.RegistrationVesselTask;
      default:
        return TaskType.SimpleVesselTask;
    }
  }

  /** Initializes the initial values. */
  private initValue(): void {
    if (isRegistrationTasksArray(this.value)) {
      this.value = this.value.sort((a, b) => a.sortIndex - b.sortIndex);
    }
    if (isRegistrationTasksArray(this.value)) {
      this.initialSortIndices = this.value.map(task => task.sortIndex);
    }
    this.initialValue = cloneDeep(this.value);
  }

  /** Fetches template resources for tasks. */
  private fetchTasksResources(): Observable<void> {
    /** Fetches data to be used with getStream. */
    return forkJoin([
      this.vesselRegisterRepo.fetchAll({}),
      this.countryRepo.fetchAll({}),
      this.helperService.fetchTemplatesResources(),
      this.vesselMainService.fetchVesselsWithClosestAccounting(true),
    ]).pipe(map(() => void(0)));
  }

  /** Subscribes to all tasks stream. */
  private handleAllTasksLengthChange(): void {
    this.registrationVesselTasksRepo.getStream().pipe(
      takeUntil(this.componentDestroyed$),
    ).subscribe(allTasks => this.allTasksLength = allTasks?.length || 0);
  }

  /** Changes disabled state of all tasks. */
  @HostBinding('attr.disabled')
  @Input('disabled')
  public setDisabledState(isDisabled?: boolean): void {
    this.tasksComponents?.forEach(task => task.setDisabledState(isDisabled));
  }

  /**
   * Sets opened state of dragged component, moves item in tasks-array and
   * scrolls dragged item into view by `scrollIntoView`.
   */
  public dropHandler(event: CdkDragDrop<IVesselTaskFull[]>) {
    this.draggedComponent.setOpenedState(this.dragComponentOpenedState);

    moveItemInArray(this.value, event.previousIndex, event.currentIndex);
    this.setTasksSortIndexes();
    this.valueChange.emit(this.value);

    this.draggedComponent.scrollIntoView();
    this.draggedComponent = null;
    this.cdr.detectChanges();
  }

  /**
   * Keeps dragged component into `draggedComponent`, its opened state into
   * `dragComponentOpenedState` and closes(folds) the task.
   */
  public dragStartHandler(taskComponent: VesselTaskComponent): void {
    this.draggedComponent = taskComponent;
    this.dragComponentOpenedState = this.draggedComponent.isOpened;
    this.draggedComponent.isOpened = false;
    this.cdr.detectChanges();
  }

  /** Validates all tasks and opens invalid tasks. */
  public validateAllTasks(): void {
    this.tasksComponents?.forEach(component => {
      component.validate();
      component.setOpenedState(component.isInvalid());
    });
  }

  /** Shows confirmation modal and after confirmation deletes the task. */
  public handleDelete(index: number, item: IVesselTaskFull): void {
    const subject: IWording = item.id
      ? label2Wording(item)
      : null;

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.chartering.areYouSureYouWantToDeleteTask,
      subjects: { subject },
    }).subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      const deletedItem: T = this.value.splice(index, 1)[0];
      this.setTasksSortIndexes();

      this.delete.emit(deletedItem);
      this.valueChange.emit(this.value);
      this.cdr.detectChanges();
    });
  }

  /** Scrolls task into view if it opens. */
  public handleOpen(taskComponent: VesselTaskComponent): void {
    if (!taskComponent.isOpened) {
      return;
    }
    taskComponent.scrollIntoView();
  }

  /**
   * Returns `false` only when all tasks components
   * `isInvalid()` methods returned values are `false`.
   */
  public isInvalid(): boolean {
    return this.tasksComponents.some(component => component.isInvalid());
  }

  /**
   * Opens/Closes all the tasks.
   * Can be used in parent components.
   */
  public setAllTasksOpenState(isOpened: boolean): void {
    this.tasksComponents?.forEach(task => task.setOpenedState(isOpened, false));
  }

  /**
   * Tracker(trackBy) for tasks ngFor.
   * Used in template only.
   *
   * Returns the id of editable item or the custom generated `uniqueId`
   * which is uid.
   */
  public identify = (i: number, item: IVesselTaskFull): string => {
    return  item.id ? `${item.id}` : item.uniqueId;
  }

  /** Returns task component which have id is equal to `taskId`. */
  public getTaskById(taskId: VesselTaskKey): VesselTaskComponent {
    return this.tasksComponents.find(t => t.value?.id === taskId);
  }
  /** Returns task component which index is equal to  passed index in current orders. */
  public getTaskByIndex(index: number): VesselTaskComponent {
    return this.tasksComponents.toArray()[index];
  }
  /** Returns task component which `@Input() value` equals to passed argument `value`. */
  public getTaskByValue(value: T): VesselTaskComponent {
    return this.tasksComponents.find(t => t.value === value);
  }

  /**
   * Returns all changed/added tasks compared with
   * tasks initial state(`this.initialTasks`).
   */
  public getTasksDifference(): T[] {
    /**
     * All properties from `allTasks` in which values are `nil`
     * should be deleted because `initialTasks` doesn't have
     * those properties initially from fetched data.
     */
    return differenceWith(
      this.value,
      this.initialValue,
      (fromAll, fromInitial) => isEqual(fromAll, fromInitial),
    );
  }

  /** Enters tasks-list edit-mode. */
  public enterEditMode(): void {
    this.isEditable = true;
    this.setAllTasksOpenState(false);
  }

  /** Cancels editing and reverts all changes to initial state of tasks. */
  public cancelEditing(): void {
    this.value = cloneDeep(this.initialValue);
    this.leaveEditMode();
    this.cancel.emit();
  }

  /** Leaves edit mode without reverting data changes. */
  public leaveEditMode(): void {
    this.isEditable = false;
    this.setAllTasksOpenState(false);
    this.cdr.detectChanges();
    this.leave.emit();
  }

  /** Returns an empty-task object (giving consideration to task-type). */
  private getNewTaskInitialValue(type: TaskType): Partial<IVesselTask> {
    const registrationTask: Partial<IRegistrationVesselTask> = {
      type,
      vesselId: this.vesselId ,
      sortIndex: this.allTasksLength || 0,
      vesselRegistrationId: this.vesselRegistrationId,
    };

    if (isRegistrationTask(registrationTask)) {
      return registrationTask;
    }

    return { vesselId: this.vesselId, type };
  }

  /**
   * Opens task detail-view modal.
   * Can be used for creation and edition.
   * If `config.value` doesn't exist - then it's creation.
   */
  public openDetailViewModal(config: Partial<ITaskComponentParams> = {}): void {
    type TaskValueType = IVesselTask;

    const taskType: TaskType = config.type || this.taskType;
    const value: Partial<TaskValueType> = this.getNewTaskInitialValue(taskType);

    this.modalService.create(getVesselTaskModalConfig<IVesselTask>({
      type: taskType,
      fetchResources: false,
      disableVesselField: this.disableVesselField,
      ...config,
      value: {
        ...value,
        ...config.value,
      }
    }));
  }

  /** Sets tasks final indices for ordering/sorting. */
  private setTasksSortIndexes(): void {
    if (!isRegistrationTasksArray(this.value)) {
      return;
    }

    this.value.forEach((task, index) => {
      /**
       * In different pages count of tasks are different but in BE-side
       * there are in one place and have one set of sort indexes.
       * If in current page this.value is the whole set of tasks, then we need to set
       * their indexes(from array) as their sortIndex-es which will override all
       * sortIndexes and set them correct values.
       * Otherwise when this.value is not the whole list, then we need to set
       * sortIndex only from initial tasks list's sortIndexes to not have 2 tasks
       * with same sortIndex.
       */
      task.sortIndex = this.allTasksLength === this.value.length
        ? index
        : this.initialSortIndices[index];
    });
  }

  /**
   * Returns corresponding delete request for any task(with
   * type T where T extends IVesselTask).
   */
  private task2DeleteRequest(task: IVesselTask): Observable<void> {
    switch (task.type) {
      case TaskType.RegistrationVesselTask:
        return this.registrationVesselTasksRepo.delete(task.id, {});
      case TaskType.SimpleVesselTask:
      default:
        return this.vesselTaskRepo.delete(task.id, {});
    }
  }

  /** Returns corresponding update request for any task using type-guard. */
  private task2UpdateRequest(task: IVesselTask): Observable<IVesselTask> {
    if (isRegistrationTask(task)) {
      return this.registrationVesselTasksRepo.update(task, {});
    }

    return this.vesselTaskRepo.update(task, {});
  }

  /** Submits if everything is valid. */
  public submit(): void {
    this.validateAllTasks();
    if (this.isInvalid()) {
      return;
    }

    this.setTasksSortIndexes();

    const changedTasks: T[] = this.getTasksDifference();
    const deletedTasks: T[]
      = differenceBy(this.initialValue, this.value, item => item.id);

    if (!(changedTasks.length || deletedTasks.length)) {
      return this.cancelEditing();
    }

    const deleteRequests$: Observable<void>[] | Observable<void> = !deletedTasks?.length
      ? of(null)
      : deletedTasks.map(t => this.task2DeleteRequest(t));

    const updateRequests$: Observable<IVesselTask>[] = changedTasks
      .map(t => this.task2UpdateRequest(t));

    /**
     * Update requests should not be sent if a single delete request fails.
     * Otherwise `sortIndex`es of tasks will be mixed-up.
     */
    forkJoin(deleteRequests$).pipe(
      switchMap(() => forkJoin(updateRequests$))
    ).subscribe(() => this.leaveEditMode());
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
