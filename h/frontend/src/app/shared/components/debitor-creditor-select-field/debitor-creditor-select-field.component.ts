import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { makeProvider, AbstractValueAccessor } from 'src/app/core/abstracts/value-accessor';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/debitors-creditors.config';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { IDebitorCreditorFull, DebitorCreditorKey } from 'src/app/core/models/resources/IDebitorCreditor';
import { DebitorCreditorMainService } from 'src/app/core/services/mainServices/debitor-creditor-main.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'nv-debitor-creditor-select-field',
  templateUrl: './debitor-creditor-select-field.component.html',
  styleUrls: ['./debitor-creditor-select-field.component.scss'],
  providers: [makeProvider(DebitorCreditorSelectFieldComponent)],
})
export class DebitorCreditorSelectFieldComponent extends AbstractValueAccessor implements OnInit {

  public getGridConfig = getGridConfig;
  public resourceFormControl = new FormControl({ value: null });
  public debitorCreditor$: Observable<IDebitorCreditorFull[]>;
  private debitorsCreditors: IDebitorCreditorFull[] = [];

  @Input() public queryParams = {};
  // Default id that we always want to include
  @Input() public defaultId: DebitorCreditorKey;

  @Output() public change = new EventEmitter<IDebitorCreditorFull>(null);

  constructor(
    private debitorCreditorService: DebitorCreditorMainService,
  ) {
    super();
  }

  public ngOnInit(): void {
    this.debitorCreditor$ = this.debitorCreditorService
      .fetchAllDebitorsCreditorsRows([this.defaultId])
        .pipe(tap(dcs => this.debitorsCreditors = dcs));
  }

  public getLabel = (debitorCreditor: IDebitorCreditorFull) =>
    `${debitorCreditor.no} ${debitorCreditor.company.companyName}`

  public onSelect(value: DebitorCreditorKey): void {
    this.onDebitorCreditorSelect(value);
    this.onChange(value);
  }

  public writeValue(value: DebitorCreditorKey): void {
    this.onDebitorCreditorSelect(value);
    this.resourceFormControl.setValue(value);
  }

  public setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.resourceFormControl.disable();
    } else {
      this.resourceFormControl.enable();
    }
  }

  private onDebitorCreditorSelect(id: DebitorCreditorKey) {
    const debitorCreditor = this.debitorsCreditors.find(dc => dc.id === id);
    this.change.emit(debitorCreditor);
  }

}
