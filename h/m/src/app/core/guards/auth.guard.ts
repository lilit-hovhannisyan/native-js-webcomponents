import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { AuthStoreService } from '@core-services/auth-store.service';
import { UserWebService } from '@core-services/user.web-service';

import { UserProfileModel } from '@shared/models/user-profile/user-profile.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  redirectToNotAuthorizedPage = 'auth';

  constructor(private authStoreService: AuthStoreService, private userWebService: UserWebService, private router: Router) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authStoreService.user && this.authStoreService.company) {
      return true;
    } else if (sessionStorage.getItem('loggedIn')) {
      sessionStorage.removeItem('loggedIn');

      return this.userWebService.getCurrentUser().pipe(
        map((response: UserProfileModel) => {
          if (response) {
            this.authStoreService.user = response.user ? response.user : null;
            this.authStoreService.company = response.company ? response.company : null;
            this.authStoreService.permissions = response.permissions ? response.permissions : null;
            this.authStoreService.language = response.user.attributes.language;
            console.log('LANG SET: ', response.user.attributes.language);
            return true;
          } else {
            this.router.navigate([this.redirectToNotAuthorizedPage]);
            return false;
          }
        }),
        catchError(() => {
          this.router.navigate([this.redirectToNotAuthorizedPage]);
          return of(false);
        })
      );
    } else {
      this.router.navigate([this.redirectToNotAuthorizedPage]);
      return false;
    }
  }
}
