import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild, OnDestroy, Injector } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Subscription } from 'rxjs';

import { PatternValidator } from '@core-validators/custom-validators';

import { BaseLoggerComponent } from '@features/tracking-system';

import { MePopupAccountI } from '@shared/me-components/me-popup-account/me-popup-account.interface';
import { MePopupAccountService } from '@shared/me-components/me-popup-account/me-popup-account.service';
import { AccountModel } from '@shared/models/account/account.model';

import { AuthWebService } from '../auth.web-service';

@Component({
  selector: 'me-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss']
})
export class CreateAccountComponent extends BaseLoggerComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('popupAccountForm', { read: TemplateRef }) popupAccountFormRef!: TemplateRef<Component>;
  @ViewChild('popupLegalForm', { read: TemplateRef }) legalFormRef!: TemplateRef<Component>;
  @ViewChild('popupCancel', { read: TemplateRef }) popupCancelRef!: TemplateRef<Component>;
  accountForm!: FormGroup;
  legalForm!: FormGroup;
  token: string = '';
  modalTitle: string = 'Create your new MarketPlace Platform account.';
  checkPasswordStrengthOpen: boolean = false;
  showPassword: boolean = false;
  isLegalForm: boolean = false;

  routeSubscription!: Subscription;
  private _createaccountSubscription!: Subscription;

  private _modal!: MePopupAccountI;

  get newPasswordControl(): AbstractControl | null {
    return this.accountForm.get('newPassword');
  }

  get confirmPasswordControl(): AbstractControl | null {
    return this.accountForm.get('confirmPassword');
  }
  constructor(
    injector: Injector,
    private router: Router,
    private route: ActivatedRoute,
    private popupAccountService: MePopupAccountService,
    private formBuilder: FormBuilder,
    private authWebService: AuthWebService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.routeSubscription = this.route.params.subscribe((params: Params) => {
      this.token = params.token;
    });
  }

  ngAfterViewInit(): void {
    this._modal = {
      title: 'Create your new MarketPlace Platform account',
      content: this.popupAccountFormRef
    };
    this._createaccountSubscription = this.popupAccountService.openPopupSelect(this._modal).subscribe((event: string) => {
      if (event === 'close') {
        this.onCancelclick();
      }
    });
    this.generateAccountForm();
    this.generateLegalForm();
  }

  generateAccountForm(): void {
    this.accountForm = new FormGroup(
      {
        firstName: new FormControl('', [Validators.required]),
        lastName: new FormControl('', [Validators.required]),
        prefferedName: new FormControl('', [Validators.required]),
        newPassword: new FormControl('', [
          Validators.required,
          // check whether the entered password has a number
          PatternValidator(/\d/, { hasNumber: true }),
          // check whether the entered password has upper case letter
          PatternValidator(/[A-Z]/, { hasCapitalCase: true }),
          // check whether the entered password has a lower-case letter
          PatternValidator(/[a-z]/, { hasSmallCase: true }),
          // check whether the entered password has a special character
          PatternValidator(/[\!#$%&\\(\)@\_\.]/, { hasSpecialCharacters: true }),
          // Has a minimum length of 8 characters
          Validators.minLength(8)
        ]),
        confirmPassword: new FormControl('', [Validators.required])
      },
      {
        validators: (control: AbstractControl) =>
          control.get('newPassword')?.value !== control.get('confirmPassword')?.value ? { noPassswordMatch: true } : null
      }
    );
  }

  generateLegalForm(): void {
    this.legalForm = this.formBuilder.group({
      LA001: [false, Validators.requiredTrue],
      LA002: [false, Validators.requiredTrue],
      LA003: [false, Validators.requiredTrue],
      LA004: [false, Validators.requiredTrue]
    });
  }

  checkPasswordStrength(): void {
    this.checkPasswordStrengthOpen = !this.checkPasswordStrengthOpen;
  }

  changeShowPasswordState(): void {
    this.showPassword = !this.showPassword;
  }

  submitForm(): void {
    this._modal = {
      title: 'Legal Agreements',
      content: this.legalFormRef
    };
    this.popupAccountService.pushDataModelToPopup(this._modal);
  }

  onCancelclick(): void {
    this.closepopup();
    this.router.navigate(['auth/login']);
  }

  onDontAgreeClick(): void {
    this._modal = {
      title: '',
      content: this.popupCancelRef
    };
    this.popupAccountService.pushDataModelToPopup(this._modal);
  }

  submitLegalForm(): void {
    if (this.legalForm.invalid) {
      return;
    }
    const data: AccountModel = {
      token: this.token,
      newPassword: this.newPasswordControl?.value,
      user: {
        firstName: this.accountForm.get('firstName')?.value,
        lastName: this.accountForm.get('lastName')?.value,
        prefferedName: this.accountForm.get('prefferedName')?.value
      },
      legalAgreement: [
        { id: 'LA001', accepted: this.legalForm.value.LA001 },
        { id: 'LA002', accepted: this.legalForm.value.LA001 },
        { id: 'LA003', accepted: this.legalForm.value.LA003 },
        { id: 'LA004', accepted: this.legalForm.value.LA004 }
      ]
    };
    this.authWebService.createAccount(data).subscribe(() => {
      this.onCancelclick();
    });
  }

  ngOnDestroy(): object {
    if (this._createaccountSubscription) {
      this._createaccountSubscription.unsubscribe();
    }
    return super.ngOnDestroy();
  }

  private closepopup(): void {
    this.popupAccountService.closePopup();
  }
}
