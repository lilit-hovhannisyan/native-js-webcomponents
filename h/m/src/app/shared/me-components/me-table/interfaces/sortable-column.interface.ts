import { TypeSort } from '../types/sort.type';

interface SortableColumnI {
  field: string;
  sortBy: TypeSort;
}

export default SortableColumnI;
