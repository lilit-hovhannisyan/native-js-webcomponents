import {Component, OnInit} from '@angular/core';
import {portfolioItems} from '../../../../../shared-layer/l-hovhanni/dal/portfolio.model';
import {ActivatedRoute, Router} from '@angular/router';
import {RouteManagementService, PopupService} from 'l-builder';

@Component ({
  selector : 'app-portfolio',
  templateUrl : './portfolio.component.html',
  styleUrls : ['./portfolio.component.less']
})

export class PortfolioComponent implements OnInit {
  tabs: Array<string> = ['all', 'recent'];
  portfolioItems: any;
  portfolioItemsSelected = [];
  portfolioGroupType;


  constructor(private popupService: PopupService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private routeManagementService: RouteManagementService) {}

  columns = 4;
  colMax = 12;
  _columns;

  ngOnInit() {
    this.onResize({windowWidth: window.innerWidth});
    this.portfolioItems = portfolioItems.map( item => {
        return {
            type: 'img',
            isLazy: true,
            src: {
                original: item.src,
                lazy: 'lazy/' + item.src,
            },

            // src: item.src,


            photographer: item.photographer,
            project: item.project,
            recent: item.recent
        };
    });

    const activeTab = this.activatedRoute.snapshot.queryParams.projects;
    this.portfolioGroupType = activeTab && this.tabs.includes(activeTab) ? activeTab : 'all';

    this.routeManagementService.setRouteQuery('projects', this.portfolioGroupType);

    // build grid
    this.groupImages(this.portfolioGroupType);

    // force popup
    const activePhoto = this.activatedRoute.snapshot.queryParams.photo;
    if (activePhoto) {
      const currentPhoto = this.portfolioItems.find(photo => {
          return photo.src.original === activePhoto || photo.src === activePhoto;
      });
      if (currentPhoto) {
          this.onOpenPopup(currentPhoto);
      }
    }
  }

  groupImages(arrayType) {
      const arr = arrayType === 'all' ? this.portfolioItems : this.portfolioItems.filter( i => i.recent );
      this.portfolioItemsSelected = arr;

      // console.log('selected', this.portfolioItemsSelected);
  }

  onOpenPopup(portfolioItem) {
    this.popupService.openPopup(
        'IMG_SLIDER',
        {
            portfolioItem: portfolioItem,
            portfolioItems: this.portfolioItemsSelected,
        },
        {
            query: 'projects',
            param: this.portfolioGroupType
        });

    this.routeManagementService.setRouteQuery('photo', portfolioItem.src.original || portfolioItem.src);
  }

  changeMode(count) {
    this.columns = count;
    this.groupImages(this.portfolioGroupType);
  }

  changeGroup(type) {
      this.portfolioGroupType = type;
      this.groupImages(this.portfolioGroupType);

      this.routeManagementService.setRouteQuery('projects', type);
  }

  onResize(event) {
      if (event.windowWidth <= 600) {
        this.columns = 1;
        this.colMax = 6;
      } else if (event.windowWidth <= 800) {
          this.columns = 2;
          this.colMax = 8;
      } else {
          this.columns = 4;
          this.colMax = 12;
      }

      this._columns = +this.columns;
  }

  onNavigateTo(page) {
      this.router.navigate([page]);
  }

  onImageLoad(event) {
      if (event.original) {
          const item = this.portfolioItems.find( _item => _item.src === event);

          item.src = event.original;
          item.isLazy = false;

          // console.log('app', item.src)
      }
  }
}



