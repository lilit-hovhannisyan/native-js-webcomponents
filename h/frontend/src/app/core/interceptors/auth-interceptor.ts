import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpEventType, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { skipWhile, catchError, switchMap, filter, take, finalize } from 'rxjs/operators';

import { EHttpRequest } from '../models/http/EHttpRequest';
import { EnvironmentService } from '../services/environment.service';
import { AuthenticationService } from '../../authentication/services/authentication.service';
import { WrongHttpRequestUsageException } from '../classes/exceptions/WrongHttpRequestUsageException';
import { UNAUTHORIZED } from 'http-status-codes';

/**
 * Sets `access_token` on requests
 *
 * Refreshes `access_token` when expired
 */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  private isRefreshing = false;
  private isRefreshTokenReadySubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    private environmentService: EnvironmentService,
    private authService: AuthenticationService,
  ) { }

  public intercept(request: EHttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!(request instanceof EHttpRequest) || !request.options) {
      throw new WrongHttpRequestUsageException(
        'Http request created wrong, consider using HttpGet, HttpPost, HttpPut, ' +
        'HttpPatch, HttpDelete instead.'
      );
    }
    return next.handle(this.addHeaders(request)).pipe(
      skipWhile((event: HttpEvent<any>) => event && event.type === HttpEventType.Sent),
      catchError((response: HttpErrorResponse) => {
        if (this.isTokenExpirdException(response)) {
          if (this.isRefreshing) {
            return this.isRefreshTokenReadySubject.pipe(
              filter(result => result !== false),
              take(1),
              switchMap(() => next.handle(this.addHeaders(request)))
            );
          }

          this.isRefreshing = true;
          this.isRefreshTokenReadySubject.next(false);
          return this.authService.refreshToken().pipe(
            switchMap(() => {
              this.isRefreshTokenReadySubject.next(true);
              return next.handle(this.addHeaders(request));
            }),
            finalize(() => this.isRefreshing = false)
          );
        }

        return throwError(response);
      })
    );
  }

  public addHeaders(request: EHttpRequest<any>): EHttpRequest<any> | HttpRequest<any> {
    const cleanedUrl = request.url.replace(/\/\/+/g, '/');
    const session = this.authService.getSessionInfo();
    const headers = {
      'Authorization': `Bearer ${session.tokenResource.accessToken}`,
      'Accept': request.headers.get('accept') || 'application/vnd.navido-v1+json',
      ...((typeof request.getAdditionalHeaders === 'function') ? request.getAdditionalHeaders() : {})
    };


    return request.noModify
      ? request
      : request.cloneWithEHttp({
        url: `${this.environmentService.apiHost}${cleanedUrl}`,
        withCredentials: true,
        setHeaders: headers,
        instance: request,
      });
  }

  public isTokenExpirdException(response: HttpErrorResponse) {
    return !!(
      response.status === UNAUTHORIZED
      // TODO: remove the "response.error !== null" check once back-end fixed
      && response.error !== null
      && Array.isArray(response.error.errors)
      && response.error.errors.find(error => error.key === 'token-expired')
    );
  }
}
