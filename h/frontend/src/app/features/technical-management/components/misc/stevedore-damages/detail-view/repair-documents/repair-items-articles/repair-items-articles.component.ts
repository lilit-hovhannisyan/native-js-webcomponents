import { Component, OnInit, Input } from '@angular/core';
import { IArticleFull, IArticleRow } from '../../../../../../../../core/models/resources/IArticle';
import { Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { getStevedoreRepairArticlesGridConfig } from 'src/app/core/constants/nv-grid-configs/stevedore-repair-articles.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { ArticleMainService } from 'src/app/core/services/mainServices/article-main.service';
import { RepairItemsService } from '../repair-items.service';
import { map } from 'rxjs/operators';

@Component({
  templateUrl: './repair-items-articles.component.html',
  styleUrls: ['./repair-items-articles.component.scss']
})
export class RepairItemsArticlesComponent implements OnInit {

  @Input() public closeModal: () => void;
  @Input() public filterGridById: number;

  public dataSource$: Observable<IArticleRow[]>;
  public gridConfig: NvGridConfig;
  public selectedArticles: IArticleFull[] = [];
  public wording = wording;

  constructor(
    private articleService: ArticleMainService,
    private repairItemsService: RepairItemsService,
  ) { }

  public ngOnInit(): void {
    this.gridConfig = getStevedoreRepairArticlesGridConfig();
    this.dataSource$ = this.filterGridById
      ? this.articleService.getStream({}, { isActive: true }).pipe(
          map(articles => articles.filter(a => a.id === this.filterGridById)),
          map(a => this.articleService.toRows(a))
        )
      : this.articleService.getStream({}, { isActive: true }).pipe(
        map(a => this.articleService.toRows(a))
      );
  }

  public add() {
    const newRepairItems = this.selectedArticles
      .map(article => this.repairItemsService.convertArticleToRepairItem(article));

    this.repairItemsService.addNewRepairItems(newRepairItems);
    this.closeModal();
  }

  public rowSelect(rows: IArticleFull[]) {
    this.selectedArticles = rows;
  }
}
