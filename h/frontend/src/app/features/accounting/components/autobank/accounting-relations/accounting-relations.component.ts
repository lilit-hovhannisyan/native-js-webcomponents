import { Component, OnInit } from '@angular/core';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { NvGridConfig, NvExpandedRowEvent } from 'nv-grid';
import { SelectType } from 'src/app/core/models/SelectType';
import { IMandatorLookupRow, mandatorNoModifier } from 'src/app/core/models/resources/IMandator';
import { BankAccountRepositoryService } from 'src/app/core/services/repositories/bank-account-repository.service';
import { forkJoin, Observable, Subject } from 'rxjs';
import {
  getAccountingRelationsAccountConfig,
  getAccountingRelationsBankConfig
} from 'src/app/core/constants/nv-grid-configs/accounting-relations.config';
import { BankRepositoryService } from 'src/app/core/services/repositories/bank-repository.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { AccountingRelationRepositoryService } from 'src/app/core/services/repositories/accounting-relations-repository.service';
import { IBankAccountRelation, BankAccountKey } from 'src/app/core/models/resources/IBankAccount';
import { MandatorMainService } from 'src/app/core/services/mainServices/mandator-main.service';
import { AccountValidatorsService } from 'src/app/core/services/validators/account-validators.service';

export interface AccountRelationsGrid {
  [mandatorId: number]: IBankAccountRelation[];
}
@Component({
  selector: 'nv-accounting-relations',
  templateUrl: './accounting-relations.component.html',
  styleUrls: ['./accounting-relations.component.scss']
})
export class AccountingRelationsComponent implements OnInit {
  public gridConfig: NvGridConfig;
  public SelectType = SelectType;
  public mandatorNoModifier = mandatorNoModifier;
  public selectedItem: { id: number, companyId: number } = { id: null, companyId: null };
  public bankAccountGridConfig: NvGridConfig;
  public wording = wording;
  public changesSubject = new Subject<IBankAccountRelation>();
  public Object = Object;
  public editMode = false;
  private changesMap = new Map<BankAccountKey, IBankAccountRelation>();
  public mandators$: Observable<IMandatorLookupRow[]>;

  constructor(
    public mandatorRepo: MandatorRepositoryService,
    public bankRepo: BankRepositoryService,
    public bankAccountRepo: BankAccountRepositoryService,
    public currencyRepo: CurrencyRepositoryService,
    public bookingAccountRepo: BookingAccountRepositoryService,
    public mandatorAccountRepo: MandatorAccountRepositoryService,
    public accountValidatorsService: AccountValidatorsService,
    public accountingRelationRepo: AccountingRelationRepositoryService,
    private mandatorMainService: MandatorMainService,
  ) { }

  public enterEditMode() {
    this.editMode = true;
    this.bankAccountGridConfig.editForm.disableEditColumns = false;
  }

  public cancelEditing() {
    this.changesMap.clear();
    this.editMode = false;
    this.bankAccountGridConfig.editForm.disableEditColumns = true;
  }

  public submit() {
    const changes: IBankAccountRelation[] = Array.from(this.changesMap.values());
    this.accountingRelationRepo
      .saveRelations(changes)
      .subscribe(() => {
        this.bankAccountRepo.fetchAll({}).subscribe();
        this.cancelEditing();
      });
  }

  public ngOnInit() {
    forkJoin([
      this.mandatorRepo.fetchLookups({}),
      this.currencyRepo.fetchAll({}),
      this.bookingAccountRepo.fetchAll({}),
      this.bankRepo.fetchAll({}),
      this.bankAccountRepo.fetchAll({}),
    ]).subscribe();
    this.accountingRelationRepo.fetchAll({}).subscribe();
    this.mandators$ = this.mandatorMainService.fetchLookupsWithRolesOnly();

    this.gridConfig = getAccountingRelationsBankConfig();

    this.changesSubject.subscribe((bankAccountRelation: IBankAccountRelation) => {
      this.changesMap.set(bankAccountRelation.id, bankAccountRelation);
    });

    this.bankAccountGridConfig = getAccountingRelationsAccountConfig(
      {
        bookingAccount: [
          this.accountValidatorsService.doesBookingAccountExist,
          this.accountValidatorsService.isBookingAccountActive,
          this.accountValidatorsService.isBookingAccountRelatedToMandator
        ]
      },
      {
        currency: this.currencyRepo.getPropertyById,
        bookingAccount: this.bookingAccountRepo.getPropertyById
      }
    );
  }

  public setExpandableComponent = (expandedRowEvent: NvExpandedRowEvent) => {
    this.gridConfig.expandableComponentConfig.inputs = {
      gridConfig: this.bankAccountGridConfig,
      bankId: expandedRowEvent.row.id,
      mandatorId: this.selectedItem.id,
      companyId: this.selectedItem.companyId,
      changesSubscriber: this.changesSubject
    };
  }

  public itemChanged(item: { id: number, companyId: number }) {
    this.selectedItem = item;
    this.mandatorAccountRepo.fetchAll({ queryParams: { mandatorId: item.id } }).subscribe();
  }

  public customValidation = (): boolean => {
    return !this.changesMap.size;
  }
}
