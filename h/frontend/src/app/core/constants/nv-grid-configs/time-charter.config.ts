import { NvRowHeight, NvGridButtonsPosition, NvColumnDataType, NvGridConfig } from 'nv-grid';
import { IResource } from 'src/app/core/models/resources/IResource';
import { Operations } from 'src/app/core/models/Operations';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';

const timeChartersUrl = url(sitemap.chartering.children.misc.children.timeCharters);

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean,
): NvGridConfig => {
  return ({
    gridName: 'timeCharterList',
    title: '', // wording.chartering.timeCharter,
    showRowIndex: true,
    showPaging: false,
    showFooter: false,
    hideToolbar: true,
    hideAllFilters: true,
    rowHeight: NvRowHeight.midSize,
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    columns: [
      // {
      //   key: 'yearAndNo',
      //   title: wording.chartering.yearAndNo,
      //   isSortable: true,
      //   width: 80,
      //   filter: {
      //     values: []
      //   }
      // },
      {
        key: 'from',
        title: wording.general.from,
        width: 210,
        isSortable: true,
        dataType: NvColumnDataType.DateTime,
        filter: {
          values: []
        }
      },
      {
        key: 'until',
        title: wording.general.until,
        isSortable: true,
        dataType: NvColumnDataType.DateTime,
        width: 210,
        filter: {
          values: []
        }
      },
      {
        key: 'companyName',
        title: wording.chartering.charterer,
        isSortable: true,
        dataType: NvColumnDataType.String,
        width: 200,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editTimeCharter',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (entry: IResource<number>) => actions.edit(entry.id),
      }
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig(timeChartersUrl)
      : [],
  });
};


