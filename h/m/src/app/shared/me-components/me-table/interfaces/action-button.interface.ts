import { Type } from '@angular/core';
import { Subject } from 'rxjs';
import { MeContextMenuI } from '@shared/me-components/me-context-menu/me-context-menu-inteface';

interface ActionButtonI {
  component: Type<{ data: any; tableDataChangeEvent?: Subject<any> }>;
  componentData: ActionButtonComponent;
  type: string;
}

type ActionButtonComponent =
  | {
      iconName: string;
    }
  | MeContextMenuI;

export default ActionButtonI;
