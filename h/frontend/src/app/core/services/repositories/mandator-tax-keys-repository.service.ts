import { Injectable } from '@angular/core';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { IMandatorTaxKey, IMandatorTaxKeyDTO, IMandatorTaxKeyCreate } from '../../models/resources/IMandatorTaxKey';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class MandatorTaxKeysRepositoryService extends AbstractRepository<
IMandatorTaxKey,
IMandatorTaxKeyDTO,
IMandatorTaxKeyCreate,
RepoParams<IMandatorTaxKey>,
number
> {
  protected resourceType = 'MandatorTaxKey';
  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/mandator-tax-keys/';
  }

  protected postProcess(item: IMandatorTaxKeyDTO): IMandatorTaxKey { return item; }
}
