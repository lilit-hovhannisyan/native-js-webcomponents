import { Component, Input, EventEmitter, Output, ViewEncapsulation, ChangeDetectorRef, HostListener } from '@angular/core';
import { IUpload } from 'src/app/core/models/resources/IUpload';
import { ICropper } from 'src/app/core/models/ICropper';
import { FileUploaderService } from '../../services/file-uploader.service';

@Component({
  selector: 'nv-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss']
})
export class ImageUploaderComponent {
  @Input() public inEditingMode = false;
  @Input() public withCropper = true;
  @Input() public label = 'Change image';
  @Output() public imageUploaded: EventEmitter<IUpload> = new EventEmitter<IUpload>();
  public options: ICropper;
  public cropperMode = false;

  constructor(
    private fileUploaderService: FileUploaderService,
    private cdr: ChangeDetectorRef) {
  }

  @HostListener('window:keydown.escape')
  public keyEvent(): void {
    if (this.options
      && this.cropperMode
      && this.inEditingMode
      && this.withCropper) {
      this.leaveCropperMode();
    }
  }

  public onFileChange(el): void {
    if (this.withCropper) {
      this.options = {
        imageFile: el.target.files[0],
      };
      this.enterCropperMode();
    } else {
      this.uploadImage(el.target.files[0]);
    }

    el.target.value = '';
  }

  public enterCropperMode(): void {
    this.cropperMode = true;
    this.cdr.detectChanges();
  }

  public leaveCropperMode(): void {
    this.cropperMode = false;
    this.cdr.detectChanges();
  }

  public uploadImage(data: File): void {
    const formData = new FormData();
    const file = data;
    if (file) {
      if (!file.type.match('image.*')) { throw new Error('av: its not an image'); }
      formData.append('files', file, file.name || this.options.imageFile.name);
      this.fileUploaderService.upload(formData, 'images')
        .subscribe((upload: IUpload) => {
          this.imageUploaded.emit(upload);
          this.leaveCropperMode();
          this.getLogoFileInputElement().value = '';
        });
    }
  }

  public getLogoFileInputElement = (): HTMLInputElement =>
    (document.getElementById('inputFileLogo') as HTMLInputElement)

  public uploadLogoBarClicked = (): void =>
    this.getLogoFileInputElement().click() // triggering click of hidden input element
}
