import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { NvColumnDataType, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IResource } from 'src/app/core/models/resources/IResource';
import { Validators } from 'src/app/core/classes/validators';
import { FormGroup } from '@angular/forms';
import { IRepairItemRow } from '../../../../../../../core/models/resources/IRepairItem';
import { toFloat } from 'src/app/core/helpers/general';

export const getStevedoreDamageRepairItemsGridConfig = (
  actions: IGridConfigActions,
  add: (id?: number) => void,
  calculateAmountControl: () => void,
  isGridDisabled: () => boolean,
  showRowButtons: () => void,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'StevedoreRepairGridConfig',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    editForm: {
      allowCreateNewRow: false,
      showDiscardChangesButton: true,
    },
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'articleId',
        hidden: true,
        visible: false,
      },
      {
        key: 'article',
        hidden: true,
        visible: false,
      },
      {
        key: 'articleUnit',
        hidden: true,
        visible: false,
      },
      {
        key: 'currency',
        hidden: true,
        visible: false,
      },
      {
        key: 'id',
        hidden: true,
      },
      {
        key: 'articleName',
        title: wording.technicalManagement.article,
        width: 157,
        isSortable: true,
        filter: {
          values: [],
        },
        editControl: {
          editable: false,
        }
      },
      {
        key: 'quantity',
        title: wording.technicalManagement.quantity,
        width: 130,
        isSortable: true,
        filter: {
          values: []
        },
        editControl: {
          editable: true,
          validation: {validators: [Validators.min(1)]},
          onValueChange: (form: FormGroup) => {
            const {quantity, pricePerUnit, total} = form.controls;
            total.setValue(toFloat(quantity.value * pricePerUnit.value));
          }
        },
        dataType: NvColumnDataType.Number,
      },
      {
        key: 'unitName',
        title: wording.technicalManagement.unit,
        width: 130,
        isSortable: true,
        filter: {
          values: []
        },
        editControl: {
          editable: false,
        }
      },
      {
        key: 'pricePerUnit',
        title: wording.technicalManagement.pricePerUnit,
        decimalDigitsInfo: '1.2-2',
        width: 150,
        dataType: NvColumnDataType.Decimal,
        isSortable: true,
        filter: {
          values: []
        },
        editControl: {
          editable: true,
          validation: {validators: [Validators.min(0)]},
          onValueChange: (form: FormGroup) => {
            const {quantity, pricePerUnit, total} = form.controls;
            total.setValue(toFloat(quantity.value * pricePerUnit.value));
          }
        }
      },
      {
        key: 'isoCode',
        title: wording.accounting.currency,
        width: 130,
        isSortable: true,
        dataType: NvColumnDataType.Date,
        filter: {
          values: []
        },
        editControl: {
          editable: false,
        }
      },
      {
        key: 'total',
        title: wording.technicalManagement.total,
        width: 130,
        isSortable: true,
        dataType: NvColumnDataType.Decimal,
        filter: {
          values: []
        },
        editControl: {
          editable: false,
          onValueChange: () => {
            calculateAmountControl();
          }
        }
      },
    ],
    buttons: [
      {
        icon: 'eye',
        description: wording.general.view,
        name: 'deleteStevedoreRepair',
        tooltip: wording.general.view,
        actOnEnter: true,
        actOnDoubleClick: true,
        func: (entity: IRepairItemRow) => {
          add(entity.articleId);
        },
        disabled: isGridDisabled
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteStevedoreRepair',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: (entity: IResource<number>) => actions.delete(entity),
        disabled: isGridDisabled
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.add,
        tooltip: wording.general.add,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: () => add(),
        disabled: () => isGridDisabled()
      }
    ]
  });
};
