import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '../../shared/components/page-not-found/page-not-found.component';
import { BaseModuleComponent } from './components/base-module.component';

const appRoutes: Routes = [
  {
    path: '',
    component: BaseModuleComponent,
    children: [
        {
          path: 'main',
          loadChildren: () => import('../main/main.module').then(m => m.MainModule), // lazy
        },
        {
          path: 'accounting',
          loadChildren: () => import('../accounting/accounting.module').then(m => m.AccountingModule), // lazy
        },
        {
          path: 'payments',
          loadChildren: () => import('../payments/payments.module').then(m => m.PaymentsModule), // lazy
        },
        {
          path: 'examples',
          loadChildren: () => import('../examples/examples.module').then(m => m.ExamplesModule), // lazy
        },
        {
          path: 'system',
          loadChildren: () => import('../system/system.module').then(m => m.SystemModule), // lazy
        },
        {
          path: 'chartering',
          loadChildren: () => import('../chartering/chartering.module').then(m => m.CharteringModule), // lazy
        },
        {
          path: 'technical-management',
          loadChildren: () => import('../technical-management/technical-management.module').then(m => m.TechnicalManagementModule), // lazy
        },
        {
          path: '404',
          component: PageNotFoundComponent
        },
      ]
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }

];

/**
 * @see https://angular.io/guide/lazy-loading-ngmodules
 */
@NgModule({
  imports: [
    RouterModule.forChild(
      appRoutes
    )
  ]
})
export class BaseRoutingModule {
}
