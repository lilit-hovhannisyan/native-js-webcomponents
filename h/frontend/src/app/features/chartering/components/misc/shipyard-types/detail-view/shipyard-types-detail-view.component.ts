import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { sitemap } from '../../../../../../core/constants/sitemap/sitemap';
import { url } from '../../../../../../core/constants/sitemap/sitemap-entry';
import { BaseComponentService } from '../../../../../../core/services/base-component.service';
import { ResourceDetailView } from '../../../../../../core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { IShipyardType, IShipyardTypeCreate } from 'src/app/core/models/resources/IShipyardType';
import { ShipyardTypeRepositoryService } from '../../../../../../core/services/repositories/shipyard-type-repository.service';

@Component({
  templateUrl: './shipyard-types-detail-view.component.html',
  styleUrls: ['./shipyard-types-detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShipyardTypesDetailViewComponent
  extends ResourceDetailView<IShipyardType, IShipyardTypeCreate>
  implements OnInit {

  public repoParams = {};
  public routeBackUrl = url(sitemap.chartering.children.misc.children.shipyardTypes);
  public form: FormGroup;

  protected resourceDefault = { } as IShipyardType;

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    shipyardTypesRepo: ShipyardTypeRepositoryService,
    baseComponentService: BaseComponentService,
    private cdr: ChangeDetectorRef,
  ) {
    super(shipyardTypesRepo, baseComponentService);
  }

  public ngOnInit() {
    this.init();

    const { id } = this.route.snapshot.params;
    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => this.resource.label;

  public init(shipyardType: IShipyardType = this.resourceDefault) {
    this.initForm(this.fb.group({
      label: [shipyardType.label, Validators.required],
    }));
    this.cdr.detectChanges();
  }

  public submit() {
    if (!this.form.valid) { return; }

    const { value } = this.form;
    const shipyardType = this.creationMode
      ? { ...value }
      : { ...this.resource, ...value };

    this.creationMode
      ? this.createResource(shipyardType)
      : this.updateResource(shipyardType);
  }
}
