import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from '../../../core/classes/validators';
import { validateAllFormFields } from '../../../shared/helpers/validateAllFormFields';
import { UserRepositoryService } from '../../../core/services/repositories/user-repository.service';
import { NotificationsService } from '../../../core/services/notification.service';
import { NotificationType } from '../../../core/models/INotification';
import { wording } from 'src/app/core/constants/wording/wording';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';

@Component({
  templateUrl: './send-verification-link.component.html',
  styleUrls: ['./send-verification-link.component.scss']
})
export class SendVerificationLinkComponent implements OnInit {
  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userRepositoryService: UserRepositoryService,
    private notificationsService: NotificationsService,
  ) { }

  public ngOnInit(): void {
    this.initializeForm();
  }

  public initializeForm(): void {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  public onSubmit(): void {
    if (!this.form.valid) {
      validateAllFormFields(this.form);
      return;
    }

    this.userRepositoryService.sendActivation(this.form.get('email').value).subscribe(() => {
      this.notificationsService.notify(
        NotificationType.Success,
        wording.general.sent,
        setTranslationSubjects(
          wording.general.requestSent,
          { EMAIL: this.form.get('email').value },
        ));
      this.form.reset();
    });
  }
}
