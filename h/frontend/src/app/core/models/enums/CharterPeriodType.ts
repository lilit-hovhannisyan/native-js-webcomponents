export enum CHARTER_PERIOD_TYPE {
  Period = 0,
  Option,
  OptedPeriod,
  NonDeclaredOption,
  Extension,
}
