import {Component, OnInit} from '@angular/core';
import {ProjectsService} from '../../services/projects.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-top-block',
  templateUrl: './top-block.component.html',
  styleUrls: ['./top-block.component.less']
})
export class TopBlockComponent implements OnInit {
  portfolioItems;
  // _currentProject = Subscription.EMPTY;
  currentProject;


  constructor(private projectsService: ProjectsService) { }

  ngOnInit() {
      this.portfolioItems = [];
      this.projectsService.getCurrentProject();
      this.projectsService.currentProject.subscribe( project => {
          this.currentProject = project;
          this.portfolioItems = project.items;
      });
  }
}
