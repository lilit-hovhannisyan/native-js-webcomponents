import { Type } from 'class-transformer';

export class FileUploadModel {
  @Type(() => ThumbnailDocument)
  thumbnail?: ThumbnailDocument;
  @Type(() => DocumentModel)
  document?: DocumentModel;
  @Type(() => ThreeDdocumentModel)
  '3Ddocument'?: ThreeDdocumentModel;
  @Type(() => LogoModel)
  logo?: LogoModel;
  @Type(() => CoverImageModel)
  coverImage?: CoverImageModel;
}

export class FileUploadType {
  type: string = '';
}

export class BaseUploadModel extends FileUploadType {
  maxHeight: number = 0;
  maxWidth: number = 0;
  minHeight: number = 0;
  minWidth: number = 0;
  maxSize: number = 0;
  supportedFiles: string = '';
  limits: { domain: string; attribute: string }[] = [];
}

export class ThumbnailDocument extends BaseUploadModel {}

export class DocumentModel extends BaseUploadModel {}

export class ThreeDdocumentModel extends BaseUploadModel {}

export class LogoModel extends BaseUploadModel {}

export class CoverImageModel extends BaseUploadModel {}

export class CertificateDocumentModel extends BaseUploadModel {}
