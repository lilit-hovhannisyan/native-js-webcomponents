import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getFlagHistoryConfig = (): NvGridConfig => {
  return ({
    title: wording.chartering.flagHistory,
    gridName: 'flagHistory',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    sortBy: 'from',
    isSortAscending: false,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 10,
    },
    columns: [
      {
        key: 'from',
        title: wording.general.from,
        width: 180,
        isSortable: true,
        filter: {
          values: []
        },
        dataType: NvColumnDataType.Date,
      },
      {
        key: 'flagPath',
        title: wording.general.flag,
        width: 150,
        isSortable: false,
        filter: {
          values: []
        },
      },
      {
        key: 'countryName',
        title: wording.general.country,
        width: 200,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'portOfRegistry',
        title: wording.chartering.portOfRegistry,
        width: 200,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'callSign',
        title: wording.chartering.callSign,
        width: 200,
        isSortable: true,
        filter: {
          values: []
        }
      },

      {
        key: 'company',
        title: wording.chartering.company,
        width: 500,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'register',
        title: wording.chartering.register,
        width: 200,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isTraining',
        title: wording.chartering.training,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [],
    toolbarButtons: [],
  });
};
