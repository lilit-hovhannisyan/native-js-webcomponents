import { NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { wording } from 'src/app/core/constants/wording/wording';
import { IResource } from 'src/app/core/models/resources/IResource';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'ShipyardsGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'label',
        title: wording.general.name,
        width: 350,
        isSortable: true,
        filter: {
          values: []
        }
      }
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editShipyard',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (entity: IResource<number>) => actions.edit(entity.id),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteShipyard',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.UPDATE),
        func: (entity: IResource<number>) => actions.delete(entity),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.UPDATE),
        func: () => actions.add()
      }
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
