import { Injectable } from '@angular/core';
import { IMessageDTO, IMessage, IMessageCreate, MessageKey } from '../../models/resources/IMessage';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';

export interface IMessageRepoParams extends RepoParams<IMessage> {
  queryParams?: {
    maxAgeInHours: number;
  };
}

@Injectable({
  providedIn: 'root'
})
export class MessageRepositoryService extends AbstractRepository<
IMessage,
IMessageDTO,
IMessageCreate,
RepoParams<IMessage>,
MessageKey
> {
  protected resourceType = 'Message';
  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/messages/';
  }

  protected postProcess(item: IMessageDTO): IMessage { return item; }
}
