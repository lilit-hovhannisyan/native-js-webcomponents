import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { MePopupVideoComponent } from './me-popup-video.component';
import { MePopupVideoI } from './me-popup-video.interface';

@Injectable()
export class MePopupVideoService {
  constructor(private dialog: MatDialog) {}

  openVideoPopupAlert(data: MePopupVideoI): void {
    this.dialog.open(MePopupVideoComponent, { data, panelClass: 'no-padding-popup' });
  }
}
