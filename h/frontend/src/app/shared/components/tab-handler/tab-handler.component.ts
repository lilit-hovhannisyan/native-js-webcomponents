import { Component, InjectionToken, Injector, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { ResourceKey } from 'src/app/core/models/resources/IResource';

export interface TabHandlerHelper {
  generateTitle: (id: ResourceKey) => Observable<any>;
  title$: Observable<string>;
  buttonsCount: number;
}

export interface TabHandlerData {
  parentNode: SitemapNode;
  backNode?: SitemapNode;
  helperService?: InjectionToken<TabHandlerHelper>;
}

@Component({
  selector: 'nv-tab-handler',
  templateUrl: './tab-handler.component.html',
  styleUrls: ['./tab-handler.component.scss']
})
export class TabHandlerComponent implements OnInit, OnDestroy {
  private componentDestroyedSubject: Subject<boolean> = new Subject<boolean>();
  private componentDestroyed$: Observable<boolean> = this.componentDestroyedSubject.asObservable();

  public isCreationMode = false;
  public parentNode: SitemapNode;
  private backNode: SitemapNode;
  private helperService: TabHandlerHelper;
  public wording = wording;

  public get buttonsCount(): number {
    return this.helperService?.buttonsCount || 0;
  }
  public get buttonsCountClass(): string {
    return this.buttonsCount
      ? `has-${this.buttonsCount}-buttons`
      : '';
  }

  public titleSubscription: Subscription;
  // title can be either default Create or custom generate title from helperService;
  // if no title is being set, tab title will be displayed
  public title$: Observable<string>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private injector: Injector,
  ) { }

  public ngOnInit(): void {
    this.route.data
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe((value: TabHandlerData) => {
        this.parentNode = value.parentNode;
        this.backNode = value.backNode;

        if (value.helperService) {
          this.helperService = this.injector.get<any>(value.helperService);
        }
      });

    this.route.params
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(({ id }) => {
        this.isCreationMode = id === 'new';

        if (!this.helperService) {
          this.title$ = of(null);
          return;
        }
        if (this.titleSubscription || this.isCreationMode) {
          return;
        }

        this.titleSubscription = this.helperService
          .generateTitle(+id)
          .pipe(takeUntil(this.componentDestroyed$))
          .subscribe(() => {
            this.title$ = this.helperService.title$;
          });
      });
  }

  public routeBack = (): void => {
    this.router.navigate([url(this.backNode || this.parentNode)]);
  }

  public tabDisabler = (node: SitemapNode): boolean => {
    const basicNode = this.parentNode.children.id.children.basic;
    return this.isCreationMode
      ? node === basicNode
      : true;
  }

  public ngOnDestroy(): void {
    this.componentDestroyedSubject.next();
    this.componentDestroyedSubject.complete();
  }
}
