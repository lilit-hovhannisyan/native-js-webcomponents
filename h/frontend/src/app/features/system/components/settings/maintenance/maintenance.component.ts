import { Component, OnInit } from '@angular/core';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { Observable } from 'rxjs';
import { IMaintenanceSchedule, MaintenanceScheduleKey } from 'src/app/core/models/resources/IMaintenanceSchedule';
import { NvGridConfig } from 'nv-grid';
import { getMaintenanceGridConfig } from 'src/app/core/constants/nv-grid-configs/maintenance.config';
import { MaintenanceScheduleRepository } from 'src/app/core/services/repositories/maintenance-schedule-reopsitory.service';
import { switchMap } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd/modal';
import { wording } from 'src/app/core/constants/wording/wording';
import { BaseComponent } from 'src/app/core/abstracts/BaseComponent';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { Language } from 'src/app/core/models/language';
import { MaintenanceDetailViewComponent } from './maintenance-detail-view/maintenance-detail-view.component';

@Component({
  selector: 'nv-maintenance',
  templateUrl: './maintenance.component.html',
  styleUrls: ['./maintenance.component.scss']
})
export class MaintenanceComponent extends BaseComponent implements OnInit {
  public dataSource$: Observable<IMaintenanceSchedule[]>;
  public gridConfig: NvGridConfig;
  private language: Language;
  constructor(
    public gridConfigService: GridConfigService,
    private maintenanceScheduleRepo: MaintenanceScheduleRepository,
    private modalService: NzModalService,
    bcs: BaseComponentService,
  ) {
    super(bcs);
  }

  public ngOnInit(): void {
    this.language = this.settingsService.language;
    this.gridConfig = getMaintenanceGridConfig(
      {
        delete: this.onDelete,
        edit: (id: MaintenanceScheduleKey) => { this.router.navigate([`${this.router.url}/${id}`]); },
        add: () => this.router.navigate([`${this.router.url}/new`])
      },
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );

    this.dataSource$ = this.maintenanceScheduleRepo.fetchAll({})
      .pipe(
        switchMap(() => this.maintenanceScheduleRepo.getStream())
      );
  }

  private onDelete = (id: MaintenanceScheduleKey) => {
    this.confirmationService.confirm({
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDeactivateSchedule,
      okButtonWording: wording.general.yes,
    }).subscribe(confirmed => confirmed && this.maintenanceScheduleRepo.deactivate(id).subscribe());
  }

}
