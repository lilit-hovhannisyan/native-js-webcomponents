import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { wording } from 'src/app/core/constants/wording/wording';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { SessionInfo } from 'src/app/authentication/models/sessionInfo';
import { IDataResponseBody } from 'src/app/core/models/responses/IDataResponseBody';

@Component({
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.scss']
})
export class LoginViewComponent implements OnInit {
  private redirectUrl: string;
  public form: FormGroup;
  public wording = wording;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
  ) { }

  public ngOnInit() {
    this.redirectUrl = this.activatedRoute.snapshot.queryParams.redirectUrl || '/base/main';
    this.initForm();
  }

  private initForm() {
    this.form = this.fb.group({
      userIdentifier: ['admin@navido.de', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(4)]],
    });
  }

  public signInAction() {
    this.authService.login(this.form.value)
      .subscribe((res: IDataResponseBody<SessionInfo>) => {
        this.authService.handleSessionInfoResponse(res, this.redirectUrl);
      });
  }

  public openPrivacyPolicy(): void {
    window.open('assets/pdf/privacy-policy.pdf', '_blank');
  }
}
