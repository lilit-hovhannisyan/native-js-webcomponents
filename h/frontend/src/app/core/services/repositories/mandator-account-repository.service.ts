import { Injectable } from '@angular/core';
import { IMandatorAccountDTO, IMandatorAccount, IMandatorAccountCreate } from '../../models/resources/IMandatorAccount';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { Observable } from 'rxjs';
import { tap, switchMap } from 'rxjs/operators';
import { sortByKey } from '../../helpers/general';
import { MandatorRepositoryService } from './mandator-repository.service';
import { HttpPost } from '../../models/http/http-post';

@Injectable({
  providedIn: 'root'
})
export class MandatorAccountRepositoryService extends AbstractRepository<
  IMandatorAccount,
  IMandatorAccountDTO,
  IMandatorAccountCreate,
  RepoParams<IMandatorAccount>,
  number
> {
  protected resourceType = 'MandatorAccount';
  public sortAsce = true;
  constructor(
    repositoryService: RepositoryService,
    private mandatorRepo: MandatorRepositoryService,
  ) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/mandator-accounts';
  }

  public createMandatorAccounts(mandatorId: number, accountIds: number[]): Observable<IMandatorAccount[]> {
    return this.mandatorRepo.fetchBookingAccounts(mandatorId).pipe(
      switchMap(bookingAccounts => {
        const accountsNewArray = bookingAccounts.map(account => account.bookingAccountId).concat(accountIds);
        return this.mandatorRepo.updateBookingAccounts(mandatorId, accountsNewArray);
      }),
      tap((entities: IMandatorAccount[]) => {
        this.addToStream(entities);
        this.streamSubject.getValue().sort(sortByKey('id', false));
      })
    );
  }

  /**
   * sets `isOpenItem` for all mandator accounts
   * @param mandatorId
   * @param allAccountsIds
   */
  public updateMandatorAccountsIsOpenItem(
    mandatorId: number,
    mandatorAccountIds: number[]
  ): Observable<IMandatorAccount[]> {
    const request = new HttpPost(
      `/api/mandators/${mandatorId}/open-item-mandator-booking-accounts`,
      mandatorAccountIds
    );
    return this.repositoryService.httpClient.request(request)
      .pipe(this.repositoryService.mapToItemsOperator());
  }

  protected postProcess(item: IMandatorAccountDTO): IMandatorAccount { return item; }
}
