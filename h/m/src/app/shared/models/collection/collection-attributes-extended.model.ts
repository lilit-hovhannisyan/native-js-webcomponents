export class CollectionAttributesExtendedModel {
  ownerRef: string = '';
  thumbnail: string = '';
  creator: string = '';
  modifier: string = '';
}
