import { Injectable } from '@angular/core';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { CompanyRepositoryService } from '../repositories/company-repository.service';
import { ICompanyPartnerContribution, ICompanyPartnerContributionFull, AmountChangeType } from '../../models/resources/ICompanyPartnerContribution';
import { ICompany, CompanyKey } from '../../models/resources/ICompany';
import * as moment from 'moment';
import { CompanyContributionsRepositoryService, ICompanyContributionRepoParams } from '../repositories/company-partner-contribution-repository.service';
import { CompanyLegalEntityRepositoryService } from '../repositories/company-legal-entity-repository.service';
import { ICompanyLegalEntity } from '../../models/resources/ICompanyLegalEntity';
import { RepoParams } from '../../abstracts/abstract.repository';
import { CAPITAL_TYPE, COMPANY_LEGAL_FORM } from '../../models/enums/company-legal-form';
import { ITrusteePartnerContributionFull } from 'src/app/core/models/resources/ITrusteePartnerContribution';
import { safeFloatSum } from 'src/app/shared/helpers/general';
import { orderBy, groupBy, Dictionary } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ContributionMainService {
  constructor(
    private contributionsRepo: CompanyContributionsRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private legalEntityRepo: CompanyLegalEntityRepositoryService,
  ) { }

  public fetchAll(params: ICompanyContributionRepoParams = {}): Observable<ICompanyPartnerContributionFull[]> {
    return forkJoin([
      this.contributionsRepo.fetchAll(params),
      this.companyRepo.fetchAll({}),
    ]).pipe(
      map(res => this.getFullList(...res)),
    );
  }

  public getStream(params: ICompanyContributionRepoParams = {}): Observable<ICompanyPartnerContributionFull[]> {
    const getStreamParams = params?.queryParams?.cid
      ? { companyId: params?.queryParams?.cid }
      : {};

    return forkJoin([
      this.contributionsRepo.fetchAll(params),
      this.companyRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => {
        return combineLatest([
          this.contributionsRepo.getStream(getStreamParams),
          this.companyRepo.getStream({}),
        ]);
      }),
      map(res => this.getFullList(...res)),
    );
  }

  private getFullList(
    contributions: ICompanyPartnerContribution[],
    partners: ICompany[]
  ): ICompanyPartnerContributionFull[] {
    const partnersMap: Map<CompanyKey, ICompany> = new Map(
      partners.map(p => ([p.id, p]))
    );

    return contributions.map(c => this.getFull(c, partnersMap));
  }

  private getFull(
    contribution: ICompanyPartnerContribution,
    partnersMap: Map<CompanyKey, ICompany>,
  ): ICompanyPartnerContributionFull {
    const partnerCompany: ICompany = partnersMap.get(contribution.partnerId);
    return { ...partnerCompany, ...contribution };
  }

  /**
   * Filters contributions by date
   * @param contributions
   * @param date
   */
  public filterContributions<T extends ICompanyPartnerContribution>(
    contributions?: T[],
    date?: string,
  ): T[] {
    const dateMoment = moment(date);
    if (!contributions || !dateMoment.isValid()) {
      return contributions || [];
    }

    return contributions.filter(c => moment(c.from).isSameOrBefore(date, 'd'));
  }

  public getCompaniesLatestContributions<
    T extends ICompanyPartnerContribution | ITrusteePartnerContributionFull
  >(
    contributions: T[],
  ): Map<CompanyKey, T> {
    const contributionsMap: Map<CompanyKey, T> = new Map<CompanyKey, T>();
    const sortedContributions: T[] = orderBy(contributions, 'id');
    const groupedContributions: Dictionary<T[]> = groupBy(sortedContributions, 'partnerId');
    const partnersHasZeroContribution: Map<CompanyKey, boolean> = new Map<CompanyKey, boolean>();

    Object.entries(groupedContributions).forEach(
      ([partnerId, contributionsGroup]: [string, T[]]) => {
        partnersHasZeroContribution.set(
          +partnerId,
          this.isInZeroValueState(contributionsGroup as ICompanyPartnerContributionFull[])
        );
      }
    );

    sortedContributions?.forEach(item => {
      const contribution: T = contributionsMap.get(item.partnerId);
      const contributionAmount = safeFloatSum(
        (contribution?.contributionAmount || 0),
        item.contributionAmount,
      );
      const hasContributionWith0Amount: boolean = item.contributionIsZero
        || contribution?.contributionIsZero;
      const isIncrease = contribution?.amountChangeType === AmountChangeType.Increase
        || item.amountChangeType === AmountChangeType.Increase;
      const amountChangeType = isIncrease ? AmountChangeType.Increase : AmountChangeType.Decrease;

      // needed to keep contribution with latest date
      const latestContributionOfCompany: ICompanyPartnerContribution | ITrusteePartnerContributionFull
        = !contribution || (moment(contribution.from).isSameOrBefore(item.from)) ? item : contribution;

      contributionsMap.set(item.partnerId, {
        ...latestContributionOfCompany,
        contributionAmount,
        contributionIsZero: partnersHasZeroContribution.get(item.partnerId),
        amountChangeType,
      } as T);
    });

    return contributionsMap;
  }

  public isInZeroValueState(contributions: ICompanyPartnerContributionFull[]): boolean {
    const zeroValueContributions: ICompanyPartnerContributionFull[] = contributions.filter(
      (c) => c.contributionIsZero
    );

    return zeroValueContributions.reduce((
      zeroValueCount: number,
      contribution: ICompanyPartnerContributionFull,
    ) => {
      const zeroValueSign: number
        = contribution.amountChangeType === AmountChangeType.Increase
          ? 1
          : -1;

      return zeroValueCount + zeroValueSign;
    }, 0) > 0;
  }

  public getFinalContribution(
    companyId: CompanyKey,
    partnerId: CompanyKey,
    from?: string,
  ): Observable<number> {
    return this.contributionsRepo.fetchCompanyContributions(companyId)
      .pipe(
        map((contributions: ICompanyPartnerContribution[]) => {
          const partnerContributions: ICompanyPartnerContributionFull[] =
            contributions.filter(c => c.partnerId === partnerId) as ICompanyPartnerContributionFull[];

          return this.getFinalAmount(partnerContributions, from);
        })
      );
  }

  public getFinalAmount(
    contributions: ICompanyPartnerContributionFull[],
    from?: string,
  ): number {
    const filteredContributions: ICompanyPartnerContributionFull[] = !!from
      ? this.filterContributions(contributions, from)
      : contributions;

    return filteredContributions.reduce((
      contributionAmount: number,
      contribution: ICompanyPartnerContributionFull
    ) => {
      contributionAmount = safeFloatSum(
        contributionAmount,
        contribution.contributionAmount,
      );

      return contributionAmount;
    }, 0);
  }

  public getTotalContributionAmount(
    contributions: ICompanyPartnerContributionFull[],
  ): number {
    return contributions.reduce((sum, c) => sum + c.contributionAmount, 0);
  }

  public sortByDate<T extends ICompanyPartnerContribution>(
    contributions: T[],
  ): T[] {
    return contributions.sort((a, b) => {
      return moment(a.from).isAfter(b.from) ? -1 : 1;
    });
  }

  public showLegalTabsWarning(company: ICompany): Observable<boolean[]> {
    type entityType = ICompanyLegalEntity & ICompanyPartnerContribution;

    const params: RepoParams<entityType> = { queryParams: { cid: company.id } };
    const filterParams: Partial<entityType> = { companyId: company.id };

    return forkJoin([
      this.legalEntityRepo.fetchAll(params),
      this.contributionsRepo.fetchAll(params as ICompanyContributionRepoParams),
    ]).pipe(
      switchMap(() => combineLatest([
        this.legalEntityRepo.getStream(filterParams),
        this.contributionsRepo.getStream(filterParams),
      ])),
      map(([[legalEntity], contributions]: ([ICompanyLegalEntity[], ICompanyPartnerContributionFull[]])) => {
        return [
          this.shouldShowLegalEntityTabWarning(
            company,
            legalEntity,
            this.filterContributions(
              contributions,
              moment().startOf('d').toISOString(),
            ),
          ),
          this.shouldShowTrusteeAndSubParticipantsTabWarnings(contributions),
        ];
      }),
    );
  }

  public shouldShowLegalEntityTabWarning(
    company: ICompany,
    legalEntity: ICompanyLegalEntity,
    contributions: ICompanyPartnerContributionFull[],
  ): boolean {
    if (!(legalEntity && company)) {
      return false;
    }
    const { sumOfContributions, shareCapital, sumOfNonParValueShares, capitalType } = legalEntity || {};

    const sumValue: number = typeof sumOfContributions === 'number'
      ? sumOfContributions
      : shareCapital;

    const isNonParValueShare: boolean = company.legalForm === COMPANY_LEGAL_FORM.StockCorporation // AG
      && capitalType === CAPITAL_TYPE.NonParValueShare;

    const resultSum: number = isNonParValueShare
      ? sumOfNonParValueShares
      : sumValue;

    if (typeof resultSum !== 'number' && !contributions?.length) {
      return false;
    }

    const contributionsSum: number = contributions.reduce((sum, c) => sum + c.contributionAmount, 0);
    return contributionsSum !== resultSum;
  }

  private shouldShowTrusteeAndSubParticipantsTabWarnings(
    contributions: ICompanyPartnerContributionFull[],
  ): boolean {
    return contributions.length === 0;
  }
}
