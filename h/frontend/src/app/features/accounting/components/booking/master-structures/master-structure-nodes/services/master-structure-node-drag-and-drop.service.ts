import { Injectable } from '@angular/core';
import { NzTreeNode, NzFormatBeforeDropEvent, NzTreeComponent } from 'ng-zorro-antd/tree';
import { IMasterStructureNodeTemplate } from 'src/app/core/models/resources/IMasterStructureNode';
import { MasterStructureNodeHelperService } from './master-structure-node-helper.service';
import { SwitchingAccountsService } from '../../switching-nodes/services/switching-accounts.service';
import { Observable, of } from 'rxjs';
import { MasterStructureBookingAccountService } from '../../master-structure-booking-accounts/master-structure-booking-account.service';
import { NodeSummationService } from '../../node-summation/node-summation.service';
import { MasterStructureNodeSharedService } from './master-structure-node-shared.service';

@Injectable({
  providedIn: 'root'
})
export class MasterStructureNodeDragAndDropService {

  constructor(
    private nodesHelperService: MasterStructureNodeHelperService,
    private switchingAccountsService: SwitchingAccountsService,
    private bookingAccountsService: MasterStructureBookingAccountService,
    private nodeSummationService: NodeSummationService,
    private nodesSharedService: MasterStructureNodeSharedService,
  ) { }

  public isDropAllowed(
    confirm: NzFormatBeforeDropEvent,
    nodesComponent: NzTreeComponent,
  ): Observable<boolean> {
    if (this.bookingAccountsService.isClosestSiblingAccountType(confirm)) {
      return of(false);
    }

    const {
      isInvalidSummationDrop,
      isInvalidAccountDrop,
      isInvalidForEarning,
      isInvalidForExpense,
      isInvalidDrop,
      isDropBetweenAccounts,
      isInvalidSwitchingAccountDrop,
    } = this.getInvalidDropCases(confirm);

    if (isDropBetweenAccounts && !isInvalidDrop) {
      this.bookingAccountsService
        .cancelDropIfBetweenAccounts(confirm.dragNode, confirm.node, nodesComponent);
      return of(false);
    }

    if (isInvalidDrop) {
      this.bookingAccountsService.showAccountTypeWarningIfNecessary(
        confirm.dragNode,
        isInvalidForEarning,
        isInvalidForExpense,
        isInvalidSummationDrop,
        isInvalidAccountDrop
      );

      this.switchingAccountsService
        .showSwitchingAccountWarningIfNecessary(isInvalidSwitchingAccountDrop);
      return of(false);
    } else {
      return of(true);
    }
  }

  private getInvalidDropCases(
    confirm: NzFormatBeforeDropEvent,
  ): { [key: string]: boolean } {
    const dragNode: NzTreeNode = confirm.dragNode;
    const targetNode: NzTreeNode = confirm.node;
    const isInvalidSummationDrop: boolean = this.nodeSummationService
      .isInvalidDrop(confirm);
    const isInvalidAccountDrop: boolean = dragNode.origin.bookingAccountId
      && targetNode.level === 0 && confirm.pos !== 0;
    const {
      isInvalidForEarning,
      isInvalidForExpense,
    } = this.nodesSharedService
      .isDropInvalidForExpenseAndEarning(dragNode, targetNode, confirm.pos);
    const isInvalidSwitchingAccountDrop: boolean = this.switchingAccountsService
      .isValidSwitchingAccountDrop(confirm);

    const isInvalidDrop: boolean = isInvalidSummationDrop
      || isInvalidAccountDrop
      || isInvalidForExpense
      || isInvalidForEarning
      || isInvalidSwitchingAccountDrop;
    const isDropBetweenAccounts: boolean = this.bookingAccountsService
      .isDropBetweenAccounts(confirm, isInvalidForEarning, isInvalidForExpense);

    return {
      isInvalidSummationDrop,
      isInvalidAccountDrop,
      isInvalidForEarning,
      isInvalidForExpense,
      isInvalidDrop,
      isDropBetweenAccounts,
      isInvalidSwitchingAccountDrop,
    };
  }

  public getNodesAfterDrop(nzNodes: NzTreeNode[]): IMasterStructureNodeTemplate[] {
    let templateNodes: IMasterStructureNodeTemplate[] | NzTreeNode[] = nzNodes;

    this.nodesHelperService.mapNodes(
      templateNodes,
      (node: NzTreeNode) => {
        node.origin.children = node.getChildren();
        this.nodesHelperService.sortChildNodes(node, 'children');
      },
      'getChildren'
    );

    templateNodes = templateNodes.map(n => ({
      ...n.origin,
      parentNodeId: n.origin.level === 1
        ? n.origin.parentNodeId
        : null,
      level: 1
    })) as any as IMasterStructureNodeTemplate[];

    this.nodesHelperService.mapNodes(
      templateNodes,
      (node: IMasterStructureNodeTemplate) => {
        node.children = node.children.map(n => ({
          ...n.origin,
          parentNodeId: node.id,
          level: node.level + 1,
        }));
        node.childNodes = node.children;
      },
      'children'
    );

    return templateNodes;
  }
}
