import { MeBreadcrumbActionI } from './me-breadcrumb-action.interface';

export interface MeBreadcrumbI {
  activePageName?: string;
  actions: MeBreadcrumbActionI[];
}
