import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatProgressBarModule } from '@angular/material/progress-bar';
import { IconsModule } from '@shared-modules/icons.module';

import { MeButtonModule } from '../me-button/me-button.module';
import { MeDragDropModule } from '@shared-directives/me-drag-drop/me-drag-drop.module';
import { MeUploadFileComponent } from './me-upload-file.component';

@NgModule({
  declarations: [MeUploadFileComponent],
  imports: [CommonModule, MatProgressBarModule, MeButtonModule, IconsModule, MeDragDropModule],
  exports: [MeUploadFileComponent]
})
export class MeUploadFileModule {}
