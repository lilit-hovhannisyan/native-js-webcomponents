import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { HttpPost } from '../../models/http/http-post';
import {
  AccountingRelationKey,
  IAccountingRelation,
  IAccountingRelationCreate,
  IAccountingRelationDTO
} from '../../models/resources/IAccountingRelation';
import { IBankAccountRelation } from '../../models/resources/IBankAccount';
import { IResourceCollection } from '../../models/resources/IResourceCollection';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class AccountingRelationRepositoryService extends AbstractRepository<
IAccountingRelation,
IAccountingRelationDTO,
IAccountingRelationCreate,
RepoParams<IAccountingRelation>,
AccountingRelationKey
>  {
  protected resourceType = 'AutobankAccountingRelation';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IAccountingRelationDTO): IAccountingRelation => item;

  public getBaseUrl = () => '/api/autobank/accounting-relations';

  public saveRelations(bankAccountRelations: IBankAccountRelation[]): Observable<IAccountingRelation[]> {
    const relations: IAccountingRelation[] = bankAccountRelations.map(bankAccountRelation =>
      ({
        bankAccountId: bankAccountRelation.id,
        bankAccountNo: bankAccountRelation.bankAccountNo,
        mandatorId: bankAccountRelation.mandatorId,
        bookingAccountId: bankAccountRelation.bookingAccountId,
        id: bankAccountRelation.relationId,
        _type: this.resourceType
      })
    );

    const url = this.getBaseUrl();
    const request = new HttpPost(url, relations);
    const request$ = this.repositoryService.httpClient.request(request).pipe(
      map(
        (response:
          HttpResponse<IDataResponseBody<IResourceCollection<IAccountingRelation>>>) =>
          response.body.data.items
      ),
      tap((accountingRelations: IAccountingRelation[]) =>
        this.updateStream(accountingRelations))
    );
    return request$;
  }
}
