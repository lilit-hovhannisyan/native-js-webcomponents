import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeGroupAttributesComponent } from './me-group-attributes.component';

@NgModule({
  declarations: [MeGroupAttributesComponent],
  imports: [CommonModule],
  exports: [MeGroupAttributesComponent]
})
export class MeGroupAttributesModule {}
