import { CacheService } from './cache.service';
import { Injectable } from '@angular/core';
import { ICacheItem, ICacheTagMeta as ICacheTag } from 'src/app/core/models/cache/ICacheItem';
import { ResourceKey, IResource } from 'src/app/core/models/resources/IResource';

export const TTL_SHORT = 1000 * 60 * 1; // 1 min
export const TTL_MEDIUM = 1000 * 60 * 10; // 10 min
export const TTL_LONG = 1000 * 60 * 60; // 1h
export const TTL_FOREVER = 1000 * 60 * 60 * 24; // 1day

/**
 * This service uses the caching service to manage cached entities.
 *
 * The tracking service provides an API to cache resource collections as
 * well as individual entities.
 *
 * When tracking a collection, all items of this collection will be
 * separately tracked as well so that later requests to single entities
 * may be handled via caching by returning entities that were previously
 * fetched via a collection.
 */
@Injectable({
  providedIn: 'root'
})
export class ResourceTrackerService {

  private namespace = '/resources';
  private resourcePath = `${this.namespace}/{type}/single/{id}`;
  private collectionPath = `${this.namespace}/{type}/collections/{url}`;

  constructor(
    private cacheService: CacheService,
  ) {}

  private getResourceKey<T extends ResourceKey>(resource: IResource<T>): string {
    return this.resourcePath
      .replace('{type}', resource._type)
      .replace('{id}', resource.id.toString());
  }

  /**
   * Note: if the url is not provided (empty string), a prefix is returned that
   * can be used to retrieve all collections of a resource type
   * via the cache method `CacheService.getItemsByRegex()`.
   */
  private getResourceCollectionKey(resourceType: string, url: string = ''): string {
    return this.collectionPath
      .replace('{type}', resourceType)
      .replace('{url}', url.replace(/^\//, ''));
  }

  private getResourceCollectionKeyPrefix(resourceType: string): string {
    return this.getResourceCollectionKey(resourceType, '');
  }

  public saveOne<Entity extends IResource<ResourceKey>>(
    resource: Entity,
    expiresAt?: Date
  ): boolean {
    const item = this.cacheService.getItem(this.getResourceKey(resource));
    item.value = resource;
    item.expiresAt = expiresAt;

    return this.cacheService.save(item);
  }

  public getOne<Entity>(resource: IResource<ResourceKey>): ICacheItem<Entity> {
    return this.cacheService.getItem<Entity>(this.getResourceKey(resource));
  }

  public getCollection<Entity extends IResource<ResourceKey>>(
    resourceType: string,
    url: string
  ): ICacheItem<Entity[]> {
    return this.cacheService.getItem<Entity[]>(
      this.getResourceCollectionKey(resourceType, url)
    );
  }

  public saveCollection<Entity extends IResource<ResourceKey>>(
    resourceType: string,
    resources: Entity[],
    url: string,
    expiresAt: Date
  ): boolean {
    resources.forEach(r => this.saveOne(r, expiresAt));
    const collectionItem = this.cacheService.getItem<Entity[]>(
      this.getResourceCollectionKey(resourceType, url)
    );
    collectionItem.value = resources;
    collectionItem.expiresAt = expiresAt;

    return this.cacheService.save(collectionItem);
  }

  public deleteCollection(baseUrl: string): boolean {
    return this.cacheService.deleteItem(baseUrl);
  }

  /**
   * Clears all collections in which the type could potentially occur.
   *
   * @param type the resource type
   */
  public deleteCollectionsByType(type: string): void {
    this.cacheService.deleteItems(
      this.cacheService
        .getItemsByPrefix(this.getResourceCollectionKeyPrefix(type))
        .map(item => item.key)
    );
  }

  public deleteOne<T extends ResourceKey>(resource: IResource<T>): boolean {
    return this.cacheService.deleteItem(this.getResourceKey(resource));
  }
}
