import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

export type NvBroadcastMessages = 'booked' | 'invoceSaved';

@Injectable({
  providedIn: 'root'
})
export class BoradcastService {
  private broadcastChannel = new BroadcastChannel('nv-broadcast');
  private boradcastSubject = new Subject<NvBroadcastMessages>();

  constructor() {
    this.broadcastChannel.onmessage = (ev: MessageEvent) => {
      this.boradcastSubject.next(ev.data);
    };
  }

  public sendBroadcastMessage(message: NvBroadcastMessages) {
    this.broadcastChannel.postMessage(message);
  }

  public listenToBroadcastMessages(): Observable<NvBroadcastMessages> {
    return this.boradcastSubject.asObservable();
  }
}
