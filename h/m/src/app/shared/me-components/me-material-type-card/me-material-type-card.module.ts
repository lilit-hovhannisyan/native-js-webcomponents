import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeIconsProviderModule } from '@shared-modules/me-icons.module';

import { MeMaterialTypeCardComponent } from './me-material-type-card.component';

@NgModule({
  declarations: [MeMaterialTypeCardComponent],
  imports: [CommonModule, MeIconsProviderModule],
  exports: [MeMaterialTypeCardComponent]
})
export class MeMaterialTypeCardModule {}
