import { Component, OnInit, OnDestroy, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, from, Observable, Subject, Subscription } from 'rxjs';
import { concatMap, finalize } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { TranslateService } from '@ngx-translate/core';

import { BackOfficeService } from '@features/backoffice/backoffice.web-service';
import { TableViewService } from '@shared/me-components/me-table/table-view.service';
import {
  MeTableActionButtonPropertiesI,
  MeTableCellClickEventI,
  MeTableColumnI,
  MeTableColumnOptionsI,
  MeTableDataSourceI
} from '@shared/me-components/me-table';
import { TableDateComponent } from '@shared/me-components/me-table/table-date/table-date.component';
import { TableImageWithTitleComponent } from '@shared/me-components/me-table/table-image-with-title/table-image-with-title.component';
import { TableLabelComponent } from '@shared/me-components/me-table/table-label/table-label.component';
import { TableSelectComponent } from '@shared/me-components/me-table/table-select/table-select.component';
import { GroupModel } from '@shared/models/groups/group.model';
import { EntityI } from '@core-interfaces/entity.interface';
import { MARKETPLACE_BASE_API_URL, ME_DOMAIN_APPLICATION_GROUP, ME_DOMAIN_APPLICATION_USER } from '@shared/constants';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { UserModel } from '@shared/models/user/user.model';
import { MeAddListCardI } from '@shared/me-components/me-add-list-card/me-add-list-card.interface';
import { getOid, getUserInitials } from '@shared/utils';
import { MeAddListPopupService } from '@shared/me-components/me-popup-add-list/me-popup-add-list.service';
import { MeSearchInputI } from '@shared/me-components/me-search-input/me-search-input-interface';
import { QuickSearchModel } from '@shared/models/search/quick-search.model';
import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { UsersStoreService } from '@features/users/users-store.service';
import { UserActionsService } from '@features/backoffice/users-groups/user-actions.service';

@Component({
  selector: 'me-company-groups',
  templateUrl: './company-groups.component.html',
  styleUrls: ['./company-groups.component.scss'],
  providers: [MeAddListPopupService, UsersStoreService, UserActionsService]
})
export class CompanyGroupsComponent implements OnInit, OnDestroy {
  getCardOid = getOid;
  private readonly NUMBER_OF_ITEMS_ON_PAGE = 50;
  private subs = new SubSink();
  private usersIntersectionMap: Map<string, void> = new Map();
  companyOID: string | null = '';
  usersSearchFromListPopupKeyword: string | undefined = '';
  searchUsersFromAddListPopup!: MeSearchInputI;
  tableItems: MeTableDataSourceI[] = [];
  tableColumns: MeTableColumnI[] = [];
  tableColumnOptions: MeTableColumnOptionsI[] = [];
  users!: UserModel[];
  addUserListCardItems: MeAddListCardI[] = [];
  addUserListCardItemsToSend: string[] = [];
  removeUserListCardItemsToSend: string[] = [];
  addGroupListCardItems: MeAddListCardI[] = [];
  addGroupListCardItemsToSend: string[] = [];
  removeGroupListCardItemsToSend: string[] = [];
  tableLoader = false;
  usersTrackForBottomReach = false;
  isUserSearch = false;

  _cellClickEventSubject!: Subscription;
  cellClickEventSubject = new Subject<MeTableCellClickEventI>();

  i18nStrings: { [key: string]: string } = {
    statusChange: '',
    statusChangeSuccess: '',
    users: '',
    usersInGroupUpdated: '',
    bookGroupAndRoles: '',
    successfullyUpdated: '',
    searchFor: ''
  };

  @ViewChild('addUsersList', { read: TemplateRef }) addUsersList!: TemplateRef<Component>;
  @ViewChild('addGroupsList', { read: TemplateRef }) addGroupsList!: TemplateRef<Component>;

  constructor(
    private backOfficeService: BackOfficeService,
    private tableViewService: TableViewService,
    private basicAlertService: MeBasicAlertService,
    private _activeRoute: ActivatedRoute,
    private meAddListPopupService: MeAddListPopupService,
    private usersStoreService: UsersStoreService,
    public userActionsService: UserActionsService,
    private translate: TranslateService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.companyOID = this._activeRoute.snapshot.paramMap.get('id');
    this.tableColumns = [
      {
        field: 'user',
        headerName: 'User'
      },
      {
        field: 'createdOn',
        headerName: 'Creation Date'
      },
      {
        field: 'roleRef',
        headerName: 'Permission'
      },
      {
        field: 'actions',
        headerName: 'Actions'
      }
    ];

    if (this.companyOID) {
      this.getAllUsers();
      this.subs.sink = this.backOfficeService.getBoCompanyGroups(this.companyOID).subscribe((response) => {
        if (response && response.entities) {
          this.prepareTableData(response.entities);
        }
      });
    }

    this.subs.sink = this.translate.get(Object.keys(this.i18nStrings)).subscribe((translations) => {
      Object.keys(this.i18nStrings).forEach((key) => {
        this.i18nStrings[key] = translations[key];
      });

      this.searchUsersFromAddListPopup = {
        placeholder: this.i18nStrings.searchFor,
        keyword: ''
      };
    });

    this._cellClickEventSubject = this.cellClickEventSubject.subscribe((response) => {
      const { colIndex, data, rowOID, parentRowOID } = response;

      switch (colIndex) {
        case 2:
          this.updateGroupRole(rowOID, data.role);
          break;
        case 3:
          if (data.type === 'edit') {
            if (parentRowOID) {
              this._router.navigate(['../../../user/edit', rowOID], { relativeTo: this._activeRoute });
            } else {
              this._router.navigate(['../../../group/edit', rowOID], { relativeTo: this._activeRoute });
            }
          } else if (data.type === 'add-user') {
            this.getGroupUsers(rowOID);
          } else if (data.type === 'selectedContextMenu') {
            let userActivateOrDeactivateObs!: Observable<UserModel>;

            switch (data.key) {
              case 'activate':
                userActivateOrDeactivateObs = this.userActionsService.activateUser(rowOID);
                break;
              case 'deactivate':
                userActivateOrDeactivateObs = this.userActionsService.deActivateUser(rowOID);
                break;
              case 'manageGroups':
                this.openPopUp('groups', rowOID, 'Add to Group', false);
                // this.openGroupsListPopUp(rowOID);
                this.userActionsService.prepareAddListGroupCardData(rowOID).then((response) => {
                  this.addGroupListCardItems = response;
                });
            }

            if (userActivateOrDeactivateObs) {
              this.subs.sink = userActivateOrDeactivateObs?.subscribe(
                () => {
                  this.getExpandedDataAndSetCellComponents(parentRowOID || '');
                  this.basicAlertService.openBasicAlert({
                    mode: 'success',
                    title: this.i18nStrings.statusChange,
                    content: this.i18nStrings.statusChangeSuccess
                  });
                },
                (error: ErrorResponseI) => {
                  this.basicAlertService.openBasicAlert({
                    mode: 'error',
                    title: error.error.title,
                    content: error.error.details
                  });
                }
              );
            }
          }
          break;
      }
    });
  }

  updateGroupRole(groupOID: string, roleRef: string) {
    const changeGroupRole: EntityI = {
      domainName: ME_DOMAIN_APPLICATION_GROUP,
      attributes: {
        roleRef
      }
    };
    this.tableLoader = true;

    this.subs.sink = this.backOfficeService
      .updateBoGroup(groupOID, changeGroupRole)
      .pipe(finalize(() => (this.tableLoader = false)))
      .subscribe(
        () => {
          this.basicAlertService.openBasicAlert({
            mode: 'success',
            title: this.i18nStrings.bookGroupAndRoles,
            content: this.i18nStrings.successfullyUpdated
          });
        },
        (error: ErrorResponseI) => {
          console.log('error', error);
          this.basicAlertService.openBasicAlert({
            mode: 'error',
            title: error.error.title,
            content: error.error.details
          });
        }
      );
  }

  async prepareTableData(data: GroupModel[]) {
    this.tableLoader = true;
    const roles = await this.backOfficeService.getBoRoles().toPromise();
    const actionButtons: MeTableActionButtonPropertiesI[] = [
      {
        iconName: 'edit-2',
        type: 'edit'
      },
      {
        iconName: 'user-plus',
        type: 'add-user'
      }
    ];
    const generatedActionButtons = this.tableViewService.genereateActionButtons(actionButtons);
    const customComponents: MeTableDataSourceI[] = data.map((d) => {
      return {
        rowOID: d.oid,
        hasChildren: true,
        renderedCells: [
          generatedActionButtons,
          {
            forField: 'user',
            component: TableImageWithTitleComponent,
            componentData: { url: '', title: d.attributes.name }
          },
          {
            forField: 'createdOn',
            component: TableDateComponent,
            // @ts-ignore
            componentData: new Date(d.createdOn)
          },
          {
            forField: 'roleRef',
            component: TableSelectComponent,
            componentData: {
              displayName: 'Roles',
              internalName: 'role',
              value: d.attributes.roleRef,
              values: roles.entities.map((entitie: any) => ({ displayName: entitie.attributes.name, value: entitie.oid }))
            }
          }
        ]
      };
    });

    this.tableItems = customComponents;
    this.tableLoader = false;
  }

  getExpandedDataAndSetCellComponents(rowOID: string) {
    const actionButtons: MeTableActionButtonPropertiesI[] = [
      {
        iconName: 'edit-2',
        type: 'edit'
      },
      {
        contextMenuItems: [
          {
            isActiveAttrType: true,
            key: '',
            value: ''
          },
          { key: 'manageGroups', value: 'Manage Groups' }
        ],
        type: 'selectedContextMenu'
      }
    ];

    this.subs.sink = this.backOfficeService.getBoGroupUsers(rowOID).subscribe((response) => {
      const index = this.tableItems.findIndex((item) => item.rowOID === rowOID);

      if (response.entities) {
        this.tableItems[index].children = response.entities.map((data) => {
          const {
            attributes: { avatar, firstName, lastName, isActive },
            oid
          } = data;
          const generatedActionButtons = this.tableViewService.genereateActionButtons(actionButtons, isActive);

          return {
            rowOID: oid,
            renderedCells: [
              {
                component: TableImageWithTitleComponent,
                componentData: { url: avatar, title: `${firstName} ${lastName}` }
              },
              {
                component: TableDateComponent,
                // @ts-ignore
                componentData: new Date(data.createdOn)
              },
              {
                component: TableLabelComponent,
                componentData: isActive
              },
              generatedActionButtons
            ]
          };
        });
      }
    });
  }

  getAllUsers(skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false) {
    this.subs.sink = this.backOfficeService.getBoUsers(skip, top).subscribe((response) => {
      if (response) {
        this.handleUsersResponse(response, append);
      }
    });
  }

  getGroupUsers(groupOID: string) {
    this.subs.sink = this.backOfficeService.getBoGroupUsers(groupOID).subscribe((response) => {
      this.usersIntersectionMap.clear();

      for (const d of response.entities) {
        this.usersIntersectionMap.set(d.oid);
      }

      this.prepareAddListUserCardData();
      this.openPopUp('users', groupOID, 'Add User');
      // this.openUsersListPopUp(groupOID);
    });
  }

  openPopUp(type: 'users' | 'groups', oid: string, title: string, addOrDeleteUserFromGroup: boolean = true) {
    let contentRef: TemplateRef<Component>;
    let addItems: string[] = [];
    let removeItems: string[] = [];

    if (type === 'users') {
      contentRef = this.addUsersList;
      addItems = this.addUserListCardItemsToSend;
      removeItems = this.removeUserListCardItemsToSend;
    } else {
      contentRef = this.addGroupsList;
      addItems = this.addGroupListCardItemsToSend;
      removeItems = this.removeGroupListCardItemsToSend;
    }

    this.subs.sink = this.meAddListPopupService.openPopup({ title }, contentRef).subscribe((popUpEvent: string) => {
      if (popUpEvent === 'addUsersAndClose') {
        this.subs.sink = this.removeOrAddRequestsFromPopUps(addItems, removeItems, oid, addOrDeleteUserFromGroup).subscribe(() => {
          if (type === 'users') {
            this.addUserListCardItemsToSend = [];
            this.removeUserListCardItemsToSend = [];
          } else {
            this.addGroupListCardItemsToSend = [];
            this.removeGroupListCardItemsToSend = [];
          }

          this.basicAlertService.openBasicAlert({
            mode: 'success',
            title: this.i18nStrings.successfullyUpdated,
            content: this.i18nStrings.usersInGroupUpdated
          });
        });
      }
    });
  }

  removeOrAddRequestsFromPopUps(
    addItems: string[],
    removeItems: string[],
    targetOID: string,
    addOrDeleteUserFromGroup = true
  ): Observable<never> {
    const fjObject: { [key: string]: Observable<boolean> } = {};

    if (addItems && addItems.length > 0) {
      fjObject.add = from(addItems).pipe(
        concatMap((oid) => {
          if (addOrDeleteUserFromGroup) {
            return this.backOfficeService.addUserToBoGroup(targetOID, oid);
          }

          return this.backOfficeService.addUserToBoGroup(oid, targetOID);
        })
      );
    }
    if (removeItems && removeItems.length > 0) {
      fjObject.remove = from(removeItems).pipe(
        concatMap((oid) => {
          if (addOrDeleteUserFromGroup) {
            return this.backOfficeService.deleteUserFromBoGroup(targetOID, oid);
          }

          return this.backOfficeService.deleteUserFromBoGroup(oid, targetOID);
        })
      );
    }
    if (Object.keys(fjObject).length > 0) {
      return forkJoin(fjObject);
    }

    return Observable.throw('There is nothing to add or delete');
  }

  handleAddListCardEvent(
    type: 'users' | 'groups',
    event: { eventName: string; payload: MeAddListCardI },
    index: number,
    cardList: MeAddListCardI[]
  ) {
    switch (event.eventName) {
      case 'add':
        if (event.payload && event.payload.oid) {
          if (type === 'users') {
            if (!this.addUserListCardItemsToSend.includes(event.payload.oid)) {
              this.addUserListCardItemsToSend.push(event.payload.oid);
            }
          } else {
            if (!this.addGroupListCardItemsToSend.includes(event.payload.oid)) {
              this.addGroupListCardItemsToSend.push(event.payload.oid);
            }
          }
        }
        break;
      case 'remove':
        if (event.payload && event.payload.oid) {
          if (type === 'users') {
            if (
              !this.removeUserListCardItemsToSend.includes(event.payload.oid) &&
              !this.addUserListCardItemsToSend.includes(event.payload.oid)
            ) {
              this.removeUserListCardItemsToSend.push(event.payload.oid);
            } else {
              this.addUserListCardItemsToSend = this.addUserListCardItemsToSend.filter((card) => card !== event.payload.oid);
            }
          } else {
            if (
              !this.removeGroupListCardItemsToSend.includes(event.payload.oid) &&
              !this.addGroupListCardItemsToSend.includes(event.payload.oid)
            ) {
              this.removeGroupListCardItemsToSend.push(event.payload.oid);
            } else {
              this.addGroupListCardItemsToSend = this.addGroupListCardItemsToSend.filter((card) => card !== event.payload.oid);
            }
          }
        }
        break;
    }

    if (type === 'users') {
      cardList[index] = {
        ...cardList[index],
        isInGroup: this.addUserListCardItemsToSend.includes(event.payload.oid)
      };
    } else {
      cardList[index] = {
        ...cardList[index],
        isInGroup: this.addGroupListCardItemsToSend.includes(event.payload.oid)
      };
    }
  }

  bottomReachedHandlerForAddList() {
    if (this.usersTrackForBottomReach) {
      if (this.isUserSearch) {
        this.searchUserFromAddListPopup(
          this.usersSearchFromListPopupKeyword || '',
          this.usersStoreService.userNextId,
          this.NUMBER_OF_ITEMS_ON_PAGE,
          true
        );
      } else {
        this.getAllUsers(this.usersStoreService.userNextId, this.NUMBER_OF_ITEMS_ON_PAGE, true);
      }
    }
  }

  searchUserFromAddListPopup(searchValue: string, skip: number = 0, top: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false) {
    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_USER,
      criteriaQuick: searchValue.trim() || undefined,
      orderByAttr: undefined,
      orderByDir: undefined
    };

    this.subs.sink = this.subs.sink = this.backOfficeService.searchBoUsers(searchFilter, skip, top).subscribe((response) => {
      if (response) {
        this.handleUsersResponse(response, append);
      }
    });
  }

  prepareAddListUserCardData(): void {
    this.subs.sink = this.usersStoreService.userEntities$.subscribe((response) => {
      this.addUserListCardItems = [];

      if (response) {
        // this.users = response.entities;
        this.addUserListCardItems = response.map((user) => {
          const {
            attributes: { firstName, lastName, uid, avatar },
            oid
          } = user;

          return {
            firstName,
            lastName,
            uid,
            oid,
            initials: firstName === '[INVITED USER]' ? 'IU' : getUserInitials(`${firstName} ${lastName}`),
            thumbnail: avatar ? `${MARKETPLACE_BASE_API_URL}/files/download/${avatar}` : '',
            type: 'user',
            showButton: true,
            isInGroup: this.usersIntersectionMap.has(oid)
          };
        });
      }
    });
  }

  handleSearchUsersFromAddListPopup(
    searchValue: string,
    skip: number = 0,
    top: number = this.NUMBER_OF_ITEMS_ON_PAGE,
    append: boolean = false
  ): void {
    this.usersSearchFromListPopupKeyword = searchValue;

    this.searchUsersFromAddListPopup = {
      ...this.searchUsersFromAddListPopup,

      placeholder: this.i18nStrings.users,
      keyword: searchValue as string
    };

    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_USER,
      criteriaQuick: searchValue.trim() || undefined
    };

    this.subs.sink = this.backOfficeService.searchBoUsers(searchFilter, skip, top).subscribe((response) => {
      this.handleUsersResponse(response, append, true);
    });
  }

  private handleUsersResponse(response: ArrayResponseI<UserModel>, append: boolean = false, isSearch: boolean = false): void {
    this.usersStoreService.userNextId = response.nextID;
    if (append) {
      this.usersStoreService.appenduserEntities(response.entities);
    } else {
      this.usersStoreService.userEntities = response.entities;
      this.usersStoreService.userTotalCount = response.totalCount;
      this.isUserSearch = isSearch;
      this.usersTrackForBottomReach = true;
    }

    if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
      this.usersTrackForBottomReach = false;
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();

    if (this._cellClickEventSubject) {
      this._cellClickEventSubject.unsubscribe();
    }
  }
}
