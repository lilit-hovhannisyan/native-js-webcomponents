import { sitemap } from './sitemap';
import { SitemapNode } from './sitemap';
import { IWording } from '../../models/resources/IWording';
import { withTrailingSlash } from '../../helpers/general';

/* tslint:disable no-shadowed-variable */

/**
* when a url contains params like '.../:mandatorID/...' a url param like { mandatorID: 44 }
* can be used to transmit information, so the params can be replaced by the corresponding values
*/
export type UrlParams = { [param: string]: string | number };


/**
 * Iterates over all sections and topics of the sitemap.
 *
 * @param walker The `walker` (also sometimes called "texas ranger") callback is invoked
 *   each time a sitemap entry is visited. An entry can either be a section or a topic
 *   inside the previously visited section.
 */
export let walkTheSitemap: (walker: (entry: SitemapEntry) => void) => void;

/**
 * Use the static methods provided by this class to create sitemap entries
 * which get are more feature rich representation of a sitemap nodes.
 *
 * Unlike a sitemap node, an entry includes various meta information about
 * the node, such as its position in the sitemap, its ancestors,
 * its level, its unique key, its full url etc.
 */
export class SitemapEntry {
  constructor(
    public readonly key: string,
    public readonly node: SitemapNode,
    public readonly parent?: SitemapEntry,
  ) { }

  /**
   * This helper returns the best match for the provided url.
   *
   * "Best match" means it will retrieve the entry with the greatest `depth`
   * for the url.
   *
   * @param url a pathname without the domain part
   * @param prefix an optional prefix that will be added in front of the url.
   */
  public static getByUrl(url: string): SitemapEntry | undefined {
    let entry: SitemapEntry;
    walkTheSitemap((curEntry: SitemapEntry) => {
      if (curEntry.matchesUrl(url) && (!entry || curEntry.isDeeperThan(entry))) {
        entry = curEntry;
      }
    });

    return entry;
  }

  public static getByZoneId(zoneId: string = ''): SitemapEntry | undefined {
    let node: SitemapNode;
    let childNodes: Array<SitemapNode> = Object.values(sitemap);

    zoneId.split('.').forEach(zone => {
      let foundNode = childNodes.find(n => n.zone === zone);

      if (foundNode && foundNode.children) {
        childNodes = Object.values(foundNode.children);
      } else if (!foundNode && node && node.children && node.children.id && node.children.id.children) {
        childNodes = Object.values(node.children.id.children);
        foundNode = childNodes.find(n => n.zone === zone);
      }

      node = foundNode;
    });

    return SitemapEntry.getByNode(node);
  }

  public static getByNode(node: SitemapNode): SitemapEntry {
    let entry: SitemapEntry;
    walkTheSitemap((curEntry: SitemapEntry) => {
      if (curEntry.node === node) {
        entry = curEntry;
      }
    });

    return entry;
  }

  public static getUrlParams(url: string): UrlParams {
    const urlParams = {};
    let currentUrl = '';
    url.split('/').forEach(urlSegment => {
      currentUrl = currentUrl + urlSegment + '/';
      const entry = SitemapEntry.getByUrl(currentUrl);
      if (!entry) {
        return;
      }
      const nodePath = entry.node.path;
      const isParam = nodePath.includes(':'); // param-nodes in the sitemap have a path starting with a colon e.g. ':mandatorId'
      if (isParam) {
        const paramName = nodePath.replace(':', ''); // remove the colon
        urlParams[paramName] = urlSegment;
      }
    });

    return urlParams;
  }

  public getKeyByLevel(level): string | undefined {
    return this.getCompoundKey().split('.')[level];
  }


  /**
   * Returns an entries `level`, which is a hierarchy index that descibes
   * how deep the topic is nested.
   *
   * A SitemapEntry which also includes a topic has a greater
   * nesting level than a SitemapEntry which just corresponds to a section.
   */
  public getLevel(): number {
    let level = 0;
    let curEntry = this.parent;

    while (curEntry) {
      curEntry = curEntry.parent;
      level++;
    }

    return level;
  }

  /**
   * Returns a sidemap-wide unique key to identify this entry.
   */
  public getCompoundKey(): string {
    let compoundKey = this.key;
    let curEntry = this.parent;

    while (curEntry) {
      compoundKey = `${curEntry.key}.${compoundKey}`;
      curEntry = curEntry.parent;
    }

    return compoundKey;
  }

  /**
   * Returns the full zone of the entry. Containting all parent zones:
   * e.g.: 'main.system.settings' or 'main.system'
   */
  private getCompoundZone(): string {
    if (this.node.fullZone) {
      return this.node.fullZone;
    }

    let compoundZone = this.node.zone === '*' ? '' : this.node.zone;
    let curEntry = this.parent;

    while (curEntry) {
      const currentZone = curEntry.node.zone;

      if (currentZone !== '*') { // asterisk as zone is used for param-nodes, that usually just contain an id
        compoundZone = compoundZone
          ? `${currentZone}.${compoundZone}`
          : currentZone;
      }
      curEntry = curEntry.parent;
    }

    return compoundZone;
  }

  /**
   * Returns the full title of the entry. Containting all parent zones:
   * e.g.: 'Main/System/Settings' or 'Main/System'
   */
  private getCompoundTitle(): IWording {
    if (this.node.fullTitle) {
      return this.node.fullTitle;
    }

    let compoundTitleEn = this.node.zone === '*'
      ? ''
      : this.node.title['en'];
    let compoundTitleDe = this.node.zone === '*'
      ? ''
      : this.node.title['de'];
    let curEntry = this.parent;

    while (curEntry) {
      if (curEntry.node.zone !== '*') {
        const currentTitleEn = curEntry.node.title['en'];
        const currentTitleDe = curEntry.node.title['de'];

        compoundTitleEn = compoundTitleEn
          ? `${currentTitleEn}/${compoundTitleEn}`
          : compoundTitleEn;

        compoundTitleDe = compoundTitleDe
          ? `${currentTitleDe}/${compoundTitleDe}`
          : compoundTitleDe;
      }

      curEntry = curEntry.parent;
    }

    return {
      en: compoundTitleEn,
      de: compoundTitleDe,
    };
  }

  /**
   * Will concatenate all the paths of an entry and return the resulting url.
   */
  public toUrl(params?: UrlParams) {
    let url = this.node.path;
    let curEntry = this.parent;

    while (curEntry) {
      url = `${curEntry.node.path}/${url}`;
      curEntry = curEntry.parent;
    }

    // the concatinated paths can contain params like ':mandatorID' in this cases
    // a params object needs to be provieded so they can be replaced by actual values
    if (params) {
      Object.entries(params).forEach(([toReplace, replaceBy]) => {
        url = url.replace(`:${toReplace}`, `${replaceBy}`);
      });
    }

    return `/base/${url}`;
  }

  /**
   * Returns true if the given url starts with an url corresponding to
   * this entry.
   */
  public matchesUrl(checkUrl: string) {

    // remove params-part from the string if present.
    const urlWithoutParams = checkUrl.split('?')[0];

    // replace urlSegments that contain IDs with asterisk
    const checkUrlClean = withTrailingSlash(urlWithoutParams).replace(/\/(([0-9]+)|(new))\//g, '/*/');
    // replace urlSegments that contain params ( e.g.: '/:mandatorId/' ) with asterisk
    const nodeUrlClean = withTrailingSlash(this.toUrl()).replace(/\/:(.*?)\//g, '/*/');
    return checkUrlClean.startsWith(nodeUrlClean);
  }

  /**
   * Returns true if this instance's level in the sitemap is
   * more nested than the passed entry.
   */
  public isDeeperThan(entry: SitemapEntry) {
    return this.getLevel() > entry.getLevel();
  }

  /**
   * Returns the zone that this sitemap entry is related to.
   */
  public toZone(): string {
    return this.getCompoundZone();
  }

  public toTitle(): IWording {
    return this.getCompoundTitle();
  }

  // checks `hidden` in hierarchy
  public isHidden(): boolean {
    if (!this.parent) {
      return this.node.hidden;
    }

    return this.node.hidden || this.parent.isHidden();
  }
}


/**
 * Convenience wrapper for SitemapEntry.getByNode(node).toZone()
 */
export const zone = (node: SitemapNode) => SitemapEntry.getByNode(node).toZone();

/**
 * Convenience wrapper for SitemapEntry.getByNode(node).toUrl()
 */
export const url = (node: SitemapNode, params: UrlParams = undefined) => SitemapEntry.getByNode(node).toUrl(params);

export const getCategoryByUrl = (url: string): string | undefined => SitemapEntry.getByUrl(url)
  && SitemapEntry.getByUrl(url).getKeyByLevel(0);
export const getSectionByUrl = (url: string): string | undefined => SitemapEntry.getByUrl(url)
  && SitemapEntry.getByUrl(url).getKeyByLevel(1);
export const getTopicByUrl = (url: string): string | undefined => SitemapEntry.getByUrl(url)
  && SitemapEntry.getByUrl(url).getKeyByLevel(2);

export const getTitleByUrl = (url: string): IWording => SitemapEntry.getByUrl(url).node.title;

export const getNodeByCompoundKey = (compoundKey: string): SitemapNode => {
  const keys = compoundKey.split('.');
  let entry = sitemap;
  keys.forEach((key, i) => {
    if (i === 0) { entry = entry[key]; }
    if (i === 1) { entry = entry['children'][key]; }
    if (i === 2) { entry = entry['children'][key]; }
  });
  return entry as any as SitemapNode;
};

export const getNodeByFullZone = (fullZone: string): SitemapNode => {
  const keys = fullZone.split('.');
  let entry = sitemap;
  keys.forEach((key, i) => {
    if (i === 0) {
      const entryKey = Object.keys(entry).find(k => entry[k].zone === key);
      entry = entry[entryKey];
    }
    if (i === 1) {
      const entryKey = Object.keys(entry['children'])
        .find(k => entry['children'][k].zone === key);
      entry = entry['children'][entryKey];
    }
    if (i === 2) {
      const entryKey = Object.keys(entry['children'])
        .find(k => entry['children'][k].zone === key);
      entry = entry['children'][entryKey];
    }
  });
  return entry as any as SitemapNode;
};

walkTheSitemap = (walker: (entry: SitemapEntry) => void) => {

  // TODO: do this as a loop

  Object.entries(sitemap).forEach(([categoryKey, categoryNode]) => {
    const categoryEntry = new SitemapEntry(categoryKey, categoryNode);
    walker(categoryEntry);

    Object.entries(categoryNode.children).forEach(([sectionKey, sectionNode]) => {
      const sectionEntry = new SitemapEntry(sectionKey, sectionNode, categoryEntry);
      walker(sectionEntry);

      Object.entries(sectionNode.children).forEach(([topicKey, topicNode]: [string, SitemapNode]) => {
        const topicEntry = new SitemapEntry(topicKey, topicNode, sectionEntry);
        walker(topicEntry);

        if (!topicNode.children) { return; }
        Object.entries(topicNode.children).forEach(([subTopicKey, subTopicNode]: [string, SitemapNode]) => {
          const subTopicEntry = new SitemapEntry(subTopicKey, subTopicNode, topicEntry);
          walker(subTopicEntry);

          if (!subTopicNode.children) { return; }
          Object.entries(subTopicNode.children).forEach(([subSubTopicKey, subSubTopicNode]: [string, SitemapNode]) => {
            const subSubTopicEntry = new SitemapEntry(subSubTopicKey, subSubTopicNode, subTopicEntry);
            walker(subSubTopicEntry);

          });
        });
      });
    });
  });
};

export const addTabsFullZoneAndTitle = (
  sitemapNode: SitemapNode,
  tabs = [],
): SitemapNode[] => {
  const sitemapEntry = SitemapEntry.getByNode(sitemapNode);
  const fullZone = sitemapNode.fullZone || sitemapEntry?.toZone();
  const fullTitle = sitemapNode.fullTitle || sitemapEntry?.toTitle();
  const tabWithZone = tabs.find(t => sitemapNode.parentFullZone
    ? !(sitemapNode.parentFullZone === t.fullZone)
    : t.fullZone === fullZone
  );
  if (!tabWithZone && !sitemapNode.isFakeChild && fullZone) {
    tabs.push({
      ...sitemapNode,
      fullZone,
      fullTitle,
    });
  }

  if (sitemapNode.children) {
    Object.keys(sitemapNode.children).forEach(key => {
      addTabsFullZoneAndTitle(sitemapNode.children[key], tabs);
    });
  }

  return tabs;
};
