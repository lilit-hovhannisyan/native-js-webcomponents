import { CurrencyKey } from './../../models/resources/ICurrency';
import { Injectable } from '@angular/core';
import { ICurrency, ICurrencyDTO, ICurrencyCreate } from '../../models/resources/ICurrency';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { TTL_LONG } from 'src/app/shared/services/resource-tracker.service';

export interface ICurrencyRepoParams extends RepoParams<ICurrency> {
  currencyId?: number;
  includeInactives?: boolean;
  queryParams?: {
    startDate?: string;
    endDate?: string;
    limit?: number;
  };
}

@Injectable({
  providedIn: 'root'
})
export class CurrencyRepositoryService extends AbstractRepository<
  ICurrency,
  ICurrencyDTO,
  ICurrencyCreate,
  ICurrencyRepoParams,
  CurrencyKey
> {
  protected resourceType = 'Currency';
  protected resourceTTL = TTL_LONG;
  protected useCachePerDefault = true;

  public sortAsce = true;
  public sortKey = 'isoCode';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess(item: ICurrencyDTO): ICurrency { return item; }

  public getBaseUrl = () => '/api/currencies/';

  /**
   * @param {ICurrency[]} currencies list of currencies
   * @param {CurrencyKey} currencyId optional, can be used when we want that one non active currency
   * that's preselected in form.
   * @returns {ICurrency[]} list of currencies that are filtered by active and can have
   * in addition one non active currency that we need in a form
  */
  public filterActiveOnly = (currencies: ICurrency[], currencyId: CurrencyKey = 0): ICurrency[] =>
    currencies.filter(c => c.isActive || c.id === currencyId)
}
