import { IResource } from './IResource';
import { IWording } from './IWording';
import { BookingCodeKey } from './IBookingCode';

export type OpenItemAccountSheetsKey = number;

interface ISharedProps {
  amountLocal: number;
  amountTransaction: number;
  bookingAccountId: number;
  bookingCodeId: BookingCodeKey;
  bookingDate: string;
  bookingId: number;
  currencyId: number;
  taxKeyId: number;
  debitCreditType: number;
  discount: number;
  exchangeRate: number;
  id: number;
  invoiceDate: Date;
  isOpen: boolean;
  mandatorId: number;
  oppositeBookingAccountId: number;
  paymentDate: Date;
  period: number;
  text: string;
  userName: string;
  voucherDate: Date;
  voucherNumber: string;
  remarks: string;
}

export interface IOpenItemAccountSheetsCreate extends ISharedProps { }

export interface IOpenItemAccountSheetsBase extends ISharedProps, IResource<number> { }

export interface IOpenItemAccountSheetsDTO extends IOpenItemAccountSheetsBase { }

export interface IOpenItemAccountSheets extends IOpenItemAccountSheetsDTO { }

export interface IOpenItemAccountSheetsFull extends IOpenItemAccountSheets {
  oppositeAccountNo: number | string;
  accountName: IWording;
  currencyIsoCode: string;
  bookingCode?: IWording;
}

