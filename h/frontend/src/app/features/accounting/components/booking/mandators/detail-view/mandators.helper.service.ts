
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { IMandator, MandatorKey, mandatorNoModifier } from 'src/app/core/models/resources/IMandator';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';

@Injectable({ providedIn: 'root' })
export class MandatorsHelperService {
  private titleSubject: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public title$: Observable<string> = this.titleSubject.asObservable();

  constructor(
    public settingsService: SettingsService,
    public mandatorRepo: MandatorRepositoryService,
  ) { }

  // Subscribe to any changes to the stream in order
  // to avoid calling update title
  // in each component which changes the mandator
  // the mandator repo is the place where we detect changes
  public generateTitle = (mandatorId: MandatorKey): Observable<void> => {
    return this.mandatorRepo.getStream()
      .pipe(
        filter(mandators => mandators.length > 0),
        map(mandators => mandators.find(mandator => mandator.id === +mandatorId)),
        tap(mandator => this.updateTitle(mandator)),
        map(() => null)
      );
  }

  private getTitle(mandator: IMandator): string {
    return mandator
      ? `${mandatorNoModifier(mandator.no)} ${mandator.name}`
      : null;
  }

  public updateTitle(mandator: IMandator): void {
    this.titleSubject.next(this.getTitle(mandator));
  }
}
