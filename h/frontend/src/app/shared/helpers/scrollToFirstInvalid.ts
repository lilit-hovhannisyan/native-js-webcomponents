import { FormGroup } from '@angular/forms';

export const scrollToElement = (el: Element): void => {
  if (el) {
    el.scrollIntoView({ behavior: 'smooth' });
  }
};

export const scrollToError = (formElement: Element, isGrid: boolean = false): void => {

  if (isGrid) {
    return scrollToElement(formElement);
  }

  const firstElementAntHasError = formElement.querySelector('.ant-form-item-has-error');
  const firstElementNgInvalid = formElement.querySelector('.ng-invalid');
  return firstElementAntHasError
    ? scrollToElement(firstElementAntHasError)
    : (firstElementNgInvalid
      ? scrollToElement(firstElementNgInvalid)
      : null);
};

export async function scrollToFirstInvalid(
  formGroup: FormGroup,
  formElement?: Element,
  isGrid: boolean = false
): Promise<any> {
  if (formGroup.invalid) {
    setTimeout(() => {
      Object.entries(formGroup.controls).forEach(([controlName, formControl]) => {
        if (formControl.invalid) {
          formControl.markAsDirty();
          formControl.markAsTouched();
          formControl.updateValueAndValidity();
        }
      });
      formElement ? scrollToError(formElement, isGrid) : null;
    });

  }
}




