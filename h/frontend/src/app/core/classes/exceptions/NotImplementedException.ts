import { AppException } from './AppException';

/**
 * Thrown if a function is not implemented
 */
export class NotImplementedException extends AppException {
}
