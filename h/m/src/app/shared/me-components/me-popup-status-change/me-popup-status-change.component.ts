import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Injector,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BaseLoggerComponent } from '@features/tracking-system';

import { MeButtonI } from '../me-button/me-button.interface';
import { MePopupStatusChangeI } from './me-popup-status-change.interface';

@Component({
  templateUrl: './me-popup-status-change.component.html',
  styleUrls: ['./me-popup-status-change.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MePopupStatusChangeComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  componentVersion: string = '0.0.1';
  dataModel!: MePopupStatusChangeI;

  @Output() eventOccurs: EventEmitter<{ eventName: string; payload?: string }> = new EventEmitter();

  cancelBtn: MeButtonI = { color: 'secondary', type: 'regular' };
  changeBtn: MeButtonI = { color: 'primary', type: 'regular' };

  // Internal use
  content!: TemplateRef<Component>;

  constructor(
    injector: Injector,
    public dialogRef: MatDialogRef<MePopupStatusChangeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MePopupStatusChangeI,
    private cdr: ChangeDetectorRef
  ) {
    super(injector);
    this.dataModel = data;
  }

  ngOnInit(): void {}

  refreshData(data: MePopupStatusChangeI): void {
    this.dataModel = data;
    this.cdr.detectChanges();
  }

  customEvent(eventName: string, payload?: string): void {
    this.eventOccurs.emit({ eventName, payload });
  }

  ngOnDestroy(): object {
    return super.ngOnDestroy();
  }
}
