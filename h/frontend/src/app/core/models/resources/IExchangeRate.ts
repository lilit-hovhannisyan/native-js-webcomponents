import { IResource } from './IResource';
import { ICurrency } from './ICurrency';
import { IWithLabel } from './IWithLabel';

export type ExchangeRateKey = number;

interface ISharedProps {
  currencyId: number;
  rate: number;
  rateUSD: number;
  date: string;
  source: string;
  importDate: string;
}

export interface IExchangeRateBase extends ISharedProps, IResource<ExchangeRateKey> { }

export interface IExchangeRateCreate extends ISharedProps { }

export interface IExchangeRateDTO extends IExchangeRateBase { }

export interface IExchangeRate extends IExchangeRateDTO, IWithLabel { }

export interface IExchangeRateFull extends IExchangeRate, ICurrency { }

export interface IExchangeRateRow extends IExchangeRateFull { }

export interface IForeignExchangeRate {
  [date: string]: IExchangeRate;
}
