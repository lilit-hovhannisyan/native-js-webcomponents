import { HttpPost } from './../../models/http/http-post';
import { IResourceCollection } from './../../models/resources/IResourceCollection';
import { IDataResponseBody } from './../../models/responses/IDataResponseBody';
import { IRole, RoleKey } from './../../models/resources/IRole';
import { UserKey } from './../../models/resources/IUser';
import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { IUserRole, IUserRoleDTO, IUserRoleCreate, UserRoleKey } from '../../models/resources/IUserRole';
import { RepositoryService } from '../repository.service';
import { forkJoin, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { RoleRepositoryService } from './role-repository.service';
import { convertToMap } from 'src/app/shared/helpers/general';
import { SessionInfo } from 'src/app/authentication/models/sessionInfo';

@Injectable({
  providedIn: 'root'
})
export class UserRoleRepositoryService extends AbstractRepository<
  IUserRole,
  IUserRoleDTO,
  IUserRoleCreate,
  RepoParams<IUserRole>,
  UserRoleKey
> {
  protected resourceType = 'UserRole';
  constructor(
    repositoryService: RepositoryService,
    private roleRepo: RoleRepositoryService,
    private authenticationService: AuthenticationService,
  ) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/user-roles/';
  }

  public setRolesForUser(userId: UserKey, roleIds: RoleKey[]): Observable<IUserRole[]> {
    const url = `/api/users/${userId}/user-roles`;
    const request = new HttpPost(url, roleIds);
    const request$ = this.repositoryService.httpClient.request(request).pipe(
      map((response: HttpResponse<IDataResponseBody<IResourceCollection<IUserRole>>>) => response.body.data.items),
      tap((userRoles: IUserRole[]) => {
        this.pushToStream(userId, undefined, userRoles);
        this.updateRolesInLocalStorage();
      }),
    );
    return request$;
  }

  public setUsersForRole(userIds: UserKey[], roleId: RoleKey): Observable<IUserRole[]> {
    const url = `/api/roles/${roleId}/user-roles`;
    const request = new HttpPost(url, userIds);
    const request$ = this.repositoryService.httpClient.request(request).pipe(
      map((response: HttpResponse<IDataResponseBody<IResourceCollection<IUserRole>>>) => response.body.data.items),
      tap((userRoles: IUserRole[]) => {
        this.pushToStream(undefined, roleId, userRoles);
        this.updateRolesInLocalStorage();
      })
    );
    return request$;
  }

  public pushToStream(
    userId: UserKey | undefined,
    roleId: RoleKey | undefined,
    userRoles: IUserRole[]
  ) {
    const allUserRoles = this.streamSubject.getValue();
    // first remove all previous userRoles of current user from stream
    const allUserRolesFiltered = allUserRoles.filter(userRole => userId
      ? userRole.userId !== userId
      : userRole.roleId !== roleId
    );
    // then add new userRoles ( server sends down all userRoles of current user )
    // ( this way we can also remove userRoles from the stream )
    this.streamSubject.next([ ...allUserRolesFiltered, ...userRoles ]);
  }

  protected postProcess(item: IUserRoleDTO): IUserRole { return item; }

  public updateRolesInLocalStorage(): void {
    const sessionInfo: SessionInfo = this.authenticationService.getSessionInfo();
    const queryParams = { userId: sessionInfo.user.id };

    forkJoin([
      this.roleRepo.fetchAll({}),
      super.fetchAll({ queryParams }),
    ]).subscribe(([roles, userRoles]) => {
      const userRoleMap: Map<UserRoleKey, IUserRole> = convertToMap(userRoles, 'roleId');
      const rolesToUpdate: IRole[] = roles.filter(r => userRoleMap.has(r.id));
      this.authenticationService.updateRoles(rolesToUpdate);
    });
  }
}
