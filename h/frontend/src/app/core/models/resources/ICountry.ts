import { IWithActive } from './IWithActive';
import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';

export type CountryKey = number;
interface ISharedProps extends IWithLabels, IWithActive {
  isoNo: number;
  isoAlpha2: string;
  isoAlpha3: string;
  nationality: string;
  euMember: boolean;
  sepaParticipant: boolean;
  ibanLength: number;
  currencyIsoCode: string;
  bbkShort?: string;
  bbkNo?: number;
  bbkCode?: string;
  countryEmoji: string;
  countryFlagPath: string;
}

export interface ICountryBase extends ISharedProps, IResource<number> { }

export interface ICountryCreate extends ISharedProps { }

export interface ICountryDTO extends ICountryBase { }

export interface ICountry extends ICountryDTO, IWithLabel { }

export interface ICountryRow extends ICountry { }
