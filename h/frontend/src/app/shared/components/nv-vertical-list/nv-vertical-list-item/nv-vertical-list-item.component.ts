import { Component, HostBinding, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'nv-vertical-list-item',
  templateUrl: './nv-vertical-list-item.component.html',
  styleUrls: ['./nv-vertical-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NvVerticalListItemComponent {
  @HostBinding('class.selected')
  @Input() public selected = false;
}
