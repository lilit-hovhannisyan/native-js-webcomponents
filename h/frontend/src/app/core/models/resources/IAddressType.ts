import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';

export type AddressTypeKey = number;

interface ISharedProps extends IWithLabels { }

export interface IAddressTypeCreate extends ISharedProps { }

export interface IAddressTypeBase extends ISharedProps, IResource<number> { }

export interface IAddressTypeDTO extends IAddressTypeBase { }

export interface IAddressType extends IAddressTypeDTO, IWithLabel { }

