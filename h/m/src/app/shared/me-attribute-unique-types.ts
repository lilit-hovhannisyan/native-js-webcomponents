export enum MeAttributeUniqueTypes {
  NOT_UNIQUE = 0,
  UNIQUE = 1,
  NO_UNIQUE_FLAG = 2
}
