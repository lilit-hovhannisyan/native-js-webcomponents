import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef } from '@angular/material/snack-bar';
import { takeUntil } from 'rxjs/operators';

import { MeBasicAlertComponent } from 'src/app/shared/me-components/me-basic-alert/me-basic-alert.component';

import { MeBasicAlertI } from './me-basic-alert.interface';

@Injectable()
export class MeBasicAlertService {
  constructor(private snackBar: MatSnackBar, private basicAlertConfiguration: MatSnackBarConfig) {}

  openBasicAlert(data: MeBasicAlertI): void {
    const barRef: MatSnackBarRef<MeBasicAlertComponent> = this.snackBar.openFromComponent(MeBasicAlertComponent, {
      ...this.basicAlertConfiguration,
      data
    });

    barRef.instance.eventOccurs.pipe(takeUntil(barRef.afterDismissed())).subscribe((event: { eventName: string }) => {
      if (event.eventName === 'exit') {
        this.snackBar.dismiss();
      }
    });
  }
}
