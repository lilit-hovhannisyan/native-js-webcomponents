import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseWebService } from '@core-services/base.web-service';
import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { MaterialEntityI } from './material-entity.interface';
import { MaterialModel } from '@shared/models/materials/material-model';
import { constructUrl } from '@shared/utils';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { BaseSearchModel } from '@shared/models/search/search-base.model';

@Injectable()
export class MaterialCreateEditWebService {
  constructor(private baseWebService: BaseWebService) {}

  createMaterial(data: MaterialEntityI): Observable<MaterialModel> {
    return this.baseWebService.postRequest<MaterialModel, MaterialEntityI>(`${MARKETPLACE_BASE_API_URL}/materials`, data, MaterialModel);
  }

  getMaterialById(materialOID: string): Observable<MaterialModel> {
    return this.baseWebService.getRequest<MaterialModel>(`${MARKETPLACE_BASE_API_URL}/materials/${materialOID}`, MaterialModel);
  }

  updateMaterial(materialOID: string, data: MaterialEntityI): Observable<MaterialModel> {
    return this.baseWebService.putRequest<MaterialModel, MaterialEntityI>(
      `${MARKETPLACE_BASE_API_URL}/materials/${materialOID}`,
      data,
      MaterialModel
    );
  }

  duplicateMaterial(materialOID: string, data: MaterialEntityI): Observable<MaterialModel> {
    return this.baseWebService.postRequest<MaterialModel, MaterialEntityI>(
      `${MARKETPLACE_BASE_API_URL}/materials/${materialOID}/copy`,
      data,
      MaterialModel
    );
  }

  getDocuments(from?: number, to?: number): Observable<ArrayResponseI<MeDocumentModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/documents`, from, to);
    return this.baseWebService.getRequestForArray<MeDocumentModel>(url, MeDocumentModel);
  }

  getMaterialDocuments(materialOid: string, from?: number, to?: number): Observable<ArrayResponseI<MeDocumentModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/materials/${materialOid}/documents`, from, to);
    return this.baseWebService.getRequestForArray<MeDocumentModel>(url, MeDocumentModel);
  }

  searchDocuments(data: BaseSearchModel, skip?: number, top?: number): Observable<ArrayResponseI<MeDocumentModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/documents/search`, skip, top);
    return this.baseWebService.postRequestForArray<MeDocumentModel, BaseSearchModel>(url, data, MeDocumentModel);
  }

  searchMaterialDocuments(materialOid: string, data: BaseSearchModel): Observable<ArrayResponseI<MeDocumentModel>> {
    return this.baseWebService.postRequestForArray<MeDocumentModel, BaseSearchModel>(
      `${MARKETPLACE_BASE_API_URL}/materials/${materialOid}/documents/search`,
      data,
      MeDocumentModel
    );
  }

  submitDocuments(materialOid: string, documentOids: string[]): Observable<boolean> {
    return this.baseWebService.postRequest(`${MARKETPLACE_BASE_API_URL}/materials/${materialOid}/documents`, documentOids);
  }

  removeDocuments(materialOid: string, documentOids: string[]): Observable<void> {
    return this.baseWebService.deleteRequest(`${MARKETPLACE_BASE_API_URL}/materials/${materialOid}/documents`, documentOids);
  }
}
