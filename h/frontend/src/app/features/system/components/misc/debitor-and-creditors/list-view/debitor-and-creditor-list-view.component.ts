import { Component, OnInit, ViewChild } from '@angular/core';
import { NvGridConfig, GridComponent } from 'nv-grid';
import { Observable } from 'rxjs';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { IDebitorCreditorRow } from 'src/app/core/models/resources/IDebitorCreditor';
import { DebitorCreditorMainService } from 'src/app/core/services/mainServices/debitor-creditor-main.service';
import { map } from 'rxjs/operators';
import { DebitorCreditorRepositoryService } from 'src/app/core/services/repositories/debitor-creditor-repository.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/debitors-creditors.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';

@Component({
  templateUrl: './debitor-and-creditor-list-view.component.html',
  styleUrls: ['./debitor-and-creditor-list-view.component.scss']
})
export class DebitorAndCreditorListViewComponent implements OnInit {
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IDebitorCreditorRow[]>;
  @ViewChild('grid', { static: true }) public gridComponent: GridComponent;

  constructor(
    private creditorDebitorService: DebitorCreditorMainService,
    private creditorDebitorRepo: DebitorCreditorRepositoryService,
    public gridConfigService: GridConfigService,
    private confirmationService: ConfirmationService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    private authService: AuthenticationService,
    private router: Router
  ) { }

  public ngOnInit() {
    this.dataSource$ = this.creditorDebitorService.getStream()
      .pipe(map(cds => this.creditorDebitorService.toRows(cds)));

    this.gridConfig = getGridConfig(
      {
        edit: this.onEdit,
        delete: this.onDelete,
        add: () => this.router.navigate([`${this.router.url}/new`]),
      },
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
    );
  }

  private onEdit = (id: number) => this.router.navigate([`${this.router.url}/${id}`]);

  private onDelete = (debitorCreditor: IDebitorCreditorRow): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: debitorCreditor.no.toString() },
    }).subscribe(confirmed => confirmed && this.delete(debitorCreditor));
  }

  private delete(debitorCreditor: IDebitorCreditorRow) {
    this.creditorDebitorRepo.delete(debitorCreditor.id, {})
      .subscribe(() => {
        /**
         * When a debitorCreditor gets deleted,
         * the backend automatically deletes its connected bookingAccounts.
         * We need to clear the stream so they don't appear in the stream anymore.
         */
        this.bookingAccountRepo.removeFromStream(debitorCreditor.debitorAccountId);
        this.bookingAccountRepo.removeFromStream(debitorCreditor.creditorAccountId);
      });
  }
}
