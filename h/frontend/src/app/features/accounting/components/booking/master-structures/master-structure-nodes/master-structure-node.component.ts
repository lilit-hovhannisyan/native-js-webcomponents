import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NzContextMenuService, NzDropdownMenuComponent } from 'ng-zorro-antd/dropdown';
import { NzFormatBeforeDropEvent, NzFormatEmitEvent, NzTreeComponent, NzTreeNode, NzTreeNodeOptions } from 'ng-zorro-antd/tree';
import { NzModalService } from 'ng-zorro-antd/modal';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Validators } from 'src/app/core/classes/validators';
import { wording } from 'src/app/core/constants/wording/wording';
import { MASTER_STRUCTURE_TYPES } from 'src/app/core/models/enums/master-structure-type';
import { Language } from 'src/app/core/models/language';
import { IMasterStructureNode, IMasterStructureNodeTemplate, MASTER_STRUCTURE_NODE_TYPE } from 'src/app/core/models/resources/IMasterStructureNode';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { IMasterStructureFull } from '../../../../../../core/models/resources/IMasterStructure';
import { MasterStructureBookingAccountService } from '../master-structure-booking-accounts/master-structure-booking-account.service';
import { SwitchingNodesService } from '../switching-nodes/services/switching-nodes.service';
import { NodeSummationService } from '../node-summation/node-summation.service';
import { MasterStructureNodeSharedService } from './services/master-structure-node-shared.service';
import { MasterStructureNodeDragAndDropService } from './services/master-structure-node-drag-and-drop.service';
import { MasterStructureNodeHelperService } from './services/master-structure-node-helper.service';
import { MasterStructureBookingAccountsComponent } from '../master-structure-booking-accounts/master-structure-booking-accounts.component';
import { SwitchingNodesComponent } from '../switching-nodes/switching-nodes.component';

@Component({
  selector: 'nv-master-structure-nodes',
  templateUrl: './master-structure-node.component.html',
  styleUrls: ['./master-structure-node.component.scss']
})
export class MasterStructureNodeComponent implements OnInit, OnDestroy {
  @ViewChild('nodesComponent') private nodesComponent: NzTreeComponent;
  @ViewChild('nodeForm') private nodeFormTmpl: TemplateRef<HTMLFormElement>;

  @Input() public set structure(value: IMasterStructureFull) {
    if (value) {
      this.currentStructure = value;
      this.nodesHelperService.initCurrentStructure(this.currentStructure);
      this.nodeSummationService.initCurrentStructure(this.currentStructure);
      this.nodesSharedService.initCurrentStructure(this.currentStructure);
      if ((!this.nodes || this.nodes.length === 0) || !this._editMode) {
        this.rebuildNodes();
      }
    }
  }
  @Input() public set editMode(value: boolean) {
    this._editMode = value;
    if (!this._editMode && this.currentStructure) {
      this.rebuildNodes();
    }
  }
  @Input() public set selectedLanguage(value: Language) {
    this.language = value;
    this.switchingNodesService.setLanguage(this.language);
    this.nodesHelperService.setLanguage(this.language);
    this.bookingAccountsService.setLanguage(this.language);
    this.nodesSharedService.setLanguage(this.language);
  }

  @Output()
  private nodeChange: EventEmitter<IMasterStructureNodeTemplate[]> = new EventEmitter();

  public currentStructure: IMasterStructureFull;
  public nodes: IMasterStructureNodeTemplate[];
  public selectedNode: NzTreeNode;
  public language: Language;
  public wording = wording;
  public _editMode: boolean;
  /**
   * This form is for creating/editing nodes
   */
  public form: FormGroup;
  private nodesSubject$: BehaviorSubject<IMasterStructureNodeTemplate[]>
    = new BehaviorSubject([]);
  public nodes$: Observable<IMasterStructureNodeTemplate[]> = this.nodesSubject$
    .asObservable()
    .pipe(
      tap(nodes => {
        this.emitNodes(nodes);
      })
    );
  public MASTER_STRUCTURE_NODE_TYPE = MASTER_STRUCTURE_NODE_TYPE;
  public MASTER_STRUCTURE_TYPES = MASTER_STRUCTURE_TYPES;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public isSummationVisible: boolean;
  public isCurrentYearResultVisible: boolean;
  public isPreviousYearResultVisible: boolean;
  public isCreationMode: boolean;
  public filteredSwitchingNodes: string[] = [];

  constructor(
    private nzContextMenuService: NzContextMenuService,
    private modalService: NzModalService,
    private confirmationService: ConfirmationService,
    private fb: FormBuilder,
    private bookingAccountsService: MasterStructureBookingAccountService,
    private nodesHelperService: MasterStructureNodeHelperService,
    private switchingNodesService: SwitchingNodesService,
    private nodeSummationService: NodeSummationService,
    private nodesSharedService: MasterStructureNodeSharedService,
    private nodeDragAndDropService: MasterStructureNodeDragAndDropService,
  ) { }

  public ngOnInit(): void {
    this.initForm();
    this.switchingNodesService.initForm(this.form);
    this.nodesSharedService.initForm(this.form);
  }

  /**
   * Ng Zorro does not detect changes, so every time there is a change in tree we need to
   * call this method, so ng zorro would be updated
   */
  private refreshNodes = (): void => {
    this.nodesSubject$.next(this.nodesHelperService.mapNodes(
      this.nodesHelperService.copyNodes(this.nodes),
      (node: IMasterStructureNodeTemplate) => {
        if (node.nodeType === MASTER_STRUCTURE_NODE_TYPE.NODE) {
          node.children = node.childNodes as IMasterStructureNodeTemplate[];
        }
      }
    ));
  }

  private rebuildNodes(): void {
    this.nodes = this.nodesHelperService.buildNodeTree(
      this.nodesHelperService.copyNodes(this.currentStructure.nodes || [])
    );
    this.refreshNodes();
  }

  private changeNode(makeChange: (node: IMasterStructureNodeTemplate) => void): void {
    this.nodesHelperService.changeNode(this.nodes, this.selectedNode, makeChange);
    this.refreshNodes();
  }

  private emitNodes(nodes: IMasterStructureNodeTemplate[]): void {
    this.nodeChange.emit(this.nodesHelperService.finalizeNodes(nodes));
  }

  private initForm(): void {
    this.form = this.fb.group({
      labelEn: ['', [Validators.required, Validators.maxLength(170)]],
      labelDe: ['', [Validators.required, Validators.maxLength(170)]],
      externalSystemCode: ['', Validators.maxLength(50)],
      hasSwitchingNode: [false],
      switchingNodeName: [''],
      switchingNodeExternalCode: [''],
      switchingNodeId: [],
    });
  }

  public contextMenu(
    $event: MouseEvent,
    menu: NzDropdownMenuComponent,
    node?: NzTreeNode
  ): void {
    if (!this._editMode) {
      return;
    }

    this.selectedNode = node;
    this.isSummationVisible = this.nodeSummationService
      .checkIfSummationIsVisible(this.selectedNode);
    this.isCurrentYearResultVisible = this.nodesSharedService
      .validateYearResults('hasResultCurrentYear', this.nodes, this.selectedNode);
    this.isPreviousYearResultVisible = this.nodesSharedService
      .validateYearResults('hasResultPreviousYear', this.nodes, this.selectedNode);
    this.nzContextMenuService.create($event, menu);
  }

  public closeContextmenu(): void {
    this.nzContextMenuService.close();
  }

  public highlightNode(): void {
    this.changeNode(node => node.isHighlighted = !node.isHighlighted);
  }

  public makeExpenseItem(): void {
    this.changeNode(node => node.hasExpenseItem = !node.hasExpenseItem);
  }

  public makeCurrentYearResult(): void {
    this.changeNode(node => node.hasResultCurrentYear = !node.hasResultCurrentYear);
  }

  public makePrevYearResult(): void {
    this.changeNode(node => node.hasResultPreviousYear = !node.hasResultPreviousYear);
  }

  public openCreationModal(): void {
    this.isCreationMode = true;
    this.form.reset();
    this.defineHasSwitchingNodeCheckboxStatus();

    this.modalService.create({
      nzTitle: this.wording.accounting.addNode[this.language],
      nzContent: this.nodeFormTmpl,
      nzClosable: false,
      nzOnOk: this.onCreateModalSave(),
      nzOnCancel: () => {
        this.isCreationMode = false;
        this.switchingNodesService.unsubscribeSwitchingExternalCode();
      }
    });
  }

  private onCreateModalSave(): () => void {
    return () => {
      const { hasSwitchingNode } = this.form.controls;
      this.switchingNodesService.setSwitchingNodeNameValidators();
      validateAllFormFields(this.form);

      if (this.form.invalid) {
        return false;
      }

      this.switchingNodesService.unsubscribeSwitchingExternalCode();
      const addedNode: IMasterStructureNodeTemplate = this
        .addNode(this.selectedNode, this.form.value);

      if (hasSwitchingNode.value) {
        const switchingNode: IMasterStructureNodeTemplate = this.switchingNodesService
          .searchAndGetSwitchingNode(addedNode, this.nodes);
        addedNode.switchingNodeId = switchingNode.id;
        this.switchingNodesService
          .applyChangesToSwitchingNode(addedNode, this.nodes, switchingNode);
      }

      this.isCreationMode = false;
      this.refreshNodes();
    };
  }

  public openEditModal(): void {
    this.changeNode(node => {
      const {
        labelEn,
        labelDe,
        externalSystemCode,
        hasSwitchingNode,
      } = this.form.controls;

      labelEn.setValue(node.labelEn);
      labelDe.setValue(node.labelDe);
      externalSystemCode.setValue(node.externalSystemCode);
      hasSwitchingNode.setValue(node.hasSwitchingNode);

      const switchingNode: IMasterStructureNodeTemplate = this.switchingNodesService
        .searchAndGetSwitchingNode(node, this.nodes);

      this.switchingNodesService.setSwitchingNodeValuesToForm(switchingNode);
      this.defineHasSwitchingNodeCheckboxStatus();
      this.switchingNodesService.confirmExternalCodeChange(switchingNode);
      this.showEditModal(node, switchingNode);
    });
  }

  public showEditModal(
    foundNode: IMasterStructureNodeTemplate,
    oldSwitchingNode: IMasterStructureNodeTemplate,
  ): void {
    const { switchingNodeId } = this.form.controls;

    this.modalService.create({
      nzTitle: this.wording.accounting.editNode[this.language],
      nzContent: this.nodeFormTmpl,
      nzClosable: false,
      nzOnOk: () => {
        const isSuccess: boolean = this.nodesSharedService
          .onEditModalSave(this.nodes, foundNode, oldSwitchingNode);

        if (!isSuccess) {
          return;
        }

        this.refreshNodes();
      },
      nzOnCancel: () => {
        switchingNodeId.setValue(null, { emitEvent: false });
        this.switchingNodesService.unsubscribeSwitchingExternalCode();
      },
    });
  }

  public openDeleteModal(): void {
    this.nodesHelperService.mapNodes(
      this.nodes,
      (foundNode: IMasterStructureNode, i: number, parent?: IMasterStructureNode) => {
        if (foundNode.id !== this.selectedNode.key) {
          return;
        }

        if (foundNode.parentNodeId && foundNode.level === 1) {
          this.nodeSummationService.showNodeUsedInSummationWarning();
          return;
        }
        let switchingNode: IMasterStructureNode;

        if (foundNode.switchingNodeId) {
          switchingNode = this.switchingNodesService
            .getSwitchingNode(this.nodes, foundNode);
        }

        this.showDeleteModal(foundNode, switchingNode, parent);
      }
    );
  }

  private showDeleteModal(
    foundNode: IMasterStructureNode,
    switchingNode: IMasterStructureNode,
    parent: IMasterStructureNode,
  ): void {
    this.confirmationService.confirm({
      wording: this.nodesSharedService
        .getDeleteMessage(this.nodes, foundNode, switchingNode),
      okButtonWording: !!foundNode.childNodes.length
        ? this.wording.general.deleteAll
        : this.wording.general.delete,
      cancelButtonWording: this.wording.general.cancel
    }).subscribe(isConfirmed => {
      if (!isConfirmed) {
        return;
      }

      this.nodes = this.nodesSharedService
        .onDeleteModalConfirm(this.nodes, foundNode, parent);
      this.refreshNodes();
    });
  }

  public openBookingAccountsModal(): void {
    this.bookingAccountsService.openBookingAccountsModal(
      this.nodes,
      this.selectedNode as any as IMasterStructureNodeTemplate,
      this.refreshNodes,
      this.addNode,
      MasterStructureBookingAccountsComponent,
    );
  }

  public openSummationModal(): void {
    this.nodeSummationService
      .openSummationModal(this.nodes, this.selectedNode, this.refreshNodes);
  }

  public openSwitchingNodesModal(): void {
    this.switchingNodesService.openSwitchingNodesModal(this.nodes, this.selectedNode, SwitchingNodesComponent);
  }

  public onSwitchingNameBlur(): void {
    this.switchingNodesService.onSwitchingNameBlur(this.nodes);
  }

  public onSwitchingNodeSearch(value: string): void {
    this.filteredSwitchingNodes = this.switchingNodesService.onSwitchingNodeSearch(value);
  }

  private defineHasSwitchingNodeCheckboxStatus(): void {
    this.switchingNodesService
      .defineHasSwitchingNodeCheckboxStatus(this.nodes, this.selectedNode);
    this.filteredSwitchingNodes = this.switchingNodesService.getFilteredSwitchingNodes();
    this.switchingNodesService.listenSwitchingNodeChanges(
      this.nodes,
      this.isCreationMode,
      this.componentDestroyed$
    );
  }

  private addNode = (
    selectedNode: IMasterStructureNodeTemplate | NzTreeNodeOptions,
    newNode: IMasterStructureNode,
  ): IMasterStructureNodeTemplate => {
    if (!selectedNode) {
      const addedNode: IMasterStructureNodeTemplate = this.nodesHelperService
        .addFirstLevelNode(newNode);
      this.nodes.push(addedNode);
      return addedNode;
    }

    return this.nodesHelperService.addChildNode(this.nodes, selectedNode, newNode);
  }

  public closeTree(): void {
    this.nodesHelperService.closeTree(this.nodes);
    this.refreshNodes();
  }

  public openTree(): void {
    this.nodesHelperService.openTree(this.nodes);
    this.refreshNodes();
  }

  public onExpandedChange(event: NzFormatEmitEvent): void {
    this.nodesHelperService.onExpandedChange(event, this.nodes);
  }

  public onDragEnter(event: NzFormatEmitEvent): void {
    if (event.dragNode.origin.nodeType === MASTER_STRUCTURE_NODE_TYPE.SUMMATION) {
      event.node.isExpanded = false;
    }
  }

  public beforeDrop = (confirm: NzFormatBeforeDropEvent): Observable<boolean> => {
    return this.nodeDragAndDropService.isDropAllowed(confirm, this.nodesComponent);
  }

  public onDrop(event?: NzFormatEmitEvent): void {
    const nzNodes: NzTreeNode[] = this.nodesComponent.getTreeNodes();
    this.nodes = this.nodeDragAndDropService.getNodesAfterDrop(nzNodes);
    this.nodesComponent.renderTree();
    this.emitNodes(this.nodes);
    this.scrollToTheNode(event.dragNode.key);
  }

  private scrollToTheNode(nodeKey: string): void {
    setTimeout(() => {
      document.querySelector(`[data-node-key='${nodeKey}']`)
        .scrollIntoView({ behavior: 'smooth', block: 'center' });
    });
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
