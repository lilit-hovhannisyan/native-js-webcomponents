import { Injectable, Type } from '@angular/core';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { IMasterStructure } from 'src/app/core/models/resources/IMasterStructure';
import { ACCOUNTING_TYPES } from 'src/app/core/models/enums/accounting-types';
import { MASTER_STRUCTURE_TYPES } from 'src/app/core/models/enums/master-structure-type';
import { ACCOUNTING_RELATIONS } from 'src/app/core/models/enums/accounting-relations';
import { NzFormatBeforeDropEvent, NzTreeNode, NzTreeComponent, NzTreeService } from 'ng-zorro-antd/tree';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { MasterStructureBookingAccountsComponent } from './master-structure-booking-accounts.component';
import { IMasterStructureNodeTemplate, IMasterStructureNode } from 'src/app/core/models/resources/IMasterStructureNode';
import { Language } from 'src/app/core/models/language';
import { SwitchingNodesService } from '../switching-nodes/services/switching-nodes.service';
import { SwitchingAccountsService } from '../switching-nodes/services/switching-accounts.service';

export interface IBookingAccountHashTable {
  [key: number]: IBookingAccount;
}

@Injectable({
  providedIn: 'root'
})
export class MasterStructureBookingAccountService {
  private currentStructure: IMasterStructure;

  public bookingAccounts$: Observable<IBookingAccount[]>;
  private allBookingAccounts: IBookingAccountHashTable = {};
  private unusedBookingAccounts: IBookingAccount[] = [];

  public usedBookingAccountIdsSubject$:
    BehaviorSubject<number[]> = new BehaviorSubject([]);
  public usedBookingAccounts$:
    Observable<IBookingAccountHashTable> = this.usedBookingAccountIdsSubject$
      .asObservable()
      .pipe(
        map(accountsIds => {
          const hashTable: { [key: number]: IBookingAccount } = {};
          accountsIds.forEach(id => hashTable[id] = this.allBookingAccounts[id]);
          return hashTable;
        })
      );
  private language: Language;
  private modalRef: NzModalRef;

  constructor(
    private bookingAccountRepo: BookingAccountRepositoryService,
    private confirmationService: ConfirmationService,
    private modalService: NzModalService,
    private switchingNodesService: SwitchingNodesService,
    private switchingAccountsService: SwitchingAccountsService,
  ) { }

  public setLanguage(language: Language): void {
    this.language = language;
  }

  public setMasterStructure(structure: IMasterStructure): void {
    if (this.currentStructure && structure.id !== this.currentStructure.id) {
      this.currentStructure = structure;
      this.usedBookingAccountIdsSubject$.next([]);
    } else {
      this.currentStructure = structure;
    }
  }

  public getCurrentStructure(): IMasterStructure {
    return this.currentStructure;
  }

  public fetchBookingAccounts(): Observable<IBookingAccount[]> {
    this.bookingAccounts$ = combineLatest([
      this.usedBookingAccounts$,
      this.bookingAccountRepo
        .getStream({ isActive: true })
        .pipe(
          tap(accounts => accounts
            .forEach(account => this.allBookingAccounts[account.id] = account)
          ),
        ),
    ]).pipe(
        map(([usedAccounts, allAccounts]) => JSON.parse(JSON.stringify(allAccounts))
          .filter(a => {
            return this.currentStructure
              && this.hasTheSameMasterStructureType(a)
              && this.hasTheSameAccountingType(a)
              && !usedAccounts[a.id];
          })
        ),
        tap(accounts => {
          this.unusedBookingAccounts = accounts;
        })
      );

    return this.bookingAccounts$;
  }

  public addUsedBookingAccount(ids: number | number[]): void {
    const existingIds: number[] = this.usedBookingAccountIdsSubject$.getValue();
    const usedIds: number[] = Array.isArray(ids)
      ? [...existingIds, ...ids]
      : [...existingIds, ids];
    this.usedBookingAccountIdsSubject$.next(usedIds);
  }

  public deleteUsedBookingAccounts(ids: number | number[]): void {
    const usedIdsFrequency: { [key: number]: number } = {};
    const isArrayOfIds: boolean = Array.isArray(ids);

    if (isArrayOfIds) {
      (ids as number[]).forEach(id => usedIdsFrequency[id] = 1);
    }

    this.usedBookingAccountIdsSubject$.next(
      isArrayOfIds
        ? this.usedBookingAccountIdsSubject$
          .getValue()
          .filter(id => !usedIdsFrequency[id])
        : this.usedBookingAccountIdsSubject$.getValue().filter(id => id !== ids)
    );
  }

  public hasTheSameAccountingType(
    account: IBookingAccount,
    type?: ACCOUNTING_TYPES,
  ): boolean {
    const accountingType: ACCOUNTING_TYPES = type === undefined
      ? this.currentStructure.accountingType
      : type;
    return accountingType === (accountingType & account.accountingType);
  }

  public hasTheSameMasterStructureType(
    account: IBookingAccount,
    relation?: MASTER_STRUCTURE_TYPES,
  ): boolean {
    let accountingRelation: ACCOUNTING_RELATIONS;
    if (relation !== undefined) {
      const isBwa: boolean = relation
        === MASTER_STRUCTURE_TYPES.BUSINESS_MANAGEMENT_EVALUATION;
      const isProfitAndLoss: boolean = relation
        === MASTER_STRUCTURE_TYPES.PROFIT_AND_LOSS_STATEMENT;
      accountingRelation = isBwa || isProfitAndLoss
        ? account.accountingRelation === ACCOUNTING_RELATIONS.PROFITANDLOSSEXPENSES
          ? ACCOUNTING_RELATIONS.PROFITANDLOSSEXPENSES
          : ACCOUNTING_RELATIONS.PROFITANDLOSSEARNINGS
        : relation as any as ACCOUNTING_RELATIONS;
      return account.accountingRelation === accountingRelation;
    } else if (this.currentStructure) {
      const isBwa: boolean = this.currentStructure.masterStructureType
        === MASTER_STRUCTURE_TYPES.BUSINESS_MANAGEMENT_EVALUATION;
      const isProfitAndLoss: boolean = this.currentStructure.masterStructureType
        === MASTER_STRUCTURE_TYPES.PROFIT_AND_LOSS_STATEMENT;
      accountingRelation = isBwa || isProfitAndLoss
          ? account.accountingRelation === ACCOUNTING_RELATIONS.PROFITANDLOSSEXPENSES
            ? ACCOUNTING_RELATIONS.PROFITANDLOSSEXPENSES
            : ACCOUNTING_RELATIONS.PROFITANDLOSSEARNINGS
          : this.currentStructure.masterStructureType as any as ACCOUNTING_RELATIONS;

      return account.accountingRelation === accountingRelation;
    }

    return true;
  }

  /**
   * Gives back the current structure's(which is in edit mode at that moment) unused
   * bookingAccounts
   */
  public getUnusedBookingAccounts(): IBookingAccount[] {
    return this.unusedBookingAccounts;
  }

  /**
   * Gives back the structure's unused bookingAccounts, usually used in ngOnInit to get it
   * for each structure
   */
  public getStructureUnusedBookingAccounts(
    structure: IMasterStructure,
    usedBookingAccounts: IBookingAccount[],
  ): IBookingAccount[] {
    const allBookingAccounts: IBookingAccount[] = Object.values(this.allBookingAccounts);
    const usedAccountsHashTable: { [key: number]: IBookingAccount } = {};
    usedBookingAccounts.forEach(a => usedAccountsHashTable[a.id] = a);

    const bookingAccounts: IBookingAccount[] = allBookingAccounts
      .filter(account =>
        this.hasTheSameMasterStructureType(account, structure.masterStructureType)
        && this.hasTheSameAccountingType(account, structure.accountingType)
        && !usedAccountsHashTable[account.id]
      );

    return bookingAccounts;
  }

  /**
   * Gives back the structure's used bookingAccounts, usually used in ngOnInit to get it
   * for each structure
   */
  public getStructureUsedBookingAccounts(structure: IMasterStructure): IBookingAccount[] {
    const bookingAccounts: IBookingAccount[] = [];
    structure.nodes.forEach(node => {
      if (node.bookingAccountId && this.allBookingAccounts[node.bookingAccountId]) {
        bookingAccounts.push(this.allBookingAccounts[node.bookingAccountId]);
      }
    });
    return bookingAccounts;
  }

  public isDropBetweenAccounts(
    confirm: NzFormatBeforeDropEvent,
    isInvalidForEarning: boolean,
    isInvalidForExpense: boolean,
  ): boolean {
    const dragNode: NzTreeNode = confirm.dragNode;
    const targetNode: NzTreeNode = confirm.node;

    if (
      confirm.pos !== 0
      && targetNode.level !== 0
      && targetNode.origin.bookingAccountId
      && !isInvalidForEarning
      && !isInvalidForExpense
    ) {
      const dropParent: NzTreeNode = targetNode.parentNode;
      const dropRelativeNodeIndex: number = dropParent.children
        .findIndex(n => n.key === targetNode.key);
      const closestSiblingNode: NzTreeNode = dropParent
        .children[dropRelativeNodeIndex + confirm.pos];
      const isDropBetweenAccountNodes: boolean = !dragNode.origin.bookingAccountId
        && !!closestSiblingNode?.origin.bookingAccountId;

      return isDropBetweenAccountNodes;
    }
    return false;
  }

  /**
   * if drop is going to be between account nodes, we should cancel it and drop the
   * dragged node at the end of the list
   * */
  public cancelDropIfBetweenAccounts(
    dragNode: NzTreeNode,
    targetNode: NzTreeNode,
    nodesComponent: NzTreeComponent,
  ): void {
    const dragParent: NzTreeNode = dragNode.parentNode;

    if (!dragParent) {
      const nzTreeService: NzTreeService = nodesComponent.nzTreeService;
      nzTreeService.rootNodes = nzTreeService.rootNodes
        .filter(n => n.key !== dragNode.key);
    } else {
      dragParent.children = dragParent.children.filter(n => n.key !== dragNode.key);
    }
    nodesComponent.nzTreeService.dropAndApply(targetNode.parentNode, 0);
  }

  public openBookingAccountsModal(
    nodes: IMasterStructureNodeTemplate[],
    selectedNode: IMasterStructureNodeTemplate,
    refreshNodes: () => void,
    addNode: (
      selectedNode: IMasterStructureNodeTemplate,
      newNode: IMasterStructureNode,
    ) => IMasterStructureNodeTemplate,
    component: Type<MasterStructureBookingAccountsComponent>,
  ): void {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.accounting.accountSelection[this.language],
      nzClosable: true,
      nzContent: component,
      nzComponentParams: {
        selectedNode: selectedNode.origin as any as IMasterStructureNodeTemplate,
        currentStructure: this.getCurrentStructure(),
        bookingAccounts$: this.bookingAccounts$,
      },
      nzFooter: [
        {
          label: wording.general.add[this.language],
          disabled: componentInstance => !componentInstance.selectedAccounts.length,
          onClick: this.onBookingAccountsModalSave(
            nodes,
            selectedNode,
            refreshNodes,
            addNode
          ),
        }
      ]
    });
  }

  private onBookingAccountsModalSave(
    nodes: IMasterStructureNodeTemplate[],
    selectedNode: IMasterStructureNodeTemplate,
    refreshNodes: () => void,
    addNode: (
      selectedNode: IMasterStructureNodeTemplate,
      newNode: IMasterStructureNode,
    ) => IMasterStructureNodeTemplate,
  ): (componentInstance: MasterStructureBookingAccountsComponent) => void {
    return (componentInstance: MasterStructureBookingAccountsComponent) => {
      const bookingAccountIds: number[] = [];
      let switchingNode: IMasterStructureNode;
      let isSwitchingAccountWarningShown = false;

      if (selectedNode.origin.switchingNodeId) {
        switchingNode = this.switchingNodesService.getSwitchingNode(
          nodes,
          selectedNode.origin as any as IMasterStructureNode
        );
      }

      componentInstance.selectedAccounts.forEach(account => {
        const newNode: IMasterStructureNode = this.switchingAccountsService
          .createAccountOrSwitchingAccount(account);
        addNode(selectedNode, newNode);

        if (account.isSwitchingAccount && switchingNode) {
          addNode(switchingNode as IMasterStructureNodeTemplate, newNode);

          if (!isSwitchingAccountWarningShown) {
            this.switchingAccountsService
              .showContainsSwitchingAccountWarning(switchingNode);
            isSwitchingAccountWarningShown = true;
          }
        }

        bookingAccountIds.push(account.id);
      });

      refreshNodes();
      this.addUsedBookingAccount(bookingAccountIds);
      this.modalRef.close();
    };
  }

  public isClosestSiblingAccountType(confirm: NzFormatBeforeDropEvent): boolean {
    const dragNode: NzTreeNode = confirm.dragNode;
    const targetNode: NzTreeNode = confirm.node;

    if (
      confirm.pos !== 0
      && dragNode.parentNode?.key === targetNode.parentNode?.key
      && targetNode.origin.bookingAccountId
    ) {
      const targetParent: NzTreeNode = targetNode.parentNode;
      const targetNodeIndex: number = targetParent.children
        .findIndex(n => n.key === targetNode.key);
      const closestSibling: NzTreeNode = targetParent
        .children[targetNodeIndex + confirm.pos];

      return !closestSibling || closestSibling.origin.bookingAccountId;
    }

    return false;
  }

  public showAccountTypeWarningIfNecessary(
    draggedNode: NzTreeNode,
    isInvalidForEarning: boolean,
    isInvalidForExpense: boolean,
    isInvalidSummationDrop: boolean,
    isInvalidAccountDrop: boolean,
  ): void {
    if (
      (isInvalidForEarning || isInvalidForExpense)
      && !isInvalidAccountDrop
      && !isInvalidSummationDrop
    ) {
      this.confirmationService.confirm({
        wording: draggedNode.origin.bookingAccountId
          ? wording.accounting.accountTypeDoesNotMatchNodeType
          : wording.accounting.nodesTypesDoNotMatch,
        cancelButtonWording: null,
        title: null,
        okButtonWording: wording.general.ok,
      }).subscribe();
    }
  }
}
