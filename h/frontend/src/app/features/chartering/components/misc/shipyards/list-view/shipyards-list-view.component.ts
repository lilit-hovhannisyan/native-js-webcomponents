import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { ShipyardRepositoryService } from 'src/app/core/services/repositories/shipyard-repository.service';
import { IShipyard } from 'src/app/core/models/resources/IShipyard';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/shipyards.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-shipyards-list-view',
  templateUrl: './shipyards-list-view.component.html',
  styleUrls: ['./shipyards-list-view.component.scss']
})
export class ShipyardsListViewComponent implements OnInit {
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IShipyard[]>;

  constructor(
    private shipyardRepo: ShipyardRepositoryService,
    private confirmationService: ConfirmationService,
    private authService: AuthenticationService,
    private router: Router,
    public gridConfigService: GridConfigService,
  ) { }
  public ngOnInit() {
    this.shipyardRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.shipyardRepo.getStream();

    const actions = {
      edit: (id) => this.router.navigate([`${this.router.url}/${id}`]),
      delete: this.deleteClicked,
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    this.gridConfig = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  public deleteClicked = (shipyard: IShipyard) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: shipyard.label },
    }).subscribe(confirmed => confirmed && this.shipyardRepo.delete(shipyard.id, {}).subscribe());
  }
}
