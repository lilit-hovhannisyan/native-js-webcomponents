import { Pipe, PipeTransform } from '@angular/core';
import { SettingsService } from 'src/app/core/services/settings.service';
import * as moment from 'moment';
import { NvLocale, dateFormatTypes, timeFormatTypes } from 'src/app/core/models/dateFormat';

/**
 * This pipe returns the date in chosen system format
 */
@Pipe({
  name: 'systemDate',
})
export class SystemDatePipe implements PipeTransform {
  constructor(
    private settingsService: SettingsService,
  ) { }

  public transform(value: string, withTime?: boolean): string {
    const momentValue: moment.Moment = moment(value);

    if (!momentValue.isValid()) {
      return '';
    }

    const locale: NvLocale = this.settingsService.locale;
    const dateFormat: string = dateFormatTypes[locale];
    const timeFormat: string = timeFormatTypes[locale];

    const format: string = withTime
      ? `${dateFormat} ${timeFormat}`
      : dateFormat;

    return momentValue.format(format);
  }
}
