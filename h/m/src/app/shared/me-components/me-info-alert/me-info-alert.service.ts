import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef } from '@angular/material/snack-bar';
import { takeUntil } from 'rxjs/operators';

import { MeInfoAlertComponent } from '@shared/me-components/me-info-alert/me-info-alert.component';

import { MeInfoAlertI } from './me-info-alert.interface';
import { Subject } from 'rxjs';

@Injectable()
export class MeInfoAlertService {
  constructor(private snackBar: MatSnackBar, private infoAlertConfiguration: MatSnackBarConfig) {}

  openInfoAlert(data: MeInfoAlertI): Subject<string> {
    const subjectToReturn = new Subject<string>();
    const barRef: MatSnackBarRef<MeInfoAlertComponent> = this.snackBar.openFromComponent(MeInfoAlertComponent, {
      panelClass: ['info-snack'],
      ...this.infoAlertConfiguration,
      horizontalPosition: 'left',
      duration: undefined,
      data
    });

    barRef.instance.eventOccurs.pipe(takeUntil(barRef.afterDismissed())).subscribe((event: { eventName: string }) => {
      subjectToReturn.next(event.eventName);
      if (event.eventName === 'exit') {
        this.snackBar.dismiss();
      }
    });
    return subjectToReturn;
  }

  closeInfoAlert(): void {
    this.snackBar.dismiss();
  }
}
