import { Component, OnInit, OnDestroy } from '@angular/core';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { NvGridConfig } from 'nv-grid';
import { Observable, BehaviorSubject, ReplaySubject } from 'rxjs';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/open-item-accounts.config';
import { BaseComponent } from 'src/app/core/abstracts/BaseComponent';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { IMandatorAccountRow } from 'src/app/core/models/resources/IMandatorAccount';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { MandatorAccountMainService } from 'src/app/core/services/mainServices/mandator-account-main.service';
import { filter, switchMap, takeUntil, map } from 'rxjs/operators';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { mandatorNoModifier, MandatorKey, IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { SelectType } from 'src/app/core/models/SelectType';
import { MandatorMainService } from 'src/app/core/services/mainServices/mandator-main.service';
import { MandatorAccountCreateViewComponent } from '../../mandators/detail-view/mandator-account/detail-view/mandator-account-create-view/mandator-account-create-view.component';
import { MandatorAccountEditViewComponent } from '../../mandators/detail-view/mandator-account/detail-view/mandator-account-edit-view/mandator-account-edit-view.component';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { NotificationType } from 'src/app/core/models/INotification';
import { ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';
import { SettingsRepositoryService } from 'src/app/core/services/repositories/settings-repository.service';
import { IStartEndRange } from 'src/app/core/models/resources/ISettings';
import { DEBITOR_CREDITOR } from 'src/app/core/models/enums/debitor-creditor';

@Component({
  selector: 'nv-open-item-accounts-list-view',
  templateUrl: './open-item-accounts-list-view.component.html',
  styleUrls: ['./open-item-accounts-list-view.component.scss']
})
export class OpenItemAccountsListViewComponent extends BaseComponent implements OnInit, OnDestroy {
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IMandatorAccountRow[]>;
  public mandators$: Observable<IMandatorLookup[]>;
  public SelectType = SelectType;
  public mandatorNoModifier = mandatorNoModifier;

  private mandatorId = new BehaviorSubject<MandatorKey>(null);
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  private modalRef?: NzModalRef;
  private debitorCreditorRanges: IStartEndRange<DEBITOR_CREDITOR>[];

  constructor(
    public gridConfigService: GridConfigService,
    bcs: BaseComponentService,
    private mandatorAccountRepo: MandatorAccountRepositoryService,
    private mandatorAccountMainService: MandatorAccountMainService,
    private mandatorRepo: MandatorRepositoryService,
    private mandatorMainService: MandatorMainService,
    private modalService: NzModalService,
    private notificationService: NotificationsService,
    private settingsRepo: SettingsRepositoryService,
  ) {
    super(bcs);
  }

  public ngOnInit() {
    this.initGridConfig();

    this.mandatorRepo.fetchLookups({}).subscribe();
    this.mandators$ = this.mandatorMainService.fetchLookupsWithRolesOnly()
      .pipe(takeUntil(this.componentDestroyed$));

    this.settingsRepo.fetchAll().subscribe(settings => {
      this.debitorCreditorRanges = settings.system.debitorCreditorRanges;
    });

    this.dataSource$ = this.mandatorId
      .pipe(
        takeUntil(this.componentDestroyed$),
        filter(mandatorId => !!mandatorId),
        switchMap((mandatorId) => this.mandatorAccountMainService
          .getStream({ mandatorId })
          .pipe(map(accounts => accounts.filter(a => a.isOpenItem)))
        )
      );
  }

  private initGridConfig() {
    const actions = {
      add: () => this.openDetailViewModal(
        MandatorAccountCreateViewComponent,
        {
          mandatorId: this.mandatorId.value,
          onlyOpenItemFalse: true,
          setOpenItemTrueOnSelect: true,
        },
        wording.accounting.mandatorAccounts[this.settingsService.language]
      ),
      edit: (mandatorAccount: IMandatorAccountRow) => this.openDetailViewModal(
        MandatorAccountEditViewComponent,
        {
          mandatorId: this.mandatorId.value,
          mandatorAccount,
          canEdit: this.authService.canPerformOperationOnCurrentUrl(ACL_ACTIONS.UPDATE),
          showOnlyOpenItem: true,
        },
        wording.general.account[this.settingsService.language]
      ),
      delete: (mandatorAccount: IMandatorAccountRow) =>
        this.deleteRowClicked(mandatorAccount)
    };

    this.gridConfig = getGridConfig(actions, (operation: number) =>
      this.authService.canPerformOperationOnCurrentUrl(operation));

    this.gridConfig.toolbarButtons = this.gridConfig
      .toolbarButtons.map(b => ({ ...b, disabled: () => !this.mandatorId.value }));
  }

  private isInDebitorCreditorRange(mandatorAccount: IMandatorAccountRow): boolean {
    return this.debitorCreditorRanges.some(range => mandatorAccount.no >= range.start
      && mandatorAccount.no <= range.end);
  }

  private deleteRowClicked = (mandatorAccount: IMandatorAccountRow) => {
    if (!mandatorAccount.isOpenItem) {
      return this.notificationService
        .notify(
          NotificationType.Info,
          wording.general.warning,
          wording.general.openItemIsAlreadyFalse
        );
    }

    if (this.isInDebitorCreditorRange(mandatorAccount)) {
      return this.notificationService
        .notify(
          NotificationType.Info,
          wording.general.warning,
          wording.chartering.debitorAndCreditorAreOpenItems
        );
    }

    this.confirmationService.confirm({
      wording: wording.general.deleteAssignmentOpenItem,
      okButtonWording: wording.general.remove,
      cancelButtonWording: wording.general.cancel,
    }).subscribe(confirmed =>
      confirmed && this.mandatorAccountRepo
        .update({ ...mandatorAccount, isOpenItem: false }, {})
        .subscribe()
    );
  }

  public onMandatorSelect(id: MandatorKey) {
    this.mandatorId.next(id);
  }

  private openDetailViewModal(
    component: any,
    componentParams: any,
    title: string
  ): void {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: title,
      nzContent: component,
      nzMaskClosable: true,
      nzComponentParams: {
        ...componentParams,
        closeModal: (res: { refresh?: boolean }) => {
          if (res && res.refresh) {
            this.mandatorId.next(this.mandatorId.value);
          }
          this.modalRef.destroy();
        }
      },
      nzFooter: null
    });
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
