import { AbstractControl, FormGroup } from '@angular/forms';

/**
 * Marks all controls of a form touched
 * @param controls
 */
export const markControlsAsTouched = (form: FormGroup): void => {
  const { controls } = form;
  for (const ctrlName in controls) {
    if (controls.hasOwnProperty(ctrlName)) {
      !controls[ctrlName].touched && controls[ctrlName].markAsTouched();
    }
  }
};

export const markControlAsTouched = (control: AbstractControl): void => {
  if (!control.touched) {
    control.markAsTouched();
  }
};
