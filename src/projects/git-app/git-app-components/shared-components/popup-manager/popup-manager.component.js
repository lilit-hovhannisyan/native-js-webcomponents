import {BaseComponent} from '../../../../../library/core/base-component/base.component.js';
import {basePath} from '../../../data/base.path.js';

export class PopupManagaer extends BaseComponent {
  constructor() {
    super(basePath + 'shared-components/popup-manager/popup-manager.component.css');
  }

  render = () => {
    const config = {
      tagName: 'div',
      attr: {
        class: 'popup-container'
      },
      subElements: [
        {
          tagName: 'button-component',
          attr: {
            class: 'close',
            icon: 'icon-close'
          },
          events: {
            click: this.destroyPopup
          }
        },
        {
          tagName: 'block-component',
          attr: {
            class: 'popup-body',
          },
          subElements: [
            {
              tagName: 'git-user-single',
              attr: {
                userid: this.getAttribute('userid'),
              },
            }
          ]
        }
      ]
    };

    const el = this.createElementWithAttributes(config);
    this.shadow.appendChild(el);
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }

  destroyPopup = () => {
    this.remove();
  }
}