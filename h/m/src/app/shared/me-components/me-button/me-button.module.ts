import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeButtonComponent } from './me-button.component';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [MeButtonComponent],
  imports: [CommonModule, MatButtonModule],
  exports: [MeButtonComponent]
})
export class MeButtonModule {}
