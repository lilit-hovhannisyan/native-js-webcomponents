import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BasePermissionGuard } from '@core-guards/base-permission.guard';
import { MePermissionTypes } from '@shared/me-permission-types';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'view',
    pathMatch: 'full'
  },
  {
    path: 'view',
    loadChildren: () => import('../digital-profile-view/digital-profile-view.module').then((m) => m.DigitalProfileViewModule),
    data: {
      permission: MePermissionTypes.COMPANY_VIEW
    },
    canActivate: [BasePermissionGuard]
  },
  {
    path: 'view/:id',
    loadChildren: () => import('../digital-profile-view/digital-profile-view.module').then((m) => m.DigitalProfileViewModule),
    data: {
      permission: MePermissionTypes.COMPANY_VIEW
    },
    canActivate: [BasePermissionGuard]
  },
  {
    path: 'edit',
    loadChildren: () => import('../digital-profile-edit/digital-profile-edit.module').then((m) => m.DigitalProfileEditModule),
    data: {
      permission: MePermissionTypes.COMPANY_MODIFY
    },
    canActivate: [BasePermissionGuard]
  },
  {
    path: 'edit/:certificateId',
    loadChildren: () => import('../digital-profile-edit/digital-profile-edit.module').then((m) => m.DigitalProfileEditModule),
    data: {
      permission: MePermissionTypes.CERTIFICATE_MODIFY
    },
    canActivate: [BasePermissionGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DigitalProfileRoutingModule {}
