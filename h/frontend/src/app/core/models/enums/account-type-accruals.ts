export enum ACCOUNT_TYPE_ACCRUALS {
  NONE,
  AccountPrepaidExpenses,
  AccountDeferredIncome
}
