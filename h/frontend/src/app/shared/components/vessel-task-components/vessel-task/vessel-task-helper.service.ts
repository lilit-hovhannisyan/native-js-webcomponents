import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { TaskType } from 'src/app/core/models/enums/vessel-registration-tasks';
import { Language } from 'src/app/core/models/language';
import { ITaskTemplate, TaskTemplateKey } from 'src/app/core/models/resources/Task-Templates/ITaskTemplate';
import { ITaskTemplateCategory, TaskTemplateCategoryKey } from 'src/app/core/models/resources/Task-Templates/ITaskTemplateCategory';
import { TaskTemplateCategoryRepositoryService } from 'src/app/core/services/repositories/task-templates/task-template-category-repository.service';
import { TaskTemplateRepositoryService } from 'src/app/core/services/repositories/task-templates/task-template-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { convertToMap } from 'src/app/shared/helpers/general';

@Injectable({
  providedIn: 'root',
})
export class VesselTaskHelperService {
  /** Gives `true` if chosen language is English, otherwise - `false`. */
  public isEnglish: Observable<boolean> = this.settingsService.languageSubject.pipe(
    distinctUntilChanged(),
    map(language => language !== Language.DE),
  );

  constructor(
    private settingsService: SettingsService,
    private taskTemplateCategoryRepositoryService: TaskTemplateCategoryRepositoryService,
    private taskTemplateRepo: TaskTemplateRepositoryService,
  ) { }

  /** Only fetches categories. */
  public fetchCategories(): Observable<ITaskTemplateCategory[]> {
    return this.taskTemplateCategoryRepositoryService.fetchAll({});
  }
  /** Only fetches task-templates. */
  public fetchTaskTemplates(): Observable<ITaskTemplate[]> {
    return this.taskTemplateRepo.fetchAll({});
  }

  /** Fetches templates resources in necessary order. */
  public fetchTemplatesResources(): Observable<void> {
    return this.fetchCategories().pipe(
      switchMap(() => this.fetchTaskTemplates()),
      map(() => void(0)),
    );
  }

  /** Returns task templates with related checklist templates. */
  public getTaskTemplatesStream(taskType: TaskType): Observable<ITaskTemplate[]> {
    return this.taskTemplateCategoryRepositoryService.getStream({type: taskType}).pipe(
      switchMap(([category]) => {
        return this.taskTemplateRepo.getStream({ categoryId: category?.id });
      }),
    );
  }
}
