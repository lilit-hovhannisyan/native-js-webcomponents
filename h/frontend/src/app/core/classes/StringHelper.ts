import { ONLYDIGITS } from '../constants/regexp';

export class StringHelper {
  public static capitalizeFirstLetter(str: string): string {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  public static trimNoneNumericChars(str: string): string {
    return str.replace(ONLYDIGITS, '');
  }
}
