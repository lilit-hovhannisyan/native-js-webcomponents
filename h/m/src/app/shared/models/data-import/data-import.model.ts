export class DataImportModel {
  importMaterials: boolean = false;
  importCollections: boolean = false;
  importUsers: boolean = false;
}
