import { NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';

export const getVesselAccountingGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'VesselsAccountingGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'vesselNo',
        title: wording.chartering.vesselNo,
        width: 250,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'vesselName',
        title: wording.chartering.vesselName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        actOnDoubleClick: true,
        func: row => actions.select(row),
        description: wording.autocomplete.selectRow,
        icon: 'select',
        actOnEnter: true,
        hidden: actions.select === undefined,
        tooltip: wording.general.select,
        name: 'select'
      }
    ],
    toolbarButtons: [],
  });
};
