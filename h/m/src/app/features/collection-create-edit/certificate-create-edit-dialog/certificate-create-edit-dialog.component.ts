import { Component, Inject, Injector, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { EntitiesUpdateI } from '@core-interfaces/entities-update.interface';
import { EntityI } from '@core-interfaces/entity.interface';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { KeyValueI } from '@core-interfaces/key-value.interface';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { MeTranslationService } from '@core-services/me-translation.service';
import { dateRangeValidator } from '@core-validators/custom-validators';
import { DigitalProfileEditStoreService } from '@features/digital-profile-edit/digital-profile-edit-store.service';
import { DigitalProfileEditWebService } from '@features/digital-profile-edit/digital-profile-edit.web-service';
import { DigitalProfileStoreService } from '@features/digital-profile/digital-profile-store.service';
import { DigitalProfileWebService } from '@features/digital-profile/digital-profile.web-service';
import { BaseLoggerComponent } from '@features/tracking-system';
import {
  MARKETPLACE_BASE_API_URL,
  ME_DOMAIN_APPLICATION_ADDRESS,
  ME_DOMAIN_APPLICATION_CERTIFICATE_TO_COMPANY,
  ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_CERTIFICATE
} from '@shared/constants';
import { MeAttributeLayoutTypes } from '@shared/me-attribute-layout-types';
import { MeAttributeTypes } from '@shared/me-attribute-types';
import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';
import { MeSelectInputComponent } from '@shared/me-components/me-attribute/me-select-input/me-select-input.component';
import { MeBasicAlertI } from '@shared/me-components/me-basic-alert/me-basic-alert.interface';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeButtonI } from '@shared/me-components/me-button/me-button.interface';
import { MeDocumentLineI } from '@shared/me-components/me-document-line/me-document-line.interface';
import { MeEmptyContentI } from '@shared/me-components/me-empty-content/me-empty-content-interface';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { MeUploadFileI } from '@shared/me-components/me-upload-file/me-upload-file.interface';
import { MeFileUploadTypes } from '@shared/me-file-upload-types';
import { AttributeModelTypes, DMChoiceAttributeModel } from '@shared/models/attribute/attribute.model';
import { MeSelectInputAttribute } from '@shared/models/attribute/me-select-input-attribute.model';
import { CertificateAssociationModel } from '@shared/models/certificate-association/certificate-association.model';
import { CertificateModel } from '@shared/models/certificate/certificate.model';
import { DigitalProfileModel } from '@shared/models/digital-profile/digital-profile.model';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { EnumValueModel } from '@shared/models/enum/enum-value.model';
import { FileUploadResponseModel } from '@shared/models/file-upload/file-upload-response.model';
import { LayoutGroupModel } from '@shared/models/layout/layout-group.model';
import { LayoutModel } from '@shared/models/layout/layout.model';
import { FileWebService } from '@shared/services/file.web-service';
import { InputAttributeService } from '@shared/services/input-attribute.service';
import { bytesToSize, getFormatedDate } from '@shared/utils';
import { combineLatest, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CertificateCreateEditDialogDataI } from './certificate-create-edit-dialog-data.interface';

@Component({
  selector: 'me-certificate-create-edit-dialog',
  templateUrl: './certificate-create-edit-dialog.component.html',
  styleUrls: ['./certificate-create-edit-dialog.component.scss'],
  providers: [
    FileWebService,
    InputAttributeService,
    DigitalProfileStoreService,
    DigitalProfileEditStoreService,
    // DocumentCreateWebService,
    DigitalProfileEditWebService
  ]
})
export class CertificateCreateEditDialogComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  public certificateModel: CertificateAssociationModel | undefined;

  componentVersion: string = '0.0.1';
  certificateId: string | null = null;
  leaveWarningModel!: MeSweetAlertI;
  applyCheck: boolean = false;

  digitalProfile: DigitalProfileModel[] = [];
  isFirstCertificate: boolean = false;
  emptyCompanyContent!: MeEmptyContentI;

  certificate!: DigitalProfileModel;

  buttonCancel!: { label: string; data: MeButtonI };
  buttonSubmit!: { label: string; data: MeButtonI };
  buttonNext!: { label: string; data: MeButtonI };
  buttonAddLocation!: { label: string; data: MeButtonI };
  buttonAddFirstCertificate!: { label: string; data: MeButtonI };

  layouts: LayoutModel[] = [];
  groups: LayoutGroupModel[] = [];
  attributes: AttributeModelTypes[] = [];

  // For 2nd Tab
  certificates: CertificateModel[] = [];
  certificateTypeAttributeItem!: MeAttributeItem;
  certificateTypesAttribute: DMChoiceAttributeModel = new DMChoiceAttributeModel();
  selectedDocumentType!: string;
  selectedCertificateLogo!: string;

  certificateAssociation: CertificateAssociationModel[] = [];
  selectedcertificateassociated!: CertificateAssociationModel | null;

  certificateAttributes: AttributeModelTypes[] = [];
  certificateInfoLayoutGroups!: LayoutGroupModel[];
  certificateStepOne: string[] = ['generalAttributes'];
  certificateInfoAttributeForm!: FormGroup;
  certificateInfoAttributeItemMap!: Map<string, MeAttributeItem>;

  // uploadImageData!: MeUploadImageI;
  uploadFileData!: MeUploadFileI;
  // uploadedDocument!: MeDocumentModel;
  documentCards: MeDocumentLineI[] = [];
  currentDocument!: MeDocumentModel;

  documentInfoLayoutGroups!: LayoutGroupModel[];
  documentAttributes: AttributeModelTypes[] = [];
  documentInfoAttributeForm!: FormGroup;
  documentInfoAttributeItemMap!: Map<string, MeAttributeItem>;

  materialTypesAttribute: DMChoiceAttributeModel = new DMChoiceAttributeModel();

  private _definitionStoreDataLoadedSubscription!: Subscription;
  private _getI18nStringsSubscription!: Subscription;
  private _updateCompanySubscription!: Subscription;
  private _getCompanySubscription!: Subscription;
  private _getUploadedImageReferenceIDSubscription!: Subscription;
  private _getCertificateSubscription!: Subscription;
  private _updateCertificateSubscription!: Subscription;
  private _getCertificateAssociationSubscription!: Subscription;
  private _certificateSubscription!: Subscription;
  private _certificateAssociationSubscription!: Subscription;
  private _documentSubscription!: Subscription;
  private _checkUniqueAttributesSubscription!: Subscription;

  private isChangeInForm: boolean = false;
  private _takeUntilDestroy$: Subject<void> = new Subject();
  private certificatefileAttributeName = 'certificate';
  private cerDocumentfileAttributeName = 'certDocument';

  i18nStrings: MeTranslationI = {};

  public ready: boolean = false;

  private _isInProgress: boolean = false;
  public get isInProgress(): boolean {
    return this._isInProgress;
  }
  public set isInProgress(progress: boolean) {
    this._isInProgress = progress;
    this.updateSubmitButton();
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: CertificateCreateEditDialogDataI,
    private matDialogRef: MatDialogRef<CertificateCreateEditDialogComponent>,
    injector: Injector,
    private translate: MeTranslationService,
    private definitionsStoreService: DefinitionsStoreService,
    public inputAttributeService: InputAttributeService,
    private meBasicAlertService: MeBasicAlertService,
    private webService: DigitalProfileWebService,
    private webeditService: DigitalProfileEditWebService,
    private digitalProfileStoreService: DigitalProfileStoreService,
    private digitalProfileEditStoreService: DigitalProfileEditStoreService,
    private meSweetAlertService: MeSweetAlertService,
    private fileWebService: FileWebService
  ) {
    super(injector);
    this.certificateModel = data.certificateModel;
    this.certificateId = this.certificateModel?.certificate.oid || '';
  }

  ngOnInit(): void {
    if (this.digitalProfileStoreService.digitalprofile.length > 0) {
      this.digitalProfile = this.digitalProfileStoreService.digitalprofile;
      this.initializeEdit();
    } else {
      this.getdigitalProfile();
    }

    this.getTraslation();

    this.emptyCompanyContent = {
      title: this.i18nStrings.addYourFirstCertificate,
      comment: this.i18nStrings.addYourFirstCertificateDescription,
      buttonText: this.i18nStrings.add,
      emptyIconName: 'empty-certificates'
    };

    this.leaveWarningModel = {
      icon: 'alert-triangle',
      mode: 'warning',
      title: 'Warning',
      message: 'You are leaving page, data will be lost!',
      type: { name: MeSweetAlertTypeEnum.submit, buttons: { submit: 'Leave', cancel: 'Stay on page' } }
    };
  }

  getTraslation(): void {
    this._getI18nStringsSubscription = this.translate.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;
      this.prepareButtons();
    });
  }

  getdigitalProfile(): void {
    this._getCompanySubscription = this.webService.getCompnay().subscribe((val: ArrayResponseI<DigitalProfileModel>) => {
      this.digitalProfile = val.entities;
      this.digitalProfileStoreService.digitalprofile = val.entities;
      this.initializeEdit();
    });
  }

  prepareButtons(): void {
    this.buttonNext = { label: this.i18nStrings.next, data: { type: 'regular', color: 'primary' } };
    this.buttonCancel = { label: this.i18nStrings.cancel, data: { type: 'regular', color: 'secondary' } };
    this.updateSubmitButton();
    this.buttonAddLocation = { label: this.i18nStrings.addLocation, data: { type: 'transparent', color: 'primary' } };
    this.buttonAddFirstCertificate = { label: this.i18nStrings.add, data: { type: 'regular', color: 'secondary' } };
    // this.buttonFinish = { label: this.i18nStrings.finish, data: { type: 'regular', color: 'primary' } };
  }

  updateSubmitButton(): void {
    this.buttonSubmit = {
      label: this.certificateModel ? this.i18nStrings.update : this.i18nStrings.add,
      data: { type: 'regular', color: 'primary', disabled: this.isInProgress }
    };
  }

  initializeEdit(): void {
    const certificatedp = this.digitalProfileStoreService.getDigitalProfile(ME_DOMAIN_APPLICATION_ADDRESS);
    if (certificatedp) {
      this.certificate = certificatedp;
    }

    this._definitionStoreDataLoadedSubscription = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.getCertificates();
      }
    });

    this._certificateSubscription = this.digitalProfileEditStoreService.certificate$.subscribe((data) => {
      if (data) {
        this.certificates = data;
      }
    });

    this._certificateAssociationSubscription = this.digitalProfileEditStoreService.certificateassociation$.subscribe((data) => {
      if (data) {
        this.initializeCertificateDomain();
        this.createDocumentDomain();

        const observableArray = [this.certificateInfoAttributeForm.valueChanges];
        if (this.documentInfoAttributeForm) {
          observableArray.push(this.documentInfoAttributeForm.valueChanges);
        }
        combineLatest(observableArray)
          .pipe(takeUntil(this._takeUntilDestroy$))
          .subscribe(() => {
            if (this.certificateInfoAttributeForm.dirty || this.documentInfoAttributeForm?.dirty) {
              this.applyCheck = true;
            }
          });
      }
    });
  }

  getCertificates(): void {
    this._getCertificateAssociationSubscription = this.webService
      .getCertificateAssociation()
      .subscribe((val: ArrayResponseI<CertificateAssociationModel>) => {
        this.certificateAssociation = val.entities;
        this.digitalProfileEditStoreService.certificateAssociation = this.certificateAssociation;

        this._getCertificateSubscription = this.webService.getCertificate().subscribe((val1: ArrayResponseI<CertificateModel>) => {
          this.certificates = val1.entities;
          this.digitalProfileEditStoreService.certificates = this.certificates;
          this.attachCertificateTypeControl();

          if (this.certificateId) {
            // TODO Check this in the end
            // this.stepper.selectedIndex = 1;
          }
        });
      });
  }

  initializeCertificateDomain(): void {
    const certificatedomain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_CERTIFICATE_TO_COMPANY);

    if (certificatedomain) {
      this.certificateAttributes = this.inputAttributeService.transformAttributesArray(certificatedomain.attributes);

      if (this.certificateId) {
        const data = this.digitalProfileEditStoreService.getAssociationbyCertificateId(this.certificateId);
        if (data) {
          this.isFirstCertificate = false;

          this.inputAttributeService.applyUpdateFormValues(this.certificateAttributes, data);
        }
      }

      if (certificatedomain) {
        let certificategroups = this.inputAttributeService.getLayoutGroups(
          certificatedomain.layouts,
          this.certificateAssociation.length > 0 ? MeAttributeLayoutTypes.UPDATE : MeAttributeLayoutTypes.CREATE
        );
        certificategroups = this.inputAttributeService.generateLayoutRows(certificategroups);

        this.certificateInfoLayoutGroups = this.getLayoutForGroups(certificategroups, this.certificateStepOne);
        if (this.certificateInfoLayoutGroups && this.certificateInfoLayoutGroups.length > 0) {
          this.certificateInfoAttributeForm = this.inputAttributeService.createFormGroup(
            this.getAttributesByLayoutGroups(this.certificateAttributes, this.certificateInfoLayoutGroups)
          );

          this.certificateInfoAttributeForm.setValidators(
            this.certificateInfoAttributeForm.validator
              ? [this.certificateInfoAttributeForm.validator, dateRangeValidator('issuedDate', 'expirationDate')]
              : dateRangeValidator('issuedDate', 'expirationDate')
          );
          this.certificateInfoAttributeForm.updateValueAndValidity();

          this.certificateInfoAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
            this.getAttributesByLayoutGroups(this.certificateAttributes, this.certificateInfoLayoutGroups),
            this.certificateInfoAttributeForm
          );
        }

        if (this.definitionsStoreService.thumbnailProperties) {
          const splitString = this.definitionsStoreService.thumbnailProperties.supportedFiles.split(',');
          this.uploadFileData = {
            fileName: undefined,
            isStatusInvalid: false,
            acceptFileExtensions: `.${splitString.join(',.')}`,
            supportedFiles: this.definitionsStoreService.thumbnailProperties.supportedFiles.replace(/,/g, ', '),
            maxUploadSize: bytesToSize(this.definitionsStoreService.thumbnailProperties.maxSize)
          };
        }
      }
    }
    this.ready = true;
  }

  createDocumentDomain(): void {
    const documentDomain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_CERTIFICATE);

    this.documentAttributes = this.inputAttributeService.transformAttributesArray(documentDomain.attributes);

    let documentgroups = this.inputAttributeService.getLayoutGroups(documentDomain.layouts, MeAttributeLayoutTypes.CREATE);
    documentgroups = this.inputAttributeService.generateLayoutRows(documentgroups);

    this.documentInfoLayoutGroups = this.getLayoutForGroups(documentgroups, ['generalInformation']);
    if (this.documentInfoLayoutGroups && this.documentInfoLayoutGroups.length > 0) {
      this.documentInfoAttributeForm = this.inputAttributeService.createFormGroup(
        this.getAttributesByLayoutGroups(this.documentAttributes, this.documentInfoLayoutGroups)
      );

      this.documentInfoAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
        this.getAttributesByLayoutGroups(this.documentAttributes, this.documentInfoLayoutGroups),
        this.documentInfoAttributeForm
      );
    }
  }

  handleCertificate(form: FormGroup): void {
    if (form?.errors && form.errors.range) {
      this.meBasicAlertService.openBasicAlert({
        mode: 'error',
        title: 'Date validation',
        content: 'Date of Issue should not be grater than Expiration Date'
      });
    } else {
      if (this.certificateModel) {
        this.updateCertificate(form, this.certificateModel);
      } else {
        this.createCertificate(form);
      }
    }
  }

  createCertificate(form: FormGroup): void {
    form.markAllAsTouched();
    if (form.valid) {
      const certificateChanges: EntitiesUpdateI = {
        entities: []
      };

      const certificateEntity: EntityI = {
        domainName: ME_DOMAIN_APPLICATION_CERTIFICATE_TO_COMPANY,
        attributes: {}
      };

      // Add step1 attributes and values to document entity
      if (this.certificateInfoLayoutGroups && this.certificateInfoLayoutGroups.length) {
        this.getAttributesByLayoutGroups(this.certificateAttributes, this.certificateInfoLayoutGroups).forEach((attribute) =>
          this.setAttributeWithValueToEntity(
            certificateEntity.attributes,
            attribute,
            this.certificateInfoAttributeForm,
            this.certificateAttributes
          )
        );
      }
      if (certificateEntity.attributes && certificateEntity.attributes.hasOwnProperty('certDocument')) {
        delete certificateEntity.attributes.certDocument;
      }
      certificateChanges.entities.push(certificateEntity);

      if (this.documentCards.length > 0) {
        certificateChanges.entities.push(this.createDocument('create'));
      }
      this.isInProgress = true;
      this._checkUniqueAttributesSubscription = this.inputAttributeService
        .checkUniqueAttributes(
          '',
          this.certificateAttributes,
          ME_DOMAIN_APPLICATION_CERTIFICATE_TO_COMPANY,
          this.certificateInfoAttributeForm
        )
        .subscribe(
          () => {
            this._updateCertificateSubscription = this.webeditService.createCertificate(certificateChanges).subscribe(
              () => {
                this.isInProgress = false;
                this.applyCheck = false;
                this.onSuccessCertificate(form);
              },
              () => {
                this.isInProgress = false;
              }
            );
          },
          () => {
            this.isInProgress = false;
          }
        );
    }
  }

  updateCertificate(form: FormGroup, selectedassociated: CertificateAssociationModel): void {
    form.markAllAsTouched();

    if (form.valid) {
      const certificateChanges: EntitiesUpdateI = {
        entities: []
      };
      const certificateEntity: EntityI = {
        domainName: selectedassociated.domainName,
        attributes: {}
      };
      if (this.certificateInfoLayoutGroups && this.certificateInfoLayoutGroups.length) {
        this.getAttributesByLayoutGroups(this.certificateAttributes, this.certificateInfoLayoutGroups).forEach((attribute) =>
          this.setAttributeWithValueToEntity(
            certificateEntity.attributes,
            attribute,
            this.certificateInfoAttributeForm,
            this.certificateAttributes,
            ['certificate', 'company']
          )
        );
      }
      if (certificateEntity.attributes && certificateEntity.attributes.hasOwnProperty('certDocument')) {
        delete certificateEntity.attributes.certDocument;
      }
      certificateChanges.entities.push(certificateEntity);

      if (
        selectedassociated.attributes.certDocument ===
          this.certificateInfoAttributeForm.controls[this.cerDocumentfileAttributeName].value &&
        this.currentDocument
      ) {
        certificateChanges.entities.push(this.createDocument());
      } else if (this.documentCards.length > 0) {
        certificateChanges.entities.push(this.createDocument('create'));
      }

      this.isInProgress = true;
      this._checkUniqueAttributesSubscription = this.inputAttributeService
        .checkUniqueAttributes(
          selectedassociated.oid,
          this.certificateAttributes,
          ME_DOMAIN_APPLICATION_CERTIFICATE_TO_COMPANY,
          this.certificateInfoAttributeForm
        )
        .subscribe(
          () => {
            this._updateCertificateSubscription = this.webeditService
              .updateCertificate(selectedassociated.oid, certificateChanges)
              .subscribe(
                () => {
                  this.isInProgress = false;
                  this.applyCheck = false;
                  this.onSuccessCertificate(form);
                },
                () => {
                  this.isInProgress = false;
                }
              );
          },
          () => {
            this.isInProgress = false;
          }
        );
    }
  }

  createDocument(mode?: string): EntityI {
    const documentEntity: EntityI = {
      domainName: ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_CERTIFICATE,
      attributes: {}
    };

    if (mode && mode === 'create') {
      if (this.documentInfoLayoutGroups && this.documentInfoLayoutGroups.length) {
        this.getAttributesByLayoutGroups(this.documentAttributes, this.documentInfoLayoutGroups).forEach((attribute) =>
          this.setAttributeWithValueToEntity(documentEntity.attributes, attribute, this.documentInfoAttributeForm, this.documentAttributes)
        );
      }
    } else if (this.currentDocument) {
      documentEntity.attributes = this.currentDocument.attributes;
    }

    return documentEntity;
  }

  backclick(): void {
    this.navigateToView();
  }

  handleFileFormData(recievedFile: File): void {
    const formData = new FormData();
    formData.append(
      'meta',
      JSON.stringify({
        type: MeFileUploadTypes.certificate
      })
    );
    formData.append('file', recievedFile, recievedFile.name);

    this.fileWebService
      .uploadFile(formData, FileUploadResponseModel)
      .pipe(takeUntil(this._takeUntilDestroy$))
      .subscribe(
        (response) => {
          if (response.attributes.name) {
            this.uploadFileData = {
              ...this.uploadFileData,
              fileName: response.attributes.name,
              showProgress: false
            };
            this.prepareDocumentData(response);
            this.updateDocumentData(response);
          } else {
            this.uploadFileData = {
              ...this.uploadFileData,
              showProgress: false
            };
            this.prepareDocumentData(null);
            this.updateDocumentData(new FileUploadResponseModel());
          }
        },
        (error: ErrorResponseI) => {
          this.uploadFileData = { ...this.uploadFileData, isStatusInvalid: true };
          const basicAlertData: MeBasicAlertI = {
            mode: 'error',
            title: error.error.title,
            content: error.error.details
          };
          this.meBasicAlertService.openBasicAlert(basicAlertData);
        }
      );
  }

  updateDocumentData(data: FileUploadResponseModel): void {
    if (this.documentInfoAttributeForm) {
      this.documentInfoAttributeForm.controls.title.setValue(data.attributes?.name);
      this.documentInfoAttributeForm.controls.primaryContent.setValue(data.oid);
    }

    if (this.certificateInfoAttributeForm) {
      this.certificateInfoAttributeForm.controls[this.cerDocumentfileAttributeName].setValue(data.oid);
    }
  }

  getDocument(documentId: string): void {
    this.webService.getDocument(documentId).subscribe((val: MeDocumentModel) => {
      this.documentCards = [
        {
          id: val.oid,
          oid: val.oid,
          selectionEnabled: false,
          selected: false,
          documentName: val.attributes.title,
          creationDate: getFormatedDate(val.createdOn),
          // documentExtension: document.attributes.type,
          contextMenu: {
            contextMenuItems: [
              {
                key: 'delete',
                value: this.i18nStrings.delete
              }
            ]
          }
        }
      ];
      this.currentDocument = val;
    });
  }

  prepareDocumentData(document: FileUploadResponseModel | null): void {
    if (document) {
      this.documentCards = [
        {
          id: document.oid,
          oid: document.oid,
          selectionEnabled: false,
          selected: false,
          documentName: document.attributes.name,
          creationDate: getFormatedDate(document.createdOn),
          // documentExtension: document.attributes.type,
          contextMenu: {
            contextMenuItems: [
              {
                key: 'delete',
                value: this.i18nStrings.delete
              }
            ]
          }
        }
      ];
    }
  }

  customEvent(eventName: string, payload?: string, item?: MeDocumentLineI): void {
    if (eventName === 'documentContextMenuItemClick') {
      if (payload === 'delete') {
        if (item) {
          this.deleteCertificate(item?.oid);
        }
      }
    }
  }

  deleteCertificate(documentId?: string): void {
    this.uploadFileData = {
      ...this.uploadFileData,
      fileName: '',
      showProgress: false
    };
    this.updateDocumentData(new FileUploadResponseModel());
    if (documentId) {
      this.documentCards = this.documentCards.filter((x) => x.oid !== documentId);
    } else {
      this.documentCards = [];
    }
  }

  private attachCertificateTypeControl(): void {
    this.certificateTypesAttribute.dataTypeClass = MeAttributeTypes.CHOICE;
    this.certificateTypesAttribute.displayName = this.i18nStrings.certificate;
    this.certificateTypesAttribute.internalName = 'certificate';
    this.certificateTypesAttribute.required = true;
    this.certificateTypesAttribute.values = this.getCertificateDataEnum(this.certificateId ? true : false);
    this.certificateTypesAttribute.disabled = this.certificateId ? true : false;

    this.certificateInfoAttributeForm.addControl(
      this.certificateTypesAttribute.internalName,
      new FormControl(this.certificateTypesAttribute.value, Validators.required)
    );

    this.documentInfoAttributeForm.addControl(
      this.certificateTypesAttribute.internalName,
      new FormControl(this.certificateTypesAttribute.value, Validators.required)
    );

    const documentTypesInputAttribute: MeSelectInputAttribute = new MeSelectInputAttribute();
    documentTypesInputAttribute.data = this.certificateTypesAttribute;
    documentTypesInputAttribute.form = this.certificateInfoAttributeForm;

    this.certificateTypeAttributeItem = new MeAttributeItem(MeSelectInputComponent, documentTypesInputAttribute);

    // track changes of document type
    const certificateTypeControl: AbstractControl | null = this.certificateInfoAttributeForm.get(
      this.certificateTypesAttribute.internalName
    );
    if (certificateTypeControl) {
      certificateTypeControl.valueChanges.pipe(takeUntil(this._takeUntilDestroy$)).subscribe((value) => {
        if (value && this.selectedDocumentType !== value) {
          this.selectedCertificateLogo = '';
          this.selectedDocumentType = value;
          this.updateCertificateValue(value);
        }
      });
    }

    // Add certificate Document Upload
    this.certificateInfoAttributeForm.addControl(this.cerDocumentfileAttributeName, new FormControl(''));
  }

  updateCertificateValue(certificateId: string | null): void {
    if (certificateId) {
      const data = this.digitalProfileEditStoreService.getAssociationbyCertificateId(certificateId);
      this.selectedcertificateassociated = data;

      if (data) {
        this.applyUpdateFormValues(this.certificateAttributes, data);
      } else {
        this.certificateInfoAttributeForm.reset();
        this.deleteCertificate();
      }

      if (this.certificateInfoAttributeForm) {
        this.certificateInfoAttributeForm.controls[this.certificatefileAttributeName].setValue(certificateId);
      }

      this.setCertificateLogo(certificateId);
    }
  }

  private setAttributeWithValueToEntity(
    // tslint:disable-next-line:no-any
    entity: KeyValueI<any>,
    attribute: AttributeModelTypes,
    formGroup: FormGroup,
    attr: AttributeModelTypes[],
    imutablecolumn?: string[]
  ): void {
    if (attr.indexOf(attribute) > -1) {
      const hasImmutable = imutablecolumn ? imutablecolumn.filter((x) => x === attribute.internalName).length > 0 : false;
      if (!hasImmutable) {
        if (!this.isChangeInForm) {
          this.checkIfAnyChangeinForm(attribute, formGroup);
        }
        if (
          formGroup.controls.hasOwnProperty(attribute.internalName) &&
          formGroup.controls[attribute.internalName].hasOwnProperty('value') &&
          formGroup.controls[attribute.internalName].value !== null
        ) {
          entity[attribute.internalName] = formGroup.controls[attribute.internalName].value;
        }
      }
    }
  }

  private getAttributesByLayoutGroups(attributes: AttributeModelTypes[], layoutGroups: LayoutGroupModel[]): AttributeModelTypes[] {
    const attributeMap: Map<string, AttributeModelTypes> = new Map();
    attributes.forEach((attribute) => {
      attributeMap.set(attribute.internalName, attribute);
    });

    return layoutGroups.reduce((prev, curr) => {
      return [
        ...prev,
        ...curr.attributes.map((attr) => {
          return attributeMap.get(attr.internalName);
        })
      ];
      // tslint:disable-next-line:no-any
    }, [] as any);
  }

  getLayoutForGroups(groups: LayoutGroupModel[], layoutGroupNames: string[]): LayoutGroupModel[] {
    const layoutGroups = groups.filter((group) => layoutGroupNames.findIndex((name) => group.internalName === name) > -1);

    return layoutGroups;
  }

  private checkIfAnyChangeinForm(attribute: AttributeModelTypes, formGroup: FormGroup): void {
    if (formGroup.controls[attribute.internalName].value !== attribute.value) {
      this.isChangeInForm = true;
    }
  }

  private getCertificateDataEnum(isEdit: boolean): EnumValueModel[] {
    const types: EnumValueModel[] = [];
    this.certificates.forEach((element) => {
      if (isEdit || (!isEdit && !this.digitalProfileEditStoreService.getAssociationbyCertificateId(element.oid))) {
        const enumValue = new EnumValueModel();
        enumValue.displayName = element.attributes.name;
        enumValue.value = element.oid;
        types.push(enumValue);
      }
    });
    return types;
  }

  private setCertificateLogo(val: string): void {
    const certi = this.certificates.filter((x) => x.oid === val);
    if (certi.length > 0) {
      const logoDefault = certi[0].attributes.logoDefault;
      this.selectedCertificateLogo = `${MARKETPLACE_BASE_API_URL}/files/download/${logoDefault}`;
    } else {
      this.selectedCertificateLogo = '';
    }
  }

  // tslint:disable-next-line:no-any
  private applyUpdateFormValues(attributes: AttributeModelTypes[], savedEntity: { attributes: KeyValueI<any> }): AttributeModelTypes[] {
    if (savedEntity.attributes) {
      attributes.forEach((attribute) => {
        const currentValue = savedEntity.attributes[attribute.internalName];
        if (attribute.internalName === this.cerDocumentfileAttributeName) {
          if (currentValue) {
            this.getDocument(currentValue);
          } else {
            this.deleteCertificate();
          }
        }
        attribute.value = currentValue;
        if (this.certificateInfoAttributeForm.controls[attribute.internalName]) {
          this.certificateInfoAttributeForm.controls[attribute.internalName].setValue(currentValue);
        }
      });
    }

    return attributes;
  }

  private onSuccessCertificate(form: FormGroup): void {
    const successSubscription = this.meSweetAlertService.getDataBackFromSweetAlert().subscribe(() => {
      form.markAsUntouched();
      successSubscription.unsubscribe();
      this.navigateToView();
    });

    const successAlertModel: MeSweetAlertI = {
      mode: 'success',
      icon: 'check',
      type: {
        name: MeSweetAlertTypeEnum.confirm,
        buttons: {
          confirm: this.i18nStrings.close
        }
      },
      title: this.i18nStrings.success,
      message: this.certificateModel ? this.i18nStrings.certificateUpdated : this.i18nStrings.certificateAdded
    };
    this.meSweetAlertService.openMeSweetAlert(successAlertModel);
    this.matDialogRef.close(true);
  }

  private navigateToView(): void {
    // TODO Check this part
    // this.router.navigate([`digital-profile/view`]);
  }

  ngOnDestroy(): object {
    if (this._definitionStoreDataLoadedSubscription) {
      this._definitionStoreDataLoadedSubscription.unsubscribe();
    }
    if (this._getI18nStringsSubscription) {
      this._getI18nStringsSubscription.unsubscribe();
    }
    if (this._updateCompanySubscription) {
      this._updateCompanySubscription.unsubscribe();
    }
    if (this._getCompanySubscription) {
      this._getCompanySubscription.unsubscribe();
    }
    if (this._getUploadedImageReferenceIDSubscription) {
      this._getUploadedImageReferenceIDSubscription.unsubscribe();
    }
    if (this._getCertificateSubscription) {
      this._getCertificateSubscription.unsubscribe();
    }
    if (this._updateCertificateSubscription) {
      this._updateCertificateSubscription.unsubscribe();
    }
    if (this._getCertificateAssociationSubscription) {
      this._getCertificateAssociationSubscription.unsubscribe();
    }
    if (this._certificateSubscription) {
      this._certificateSubscription.unsubscribe();
    }
    if (this._certificateAssociationSubscription) {
      this._certificateAssociationSubscription.unsubscribe();
    }
    if (this._documentSubscription) {
      this._documentSubscription.unsubscribe();
    }
    if (this._checkUniqueAttributesSubscription) {
      this._checkUniqueAttributesSubscription.unsubscribe();
    }
    return super.ngOnDestroy();
  }
}
