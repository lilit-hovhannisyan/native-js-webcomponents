import { IUserAccountConfigGeneral, IUser } from 'src/app/core/models/resources/IUser';
import { SessionInfo } from './../models/sessionInfo';
import { SitemapEntry } from './../../core/constants/sitemap/sitemap-entry';
import { IRole, Permissions } from './../../core/models/resources/IRole';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpEvent } from '@angular/common/http';
import { map, shareReplay, tap } from 'rxjs/operators';
import { LoginData } from '../models/loginData';
import { IDataResponseBody } from '../../core/models/responses/IDataResponseBody';
import { AppException } from '../../core/classes/exceptions/AppException';
import { HttpPost } from '../../core/models/http/http-post';
import { Observable, BehaviorSubject, from, Subject } from 'rxjs';
import { SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { Router } from '@angular/router';
import { RequestsService } from 'src/app/core/services/requests.service';
import { CacheService } from 'src/app/shared/services/cache.service';

/**
 * A bit mask of actions that can be performed in a zone. Uses for checking user
 * permissions.
 */
export enum ACL_ACTIONS {
  CREATE = 1,   // 0001
  READ = 2, // 0010
  UPDATE = 4, // 0100
  DELETE = 8, // 1000
}
export const localStorageKey = '9fc9c5f6-55ab-4818-9108-432691c126f8';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private emptySessionData: SessionInfo | any = {
    user: {},
    tokenResource: {},
    roles: []
  };

  /**
   * Usually used to check if user is logging out in order to clear the streams in Abstract Repository
   */
  public isLoggedInSubject$ = new BehaviorSubject<boolean>(this.isLoggedIn());

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private requestsService: RequestsService,
    private cacheService: CacheService,
  ) { }

  public login(dto?: LoginData): Observable<IDataResponseBody<SessionInfo>> {
    const request = new HttpPost('/api/user-account/login', dto);
    return this.httpClient.request(request).pipe(
      map((res: HttpResponse<IDataResponseBody<SessionInfo>>) => res.body || this.emptySessionData),
      shareReplay(1)
    );
  }

  public sendLogoutRequest(): Observable<HttpEvent<any>> {
    const session = this.getSessionInfo();
    const request = new HttpPost('/api/user-account/logout', session.tokenResource);
    return this.httpClient.request(request);
  }

  public logout(options?: { redirectUrl?: string, url?: string, queryParams?: any }): void {
    const navigated$ = from(this.router.navigate(
      [options && options.url
        ? options.url
        : '/auth/login'
      ], {
      queryParams: options && options.queryParams
        ? options.queryParams
        : ({
          redirectUrl: options && options.redirectUrl,
        }),
      state: { systemAction: true }
    }));

    navigated$.subscribe(navigated => navigated && this.clearSessionInfo());
  }

  public isLoggedIn(): boolean {
    return !!this.getSessionInfo().tokenResource.accessToken;
  }

  public getSessionInfo(): SessionInfo {
    const item = localStorage.getItem(localStorageKey);
    let sessionInfo = this.emptySessionData;

    if (item) {
      try {
        sessionInfo = <SessionInfo>JSON.parse(item);
      } catch (error) {
        console.error('Could not parse session info from local-storage.', error);
      }
    }

    return sessionInfo;
  }

  public getUser = (): IUser => {
    return this.getSessionInfo().user;
  }

  /**
   * Used to persist the session info in local storage.
   * Subsequent calls to getSessionInfo will return the new data.
   */
  private storeSessionInfo(session: SessionInfo): void {
    localStorage.setItem(localStorageKey, JSON.stringify(session));
  }

  /**
   * This method should be used to propagate changes
   * to the user such as a language change.
   * TODO: after updating the user, the session info should be refetched.
   */
  public storeUserConfigInfo(config: IUserAccountConfigGeneral): void {
    const session = this.getSessionInfo();
    this.storeSessionInfo({ ...session, user: { ...session.user, ...config } });
  }

  public updateProfile = (user: IUser) => {
    const session = this.getSessionInfo();
    this.storeSessionInfo({ ...session, user: { ...session.user, ...user } });
  }

  public updateRoles = (roles: IRole[]) => {
    const session: SessionInfo = this.getSessionInfo();
    this.storeSessionInfo({ ...session, roles });
  }

  public handleSessionInfoResponse = (content: IDataResponseBody<SessionInfo>, redirectUrl: string): void => {
    this.storeSessionInfo(content.data);
    this.isLoggedInSubject$.next(this.isLoggedIn());
    this.router.navigate([redirectUrl]);
  }

  public clearSessionInfo() {
    this.sendLogoutRequest().subscribe();
    this.storeSessionInfo(this.emptySessionData);
    this.isLoggedInSubject$.next(false);
    this.requestsService.removeRequests();
    this.clearStorage();
    this.cacheService.clear();
  }

  private clearStorage(): void {
    sessionStorage.removeItem(localStorageKey);
    localStorage.clear();
  }

  public refreshToken = (): Observable<SessionInfo> => {
    const refreshToken = this.getSessionInfo().tokenResource.refreshToken;
    const request = new HttpPost('/api/user-account/token/refresh', { refreshToken });

    const response = this.httpClient.request(request).pipe(
      map((res: HttpResponse<IDataResponseBody<SessionInfo>>) => res.body.data),
      tap(sessionInfo => { this.storeSessionInfo(sessionInfo); }),
      shareReplay(1)
    );
    return response;
  }


  /**
   * Returns a flattened aggregated list of all user permissions,
   * extraced from the roles in the session object.
   */
  public getPermissions(): Permissions {
    const { roles } = this.getSessionInfo();
    const permissions = {};
    roles.forEach(role => {
      Object.entries(role.permissions).forEach(([zone, operation]) => {
        permissions[zone] = permissions[zone]
          ? permissions[zone] | operation
          : operation;
      });
    });

    return permissions;
  }

  /**
   * returns list of ids of roles
   */
  public getRolesIds = (): number[] => this.getSessionInfo().roles.map(r => r.id);

  /**
   * This function can be used to check if user has required
   * permissions for particular operation.
   *
   * A user can perform an operation if he/she is a superadmin
   * or has the required permissions set in at least one of his/her roles.
   *
   * Permissions on a more specific zone always implies the right for a more
   * abstract zone. e.g.: 'system.misc.companies' includes 'system.misc'
   * so a user with the permission { system.misc.companies:  1 } can access
   * 'system.misc' automatically. This is needed so the the user can
   * navigate to child routes.
   */
  public can(operation: number, zoneId: string): boolean {
    const { user } = this.getSessionInfo();
    if (!this.getSessionInfo()) {
      throw new AppException('session info does not exist!');
    }

    if (user.isSuperAdmin || user.isAdmin) {
      return true;
    }

    const permissions = this.getPermissions();

    return (permissions[zoneId] & operation) === operation;
  }

  public getZoneByUrl = (url: string): string => {
    return SitemapEntry.getByUrl(url).toZone();
  }

  public canPerformOperationOnNode(operation: number, node: SitemapNode) {
    const zoneId = SitemapEntry.getByNode(node).toZone();

    return this.can(operation, zoneId);
  }

  public canPerformOperationOnCurrentUrl(operation: number) {
    const zoneId = SitemapEntry.getByUrl(this.router.url)?.toZone();

    return this.can(operation, zoneId);
  }

  public canRead(zoneId: string) {
    return this.can(ACL_ACTIONS.READ, zoneId);
  }

  public canCreate(zoneId: string) {
    return this.can(ACL_ACTIONS.CREATE, zoneId);
  }

  public canUpdate(zoneId: string) {
    return this.can(ACL_ACTIONS.UPDATE, zoneId);
  }

  public canDelete(zoneId: string) {
    return this.can(ACL_ACTIONS.DELETE, zoneId);
  }

  public canAccessUrl(url: string): boolean {
    return this.canAccessEntry(SitemapEntry.getByUrl(url));
  }

  public canAccessNode(node: SitemapNode): boolean {
    return this.canAccessEntry(SitemapEntry.getByNode(node));
  }

  public canAccessEntry(entry: SitemapEntry | undefined) {

    if (!entry) { return false; }
    if (entry.node.freeAccess) { return true; }

    const { user } = this.getSessionInfo();
    if (user.isAdmin) { return true; }

    const zoneId = entry.toZone();
    const permissions = this.getPermissions();

    return permissions[zoneId] !== undefined
      ? this.canRead(zoneId)
      : this.isThereAnyPermissionWithAccess(permissions, zoneId);
  }

  private isThereAnyPermissionWithAccess(
    permissions: Permissions,
    zoneId: string,
  ): boolean {
    return Object.entries(permissions).some(([curZoneId, permission]) =>
      new RegExp(`${zoneId}([.]|$)`).test(curZoneId) &&
        ((permission & ACL_ACTIONS.READ) === ACL_ACTIONS.READ)
      );
  }
}
