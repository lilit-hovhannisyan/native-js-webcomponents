import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IVesselRegister, IVesselRegisterCreate, IVesselRegisterDTO, VesselRegisterKey } from '../../models/resources/IVesselRegister';
import { RepositoryService } from '../repository.service';
import { VesselKey } from '../../models/resources/IVessel';

export interface IVesselRegisterRepoParams extends RepoParams<IVesselRegister> {
  queryParams?: {
    vesselId: VesselKey;
  };
}

@Injectable({
  providedIn: 'root'
})
export class VesselRegisterRepositoryService extends AbstractRepository<
  IVesselRegister,
  IVesselRegisterDTO,
  IVesselRegisterCreate,
  IVesselRegisterRepoParams,
  VesselRegisterKey
> {
  protected resourceType = 'VesselRegister';
  protected sortKey = 'from';
  protected sortAsce = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/vessel-registers';
  }

  protected postProcess(item: IVesselRegisterDTO): IVesselRegister { return item; }

}
