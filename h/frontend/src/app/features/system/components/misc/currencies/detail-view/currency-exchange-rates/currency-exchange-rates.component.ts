import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { IExchangeRate } from 'src/app/core/models/resources/IExchangeRate';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { ActivatedRoute } from '@angular/router';
import { getExchangeRatesGridConfig } from 'src/app/core/constants/nv-grid-configs/exchange-rates.config';
import { ExchangeRateRepositoryService } from 'src/app/core/services/repositories/exchange-rate-repository.service';
import * as moment from 'moment';
import { NvGridConfig } from 'nv-grid';
import { tap } from 'rxjs/operators';
import { BrowserTitleService } from 'src/app/routing/services/browser-title.service';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  selector: 'nv-currency-exchange-rates',
  templateUrl: './currency-exchange-rates.component.html',
  styleUrls: ['./currency-exchange-rates.component.scss']
})
export class CurrencyExchangeRatesComponent implements OnInit {
  private labelKey: string;

  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IExchangeRate[]>;

  constructor(
    private currencyRepo: CurrencyRepositoryService,
    public gridConfigService: GridConfigService,
    public route: ActivatedRoute,
    private exchangeRateRepo: ExchangeRateRepositoryService,
    private browserTitleService: BrowserTitleService,
    private settingsService: SettingsService,
  ) { }

  public ngOnInit(): void {
    this.labelKey = getLabelKeyByLanguage(this.settingsService.language);
    const currencyId = this.route.parent.snapshot.params.id;
    this.currencyRepo.fetchOne(currencyId, {})
      .pipe(
        tap(currency =>
          this.browserTitleService.setTitleWithInfo(currency[this.labelKey]))
      )
      .subscribe(currency => {

        this.gridConfig = getExchangeRatesGridConfig(currency.isoCode);
        this.dataSource$ = this.exchangeRateRepo.fetchAll({
          queryParams: {
            startDate: moment().subtract(365, 'days').startOf('day').format('YYYY-MM-DD'),
            endDate: moment().startOf('day').format('YYYY-MM-DD'),
            currencyId: currencyId,
          }
        });
      });
  }
}
