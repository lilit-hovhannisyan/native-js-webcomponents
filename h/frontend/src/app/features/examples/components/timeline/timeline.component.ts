import { Component } from '@angular/core';
import { TimelineItem } from '../../../../core/models/TimelineItem';

@Component({
  templateUrl: './timeline.component.html',
})
export class TimelineExampleComponent {

  public timelineData: any;

  constructor() {
    this.timelineData = this.getItems();
  }

  public getItems(): TimelineItem[] {
    const mocks = new Array(5).fill({});
    return mocks.map((e, index) => {
      const item = new TimelineItem();
      item.timestamp = new Date().toISOString();
      item.activity = `activity-${index + 1}`;
      item.initiator = {
        id: `id-${index + 1}`,
        name: `name-${index + 1}`
      };
      return item;
    });
  }
}
