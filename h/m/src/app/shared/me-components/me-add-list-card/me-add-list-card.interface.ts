export interface MeAddListCardI {
  firstName?: string;
  lastName?: string;
  initials?: string;
  uid?: string;
  oid: string;
  groupName?: string;
  groupRole?: string;
  description?: string;
  thumbnail: string;
  type: 'user' | 'group';
  showButton: boolean;
  isInGroup?: boolean;
}
