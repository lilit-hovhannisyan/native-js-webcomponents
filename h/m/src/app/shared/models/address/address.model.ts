import { Type } from 'class-transformer';
import { BaseModel } from '../base-model/base-model';
import { AddressAttributesExtendedModel } from './address-attributes-extended.model';
import { AddressAttributesModel } from './address-attributes.model';

export class AddressModel extends BaseModel {
  domainName: string = '';
  oid: string = '';
  @Type(() => AddressAttributesModel)
  attributes: AddressAttributesModel = new AddressAttributesModel();
  @Type(() => AddressAttributesExtendedModel)
  attributesExtended: AddressAttributesExtendedModel = new AddressAttributesExtendedModel();
  creatorOID: string = '';
  ownerOID: string = '';
}
