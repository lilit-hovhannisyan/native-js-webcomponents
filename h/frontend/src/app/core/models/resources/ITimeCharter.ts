import { IVoyage } from './IVoyage';
import { IDebitorCreditor } from 'src/app/core/models/resources/IDebitorCreditor';
import { ICompany } from './ICompany';
import { IWithActive } from './IWithActive';
import { IResource } from './IResource';

export type TimeCharterKey = number;

interface ISharedProps extends IWithActive {
  voyageId: number;
  from: string;
  until?: string;
  debitorCreditorId: number; // charterer
  billingAddress?: string;
  charterParty?: string;
  charterPartyDate?: string;
  laycanFrom?: string;
  laycanUntil?: string;
}

export interface ITimeCharterBase extends ISharedProps, IResource<number> { }

export interface ITimeCharterCreate extends ISharedProps { }

export interface ITimeCharterDTO extends ITimeCharterBase { }

export interface ITimeCharter extends ITimeCharterDTO { }

export interface ITimeCharterFull extends ITimeCharter {
  company: ICompany;
  debitorCreditor: IDebitorCreditor;
  voyage: IVoyage;
}
