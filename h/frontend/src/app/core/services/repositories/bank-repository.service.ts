import { BankKey } from './../../models/resources/IBank';
import { Injectable } from '@angular/core';
import { IBank, IBankDTO, IBankCreate } from '../../models/resources/IBank';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { TTL_LONG } from 'src/app/shared/services/resource-tracker.service';

@Injectable({
  providedIn: 'root'
})
export class BankRepositoryService extends AbstractRepository<
  IBank,
  IBankDTO,
  IBankCreate,
  RepoParams<IBank>,
  BankKey
>  {
  protected resourceType = 'Bank';
  protected resourceTTL = TTL_LONG;
  protected useCachePerDefault = true;
  public sortAsce = true;
  public sortKey = 'name';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/banks/';
  }

  protected postProcess(item: IBankDTO): IBank { return item; }
}
