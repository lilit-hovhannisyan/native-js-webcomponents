import { Component, OnInit, OnDestroy, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Subscription, combineLatest, Observable, ReplaySubject } from 'rxjs';
import { map, distinctUntilChanged, takeUntil, tap, filter, switchMap, startWith } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { ICompany, ICompanyCreate, CompanyKey } from 'src/app/core/models/resources/ICompany';
import { Validators } from 'src/app/core/classes/validators';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { CompanyRepositoryService, CompanyRelations } from 'src/app/core/services/repositories/company-repository.service';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { AccountService } from 'src/app/core/services/account.service';
import { IUpload } from 'src/app/core/models/resources/IUpload';
import { SalutationRepositoryService } from 'src/app/core/services/repositories/salutation-repository.service';
import { ISalutation } from 'src/app/core/models/resources/ISalutation';
import { ITitle } from 'src/app/core/models/resources/ITitle';
import { CompaniesHelperService } from '../companies.helper.service';
import { AddressMainService } from 'src/app/core/services/mainServices/address-main.service';
import { mandatorNoModifier } from 'src/app/core/models/resources/IMandator';
import { getCompanyLegalForm, COMPANY_LEGAL_FORM, COMPANY_LEGAL_FORM_TYPE, getCompanyLegalFormTypes } from 'src/app/core/models/enums/company-legal-form';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { ICompanyBasicFormControls, companyBasicFormControlsNames } from './company-basic-helper';
import { IBusinessName } from 'src/app/core/models/resources/IBusinessName';
import { uniqueId } from 'lodash';
import { TitleRepositoryService } from 'src/app/core/services/repositories/title-repository.service';

@Component({
  selector: 'nv-basic',
  templateUrl: './company-basic.component.html',
  styleUrls: ['./company-basic.component.scss']
})
export class CompanyBasicComponent extends ResourceDetailView<ICompany, ICompanyCreate>
  implements OnInit, OnDestroy {

  protected repoParams = {};
  protected resourceDefault = { isActive: true, isPrivatePerson: true, businessNames: [] } as ICompany;

  private legalNode: SitemapNode = sitemap.system.children.misc.children.companies.children.id.children.legal;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  private fieldsSubscription: Subscription;
  private typedCompanyName: string;
  private id: CompanyKey | string; // can be 'new'

  public routeBackUrl = this.url(sitemap.system.children.misc.children.companies);
  public salutations$: Observable<ISalutation[]>;
  public titles$: Observable<ITitle[]>;
  public canCheckVatId$: Observable<boolean>;
  public relations: CompanyRelations = {};
  public companyLegalForms: ISelectOption<COMPANY_LEGAL_FORM>[] = getCompanyLegalForm();
  public companyLegalFormTypes: ISelectOption<COMPANY_LEGAL_FORM_TYPE>[] = [];
  public controls: ICompanyBasicFormControls = {} as ICompanyBasicFormControls;
  public formElement: ElementRef;
  public businessNameHasError: boolean;


  constructor(
    private fb: FormBuilder,
    public accountService: AccountService,
    private companyService: CompanyRepositoryService,
    private companiesHelperService: CompaniesHelperService,
    protected salutationService: SalutationRepositoryService,
    protected titlesService: TitleRepositoryService,
    private addressService: AddressMainService,
    public route: ActivatedRoute,
    baseComponentService: BaseComponentService,
    private cdr: ChangeDetectorRef,
  ) {
    super(companyService, baseComponentService);
  }

  public ngOnInit(): void {
    this.salutations$ = this.salutationService.getStream();
    this.titles$ = this.titlesService.getStream();
    const { id } = this.route.parent.snapshot.params;
    this.id = id;

    if (id === 'new') {
      this.enterCreationMode();
    } else {
      this.loadResourceWithRelations(id);
    }
  }

  public getTitle = () => this.resource.companyName;

  public init(company: ICompany = this.resourceDefault): void {
    this.companiesHelperService.legalTabDataMissing$.next(!company.legalForm);
    this.initForm(this.fb.group({
      salutationId: [company.salutationId, Validators.required],
      titleId: [company.titleId, Validators.required],
      isPrivatePerson: [company.isPrivatePerson, Validators.required],
      firstName: [company.firstName, [Validators.maxLength(50)]],
      lastName: [company.lastName, [Validators.maxLength(50)]],
      companyName: [company.companyName, [Validators.required, Validators.maxLength(100)]],
      taxNo: [company.taxNo, [Validators.maxLength(15)]],
      vatId: [company.vatId, [Validators.maxLength(20)]],
      isActive: [company.isActive],
      logoId: [company.logoId],
      vatIdValid: [null],
      mandator: [this.getMandatorName()],
      debitorCreditor: [this.getDebitorCreditorName(company)],
      legalForm: [company.legalForm],
      legalFormType: [company.legalFormType],
      isLegalDepartmentResponsible: [company.isLegalDepartmentResponsible],
      businessNames: [company.businessNames]
    }));

    companyBasicFormControlsNames.forEach(c => this.controls[c] = this.form.get(c));

    this.initCanCheckVatId(company.id);
    const disabledFields = ['mandator', 'debitorCreditor'];

    this.companiesHelperService.companyHasLegalData$.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(legalData => {
      if (legalData) {
        disabledFields.push('legalForm');
        disabledFields.push('legalFormType');
      }
    });

    if (!company.legalFormType && !disabledFields.includes('legalFormType')) {
      disabledFields.push('legalFormType');
    }
    this.setDisabledFields(disabledFields);
    this.initFormMode();
    this.setCompanyLegalFormTypes(company.legalForm);
    this.enableOrDisableLegalFormType();
    this.setLegalNodeVisibilityMechanism();
    this.cdr.detectChanges();
  }

  private setLegalNodeVisibilityMechanism(): void {
    const { isPrivatePerson } = this.form.controls;

    isPrivatePerson.valueChanges.pipe(
      distinctUntilChanged(),
      startWith(isPrivatePerson.value),
    ).subscribe(value => this.legalNode.hidden = value);
  }

  private initFormMode() {
    this.typedCompanyName = this.form.value.companyName;

    if (this.inEditMode.value) {
      this.setFormMode(this.form.value.isPrivatePerson);
    }
    const isPrivatePersonCtrl = this.controls.isPrivatePerson;

    this.inEditMode
      .pipe(
        takeUntil(this.componentDestroyed$),
        distinctUntilChanged(),
        filter(inEdit => inEdit),
        switchMap(() => isPrivatePersonCtrl.valueChanges
          .pipe(startWith(isPrivatePersonCtrl.value))),
        tap(isPrivatePerson => this.setFormMode(isPrivatePerson))
      ).subscribe();
  }

  private setFormMode(isPrivatePerson: boolean) {
    if (isPrivatePerson) {
      this.switchToPrivatePersonMode();
    } else {
      this.switchToCompanyMode();
    }

    this.updateValuesAndValidity();
  }

  private updateValuesAndValidity() {
    this.controls.salutationId.updateValueAndValidity();
    this.controls.titleId.updateValueAndValidity();
    this.controls.firstName.updateValueAndValidity();
    this.controls.lastName.updateValueAndValidity();
    this.controls.companyName.updateValueAndValidity();
  }

  private switchToPrivatePersonMode() {
    const { salutationId, titleId, firstName, lastName, companyName } = this.form.controls;

    salutationId.setValidators([Validators.required]);
    titleId.setValidators([Validators.required]);
    firstName.setValidators([Validators.required, Validators.maxLength(50)]);
    lastName.setValidators([Validators.required, Validators.maxLength(50)]);
    companyName.setValidators([Validators.maxLength(100)]);
    this.disableField('companyName');

    this.fillCompanyName();
  }

  private switchToCompanyMode() {
    const { salutationId, titleId, firstName, lastName, companyName } = this.form.controls;

    salutationId.setValidators(null);
    titleId.setValidators(null);
    firstName.setValidators([Validators.maxLength(50)]);
    lastName.setValidators([Validators.maxLength(50)]);
    companyName.setValidators([Validators.required]);
    companyName.setValue(this.typedCompanyName);
    companyName.disable();

    if (this.fieldsSubscription) {
      this.fieldsSubscription.unsubscribe();
    }
  }

  private fillCompanyName() {
    const {
      firstName: firstNameControl,
      lastName: lastNameControl,
      companyName
    } = this.form.controls;

    this.typedCompanyName = companyName.value;
    this.fieldsSubscription = combineLatest([
      lastNameControl.valueChanges,
      firstNameControl.valueChanges,
    ])
      .pipe(
        takeUntil(this.componentDestroyed$),
        map(([lastName, firstName]) => [lastName || '', firstName || ''])
      )
      .subscribe(([lastName, firstName]) => {
        if (lastName && firstName) {
          companyName.setValue(`${lastName}, ${firstName}`);
        } else {
          companyName.setValue(lastName || firstName);
        }
      });
  }

  private initCanCheckVatId(companyId: number): void {
    if (!this.creationMode) {
      this.canCheckVatId$ =
        this.addressService.fetchAll({ queryParams: { companyId } })
          .pipe(
            map(addresses => {
              const address = addresses.find(a => a.isTypeDefault && a.addressTypeId === 1);
              return address
                && address.country
                && address.country.euMember
                && address.country.isoAlpha2 !== 'DE';
            })
          );
    }
  }

  public submit(): void {
    let freshResource: ICompany = this.creationMode
      ? this.form.getRawValue()
      : ({ ...this.resource, ...this.form.getRawValue() });

    if (!freshResource.isPrivatePerson && !freshResource.businessNames.length) {
      this.businessNameHasError = true;
      return;
    }

    if (this.form.invalid) { return; }

    if (freshResource.isPrivatePerson) {
      freshResource = { ...freshResource, legalForm: null, legalFormType: null };
    }

    // we need to remove randomly added unique id on new names
    this.id = this.route.parent.snapshot.params.id;
    freshResource = {
      ...freshResource,
      businessNames: freshResource.businessNames
        .map(bn => {
          if (!bn.companyId) {
            return {
              ...bn,
              id: null
            };
          }
          return bn;
        })
        .map(b => ({
          ...b,
          companyId: this.id === 'new' ? null : this.id as CompanyKey
        }))
    };

    const action$: Observable<ICompany> = this.creationMode
      ? this.createResource(freshResource)
      : this.updateResource(freshResource);

    action$.subscribe(res => {
      this.companiesHelperService.generateTitle(res);
      this.companiesHelperService.setCompany(res.id);
    });
  }

  public updateLogo(upload: IUpload): void {
    this.form.patchValue({ logoId: upload.id });
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public validateVatId(vatId: string): void {
    this.companyService.validateVatId(vatId)
      .subscribe(
        isValid => this.controls.vatIdValid.patchValue(isValid),
        () => this.controls.vatIdValid.patchValue(null)
      );
  }

  private loadResourceWithRelations = (id: CompanyKey): void => {
    this.companyService.fetchRelations(+id)
      .pipe(
        tap(relations => {
          this.relations = relations;
          this.loadResource(id);
        })
      ).subscribe();
  }

  private getMandatorName = (): string => {
    const { mandator } = this.relations;

    return mandator
      ? `${mandatorNoModifier(mandator.no)} ${mandator.name}`
      : '';
  }

  private getDebitorCreditorName = (company: ICompany): string => {
    const { debitorCreditor } = this.relations;

    return debitorCreditor
      ? `${debitorCreditor.no} ${company.companyName}`
      : '';
  }

  public onLegalFormChange(legalForm: COMPANY_LEGAL_FORM) {
    const legalFormTypeControl = this.controls.legalFormType;
    this.setCompanyLegalFormTypes(legalForm);
    const defaultType = this.companyLegalFormTypes.find(c => c.default);
    if (defaultType) {
      legalFormTypeControl.patchValue(defaultType.value);
    } else {
      legalFormTypeControl.patchValue(null);
    }
  }

  private setCompanyLegalFormTypes(legalForm: COMPANY_LEGAL_FORM) {
    const legalFormTypeControl = this.controls.legalFormType;
    this.companyLegalFormTypes = getCompanyLegalFormTypes(legalForm);
    if (!this.companyLegalFormTypes.length) {
      legalFormTypeControl.patchValue(null);
    }
  }

  private enableOrDisableLegalFormType(): void {
    const { legalForm } = this.controls;

    legalForm.valueChanges
      .pipe(
        startWith(legalForm.value)
      )
      .subscribe(() => {
        if (legalForm.value === COMPANY_LEGAL_FORM.LimitedPartnership
          || legalForm.value === COMPANY_LEGAL_FORM.CompanyWithLimitedLiability
        ) {
          this.enableLegalFormType();
        } else {
          this.disableLegalFormType();
        }
      });
  }

  private enableLegalFormType(): void {
    if (this.form.enabled) {
      this.controls.legalFormType.enable();
    }
    this.controls.legalFormType.setValidators([Validators.required]);
  }

  private disableLegalFormType(): void {
    this.controls.legalFormType.patchValue(null);
    this.controls.legalFormType.disable();
    this.controls.legalFormType.clearValidators();
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

  public onBusinessNameAdd(businessName: IBusinessName): void {
    const businessNamesCtrl = this.form.get('businessNames');
    const businessNames: IBusinessName[] = [...businessNamesCtrl.value];
    // edited
    if (businessName.id) {
      const editedBusinessNameIndex = businessNames
        .findIndex(bn => bn.id === businessName.id);
      businessNames.splice(editedBusinessNameIndex, 1, businessName);
      businessNamesCtrl.patchValue(businessNames);
    } else {
      // new business name
      const newBusinessName: IBusinessName = {
        name: businessName.name,
        from: businessName.from,
        id: uniqueId('temp') as any as number  // we need unique ids to enable only last name
      } as IBusinessName;
      businessNamesCtrl.patchValue([
        ...businessNames,
        newBusinessName,
      ]);
    }
    this.cdr.detectChanges();
    this.businessNameHasError = false;
  }

  public onBusinessNameDelete(businessName: IBusinessName): void {
    const businessNamesCtrl = this.form.get('businessNames');
    const businessNames: IBusinessName[] = businessNamesCtrl.value;
    const newBusinessNames: IBusinessName[] = businessNames
      .filter(bn => bn.id !== businessName.id);
    businessNamesCtrl.patchValue(newBusinessNames);
  }
}
