import * as httpStatuses from 'http-status-codes';

/***
 * This data is scraped from https://wiki.selfhtml.org/wiki/HTTP/Statuscodes
 */

const wordingHttpStatus2xx = {
  [httpStatuses.OK]: { 'en': 'OK', 'de': 'OK' },
  [httpStatuses.CREATED]: { 'en': 'Created', 'de': 'Erzeugt' },
  [httpStatuses.ACCEPTED]: { 'en': 'Accepted', 'de': 'Akzeptiert' },
  [httpStatuses.NON_AUTHORITATIVE_INFORMATION]: { 'en': 'Non-Authoritative Information', 'de': 'Unverbindliche Information' },
  [httpStatuses.NO_CONTENT]: { 'en': 'No Content', 'de': 'Kein Inhalt' },
  [httpStatuses.RESET_CONTENT]: { 'en': 'Reset Content', 'de': 'Inhalt zurücksetzen' },
  [httpStatuses.PARTIAL_CONTENT]: { 'en': 'Partial Content', 'de': 'Partieller Inhalt' },
  [httpStatuses.MULTI_STATUS]: { 'en': 'Multi-Status', 'de': 'Multi-Status' },
};

const wordingHttpStatus3xx = {
  [httpStatuses.MULTIPLE_CHOICES]: { 'en': 'Multiple Choices', 'de': 'Mehrere Möglichkeiten' },
  [httpStatuses.MOVED_PERMANENTLY]: { 'en': 'Moved Permanently', 'de': 'Dauerhaft verschoben' },
  [httpStatuses.MOVED_TEMPORARILY]: { 'en': 'Found', 'de': 'Gefunden' },
  [httpStatuses.SEE_OTHER]: { 'en': 'See Other', 'de': 'Siehe anderswo' },
  [httpStatuses.NOT_MODIFIED]: { 'en': 'Not Modified', 'de': 'Nicht verändert' },
  [httpStatuses.TEMPORARY_REDIRECT]: { 'en': 'Temporary Redirect', 'de': 'Zeitweise Umleitung' },
  [httpStatuses.PERMANENT_REDIRECT]: { 'en': 'Permanent Redirect', 'de': 'dauerhafte Umleitung' }
};

const wordingHttpStatus4xx = {
  [httpStatuses.BAD_REQUEST]: { 'en': 'Bad Request', 'de': 'Ungültige Anfrage' },
  [httpStatuses.UNAUTHORIZED]: { 'en': 'Unauthorized', 'de': 'Unautorisiert' },
  [httpStatuses.PAYMENT_REQUIRED]: { 'en': 'Payment Required', 'de': 'Bezahlung benötigt' },
  [httpStatuses.FORBIDDEN]: { 'en': 'Forbidden', 'de': 'Verboten' },
  [httpStatuses.NOT_FOUND]: { 'en': 'Not Found', 'de': 'Nicht gefunden' },
  [httpStatuses.METHOD_NOT_ALLOWED]: { 'en': 'Method Not Allowed', 'de': 'Methode nicht erlaubt' },
  [httpStatuses.NOT_ACCEPTABLE]: { 'en': 'Not Acceptable', 'de': 'Nicht akzeptabel' },
  [httpStatuses.PROXY_AUTHENTICATION_REQUIRED]: { 'en': 'Proxy Authentication Required', 'de': 'Proxy-Authentifizierung benötigt' },
  [httpStatuses.REQUEST_TIMEOUT]: { 'en': 'Request Time-out', 'de': 'Anfrage-Zeitüberschreitung' },
  [httpStatuses.CONFLICT]: { 'en': 'Conflict', 'de': 'Konflikt' },
  [httpStatuses.GONE]: { 'en': 'Gone', 'de': 'Verschwunden' },
  [httpStatuses.LENGTH_REQUIRED]: { 'en': 'Length Required', 'de': 'Länge benötigt' },
  [httpStatuses.PRECONDITION_FAILED]: { 'en': 'Precondition Failed', 'de': 'Vorbedingung missglückt' },
  [httpStatuses.REQUEST_TOO_LONG]: { 'en': 'Request Entity Too Large', 'de': 'Anfrage-Entität zu groß' },
  [httpStatuses.REQUEST_URI_TOO_LONG]: { 'en': 'Request-URI Too Large', 'de': 'Anfrage-URI zu lang' },
  [httpStatuses.UNSUPPORTED_MEDIA_TYPE]: { 'en': 'Unsupported Media Type', 'de': 'Nicht unterstützter Medientyp' },
  [httpStatuses.REQUESTED_RANGE_NOT_SATISFIABLE]: { 'en': 'Requested range not satisfiable', 'de': 'Anfrage-Bereich nicht erfüllbar' },
  [httpStatuses.EXPECTATION_FAILED]: { 'en': 'Expectation Failed', 'de': 'Erwartung missglückt' },
  [httpStatuses.UNPROCESSABLE_ENTITY]: { 'en': 'Unprocessable Entity', 'de': 'kann nicht verarbeitet werden' },
  [httpStatuses.LOCKED]: { 'en': 'Locked', 'de': 'gesperrt' },
  [httpStatuses.FAILED_DEPENDENCY]: { 'en': 'Failed Dependency', 'de': 'vorhergehende Bedingung nicht erfüllt' },
  [httpStatuses.PRECONDITION_REQUIRED]: { 'en': 'Precondition Required', 'de': 'Vorbedingung benötigt' },
  [httpStatuses.TOO_MANY_REQUESTS]: { 'en': 'Too Many Requests', 'de': 'zu viele Anfragen' },
  [httpStatuses.REQUEST_HEADER_FIELDS_TOO_LARGE]: { 'en': 'Request Header Fields Too Large', 'de': 'Länge zu groß' },
};
const wordingHttpStatus5xx = {
  [httpStatuses.INTERNAL_SERVER_ERROR]: { 'en': 'Internal Server Error', 'de': 'Interner Server-Fehler' },
  [httpStatuses.NOT_IMPLEMENTED]: { 'en': 'Not Implemented', 'de': 'Nicht implementiert' },
  [httpStatuses.BAD_GATEWAY]: { 'en': 'Bad Gateway', 'de': 'Schlechtes Portal' },
  [httpStatuses.SERVICE_UNAVAILABLE]: { 'en': 'Service Unavailable', 'de': 'Dienst nicht verfügbar' },
  [httpStatuses.GATEWAY_TIMEOUT]: { 'en': 'Gateway Time-out', 'de': 'Portal-Auszeit' },
  [httpStatuses.HTTP_VERSION_NOT_SUPPORTED]: { 'en': 'HTTP Version Not Supported', 'de': 'HTTP-Version nicht unterstützt' },
  [httpStatuses.INSUFFICIENT_STORAGE]: { 'en': 'Insufficient Storage', 'de': 'Speicher des Servers reicht nicht aus' },
  [httpStatuses.NETWORK_AUTHENTICATION_REQUIRED]: { 'en': 'Network Authentication Required', 'de': 'Identizifierung benötigt' }
};

export const wordingHttpStatus = {
  // ...wordingHttpStatus2xx,
  // ...wordingHttpStatus3xx,
  ...wordingHttpStatus4xx,
  ...wordingHttpStatus5xx,

  somethingWentWrong: { en: 'Something went wrong', de: 'Something went wrong' },
};
