import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { ArticleKey, IArticle, IArticleCreate, IArticleDTO } from 'src/app/core/models/resources/IArticle';
import { RepositoryService } from 'src/app/core/services/repository.service';

@Injectable({
  providedIn: 'root',
})
export class ArticleRepositoryService extends AbstractRepository<
  IArticle,
  IArticleDTO,
  IArticleCreate,
  RepoParams<IArticle>,
  ArticleKey> {

  protected resourceType = 'article';
  public sortAsce = true;
  public sortKey = 'id';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IArticleDTO): IArticle => item;
  public getBaseUrl = () => `/api/articles`;
}
