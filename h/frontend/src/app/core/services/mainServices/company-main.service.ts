import { Injectable } from '@angular/core';
import { forkJoin, Observable, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ISalutation } from '../../models/resources/ISalutation';
import { ITitle } from '../../models/resources/ITitle';
import { ICompany, ICompanyFull, ICompanyRow } from '../../models/resources/ICompany';
import { CompanyRepositoryService } from '../repositories/company-repository.service';
import { SalutationRepositoryService } from '../repositories/salutation-repository.service';
import { TitleRepositoryService } from '../repositories/title-repository.service';


@Injectable({
  providedIn: 'root'
})
export class CompanyMainService {

  constructor(
    private companyRepo: CompanyRepositoryService,
    private salutationRepo: SalutationRepositoryService,
    private titleRepo: TitleRepositoryService,
  ) { }

  private getCompanyFullList(
    companies: ICompany[],
    salutations: ISalutation[],
    title: ITitle[],
  ): ICompanyFull[] {
    return companies.map(company => this.getCompanyFull(company, salutations, title));
  }

  private getCompanyFull(
    company: ICompany,
    salutations: ISalutation[],
    titles: ITitle[],
  ): ICompanyFull {
    const salutation: ISalutation = salutations.find(saluattion => saluattion.id === company.salutationId);
    const title: ITitle = titles.find(titles => titles.id === company.titleId);
    return {
      ...company,
      salutation,
      title,
    };
  }

  public fetchAll(): Observable<ICompanyFull[]> {
    return forkJoin([
      this.companyRepo.fetchAll({}),
      this.salutationRepo.fetchAll({}),
      this.titleRepo.fetchAll({}),
    ]).pipe(map(res => this.getCompanyFullList(...res)));
  }

  public getStream(): Observable<ICompanyRow[]> {
    return forkJoin([
      this.companyRepo.fetchAll({}),
      this.salutationRepo.fetchAll({}),
      this.titleRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.companyRepo.getStream(),
        this.salutationRepo.getStream(),
        this.titleRepo.getStream(),
      ])),
      map(collection => this.toRows(this.getCompanyFullList(...collection)))
    );
  }

  public toRow(company: ICompanyFull): ICompanyRow {
    const { salutation } = company;
    const { title } = company;
    return ({
      ...company,
      salutationLabel: salutation?.displayLabel,
      titleLabel: title?.displayLabel,
    });
  }

  public toRows(companies: ICompanyFull[]): ICompanyRow[] {
    return companies.map((company) => this.toRow(company));
  }
}
