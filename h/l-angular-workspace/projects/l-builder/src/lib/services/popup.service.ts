import {EventEmitter, Injectable, Output} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PopupService {
  _popupData;
  popupData = new BehaviorSubject(this._popupData);

  openPopup(popupName: string, data: any, queryObj?: {query: string, param: string}) {
    this.setPopupData(popupName, data, queryObj);
    this.getPopupData();
  }

  setPopupData(popupName: string, data: any, queryObj?: {query: string, param: string}) {
    this._popupData = {popupName, data, queryObj};
  }

  getPopupData() {
     this.popupData.next(this._popupData);
  }
}
