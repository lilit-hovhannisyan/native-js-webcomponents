import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ValidationService} from '../../services/validation.service';

@Component({
    selector: 'lavash-graphics',
    templateUrl: './graphics.component.html',
    styleUrls: ['graphics.component.less']
})

export class GraphicsComponent {
    @Input() graphics?: string;
    @Input() graphicsPosition?: 'left' | 'right' | 'outer-left' | 'outer-right' = 'left';

    @Output() graphicClick = new EventEmitter();

    constructor(public validationService: ValidationService) {}

    onGraphicClick(event) {
       this.graphicClick.emit(event);
    }
}