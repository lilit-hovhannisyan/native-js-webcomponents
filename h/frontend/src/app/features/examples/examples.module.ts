import { NgModule } from '@angular/core';
import { DECLARATIONS, ExamplesRoutingModule } from './examples-routing.module';
import { Form1Component } from './components/wizard/form1';
import { Form2Component } from './components/wizard/form2';
import { Form3Component } from './components/wizard/form3';
import { WizardExampleComponent } from './components/wizard/wizardExample.component';
import { TimelineExampleComponent } from './components/timeline/timeline.component';
import { LogsComponent } from './components/logs/logs.component';
import { FooComponent } from './components/acl/foo/foo.component';
import { BarComponent } from './components/acl/bar/bar.component';
import { BazComponent } from './components/acl/baz/baz.component';
import { Test1Component } from './components/acl/test1/test1.component';
import { Test2Component } from './components/acl/test2/test2.component';
import { Test3Component } from './components/acl/test3/test3.component';
import { SharedModule } from '../../shared/shared.module';
import { FormWithFrameComponent } from './components/form-with-frame/form-with-frame.component';
import { DateboxComponent } from './components/datebox/datebox.component';
import { UserCompactComponent } from './components/user-compact/user-compact.component';
import { RealTimeChatComponent } from './components/chat-room/real-time-chat/real-time-chat.component';

@NgModule({
  imports: [
    ExamplesRoutingModule,
    SharedModule
  ],
  declarations: [
    DECLARATIONS,
    Form1Component,
    Form2Component,
    Form3Component,
    WizardExampleComponent,
    TimelineExampleComponent,
    LogsComponent,
    FooComponent,
    BarComponent,
    BazComponent,
    Test1Component,
    Test2Component,
    Test3Component,
    FormWithFrameComponent,
    DateboxComponent,
    UserCompactComponent,
    RealTimeChatComponent
  ]
})
export class ExamplesModule {
}
