import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { VesselRoleKey } from 'src/app/core/models/resources/IVesselRole';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { HttpPost } from 'src/app/core/models/http/http-post';
import { IResourceCollection } from 'src/app/core/models/resources/IResourceCollection';
import { IDataResponseBody } from 'src/app/core/models/responses/IDataResponseBody';
import { RoleKey } from 'src/app/core/models/resources/IRole';
import { RepoParams, AbstractRepository } from 'src/app/core/abstracts/abstract.repository';
import { IVesselRole, IVesselRoleDTO, IVesselRoleCreate } from 'src/app/core/models/resources/IVesselRole';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class VesselRoleRepositoryService extends AbstractRepository<
  IVesselRole,
  IVesselRoleDTO,
  IVesselRoleCreate,
  RepoParams<IVesselRole>,
  VesselRoleKey
> {
  protected resourceType = 'VesselRole';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/vessel-roles/';
  }

  public setRolesForVessel(
    roleIds: RoleKey[],
    vesselId: VesselKey,
  ): Observable<IVesselRole[]> {
    const url = `/api/vessels/${vesselId}/roles`;
    const request: HttpPost<RoleKey[]> = new HttpPost(url, roleIds);
    const request$: Observable<IVesselRole[]> = this.repositoryService.httpClient
      .request(request).pipe(
        map((response: HttpResponse<
            IDataResponseBody<IResourceCollection<IVesselRole>>
          >) => response.body.data.items),
        tap((vesselRoles: IVesselRole[]) => {
          this.pushToStream(vesselRoles, undefined, vesselId);
        })
      );
    return request$;
  }

  public setVesselsForRole(
    vesselIds: VesselKey[],
    roleId: RoleKey,
  ): Observable<IVesselRole[]> {
    const url = `/api/roles/${roleId}/vessel-roles`;
    const request: HttpPost<VesselKey[]> = new HttpPost(url, vesselIds);
    const request$: Observable<IVesselRole[]> = this.repositoryService.httpClient
      .request(request).pipe(
        map((response: HttpResponse<
            IDataResponseBody<IResourceCollection<IVesselRole>>
          >) => response.body.data.items),
        tap((vesselRoles: IVesselRole[]) => {
          this.pushToStream(vesselRoles, roleId);
        })
      );
    return request$;
  }

  private pushToStream(
    vesselRoles?: IVesselRole[],
    roleId?: RoleKey,
    vesselId?: VesselKey,
  ): void {
    const allVesselRoles: IVesselRole[] = this.streamSubject.getValue();
    // first remove all previous VesselRoles of current Vessel from stream
    const allVesselRolesFiltered: IVesselRole[] = allVesselRoles
      .filter(vesselRole => vesselId
        ? vesselRole.vesselId !== vesselId
        : vesselRole.roleId !== roleId
      );
    // then add new VesselRoles ( server sends down all VesselRoles of current Vessel )
    // ( this way we can also remove VesselRoles from the stream )
    this.streamSubject.next([ ...allVesselRolesFiltered, ...vesselRoles ]);
  }

  protected postProcess(item: IVesselRoleDTO): IVesselRole { return item; }
}
