import { IUser } from 'src/app/core/models/resources/IUser';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { ActivatedRoute, Router } from '@angular/router';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { ReplaySubject } from 'rxjs';
import { UsersHelperService } from './users.helper.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './users-detail-view.component.html',
  styleUrls: ['./users-detail-view.component.scss'],
  providers: [UsersHelperService],
})
export class UserDetailViewComponent implements OnInit, OnDestroy {
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public title: string;
  public user?: IUser;
  public sitemap = sitemap;

  public get isCreationMode() {
    return this.route.snapshot.params.id === 'new';
  }

  constructor(
    private route: ActivatedRoute,
    public userRepository: UserRepositoryService,
    public usersHelperService: UsersHelperService,
    private router: Router,
  ) { }

  public ngOnInit() {
    const idParam = this.route.snapshot.params.id;

    if (this.isCreationMode) {
      this.usersHelperService.generateTitle();
    } else {
      const id = parseInt(idParam, 10);

      this.userRepository.fetchOne(id, { useCache: true })
        .subscribe(user => {
          this.usersHelperService.generateTitle(user);
        });
    }

    this.usersHelperService.editViewTitle$.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(title => {
      this.title = title;
    });
  }

  public routeBack = () => {
    this.router.navigate([
      url(sitemap.system.children.misc.children.users)
    ]);
  }

  public tabDisabler = (node: SitemapNode) => {
    const basicNode = sitemap.system.children.misc.children.users.children.id.children.basic;
    // in isCreationMode just enable the basicTab
    return this.isCreationMode ? node === basicNode : true;
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
