import { FormGroup } from '@angular/forms';
import { IAddress } from '../../../core/models/resources/IAddress';
import { Component, Input, OnInit } from '@angular/core';
import { ICountry } from '../../../core/models/resources/ICountry';
import { Validators } from 'src/app/core/classes/validators';
import { Observable } from 'rxjs';
import { CountryRepositoryService } from '../../../core/services/repositories/country-repository.service';

/**
 * A generic address form which can be used for different entities in the system.
 * It consists of a the following default fields:
 * 1. street
 * 2. postal code
 * 3. province
 * 4. city
 * 5. country
 */
@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent implements OnInit {
  @Input() public parentForm: FormGroup;
  public isoCountries$: Observable<ICountry[]>;

  constructor(private countryRepository: CountryRepositoryService) { }

  public static getFormGroup(address: IAddress | undefined, needToValidate: Function): object {

    // we pass needToValidate in as a function, because we want to get a reference to the
    // addressVisible-flag of the calling class, and not a frozen boolean that doesnt
    // update. -> Therefore this function nesting
    const preVal = (valFunc) => (...args) => needToValidate()
      ? valFunc(...args)
      : null;

    const ad = (address || {}) as IAddress;
    return {
      postalCode: [ad && ad.postalCode || '', preVal(Validators.required)],
      city: [ad && ad.city || '', preVal(Validators.required)],
      province: [ad && ad.province || '', preVal(Validators.required)],
      street: [ad && ad.street || '', preVal(Validators.required)],
    };
  }

  public ngOnInit() {
    this.isoCountries$ = this.countryRepository.fetchAll({});
  }
}
