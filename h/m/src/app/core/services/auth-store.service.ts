import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { CompanyModel } from '@shared-models/company/company.model';
import { UserModel } from '@shared-models/user/user.model';
import { PermissionModel } from '@shared/models/permission/permission.model';

@Injectable({
  providedIn: 'root'
})
export class AuthStoreService {
  private _isForgotPasswordEmailSent: boolean = false;

  private readonly _user = new BehaviorSubject<UserModel | null>(null);
  readonly user$ = this._user.asObservable();

  private readonly _company = new BehaviorSubject<CompanyModel | null>(null);
  readonly company$ = this._company.asObservable();

  private readonly _permissions = new BehaviorSubject<PermissionModel[] | null>(null);
  readonly permissions$ = this._permissions.asObservable();

  private readonly _language = new BehaviorSubject<string | null>(null);
  readonly language$ = this._language.asObservable();

  // forgot email
  setIsForgotPasswordEmailSent(value: boolean): void {
    this._isForgotPasswordEmailSent = value;
  }

  getIsForgotPasswordEmailSent(): boolean {
    return this._isForgotPasswordEmailSent;
  }

  // user
  get user(): UserModel | null {
    return this._user.getValue();
  }

  set user(data: UserModel | null) {
    this._user.next(data);
  }

  // company
  get company(): CompanyModel | null {
    return this._company.getValue();
  }

  set company(data: CompanyModel | null) {
    this._company.next(data);
  }

  // permissions
  get permissions(): PermissionModel[] | null {
    return this._permissions.getValue();
  }

  set permissions(data: PermissionModel[] | null) {
    this._permissions.next(data);
  }

  get language(): string | null {
    return this._language.getValue();
  }

  set language(language: string | null) {
    this._language.next(language);
  }

  isAllowed(action: string): boolean {
    const actionSection: string[] = this.parsePermission(action);
    return this.permissions
      ? this.permissions.findIndex((permission) => {
          const permissionSection = this.parsePermission(permission.permission);
          return permissionSection[1] === '*' ? actionSection[0] === permissionSection[0] : permission.permission === action;
        }) > -1
      : false;
  }

  parsePermission(permission: string): string[] {
    permission = permission || ':';
    const lastIndex: number = permission.lastIndexOf(':');
    return lastIndex !== -1 ? [permission.substring(0, lastIndex), permission.substring(lastIndex + 1)] : [permission, '*'];
  }
}
