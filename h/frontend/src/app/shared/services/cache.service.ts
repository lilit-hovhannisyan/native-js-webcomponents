import { filter } from 'rxjs/operators';
import { AppException } from './../../core/classes/exceptions/AppException';
import { InvalidArgumentException } from '../../core/classes/exceptions/InvalidArgumentException';
import { Injectable } from '@angular/core';
import { ICacheItem, ICacheTagMeta as ICacheTag } from 'src/app/core/models/cache/ICacheItem';
import { ResourceKey, IResource } from 'src/app/core/models/resources/IResource';

// tslint:disable-next-line:class-name
interface _ICacheItem {
  value?: any;
  key: string;
  expiresAt?: Date;
}

interface Cache {
  items: Map<string, _ICacheItem>;
  deferredItems: Map<string, ICacheItem<any>>;
}

/**
 * An almost(*) [PSR-6](https://www.php-fig.org/psr/psr-6)
 * conform cache service.
 *
 * (*) Deviates from the spec in that it allows any
 * symbols (also those reserved by PSR-6) to be contained in the key.
 */
@Injectable({
  providedIn: 'root'
})
export class CacheService {

  private static validKeySymbols = /.+/;

  private cache: Cache = {
    items: new Map(),
    deferredItems: new Map(),
  };

  /**
   * Returns a Cache Item representing the specified key.
   *
   * This method always returns a CacheItemInterface object, even in case of
   * a cache miss. It does not return null.
   *
   * @param $key
   *   The key for which to return the corresponding Cache Item.
   *   A string of at least one character that uniquely identifies a cached item.
   *   Supported keys are consisting of the characters A-Z, a-z, 0-9, "_", and "."
   *
   * @throws InvalidArgumentException
   *   If the $key string is not a legal value
   *
   * @return CacheItemInterface
   *   The corresponding Cache Item.
   */
  public getItem<T>(key: string): ICacheItem<T> {
    this.validateKey(key);
    const { items } = this.cache;
    const item = items.get(key);
    const isFound = items.has(key);
    const hasExpired = (
      !isFound ||
      (
        item.expiresAt === undefined ||
        item.expiresAt.getTime() < Date.now()
      )
    );

    if (hasExpired) {
      this.deleteItem(key);
    }

    const source = hasExpired ? { key } : item;

    // we never return the internal representation of the cache item
    // but create a copy to avoid cache manipulation from outside.
    return {
      key,
      value: source.value,
      expiresAt: source.expiresAt,
      isHit: !hasExpired
    };
  }

  /**
   * Returns a traversable set of cache items.
   *
   * @param $keys
   *   An indexed array of keys of items to retrieve.
   *
   * @throws InvalidArgumentException
   *   If any of the keys in $keys are not a legal value.
   *
   * @return
   *   A traversable collection of Cache Items keyed by the cache keys of
   *   each item. A Cache item will be returned for each key, even if that
   *   key is not found. However, if no keys are specified then an empty
   *   traversable is returned instead.
   */
  public getItems<T>(keys: string[]): ICacheItem<T>[] {
    return keys.map(el => this.getItem<T>(el));
  }

  /**
   * Returns a traversable set of cache items that are not expired and
   * whose keys match the specified regex.
   *
   * Note: This goes beyong PSR-6
   *
   * @param regexp the regular expression to match the keys agains.
   */
  public getItemsByRegex<T>(regexp: RegExp): ICacheItem<T>[] {
    return this.getItemsBySelector((key) => regexp.test(key));
  }

  /**
   * Returns a traversable set of cache items that are not expired and
   * whose keys match the specified key prefix.
   *
   * Note: This goes beyong PSR-6
   *
   * @param keyPrefix the prefix to match the keys agains.
   */
  public getItemsByPrefix<T>(keyPrefix: string): ICacheItem<T>[] {
    return this.getItemsBySelector((key) => key.startsWith(keyPrefix));
  }

  /**
   * Returns a traversable set of cache items that are not expired and
   * whose keys are accepted by the selector.
   *
   * Note: This goes beyong PSR-6
   *
   * @param selector the function to select items by key
   */
  public getItemsBySelector<T>(selector: (key: string) => boolean): ICacheItem<T>[] {
    const matchingItems: ICacheItem<T>[] = [];
    for (const key of this.cache.items.keys()) {
      if (!selector(key)) {
        continue;
      }
      const item = this.getItem<T>(key);
      if (!item.isHit) {
        continue;
      }
      matchingItems.push(item);

    }
    return matchingItems;
  }

  /**
   * Confirms if the cache contains specified cache item.
   *
   * @param $key
   *   The key for which to check existence.
   *
   * @throws InvalidArgumentException
   *   If the $key string is not a legal value
   *
   * @return
   *   True if item exists in the cache, false otherwise.
   */
  public hasItem(key: string): boolean {
    return this.getItem(key).isHit;
  }

  /**
   * Deletes all items in the pool.
   *
   * @return
   *   True if the pool was successfully cleared. False if there was an error.
   */
  public clear(): boolean {
    this.cache.items.clear();

    return true;
  }

  /**
   * Removes the item from the pool.
   *
   * @param $key
   *   The key to delete.
   *
   * @throws InvalidArgumentException
   *   If the $key string is not a legal value
   *
   * @return
   *   True if the item was successfully removed. False if there was an error.
   */
  public deleteItem(key: string): boolean {
    this.validateKey(key);

    return this.cache.items.delete(key);
  }

  /**
   * Removes multiple items from the pool.
   *
   * @param $keys
   *   An array of keys that should be removed from the pool.
   *
   * @throws InvalidArgumentException
   *   If any of the keys in $keys are not a legal value.
   *
   * @return
   *   True if the items were successfully removed. False if there was an error.
   */
  public deleteItems(keys: string[]): boolean {
    keys.forEach(key => this.validateKey(key));
    keys.forEach(key => this.cache.items.delete(key));

    return true;
  }

  /**
   * Persists the cache item.
   *
   * @param $item
   *   The cache item to save.
   *
   * @return
   *   True if the item was successfully persisted. False if there was an error.
   */
  public save(item: ICacheItem<any>): boolean {
    this.cache.items.set(item.key, item);

    return true;
  }

  /**
   * Sets a cache item to be persisted later.
   *
   * @param $item
   *   The cache item to save.
   *
   * @return
   *   False if the item could not be queued or if a commit
   *   was attempted and failed. True otherwise.
   */
  public saveDeferred(item: ICacheItem<any>): boolean {
    this.cache.deferredItems.set(item.key, item);
    return true;
  }

  /**
   * Persists any deferred cache items.
   *
   * @return
   *   True if all not-yet-saved items were successfully saved
   *   or there were none. False otherwise.
   *
   *   Note: if the commit fails, a rollback is conducted.
   */
  public commit() {
    const { deferredItems } = this.cache;
    const prevCacheState = [];
    try {
      deferredItems.forEach(item => {
        prevCacheState.push(this.getItem(item.key));
        const isSuccess = this.save(item);
        if (!isSuccess) {
          throw new AppException(`Could not save item with key: ${item.key}`);
        }
      });
    } catch (e) {
      try {
        prevCacheState.forEach(item => {
          const isSuccess = item.isHit
            ? this.save(item)
            : this.deleteItem(item.key);
          if (!isSuccess) {
            throw new AppException('Rollback failed for cache commit');
          }
        });
      } catch (e) {
        console.warn(e);
      }
      return false;
    }

    return true;
  }

  private validateKey(key: string) {
    if (!CacheService.validKeySymbols.test(key)) {
      throw new InvalidArgumentException('Unsupported characters in key');
    }
  }
}
