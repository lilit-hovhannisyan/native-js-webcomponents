import { NvColumnDataType, NvFilterControl, NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';
import { getContextMenuButtonsConfig } from '../../helpers/general';
import { wordingAccounting } from '../wording/accounting';
import { wordingGeneral } from '../wording/general';
import { Operations } from '../../models/Operations';

export const getCostCentersGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'costCentersGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    rowSelectionType: actions ? null : NvGridRowSelectionType.RadioButton,
    hideRefreshButton: true,
    sortBy: 'no',
    isSortAscending: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 50
    },
    columns: [
      {
        key: 'no',
        title: wordingAccounting.no,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wordingAccounting.costCenterName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'typeLabel',
        title: wordingAccounting.type,
        width: 200,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isActive',
        title: wordingGeneral.active,
        width: 75,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: actions
      ? [
        {
          icon: 'edit',
          description: wordingGeneral.edit,
          name: 'editCostCenter',
          tooltip: wordingGeneral.edit,
          actOnDoubleClick: true,
          func: actions.edit,
          hidden: !canPerform(Operations.UPDATE)
        },
        {
          icon: 'delete',
          description: wordingGeneral.delete,
          name: 'deleteCostCenter',
          tooltip: wordingGeneral.delete,
          actOnDoubleClick: false,
          func: actions.delete,
          hidden: !canPerform(Operations.DELETE)
        }
      ]
      : [],
    toolbarButtons: actions
      ? [
        {
          title: wordingGeneral.create,
          tooltip: wordingGeneral.create,
          icon: 'plus-circle-o',
          func: actions.add,
          hidden: () => !canPerform(Operations.CREATE)
        }
      ]
      : [],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};

