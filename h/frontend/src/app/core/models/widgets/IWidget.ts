import { GridsterItem } from 'angular-gridster2/lib/gridsterItem.interface';
import { IResource } from '../resources/IResource';
import { WidgetType } from './WidgetType';

export interface IWidgetBase extends IResource<number> {
  type: WidgetType;
}

export interface IWidgetDTO extends IWidgetBase {
  position: IWidgetPositionDTO;
}

export interface IWidgetCreate extends IWidgetDTO {}

export interface IWidget extends IWidgetBase, GridsterItem { }

export interface IWidgetPositionDTO {
  x: number;
  y: number;
  width: number;
  height: number;
}
