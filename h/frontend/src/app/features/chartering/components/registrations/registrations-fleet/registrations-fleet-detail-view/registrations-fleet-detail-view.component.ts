import { Component, OnInit, OnDestroy } from '@angular/core';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { RegistrationFleetHelperService } from './registrations-fleet-helper.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { IWording } from 'src/app/core/models/resources/IWording';
import { startWith, switchMap, takeUntil, tap } from 'rxjs/operators';
import { RegistrationVesselTasksRepositoryService } from 'src/app/core/services/repositories/registration-vessel-tasks-repository.service';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { VesselRegisterRepositoryService } from 'src/app/core/services/repositories/vessel-registeries-repository.service';
import { IVesselRegister } from 'src/app/core/models/resources/IVesselRegister';
import { findClosestWithFromUntil } from 'src/app/core/helpers/general';

@Component({
  selector: 'nv-registrations-fleet-detail-view',
  templateUrl: './registrations-fleet-detail-view.component.html',
  styleUrls: ['./registrations-fleet-detail-view.component.scss'],
  providers: [RegistrationFleetHelperService],
})
export class RegistrationsFleetDetailViewComponent implements OnInit, OnDestroy {
  public sitemap = sitemap.chartering.children.registrations.children.registrationsFleet.children.id;
  public title: string;
  public createWording: IWording = wording.general.create;
  public componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public registers: IVesselRegister[];
  public selectedRegister: IVesselRegister;
  public wording = wording;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public helperService: RegistrationFleetHelperService,
    private registrationVesselTaskRepo: RegistrationVesselTasksRepositoryService,
    private vesselRegistriesRepo: VesselRegisterRepositoryService,
  ) { }

  public ngOnInit() {
    const vesselId: VesselKey = +this.activatedRoute.snapshot.params.id;

    this.fetchResources(vesselId);
    this.registerSelectHandler(vesselId);
    this.helperService.generateTitle(vesselId)
      .subscribe(title => this.title = title);
  }

  /** Fetches additional resources for tabs. */
  private fetchResources(vesselId: VesselKey): void {
    this.registrationVesselTaskRepo.fetchAll({}).subscribe();
    this.fetchRegisters(vesselId);
  }

  public get isCreationMode() {
    return this.activatedRoute.snapshot.params.id === 'new';
  }

  public routeBack = () => {
    this.router.navigate([
      url(sitemap.chartering.children.registrations.children.registrationsFleet)
    ]);
  }

  public tabDisabler = (node: SitemapNode) => {
    const basicNode = this.sitemap.children.basic;

    // in creationMode just enable the basicTab
    return this.isCreationMode ? node === basicNode : true;
  }

  private fetchRegisters(vesselId: VesselKey): void {
    this.vesselRegistriesRepo.fetchAll({ queryParams: { vesselId }}).subscribe();
    this.vesselRegistriesRepo.getStream({ vesselId }).subscribe(registers => {
      this.registers = registers;

      const currentSelected: IVesselRegister = this.selectedRegister
        && this.registers.find(r => r.id === this.selectedRegister.id);

      const newRegister: IVesselRegister = currentSelected
        || findClosestWithFromUntil(this.registers)
        || this.registers[this.registers.length];

      this.helperService.selectedRegisterSubject.next(newRegister);
    });
  }

  /** Handles register-slider change and sets task-tab's title. */
  private registerSelectHandler(vesselId: VesselKey): void {
    this.helperService.selectedRegisterSubject.pipe(
      startWith(),
      tap(r => this.selectedRegister = r),
      switchMap(register => {
        return this.helperService.changeTasksTabTitle(vesselId, register?.id);
      }),
      takeUntil(this.componentDestroyed$),
    ).subscribe();
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
    this.helperService.resetTasksTabDefaultTitle();
  }
}
