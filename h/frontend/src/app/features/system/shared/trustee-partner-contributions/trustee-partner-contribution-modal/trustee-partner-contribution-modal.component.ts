import { Component, OnInit, Input, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { Observable, forkJoin, ReplaySubject, of, BehaviorSubject } from 'rxjs';
import { map, tap, catchError, withLatestFrom, distinctUntilChanged, startWith } from 'rxjs/operators';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { CompanyLegalEntityRepositoryService } from 'src/app/core/services/repositories/company-legal-entity-repository.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { ICompany, CompanyKey } from 'src/app/core/models/resources/ICompany';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { wording } from 'src/app/core/constants/wording/wording';
import { Validators } from 'src/app/core/classes/validators';
import { IWording } from 'src/app/core/models/resources/IWording';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { SettingsService } from 'src/app/core/services/settings.service';
import { ContributionModalHelperService } from './trustee-partner-contribution-modal.helper.service';
import { ITrusteePartnerContribution, ITrusteePartnerContributionFull } from 'src/app/core/models/resources/ITrusteePartnerContribution';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { ICompanyLegalEntity } from 'src/app/core/models/resources/ICompanyLegalEntity';
import { CompanyLegalEntityMainService } from 'src/app/core/services/mainServices/company-legal-entity-main.service';
import { TrusteePartnerContributionsRepositoryService } from 'src/app/core/services/repositories/trustee-partner-contribution-repository.service';
import { CompanyContributionsRepositoryService } from 'src/app/core/services/repositories/company-partner-contribution-repository.service';
import { ICompanyPartnerContribution } from 'src/app/core/models/resources/ICompanyPartnerContribution';
import { convertToMap } from 'src/app/shared/helpers/general';
import { LatestContributionsMap, TrusteePartnerContributionMainService } from 'src/app/core/services/mainServices/trustee-partner-contributions-main.service';
import { COMPANY_LEGAL_FORM } from 'src/app/core/models/enums/company-legal-form';
import { ContributionMainService } from 'src/app/core/services/mainServices/company-contributions-main.service';
import { LegalEntityPartnersModalComponent } from '../../legal-entity-partners-modal/legal-entity-partners-modal.component';
import { COMPANY_CONTRIBUTION_TYPES, createPartnerTooltipTexts, increasePartnerLabels, partnerTrusteeLabelsByLegal, reducePartnerLabels, sectionTitles } from '../enums/company-contributions';
import { decimalValidators } from '../../../components/misc/companies/detail-view/company-legal-tab/shareholders-partners/contribution-modal/contribution-modal.helper.service';

@Component({
  templateUrl: './trustee-partner-contribution-modal.component.html',
  styleUrls: ['../trustee-partner-contribution-modal.component.scss'],
  providers: [ContributionModalHelperService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrusteePartnerContributionModalComponent implements OnInit, OnDestroy {
  @Input() public companyId: CompanyKey;
  @Input() public contributorType: COMPANY_CONTRIBUTION_TYPES;
  @Input() public latestContributionsMap: LatestContributionsMap;
  @Input() public allContributions: ITrusteePartnerContributionFull[];
  @Input() public isNonParValueShare: boolean;
  private shareholdersContributions: ICompanyPartnerContribution[] = [];
  private theOtherContributions: ITrusteePartnerContribution[] = [];
  private shareholderLatestContributionsMap: Map<CompanyKey, ICompanyPartnerContribution>;

  public increasePartnerLabel: IWording;
  public partnerTrusteeLabel: IWording;
  public reducePartnerLabel: IWording;
  public contributionLabel: IWording;
  public contributionNewLabel: IWording;
  public contributionChangeLabel: IWording;
  public createPartnerTooltipText: IWording;
  public createPartnerModalTitle: IWording;

  public form: FormGroup;
  public wording = wording;
  public companies$: Observable<ICompany[]>;
  public increaseShareholders$: Observable<ICompany[]>;
  public decreaseShareholders$: Observable<ICompany[]>;
  public partners: ICompany[] = [];
  public reduceShareholdersPartners: ICompany[];
  public currency: ICurrency;
  public company: ICompany;
  public legalEntity: ICompanyLegalEntity;
  public isPendingRequest: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private modalService: NzModalService,
    private companyRepo: CompanyRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private companyLegalEntityRepo: CompanyLegalEntityRepositoryService,
    private companyLegalEntityMainService: CompanyLegalEntityMainService,
    private trusteeContributionRepo: TrusteePartnerContributionsRepositoryService,
    private settingsService: SettingsService,
    private helperService: ContributionModalHelperService,
    private changeDetectorRef: ChangeDetectorRef,
    private shareholderContributionsRepo: CompanyContributionsRepositoryService,
    private shareholderContributionsMainService: ContributionMainService,
    private trusteeContributionsMainService: TrusteePartnerContributionMainService,
  ) { }

  public ngOnInit() {
    this.setCompanies();
    this.setPartnerCompanies();

    this.fetchResources().subscribe(() => {
      this.setLabels();
      this.init();
    });
  }

  private init(): void {
    this.form = this.fb.group({
      companyId: [this.companyId],
      from: [null, [Validators.required]],
      currencyId: [{ value: this.currency.id, disabled: true }],
      currency: [{ value: this.currency.isoCode, disabled: true }],

      increasePartnerId: [null, [Validators.required]],
      increaseShareholderId: [null, [Validators.required]],
      increaseContribution: [{ value: 0, disabled: true }],
      increaseContributionNew: [null, [Validators.required, ...decimalValidators]],

      reducePartnerId: [null, [Validators.required]],
      reduceShareholderId: [null, [Validators.required]],
      reduceContribution: [{ value: 0, disabled: true }],
      reduceContributionNew: [null, [Validators.required, ...decimalValidators]],

      contributionChange: [null, [Validators.required, ...decimalValidators]],
    });

    this.helperService.init(
      this.form,
      this.componentDestroyed$,
      this.companyId,
      this.contributorType,
      this.shareholderLatestContributionsMap,
      this.latestContributionsMap,
      this.allContributions,
      this.shareholdersContributions,
      this.theOtherContributions,
    );
    this.helperService.initValidations();
    this.helperService.initFinalContribution('increasePartnerId', 'increaseShareholderId', 'increaseContribution');
    this.helperService.initFinalContribution('reducePartnerId', 'reduceShareholderId', 'reduceContribution');
    this.changeDetectorRef.detectChanges();
    this.setShareholders();
    this.setTrusteeCompanies();
  }

  private setTrusteeCompanies(): void {
    const { reducePartnerId, reduceShareholderId } = this.form.controls;
    reducePartnerId.valueChanges.pipe(
      distinctUntilChanged()
    ).subscribe(partnerId => {
      this.reduceShareholdersPartners = this.trusteeContributionsMainService.getConnectedTrustees(
        reducePartnerId.value,
        this.partners,
        this.allContributions,
      );

      if (this.reduceShareholdersPartners.length) {
        reduceShareholderId.enable();
      } else {
        reduceShareholderId.disable();
      }

      reduceShareholderId.setValue(
        this.reduceShareholdersPartners.length === 1
          ? this.reduceShareholdersPartners[0].id
          : null
      );
    });
  }

  public submit(): void {
    validateAllFormFields(this.form);
    if (this.form.invalid) { return; }

    const contributions = this.getEnteredContributions();
    const legalEntityRequest = this.legalEntity.id
      ? of(this.legalEntity)
      : this.companyLegalEntityMainService.createDefault(this.companyId, this.currency.id);

    this.isPendingRequest.next(true);

    forkJoin([
      legalEntityRequest,
      ...contributions.map((c) => this.trusteeContributionRepo.create(c, { contributorType: this.contributorType })),
    ]).pipe(
      catchError(() => of([])),
    ).subscribe(([newLegalEntity, ...newContributions]) => {
      if (newContributions.length) {
        this.modalRef.close(newContributions);
      } else {
        this.isPendingRequest.next(false);
      }
    });
  }

  public cancel(): void {
    this.modalRef.close();
  }

  public openPartnersModal(partnerControl: AbstractControl): void {
    const { increasePartnerId, reducePartnerId } = this.form.controls;
    const partnersIds: CompanyKey[] = [this.companyId];

    if (increasePartnerId.value) {
      partnersIds.push(increasePartnerId.value);
    }

    if (reducePartnerId.value) {
      partnersIds.push(reducePartnerId.value);
    }

    this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: this.createPartnerModalTitle[this.settingsService.language],
      nzContent: LegalEntityPartnersModalComponent,
      nzComponentParams: {
        partnersIds,
        onlyCreate: true,
      },
      nzWidth: 500,
      nzClosable: true,
    }).afterClose.subscribe((companyId: CompanyKey) => {
      if (!companyId) {
        return;
      }

      partnerControl.setValue(companyId);
    });
  }

  private setLabels(): void {
    const legalForm: COMPANY_LEGAL_FORM = this.company.legalForm;

    this.increasePartnerLabel = increasePartnerLabels.get(this.contributorType);
    this.reducePartnerLabel = reducePartnerLabels.get(this.contributorType);
    this.partnerTrusteeLabel = partnerTrusteeLabelsByLegal.get(legalForm).get(this.contributorType);
    this.createPartnerTooltipText = createPartnerTooltipTexts.get(this.contributorType);
    this.createPartnerModalTitle = sectionTitles.get(this.contributorType);
    this.contributionLabel = this.isNonParValueShare
      ? wording.system.numberOfNonParValueShares
      : wording.system.contributionAmount_v2;
    this.contributionNewLabel = this.isNonParValueShare
      ? wording.system.numberOfNonParValueSharesNew
      : wording.system.contributionAmountNew_v2;
    this.contributionChangeLabel = this.isNonParValueShare
      ? wording.system.numberOfNonParValueSharesChange
      : wording.system.contributionChangeAmount;
  }

  private fetchResources(): Observable<[
    ICompanyLegalEntity,
    ICurrency[],
    ICompany[],
    ICompanyPartnerContribution[],
    ITrusteePartnerContribution[]
  ]> {
    const theOtherContributionType: COMPANY_CONTRIBUTION_TYPES
      = this.contributorType === COMPANY_CONTRIBUTION_TYPES.TrustorContribution
        ? COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution
        : COMPANY_CONTRIBUTION_TYPES.TrustorContribution;

    return forkJoin([
      this.companyLegalEntityRepo.fetchOne(this.companyId, {}),
      this.currencyRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.shareholderContributionsRepo.fetchCompanyContributions(this.companyId),
      this.trusteeContributionRepo.fetchTrusteePartnerContributions(
        this.companyId,
        theOtherContributionType,
      ),
    ]).pipe(
      tap(([legalEntity, currencies, companies, shareholdersContributions, theOtherContributions]) => {
        this.legalEntity = legalEntity;
        this.shareholdersContributions = shareholdersContributions;
        this.theOtherContributions = theOtherContributions;
        this.company = companies.find(c => c.id === this.companyId);
        this.currency = legalEntity && legalEntity.currencyId
          ? currencies.find(c => c.id === legalEntity.currencyId)
          : currencies.find(c => c.isoCode === 'EUR');
      })
    );
  }

  private getEnteredContributions(): ITrusteePartnerContribution[] {
    const contributions: ITrusteePartnerContribution[] = [];
    const { increasePartnerId, reducePartnerId } = this.form.controls;

    if (increasePartnerId.value) {
      contributions.push(this.getEnteredIncreaseContribution());
    }

    if (reducePartnerId.value) {
      contributions.push(this.getEnteredReduceContribution());
    }

    return contributions;
  }

  private getEnteredIncreaseContribution(): ITrusteePartnerContribution {
    const {
      from,
      increasePartnerId,
      increaseShareholderId,
      increaseContribution,
      increaseContributionNew,
    } = this.form.controls;

    return {
      companyId: this.companyId,
      from: from.value,
      partnerId: increasePartnerId.value,
      shareholderId: increaseShareholderId.value,
      contributionAmount: increaseContributionNew.value - increaseContribution.value,
    } as ITrusteePartnerContribution;
  }

  private getEnteredReduceContribution(): ITrusteePartnerContribution {
    const {
      from,
      reducePartnerId,
      reduceShareholderId,
      reduceContribution,
      reduceContributionNew,
    } = this.form.controls;

    return {
      companyId: this.companyId,
      from: from.value,
      partnerId: reducePartnerId.value,
      shareholderId: reduceShareholderId.value,
      contributionAmount: reduceContributionNew.value - reduceContribution.value,
    } as ITrusteePartnerContribution;
  }

  private setCompanies(): void {
    this.companies$ = this.companyRepo.getStream().pipe(
      map((companies: ICompany[]) => companies.filter(c => c.id !== this.companyId))
    );
  }

  private setShareholders(): void {
    const { from } = this.form.controls;

    from.valueChanges.pipe(
      startWith(from.value),
      distinctUntilChanged(),
    ).subscribe(value => {
      this.setIncreaseShareholders(value);
      this.setDecreaseShareholders(value);
    });
  }

  private setIncreaseShareholders(date: string): void {
    const finalContributions = this.shareholderContributionsMainService.getCompaniesLatestContributions(
      this.shareholderContributionsMainService.filterContributions(
        this.shareholdersContributions,
        date,
      ),
    );

    this.increaseShareholders$ = this.companies$.pipe(
      map(companies => companies.filter(c => {
        return finalContributions.get(c.id)?.contributionAmount > 0;
      })),
    );
  }

  private setDecreaseShareholders(date: string): void {
    const { reducePartnerId } = this.form.controls;
    const partnersSummaryContributions: Map<CompanyKey, ITrusteePartnerContributionFull> =
      this.trusteeContributionsMainService.getPartnersSummaryContributions(
        this.trusteeContributionsMainService.getCompaniesLatestContributions(
          this.trusteeContributionsMainService.filterContributions(
            this.allContributions,
            date,
          )
        )
      );

    this.decreaseShareholders$ = this.companies$.pipe(
      map(companies => companies.filter(c => {
        return partnersSummaryContributions.get(c.id)?.contributionAmount > 0;
      })),
      tap(companies => {
        if (companies.length) {
          reducePartnerId.enable();
        } else {
          reducePartnerId.disable();
        }
      })
    );
  }

  private setPartnerCompanies(): void {
    this.shareholderContributionsRepo.fetchCompanyContributions(this.companyId).pipe(
      withLatestFrom(this.companies$),
      tap(([shareholdersContributions, companies]) => {
        this.shareholderLatestContributionsMap = this.shareholderContributionsMainService
          .getCompaniesLatestContributions(shareholdersContributions);

        const partnersMap: Map<CompanyKey, ICompanyPartnerContribution>
          = convertToMap<ICompanyPartnerContribution, CompanyKey>(shareholdersContributions, 'partnerId');

        this.partners = companies.filter(company => !!partnersMap.get(company.id));
      })
    ).subscribe();
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }
}
