import { ChangeDetectorRef, ChangeDetectionStrategy, Component, Inject, TemplateRef, Output, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { MePopupUserInfoI } from './me-popup-user-info.interface';

import { MeLabelI } from '../me-label/me-label-inteface';
import { MeUserImageI } from '../me-user-image/me-user-image.interface';

@Component({
  selector: 'me-popup-user-info',
  templateUrl: './me-popup-user-info.component.html',
  styleUrls: ['./me-popup-user-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MePopupUserInfoComponent {
  userImage!: MeUserImageI;

  statusLabel!: MeLabelI;

  private _dataModel!: MePopupUserInfoI;
  content!: TemplateRef<Component>;
  @Output() popupUserInfoEvent: EventEmitter<{ eventName: string }> = new EventEmitter();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { dataModel: MePopupUserInfoI; content: TemplateRef<Component> },
    private cdr: ChangeDetectorRef
  ) {
    this._dataModel = data.dataModel;
    this.content = data.content;
    this.statusLabel = { type: this.dataModel?.status ? 'regular' : 'inversed', color: 'primary', margin: 'none' };
    this.userImage = {
      type: this._dataModel.thumbnail ? 'avatar' : 'initials',
      size: 'medium',
      shape: 'rounded',
      initials: this._dataModel.userInitials,
      imageUrl: this._dataModel.thumbnail,
      alt: ''
    };
  }

  set dataModel(data: MePopupUserInfoI) {
    this._dataModel = data;
    this.cdr.detectChanges();
  }

  get dataModel(): MePopupUserInfoI {
    return this._dataModel;
  }

  customEvent(eventName: string): void {
    this.popupUserInfoEvent.emit({ eventName });
  }
}
