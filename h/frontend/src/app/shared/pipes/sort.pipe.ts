import { Pipe, PipeTransform } from '@angular/core';
import { sortArray } from '../helpers/general';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  public transform(value: any[], sortKey: string, sortOrder: 'ascend' | 'descend' = 'ascend' ): any[] {
    return sortArray(value, sortKey, sortOrder);
  }
}

