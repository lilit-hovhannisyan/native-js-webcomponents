import { HttpErrorResponse } from '@angular/common/http';
import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { AuthStoreService } from '@core-services/auth-store.service';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { RouterStoreService } from '@core-services/router-store.service';
import { PatternValidator } from '@core-validators/custom-validators';
import { GlobalService } from '@core-services/global.service';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { AuthWebService } from '@features/auth/auth.web-service';
import { BaseLoggerComponent } from '@features/tracking-system';

import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeButtonI } from '@shared/me-components/me-button/me-button.interface';
import { UserProfileModel } from '@shared/models/user-profile/user-profile.model';
import { MeUploadImageI } from '@shared/me-components/me-upload-image/me-upload-image.interface';
import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { bytesToSize } from '@shared/utils';
import { FileWebService } from '@shared/services/file.web-service';
import { FileUploadResponseModel } from '@shared/models/file-upload/file-upload-response.model';
import { MeBasicAlertI } from '@shared/me-components/me-basic-alert/me-basic-alert.interface';
import { UserProfileWebService } from './user-profile.web-service';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
  providers: [UserProfileWebService, AuthWebService, FileWebService]
})
export class UserProfileComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  componentVersion: string = '0.0.1';

  attributesForm!: FormGroup;

  userProfile: UserProfileModel = new UserProfileModel();
  uploadImageData!: MeUploadImageI;
  private thumbnailAttributeName = 'avatar';

  i18nStrings: MeTranslationI = {};

  uploadButton: MeButtonI = { type: 'transparent', color: 'primary' };
  cancelButton: MeButtonI = { type: 'regular', color: 'secondary' };
  updateButton: MeButtonI = { type: 'regular', color: 'primary', disabled: true };

  _updateUserProfileSubscription!: Subscription;
  _getI18nStringsSubscription!: Subscription;
  private _getUploadedImageReferenceIDSubscription!: Subscription;
  private _definitionStoreDataLoadedSubscription!: Subscription;

  get firstNameControl(): AbstractControl | null {
    return this.attributesForm.get('firstName');
  }

  get lastNameControl(): AbstractControl | null {
    return this.attributesForm.get('lastName');
  }

  get preferredNameControl(): AbstractControl | null {
    return this.attributesForm.get('preferredName');
  }

  get jobTitleControl(): AbstractControl | null {
    return this.attributesForm.get('jobTitle');
  }

  get languageControl(): AbstractControl | null {
    return this.attributesForm.get('language');
  }

  // These are variables for second tab - password chang
  passwordForm!: FormGroup;
  // reset: boolean = false;
  changePasswordSent: boolean = false;

  changePasswordSubscription!: Subscription;
  getSweetAlertSubscription!: Subscription;

  checkPasswordStrengthOpen: boolean = false;
  showPassword: boolean = false;

  availableLanguages: string[] = this.translateService.getLanguages();

  // @ViewChild('passwordField') passwordField!: ElementRef;

  get oldPasswordControl(): AbstractControl | null {
    return this.passwordForm.get('oldPassword');
  }

  get newPasswordControl(): AbstractControl | null {
    return this.passwordForm.get('newPassword');
  }

  get confirmPasswordControl(): AbstractControl | null {
    return this.passwordForm.get('confirmPassword');
  }

  constructor(
    injector: Injector,
    private definitionsStoreService: DefinitionsStoreService,
    private fileWebService: FileWebService,
    private globalService: GlobalService,
    private translateService: MeTranslationService,
    private authStoreService: AuthStoreService,
    private userProfileWebService: UserProfileWebService,
    private authWebService: AuthWebService,
    private basicAlertService: MeBasicAlertService,
    private router: Router,
    private routerStoreService: RouterStoreService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.getTranslations();

    if (this.authStoreService.user) {
      this.userProfile.user = this.authStoreService.user;
    }
    if (this.authStoreService.company) {
      this.userProfile.company = this.authStoreService.company;
    }

    this.globalService.activateLoader();
    this._definitionStoreDataLoadedSubscription = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.globalService.deactivateLoader();
        if (this.userProfile) {
          const profileObj: { firstName: string; lastName: string; preferredName: string; jobTitle: string; language: string } = {
            firstName: this.userProfile.user.attributes.firstName,
            lastName: this.userProfile.user.attributes.lastName,
            preferredName: this.userProfile.user.attributes.preferredName,
            jobTitle: this.userProfile.user.attributes.jobTitle,
            language: this.userProfile.user.attributes.language
          };
          this.generateUpdateForm(profileObj);
        }

        this.attributesForm.statusChanges.subscribe(() => {
          this.updateButton = { ...this.updateButton, disabled: !this.attributesForm.dirty || this.attributesForm.invalid };
        });

        this.attributesForm.addControl(this.thumbnailAttributeName, new FormControl(null));
        this.updateThumbnail(this.userProfile.user.attributes.avatar);
        if (this.definitionsStoreService.thumbnailProperties) {
          const splitString = this.definitionsStoreService.thumbnailProperties.supportedFiles.split(',');
          this.uploadImageData = {
            title: this.i18nStrings.userImage,
            thumbnail: this.userProfile.user.attributes.avatar
              ? `${MARKETPLACE_BASE_API_URL}/files/download/${this.userProfile.user.attributes.avatar}`
              : '',
            thumbnailProperties: this.definitionsStoreService.thumbnailProperties,
            uploadImageButton: { type: 'regular', color: 'primary' },
            isThumbnailStatusInvalid: false,
            acceptPhotoExtensions: `.${splitString.join(',.')}`,
            supportedThumbnailFiles: this.definitionsStoreService.thumbnailProperties.supportedFiles.replace(/,/g, ', '),
            maxUploadSize: bytesToSize(this.definitionsStoreService.thumbnailProperties.maxSize)
          };
        }

        this.generatePasswordForm();
      }
    });
  }

  getTranslations(): void {
    this._getI18nStringsSubscription = this.translateService.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;
    });
  }

  private generateUpdateForm(profileAtts: {
    firstName: string;
    lastName: string;
    preferredName: string;
    jobTitle: string;
    language: string;
  }): void {
    this.attributesForm = new FormGroup({
      firstName: new FormControl(profileAtts.firstName, [Validators.required]),
      lastName: new FormControl(profileAtts.lastName, [Validators.required]),
      preferredName: new FormControl(profileAtts.preferredName),
      jobTitle: new FormControl(profileAtts.jobTitle),
      language: new FormControl(profileAtts.language)
    });
  }

  private generatePasswordForm(): void {
    this.passwordForm = new FormGroup(
      {
        oldPassword: new FormControl('', [Validators.required]),
        newPassword: new FormControl('', [
          Validators.required,
          // check whether the entered password has a number
          PatternValidator(/\d/, { hasNumber: true }),
          //  check whether the entered password has upper case letter
          PatternValidator(/[A-Z]/, { hasCapitalCase: true }),
          //  check whether the entered password has a lower-case letter
          PatternValidator(/[a-z]/, { hasSmallCase: true }),
          //  check whether the entered password has a special character
          PatternValidator(/[\!#$%&\\(\)@\_\.]/, { hasSpecialCharacters: true }),
          //  Has a minimum length of 8 characters
          Validators.minLength(8)
        ]),
        confirmPassword: new FormControl('', [Validators.required])
      },
      {
        validators: (control: AbstractControl) =>
          control.get('newPassword')?.value !== control.get('confirmPassword')?.value ? { noPassswordMatch: true } : null
      }
    );
    // Manually removing errors because stepper setting form fields invalid after submit and on step change...
    setTimeout(() => {
      Object.keys(this.passwordForm.controls).forEach((name) => {
        const control = this.passwordForm.controls[name];
        control.setErrors(null);
      });
    }, 0);
  }

  handleImageFormData(recievedFile: File): void {
    const formData = new FormData();
    formData.append(
      'meta',
      JSON.stringify({
        type: this.definitionsStoreService.thumbnailProperties?.type
      })
    );
    formData.append('file', recievedFile, recievedFile.name);

    this._getUploadedImageReferenceIDSubscription = this.fileWebService.uploadFile(formData, FileUploadResponseModel).subscribe(
      (response) => {
        this.uploadImageData = {
          thumbnail: `${MARKETPLACE_BASE_API_URL}/files/download/${response.oid}`,
          showProgress: false
        };
        if (response.oid) {
          this.updateThumbnail(response.oid);
        } else {
          this.updateThumbnail(null);
        }
      },
      (error: ErrorResponseI) => {
        this.uploadImageData = { ...this.uploadImageData, isThumbnailStatusInvalid: true };
        // TODO move this to some service that will show basic error alerts
        const basicAlertData: MeBasicAlertI = {
          mode: 'error',
          title: error.error.title,
          content: error.error.details
        };
        this.basicAlertService.openBasicAlert(basicAlertData);
      }
    );
  }

  updateThumbnail(thumbnailOR: string | null): void {
    if (this.attributesForm) {
      this.attributesForm.controls[this.thumbnailAttributeName].setValue(thumbnailOR);
      this.attributesForm.markAsDirty();
    }
  }

  updateUserProfile(): void {
    this._updateUserProfileSubscription = this.userProfileWebService
      .updateUser({
        entities: [
          {
            domainName: this.userProfile.user.domainName,
            attributes: this.attributesForm.value
          }
        ]
      })
      .pipe(
        finalize(() => {
          this.basicAlertService.openBasicAlert({
            mode: 'success',
            title: this.i18nStrings.congratulations,
            content: this.i18nStrings.profileUpdated
          });
        })
      )
      .subscribe(
        (response) => {
          this.authStoreService.user = response.user;
          this.authStoreService.company = response.company;
          this.authStoreService.language = response.user.attributes.language;
        },
        (error: HttpErrorResponse) => {
          console.log(error);
        }
      );
  }

  cancel(event: Event): void {
    event.preventDefault();
    const prevUrl = this.routerStoreService.getPreviousUrl();
    if (prevUrl === '/user-profile') {
      this.router.navigate(['/']);
    } else {
      this.router.navigate([prevUrl]);
    }
  }

  activateTabIndex(index: number): void {
    if (index === 1) {
      this.generatePasswordForm();
    }
  }

  changePassword(): void {
    this.changePasswordSubscription = this.authWebService
      .requestChangePassword({
        oldPassword: this.oldPasswordControl?.value,
        newPassword: this.newPasswordControl?.value
      })
      .pipe(
        finalize(() => {
          this.changePasswordSent = false;
        })
      )
      .subscribe(
        () => {
          this.changePasswordSent = true;
          this.generatePasswordForm();

          this.basicAlertService.openBasicAlert({
            mode: 'success',
            title: this.i18nStrings.congratulations,
            content: this.i18nStrings.passwordChanged
          });
        },
        (error: ErrorResponseI) => {
          // TODO move this to some service that will show basic error alerts
          this.basicAlertService.openBasicAlert({
            mode: 'error',
            title: error.error.title,
            content: error.error.details
          });
        }
      );
  }

  checkPasswordStrength(): void {
    this.checkPasswordStrengthOpen = !this.checkPasswordStrengthOpen;
  }

  changeShowPasswordState(): void {
    this.showPassword = !this.showPassword;
  }

  ngOnDestroy(): object {
    if (this._updateUserProfileSubscription) {
      this._updateUserProfileSubscription.unsubscribe();
    }
    if (this._getI18nStringsSubscription) {
      this._getI18nStringsSubscription.unsubscribe();
    }
    if (this.changePasswordSubscription) {
      this.changePasswordSubscription.unsubscribe();
    }
    if (this.getSweetAlertSubscription) {
      this.getSweetAlertSubscription.unsubscribe();
    }
    if (this._getUploadedImageReferenceIDSubscription) {
      this._getUploadedImageReferenceIDSubscription.unsubscribe();
    }
    if (this._definitionStoreDataLoadedSubscription) {
      this._definitionStoreDataLoadedSubscription.unsubscribe();
    }
    return super.ngOnDestroy();
  }
}
