import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { IBookingAccount, BookingAccountKey } from 'src/app/core/models/resources/IBookingAccount';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { BookingAccountMainService } from 'src/app/core/services/mainServices/booking-account-main.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/booking-accounts.config';
import { AbstractValueAccessor, makeProvider } from 'src/app/core/abstracts/value-accessor';
import { SettingsService } from 'src/app/core/services/settings.service';
@Component({
  selector: 'nv-booking-account-select-field',
  templateUrl: './booking-account-select-field.component.html',
  providers: [makeProvider(BookingAccountSelectFieldComponent)],
})
export class BookingAccountSelectFieldComponent extends AbstractValueAccessor implements OnInit {
  @Input() public bookingAccounts$: Observable<IBookingAccount[]>;
  @Input() public queryParams = {};
  @Input() public useFullEntity = false;

  @Output() public valueChange = new EventEmitter<IBookingAccount>();

  private bookingAccounts: IBookingAccount[] = [];

  public getGridConfig = getGridConfig;
  public resourceFormControl = new FormControl({ value: null });

  constructor(
    private bookingAccountRepo: BookingAccountRepositoryService,
    private bookingAccountService: BookingAccountMainService,
    private settingsService: SettingsService,
  ) {
    super();
  }

  public ngOnInit(): void {
    const language = this.settingsService.language;
    this.getLabel = (bookingAccount: IBookingAccount) => `${bookingAccount.no} ${bookingAccount.displayLabel[language]}`;
    if (!this.bookingAccounts$) {
      if (this.useFullEntity) {
        this.bookingAccounts$ = this.bookingAccountService.getStream();
      } else {
        this.bookingAccounts$ = this.bookingAccountRepo.fetchAll({ queryParams: this.queryParams });
      }
    }
  }

  public getLabel = (bookingAccount: IBookingAccount) =>
    `${bookingAccount.no} ${bookingAccount.displayLabel}`

  public onSelect(value: number): void {
    this.onChange(value);
    this.onTouched(value);
    this.onBookingAccount(value);
  }

  public writeValue(value: number): void {
    this.onBookingAccount(value);
    this.resourceFormControl.setValue(value);
  }

  public setDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.resourceFormControl.disable();
    } else {
      this.resourceFormControl.enable();
    }
  }

  private onBookingAccount(id: BookingAccountKey) {
    const bookingAccount = this.bookingAccounts.find(b => b.id === id);
    this.valueChange.emit(bookingAccount);
  }
}
