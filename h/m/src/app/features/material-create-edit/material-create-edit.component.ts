import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { AfterViewInit, Component, ElementRef, Injector, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MatHorizontalStepper, MatStep } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';

import { combineLatest, Subject, Subscription } from 'rxjs';
import { debounceTime, finalize, skip, take } from 'rxjs/operators';

import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { GlobalService } from '@core-services/global.service';
import { DeactivatableComponent } from '@core-interfaces/deactivatable-component.interface';
import { RouterStoreService } from '@core-services/router-store.service';

import { BaseLoggerComponent } from '@features/tracking-system';
import { DocumentStoreService } from '@features/documents/documents-store.service';
import {
  MARKETPLACE_BASE_API_URL,
  MATERIAL_ATTRIBUTE_ASK_FOR_QUOTE,
  MATERIAL_ATTRIBUTE_PRICE,
  MATERIAL_ATTRIBUTE_PRICE_RMB,
  ME_DOMAIN_APPLICATION_DOCUMENT,
  ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL,
  ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL,
  ME_DOMAIN_APPLICATION_MATERIAL
} from '@shared/constants';
import { MeAttributeLayoutTypes } from '@shared/me-attribute-layout-types';
import { MeAttributeTypes } from '@shared/me-attribute-types';
import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';
import { MeSelectInputComponent } from '@shared/me-components/me-attribute/me-select-input/me-select-input.component';
import { MeBasicAlertI } from '@shared/me-components/me-basic-alert/me-basic-alert.interface';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeButtonI } from '@shared/me-components/me-button/me-button.interface';
import { MeDocumentCardI } from '@shared/me-components/me-document-card/me-document-card.interface';
import { MeEmptyContentI } from '@shared/me-components/me-empty-content/me-empty-content-interface';
import { MePopupSelectService } from '@shared/me-components/me-popup-select/me-popup-select.service';
import { MeSearchBarI } from '@shared/me-components/me-search-bar/me-search-bar.interface';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { MeUploadImageI } from '@shared/me-components/me-upload-image/me-upload-image.interface';
import { RequiredConstraintState } from '@shared/models/attribute/attribute.constraint.model';
import { AttributeModelTypes, DMChoiceAttributeModel } from '@shared/models/attribute/attribute.model';
import { MeSelectInputAttribute } from '@shared/models/attribute/me-select-input-attribute.model';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { DomainModel } from '@shared/models/domain/domain.model';
import { EnumValueModel } from '@shared/models/enum/enum-value.model';
import { FileUploadResponseModel } from '@shared/models/file-upload/file-upload-response.model';
import { LayoutGroupAttributeModel } from '@shared/models/layout/layout-group-attribute.model';
import { LayoutGroupModel } from '@shared/models/layout/layout-group.model';
import { MaterialModel } from '@shared/models/materials/material-model';
import { bytesToSize, getFormatedDate, mapExtensionToIcon } from '@shared/utils';
import { MePopupSelectI } from '@shared/me-components/me-popup-select/me-popup-select.interface';
import { MeBreadcrumbActionI } from '@shared/me-components/me-breadcrumb/me-breadcrumb-action.interface';
import { PropertiesModel } from '@shared/models/attribute/attribute.properties.model';
import { QuickSearchModel } from '@shared/models/search/quick-search.model';
import { MeSearchDirectionTypes, MeSearchSortByTypes } from '@shared/me-search-sort-by-types';
import { FileWebService } from '@shared/services/file.web-service';
import { InputAttributeService } from '@shared/services/input-attribute.service';

import { MaterialDocumentStoreService } from './material-documents-store.service';
import { MaterialCreateEditWebService } from './material-create-edit.web-service';
import { MaterialEntityI } from './material-entity.interface';
import { SubSink } from 'subsink';
import { HttpErrorResponse } from '@angular/common/http';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-material-create-edit',
  templateUrl: './material-create-edit.component.html',
  styleUrls: ['./material-create-edit.component.scss'],
  providers: [MaterialCreateEditWebService, MePopupSelectService, MaterialDocumentStoreService, FileWebService, InputAttributeService]
})
export class MaterialCreateEditComponent extends BaseLoggerComponent implements OnInit, OnDestroy, AfterViewInit, DeactivatableComponent {
  componentVersion: string = '0.0.1';

  private subs = new SubSink();

  domain: DomainModel | undefined;
  groups: LayoutGroupModel[] = [];
  material!: MaterialModel;
  extendedGroups: LayoutGroupModel[] = [];
  attributes: AttributeModelTypes[] = []; // array of general material attribute objects (tab 1 and 2)
  extendedAttributes: AttributeModelTypes[] = []; // array of material specific attribute objects (tab 3)
  componentLoading: boolean = true;
  stepOneGroups: string[] = ['materialGeneralInfo'];
  stepTwoGroups: string[] = ['physicalSampleBookInfo', 'materialBasicInfo'];
  generalInfoLayoutGroups!: LayoutGroupModel[];
  generalAttributeLayoutGroups!: LayoutGroupModel[];
  extendedLayoutGroups!: LayoutGroupModel[];
  selectedMaterialType!: string;
  // Buttons used in form
  buttonNext!: { label: string; data: MeButtonI };
  buttonBack!: { label: string; data: MeButtonI };
  buttonCreate!: { label: string; data: MeButtonI };
  buttonFinish!: { label: string; data: MeButtonI };
  buttonSubmit!: { label: string; data: MeButtonI };

  @ViewChild('scrollableDiv') scrollableDiv!: ElementRef;
  @ViewChild('stepper') stepper!: MatHorizontalStepper;
  @ViewChild('stepOne') stepOne!: MatStep;
  @ViewChild('stepFour') materialsStep!: MatStep;
  @ViewChild('popupDocumentGrid', { read: TemplateRef }) popupDocumentGridRef!: TemplateRef<Component>;
  @ViewChild('addButton', { read: TemplateRef }) addButton!: TemplateRef<Component>;

  private appliedState: RequiredConstraintState = RequiredConstraintState.draft;
  breadcrumbActions: MeBreadcrumbActionI[] = [];

  // Tab forms
  materialInfoAttributeForm!: FormGroup;
  generalAttributesForm!: FormGroup;
  materialPropertiesAttributeForm!: FormGroup | null;
  documentsForm: FormGroup = new FormGroup({});
  materialTypeAttributeItem!: MeAttributeItem;
  materialInfoAttributeItemMap!: Map<string, MeAttributeItem>;
  generalAttributeItemMap!: Map<string, MeAttributeItem>;
  materialPropertiesAttributeItemMap!: Map<string, MeAttributeItem>;
  uploadImageData!: MeUploadImageI;
  documentsIsSearchTab: boolean = false;
  documentPopupData!: MePopupSelectI;
  searchBarDataMaterialDocuments!: MeSearchBarI;

  materialTypesAttribute: DMChoiceAttributeModel = new DMChoiceAttributeModel();

  // Documents popup
  private documentFilterChanged: Subject<void> = new Subject();
  private selectedDocumentType: string = ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL;
  documentsIsSearch: boolean = false;
  documentsTrackForBottomReach: boolean = false;
  documentsSearchBar!: MeSearchBarI;
  documentDomain!: DomainModel;
  documentsSearchKeyword: string = '';
  sortByValue: MeSearchSortByTypes = MeSearchSortByTypes.title;
  sortByDirection: MeSearchDirectionTypes = MeSearchDirectionTypes.ascending;
  areDocumentsLoading: boolean = false;
  doocumentsEmptyContent!: MeEmptyContentI;
  filterDelay: number = 500;
  documentSelectionStore!: DocumentStoreService;
  isShowSelectedActive: boolean = false;

  documentCardItems: MeDocumentCardI[] = [];
  addedDocumentCardItems: MeDocumentCardI[] = [];
  emptyContent!: MeEmptyContentI;

  applyCheck: boolean = false;
  leaveWarningModel!: MeSweetAlertI;

  private thumbnailAttributeName = 'thumbnail';
  invalidThumbnail: boolean = false;
  private materialId: string | null = '';
  private readonly NUMBER_OF_ITEMS_ON_PAGE = 50;
  private _addedFrom: number = 0;
  private _addedTo: number = this.NUMBER_OF_ITEMS_ON_PAGE;

  private _getSweetAlertSubscription!: Subscription;
  private _getUploadedImageReferenceIDSubscription!: Subscription;
  private _getI18nStringsSubscription!: Subscription;
  private _getMaterialSubscription!: Subscription;
  private _createMaterialSubscription!: Subscription;
  private _updateMaterialSubscription!: Subscription;
  private _removeDocumentSubscription!: Subscription;
  private _definitionStoreDataLoadedSubscription!: Subscription;
  private _materialTypeValueChangesSubscription!: Subscription;
  private _getDocumentDataForDocumentCardSubscription!: Subscription;
  private _submitDocumentsSubscription!: Subscription;
  private _getAddedMaterialDocumentsSubscription!: Subscription;
  private _getAddedDocumentDataForDocumentCardSubscription!: Subscription;
  private _listenForFormChangeSubscription!: Subscription;
  private _listenForSteperLoadSubscription!: Subscription;
  private _listenForDocumentTypeChangeSubscription!: Subscription;
  private _getDocumentsSubscription!: Subscription;
  private _listenForFilterChangeSubscription!: Subscription;
  private _checkUniqueAttributesSubscription!: Subscription;

  i18nStrings: MeTranslationI = {};

  componentHeader!: MeSearchBarI;
  documentsStepEnabled: boolean = false;
  isDuplicateMaterial: boolean = false;

  private _isInProgress: boolean = false;
  public get isInProgress(): boolean {
    return this._isInProgress;
  }
  public set isInProgress(progress: boolean) {
    this._isInProgress = progress;
    this.updateSubmitButton();
  }

  constructor(
    injector: Injector,
    private translate: MeTranslationService,
    private definitionsStoreService: DefinitionsStoreService,
    private meBasicAlertService: MeBasicAlertService,
    private meSweetAlertService: MeSweetAlertService,
    public inputAttributeService: InputAttributeService,
    private webService: MaterialCreateEditWebService,
    private fileWebService: FileWebService,
    private router: Router,
    private route: ActivatedRoute,
    private popupSelectService: MePopupSelectService,
    private _documentStoreService: MaterialDocumentStoreService,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private globalService: GlobalService,
    private routerStoreService: RouterStoreService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.materialId = this.route.snapshot.paramMap.get('id');
    this.isDuplicateMaterial = this.router.url.indexOf('/materials/duplicate') > -1;
    // subscribe to translations
    this._getI18nStringsSubscription = this.translate.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;

      this.prepareButtons();
      this.documentsSearchBar = {
        filterEnabled: false,
        search: {
          placeholder: this.i18nStrings.searchFor,
          keyword: '',
          minCharacters: 1
        },
        sort: {
          placeholder: this.i18nStrings.sortBy,
          options: [
            { key: 'azAlphabetically', value: this.i18nStrings.azAlphabetically },
            { key: 'zaAlphabetically', value: this.i18nStrings.zaAlphabetically },
            { key: 'newest', value: this.i18nStrings.newest },
            { key: 'oldest', value: this.i18nStrings.oldest }
          ],
          selectedIndex: 0
        },
        selectButton: {
          enabled: false,
          placeholder: ''
        },
        operationButtons: []
      };
      this.emptyContent = {
        title: this.i18nStrings.addYourFirstDocument,
        comment: this.i18nStrings.addYourFirstDocumentDescription,
        buttonText: this.i18nStrings.add,
        emptyIconName: 'empty-documents'
      };

      this.leaveWarningModel = {
        icon: 'alert-triangle',
        mode: 'warning',
        title: 'Warning',
        message: this.i18nStrings.leaveWarningMessage,
        type: { name: MeSweetAlertTypeEnum.submit, buttons: { submit: 'Leave', cancel: 'Stay on page' } }
      };

      this.breadcrumbActions = [
        {
          button: {
            id: 'cancelButton',
            data: { color: 'secondary', type: 'regular' },
            text: this.i18nStrings.cancel
          },
          callBack: () => {
            const lastRoute = this.routerStoreService.getPreviousUrl();
            if (lastRoute === decodeURI(this.router.routerState.snapshot.url)) {
              this.router.navigate(['materials']);
            } else {
              this.router.navigate([lastRoute]);
            }
          }
        }
      ];
      this.breadcrumbSupportService.breadcrumbActions = this.breadcrumbActions;

      this.globalService.activateLoader();
      if (this.materialId) {
        this.initializeEdit();
      } else {
        this.initializeCreate();
      }
    });
    this.setSearchBarDataMaterialDocuments();
  }

  private prepareFilterData(): void {
    this.documentDomain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL);

    const documentTypes = [ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL, ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL].map((dom) => {
      const domainObj = this.definitionsStoreService.getDomain(dom);
      return {
        displayName: domainObj.displayName,
        value: domainObj.internalName
      };
    });

    const dmDocumentTypeFilterAttribute: DMChoiceAttributeModel = {
      constraints: [],
      dataTypeClass: 'moa',
      displayName: this.i18nStrings.documentType,
      internalName: 'documentType',
      properties: new PropertiesModel(),
      required: false,
      optRequired: false,
      value: this.selectedDocumentType,
      values: documentTypes,
      disabled: false,
      unique: false
    };

    const meDocumentTypeFilterAttribute: MeSelectInputAttribute = new MeSelectInputAttribute();
    meDocumentTypeFilterAttribute.data = dmDocumentTypeFilterAttribute;
    meDocumentTypeFilterAttribute.form = new FormGroup({
      [dmDocumentTypeFilterAttribute.internalName]: new FormControl(dmDocumentTypeFilterAttribute.value)
    });

    this.documentsSearchBar = {
      ...this.documentsSearchBar,
      singleValueFilter: new MeAttributeItem(MeSelectInputComponent, meDocumentTypeFilterAttribute)
    };

    this._listenForDocumentTypeChangeSubscription = meDocumentTypeFilterAttribute.form.controls.documentType.valueChanges
      .pipe(skip(1))
      .subscribe((value) => {
        this.selectedDocumentType = value;
        this.documentFilterChanged.next();
      });
  }

  private initializeCreate(): void {
    this._definitionStoreDataLoadedSubscription = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        if (this.definitionsStoreService.thumbnailProperties) {
          const splitString = this.definitionsStoreService.thumbnailProperties.supportedFiles.split(',');
          this.uploadImageData = {
            thumbnail: '',
            thumbnailProperties: this.definitionsStoreService.thumbnailProperties,
            uploadImageButton: { type: 'regular', color: 'primary' },
            isThumbnailStatusInvalid: false,
            acceptPhotoExtensions: `.${splitString.join(',.')}`,
            supportedThumbnailFiles: this.definitionsStoreService.thumbnailProperties.supportedFiles.replace(/,/g, ', '),
            maxUploadSize: bytesToSize(this.definitionsStoreService.thumbnailProperties.maxSize)
          };
        }

        this.domain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_MATERIAL);
        this.globalService.deactivateLoader();

        if (this.domain) {
          // transform each domain attribute to AttributeModel class or its extensions (classes that extend it)
          this.attributes = this.inputAttributeService.transformAttributesArray(this.domain.attributes);

          // populate groups from selected layout
          this.groups = this.inputAttributeService.getLayoutGroups(this.domain.layouts, MeAttributeLayoutTypes.CREATE);

          // sort and generate rows
          this.groups = this.inputAttributeService.generateLayoutRows(this.groups);

          // generate angular forms which will have form group as root for every tab
          // each attribute will be a form control
          // add validators to attribtes where applicable
          // load enums to single/multiple choices elements
          this.generalInfoLayoutGroups = this.getLayoutForGroups(this.stepOneGroups);
          if (this.generalInfoLayoutGroups && this.generalInfoLayoutGroups.length) {
            this.materialInfoAttributeForm = this.inputAttributeService.createFormGroup(
              this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups),
              this.appliedState
            );

            this.materialInfoAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
              this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups),
              this.materialInfoAttributeForm,
              this.appliedState
            );

            this._pricingAttributesCustomChecker();
          }
          this.generalAttributeLayoutGroups = this.getLayoutForGroups(this.stepTwoGroups);
          if (this.generalAttributeLayoutGroups && this.generalAttributeLayoutGroups.length) {
            this.generalAttributesForm = this.inputAttributeService.createFormGroup(
              this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalAttributeLayoutGroups),
              this.appliedState
            );

            this.generalAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
              this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalAttributeLayoutGroups),
              this.generalAttributesForm,
              this.appliedState
            );
          }

          this.attachMaterialTypeControl(this.domain);

          // get attribute and create control for thumbnail
          const isRequired: boolean = this.inputAttributeService.isAttributeRequired(
            this.attributes,
            this.thumbnailAttributeName,
            this.appliedState
          );
          const validators: ValidatorFn[] = [];
          if (isRequired) {
            validators.push(Validators.required);
          }
          this.materialInfoAttributeForm.addControl(this.thumbnailAttributeName, new FormControl(null, validators));

          this._listenForFormChangeSubscription = this.materialInfoAttributeForm.valueChanges.subscribe(() => {
            if (this.materialInfoAttributeForm.dirty) {
              this.applyCheck = true;
            }
          });
        }
      }
    });
  }

  initializeEdit(): void {
    this._definitionStoreDataLoadedSubscription = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this._getMaterialSubscription = this.webService.getMaterialById(encodeURI(this.materialId as string)).subscribe((response) => {
          this.material = response;
          if (!this.isDuplicateMaterial) {
            this.documentsStepEnabled = true;
          }
          this.getAddedMaterialDocuments();
          this.prepareAddedDocumentData();
          if (this.material.state) {
            this.appliedState = this.material.state;
          }
          if (this.definitionsStoreService.thumbnailProperties) {
            const splitString = this.definitionsStoreService.thumbnailProperties.supportedFiles.split(',');
            this.uploadImageData = {
              thumbnail: this.material.attributes.thumbnail
                ? `${MARKETPLACE_BASE_API_URL}/files/download/${this.material.attributes.thumbnail}`
                : '',
              thumbnailProperties: this.definitionsStoreService.thumbnailProperties,
              uploadImageButton: { type: 'regular', color: 'primary' },
              isThumbnailStatusInvalid: false,
              acceptPhotoExtensions: `.${splitString.join(',.')}`,
              supportedThumbnailFiles: this.definitionsStoreService.thumbnailProperties.supportedFiles.replace(/,/g, ', '),
              maxUploadSize: bytesToSize(this.definitionsStoreService.thumbnailProperties.maxSize)
            };
          }
          this.domain = this.definitionsStoreService.getDescendantDomain(ME_DOMAIN_APPLICATION_MATERIAL, this.material.domainName);
          this.globalService.deactivateLoader();
          if (this.domain) {
            this.attributes = this.inputAttributeService.transformAttributesArray(this.domain.attributes);
            this.inputAttributeService.applyUpdateFormValues(this.attributes, this.material);
            this.groups = this.inputAttributeService.getLayoutGroups(this.domain.layouts, MeAttributeLayoutTypes.UPDATE);
            this.groups = this.inputAttributeService.generateLayoutRows(this.groups);

            this.generalInfoLayoutGroups = this.getLayoutForGroups(this.stepOneGroups);
            if (this.generalInfoLayoutGroups && this.generalInfoLayoutGroups.length) {
              this.materialInfoAttributeForm = this.inputAttributeService.createFormGroup(
                this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups),
                this.appliedState
              );

              // get attribute and create control for thumbnail
              const isRequired: boolean = this.inputAttributeService.isAttributeRequired(
                this.attributes,
                this.thumbnailAttributeName,
                this.appliedState
              );
              const validators: ValidatorFn[] = [];
              if (isRequired) {
                validators.push(Validators.required);
              }
              this.materialInfoAttributeForm.addControl(
                this.thumbnailAttributeName,
                new FormControl(this.material.attributes.thumbnail, validators)
              );

              this.materialInfoAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
                this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups),
                this.materialInfoAttributeForm,
                this.appliedState
              );

              this._pricingAttributesCustomChecker();
            }

            this.generalAttributeLayoutGroups = this.getLayoutForGroups(this.stepTwoGroups);
            if (this.generalAttributeLayoutGroups && this.generalAttributeLayoutGroups.length) {
              this.generalAttributesForm = this.inputAttributeService.createFormGroup(
                this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalAttributeLayoutGroups),
                this.appliedState
              );

              this.generalAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
                this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalAttributeLayoutGroups),
                this.generalAttributesForm,
                this.appliedState
              );
            }

            this.extendedLayoutGroups = this.getLayoutForGroupsExcluding([...this.stepOneGroups, ...this.stepTwoGroups]);
            if (this.extendedLayoutGroups && this.extendedLayoutGroups.length) {
              this.materialPropertiesAttributeForm = this.inputAttributeService.createFormGroup(
                this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.extendedLayoutGroups),
                this.appliedState
              );

              this.materialPropertiesAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
                this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.extendedLayoutGroups),
                this.materialPropertiesAttributeForm,
                this.appliedState
              );
            }

            // Track changes, if user changed value check when he try to leave page
            // Combine latest (instead of race) is currently best way to listen for multiple observables
            const observableArray = [this.materialInfoAttributeForm.valueChanges, this.generalAttributesForm.valueChanges];
            if (this.materialPropertiesAttributeForm) {
              observableArray.push(this.materialPropertiesAttributeForm.valueChanges);
            }
            this._listenForFormChangeSubscription = combineLatest(observableArray).subscribe(() => {
              if (this.materialInfoAttributeForm.dirty || this.generalAttributesForm.dirty || this.materialPropertiesAttributeForm?.dirty) {
                this.applyCheck = true;
              }
            });

            this.attachMaterialTypeControl(this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_MATERIAL), true);

            if (this.isDuplicateMaterial) {
              this.materialInfoAttributeForm.markAsDirty();
            }
          }
        });
      }
    });
  }

  ngAfterViewInit(): void {
    // TODO: can be sync with new loader, now there is small delay on loading finish
    if (this.materialId) {
      this._listenForSteperLoadSubscription = this.stepper.animationDone.pipe(take(1)).subscribe(() => {
        this.stepper.steps.forEach((s) => (s.interacted = true));
      });
    }
  }

  checkStepBeforeSwitch(switchedTo: number): void {
    const currentStep: MatStep = this.stepper.steps.toArray()[this.stepper.selectedIndex];
    if (!currentStep.completed) {
      if (this.stepper.selectedIndex !== switchedTo) {
        currentStep.stepControl.markAllAsTouched();
      }
      if (this.stepper.selectedIndex < switchedTo) {
        const emptyRequiredAttributes: AttributeModelTypes[] = [];
        const currentStepControl: FormGroup = currentStep.stepControl as FormGroup;
        // Collect all empty required attributes
        Object.keys(currentStepControl.controls).forEach((controlName) => {
          const validationErrors: ValidationErrors | null = currentStepControl.controls[controlName].errors;
          if (validationErrors && validationErrors.required) {
            const attribute = [...this.attributes, ...this.extendedAttributes, this.materialTypesAttribute].find(
              (attr) => attr.internalName === controlName
            );
            if (attribute) {
              emptyRequiredAttributes.push(attribute);
            }
          }
        });

        if (emptyRequiredAttributes.length > 0) {
          const basicAlertData: MeBasicAlertI = {
            mode: 'error',
            title: this.i18nStrings.insertRequiredData,
            content: this.i18nStrings.requiredFields
          };
          emptyRequiredAttributes.forEach((attribute, index) => {
            basicAlertData.content += ` ${attribute.displayName}`;
            basicAlertData.content += emptyRequiredAttributes.length - 1 !== index ? ',' : '';
          });
          this.meBasicAlertService.openBasicAlert(basicAlertData);

          // const queryString = emptyRequiredAttributes.find((attr) => attr.internalName === 'thumbnail')
          //   ? '.thumbnail-container'
          //   : 'app-form-attribute mat-form-field.mat-form-field-invalid';

          // setTimeout(() => {
          //   this.scrollToElementProvider.scrollToElementByQuery(queryString);
          //   this.scrollToElementProvider.doScroll();
          // }, 100);
        }
      }
    }
  }

  // On tab index change set previous step form as touched
  markFormAsTouched(changeObj: StepperSelectionEvent): void {
    changeObj.previouslySelectedStep.stepControl.markAllAsTouched();
  }

  checkIfThumbnailIsSet(): void {
    // const firstStepControl: FormGroup = this.stepper.steps.first.stepControl;
    // if (firstStepControl.controls.thumbnail.status === 'INVALID') {
    //   this.globalService.setThumbnailStatusInvalid(true);
    // }
  }

  prepareButtons(): void {
    this.buttonNext = { label: this.i18nStrings.next, data: { type: 'regular', color: 'primary' } };
    this.buttonBack = { label: this.i18nStrings.back, data: { type: 'regular', color: 'secondary' } };
    this.updateSubmitButton();
    this.buttonFinish = { label: this.i18nStrings.finish, data: { type: 'regular', color: 'primary' } };
  }

  updateSubmitButton(): void {
    this.buttonSubmit = {
      label: this.isDuplicateMaterial ? this.i18nStrings.duplicate : this.materialId ? this.i18nStrings.update : this.i18nStrings.create,
      data: { type: 'regular', color: 'primary', disabled: this.isInProgress }
    };
  }

  // update 3rd steppers step dependent on selected material type
  updateMaterialProperties(materialType: string): void {
    this.domain = this.definitionsStoreService.getDescendantDomain(ME_DOMAIN_APPLICATION_MATERIAL, materialType);
    if (this.domain) {
      // get material attributes
      this.attributes = this.inputAttributeService.transformAttributesArray(this.domain.attributes);
      this.groups = this.inputAttributeService.getLayoutGroups(this.domain.layouts, MeAttributeLayoutTypes.CREATE);
      this.groups = this.inputAttributeService.generateLayoutRows(this.groups);

      this.generalInfoLayoutGroups = this.getLayoutForGroups(this.stepOneGroups);
      if (this.generalInfoLayoutGroups && this.generalInfoLayoutGroups.length) {
        this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups).forEach((attr) => {
          if (!this.materialInfoAttributeForm.contains(attr.internalName)) {
            this.materialInfoAttributeForm.addControl(
              attr.internalName,
              new FormControl(null, this.inputAttributeService.applyConstraints(attr, this.appliedState))
            );

            if (!this.materialInfoAttributeItemMap.has(attr.internalName)) {
              const tempMap: Map<string, MeAttributeItem> = this.inputAttributeService.createAttributeItemMap(
                [attr],
                this.materialInfoAttributeForm,
                this.appliedState
              );
              this.materialInfoAttributeItemMap.set(attr.internalName, tempMap.get(attr.internalName) as MeAttributeItem);
            }
          }
        });
      }

      this.generalAttributeLayoutGroups = this.getLayoutForGroups(this.stepTwoGroups);
      if (this.generalAttributeLayoutGroups && this.generalAttributeLayoutGroups.length) {
        this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalAttributeLayoutGroups).forEach((attr) => {
          if (!this.generalAttributesForm.contains(attr.internalName)) {
            this.generalAttributesForm.addControl(
              attr.internalName,
              new FormControl(null, this.inputAttributeService.applyConstraints(attr, this.appliedState))
            );

            if (!this.generalAttributeItemMap.has(attr.internalName)) {
              const tempMap: Map<string, MeAttributeItem> = this.inputAttributeService.createAttributeItemMap(
                [attr],
                this.generalAttributesForm,
                this.appliedState
              );
              this.generalAttributeItemMap.set(attr.internalName, tempMap.get(attr.internalName) as MeAttributeItem);
            }
          }
        });
      }

      this.extendedLayoutGroups = this.getLayoutForGroupsExcluding([...this.stepOneGroups, ...this.stepTwoGroups]);
      if (this.extendedLayoutGroups && this.extendedLayoutGroups.length) {
        this.materialPropertiesAttributeForm = this.inputAttributeService.createFormGroup(
          this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.extendedLayoutGroups),
          this.appliedState
        );

        this.materialPropertiesAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
          this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.extendedLayoutGroups),
          this.materialPropertiesAttributeForm,
          this.appliedState
        );
      } else {
        this.materialPropertiesAttributeForm = null;
        this.materialPropertiesAttributeItemMap = new Map();
      }
    }
  }

  getLayoutForGroups(layoutGroupNames: string[]): LayoutGroupModel[] {
    const layoutGroups = this.groups.filter((group) => layoutGroupNames.findIndex((name) => group.internalName === name) > -1);

    return layoutGroups;
  }

  getLayoutForGroupsExcluding(layoutGroupNames: string[]): LayoutGroupModel[] {
    const layoutGroups = this.groups.filter((group) => layoutGroupNames.findIndex((name) => group.internalName === name) === -1);

    return layoutGroups;
  }

  handleMaterial(form: FormGroup): void {
    if (this.materialId) {
      this.updateMaterial(form);
    } else {
      this.createMaterial(form);
    }
  }

  createMaterial(form: FormGroup): void {
    form.markAllAsTouched();
    if (form.valid) {
      const materialEntity: MaterialEntityI = {
        // Set material domain name
        domainName: this.selectedMaterialType,
        attributes: {}
      };

      // Add step1 attributes and values to material entity
      if (this.generalInfoLayoutGroups && this.generalInfoLayoutGroups.length) {
        this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups).forEach((attribute) => {
          this.setAttributeWithValueToEntity(materialEntity, attribute, this.materialInfoAttributeForm);
        });
      }

      // Add thumbnail
      materialEntity.attributes[this.thumbnailAttributeName] = this.materialInfoAttributeForm.controls[this.thumbnailAttributeName].value;

      // Add step2 attributes and values to material entity
      if (this.generalAttributeLayoutGroups && this.generalAttributeLayoutGroups.length) {
        this.inputAttributeService
          .getAttributesByLayoutGroups(this.attributes, this.generalAttributeLayoutGroups)
          .forEach((attribute) => this.setAttributeWithValueToEntity(materialEntity, attribute, this.generalAttributesForm));
      }

      // Add step3 attributes and values to material entity
      if (this.extendedLayoutGroups && this.extendedLayoutGroups.length) {
        this.inputAttributeService
          .getAttributesByLayoutGroups(this.attributes, this.extendedLayoutGroups)
          .forEach((attribute) =>
            this.setAttributeWithValueToEntity(materialEntity, attribute, this.materialPropertiesAttributeForm as FormGroup)
          );
      }

      this.globalService.activateLoader();
      this.isInProgress = true;
      this._checkUniqueAttributesSubscription = this.inputAttributeService
        .checkUniqueAttributes('', this.attributes, ME_DOMAIN_APPLICATION_MATERIAL, this.materialInfoAttributeForm)
        .pipe(
          take(1),
          finalize(() => {
            this.globalService.deactivateLoader();
          })
        )
        .subscribe(
          () => {
            this._createMaterialSubscription = this.webService
              .createMaterial(materialEntity)
              .pipe(
                finalize(() => {
                  this.globalService.deactivateLoader();
                })
              )
              .subscribe((response) => {
                this.material = response;
                this.documentsStepEnabled = true;
                this.getAddedMaterialDocuments();
                this.prepareAddedDocumentData();
                // start tracking for response from sweet alert
                this._getSweetAlertSubscription = this.meSweetAlertService.getDataBackFromSweetAlert().subscribe((data) => {
                  this.materialInfoAttributeForm.markAsUntouched();
                  this.applyCheck = false;
                  if (data.confirmed) {
                    this.router.navigate([`materials/view/${response.oid}`]);
                  } else {
                    this.stepper.next();
                  }
                  // stop tracking alert
                  this._getSweetAlertSubscription.unsubscribe();
                });

                const sweetAlertModel: MeSweetAlertI = {
                  mode: 'success',
                  icon: 'check',
                  type: {
                    name: MeSweetAlertTypeEnum.submit,
                    buttons: {
                      submit: this.i18nStrings.finish,
                      cancel: this.i18nStrings.continue
                    }
                  },
                  title: this.i18nStrings.materialCreatedAlertTitle,
                  message: this.i18nStrings.materialCreatedAlertMessage
                };
                this.meSweetAlertService.openMeSweetAlert(sweetAlertModel);
              });
          },
          () => {
            this.globalService.deactivateLoader();
            this.isInProgress = false;
            this.stepper.selectedIndex = 0;
            this.showAttributesNotUniqueError();
          }
        );
    }
  }

  updateMaterial(form: FormGroup): void {
    form.markAllAsTouched();
    if (form.valid) {
      const materialChanges: MaterialEntityI = {
        domainName: this.material.domainName,
        attributes: {}
      };

      // Add step1 attributes and values to material entity
      if (this.generalInfoLayoutGroups && this.generalInfoLayoutGroups.length) {
        this.generalInfoLayoutGroups[0].attributes.forEach((attribute) =>
          this.setAttributeWithValueToEntity(materialChanges, attribute, this.materialInfoAttributeForm)
        );
      }

      // Add thumbnail
      materialChanges.attributes[this.thumbnailAttributeName] = this.materialInfoAttributeForm.controls[this.thumbnailAttributeName].value;

      // Add step2 attributes and values to material entity
      if (this.generalAttributeLayoutGroups && this.generalAttributeLayoutGroups.length) {
        this.inputAttributeService
          .getAttributesByLayoutGroups(this.attributes, this.generalAttributeLayoutGroups)
          .forEach((attribute) => this.setAttributeWithValueToEntity(materialChanges, attribute, this.generalAttributesForm));
      }

      // Add step3 attributes and values to material entity
      if (this.extendedLayoutGroups && this.extendedLayoutGroups.length) {
        this.inputAttributeService
          .getAttributesByLayoutGroups(this.attributes, this.extendedLayoutGroups)
          .forEach((attribute) =>
            this.setAttributeWithValueToEntity(materialChanges, attribute, this.materialPropertiesAttributeForm as FormGroup)
          );
      }

      this.globalService.activateLoader();
      this.isInProgress = true;
      if (!this.isDuplicateMaterial) {
        this._checkUniqueAttributesSubscription = this.inputAttributeService
          .checkUniqueAttributes(this.material.oid, this.attributes, ME_DOMAIN_APPLICATION_MATERIAL, this.materialInfoAttributeForm)
          .pipe(
            take(1),
            finalize(() => {
              this.globalService.deactivateLoader();
            })
          )
          .subscribe(
            () => {
              this._updateMaterialSubscription = this.webService
                .updateMaterial(encodeURI(this.material.oid), materialChanges)
                .subscribe(() => {
                  // start tracking for response
                  const successSubscription = this.meSweetAlertService.getDataBackFromSweetAlert().subscribe(() => {
                    form.markAsUntouched();
                    this.applyCheck = false;
                    this.router.navigate([`materials/view/${this.material.oid}`]);
                    successSubscription.unsubscribe();
                  });

                  const successAlertModel: MeSweetAlertI = {
                    mode: 'success',
                    icon: 'check',
                    type: {
                      name: MeSweetAlertTypeEnum.confirm,
                      buttons: {
                        confirm: this.i18nStrings.continue
                      }
                    },
                    title: this.i18nStrings.success,
                    message: this.i18nStrings.materialUpdated
                  };
                  this.meSweetAlertService.openMeSweetAlert(successAlertModel);
                });
            },
            () => {
              this.stepper.selectedIndex = 0;
              this.showAttributesNotUniqueError();
            }
          );
      } else {
        this._checkUniqueAttributesSubscription = this.inputAttributeService
          .checkUniqueAttributes('', this.attributes, ME_DOMAIN_APPLICATION_MATERIAL, this.materialInfoAttributeForm)
          .pipe(
            take(1),
            finalize(() => {
              this.globalService.deactivateLoader();
            })
          )
          .subscribe(
            () => {
              this._updateMaterialSubscription = this.webService
                .duplicateMaterial(encodeURI(this.material.oid), materialChanges)
                .subscribe((material) => {
                  // start tracking for response
                  const successSubscription = this.meSweetAlertService.getDataBackFromSweetAlert().subscribe(() => {
                    form.markAsUntouched();
                    this.applyCheck = false;
                    this.router.navigate([`materials/view/${material.oid}`]);
                    successSubscription.unsubscribe();
                  });

                  const successAlertModel: MeSweetAlertI = {
                    mode: 'success',
                    icon: 'check',
                    type: {
                      name: MeSweetAlertTypeEnum.confirm,
                      buttons: {
                        confirm: this.i18nStrings.continue
                      }
                    },
                    title: this.i18nStrings.success,
                    message: this.i18nStrings.materialDuplicated
                  };
                  this.meSweetAlertService.openMeSweetAlert(successAlertModel);
                });
            },
            () => {
              this.stepper.selectedIndex = 0;
              this.showAttributesNotUniqueError();
              this.isInProgress = false;
            }
          );
      }
    }
  }

  private attachMaterialTypeControl(domain: DomainModel, disabled: boolean = false): void {
    // create attribute and control for material types
    this.materialTypesAttribute.dataTypeClass = MeAttributeTypes.CHOICE;
    this.materialTypesAttribute.displayName = this.i18nStrings.materialType;
    this.materialTypesAttribute.internalName = 'materialType';
    this.materialTypesAttribute.required = true;
    this.materialTypesAttribute.disabled = disabled;
    if (disabled && this.domain) {
      this.materialTypesAttribute.value = this.domain.internalName;
    }
    if (domain) {
      this.materialTypesAttribute.values = domain.descendants.map((materialDomain) => {
        const enumValue = new EnumValueModel();
        enumValue.displayName = materialDomain.displayName;
        enumValue.value = materialDomain.internalName;
        return enumValue;
      });
    }

    this.materialInfoAttributeForm.addControl(
      this.materialTypesAttribute.internalName,
      new FormControl(this.materialTypesAttribute.value, Validators.required)
    );

    const materialTypesInputAttribute: MeSelectInputAttribute = new MeSelectInputAttribute();
    materialTypesInputAttribute.data = this.materialTypesAttribute;
    materialTypesInputAttribute.form = this.materialInfoAttributeForm;

    this.materialTypeAttributeItem = new MeAttributeItem(MeSelectInputComponent, materialTypesInputAttribute);

    // track changes of material type
    const materialTypeControl: AbstractControl | null = this.materialInfoAttributeForm.get(this.materialTypesAttribute.internalName);
    if (materialTypeControl && !disabled) {
      this._materialTypeValueChangesSubscription = materialTypeControl.valueChanges.subscribe((value) => {
        if (value) {
          this.selectedMaterialType = value;
          this.updateMaterialProperties(value);
        }
      });
    }
  }

  private setAttributeWithValueToEntity(
    entity: MaterialEntityI,
    attribute: AttributeModelTypes | LayoutGroupAttributeModel,
    formGroup: FormGroup
  ): void {
    if (
      formGroup.controls.hasOwnProperty(attribute.internalName) &&
      formGroup.controls[attribute.internalName].hasOwnProperty('value') &&
      formGroup.controls[attribute.internalName].value !== null
    ) {
      entity.attributes[attribute.internalName] = formGroup.controls[attribute.internalName].value;
    }
  }

  updateThumbnail(thumbnailOR: string | null): void {
    if (this.materialInfoAttributeForm) {
      this.materialInfoAttributeForm.markAsDirty();
      this.materialInfoAttributeForm.controls[this.thumbnailAttributeName].setValue(thumbnailOR);
    }
  }

  handleImageFormData(recievedFile: File): void {
    const formData = new FormData();
    formData.append(
      'meta',
      JSON.stringify({
        type: this.definitionsStoreService.thumbnailProperties?.type
      })
    );
    formData.append('file', recievedFile, recievedFile.name);

    this._getUploadedImageReferenceIDSubscription = this.fileWebService.uploadFile(formData, FileUploadResponseModel).subscribe(
      (response) => {
        this.uploadImageData = {
          thumbnail: `${MARKETPLACE_BASE_API_URL}/files/download/${response.oid}`,
          showProgress: false
        };
        if (response.oid) {
          this.updateThumbnail(response.oid);
        } else {
          this.updateThumbnail(null);
        }
      },
      (_error: ErrorResponseI) => {
        this.uploadImageData = { ...this.uploadImageData, isThumbnailStatusInvalid: true };
      }
    );
  }

  handleAddButtonClick(): void {
    this.documentPopupData = { selectedCount: 0, title: this.i18nStrings.addDocuments, showActions: true };
    this.doocumentsEmptyContent = {
      title: this.i18nStrings.addYourFirstDocument,
      comment: this.i18nStrings.addYourFirstDocumentDescription,
      buttonText: this.i18nStrings.add,
      emptyIconName: 'empty-documents'
    };
    this.documentSelectionStore = new DocumentStoreService();
    this.prepareFilterData();
    this.prepareDocumentData();
    this.handleDocumentsSearch(this.documentsSearchKeyword);
    // Put in comment this line of code to stop producing popup
    this.popupSelectService.openPopupSelect(this.documentPopupData, this.popupDocumentGridRef).subscribe((event: string) => {
      switch (event) {
        case 'close':
          this.resetPopupAfterClose();
          break;
        case 'clearSelection':
          this.documentSelectionStore.documentEntities = [];
          this.handleDocumentsSearch(this.documentsSearchKeyword || '');
          this.setIsShowSelectedActive(false);
          break;
        case 'showSelected':
          this._documentStoreService.documentEntities = this.documentSelectionStore.documentEntities;
          this.documentsTrackForBottomReach = false;
          this.setIsShowSelectedActive(true);
          break;
        case 'showAll':
          this.handleDocumentsSearch(this.documentsSearchKeyword || '');
          this.setIsShowSelectedActive(false);
          break;
        case 'addItems':
          const documentOids: string[] = this.documentSelectionStore.documentEntities.map((document) => {
            return document.oid;
          });

          if (documentOids.length) {
            this.submitDocuments(this.material.oid, documentOids);
            this.buttonFinish = { ...this.buttonFinish, data: { ...this.buttonFinish.data, disabled: false } };
          }
          this.resetPopupAfterClose();
          break;
        default:
          break;
      }
    });
    // Listen for any sort/filter criteria
    this._listenForFilterChangeSubscription = this.documentFilterChanged.pipe(debounceTime(this.filterDelay)).subscribe(() => {
      this.handleDocumentsSearch(this.documentsSearchKeyword || '');
    });
  }

  private setIsShowSelectedActive(status: boolean): void {
    this.isShowSelectedActive = status;
    this.updateMaterialPopup({
      showOnlySelected: status
    });
  }

  private resetPopupAfterClose(): void {
    this.documentSelectionStore.documentEntities = [];
    if (this._listenForFilterChangeSubscription) {
      this._listenForFilterChangeSubscription.unsubscribe();
    }
    if (this._listenForDocumentTypeChangeSubscription) {
      this._listenForDocumentTypeChangeSubscription.unsubscribe();
    }
  }

  goToCreateDocument(): void {
    this.popupSelectService.closePopup();
    this.router.navigate(['documents', 'create']);
  }

  updateMaterialPopup(materialPopup: Partial<MePopupSelectI>): void {
    this.documentPopupData = { ...this.documentPopupData, ...materialPopup };
    this.popupSelectService.pushDataModelToPopup(this.documentPopupData);
  }

  getDocumentOid(_index: number, document: MeDocumentCardI): string {
    return document.oid;
  }

  documentsBottomReachedHandler(): void {
    if (this.documentsTrackForBottomReach) {
      this.handleDocumentsSearch(
        this.documentsSearchKeyword || '',
        this._documentStoreService.documentNextId,
        this.NUMBER_OF_ITEMS_ON_PAGE,
        true
      );
    }
  }

  dcumentCardCustomEvent(eventName: string, payload?: string, item?: MeDocumentCardI | MeDocumentModel): void {
    if (eventName === 'contextMenuItemClick') {
      switch (payload) {
        case 'edit':
          this.router.navigate(['documents', 'edit', item?.oid]);
          break;
        case 'download':
          const file = this._documentStoreService.documentEntities.find((d) => d.oid === item?.oid);
          if (file && file.attributes.primaryContent) {
            window.open(`${MARKETPLACE_BASE_API_URL}/files/download/${file.attributes.primaryContent}`, '_blank');
          } else {
            this.meBasicAlertService.openBasicAlert({
              mode: 'error',
              content: this.i18nStrings.noFile,
              title: this.i18nStrings.emptyDocument
            });
          }
          break;
        default:
          break;
      }
    }
  }

  handleDocumentsSearch(searchValue: string, from: number = 0, to: number = this.NUMBER_OF_ITEMS_ON_PAGE, append: boolean = false): void {
    this.documentsSearchKeyword = searchValue;
    this.documentsSearchBar = {
      ...this.documentsSearchBar,
      search: { placeholder: this.i18nStrings.searchFor, keyword: this.documentsSearchKeyword as string }
    };

    const searchCriteria: QuickSearchModel = {
      domain: this.selectedDocumentType,
      criteriaQuick: this.documentsSearchKeyword.trim() || undefined,
      orderByAttr: this.sortByValue.trim() || undefined,
      orderByDir: this.sortByDirection.trim() || undefined
    };

    this.areDocumentsLoading = true;
    this._getDocumentsSubscription = this.webService
      .searchDocuments(searchCriteria, from, to)
      .pipe(
        finalize(() => {
          this.areDocumentsLoading = false;
        })
      )
      .subscribe((response: ArrayResponseI<MeDocumentModel>) => {
        this._documentStoreService.documentNextId = response.nextID;
        if (append) {
          this._documentStoreService.appendDocumentEntities(response.entities);
        } else {
          this._documentStoreService.documentEntities = response.entities;
          this.documentsIsSearch = true;
          this.documentsTrackForBottomReach = true;
          this.updateMaterialPopup({
            selectedCount: this.documentSelectionStore.documentEntities.length
          });
        }
        // If reached end of pagination unsubscribe from scroll
        if (response.totalCount < this.NUMBER_OF_ITEMS_ON_PAGE) {
          this.documentsTrackForBottomReach = false;
        }
      });
  }

  prepareDocumentData(): void {
    this._getDocumentDataForDocumentCardSubscription = this._documentStoreService.documentEntities$.subscribe((data: MeDocumentModel[]) => {
      if (data) {
        this.documentCardItems = data.map((document: MeDocumentModel) => {
          return {
            contextMenu: {
              contextMenuItems: [
                {
                  key: 'edit',
                  value: this.i18nStrings.edit
                },
                {
                  key: 'download',
                  value: this.i18nStrings.download
                }
              ]
            },
            creationDate: getFormatedDate(document.createdOn),
            documentName: document.attributes.title,
            oid: document.oid,
            selected: this.documentSelectionStore.documentEntities.findIndex((doc) => doc.oid === document.oid) !== -1,
            selectionEnabled: true,
            documentThumbnail: document.attributes.thumbnail
              ? `${MARKETPLACE_BASE_API_URL}/files/download/${document.attributes.thumbnail}`
              : undefined,
            documentExtensionIcon: mapExtensionToIcon(document.attributesExtended.extension)
          };
        });
      }
    });
  }

  getAddedMaterialDocuments(from: number = this._addedFrom, to: number = this._addedTo, append: boolean = false): void {
    this.globalService.activateLoader();
    this._getAddedMaterialDocumentsSubscription = this.webService
      .getMaterialDocuments(this.material.oid, from, to)
      .pipe(
        finalize(() => {
          this.globalService.deactivateLoader();
        })
      )
      .subscribe((response: ArrayResponseI<MeDocumentModel>) => {
        this._documentStoreService.addedDocumentNextId = response.nextID;
        if (append) {
          this._documentStoreService.appendAddedDocumentEntities(response.entities);
        } else {
          this._documentStoreService.addedDocumentEntities = response.entities;
        }
      });
  }

  prepareAddedDocumentData(): void {
    this._getAddedDocumentDataForDocumentCardSubscription = this._documentStoreService.addedDocumentEntities$.subscribe(
      (data: MeDocumentModel[]) => {
        if (data) {
          this.addedDocumentCardItems = data.map((document: MeDocumentModel) => {
            return {
              contextMenu: {
                contextMenuItems: [{ key: 'remove', value: this.i18nStrings.remove }]
              },
              creationDate: getFormatedDate(document.createdOn),
              documentName: document.attributes.title,
              oid: document.oid,
              selected: false,
              selectionEnabled: false,
              documentThumbnail: document.attributes.thumbnail
                ? `${MARKETPLACE_BASE_API_URL}/files/download/${document.attributes.thumbnail}`
                : undefined,
              documentExtensionIcon: mapExtensionToIcon(document.attributesExtended.extension)
            };
          });
        }
      }
    );
  }

  toggleCardItemSelection(documentOid: string, index: number): void {
    const docObject = this._documentStoreService.documentEntities.find((doc) => doc.oid === documentOid);
    if (docObject) {
      if (this.documentSelectionStore.documentEntities.includes(docObject)) {
        this.documentCardItems[index] = { ...this.documentCardItems[index], selected: false };
        this.documentSelectionStore.documentEntities = this.documentSelectionStore.documentEntities.filter(
          (doc) => doc.oid !== documentOid
        );
      } else {
        this.documentCardItems[index] = { ...this.documentCardItems[index], selected: true };
        this.documentSelectionStore.documentEntities = [...this.documentSelectionStore.documentEntities, docObject];
      }
    }
    this.updateMaterialPopup({ selectedCount: this.documentSelectionStore.documentEntities.length });
  }

  documentClickHandler(oid: string): void {
    this.documentCardItems = this.documentCardItems.map((document) => {
      return document.oid === oid ? { ...document, selected: !document.selected } : document;
    });
    let selectCount: number = 0;
    selectCount = this.documentCardItems.filter((document) => {
      return document.selected;
    }).length;
    this.popupSelectService.pushDataModelToPopup({ ...this.documentPopupData, selectedCount: selectCount });
  }

  documentsHandleSortByEvent(sortByValue: string): void {
    if (this.documentsSearchBar.sort) {
      this.documentsSearchBar = {
        ...this.documentsSearchBar,
        sort: {
          ...this.documentsSearchBar.sort,
          selectedIndex: this.documentsSearchBar.sort.options.findIndex((x) => x.key === sortByValue) || 0
        }
      };
      switch (sortByValue) {
        case 'azAlphabetically':
          this.sortByValue = MeSearchSortByTypes.title;
          this.sortByDirection = MeSearchDirectionTypes.ascending;
          break;
        case 'zaAlphabetically':
          this.sortByValue = MeSearchSortByTypes.title;
          this.sortByDirection = MeSearchDirectionTypes.descending;
          break;
        case 'newest':
          this.sortByValue = MeSearchSortByTypes.created;
          this.sortByDirection = MeSearchDirectionTypes.descending;
          break;
        case 'oldest':
          this.sortByValue = MeSearchSortByTypes.created;
          this.sortByDirection = MeSearchDirectionTypes.ascending;
          break;
      }
      this.documentFilterChanged.next();
    }
  }

  submitDocuments(materialOid: string, documentOids: string[]): void {
    this._submitDocumentsSubscription = this.webService.submitDocuments(materialOid, documentOids).subscribe(() => {
      const successAlertModel: MeSweetAlertI = {
        mode: 'success',
        icon: 'check',
        type: {
          name: MeSweetAlertTypeEnum.confirm,
          buttons: {
            confirm: this.i18nStrings.close
          }
        },
        title: this.i18nStrings.success,
        message: this.i18nStrings.addDocumentsSuccess
      };
      this.meSweetAlertService.openMeSweetAlert(successAlertModel);
      this.getAddedMaterialDocuments();
    });
  }

  setSearchBarDataMaterialDocuments(): void {
    this.searchBarDataMaterialDocuments = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: ''
      },
      sort: {
        placeholder: this.i18nStrings.sortBy,
        options: [
          { key: 'azAlphabetically', value: this.i18nStrings.azAlphabetically },
          { key: 'zaAlphabetically', value: this.i18nStrings.zaAlphabetically },
          { key: 'newest', value: this.i18nStrings.newest },
          { key: 'oldest', value: this.i18nStrings.oldest }
        ],
        selectedIndex: 0
      }
    };
  }

  handleSearchInputEventMaterialDocuments(searchValue: string): void {
    this.searchBarDataMaterialDocuments = {
      ...this.searchBarDataMaterialDocuments,
      search: { placeholder: this.i18nStrings.searchFor, keyword: searchValue }
    };
    let orderByAttr: string = '';
    let orderByDir: string = '';
    switch (this.searchBarDataMaterialDocuments.sort?.options[this.searchBarDataMaterialDocuments.sort.selectedIndex].key) {
      case 'azAlphabetically':
        orderByAttr = MeSearchSortByTypes.title;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
      case 'zaAlphabetically':
        orderByAttr = MeSearchSortByTypes.title;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'newest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.descending;
        break;
      case 'oldest':
        orderByAttr = MeSearchSortByTypes.created;
        orderByDir = MeSearchDirectionTypes.ascending;
        break;
    }
    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_DOCUMENT,
      criteriaQuick: searchValue,
      orderByAttr,
      orderByDir
    };
    this.globalService.activateLoader();
    this.subs.sink = this.webService
      .searchMaterialDocuments(this.material.oid, searchFilter)
      .pipe(
        finalize(() => {
          this.globalService.deactivateLoader();
        })
      )
      .subscribe(
        (response: ArrayResponseI<MeDocumentModel>) => {
          if (response && response.entities) {
            this._documentStoreService.addedDocumentEntities = response.entities;
            this.documentsIsSearchTab = true;
          }
        },
        (error: ErrorResponseI | HttpErrorResponse) => {
          console.error(error);
        }
      );
  }

  handleSortByEventMaterialDocuments(sortByValue: string): void {
    if (this.searchBarDataMaterialDocuments.sort) {
      this.searchBarDataMaterialDocuments.sort.selectedIndex = this.searchBarDataMaterialDocuments.sort.options.findIndex(
        (x) => x.key === sortByValue
      );
    }
    this.handleSearchInputEventMaterialDocuments(
      this.searchBarDataMaterialDocuments.search ? this.searchBarDataMaterialDocuments.search.keyword : ''
    );
  }

  addButtonToBreadcrumb(): void {
    setTimeout(() => {
      if (this.stepper.selected === this.materialsStep) {
        this.breadcrumbSupportService.breadcrumbActions = [
          ...this.breadcrumbActions,
          {
            button: {
              id: '1',
              data: { color: 'primary', type: 'transparent' },
              text: this.i18nStrings.add,
              content: this.addButton
            },
            callBack: () => {
              this.handleAddButtonClick();
            }
          }
        ];
      } else {
        this.breadcrumbSupportService.breadcrumbActions = [...this.breadcrumbActions];
      }
    });
  }

  removeDocument(event: { eventName: string; payload?: string }, documentOid: string): void {
    if (event.eventName === 'contextMenuItemClick' && event.payload === 'remove') {
      const alertAnswerSubscription = this.meSweetAlertService.getDataBackFromSweetAlert().subscribe(({ confirmed }) => {
        if (confirmed) {
          this.handleDocumentsRemoval(this.material.oid, [documentOid]);
        }
        alertAnswerSubscription.unsubscribe();
      });

      const questionAlertModel: MeSweetAlertI = {
        mode: 'warning',
        icon: 'help-circle',
        type: {
          name: MeSweetAlertTypeEnum.submit,
          buttons: {
            submit: this.i18nStrings.yes,
            cancel: this.i18nStrings.no
          }
        },
        title: this.i18nStrings.removeDocuments,
        message: this.i18nStrings.removeDocumentsDescription
      };
      this.meSweetAlertService.openMeSweetAlert(questionAlertModel);
    }
  }

  handleDocumentsRemoval(materialOid: string, documentOids: string[]): void {
    this._removeDocumentSubscription = this.webService.removeDocuments(materialOid, documentOids).subscribe(() => {
      const successAlertModel: MeSweetAlertI = {
        mode: 'success',
        icon: 'check',
        type: {
          name: MeSweetAlertTypeEnum.confirm,
          buttons: {
            confirm: this.i18nStrings.close
          }
        },
        title: this.i18nStrings.success,
        message: this.i18nStrings.removeDocumentsSuccess
      };
      this.meSweetAlertService.openMeSweetAlert(successAlertModel);
      this.getAddedMaterialDocuments();
    });
  }

  openMaterial(): void {
    this.router.navigate([`materials/view/${this.material.oid}`]);
  }

  showAttributesNotUniqueError(): void {
    const sweetAlertModel: MeSweetAlertI = {
      mode: 'danger',
      icon: 'x-octagon',
      type: {
        name: MeSweetAlertTypeEnum.confirm,
        buttons: {
          confirm: this.i18nStrings.ok
        }
      },
      title: this.i18nStrings.error,
      message: this.i18nStrings.attributesNotUnique
    };

    this.meSweetAlertService.openMeSweetAlert(sweetAlertModel);
  }

  /**
   * Requirement from business:
   * When attribute ask for quote is checked, price and price rmb fields should be disabled, also if there was no price set, set it to 0.
   * When price is 0, disable price rmb field.
   */
  private _pricingAttributesCustomChecker(): void {
    const attrAskForQuote: AbstractControl | null = this.materialInfoAttributeForm.get(MATERIAL_ATTRIBUTE_ASK_FOR_QUOTE);
    const attrPrice: AbstractControl | null = this.materialInfoAttributeForm.get(MATERIAL_ATTRIBUTE_PRICE);
    const attrPriceRMB: AbstractControl | null = this.materialInfoAttributeForm.get(MATERIAL_ATTRIBUTE_PRICE_RMB);

    if (attrAskForQuote && attrPrice && attrPriceRMB) {
      this.subs.sink = attrAskForQuote.valueChanges.subscribe((val: boolean | null) => {
        if (val !== null) {
          if (val) {
            attrPrice.disable();
            if (attrPrice.value === null) {
              attrPrice.setValue(0);
            }
            attrPriceRMB.disable();
          } else {
            attrPrice.enable();
            if (attrPrice.value !== null && attrPrice.value > 0) {
              attrPriceRMB.enable();
            }
          }
        }
      });

      this.subs.sink = attrPrice.valueChanges.subscribe((val: number | null) => {
        if (val === null || val === 0) {
          attrPriceRMB.disable();
        } else {
          attrPriceRMB.enable();
        }
      });
    }
  }

  ngOnDestroy(): object {
    if (this._getSweetAlertSubscription) {
      this._getSweetAlertSubscription.unsubscribe();
    }
    if (this._getUploadedImageReferenceIDSubscription) {
      this._getUploadedImageReferenceIDSubscription.unsubscribe();
    }
    if (this._getI18nStringsSubscription) {
      this._getI18nStringsSubscription.unsubscribe();
    }
    if (this._createMaterialSubscription) {
      this._createMaterialSubscription.unsubscribe();
    }
    if (this._updateMaterialSubscription) {
      this._updateMaterialSubscription.unsubscribe();
    }
    if (this._getMaterialSubscription) {
      this._getMaterialSubscription.unsubscribe();
    }
    if (this._definitionStoreDataLoadedSubscription) {
      this._definitionStoreDataLoadedSubscription.unsubscribe();
    }
    if (this._materialTypeValueChangesSubscription) {
      this._materialTypeValueChangesSubscription.unsubscribe();
    }
    if (this._submitDocumentsSubscription) {
      this._submitDocumentsSubscription.unsubscribe();
    }
    if (this._removeDocumentSubscription) {
      this._removeDocumentSubscription.unsubscribe();
    }
    if (this._getDocumentDataForDocumentCardSubscription) {
      this._getDocumentDataForDocumentCardSubscription.unsubscribe();
    }
    if (this._getAddedMaterialDocumentsSubscription) {
      this._getAddedMaterialDocumentsSubscription.unsubscribe();
    }
    if (this._getAddedDocumentDataForDocumentCardSubscription) {
      this._getAddedDocumentDataForDocumentCardSubscription.unsubscribe();
    }
    if (this._listenForFormChangeSubscription) {
      this._listenForFormChangeSubscription.unsubscribe();
    }
    if (this._listenForSteperLoadSubscription) {
      this._listenForSteperLoadSubscription.unsubscribe();
    }
    if (this._listenForDocumentTypeChangeSubscription) {
      this._listenForDocumentTypeChangeSubscription.unsubscribe();
    }
    if (this._getDocumentsSubscription) {
      this._getDocumentsSubscription.unsubscribe();
    }
    if (this._listenForFilterChangeSubscription) {
      this._listenForFilterChangeSubscription.unsubscribe();
    }
    this.subs.unsubscribe();
    if (this._checkUniqueAttributesSubscription) {
      this._checkUniqueAttributesSubscription.unsubscribe();
    }
    return super.ngOnDestroy();
  }
}
