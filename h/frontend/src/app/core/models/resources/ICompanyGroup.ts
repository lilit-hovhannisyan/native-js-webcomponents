import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';
import { IWithLabels } from './IWithLabels';
import { IWithActive } from './IWithActive';

export type CompanyGroupKey = number;

interface ISharedProps extends IWithLabels, IWithActive { }

export interface ICompanyGroupBase extends ISharedProps, IResource<CompanyGroupKey> { }

export interface ICompanyGroupCreate extends ISharedProps { }

export interface ICompanyGroupDTO extends ICompanyGroupBase { }

export interface ICompanyGroup extends ICompanyGroupDTO, IWithLabel { }
