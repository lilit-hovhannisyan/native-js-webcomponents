import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';

interface ISharedProps extends IWithLabels { }

export interface IContactRoleCreate extends ISharedProps { }

export interface IContactRoleBase extends ISharedProps, IResource<number> { }

export interface IContactRoleDTO extends IContactRoleBase { }

export interface IContactRole extends IContactRoleDTO, IWithLabel { }




