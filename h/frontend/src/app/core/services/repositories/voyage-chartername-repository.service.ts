import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RepoParams } from '../../abstracts/abstract.repository';
import { HttpDelete } from '../../models/http/http-delete';
import { HttpGet } from '../../models/http/http-get';
import { HttpPost } from '../../models/http/http-post';
import { HttpPut } from '../../models/http/http-put';
import { IResourceCollection } from '../../models/resources/IResourceCollection';
import { VesselKey } from '../../models/resources/IVessel';
import { IVesselPreDelayCharterName } from '../../models/resources/IVesselPreDelayCharterName';
import { VoyageKey } from '../../models/resources/IVoyage';
import { IVoyageCharterName, IVoyageCharterNameResource } from '../../models/resources/IVoyageCharterName';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';
import { RepositoryService } from '../repository.service';

export interface IVoyageRepoParams extends RepoParams<IVoyageCharterName> {
  queryParams?: {
    vesselId?: VesselKey;
  };
}

declare type IPreDeliveriesResource = HttpResponse<IDataResponseBody<IResourceCollection<IVesselPreDelayCharterName>>>;

@Injectable({
  providedIn: 'root'
})
export class VoyageCharternameRepositoryService {
  protected resourceType = 'VoyageCharterName';
  private readonly baseUrl = '/api/chartername';

  constructor(
    private repositoryService: RepositoryService,
  ) { }

  public create(entities: IVoyageCharterName[]): Observable<IVoyageCharterName[]> {
    const url = `${this.baseUrl}/voyages`;
    const request = new HttpPost(url, entities);

    return this.repositoryService.httpClient.request(request).pipe(
      map((result: IVoyageCharterNameResource) => result.body.data.items),
    );
  }
  public update(voyageId: VoyageKey, entities: IVoyageCharterName[]) {
    const url = `${this.baseUrl}/voyages/${voyageId}`;
    const request = new HttpPut(url, entities);

    return this.repositoryService.httpClient.request(request).pipe(
      map((result: IVoyageCharterNameResource) => result.body.data.items),
    );
  }

  public fetchAll(options: IVoyageRepoParams): Observable<IVoyageCharterName[]> {
    const request = new HttpGet(this.baseUrl, { init: { params: options.queryParams } });

    return this.repositoryService.httpClient.request(request).pipe(
      map((result: IVoyageCharterNameResource) => result.body.data.items),
    );
  }

  public fetchAllPreDeliveryCharternames(): Observable<IVesselPreDelayCharterName[]> {
    const url = `${this.baseUrl}/vessels`;

    return this.repositoryService.httpClient.request(new HttpGet(url)).pipe(
      map((response: IPreDeliveriesResource) => response.body.data.items)
    );
  }

  public deleteAllByVoyageId(voyageId: VoyageKey) {
    const url = `${this.baseUrl}/voyages/${voyageId}`;
    const request = new HttpDelete(url);
    return this.repositoryService.httpClient.request(request);
  }
}
