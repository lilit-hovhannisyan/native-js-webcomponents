import { IRole } from 'src/app/core/models/resources/IRole';
import { RoleRepositoryService } from './../../../../../../core/services/repositories/role-repository.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { ActivatedRoute, Router } from '@angular/router';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RolesHelperService } from './roles.helper.service';

@Component({
  templateUrl: './roles-detail-view.component.html',
  styleUrls: ['./roles-detail-view.component.scss'],
  providers: [RolesHelperService],
})
export class RolesDetailViewComponent implements OnInit, OnDestroy {
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public title: string;
  public role?: IRole;
  public sitemap = sitemap;

  public get isCreationMode() {
    return this.route.snapshot.params.id === 'new';
  }

  constructor(
    private route: ActivatedRoute,
    private roleRepo: RoleRepositoryService,
    private rolesHelperService: RolesHelperService,
    private router: Router
  ) { }

  public ngOnInit() {
    const idParam = this.route.snapshot.params.id;

    if (this.isCreationMode) {
      this.rolesHelperService.generateTitle();
    } else {
      const id = parseInt(idParam, 10);

      this.roleRepo.fetchOne(id, { useCache: true })
        .subscribe(role => {
          this.rolesHelperService.generateTitle(role);
        });
    }

    this.rolesHelperService.editViewTitle$.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(title => {
      this.title = title;
    });
  }

  public routeBack = () => {
    this.router.navigate([
      url(sitemap.system.children.misc.children.roles)
    ]);
  }

  public tabDisabler = (node: SitemapNode) => {
    const basicNode = sitemap.system.children.misc.children.roles.children.id.children.basic;
    // in isCreationMode just enable the basicTab
    return this.isCreationMode ? node === basicNode : true;
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
