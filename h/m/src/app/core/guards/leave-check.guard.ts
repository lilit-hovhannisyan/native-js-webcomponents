import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, NavigationEnd } from '@angular/router';

import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DeactivatableComponent } from '@core-interfaces/deactivatable-component.interface';

import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { MeSweetAlertI } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';

@Injectable({ providedIn: 'root' })
export class LeaveCheckGuard implements CanDeactivate<DeactivatableComponent> {
  private $destroy: Subject<void> = new Subject();
  private redirectTo: string = '';
  private selfCall: boolean = false;

  constructor(private router: Router, private sweetAlertService: MeSweetAlertService) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.$destroy.next();
      }
    });
  }

  canDeactivate(
    component: DeactivatableComponent,
    _currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!component.applyCheck) {
      return true;
    } else if (this.selfCall) {
      this.selfCall = false;
      return true;
    } else {
      this.redirectTo = decodeURI(nextState ? nextState.url : currentState.url);

      this.sweetAlertService
        .getDataBackFromSweetAlert()
        .pipe(takeUntil(this.$destroy))
        .subscribe((data: MeSweetAlertI) => {
          if (data.confirmed) {
            this.selfCall = true;
            this.router.navigate([this.redirectTo]);
          } else {
            this.$destroy.next();
          }
        });
      this.sweetAlertService.openMeSweetAlert(component.leaveWarningModel);

      return false;
    }
  }
}
