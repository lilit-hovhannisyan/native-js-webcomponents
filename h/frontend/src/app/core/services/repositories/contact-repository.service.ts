import { ContactKey } from './../../models/resources/IContact';
import { Injectable } from '@angular/core';
import { IContact, IContactDTO, IContactCreate } from '../../models/resources/IContact';
import { RepositoryService } from '../repository.service';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';

@Injectable({
  providedIn: 'root'
})
export class ContactRepositoryService extends AbstractRepository<
  IContact,
  IContactDTO,
  IContactCreate,
  RepoParams<IContact>,
  ContactKey
> {

  protected resourceType = 'Contact';
  public sortAsce = true;
  public sortKey = 'contactPerson';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }


  protected postProcess(item: IContactDTO): IContact { return item; }

  public getBaseUrl(params: RepoParams<IContact>): string {
    return `/api/contacts`;
  }
}

