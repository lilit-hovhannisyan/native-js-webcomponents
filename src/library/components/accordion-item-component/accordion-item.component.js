import {BaseComponent} from '../../core/base-component/base.component.js';
import {
  externalStyleBasePath,
  createElementWithAttributes,
  canElementBeRendered,
  removeStaticAtributesFromElNotToRender
} from '../../helpers/random.helpers.js';

export class AccordionItemComponent extends BaseComponent {
  isOpen;
  constructor() {
    super(externalStyleBasePath + 'accordion-item-component/accordion-item.component.css');
  }

  render = () => {
    const accordionItemConfig = {
      tagName: 'div',
      attr: {
        class: 'accordion-container'
      },
      subElements: [
        {
          tagName: 'button-component',
          attr: {
            class: 'accordion-header',
            text: this.getAttribute('header'),
            icon: this.getAttribute('icon'),
            style: 'fill'
          },
          events: {
            click: this.toggleAccordion
          }
        },
        {
          tagName: 'text-component',
          attr: {
            class: 'accordion-body',
            selector: 'div',
            text: this.getAttribute('body'),
          }
        }
      ]
    }

    const accordionItemEl = createElementWithAttributes(accordionItemConfig);
    this.shadow.appendChild(accordionItemEl);

    this.isOpen = this.getAttribute('open') === 'true';
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }

  static get observedAttributes() {
    return ['open'];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    this.updateOpenState();
  }

  toggleAccordion = () => {
    this.setAttribute('open', !this.isOpen);
  }

  updateOpenState() {
    this.isOpen = !this.isOpen;
  }
}