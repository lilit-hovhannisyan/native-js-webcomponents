import { BookingAccountKey } from './../../models/resources/IBookingAccount';
import { Injectable } from '@angular/core';
import { RepositoryService } from '../repository.service';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IBookingAccount, IBookingAccountDTO, IBookingAccountCreate } from '../../models/resources/IBookingAccount';

export interface BookingAccountParams extends RepoParams<IBookingAccount> {
  mandatorId?: number;
}
@Injectable({
  providedIn: 'root'
})
export class BookingAccountRepositoryService extends AbstractRepository<
  IBookingAccount,
  IBookingAccountDTO,
  IBookingAccountCreate,
  BookingAccountParams,
  BookingAccountKey
>  {
  protected resourceType = 'BookingAccount';
  public sortAsce = true;
  public sortKey = 'no';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess(item: IBookingAccountDTO): IBookingAccount { return item; }

  public getBaseUrl(params: BookingAccountParams): string {
    return params.mandatorId
      ? `/api/mandators/${params.mandatorId}/booking-accounts`
      : '/api/booking-accounts';
  }
}
