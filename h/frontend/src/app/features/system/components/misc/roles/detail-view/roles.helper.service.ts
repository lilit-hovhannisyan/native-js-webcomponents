
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { wording } from 'src/app/core/constants/wording/wording';
import { SettingsService } from 'src/app/core/services/settings.service';
import { IRole } from 'src/app/core/models/resources/IRole';

@Injectable()
export class RolesHelperService {
  public editViewTitleSubject: BehaviorSubject<string> = new BehaviorSubject('');
  public editViewTitle$ = this.editViewTitleSubject.asObservable();
  constructor(
    public settingsService: SettingsService,
  ) { }

  public generateTitle = (role?: IRole) => {
    let title = wording.general.create[this.settingsService.language];

    if (role) {
      title = role.displayLabel[this.settingsService.language];
    }

    this.editViewTitleSubject.next(title);
  }
}
