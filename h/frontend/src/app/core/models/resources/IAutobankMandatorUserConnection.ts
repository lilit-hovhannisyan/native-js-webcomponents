import { IResource } from './IResource';
import { AutobankPermissionKey } from './IAutobankPermission';
import { IPermissionGroupUserMandatorConnection } from './IPermissionGroup';

export type AutobankMandatorUserConnectionKey = number;

interface ISharedProps extends IPermissionGroupUserMandatorConnection {
  autobankPermissionId: AutobankPermissionKey;
}

export interface IAutobankMandatorUserConnectionBase extends ISharedProps, IResource<AutobankMandatorUserConnectionKey> { }

export interface IAutobankMandatorUserConnectionCreate extends ISharedProps { }

export interface IAutobankMandatorUserConnectionDTO extends IAutobankMandatorUserConnectionBase { }

export interface IAutobankMandatorUserConnection extends IAutobankMandatorUserConnectionDTO { }

