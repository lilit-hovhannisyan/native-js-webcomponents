import { MandatorKey } from './../../models/resources/IMandator';
import { Injectable } from '@angular/core';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { MandatorBookingCodeRepositoryService } from '../repositories/mandator-booking-code-repository.service';
import { IMandatorBookingCode, IMandatorBookingCodeFull } from '../../models/resources/IMandatorBookingCode';
import { IBookingCode } from '../../models/resources/IBookingCode';
import { BookingCodeRepositoryService } from '../repositories/booking-code-repository.service';
import { sortBy } from 'lodash';

export interface IMandatorBookingCodeRow extends IMandatorBookingCodeFull {
  label: string;
}

@Injectable({
  providedIn: 'root'
})
export class MandatorBookingCodeMainService {
  constructor(
    private mandatorBookingCodeRepo: MandatorBookingCodeRepositoryService,
    private bookingCodeRepo: BookingCodeRepositoryService
  ) { }

  public fetchAll(params: { mandatorId: MandatorKey }): Observable<IMandatorBookingCodeFull[]> {
    return forkJoin([
      this.mandatorBookingCodeRepo.fetchAll({ queryParams: params }),
      this.bookingCodeRepo.fetchAll(params),
    ]).pipe(map(entityCollections => this.getMandatorBookingCodeFullList(...entityCollections)));
  }

  public fetchOne(id: number, params: { mandatorId?: MandatorKey }): Observable<IMandatorBookingCodeFull> {
    return this.composeMandatorBookingCode(
      this.mandatorBookingCodeRepo.fetchOne(id, {}),
      this.bookingCodeRepo.fetchAll(params),
    );
  }

  private getMandatorBookingCodeFullList(
    mandatorBookingCodes: IMandatorBookingCode[],
    bookingCodes: IBookingCode[]
  ): IMandatorBookingCodeFull[] {
    return mandatorBookingCodes.map(mandator => {
      return this.getMandatorBookingCodeFull(mandator, bookingCodes);
    });
  }

  private getMandatorBookingCodeFull(
    mandatorBookingCode: IMandatorBookingCode,
    bookingCodes: IBookingCode[]
  ): IMandatorBookingCodeFull {
    const bookingCode = bookingCodes.find(el => el.id === mandatorBookingCode.bookingCodeId);
    return { ...bookingCode, ...mandatorBookingCode };
  }

  private composeMandatorBookingCode(
    mandatorBookingCode$: Observable<IMandatorBookingCode>,
    bookingCodes$: Observable<IBookingCode[]>,
  ): Observable<IMandatorBookingCodeFull> {
    return forkJoin([mandatorBookingCode$, bookingCodes$]).pipe(
      map((entityCollections) => this.getMandatorBookingCodeFull(...entityCollections))
    );
  }

  public getStream(params: { mandatorId?: MandatorKey } = {}): Observable<IMandatorBookingCode[]> {
    return forkJoin([
      this.mandatorBookingCodeRepo.fetchAll({ queryParams: params }),
      this.bookingCodeRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.mandatorBookingCodeRepo.getStream()
          .pipe(map(mbCodes => this.getFilteredMandatorBookingCodes(mbCodes, params.mandatorId))),
        this.bookingCodeRepo.getStream()
      ])),
      map(collections => sortBy(this.getMandatorBookingCodeFullList(...collections), ['no'])),
    );
  }

  public getFilteredMandatorBookingCodes(mandatorBookingCodes, mandatorId) {
    if (!mandatorId) { return mandatorBookingCodes; }
    return mandatorBookingCodes.filter(mbCode => mbCode.mandatorId === mandatorId);
  }
}
