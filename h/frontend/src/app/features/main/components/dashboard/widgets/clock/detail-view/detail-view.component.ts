import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { tz } from 'moment-timezone';
import { Validators } from 'src/app/core/classes/validators';
import { wording } from 'src/app/core/constants/wording/wording';
@Component({
  selector: 'nv-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class ClockDetailViewComponent implements OnInit {
  @Input() public closeModal: () => void;
  @Input() public submit: (data?) => any;
  public timezones: string[] = [];
  public form: FormGroup;
  public wording = wording;
  constructor(
    private fb: FormBuilder,
  ) { }

  public ngOnInit() {
    this.timezones = tz.names();
    this.initForm();
  }

  private initForm() {
    this.form = this.fb.group({
      timeZone: [null, Validators.required]
    });
  }
}
