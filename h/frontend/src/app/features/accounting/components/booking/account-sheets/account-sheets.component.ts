import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NvGridConfig, GridComponent, NvExcelExportInfo } from 'nv-grid';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { wording } from 'src/app/core/constants/wording/wording';
import { ReplaySubject, forkJoin, BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { SettingsService } from 'src/app/core/services/settings.service';
import { DEBIT_CREDIT } from 'src/app/core/models/enums/debitCredit';
import { AccountStatementRepositoryService, AccountStatementQueryParams } from 'src/app/core/services/repositories/account-statement-repository.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { getFiscalCalendarDate, mandatorNoModifier, IMandatorLookup, MandatorKey } from 'src/app/core/models/resources/IMandator';
import { gridConfig as mandatorGridConfig } from 'src/app/shared/components/autocomplete/modals/mandator-grid-config';
import { gridConfig as accountGridConfig } from 'src/app/shared/components/autocomplete/modals/account-grid-config';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { IAccountStatement, IAccountStatementFull } from 'src/app/core/models/resources/IAccount-statement';
import { BalanceRepositoryService } from 'src/app/core/services/repositories/balance-repository.service';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { IBalance } from 'src/app/core/models/resources/IBalance';
import { Router, ActivatedRoute } from '@angular/router';
import { ACL_ACTIONS, AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { TaxKeyRepositoryService } from 'src/app/core/services/repositories/tax-key-repository.service';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { takeUntil, tap, switchMap, distinctUntilChanged, startWith, map } from 'rxjs/operators';
import { CostTypeRepositoryService } from 'src/app/core/services/repositories/cost-type-repository.service';
import { CostTypeKey, ICostType } from 'src/app/core/models/resources/ICostTypes';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { ICurrency, CurrencyKey } from 'src/app/core/models/resources/ICurrency';
import { formatNumberWrapper, getDateTimeFormat, getLocalDateTime } from 'src/app/shared/helpers/general';
import { ICostCenter, CostCenterKey } from 'src/app/core/models/resources/ICostCenter';
import { CostCenterRepositoryService } from 'src/app/core/services/repositories/cost-center-repository.service';
import { CostUnitRepositoryService } from '../../../../../core/services/repositories/cost-unit-repository.service';
import { CostUnitKey, ICostUnit } from '../../../../../core/models/resources/ICostUnit';
import { convertToMap } from '../../../../../shared/helpers/general';
import { BookingAccountKey, IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { ITaxKey } from 'src/app/core/models/resources/ITaxKeys';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { IBookingCode, BookingCodeKey } from 'src/app/core/models/resources/IBookingCode';
import { IWording } from 'src/app/core/models/resources/IWording';
import { ACCOUNTING_TYPES, accountingTypeOptionsFull } from 'src/app/core/models/enums/accounting-types';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { BookingRepositoryService } from 'src/app/core/services/repositories/booking-repository.service';
import { IBooking } from 'src/app/core/models/resources/IBooking';
import { IBookingLine } from 'src/app/core/models/resources/IBookingLine';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { IMandatorAccount } from 'src/app/core/models/resources/IMandatorAccount';
import { getGridConfigBookingLines, getGridConfigCalculatedData } from 'src/app/core/constants/nv-grid-configs/account-sheets.config';
import { AccountValidatorsService } from 'src/app/core/services/validators/account-validators.service';
import { MandatorValidatorsService } from 'src/app/core/services/validators/mandator-validators.service';
import { ExcelExportService } from '../../../helpers/excel-export-helper.service';

interface ISummaryLine {
  id: number;
  amountBroughtForward: string;
  currencyId: CurrencyKey;
  currencyIsoCode: string;
  year: number;
  debit: number;
  credit: number;
}

@Component({
  selector: 'nv-account-sheets',
  templateUrl: './account-sheets.component.html',
  styleUrls: ['./account-sheets.component.scss']
})
export class AccountSheetsComponent implements OnInit, OnDestroy {
  @ViewChild('bookingLines') public bookingLinesGrid: GridComponent;

  public mainGridConfig: NvGridConfig;
  public sideGridConfig: NvGridConfig;
  public form: FormGroup;
  public formSummary: FormGroup;
  public wording = wording;
  public ACL_ACTIONS = ACL_ACTIONS;

  private bookingLinesObject: ReplaySubject<IAccountStatementFull[]> = new ReplaySubject<any[]>(1);
  public bookingLines$ = this.bookingLinesObject.asObservable();

  private summaryLinesSubject: ReplaySubject<any[]> = new ReplaySubject<any[]>(1);
  public calculatedData$ = this.summaryLinesSubject.asObservable();

  public displayedCurrencies$ = new BehaviorSubject<ICurrency[]>([]);

  public mandatorGridConfig = mandatorGridConfig();
  public accountGridConfig = accountGridConfig;

  public mandatorNoModifier = mandatorNoModifier;

  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  public maxSelectableYear: Number;
  public accountingTypeOptions: ISelectOption<ACCOUNTING_TYPES>[] = [];
  public customTitle: IWording = wording.accounting.accountSheets;


  /**
   * stores keys required when we want to predefine values from booking lines
   */
  private predefineRequiredKeys: string[] = [
    'bookingAccountId',
    'mandatorId',
    'year',
  ];

  public summaryLines: ISummaryLine[];

  public accountSubject: BehaviorSubject<IBookingAccount[]> = new BehaviorSubject<IBookingAccount[]>([]);

  public disableOpenitemsIcon = true;
  public openItemsNode = sitemap.accounting.children.booking.children.openItemAccountSheets;
  public openItemsZone = SitemapEntry.getByNode(this.openItemsNode).toZone();
  public canAccessOpenItems = false;
  private dateTimeFormat: string;

  constructor(
    private formBuilder: FormBuilder,
    private settingsService: SettingsService,
    public bookingAccountRepo: BookingAccountRepositoryService,
    public mandatorAccountRepo: MandatorAccountRepositoryService,
    private router: Router,
    public mandatorRepo: MandatorRepositoryService,
    private taxKeyRepo: TaxKeyRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private accountStatementRepositoryService: AccountStatementRepositoryService,
    private balanceRepo: BalanceRepositoryService,
    private authService: AuthenticationService,
    private mandatorValidatorsService: MandatorValidatorsService,
    private accountValidatorService: AccountValidatorsService,
    private route: ActivatedRoute,
    private costTypeRepo: CostTypeRepositoryService,
    private costCenterRepo: CostCenterRepositoryService,
    private costUnitRepo: CostUnitRepositoryService,
    private bookingCodeRepo: BookingCodeRepositoryService,
    private bookingRepo: BookingRepositoryService,
    private excelExportService: ExcelExportService,
  ) {
  }

  public ngOnInit() {
    this.dateTimeFormat = getDateTimeFormat(this.settingsService.locale);
    this.canAccessOpenItems = this.authService.canAccessNode(this.openItemsNode);
    this.initForm();
    this.initFormSummary();
    this.mandatorIdChanges();
    this.filtersChangesHandler();
    this.currencyIdChanges();
    this.mandatorBookingChangesHandler();

    this.updateDisplayedCurrencies();

    this.maxSelectableYear = new Date().getFullYear();

    forkJoin([
      this.mandatorRepo.fetchLookups({}),
      this.bookingAccountRepo.fetchAll({}),
      this.bookingCodeRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.taxKeyRepo.fetchAll({}),
      this.costTypeRepo.fetchAll({}),
      this.costCenterRepo.fetchAll({}),
      this.costUnitRepo.fetchAll({}),
      this.bookingRepo.fetchAll({}),
    ]).pipe(
      takeUntil(this.componentDestroyed$),
      tap(() => {
        if (history.state.selectedMandatorId) {
          this.form.get('mandatorId').setValue(history.state.selectedMandatorId);
          this.form.get('bookingAccountId').setValue(history.state.selectedAccountId);
          this.form.get('accountingNorm').setValue(history.state.selectedNorm);
          this.form.get('year').setValue(history.state.selectedYear);
        }

        const queryParamsMap = this.route.snapshot.queryParamMap;
        if (this.predefineRequiredKeys.every(key => queryParamsMap.has(key))) {
          this.predefineRequiredKeys.forEach(key => {
            const control = this.form.get(key);
            control.setValue(+queryParamsMap.get(key));
          });
        }
      })
    ).subscribe();

    const actions = {
      clone: (accountStatement: IAccountStatementFull) => this.cloneBooking(accountStatement),
      show: (accountStatement: IAccountStatementFull) => this.showBooking(accountStatement),
      edit: (accountStatement: IAccountStatementFull) => this.router.navigate([
        url(sitemap.accounting.children.booking.children.bookings),
        accountStatement.bookingId,
      ]),
    };

    this.mainGridConfig = getGridConfigBookingLines(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
      { debitCredit: this.debitCreditCustomFormat },
      this.onExcelExport
    );
    this.sideGridConfig = getGridConfigCalculatedData(this.settingsService.locale);

    this.bookingLinesObject.next([]);
    this.summaryLinesSubject.next([]);
  }

  public next(backwards = false): void {
    const accountsOfMandators = this.mandatorAccountRepo.getStreamValue()
      .filter((mandatorBookingAccount) => mandatorBookingAccount.mandatorId === this.form.get('mandatorId').value);


    const currentIndex = accountsOfMandators
      .findIndex(bookingAccount => bookingAccount.bookingAccountId === this.form.get('bookingAccountId').value);

    const newIndex = backwards ? currentIndex - 1 : currentIndex + 1;
    const foundBookingAccount = accountsOfMandators[newIndex];

    if (foundBookingAccount) {
      this.form.get('bookingAccountId').patchValue(foundBookingAccount.bookingAccountId);
    }
  }

  private updateYearByMandatorFiscalYear(mandatorId: Number): void {
    const mandator: IMandatorLookup = this.mandatorRepo.getLookupStreamValue()
      .find(m => m.id === mandatorId);

    if (mandator) {
      const fiscalCalendarYear = getFiscalCalendarDate(new Date(), mandator.startFiscalYear).getFullYear();
      this.form.get('year').patchValue(fiscalCalendarYear ? fiscalCalendarYear : null);
    }
  }

  private mandatorIdChanges(): void {
    this.form.get('mandatorId').valueChanges
      .pipe(
        takeUntil(this.componentDestroyed$),
        tap(mandatorId => this.updateYearByMandatorFiscalYear(mandatorId)),
        switchMap(mandatorId => this.mandatorAccountRepo.fetchAll({ queryParams: { mandatorId: mandatorId } })),
        switchMap(mandatorAccount => this.updateBookingAccounts(mandatorAccount))
      ).subscribe(() => {
        this.form.get('bookingAccountId').updateValueAndValidity({ emitEvent: false });
        this.search();
      });
  }

  private updateBookingAccounts(mandatorAccounts: IMandatorAccount[]): Observable<IBookingAccount[]> {
    const openMandatorAccounts: IMandatorAccount[] =
      mandatorAccounts.filter((mandatorAccount) => mandatorAccount.isOpenItem === true);

    return this.bookingAccountRepo.getStream()
      .pipe(
        map(bookingAccounts => bookingAccounts
          .filter(bookingAccount => openMandatorAccounts
            .some(a => a.bookingAccountId === bookingAccount.id))
        ),
        tap(bookingAccounts => {
          this.accountSubject.next(bookingAccounts);
          this.enableDisableOpenItems();
        })
      );
  }

  private filtersChangesHandler(): void {
    const { year, bookingAccountId, accountingNorm } = this.form.controls;

    combineLatest([
      year.valueChanges.pipe(distinctUntilChanged()),
      bookingAccountId.valueChanges.pipe(distinctUntilChanged()),
      accountingNorm.valueChanges.pipe(distinctUntilChanged()),
    ]).pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(() => {
      this.enableDisableOpenItems();
      this.search();
    });
  }

  private enableDisableOpenItems() {
    const { mandatorId, bookingAccountId, accountingNorm } = this.form.controls;
    const selectedMandatorId: MandatorKey = mandatorId.value;
    const selectedAccountId: BookingAccountKey = bookingAccountId.value;
    const selectedNorm: ACCOUNTING_TYPES = accountingNorm.value;

    if (!(selectedMandatorId && selectedAccountId && selectedNorm)) {
      this.disableOpenitemsIcon = true;
      return;
    }

    this.disableOpenitemsIcon = !this.accountSubject.getValue()
      .some(e => e.id === selectedAccountId);
  }

  public goToOpenItems() {
    const { mandatorId, bookingAccountId, accountingNorm, year } = this.form.controls;
    const selectedMandatorId: MandatorKey = mandatorId.value;
    const selectedAccountId: BookingAccountKey = bookingAccountId.value;
    const selectedNorm: ACCOUNTING_TYPES = accountingNorm.value;
    const selectedYear: number = year.value;
    const baseUrl = url(sitemap.accounting.children.booking.children.openItemAccountSheets);
    this.router.navigate([baseUrl], {
      state: {
        selectedNorm: selectedNorm,
        selectedMandatorId: selectedMandatorId,
        selectedAccountId: selectedAccountId,
        selectedYear: selectedYear
      }
    });
  }

  private mandatorBookingChangesHandler(): void {
    const {
      mandatorId: mandatorCtrl,
      bookingAccountId: bookingAccountCtrl,
    } = this.form.controls;

    combineLatest([
      mandatorCtrl.valueChanges.pipe(distinctUntilChanged()),
      bookingAccountCtrl.valueChanges.pipe(distinctUntilChanged()),
    ]).pipe(
      takeUntil(this.componentDestroyed$),
      startWith([]),
    ).subscribe(() => {
      const { mandatorId, bookingAccountId } = this.form.getRawValue();

      const mandator: IMandatorLookup = this.mandatorRepo
        .getLookupStreamValue()
        .find(m => m.id === mandatorId);
      const bookingAccount: IBookingAccount = this.bookingAccountRepo
        .getStreamValue()
        .find(account => account.id === bookingAccountId);

      this.setAccountingNorms(mandator, bookingAccount);
    });
  }

  private setCustomTitle(type?: ACCOUNTING_TYPES): void {
    const defaultWording: IWording = wording.accounting.accountSheets;
    const norm: ISelectOption<ACCOUNTING_TYPES> = accountingTypeOptionsFull
      .find(option => option.value === type);
    const normWording: IWording = norm?.displayLabel;

    this.customTitle = type
      ? {
        en: `${defaultWording.en} (${normWording?.en})`,
        de: `${defaultWording.de} (${normWording?.de})`,
      }
      : defaultWording;
  }

  private setAccountingNorms(
    mandator: IMandatorLookup,
    bookingAccount: IBookingAccount,
  ): void {
    if (!(mandator && bookingAccount)) {
      this.accountingTypeOptions = accountingTypeOptionsFull.map(option => ({
        ...option,
        disabled: true,
      }));
      return;
    }

    this.setAccountingNormOptions(mandator, bookingAccount);
    this.setAccountingNormDefault(mandator.accountingType);
  }

  private setAccountingNormOptions(
    mandator: IMandatorLookup,
    bookingAccount: IBookingAccount,
  ): void {
    const allBookings: IBooking[] = this.bookingRepo.getStreamValue();

    this.accountingTypeOptions = accountingTypeOptionsFull.map(option => {
      const hasBooking: boolean = allBookings.some(booking => {
        if (booking.accountingType !== option.value) {
          return false;
        }

        return booking.bookingLines.some((bookingLine: IBookingLine) => {
          return bookingLine.mandatorId === mandator.id
            && bookingLine.bookingAccountId === bookingAccount.id;
        });
      });

      return {
        ...option,
        disabled: !hasBooking,
      };
    }).filter(({ value, disabled }) => {
      const exists: boolean = !!(
        bookingAccount.accountingType
        & mandator.accountingType
        & value
      );

      return exists || !disabled;
    });
  }

  private setAccountingNormDefault(accountingType: number): void {
    const { accountingNorm } = this.form.controls;

    /**
     * To check norm is enabled or not just needed to use binary-and operator.
     *
     * `enabledNorms & ACCOUNTING_TYPES.HGB` returns `ACCOUNTING_TYPES.HGB` if
     * HGB is not disabled, otherwise it returns 0
     */
    let enabledNorms: number;
    let existedNorms: number;
    this.accountingTypeOptions.forEach(({ disabled, value }) => {
      existedNorms = existedNorms | value;
      enabledNorms = disabled ? enabledNorms : enabledNorms | value;
    }, 0);

    if (existedNorms & accountingNorm.value) {
      return;
    }

    // Keep order
    const norm: ACCOUNTING_TYPES = ACCOUNTING_TYPES.HGB & enabledNorms & accountingType
      || ACCOUNTING_TYPES.IFRS & enabledNorms & accountingType
      || ACCOUNTING_TYPES.USGAAP & enabledNorms & accountingType
      || ACCOUNTING_TYPES.EBalance & enabledNorms & accountingType
      || null;
    accountingNorm.setValue(norm);
  }

  private currencyIdChanges(): void {
    this.formSummary.get('currencyId').valueChanges
      .pipe(
        distinctUntilChanged(),
        takeUntil(this.componentDestroyed$)
      ).subscribe(currencyId => {
        this.handleCurrencyIdChanges(currencyId);
      });
  }

  private handleCurrencyIdChanges(currencyId: any): void {
    const currency: ICurrency = this.getCurrency(currencyId);

    // Set column grid filter condition
    const currencyColumn = this.bookingLinesGrid?.gridConfig?.columns
      .find(column => column.key === 'currencyIsoCode');

    if (!currencyColumn) {
      return;
    }

    if (currency) {
      currencyColumn.filter.values = [currency.isoCode];
      currencyColumn.filter.form?.get('value').patchValue(currency.isoCode, { emitEvent: false });
    } else {
      currencyColumn.filter.values = [];
      currencyColumn.filter.form?.get('value').patchValue('', { emitEvent: false });
    }

    this.bookingLinesGrid.filterGrid();
  }

  private initForm(): void {
    this.form = this.formBuilder.group({
      mandatorId: [null, [
        Validators.required,
        this.mandatorValidatorsService.doesMandatorExist
      ]],
      bookingAccountId: [null, [
        Validators.required,
        this.accountValidatorService.doesBookingAccountExist
      ]],
      year: [new Date().getFullYear(), [Validators.required, Validators.year]],
      accountingNorm: [null],
    });
  }

  private initFormSummary(): void {
    this.formSummary = this.formBuilder.group({
      currencyId: null
    });
  }

  public cloneBooking = (bookingLine: IAccountStatementFull) => {
    window.open(`${url(sitemap.accounting.children.booking.children.bookings)}/new?clones=${bookingLine.bookingId}`);
  }

  public showBooking = (bookingLine: IAccountStatementFull) => {
    window.open(`${url(sitemap.accounting.children.booking.children.bookings)}/${bookingLine.bookingId}?readOnly=true`);
  }

  private debitCreditCustomFormat = (debitCredit: number): string => {
    return debitCredit === 1
      ? this.settingsService.debitCredit === DEBIT_CREDIT.plusMinus ? '+' : this.wording.accounting.DEBIT[this.settingsService.language]
      : debitCredit === -1
        ? this.settingsService.debitCredit === DEBIT_CREDIT.plusMinus ? '-' : this.wording.accounting.CREDIT[this.settingsService.language]
        : null;
  }

  public search(): void {
    this.accountStatementRepositoryService.clearStream();
    this.balanceRepo.clearStream();
    this.updateSummaryGrid();
    this.updateDisplayedCurrencies();

    const { year, mandatorId, bookingAccountId, accountingNorm } = this.form.controls;

    const selectedYear: number = year.value;
    const selectedMandatorId: MandatorKey = mandatorId.value;
    const selectedAccountId: BookingAccountKey = bookingAccountId.value;
    const selectedNorm: ACCOUNTING_TYPES = accountingNorm.value;

    this.setCustomTitle(selectedNorm);

    if (!(selectedYear && selectedMandatorId && selectedAccountId && accountingNorm)) {
      return;
    }

    const queryParams: AccountStatementQueryParams = {
      mandatorId: selectedMandatorId,
      bookingAccountId: selectedAccountId,
      year: selectedYear,
      accountingType: selectedNorm,
    };

    const accountStatementsRequest$: Observable<IAccountStatement[]> = selectedNorm
      ? this.accountStatementRepositoryService.fetchAll({ queryParams })
      : of([]);

    forkJoin([
      accountStatementsRequest$,
      this.balanceRepo.fetchAll({
        queryParams: {
          mandatorId: selectedMandatorId,
          bookingAccountId: selectedAccountId,
          year: selectedYear
        }
      }),
    ])
      .subscribe(([accountStatements]: [IAccountStatement[], IBalance[]]) => {
        const accountStatementsFull = this.convertToAccountStatementFull(accountStatements);

        this.bookingLinesObject.next(accountStatementsFull);
        this.updateDisplayedCurrencies();
        this.updateSummaryGrid();
      });
  }

  private getBookingLines(): IAccountStatementFull[] {
    return this.bookingLinesGrid
      ? this.bookingLinesGrid.rawRows
      : [];
  }

  private sumOf(array: any[]): number {
    return +Math.abs(array.reduce((prev, cur) => +prev + +cur, 0));
  }

  public calcTransactionAmountSumForRows(
    accountStatements: IAccountStatementFull[],
    debitCreditType: number,
    currencyId: CurrencyKey): number {
    return this.sumOf(
      accountStatements
        .filter(row => row.debitCreditType === debitCreditType && row.currencyId === currencyId)
        .map(row => row.amountTransaction));
  }

  public calcLocalAmountSumForRows(
    accountStatements: IAccountStatementFull[],
    debitCreditType: number): number {
    return this.sumOf(
      accountStatements
        .filter(row => row.debitCreditType === debitCreditType)
        .map(row => row.amountLocal));
  }

  public getDebitCreditSign(value: number): string {
    return value > 0
      ? this.settingsService.debitCredit === DEBIT_CREDIT.plusMinus ? '+' :
        wording.accounting.DEBIT[this.settingsService.language]
      : value < 0
        ? this.settingsService.debitCredit === DEBIT_CREDIT.plusMinus ? '-'
          : wording.accounting.CREDIT[this.settingsService.language]
        : '';
  }

  private getAmountString(amount: number): string {
    const formattedAmount = formatNumberWrapper(Math.abs(amount), 1, 2, 2, this.settingsService.locale);
    return `${formattedAmount} ${this.getDebitCreditSign(amount)}`;
  }

  private getPreviousYearSummaryLine(
    year: number,
    forAllCurrencies: boolean,
    amountCurrency: ICurrency,
    balances: IBalance[]
  ): ISummaryLine {

    let debitAmount = 0;
    let creditAmount = 0;
    let amountBroughtForward = '';

    if (forAllCurrencies) {
      const sumBalance: IBalance = this.calcSumBalanceForAllCurrencies(balances, year);

      if (sumBalance) {
        debitAmount = sumBalance.debitAmountLocal;
        creditAmount = sumBalance.creditAmountLocal;
        amountBroughtForward = this.getAmountString(sumBalance.previousYearLocal);
      }
    } else {
      const balance: IBalance = balances.find(b =>
        b.year === year && b.currencyId === amountCurrency.id);

      if (balance) {
        debitAmount = balance.debitAmountTransaction;
        creditAmount = balance.creditAmountTransaction;
        amountBroughtForward = this.getAmountString(balance.previousYearTransaction);
      }
    }

    const calculatedBalance = {
      id: 1,
      amountBroughtForward: amountBroughtForward,
      year: year,
      currencyId: amountCurrency.id,
      currencyIsoCode: amountCurrency.isoCode,
      debit: debitAmount,
      credit: creditAmount
    } as ISummaryLine;

    return calculatedBalance;
  }

  private getCurrentYearSummaryLine(
    year: number,
    forAllCurrencies: boolean,
    amountCurrency: ICurrency,
    balances: IBalance[],
    bookingLines: IAccountStatementFull[]): ISummaryLine {

    let debitAmount = 0;
    let creditAmount = 0;
    let amountBroughtForward = '';

    if (forAllCurrencies) {
      const sumBalance: IBalance = this.calcSumBalanceForAllCurrencies(balances, year);

      if (sumBalance) {
        // Note: the amount is recalculated from booking lines here, based on grid filtering
        debitAmount = this.calcLocalAmountSumForRows(bookingLines, +1);
        creditAmount = this.calcLocalAmountSumForRows(bookingLines, -1);
        amountBroughtForward = this.getAmountString(sumBalance.previousYearLocal);
      }
    } else {
      const balance: IBalance = balances.find(b =>
        b.year === year && b.currencyId === amountCurrency.id);

      if (balance) {
        // Note: the amount is recalculated from booking lines here, based on grid filtering
        debitAmount = this.calcTransactionAmountSumForRows(bookingLines, +1, amountCurrency.id);
        creditAmount = this.calcTransactionAmountSumForRows(bookingLines, -1, amountCurrency.id);
        amountBroughtForward = this.getAmountString(balance.previousYearTransaction);
      }
    }

    const summaryLine = {
      id: 0,
      year: year,
      amountBroughtForward: amountBroughtForward,
      currencyId: amountCurrency.id,
      currencyIsoCode: amountCurrency.isoCode,
      debit: debitAmount,
      credit: creditAmount
    } as ISummaryLine;

    return summaryLine;
  }

  private updateDisplayedCurrencies(): void {
    const balances: IBalance[] = this.balanceRepo.getStreamValue();

    // Displayed currencies come from balances
    const currencies = Array.from(new Set(balances
      .map(bl => this.getCurrency(bl.currencyId)))) || [];

    this.displayedCurrencies$.next(currencies);

    // Enable or disable the currency selectbox
    const currencyControl: AbstractControl = this.formSummary.get('currencyId');
    if (currencies.length === 0) {
      currencyControl.disable();
    } else {
      currencyControl.enable();
    }

    // Try to select currency from booking line grid filter
    let selectCurrencyValue = null;
    const currencyColumn = this.bookingLinesGrid?.gridConfig?.columns
      .find(column => column.key === 'currencyIsoCode');
    const filterCurrencyIsoCode = currencyColumn?.filter?.values[0];

    if (filterCurrencyIsoCode && typeof filterCurrencyIsoCode === 'string') {
      const filterCurrency = currencies.find(c => c.isoCode === filterCurrencyIsoCode);
      selectCurrencyValue = filterCurrency?.id;
    }

    // If no value is selected, select default
    currencyControl.patchValue(selectCurrencyValue || 'default');
  }

  private calcSumBalanceForAllCurrencies(balances: IBalance[], year: number): IBalance {
    // Sum up local amounts for all balances for a year,
    // there is one balance per year for each currency.
    // Note: local amount is in mandator currency, so it can be added up.
    return balances
      .filter(b => b.year === year)
      .reduce((sumBalance, currentBalance) => {
        sumBalance.creditAmountLocal += currentBalance.creditAmountLocal;
        sumBalance.debitAmountLocal += currentBalance.debitAmountLocal;
        sumBalance.previousYearLocal += currentBalance.previousYearLocal;
        return sumBalance;
      },
        // Start value
        {
          creditAmountLocal: 0,
          debitAmountLocal: 0,
          previousYearLocal: 0,
        } as IBalance);
  }

  public handleGridFilterChanged(): void {
    this.updateDisplayedCurrencies();
    this.updateSummaryGrid();
  }

  private updateSummaryGrid(): void {
    const selectedYear = this.form.get('year').value;
    const selectedMandatorId: MandatorKey = this.form.get('mandatorId').value;
    const selectedBookingAccountId: BookingAccountKey = this.form.get('bookingAccountId').value;

    if (!selectedYear || !selectedMandatorId || !selectedBookingAccountId) {
      this.summaryLines = [];
      this.summaryLinesSubject.next(this.summaryLines);
      return;
    }

    const selectedCurrencyId: (CurrencyKey | string) = this.formSummary.get('currencyId').value;
    const selectedMandator: IMandatorLookup = this.mandatorRepo.getLookupStreamValue()
      .find(mandator => mandator.id === selectedMandatorId);

    const balances: IBalance[] = this.balanceRepo.getStreamValue();
    const bookingLines: IAccountStatementFull[] = this.getBookingLines();

    // Should balances be displayed for all currencies or for a specific currency?
    const forAllCurrencies = selectedCurrencyId === 'default';

    // Balance for all currencies sums up local amounts of balances for all currencies and the year.
    // Balance for specific currency uses transaction amount of balance for currency and the year.
    const amountCurrencyId: CurrencyKey = forAllCurrencies ? selectedMandator.currencyId : selectedCurrencyId as CurrencyKey;
    const amountCurrency: ICurrency = this.getCurrency(amountCurrencyId);
    this.summaryLines = [];
    this.summaryLines[0] = this.getPreviousYearSummaryLine(
      selectedYear - 1,
      forAllCurrencies,
      amountCurrency,
      balances
    );

    this.summaryLines[1] = this.getCurrentYearSummaryLine(
      selectedYear,
      forAllCurrencies,
      amountCurrency,
      balances,
      bookingLines
    );

    return this.summaryLinesSubject.next(this.summaryLines);
  }

  private convertToAccountStatementFull(accountStatements: IAccountStatement[]): IAccountStatementFull[] {
    const costTypeMap: Map<CostTypeKey, ICostType> = convertToMap<ICostType, CostTypeKey>(
      this.costTypeRepo.getStreamValue()
    );

    const costCenterMap: Map<CostCenterKey, ICostCenter> = convertToMap<ICostCenter, CostCenterKey>(
      this.costCenterRepo.getStreamValue()
    );

    const costUnitMap: Map<CostUnitKey, ICostUnit> = convertToMap<ICostUnit, CostUnitKey>(
      this.costUnitRepo.getStreamValue()
    );
    const bookingCodesMap: Map<BookingCodeKey, IBookingCode> = convertToMap(
      this.bookingCodeRepo.getStreamValue(),
    );

    const labelKey = getLabelKeyByLanguage(this.settingsService.language);
    return accountStatements.map((accountStatement: IAccountStatement) => {
      const oppositeAccount = this.bookingAccountRepo.getStreamValue()
        .find(bookingAccount => bookingAccount.id === accountStatements[0].oppositeBookingAccountId);

      const costType: ICostType = costTypeMap.get(accountStatement.costTypeId);
      const costCenter: ICostCenter = costCenterMap.get(accountStatement.costCenterId);
      const costUnit: ICostUnit = costUnitMap.get(accountStatement.costUnitId);
      const bookingCode: IBookingCode = bookingCodesMap.get(accountStatement.bookingCodeId);
      const bookingCodeWording: IWording = {
        en: `${bookingCode.no} ${bookingCode.labelEn}`,
        de: `${bookingCode.no} ${bookingCode.labelDe}`,
      };

      return ({
        ...accountStatement,
        oppositeAccountNo: accountStatement.oppositeBookingAccountId === 0
          ? 'Diverse'
          : oppositeAccount
            ? oppositeAccount.no
            : null,
        accountName: oppositeAccount ? oppositeAccount.displayLabel : null,
        currencyIsoCode: this.getCurrencyIsoCode(accountStatement.currencyId),
        tax: this.getTaxKeyCode(accountStatement.taxKeyId),
        costTypeNo: costType?.no,
        costTypeName: costType && costType[labelKey],
        costCenterName: costCenter && costCenter[labelKey],
        costCenterNo: costCenter?.no,
        costUnitNo: costUnit?.no,
        costUnitName: costUnit && costUnit[labelKey],
        bookingCode: bookingCodeWording,
        bookingDate: getLocalDateTime(accountStatement.bookingDate, this.dateTimeFormat)
      });
    });
  }

  private getCurrency(currencyId: number): ICurrency {
    return this.currencyRepo.getStreamValue().find(currency => currency.id === currencyId);
  }

  private getCurrencyIsoCode(currencyId: number): string {
    const foundCurrency: ICurrency = this.getCurrency(currencyId);
    return foundCurrency ? foundCurrency.isoCode : null;
  }

  private getTaxKeyCode(taxKeyId: number): string {
    const foundTaxKey: ITaxKey = this.taxKeyRepo.getStreamValue().find(taxKey => taxKey.id === taxKeyId);
    return foundTaxKey ? foundTaxKey.code : null;
  }

  public isButtonDisabled(backwards = false): boolean {
    const mandatorControl = this.form.get('mandatorId');
    const bookingAccountControl = this.form.get('bookingAccountId');

    const bookingAccounts = this.mandatorAccountRepo.getStreamValue()
      .filter(mandatorBookingAccount =>
        mandatorBookingAccount.mandatorId === mandatorControl.value
      );

    const currentIndex = bookingAccounts
      .findIndex(mandatorBookingAccount =>
        mandatorBookingAccount.bookingAccountId === bookingAccountControl.value
      );

    return backwards
      ? currentIndex < 1
      : currentIndex === bookingAccounts.length - 1;
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public onExcelExport = (excelExportInfo: NvExcelExportInfo): void => {
    const year = this.form.get('year').value;
    const mandatorId = this.form.get('mandatorId').value;
    const bookingAccountId = this.form.get('bookingAccountId').value;
    const accountingNormId: ACCOUNTING_TYPES = this.form.get('accountingNorm').value;

    this.excelExportService.fetchExcelExportInfo(excelExportInfo, year, mandatorId, bookingAccountId, accountingNormId);
  }
}
