import { NvFilterControl, NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getPartnersGridConfig = (): NvGridConfig => {
  return ({
    gridName: 'partnersGridConfig',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    rowSelectionType: NvGridRowSelectionType.Checkbox,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'id',
        hidden: true,
        visible: true,
      },
      {
        key: 'companyName',
        title: wording.accounting.companyName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'firstName',
        title: wording.general.firstName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'lastName',
        title: wording.general.lastName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'salutationLabel',
        title: wording.accounting.salutation,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'taxNo',
        title: wording.accounting.taxNumber,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Select,
          selectValues: []
        }
      },
      {
        key: 'vatId',
        title: wording.accounting.vatId,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Select,
          selectValues: []
        }
      },
    ],
  });
};
