import {Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter} from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { COMPANY_LEGAL_FORM, COMPANY_LEGAL_FORM_TYPE, getCompanyLegalForm, getCompanyLegalFormTypes } from 'src/app/core/models/enums/company-legal-form';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { ILegalFormFilter } from '../legal-type-tree/legal-type-tree.component';

@Component({
  selector: 'nv-legal-type-dropdown',
  templateUrl: './legal-type-dropdown.component.html',
  styleUrls: ['./legal-type-dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LegalTypeDropdownComponent implements OnInit {
  @Input() public legalForm: COMPANY_LEGAL_FORM;
  @Input() public legalFormType: COMPANY_LEGAL_FORM_TYPE;
  @Output() public legalFormChange: EventEmitter<ILegalFormFilter> = new EventEmitter();

  public form: FormGroup;
  public companyLegalForms: ISelectOption<COMPANY_LEGAL_FORM>[] = getCompanyLegalForm();
  public companyLegalFormTypes: ISelectOption<COMPANY_LEGAL_FORM_TYPE>[] = [];
  public wording = wording;

  constructor(
    private fb: FormBuilder,
  ) { }

  public ngOnInit(): void {
    this.form = this.fb.group({
      legalForm: null,
      legalFormType: null,
    });
    this.enableOrDisableLegalFormType();

    this.form.get('legalForm').patchValue(this.legalForm);
    this.form.get('legalFormType').patchValue(this.legalFormType);

    if (this.legalForm) {
      this.setCompanyLegalFormTypes(this.legalForm);
    }
  }

  public onLegalFormChange(legalForm: COMPANY_LEGAL_FORM) {
    const legalFormTypeControl = this.form.get('legalFormType');
    this.setCompanyLegalFormTypes(legalForm);

    const defaultType = this.companyLegalFormTypes.find(c => c.default);
    if (defaultType) {
      legalFormTypeControl.patchValue(defaultType.value);
    } else {
      legalFormTypeControl.patchValue(null);
    }

    this.legalFormChange.emit({
      legalForm: this.form.get('legalForm').value,
      legalFormType: this.form.get('legalFormType').value,
    });
  }

  public onLegalFormTypeChange(legalFormType: COMPANY_LEGAL_FORM_TYPE): void {
    this.legalFormChange.emit({
      legalForm: this.form.get('legalForm').value,
      legalFormType: this.form.get('legalFormType').value,
    });
  }

  private setCompanyLegalFormTypes(legalForm: COMPANY_LEGAL_FORM) {
    const legalFormTypeControl = this.form.get('legalFormType');
    this.companyLegalFormTypes = getCompanyLegalFormTypes(legalForm);
    if (!this.companyLegalFormTypes.length) {
      legalFormTypeControl.patchValue(null);
    }
  }

  private enableOrDisableLegalFormType(): void {
    const legalFormTypeControl = this.form.get('legalFormType');

    const legalForm: FormControl = this.form.get('legalForm') as FormControl;

    legalForm.valueChanges.subscribe(() => {
      if (legalForm.value === COMPANY_LEGAL_FORM.LimitedPartnership
        || legalForm.value === COMPANY_LEGAL_FORM.CompanyWithLimitedLiability) {
        legalFormTypeControl.enable();
        legalFormTypeControl.setValidators([Validators.required]);
      } else {
        legalFormTypeControl.patchValue(null);
        legalFormTypeControl.disable();
        legalFormTypeControl.setValidators(null);
      }
    });

  }

}
