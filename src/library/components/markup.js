import {getAccordionData} from '../library/data/ranom.data.js'

const onBtnClick = () => alert('button is clicked');


export const markup = `
  <text-component selector="h3">Accordion</text-component>
  <accordion-component data="${getAccordionData}" open="1"></accordion-component>
  
  <text-component selector="h3">Button</text-component>
  <button-component text="click on me" click="${onBtnClick}" class="btn"></button-component>
  
  <text-component selector="h3">Data binding</text-component>
  <data-binding-component initValue="init"></data-binding-component>
`;