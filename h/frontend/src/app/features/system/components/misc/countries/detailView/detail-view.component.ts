import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { BaseComponentService } from '../../../../../../core/services/base-component.service';
import { ICountry, ICountryCreate } from '../../../../../../core/models/resources/ICountry';
import { sitemap } from '../../../../../../core/constants/sitemap/sitemap';
import { Validators } from '../../../../../../core/classes/validators';
import { CountryRepositoryService } from '../../../../../../core/services/repositories/country-repository.service';
import { ResourceDetailView } from '../../../../../../core/abstracts/ResourceDetailView';

@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class CountryDetailViewComponent extends ResourceDetailView<ICountry, ICountryCreate> implements OnInit {

  protected repoParams = {};
  protected resourceDefault = {
    isActive: true,
    euMember: false,
    sepaParticipant: false
  } as ICountry;

  public routeBackUrl: string;
  public country?: any;

  constructor(
    public route: ActivatedRoute,
    private fb: FormBuilder,
    countryRepo: CountryRepositoryService,
    baseComponentService: BaseComponentService
  ) {
    super(countryRepo, baseComponentService);
  }


  public ngOnInit(): void {
    this.init();
    this.routeBackUrl = this.url(sitemap.system.children.misc.children.countries);
    const { id } = this.route.snapshot.params;

    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);

  }

  public getTitle = () => `${this.resource.isoAlpha2} ${this.resource[this.labelKey]}`;

  public init(country: ICountry = this.resourceDefault): void {
    this.initForm(this.fb.group({
      labelDe: [country.labelDe, [Validators.required, Validators.maxLength(50)]],
      labelEn: [country.labelEn, [Validators.required, Validators.maxLength(50)]],
      isoNo: [country.isoNo, [Validators.required, Validators.min(0)]],
      isoAlpha3: [country.isoAlpha3, [Validators.required, Validators.exactLength(3)]],
      isoAlpha2: [country.isoAlpha2, [Validators.required, Validators.exactLength(2)]],
      nationality: [country.nationality, [Validators.required, Validators.maxLength(3)]],
      euMember: [country.euMember, Validators.required],
      sepaParticipant: [country.sepaParticipant, Validators.required],
      ibanLength: [country.ibanLength, [Validators.required, Validators.min(0), Validators.maxLength(2)]],
      currencyIsoCode: [country.currencyIsoCode, [Validators.required, Validators.maxLength(5)]],
      bbkShort: [country.bbkShort, [ Validators.maxLength(20)]],
      bbkNo: [country.bbkNo, [ Validators.maxLength(5)]],
      bbkCode: [country.bbkCode, [ Validators.maxLength(5)]],
      isActive: [country.isActive]
    }));
  }

  public submit(): void {
    if (!this.form.valid) { return; }

    const { value } = this.form;

    const freshCountry = this.creationMode
      ? { ...value }
      : { ...this.resource, ...value };

    this.creationMode
      ? this.createResource(freshCountry)
      : this.updateResource(freshCountry);
  }
}
