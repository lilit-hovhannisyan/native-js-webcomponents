import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';

import { VgCoreModule } from 'ngx-videogular';

import { MeComponentLoadingModule } from '@shared/me-directives/me-component-loader/me-component-loader.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MeAttributeModule } from '@shared/me-components/me-attribute/me-attribute.module';
import { MeButtonModule } from '@shared/me-components/me-button/me-button.module';
import { MeUploadFileModule } from '@shared/me-components/me-upload-file/me-upload-file.module';

import { DocumentCreateRoutingModule } from './document-create-edit-routing.module';
import { DocumentCreateComponent } from './document-create-edit.component';

@NgModule({
  declarations: [DocumentCreateComponent],
  imports: [
    CommonModule,
    MeButtonModule,
    MeAttributeModule,
    MatStepperModule,
    IconsModule,
    MeUploadFileModule,
    MeComponentLoadingModule,
    ReactiveFormsModule,
    DocumentCreateRoutingModule,
    VgCoreModule
  ]
})
export class DocumentCreateModule {}
