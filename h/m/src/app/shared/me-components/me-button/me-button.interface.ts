export interface MeButtonI {
  disabled?: boolean;
  type: 'regular' | 'transparent';
  color: 'primary' | 'secondary' | 'info' | 'success' | 'danger' | 'warning' | 'link';
  rounded?: boolean;
}
