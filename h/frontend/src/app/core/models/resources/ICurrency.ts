import { IWithLabels } from './IWithLabels';
import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';

export type CurrencyKey = number;

interface ISharedProps extends IWithLabels {
  isoCode: string;
  symbol: string;
  isActive: boolean;
  isFavorite?: boolean;
  isAllowedLocal?: boolean;
}

export interface ICurrencyBase extends ISharedProps, IResource<CurrencyKey> { }

export interface ICurrencyCreate extends ISharedProps { }

export interface ICurrencyDTO extends ICurrencyBase { }

export interface ICurrency extends ICurrencyDTO, IWithLabel { }

export interface ICurrencyRow extends ICurrency {
}

