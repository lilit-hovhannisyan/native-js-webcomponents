import { IResource } from '../resources/IResource';
import { IWithActive } from './IWithActive';
import { IWording } from './IWording';

export type PortKey = number;

interface ISharedProps extends IWithActive {
  shortName: string;
  name: string;
  unLoCode: string;
  cordsLat: number;
  cordsLong: number;
  countryId: number;
}

export interface IPortBase extends ISharedProps, IResource<PortKey> { }

export interface IPortCreate extends ISharedProps { }

export interface IPortDTO extends IPortBase { }

export interface IPort extends IPortDTO { }

export interface IPortFull extends IPort {
  countryName: IWording;
  countryIsoAlpha3: string;
  countryFlagPath: string;
}
