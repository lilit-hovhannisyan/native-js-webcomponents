import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeNewRightSidebarComponent } from './me-new-right-sidebar.component';

@NgModule({
  declarations: [MeNewRightSidebarComponent],
  imports: [CommonModule],
  exports: [MeNewRightSidebarComponent]
})
export class MeNewRightSidebarModule {}
