import { IResource } from './IResource';
import { CompanyKey } from './ICompany';

export type BusinessNameKey = number;

interface ISharedProps {
  companyId: CompanyKey;
  from: string;
  name: string;
}

export interface IBusinessNameBase extends ISharedProps, IResource<CompanyKey> { }

export interface IBusinessNameCreate extends ISharedProps { }

export interface IBusinessNameDTO extends IBusinessNameBase { }

export interface IBusinessName extends IBusinessNameDTO { }
