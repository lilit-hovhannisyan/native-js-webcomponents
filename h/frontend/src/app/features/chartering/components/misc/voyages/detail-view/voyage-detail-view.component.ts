import { SitemapNode } from './../../../../../../core/constants/sitemap/sitemap';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { dateFormatTypes } from 'src/app/core/models/dateFormat';
import * as moment from 'moment';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { SettingsService } from 'src/app/core/services/settings.service';
import { VoyagesMainService } from 'src/app/core/services/mainServices/voyages-main.service';

@Component({
  templateUrl: './voyage-detail-view.component.html',
  styleUrls: ['./voyage-detail-view.component.scss'],
})
export class VoyageDetailViewComponent implements OnInit {
  public voyageTitle = '';
  public routerBackQueryParams = {};
  private dateFormat: string;
  public sitemap = sitemap;
  public titleWording = wording.general.create;

  public isCreationMode = false;

  constructor(
    private voyagesService: VoyagesMainService,
    private route: ActivatedRoute,
    private router: Router,
    private settingsService: SettingsService
  ) { }

  public ngOnInit(): void {
    this.dateFormat = dateFormatTypes[this.settingsService.locale];

    this.route.params.subscribe(({id}) => {
      this.isCreationMode = id === 'new';

      this.titleWording = this.isCreationMode ? wording.general.create : wording.chartering.voyage;
      if (this.isCreationMode) { return; }
      this.setTitleAndQueryParams();
    });
  }

  public setTitleAndQueryParams(): void {
    const { id } = this.route.snapshot.params;

    if (this.isCreationMode) {
      const vesselAccountingId = +this.route.snapshot.queryParams['vesselAccountingId'];
      this.routerBackQueryParams = { vesselAccountingId };
    } else {
      this.voyagesService.fetchOne(id, {}).subscribe(voyage => {
        this.voyageTitle = `:
          ${voyage.vesselAccounting.vesselNo.toString().padStart(4, '0')}
          ${voyage.vesselAccounting.vesselName}
          ${voyage.year}-${voyage.no.toString().padStart(3, '0')}\t
          (${moment(voyage.from).format(this.dateFormat)} -
           ${moment(voyage.until).format(this.dateFormat)})`;
        this.routerBackQueryParams = { vesselAccountingId: voyage.vesselAccountingId };
      });
    }

  }

  public routeBack = (): void => {
    this.router.navigate(
      [url(sitemap.chartering.children.misc.children.voyages)],
      { queryParams: this.routerBackQueryParams },
    );
  }

  public tabDisabler = (node: SitemapNode): boolean => {
    const basicNode = sitemap.chartering.children.misc.children.voyages.children.id.children.basic;
    // in createMode only basic should be enabled
    return this.isCreationMode ? node === basicNode : true;
  }
}
