import { FormGroup } from '@angular/forms';
import { MeSelectInputI } from '@shared/me-components/me-attribute/me-select-input/me-select-input.interface';
import { EnumValueModel } from '../enum/enum-value.model';
import { DMChoiceAttributeModel } from './attribute.model';

export class MeSelectInputAttribute implements MeSelectInputI {
  private _data!: DMChoiceAttributeModel;
  private _form!: FormGroup;

  constructor() {}

  public set data(data: DMChoiceAttributeModel) {
    this._data = data;
  }

  public get data(): DMChoiceAttributeModel {
    return this._data;
  }

  public set form(form: FormGroup) {
    this._form = form;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get displayName(): string {
    return this.data.displayName;
  }

  public get internalName(): string {
    return this.data.internalName;
  }

  public get required(): boolean {
    return this.data.required;
  }

  public get optRequired(): boolean {
    return this.data.optRequired;
  }

  public get value(): string {
    return this.data.value;
  }

  public get values(): EnumValueModel[] {
    return this.data.values;
  }

  public get disabled(): boolean {
    return this.data.disabled;
  }
}
