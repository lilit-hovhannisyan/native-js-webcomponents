import {updateOutlet, getParsedUrl} from '../../../../../library/core/route-manager/route.manager.js';
import {
  updateHistoryAndDocumentTitle,
  traverseInShadowAndGetElement
} from '../../../../../library/helpers/random.helpers.js';
import {BaseComponent} from '../../../../../library/core/base-component/base.component.js';
import {basePath} from '../../../data/base.path.js';
import {getRoutes} from '../../../data/git.routes.js';

export class Navigation extends BaseComponent {
  constructor() {
    super(basePath +  'shared-components/navigation/navigation.component.css');
  }

  render = () => {
    const config = {
      tagName: 'button-component',
      attr: {
        class: 'navigation',
        text: this.getAttribute('text'),
        icon: this.getAttribute('icon'),
      },
      events: {
        click: this.handleRouteChange
      }
    };
    const el = this.createElementWithAttributes(config);


    this.removeStaticAtributesFromElNotToRender(this, ['text']);
    this.shadow.appendChild(el);

    this.updateNavigationState(getParsedUrl(location.pathname));
  }

  connectedCallback() {
    super.connectedCallback(this.render);
    this.handleBrowserBackForward();
  }

  handleRouteChange = () => {
    this.path = this.getAttribute('path');
    this.route = getRoutes().find(r => r.path === this.path );

    this.updateNavigationState(this.route.path);
    updateHistoryAndDocumentTitle(this.route.title, this.route.path);

    updateOutlet(this.route);
  }

  handleBrowserBackForward = () => {
    window.addEventListener('popstate', () => {
      this.updateNavigationState(getParsedUrl());
    });
  }

  updateNavigationState(compareWithPath) {
    let header = traverseInShadowAndGetElement(document.body, 'navigation-block');
    Array.from(header.children).forEach( i => {
      i.removeAttribute('active');

      if (i.getAttribute('path') === compareWithPath ) {
        i.setAttribute('active', 'true');
      }
    });
  }

  disconnectedCallback() {
    // remove all listeners
    window.removeEventListener('popstate', this.handleBrowserBackForward);
  }
}