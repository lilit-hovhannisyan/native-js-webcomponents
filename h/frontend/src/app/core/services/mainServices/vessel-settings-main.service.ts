import { Injectable } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { VesselSettingsRepositoryService } from 'src/app/core/services/repositories/vessel-settings-repository.service';
import { IVesselSetting } from 'src/app/core/models/resources/IVesselSetting';
import { VesselKey } from 'src/app/core/models/resources/IVessel';

@Injectable({ providedIn: 'root' })
export class VesselSettingsMainService {
  constructor(
    private vesselSettingsRepo: VesselSettingsRepositoryService,
  ) { }

  public fetchAllByVesselId(vesselId: VesselKey): Observable<IVesselSetting[]> {
    this.vesselSettingsRepo.fetchAll({ queryParams: { vesselId } }).subscribe();

    return this.vesselSettingsRepo.getStream()
      .pipe(
        map(vesselSettings => {
          return vesselSettings.filter(s => s.vesselId === vesselId);
        })
      );
  }

  /*
  Currently there is no endpoint for bunch update
  but services.lenght is up to 7
  */
  public updateSettings = (
    vesselId: VesselKey,
    services: IVesselSetting[],
  ): Observable<IVesselSetting[]> =>
    forkJoin(
      services.map((service) =>
        this.vesselSettingsRepo.update(service, {
          queryParams: { vesselId },
          isNotificationHidden: true,
        })
      )
    )
}
