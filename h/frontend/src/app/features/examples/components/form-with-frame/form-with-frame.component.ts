import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'nv-form-with-frame',
  templateUrl: './form-with-frame.component.html',
  styleUrls: ['./form-with-frame.component.scss']
})

export class FormWithFrameComponent implements OnInit {
  public repoParams = {};
  public routeBackUrl = url(sitemap.examples.children.mixed);
  public form: FormGroup;

  constructor(private fb: FormBuilder, private router: Router) { }

  public ngOnInit() {
    this.init();
  }

  public init() {
    this.form = this.fb.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]]
    });
  }

  public submit() {
    if (!this.form.valid) { return; }
    alert(`Hello, ${this.form.value.firstName}`);
  }

  public routeBack = () => this.router.navigate([this.routeBackUrl]);
}
