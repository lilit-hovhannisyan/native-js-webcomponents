import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { INotification, NotificationType } from '../models/INotification';
import { SettingsService } from './settings.service';
import { IWording } from '../models/resources/IWording';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  private _notification: Subject<INotification> = new Subject();
  public readonly notification$: Observable<INotification> = this._notification.asObservable();

  constructor(
    private nzNotification: NzNotificationService,
    private settingsService: SettingsService
  ) { }

  public initNotifications(): void {
    this.notification$.subscribe((notification: INotification) => {
      const params = { nzDuration: notification.duration, nzAnimate: true };
      const title = notification.title[this.settingsService.language];
      const message = notification.message[this.settingsService.language];

      switch (notification.type) {
        case NotificationType.Success:
          this.nzNotification.success(title, message, params);
          break;
        case NotificationType.Error:
          this.nzNotification.error(title, message, params);
          break;
        case NotificationType.Warning:
          this.nzNotification.warning(title, message, params);
          break;
        default:
          this.nzNotification.warning(title, message, params);
      }
    });
  }

  public notify(type: NotificationType, title: IWording, message: IWording, duration: number = 4000): void {
    this._notification.next({ type, title, message, duration });
  }

  public clearNotifications() {
    this.nzNotification.remove();
  }
}
