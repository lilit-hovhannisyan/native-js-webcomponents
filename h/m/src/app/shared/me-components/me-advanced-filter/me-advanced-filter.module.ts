import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MeIconsProviderModule } from '@shared/modules/me-icons.module';

import { MeButtonModule } from '../me-button/me-button.module';
import { MeAdvancedFilterComponent } from './me-advanced-filter.component';

@NgModule({
  imports: [CommonModule, MeIconsProviderModule, MeButtonModule],
  exports: [MeAdvancedFilterComponent],
  declarations: [MeAdvancedFilterComponent]
})
export class MeAdvancedFilterModule {}
