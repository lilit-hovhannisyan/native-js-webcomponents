import { Component, Input } from '@angular/core';
import { MeInputErrorI } from './me-input-error.interface';

@Component({
  selector: 'me-input-error',
  templateUrl: './me-input-error.component.html'
})
export class MeInputErrorComponent {
  @Input() data!: MeInputErrorI;
}
