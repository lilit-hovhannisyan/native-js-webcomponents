import { Component, Input, AfterViewInit } from '@angular/core';
import { GridComponent, NvColumnConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

/**
 * This component shows checkbox, and on it's value changes it shows and hides
 * 'isActive' column from grid and shows inactive entities with active entities if
 * checkbox checked, otherwise it only shows active entities.
 */
@Component({
  selector: 'nv-inactive-entities-checkbox',
  templateUrl: './inactive-entities-checkbox.component.html',
  styleUrls: ['./inactive-entities-checkbox.component.scss']
})
export class InactiveEntitiesCheckboxComponent implements AfterViewInit {
  @Input() public gridComponent: GridComponent;

  public column: NvColumnConfig;
  public checkbox: boolean;
  public wording = wording;
  constructor() { }

  public ngAfterViewInit() {
    this.column = this.gridComponent.gridConfig.columns.find(col => col.key === 'isActive');
    this.onCheckboxChange(this.checkbox);
  }

  public onCheckboxChange(value: boolean): void {
    // if checkbox checked passes empty array so all active and inactive entities will be
    // displayed, if checkbox unchecked pass true so will be displayed only active entities
    this.column.filter = {
      ...this.column.filter,
      values: value ? [] : ['1']
    };
    this.gridComponent.filterGrid();
  }
}
