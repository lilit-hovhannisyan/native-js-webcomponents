import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { ICostUnit } from 'src/app/core/models/resources/ICostUnit';
import { NvGridConfig } from 'nv-grid';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { wording } from 'src/app/core/constants/wording/wording';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { CostUnitRepositoryService } from 'src/app/core/services/repositories/cost-unit-repository.service';
import { getCostUnitsGridConfig } from 'src/app/core/constants/nv-grid-configs/cost-units.config';
import { BookingRepositoryService } from 'src/app/core/services/repositories/booking-repository.service';
import { IBookingLine } from 'src/app/core/models/resources/IBookingLine';
import { CostUnitMainService } from 'src/app/core/services/mainServices/cost-unit-main.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'nv-cost-units-list-view',
  templateUrl: './cost-units-list-view.component.html',
  styleUrls: ['./cost-units-list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CostUnitsListViewComponent implements OnInit {
  public dataSource$: Observable<ICostUnit[]>;
  public gridConfig: NvGridConfig;
  private bookingLines: IBookingLine[] = [];

  constructor(
    public gridConfigService: GridConfigService,
    private costUnitRepo: CostUnitRepositoryService,
    private router: Router,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private bookingRepo: BookingRepositoryService,
    private costUnitService: CostUnitMainService,
  ) { }

  public ngOnInit(): void {
    this.initNvGrid();
  }

  private initNvGrid(): void {
    const gridActions: IGridConfigActions = {
      edit: (row: ICostUnit) => {
        this.router.navigate([`${this.router.url}/${row.id}`]);
      },
      delete: (row: ICostUnit) => this.delete(row),
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    const canPerform: (operation: number) => boolean = (operation: number) => {
      return this.authService.canPerformOperationOnCurrentUrl(operation);
    };

    this.bookingRepo.fetchAll({}).subscribe();
    this.bookingRepo.getStream().subscribe(bookings => {
      this.bookingLines = bookings.flatMap(b => b.bookingLines);
      this.gridConfig = getCostUnitsGridConfig(gridActions, canPerform);
    });

    this.dataSource$ = this.costUnitService.getStream().pipe(
      map(costUnits => this.costUnitService.toRows(costUnits)),
    );
  }

  private delete: (costUnit: ICostUnit) => void = (costUnit: ICostUnit): void => {
    const isBooked: boolean = !!this.bookingLines.find(b => b.costUnitId === costUnit.id);

    if (isBooked) {
      this.confirmationService.confirm({
        okButtonWording: wording.general.ok,
        cancelButtonWording: null,
        wording: wording.accounting.costUnitBookedAndCannotBeDeleted
      }).subscribe();
      return;
    }

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: setTranslationSubjects(
        wording.general.areYouSureYouWantToDelete,
        { subject: costUnit.displayLabel },
      ),
    }).subscribe(confirmed => {
      if (confirmed) {
        this.costUnitRepo.delete(costUnit.id, {}).subscribe();
      }
    });
  }
}
