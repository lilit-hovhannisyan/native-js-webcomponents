import { NvColumnDataType, NvFilterControl, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';
import { getContextMenuButtonsConfig } from '../../helpers/general';
import { wordingGeneral } from '../wording/general';
import { Operations } from '../../models/Operations';
import { wordingChartering } from '../wording/chartering';
import { wording } from '../wording/wording';
import { IRegistrationFleet } from '../../models/IRegistrationFleet';
import { hasValidSettingRangeAndType } from 'src/app/features/chartering/components/registrations/registrations-fleet/registrations-fleet-list-view/registrations-fleet-list-view.helper';

export const getRegistrationsFleetGridConfig: (
  actions: IGridConfigActions,
  canPerform: (operations: Operations) => boolean,
) => NvGridConfig = (
  actions: IGridConfigActions,
  canPerform: (operations: Operations) => boolean,
  ): NvGridConfig => {
    return ({
      gridName: 'registrationsFleetGrid',
      rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
      hideRefreshButton: true,
      isSortAscending: true,
      showExcelButton: true,
      clearFiltersButton: true,
      paging: {
        pageNumber: 1,
        pageSize: 50
      },
      columns: [
        {
          key: 'vesselName',
          title: wordingChartering.vessel,
          width: 150,
          isSortable: true,
          filter: {
            values: []
          }
        },
        {
          key: 'charterName',
          title: wordingChartering.chartername,
          width: 150,
          isSortable: true,
          filter: {
            values: []
          }
        },
        {
          key: 'vesselTypeName',
          title: wordingChartering.vesselType,
          width: 150,
          isSortable: true,
          filter: {
            values: []
          },
        },
        {
          key: 'client',
          title: wordingChartering.client,
          width: 150,
          isSortable: true,
          filter: {
            values: []
          },
        },
        {
          key: 'comments',
          title: wordingChartering.comments,
          width: 150,
          isSortable: true,
          filter: {
            values: []
          },
        },
        {
          key: 'flagPath',
          title: wording.general.flag,
          width: 70,
          isSortable: false,
          filter: {
            values: []
          },
          customTooltip: () => wording.general.flag
        },
        {
          key: 'periodFrom',
          title: wordingChartering.periodFrom,
          width: 150,
          isSortable: true,
          filter: {
            controlType: NvFilterControl.Date,
            values: []
          },
          dataType: NvColumnDataType.Date
        },
        {
          key: 'periodUntil',
          title: wordingChartering.periodTill,
          width: 150,
          isSortable: true,
          filter: {
            controlType: NvFilterControl.Date,
            values: []
          },
          dataType: NvColumnDataType.Date
        },
        {
          key: 'typeOfRegistrationName',
          title: wordingChartering.typeOfRegistration,
          width: 150,
          isSortable: true,
          filter: {
            controlType: NvFilterControl.Select,
            values: []
          },
        },
        {
          key: 'ssrNo',
          title: wordingChartering.ssrNo,
          width: 150,
          isSortable: true,
          filter: {
            values: []
          },
          dataType: NvColumnDataType.Number
        },
        {
          key: 'callSign',
          title: wordingChartering.callSign,
          width: 150,
          isSortable: true,
          filter: {
            values: []
          },
        },
        {
          key: 'officialNo',
          title: wordingChartering.officialNo,
          width: 150,
          isSortable: true,
          filter: {
            values: []
          },
        },
        {
          key: 'imo',
          title: wordingChartering.imoNo,
          width: 150,
          isSortable: true,
          filter: {
            values: []
          },
          dataType: NvColumnDataType.Number
        },
        {
          key: 'registeredOwner',
          title: wordingChartering.registeredOwner,
          width: 150,
          isSortable: true,
          filter: {
            values: []
          },
        },
        {
          key: 'bbCharterer',
          title: wordingChartering.bbCharterer,
          width: 150,
          isSortable: true,
          filter: {
            values: []
          },
        },
      ],
      buttons: actions
        ? [
          {
            icon: 'edit',
            description: wordingGeneral.edit,
            name: 'editRegistrationsFleet',
            tooltip: wordingGeneral.edit,
            actOnDoubleClick: true,
            func: actions.edit,
            hidden: !canPerform(Operations.UPDATE)
          },
          {
            icon: 'delete',
            description: wordingGeneral.delete,
            name: 'deleteRegistrationsFleet',
            tooltip: wordingGeneral.delete,
            actOnDoubleClick: false,
            func: actions.delete,
            hidden: !canPerform(Operations.DELETE),
            disabled: (regFleet: IRegistrationFleet) => {
              return !hasValidSettingRangeAndType(regFleet.settings);
            }
          }
        ]
        : [],
      toolbarButtons: [],
      contextMenuButtons: canPerform(Operations.READ)
        ? getContextMenuButtonsConfig()
        : [],
    });
  };

