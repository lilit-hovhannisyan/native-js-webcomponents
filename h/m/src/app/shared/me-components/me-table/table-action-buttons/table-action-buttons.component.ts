import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import ActionButtonI from '../interfaces/action-button.interface';
import { TableActionButtonDirective } from './table-action-buttons.directive';

@Component({
  selector: 'me-table-action-buttons',
  templateUrl: './table-action-buttons.component.html',
  styleUrls: ['./table-action-buttons.component.scss'],
  viewProviders: [TableActionButtonDirective]
})
export class TableActionButtonsComponent implements OnInit {
  @Input() data!: ActionButtonI[];
  @Output() tableDataChangeEvent = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  addSubjectProperty(params: ActionButtonI): ActionButtonI & { subject: EventEmitter<any> } {
    const { componentData, component, type } = params;
    const adad = { componentData, component, type, subject: this.tableDataChangeEvent };
    console.log('adad', adad);
    return adad;
  }
}
