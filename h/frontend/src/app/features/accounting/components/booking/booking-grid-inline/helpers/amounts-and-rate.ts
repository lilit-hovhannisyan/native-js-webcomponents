import { FormGroup, AbstractControl } from '@angular/forms';
import { isNumber } from 'src/app/shared/helpers/general';
import { Injectable } from '@angular/core';

enum AmountRateFormControlKeys {
  TC = 'amountTransaction',
  Rate = 'exchangeRate',
  LC = 'amountLocal',
}

class FormControlUpdater {
  public isValueChanging: boolean;

  public updateFormControl(control: AbstractControl, newValue: any) {
    if (this.isValueChanging) {
      return;
    }

    if (control.value === newValue) {
      return;
    }

    this.isValueChanging = true;
    control.patchValue(newValue);
    this.isValueChanging = false;
  }
}

@Injectable({
  providedIn: 'root'
})
export class AmountAndRate {

  private formControlUpdater: FormControlUpdater = new FormControlUpdater();

  private multiply = (firstNumber: number, secondNumber: number, digits: number) =>
    +(firstNumber * secondNumber).toFixed(digits)

  private divide = (firstNumber: number, secondNumber: number, digits: number) =>
    +(firstNumber / secondNumber).toFixed(digits)

  private isValidNumber = (...numbers: any[]): number => {
    return numbers.reduce((prev: boolean, cur: any) => prev && cur && isNumber(+cur), true);
  }

  private calculateAmountTransaction = (rate: number, amountLocal: number): number => {
    return +this.multiply(amountLocal, rate, 2);
  }

  private calculateAmountLocal = (amountTransaction: number, rate: number): number => {
    return amountTransaction === 0 || rate === 0
      ? 0
      : +this.divide(amountTransaction, rate, 2);
  }

  private calculateRate = (amountTransaction: number, amountLocal: number): number => {
    return amountTransaction === 0 || amountLocal === 0
      ? 0
      : +this.divide(amountTransaction, amountLocal, 5);
  }

  // Case 1: if TC changes calculate LC.
  // If Rate is empty, calculate Rate instead.
  public handleAmountTransactionChange = (form: FormGroup) => {
    if (this.formControlUpdater.isValueChanging) {
      return;
    }

    const amountTransactionControl = form.get(AmountRateFormControlKeys.TC);
    const amountLocalControl = form.get(AmountRateFormControlKeys.LC);
    const rateControl = form.get(AmountRateFormControlKeys.Rate);

    const amountTransaction = amountTransactionControl.value;
    const amountLocal = amountLocalControl.value;
    const rate = rateControl.value;

    if (!this.isValidNumber(amountTransaction)) {
      return;
    }

    // Only process changed values
    const previousAmountTransaction = form.value[AmountRateFormControlKeys.TC];
    if (+amountTransaction === +previousAmountTransaction) {
      return;
    }

    if (this.isValidNumber(rate)) {
      // calculate LC
      const newValue = +this.calculateAmountLocal(+amountTransaction, +rate);
      this.formControlUpdater.updateFormControl(amountLocalControl, newValue);
    } else if (this.isValidNumber(amountLocal)) {
      // calculate Rate
      const newValue = +this.calculateRate(+amountTransaction, +amountLocal);
      this.formControlUpdater.updateFormControl(rateControl, newValue);
    }
  }

  // Case 2: if Rate changes calculate LC.
  // If TC is empty, calculate TC instead.
  public handleRateChange = (form: FormGroup) => {
    if (this.formControlUpdater.isValueChanging) {
      return;
    }

    // https://navido.atlassian.net/browse/NAVIDO-772
    const amountTransactionControl = form.get(AmountRateFormControlKeys.TC);
    const amountLocalControl = form.get(AmountRateFormControlKeys.LC);
    const rateControl = form.get(AmountRateFormControlKeys.Rate);

    const amountTransaction = amountTransactionControl.value;
    const amountLocal = amountLocalControl.value;
    const rate = rateControl.value;

    if (!this.isValidNumber(rate)) {
      return;
    }

    // Only process changed values
    const previousRate = form.value[AmountRateFormControlKeys.Rate];
    if (+rate === +previousRate) {
      return;
    }

    if (this.isValidNumber(amountTransaction)) {
      // calculate LC
      const newValue = +this.calculateAmountLocal(+amountTransaction, +rate);
      this.formControlUpdater.updateFormControl(amountLocalControl, newValue);
    } else if (this.isValidNumber(amountLocal)) {
      // calculate TC
      const newValue = +this.calculateAmountTransaction(+rate, +amountLocal);
      this.formControlUpdater.updateFormControl(amountTransactionControl, newValue);
    }
  }

  // Case 3: if LC changes calculate Rate.
  // If TC is empty, calculate TC instead.
  public handleAmountLocalChange = (form: FormGroup) => {
    if (this.formControlUpdater.isValueChanging) {
      return;
    }

    // https://navido.atlassian.net/browse/NAVIDO-771
    const amountTransactionControl = form.get(AmountRateFormControlKeys.TC);
    const amountLocalControl = form.get(AmountRateFormControlKeys.LC);
    const rateControl = form.get(AmountRateFormControlKeys.Rate);

    const amountTransaction = amountTransactionControl.value;
    const amountLocal = amountLocalControl.value;
    const rate = rateControl.value;

    if (!this.isValidNumber(amountLocal)) {
      return;
    }

    // Only process changed values
    const previousAmountLocal = form.value[AmountRateFormControlKeys.LC];
    if (+amountLocal === +previousAmountLocal) {
      return;
    }

    if (this.isValidNumber(amountTransaction)) {
      // calculate Rate
      const newValue = +this.calculateRate(+amountTransaction, +amountLocal);
      this.formControlUpdater.updateFormControl(rateControl, newValue);
    } else if (this.isValidNumber(rate)) {
      // calculate TC
      const newValue = +this.calculateAmountTransaction(+rate, +amountLocal);
      this.formControlUpdater.updateFormControl(amountTransactionControl, newValue);
    }
  }
}

export const enableOrDisableAmountLocal = (
  bookingLineCurrencyId: number,
  mandatorCurrencyId: number,
  amountLocal: AbstractControl,
  leadingAccount: boolean
) => {
  // https://navido.atlassian.net/browse/NAVIDO-763
  if ((+bookingLineCurrencyId === +mandatorCurrencyId) && amountLocal.enabled) {
    return amountLocal.disable({ emitEvent: false });
  }

  if (
    +bookingLineCurrencyId !== +mandatorCurrencyId
    && amountLocal.disabled
    && !leadingAccount
  ) {
    return amountLocal.enable({ emitEvent: false });
  }
};
