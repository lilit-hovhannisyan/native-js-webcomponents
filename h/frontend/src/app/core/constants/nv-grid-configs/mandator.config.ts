import { Router } from '@angular/router';
import { NvColumnDataType, NvFilterControl, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { Operations } from 'src/app/core/models/Operations';
import { IMandatorFull, mandatorNoModifier } from 'src/app/core/models/resources/IMandator';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';
import { IMandator } from '../../models/resources/IMandator';
import { sitemap } from '../sitemap/sitemap';
import { url } from '../sitemap/sitemap-entry';

const mandatorsUrl = url(sitemap.accounting.children.booking.children.mandators);

export const getGridConfig = (
  actions: IGridConfigActions,
  router: Router,
  canPerform?: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'mandatorGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 25
    },
    columns: [
      {
        key: 'no',
        title: wording.accounting.no,
        width: 50,
        isSortable: true,
        filter: {
          values: []
        },
        customFormatFn: (mandator: IMandatorFull) => mandatorNoModifier(mandator['no'])
      },
      {
        key: 'name',
        title: wording.general.name,
        width: 264,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'isoCode',
        title: wording.accounting.localCurrency,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Select,
          selectValues: []
        }
      },
      {
        key: 'lastClosedYearView',
        title: wording.accounting.lastClosedYear,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText,
        }
      },
      {
        key: 'lastClosedPeriod',
        title: wording.accounting.lastClosedPeriod,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.FreeText,
        }
      },
      {
        key: 'hgb',
        title: wording.accounting.hgb,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'ifrs',
        title: wording.accounting.ifrs,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'usgaap',
        title: wording.accounting.usgaap,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'eBalance',
        title: wording.accounting.eBalance,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'companyName',
        title: wording.accounting.companyName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'isActive',
        title: wording.accounting.active,
        width: 85,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean,
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isCostAccounting',
        title: wording.accounting.costAccounting,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isInvoiceWorkflow',
        title: wording.accounting.invoiceWorkflow,
        width: 154,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'startFiscalYear',
        title: wording.accounting.startFiscalYear,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'mandatorTypeName',
        title: wording.accounting.mandatorType,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Select,
          values: []
        }
      },
      {
        key: 'legalFormWording',
        title: wording.accounting.legalForm,
        width: 280,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'firstBookingYear',
        title: wording.accounting.firstBookingYear,
        width: 154,
        isSortable: true,
        filter: {
          values: []
        }
      }, {
        key: 'start',
        title: wording.accounting.start,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.DateTime,
        filter: {
          values: []
        }
      }
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editMandator',
        tooltip: wording.general.edit,
        actOnEnter: !actions.select,
        actOnDoubleClick: !actions.select,
        hidden: canPerform ? !canPerform(Operations.UPDATE) : true,
        func: (mandator: IMandator) => {
          if (router) {
            router.navigate([`${mandatorsUrl}/${mandator.id}`]);
          }
        },
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteMandator',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: canPerform ? !canPerform(Operations.DELETE) : true,
        func: actions.delete,
      },
      {
        actOnDoubleClick: true,
        func: row => actions.select(row),
        description: wording.autocomplete.selectRow,
        icon: 'select',
        actOnEnter: true,
        hidden: actions.select === undefined,
        tooltip: wording.general.select,
        name: 'select'
      }
    ],
    contextMenuButtons: canPerform && canPerform(Operations.READ) ? getContextMenuButtonsConfig() : [],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => canPerform ? !canPerform(Operations.CREATE) : true,
        func: () => {
          if (router) {
            router.navigate([`${mandatorsUrl}/new`]);
          }
        }
      }
    ]
  });
};
