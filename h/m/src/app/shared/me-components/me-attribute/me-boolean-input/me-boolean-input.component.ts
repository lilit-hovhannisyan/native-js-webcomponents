import { Component, ElementRef, OnDestroy, Optional, Self, AfterViewInit, OnInit, Injector } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { FormGroup, ControlValueAccessor, NgControl, AbstractControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';

import { Subject, Subscription } from 'rxjs';

import { MeAttributeComponentInterface } from '../me-attribute-component.interface';
import { MeBooleanInputI } from './me-boolean-input.interface';
import { BaseLoggerComponent } from '@features/tracking-system';

@Component({
  templateUrl: './me-boolean-input.component.html',
  styleUrls: ['./me-boolean-input.component.scss'],
  providers: [
    {
      provide: MatFormFieldControl,
      useExisting: MeBooleanInputComponent
    }
  ]
})
export class MeBooleanInputComponent
  extends BaseLoggerComponent
  implements MeAttributeComponentInterface, ControlValueAccessor, MatFormFieldControl<boolean>, OnInit, AfterViewInit, OnDestroy {
  // variables for MatFormFieldControl interface
  static nextId = 0;
  componentVersion: string = '0.0.1';
  currentValue!: boolean | undefined;
  name: string = '';
  data!: MeBooleanInputI;
  form!: FormGroup;
  private _placeholder!: string;
  private _required = false;
  private _disabled = false;
  private _valueChangeSubscription!: Subscription | undefined;
  stateChanges = new Subject<void>();
  focused = false;
  errorState = false;
  controlType = 'me-boolean-input';
  id = `me-boolean-input-${MeBooleanInputComponent.nextId++}`;
  describedBy = '';
  // tslint:disable-next-line:no-any
  onChange = (_: any) => {};
  onTouched = () => {};

  get formControl(): AbstractControl | null {
    return this.form.get(this.data.internalName);
  }

  constructor(
    injector: Injector,
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,

    @Optional() @Self() public ngControl: NgControl
  ) {
    super(injector);
    _focusMonitor.monitor(_elementRef, true).subscribe((origin) => {
      if (this.focused && !origin) {
        this.onTouched();
      }
      this.focused = !!origin;
      this.stateChanges.next();
    });

    if (this.ngControl !== null) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit(): void {
    this.form = this.data.form;
    this.name = this.data.internalName;
    this.currentValue = this.formControl?.value;
    this.disabled = this.data.disabled;
    this._valueChangeSubscription = this.formControl?.valueChanges.subscribe((val: boolean) => {
      this.currentValue = val;
    });
  }

  ngAfterViewInit(): void {
    if (this.formControl?.value === null) {
      setTimeout(() => {
        this.formControl?.setValue(false);
      }, 0);
    }
  }

  ngOnDestroy(): object {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
    if (this._valueChangeSubscription) {
      this._valueChangeSubscription.unsubscribe();
    }
    return super.ngOnDestroy();
  }

  // methods required by MatFormFieldControl interface
  get empty(): boolean {
    return false;
  }

  get shouldLabelFloat(): boolean {
    return this.focused || !this.empty;
  }

  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  get required(): boolean {
    return this._required;
  }
  set required(value: boolean) {
    this._required = value;
    this.stateChanges.next();
  }

  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = value;

    setTimeout(() => {
      if (this.form) {
        this._disabled ? this.formControl?.disable() : this.formControl?.enable();
        this.stateChanges.next();
      }
    }, 0);
  }

  get value(): boolean {
    return this.formControl?.value;
  }
  set value(bool: boolean) {
    this.formControl?.setValue(bool);
    this.stateChanges.next();
  }

  setDescribedByIds(ids: string[]): void {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(): void {}

  writeValue(bool: boolean): void {
    this.value = bool;
  }
  // tslint:disable-next-line:no-any
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  // tslint:disable-next-line:no-any
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
