import { Component, EventEmitter, Input, Output } from '@angular/core';

import { MeBreadcrumbActionI } from './me-breadcrumb-action.interface';
import { MeBreadcrumbI } from './me-breadcrumb.interface';

@Component({
  selector: 'me-breadcrumb',
  templateUrl: './me-breadcrumb.component.html',
  styleUrls: ['./me-breadcrumb.component.scss']
})
export class MeBreadcrumbComponent {
  @Input() dataModel!: MeBreadcrumbI;
  @Output() goToHome: EventEmitter<void> = new EventEmitter();

  // Additional options for using this breadcrumbs can be found in npm package xng-breadcrumb
  constructor() {}

  onGoToHomeClick(): void {
    this.goToHome.emit();
  }

  trackById(_index: number, item: MeBreadcrumbActionI): string {
    return item.button.id;
  }
}
