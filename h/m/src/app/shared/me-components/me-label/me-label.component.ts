import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { MeLabelI } from './me-label-inteface';

@Component({
  selector: 'me-label',
  templateUrl: './me-label.component.html',
  styleUrls: ['./me-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeLabelComponent implements OnInit {
  @Input() dataModel!: MeLabelI;

  constructor() {}

  ngOnInit(): void {}
}
