import { Component, AfterContentInit, AfterContentChecked, ContentChild, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormControlName, FormGroup, FormGroupDirective, AbstractControl } from '@angular/forms';
import { NVFormControl } from 'src/app/core/classes/NVFormControl';
import { startWith } from 'rxjs/operators';

/**
 * This component is used for wrapping inputs and add for those label and validation
 * error messages, it's used for almost all inputs in project
 */
@Component({
  selector: 'nv-input-wrapper',
  templateUrl: './input-wrapper.component.html',
  styleUrls: ['./input-wrapper.component.scss']
})
export class InputWrapperComponent implements AfterContentInit, AfterContentChecked, OnChanges {

  // gets FormControlName, which is passed as ngContent for this component
  @ContentChild(FormControlName, { static: false })
  public controlName: FormControlName;

  public formControl: NVFormControl | null;
  public formGroup: FormGroup;
  @Input() public label = '';
  @Input() public formLayout: 'horizontal' | 'vertical' | 'inline' = 'vertical';
  @Input() public horizontal = false;
  @Input() public showErrMessages = true;
  @Input() public redBorderError = true;
  @Input() public infoText: string;
  @Input() public showErrorInPopover = true;
  @Input() public requiredSymbol: boolean;
  @Input() public showLabel = true;
  @Input() public className = '';

  // Grid properties
  // Used to modify the Grid system, which is based on 24 cols(cells).
  @Input() public labelCol = 8;
  @Input() public inputCol = 16;
  @Input() public inputOffset = 0;
  @Input() public formItemCol = 24; // full item col i.e. label + input

  public required: boolean;

    // to avoid showing error messages when control disabled
  public get hasErrors(): boolean {
    return this.formControl.disabled ? false : !!this.formControl.errors;
  }

  constructor(
    public formGroupDirective: FormGroupDirective,
  ) { }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.requiredSymbol) {
      this.setRequiredIndicator();
    }
  }

  public ngAfterContentInit(): void {
    this.initFormControl();
  }

  public ngAfterContentChecked(): void {
    // if parent form(this.formGroupDirective.form) reference changed(form rebuilt) then init
    // new formGroup with new formControl
    if (this.formGroupDirective.form !== this.formGroup) {
      this.initFormControl();
    }
  }

  /**
   * Dynamic Gets formGroup from FormGroupDirective.form, then with FormControlName.name gets
   * formControl from formGroup
   */
  private initFormControl(): void {
    this.formGroup = this.formGroupDirective.form;
    this.formControl = this.formGroup.get(this.controlName.name as string) as NVFormControl;
    this.formControl.label = this.label;
    this.formControl.statusChanges.pipe(startWith(0)).subscribe(() => this.setRequiredIndicator());
  }

  private setRequiredIndicator(): void {
    // if @Input() requiredSymbol is passed in, it overrides the
    // validator.required
    if (this.requiredSymbol !== undefined) {
      this.required = !!this.requiredSymbol;

      return;
    }

    if (this.formControl && this.formControl.validator) {
      const validator = this.formControl.validator({} as AbstractControl);
      this.required = validator && validator.required;
    } else {
      this.required = false;
    }
  }
}


