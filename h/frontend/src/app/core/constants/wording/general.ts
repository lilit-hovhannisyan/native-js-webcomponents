// tslint:disable: max-line-length
export const wordingGeneral = {
  signOut: { en: 'Sign out', de: 'Ausloggen' },
  yes: { en: 'Yes', de: 'Ja' },
  no: { en: 'No', de: 'Nein' },
  numberNo: { en: 'No', de: 'Nr' },
  confirm: { en: 'Confirm', de: 'Bestätigen' },
  confirmation: { en: 'Confirmation', de: 'Bestätigung' },
  cancel: { en: 'Cancel', de: 'Abbrechen' },
  submit: { en: 'Submit', de: 'Einreichen' },
  from: { en: 'From', de: 'Von' },
  to: { en: 'To', de: 'Bis' },
  until: { en: 'Until', de: 'Bis' },
  admin: { en: 'Administrator', de: 'Administrator' },
  areYouSure: {
    en: 'Are you sure?',
    de: 'Sind Sie sicher?'
  },
  doesNotExist: { en: 'Does not exist', de: 'Existiert nicht' },
  areYouSureYouWantToDelete: {
    en: 'Are you sure you want to delete *{subject}* ?',
    de: 'Möchten Sie *{subject}* wirklich löschen ?'
  },
  edit: { en: 'Edit', de: 'Bearbeiten' },
  delete: { en: 'Delete', de: 'Löschen' },
  create: { en: 'Create', de: 'Erstellen' },
  clone: { en: 'Clone', de: 'Klonen' },
  add: { en: 'Add', de: 'Hinzufügen' },
  download: { en: 'Download', de: 'Download' },
  back: { en: 'Back', de: 'Zurück' },
  next: { en: 'Next', de: 'Nächster' },
  done: { en: 'Done', de: 'Erledigt' },
  set: { en: 'Set', de: 'Zuweisen' },
  changeForAll: { en: 'Change for all', de: 'Für alle ändern' },
  quit: { en: 'Quit', de: 'Verlassen' },
  deactivate: { en: 'Deactivate', de: 'Deaktivieren' },
  account: { en: 'Account', de: 'Konto' },
  label: { en: 'Label', de: 'Bezeichnung' },
  save: { en: 'Save', de: 'Speichern' },
  name: { en: 'Name', de: 'Name' },
  description: { en: 'Description', de: 'Beschreibung' },
  remarks: { en: 'Remarks', de: 'Anmerkungen' },
  start: { en: 'Start', de: 'Start' },
  logo: { en: 'Logo', de: 'Logo' },
  settings: { en: 'Settings', de: 'Settings' },
  status: { en: 'Status', de: 'Status' },
  hide: { en: 'Hide', de: 'Hide' },
  zipCode: { en: 'ZIP Code', de: 'Postleitzahl' },
  city: { en: 'City', de: 'Stadt' },
  street: { en: 'Street', de: 'Straße' },
  country: { en: 'Country', de: 'Land' },
  shortName: { en: 'Short Name', de: 'Abkürzung' },
  shortNameFull: { en: 'Short Name', de: 'Kurzname' },
  longitude: { en: 'Longitude', de: 'Längengrad' },
  latitude: { en: 'Latitude', de: 'Breitengrad' },
  addressType: { en: 'Address Type', de: 'Adresstyp' },
  addresses: { en: 'Addresses', de: 'Adressen' },
  true: { en: 'true', de: 'true' },
  false: { en: 'false', de: 'false' },
  firstName: { en: 'First Name', de: 'Vorname' },
  lastName: { en: 'Last Name', de: 'Nachname' },
  userName: { en: 'User Name', de: 'Benutzername' },
  displayName: { en: 'Display Name', de: 'Anzeigename' },
  isActive: { en: 'Is Active', de: 'ist aktiv' },
  lastLoggedIn: { en: 'Last Logged in', de: 'Letzte Anmeldung' },
  location: { en: 'Location', de: 'Standort' },
  enter: { en: 'Enter', de: 'Enter' },
  select: { en: 'Select', de: 'Select' },
  details: { en: 'Details', de: 'Einzelheiten' },
  open: { en: 'open', de: 'offen' },
  resetSelection: { en: 'All shareholdings', de: 'Alle Beteiligungen' },
  doYouWantToLeave: { en: 'Do you want to leave the page?', de: 'Möchten Sie die Seite verlassen?' },
  youHaveUnsavedChanges: {
    en: 'You have unsaved changes! If you leave, your changes will be lost.',
    de: 'Sie haben nicht gespeicherte Änderungen! Wenn Sie diese Seite verlassen, gehen Ihre Änderungen verloren.'
  },
  thisItemIsInactive: {
    en: 'This item is inactive',
    de: 'This item is inactive',
  },
  addUserRoles: {
    en: `Add user roles`,
    de: `Add user roles`
  },
  showInactiveEntities: { en: 'Show Inactive Entities', de: 'Inaktive Elemente anzeigen' },
  textTooShort: { en: 'Text too short', de: 'Text zu kurz' },

  systemDefault: { en: 'System default', de: 'System Default' },
  isWholeDayEvent: { en: 'Is whole day event', de: 'Is whole day event(de)' },
  none: { en: 'None', de: 'Kein' },
  roleRights: { en: 'Role rights', de: 'Rollen Rechte' },
  relatedZones: { en: 'Related Zones', de: 'Zugehörige Zonen' },
  relatedRoleZonePermissions: {
    en: 'Related Role-Zone-Permissions',
    de: 'Zugehörige Rollen-Zonen-Rechte'
  },

  systemTypesCanNotBeDeleted: {
    en: 'System Types can not be deleted',
    de: 'System Typen können nicht gelöscht werden'
  },

  topbarNotifications: {
    en: 'Notifications and Information',
    de: 'Benachrichtigungen und Infos',
  },

  bookmark: {
    en: 'Bookmark',
    de: 'Bookmark'
  },
  addClock: {
    en: 'Add Clock',
    de: 'Add Clock'
  },
  addTimeZone: {
    en: 'Add Timezone',
    de: 'Zeitzone hinzufügen'
  },
  humidity: {
    en: 'Humidity',
    de: 'Feuchtigkeit',
  },
  wind: {
    en: 'Wind',
    de: 'Wind'
  },
  bookmarkTopics: {
    en: 'Bookmarked Topics',
    de: 'Themen mit Lesezeichen'
  },
  todoList: {
    en: 'To Do List',
    de: 'To Do Liste'
  },
  active: { en: 'Active', de: 'Aktiv' },
  // validation error messages
  validationError: { en: 'Validation Error', de: 'Validation Error' },
  bookingIsNotValid: { en: 'Booking is not valid', de: 'Booking is not valid' },
  pleaseCheckTheHighlightedFields: {
    en: 'Please check the highlighted fields',
    de: 'Please check the highlighted fields'
  },
  theValueNeedsToBeTrue: {
    en: 'The value needs to be true',
    de: 'The value needs to be true'
  },
  theFieldIsRequired: { en: 'The field is required', de: 'Das Feld ist erforderlich' },
  invalidEmail: { en: 'Invalid Email', de: 'Ungültige E-Mail' },
  theValueIsNotValid: { en: 'The value is not valid', de: 'Der Wert ist ungültig' },
  onlyNumbersAllowed: { en: 'Only Numbers Allowed', de: 'Nur Zahlen erlaubt' },
  wholeNumbersAllowed: { en: 'Only whole numbers i.e., {0,1,2,...}', de: 'nur ganze Zahlen i.e., {0,1,2,...}' },
  numberIsWithinRange: {
    en: 'Not allowed to use a number within the reserved settings range: [{from}, {to}]',
    de: 'Not allowed to use a number within the reserved settings range: [{from}, {to}]'
  },
  invalidYear: { en: 'Invalid Year', de: 'Ungültiges Jahr' },
  invalidLatitude: {
    en: 'Must be between -90 to 90. Maximum of 6 places behind the decimal point.',
    de: 'Gültiger bereich ist -90 to 90. Maximal 6 Nachkommastellen.'
  },
  invalidLongitude: {
    en: 'Must be between -180 to 180. Maximum of 6 places behind the decimal point.',
    de: 'Gültiger bereich ist -180 to 180. Maximal 6 Nachkommastellen.'
  },
  requiredDateFormat: {
    en: 'The input is not a valid date. ( Required format: "DDMMYYYY")',
    de: 'The input is not a valid date. ( Required format: "DDMMYYYY")'
  },
  inputMustBeANumber: {
    en: 'Input must be a number',
    de: 'Die Eingabe muss eine Zahl sein'
  },
  theValueShouldBeGreaterThan: {
    en: 'The value should be greater than {subject}',
    de: 'Der Wert sollte größer als sein {subject} sein'
  },
  theValueShouldBeLessThan: {
    en: 'The value should be less than {subject}',
    de: 'Der Wert sollte kleiner als {subject} sein'
  },
  theValueShouldBeGreaterOrEqualThan: {
    en: 'The value should be greater or equal than {subject}',
    de: 'The value should be greater or equal than {subject}'
  },
  theValueShouldBeLessOrEqualThan: {
    en: 'Should not be greater than {subject}',
    de: 'Darf nicht größer sein als {subject}'
  },
  theValueShouldNotBeEqual: {
    en: 'The value should not be equal to {subject}',
    de: 'The value should not be equal to {subject}'
  },
  minLengthNeedsToBe: {
    en: 'Minimum length is {subject}',
    de: 'Minimum length is {subject}'
  },
  maxLengthIs: {
    en: 'Maximum length is {subject}',
    de: 'Maximum length is {subject}'
  },
  theLengthNeedsToBe: {
    en: 'The length needs to be {subject}',
    de: 'The length needs to be {subject}'
  },
  youNeedToCheckMinimumCheckbox: {
    en: 'You need to check minimum {subject} checkbox(es)',
    de: 'You need to check minimum {subject} checkbox(es)'
  },
  bookingAccountNotRelatedToMandator: {
    en: 'Account is not yet related to chosen mandator, create relation?',
    de: 'Konto ist noch nicht mit dem ausgewählten Mandanten verbunden, eine Beziehung erstellen?'
  },
  voucherTypeNotRelatedToMandator: {
    en: 'Voucher Type is not yet related to chosen mandator',
    de: 'Voucher Type ist noch nicht mit dem ausgewählten Mandanten verbunden'
  },
  bookingCodeNotRelatedToMandator: {
    en: 'Code is not yet related to chosen mandator, create relation?',
    de: 'Code ist noch nicht mit dem ausgewählten Mandanten verbunden, eine Beziehung erstellen?'
  },
  notRelatedToMandator: {
    en: 'The selected value is not related to the mandator, please add relation in mandators maindata',
    de: 'Der gewählte Wert ist dem Mandanten nicht zugewiesen, bitte führen Sie dies in den Stammdaten des Mandaten aus'
  },
  bankAccountNotRelatedToCompany: {
    en: 'Bank Account is not related to the company',
    de: 'Bank Account is not related to the company',
  },
  notYetRelated: {
    en: 'Not yet related',
    de: 'Noch nicht verwandt'
  },
  showNotRelatedAccounts: { en: 'Show Not Related Accounts', de: 'Show Not Related Accounts' },
  hideNotRelatedAccounts: { en: 'Hide Not Related Accounts', de: 'Hide Not Related Accounts' },
  rangeStart: { en: 'Range Start', de: 'Bereichsanfang' },
  rangeEnd: { en: 'Range End', de: 'Bereichsende' },
  warning: { en: 'Warning', de: 'Warnung' },
  invalidFields: { en: 'Invalid fields', de: 'Ungültige Felder' },
  date: { en: 'Date', de: 'Datum' },
  rate: { en: 'Rate', de: 'Rate' },
  rates: { en: 'Rates', de: 'Kurse' },
  company: { en: 'Company', de: 'Firma' },
  contacts: { en: 'Contacts', de: 'Contacts' },
  printPreview: { en: 'Print Preview', de: 'Druckvorschau' },
  allAddresses: { en: 'All Addresses', de: 'Alle Adressen' },
  companyName: { en: 'Company Name', de: 'Firmenname' },
  copyToClipboard: { en: 'Copy to clipboard', de: 'In Zwischenablage kopieren' },
  copiedToClipboard: { en: 'Copied to clipboard', de: 'Copied to clipboard' },
  function: { en: 'Function', de: 'Funktion' },
  notAllowed: { en: 'Not Allowed', de: 'Nicht erlaubt' },
  locked: { en: 'Locked', de: 'gesperret' },
  blocked: { en: 'Blocked', de: 'Blockiert' },
  year: { en: 'Year', de: 'Jahr' },
  period: { en: 'Period', de: 'Period' },
  cantAddMore: { en: `Can't add more`, de: 'Kann nicht mehr hinzufügen' },
  invalidInput: { en: 'Invalid Input', de: 'Ungültige Eingabe' },
  maxDecimalDigits: { en: 'Maximum decimal digits: {DIGITS}', de: 'Maximale Nachkommastellen: {DIGITS}' },
  invalidDate: { en: 'Invalid Date', de: 'Ungültiges Datum' },
  days: { en: 'Days', de: 'Tage' },
  showCompanyAndBankDetails: { en: 'show company and bank details', de: 'Firmen- und Bankdaten anzeigen' },
  invalidNumber: { en: 'Invalid Number', de: 'Ungültige Nummer' },
  code: { en: 'Code', de: 'Code' },
  group: { en: 'Group', de: 'Gruppe' },
  labelEnglish: { en: 'Description English', de: 'Bezeichnung Englisch' },
  labelGerman: { en: 'Description German', de: 'Bezeichnung Deutsch' },
  definition: { en: 'Definition', de: 'Definition' },
  refreshList: { en: 'Refresh List', de: 'Refresh List' },
  markFavorite: { en: 'Mark/Unmark Favorite', de: 'Mark/Unmark Favorite' },
  filterFavorites: { en: 'Filter Favorites', de: 'Filter Favorites' },
  getsGenerated: { en: 'Gets generated', de: 'Gets generated' },
  amount: { en: 'Amount', de: 'Betrag' },
  addNewRow: { en: 'Add new row', de: 'Add new row' },
  clear: { en: 'Clear', de: 'Löschen' },
  search: { en: 'Search', de: 'Suchen' },
  dateShouldBeAfterBefore: {
    en: 'Date should be after {afterDate} and before {beforeDate}',
    de: 'Rechnungsdatum muß nach dem {afterDate} und vor dem {beforeDate} liegen',
  },
  dateShouldBeGreater: {
    en: 'The date should be greater than {afterDate}',
    de: 'Das Datum muss größer als {afterDate} sein.',
  },
  dateShouldBeSmaller: {
    en: 'Date should be smaller than {beforeDate}',
    de: 'Date should be smaller than {beforeDate}',
  },
  dateShouldBeGreaterOrEqual: {
    en: 'Date should be greater or equal than {afterDate}',
    de: 'Date should be greater or equal than {afterDate}',
  },
  dateShouldBeSmallerOrEqual: {
    en: 'The date should be smaller or equal than {beforeDate}',
    de: 'The date should be smaller or equalt than {beforeDate}',
  },
  midrateCalculation: { en: 'Midrate Calculation', de: 'Mittelkursberechnung' },
  history: { en: 'History', de: 'Verlauf' },
  copyExcel: { en: 'Copy/Excel', de: 'Copy/Excel' },
  ok: { en: 'OK', de: 'OK' },
  discard: { en: 'Discard', de: 'Verwerfen' },
  midrate: { en: 'Midrate', de: 'Midrate' },
  copyValue: { en: 'Copy Value', de: 'Copy Value' },
  notFound: { en: 'Page Not Found', de: 'Seite nicht gefunden' },
  notFoundMessage: {
    en: `The page that you requested is not found.
      Maybe the link you clicked is broken or the page has been removed.`,
    de: `Die angeforderte Seite konnte nicht gefunden werden.
      Vielleicht ist der Link fehlerhaft oder die Seite wurde entfernt.
    `
  },
  localCurrencyCannotBeEqualToForeignCurrency: {
    en: 'Local Currency Cannot Be Equal To Foreign Currency',
    de: 'Local Currency Cannot Be Equal To Foreign Currency'
  },
  favorite: { en: 'Favorite', de: 'Favorite' },
  openInNewTab: {
    en: 'open in new tab',
    de: 'in neuem tab öffnen'
  },
  openInNewWindow: {
    en: 'open in new window',
    de: 'in neuem Fenster öffnen'
  },
  source: { en: 'Source', de: 'Source' },
  importDate: { en: 'Import Date', de: 'Import Date' },
  rateDate: { en: 'Rate date', de: 'Rate date' },
  rangeCannotBeGreaterThanAYear: {
    en: 'Range Cannot Be Greater Than 365 days',
    de: 'Range Cannot Be Greater Than 365 days'
  },
  inputSearchText: { en: 'Input search text', de: 'Suchtext eingeben' },
  nameEn: { en: 'Name EN', de: 'Name EN' },
  nameDe: { en: 'Name DE', de: 'Name DE' },
  nameEnglish: { en: 'Name English', de: 'Name English' },
  nameGerman: { en: 'Name German', de: 'Name German' },
  roleRightsGruppeRechte: { en: 'Role rights', de: 'Gruppe-Rechte' },
  assignedRights: { en: 'Assigned rights', de: 'Zugewiesene Rechte' },
  assignedZonePermissions: {
    en: 'Assigned Zone-Permissions',
    de: 'Zugewiesene Zonen-Rechte'
  },
  theme: { en: 'Account Theme', de: 'Account Theme' },
  dark: { en: 'Dark', de: 'Dark' },
  light: { en: 'Light', de: 'Hell' },
  auto: { en: 'Automatic', de: 'Automatisch' },
  apply: { en: 'Apply', de: 'Apply' },
  roles: { en: 'Roles', de: 'Rollen' },
  requestNewPassword: { en: 'Request a new password', de: 'Neues Password anfordern' },
  requestIsGoingToBeSent: {
    en: 'A password change request will be sent to {EMAIL}',
    de: 'Eine Passwortänderungsanforderung wird an {EMAIL} gesendet'
  },
  requestSent: {
    en: 'The request has been sent to {EMAIL}.',
    de: 'Die Anforderung wurde an {EMAIL} gesendet.'
  },
  sent: { en: 'Sent', de: 'Geschickt' },
  newDocument: { en: 'Create Document', de: 'Erstelle Dokument' },
  newDocumentRequest: {
    en: 'You will be notified when the requested document is ready. Please check your messages.',
    de: 'Sie werden benachrichtigt wenn das angeforderte Dokument erstellt ist. Bitte prüfen Sie Ihre Nachrichten.'
  },
  error: { en: 'Error', de: 'Fehler' },
  noPermission: {
    en: 'You do not have sufficient permissions to perform this operation',
    de: 'Sie verfügen nicht über ausreichende Berechtigungen, um diesen Vorgang auszuführen'
  },
  somethingWentWrong: {
    en: 'Something went wrong',
    de: 'Etwas ist schief gelaufen'
  },
  success: {
    en: 'Success',
    de: 'Erfolgreich'
  },
  createdSuccess: {
    en: 'Created successfully',
    de: 'Erfolgreich erstellt'
  },
  updatedSuccess: {
    en: 'Updated successfully',
    de: 'Erfolgreich aktualisiert'
  },
  deletedSuccess: {
    en: 'Deleted successfully',
    de: 'Erfolgreich entfernt'
  },
  connectionLost: {
    en: 'Connection Lost',
    de: 'Connection Lost',
  },
  connectionRetrieved: {
    en: 'Connection retrieved',
    de: 'Connection retrieved',
  },
  online: {
    en: 'Online',
    de: 'Online'
  },
  offline: {
    en: 'Offline',
    de: 'Offline',
  },
  dateShouldBeInThePast: {
    en: 'Date should be in the past',
    de: 'Datum sollte in der Vergangenheit liegen',
  },
  noData: { en: 'No data', de: 'Keine daten' },
  about: { en: 'About', de: 'About' },
  commitHash: { en: 'Commit Hash', de: 'Commit Hash' },
  tag: { en: 'Tag', de: 'Tag' },
  view: { en: 'View', de: 'View' },
  totalReports: { en: 'Total Reports', de: 'Total Reports' },
  clearFilters: { en: 'Clear Filters', de: 'Clear Filters' },
  basic: { en: 'Basic', de: 'Allgemein' },
  selectAll: { en: 'Select all', de: 'Alle Schiffe' },
  clearAll: { en: 'Clear all', de: 'Keine Schiffe' },
  filter: { en: 'Filter', de: 'Filter' },
  notActiveWarning: {
    en: '{subject} is not active. Please use another or discard your changes.',
    de: '{subject} ist nicht mehr aktiv. Bitte wählen Sie eine Alternative oder verwerfen Sie Ihre Änderungen.'
  },
  invalidDiscountValue: {
    en: 'Discount value does not exist in payment conditions',
    de: 'Discount value does not exist in payment conditions',
  },
  unsavedChanges: {
    en: 'Unsaved changes',
    de: 'Nicht gespeicherte Änderungen'
  },
  deleted: { en: 'Deleted', de: 'Gelöscht' },
  // TODO: add German translations
  cannotEditDeletedItem: {
    en: 'Deleted invoice cannot be changed',
    de: 'Deleted invoice cannot be changed'
  },
  deleteReason: {
    en: 'Deletion reason',
    de: 'Stornierungsgrund'
  },
  openItemIsAlreadyFalse: {
    en: 'Open Item is already false',
    de: 'Open Item is already false',
  },
  deleteAssignmentOpenItem: {
    en: 'Are you sure to remove the assignment as an open item account?',
    de: 'Wollen Sie dieses Konto tatsächlich nicht mehr als OP-Konto definieren?'
  },
  remove: {
    en: 'Remove',
    de: 'Entfernen'
  },
  deleteAll: {
    en: 'Delete All',
    de: 'Alle löschen',
  },
  downloadAsExcel: {
    en: 'Download as Excel',
    de: 'Als Excel herunterladen',
  },
  excel: {
    en: 'Excel',
    de: 'Excel',
  },
  noResults: {
    en: 'No Results',
    de: 'Keine Treffer'
  },
  domain: { en: 'Domain', de: 'Domain' },
  allowedUserEmailDomains: {
    en: 'Allowed User Email Domains',
    de: 'Zugelassene Benutzer Email Domänen',
  },
  valueAlreadyExists: {
    en: 'Value already exists',
    de: 'Value already exists',
  },
  downloadAsPdf: {
    en: 'Download as PDF',
    de: 'PDF Download'
  },
  pdf: {
    en: 'PDF',
    de: 'PDF'
  },
  entity: {
    en: 'Entity',
    de: 'Entität'
  },
  subEntity: {
    en: 'Sub-Entity',
    de: 'Sub-Entität'
  },
  actionType: {
    en: 'Action',
    de: 'Aktion'
  },
  member: {
    en: 'Property',
    de: 'Eigenschaft'
  },
  oldValue: {
    en: 'Old Value',
    de: 'Alter Wert'
  },
  newValue: {
    en: 'New Value',
    de: 'Neuer Wert'
  },
  createDate: { en: 'Create date', de: 'Erstellungsdatum' },
  createdBy: { en: 'Created by', de: 'Erstellt von' },
  createdOn: { en: 'Created on', de: 'Erstellt am' },
  editDate: { en: 'Edit date', de: 'Bearbeitungsdatum' },
  editedBy: { en: 'Edited by', de: 'Bearbeitet von' },
  on: { en: 'on', de: 'am' },
  changeDate: { en: 'Change date', de: 'Änderungsdatum' },
  changedBy: { en: 'Changed by', de: 'Geändert von' },
  maintenanceMode: {
    en: 'Maintenance Mode',
    de: 'Maintenance Mode'
  },
  maintenanceModeMessage: {
    en: 'Website is in maintenance mode. Please try again later.',
    de: 'Website befindet sich im Wartungsmodus, bitte versuchen Sie es später.'
  },
  message: {
    en: 'Message',
    de: 'Nachricht'
  },
  startDate: {
    en: 'Start Date',
    de: 'Startdatum'
  },
  estimatedEndTime: {
    en: 'Estimated End Time',
    de: 'Voraussichtlicher Endzeitpunkt'
  },
  scheduleMaintenance: {
    en: 'Schedule Maintenance',
    de: 'Schedule Maintenance'
  },
  areYouSureYouWantToDeactivateSchedule: {
    en: 'Are you sure you want to deactivate this schedule ?',
    de: 'Are you sure you want to deactivate this schedule ?'
  },
  stop: { en: 'Stop', de: 'Halt' },
  deactivated: { en: 'Deactivated', de: 'Deactivated' },
  actualEndTime: {
    en: 'Actual End Time',
    de: 'Tatsächlicher Endzeitpunkt',
  },
  dateShouldBeInFuture: {
    en: 'Date should be in the future',
    de: 'Date should be in the future',
  },
  relate: {
    en: 'Relate',
    de: 'Verknüpfen'
  },
  saveAndDelete: {
    en: 'Save and Delete',
    de: 'Speichern und Löschen'
  },
  number: {
    en: 'Number',
    de: 'Nummer',
  },
  type: {
    en: 'Type',
    de: 'Type',
  },
  newEntry: {
    en: 'New Entry',
    de: 'Neuer Eintrag',
  },
  passwordPolicies: {
    en: 'Length min. 12 characters. Must not contain the first or last name of the user. Do not use words from the dictionary. Use at least 3 of the 4 character types (upper case, lower case, numbers, special characters)',
    de: 'Länge min. 12 Zeichen. Darf nicht Ihren Vor- oder Nachnamen enthalten. Verzichten Sie auf bekannte Wörter. Verwendung von min. 3 der 4 Zeichenarten (Großbuchstaben, Kleinbuchstaben, Zahlen, Sonderzeichen)',
  },
  dataMissing: {
    en: 'Data is missing',
    de: 'Fehlende Daten',
  },
  checkNow: { en: 'Check now', de: 'Jetzt prüfen' },
  valid: { en: 'Valid', de: 'Gültig' },
  invalid: { en: 'Invalid', de: 'Ungültig' },
  flag: { en: 'Flag', de: 'Flagge' },
  sitemapDescription: { en: 'Description...', de: 'Beschreibung...' },
  sitemapMisc: { en: 'Misc', de: 'Misc' },
  service: { en: 'Service', de: 'Service' },
  selectService: { en: 'Select Service', de: 'Select Service' },
  relations: { en: 'Relations', de: 'Verknüpfungen' },
  useAsTemplate: { en: 'use as template', de: 'Vorlage zur Neuanlage' },
  clonedEntityNotFound: { en: 'Cloned entity not found', de: 'Kopierte Entität nicht gefunden' },
  all: { en: 'All', de: 'Alle' },
  value1MustBeGreaterThanValue2: {
    en: '{VALUE_1} must be greater than {VALUE_2}',
    de: '{VALUE_1} must be greater than {VALUE_2}'
  },
  value1MustBeGreaterOrEqualThanValue2: {
    en: '{VALUE_1} must be greater or equal than {VALUE_2}',
    de: '{VALUE_1} must be greater or equal than {VALUE_2}'
  },
  onlyNumbersAndLetters: {
    en: 'Only Numbers and Letters Allowed',
    de: 'Only Numbers and Letters Allowed'
  },
  chooseDate: { en: 'Choose Date', de: 'Datum wählen' },
  companyGroupIsActive: {
    en: 'A company group needs to be set to inactive first in order to be able to delete this company group.',
    de: 'Eine Konzerngruppe muss zuerst auf inaktiv gesetzt werden, um sie endgültig zu löschen.'
  },
  companyGroupRelationsWillBeDeleted: {
    en: 'You have set this company group to inactive. The connection between this company group and all linked companies will be deleted. Do you really want to save your changes and set this company group to inactive?',
    de: 'Sie haben die Konzerngruppe deaktiviert. Die Verbindung zwischen dieser Konzerngruppe und allen verknüpften Geschäftsbeziehungen wird entfernt. Wollen Sie Ihre Änderungen speichern und die Konzerngruppe auf inaktiv setzen?'
  },
  showCompany: { en: 'Show Company', de: 'Gesellschaft anzeigen' },
  privacyPolicy: { en: 'Privacy Policy', de: 'Datenschutzrichtlinie' },
  noLabel: { en: '\u200B', de: '\u200B' },
  refresh : { en: 'Refresh', de: 'Aktualisieren' },
  inactive: { en: 'Inactive', de : 'Inaktiv' },
  checklistItems: { en: 'Checklist items', de : 'Checklisten Positionen' },
  started: { en: 'Started', de : 'Begonnen' },
  color: { en: 'Color', de: 'Farbe' },
  assign: { en: 'Assign', de: 'Assign' },
  areYouSureYouWantToAssignChecklistToTaskTemplate: {
    en: 'Are you sure you want to assign checklist *{checklistLabel}* to *{taskTemplateLabel}* task template?',
    de: 'Are you sure you want to assign checklist *{checklistLabel}* to *{taskTemplateLabel}* task template?',
  },
  cannotModifySystemDefaultItems: {
    en: 'Cannot modify System Default items',
    de: 'Cannot modify System Default items'
  },
  selectVesselType: { en: 'Select Vessel Type', de: 'Select Vessel Type' },
  // TODO: add german translations
  applyTaskTemplate: { en: 'Apply task template', de: 'Apply task template' },
  enDe: { en: 'EN / DE', de: 'EN / DE' },
  taskNotEnabledForChosenLanguage: {
    en: 'Task and its items not enabled for chosen language',
    de: 'Für Ihre Spracheinstellung liegt kein Name der Aufgabe und Tätigkeiten vor',
  },
  language: { en: 'Language', de: 'Sprache' },

};
