import { ArrayResponseI } from './../../core/interfaces/array-response.interface';
import { EntitiesUpdateI } from '@core-interfaces/entities-update.interface';
import { KeyValueI } from '@core-interfaces/key-value.interface';

import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { Component, Injector, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, ValidationErrors } from '@angular/forms';
import { MatHorizontalStepper, MatStep } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { DefinitionsStoreService } from '@core-services/definitions-store.service';

import { BaseLoggerComponent } from '@features/tracking-system';
import { ME_DOMAIN_APPLICATION_ADDRESS, ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL } from '@shared/constants';
import { MeAttributeLayoutTypes } from '@shared/me-attribute-layout-types';
import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';
import { MeBasicAlertI } from '@shared/me-components/me-basic-alert/me-basic-alert.interface';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeButtonI } from '@shared/me-components/me-button/me-button.interface';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { MeEmptyContentI } from '@shared/me-components/me-empty-content/me-empty-content-interface';

import { AttributeModelTypes, DMChoiceAttributeModel } from '@shared/models/attribute/attribute.model';
import { DomainModel } from '@shared/models/domain/domain.model';
import { LayoutGroupModel } from '@shared/models/layout/layout-group.model';
import { DigitalProfileModel } from '@shared/models/digital-profile/digital-profile.model';
import { LayoutModel } from '@shared/models/layout/layout.model';

import { DigitalProfileStoreService } from '@features/digital-profile/digital-profile-store.service';
import { DigitalProfileWebService } from '@features/digital-profile/digital-profile.web-service';
// import { DocumentCreateWebService } from '@features/document-create-edit/document-create-edit.web-service';

import { InputAttributeService } from '@shared/services/input-attribute.service';

import { DigitalProfileEditWebService } from './digital-profile-edit.web-service';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';
import { RouterStoreService } from '@core-services/router-store.service';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-digital-profile-edit',
  templateUrl: './digital-profile-edit.component.html',
  styleUrls: ['./digital-profile-edit.component.scss'],
  providers: [InputAttributeService, DigitalProfileStoreService, DigitalProfileEditWebService]
})
export class DigitalProfileEditComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  componentVersion: string = '0.0.1';
  certificateId: string | null = null;
  leaveWarningModel!: MeSweetAlertI;
  applyCheck: boolean = false;

  digitalProfile: DigitalProfileModel[] = [];
  isFirstCertificate: boolean = true;
  emptyCompanyContent!: MeEmptyContentI;

  company!: DigitalProfileModel;
  address!: DigitalProfileModel;
  extendedAttributes: AttributeModelTypes[] = []; // array of material specific attribute objects (tab 3)

  buttonCancel!: { label: string; data: MeButtonI };
  buttonSubmit!: { label: string; data: MeButtonI };
  buttonNext!: { label: string; data: MeButtonI };
  buttonAddLocation!: { label: string; data: MeButtonI };
  buttonAddFirstCertificate!: { label: string; data: MeButtonI };

  @ViewChild('stepper') stepper!: MatHorizontalStepper;

  layouts: LayoutModel[] = [];
  groups: LayoutGroupModel[] = [];
  attributes: AttributeModelTypes[] = [];

  // For 1st tab
  companydomain: DomainModel | undefined;
  companylayouts: LayoutModel[] = [];
  companyAttributes: AttributeModelTypes[] = [];
  stepOneGroups: string[] = ['companyOverview', 'businessInformation', 'address', 'contactInformation'];
  companyInfoLayoutGroups!: LayoutGroupModel[];
  companyInfoAttributeForm!: FormGroup;
  comapanyInfoAttributeItemMap!: Map<string, MeAttributeItem>;

  addresslayouts: LayoutModel[] = [];
  addressAttributes: AttributeModelTypes[] = [];

  materialTypesAttribute: DMChoiceAttributeModel = new DMChoiceAttributeModel();

  private _definitionStoreDataLoadedSubscription!: Subscription;
  private _getI18nStringsSubscription!: Subscription;
  private _updateCompanySubscription!: Subscription;
  private _getCompanySubscription!: Subscription;

  private isChangeInForm: boolean = false;
  private _takeUntilDestroy$: Subject<void> = new Subject();

  i18nStrings: MeTranslationI = {};

  constructor(
    injector: Injector,
    private router: Router,
    private route: ActivatedRoute,
    private translate: MeTranslationService,
    private definitionsStoreService: DefinitionsStoreService,
    public inputAttributeService: InputAttributeService,
    private meBasicAlertService: MeBasicAlertService,
    private webService: DigitalProfileWebService,
    private webeditService: DigitalProfileEditWebService,
    private digitalProfileStoreService: DigitalProfileStoreService,
    private meSweetAlertService: MeSweetAlertService,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private routerStoreService: RouterStoreService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.certificateId = this.route.snapshot.paramMap.get('certificateId');

    if (this.digitalProfileStoreService.digitalprofile.length > 0) {
      this.digitalProfile = this.digitalProfileStoreService.digitalprofile;
      this.initializeEdit();
    } else {
      this.getdigitalProfile();
    }

    this.getTraslation();

    this.emptyCompanyContent = {
      title: this.i18nStrings.addYourFirstCertificate,
      comment: this.i18nStrings.addYourFirstCertificateDescription,
      buttonText: this.i18nStrings.add,
      emptyIconName: 'empty-certificates'
    };

    this.leaveWarningModel = {
      icon: 'alert-triangle',
      mode: 'warning',
      title: 'Warning',
      message: 'You are leaving page, data will be lost!',
      type: { name: MeSweetAlertTypeEnum.submit, buttons: { submit: 'Leave', cancel: 'Stay on page' } }
    };

    this.breadcrumbSupportService.breadcrumbActions = [
      {
        button: {
          id: 'cancelButton',
          data: { color: 'secondary', type: 'regular' },
          text: this.i18nStrings.cancel
        },
        callBack: () => {
          const lastRoute = this.routerStoreService.getPreviousUrl();
          if (lastRoute === decodeURI(this.router.routerState.snapshot.url)) {
            this.backclick();
          } else {
            this.router.navigate([lastRoute]);
          }
        }
      }
    ];
  }

  getTraslation(): void {
    this._getI18nStringsSubscription = this.translate.getTranslations().subscribe((translations) => {
      this.i18nStrings = translations;
      this.prepareButtons();
    });
  }

  getdigitalProfile(): void {
    this._getCompanySubscription = this.webService.getCompnay().subscribe((val: ArrayResponseI<DigitalProfileModel>) => {
      this.digitalProfile = val.entities;
      this.digitalProfileStoreService.digitalprofile = val.entities;
      this.initializeEdit();
    });
  }

  prepareButtons(): void {
    this.buttonNext = { label: this.i18nStrings.next, data: { type: 'regular', color: 'primary' } };
    this.buttonCancel = { label: this.i18nStrings.cancel, data: { type: 'regular', color: 'secondary' } };
    this.buttonSubmit = { label: this.i18nStrings.saveChanges, data: { type: 'regular', color: 'primary' } };
    this.buttonAddLocation = { label: this.i18nStrings.addLocation, data: { type: 'transparent', color: 'primary' } };
    this.buttonAddFirstCertificate = { label: this.i18nStrings.add, data: { type: 'regular', color: 'secondary' } };
    // this.buttonFinish = { label: this.i18nStrings.finish, data: { type: 'regular', color: 'primary' } };
  }

  initializeEdit(): void {
    const companydp = this.digitalProfileStoreService.getDigitalProfile(ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL);
    if (companydp) {
      this.company = companydp;
    }

    const addressdp = this.digitalProfileStoreService.getDigitalProfile(ME_DOMAIN_APPLICATION_ADDRESS);
    if (addressdp) {
      this.address = addressdp;
    }

    this._definitionStoreDataLoadedSubscription = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.companydomain = this.definitionsStoreService.getDomain(
          this.company.domainName || ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL
        );
        const addressdomain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_ADDRESS);

        let companygroups: LayoutGroupModel[] = [];
        let addressgroups: LayoutGroupModel[] = [];

        if (this.companydomain) {
          this.companyAttributes = this.inputAttributeService.transformAttributesArray(this.companydomain.attributes);
          this.companylayouts = this.companydomain.layouts;
          companygroups = this.inputAttributeService.getLayoutGroups(this.companydomain.layouts, MeAttributeLayoutTypes.UPDATE);
        }
        if (addressdomain) {
          this.addressAttributes = this.inputAttributeService.transformAttributesArray(addressdomain.attributes);
          this.addresslayouts = addressdomain.layouts;
          addressgroups = this.inputAttributeService.getLayoutGroups(addressdomain.layouts, MeAttributeLayoutTypes.UPDATE);
        }
        this.attributes = this.companyAttributes.concat(this.addressAttributes);
        this.layouts = this.companylayouts.concat(this.addresslayouts);

        if (this.company) {
          this.inputAttributeService.applyUpdateFormValues(this.attributes, this.company);
        }
        if (this.address) {
          this.inputAttributeService.applyUpdateFormValues(this.attributes, this.address);
        }

        this.groups = companygroups.concat(addressgroups);
        this.groups = this.inputAttributeService.generateLayoutRows(this.groups);

        this.companyInfoLayoutGroups = this.getLayoutForGroups(this.groups, this.stepOneGroups);
        if (this.companyInfoLayoutGroups && this.companyInfoLayoutGroups.length > 0) {
          this.companyInfoAttributeForm = this.inputAttributeService.createFormGroup(
            this.getAttributesByLayoutGroups(this.attributes, this.companyInfoLayoutGroups)
          );

          this.comapanyInfoAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
            this.getAttributesByLayoutGroups(this.attributes, this.companyInfoLayoutGroups),
            this.companyInfoAttributeForm
          );
        }

        this.companyInfoAttributeForm.valueChanges.pipe(takeUntil(this._takeUntilDestroy$)).subscribe(() => {
          if (this.companyInfoAttributeForm.dirty) {
            this.applyCheck = true;
          }
        });
      }
    });
  }

  checkStepBeforeSwitch(switchedTo: number): void {
    const currentStep: MatStep = this.stepper.steps.toArray()[this.stepper.selectedIndex];
    if (!currentStep.completed && currentStep.stepControl) {
      if (this.stepper.selectedIndex !== switchedTo) {
        currentStep.stepControl.markAllAsTouched();
      }
      if (this.stepper.selectedIndex < switchedTo) {
        const emptyRequiredAttributes: AttributeModelTypes[] = [];
        const currentStepControl: FormGroup = currentStep.stepControl as FormGroup;

        Object.keys(currentStepControl.controls).forEach((controlName) => {
          const validationErrors: ValidationErrors | null = currentStepControl.controls[controlName].errors;
          if (validationErrors && validationErrors.required) {
            const attribute = [...this.attributes, ...this.extendedAttributes, this.materialTypesAttribute].find(
              (attr) => attr.internalName === controlName
            );
            if (attribute) {
              emptyRequiredAttributes.push(attribute);
            }
          }
        });

        if (emptyRequiredAttributes.length > 0) {
          const basicAlertData: MeBasicAlertI = {
            mode: 'error',
            title: this.i18nStrings.insertRequiredData,
            content: this.i18nStrings.requiredFields
          };
          emptyRequiredAttributes.forEach((attribute, index) => {
            basicAlertData.content += ` ${attribute.displayName}`;
            basicAlertData.content += emptyRequiredAttributes.length - 1 !== index ? ',' : '';
          });
          this.meBasicAlertService.openBasicAlert(basicAlertData);
        }
      }
    }
  }

  updateCompany(form: FormGroup): void {
    form.markAllAsTouched();
    if (form.valid) {
      const companyChanges: EntitiesUpdateI = {
        entities: []
      };

      const companyentity = {
        domainName: this.company ? this.company.domainName : ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL,
        attributes: new FormData()
      };
      this.setAttributesToentity(companyentity.attributes, this.companyAttributes, ['name']);

      const addressentity = {
        domainName: this.address ? this.address.domainName : ME_DOMAIN_APPLICATION_ADDRESS,
        attributes: new FormData()
      };
      this.setAttributesToentity(addressentity.attributes, this.addressAttributes);

      companyChanges.entities.push(companyentity);
      companyChanges.entities.push(addressentity);

      if (!this.isChangeInForm) {
        this.applyCheck = false;
        this.backclick();
        return;
      }

      this._updateCompanySubscription = this.webeditService.updateCompany(companyChanges).subscribe(() => {
        this.applyCheck = false;

        const successSubscription = this.meSweetAlertService.getDataBackFromSweetAlert().subscribe(() => {
          form.markAsUntouched();
          successSubscription.unsubscribe();
          this.backclick();
        });

        const successAlertModel: MeSweetAlertI = {
          mode: 'success',
          icon: 'check',
          type: {
            name: MeSweetAlertTypeEnum.confirm,
            buttons: {
              confirm: this.i18nStrings.continue
            }
          },
          title: this.i18nStrings.success,
          message: this.i18nStrings.digitalProfileUpdated
        };
        this.meSweetAlertService.openMeSweetAlert(successAlertModel);
      });
    }
  }

  backclick(): void {
    this.navigateToView();
  }

  // On tab index change set previous step form as touched
  markFormAsTouched(changeObj: StepperSelectionEvent): void {
    if (changeObj.previouslySelectedStep.stepControl) {
      changeObj.previouslySelectedStep.stepControl.markAllAsTouched();
    }
  }

  private setAttributesToentity(entity: FormData, formattributes: AttributeModelTypes[], imutablecolumn?: string[]): void {
    this.getAttributesByLayoutGroups(this.attributes, this.companyInfoLayoutGroups).forEach((attribute) => {
      this.setAttributeWithValueToEntity(entity, attribute, this.companyInfoAttributeForm, formattributes, imutablecolumn);
    });
  }

  private setAttributeWithValueToEntity(
    // tslint:disable-next-line:no-any
    entity: KeyValueI<any>,
    attribute: AttributeModelTypes,
    formGroup: FormGroup,
    attr: AttributeModelTypes[],
    imutablecolumn?: string[]
  ): void {
    if (attr.indexOf(attribute) > -1) {
      const hasImmutable = imutablecolumn ? imutablecolumn.filter((x) => x === attribute.internalName).length > 0 : false;
      if (!hasImmutable) {
        if (!this.isChangeInForm) {
          this.checkIfAnyChangeinForm(attribute, formGroup);
        }
        if (
          formGroup.controls.hasOwnProperty(attribute.internalName) &&
          formGroup.controls[attribute.internalName].hasOwnProperty('value') &&
          formGroup.controls[attribute.internalName].value !== null
        ) {
          entity[attribute.internalName] = formGroup.controls[attribute.internalName].value;
        }
      }
    }
  }

  private getAttributesByLayoutGroups(attributes: AttributeModelTypes[], layoutGroups: LayoutGroupModel[]): AttributeModelTypes[] {
    const attributeMap: Map<string, AttributeModelTypes> = new Map();
    attributes.forEach((attribute) => {
      attributeMap.set(attribute.internalName, attribute);
    });

    return layoutGroups.reduce((prev, curr) => {
      return [
        ...prev,
        ...curr.attributes.map((attr) => {
          return attributeMap.get(attr.internalName);
        })
      ];
      // tslint:disable-next-line:no-any
    }, [] as any);
  }

  getLayoutForGroups(groups: LayoutGroupModel[], layoutGroupNames: string[]): LayoutGroupModel[] {
    const layoutGroups = groups.filter((group) => layoutGroupNames.findIndex((name) => group.internalName === name) > -1);
    return layoutGroups;
  }

  private checkIfAnyChangeinForm(attribute: AttributeModelTypes, formGroup: FormGroup): void {
    if (
      (formGroup.controls[attribute.internalName].value || attribute.value) &&
      formGroup.controls[attribute.internalName].value !== attribute.value
    ) {
      this.isChangeInForm = true;
    }
  }

  private navigateToView(): void {
    this.router.navigate([`digital-profile/view`]);
  }

  ngOnDestroy(): object {
    if (this._definitionStoreDataLoadedSubscription) {
      this._definitionStoreDataLoadedSubscription.unsubscribe();
    }
    if (this._getI18nStringsSubscription) {
      this._getI18nStringsSubscription.unsubscribe();
    }
    if (this._updateCompanySubscription) {
      this._updateCompanySubscription.unsubscribe();
    }
    if (this._getCompanySubscription) {
      this._getCompanySubscription.unsubscribe();
    }
    return super.ngOnDestroy();
  }
}
