import { NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { IResource } from 'src/app/core/models/resources/IResource';
import { IAutobankBusinessCase } from '../../models/resources/IAutobankBusinessCase';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'autobankBusinessCasesGridMain',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'gvc',
        title: wording.accounting.gvc,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wording.general.name,
        width: 350,
        isSortable: true,
        filter: {
          values: []
        }
      }
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editAutobankBusinessCase',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (entity: IAutobankBusinessCase) => actions.edit(entity),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteAutobankBusinessCase',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.UPDATE),
        func: (entity: IAutobankBusinessCase) => actions.delete(entity),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: () => actions.add()
      }
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
