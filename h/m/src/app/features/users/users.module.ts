import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';

import { MeAddListCardModule } from '@shared-components/me-add-list-card/me-add-list-card.module';
import { MeComponentLoadingModule } from '@shared-directives/me-component-loader/me-component-loader.module';
import { MeEmptyContentModule } from '@shared-components/me-empty-content/me-empty-content.module';
import { MeIconsProviderModule } from '@shared-modules/me-icons.module';
import { MePopupAddListModule } from '@shared-components/me-popup-add-list/me-popup-add-list.module';
import { MePopupBasicInputModule } from '@shared-components/me-popup-basic-input/me-popup-basic-input.module';
import { MePopupUserInfoModule } from '@shared/me-components/me-popup-user-info/me-popup-user-info.module';
import { MeScrollToBottomModule } from '@shared/me-directives/me-scroll-to-bottom.module';
import { MeSearchBarModule } from '@shared-components/me-search-bar/me-search-bar.module';
import { MeSearchInputModule } from '@shared-components/me-search-input/me-search-input.module';
import { MeUserGroupCardModule } from '@shared-components/me-user-group-card/me-user-group-card.module';
import { MeIntroductionMessageModule } from '@shared/me-components/me-introduction-message/me-introduction-message.module';

@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    MeAddListCardModule,
    MeComponentLoadingModule,
    MeEmptyContentModule,
    MeIconsProviderModule,
    MePopupAddListModule,
    MePopupBasicInputModule,
    MePopupUserInfoModule,
    MeScrollToBottomModule,
    MeSearchBarModule,
    MeSearchInputModule,
    MeUserGroupCardModule,
    UsersRoutingModule,
    MeIntroductionMessageModule
  ]
})
export class UsersModule {}
