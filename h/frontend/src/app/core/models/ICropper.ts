export interface ICropper {
  imageFile: File;
  mainX?: number;
  mainY?: number;
  lastXCoordinate?: number;
  lastYCoordinate?: number;
  scale?: number;
  frameLength?: number;
  frameOutLength?: number;
  canvasWidth?: number;
  canvasHeight?: number;
  zoomInputText?: string;
  zoomType?: string;
  zoomMin?: number;
  zoomMax?: number;
  zoomStep?: number;
  cropButtonText?: string;
  fillStyle?: string;
}
