import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MeSearchBarModule } from '@shared/me-components/me-search-bar/me-search-bar.module';
import { MeComponentLoadingModule } from '@shared/me-directives/me-component-loader/me-component-loader.module';
import { MeDocumentCardModule } from '@shared/me-components/me-document-card/me-document-card.module';
import { MeTabGroupModuel } from '@shared/me-components/me-tab-group/me-tab-group.module';
import { MeInfoCardModule } from '@shared/me-components/me-info-card/me-info-card.module';
import { MeScrollToBottomModule } from '@shared/me-directives/me-scroll-to-bottom.module';
import { MeEmptyContentModule } from '@shared/me-components/me-empty-content/me-empty-content.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MeIntroductionMessageModule } from '@shared/me-components/me-introduction-message/me-introduction-message.module';
import { MePopupVideoModule } from '@shared/me-components/me-popup-video/me-popup-video.module';

import { DocumentsRoutingModule } from './documents-routing.module';
import { DocumentsComponent } from './documents.component';

@NgModule({
  declarations: [DocumentsComponent],
  imports: [
    CommonModule,
    DocumentsRoutingModule,
    MeSearchBarModule,
    MeDocumentCardModule,
    MeComponentLoadingModule,
    IconsModule,
    MeEmptyContentModule,
    MeTabGroupModuel,
    MeInfoCardModule,
    MeScrollToBottomModule,
    MeIntroductionMessageModule,
    MePopupVideoModule
  ]
})
export class DocumentsModule {}
