import { Component, OnInit } from '@angular/core';
import { getAutobankGridConfig } from 'src/app/core/constants/nv-grid-configs/autobank-rules.config';
import { AutobankRuleKey, IAutobankRule } from 'src/app/core/models/resources/IAutobankRule';
import { Router } from '@angular/router';
import { NvGridConfig } from 'nv-grid';
import { IWording } from 'src/app/core/models/resources/IWording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { AutobankRuleRepositoryService } from 'src/app/core/services/repositories/autobank-rule-repository.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { forkJoin, Observable } from 'rxjs';
import { SelectType } from 'src/app/core/models/SelectType';
import { IMandatorLookupRow, mandatorNoModifier } from 'src/app/core/models/resources/IMandator';
import { wording } from 'src/app/core/constants/wording/wording';
import { MandatorMainService } from 'src/app/core/services/mainServices/mandator-main.service';

@Component({
  selector: 'nv-autobank-rules-view-list',
  templateUrl: './autobank-rules-view-list.component.html',
  styleUrls: ['./autobank-rules-view-list.component.scss']
})
export class AutobankRulesViewListComponent implements OnInit {
  public gridConfig: NvGridConfig;
  public SelectType = SelectType;
  public mandatorNoModifier = mandatorNoModifier;
  public selectedMandatorId: number;
  public mandators$: Observable<IMandatorLookupRow[]>;

  constructor(
    private router: Router,
    public autobankRuleRepo: AutobankRuleRepositoryService,
    private confirmationService: ConfirmationService,
    private authService: AuthenticationService,
    public mandatorRepo: MandatorRepositoryService,
    private mandatorMainService: MandatorMainService,
  ) { }

  public ngOnInit() {
    forkJoin([
      this.mandatorRepo.fetchLookups({}),
      this.autobankRuleRepo.fetchAll({})
    ]).subscribe();

    this.mandators$ = this.mandatorMainService.fetchLookupsWithRolesOnly();

    const actions = {
      edit: this.editClicked,
      add: () => this.router.navigate([`${this.router.url}/new`]),
      delete: this.deleteClicked,
    };

    this.gridConfig = getAutobankGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
      { mandator: this.mandatorRepo.getPropertyById }
    );
  }

  private editClicked = (id: AutobankRuleKey) => {
    this.router.navigate([`${this.router.url}/${id}`]);
  }

  private deleteClicked = (autobank: IAutobankRule) => {
    const subject: IWording = {
      en: autobank.filterName,
      de: autobank.filterName,
    };

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject },
    }).subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.autobankRuleRepo.delete(autobank.id, {}).subscribe();
      }
    });
  }
}
