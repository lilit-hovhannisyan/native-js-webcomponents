interface ActionButtonPropertiesI {
  iconName?: string;
  type: string;
  contextMenuItems?: {
    isActiveAttrType?: boolean;
    value: string;
    key: string;
  }[];
}

export default ActionButtonPropertiesI;
