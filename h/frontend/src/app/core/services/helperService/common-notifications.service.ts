import { Injectable } from '@angular/core';
import { ConfirmationService } from '../confirmation.service';
import { wording } from '../../constants/wording/wording';

@Injectable({
  providedIn: 'root'
})
export class CommonNotificationsService {

  constructor(
    private confirmationService: ConfirmationService,
  ) { }

  public showSystemDefaultWarning(): void {
    this.confirmationService.warning({
      title: wording.accounting.canNotEditSystemDefaultTypes,
      okButtonWording: wording.general.ok,
      cancelButtonWording: null,
    });
  }
}
