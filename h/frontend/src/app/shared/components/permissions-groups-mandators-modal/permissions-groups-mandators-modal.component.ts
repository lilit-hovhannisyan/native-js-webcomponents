import { Component, OnInit, Input } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { Observable, of } from 'rxjs';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { getPermissionsGroupsMandatorsModalGridConfig } from './permissions-groups-mandators-modal.config';
import { cloneDeep } from 'lodash';
import { IMandatorLookup } from 'src/app/core/models/resources/IMandator';

@Component({
  templateUrl: './permissions-groups-mandators-modal.component.html',
  styleUrls: ['./permissions-groups-mandators-modal.component.scss']
})
export class PermissionsGroupsMandatorModalComponent implements OnInit {
  @Input() public mandators: IMandatorLookup[];
  @Input() public isAutobankPage = true;
  public selectedMandators: IMandatorLookup[];
  public gridConfig: NvGridConfig;
  public wording = wording;
  public dataSource$: Observable<IMandatorLookup[]>;
  public submitDisabled = true;

  constructor(
    private modalRef: NzModalRef,
  ) { }

  public ngOnInit(): void {
    this.gridConfig = getPermissionsGroupsMandatorsModalGridConfig(this.isAutobankPage);
    this.dataSource$ = of(
      // change references to not have previous checked items checked
      cloneDeep(this.mandators)
    );
  }

  public submit(): void {
    if (this.submitDisabled) {
      return;
    }

    this.modalRef.close(this.selectedMandators);
  }

  public rowSelect(mandators: IMandatorLookup[]): void {
    this.selectedMandators = mandators;
    this.submitDisabled = !mandators.length;
  }
}
