import { IWording } from './IWording';

export interface IWithLabel {
  displayLabel?: IWording;
}
