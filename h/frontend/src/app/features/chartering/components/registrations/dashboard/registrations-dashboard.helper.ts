import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { IWording } from 'src/app/core/models/resources/IWording';

export interface IRegistrationDashboardButton {
  label: IWording;
  routerLink: string;
}

export const registrationDashboardButtons: IRegistrationDashboardButton[] = [
  {
    label: wording.chartering.overview,
    routerLink: url(sitemap.chartering.children.registrations.children.registrationsFleet),
  },
  // TODO: Change routerLink of new views
  {
    label: wording.chartering.calendar,
    routerLink: url(sitemap.chartering.children.registrations.children.calendar),
  },
  {
    label: wording.chartering.vessels,
    routerLink: url(sitemap.chartering.children.misc.children.vessels),
  },
  {
    label: wording.chartering.reports,
    routerLink: url(sitemap.chartering.children.registrations.children.registrationsFleet),
  },
];
