import { Injectable } from '@angular/core';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { IDebitorCreditorFull, IDebitorCreditor, IDebitorCreditorRow } from 'src/app/core/models/resources/IDebitorCreditor';
import { map, switchMap } from 'rxjs/operators';
import { DebitorCreditorRepositoryService } from '../repositories/debitor-creditor-repository.service';
import { CompanyRepositoryService } from '../repositories/company-repository.service';
import { ICompany } from '../../models/resources/ICompany';
import { IBookingAccount } from '../../models/resources/IBookingAccount';
import { BookingAccountRepositoryService } from '../repositories/booking-account-repository.service';
import { sortBy } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class DebitorCreditorMainService {
  constructor(
    private debitorCreditorRepo: DebitorCreditorRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService,
  ) { }

  public fetchAll(): Observable<IDebitorCreditorFull[]> {
    return combineLatest([
      this.debitorCreditorRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.bookingAccountRepo.fetchAll({}),
    ]).pipe(
      map(res => this.getDebitorCreditorsFullList(...res)),
    );
  }

  /**
   *
   * @param whiteList ids to include in the returned list even if it's inactive
   */
  public fetchAllDebitorsCreditorsRows(whiteList: number[] = []): Observable<IDebitorCreditorRow[]> {
    return this.fetchAll()
      .pipe(
        map(debitorsCredtiors => debitorsCredtiors
          .filter(dc => dc.isActive || whiteList.includes(dc.id))),
        map(res => sortBy(this.toRows(res), [ db => db.companyName.toLowerCase()]))
      );
  }

  public fetchOne(id: number, params = {}): Observable<IDebitorCreditorFull> {
    return this.composeDebitorCreditorFull(
      this.debitorCreditorRepo.fetchOne(id, params),
      this.companyRepo.fetchAll({}),
      this.bookingAccountRepo.fetchAll({}),
    );
  }

  private composeDebitorCreditorFull(
    debitorAndCreditor$: Observable<IDebitorCreditor>,
    companies$: Observable<ICompany[]>,
    bookingAccounts$: Observable<IBookingAccount[]>,
  ): Observable<IDebitorCreditorFull> {
    return forkJoin([debitorAndCreditor$, companies$, bookingAccounts$]).pipe(
      map((res) => this.getDebitorCreditorFull(...res))
    );
  }

  private getDebitorCreditorsFullList(
    debitorAndCreditors: IDebitorCreditor[],
    companies: ICompany[],
    bookingAccounts: IBookingAccount[],
  ): IDebitorCreditorFull[] {
    return debitorAndCreditors.map(dcs => {
      return this.getDebitorCreditorFull(dcs, companies, bookingAccounts);
    });
  }

  private getDebitorCreditorFull(
    debitorAndCreditor: IDebitorCreditor,
    companies: ICompany[],
    bookingAccounts: IBookingAccount[],
  ): IDebitorCreditorFull {
    const company = companies.find(c => c.id === debitorAndCreditor.companyId);
    const creditorAccount = bookingAccounts.find(b => b.id === debitorAndCreditor.creditorAccountId);
    const debitorAccount = bookingAccounts.find(b => b.id === debitorAndCreditor.debitorAccountId);
    return { ...debitorAndCreditor, company, creditorAccount, debitorAccount };
  }

  public getStream(): Observable<IDebitorCreditorFull[]> {
    return forkJoin([
      this.debitorCreditorRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.bookingAccountRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.debitorCreditorRepo.getStream(),
        this.companyRepo.getStream(),
        this.bookingAccountRepo.getStream(),
      ])),
      map(res => this.getDebitorCreditorsFullList(...res))
    );
  }

  public toRow(debitorAndCreditor: IDebitorCreditorFull): IDebitorCreditorRow {
    const { company, creditorAccount, debitorAccount } = debitorAndCreditor;
    return ({
      ...debitorAndCreditor,
      companyName: company.companyName,
      creditorAccountName: creditorAccount.displayLabel,
      debitorAccountName: debitorAccount.displayLabel
    });
  }

  public toRows(debitorAndCreditors: IDebitorCreditorFull[]): IDebitorCreditorRow[] {
    return debitorAndCreditors.map(db => this.toRow(db));
  }
}
