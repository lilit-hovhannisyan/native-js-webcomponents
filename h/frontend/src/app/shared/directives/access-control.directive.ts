import { SitemapEntry } from './../../core/constants/sitemap/sitemap-entry';
import { AuthenticationService } from './../../authentication/services/authentication.service';
import { ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';
import { Directive, Input, TemplateRef } from '@angular/core';
import { ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';

/**
 * This directive uses to hide buttons, certain accounting and sections if user hasn't
 * permissions to create, update or remove, but has permission to read.
 * example how to use:
 * <ng-container *nvAccessControl="WRITE; zoneName:zone(sitemap.settings.topics.roles)">
 */
@Directive({
  selector: '[nvAccessControl]'
})
export class AccessControlDirective {

  constructor(
    private authService: AuthenticationService,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private router: Router,
  ) { }

  @Input() public nvAccessControlZoneName?: string;
  @Input() public set nvAccessControl(operation: ACL_ACTIONS) {

    const url = this.router.url;
    const zoneOfRoute = SitemapEntry.getByUrl(url).toZone();
    // if 'nvAccessControlZoneName' not provided -> uses zone of current url
    const zone = this.nvAccessControlZoneName || zoneOfRoute;
    const hasUserAccess = this.authService.can(operation, zone);

    hasUserAccess
      ? this.viewContainer.createEmbeddedView(this.templateRef)
      : this.viewContainer.clear();
  }

}
