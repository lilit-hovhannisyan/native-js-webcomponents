import { Component, Inject, Injector, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BaseLoggerComponent } from '@features/tracking-system';
import { MePopupVideoI } from './me-popup-video.interface';

@Component({
  selector: 'me-popup-video',
  templateUrl: './me-popup-video.component.html',
  styleUrls: ['./me-popup-video.component.scss']
})
export class MePopupVideoComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  componentVersion: string = '0.0.1';

  videoSources: { source: string; type: string }[] = [];

  constructor(injector: Injector, @Inject(MAT_DIALOG_DATA) public data: MePopupVideoI) {
    super(injector);
  }

  ngOnInit(): void {
    this.videoSources = [{ source: this.data.videoPath, type: 'video/mp4' }];
  }

  ngOnDestroy(): object {
    return super.ngOnDestroy();
  }
}
