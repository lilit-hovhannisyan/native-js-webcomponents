import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChange,
  ViewEncapsulation
} from '@angular/core';
import { FormControl } from '@angular/forms';

import { Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { MeSearchInputI } from './me-search-input-interface';

@Component({
  selector: 'me-search-input',
  templateUrl: './me-search-input.component.html',
  styleUrls: ['./me-search-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class MeSearchInputComponent implements OnInit, OnDestroy, OnChanges {
  // Beside data, optional input is debounceTime - time to pass until word will be searched
  @Input() dataModel: MeSearchInputI = {
    keyword: '',
    placeholder: 'Search for...'
  };
  @Output() changeEvent: EventEmitter<string> = new EventEmitter();

  searchInput: FormControl = new FormControl();

  private _debounceTime = 500;
  private _minCharacters = 1;

  private inputChangeSubscription!: Subscription;

  constructor() {}

  ngOnInit(): void {
    this._debounceTime = this.dataModel.debounceTime || this._debounceTime;
    this._minCharacters = this.dataModel.minCharacters || this._minCharacters;

    this.inputChangeSubscription = this.searchInput.valueChanges.pipe(debounceTime(this._debounceTime)).subscribe(() => {
      if (this.searchInput.value.length === 0) {
        this.changeEvent.emit(this.searchInput.value);
      } else if (this.searchInput.value.length >= this._minCharacters) {
        this.changeEvent.emit(this.searchInput.value);
      }
    });
  }

  ngOnChanges(changes: { dataModel: SimpleChange }): void {
    if (changes.dataModel && changes.dataModel.previousValue) {
      if (changes.dataModel.currentValue.keyword !== changes.dataModel.previousValue.keyword) {
        // if the change is comming from outside we are not emiting, just setting the value
        this.searchInput.setValue(this.dataModel.keyword, { emitEvent: false });
      }
    }
  }

  resetSearchInputValue(): void {
    this.searchInput.setValue('');
  }

  ngOnDestroy(): void {
    this.inputChangeSubscription.unsubscribe();
  }
}
