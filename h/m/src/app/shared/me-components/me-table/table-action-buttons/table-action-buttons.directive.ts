import { Directive, OnDestroy, ViewContainerRef, ComponentFactoryResolver, Input } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import ActionButtonI from '../interfaces/action-button.interface';

@Directive({
  selector: '[meTableActionButtonWrapper]'
})
export class TableActionButtonDirective implements OnDestroy {
  componentEventSubscription!: Subscription;

  constructor(private vc: ViewContainerRef, private cfr: ComponentFactoryResolver) {}

  @Input()
  set meTableActionButtonWrapper(cellParams: ActionButtonI & { subject: Subject<any> }) {
    const { component, componentData, subject, type } = cellParams;
    const componentFactory = this.cfr.resolveComponentFactory(component);
    const componentRef = this.vc.createComponent<any>(componentFactory);
    const { instance } = componentRef;

    instance.data = componentData;
    this.componentEventSubscription = instance.tableDataChangeEvent.subscribe((key: any) => {
      let eventData: any = { type };

      if (key) {
        eventData = { ...eventData, key };
      }
      subject.next(eventData);
    });
  }

  ngOnDestroy() {
    this.componentEventSubscription.unsubscribe();
    this.vc.clear();
  }
}
