import { system } from '../../constants/wording/system';
import { ISelectOption } from '../misc/selectOption';
import { IWording } from '../resources/IWording';

export enum COMPANY_LEGAL_FORM {
  None,
  LimitedPartnership,
  CompanyWithLimitedLiability,
  StockCorporation,
  CivilLawPartnership,
  ForeignCompany
}

export enum COMPANY_LEGAL_FORM_TYPE {
  None,
  OneShipLimitedPartnership,
  MultiShipLimitedPartnership,
  LimitedPartnership,
  GeneralPartnerCompany,
  Company
}


export enum COMPANY_GROUPS {
  PeterDohleSchiffahrtsKg,
  HammoniaReedereiGmbHCoKg,
  ErnstRussAg,
  Other,
}

export enum CAPITAL_TYPE {
  NonParValueShare,
  ParValueShare,
}

/**
 * Returns list of label value objects to display
 */
export const getCompanyLegalForm = (): ISelectOption<COMPANY_LEGAL_FORM>[] => {
  return [
    {
      value: COMPANY_LEGAL_FORM.LimitedPartnership,
      displayLabel: system.companyLegalFormKG
    },
    {
      value: COMPANY_LEGAL_FORM.CompanyWithLimitedLiability,
      displayLabel: system.companyLegalFormGmbH
    },
    {
      value: COMPANY_LEGAL_FORM.StockCorporation,
      displayLabel: system.companyLegalFormAG
    },
    {
      value: COMPANY_LEGAL_FORM.CivilLawPartnership,
      displayLabel: system.companyLegalFormGbR
    },
    {
      value: COMPANY_LEGAL_FORM.ForeignCompany,
      displayLabel: system.companyLegalFormForeignCompany
    },
  ];
};

export const getCompanyLegalFormTypes = (
  companyLegalForm: COMPANY_LEGAL_FORM
): ISelectOption<COMPANY_LEGAL_FORM_TYPE>[] => {
  if (companyLegalForm === COMPANY_LEGAL_FORM.LimitedPartnership) {
    return [
      {
        value: COMPANY_LEGAL_FORM_TYPE.OneShipLimitedPartnership,
        displayLabel: system.companyLegalFormTypeOneShipLimitedPartnership
      },
      {
        value: COMPANY_LEGAL_FORM_TYPE.MultiShipLimitedPartnership,
        displayLabel: system.companyLegalFormTypeMultishipLimitedPartnership
      },
      {
        value: COMPANY_LEGAL_FORM_TYPE.LimitedPartnership,
        displayLabel: system.companyLegalFormTypeOtherLimitedPartnership,
        default: true
      },
    ];
  } else if (companyLegalForm === COMPANY_LEGAL_FORM.CompanyWithLimitedLiability) {
    return [
      {
        value: COMPANY_LEGAL_FORM_TYPE.GeneralPartnerCompany,
        displayLabel: system.companyLegalFormTypeGeneralPartnerCompany
      },
      {
        value: COMPANY_LEGAL_FORM_TYPE.Company,
        displayLabel: system.companyLegalFormTypeOtherCompany,
        default: true
      },
    ];
  }
  return [];
};

export const partnerModalTitles: Map<COMPANY_LEGAL_FORM, IWording> = new Map([
  [COMPANY_LEGAL_FORM.LimitedPartnership, system.partner],
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, system.partner],
  [COMPANY_LEGAL_FORM.StockCorporation, system.shareholder_v2],
  // The "None" is NonParValueShare case of StockCorporation (AG)
  [COMPANY_LEGAL_FORM.None, system.shareholder_v2],
  [COMPANY_LEGAL_FORM.CivilLawPartnership, system.partner],
  [COMPANY_LEGAL_FORM.ForeignCompany, system.shareholder],
]);

export const defineManuallyLabels: Map<COMPANY_LEGAL_FORM, IWording> = new Map([
  [COMPANY_LEGAL_FORM.LimitedPartnership, system.definePartnerManually],
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, system.definePartnerManually],
  [COMPANY_LEGAL_FORM.StockCorporation, system.defineShareholderManually_v2],
  // The "None" is NonParValueShare case of StockCorporation (AG)
  [COMPANY_LEGAL_FORM.None, system.defineShareholderManually_v2],
  [COMPANY_LEGAL_FORM.CivilLawPartnership, system.definePartnerManually],
  [COMPANY_LEGAL_FORM.ForeignCompany, system.defineShareholderManually],
]);

export const contributionIncreaseLabels: Map<COMPANY_LEGAL_FORM, IWording> = new Map([
  [COMPANY_LEGAL_FORM.LimitedPartnership, system.partnerIncrease],
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, system.partnerIncrease],
  [COMPANY_LEGAL_FORM.StockCorporation, system.shareholderIncrease_v2],
  // The "None" is NonParValueShare case of StockCorporation (AG)
  [COMPANY_LEGAL_FORM.None, system.shareholderIncrease_v2],
  [COMPANY_LEGAL_FORM.CivilLawPartnership, system.partnerIncrease],
  [COMPANY_LEGAL_FORM.ForeignCompany, system.shareholderIncrease],
]);

export const shareholderReduceLabels: Map<COMPANY_LEGAL_FORM, IWording> = new Map([
  [COMPANY_LEGAL_FORM.LimitedPartnership, system.partnerReduce],
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, system.partnerReduce],
  [COMPANY_LEGAL_FORM.StockCorporation, system.shareholderReduce_v2],
  // The "None" is NonParValueShare case of StockCorporation (AG)
  [COMPANY_LEGAL_FORM.None, system.shareholderReduce_v2],
  [COMPANY_LEGAL_FORM.CivilLawPartnership, system.partnerReduce],
  [COMPANY_LEGAL_FORM.ForeignCompany, system.shareholderReduce],
]);

export const contributionAmountLabels: Map<COMPANY_LEGAL_FORM, IWording> = new Map([
  [COMPANY_LEGAL_FORM.LimitedPartnership, system.contributionAmount],
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, system.contributionAmount],
  [COMPANY_LEGAL_FORM.StockCorporation, system.nominalValue],
  // The "None" is NonParValueShare case of StockCorporation (AG)
  [COMPANY_LEGAL_FORM.None, system.numberOfNonParValueShares],
  [COMPANY_LEGAL_FORM.CivilLawPartnership, system.contributionAmount_v2],
  [COMPANY_LEGAL_FORM.ForeignCompany, system.parValueShare_v2],
]);

export const contributionAmountNewLabels: Map<COMPANY_LEGAL_FORM, IWording> = new Map([
  [COMPANY_LEGAL_FORM.LimitedPartnership, system.contributionAmountNew],
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, system.contributionAmountNew],
  [COMPANY_LEGAL_FORM.StockCorporation, system.nominalValueNew],
  // The "None" is NonParValueShare case of StockCorporation (AG)
  [COMPANY_LEGAL_FORM.None, system.numberOfNonParValueSharesNew],
  [COMPANY_LEGAL_FORM.CivilLawPartnership, system.contributionAmountNew_v2],
  [COMPANY_LEGAL_FORM.ForeignCompany, system.parValueShareNew_v2],
]);

export const contributionChangeAmountLabels: Map<COMPANY_LEGAL_FORM, IWording> = new Map([
  [COMPANY_LEGAL_FORM.LimitedPartnership, system.contributionChangeAmount],
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, system.contributionChangeAmount],
  [COMPANY_LEGAL_FORM.StockCorporation, system.nominalValueChange],
  // The "None" is NonParValueShare case of StockCorporation (AG)
  [COMPANY_LEGAL_FORM.None, system.numberOfNonParValueSharesChange],
  [COMPANY_LEGAL_FORM.CivilLawPartnership, system.contributionChangeAmount],
  [COMPANY_LEGAL_FORM.ForeignCompany, system.parValueShareChange],
]);

export const contributionModalTitles: Map<COMPANY_LEGAL_FORM, IWording> = new Map([
  [COMPANY_LEGAL_FORM.LimitedPartnership, system.managePartnersParticipation],
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, system.managePartnersParticipation],
  [COMPANY_LEGAL_FORM.StockCorporation, system.manageShareholdersParticipation],
  [COMPANY_LEGAL_FORM.CivilLawPartnership, system.managePartnersParticipation],
  [COMPANY_LEGAL_FORM.ForeignCompany, system.manageShareholdersParticipation_v2],
]);

export const contributionGridCompanyName: Map<COMPANY_LEGAL_FORM, IWording> = new Map([
  [COMPANY_LEGAL_FORM.LimitedPartnership, system.partner], // KG
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, system.partner], // GmbH
  [COMPANY_LEGAL_FORM.StockCorporation, system.shareholder_v2], // AG
  [COMPANY_LEGAL_FORM.CivilLawPartnership, system.partner], // GbR
  [COMPANY_LEGAL_FORM.ForeignCompany, system.shareholder], // ForeignCompany
]);
export const contributionsFieldsetTitle: Map<COMPANY_LEGAL_FORM, IWording> = new Map([
  [COMPANY_LEGAL_FORM.LimitedPartnership, system.partners], // KG
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, system.partners], // GmbH
  [COMPANY_LEGAL_FORM.StockCorporation, system.shareholders_v2], // AG
  [COMPANY_LEGAL_FORM.CivilLawPartnership, system.partners], // GbR
  [COMPANY_LEGAL_FORM.ForeignCompany, system.shareholders_v1], // ForeignCompany
]);

export const entryWithZeroValueLabels: Map<COMPANY_LEGAL_FORM, IWording> = new Map([
  [COMPANY_LEGAL_FORM.LimitedPartnership, system.partnerEntryWithZeroValue], // KG
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, system.partnerEntryWithZeroValue], // GmbH
  [COMPANY_LEGAL_FORM.StockCorporation, system.shareholderEntryWithZeroValue], // AG
  [COMPANY_LEGAL_FORM.CivilLawPartnership, system.partnerEntryWithZeroValue], // GbR
  [COMPANY_LEGAL_FORM.ForeignCompany, system.shareholderEntryWithZeroValue], // ForeignCompany
]);

export const withdrawWithZeroValueLabels: Map<COMPANY_LEGAL_FORM, IWording> = new Map([
  [COMPANY_LEGAL_FORM.LimitedPartnership, system.partnerWithdrawWithZeroValue], // KG
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability, system.partnerWithdrawWithZeroValue], // GmbH
  [COMPANY_LEGAL_FORM.StockCorporation, system.shareholderWithdrawWithZeroValue], // AG
  [COMPANY_LEGAL_FORM.CivilLawPartnership, system.partnerWithdrawWithZeroValue], // GbR
  [COMPANY_LEGAL_FORM.ForeignCompany, system.shareholderWithdrawWithZeroValue], // ForeignCompany
]);

export function getContributionWordingKey(
  legalForm: COMPANY_LEGAL_FORM, // from ICompany
  capitalType?: CAPITAL_TYPE, // from ICompanyLegalEntity
): string {
  if (legalForm !== COMPANY_LEGAL_FORM.StockCorporation) {
    return `${legalForm}`;
  }

  return `${legalForm}-${capitalType}`;
}
export const contributionGridContributionAmount: Map<string, IWording> = new Map([
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.LimitedPartnership),
    system.contribution_v2,
  ], // KG
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.CompanyWithLimitedLiability),
    system.contribution_v2,
  ], // GmbH
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.StockCorporation, CAPITAL_TYPE.ParValueShare),
    system.nominalValue,
  ], // AG - ParValueShare
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.StockCorporation, CAPITAL_TYPE.NonParValueShare),
    system.numberOfNonParValueShares,
  ], // AG - NoneParValueShare
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.CivilLawPartnership),
    system.contribution_v1,
  ], // GbR
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.ForeignCompany),
    system.parValueShare_v2,
  ], // ForeignCompany
]);
export const contributionGridSumContribution: Map<string, IWording> = new Map([
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.LimitedPartnership),
    system.sumContributions_v2,
  ], // KG
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.CompanyWithLimitedLiability),
    system.sumContributions_v2,
  ], // GmbH
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.StockCorporation, CAPITAL_TYPE.ParValueShare),
    system.sumShareCapital_v2,
  ], // AG - ParValueShare
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.StockCorporation, CAPITAL_TYPE.NonParValueShare),
    system.sumNonParValueShares,
  ], // AG - NoneParValueShare
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.CivilLawPartnership),
    system.sumContributions_v1,
  ], // GbR
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.ForeignCompany),
    system.sumShareCapital_v1,
  ], // ForeignCompany
]);
export const legalEntityExclamationTooltipContributions: Map<string, IWording> = new Map([
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.LimitedPartnership),
    system.contribution_v4,
  ], // KG
  // TODO: check/fix CompanyWithLimitedLiability (GmbH) when task is available
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.CompanyWithLimitedLiability),
    system.shareCapital_v2,
  ], // GmbH
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.StockCorporation, CAPITAL_TYPE.ParValueShare),
    system.shareCapital,
  ], // AG - ParValueShare
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.StockCorporation, CAPITAL_TYPE.NonParValueShare),
    system.sumOfNonParValueShares_v2,
  ], // AG - NoneParValueShare
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.CivilLawPartnership),
    system.contribution_v3,
  ], // GbR
  [
    getContributionWordingKey(COMPANY_LEGAL_FORM.ForeignCompany),
    system.shareCapital_v3,
  ], // ForeignCompany
]);
