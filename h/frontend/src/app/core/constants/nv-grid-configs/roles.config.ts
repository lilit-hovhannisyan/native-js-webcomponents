import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { IRole } from 'src/app/core/models/resources/IRole';
import { wording } from 'src/app/core/constants/wording/wording';
import { Operations } from 'src/app/core/models/Operations';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {

  return ({
    gridName: 'rolesGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'displayLabel',
        title: wording.general.name,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'description',
        title: wording.general.description,
        width: 450,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isSystemDefault',
        title: wording.general.systemDefault,
        width: 200,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editRole',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (role: IRole) => actions.edit(role.id),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteRole',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: (role: IRole) => actions.delete(role),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: () => actions.add()
      }
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
