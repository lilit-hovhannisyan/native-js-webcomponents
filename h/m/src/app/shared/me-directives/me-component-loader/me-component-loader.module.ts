import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeLoadingDirective } from './me-component-loader.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [MeLoadingDirective],
  exports: [MeLoadingDirective]
})
export class MeComponentLoadingModule {}
