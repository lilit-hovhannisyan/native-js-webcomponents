export const NUMBER: RegExp = /^[0-9]*$/;
export const REALNUMBERS: RegExp = /([0-9]+\.[0-9]*)|([0-9]*\.[0-9]+)|([0-9]+)/;
export const WHOLENUMBERS: RegExp = /^\d+$/;
export const FOURDIGITS: RegExp = /^\d{4}$/;
export const DECIMETAL: RegExp = /^[-+]?\d{1,4}(\.\d{1,4})?$/;
export const LATITUDE: RegExp = /^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/;
export const LONGITUDE: RegExp = /^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/;
export const ONLYDIGITS: RegExp = /\D+/g;
export const DEBITCREDIT: RegExp = /^[+-SHDC]$/;
export const PHONENUMBER: RegExp = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
export const COUNTRY_FLAG_EMOJI: RegExp = /[\uD83C][\uDDE6-\uDDFF][\uD83C][\uDDE6-\uDDFF]/;
export const ONLY_DIGITS_AND_LETTERS: RegExp = /^[a-zA-Z0-9]*$/;
