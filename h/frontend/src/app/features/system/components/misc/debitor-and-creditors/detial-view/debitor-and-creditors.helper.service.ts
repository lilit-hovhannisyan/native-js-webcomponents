
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IDebitorCreditorRow, IDebitorCreditor } from 'src/app/core/models/resources/IDebitorCreditor';
import { wording } from 'src/app/core/constants/wording/wording';
import { SettingsService } from 'src/app/core/services/settings.service';
import { ICompany } from 'src/app/core/models/resources/ICompany';

@Injectable()
export class DebitorAndCreditorsHelperService {
  public editViewTitleSubject: BehaviorSubject<string> = new BehaviorSubject('');
  public editViewTitle$ = this.editViewTitleSubject.asObservable();
  constructor(
    public settingsService: SettingsService,
  ) { }

  public generateTitle = (debitorCreditor?: IDebitorCreditor, companies?: ICompany[]) => {
    let title = wording.general.create[this.settingsService.language];

    if (debitorCreditor && companies) {
      const company = companies.find(c => c.id === debitorCreditor.companyId);

      title = `${debitorCreditor.no} ${company.companyName}`;
    }

    this.editViewTitleSubject.next(title);
  }
}
