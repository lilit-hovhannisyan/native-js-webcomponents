import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IVoucherType } from '../../models/resources/IVoucherType';
import { Operations } from '../../models/Operations';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {

  return ({
    gridName: 'mandatorVoucherTypesConfig',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'abbreviation',
        title: wording.accounting.abbreviation,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wording.general.name,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'useCase',
        title: wording.accounting.useCase,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isAutomaticNumbering',
        title: wording.accounting.autoNumbering,
        width: 200,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isSystemDefault',
        title: wording.general.systemDefault,
        width: 200,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      }
    ],
    buttons: [
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteVoucherType',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        func: (voucherType: IVoucherType) => actions.delete(voucherType),
        hidden: !canPerform(Operations.DELETE),
      },
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editVoucherType',
        tooltip: wording.general.edit,
        actOnDoubleClick: true,
        func: (voucherType: IVoucherType) => actions.edit(voucherType),
        hidden: !canPerform(Operations.DELETE),
      },
    ],
    toolbarButtons: [
      {
        title: wording.general.add,
        tooltip: wording.general.add,
        icon: 'plus-circle-o',
        func: () => actions.add(),
        hidden: () => !canPerform(Operations.CREATE),
      }
    ]
  });
};
