export interface MeCoverImageUploadI {
  backgroundimage?: string;
  thumbnail?: string;
  acceptPhotoExtensions?: string;
  showProgress?: boolean;
  maxUploadSize: string;
  percentageDone?: number;
  dimensions: string;
  disabled: boolean;
}
