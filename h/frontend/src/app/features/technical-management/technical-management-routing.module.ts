import { NgModule } from '@angular/core';
import { ArticlesDetailViewComponent } from 'src/app/features/technical-management/components/main/articles/detail-view/articles-detail-view.component';
import { ArticlesListViewComponent } from 'src/app/features/technical-management/components/main/articles/list-view/articles-list-view.component';
import { StevedoreDamagesBasicComponent } from 'src/app/features/technical-management/components/misc/stevedore-damages/detail-view/stevedore-damages-basic/stevedore-damages-basic.component';
import { UnsavedChangesGuard } from 'src/app/routing/services/unsaved-changes.guard';
import { PageNotFoundComponent } from '../../shared/components/page-not-found/page-not-found.component';
import { NavigationBoardComponent } from '../../shared/components/navigation-board/navigation-board.component';
import { sitemap } from '../../core/constants/sitemap/sitemap';
import { RouterModule, Routes } from '@angular/router';
import { ArticleTypesDetailViewComponent } from './components/main/article-types/detail-view/detail-view.component';
import { ArticleTypesListViewComponent } from './components/main/article-types/list-view/list-view.component';
import { ArticleClassesListViewComponent } from './components/main/article-classes/list-view/list-view.component';
import { ArticleClassesDetailViewComponent } from './components/main/article-classes/detail-view/detail-view.component';
import { ArticleUnitsListViewComponent } from './components/main/article-units/list-view/article-units-list-view.component';
import { ArticleUnitsDetailViewComponent } from './components/main/article-units/detail-view/article-units-detail-view.component';
import { StevedoreDamagesListViewComponent } from './components/misc/stevedore-damages/list-view/list-view.component';
import { StevedoreDamagesDetailViewComponent } from './components/misc/stevedore-damages/detail-view/detail-view.component';
import { OrderManagementListViewComponent } from './components/misc/order-management/list-view/list-view.component';
import { OrderManagementDetailViewComponent } from './components/misc/order-management/detail-view/detail-view.component';
import { TechnicalShippingsListViewComponent } from './components/misc/technical-shippings/list-view/list-view.component';
import { TechnicalShippingsDetailViewComponent } from './components/misc/technical-shippings/detail-view/detail-view.component';
import { RepairDocumentsComponent } from './components/misc/stevedore-damages/detail-view/repair-documents/repair-documents.component';
import { UploadDocumentsComponent } from './components/misc/stevedore-damages/detail-view/repair-documents/upload-documents/upload-documents.component';
import { AdministrationComponent } from './components/misc/stevedore-damages/detail-view/administration/administration.component';

export const routes: Routes = [
  {
    path: '',
    component: NavigationBoardComponent,
    pathMatch: 'full'
  },
  {
    path: sitemap.technicalManagement.children.misc.path,
    children: [
      {
        path: '',
        component: NavigationBoardComponent,
        pathMatch: 'full'
      },
      {
        path: sitemap.technicalManagement.children.misc.children.stevedoreDamages.path,
        children: [
          {
            path: '',
            component: StevedoreDamagesListViewComponent,
          },
          {
            path: ':id',
            component: StevedoreDamagesDetailViewComponent,
            children: [
              {
                path: sitemap.technicalManagement.children.misc.children.stevedoreDamages.children.id.children.basic.path,
                component: StevedoreDamagesBasicComponent,
                canDeactivate: [UnsavedChangesGuard],
              },
              {
                path: sitemap.technicalManagement.children.misc.children.stevedoreDamages.children.id.children.repairDocuments.path,
                component: RepairDocumentsComponent,
                canDeactivate: [UnsavedChangesGuard],
              },
              {
                path: sitemap.technicalManagement.children.misc.children.stevedoreDamages.children.id.children.administration.path,
                component: AdministrationComponent,
                canDeactivate: [UnsavedChangesGuard],
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.technicalManagement.children.misc.children.stevedoreDamages.children.id.children.basic.path,
              }
            ]
          }
        ]
      },
      {
        path: sitemap.technicalManagement.children.misc.children.orderManagement.path,
        children: [
          {
            path: '',
            component: OrderManagementListViewComponent
          },
          {
            path: ':id',
            component: OrderManagementDetailViewComponent
          }
        ]
      },
      {
        path: sitemap.technicalManagement.children.misc.children.technicalShippings.path,
        children: [
          {
            path: '',
            component: TechnicalShippingsListViewComponent
          },
          {
            path: ':id',
            component: TechnicalShippingsDetailViewComponent
          }
        ]
      },
    ]
  },
  {
    path: sitemap.technicalManagement.children.main.path,
    children: [
      {
        path: '',
        component: NavigationBoardComponent,
        pathMatch: 'full'
      },
      {
        path: sitemap.technicalManagement.children.main.children.articleClasses.path,
        children: [
          {
            path: '',
            component: ArticleClassesListViewComponent
          },
          {
            path: ':id',
            component: ArticleClassesDetailViewComponent
          }
        ]
      },
      {
        path: sitemap.technicalManagement.children.main.children.articleTypes.path,
        children: [
          {
            path: '',
            component: ArticleTypesListViewComponent
          },
          {
            path: ':id',
            component: ArticleTypesDetailViewComponent
          }
        ]
      },
      {
        path: sitemap.technicalManagement.children.main.children.articleUnits.path,
        children: [
          {
            path: '',
            component: ArticleUnitsListViewComponent
          },
          {
            path: ':id',
            component: ArticleUnitsDetailViewComponent
          }
        ]
      },
      {
        path: sitemap.technicalManagement.children.main.children.articles.path,
        children: [
          {
            path: '',
            component: ArticlesListViewComponent
          },
          {
            path: ':id',
            component: ArticlesDetailViewComponent
          }
        ]
      },
    ]
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  }
];

export const DECLARATIONS = [
  ArticleClassesListViewComponent,
  ArticleClassesDetailViewComponent,
  ArticleTypesListViewComponent,
  ArticleTypesDetailViewComponent,
  ArticleUnitsListViewComponent,
  ArticleUnitsDetailViewComponent,
  StevedoreDamagesListViewComponent,
  StevedoreDamagesDetailViewComponent,
  OrderManagementListViewComponent,
  OrderManagementDetailViewComponent,
  ArticlesListViewComponent,
  ArticlesDetailViewComponent,
  TechnicalShippingsListViewComponent,
  TechnicalShippingsDetailViewComponent,
  StevedoreDamagesBasicComponent,
  RepairDocumentsComponent,
  UploadDocumentsComponent,
  AdministrationComponent,
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TechnicalManagementRoutingModule {
}

