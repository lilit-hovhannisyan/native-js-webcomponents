import { FormGroup } from '@angular/forms';
import { MeInputErrorI } from '@shared/me-components/me-attribute/me-input-error/me-input-error.interface';
import { MeTextInputI } from '@shared/me-components/me-attribute/me-text-input/me-text-input.interface';
import { DMStringAttributeModel } from './attribute.model';

export class MeTextInputAttribute implements MeTextInputI {
  private _data!: DMStringAttributeModel;
  private _form!: FormGroup;
  private _inputErrors!: MeInputErrorI;

  constructor() {}

  public set data(data: DMStringAttributeModel) {
    this._data = data;
  }

  public get data(): DMStringAttributeModel {
    return this._data;
  }

  public set form(form: FormGroup) {
    this._form = form;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get displayName(): string {
    return this.data.displayName;
  }

  public get internalName(): string {
    return this.data.internalName;
  }

  public get required(): boolean {
    return this.data.required;
  }

  public get optRequired(): boolean {
    return this.data.optRequired;
  }

  public get value(): string {
    return this.data.value;
  }

  public get minLength(): number {
    return this.data.minLength;
  }

  public get maxLength(): number {
    return this.data.maxLength;
  }

  public get disabled(): boolean {
    return this.data.disabled;
  }

  public set inputErrors(inputErrors: MeInputErrorI) {
    this._inputErrors = inputErrors;
  }

  public get inputErrors(): MeInputErrorI {
    return this._inputErrors;
  }
}
