import { fromEvent } from 'rxjs';
import { delay, take, tap } from 'rxjs/operators';

const onScrollFinish = (scrollElem: HTMLElement) => {
  fromEvent(scrollElem, 'scroll', { passive: true })
    .pipe(
      take(1),
      delay(300),
      tap(() => {
        scrollElem.scroll({
          top: scrollElem.scrollTop - 30,
          behavior: 'smooth'
        });
      })
    )
    .subscribe();
};

export const scrollToFirstInvalidControl = (
  element: HTMLElement,
  scrollElementSelector = 'form',
  errorFieldSelector = '.invalid'
): void => {
  const scrollElement: HTMLFormElement = element.querySelector(scrollElementSelector);
  const firstInvalidControl: HTMLElement = scrollElement.querySelector(
    errorFieldSelector
  );

  firstInvalidControl.scrollIntoView({
    behavior: 'smooth',
  });
  // adjust scroll to make the label visible
  onScrollFinish(scrollElement);
};
