import { Injectable } from '@angular/core';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { Title } from '@angular/platform-browser';
import { SettingsService } from 'src/app/core/services/settings.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BrowserTitleService {

  constructor(
    private title: Title,
    private settingService: SettingsService,
    private router: Router,
  ) { }

  public setTitleByUrl(url: string) {
    const entry: SitemapEntry = SitemapEntry.getByUrl(url);
    if (entry) {
      const language = this.settingService.language;
      const title: string = entry.node.title[language];
      const desc: string = entry.node.description && entry.node.description[language];
      // if it's too deep let's show who its grandparent is
      if (entry.getLevel() > 2) {
        return this.setTitle([title, entry.parent.parent.node.title[language], 'NAVIDO', desc]);
      }
      this.setTitle([title, 'NAVIDO', desc]);
    }
  }

  public setTitleWithInfo(info: string) {
    const url: string = this.router.url;
    const entry: SitemapEntry = SitemapEntry.getByUrl(url);
    if (entry) {
      const language = this.settingService.language;
      const title: string = entry.node.title[language];
      const desc: string = entry.node.description && entry.node.description[language];
      if (entry.getLevel() > 2) {
        this.setTitle([info, title, entry.parent.parent.node.title[language], 'NAVIDO', desc]);
        return;
      }
      this.setTitle([info, title, 'NAVIDO', desc]);
      return;
    }
    this.setTitle([info]);
  }

  private setTitle(list: string[]) {
    const tabTitle = list.reduce((prev, current) => {
      if (!prev) {
        return current || '';
      }
      return current ? `${prev} | ${current}` : prev;
    }, '');
    this.title.setTitle(tabTitle);
  }
}
