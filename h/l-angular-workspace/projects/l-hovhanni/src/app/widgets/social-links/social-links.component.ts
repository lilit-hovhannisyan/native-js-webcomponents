import {Component, Input} from '@angular/core';
import {socialLinks} from '../../../../../shared-layer/l-hovhanni/dal/app.config';

@Component({
  selector: 'app-social-links',
  templateUrl: './social-links.component.html',
  styleUrls: ['./social-links.component.less']
})
export class SocialLinksComponent {
  @Input() cta = 'Feel free to get in touch:';
  socialLinks = socialLinks;
}
