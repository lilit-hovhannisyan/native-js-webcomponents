export interface MePopupSelectI {
  title: string;
  selectedCount: number;
  showOnlySelected?: boolean;
  showActions?: boolean;
}
