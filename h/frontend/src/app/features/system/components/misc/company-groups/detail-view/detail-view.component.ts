import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ElementRef } from '@angular/core';
import { ResourceDetailView } from '../../../../../../core/abstracts/ResourceDetailView';
import { ICompanyGroup, ICompanyGroupCreate } from '../../../../../../core/models/resources/ICompanyGroup';
import { BaseComponentService } from '../../../../../../core/services/base-component.service';
import { CompanyGroupRepositoryService } from '../../../../../../core/services/repositories/company-group-repository.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Validators } from '../../../../../../core/classes/validators';
import { url } from '../../../../../../core/constants/sitemap/sitemap-entry';
import { sitemap } from '../../../../../../core/constants/sitemap/sitemap';
import { wording } from '../../../../../../core/constants/wording/wording';
import { CompanyLegalEntityRepositoryService } from '../../../../../../core/services/repositories/company-legal-entity-repository.service';
import { ICompanyLegalEntity } from '../../../../../../core/models/resources/ICompanyLegalEntity';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'nv-company-groups-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompanyGroupsDetailViewComponent  extends ResourceDetailView<
  ICompanyGroup, ICompanyGroupCreate
  > implements OnInit {
  protected repoParams = {};
  protected resourceDefault = {
    isActive: true,
  } as ICompanyGroup;
  public routeBackUrl = url(
    sitemap.system.children.misc.children.companyGroups
  );
  public formElement: ElementRef;

  constructor(
    baseComponentService: BaseComponentService,
    private companyGroupRepo: CompanyGroupRepositoryService,
    private companyLegalEntityRepo: CompanyLegalEntityRepositoryService,
    public route: ActivatedRoute,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private el: ElementRef
  ) {
    super(companyGroupRepo, baseComponentService);
  }

  public ngOnInit(): void {
    const { id } = this.route.snapshot.params;

    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public init(companyGroup: ICompanyGroup = this.resourceDefault): void {
    this.initForm(this.fb.group({
      id: [companyGroup.id],
      labelEn: [companyGroup.labelEn, [
        Validators.required,
      ]],
      labelDe: [companyGroup.labelDe, [
        Validators.required,
      ]],
      isActive: [companyGroup.isActive],
    }));
  }

  public getTitle = () => this.resource.displayLabel[this.settingsService.language];

  public onIsActiveChange(isChecked: boolean): void {
    if (!this.creationMode && !isChecked) {
      this.companyLegalEntityRepo.fetchAll({}).subscribe(legalEntities => {
        const relations: ICompanyLegalEntity[] = legalEntities
          .filter(l => l.groupOfCompanyId === this.form.get('id').value);

        if (!relations.length) {
          return;
        }
        this.showConfirmationAndDeleteRelations(relations);
      });
    }
  }

  private showConfirmationAndDeleteRelations(relations: ICompanyLegalEntity[]): void {
    this.confirmationService.confirm({
      okButtonWording: wording.general.save,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.companyGroupRelationsWillBeDeleted,
    }).subscribe(confirmed => {
      if (!confirmed) {
        this.form.get('isActive').patchValue(true);
        return;
      }

      forkJoin(relations
        .map(r => this.companyLegalEntityRepo.update({ ...r, groupOfCompanyId: null }, {})))
        .subscribe();

      this.submit();
    });
  }

  public submit(): void {
    if (!this.form.valid) { return; }

    const { value } = this.form;

      this.creationMode
        ? this.createResource(value)
        : this.updateResource(value);
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
