import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { NotificationsService } from './notification.service';
import { ResourceTrackerService } from 'src/app/shared/services/resource-tracker.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { IResource, ResourceKey } from '../models/resources/IResource';
import { map } from 'rxjs/operators';
import { IDataResponseBody } from '../models/responses/IDataResponseBody';
import { IResourceCollection } from '../models/resources/IResourceCollection';
declare type Items<T extends IResource<ResourceKey>> = HttpResponse<IDataResponseBody<IResourceCollection<T>>>;

/**
 * The purpose of this service is to inject additional
 * services to the abstract repository at one central point
 * and by that AVOIDING to reference and pass additional services
 * in each child repostiory.
 */
@Injectable({
  providedIn: 'root'
})
export class RepositoryService {
  constructor(
    public httpClient: HttpClient,
    public notificationsService: NotificationsService,
    public resourceTracker: ResourceTrackerService,
    public authenticationService: AuthenticationService
  ) {
  }


  public mapToItemsOperator = <T extends IResource<ResourceKey>>() => {
    return map((response: Items<T>) => response.body.data.items);
  }
}
