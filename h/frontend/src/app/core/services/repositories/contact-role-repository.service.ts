import { ContactKey } from 'src/app/core/models/resources/IContact';
import { Injectable } from '@angular/core';
import { IContactRole, IContactRoleDTO, IContactRoleCreate } from '../../models/resources/IContactRole';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { TTL_LONG } from 'src/app/shared/services/resource-tracker.service';
import { getLabelKeyByLanguage } from '../../helpers/general';
import { SettingsService } from '../settings.service';

@Injectable({
  providedIn: 'root'
})
export class ContactRoleRepositoryService extends AbstractRepository<
IContactRole,
IContactRoleDTO,
IContactRoleCreate,
RepoParams<IContactRole>,
ContactKey
> {
  protected resourceType = 'ContactRole';
  protected resourceTTL = TTL_LONG;
  protected useCachePerDefault = true;

  public sortAsce = true;
  public sortKey = getLabelKeyByLanguage(this.settingsService.language);

  constructor(repositoryService: RepositoryService,
    private settingsService: SettingsService) {
    super(repositoryService);
  }

  protected postProcess(item: IContactRoleDTO): IContactRole { return item; }

  public getBaseUrl(repoParams: RepoParams<IContactRole>): string {
    return '/api/contact-roles';
  }
}
