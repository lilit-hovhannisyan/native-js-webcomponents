import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { AttributeGroupModel } from '@shared/models/attribute/attribute.group.model';

@Component({
  selector: 'me-group-attributes',
  templateUrl: './me-group-attributes.component.html',
  styleUrls: ['./me-group-attributes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeGroupAttributesComponent implements OnInit {
  @Input() attributeGroup!: AttributeGroupModel;

  constructor() {}

  ngOnInit(): void {}
}
