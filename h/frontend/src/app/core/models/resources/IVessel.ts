import { IVesselAccounting } from 'src/app/core/models/resources/IVesselAccounting';
import { IVoyageCharterName } from 'src/app/core/models/resources/IVoyageCharterName';
import { IResource } from './IResource';
import { IWithActive } from './IWithActive';

export type VesselKey = number;

interface ISharedProps extends IWithActive {
  imo: number;
  deliveryDate: string;
  isIntendedSale: boolean;
  officialNumber?: string;
  mmsi?: string;
}

export interface IVesselCreate extends ISharedProps { }

export interface IVesselBase extends ISharedProps, IResource<VesselKey> { }

export interface IVesselDTO extends IVesselBase { }

export interface IVessel extends IVesselDTO { }

export interface IVesselWithAccountings extends IVessel {
  vesselAccountings: IVesselAccounting[];
  lastVesselAccounting: IVesselAccounting;
  vesselCharternames: IVoyageCharterName[];
  lastVesselChartername: IVoyageCharterName;
}

export interface IVesselWithAccountingsRow extends IVesselWithAccountings {
  lastVesselAccountingNo: number;
  lastVesselAccountingName: string;
}
