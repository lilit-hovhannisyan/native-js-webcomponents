export interface MeIntroductionMessageI {
  content: string;
  buttonText: string;
}
