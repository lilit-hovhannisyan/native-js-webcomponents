export enum MeAttributeLayoutTypes {
  VIEW = 'VIEW',
  SEARCH = 'SEARCH',
  CREATE = 'CREATE',
  UPDATE = 'UPDATE'
}
