import { Type } from 'class-transformer';
import { GroupAttributesModel } from './group-attributes.model';
import { GroupAttributesExtendedModel } from './group-attributes-extended.model';

export class GroupModel {
  domainName: string = '';
  oid: string = '';
  numberOfUsers?: number = 0;
  @Type(() => GroupAttributesModel)
  attributes: GroupAttributesModel = new GroupAttributesModel();
  @Type(() => GroupAttributesExtendedModel)
  attributesExtended: GroupAttributesExtendedModel = new GroupAttributesExtendedModel();
}
