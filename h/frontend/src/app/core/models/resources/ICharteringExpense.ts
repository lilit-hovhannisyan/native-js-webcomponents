import { wording } from 'src/app/core/constants/wording/wording';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { BookingAccountKey, IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { BookingCodeKey, IBookingCode } from 'src/app/core/models/resources/IBookingCode';
import { IWithActive } from 'src/app/core/models/resources/IWithActive';
import { IWithLabel } from 'src/app/core/models/resources/IWithLabel';
import { IWithLabels } from 'src/app/core/models/resources/IWithLabels';
import { IWording } from 'src/app/core/models/resources/IWording';
import { IResource } from './IResource';

export type CharteringExpenseKey = number;

export enum CharteringExpenseType {
  Lumpsum = 1,
  AtCost = 2
}

interface ISharedProps extends IWithActive, IWithLabels {
  shortName: string;
  type: CharteringExpenseType;
  isDefaultForTcVoyages: boolean;
  bookingAccountId: BookingAccountKey;
  costType?: string;
  bookingCodeId: BookingCodeKey;
  remark?: string;
  lastChange?: string;
  change?: string;
}

export interface ICharteringExpenseBase extends ISharedProps, IResource<CharteringExpenseKey> { }

export interface ICharteringExpenseCreate extends ISharedProps { }

export interface ICharteringExpenseDTO extends ICharteringExpenseBase { }

export interface ICharteringExpense extends ICharteringExpenseDTO, IWithLabel { }

export interface ICharteringExpenseFull extends ICharteringExpense {
  bookingAccount: IBookingAccount;
  bookingCode: IBookingCode;
}

export interface ICharteringExpenseRow extends ICharteringExpenseFull {
  typeLabel: IWording;
  accountLabel: IWording;
  bookingCodeLabel: IWording;
}

export const charteringExpenseTypeOptions: ISelectOption[] = [
  { displayLabel: wording.chartering.expenseTypeLupsum, value: CharteringExpenseType.Lumpsum },
  { displayLabel: wording.chartering.expenseTypeAtCost, value: CharteringExpenseType.AtCost },
];
