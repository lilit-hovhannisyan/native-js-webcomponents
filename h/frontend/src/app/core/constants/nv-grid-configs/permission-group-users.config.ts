import { NvColumnDataType, NvFilterControl, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { IUser } from 'src/app/core/models/resources/IUser';

export const getPermissionGroupUsersGridConfig = (
  actions: IGridConfigActions,
  canPerform: (operation: Operations) => boolean,
  isAutobankPage = true,
): NvGridConfig => {
  return {
    gridName: `${isAutobankPage ? 'autobank' : 'payments'}PermissionGroupUsersGrid`,
    title: wording.system.users,
    sortBy: 'displayName',
    isSortAscending: true,
    hideToolbar: false,
    disableDragAndDrop: true,
    disableHideColumns: true,
    disableSortColumns: true,
    hideSettingsButton: true,
    hideRefreshButton: true,
    clearFiltersButton: true,
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    paging: {
      pageNumber: 1,
      pageSize: 10
    },
    columns: [
      {
        key: 'displayName',
        title: wording.general.displayName,
        width: 250,
        filter: {
          values: []
        }
      },
      {
        key: 'lastLoggedIn',
        title: wording.general.lastLoggedIn,
        dataType: NvColumnDataType.DateTime,
        filter: {
          values: []
        },
        width: 150,
      },
      {
        key: 'isActive',
        title: wording.general.isActive,
        width: 80,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'delete',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: (user: IUser) => actions.delete(user),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.add,
        tooltip: wording.general.add,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: actions.add
      }
    ],
    contextMenuButtons: [],
  };
};

