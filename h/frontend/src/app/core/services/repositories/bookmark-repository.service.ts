import { Injectable } from '@angular/core';
import { IBookmark, IBookmarkDTO, IBookmarkCreate, BookmarkKey, IBookmarkFull } from '../../models/resources/IBookmark';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { map } from 'rxjs/operators';
import { getNodeByCompoundKey } from '../../constants/sitemap/sitemap-entry';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookmarkRepositoryService extends AbstractRepository<
  IBookmark,
  IBookmarkDTO,
  IBookmarkCreate,
  RepoParams<IBookmark>,
  BookmarkKey
>  {
  protected resourceType = 'Bookmark';

  constructor(
    repositoryService: RepositoryService,
    private authService: AuthenticationService,
  ) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/bookmarks';

  public getBookmarksWithNodes(): Observable<IBookmarkFull[]> {
    const user = this.authService.getUser();
    super.fetchAll({ queryParams: { userId: user.id } }).subscribe();
    return super.getStream()
      .pipe(
        map(bookmarks => bookmarks.map(b => ({
          ...b,
          node: getNodeByCompoundKey(b.topicKey),
          })
        )),
      );
  }

  protected postProcess = (item: IBookmarkDTO): IBookmark => item;
}
