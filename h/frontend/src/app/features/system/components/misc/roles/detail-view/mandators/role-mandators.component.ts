import { GridConfigService } from '../../../../../../../core/services/grid-config.service';
import { IMandatorLookupRow } from '../../../../../../../core/models/resources/IMandator';
import { MandatorMainService } from '../../../../../../../core/services/mainServices/mandator-main.service';
import { IMandatorRole } from '../../../../../../../core/models/resources/IMandatorRole';
import { MandatorRoleRepositoryService } from '../../../../../../../core/services/repositories/mandator-role.repository.service';
import { MandatorKey } from '../../../../../../../core/models/resources/IMandator';
import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { orderBy } from 'lodash';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/roles-mandators.config';

@Component({
  templateUrl: './role-mandators.component.html',
  styleUrls: ['./role-mandators.component.scss']
})
export class RoleMandatorsComponent implements OnInit {

  public roleId: number;

  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IMandatorLookupRow[]>;

  public editMode = false;
  private editMode$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  private selectedMandators: MandatorKey[] = [];

  constructor(
    private mandatorMainService: MandatorMainService,
    private mandatorRoleRepo: MandatorRoleRepositoryService,
    public gridConfigService: GridConfigService,
    public route: ActivatedRoute,
  ) { }

  public ngOnInit(): void {
    this.gridConfig = getGridConfig();
    this.roleId = +this.route.parent.snapshot.params.id;

    const queryParams = { roleId: this.roleId };
    this.mandatorRoleRepo.fetchAll({ queryParams }).subscribe();

    this.dataSource$ = combineLatest([
      this.mandatorMainService.getLookupStream(),
      this.mandatorRoleRepo.getStream(),
      this.editMode$
    ]).pipe(map(([
      mandators,
      mandatorRoles,
      editMode,
    ]) => {
      this.selectedMandators = mandatorRoles.map(mandatorRole => mandatorRole.mandatorId);
      return orderBy(mandators
        .filter(mandator => editMode || this.mandatorIsAssignedToRole(mandator, mandatorRoles))
        .map(mandator => ({
          ...mandator,
          _checked: editMode && this.mandatorIsAssignedToRole(mandator, mandatorRoles)
        })), ['_checked', 'no'], ['desc', 'asc']);
    }));
  }

  public mandatorIsAssignedToRole = (
    mandator: IMandatorLookupRow,
    mandatorRoles: IMandatorRole[],
  ): boolean => {
    return !!mandatorRoles
      .filter(mandatorRole => mandatorRole.roleId === this.roleId)
      .find(mandatorRole => mandatorRole.mandatorId === mandator.id);
  }

  public enterEditMode() {
    this.editMode = true;
    this.editMode$.next(true);
    this.toggleGridSelectionType();
  }

  public cancelEditing() {
    this.editMode = false;
    this.editMode$.next(false);
    this.toggleGridSelectionType();
  }

  public toggleGridSelectionType = () => {
    this.gridConfig = {
      ...this.gridConfig,
      rowSelectionType: this.editMode
        ? NvGridRowSelectionType.Checkbox
        : undefined
    };
  }

  public submit() {
    this.mandatorRoleRepo.setMandatorsForRole(this.selectedMandators, this.roleId)
      .subscribe(() => this.cancelEditing());
  }

  public rowSelectionChanged(mandators: IMandatorLookupRow[]): void {
    this.selectedMandators = mandators.map(mandator => mandator.id);
  }
}
