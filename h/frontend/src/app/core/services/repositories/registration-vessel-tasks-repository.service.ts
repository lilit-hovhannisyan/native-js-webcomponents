import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IRegistrationVesselTask, IRegistrationVesselTaskDTO, IRegistrationVesselTaskCreate } from '../../models/resources/IRegistrationVesselTask';
import { VesselKey } from '../../models/resources/IVessel';
import { VesselTaskKey } from '../../models/resources/IVesselTask';

export interface RegistrationVesselTaskParams extends RepoParams<IRegistrationVesselTask> {
  queryParams?: {
    vesselId?: VesselKey;
    until?: string;
  };
}

@Injectable({
  providedIn: 'root'
})
export class RegistrationVesselTasksRepositoryService extends AbstractRepository<
  IRegistrationVesselTask,
  IRegistrationVesselTaskDTO,
  IRegistrationVesselTaskCreate,
  RegistrationVesselTaskParams,
  VesselTaskKey
> {
  protected resourceType = 'RegistrationVesselTask';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(params: RegistrationVesselTaskParams): string {
    return `/api/registration-vessel-tasks`;
  }

  public postProcess(item: IRegistrationVesselTaskDTO): IRegistrationVesselTask { return item; }
}
