import { BaseLoggerComponent } from '@features/tracking-system';
import { Component, OnInit, Injector, Input, Output, EventEmitter } from '@angular/core';

import { MeButtonI } from '../me-button/me-button.interface';
import { MeAddListCardI } from './me-add-list-card.interface';
import { MeUserImageI } from '../me-user-image/me-user-image.interface';

@Component({
  selector: 'me-add-list-card',
  templateUrl: './me-add-list-card.component.html',
  styleUrls: ['./me-add-list-card.component.scss']
})
export class MeAddListCardComponent extends BaseLoggerComponent implements OnInit {
  componentVersion: string = '0.0.1';
  addButton: MeButtonI = { color: 'link', type: 'regular' };
  removeButton: MeButtonI = { color: 'secondary', type: 'transparent' };
  userImage!: MeUserImageI;

  @Input() dataModel!: MeAddListCardI;
  @Output() addListCardEvent: EventEmitter<{ eventName: string; payload: MeAddListCardI }> = new EventEmitter();

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.userImage = {
      type: this.dataModel.thumbnail ? 'avatar' : 'initials',
      size: 'small',
      shape: 'rounded',
      initials: this.dataModel.initials,
      imageUrl: this.dataModel.thumbnail,
      alt: ''
    };
  }

  customEvent(eventName: string, payload: MeAddListCardI): void {
    this.addListCardEvent.emit({ eventName, payload });
  }
}
