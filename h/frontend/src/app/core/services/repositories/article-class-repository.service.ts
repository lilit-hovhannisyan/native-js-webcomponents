import { Injectable } from '@angular/core';
import { RepositoryService } from '../repository.service';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IArticleClass, IArticleClassDTO, IArticleClassCreate, ArticleClassKey } from '../../models/resources/IArticleClass';

@Injectable({
  providedIn: 'root'
})
export class ArticleClassRepositoryService extends AbstractRepository<
  IArticleClass,
  IArticleClassDTO,
  IArticleClassCreate,
  RepoParams<IArticleClass>,
  ArticleClassKey
> {
  protected resourceType = 'articleType';
  public sortAsce = true;
  public sortKey = 'id';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IArticleClassDTO): IArticleClass => item;
  public getBaseUrl = () => `/api/article-classes`;
}
