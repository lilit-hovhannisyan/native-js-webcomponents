import { Component, OnInit, Input } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import {
  IMaintenanceSchedule,
  IMaintenanceScheduleCreate,
  MaintenanceScheduleKey
} from 'src/app/core/models/resources/IMaintenanceSchedule';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MaintenanceScheduleRepository } from 'src/app/core/services/repositories/maintenance-schedule-reopsitory.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { Validators } from 'src/app/core/classes/validators';
import { delay } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';

@Component({
  selector: 'nv-maintenance-detail-view',
  templateUrl: './maintenance-detail-view.component.html',
  styleUrls: ['./maintenance-detail-view.component.scss']
})
export class MaintenanceDetailViewComponent
  extends ResourceDetailView<IMaintenanceSchedule, IMaintenanceScheduleCreate>
  implements OnInit {

  protected repoParams = {};
  protected resourceDefault: IMaintenanceSchedule = {} as IMaintenanceSchedule;
  public form: FormGroup;
  public routeBackUrl = url(sitemap.system.children.settings.children.maintenance);

  constructor(
    private fb: FormBuilder,
    maintenanceScheduleRepo: MaintenanceScheduleRepository,
    baseComponentService: BaseComponentService,
    private route: ActivatedRoute,
  ) {
    super(maintenanceScheduleRepo, baseComponentService);
  }

  public ngOnInit(): void {
    this.init();

    const { id } = this.route.snapshot.params;

    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => '';

  public init(
    maintenanceSchedule: IMaintenanceSchedule = this.resourceDefault
  ): void {
    const startTimeValidators = [
      Validators.required,
      Validators.isDateInputInvalid,
    ];

    if (this.creationMode) {
      startTimeValidators.push(Validators.dateShouldBeInFuture(false));
    }

    this.initForm(
      this.fb.group({
        message: [maintenanceSchedule.message, [Validators.required]],
        startTime: [
          maintenanceSchedule.startTime,
          [...startTimeValidators],
        ],
        estimatedEndTime: [
          maintenanceSchedule.estimatedEndTime,
          [
            Validators.isDateInputInvalid,
            Validators.isAfterFieldDate('startTime', false, 'minutes')
          ]
        ]
      })
    );
  }

  public submit(): void {
    if (!this.form.valid) {
      return;
    }

    const value: IMaintenanceSchedule = this.form.value;
    const payload: IMaintenanceSchedule = this.creationMode
      ? { ...value }
      : { ...this.resource, ...value };

    this.creationMode
      ? this.createResource(payload)
      : this.updateResource(payload);
  }
}
