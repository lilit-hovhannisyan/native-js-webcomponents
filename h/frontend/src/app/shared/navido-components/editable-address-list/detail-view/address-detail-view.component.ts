import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { CompanyKey } from '../../../../core/models/resources/ICompany';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { Observable, of, ReplaySubject } from 'rxjs';
import { distinctUntilChanged, map, startWith, takeUntil, tap } from 'rxjs/operators';
import { AddressKey, IAddress } from '../../../../core/models/resources/IAddress';
import { AddressRepositoryService } from '../../../../core/services/repositories/address-repository.service';
import { CountryRepositoryService } from '../../../../core/services/repositories/country-repository.service';
import { AddressTypeRepositoryService } from '../../../../core/services/repositories/address-type-repository.service';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { ACL_ACTIONS, AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { ICountry } from 'src/app/core/models/resources/ICountry';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { NvGridConfig } from 'nv-grid';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/countries.config';
@Component({
  templateUrl: './address-detail-view.component.html',
  styleUrls: ['./address-detail-view.component.scss']
})
export class AddressDetailViewComponent implements OnInit, OnDestroy {

  @Input() public addressId?: AddressKey;
  @Input() public companyId: CompanyKey;
  @Input() public closeModal: (saveClicked?: boolean) => void;

  public countries$: Observable<ICountry[]>;
  public addressTypes$ = this.addressTypeRepo.fetchAll({});
  public allAddresses: IAddress[];
  public address: IAddress;

  public creationMode: boolean;
  public form: FormGroup;
  public isValid$: Observable<boolean>;
  public countryGridConfig: NvGridConfig = getGridConfig({}, () => false);

  public addressNode = sitemap.system.children.misc.children.companies.children.id.children.addresses;
  public addressZone = SitemapEntry.getByNode(this.addressNode).toZone();
  public wording = wording;
  public ACL_ACTIONS = ACL_ACTIONS;
  private componentDestroyed$ = new ReplaySubject<boolean>(1);
  private isTypeDefaultTemp = false;

  public get isTypeDefaultEnabled(): boolean {
    return !this.address.isTypeDefault
      && !!this.form.controls.addressTypeId.value;
  }

  constructor(
    private addressRepo: AddressRepositoryService,
    private countryRepo: CountryRepositoryService,
    private addressTypeRepo: AddressTypeRepositoryService,
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
  ) {
  }

  public ngOnInit() {
    this.countries$ = this.countryRepo.fetchAll({});
    this.creationMode = !this.addressId;
    this.addressRepo.fetchAll({
      queryParams: { companyId: this.companyId }
    }).subscribe(allAddresses => {
      this.allAddresses = allAddresses;
      // on creationMode the value of the`address` will be `undefined`
      this.address = allAddresses.find(item => item.id === this.addressId) || {} as IAddress;
      this.initForm(this.address);
    });
  }

  private initForm(address = {} as IAddress) {
    this.form = this.fb.group({
      countryId: [ address.countryId, Validators.required ],
      city: [ address.city, [ Validators.required ] ],
      street: [ address.street, [ Validators.required ] ],
      zipCode: [ address.zipCode, [ Validators.required, Validators.min(0) ] ],
      addressTypeId: [ address.addressTypeId, Validators.required ],
      isTypeDefault: [ {
        value: address.isTypeDefault,
        disabled: address.isTypeDefault || !address.addressTypeId,
      } ],
    });
    this.isTypeDefaultTemp = !!this.address.isTypeDefault;

    this.isValid$ = this.form.valueChanges.pipe(
      startWith(null),
      map(() => this.form.valid)
    );

    if (!this.authService.canUpdate(this.addressZone)) {
      this.form.disable();
    }

    this.initAddressTypeChangeListener();
  }

  private initAddressTypeChangeListener(): void {
    const { isTypeDefault, addressTypeId } = this.form.controls;
    let allOfType = this.allAddresses.filter(addr => addr.addressTypeId === addressTypeId.value);

    if (allOfType.length > 2 && isTypeDefault.value) {
      addressTypeId.disable();
    } else {
      addressTypeId.enable();
    }

    addressTypeId.valueChanges.pipe(
      distinctUntilChanged(),
      tap(typeId => {
        allOfType = this.allAddresses.filter(addr => addr.addressTypeId === typeId);
      }),
      takeUntil(this.componentDestroyed$),
    ).subscribe((typeId) => {
      if (this.address.isTypeDefault && this.address.addressTypeId === typeId) {
        isTypeDefault.disable();
      } else {
        isTypeDefault.enable();
      }

      /**
       * If there is no one type-default address,
       * the current one has to be default.
       */
      const shouldBeTypeDefault = allOfType.every(addr => {
        return !addr.isTypeDefault || addr.id === this.addressId;
      });

      isTypeDefault.setValue(shouldBeTypeDefault);
      this.isTypeDefaultTemp = isTypeDefault.value;

      if (!isTypeDefault.value) {
        if (isTypeDefault.disabled) { isTypeDefault.enable(); }
        this.confirmationService.confirm({
          wording: wording.system.newDefaultAddressMessage,
          okButtonWording: wording.general.set,
          cancelButtonWording: wording.general.cancel,
        }).subscribe(isConfirmed => isTypeDefault.setValue(isConfirmed));
      } else if (isTypeDefault.enabled) {
        isTypeDefault.disable();
      }
    });
  }

  public isTypeDefaultClickHandler(): void {
    const { isTypeDefault, addressTypeId } = this.form.controls;

    if (!isTypeDefault.value || !addressTypeId.value || isTypeDefault.disabled) {
      this.isTypeDefaultTemp = isTypeDefault.value && !!addressTypeId.value;
      isTypeDefault.setValue(this.isTypeDefaultTemp);
      return;
    }

    isTypeDefault.setValue(this.isTypeDefaultTemp);
    this.confirmationService.confirm({
      wording: wording.system.changeDefaultAddressMessage,
      okButtonWording: wording.general.set,
      cancelButtonWording: wording.general.cancel,
    }).subscribe((isConfirmed: boolean) => {
      isTypeDefault.setValue(isConfirmed);
      this.isTypeDefaultTemp = isConfirmed;
    });
  }

  public saveClicked(): void {
    const companyId = this.companyId;
    const freshAddress: IAddress = this.creationMode
      ? { ...this.form.getRawValue(), companyId }
      : { ...this.form.getRawValue(), companyId, id: this.addressId };

    if (this.creationMode) {
      this.createAddress(freshAddress);
    } else {
      this.updateAddress(freshAddress);
    }
  }

  private createAddress(address: IAddress) {
    this.addressRepo.create(address, {})
      .subscribe(() => this.closeModal(true));
  }

  private updateAddress(address: IAddress) {
    const defaultShouldBeChanged =  this.address.isTypeDefault
      && address.addressTypeId !== this.address.addressTypeId;

    const initialAllOfType = this.allAddresses.filter(addr => {
      return addr.addressTypeId === this.address.addressTypeId;
    });

    if (defaultShouldBeChanged && initialAllOfType.length > 2) { return; }

    const newDefaultAddress = initialAllOfType.find(addr => !addr.isTypeDefault);
    const updateDefault$ = defaultShouldBeChanged && newDefaultAddress
      ? this.addressRepo.update({ ...newDefaultAddress, isTypeDefault: true }, {})
      : of(null);

    updateDefault$.subscribe(() => {
      this.addressRepo.update(address, {})
        .subscribe(() => this.closeModal(true));
    });
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
