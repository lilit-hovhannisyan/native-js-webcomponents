import { SitemapNode } from './../../../../core/constants/sitemap/sitemap';
import { Component, Input, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { sitemap } from '../../../../core/constants/sitemap/sitemap';
import { IUser } from '../../../../core/models/resources/IUser';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { IGitVersion } from 'src/app/core/models/IGitVersion';
import { GitInfoService } from 'src/app/core/services/git-info.service';
import { Observable } from 'rxjs';
import { CurrentUserService } from 'src/app/shared/services/current-user.service';
@Component({
  selector: 'nv-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopbarComponent implements OnInit {

  @Input() public isOnline: boolean;
  public categories = Object.values(sitemap)
    .filter((category: SitemapNode) => !category.hidden
      && this.authService.canAccessNode(category));

  public user$: Observable<IUser>;
  public url = url;
  public version: IGitVersion;

  constructor(
    private authService: AuthenticationService,
    private gitInfoService: GitInfoService,
    private currentUserService: CurrentUserService,
  ) { }

  public ngOnInit() {
    this.version = this.gitInfoService.gitInfo;
    this.user$ = this.currentUserService.getCurrentUser();
  }

  public logout(): void {
    this.authService.logout();
  }
}
