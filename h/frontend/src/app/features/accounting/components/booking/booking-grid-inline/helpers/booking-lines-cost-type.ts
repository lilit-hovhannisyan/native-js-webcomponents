import { FormGroup, AbstractControl } from '@angular/forms';
import { IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { ICostType, CostTypeKey } from 'src/app/core/models/resources/ICostTypes';
import { IAccountCostType } from 'src/app/core/models/resources/IAccountCostType';
import { ACCOUNTING_RELATIONS } from 'src/app/core/models/enums/accounting-relations';
import { Validators } from 'src/app/core/classes/validators';
import { IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { BookingKey } from 'src/app/core/models/resources/IBooking';
import { CostTypeValidatorsService } from 'src/app/core/services/validators/cost-type-validators.service';

/**
 * field CT only active if mandator of booking line has cost accounting = true
 * AND account of booking line has cost accounting = true
*/
export const enableOrDisableCostType = (
  form: FormGroup,
  mandator: IMandatorLookup,
  bookingAccount: IBookingAccount,
  accountCostTypes: IAccountCostType[],
  costTypes: ICostType[],
  costTypeValidatorsService: CostTypeValidatorsService,
  bookingId: BookingKey
): void => {
  const costTypeNoCtrl: AbstractControl = form.get('costTypeNo');
  if (mandator?.isCostAccounting && bookingAccount?.isCostAccounting) {
    const currentBookingAccountCostTypes: IAccountCostType[] = accountCostTypes
      .filter(a => a.bookingAccountId === bookingAccount.id);

    const isBalanceAccount: boolean = bookingAccount.accountingRelation === ACCOUNTING_RELATIONS.BALANCE;
    // https://navido.atlassian.net/browse/NAVIDO-2723
    /* Dennis: "CT field is active on balance accounts (see
      https://navido.atlassian.net/jira/software/c/projects/NAVIDO/issues/NAVIDO-2685 - that US says it must not be
      active)
      US: field CT only active if mandator of booking line has
      cost accounting = true and account of booking line has cost accounting = true
    */
    if (isBalanceAccount) {
      costTypeNoCtrl.disable();
      costTypeNoCtrl.patchValue(null);
      return;
    } else {
      costTypeNoCtrl.setValidators([
        Validators.required,
        costTypeValidatorsService.doesCostTypeExist,
        costTypeValidatorsService.isCostTypeRelatedToBookingAccount,
      ]);
    }
    // if there's only one option we predefine and disable it
    if (currentBookingAccountCostTypes.length === 1) {
      const costTypeId: CostTypeKey = currentBookingAccountCostTypes[0].costTypeId;
      const costType: ICostType = costTypes.find(c => c.id === costTypeId);
      costTypeNoCtrl.patchValue(costType.no);
      costTypeNoCtrl.disable();
    } else {
      costTypeNoCtrl.enable();
    }
  } else {
    costTypeNoCtrl.disable();
    if (!bookingId) {
      costTypeNoCtrl.patchValue(null);
    }
  }
};
