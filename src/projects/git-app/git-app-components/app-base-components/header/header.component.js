import {BaseComponent} from '../../../../../library/core/base-component/base.component.js';
import {basePath} from '../../../data/base.path.js';
import {getRoutes} from '../../../data/git.routes.js';

export class Header extends BaseComponent {

  constructor() {
    super(basePath + 'app-base-components/header/header.component.css');
  }

  render = () => {
    this.headerItems = getRoutes();

    let nav = '';
    for (let item of this.headerItems) {
      if (item.showInHeader) {
        nav += `<git-app-navigation text="${item.title}" path="${item.path}"></git-app-navigation>`;
      }
    }

    const html = `
      <div class="header-container">
        <div class="header-block intro">
          Developed by: 
          <a href="https://www.linkedin.com/in/lilit-hovhannisyan-armeni/" target="_blank">
          Lilit Hovhannisyan 
          </a>
          <div>Public repo: 
            <a href="https://bitbucket.org/lilit-hovhannisyan/native-js-webcomponents/src/master/" target="_blank">here</a>
          </div> 
        </div>
        <div class="header-block">
          <div class="title">Native js web components</div>
          <navigation-block class="nav">${nav}</navigation-block>
        </div>
      </div>
    `;

    const config = {
      tagName: 'header',
      attr: {
        class: 'header',
      },
      html
    };
    const el = this.createElementWithAttributes(config);

    this.removeStaticAtributesFromElNotToRender(this, ['items']);

    this.shadow.appendChild(el);
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }
}