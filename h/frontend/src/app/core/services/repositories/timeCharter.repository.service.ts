import { VoyageKey } from '../../models/resources/IVoyage';
import { getLabelKeyByLanguage } from '../../helpers/general';
import { Injectable } from '@angular/core';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { ITimeCharterCreate, ITimeCharter, ITimeCharterDTO, TimeCharterKey } from '../../models/resources/ITimeCharter';
import { SettingsService } from '../settings.service';

export interface ITimeCharterRepoParams extends RepoParams<ITimeCharter> {
  queryParams?: {
    voyageId: VoyageKey;
    date?: string;
  };
}

@Injectable({
  providedIn: 'root'
})
export class TimeCharterRepositoryService extends AbstractRepository<
ITimeCharter,
ITimeCharterDTO,
ITimeCharterCreate,
ITimeCharterRepoParams,
TimeCharterKey
>  {
  protected resourceType = 'TimeCharter';
  protected sortKey = getLabelKeyByLanguage(this.settingsService.language);
  protected sortAsce = true;

  constructor(repositoryService: RepositoryService,
    private settingsService: SettingsService) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/time-charters';

  protected postProcess = (item: ITimeCharterDTO): ITimeCharter => item;
}
