export interface IWeather {
  coord: ICoordinates;
  weather: IWeatherData[];
  base: string;
  main: IWeatherInformationGeneral;
  visibility: number;
  wind: IWind;
  clouds: IClouds;
  dt: number;
  sys: ISystemInfo;
  id: number;
  name: string;
  cod: number;
}

export interface ICoordinates {
  lon: number;
  lat: number;
}

export interface IWeatherData {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface IWeatherInformationGeneral {
  temp: number;
  pressure: number;
  humidity: number;
  temp_min: number;
  temp_max: number;
}

export interface IWind {
  speed: number;
}

export interface IClouds {
  all: number;
}

export interface ISystemInfo {
  type: number;
  id: number;
  message: number;
  country: string;
  sunrise: number;
  sunset: number;
}
