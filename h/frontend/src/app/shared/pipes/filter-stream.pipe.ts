import { Pipe, PipeTransform } from '@angular/core';
import { filter } from 'src/app/shared/helpers/general';
import { uniqBy } from 'lodash';

@Pipe({
  name: 'filterStream'
})
export class FilterStreamPipe implements PipeTransform {
  public transform(entities: any[], filters: object, uniqueProperty: string): any[] {
    return uniqueProperty
      ? uniqBy(filter(entities, filters), uniqueProperty)
      : filter(entities, filters);
  }
}
