import { ValidationErrors, AbstractControl } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { Injectable } from '@angular/core';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';


@Injectable({
  providedIn: 'root'
})
export class CompanyValidatorsService {

  constructor(private companyRepo: CompanyRepositoryService) { }

  public doesCompanyExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundCompany = this.companyRepo.getStreamValue()
      .find(company => company.id === control.value);

    const isValid = foundCompany ? true : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      companyNotfound: {
        message: wording.accounting.notFoundCompany
      }
    };
  }

  public isCompanyActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const foundCompany = this.companyRepo.getStreamValue()
      .find(company => company.id === control.value);

    const isValid = foundCompany
      ? foundCompany.isActive
      : false;

    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      companyNotActive: {
        message: wording.accounting.notActiveCompany
      }
    };
  }
}
