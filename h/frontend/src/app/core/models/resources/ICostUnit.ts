import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';
import { CostUnitCategoryKey, ICostUnitCategory } from './ICostUnitCategory';
import { IWording } from './IWording';

export type CostUnitKey = number;

interface ISharedProps extends IWithLabels {
  no: string;
  remark?: string;
  isActive: boolean;
  costUnitCategoryId: CostUnitCategoryKey;
}

export interface ICostUnitCreate extends ISharedProps { }

export interface ICostUnitBase extends ISharedProps, IResource<CostUnitKey> { }

export interface ICostUnitDTO extends ICostUnitBase { }

export interface ICostUnit extends ICostUnitDTO, IWithLabel { }

export interface ICostUnitFull extends ICostUnit {
  costUnitCategory: ICostUnitCategory;
}

export interface ICostUnitRow extends ICostUnitFull {
  costUnitCategoryName: IWording;
}
