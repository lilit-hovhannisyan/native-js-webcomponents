import { NvColumnDataType, NvFilterControl, NvGridConfig, NvGridButtonsPosition } from 'nv-grid';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { IUser } from 'src/app/core/models/resources/IUser';
import { Operations } from 'src/app/core/models/Operations';
import { wording } from '../wording/wording';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return {
    gridName: 'usersGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'id',
        title: 'ID',
        visible: false,
      },
      {
        key: 'username',
        title: wording.general.userName,
        width: 150,
        filter: {
          values: []
        }
      },
      {
        key: 'displayName',
        title: wording.general.displayName,
        width: 150,
        filter: {
          values: []
        }
      },
      {
        key: 'lastName',
        title: wording.general.lastName,
        width: 150,
        filter: {
          values: []
        }
      },
      {
        key: 'firstName',
        title: wording.general.firstName,
        width: 150,
        filter: {
          values: []
        }
      },
      {
        key: 'tel',
        title: 'Tel',
        width: 100,
        filter: {
          values: []
        },
      },
      {
        key: 'fax',
        title: 'Fax',
        width: 100,
        filter: {
          values: []
        },
      },
      {
        key: 'email',
        title: 'Email',
        width: 145,
        filter: {
          values: []
        },
      },
      {
        key: 'lastLoggedIn',
        title: wording.general.lastLoggedIn,
        dataType: NvColumnDataType.DateTime,
        filter: {
          values: []
        },
        width: 170,
      },
      {
        key: 'location',
        title: wording.general.location,
        width: 100,
        filter: {
          values: [],
          controlType: NvFilterControl.Select
        },
      },
      {
        key: 'isActive',
        title: wording.general.isActive,
        width: 80,
        filter: {
          values: [],
          controlType: NvFilterControl.Boolean
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'edit',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (user: IUser) => actions.edit(user),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'delete',
        tooltip: wording.general.delete,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.DELETE),
        func: (user: IUser) => actions.delete(user),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: actions.add
      }
    ],
    contextMenuButtons: [
      ...canPerform(Operations.READ)
        ? getContextMenuButtonsConfig()
        : [],
      {
        icon: 'lock',
        description: wording.general.requestNewPassword,
        name: 'requestNewPassword',
        tooltip: wording.general.requestNewPassword,
        actOnEnter: false,
        actOnDoubleClick: false,
        disabled: (user: IUser) => !user.isActive,
        hidden: (user: IUser) => !user.hasEmail || !canPerform(Operations.UPDATE) || user.isSuperAdmin,
        func: (user: IUser) => actions.request(user),
      },
    ]
  };
};

