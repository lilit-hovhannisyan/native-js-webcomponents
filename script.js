//tj24kd7
let cwidth = 1500
let textsize = 30
let y = 250
let data = {};
let squareSize = 30;
let textX = (cwidth / 4)*2
let yHead= 10
let yCircle= 0
let Headlinesize= 400

let effects = {
  Euphoric: "#00ff13",
  Happy: "#3bea48",
  Relaxed:"#83F52C",
  Creative: "#26c932",
  Aroused: "#30b239",
  Energetic: "#249a2c",
  Talkative: "#00ae0d",
  Giggly: "#397D02",
  Tingly: "#01810b", 
  Focused: "#0f6b16",
  Uplifted: "#2b7c31",
  Sleepy: "#418946",
  Hungry: "#7eb082",
  Dizzy: "#5f9063",
  "Dry Mouth": "#3A6629",
  Paranoid: "#48704b",
  Anxious: "#315834",
  "Dry Eyes": "#1d361e",

}

function setup() {
  createCanvas(cwidth, 30000);
  background("black");
  textSize(textsize)
}

function preload() {
  data = loadJSON("https://strainapi.evanbusse.com/tj24kd7/strains/search/all");
  console.log(data)
}


function draw() {

  //fill ("white")
  //textSize(textsize+10)
  //text ("Effects of Canabis Strains", cwidth/3,50)


  for (let effect in effects) {
    fill ("white");
    textSize(18)
    text (effect,50, yHead+=18)
    fill (Effects[effect])
    circle (35,yCircle+=18,squareSize/2)
  }


  for (let string in data) {
    fill("white");
    textSize(textsize)
    text(string, textX, y);
    


    let sqStartX = textX - squareSize
    let sqStartY = y - textsize /2
    let squares = data[string].effects.negative
    squares = squares.concat(data[string].effects.positive)
    for (i=0; i<string.length; i+=3) {}
    for (let sq of squares) {
      console.log(sq)
      fill(Effects[sq])

      circle(sqStartX, sqStartY, squareSize)
      sqStartX -= squareSize
    }

    y += textsize;


    noLoop();
  }
  
}

draw();

/*function Text(input, x, y) {
  var output = ""; // create an empty string

  for (var i = 0; i < input.length; i += 1) {
    output += input.charAt(i) + "\n"; // add each character with a line break in between
  }

  push(); // use push and pop to restore style (in this case the change in textAlign) after displaing the text
  textAlign(CENTER, TOP); // center the characters horizontaly with CENTER and display them under the provided y with TOP
  text(output, x, y); // display the text
  pop();
}
*/
/*
let y = 0;
while (y < width) {
    fill("blue");
    square(y, 100, 20);
    y = y + 50;
  }

  for (y = 0; y <= width; y += 50) {
    fill("magenta");
    square(y, 200, 20);
  }//Էս երկուսի համար պայմաններ, քանի գույն կա էդքանի համար բաժանել երկու մասի, պայմանը գրել ըստ դրական ու բացասական ազդեցությունների։

//x<width - երբ ուզում եմ լուփը կանգնի, էլ չգծի
*/


/*
let positive = [
  {
    name: "Relaxed",
    color: "#56FF0B"
  },
  { name: "Hungry", color: "#FF563D" },
  { name: "Euphoric", color: "#CB24B3" },
  { name: "Happy", color: "#FFF216" },
  { name: "Creative", color: "#3DFFF3" },
  { name: "Energetic", color: "#FFA639" },
  { name: "Talkative", color: "#BA1BFF" },
  { name: "Uplifted", color: "#18B300" },
  { name: "Tingly", color: "#FF83E0" },
  { name: "Sleepy", color: "#3D2FB3" },
  { name: "Focused", color: "#00B868" },
  { name: "Giggly", color: "#FA9900" },
  { name: "Aroused", color: "#FA3319" }
];
*/


/*let negative = [
  { name: "Dizzy", color: "#4A7D6B" },
  { name: "Dry Mouth", color: "#6FBDA2" },
  { name: "Paranoid", color: "#FA723F" },
  { name: "Dry Eyes", color: "#D3FCED" },
  { name: "Anxious", color: "#81ADFF" }
];
*/