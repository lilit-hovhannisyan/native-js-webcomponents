import { KeyValue } from '@angular/common';
import { MeButtonI } from '../me-button/me-button.interface';
import { MeLabelI } from '../me-label/me-label-inteface';

export interface MeUserGroupCardI {
  contextMenuButton?: MeButtonI;
  contextMenuItems: KeyValue<string, string>[];
  firstName?: string;
  lastName?: string;
  invitedEmail?: string;
  status?: boolean;
  groupName?: string;
  groupDescription?: string;
  groupRole?: string;
  groups?: number;
  users?: number;
  userInitials?: string;
  oid: string;
  selected: boolean;
  selectionEnabled?: boolean;
  thumbnail: string;
  type: 'user' | 'group';
  statusLabel: MeLabelI;
}
