import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'me-table-button-with-icon',
  templateUrl: './table-button-with-icon.component.html',
  styleUrls: ['./table-button-with-icon.component.scss']
})
export class TableButtonWithIconComponent implements OnInit {
  @Input() data!: { iconName: string };
  @Output() tableDataChangeEvent = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  onClick() {
    this.tableDataChangeEvent.emit();
  }
}
