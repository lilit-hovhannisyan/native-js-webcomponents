import { IResource } from './IResource';

export interface IWording {
  de: string;
  en: string;
}

export interface IWordingEntry extends IWording {
  id: number;
  key: string;
}

export interface IWordingResource extends IWordingEntry, IResource<number> {
  key: string;
  categoryId: string;
}

export type WordingMap = {
  [key: string]: IWording
};

export type ReplacementSubjects = { [key: string]: IWording | string | number };
