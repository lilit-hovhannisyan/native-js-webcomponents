import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { DECLARATIONS, TechnicalManagementRoutingModule } from './technical-management-routing.module';
import { StevedoreDamegeDocumentEditComponent } from './components/misc/stevedore-damages/detail-view/repair-documents/upload-documents/edit-modal/edit-modal.component';
import { RepairItemsArticlesComponent } from './components/misc/stevedore-damages/detail-view/repair-documents/repair-items-articles/repair-items-articles.component';
import { VesselsComponent } from './components/misc/stevedore-damages/list-view/vessels/vessels.component';
import { RepairArticleViewComponent } from './components/misc/stevedore-damages/detail-view/repair-documents/repair-items-articles/repair-article-view/repair-article-view.component';


@NgModule({
  imports: [
    TechnicalManagementRoutingModule,
    SharedModule,
  ],
  declarations: [
    ...DECLARATIONS,
    RepairItemsArticlesComponent,
    StevedoreDamegeDocumentEditComponent,
    VesselsComponent,
    RepairArticleViewComponent,
  ],
  entryComponents: [
    StevedoreDamegeDocumentEditComponent,
    RepairItemsArticlesComponent,
    RepairArticleViewComponent,
  ]
})
export class TechnicalManagementModule { }
