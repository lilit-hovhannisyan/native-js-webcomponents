import { ISelectOption } from './../../../../../../core/models/misc/selectOption';
import { Component, OnInit } from '@angular/core';
import { ICommissionType, ICommissionTypeCreate } from 'src/app/core/models/resources/ICommissionType';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { CommissionTypeRepositoryService } from 'src/app/core/services/repositories/commission-type-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { Observable } from 'rxjs';
import { IBookingCode } from 'src/app/core/models/resources/IBookingCode';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { sortBy } from 'lodash';
import { map } from 'rxjs/operators';
import { commissionTypesGroupOptions } from 'src/app/core/services/mainServices/commissiont-type-main.service';
import { NvGridConfig } from 'nv-grid';
import { getGridConfig as bookingCodeGridConfig} from 'src/app/core/constants/nv-grid-configs/booking-codes.config';

@Component({
  selector: 'nv-commission-types-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class CommissionTypesDetailViewComponent extends ResourceDetailView<
ICommissionType,
ICommissionTypeCreate
> implements OnInit {
  protected resourceDefault = { isActive: true } as ICommissionType;

  public repoParams = {};
  public routeBackUrl = this.url(sitemap.chartering.children.misc.children.commissionTypes);
  public bookingCodes$: Observable<IBookingCode[]>;
  public groupTypes: ISelectOption[] = commissionTypesGroupOptions;
  public bookingCodeGridConfig: NvGridConfig = bookingCodeGridConfig({}, () => false);

  constructor(
    commissionTypesRepo: CommissionTypeRepositoryService,
    baseComponentService: BaseComponentService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private bookingCodeRepo: BookingCodeRepositoryService,
  ) {
    super(commissionTypesRepo, baseComponentService);
  }

  public ngOnInit() {
    this.bookingCodes$ = this.bookingCodeRepo.fetchAll({})
      .pipe(map(res => sortBy(res, [r => r[getLabelKeyByLanguage(this.settingsService.language)].toLowerCase()])));
    this.init();
    const { id } = this.route.snapshot.params;
    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => `${this.resource[this.labelKey]}`;

  public init(commissionType: ICommissionType = this.resourceDefault) {
    this.initForm(this.fb.group({
      bookingCodeId: [commissionType.bookingCodeId, Validators.number],
      code: [commissionType.code, [Validators.required, Validators.maxLength(10)]],
      labelDe: [commissionType.labelDe, Validators.maxLength(50)],
      labelEn: [commissionType.labelEn, [Validators.required, Validators.maxLength(50)]],
      definition: [commissionType.definition, Validators.maxLength(3000)],
      invoiceTextEn: [commissionType.invoiceTextEn, Validators.maxLength(200)],
      invoiceTextDe: [commissionType.invoiceTextDe, Validators.maxLength(200)],
      isActive: [commissionType.isActive],
      group: [commissionType.group, Validators.required],
      id: [commissionType.id],
    }));
  }

  public submit() {
    if (this.form.invalid) { return; }
    const { value } = this.form;

    value.id
      ? this.updateResource(value).subscribe()
      : this.createResource(value).subscribe();
  }
}
