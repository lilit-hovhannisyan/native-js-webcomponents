import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { timer } from 'rxjs';

@Component({
  templateUrl: './number-input-format.component.html',
})

export class NumberInputFormatComponent implements OnInit {

  public form: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  public ngOnInit() {
    this.form = this.fb.group({
      num: [3444444.444, []]
    });
  }
}
