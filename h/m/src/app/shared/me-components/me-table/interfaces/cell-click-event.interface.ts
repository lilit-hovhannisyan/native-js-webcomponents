interface CellClickEventI {
  data: any;
  rowOID: string;
  colIndex: number;
  parentRowOID?: string;
}

export default CellClickEventI;
