import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './routing/routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { SharedModule } from './shared/shared.module';

registerLocaleData(en);

// @see https://christianliebel.com/2017/07/import-rxjs-correctly/
// @see https://github.com/ReactiveX/rxjs/issues/3018#issuecomment-347255928

const imports = [
  HttpClientModule,
  AppRoutingModule,
  CoreModule,
  BrowserAnimationsModule,
  SharedModule.forRoot(),
];


if (navigator.webdriver) {
  imports.push(NoopAnimationsModule);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports,
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
