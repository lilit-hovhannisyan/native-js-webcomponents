import { IResource } from './IResource';
import { UserKey } from './IUser';
export type MetaKey = number;

export interface MetaData {
  remarks?: string;
  createdById?: UserKey;
  createdAt?: string;
  editedById?: UserKey;
  editedAt?: string;
}

interface ISharedProps extends MetaData {
}

export interface IMetaBase extends ISharedProps, IResource<MetaKey> { }

export interface IMetaCreate extends ISharedProps { }

export interface IMetaDTO extends IMetaBase { }

export interface IMeta extends IMetaDTO { }

