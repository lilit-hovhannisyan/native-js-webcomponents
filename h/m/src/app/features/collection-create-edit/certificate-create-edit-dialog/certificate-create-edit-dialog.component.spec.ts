import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificateCreateEditDialogComponent } from './certificate-create-edit-dialog.component';

describe('CertificateCreateEditDialogComponent', () => {
  let component: CertificateCreateEditDialogComponent;
  let fixture: ComponentFixture<CertificateCreateEditDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CertificateCreateEditDialogComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificateCreateEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
