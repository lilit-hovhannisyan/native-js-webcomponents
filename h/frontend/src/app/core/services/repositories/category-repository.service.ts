import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { ICompanyCategory, ICompanyCategoryDTO } from '../../models/resources/ICompanyCategory';
import { ICategory, ICategoryDTO, CategoryKey, ICategoryCreate } from '../../models/resources/ICategory';


export interface CategoryRepoParams extends RepoParams<ICompanyCategory> {
  queryParams?: {
    limit?: number;
    offset?: number;
    withCompanyConnection?: boolean;
  };
}

@Injectable({
  providedIn: 'root'
})
export class CategoryRepositoryService extends AbstractRepository<
ICategory,
ICategoryDTO,
ICategoryCreate,
CategoryRepoParams,
CategoryKey
>  {
  protected resourceType = 'Category';
  public sortAsce = true;
  public sortKey = 'id';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: ICategoryDTO): ICategory => item;

  public getBaseUrl = (): string => '/api/company-categories/';
}
