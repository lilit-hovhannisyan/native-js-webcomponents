import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { Validators } from 'src/app/core/classes/validators';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { ICharteringExpense, ICharteringExpenseCreate } from 'src/app/core/models/resources/ICharteringExpense';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { ExpensesRepositoryService } from 'src/app/core/services/repositories/expenses-repository.service';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';

@Component({
  templateUrl: './expenses-remarks.component.html',
  styleUrls: ['./expenses-remarks.component.scss'],
})
export class ExpensesRemarksComponent extends ResourceDetailView<ICharteringExpense, ICharteringExpenseCreate> implements OnInit {
  protected repoParams = {};
  protected resourceDefault = { isActive: true } as ICharteringExpense;
  public routeBackUrl = this.url(sitemap.chartering.children.misc.children.expenses);
  public formElement: ElementRef;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    expensesRepo: ExpensesRepositoryService,
    baseComponentService: BaseComponentService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef
  ) {
    super(expensesRepo, baseComponentService);
  }

  public ngOnInit() {
    this.loadResource(+this.route.parent.snapshot.params.id).subscribe();
  }

  public init(expense: ICharteringExpense = this.resourceDefault): void {
    this.initForm(this.fb.group({
      remark: [expense.remark, Validators.maxLength(500)],
      lastChange: [expense.lastChange, Validators.maxLength(50)],
      change: [expense.change, Validators.maxLength(50)],
    }));
  }


  public submit(): void {
    if (!this.form.valid) {
      return;
    }

    this.updateResource({ ...this.resource, ...this.form.value });
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }

  public getTitle = () => this.resource.shortName;
}
