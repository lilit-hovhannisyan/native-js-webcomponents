import { Component, OnInit } from '@angular/core';
import { IMandator, IMandatorCreate, IMandatorFull } from 'src/app/core/models/resources/IMandator';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { tap, switchMap } from 'rxjs/operators';
import { DebitorCreditorRepositoryService } from '../../../../../../../core/services/repositories/debitor-creditor-repository.service';
import { DEBITOR_CREDITOR } from 'src/app/core/models/enums/debitor-creditor';
import { IDebitorCreditor } from 'src/app/core/models/resources/IDebitorCreditor';
import { MandatorMainService } from 'src/app/core/services/mainServices/mandator-main.service';

interface IDebitorCreditorWithCompanyName extends IDebitorCreditor {
  companyName: string;
}

@Component({
  selector: 'nv-mandator-corporate',
  templateUrl: './mandator-corporate.component.html',
  styleUrls: ['./mandator-corporate.component.scss']
})
export class MandatorCorporateComponent extends ResourceDetailView<
  IMandator,
  IMandatorCreate
> implements OnInit {
  protected repoParams = {};
  protected resourceDefault = { isActive: true } as IMandator;
  public routeBackUrl?: string;
  public mandator: IMandatorFull;
  public debitorCreditor = {} as IDebitorCreditorWithCompanyName;
  public DEBITOR_CREDITOR = DEBITOR_CREDITOR;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private mandatorRepo: MandatorRepositoryService,
    private mandatorService: MandatorMainService,
    private debitorCreditorRepo: DebitorCreditorRepositoryService,
    public baseComponentService: BaseComponentService,
  ) {
    super(mandatorRepo, baseComponentService);
  }

  public getTitle = () => '';

  public ngOnInit() {
    const id = this.route.parent.snapshot.params.id;
    this.mandatorService.fetchOne(+id, {})
    .pipe(
      tap(mandator => {
        this.mandator = mandator;
        this.initializeResource(mandator);
      }),
      switchMap(mandator => this.debitorCreditorRepo.getByCompanyId(mandator.companyId))
    ).subscribe(debitorCreditors => {
        this.debitorCreditor = {
          ...debitorCreditors[0],
          companyName: this.mandator.company.companyName
        };
        this.form.get('debitorCreditor').setValue(this.debitorCreditor.no);
    });
  }

  public init(mandator = {} as IMandator): void {
    this.initForm(this.fb.group({
      hasCorporateRelevance: [mandator.hasCorporateRelevance],
      corporateAccountingReference: [mandator.corporateAccountingReference],
      debitorCreditor: [this.debitorCreditor ? this.debitorCreditor.no : null]
    }));
    this.disableField('debitorCreditor');
  }

  public submit(): void {
    if (this.form.invalid) { return; }
    this.updateResource({...this.mandator, ...this.form.value}).subscribe();
  }
}
