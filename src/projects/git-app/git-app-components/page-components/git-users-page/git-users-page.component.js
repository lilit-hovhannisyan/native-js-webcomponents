import { traverseInShadowAndGetElement} from '../../../../../library/helpers/random.helpers.js';
import {BaseComponent} from '../../../../../library/core/base-component/base.component.js';
import {basePath} from '../../../data/base.path.js';

export class GitUsers extends BaseComponent {

  constructor() {
    super(basePath + 'page-components/git-users-page/git-users-page.component.css');
  }

  render = () => {
    const config = {
      tagName: 'div',
      attr: {
        class: 'git-user'
      },
      subElements: [
        {
          tagName: 'data-binding-component',
          attr: {
            placeholder: 'Search user',
            intro: 'Search results for: '
          },
        },
        {
          tagName: 'block-container',
          subElements: [
            {
              tagName: 'git-app-pagination',
              attr: {
                component: 'users'
              },
            }
          ]
        }
      ]
    };
    const el = this.createElementWithAttributes(config);

    this.shadow.appendChild(el);
  }

  connectedCallback() {
    super.connectedCallback(this.render);
    this.enterEvent();
  }

  enterEvent = (e) => {
    this.addEventListener('keypress', (event) => {
      if (event.keyCode === 13) {
        this.inputVal = traverseInShadowAndGetElement(
          traverseInShadowAndGetElement(this, 'data-binding-component'),
          'input'
        ).value;

        const container = traverseInShadowAndGetElement(this, 'block-container');
        container.innerHTML = `<git-app-pagination component="users" requestid="${this.inputVal}"></git-app-pagination>`;
      }
    });
  }

  disconnectedCallback() {
    this.removeEventListener('keypress', this.enterEvent);
  }
}