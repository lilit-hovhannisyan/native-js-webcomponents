import { Injectable } from '@angular/core';
import { WidgetType } from 'src/app/core/models/widgets/WidgetType';
import { IWidget, IWidgetDTO, IWidgetCreate } from 'src/app/core/models/widgets/IWidget';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { AbstractRepository, RepoParams } from '../../../../core/abstracts/abstract.repository';
import { HttpPost } from '../../../../core/models/http/http-post';
import { RepositoryService } from '../../../../core/services/repository.service';
import { tap, filter, distinctUntilChanged } from 'rxjs/operators';
import { TTL_FOREVER } from 'src/app/shared/services/resource-tracker.service';

@Injectable({
  providedIn: 'root'
})
export abstract class AbstractWidgetsService extends AbstractRepository<
IWidget,
IWidgetDTO,
IWidgetCreate,
RepoParams<IWidget>,
number
> {
  private dashboardWidgetItems: BehaviorSubject<IWidget[]> = new BehaviorSubject<IWidget[]>([]);

  protected resourceType = 'Widget';
  protected resourceTTL = TTL_FOREVER;
  protected useCachePerDefault = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
    // clear the subject when user is logged out
    this.repositoryService.authenticationService.isLoggedInSubject$
      .pipe(
        distinctUntilChanged(),
        filter(isLoggedIn => !isLoggedIn),
        tap(() => this.dashboardWidgetItems.next([]))
      ).subscribe();
  }

  public preProcess(w: IWidget): IWidgetDTO {
    return {
      id: w.id,
      type: w.type,
      _type: w._type,
      position: {
        x: w.x,
        y: w.y,
        width: w.cols,
        height: w.rows
      }
    };
  }

  protected postProcess(a: IWidgetDTO): IWidget {
    return {
      ...a,
      cols: a.position.width,
      rows: a.position.height,
      x: a.position.x,
      y: a.position.y
    };
  }

  public getBaseUrl() {
    return `/api/user-account/widgets`;
  }

  public addWidget(widget: IWidget): Observable<IWidget> {
    const baseUrl = `${this.getBaseUrl()}/${WidgetType[widget.type]}`;
    const _widget = this.preProcess(widget);
    const req = this.create(_widget, { baseUrl }).pipe(tap(res => this.appendWidget(({ ...widget, ...res }))));
    return req;
  }

  public getDashboardWidgets() {
    return this.dashboardWidgetItems.asObservable();
  }

  public setDashboardWidgets(widgets: IWidget[]) {
    this.dashboardWidgetItems.next(widgets);
  }

  public saveWidgets() {
    const payload = this.dashboardWidgetItems.getValue().map(this.preProcess);
    const request = new HttpPost(this.getBaseUrl(), payload);
    return this.repositoryService.httpClient.request(request);
  }

  public deleteWidget(id: number, index: number): Observable<void> {
    if (!id) {
      this.dashboardWidgetItems.getValue().splice(index, 1);
      this.dashboardWidgetItems.next(this.dashboardWidgetItems.getValue());
      return of();
    }
    this.dashboardWidgetItems.next(this.dashboardWidgetItems.getValue().filter(widget => {
      return widget.id !== id;
    }));
    return this.delete(id, {});
  }

  public appendWidget(widget: IWidget): void {
    this.dashboardWidgetItems.next([...this.dashboardWidgetItems.getValue(), { ...widget }]);
  }
}
