import { Type } from 'class-transformer';
import { BaseModel } from '../base-model/base-model';
import { CertificateModel } from '../certificate/certificate.model';
import { CertificateAssociationAttributesExtendedModel } from './certificate-association-attributes-extended.model';
import { CertificateAssociationAttributesModel } from './certificate-association-attributes.model';

export class CertificateAssociationModel extends BaseModel {
  active: boolean = false;
  @Type(() => CertificateAssociationAttributesModel)
  attributes: CertificateAssociationAttributesModel = new CertificateAssociationAttributesModel();
  @Type(() => CertificateAssociationAttributesExtendedModel)
  attributesExtended: CertificateAssociationAttributesExtendedModel = new CertificateAssociationAttributesExtendedModel();
  creatorOID: string = '';
  @Type(() => CertificateModel)
  certificate: CertificateModel = new CertificateModel();
  oid: string = '';
}
