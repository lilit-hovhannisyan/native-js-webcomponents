import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeEmptyContentComponent } from './me-empty-content.component';
import { MeButtonModule } from '../me-button/me-button.module';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';

@NgModule({
  declarations: [MeEmptyContentComponent],
  imports: [CommonModule, MeButtonModule, MeIconsProviderModule],
  exports: [MeEmptyContentComponent]
})
export class MeEmptyContentModule {}
