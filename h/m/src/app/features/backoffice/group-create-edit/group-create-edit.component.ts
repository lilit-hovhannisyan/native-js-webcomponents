import { Component, OnInit, OnDestroy, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MatHorizontalStepper } from '@angular/material/stepper';
import { SubSink } from 'subsink';
import { Subject, Subscription, from } from 'rxjs';
import { concatMap, finalize, take } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

import { BackOfficeService } from '../backoffice.web-service';
import { GlobalService } from '@core-services/global.service';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { InputAttributeService } from '@shared/services/input-attribute.service';
import { TableViewService } from '@shared/me-components/me-table/table-view.service';
import { MeAddListPopupService } from '@shared/me-components/me-popup-add-list/me-popup-add-list.service';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';

import { DomainModel } from '@shared/models/domain/domain.model';
import { LayoutGroupModel } from '@shared/models/layout/layout-group.model';
import { AttributeModelTypes } from '@shared/models/attribute/attribute.model';
import { UserModel } from '@shared/models/user/user.model';
import { RequiredConstraintState } from '@shared/models/attribute/attribute.constraint.model';

import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';
import { MeButtonI } from '@shared/me-components/me-button/me-button.interface';
import { MeSearchInputI } from '@shared/me-components/me-search-input/me-search-input-interface';
import { MeAddListCardI } from '@shared/me-components/me-add-list-card/me-add-list-card.interface';
import {
  MeTableActionButtonPropertiesI,
  MeTableCellClickEventI,
  MeTableColumnI,
  MeTableColumnOptionsI,
  MeTableDataSourceI
} from '@shared/me-components/me-table';
import { TableImageWithTitleComponent } from '@shared/me-components/me-table/table-image-with-title/table-image-with-title.component';
import { TableStringComponent } from '@shared/me-components/me-table/table-string/table-string.component';
import { MeAttributeLayoutTypes } from '@shared/me-attribute-layout-types';
import { ME_DOMAIN_APPLICATION_GROUP, ME_DOMAIN_APPLICATION_USER } from '@shared/constants';
import { getOid } from '@shared/utils';
import { ActivatedRoute, Router } from '@angular/router';
import { EntityI } from '@core-interfaces/entity.interface';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { GroupModel } from '@shared/models/groups/group.model';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { QuickSearchModel } from '@shared/models/search/quick-search.model';

@Component({
  selector: 'me-group-create-edit',
  templateUrl: './group-create-edit.component.html',
  styleUrls: ['./group-create-edit.component.scss'],
  providers: [InputAttributeService, MeAddListPopupService, TableViewService]
})
export class GroupCreateEditComponent implements OnInit, OnDestroy {
  getCardOid = getOid;
  private NUMBER_OF_ITEMS_ON_PAGE = 50;
  private subs = new SubSink();
  private appliedState = RequiredConstraintState.draft;
  private groupId: string | null = '';
  private usersIntersectionMap: Map<string, void> = new Map();
  group!: GroupModel;
  domain: DomainModel | undefined;
  groups: LayoutGroupModel[] = [];
  attributes: AttributeModelTypes[] = [];
  users: UserModel[] = [];
  generalInfoLayoutGroups!: LayoutGroupModel[];
  layoutGroups = ['generalInformation'];
  buttonNext!: { label: string; data: MeButtonI };
  buttonBack!: { label: string; data: MeButtonI };
  buttonFinish!: { label: string; data: MeButtonI };
  searchUsersFromAddListPopup!: MeSearchInputI;
  addUserListCardItems: MeAddListCardI[] = [];
  addUserListCardItemsToSend: string[] = [];
  tableItems: MeTableDataSourceI[] = [];
  tableColumns: MeTableColumnI[] = [];
  tableColumnOptions: MeTableColumnOptionsI[] = [];
  tableLoader = true;

  _cellClickEventSubject!: Subscription;
  cellClickEventSubject = new Subject<MeTableCellClickEventI>();

  groupInfoAttributeForm!: FormGroup;
  groupInfoAttributeItemMap!: Map<string, MeAttributeItem>;

  i18nStrings: { [key: string]: string } = {
    searchFor: '',
    users: '',
    generalInfo: ''
  };

  @ViewChild('stepper') stepper!: MatHorizontalStepper;
  @ViewChild('addUsersList', { read: TemplateRef }) addUsersList!: TemplateRef<Component>;

  constructor(
    private globalService: GlobalService,
    private definitionsStoreService: DefinitionsStoreService,
    private inputAttributeService: InputAttributeService,
    private meAddListPopupService: MeAddListPopupService,
    private backOfficeService: BackOfficeService,
    private tableViewService: TableViewService,
    private basicAlertService: MeBasicAlertService,
    private meSweetAlertService: MeSweetAlertService,
    private translate: TranslateService,
    private _router: Router,
    private _activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.groupId = this._activeRoute.snapshot.paramMap.get('id');
    this.globalService.activateLoader();
    this.prepareButtons();

    this.subs.sink = this.translate.get(Object.keys(this.i18nStrings)).subscribe((translations) => {
      Object.keys(this.i18nStrings).forEach((key) => {
        this.i18nStrings[key] = translations[key];
      });
      this.searchUsersFromAddListPopup = {
        placeholder: this.i18nStrings.searchFor,
        keyword: ''
      };
    });

    this.subs.sink = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        if (!this.groupId) {
          this.initializeCreate();
        } else {
          this.initializeEdit();
        }
      }
    });

    this._cellClickEventSubject = this.cellClickEventSubject.subscribe(({ colIndex, data, rowOID }) => {
      switch (colIndex) {
        case 4:
          if (data.type === 'delete') {
            this.deleteUsers(rowOID);
          } else if (data.type === 'edit') {
            this._router.navigate(['../../../user/edit', rowOID], { relativeTo: this._activeRoute });
          }
          break;
      }
    });
  }

  prepareButtons(): void {
    this.buttonNext = { label: 'Next', data: { type: 'regular', color: 'primary' } };
    this.buttonBack = { label: 'Back', data: { type: 'regular', color: 'secondary' } };
    this.buttonFinish = { label: 'Finish', data: { type: 'regular', color: 'primary' } };
  }

  initializeCreate(): void {
    this.getDomainAndGenerateForm();
  }

  initializeEdit(): void {
    this.subs.sink = this.backOfficeService.getBoGroup(encodeURI(this.groupId as string)).subscribe((response) => {
      this.group = response;
      this.getDomainAndGenerateForm();
    });
  }

  getDomainAndGenerateForm(): void {
    this.domain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_GROUP);
    this.genereateFormByDomainAttributes();
    this.globalService.deactivateLoader();
  }

  genereateFormByDomainAttributes(): void {
    if (this.domain) {
      let layoutTag = MeAttributeLayoutTypes.CREATE;

      this.attributes = this.inputAttributeService.transformAttributesArray(this.domain.attributes);

      if (this.group) {
        this.inputAttributeService.applyUpdateFormValues(this.attributes, this.group);
        layoutTag = MeAttributeLayoutTypes.UPDATE;
      }

      this.groups = this.inputAttributeService.getLayoutGroups(this.domain.layouts, layoutTag);
      this.groups = this.inputAttributeService.generateLayoutRows(this.groups);
      this.generalInfoLayoutGroups = this.getLayoutForGroups(this.layoutGroups);

      if (this.generalInfoLayoutGroups && this.generalInfoLayoutGroups.length) {
        this.groupInfoAttributeForm = this.inputAttributeService.createFormGroup(
          this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups),
          this.appliedState
        );

        const isRequired: boolean = this.inputAttributeService.isAttributeRequired(this.attributes, this.appliedState);
        const validators: ValidatorFn[] = [];

        if (isRequired) {
          validators.push(Validators.required);
        }

        this.groupInfoAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
          this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.generalInfoLayoutGroups),
          this.groupInfoAttributeForm,
          this.appliedState
        );
      }
    }
  }

  getLayoutForGroups(layoutGroupNames: string[]): LayoutGroupModel[] {
    const layoutGroups = this.groups.filter((group) => layoutGroupNames.findIndex((name) => group.internalName === name) > -1);
    return layoutGroups;
  }

  openUsersListPopUp(): void {
    this.prepareAddListUserCardData();

    this.subs.sink = this.meAddListPopupService.openPopup({ title: 'Add User' }, this.addUsersList).subscribe((popUpEvent: string) => {
      if (popUpEvent === 'addUsersAndClose') {
        if (this.addUserListCardItemsToSend && this.addUserListCardItemsToSend.length > 0) {
          this.filterUsersAndprepareTableData();
        }
      }
    });
  }

  filterUsersAndprepareTableData(): void {
    const addedUsers = this.users.filter(({ oid }) => this.addUserListCardItemsToSend.indexOf(oid) !== -1);
    this.setTableColumns();
    this.prepareTableData(addedUsers);
  }

  prepareAddListUserCardData(): void {
    this.subs.sink = this.backOfficeService.getBoUsers().subscribe((response) => {
      this.addUserListCardItems = [];
      if (response) {
        this.users = response.entities;
        this.addUserListCardItems = response.entities.map((user) => {
          const {
            attributes: { firstName, lastName, uid },
            oid
          } = user;

          return {
            firstName,
            lastName,
            uid,
            oid,
            thumbnail: 'https://baconmockup.com/500/500/',
            type: 'user',
            showButton: true,
            isInGroup: this.usersIntersectionMap.has(oid)
          };
        });
      }
    });
  }

  handleAddListCardEvent(event: { eventName: string; payload: MeAddListCardI }, index: number, cardList: MeAddListCardI[]): void {
    switch (event.eventName) {
      case 'add':
        if (event.payload && event.payload.oid) {
          if (!this.addUserListCardItemsToSend.includes(event.payload.oid)) {
            this.addUserListCardItemsToSend.push(event.payload.oid);
          }
        }
        break;
      case 'remove':
        if (event.payload && event.payload.oid) {
          if (!this.addUserListCardItemsToSend.includes(event.payload.oid)) {
            this.addUserListCardItemsToSend.push(event.payload.oid);
          } else {
            this.addUserListCardItemsToSend = this.addUserListCardItemsToSend.filter((card) => card !== event.payload.oid);
          }
        }
        break;
    }

    this.addUserListCardItemsToSend.push(event.payload.oid);
    cardList[index] = {
      ...cardList[index],
      isInGroup: true
    };
  }

  setTableColumns(): void {
    this.tableColumns = [
      {
        field: 'user',
        headerName: 'User'
      },
      {
        field: 'company',
        headerName: 'Company'
      },
      {
        field: 'groups',
        headerName: 'Groups'
      },
      {
        field: 'email',
        headerName: 'User Email'
      },
      {
        field: 'actions',
        headerName: 'Actions'
      }
    ];
  }

  prepareTableData(data: UserModel[]): void {
    this.tableLoader = true;
    const actionButtons: MeTableActionButtonPropertiesI[] = [
      {
        iconName: 'edit-2',
        type: 'edit'
      },
      {
        iconName: 'trash-2',
        type: 'delete'
      }
    ];

    const customComponents: MeTableDataSourceI[] = data.map((d) => {
      const {
        attributes: { avatar, firstName, lastName, isActive, company, email },
        numberOfGroups
      } = d;
      const generatedActionButtons = this.tableViewService.genereateActionButtons(actionButtons, isActive);

      return {
        rowOID: d.oid,
        renderedCells: [
          generatedActionButtons,
          {
            forField: 'user',
            component: TableImageWithTitleComponent,
            componentData: { url: avatar, title: `${firstName} ${lastName}` }
          },
          {
            forField: 'company',
            component: TableImageWithTitleComponent,
            componentData: { url: avatar, title: `${company}` }
          },
          {
            forField: 'groups',
            component: TableStringComponent,
            componentData: numberOfGroups
          },
          {
            forField: 'email',
            component: TableStringComponent,
            componentData: email
          }
        ]
      };
    });

    this.tableItems = customComponents;
    this.tableLoader = false;
  }

  finishSteps(): void {
    if (!this.groupId) {
      if (!this.addUserListCardItemsToSend || !this.addUserListCardItemsToSend.length) {
        this.basicAlertService.openBasicAlert({
          mode: 'error',
          title: 'Create Group',
          content: 'Please add users to group'
        });

        return;
      }

      const groupeEntity: EntityI = {
        domainName: ME_DOMAIN_APPLICATION_GROUP,
        attributes: {
          ...this.groupInfoAttributeForm.value
        }
      };

      this.globalService.activateLoader();
      this.subs.sink = this.backOfficeService.createBoGroup(groupeEntity).subscribe(
        ({ oid: groupOID }) => {
          this.addUsersToGroup(groupOID);
        },
        (error: ErrorResponseI) => {
          this.basicAlertService.openBasicAlert({
            mode: 'error',
            title: error.error.title,
            content: error.error.details
          });
        }
      );
    } else {
      const formValue = this.groupInfoAttributeForm.value;
      let isChangedVal = false;

      for (const key in formValue) {
        const formVal = formValue[key];
        // @ts-ignore
        const groupAttrVal = this.group.attributes[key];

        if (groupAttrVal !== formVal) {
          isChangedVal = true;
          break;
        }
      }

      if (isChangedVal) {
        const updatedGroupeEntity: EntityI = {
          domainName: ME_DOMAIN_APPLICATION_GROUP,
          attributes: {
            ...this.groupInfoAttributeForm.value
          }
        };

        this.subs.sink = this.backOfficeService.updateBoGroup(this.groupId, updatedGroupeEntity).subscribe(
          ({ oid }) => {
            if (!this.addUserListCardItemsToSend || !this.addUserListCardItemsToSend.length) {
              this.addUsersToGroup(oid);
            }
          },
          (error: ErrorResponseI) => {
            this.basicAlertService.openBasicAlert({
              mode: 'error',
              title: error.error.title,
              content: error.error.details
            });
          }
        );
      } else {
        this.addUsersToGroup(this.groupId);
      }
    }
  }

  getGroupUsers(): void {
    if (!this.groupId) {
      return;
    }

    this.globalService.activateLoader();
    this.setTableColumns();
    this.tableLoader = true;

    this.subs.sink = this.backOfficeService.getBoGroupUsers(this.groupId).subscribe((response) => {
      if (response && response.entities) {
        this.usersIntersectionMap.clear();
        for (const d of response.entities) {
          this.usersIntersectionMap.set(d.oid);
        }
        this.prepareTableData(response.entities);
        this.globalService.deactivateLoader();
      }
    });
  }

  addUsersToGroup(groupOID: string): void {
    let alerTitle = 'Create';
    let alertMessage = 'created';

    if (this.groupId) {
      alerTitle = 'Update';
      alertMessage = 'edited';
    }

    from(this.addUserListCardItemsToSend)
      .pipe(
        concatMap((oid) => this.backOfficeService.addUserToBoGroup(groupOID, oid)),
        finalize(() => {
          this.basicAlertService.openBasicAlert({
            mode: 'success',
            title: `${alerTitle} Group`,
            content: `The group has been successfully ${alertMessage}.`
          });

          this._router.navigate(['backoffice/users-groups'], { relativeTo: this._activeRoute });
        })
      )
      .subscribe(
        () => {},
        () => {
          this.basicAlertService.openBasicAlert({
            mode: 'error',
            title: `${alerTitle} Group`,
            content: 'Something went wrong when added users to group'
          });
        }
      );
  }

  deleteUsers(rowOID: string): void {
    if (!this.groupId) {
      if (this.addUserListCardItemsToSend.includes(rowOID)) {
        this.addUserListCardItemsToSend = this.addUserListCardItemsToSend.filter((card) => card !== rowOID);
        this.filterUsersAndprepareTableData();
      }
    } else {
      const sweetAlertModel: MeSweetAlertI = {
        mode: 'danger',
        icon: 'x-octagon',
        type: {
          name: MeSweetAlertTypeEnum.submit,
          buttons: {
            submit: 'Delete',
            cancel: 'Cancel'
          }
        },
        title: 'Delete',
        message: 'Are you sure you want to delete this user?'
      };
      this.meSweetAlertService.openMeSweetAlert(sweetAlertModel);

      this.subs.sink = this.meSweetAlertService
        .getDataBackFromSweetAlert()
        .pipe(take(1))
        .subscribe((data) => {
          if (data.confirmed) {
            this.tableLoader = true;
            if (this.groupId) {
              this.subs.sink = this.backOfficeService.deleteUserFromBoGroup(this.groupId, rowOID).subscribe(() => {
                this.getGroupUsers();
              });
            }
          }
        });
    }
  }

  handleSearchUsersFromAddListPopup(
    searchValue: string,
    skip: number = 0,
    top: number = this.NUMBER_OF_ITEMS_ON_PAGE,
    append: boolean = false
  ): void {
    this.searchUsersFromAddListPopup = {
      ...this.searchUsersFromAddListPopup,

      placeholder: this.i18nStrings.users,
      keyword: searchValue as string
    };

    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_USER,
      criteriaQuick: searchValue.trim() || undefined
    };

    this.subs.sink = this.backOfficeService.searchBoUsers(searchFilter, skip, top).subscribe((response) => {
      console.log('response', response, append);
      // this.handleUsersResponse(response, append, true);
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();

    if (this._cellClickEventSubject) {
      this._cellClickEventSubject.unsubscribe();
    }
  }
}
