import { Component, OnInit, Input, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { Observable, forkJoin, ReplaySubject, of, BehaviorSubject } from 'rxjs';
import { map, tap, catchError, distinctUntilChanged, takeUntil, startWith } from 'rxjs/operators';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { CompanyLegalEntityRepositoryService } from 'src/app/core/services/repositories/company-legal-entity-repository.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { ICompany, CompanyKey } from 'src/app/core/models/resources/ICompany';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { wording } from 'src/app/core/constants/wording/wording';
import { Validators } from 'src/app/core/classes/validators';
import { partnerModalTitles, defineManuallyLabels, contributionIncreaseLabels, shareholderReduceLabels, contributionAmountLabels, contributionAmountNewLabels, contributionChangeAmountLabels, COMPANY_LEGAL_FORM, CAPITAL_TYPE, entryWithZeroValueLabels, withdrawWithZeroValueLabels } from 'src/app/core/models/enums/company-legal-form';
import { IWording } from 'src/app/core/models/resources/IWording';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { SettingsService } from 'src/app/core/services/settings.service';
import { ContributionModalHelperService, decimalValidators } from './contribution-modal.helper.service';
import { PartnerType, ICompanyPartnerContributionFull, ICompanyPartnerContributionCreate, AmountChangeType, ICompanyPartnerContribution } from 'src/app/core/models/resources/ICompanyPartnerContribution';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { ICompanyLegalEntity } from 'src/app/core/models/resources/ICompanyLegalEntity';
import { CompanyLegalEntityMainService } from 'src/app/core/services/mainServices/company-legal-entity-main.service';
import { CompanyContributionsRepositoryService } from 'src/app/core/services/repositories/company-partner-contribution-repository.service';
import { CompaniesHelperService } from '../../../companies.helper.service';
import { ContributionMainService } from 'src/app/core/services/mainServices/company-contributions-main.service';
import { typePartnerOptions } from 'src/app/core/models/resources/ICompanyPartnerContribution';
import { TrusteePartnerContributionsRepositoryService } from 'src/app/core/services/repositories/trustee-partner-contribution-repository.service';
import { ITrusteePartnerContribution } from 'src/app/core/models/resources/ITrusteePartnerContribution';
import { orderBy } from 'lodash';
import { LegalEntityPartnersModalComponent } from '../../../../../../../shared/legal-entity-partners-modal/legal-entity-partners-modal.component';
import { COMPANY_CONTRIBUTION_TYPES } from '../../../../../../../shared/trustee-partner-contributions/enums/company-contributions';

type IPartnerChangeType = 'increasePartnerId' | 'reducePartnerId';
enum PARTNER_CHANGE_TYPE {
  increase = 'increasePartnerId',
  reduce = 'reducePartnerId',
}

@Component({
  templateUrl: './contribution-modal.component.html',
  styleUrls: ['./contribution-modal.component.scss'],
  providers: [ContributionModalHelperService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContributionModalComponent implements OnInit, OnDestroy {
  @Input() public latestContributions: Map<CompanyKey, ICompanyPartnerContributionFull>;
  @Input() public contributions: ICompanyPartnerContributionFull[] = [];
  @Input() public companyId: CompanyKey;
  public form: FormGroup;
  public wording = wording;
  public defineManuallyLabels = defineManuallyLabels;
  public shareholderIncreaseLabels = contributionIncreaseLabels;
  public shareholderReduceLabels = shareholderReduceLabels;
  public contributionAmountLabels = contributionAmountLabels;
  public contributionAmountNewLabels = contributionAmountNewLabels;
  public contributionChangeAmountLabels = contributionChangeAmountLabels;
  public entryWithZeroValueLabels = entryWithZeroValueLabels;
  public withdrawWithZeroValueLabels = withdrawWithZeroValueLabels;
  public companies$: Observable<ICompany[]>;
  public companiesWithContributions$: Observable<ICompany[]>;
  public currency: ICurrency;
  public isNotNonParValueShare = true;
  public company: ICompany;
  public legalFormKey: COMPANY_LEGAL_FORM;
  public legalEntity: ICompanyLegalEntity;
  public isPendingRequest: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public isEntryWithZeroVisible: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public isWithdrawWithZeroVisible: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public isZeroCheckboxesShown: boolean;
  public isManagingPartnersVisible: boolean;
  public partnerTypeSelectionShown = false;
  public partnerTypes = typePartnerOptions;
  private trustorsContributions: ITrusteePartnerContribution[];
  private subParticipantsContributions: ITrusteePartnerContribution[];
  public PARTNER_CHANGE_TYPE = PARTNER_CHANGE_TYPE;

  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(
    private fb: FormBuilder,
    private modalRef: NzModalRef,
    private modalService: NzModalService,
    private companyRepo: CompanyRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private companyLegalEntityRepo: CompanyLegalEntityRepositoryService,
    private companyLegalEntityMainService: CompanyLegalEntityMainService,
    private contributionRepo: CompanyContributionsRepositoryService,
    private settingsService: SettingsService,
    private helperService: ContributionModalHelperService,
    private changeDetectorRef: ChangeDetectorRef,
    private companiesHelperService: CompaniesHelperService,
    private contributionMainService: ContributionMainService,
    private trusteeContributionsRepo: TrusteePartnerContributionsRepositoryService,
  ) { }

  public ngOnInit() {
    this.companyId = this.companyId || this.companiesHelperService.company.id;

    forkJoin([
      this.companyRepo.fetchOne(this.companyId, {}),
      this.companyLegalEntityRepo.fetchOne(this.companyId, {}),
      this.currencyRepo.fetchAll({}),
      this.trusteeContributionsRepo.fetchTrusteePartnerContributions(
        this.companyId,
        COMPANY_CONTRIBUTION_TYPES.TrustorContribution
      ),
      this.trusteeContributionsRepo.fetchTrusteePartnerContributions(
        this.companyId,
        COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution
      ),
    ]).pipe(
      tap(([company, legalEntity, currencies, trustorsContributions, subParticipantsContributions]) => {
        this.company = company;
        this.companiesHelperService.setCompany(company.id);

        this.setCompanies();

        // Used to show/hide checkboxes https://navido.atlassian.net/browse/NAVIDO-2815
        this.isZeroCheckboxesShown = this.company.legalForm === COMPANY_LEGAL_FORM.LimitedPartnership // KG
          || this.company.legalForm === COMPANY_LEGAL_FORM.CivilLawPartnership // GbR
          || this.company.legalForm === COMPANY_LEGAL_FORM.ForeignCompany;

        this.isEntryWithZeroVisible.next(this.isZeroCheckboxesShown);
        this.isWithdrawWithZeroVisible.next(this.isZeroCheckboxesShown);

        this.isManagingPartnersVisible
          = this.company.legalForm === COMPANY_LEGAL_FORM.CivilLawPartnership;

        this.legalEntity = legalEntity;
        this.trustorsContributions = trustorsContributions;
        this.subParticipantsContributions = subParticipantsContributions;
        this.currency = legalEntity && legalEntity.currencyId
          ? currencies.find(c => c.id === legalEntity.currencyId)
          : currencies.find(c => c.isoCode === 'EUR');

        if (
          this.company.legalForm === COMPANY_LEGAL_FORM.StockCorporation
          && !!legalEntity.id
          && legalEntity.capitalType === CAPITAL_TYPE.NonParValueShare
        ) {
          this.legalFormKey = COMPANY_LEGAL_FORM.None;
          this.isNotNonParValueShare = false;
        } else {
          this.legalFormKey = this.company.legalForm;
        }

        this.init();
      })
    ).subscribe();
  }

  private init(): void {
    this.form = this.fb.group({
      companyId: [this.companyId],
      from: [null, [Validators.required]],
      currencyId: [{ value: this.currency.id, disabled: true }],
      currency: [{ value: this.currency.isoCode, disabled: true }],

      increasePartnerId: [null, [Validators.required]],
      increaseContribution: [{ value: 0, disabled: true }],
      increaseContributionNew: [null, [Validators.required, ...decimalValidators]],

      reducePartnerId: [null, [Validators.required]],
      reduceContribution: [{ value: 0, disabled: true }],
      reduceContributionNew: [null, [Validators.required, ...decimalValidators]],

      contributionChange: [null, [Validators.required, ...decimalValidators]],

      // partnerType
      partnerType: [null, [Validators.required]],
      partnerTypeReduce: [null],

      // Checkboxes
      entryWithZeroValue: [],
      withdrawWithZeroValue: [],
      increaseManagingPartner: [],
      reduceManagingPartner: [],
    });

    this.helperService.init(
      this.form,
      this.componentDestroyed$,
      this.companyId,
      this.trustorsContributions,
      this.subParticipantsContributions,
      this.contributions,
      this.legalEntity,
      this.isManagingPartnersVisible,
    );
    this.helperService.initValidations(this.latestContributions);
    this.helperService.initFinalContribution('increasePartnerId', 'increaseContribution');
    this.helperService.initFinalContribution('reducePartnerId', 'reduceContribution');

    this.changeDetectorRef.detectChanges();
    this.setReduceCompanies();
    this.initPartnerTypeSelection();
    this.initZeroValueCheckboxesVisibility();
  }

  private initPartnerTypeSelection(): void {
    if (this.company.legalForm !== COMPANY_LEGAL_FORM.LimitedPartnership) {
      this.form.get('partnerType').patchValue(PartnerType.None);
      this.form.get('partnerTypeReduce').patchValue(PartnerType.None);
      return;
    }

    this.partnerTypeSelectionShown = true;
    this.setPartnerTypeValue(PARTNER_CHANGE_TYPE.increase);
    this.form.get('partnerTypeReduce').disable();
  }

  public setPartnerTypeValue(partnerIdType: IPartnerChangeType): void {
    const partnerId: CompanyKey = this.form.get(partnerIdType).value;
    const from: AbstractControl = this.form.get('from');
    const partnerTypeControl: AbstractControl
      = partnerIdType === PARTNER_CHANGE_TYPE.increase
        ? this.form.get('partnerType')
        : this.form.get('partnerTypeReduce');
    const selectedPartnersContributions: ICompanyPartnerContributionFull[] = orderBy(
      this.contributions.filter((c) => c.partnerId === partnerId),
      'id'
    );
    const partnerTypeValue: number = selectedPartnersContributions.length
      ? selectedPartnersContributions[selectedPartnersContributions.length - 1].partnerType
      : null;
    const finalAmount: number = this.contributionMainService.getFinalAmount(
      selectedPartnersContributions,
      from.value,
    );

    if (
      partnerId
      && this.company.legalForm === COMPANY_LEGAL_FORM.LimitedPartnership
      && partnerIdType === PARTNER_CHANGE_TYPE.increase
      && finalAmount === 0
      && !this.helperService.isIncreasePartnerInZeroValueState
    ) {
      partnerTypeControl.enable();
    } else {
      partnerTypeControl.disable();
    }

    partnerTypeControl.patchValue(partnerTypeValue);
  }

  public submit(): void {
    validateAllFormFields(this.form);
    if (this.form.invalid) { return; }

    this.companiesHelperService.checkAndCreateLegalData(this.createContributions);
  }

  private setCompanies(): void {
    this.companies$ = this.companyRepo.getStream().pipe(
      map((companies: ICompany[]) => {
        return companies.filter(c => c.id !== this.companyId);
      })
    );
  }

  private setReduceCompanies(): void {
    const { from, reducePartnerId } = this.form.controls;

    from.valueChanges.pipe(
      distinctUntilChanged(),
      startWith(from.value),
    ).subscribe(value => {
      const latestContributions: Map<CompanyKey, ICompanyPartnerContributionFull>
        = this.contributionMainService.getCompaniesLatestContributions(
          this.contributionMainService.filterContributions(this.contributions, value),
        );

      this.companiesWithContributions$ = this.companies$.pipe(
        map(companies => companies.filter(c => {
          const companyContribution: ICompanyPartnerContributionFull
            = latestContributions.get(c.id);

          return companyContribution?.contributionAmount > 0 || companyContribution?.contributionIsZero;
        })),
        tap(companies => {
          if (companies.length) {
            reducePartnerId.enable();
          } else {
            reducePartnerId.disable();
          }
        })
      );
    });
  }

  private checkAndCreateZeroValueContribution = (): Observable<ICompanyPartnerContribution> => {
    if (!this.helperService.isIncreasePartnerInZeroValueState) {
      return of(null);
    }

    const {
      from,
      increasePartnerId,
      partnerType,
    } = this.form.controls;

    const reduceData: ICompanyPartnerContributionCreate = {
      companyId: this.companyId,
      from: from.value,
      partnerId: increasePartnerId.value,
      partnerType: partnerType.value,
      contributionAmount: 0,
      contributionIsZero: true,
      amountChangeType: AmountChangeType.Decrease,
    };

    this.isPendingRequest.next(true);

    return this.contributionRepo.create(reduceData, {}).pipe(
      tap(() => this.isPendingRequest.next(false))
    );
  }

  private createContributions = (): void => {
    this.checkAndCreateZeroValueContribution().subscribe(() => {
      this.createContribution();
    });
  }

  private createContribution = (): void => {
    const {
      from,
      increasePartnerId,
      increaseContribution,
      increaseContributionNew,
      reducePartnerId,
      reduceContribution,
      reduceContributionNew,
      entryWithZeroValue,
      withdrawWithZeroValue,
      partnerType,
      partnerTypeReduce,
      increaseManagingPartner,
      reduceManagingPartner,
    } = this.form.controls;

    const contributions: ICompanyPartnerContributionCreate[] = [];
    const contributionsIncrease: ICompanyPartnerContributionCreate[] = [];
    let contributionIncrease: ICompanyPartnerContributionCreate;
    let contributionReduce: ICompanyPartnerContributionCreate;
    let partnerTypeValue: PartnerType = partnerType.value;
    let partnerTypeReduceValue: PartnerType = partnerTypeReduce.value;

    if (this.company.legalForm === COMPANY_LEGAL_FORM.CivilLawPartnership) {
      partnerTypeValue = !increaseManagingPartner.value
        ? PartnerType.None
        : PartnerType.General;

      partnerTypeReduceValue = !reduceManagingPartner.value
        ? PartnerType.None
        : PartnerType.General;
    }

    if (increasePartnerId.value) {
      const increaseData: ICompanyPartnerContributionCreate = {
        companyId: this.companyId,
        from: from.value,
        partnerId: increasePartnerId.value,
        partnerType: partnerTypeValue,
        contributionAmount: increaseContributionNew.value - increaseContribution.value,
        contributionIsZero: entryWithZeroValue.value,
        amountChangeType: AmountChangeType.Increase,
      };
      contributions.push(increaseData);
      contributionIncrease = increaseData;


      if (partnerType.value === PartnerType.General) {
        contributionsIncrease.push(increaseData);
      }
    }

    if (reducePartnerId.value) {
      const reduceData: ICompanyPartnerContributionCreate = {
        companyId: this.companyId,
        from: from.value,
        partnerType: partnerTypeReduceValue,
        partnerId: reducePartnerId.value,
        contributionAmount: reduceContributionNew.value - reduceContribution.value,
        contributionIsZero: withdrawWithZeroValue.value,
        amountChangeType: AmountChangeType.Decrease,
      };

      // if saved: in case of KG: default type partner for
      // movement=general partner (since only general partners can have a contribution of 0)
      if (entryWithZeroValue.value && this.company.legalForm === COMPANY_LEGAL_FORM.LimitedPartnership) {
        reduceData.partnerType = PartnerType.General;
      }

      contributions.push(reduceData);
      contributionReduce = reduceData;
    }

    const legalEntityRequest = this.legalEntity.id
      ? of(this.legalEntity)
      : this.companyLegalEntityMainService.createDefault(this.companyId, this.currency.id);

    this.isPendingRequest.next(true);

    forkJoin([
      legalEntityRequest,
      ...contributions.map(c => this.contributionRepo.create(c, {})),
    ])
      .pipe(
        catchError(() => of([])),
      ).subscribe(([newLegalEntity, ...newContributions]) => {
        if (newContributions.length) {
          this.modalRef.close(newContributions);
          this.companiesHelperService.setLegalDataState(!!newContributions.length);

          // add general partners in legal entity
          this.createPartnersForKg(contributionsIncrease, newLegalEntity);

          // add managing partners in legal entity
          this.createPartnersForGBR(contributionIncrease, contributionReduce, newLegalEntity);
        } else {
          this.isPendingRequest.next(false);
        }
      });
  }

  private createPartnersForKg(
    increaseDataArr: ICompanyPartnerContributionCreate[],
    legalEntity: ICompanyLegalEntity
  ): void {
    const partners = [];
    if (this.company.legalForm === COMPANY_LEGAL_FORM.LimitedPartnership) {
      increaseDataArr.forEach(i => {
        partners.push(i.partnerId);
      });

      legalEntity.partners = [
        ...legalEntity.partners,
        ...partners
      ];

      this.companyLegalEntityRepo.update(legalEntity, {}).subscribe();
    }
  }

  private createPartnersForGBR(
    contributionIncrease: ICompanyPartnerContributionCreate,
    contributionReduce: ICompanyPartnerContributionCreate,
    legalEntity: ICompanyLegalEntity,
  ): void {
    const {
      increaseManagingPartner,
      reduceManagingPartner,
    } = this.form.controls;
    const partners = [
      ...legalEntity.partners,
    ];

    if (this.isManagingPartnersVisible) {
      // Add partner
      if (
        contributionIncrease
        && !!increaseManagingPartner.value
        && !partners.includes(contributionIncrease.partnerId)
      ) {
        partners.push(contributionIncrease.partnerId);
      }

      legalEntity.partners = partners;
      this.companyLegalEntityRepo.update(legalEntity, {}).subscribe();
    }
  }

  public cancel(): void {
    this.modalRef.close();
  }

  public openPartnersModal(partnerControl: AbstractControl): void {
    const { increasePartnerId, reducePartnerId } = this.form.controls;
    const partnersIds: CompanyKey[] = [this.companyId];
    const titleWording: IWording = partnerModalTitles.get(this.company.legalForm);

    if (increasePartnerId.value) {
      partnersIds.push(increasePartnerId.value);
    }

    if (reducePartnerId.value) {
      partnersIds.push(reducePartnerId.value);
    }

    this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: titleWording[this.settingsService.language],
      nzContent: LegalEntityPartnersModalComponent,
      nzComponentParams: {
        partnersIds,
        onlyCreate: true,
      },
      nzWidth: 500,
      nzClosable: true,
    }).afterClose.subscribe((companyId: CompanyKey) => {
      if (!companyId) {
        return;
      }

      partnerControl.setValue(companyId);

      const partnerIdType: IPartnerChangeType = partnerControl === this.form.controls.increasePartnerId
        ? PARTNER_CHANGE_TYPE.increase
        : PARTNER_CHANGE_TYPE.reduce;
      this.setPartnerTypeValue(partnerIdType);
    });
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  private initZeroValueCheckboxesVisibility(): void {
    const {
      partnerType,
      partnerTypeReduce,
      entryWithZeroValue,
      withdrawWithZeroValue,
    } = this.form.controls;

    this.getZeroValueCheckboxVisibility(partnerType).subscribe(isVisible => {
      if (!isVisible) {
        entryWithZeroValue.patchValue(null);
      }
      this.isEntryWithZeroVisible.next(isVisible);
    });
    this.getZeroValueCheckboxVisibility(partnerTypeReduce).subscribe(isVisible => {
      if (!isVisible) {
        withdrawWithZeroValue.patchValue(null);
      }
      this.isWithdrawWithZeroVisible.next(isVisible);
    });
  }

  private getZeroValueCheckboxVisibility(
    partnerTypeControl: AbstractControl,
  ): Observable<boolean> {
    return partnerTypeControl.valueChanges.pipe(
      distinctUntilChanged(),
      startWith(partnerTypeControl.value),
      map((value) => this.isZeroCheckboxesShown && value !== PartnerType.Limited)
    );
  }
}
