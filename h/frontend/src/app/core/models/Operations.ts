export enum Operations {
  /**
   * right to newly create an item
   */
  CREATE = 1 << 0, // 1

  /**
   * right to access (read) an item.
   * "read" and "create" in combination gives the user
   * the right to list all items of a type.
   */
  READ = 1 << 1, // 2

  /**
   * right to modify an existing item.
   * "update" and "create" in combination gives the user
   * the right to list all items of a type.
   */
  UPDATE = 1 << 2, // 4

  /**
   * right to delete an item
   */
  DELETE = 1 << 3, // 8

  // ALL = CREATE | READ | UPDATE | DELETE // 15
}

export const getIncludingOperations = (bitmask: number): Operations[] => {
  const ops: Operations[] = [];
  Object.keys(Operations).forEach(operationName => {
    const operationNum = Operations[operationName];
    if (bitmask & operationNum) { ops.push(operationNum); }
  });
  return ops;
};

export const getOperationName = (operation: Operations): string => {
  let name = '';
  Object.keys(Operations).forEach(opName => {
    const operationNum = Operations[opName];
    if (operation === operationNum) { name = opName; }
  });
  return name;
};
