import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../../authentication/services/authentication.service';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class ResetPasswordGuard implements CanActivate {

  constructor(
    private authService: AuthenticationService,
    private userRepositoryService: UserRepositoryService,
    private router: Router
  ) { }
  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const routerState = this.router.getCurrentNavigation().extras.state;
    const systemAction = routerState && routerState.systemAction;
    const userId = route.queryParams ? route.queryParams.userId : null;
    const confirmationCode = route.queryParams ? route.queryParams.code : null;

    if (userId && confirmationCode) {
      if (!systemAction) {
        return this.userRepositoryService.isConfirmationCodeValid(userId, confirmationCode)
          .pipe(
            map(isValid => {
              if (isValid) {
                if (this.authService.isLoggedIn()) {
                  this.authService.logout(
                    {
                      url: 'auth/reset-password',
                      queryParams: route.queryParams
                    }
                  );
                  return false;
                }
                return true;
              } else {
                this.returnToHome();
                return false;
              }
            }),
            catchError((error) => {
              this.returnToHome();
              return throwError(error);
            })
          );
      }
    } else {
      this.returnToHome();
      return of(false);
    }
    return of(true);
  }

  public returnToHome() {
    this.router.navigate(['/base/main']);
  }
}
