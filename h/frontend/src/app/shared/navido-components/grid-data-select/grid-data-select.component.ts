import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';

@Component({
  selector: 'nv-grid-data-select',
  templateUrl: './grid-data-select.component.html',
  styleUrls: ['./grid-data-select.component.scss']
})
export class GridDataSelectComponent {
  @Input() public dataSource$: Observable<any>;
  @Input() public gridConfig: NvGridConfig;
}
