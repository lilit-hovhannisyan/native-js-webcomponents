import {
  Component, ChangeDetectionStrategy, Input, HostBinding, OnDestroy, Output, EventEmitter, ElementRef, OnInit,
  Optional, ViewChild, OnChanges, SimpleChanges, ChangeDetectorRef
} from '@angular/core';
import { FormGroup, FormBuilder, ValidatorFn, AbstractControl } from '@angular/forms';
import { IRegistrationVesselTask, isRegistrationTask } from 'src/app/core/models/resources/IRegistrationVesselTask';
import { wording } from 'src/app/core/constants/wording/wording';
import { combineLatest, Observable, ReplaySubject } from 'rxjs';
import { takeUntil, distinctUntilChanged, startWith, map, tap, switchMap } from 'rxjs/operators';
import { Validators } from 'src/app/core/classes/validators';
import { ColorEvent } from 'ngx-color';
import { validateAllFormFields } from 'src/app/shared/helpers/validateAllFormFields';
import { VesselTaskTodoListComponent } from 'src/app/shared/components/vessel-task-components/vessel-task-todo-list/vessel-task-todo-list.component';
import { IVesselTaskTodoItem } from 'src/app/core/models/resources/IVesselTaskTodoItem';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';
import { RegistrationVesselTasksRepositoryService } from 'src/app/core/services/repositories/registration-vessel-tasks-repository.service';
import { isEqual, isNil } from 'lodash';
import { TaskType, VesselTaskStatus } from 'src/app/core/models/enums/vessel-registration-tasks';
import { VesselTaskHelperService } from './vessel-task-helper.service';
import { VesselTaskTemplateModalComponent } from '../vessel-task-template-modal/vessel-task-template-modal.component';
import { ITaskTemplate } from 'src/app/core/models/resources/Task-Templates/ITaskTemplate';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { IVesselAccountingListItem } from 'src/app/core/models/resources/IVesselAccounting';
import { VesselTasksRepositoryService } from 'src/app/core/services/repositories/vessel-tasks-repository.service';
import { IVesselTaskFull, TaskLanguageType } from 'src/app/core/models/resources/IVesselTask';
import { VesselRegisterRepositoryService } from 'src/app/core/services/repositories/vessel-registeries-repository.service';
import { IVesselRegister, VesselRegisterKey } from 'src/app/core/models/resources/IVesselRegister';
import { ITaskTemplateTodoItem } from 'src/app/core/models/resources/Task-Templates/ITaskTemplateTodoItem';
import { CountryKey, ICountry } from 'src/app/core/models/resources/ICountry';
import { IWording } from 'src/app/core/models/resources/IWording';
import { CountryRepositoryService } from 'src/app/core/services/repositories/country-repository.service';
import { label2Wording } from 'src/app/shared/helpers/general';

/** Shortcut type alias. */
type T = IVesselTaskFull;

@Component({
  selector: 'nv-vessel-task',
  templateUrl: './vessel-task.component.html',
  styleUrls: ['./vessel-task.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VesselTaskComponent implements OnInit, OnChanges, OnDestroy {
  public wording = wording;
  /** Used only in template for permissions. */
  public ACL_ACTIONS = ACL_ACTIONS;
  /** Only emits the next value and completes when component destroyed. */
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  /** Task's fields form. */
  public form: FormGroup;
  /** Task's todos array. */
  public todos: IVesselTaskTodoItem[] = [];
  /** Keeps done tasks count. */
  public doneTodosCount = 0;
  /** Keeps flag if exists focused input to prevent drag-drop. */
  public hasFocusedInput = false;
  /** Keeps color-picker's opened state to prevent drag-drop while it's opened. */
  public isColorPickerOpened = false;
  /** Needed to prevent opening same modal multiple times. */
  private isTemplateModalOpened = false;
  /** Tasks templates data. */
  private taskTemplates: ITaskTemplate[] = [];
  /** Tasks checklists templates data. */
  /** Needed to reduce vessel-autocomplete requests. */
  public vesselsList$: Observable<IVesselAccountingListItem[]>;
  /** Registers related to selected vessel. */
  public filteredRegisters$: Observable<IVesselRegister[]>;
  /** Shows is system language chosen English or not. */
  public isSystemEnglish = false;
  /** Will be set `systemLanguageType` if initial value is not `En` or `De`. */
  private initialLanguageType: TaskLanguageType;
  /** Helper fields controls names to exclude on value change. */
  private excludedFields: Set<string> = new Set(['showEnDe']);
  /** Set which contains all new todos. */
  private newTodosSet: Set<IVesselTaskTodoItem> = new Set();
  /** */
  public selectedCountryNameWording?: IWording;

  /** Child VesselTaskTodoListComponent's instance. */
  @ViewChild(VesselTaskTodoListComponent, { static: false })
  private todoListComponent: VesselTaskTodoListComponent;

  /**
   * Emits event when delete-icon is clicked.
   * Used directly in template.
   */
  @Output() public delete: EventEmitter<T> = new EventEmitter();
  /**
   * Emits event when fold/unfold(<</>>) icon is clicked.
   * Used directly in template.
   */
  @Output() public open: EventEmitter<boolean> = new EventEmitter();
  /** Emits on every value change. */
  @Output() private valueChange: EventEmitter<T>
    = new EventEmitter<T>();

  /** Default color for current task. */
  @Input() private defaultColor = 'gray';
  /**
   * The component fetches necessary resources if the value is `true`.
   *
   * BE CAREFUL while using this flag on task-lists.
   * Each task in the list will fetch the same resources if this flag is `true`.
   * This flag needed to reduce requests in the list and should be used only with
   * a single task-component such as task-modal.
   */
  @Input() public fetchResources = false;
  /** By task type being defines which repository does the request. */
  @Input() private type: TaskType = TaskType.SimpleVesselTask;
  /** Prevents automatically request save sending on modal submit. */
  @Input() private saveModalManually = false;
  /** Disables submit button after request is sent. */
  @Input() public saveRequestIsSent = false;
  /** Disables `vesselId` field. */
  @Input() private disableVesselField = false;
  /** Task value passed from parent component. */
  @Input() public value: T = {} as T;

  /**
   * Shows if task can be edited or not.
   * Adds `.editable` css-class when component is editable.
   *
   * CAREFUL: task should be editable and opened or in creation-mode
   * to let user edit task's fields.
   *
   * Initial value is `true` when in creation-mode or in modal-view.
   */
  @HostBinding('class.editable')
  @Input()
  public editable = this.isCreateMode || this.isModal;
  /**
   * When `true` - all fields are disabled and no submit
   * button is shown on modal-view.
   */
  @HostBinding('class.readonly')
  @Input() public readonly = false;
  /** Stretches task component and its todos to 100% width. */
  @HostBinding('class.stretched')
  @Input() public stretchFully = false;

  /**
   * Adds `.opened` css-class when task is opened.
   * Also used in current component as well as in parent components.
   *
   * When on creation-mode or in modal-view the task should always
   * be opened otherwise it has no sense.
   */
  @HostBinding('class.opened') public isOpened = this.isCreateMode || this.isModal;
  /** Adds `.invalid` css-class when `isInvalid()` returns `true`. */
  @HostBinding('class.invalid') public invalid = false;
  /**
   * Returns true when current component used as modal. `this.modalRef`
   * is optional in the constructor and will be passed(and have value) only
   * when this component will be used as `entryComponent`.
   */
  @HostBinding('class.modal')
  public get isModal(): boolean {
    return !!this.modalRef;
  }
  /** Returns `true` when in creation-mode */
  @HostBinding('class.creation')
  public get isCreateMode(): boolean {
    /** When the task has no `id`, then it's creation-mode. */
    return this.form ? !this.form.value.id : false;
  }
  /** Returns `true` when the task is editable and opened, or it's in creation-mode. */
  public get isEditMode(): boolean {
    return this.isCreateMode || this.editable && this.isOpened;
  }
  /**
   * Returns current selected languageType or TaskLanguageType
   * corresponding to system selected language.
   */
  public get languageType(): TaskLanguageType {
    return this.form?.controls.languageType.value || this.initialLanguageType;
  }
  /** Returns system language converted to `TaskLanguageType`. */
  public get systemLanguageType(): TaskLanguageType {
    return this.isSystemEnglish ? TaskLanguageType.En : TaskLanguageType.De;
  }
  /** Returns the value of `EN/DE` checkbox. */
  public get showEnDe(): boolean {
    return this.form?.controls.showEnDe.value || false;
  }
  /**
   * If `EN/DE` checkbox is unchecked - should be shown only label
   * which matches with chosen system language.
   */
  public get showLabelEn(): boolean {
    return this.isEditMode && this.showEnDe || this.isSystemEnglish;
  }
  public get showLabelDe(): boolean {
    return this.isEditMode && this.showEnDe || !this.isSystemEnglish;
  }
  /**
   * If `EN/DE` checkbox is unchecked - should be shown only label
   * which matches with chosen system language.
   */
  public get showLabelEnWarning(): boolean {
    return !this.isCreateMode
      && this.isSystemEnglish
      && this.languageType !== TaskLanguageType.All
      && !this.form?.controls.labelEn.value;
  }
  public get showLabelDeWarning(): boolean {
    return !this.isCreateMode
      && !this.isSystemEnglish
      && this.languageType !== TaskLanguageType.All
      && !this.form?.controls.labelDe.value;
  }
  /** Returns color value from `form` or default value(`defaultColor`). */
  public get color(): string {
    return this.form?.controls.color?.value || this.defaultColor;
  }
  /** Should be hidden when it's in reading mode and there are no checklist items. */
  public get showFoldArrow(): boolean {
    /**
     * CONDITION:
     * 1. on creation mode should never be displayed.
     * 2. on not creation mode
     * 2.1. when it's editable (edit-button is clicked) - always should be displayed.
     * 2.2. when it's not editable - should be displayed only when has todo items.
     */
    const isVisible: boolean  = this.editable ? true : !!this.todos.length;
    return !this.isCreateMode && isVisible;
  }

  constructor(
    private fb: FormBuilder,
    private elRef: ElementRef<HTMLElement>,
    private registrationVesselTasksRepo: RegistrationVesselTasksRepositoryService,
    private vesselTaskRepo: VesselTasksRepositoryService,
    private cdr: ChangeDetectorRef,
    private modalService: NzModalService,
    public helperService: VesselTaskHelperService,
    private vesselMainService: VesselMainService,
    private vesselRegisterRepo: VesselRegisterRepositoryService,
    private countryRepo: CountryRepositoryService,
    @Optional() private modalRef?: NzModalRef,
  ) { }

  public ngOnInit(): void {
    this.handleIsEnglishChange();
    this.init(this.value);
    this.fetchAdditionalResources();
    this.fetchRequiredResources();
    this.setCountryNameMechanism();
    /** Not-editable or folded task as a modal, or as a creator has no sense. */
    this.editable = this.editable || this.isModal || this.isCreateMode;
    this.isOpened = this.isOpened || this.isModal || this.isCreateMode;
  }

  public ngOnChanges({value}: SimpleChanges): void {
    if (value && !value.isFirstChange()) {
      this.init(this.value);
    }
  }

  /** Initializes the `form`. Used once in `ngOnInit`. */
  private init(resource: Partial<T> = {}): void {
    this.disableVesselField = this.disableVesselField && !!resource.vesselId;
    this.type = this.type || resource.type || TaskType.SimpleVesselTask;
    this.todos = resource?.todos || [];
    const useStartDate: boolean = resource.useStartDate === false ? false : true;
    this.setDoneTodosCount();

    this.form = this.fb.group({
      id: [resource.id],
      labelEn: [resource.labelEn, this.getLabelValidators()],
      labelDe: [resource.labelDe, this.getLabelValidators(false)],
      start: [resource.start, [Validators.required, Validators.isDateInputInvalid]],
      end: [resource.end, [
        Validators.isDateInputInvalid,
        Validators.isAfterFieldDate('start', false),
      ]],
      languageType: [resource.languageType || this.systemLanguageType],
      vesselId: [
        { value: resource.vesselId, disabled: this.disableVesselField },
        [ Validators.required ],
      ],
      vesselRegistrationId: [
        isRegistrationTask(resource) && resource.vesselRegistrationId,
        [Validators.required],
      ],
      remark: [resource.remark],
      color: [resource.color || this.defaultColor],
      status: [{
        value: this.getValidStatus(),
        disabled: !this.isEditMode || !!this.todos.length,
      }],
      useStartDate: [useStartDate],
      type: [this.type],
      todos: [this.todos],
      sortIndex: [isRegistrationTask(resource) && resource.sortIndex],
      showEnDe: [resource.languageType === TaskLanguageType.All],
    });

    this.handleFormValueChange();
    this.handleDateboxesChanges();
    this.handleUseStartDateChange();
    this.handleEnDeChange();
    this.setInitialLanguageType(resource.languageType);
    this.setDisabledState();
    this.setRegistersDataSource();
    this.handleRegisterChange();
    this.cdr.detectChanges();
  }

  /** Keeps `isEnglish` value. */
  private handleIsEnglishChange(): void {
    this.helperService.isEnglish
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(isEnglish => this.isSystemEnglish = isEnglish);
  }

  /** Calculates and keeps done tasks count. */
  private setDoneTodosCount(): void {
    this.doneTodosCount = this.todos.filter(t => t.status === VesselTaskStatus.Done).length;
  }

  /** Sets disabled state for all todo items of the task. */
  public setDisabledState(isDisabled = this.readonly): void {
    if (!this.form) {
      return;
    }
    if (isDisabled) {
      return this.form.disable();
    }
    this.form?.enable();

    if (this.disableVesselField) {
      this.form.controls.vesselId.disable();
    }
  }

  private setStatusDisabledState(isDisabled: boolean) {
    /**
     * If task has todos, then status should be disabled
     * and calculated based on todos statuses.
     */
    const { status } = this.form.controls;
    if (this.todos.length || isDisabled) {
      status.disable();
    } else {
      status.enable();
    }
  }

  /** Handles `form`s value change to emit component's `valueChange` event. */
  private handleFormValueChange(): void {
    this.form.valueChanges.pipe(
      takeUntil(this.componentDestroyed$),
      distinctUntilChanged((x, y) => isEqual(x, y)),
    ).subscribe(() => {
      // Some fields can be disabled but required
      const value: IRegistrationVesselTask = this.form.getRawValue();

      // The reference of `this.value` shouldn't be changed.
      Object.keys(value).forEach(k => {
        if (this.excludedFields.has(k)) {
          return;
        }

        this.value[k] = value[k];

        /**
         * Delete Nils in result to prevent unnecessary differences between
         * initial data and current, because backend sends data only with existed values.
         */
        if (isNil(this.value[k])) {
          delete this.value[k];
        }
      });

      this.valueChange.emit(this.value);
      this.isInvalid();
    });
  }

  /** Toggles end-date required-validator. */
  private handleUseStartDateChange(): void {
    const { useStartDate, end } = this.form.controls;
    useStartDate.valueChanges.pipe(
      startWith(useStartDate.value),
      takeUntil(this.componentDestroyed$)
    ).subscribe(useStart => {
      if (useStart) {
        end.setValidators([Validators.isDateInputInvalid]);
      } else {
        end.setValidators([Validators.required, Validators.isDateInputInvalid]);
      }
      end.updateValueAndValidity();
    });
  }

  /** Handles dateboxes changes to remove value when it's not valid date. */
  private handleDateboxesChanges(): void {
    const {start, end} = this.form.controls;

    [start, end].forEach(control => control.valueChanges.pipe(
      takeUntil(this.componentDestroyed$),
      distinctUntilChanged(),
    ).subscribe(() => {
      if (start.errors) {
        start.setValue(null);
      }
      end.setValidators([Validators.isAfterFieldDate('start', false)]);
      end.updateValueAndValidity();
    }));
  }

  /** Sets task's color. */
  public handleColorChange($event: ColorEvent): void {
    if (this.readonly) {
      return;
    }

    this.form.controls.color.setValue($event.color.hex);
  }

  /** Folds/Unfolds task and disables/enables task if `isEditMode === false`. */
  public setOpenedState(isOpened: boolean, emitEvent = true): void {
    /**
     * 1. Creation-task should be always opened and always editable.
     * 2. If task has no todo-item then it can't be opened in
     * read-only mode(`editable === false`). If task in edit-mode it
     * always can be opened(unfolded).
     */
    if (this.isCreateMode || !(this.todos.length || this.editable)) {
      return;
    }

    this.isOpened = isOpened;
    if (emitEvent) {
      this.open.emit(isOpened);
    }

    /**
     * `isEditMode` depends from `isOpened`.
     * Status field is shown in closed-state as well.
     */
    this.setStatusDisabledState(!this.isEditMode);
    this.cdr.detectChanges();
  }

  /**
   * Keeps already opened color-picker's hover-state.
   * Used only in template.
   */
  public handleColorPickerHover(hovered: boolean): void {
    if (this.readonly) {
      return;
    }

    this.isColorPickerOpened = hovered;
  }

  /** Keeps flag if exists focused input. */
  public setFocusState(isFocused: boolean): void {
    this.hasFocusedInput = isFocused;
  }

  /** Scrolls current task into view. Used in parent component. */
  public scrollIntoView(
    arg: boolean | ScrollIntoViewOptions = { behavior: 'smooth', block: 'start' }
  ): void {
    /**
     * vessel-autocomplete fetches data when card is being opened and loading-spinner
     * is being appeared which is stopping already started but not finished scrolling.
     * setTimeout used to delay the start of scrolling.
     */
    setTimeout(() => this.elRef.nativeElement.scrollIntoView(arg));
  }

  /** Validate all fields to enable modal submit-button. */
  public handleTodoListChange(): void {
    this.valueChange.emit(this.value);
    // set `this.invalid`
    this.isInvalid();
    this.setDoneTodosCount();
  }

  /** Returns valid status for task. */
  private getValidStatus(): VesselTaskStatus {
    if (!this.todos.length) {
      return this.value.status || VesselTaskStatus.NotStarted;
    }

    const statuses: VesselTaskStatus[] = this.todos.map(t => t.status);

    const hasOpen = statuses.some(s => s === VesselTaskStatus.Open);
    const hasInProgress = statuses.some(s => s === VesselTaskStatus.InProgress);
    const areAllDone = statuses.every(s => s === VesselTaskStatus.Done);

    /**
     * 1. If exists todo with 'open' status - then task-status is 'open'.
     * 2. If all todos have status 'done' - then task-status is 'done'.
     */
    return areAllDone && VesselTaskStatus.Done
      || hasOpen && VesselTaskStatus.Open
      || hasInProgress && VesselTaskStatus.InProgress
      || VesselTaskStatus.NotStarted;
  }

  /** Set valid status. */
  public setValidStatus(): void {
    const { status } = this.form.controls;

    if (!this.todos.length) {
      return status.enable();
    }

    status.disable();
    const newStatus = this.getValidStatus();
    status.setValue(newStatus);
    this.value.status = newStatus;
  }

  /**
   * Validates `form` and all todo items one by one.
   * Used in parent components.
   */
  public validate(): void {
    validateAllFormFields(this.form);
    this.todoListComponent.validate();
  }

  /**
   * Returns `true` only  when the `form` is not valid or one and more
   * item(s) of todo-list is/are invalid(`isInvalid` method returns `true`).
   * Used in parent components.
   */
  public isInvalid(): boolean {
    return this.invalid = this.form?.invalid || this.todoListComponent?.isInvalid();
  }

  /** Closes modal task. */
  public modalCancel(): void {
    if (this.saveModalManually) {
      this.modalRef.close(this.value);
    } else {
      this.modalRef.triggerCancel();
    }

    /**
     * On modal-view after close/cancel the new added todos
     * should be removed manually to not show them when
     * the same task second time will be opened as a modal.
     */
    this.removeNewAddedTodos();
  }

  private getSaveRequest(): Observable<T> {
    if (isRegistrationTask(this.value)) {
      return this.isCreateMode
        ? this.registrationVesselTasksRepo.create(this.value, {})
        : this.registrationVesselTasksRepo.update(this.value, {});
    }

    return this.isCreateMode
      ? this.vesselTaskRepo.create(this.value, {})
      : this.vesselTaskRepo.update(this.value, {});
  }

  /** Submits modal and sends requests. */
  public modalSubmit(): void {
    this.setSortIndex();

    this.validate();
    if (this.isInvalid()) {
      return;
    }

    this.modalRef.result = this.value;

    if (this.saveModalManually) {
      this.modalRef.triggerOk();
      return;
    }
    this.saveRequestIsSent = true;

    this.getSaveRequest().subscribe((result) => {
      this.modalRef.close(result);
      this.modalRef.triggerOk();
      this.saveRequestIsSent = false;
    });
  }

  /** Keeps new todo in the set and validates task's status. */
  public handleTodoAdd(newTodo: IVesselTaskTodoItem): void {
    this.newTodosSet.add(newTodo);
    this.setValidStatus();
  }

  /** Removes all new todos from original array `task.todos`. */
  private removeNewAddedTodos(): void {
    /**
     * Each splice will change the length of array and only using
     * forEach wouldn't be enough to remove all necessary items,
     * even it can remove unnecessary items(which aren't new).
     * So the solution is to check and remove items in reverse order.
     */
    Object.keys(this.todos).reverse().forEach(index => {
      if (this.newTodosSet.has(this.todos[index])) {
        this.todos.splice(+index, 1);
      }
    });
  }

  /** Gets all tasks with `getStream` and fill `sortIndex` with `length`. */
  private setSortIndex(): void {
    const {sortIndex, vesselId } = this.form.controls;

    const tasks = this.registrationVesselTasksRepo.getStreamValue()
      .filter(t => t.vesselId === vesselId.value);

    sortIndex.setValue(tasks.length);
  }

  /** Fetches templates data without keeping anything. */
  private fetchAdditionalResources(): void {
    if (!this.fetchResources) {
      return;
    }
    /** Fetches data to be used with getStream. */
    this.vesselRegisterRepo.fetchAll({}).subscribe();
    this.helperService.fetchTemplatesResources().subscribe();
    this.vesselMainService.fetchVesselsWithClosestAccounting(true).subscribe();
    this.countryRepo.fetchAll({}).subscribe();
  }

  /** Fetches necessary resources. */
  private fetchRequiredResources(): void {
    this.vesselsList$ = this.vesselMainService.getVesselsWithClosestAccountingStream(true);

    this.helperService.getTaskTemplatesStream(this.type).subscribe(templates => {
      this.taskTemplates = templates;
    });
  }

  /** Sets filteredRegisters$ data source. */
  private setRegistersDataSource(): void {
    const control: AbstractControl = this.form.controls.vesselId;

    this.filteredRegisters$ = control.valueChanges.pipe(
      distinctUntilChanged(),
      startWith(control.value),
      map(vesselId => {
        if (!vesselId) {
          return [];
        }

        return this.vesselRegisterRepo.getStreamValue()
          .filter(r => r.vesselId === vesselId);
      })
    );
  }

  private getSelectedRegisterChange(): Observable<IVesselRegister> {
    const { vesselRegistrationId } = this.form.controls;

    return vesselRegistrationId.valueChanges.pipe(
      distinctUntilChanged(),
      startWith(vesselRegistrationId.value),
      switchMap(id => this.vesselRegisterRepo.getStream({id})),
      map(([register]) => register),
      takeUntil(this.componentDestroyed$),
    );
  }

  /** Automatically sets start & end inputs if inputs are empty. */
  private handleRegisterChange(): void  {
    const { start, end } = this.form.controls;
    this.getSelectedRegisterChange().subscribe(register => {
      if (!register) {
        return;
      }
      if (!start.value) {
        start.setValue(register.from);
      }
      if (!end.value && register.until) {
        end.setValue(register.until);
      }
      end.setValidators([Validators.isAfterFieldDate('start', false)]);
      start.updateValueAndValidity();
      end.updateValueAndValidity();
    });
  }

  private setCountryNameMechanism(): void {
    combineLatest([
      this.getSelectedRegisterChange(),
      this.countryRepo.getStream(),
    ]).pipe(
      map(([register, countries]) => {
        if (!register) {
          return null;
        }

        // Should be shown secondary||primary country name in task-name section.
        const registerCountryId: CountryKey = register.secondaryCountryId
          || register.primaryCountryId;
        return countries.find(c => c.id === registerCountryId);
      }),
      map(country => country && label2Wording(country))
    ).subscribe(name => {
      this.selectedCountryNameWording = name;
      this.cdr.detectChanges();
    });
  }

  /** Applies task-template with related checklist-template. */
  private applyTaskTemplate(template: ITaskTemplate): void {
    const { labelEn, labelDe, color, useStartDate } = this.form.controls;

    labelEn.setValue(template.labelEn);
    labelDe.setValue(template.labelDe);
    color.setValue(template.color);
    useStartDate.setValue(template.useStartDate);
    this.applyTodoTemplate(template.todos);
  }

  /** Applies checklist-template to current list. */
  private applyTodoTemplate(todos: ITaskTemplateTodoItem[]): void {
    const newTodos: IVesselTaskTodoItem[] = todos as unknown as IVesselTaskTodoItem[];
    this.todos.push(...newTodos);
    this.todoListComponent.handleTodoChange();
  }

  /** Open tasks templates modal for applying chosen template. */
  public openTaskTemplateModal(): void {
    if (this.isTemplateModalOpened) {
      return;
    }
    this.isTemplateModalOpened = true;

    this.modalService.create({
      nzContent: VesselTaskTemplateModalComponent,
      nzComponentParams: { dataSource: this.taskTemplates },
      nzClosable: false,
    }).afterClose.subscribe(result => {
      this.isTemplateModalOpened = false;
      if (!result) {
        return;
      }

      this.applyTaskTemplate(result);
    });
  }

  /** Returns corresponding validators for label(En/De) field. */
  private getLabelValidators(isEn = true): ValidatorFn[] {
    const matches: boolean = isEn
      ? this.languageType === TaskLanguageType.En || this.languageType === TaskLanguageType.All
      : this.languageType === TaskLanguageType.De || this.languageType === TaskLanguageType.All;
    return matches
      ? [Validators.required, Validators.maxLength(500)]
      : [];
  }

  /** Sets `systemLanguageType` if `languageType` is not `En` or `De`. */
  private setInitialLanguageType(languageType: TaskLanguageType): void {
    this.initialLanguageType = languageType === TaskLanguageType.En && TaskLanguageType.En
      || languageType === TaskLanguageType.De && TaskLanguageType.De
      || this.systemLanguageType;
  }

  /** Handles `EN/DE` checkbox change. */
  private handleEnDeChange(): void {
    const { showEnDe, languageType, labelEn, labelDe } = this.form.controls;

    showEnDe.valueChanges.pipe(
      distinctUntilChanged(),
      takeUntil(this.componentDestroyed$),
    ).subscribe(isChecked => {
      /**
       * If `EN/DE` checkbox is checked, then languageType is `TaskLanguageType.All`.
       * Otherwise it is initialLanguageType, which is system default when initial
       * value is not `En` or `De`.
       */
      const newType: TaskLanguageType = isChecked
        ? TaskLanguageType.All
        : this.initialLanguageType ;

      languageType.setValue(newType);
      labelEn.setValidators(this.getLabelValidators());
      labelDe.setValidators(this.getLabelValidators(false));
      labelEn.updateValueAndValidity();
      labelDe.updateValueAndValidity();
    });
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
