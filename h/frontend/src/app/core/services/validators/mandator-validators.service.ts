import { ValidationErrors, AbstractControl } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { Injectable } from '@angular/core';
import { ACCOUNTING_TYPES } from 'src/app/core/models/enums/accounting-types';
import { getFiscalCalendarDate, isMandatorTypeParentOrVessel, mandatorNoModifier, IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { MandatorRoleRepositoryService } from 'src/app/core/services/repositories/mandator-role.repository.service';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { MandatorBlockedYearsRepositoryService } from 'src/app/core/services/repositories/mandator-blocked-years-repository.service';
import { MandatorClosedYearsRepositoryService } from 'src/app/core/services/repositories/mandator-closed-year-repository.service';
import { ToCurrentDateFormatPipe } from 'src/app/shared/pipes/to-current-date-format.pipe';
import { SettingsService } from 'src/app/core/services/settings.service';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { isAccountingTypeValid } from './validators';
import { BookingFormService } from '../booking-form.service';

/** Mandator No Validations */

@Injectable({
  providedIn: 'root'
})
export class MandatorValidatorsService {

  constructor(
    private bookingFormService: BookingFormService,
    private mandatorRepo: MandatorRepositoryService,
    private mandatorRoleRepo: MandatorRoleRepositoryService,
    private mandatorBlockedYearsRepo: MandatorBlockedYearsRepositoryService,
    private userRepo: UserRepositoryService,
    private mandatorClosedYearsRepo: MandatorClosedYearsRepositoryService,
    private settingsService: SettingsService,
    private authService: AuthenticationService,
  ) { }

  public doesMandatorHaveAnyRole = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const foundRole = this.mandatorRoleRepo.getStreamValue()
      .find(role => role.mandatorId === control.value);

    const currentUser = this.authService.getUser();
    const isValid = foundRole
      || currentUser.isSuperAdmin
      || currentUser.isAdmin;

    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      mandatorHasNoRoles: {
        message: wording.accounting.mandatorHasNoRoles
      }
    };
  }

  public isMandatorActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === control.value);

    const isValid = foundMandator ? foundMandator.isActive : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      mandatorNotActive: {
        message: wording.accounting.notActiveMandator
      }
    };
  }

  public isMandatorNoActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.no === control.value);

    const isValid = foundMandator ? foundMandator.isActive : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      mandatorNotActive: {
        message: wording.accounting.notActiveMandator
      }
    };
  }

  /**
   * In booking form mandator field doesn't have mandator id, it has mandator no
   * be careful when using this validator it checks using mandator no
   */
  public validateMandatorAccountingType = (control: AbstractControl): ValidationErrors | null => {
    const bookingForm = this.bookingFormService.bookingFormSubject.value;
    if (!control.value || !bookingForm) {
      return null;
    }
    const currentAccountingType = bookingForm.get('accountingType').value;

    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.no === control.value);

    const isValid = foundMandator
      ? isAccountingTypeValid(
        currentAccountingType,
        foundMandator.accountingType
      )
      : false;
    if (isValid || !foundMandator) {
      return null;
    }

    markControlAsTouched(control);
    return {
      notValidAccountingType: {
        message: setTranslationSubjects(
          wording.accounting.notValidAccountingType,
          {
            SUBJECT: 'Mandator',
            NUMBER: mandatorNoModifier(foundMandator.no),
            NAME: ACCOUNTING_TYPES[currentAccountingType],
          },
        ),
      }
    };
  }

  public validateFirstBookingYear = (control: AbstractControl): ValidationErrors | null => {
    const bookingForm = this.bookingFormService.bookingFormSubject.value;
    if (!control.value || !bookingForm) {
      return null;
    }
    const currentYear = bookingForm.get('year').value;
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === control.value);

    if (!foundMandator) {
      return null;
    }
    const fiscalCalendarYear = getFiscalCalendarDate(new Date(), foundMandator.startFiscalYear).getFullYear();
    const isValid = currentYear >= foundMandator.firstBookingYear && currentYear <= fiscalCalendarYear;

    if (isValid) {
      return null;
    }
    markControlAsTouched(control);

    return {
      invalidBookingYear: {
        message: setTranslationSubjects(
          wording.accounting.invalidBookingYear,
          { FIRST: foundMandator.firstBookingYear, SECOND: fiscalCalendarYear },
        ),
      }
    };
  }

  public isYearBlockedForMandator = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value || !control.parent) {
      return null;
    }
    const mandatorId = +control.parent.get('mandatorId').value;
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === mandatorId);

    if (!foundMandator) {
      return null;
    }
    const bookingForm = this.bookingFormService.bookingFormSubject.value;

    const foundBlockedYear = this.mandatorBlockedYearsRepo.getStreamValue()
      .filter(mandatorBlockedBookingYear => mandatorBlockedBookingYear.mandatorId === foundMandator.id)
      .find(blockedYear => blockedYear.year === +bookingForm.get('year').value && blockedYear.period === 13);

    if (!foundBlockedYear) {
      return null;
    }

    markControlAsTouched(control);
    return {
      yearIsBlockedForMandator: {
        message: setTranslationSubjects(
          wording.accounting.yearIsBlockedForMandator,
          {
            YEAR: +bookingForm.get('year').value,
            NUMBER: mandatorNoModifier(foundMandator.no),
            USER: this.userRepo.getPropertyById(foundBlockedYear.blockedBy, 'displayName'),
            TIME: new ToCurrentDateFormatPipe(this.settingsService).transform(foundBlockedYear.blockedAt, { withTime: true })
          },
        )
      }
    };
  }

  public isYearClosedForMandator = (control: AbstractControl): ValidationErrors | null => {
    const bookingForm = this.bookingFormService.bookingFormSubject.value;
    if (!control.value || !control.parent || !bookingForm) {
      return null;
    }

    const currentYear = bookingForm.get('year').value;
    const mandatorId = +control.parent.get('mandatorId').value;
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === mandatorId);

    if (!foundMandator) {
      return null;
    }

    const foundClosedYear = this.mandatorClosedYearsRepo.getStreamValue()
      .filter(mandatorClosedBookingYear => mandatorClosedBookingYear.mandatorId === foundMandator.id)
      .find(closedYear => closedYear.year >= +currentYear && closedYear.period === 13);


    if (!foundClosedYear) {
      return null;
    }

    markControlAsTouched(control);
    return {
      yearIsClosedForMandator: {
        message: setTranslationSubjects(
          wording.accounting.yearIsClosedForMandator,
          { YEAR: currentYear, NUMBER: mandatorNoModifier(foundMandator.no) },
        )
      }
    };
  }

  public isPeriodClosedForMandator = (control: AbstractControl): ValidationErrors | null => {
    const bookingForm = this.bookingFormService.bookingFormSubject.value;
    if (
      !control.value
      || !control.parent
      || !control.parent.get('mandatorId').value
      || !bookingForm
    ) {
      return null;
    }
    const mandatorId = control.parent.get('mandatorId').value;
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === +mandatorId);

    if (!foundMandator) {
      return null;
    }

    const currentYear = bookingForm.get('year').value;
    const currentPeriod = bookingForm.get('period').value;
    const isPeriodClosed = this.mandatorClosedYearsRepo.getStreamValue()
      .filter(mandatorClosedYear => mandatorClosedYear.mandatorId === foundMandator.id)
      .some(closedYear => closedYear.year === +currentYear
        && closedYear.period >= +currentPeriod
      );

    if (!isPeriodClosed) {
      return null;
    }

    return {
      periodIsClosedForMandator: {
        message: setTranslationSubjects(
          wording.accounting.periodIsClosedForMandator,
          {
            PERIOD: currentPeriod,
            YEAR: currentYear,
            NUMBER: mandatorNoModifier(foundMandator.no)
          },
        )
      }
    };
  }

  public isPeriodBlockedForMandator = (control: AbstractControl): ValidationErrors | null => {
    const bookingForm = this.bookingFormService.bookingFormSubject.value;
    if (
      !control.value
      || !control.parent
      || !control.parent.get('mandatorId').value
      || !bookingForm
    ) {
      return null;
    }
    const mandatorId = control.parent.get('mandatorId').value;
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === +mandatorId);

    if (!foundMandator) {
      return null;
    }
    const currentYear = bookingForm.get('year').value;
    const currentPeriod = bookingForm.get('period').value;
    const foundBlockedPeriod = this.mandatorBlockedYearsRepo.getStreamValue()
      .filter(mandatorBlockedYear => mandatorBlockedYear.mandatorId === foundMandator.id)
      .find(blockedYear => blockedYear.year === +currentYear && blockedYear.period >= +currentPeriod);

    if (!foundBlockedPeriod) {
      return null;
    }

    return {
      periodIsBlockedForMandator: {
        message: setTranslationSubjects(
          wording.accounting.periodIsBlockedForMandator,
          {
            PERIOD: currentPeriod,
            YEAR: currentYear,
            NUMBER: mandatorNoModifier(foundMandator.no),
            USER: this.userRepo.getPropertyById(foundBlockedPeriod.blockedBy, 'displayName'),
            TIME: new ToCurrentDateFormatPipe(this.settingsService).transform(foundBlockedPeriod.blockedAt, { withTime: true })
          },
        )
      }
    };
  }

  public isAutoBankWorkflow = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === control.value);
    const isValid = foundMandator?.isAutoBankWorkflow
      ? true
      : false;
    if (isValid || !foundMandator) {
      return null;
    }

    markControlAsTouched(control);
    return {
      mandatorNotFound: {
        message: wording.accounting.mandatorNotAutobankWorkflow
      }
    };
  }

  public doesMandatorExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === control.value);
    const isValid = foundMandator ? true : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      mandatorNotFound: {
        message: wording.accounting.notFoundMandator
      }
    };
  }

  public doesMandatorNoExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.no === control.value);
    const isValid = foundMandator ? true : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      mandatorNotFound: {
        message: wording.accounting.notFoundMandator
      }
    };
  }

  public doesMandatorHaveVesselRelation = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundMandator: IMandatorLookup = this.mandatorRepo
      .getLookupStreamValue()
      .find(mandator => mandator.id === control.value);
    const isValid = foundMandator
      ? isMandatorTypeParentOrVessel(foundMandator.mandatorType)
      : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      mandatorHasNoParentOrVesselType: {
        message: wording.accounting.mandatorHasNoParentOrVesselType
      }
    };
  }
}
