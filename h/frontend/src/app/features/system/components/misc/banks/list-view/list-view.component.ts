import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { Observable } from 'rxjs';
import { BankRepositoryService } from '../../../../../../core/services/repositories/bank-repository.service';
import { BankMainService, IBankFull } from '../../../../../../core/services/mainServices/bank-main.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/banks.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { Router } from '@angular/router';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class BankListViewComponent implements OnInit {

  public dataSource$: Observable<IBankFull[]>;
  public banksGridConfig: NvGridConfig;

  constructor(
    private mainService: BankMainService,
    private bankRepo: BankRepositoryService,
    private confirmationService: ConfirmationService,
    public gridConfigService: GridConfigService,
    private authService: AuthenticationService,
    private router: Router
  ) { }

  public ngOnInit(): void {
    this.dataSource$ = this.mainService.getStream()
      .pipe(map(banks => this.mainService.toRows(banks)));

    const gridActions = {
      edit: (row: IBankFull) => { this.router.navigate([`${this.router.url}/${row.id}`]); },
      delete: (row: IBankFull) => this.deleteBankClicked(row),
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    this.banksGridConfig = getGridConfig(
      gridActions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  public deleteBankClicked = (bankFull: IBankFull): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: bankFull.name },
    }).subscribe(confirmed => confirmed && this.bankRepo.delete(bankFull.id, {}).subscribe());
  }
}
