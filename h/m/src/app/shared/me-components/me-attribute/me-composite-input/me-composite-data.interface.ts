export interface MeCompositeItemI {
  name: string;
  percentage: number;
}

export interface MeCompositeDataI {
  components: MeCompositeItemI[];
}
