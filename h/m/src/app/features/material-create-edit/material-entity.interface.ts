import { KeyValueI } from '@core-interfaces/key-value.interface';

export interface MaterialEntityI {
  domainName: string;
  attributes: KeyValueI<string | number | boolean | string[]>;
}
