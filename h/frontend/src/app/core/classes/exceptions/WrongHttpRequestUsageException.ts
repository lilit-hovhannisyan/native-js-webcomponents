import { AppException } from './AppException';

/**
 * Thrown if http request created wrong, consider using HttpGet, HttpPost, HttpPut,
 * HttpPatch, HttpDelete instead.
 */
export class WrongHttpRequestUsageException extends AppException {
}
