import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'lavash-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.less']
})
export class TextareaComponent {
  @Input() placeholder: string;

  @Input() placeholderType: 'hidden' | 'top' | 'bottom' | 'label' = 'hidden';
  @Input() value: string;
  @Input() noClearEvent: boolean;

  @Output() inputEvent = new EventEmitter();
  @Output() blurEvent = new EventEmitter();
  @Output() focusEvent = new EventEmitter();
  @Output() enterEvent = new EventEmitter();
  @Output() keydownEvent = new EventEmitter();
  @Output() deleteEvent = new EventEmitter();

  // custom icon OR image
  @Input() graphics?: string;
  @Input() graphicsPosition = 'left';

  onInput(event) {
    this.inputEvent.emit(event);
  }

  onFocus(event) {
      this.focusEvent.emit(event);
  }

  onKeyDown(event) {
      const keyCode = event.keyCode;

      if (keyCode === 13) {
          this.enterEvent.emit(this.value);
      }

      this.keydownEvent.emit({keyCode: keyCode, value: event.value});
  }

  onBlur(event) {
      this.blurEvent.emit(event);
  }

  onRemoveValue() {
      this.deleteEvent.emit();
  }
}
