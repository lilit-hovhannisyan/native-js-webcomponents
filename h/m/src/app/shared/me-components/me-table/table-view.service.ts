import { Injectable } from '@angular/core';
import { KeyValue } from '@angular/common';

import { AttributeModelTypes } from '@shared/models/attribute/attribute.model';
import { LayoutGroupAttributeModel } from '@shared/models/layout/layout-group-attribute.model';
import { MeAttributeTypes } from '@shared/me-attribute-types';
import { MeAttributeLayoutTypes } from '@shared/me-attribute-layout-types';
import { MeBasicAlertService } from '../me-basic-alert/me-basic-alert.service';

import { TableLabelComponent } from './table-label/table-label.component';
import { TableStringComponent } from './table-string/table-string.component';
import { TableEmailComponent } from './table-email/table-email.component';
import { TableDateComponent } from './table-date/table-date.component';
import { TableListComponent } from './table-list/table-list.component';
import { TableActionButtonsComponent } from './table-action-buttons/table-action-buttons.component';
import { TableButtonWithIconComponent } from './table-button-with-icon/table-button-with-icon.component';
import { TableContextMenuComponent } from './table-context-menu/table-context-menu.component';
import { TableImageWithTitleComponent } from './table-image-with-title/table-image-with-title.component';
import { TableSelectComponent } from './table-select/table-select.component';
import ColumnI from './interfaces/column.interface';
import DataSourceI from './interfaces/data-source.interface';
import RenderCellI from './interfaces/render-cell.interface';
import ActionButtonPropertiesI from './interfaces/action-button-properties.interface';
import GenereateColumnsI, {
  ActionsButtonComponentI,
  AttechComponentDataAndRowOIDToCellI,
  CellComponentI
} from './interfaces/table-view.interface';
import { TableComponentTypes } from './constants';

@Injectable()
export class TableViewService {
  constructor(private basicAlertService: MeBasicAlertService) {}

  genereateColumns({ layouts, attributes, customColumns }: GenereateColumnsI): ColumnI[] | undefined {
    if (!layouts.length || !layouts) {
      this.basicAlertService.openBasicAlert({
        mode: 'error',
        title: 'Table service',
        content: 'Layouts from domain is empty!'
      });
      return;
    }

    const viewLayout = layouts.find(({ tag }) => tag === MeAttributeLayoutTypes.VIEW);
    if (!viewLayout) {
      this.basicAlertService.openBasicAlert({
        mode: 'error',
        title: 'Table service',
        content: 'Layout with name VIEW does not found'
      });
      return;
    }

    const generalInformationGroup = viewLayout.groups.find(
      ({ internalName }) => internalName === 'generalInformation' || internalName === 'companyOverview'
    );
    if (!generalInformationGroup) {
      this.basicAlertService.openBasicAlert({
        mode: 'error',
        title: 'Table service',
        content: 'Group with name `generalInformation` does not found'
      });
      return;
    }

    const sortedLayoutGroupAttributes: LayoutGroupAttributeModel[] = generalInformationGroup.attributes.sort((a, b) => {
      return a.row - b.row;
    });

    const columns: ColumnI[] = sortedLayoutGroupAttributes.map(({ internalName }) => {
      const attribute = attributes.find(({ internalName: attrInternalName }) => attrInternalName === internalName);

      return {
        field: internalName,
        headerName: attribute?.displayName || internalName.charAt(0).toUpperCase() + internalName.slice(1)
      };
    });

    if (customColumns) {
      customColumns.forEach(({ field, headerName }) => {
        columns.push({ field, headerName });
      });
    }

    columns.push({
      field: 'actions',
      headerName: 'Actions'
    });

    return columns;
  }

  genereateComponentsForCell(columns: ColumnI[], domainAttributes: AttributeModelTypes[]) {
    const cellComponents: CellComponentI[] = [];

    columns.forEach(({ field }) => {
      const attribute = domainAttributes.find(({ internalName }) => internalName === field);

      if (attribute) {
        const { dataTypeClass, internalName } = attribute;
        const cellComponent = this.getComponentByType(dataTypeClass);

        if (cellComponent) {
          cellComponents.push({ forField: internalName, component: cellComponent });
        }
      }
    });

    return cellComponents;
  }

  attechComponentDataAndRowOIDToCell({
    entities,
    cellComponents,
    actionButtonProperties,
    customComponents,
    replaceComponentsByFieldName
  }: AttechComponentDataAndRowOIDToCellI): DataSourceI[] {
    const newEntities: DataSourceI[] = [];

    entities.forEach((entity) => {
      const { attributes, oid } = entity;
      const newCellComponents: RenderCellI[] = [];

      for (const key in attributes) {
        const cellComponent = cellComponents.find(({ forField }) => forField === key);

        if (cellComponent) {
          const componentData = attributes[key];
          newCellComponents.push({ ...cellComponent, componentData });
        }
      }

      if (customComponents) {
        const customComponent = customComponents.find((rc) => rc.rowOID === oid);

        if (customComponent) {
          customComponent.cells.forEach(({ componentType, fieldName, componentData }) => {
            const component = this.getComponentByType(componentType);
            if (component) {
              newCellComponents.push({
                forField: fieldName,
                component,
                componentData
              });
            }
          });
        }
      }

      if (replaceComponentsByFieldName) {
        const re = replaceComponentsByFieldName.find((rc) => rc.rowOID === oid);

        if (re) {
          re.cells.forEach((cell) => {
            const index = newCellComponents.findIndex((nc) => nc.forField === cell.fieldName);
            const component = this.getComponentByType(cell.componentType);

            if (component) {
              if (index > -1) {
                newCellComponents[index].component = component;
                newCellComponents[index].componentData = cell.componentData;
              } else {
                newCellComponents.push({
                  forField: cell.fieldName,
                  component: component,
                  componentData: cell.componentData
                });
              }
            }
          });
        }
      }

      const actionButtons = this.genereateActionButtons(actionButtonProperties, attributes?.isActive);

      newCellComponents.push({ ...actionButtons });
      newEntities.push({ renderedCells: [...newCellComponents], rowOID: oid });
    });

    return newEntities;
  }

  genereateActionButtons(buttonProperties: ActionButtonPropertiesI[], isActiveAttrVal?: boolean | undefined) {
    const actionsBtnComponent: ActionsButtonComponentI = {
      forField: 'actions',
      component: TableActionButtonsComponent,
      componentData: []
    };

    buttonProperties.forEach(({ iconName, type, contextMenuItems }) => {
      if (iconName) {
        actionsBtnComponent.componentData.push({
          component: TableButtonWithIconComponent,
          componentData: { iconName },
          type
        });
      }

      if (contextMenuItems) {
        const contextMenuItemsList: KeyValue<string, string>[] = [];

        contextMenuItems.forEach(({ isActiveAttrType, key, value }) => {
          let contextMenuItem: KeyValue<string, string>;

          if (isActiveAttrType) {
            contextMenuItem = {
              key: isActiveAttrVal ? 'deactivate' : 'activate',
              value: isActiveAttrVal ? 'Deactivate' : 'Activate'
            };
          } else {
            contextMenuItem = {
              key,
              value
            };
          }

          contextMenuItemsList.push({ ...contextMenuItem });
        });
        actionsBtnComponent.componentData.push({
          component: TableContextMenuComponent,
          componentData: { contextMenuItems: contextMenuItemsList },
          type
        });
      }
    });

    return actionsBtnComponent;
  }

  private getComponentByType(componentType: string) {
    let component;

    switch (componentType) {
      case MeAttributeTypes.BOOLEAN:
        component = TableLabelComponent;
        break;

      case MeAttributeTypes.STRING:
        component = TableStringComponent;
        break;

      case MeAttributeTypes.URL:
        component = TableStringComponent;
        break;

      case MeAttributeTypes.CHOICE:
        component = TableStringComponent;
        break;

      case MeAttributeTypes.TEXT:
        component = TableStringComponent;
        break;

      case MeAttributeTypes.INTEGER:
        component = TableStringComponent;
        break;

      case 'core.fc.datatype.DMEmail':
        component = TableEmailComponent;
        break;

      case MeAttributeTypes.DATE_TIME:
        component = TableDateComponent;
        break;

      // case MeAttributeTypes.OBJECT_REF:
      //   cellComponents.push({ forField: internalName, component: TableImageComponent });
      //   break;

      case MeAttributeTypes.MOA_LIST:
        component = TableListComponent;
        break;
      case TableComponentTypes.SELECT:
        component = TableSelectComponent;
        break;
      case TableComponentTypes.STRING:
        component = TableStringComponent;
        break;
      case TableComponentTypes.IMAGE_WITH_TITLE:
        component = TableImageWithTitleComponent;
        break;
    }

    return component;
  }
}
