import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'lavash-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.less']
})
export class ImageComponent implements OnInit, OnChanges {
  @Input() path = '';
  @Input() src: string | LazyLoadImg;
  @Input() alt?: string;

  @Input() isLazy = false;
  lazyClassName =  '';
  _src;

  imgProportion = '';

  @Output() loadEvent = new EventEmitter();

  ngOnInit() {
      this.getImgSrc();
  }

  ngOnChanges() {
     this.getImgSrc();
  }

  getImgSrc() {
      if (!this.isLazy) {
          this._src = this.path + this.src;
          return;
      }

      this._src = this.path + this.src['lazy'];
      this.lazyClassName =  'l-lazy';
  }

  onLoad(event, img) {
    this.imgProportion = img.getBoundingClientRect().width >= img.getBoundingClientRect().height ?
        'l-img-width' : 'l-img-height';

    if (this.isLazy) {
        this._src = this.path + this.src['original'];
    }

    this.loadEvent.emit(this.src);
    this.lazyClassName = '';
  }
}

interface LazyLoadImg {
  original: string;
  lazy: string;
}
