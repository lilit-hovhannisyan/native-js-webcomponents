import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MatSnackBarConfig } from '@angular/material/snack-bar';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';

// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, '/assets/i18n/', `.json?${new Date().getTime()}`);
}

import { throwIfAlreadyLoaded } from '@core-guards/module-import.guard';

import { AuthLayoutModule } from '@layouts/auth-layout/auth-layout.module';
import { MainLayoutModule } from '@layouts/main-layout/main-layout.module';
import { BackofficeLayoutModule } from '@layouts/backoffice-layout/backoffice-layout.module';

import { MeBasicAlertModule } from '@shared-components/me-basic-alert/me-basic-alert.module';
import { MeInfoAlertModule } from '@shared/me-components/me-info-alert/me-info-alert.module';
import { MeSweetAlertModule } from '@shared-components/me-sweet-alert/me-sweet-alert.module';

import { WindowRef } from './providers/window-ref.provider';
import { HttpInterceptorProviders } from './interceptors/index';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    MainLayoutModule,
    AuthLayoutModule,
    BackofficeLayoutModule,
    MeBasicAlertModule,
    MeSweetAlertModule,
    MeInfoAlertModule
  ],
  providers: [
    WindowRef,
    HttpInterceptorProviders,
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: {
        appearance: 'outline',
        floatLabel: 'auto'
      }
    },
    {
      provide: 'tracker-config',
      useValue: { appVersion: '0.0.1', trackerEnabled: true, enableDebugging: false }
    },
    {
      provide: MatSnackBarConfig,
      useValue: {
        duration: 7000,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      }
    }
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded<CoreModule>(parentModule, 'CoreModule');
  }
}
