import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { getExpensesGridConfig } from 'src/app/core/constants/nv-grid-configs/expenses.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { Operations } from 'src/app/core/models/Operations';
import { CharteringExpenseKey, ICharteringExpenseFull, ICharteringExpenseRow, ICharteringExpense } from 'src/app/core/models/resources/ICharteringExpense';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { ExpensesMainService } from 'src/app/core/services/mainServices/expenses-main.service';
import { ExpensesRepositoryService } from 'src/app/core/services/repositories/expenses-repository.service';

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss']
})
export class ExpensesListViewComponent implements OnInit {
  public expensesGridConfig: NvGridConfig;
  public dataSource$: Observable<ICharteringExpenseRow[]>;

  constructor(
    public gridConfigService: GridConfigService,
    private expensesMainService: ExpensesMainService,
    private expensesRepo: ExpensesRepositoryService,
    private router: Router,
    private confirmationService: ConfirmationService,
    private authService: AuthenticationService,
  ) { }

  public ngOnInit() {
    this.dataSource$ = this.expensesMainService.getStream({})
      .pipe(map((expenses: ICharteringExpenseFull[]) => this.expensesMainService.toRows(expenses)));

    this.expensesGridConfig = getExpensesGridConfig(
      {
        edit: (id: CharteringExpenseKey) => this.router.navigate([this.router.url, id]),
        add: () => this.router.navigate([this.router.url, 'new']),
        delete: this.deleteHandler
      },
      (operation: Operations) => this.authService.canPerformOperationOnCurrentUrl(operation),
    );
  }

  public deleteHandler = (expense: ICharteringExpense): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: '', },
    }).subscribe(confirmed => confirmed && this.expensesRepo.delete(expense.id, {}).subscribe());
  }
}
