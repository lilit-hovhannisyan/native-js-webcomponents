import { NvExcelExportInfo } from 'nv-grid';
import { accountNoModifier, getBookingAccountDescription, IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { mandatorNoModifier, getMandatorDescription, IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { getFormmattedDate } from 'src/app/shared/helpers/general';
import { wording } from 'src/app/core/constants/wording/wording';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { accountingTypeOptionsFull, ACCOUNTING_TYPES } from 'src/app/core/models/enums/accounting-types';
import { ToCurrentDateFormatPipe } from 'src/app/shared/pipes/to-current-date-format.pipe';
import { SettingsService } from 'src/app/core/services/settings.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ExcelExportService {

  constructor(
    private settingsService: SettingsService,
    private mandatorRepo: MandatorRepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService
  ) { }

  public fetchExcelExportInfo =
  ( excelExportInfo: NvExcelExportInfo,
    year: number,
    mandatorId: number,
    bookingAccountId: number,
    accountingNormId: number ): void => {
    const currentDate = new Date();
    const createdOn = new ToCurrentDateFormatPipe(this.settingsService).transform(currentDate, { withTime: true });
    const mandator: IMandatorLookup = mandatorId ? this.mandatorRepo.getLookupStreamValue()
      .find(m => m.id === mandatorId) : null;
    const bookingAccount: IBookingAccount = bookingAccountId ? this.bookingAccountRepo.getStreamValue()
      .find(account => account.id === bookingAccountId) : null;
    const language = this.settingsService.language;
    const accountingNorm: ISelectOption<ACCOUNTING_TYPES> = accountingTypeOptionsFull
      .find(option => option.value === accountingNormId);

    const createdOnString = wording.general.createdOn[language];
    const yearString = wording.general.year[language];
    const mandatorString = wording.accounting.mandator[language];
    const bookingAccountString = wording.accounting.bookingAccount[language];
    const accountingNormString = wording.accounting.accountingNorm[language];

    excelExportInfo.headerData = {};
    excelExportInfo.headerData[`${createdOnString}:`] = createdOn;
    excelExportInfo.headerData[`${yearString}:`] = year;
    excelExportInfo.headerData[`${mandatorString}:`] =
      getMandatorDescription(mandator);
    excelExportInfo.headerData[`${bookingAccountString}:`] =
      getBookingAccountDescription(bookingAccount, language);
    excelExportInfo.headerData[`${accountingNormString}:`] =
      accountingNorm ? accountingNorm.displayLabel[language] : '';

    // Adjust sign, see https://navido.atlassian.net/browse/NAVIDO-3099
    excelExportInfo.rows.forEach(row => {
      if (row['debitCreditType'] === -1) {
        row['amountTransaction'] = -row['amountTransaction'];
        row['amountLocal'] = -row['amountLocal'];
      }
    });

    excelExportInfo.fileName =
      `${mandator ? mandatorNoModifier(mandator.no) : ''}_${year}_` +
      `${bookingAccount ? accountNoModifier(bookingAccount.no) : ''}_` +
      getFormmattedDate(currentDate, 'DDMMYYYY');
  }
}


