import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { ICompanyGroup} from '../../../../../../core/models/resources/ICompanyGroup';
import { CompanyGroupRepositoryService } from '../../../../../../core/services/repositories/company-group-repository.service';
import { GridConfigService } from '../../../../../../core/services/grid-config.service';
import { ConfirmationService } from '../../../../../../core/services/confirmation.service';
import { AuthenticationService } from '../../../../../../authentication/services/authentication.service';
import { wording } from '../../../../../../core/constants/wording/wording';
import { getCompanyGroupGridConfig } from '../../../../../../core/constants/nv-grid-configs/company-groups.config';
import { CompanyLegalEntityRepositoryService } from '../../../../../../core/services/repositories/company-legal-entity-repository.service';
import { ICompanyLegalEntity } from '../../../../../../core/models/resources/ICompanyLegalEntity';

@Component({
  selector: 'nv-company-groups-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompanyGroupsListViewComponent implements OnInit {
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<ICompanyGroup[]>;

  constructor(
    private companyGroupRepo: CompanyGroupRepositoryService,
    private router: Router,
    public gridConfigService: GridConfigService,
    private confirmationService: ConfirmationService,
    private authService: AuthenticationService,
    private legalEntityRepo: CompanyLegalEntityRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.companyGroupRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.companyGroupRepo.getStream();

    const gridActions = {
      add: () => this.router.navigate([`${this.router.url}/new`]),
      edit: (group: ICompanyGroup) => this.router.navigate([`${this.router.url}/${group.id}`]),
      delete: this.deleteClicked
    };

    this.gridConfig = getCompanyGroupGridConfig(
      gridActions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  public deleteClicked = (group: ICompanyGroup) => {
    this.legalEntityRepo.fetchAll({}).subscribe(legalEntities => {
      const relatedLegalEntities: ICompanyLegalEntity[] = legalEntities
        .filter(l => l.groupOfCompanyId === group.id);

      if (relatedLegalEntities.length) {
        this.confirmationService.confirm({
          okButtonWording: wording.general.ok,
          cancelButtonWording: null,
          wording: wording.general.companyGroupIsActive,
        }).subscribe();
        return;
      }

      this.confirmationService.confirm({
        okButtonWording: wording.general.delete,
        cancelButtonWording: wording.general.cancel,
        wording: wording.general.areYouSureYouWantToDelete,
        subjects: { subject: group.displayLabel },
      }).subscribe(confirmed => confirmed && this.companyGroupRepo.delete(group.id, {}).subscribe());
    });
  }
}
