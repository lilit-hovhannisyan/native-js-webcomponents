
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IDebitorCreditorRow, IDebitorCreditor } from 'src/app/core/models/resources/IDebitorCreditor';
import { wording } from 'src/app/core/constants/wording/wording';
import { SettingsService } from 'src/app/core/services/settings.service';
import { ICompany } from 'src/app/core/models/resources/ICompany';
import { ICharteringExpense } from 'src/app/core/models/resources/ICharteringExpense';

@Injectable()
export class ExpensesHelperService {
  public editViewTitleSubject: BehaviorSubject<string> = new BehaviorSubject('');
  public editViewTitle$ = this.editViewTitleSubject.asObservable();
  constructor(
    public settingsService: SettingsService,
  ) { }

  public generateTitle = (expense?: ICharteringExpense) => {
    let title = wording.general.create[this.settingsService.language];

    if (expense) {
      const labelText = expense.displayLabel
        ? `${expense.displayLabel[this.settingsService.language]}`
        : '';

      title = labelText;
    }

    this.editViewTitleSubject.next(title);
  }
}
