import {Component, Input} from '@angular/core';
import {portfolioPath} from '../../../../../shared-layer/l-hovhanni/dal/portfolio.model';
import {ProjectsService} from '../../services/projects.service';

@Component({
  selector: 'app-img-slider',
  templateUrl: './img-slider.component.html',
  styleUrls: ['./img-slider.component.less']
})
export class ImgSliderComponent {
  @Input() sliderItem;
  @Input() sliderItems;

  alt = 'L-hovhanni image';
  portfolioPath = portfolioPath + '/';

  constructor(private projectsService: ProjectsService) {}

  onSlideEvent (event) {
    this.sliderItem = event;
    this.projectsService.setCurrentPhoto(event);
    this.projectsService.setCurrentPhotoQuery();
  }

  getImgPosition() {
      return window.innerWidth > 780 ? 'left' : 'center';
  }
}
