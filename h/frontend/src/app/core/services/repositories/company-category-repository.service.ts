import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { ICompanyCategory, ICompanyCategoryCreate, ICompanyCategoryDTO, CompanyCategoryKey } from '../../models/resources/ICompanyCategory';
import { Observable } from 'rxjs';
import { CompanyKey } from '../../models/resources/ICompany';
import { tap, map } from 'rxjs/operators';
import { HttpPut } from '../../models/http/http-put';
import { HttpResponse } from '@angular/common/http';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';
import { IResourceCollection } from '../../models/resources/IResourceCollection';


export interface CompanyCategoryRepoParams extends RepoParams<ICompanyCategory> {
  queryParams?: {
    limit?: number;
    offset?: number;
  };
  companyId: CompanyKey;
}

/**
 * To fetch all connections use service `CategoryRepositoryService`
 * `fetchAll` method with query param `withCompanyConnection=true`
 */

declare type CategoryResponse = HttpResponse<IDataResponseBody<IResourceCollection<ICompanyCategory>>>;

@Injectable({
  providedIn: 'root'
})
export class CompanyCategoryRepositoryService extends AbstractRepository<
ICompanyCategory,
ICompanyCategoryDTO,
ICompanyCategoryCreate,
CompanyCategoryRepoParams,
CompanyCategoryKey
>  {
  protected resourceType = 'CompanyCategory';
  public sortAsce = true;
  public sortKey = 'id';

  private companyId: CompanyKey;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: ICompanyCategoryDTO): ICompanyCategory => item;

  public getBaseUrl(params: CompanyCategoryRepoParams): string {
    return `/api/companies/${params.companyId}/categories`;
  }

  public updateCategories(categories: ICompanyCategory[], options: CompanyCategoryRepoParams): Observable<ICompanyCategory[]> {
    const url = this.getBaseUrl(options);
    const request = new HttpPut(url, categories, { init: { params: options.queryParams } });
    request.setErrorHandler(options.errorHandler);

    return this.repositoryService.httpClient.request(request)
      .pipe(map((response: CategoryResponse) => response.body?.data.items));
  }
}
