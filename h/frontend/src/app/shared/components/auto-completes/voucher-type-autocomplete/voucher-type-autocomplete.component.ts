import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { MandatorVoucherTypeRepositoryService } from 'src/app/core/services/repositories/mandator-voucher-type-repository.service';
import { VoucherTypeRepositoryService } from 'src/app/core/services/repositories/voucher-type-repository.service';
import { gridConfig } from 'src/app/shared/components/autocomplete/modals/voucher-type-grid-config';

@Component({
  selector: 'nv-voucher-type-autocomplete',
  templateUrl: './voucher-type-autocomplete.component.html',
  styleUrls: ['./voucher-type-autocomplete.component.scss']
})
export class VoucherTypeAutocompleteComponent {
  public wording = wording;
  public gridConfig = gridConfig;

  @Input() public form: FormGroup;
  @Input() public controlName = 'voucherTypeId';
  @Input() public filters: object;
  @Input() public onlyOfMandator: boolean;
  @Input() public mandatorControlName = 'mandatorId';

  constructor(
    public voucherTypeRepo: VoucherTypeRepositoryService,
    public mandatorVoucherTypeRepo: MandatorVoucherTypeRepositoryService
  ) { }

}
