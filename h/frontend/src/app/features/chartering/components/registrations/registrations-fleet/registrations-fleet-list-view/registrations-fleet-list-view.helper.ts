import * as moment from 'moment';
import { VESSEL_SETTING_TYPE } from 'src/app/core/models/enums/vessel-setting-type';
import { IVesselSetting } from 'src/app/core/models/resources/IVesselSetting';
import { IVesselAccountingSpecification } from '../../../../../technical-management/components/misc/stevedore-damages/list-view/vessels/vessels.component';

export const getFilteredVesselsList = (
  vesselAccountings: IVesselAccountingSpecification[],
  query: string,
) => {
  if (!query) {
    return vesselAccountings;
  }

  return vesselAccountings.filter(v => {
    return checkForVesselName(v, query);
  });
};

function checkForVesselName(
  vessel: IVesselAccountingSpecification,
  query: string
): boolean {
  const usrInput: string = query.toLowerCase().trim();
  const vesselName = vessel.vesselName
    ? vessel.vesselName.toLowerCase()
    : vessel.imo || '';
  const vesselNo = vessel.vesselNo ? vessel.vesselNo.toString() : '';
  const vesselTypeDisplayValue = vessel.vesselTypeDisplayValue.toLowerCase();

  const case1 = `${vesselName} ${vesselNo} ${vesselTypeDisplayValue}`;
  const case2 = `${vesselNo} ${vesselName} ${vesselTypeDisplayValue}`;
  const case3 = `${vesselTypeDisplayValue} ${vesselName} ${vesselNo}`;

  return case1.startsWith(usrInput)
    || case2.startsWith(usrInput)
    || case3.startsWith(usrInput);
}

export const hasValidSettingRangeAndType = (settings: IVesselSetting[]): boolean => {
  const maxDate: moment.Moment = moment('01.01.9999');
  return !!settings.find(s => {
    return moment().isBetween(s.from, s.until || maxDate)
      && !!(s.serviceType & VESSEL_SETTING_TYPE.Registration);
  });
};
