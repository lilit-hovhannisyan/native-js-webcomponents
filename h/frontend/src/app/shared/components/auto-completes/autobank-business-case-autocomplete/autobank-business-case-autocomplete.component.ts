import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { AutobankBusinessCaseRepositoryService } from 'src/app/core/services/repositories/autobank-business-case-repository.service';

@Component({
  selector: 'nv-autobank-business-case-autocomplete',
  templateUrl: './autobank-business-case-autocomplete.component.html',
  styleUrls: ['./autobank-business-case-autocomplete.component.scss']
})
export class AutobankBusinessCaseAutocompleteComponent {
  public wording = wording;
  public gridConfig = null;

  @Input() public form: FormGroup;
  @Input() public controlName;
  @Input() public filters: object;
  @Input() public showLabel = true;
  @Input() public setNullIfNotFound = false;
  @Input() public setId = true;
  @Input() public displayOneProperty: boolean;
  @Input() public openListOnInit: boolean;
  @Input() public autofocus: boolean;

  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();

  constructor(
    public autobankBusinessCaseRepo: AutobankBusinessCaseRepositoryService,
  ) {
  }
}
