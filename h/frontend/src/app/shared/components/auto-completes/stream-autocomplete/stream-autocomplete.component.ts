import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'nv-stream-autocomplete',
  templateUrl: './stream-autocomplete.component.html',
  styleUrls: ['./stream-autocomplete.component.scss']
})
export class StreamAutocompleteComponent {
  @Input() public transformKey: string;
  @Input() public resourceToTransform: Observable<any>;
  @Input() public transformfilters: object = {};

  @Input() public resource: Observable<any>;
  @Input() public filters: object = {};

  @Input() public propertyToDisplayModifier: (value: any) => any;
  @Input() public properities: string[];
  @Input() public form: FormGroup;
  @Input() public controlName: string;
  @Input() public autofocus = false;
  @Input() public setNullIfNotFound = false;
}
