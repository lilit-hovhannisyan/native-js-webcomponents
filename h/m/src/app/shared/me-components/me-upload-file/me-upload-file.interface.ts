export interface MeUploadFileI {
  fileName?: string;
  isStatusInvalid?: boolean;
  acceptFileExtensions?: string;
  supportedFiles?: string;
  maxUploadSize?: string;
  percentageDone?: number;
  showProgress?: boolean;
}
