import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IMeta, IMetaCreate, IMetaDTO, MetaKey } from '../../models/resources/IMeta';
import { RepositoryService } from '../repository.service';

export interface MetaParams extends RepoParams<IMeta> {
  entityName: string;
}

@Injectable({
  providedIn: 'root'
})
export class MetaRepositoryService extends AbstractRepository<
IMeta,
IMetaDTO,
IMetaCreate,
MetaParams,
MetaKey
>  {
  protected resourceType = 'Meta';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IMetaDTO): IMeta => item;

  public getBaseUrl(params: MetaParams): string {
    return `/api/${params.entityName}`;
  }
}
