export class CertificateAssociationAttributesModel {
  certDocument: string = '';
  issuedDate: string = '';
  certificate: string = '';
  name: string = '';
  description: string = '';
  company: string = '';
  expirationDate: string = '';
  urn: string = '';
}
