import {
  ChangeDetectionStrategy,
  Component,
  OnInit
} from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { VesselRepositoryService } from 'src/app/core/services/repositories/vessel-repository.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/vessels.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { IVesselWithAccountings } from 'src/app/core/models/resources/IVessel';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { VesselAccountingKey } from 'src/app/core/models/resources/IVesselAccounting';

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VesselListViewComponent implements OnInit {
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IVesselWithAccountings[]>;

  constructor(
    private vesselRepo: VesselRepositoryService,
    private confirmationService: ConfirmationService,
    public gridConfigService: GridConfigService,
    private authService: AuthenticationService,
    private vesselMainService: VesselMainService,
    private router: Router,
  ) { }

  public ngOnInit() {
    this.dataSource$ = this.vesselMainService.fetchWithRolesOnly({});

    const actions = {
      edit: (id: VesselAccountingKey) =>
        this.router.navigate([`${this.router.url}/${id}`]),
      delete: this.deleteClicked,
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    this.gridConfig = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  public deleteClicked = (vessel: IVesselWithAccountings) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: vessel.imo.toString() },
    }).subscribe(confirmed => confirmed && this.vesselRepo.delete(vessel.id, {}).subscribe());
  }
}
