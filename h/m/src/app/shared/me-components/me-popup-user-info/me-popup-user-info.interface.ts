import { KeyValue } from '@angular/common';
import { MeButtonI } from '../me-button/me-button.interface';

export interface MePopupUserInfoI {
  contextMenuButton?: MeButtonI;
  contextMenuItems?: KeyValue<string, string>[];
  firstName?: string;
  lastName?: string;
  userInitials?: string;
  email?: string;
  status?: boolean;
  uid?: string;
  oid?: string;
  thumbnail?: string;
}
