import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SubSink } from 'subsink';
import { Subject, Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

import { GlobalService } from '@core-services/global.service';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { DigitalProfileStoreService } from '@features/digital-profile/digital-profile-store.service';
import { BackOfficeService } from '@features/backoffice/backoffice.web-service';
import { AttributesService } from '@shared/services/attributes.service';
import { UserActionsService } from '@features/backoffice/users-groups/user-actions.service';
import { TableViewService } from '@shared/me-components/me-table/table-view.service';
import { FileWebService } from '@shared/services/file.web-service';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';

import { TableImageWithTitleComponent } from '@shared/me-components/me-table/table-image-with-title/table-image-with-title.component';
import { TableDateComponent } from '@shared/me-components/me-table/table-date/table-date.component';

import {
  MARKETPLACE_BASE_API_URL,
  ME_DOMAIN_APPLICATION_ADDRESS,
  ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL
} from '@shared/constants';
import { MeCoverImageUploadI } from '@shared/me-components/me-cover-image-upload/me-cover-image-upload.interface';
import { MeLogoUploadI } from '@shared/me-components/me-logo-upload/me-logo-upload.interface';
import { AttributeGroupModel } from '@shared/models/attribute/attribute.group.model';
import { AttributeModel } from '@shared/models/attribute/attribute.model';
import { DigitalProfileAttributesModel } from '@shared/models/digital-profile/digital-profile-attributes.model';
import { DigitalProfileModel } from '@shared/models/digital-profile/digital-profile.model';
import { DomainModel } from '@shared/models/domain/domain.model';
import { LayoutGroupModel } from '@shared/models/layout/layout-group.model';
import { UserModel } from '@shared/models/user/user.model';
import { bytesToSize } from '@shared/utils';
import { MeAttributeLayoutTypes } from '@shared/me-attribute-layout-types';
import { MeLabelI } from '@shared/me-components/me-label/me-label-inteface';
import {
  MeTableActionButtonPropertiesI,
  MeTableCellClickEventI,
  MeTableColumnI,
  MeTableColumnOptionsI,
  MeTableDataSourceI
} from '@shared/me-components/me-table';
import { MeFileUploadTypes } from '@shared/me-file-upload-types';
import { FileUploadResponseModel } from '@shared/models/file-upload/file-upload-response.model';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { ArrayRequestI } from '@core-interfaces/array-request.interface';
import { EntityI } from '@core-interfaces/entity.interface';

@Component({
  selector: 'me-company-view',
  templateUrl: './company-view.component.html',
  styleUrls: ['./company-view.component.scss'],
  providers: [AttributesService, DigitalProfileStoreService, TableViewService, UserActionsService, FileWebService]
})
export class CompanyViewComponent implements OnInit, OnDestroy {
  private subs = new SubSink();
  companyId!: string;
  company!: DigitalProfileModel;
  companyAttributes!: DigitalProfileAttributesModel;
  address!: DigitalProfileModel;
  languageLabel!: MeLabelI;
  companyLayoutAttributes: AttributeModel<unknown>[] = [];
  companyLayoutGroups: LayoutGroupModel[] = [];
  companyGroups: LayoutGroupModel[] | undefined = [];
  companyInfoAttributeGroups: Array<AttributeGroupModel> = new Array<AttributeGroupModel>();
  companyDomain: DomainModel | undefined;
  addressDomain: DomainModel | undefined;
  addressAttributes: AttributeModel<unknown>[] = [];
  addressLayoutGroups: LayoutGroupModel[] = [];
  userTableItems: MeTableDataSourceI[] = [];
  userTableColumns: MeTableColumnI[] = [];
  userTableColumnOptions: MeTableColumnOptionsI[] = [];
  addressGroups: AttributeGroupModel = new AttributeGroupModel();

  uploadLogoData!: MeLogoUploadI;
  uploadCoverImageData!: MeCoverImageUploadI;
  companyImageForm!: FormGroup;

  firstTabGroup: string = 'companyOverview';
  isCompanyEdit: boolean = true;
  tableLoader = false;
  openTabsIndex = 0;

  _cellClickEventSubject!: Subscription;
  cellClickEventSubject = new Subject<MeTableCellClickEventI>();

  i18nStrings: { [key: string]: string } = {
    notAvailable: '',
    edit: '',
    company: '',
    info: '',
    years: '',
    experience: '',
    employees: '',
    employed: '',
    certificates: '',
    sustainability: '',
    companyIntroduction: '',
    logoUpdate: '',
    logoUpdateSuccessfully: '',
    coverImageUpdate: '',
    coverImageUpdateSuccessfully: ''
  };

  constructor(
    private globalService: GlobalService,
    private attributesService: AttributesService,
    private definitionsStoreService: DefinitionsStoreService,
    private backOfficeService: BackOfficeService,
    private translate: TranslateService,
    private tableViewService: TableViewService,
    private fileWebService: FileWebService,
    private basicAlertService: MeBasicAlertService,
    public userActionsService: UserActionsService,
    private _activeRoute: ActivatedRoute,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.companyId = this._activeRoute.snapshot.params.id;

    this.globalService.activateLoader();
    this.setTableColumns();
    this.subs.sink = this.translate.get(Object.keys(this.i18nStrings)).subscribe((translations) => {
      Object.keys(this.i18nStrings).forEach((key) => {
        this.i18nStrings[key] = translations[key];
      });
    });

    this.subs.sink = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.companyDomain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL);
        console.log('this.companyDomain', this.companyDomain);
        this.addressDomain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_ADDRESS);
        this.getCompany();
      }
    });

    this.languageLabel = { type: 'inversed', color: 'primary', margin: 'none' };
  }

  setTableColumns(): void {
    this.userTableColumns = [
      {
        field: 'user',
        headerName: 'User'
      },
      {
        field: 'createdOn',
        headerName: 'Creation Date'
      },
      {
        field: 'status',
        headerName: 'Status'
      },
      {
        field: 'actions',
        headerName: 'Actions'
      }
    ];
  }

  initializeView(): void {
    if (this.addressDomain) {
      this.addressAttributes = this.attributesService.transformAttributesArray(this.addressDomain.attributes);
      this.addressLayoutGroups = this.attributesService.getLayoutGroups(this.addressDomain.layouts, MeAttributeLayoutTypes.VIEW);

      for (const group of this.addressLayoutGroups) {
        this.addressGroups = this.attributesService.getAttributesLayoutByGroup(
          group,
          this.addressAttributes,
          this.addressDomain.attributes
        );
      }
      this.company.attributes.address = [];
      let addressValues = '';
      this.addressGroups = this.attributesService.convertAttributesForDisplaying([this.addressGroups], this.addressDomain)[0];

      for (const item of this.addressGroups.attributes) {
        addressValues = addressValues === '' ? item.convertedValue : `${addressValues}, ${item.convertedValue}`;
      }

      this.company.attributes.address = addressValues;
    }

    if (this.companyDomain) {
      this.companyLayoutAttributes = this.attributesService.transformAttributesArray(this.companyDomain.attributes);
      this.companyLayoutGroups = this.attributesService.getLayoutGroups(this.companyDomain.layouts, MeAttributeLayoutTypes.VIEW);
      this.companyGroups = this.attributesService.getLayoutGroupsExceptGeneral(this.companyLayoutGroups, this.firstTabGroup);

      if (this.companyGroups) {
        for (const group of this.companyGroups) {
          let attribute = this.attributesService.getAttributesLayoutByGroup(group, this.companyLayoutAttributes, this.companyAttributes);
          attribute = this.attributesService.convertAttributesForDisplaying([attribute], this.companyDomain)[0];
          attribute = this.handleLanguageCapabilities(attribute);
          this.companyInfoAttributeGroups.push(attribute);
        }
      }
    }

    if (this.definitionsStoreService.logoProperties) {
      const splitString = this.definitionsStoreService.logoProperties.supportedFiles.split(',');

      this.uploadLogoData = {
        thumbnail: this.companyAttributes.logo ? MARKETPLACE_BASE_API_URL + '/files/download/' + this.companyAttributes.logo : '',
        acceptPhotoExtensions: splitString ? `.${splitString.join(',.')}` : '',
        // showProgress: false,
        backgroundimage: '/assets/images/company-no-image.svg',
        maxUploadSize: bytesToSize(this.definitionsStoreService.logoProperties.maxSize),
        dimensions: `${this.definitionsStoreService.logoProperties.minWidth} x ${this.definitionsStoreService.logoProperties.minHeight} px`,
        disabled: !this.isCompanyEdit
      };
    }

    if (this.definitionsStoreService.coverImageProperties) {
      const splitString = this.definitionsStoreService.coverImageProperties.supportedFiles.split(',');

      this.uploadCoverImageData = {
        thumbnail: this.companyAttributes.coverImage
          ? MARKETPLACE_BASE_API_URL + '/files/download/' + this.companyAttributes.coverImage
          : '',
        acceptPhotoExtensions: splitString ? `.${splitString.join(',.')}` : '',
        backgroundimage: '/assets/images/cover-no-image.png',
        maxUploadSize: bytesToSize(this.definitionsStoreService.coverImageProperties.maxSize),
        dimensions: `${this.definitionsStoreService.coverImageProperties.minWidth} x ${this.definitionsStoreService.coverImageProperties.minHeight} px`,
        disabled: !this.isCompanyEdit
      };
    }
  }

  getCompany(): void {
    this.subs.sink = this.backOfficeService.getBoCompany(encodeURI(this.companyId as string)).subscribe((response) => {
      console.log(`response`, response);
      this.companyAttributes = response.attributes;
      this.company = response;
      this.initializeView();
      this.globalService.deactivateLoader();
    });
  }

  prepareUserTableData(data: UserModel[]): void {
    this.tableLoader = true;
    const actionButtons: MeTableActionButtonPropertiesI[] = [
      {
        iconName: 'edit-2',
        type: 'edit'
      },
      {
        contextMenuItems: [
          {
            isActiveAttrType: true,
            key: '',
            value: ''
          },
          { key: 'manageGroups', value: 'Manage Groups' }
        ],
        type: 'selectedContextMenu'
      }
    ];

    const customComponents: MeTableDataSourceI[] = data.map((d) => {
      const {
        attributes: { avatar, firstName, lastName, isActive }
      } = d;
      const generatedActionButtons = this.tableViewService.genereateActionButtons(actionButtons, isActive);

      return {
        rowOID: d.oid,
        renderedCells: [
          generatedActionButtons,
          {
            forField: 'user',
            component: TableImageWithTitleComponent,
            componentData: { url: avatar, title: `${firstName} ${lastName}` }
          },
          {
            forField: 'createdOn',
            component: TableDateComponent,
            // @ts-ignore
            componentData: new Date(d.createdOn)
          },
          {
            forField: 'status',
            component: TableDateComponent,
            componentData: isActive
          }
        ]
      };
    });

    this.userTableItems = customComponents;
    this.tableLoader = false;
  }

  handleLanguageCapabilities(attribute: AttributeGroupModel): AttributeGroupModel {
    const attr = attribute.attributes.filter((x) => x.internalName === 'languageCapabilities');

    attr.forEach((element) => {
      element.value = element.convertedValue.split(',').map((item) => {
        return item.trim();
      });
    });

    return attribute;
  }

  handleImageFormData(recievedFile: File, type: string): void {
    const formData = new FormData();
    formData.append(
      'meta',
      JSON.stringify({
        type: type === 'logo' ? MeFileUploadTypes.logo : MeFileUploadTypes.coverImage
      })
    );
    formData.append('file', recievedFile, recievedFile.name);

    this.globalService.activateLoader();
    this.subs.sink = this.fileWebService.uploadFile(formData, FileUploadResponseModel).subscribe(
      ({ oid }) => {
        if (type === 'logo') {
          this.uploadLogoData = {
            ...this.uploadLogoData,
            thumbnail: `${MARKETPLACE_BASE_API_URL}/files/download/${oid}`,
            showProgress: false
          };
        } else {
          this.uploadCoverImageData = {
            ...this.uploadCoverImageData,
            thumbnail: `${MARKETPLACE_BASE_API_URL}/files/download/${oid}`,
            showProgress: false
          };
        }
        if (oid) {
          this.updateCompanyLogoOrCover(oid, type);
        }
      },
      ({ error: { title, details } }: ErrorResponseI) => {
        this.globalService.deactivateLoader();
        this.uploadLogoData = { ...this.uploadLogoData };
        this.uploadCoverImageData = { ...this.uploadCoverImageData };
        this.basicAlertService.openBasicAlert({
          mode: 'error',
          title: title,
          content: details
        });
      }
    );
  }

  updateCompanyLogoOrCover(imageOID: string, type: string) {
    let attributes = {};
    const companyUpdate: ArrayRequestI<EntityI> = {
      entities: []
    };

    if (type === 'logo') {
      attributes = { logo: imageOID };
    } else {
      attributes = { coverImage: imageOID };
    }

    const companyentity = {
      domainName: ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL,
      attributes
    };
    companyUpdate.entities.push(companyentity);

    this.subs.sink = this.backOfficeService.updateBoCompany(this.companyId, companyUpdate).subscribe(() => {
      this.globalService.deactivateLoader();
      this.basicAlertService.openBasicAlert({
        mode: 'success',
        title: type === 'logo' ? this.i18nStrings.logoUpdate : this.i18nStrings.coverImageUpdate,
        content: type === 'logo' ? this.i18nStrings.logoUpdateSuccessfully : this.i18nStrings.coverImageUpdateSuccessfully
      });
    });
  }

  tabChanged(e: { index: number }): void {
    this.openTabsIndex = e.index;
  }

  editCompany(): void {
    this._router.navigate(['../../edit', this.companyId], { relativeTo: this._activeRoute });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
