
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { CurreniesHelperService } from './currenies.helper.service';

@Component({
  selector: 'nv-currency-detail-view',
  templateUrl: './currency-detail-view.component.html',
  styleUrls: ['./currency-detail-view.component.scss'],
  providers: [CurreniesHelperService],
})
export class CurrencyDetailViewComponent implements OnInit, OnDestroy {
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public currency: ICurrency;
  public sitemap = sitemap;
  public wording = wording;
  public title: string;

  public get isCreationMode() {
    return this.route.snapshot.params.id === 'new';
  }

  constructor(
    private route: ActivatedRoute,
    private currencyRepo: CurrencyRepositoryService,
    private curreniesHelperService: CurreniesHelperService,
    private router: Router,
  ) { }

  public ngOnInit() {
    const idParam = this.route.snapshot.params.id;

    if (this.isCreationMode) {
      this.curreniesHelperService.generateTitle();
    } else {
      const id = parseInt(idParam, 10);

      this.currencyRepo.fetchOne(id, { useCache: true })
        .subscribe(currency => {
          this.curreniesHelperService.generateTitle(currency);
        });
    }

    this.curreniesHelperService.editViewTitle$.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(title => {
      this.title = title;
    });
  }

  public routeBack = () => {
    this.router.navigate([
      url(sitemap.system.children.misc.children.currencies)
    ]);
  }

  public tabDisabler = (node: SitemapNode) => {
    const basic = sitemap.system.children.misc.children.currencies.children.id.children.basic;
    return this.isCreationMode ? node === basic : true;
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
