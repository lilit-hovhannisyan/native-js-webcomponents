import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShareholdingsMainService } from '../../../../../../../core/services/mainServices/shareholdings-main.service';
import { CompanyKey } from '../../../../../../../core/models/resources/ICompany';

@Component({
  templateUrl: './representatives.component.html',
  styleUrls: ['./representatives.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareholdingsRepresentativesComponent implements OnInit {
  public companyId: CompanyKey;

  constructor(
    private activatedRoute: ActivatedRoute,
    private shareholdingsMainService: ShareholdingsMainService,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    const shareholdingId: CompanyKey = +this.activatedRoute.parent.snapshot.params.id;

    this.shareholdingsMainService.getShareholdingById(shareholdingId).subscribe(shareholding => {
      if (shareholding) {
        this.companyId = shareholding.id;
        this.cdr.markForCheck();
      }
    });
  }
}
