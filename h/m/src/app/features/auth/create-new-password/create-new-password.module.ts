import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { SharedModule } from '@shared/shared.module';
import { IconsModule } from '@shared/modules/icons.module';

import { CreateNewPasswordRoutingModule } from './create-new-password-routing.module';
import { CreateNewPasswordComponent } from './create-new-password.component';
import { I18nModule } from '@shared/modules/i18n.module';

@NgModule({
  declarations: [CreateNewPasswordComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatTooltipModule,
    CreateNewPasswordRoutingModule,
    SharedModule,
    MatFormFieldModule,
    IconsModule,
    MatButtonModule,
    MatInputModule,
    I18nModule
  ]
})
export class CreateNewPasswordModule {}
