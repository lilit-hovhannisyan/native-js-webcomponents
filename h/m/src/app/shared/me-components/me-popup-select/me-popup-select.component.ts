import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Inject, Injector, Output, TemplateRef } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BaseLoggerComponent } from '@features/tracking-system';

import { MeButtonI } from '../me-button/me-button.interface';
import { MePopupSelectI } from './me-popup-select.interface';

@Component({
  templateUrl: './me-popup-select.component.html',
  styleUrls: ['./me-popup-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MePopupSelectComponent extends BaseLoggerComponent {
  componentVersion: string = '0.0.1';
  @Output() popupSelectEvent: EventEmitter<{ eventName: string }> = new EventEmitter();

  showSelectedBtn: MeButtonI = { color: 'link', type: 'regular' };
  showAllBtn: MeButtonI = { color: 'link', type: 'regular' };
  clearBtn: MeButtonI = { color: 'link', type: 'regular' };
  cancelBtn: MeButtonI = { color: 'secondary', type: 'regular' };
  addBtn: MeButtonI = { color: 'primary', type: 'regular' };

  // Internal use
  private _dataModel!: MePopupSelectI;
  content!: TemplateRef<Component>;

  constructor(
    injector: Injector,
    @Inject(MAT_DIALOG_DATA) public data: { dataModel: MePopupSelectI; content: TemplateRef<Component> },
    private cdr: ChangeDetectorRef
  ) {
    super(injector);
    this._dataModel = data.dataModel;
    this.addBtn.disabled = this._dataModel.selectedCount === 0;
    this.content = data.content;
  }

  set dataModel(data: MePopupSelectI) {
    this._dataModel = data;
    this.addBtn = { ...this.addBtn, disabled: this._dataModel.selectedCount === 0 };
    this.cdr.detectChanges();
  }

  get dataModel(): MePopupSelectI {
    return this._dataModel;
  }

  customEvent(eventName: string): void {
    this.popupSelectEvent.emit({ eventName });
  }
}
