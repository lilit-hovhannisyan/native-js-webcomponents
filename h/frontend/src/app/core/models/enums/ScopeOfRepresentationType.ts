import { wording } from '../../constants/wording/wording';
import { IWording } from '../resources/IWording';

export type ScopeOfRepresentationMap = Map<ScopeOfRepresentationType, IWording>;

export enum ScopeOfRepresentationType {
  SolePowerOfRepresentation = 1,
  JointPowerOfRepresentation = 2,
}

export const scopeOfRepresentationTypeOptions: ScopeOfRepresentationMap = new Map([
  [
    ScopeOfRepresentationType.SolePowerOfRepresentation,
    wording.system.solePowerOfRepresentation
  ],
  [
    ScopeOfRepresentationType.JointPowerOfRepresentation,
    wording.system.jointPowerOfRepresentation
  ],
]);
