import { NvColumnDataType, NvFilterControl, NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getStevedoreRepairArticlesGridConfig = (): NvGridConfig => {
  return ({
    gridName: 'stevedoreRepairArticlesGridConfig',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    rowSelectionType: NvGridRowSelectionType.Checkbox,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'id',
        hidden: true,
        visible: true,
      },
      {
        key: 'displayLabel',
        title: wording.general.name,
        width: 500,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'no',
        title: wording.technicalManagement.articleNo,
        width: 120,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'articleClassName',
        title: wording.technicalManagement.articleClass,
        width: 230,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'articleTypeName',
        title: wording.technicalManagement.articleType,
        width: 230,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'description',
        title: wording.technicalManagement.description,
        width: 230,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'isoCode',
        title: wording.accounting.isoCode,
        width: 180,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'unitName',
        title: wording.technicalManagement.unit,
        width: 200,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'salePrice',
        title: wording.technicalManagement.salePrice,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        dataType: NvColumnDataType.Decimal
      },
      {
        key: 'isSystemDefault',
        title: wording.general.systemDefault,
        width: 120,
        isSortable: true,
        hidden: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isActive',
        title: wording.accounting.active,
        width: 120,
        hidden: true,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'manufacturerManual',
        title: wording.technicalManagement.manufacturerManual,
        width: 150,
        hidden: true,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'creditorDebitorName',
        title: wording.technicalManagement.artilceCreditorDebitor,
        width: 150,
        hidden: true,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'itemType',
        title: wording.technicalManagement.itemType,
        width: 150,
        hidden: true,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'year',
        title: wording.technicalManagement.yearOfManufacture,
        width: 120,
        hidden: true,
        isSortable: true,
        filter: {
          values: []
        },
      },
      {
        key: 'serialNo',
        title: wording.technicalManagement.serialNo,
        width: 150,
        hidden: true,
        isSortable: true,
        filter: {
          values: []
        },
      },
    ],
  });
};
