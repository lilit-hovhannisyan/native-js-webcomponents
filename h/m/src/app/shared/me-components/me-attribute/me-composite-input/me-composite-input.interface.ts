import { FormGroup } from '@angular/forms';
import { MeCompositeDataI } from './me-composite-data.interface';

export interface MeCompositeInputI {
  form: FormGroup;
  internalName: string;
  displayName: string;
  required: boolean;
  optRequired: boolean;
  value: MeCompositeDataI;
  values: { displayName: string; value: string }[];
  disabled: boolean;
}
