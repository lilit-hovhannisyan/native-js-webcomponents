
import { Injectable } from '@angular/core';
import { COMPANY_LEGAL_FORM } from 'src/app/core/models/enums/company-legal-form';
import { BehaviorSubject } from 'rxjs';
import { AmountChangeType, CompanyPartnerContributionKey, ICompanyPartnerContribution, PartnerType } from '../../../../../../../../core/models/resources/ICompanyPartnerContribution';
import { CompanyKey, ICompany } from '../../../../../../../../core/models/resources/ICompany';
import { ContributionMainService } from '../../../../../../../../core/services/mainServices/company-contributions-main.service';

type LegalEntityFields = {
  [key in COMPANY_LEGAL_FORM]: {
    [key: string]: boolean
  }
};

export const legalEntityFields: LegalEntityFields = {
  [COMPANY_LEGAL_FORM.None]: {},

  // KG (Limited Partnership)
  [COMPANY_LEGAL_FORM.LimitedPartnership]: {
    // Commercial Register
    businessName: true,
    partners: true,
    hra: true,
    commercialRegister: true,
    dateOfEntry: true,
    sumOfContributions: true,
    currencyId: true,

    // Classification
    companyGroup: true,
    isShellCompany: true,
    isUnitaryLimitedPartnership: true,

    // Remark
    remark: true,

    // Trustor/ Sub-Participant/ Silent Partner
    trustor: true,
    subParticipant: true,
    silentPartner: true,

    // Status
    isPreliminaryInsolvencyProceeding: true,
    preliminaryInsolvencyProceedingStartDate: true,
    preliminaryInsolvencyAdministrator: true,

    isInsolvencyProceeding: true,
    insolvencyProceedingStartDate: true,

    isInLiquidation: true,
    liquidationStartDate: true,
    liquidator: true,

    isDeleted: true,
    deletedStartDate: true,

    isSold: true,
    soldStartDate: true,
    buyer: true,
  },

  // GmbH (Company with Limited Liability)
  [COMPANY_LEGAL_FORM.CompanyWithLimitedLiability]: {
    // Commercial Register
    businessName: true,
    hrb: true,
    commercialRegister: true,
    dateOfEntry: true,
    shareCapital_v2: true,
    currencyId: true,

    // Classification
    companyGroup: true,
    isShellCompany: true,

    // Remark
    remark: true,

    // Trustor/ Sub-Participant/ Silent Partner
    trustor: true,
    subParticipant: true,
    silentPartner: true,

    // Status
    isPreliminaryInsolvencyProceeding: true,
    preliminaryInsolvencyProceedingStartDate: true,
    preliminaryInsolvencyAdministrator: true,

    isInsolvencyProceeding: true,
    insolvencyProceedingStartDate: true,

    isInLiquidation: true,
    liquidationStartDate: true,
    liquidator: true,

    isDeleted: true,
    deletedStartDate: true,

    isSold: true,
    soldStartDate: true,
    buyer: true,
  },

  // Aktiengesellschaft (AG)
  [COMPANY_LEGAL_FORM.StockCorporation]: {
    // Commercial Register
    businessName: true,
    hrb: true,
    commercialRegister: true,
    dateOfEntry: true,
    shareCapital: true,
    currencyId: true,
    capitalType: true,
    sumOfNonParValueShares: true,

    // Classification
    companyGroup: true,
    isShellCompany: true,

    // Remark
    remark: true,

    // Trustor/ Sub-Participant/ Silent Partner
    trustor: true,
    subParticipant: true,
    silentPartner: true,

    // Status
    isPreliminaryInsolvencyProceeding: true,
    preliminaryInsolvencyProceedingStartDate: true,
    preliminaryInsolvencyAdministrator: true,

    isInsolvencyProceeding: true,
    insolvencyProceedingStartDate: true,

    isInLiquidation: true,
    liquidationStartDate: true,
    liquidator: true,

    isDeleted: true,
    deletedStartDate: true,

    isSold: true,
    soldStartDate: true,
    buyer: true,
  },

  // GbR (Civil Law Partnership)
  [COMPANY_LEGAL_FORM.CivilLawPartnership]: {
    // Commercial Register
    businessName_v2: true,
    partners: true,
    sumOfContributions_v2: true,
    currencyId: true,

    // Classification
    companyGroup: true,

    // Remark
    remark: true,

    // Trustor/ Sub-Participant/ Silent Partner
    trustor: true,
    subParticipant: true,
    silentPartner: true,

    // Status
    isPreliminaryInsolvencyProceeding: true,
    preliminaryInsolvencyProceedingStartDate: true,
    preliminaryInsolvencyAdministrator: true,

    isInsolvencyProceeding: true,
    insolvencyProceedingStartDate: true,

    isInLiquidation: true,
    liquidationStartDate: true,
    liquidator: true,

    isDeleted: true,
    deletedStartDate: true,

    isSold: true,
    soldStartDate: true,
    buyer: true,
  },

  // Auslandsgesellschaft (Foreign Company)
  [COMPANY_LEGAL_FORM.ForeignCompany]: {
    // Commercial Register
    businessName: true,
    companyNumber: true,
    commercialRegister: true,
    dateOfEntry: true,
    shareCapital_v3: true,
    currencyId: true,

    // Classification
    companyGroup: true,
    isDormantCompany: true,

    // Remark
    remark: true,

    // Trustor/ Sub-Participant/ Silent Partner
    trustor: true,

    // Status
    isPreliminaryInsolvencyProceeding: true,
    preliminaryInsolvencyProceedingStartDate: true,
    preliminaryInsolvencyAdministrator: true,

    isInsolvencyProceeding: true,
    insolvencyProceedingStartDate: true,

    isInLiquidation: true,
    liquidationStartDate: true,
    liquidator: true,

    isDeleted: true,
    deletedStartDate: true,

    isSold: true,
    soldStartDate: true,
    buyer: true,
  },
};

@Injectable({
  providedIn: 'root'
})
export class LegalEntityHelperService {

  constructor(
    private contributionMainService: ContributionMainService,
  ) { }

  public legalForm$: BehaviorSubject<COMPANY_LEGAL_FORM> = new BehaviorSubject(COMPANY_LEGAL_FORM.None);
  public legalForm: COMPANY_LEGAL_FORM;

  public subscribeToLegalForm(): void {
    this.legalForm$.subscribe(legalForm => this.legalForm = legalForm);
  }

  public isFieldAvailable = (fieldName: string): boolean => {
    return !!legalEntityFields[this.legalForm][fieldName];
  }

  public getCurrentPartnerList(
    contributions: ICompanyPartnerContribution[],
    company: ICompany,
  ): CompanyKey[] {
    const currentContributionIds: CompanyPartnerContributionKey[] = [];
    const companyContributions: ICompanyPartnerContribution[] = contributions.filter(
      (c: ICompanyPartnerContribution) => c.companyId === company.id
    );
    const filteredContributions: ICompanyPartnerContribution[] = this.contributionMainService
      .filterContributions(companyContributions, new Date().toDateString());

    const latestContributions: Map<CompanyKey, ICompanyPartnerContribution> = this.contributionMainService
      .getCompaniesLatestContributions(filteredContributions);


    latestContributions.forEach(c => {
      if (
        c.partnerType === PartnerType.General
        && (
          !!c.contributionAmount
          || (c.contributionIsZero && c.amountChangeType === AmountChangeType.Increase)
        )
      ) {
        currentContributionIds.push(c.partnerId);
      }
    });

    return currentContributionIds;
  }
}
