import { Component, OnInit, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { IArticleClass, IArticleClassCreate } from 'src/app/core/models/resources/IArticleClass';
import { FormBuilder } from '@angular/forms';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { ActivatedRoute } from '@angular/router';
import { ArticleClassRepositoryService } from 'src/app/core/services/repositories/article-class-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { Validators } from 'src/app/core/classes/validators';

@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class ArticleClassesDetailViewComponent
  extends ResourceDetailView<IArticleClass, IArticleClassCreate>
  implements OnInit {

  public repoParams = {};
  public routeBackUrl = url(sitemap.technicalManagement.children.main.children.articleClasses);
  protected resourceDefault = { isActive: true } as IArticleClass;
  public formElement: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    articleClassRepo: ArticleClassRepositoryService,
    baseComponentService: BaseComponentService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef,
  ) {
    super(articleClassRepo, baseComponentService);
  }

  public ngOnInit(): void {
    const { id } = this.route.snapshot.params;

    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public getTitle = () => this.resource.abbreviation;

  public init(articleClass: IArticleClass = this.resourceDefault): void {
    this.initForm(this.fb.group({
      labelEn: [articleClass.labelEn, [Validators.required, Validators.maxLength(50)]],
      labelDe: [articleClass.labelDe, [Validators.required, Validators.maxLength(50)]],
      abbreviation: [articleClass.abbreviation, [Validators.required, Validators.maxLength(2)]],
      isSystemDefault: [articleClass.isSystemDefault || false],
      isActive: [articleClass.isActive],
    }));

    if (!this.isAdmin) {
      this.disableField('isActive');
    }
  }

  public submit(): void {
    if (!this.form.valid) { return; }

    const { value } = this.form;
    if (this.creationMode) {
      this.createResource(value);
    } else {
      this.updateResource({ ...this.resource, ...value });
    }
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
