export enum MePermissionTypes {
  ROOT_ROLE = 'roots',
  // ANY = IACLManager.ALL,

  IMPORT = 'import',

  USER_ANY = 'user:*',
  USER_LIST = 'user:list',

  COMPANY_ANY = 'company:*',
  COMPANY_CREATE = 'company:create',
  COMPANY_MODIFY = 'company:modify',
  COMPANY_VIEW = 'company:view',
  COMPANY_DELETE = 'company:delete',
  COMPANY_LIST = 'company:list',

  COMPANY_USER_ANY = 'company:user:*',
  COMPANY_USER_CREATE = 'company:user:create',
  COMPANY_USER_MODIFY = 'company:user:modify',
  COMPANY_USER_DELETE = 'company:user:delete',
  COMPANY_USER_VIEW = 'company:user:view',
  COMPANY_USER_LIST = 'company:user:list',
  COMPANY_PERMISSIONS = 'company:permissions',

  COLLECTION_ANY = 'collection:*',
  COLLECTION_CREATE = 'collection:create',
  COLLECTION_MODIFY = 'collection:modify',
  COLLECTION_DELETE = 'collection:delete',
  COLLECTION_VIEW = 'collection:view',
  COLLECTION_PERMISSIONS = 'collection:permissions',

  DOCUMENT_ANY = 'document:*',
  DOCUMENT_CREATE = 'document:create',
  DOCUMENT_MODIFY = 'document:modify',
  DOCUMENT_DELETE = 'document:delete',
  DOCUMENT_VIEW = 'document:view',

  MATERIAL_ANY = 'material:*',
  MATERIAL_CREATE = 'material:create',
  MATERIAL_MODIFY = 'material:modify',
  MATERIAL_DELETE = 'material:delete',
  MATERIAL_VIEW = 'material:view',
  MATERIAL_PERMISSIONS = 'material:permissions',

  STATE_ANY = 'state:*',
  STATE_MATERIAL_CHANGE = 'state:material:change',
  STATE_COLLECTION_CHANGE = 'state:collection:change',
  STATE_DOCUMENT_CHANGE = 'state:document:change',

  CERTIFICATE_ANY = 'certificate:*',
  CERTIFICATE_CREATE = 'certificate:create',
  CERTIFICATE_MODIFY = 'certificate:modify',
  CERTIFICATE_DELETE = 'certificate:delete',
  CERTIFICATE_VIEW = 'certificate:view'
}
