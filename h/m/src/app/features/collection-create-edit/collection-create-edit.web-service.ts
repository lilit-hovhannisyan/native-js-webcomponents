import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseWebService } from '@core-services/base.web-service';
import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { CollectionEntityI } from './collection-entity.interface';
import { CollectionModel } from '@shared/models/collection/collection.model';
import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { MaterialModel } from '@shared/models/materials/material-model';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { constructUrl } from '@shared/utils';
import { BaseSearchModel } from '@shared/models/search/search-base.model';

@Injectable()
export class CollectionCreateEditWebService {
  constructor(private baseWebService: BaseWebService) {}

  createCollection(data: CollectionEntityI): Observable<CollectionModel> {
    return this.baseWebService.postRequest<CollectionModel, CollectionEntityI>(
      `${MARKETPLACE_BASE_API_URL}/collections`,
      data,
      CollectionModel
    );
  }

  getCollectionById(collectionOID: string): Observable<CollectionModel> {
    return this.baseWebService.getRequest<CollectionModel>(`${MARKETPLACE_BASE_API_URL}/collections/${collectionOID}`, CollectionModel);
  }

  updateCollection(collectionOID: string, data: CollectionEntityI): Observable<CollectionModel> {
    return this.baseWebService.putRequest<CollectionModel, CollectionEntityI>(
      `${MARKETPLACE_BASE_API_URL}/collections/${collectionOID}`,
      data,
      CollectionModel
    );
  }

  getCollectionMaterials(collectionOID: string, from?: number, to?: number): Observable<ArrayResponseI<MaterialModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/collections/${collectionOID}/materials`, from, to);
    return this.baseWebService.getRequestForArray<MaterialModel>(url, MaterialModel);
  }

  addMaterialsToCollection(collectionOID: string, materialsOIDs: string[]): Observable<void> {
    return this.baseWebService.postRequest<void, string[]>(
      `${MARKETPLACE_BASE_API_URL}/collections/${collectionOID}/materials`,
      materialsOIDs,
      CollectionModel
    );
  }

  getCollectionDocuments(collectionOid: string, from?: number, to?: number): Observable<ArrayResponseI<MeDocumentModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/collections/${collectionOid}/documents`, from, to);
    return this.baseWebService.getRequestForArray<MeDocumentModel>(url, MeDocumentModel);
  }

  getDocuments(from?: number, to?: number): Observable<ArrayResponseI<MeDocumentModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/documents`, from, to);
    return this.baseWebService.getRequestForArray<MeDocumentModel>(url, MeDocumentModel);
  }

  removeDocuments(collectionOid: string, documentOids: string[]): Observable<boolean> {
    return this.baseWebService.deleteRequest(`${MARKETPLACE_BASE_API_URL}/collections/${collectionOid}/documents`, documentOids);
  }

  submitDocuments(collectionOid: string, documentOids: string[]): Observable<boolean> {
    return this.baseWebService.postRequest(`${MARKETPLACE_BASE_API_URL}/collections/${collectionOid}/documents`, documentOids);
  }

  searchCollectionMaterials(
    collectionOid: string,
    data: BaseSearchModel,
    skip?: number,
    top?: number
  ): Observable<ArrayResponseI<MaterialModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/collections/${collectionOid}/materials/search`, skip, top);
    return this.baseWebService.postRequestForArray<MaterialModel, BaseSearchModel>(url, data, MaterialModel);
  }

  searchCollectionDocuments(
    collectionOid: string,
    data: BaseSearchModel,
    skip?: number,
    top?: number
  ): Observable<ArrayResponseI<MeDocumentModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/collections/${collectionOid}/documents/search`, skip, top);
    return this.baseWebService.postRequestForArray<MeDocumentModel, BaseSearchModel>(url, data, MeDocumentModel);
  }

  duplicateCollection(collectionOID: string, data: CollectionEntityI): Observable<CollectionModel> {
    return this.baseWebService.postRequest<CollectionModel, CollectionEntityI>(
      `${MARKETPLACE_BASE_API_URL}/collections/${collectionOID}/copy`,
      data,
      CollectionModel
    );
  }
}
