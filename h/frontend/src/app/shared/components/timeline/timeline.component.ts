import { Component, OnInit, Input } from '@angular/core';
import { TimelineItem } from '../../../core/models/TimelineItem';

/**
 * This accounting is used to show data in time line list
 */
@Component({
  selector: 'nv-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {

  public displayItems: string[] = [];
  @Input() public separator = ' | ';
  @Input() public timelineItems: TimelineItem[];

  public ngOnInit(): void {
    this.displayItems = this.timelineItems.map(item => {
      return `${item.timestamp}${this.separator}${item.initiator.name}${this.separator}${item.activity}`;
    });
  }
}
