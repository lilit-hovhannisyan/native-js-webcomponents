import { NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { IUser } from 'src/app/core/models/resources/IUser';
import { IMandatorLookup, mandatorNoModifier } from 'src/app/core/models/resources/IMandator';

export const getPermissionGroupMandatorsGridConfig = (
  actions: IGridConfigActions,
  canPerform: (operation: Operations) => boolean,
  isAutobankPage = true,
): NvGridConfig => {
  return {
    gridName: `${isAutobankPage ? 'autobank' : 'payments'}PermissionGroupMandatorsGrid`,
    title: wording.accounting.mandators,
    sortBy: 'no',
    isSortAscending: true,
    hideToolbar: false,
    disableDragAndDrop: true,
    disableHideColumns: true,
    disableSortColumns: true,
    hideSettingsButton: true,
    hideRefreshButton: true,
    clearFiltersButton: true,
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    paging: {
      pageNumber: 1,
      pageSize: 10
    },
    columns: [
      {
        key: 'no',
        title: wording.general.numberNo,
        width: 80,
        customFormatFn: (mandator: IMandatorLookup) => mandatorNoModifier(mandator.no),
        filter: {
          values: []
        }
      },
      {
        key: 'name',
        title: wording.general.name,
        width: 300,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'delete',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: (user: IUser) => actions.delete(user),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.add,
        tooltip: wording.general.add,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: actions.add
      }
    ],
    contextMenuButtons: [],
  };
};
