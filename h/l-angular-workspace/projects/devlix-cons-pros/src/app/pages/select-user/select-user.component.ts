import { Component } from '@angular/core';
import {Field} from 'l-builder';

@Component({
  selector: 'app-select-user',
  templateUrl: './select-user.component.html',
  styleUrls: ['./select-user.component.less']
})
export class SelectUserComponent {
    fieldsConfig: Field[] = [
        {
            component: 'input',
            name: 'userId',
            descriptors: {
                placeholder: 'select user: ex. lilit hovhannisyan'
            },
            validationRules: {
                required: {
                    value: true,
                    customErrMsg: 'User name is required'
                }
            }
        }
    ];

}