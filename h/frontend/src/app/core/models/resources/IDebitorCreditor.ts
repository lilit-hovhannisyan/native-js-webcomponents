import { IWithLabels } from './IWithLabels';
import { IResource } from './IResource';
import { IBookingAccount } from './IBookingAccount';
import { ICompany } from './ICompany';
import { IWording } from './IWording';
import { IWithLabel } from './IWithLabel';

export type DebitorCreditorKey = number;

interface ISharedProps extends IWithLabels, IWithLabel {
  no: number;
  creditorAccountId: number;
  debitorAccountId: number;
  isActive: boolean;
  remark: string;
  customersGroup: string;
  companyId: number;
  isIntercompanyRelevant: boolean;
}

export interface IDebitorCreditorBase extends ISharedProps, IResource<number> { }

export interface IDebitorCreditorCreate extends ISharedProps { }

export interface IDebitorCreditorDTO extends IDebitorCreditorBase { }

export interface IDebitorCreditor extends IDebitorCreditorDTO { }

export interface IDebitorCreditorFull extends IDebitorCreditor {
  creditorAccount: IBookingAccount;
  debitorAccount: IBookingAccount;
  company: ICompany;
}

export interface IDebitorCreditorRow extends IDebitorCreditorFull {
  creditorAccountName: IWording;
  debitorAccountName: IWording;
  companyName: string;
}

export interface IDebitorCreditorBasicTab extends IDebitorCreditor {
  creditorAccountNo?: string;
  debitorAccountNo?: string;
}




