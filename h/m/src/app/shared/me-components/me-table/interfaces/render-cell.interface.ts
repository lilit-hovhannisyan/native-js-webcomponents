import RederedComponentI from './rendered-component.interface';

interface RenderCellI extends RederedComponentI {
  forField: string;
}

export default RenderCellI;
