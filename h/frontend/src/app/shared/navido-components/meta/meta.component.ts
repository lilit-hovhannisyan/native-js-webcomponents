import { ChangeDetectorRef, Component, InjectionToken, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { shareReplay, tap } from 'rxjs/operators';
import { AbstractRepository, RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { wording } from 'src/app/core/constants/wording/wording';
import { IMeta } from 'src/app/core/models/resources/IMeta';
import { IResource, ResourceKey } from 'src/app/core/models/resources/IResource';
import { UserKey } from 'src/app/core/models/resources/IUser';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { CommonNotificationsService } from 'src/app/core/services/helperService/common-notifications.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';

export interface MetaComponentData<Entity extends IResource<ResourceKey>, EntityCreate> {
  service: InjectionToken<AbstractRepository<Entity,
    IResource<ResourceKey>,
    EntityCreate,
    RepoParams<Entity>,
    ResourceKey
  >>;
  fetchOneMethodName?: string;
}
@Component({
  templateUrl: './meta.component.html',
  styleUrls: ['./meta.component.scss']
})
export class MetaComponent<Entity extends IMeta, EntityCreate>
  implements OnInit {
  public entity: Entity;
  public form: FormGroup;
  public wording = wording;
  public editMode = false;

  private isSuperAdmin: boolean;

  private metaRepositoryService: AbstractRepository<
    Entity,
    IResource<ResourceKey>,
    EntityCreate,
    RepoParams<Entity>,
    ResourceKey
  >;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private userRepo: UserRepositoryService,
    private injector: Injector,
    private commonNotificationsService: CommonNotificationsService,
    private authService: AuthenticationService,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    this.isSuperAdmin = this.authService.getUser().isSuperAdmin;
    this.route.data.subscribe((value: MetaComponentData<Entity, EntityCreate>) => {
      this.metaRepositoryService = this.injector
        .get<AbstractRepository<
          Entity,
          IResource<ResourceKey>,
          EntityCreate,
          RepoParams<Entity>,
          ResourceKey>
        >(value.service);
      forkJoin([
        this.fetchOne(value.fetchOneMethodName),
        this.userRepo.fetchAll({})
      ]).subscribe(([entity, users]) => {
        this.entity = entity;
        this.initForm(this.entity);
        this.cdr.detectChanges();
      });
    });
  }

  public enterEditMode() {
    if (this.entity && this.entity['isSystemDefault'] && !this.isSuperAdmin) {
      this.commonNotificationsService.showSystemDefaultWarning();
      return;
    }
    this.editMode = true;
    this.form.get('remarks').enable();
  }

  public cancelEditing() {
    this.editMode = false;
    this.form.disable();
    this.initForm(this.entity);
  }

  private initializeResource(resource: Entity) {
    this.entity = resource;
  }

  public submit() {
    this.entity.remarks = this.form.value.remarks;
    this.metaRepositoryService.update(this.entity, {})
      .pipe(
        tap(freshResource => {
          this.initializeResource(freshResource);
          this.cancelEditing();
        }),
        shareReplay(1)
      ).subscribe();
  }

  private initForm(entity: Entity) {
    this.form = this.formBuilder.group({
      remarks: [{
        value: entity.remarks,
        disabled: !this.editMode
      }],
      createdById: [{
        value: entity.createdById,
        disabled: !this.editMode
      }],
      createdAt: [{
        value: entity.createdAt,
        disabled: !this.editMode
      }],
      editedById: [{
        value: entity.editedById,
        disabled: !this.editMode
      }],
      editedAt: [{
        value: entity.editedAt,
        disabled: !this.editMode
      }],
      editedByName: [{
        value: this.getNameOfUserId(entity?.editedById),
        disabled: !this.editMode
      }],
      createdByName: [{
        value: this.getNameOfUserId(entity?.createdById),
        disabled: !this.editMode
      }],
    });
  }

  private getNameOfUserId(id: UserKey) {
    const foundUser = this.userRepo.getStreamValue()
      .find(user => user.id === id);
    return foundUser ? `${foundUser.firstName} ${foundUser.lastName}` : '';
  }

  private fetchOne(methodName = 'fetchOne'): Observable<Entity> {
    const id = this.route.parent.snapshot.params.id
      || this.route.parent.parent.snapshot.params.id;
    return this.metaRepositoryService[methodName](id, {});
  }
}
