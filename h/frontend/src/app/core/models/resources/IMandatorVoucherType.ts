import { IResource } from './IResource';
import { IVoucherTypeBase } from './IVoucherType';

export type MandatorVoucherTypeKey = number;

interface ISharedProps {
  mandatorId: number;
  voucherTypeId: number;
  isAutomaticNumbering: boolean;
}

export interface IMandatorVoucherTypeBase extends ISharedProps, IResource<number>, IVoucherTypeBase { }

export interface IMandatorVoucherTypeCreate extends ISharedProps { }

export interface IMandatorVoucherTypeDTO extends IMandatorVoucherTypeBase { }

export interface IMandatorVoucherType extends IMandatorVoucherTypeDTO { }

export interface IMandatorVoucherTypeFull extends IMandatorVoucherType { }

export interface IMandatorVoucherTypeRow extends IMandatorVoucherTypeFull { }
