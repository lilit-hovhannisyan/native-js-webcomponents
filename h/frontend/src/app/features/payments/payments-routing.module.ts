import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { UnsavedChangesGuard } from 'src/app/routing/services/unsaved-changes.guard';
import { NavigationBoardComponent } from '../../shared/components/navigation-board/navigation-board.component';
import { PageNotFoundComponent } from '../../shared/components/page-not-found/page-not-found.component';
import { PaymentsPermissionsComponent } from './components/misc/payments-permissions/payments-permissions.component';
import { PaymentsPermissionsBasicComponent } from './components/misc/payments-permissions/payments-permissions-basic/payments-permissions-basic.component';
import { PermissionsHistoryComponent } from 'src/app/shared/components/permissions-history/permissions-history.component';

export const routes: Routes = [
  {
    path: '',
    component: NavigationBoardComponent,
    pathMatch: 'full'
  },
  {
    path: sitemap.payments.children.misc.path,
    children: [
      {
        path: '',
        component: NavigationBoardComponent,
        pathMatch: 'full'
      },
      {
        path: sitemap.payments.children.misc.children.paymentsPermissions.path,
        children: [{
          path: '',
          component: PaymentsPermissionsComponent,
          children: [{
            path: sitemap.payments.children.misc.children.paymentsPermissions.children.id.path,
            canDeactivate: [UnsavedChangesGuard],
            children: [
              {
                path: sitemap.payments.children.misc.children.paymentsPermissions.children.id.children.basic.path,
                component: PaymentsPermissionsBasicComponent,
                canDeactivate: [UnsavedChangesGuard],
              },
              {
                path: sitemap.payments.children.misc.children.paymentsPermissions.children.id.children.history.path,
                component: PermissionsHistoryComponent,
                data: {
                  entityKey: 'id',
                  entity: 'PaymentPermission',
                  entityName: 'payments/permissions',
                },
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.payments.children.misc.children.paymentsPermissions.children.id.children.basic.path,
              }
            ]
          }],
        }],
      },
    ],
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  }
];

export const ROUTING_DECLARATIONS = [
  PaymentsPermissionsComponent,
  PaymentsPermissionsBasicComponent,
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentsRoutingModule {
}

