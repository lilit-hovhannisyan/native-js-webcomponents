import { EnumValueModel } from '@shared/models/enum/enum-value.model';

interface SelectComponentDataI {
  displayName: string;
  internalName: string;
  value: string;
  values: EnumValueModel[];
}

export default SelectComponentDataI;
