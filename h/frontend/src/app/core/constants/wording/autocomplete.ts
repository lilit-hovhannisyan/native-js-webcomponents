export const wordingAutocomplete = {
  selectRow: { en: 'Select Row', de: 'Auswählen' },
  doesNotExist: { en: 'doesn\'t exist', de: 'existiert nicht' },
  is: { en: 'is', de: 'ist' },
  notAvailable: { en: 'not available', de: 'darf nicht benutzt werden' },
  pleaseChooseOrTalk: {
    en: 'Please choose another or speak to person in charge',
    de: 'Bitte wählen Sie einen anderen aus oder sprechen Sie mit dem Verantwortlichen'
  },
  set: { en: 'Set', de: 'Lege' },
  close: { en: 'Close', de: 'Schließen' },
  choose: { en: 'Choose', de: 'Wähle' },
  locked: { en: 'locked', de: 'gesperrt' },
  hidden: { en: 'hidden', de: 'verborgen' },
  notActive: { en: 'Not Active', de: 'Nicht Aktiv' },
  notAllowed: { en: 'not allowed', de: 'nicht erlaubt' },
  notRelatedYet: { en: 'not yet related', de: 'noch nicht verknüpft' },
  showAll: { en: 'Show all', de: 'Alle anzeigen' }
};
