import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { wording } from 'src/app/core/constants/wording/wording';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';

@Component({
  selector: 'mandator-tax-rel-info',
  templateUrl: './tax-rel-info.component.html',
  styleUrls: ['./tax-rel-info.component.scss'],
})
export class MandatorTaxRelInfoComponent implements OnInit {
  @Input() public mandatorId: number;
  public wording = wording;
  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private mandatorRepo: MandatorRepositoryService,
    private companyRepo: CompanyRepositoryService,
  ) { }

  public ngOnInit() {
    this.fetchRelInfo();
  }

  private fetchRelInfo() {
    this.mandatorRepo.fetchOne(this.mandatorId, {})
      .pipe(switchMap(mandator => {
        return this.companyRepo.fetchOne(mandator.companyId, {});
      }))
      .subscribe(company => {
        const { taxNo, vatId } = company;

        this.form = this.fb.group({
          taxNo: [taxNo],
          vatId: [vatId],
        });
        this.form.disable();
      });
  }
}
