import { TaxKeyKey } from './../../models/resources/ITaxKeys';
import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { ITaxKey, ITaxKeyDTO, ITaxKeyCreate } from '../../models/resources/ITaxKeys';
import { RepositoryService } from '../repository.service';
import { getLabelKeyByLanguage } from '../../helpers/general';
import { SettingsService } from '../settings.service';

export interface TaxKeyParams extends RepoParams<ITaxKey> {
  mandatorId?: TaxKeyKey;
}

@Injectable({
  providedIn: 'root'
})
export class TaxKeyRepositoryService extends AbstractRepository<
ITaxKey,
ITaxKeyDTO,
ITaxKeyCreate,
TaxKeyParams,
TaxKeyKey
>  {
  protected resourceType = 'TaxKey';
  public sortAsce = true;
  public sortKey = getLabelKeyByLanguage(this.settingsService.language);
  constructor(repositoryService: RepositoryService,
    private settingsService: SettingsService) {
    super(repositoryService);
  }

  protected postProcess(item: ITaxKeyDTO): ITaxKey { return item; }

  public getBaseUrl(params: TaxKeyParams): string {
    return params.mandatorId
      ? `/api/mandators/${params.mandatorId}/tax-keys`
      : '/api/tax-keys/';
  }
}
