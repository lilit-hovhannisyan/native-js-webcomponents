import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { NvGridConfig, GridComponent, NvColumnConfig, NvExcelExportInfo } from 'nv-grid';
import { getMainGridConfig, getSideGridConfig } from 'src/app/core/constants/nv-grid-configs/open-item-account-sheets.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { SettingsService } from 'src/app/core/services/settings.service';
import { DEBIT_CREDIT } from 'src/app/core/models/enums/debitCredit';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { takeUntil, tap, switchMap, distinctUntilChanged, startWith, map } from 'rxjs/operators';
import { Observable, ReplaySubject, forkJoin, BehaviorSubject, EMPTY, of, combineLatest } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { IMandatorLookup, MandatorKey } from 'src/app/core/models/resources/IMandator';
import { ICurrency, CurrencyKey } from 'src/app/core/models/resources/ICurrency';
import { BookingAccountKey, IBookingAccount } from 'src/app/core/models/resources/IBookingAccount';
import { IBalance } from 'src/app/core/models/resources/IBalance';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { ACCOUNTING_TYPES, accountingTypeOptionsFull } from 'src/app/core/models/enums/accounting-types';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { IBooking } from 'src/app/core/models/resources/IBooking';
import { IBookingLine } from 'src/app/core/models/resources/IBookingLine';
import { IWording } from 'src/app/core/models/resources/IWording';
import { BookingRepositoryService } from 'src/app/core/services/repositories/booking-repository.service';
import { OpenItemAccountSheetsRepositoryService, OpenItemAccountSheetsQueryParams } from 'src/app/core/services/repositories/open-item-account-sheets-repository.service';
import { IOpenItemAccountSheetsFull, IOpenItemAccountSheets } from 'src/app/core/models/resources/IOpenItemAccountSheets';
import { convertToMap, getDateTimeFormat, getLocalDateTime } from '../../../../../shared/helpers/general';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { IBookingCode, BookingCodeKey } from 'src/app/core/models/resources/IBookingCode';
import { IMandatorAccount } from 'src/app/core/models/resources/IMandatorAccount';
import { getParentScreenUrl } from 'src/app/core/helpers/makeUrl';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { AccountValidatorsService } from 'src/app/core/services/validators/account-validators.service';
import { MandatorValidatorsService } from 'src/app/core/services/validators/mandator-validators.service';
import { ExcelExportService } from '../../../helpers/excel-export-helper.service';

interface ISummaryLine {
  currencyId: CurrencyKey;
  currencyIsoCode: string;
  debit: number;
  credit: number;
}

@Component({
  selector: 'nv-open-item-account-sheets',
  templateUrl: './open-item-account-sheets.component.html',
  styleUrls: ['./open-item-account-sheets.component.scss']
})
export class OpenItemAccountSheetsComponent implements OnInit, OnDestroy {
  @ViewChild('mainGrid') public mainGrid: GridComponent;

  public gridConfig: NvGridConfig;
  public sideGridConfig: NvGridConfig;

  public form: FormGroup;
  public formSummary: FormGroup;

  public wording = wording;

  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  private currencyLinesSubject: ReplaySubject<ISummaryLine[]> = new ReplaySubject<ISummaryLine[]>(1);
  public currencyData$ = this.currencyLinesSubject.asObservable();

  public currencyLines: ISummaryLine[];

  public displayedCurrencies$ = new BehaviorSubject<ICurrency[]>([]);

  public accountingTypeOptions: ISelectOption<ACCOUNTING_TYPES>[] = [];

  public customTitle: IWording = wording.accounting.openItemAccountSheets;

  private openItemLinesObject: ReplaySubject<IOpenItemAccountSheetsFull[]> = new ReplaySubject<any[]>(1);
  public openItemLines$ = this.openItemLinesObject.asObservable();

  public accountSubject: BehaviorSubject<IBookingAccount[]> = new BehaviorSubject<IBookingAccount[]>([]);
  public accountLookup$ = this.accountSubject.asObservable();

  public comesFromAccountSheets = false;
  public parentUrl = getParentScreenUrl(this.router.url);

  public state: any;
  private dateTimeFormat: string;

  constructor(
    private formBuilder: FormBuilder,
    public gridConfigService: GridConfigService,
    private authService: AuthenticationService,
    private mandatorValidatorsService: MandatorValidatorsService,
    private accountValidatorService: AccountValidatorsService,
    private settingsService: SettingsService,
    public mandatorAccountRepo: MandatorAccountRepositoryService,
    public bookingAccountRepo: BookingAccountRepositoryService,
    private router: Router,
    private route: ActivatedRoute,
    public mandatorRepo: MandatorRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private bookingRepo: BookingRepositoryService,
    private openItemsRepositoryService: OpenItemAccountSheetsRepositoryService,
    private bookingCodeRepo: BookingCodeRepositoryService,
    private excelExportService: ExcelExportService,
  ) {
  }

  public ngOnInit() {
    this.dateTimeFormat = getDateTimeFormat(this.settingsService.locale);
    this.initForm();
    this.initFormSummary();
    this.mandatorIdChanges();
    this.filtersChangesHandler();
    this.mandatorBookingChangesHandler();
    this.currencyIdChanges();
    this.updateDisplayedCurrencies();

    forkJoin([
      this.mandatorRepo.fetchLookups({}),
      this.bookingRepo.fetchAll({}),
      this.bookingCodeRepo.fetchAll({}),
      this.bookingAccountRepo.fetchAll({})
    ]).pipe(
      takeUntil(this.componentDestroyed$),
      tap(() => {
        if (history.state.selectedMandatorId) {
          this.comesFromAccountSheets = true;
          this.state = {
            selectedMandatorId: history.state.selectedMandatorId,
            selectedAccountId: history.state.selectedAccountId,
            selectedNorm: history.state.selectedNorm,
            selectedYear: history.state.selectedYear
          };
          this.form.get('bookingMandatorId').setValue(history.state.selectedMandatorId);
          this.form.get('bookingAccountId').setValue(history.state.selectedAccountId);
          this.form.get('accountingNorm').setValue(history.state.selectedNorm);
          this.form.disable();
        }
      })
    ).subscribe();

    const actions = {
      show: (bookingLine: IBookingLine) => this.showBooking(bookingLine)
    };

    this.gridConfig = getMainGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation),
      { debitCredit: this.debitCreditCustomFormat },
      this.onExcelExport
    );

    this.sideGridConfig = getSideGridConfig(this.settingsService.locale);

    this.openItemLinesObject.next([]);
    this.currencyLinesSubject.next([]);
  }

  public onExcelExport = (excelExportInfo: NvExcelExportInfo): void => {
    const year = new Date().getFullYear();
    const mandatorId = this.form.get('bookingMandatorId').value;
    const bookingAccountId = this.form.get('bookingAccountId').value;
    const accountingNormId: ACCOUNTING_TYPES = this.form.get('accountingNorm').value;

    this.excelExportService.fetchExcelExportInfo(excelExportInfo, year, mandatorId, bookingAccountId, accountingNormId);
  }

  public goToPreviousRoute() {
    const parentUrl = url(sitemap.accounting.children.booking.children.accountSheets);
    this.router.navigate([parentUrl], { state: this.state });
  }

  public goToParentUrl() {
    const parentUrl = getParentScreenUrl(this.router.url);
    this.router.navigate([parentUrl]);
  }

  public showBooking = (bookingLine: IBookingLine) => {
    window.open(`${url(sitemap.accounting.children.booking.children.bookings)}/${bookingLine.bookingId}?readOnly=true`);
  }

  private filtersChangesHandler(): void {
    const { bookingAccountId, accountingNorm } = this.form.controls;

    combineLatest([
      bookingAccountId.valueChanges.pipe(distinctUntilChanged()),
      accountingNorm.valueChanges.pipe(distinctUntilChanged()),
    ]).pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(() => this.search());
  }

  private setCustomTitle(type?: ACCOUNTING_TYPES): void {
    const defaultWording: IWording = wording.accounting.openItemAccountSheets;
    const norm: ISelectOption<ACCOUNTING_TYPES> = accountingTypeOptionsFull
      .find(option => option.value === type);
    const normWording: IWording = norm?.displayLabel;

    this.customTitle = type
      ? {
        en: `${defaultWording.en} (${normWording?.en})`,
        de: `${defaultWording.de} (${normWording?.de})`,
      }
      : defaultWording;
  }

  private mandatorBookingChangesHandler(): void {
    const {
      bookingMandatorId: mandatorCtrl,
      bookingAccountId: bookingAccountCtrl,
    } = this.form.controls;

    combineLatest([
      mandatorCtrl.valueChanges.pipe(distinctUntilChanged()),
      bookingAccountCtrl.valueChanges.pipe(distinctUntilChanged()),
    ]).pipe(
      takeUntil(this.componentDestroyed$),
      startWith([]),
    ).subscribe(() => {
      const { bookingMandatorId, bookingAccountId } = this.form.getRawValue();

      const mandator: IMandatorLookup = this.mandatorRepo
        .getLookupStreamValue()
        .find(m => m.id === bookingMandatorId);
      const bookingAccount: IBookingAccount = this.bookingAccountRepo
        .getStreamValue()
        .find(account => account.id === bookingAccountId);

      this.setAccountingNorms(mandator, bookingAccount);
    });
  }

  private setAccountingNorms(
    mandator: IMandatorLookup,
    bookingAccount: IBookingAccount,
  ): void {
    if (!(mandator && bookingAccount)) {
      this.accountingTypeOptions = accountingTypeOptionsFull.map(option => ({
        ...option,
        disabled: true,
      }));
      return;
    }

    this.setAccountingNormOptions(mandator, bookingAccount);
    this.setAccountingNormDefault(mandator.accountingType);
  }

  private setAccountingNormOptions(
    mandator: IMandatorLookup,
    bookingAccount: IBookingAccount,
  ): void {
    const allBookings: IBooking[] = this.bookingRepo.getStreamValue();

    this.accountingTypeOptions = accountingTypeOptionsFull.map(option => {
      const hasBooking: boolean = allBookings.some(booking => {
        if (booking.accountingType !== option.value) {
          return false;
        }

        return booking.bookingLines.some((bookingLine: IBookingLine) => {
          return bookingLine.mandatorId === mandator.id
            && bookingLine.bookingAccountId === bookingAccount.id;
        });
      });

      return {
        ...option,
        disabled: !hasBooking,
      };
    }).filter(({ value, disabled }) => {
      const exists: boolean = !!(
        bookingAccount.accountingType
        & mandator.accountingType
        & value
      );

      return exists || !disabled;
    });
  }

  private setAccountingNormDefault(accountingType: number): void {
    const { accountingNorm } = this.form.controls;

    /**
     * To check norm is enabled or not just needed to use binary-and operator.
     *
     * `enabledNorms & ACCOUNTING_TYPES.HGB` returns `ACCOUNTING_TYPES.HGB` if
     * HGB is not disabled, otherwise it returns 0
     */
    let enabledNorms: number;
    let existedNorms: number;
    this.accountingTypeOptions.forEach(({ disabled, value }) => {
      existedNorms = existedNorms | value;
      enabledNorms = disabled ? enabledNorms : enabledNorms | value;
    }, 0);

    if (existedNorms & accountingNorm.value) {
      return;
    }

    // Keep order
    const norm: ACCOUNTING_TYPES = ACCOUNTING_TYPES.HGB & enabledNorms & accountingType
      || ACCOUNTING_TYPES.IFRS & enabledNorms & accountingType
      || ACCOUNTING_TYPES.USGAAP & enabledNorms & accountingType
      || ACCOUNTING_TYPES.EBalance & enabledNorms & accountingType
      || null;
    accountingNorm.setValue(norm);
  }

  private updateDisplayedCurrencies(): void {
    // Enable or disable the currency selectbox
    const currencyControl: AbstractControl = this.formSummary.get('currencyId');
    if (this.displayedCurrencies$.value.length === 0) {
      currencyControl.disable();
    } else {
      currencyControl.enable();
    }

    // Try to select currency from booking line grid filter
    let selectCurrencyValue = null;
    const currencyColumn: NvColumnConfig = this.mainGrid?.gridConfig?.columns
      .find(column => column.key === 'currencyIsoCode');

    const filterCurrencyIsoCode = currencyColumn?.filter?.values[0];

    if (filterCurrencyIsoCode && typeof filterCurrencyIsoCode === 'string') {
      const filterCurrency = this.displayedCurrencies$.value
        .find(c => c.isoCode === filterCurrencyIsoCode);
      selectCurrencyValue = filterCurrency?.id;
    }

    // If no value is selected, select default
    currencyControl.patchValue(selectCurrencyValue || 'default');
  }


  private initFormSummary(): void {
    this.formSummary = this.formBuilder.group({
      currencyId: null
    });
  }

  private currencyIdChanges(): void {
    this.formSummary.get('currencyId').valueChanges
      .pipe(
        distinctUntilChanged(),
        takeUntil(this.componentDestroyed$)
      ).subscribe(currencyId => {
        this.handleCurrencyIdChanges(currencyId);
      });
  }

  public handleGridFilterChanged(): void {
    this.updateDisplayedCurrencies();
    this.updateSummaryGrid();
  }

  private handleCurrencyIdChanges(currencyId: any): void {
    const currency: ICurrency = this.getCurrency(currencyId);

    // Set column grid filter condition
    const currencyColumn = this.mainGrid?.gridConfig?.columns
      .find(column => column.key === 'currencyIsoCode');

    if (!currencyColumn) {
      return;
    }

    if (currency) {
      currencyColumn.filter.values = [currency.isoCode];
      currencyColumn.filter.form?.get('value')
        .patchValue(currency.isoCode, { emitEvent: false });
    } else {
      currencyColumn.filter.values = [];
      currencyColumn.filter.form?.get('value').patchValue(null, { emitEvent: false });
    }

    this.mainGrid.filterGrid();
  }

  private getCurrency(currencyId: number): ICurrency {
    return this.currencyRepo.getStreamValue().find(currency => currency.id === currencyId);
  }

  private getCurrencyIsoCode(currencyId: number): string {
    const foundCurrency: ICurrency = this.getCurrency(currencyId);
    return foundCurrency ? foundCurrency.isoCode : null;
  }

  private mandatorIdChanges(): void {
    this.form.get('bookingMandatorId').valueChanges
      .pipe(
        takeUntil(this.componentDestroyed$),
        switchMap(mandatorId => {
          if (mandatorId) {
            return this.mandatorAccountRepo.fetchAll({ queryParams: { mandatorId: mandatorId } });
          }
          return EMPTY;
        }),
        switchMap(mandatorAccount => this.accountLookup$ = this.updateBookingAccounts(mandatorAccount))
      ).subscribe(() => {
        if (!this.accountSubject.getValue()
          .some(e => e.id === this.form.get('bookingAccountId').value)) {
          this.form.get('bookingAccountId').patchValue(null);
        }

        this.form.get('bookingAccountId').updateValueAndValidity({ emitEvent: false });

        if (!this.form.get('bookingAccountId').valid) {
          this.openItemLinesObject.next([]);
          this.currencyLinesSubject.next([]);
        }
        this.search();
      });
  }

  private updateBookingAccounts(mandatorAccounts: IMandatorAccount[]): Observable<IBookingAccount[]> {
    const filteredAccounts: IMandatorAccount[] =
      mandatorAccounts.filter((mandatorAccount) => mandatorAccount.isOpenItem === true);

    if (filteredAccounts.length === 0) {
      this.openItemLinesObject.next([]);
      this.currencyLinesSubject.next([]);
    }

    return this.bookingAccountRepo.getStream()
      .pipe(
        map(bookingAccounts => bookingAccounts
          .filter(bookingAccount => filteredAccounts
            .some(a => a.bookingAccountId === bookingAccount.id))
        ),
        tap(bookingAccounts => {
          this.accountSubject.next(bookingAccounts);
        })
      );
  }

  private updateAccountLookup(filteredAccounts: IMandatorAccount[]) {
    this.accountLookup$ = this.bookingAccountRepo.getStream()
      .pipe(
        map(bookingAccounts => bookingAccounts
          .filter(bookingAccount => filteredAccounts
            .some(a => a.bookingAccountId === bookingAccount.id))
        )
      );

    this.accountLookup$
      .subscribe(mandatorAccounts => this.accountSubject.next(mandatorAccounts));
    if (!this.accountSubject.getValue()
      .some(e => e.id === this.form.get('bookingAccountId').value)) {
      this.form.get('bookingAccountId').patchValue(null);
    }
  }

  private debitCreditCustomFormat = (debitCredit: number): string => {
    return debitCredit === 1
      ? this.settingsService.debitCredit === DEBIT_CREDIT.plusMinus
        ? '+'
        : this.wording.accounting.DEBIT[this.settingsService.language]
      : debitCredit === -1
        ? this.settingsService.debitCredit === DEBIT_CREDIT.plusMinus
          ? '-'
          : this.wording.accounting.CREDIT[this.settingsService.language]
        : null;
  }

  private initForm(): void {
    this.form = this.formBuilder.group({
      bookingMandatorId: [null, [
        Validators.required,
        this.mandatorValidatorsService.doesMandatorExist,
        this.mandatorValidatorsService.isMandatorActive
      ]],
      bookingAccountId: [null, [
        Validators.required,
        this.accountValidatorService.doesBookingAccountExist,
        this.accountValidatorService.isBookingAccountActive,
      ]],
      accountingNorm: [null]
    });
  }

  public isButtonDisabled(backwards = false): boolean {

    const bookingAccountControl = this.form.get('bookingAccountId');

    const accountsOfMandators: IBookingAccount[] = this.accountSubject.getValue();

    const currentIndex = accountsOfMandators
      .findIndex(bookingAccount =>
        bookingAccount.id === bookingAccountControl.value);

    return backwards
      ? currentIndex < 1
      : currentIndex === accountsOfMandators.length - 1;
  }
  public next(backwards = false): void {
    const accountsOfMandators: IBookingAccount[] = this.accountSubject.getValue();

    const currentIndex = accountsOfMandators
      .findIndex(bookingAccount =>
        bookingAccount.id === this.form.get('bookingAccountId').value);

    const newIndex = backwards ? currentIndex - 1 : currentIndex + 1;
    const foundBookingAccount = accountsOfMandators[newIndex];

    if (foundBookingAccount) {
      this.form.get('bookingAccountId').patchValue(foundBookingAccount.id);
    }
  }

  public search(): void {
    this.openItemsRepositoryService.clearStream();
    this.displayedCurrencies$.next([]);

    // clear streams, updateCurrency Grid, fetch results from corresponding service,
    // subscribe and fill grids
    const { bookingMandatorId, bookingAccountId, accountingNorm } = this.form.controls;


    const selectedMandatorId: MandatorKey = bookingMandatorId.value;
    const selectedAccountId: BookingAccountKey = bookingAccountId.value;
    const selectedNorm: ACCOUNTING_TYPES = accountingNorm.value;

    this.setCustomTitle(selectedNorm);

    if (!(selectedMandatorId && selectedAccountId && accountingNorm)) {
      return;
    }

    const queryParams: OpenItemAccountSheetsQueryParams = {
      mandatorId: selectedMandatorId,
      bookingAccountId: selectedAccountId,
      accountingType: selectedNorm,
    };

    const openItemsRequest$: Observable<IOpenItemAccountSheets[]> = selectedNorm
      ? this.openItemsRepositoryService.fetchAll({ queryParams })
      : of([]);

    openItemsRequest$
      .subscribe((openItems) => {
        const openItemsFull = this.convertToOpenItemsFull(openItems);
        this.openItemLinesObject.next(openItemsFull);
        // Get currencies from main grid
        const currencies = openItemsFull.reduce((result, current) => {
          if (!result.some(e => e.id === current.currencyId)) {
            return [...result, this.getCurrency(current.currencyId)];
          } else {
            return result;
          }
        }, []);

        this.displayedCurrencies$.next(currencies);
        this.updateDisplayedCurrencies();
        this.updateSummaryGrid();
      });
  }

  private convertToOpenItemsFull(openItems: IOpenItemAccountSheets[]): IOpenItemAccountSheetsFull[] {

    const bookingCodesMap: Map<BookingCodeKey, IBookingCode> = convertToMap(
      this.bookingCodeRepo.getStreamValue(),
    );

    return openItems.map((openItem: IOpenItemAccountSheets) => {
      const oppositeAccount = this.bookingAccountRepo.getStreamValue()
        .find(bookingAccount => bookingAccount.id === openItems[0].oppositeBookingAccountId);

      const bookingCode: IBookingCode = bookingCodesMap.get(openItem.bookingCodeId);

      const bookingCodeWording: IWording = {
        en: `${bookingCode.no} ${bookingCode.labelEn}`,
        de: `${bookingCode.no} ${bookingCode.labelDe}`,
      };

      return ({
        ...openItem,
        oppositeAccountNo: openItem.oppositeBookingAccountId === 0
          ? 'Diverse'
          : oppositeAccount
            ? oppositeAccount.no
            : null,
        accountName: oppositeAccount ? oppositeAccount.displayLabel : null,
        currencyIsoCode: this.getCurrencyIsoCode(openItem.currencyId),
        bookingCode: bookingCodeWording,
        bookingDate: getLocalDateTime(openItem.bookingDate, this.dateTimeFormat)
      });
    });
  }

  private updateSummaryGrid(): void {
    const selectedNorm = this.form.get('accountingNorm').value;
    const selectedMandatorId: MandatorKey = this.form.get('bookingMandatorId').value;
    const selectedBookingAccountId: BookingAccountKey = this.form.get('bookingAccountId').value;
    const openItemLines: IOpenItemAccountSheetsFull[] = this.getOpenItemLines();

    const isValid: Boolean = !selectedNorm || !selectedMandatorId
      || !selectedBookingAccountId || (!this.form.valid && !this.form.disabled)
      || openItemLines.length === 0;

    if (isValid) {
      this.currencyLines = [];
      this.currencyLinesSubject.next(this.currencyLines);
      return;
    }

    const selectedCurrencyId: CurrencyKey = +this.formSummary.get('currencyId').value;
    const selectedMandator: IMandatorLookup = this.mandatorRepo.getLookupStreamValue()
      .find(mandator => mandator.id === selectedMandatorId);

    // Should balances be displayed for all currencies or for a specific currency?
    const forAllCurrencies = selectedCurrencyId === null
      || selectedCurrencyId === undefined || isNaN(selectedCurrencyId);

    const amountCurrencyId: CurrencyKey = forAllCurrencies
      ? selectedMandator.currencyId
      : selectedCurrencyId;

    const amountCurrency: ICurrency = this.getCurrency(amountCurrencyId);

    this.currencyLines = [
      this.getSummaryLine(forAllCurrencies, amountCurrency, openItemLines)
    ];

    return this.currencyLinesSubject.next(this.currencyLines);
  }

  private getSummaryLine(
    forAllCurrencies: boolean,
    amountCurrency: ICurrency,
    openItemLines: IOpenItemAccountSheetsFull[]): ISummaryLine {

    let debitAmount = 0;
    let creditAmount = 0;

    if (forAllCurrencies) {
      const sumLines: IBalance = this.calcSumBalanceForAllCurrencies(openItemLines);
      if (sumLines) {
        // Note: the amount is recalculated from booking lines here,
        // based on grid filtering
        debitAmount = this.calcLocalAmountSumForRows(openItemLines, +1);
        creditAmount = this.calcLocalAmountSumForRows(openItemLines, -1);
      }
    } else {
      const openLine: IOpenItemAccountSheetsFull = openItemLines
        .find(b => b.currencyIsoCode === amountCurrency.isoCode);

      if (openLine) {
        // Note: the amount is recalculated from booking lines here, based on grid filtering
        debitAmount = this.calcTransactionAmountSumForRows(openItemLines, +1, amountCurrency.isoCode);
        creditAmount = this.calcTransactionAmountSumForRows(openItemLines, -1, amountCurrency.isoCode);
      }
    }

    const summaryLine = {
      currencyId: amountCurrency.id,
      currencyIsoCode: amountCurrency.isoCode,
      debit: debitAmount,
      credit: creditAmount
    } as ISummaryLine;

    return summaryLine;
  }

  private calcSumBalanceForAllCurrencies(openLines: IOpenItemAccountSheetsFull[]): IBalance {
    // Sum up local amounts for all grid lines,

    // Note: local amount is in mandator currency, so it can be added up.
    return openLines
      .reduce((sumBalance, currentBalance) => {
        sumBalance.creditAmountLocal += currentBalance.debitCreditType < 0 ? currentBalance.amountLocal : 0;
        sumBalance.debitAmountLocal += currentBalance.debitCreditType > 0 ? currentBalance.amountLocal : 0;
        return sumBalance;
      },
        // Start value
        {
          creditAmountLocal: 0,
          debitAmountLocal: 0,
        } as IBalance);
  }

  public calcTransactionAmountSumForRows(
    openLines: IOpenItemAccountSheetsFull[],
    debitCreditType: number,
    currencyIsoCode: string): number {

    return this.sumOf(
      openLines
        .filter(row =>
          row.debitCreditType === debitCreditType && row.currencyIsoCode === currencyIsoCode)
        .map(row => row.amountTransaction));
  }


  public calcLocalAmountSumForRows(
    accountStatements: IOpenItemAccountSheetsFull[],
    debitCreditType: number): number {
    return this.sumOf(
      accountStatements
        .filter(row => row.debitCreditType === debitCreditType)
        .map(row => row.amountLocal));
  }

  private sumOf(array: any[]): number {
    return +Math.abs(array.reduce((prev, cur) => +prev + +cur, 0));
  }

  private getOpenItemLines(): IOpenItemAccountSheetsFull[] {
    return this.mainGrid
      ? this.mainGrid.rawRows
      : [];
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

}
