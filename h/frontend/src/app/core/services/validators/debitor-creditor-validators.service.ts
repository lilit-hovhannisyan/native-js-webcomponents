import { ValidationErrors, AbstractControl } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { Injectable } from '@angular/core';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { DebitorCreditorRepositoryService } from 'src/app/core/services/repositories/debitor-creditor-repository.service';


@Injectable({
  providedIn: 'root'
})
export class DebitorCreditorValidatorsService {

  constructor(private debitorCreditorRepo: DebitorCreditorRepositoryService) { }

  public doesDdebitorCreditorExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundDebitorCreditor = this.debitorCreditorRepo.getStreamValue()
      .find(debitorCreditor => debitorCreditor.id === control.value);

    const isValid = foundDebitorCreditor ? true : false;
    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      debitorCreditorNotFound: {
        message: wording.accounting.notFoundDebitorCreditor
      }
    };
  }

  public isDebitorCreditorActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const foundDebitorCreditor = this.debitorCreditorRepo.getStreamValue()
      .find(debitorCreditor => debitorCreditor.id === control.value);

    const isValid = foundDebitorCreditor
      ? foundDebitorCreditor.isActive
      : false;

    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      debitorCreditorNotActive: {
        message: wording.accounting.notActiveDebitorCreditor
      }
    };
  }
}
