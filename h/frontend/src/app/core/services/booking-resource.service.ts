import { Injectable } from '@angular/core';
import { BehaviorSubject, forkJoin } from 'rxjs';
import { IPaymentCondition, IPaymentStepRow } from 'src/app/core/models/resources/IPaymentConditions';
import { BookingRepositoryService } from 'src/app/core/services/repositories/booking-repository.service';
import { DebitorCreditorRepositoryService } from 'src/app/core/services/repositories/debitor-creditor-repository.service';
import { PaymentConditionsRepositoryService } from 'src/app/core/services/repositories/payment-conditions-repository.service';
import { BookingAccountRepositoryService } from 'src/app/core/services/repositories/booking-account-repository.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { VoucherTypeRepositoryService } from 'src/app/core/services/repositories/voucher-type-repository.service';
import { map } from 'rxjs/operators';
import { TaxKeyRepositoryService } from 'src/app/core/services/repositories/tax-key-repository.service';
import { MandatorClosedYearsRepositoryService } from 'src/app/core/services/repositories/mandator-closed-year-repository.service';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { MandatorBlockedYearsRepositoryService } from 'src/app/core/services/repositories/mandator-blocked-years-repository.service';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { CostTypeRepositoryService } from 'src/app/core/services/repositories/cost-type-repository.service';
import { AccountCostTypeRepositoryService } from 'src/app/core/services/repositories/account-cost-type-repository.service';
import { CostCenterRepositoryService } from 'src/app/core/services/repositories/cost-center-repository.service';
import { CostUnitRepositoryService } from './repositories/cost-unit-repository.service';

export interface IPaymentConditionOfAccountData {
  [bookingAccountId: number]: IPaymentStepRow[];
}

@Injectable({
  providedIn: 'root'
})
export class BookingResourceService {
  public paymentConditions: BehaviorSubject<IPaymentCondition[]> = new BehaviorSubject<IPaymentCondition[]>([]);
  public paymentConditionsOfAccounts: BehaviorSubject<IPaymentConditionOfAccountData> = new BehaviorSubject({});

  constructor(
    private bookingRepo: BookingRepositoryService,
    private mandatorRepo: MandatorRepositoryService,
    private debitorCreditorRepo: DebitorCreditorRepositoryService,
    private paymentConditionsRepo: PaymentConditionsRepositoryService,
    private bookingAccountRepo: BookingAccountRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private bookingCodeRepo: BookingCodeRepositoryService,
    private voucherTypeRepo: VoucherTypeRepositoryService,
    private taxKeyRepo: TaxKeyRepositoryService,
    private mandatorClosedYearsRepo: MandatorClosedYearsRepositoryService,
    private mandatorBlockedYearsRepo: MandatorBlockedYearsRepositoryService,
    private mandatorAccountRepo: MandatorAccountRepositoryService,
    private costTypeRepo: CostTypeRepositoryService,
    private accountCostTypeRepo: AccountCostTypeRepositoryService,
    private costCenterRepo: CostCenterRepositoryService,
    private costUnitRepo: CostUnitRepositoryService,
  ) { }
  public fetch() {
    return this.fetchResources().pipe(
      map((
        [
          bookingAccounts, currencies, voucherTypes, mandators,
          [
            bookingCodes,
            bookings,
            closedYears,
            blockedYears,
            costCenters,
            costUnits,
          ],
          [
            debitorCreditors,
            paymentConditions,
            taxKeys,
            mandatorAccounts,
            costTypes,
          ]
        ]) => {
        this.paymentConditions.next(paymentConditions);
      })
    );
  }

  private fetchResources() {
    // Note: forkJoin allows up to 6 Observable as parameters.
    // As a workaround nested forkJoin calls are used here.
    return forkJoin([
      this.bookingAccountRepo.fetchAll({}),
      this.currencyRepo.fetchAll({ useCache: true }),
      this.voucherTypeRepo.fetchAll({}),
      this.mandatorRepo.fetchLookups({}),
      forkJoin([
        this.bookingCodeRepo.fetchAll({}),
        this.bookingRepo.fetchAll({}),
        this.mandatorClosedYearsRepo.fetchAll({}),
        this.mandatorBlockedYearsRepo.fetchAll({}),
        this.costCenterRepo.fetchAll({}),
        this.costUnitRepo.fetchAll({}),
      ]),
      forkJoin([
        this.debitorCreditorRepo.fetchAll({}),
        this.paymentConditionsRepo.fetchAll({}),
        this.taxKeyRepo.fetchAll({}),
        this.mandatorAccountRepo.fetchAll({}),
        this.costTypeRepo.fetchAll({}),
        this.accountCostTypeRepo.fetchAll({}),
      ])
    ]);
  }
}
