import { environment } from '@environments/environment';

// General
export const ME_MEV_DELIMITER: string = '|<∗>|';
export const ME_COPYRIGHT_YEAR: number = environment.COPYRIGHT_YEAR;

// API
const ME_BASE_API_URL: string = environment.BASE_API_URL;
export const MATE_BASE_API_URL: string = ME_BASE_API_URL + '/mate';
export const MARKETPLACE_BASE_API_URL: string = ME_BASE_API_URL + '/marketplace';
export const BACKOFFICE_BASE_API_URL: string = ME_BASE_API_URL + '/marketplace-backoffice';

// TRACKING API
export const ME_TRACKING_BASE_API_URL: string = environment.TRACKING_BASE_API_URL;

// Domains
export const ME_DOMAIN_APPLICATION_ADDRESS: string = 'MEAddress';
export const ME_DOMAIN_APPLICATION_BOOK: string = 'MEBook';
export const ME_DOMAIN_APPLICATION_BUCKET: string = 'MTBucket';
export const ME_DOMAIN_APPLICATION_CERTIFICATE: string = 'MECertificate';
export const ME_DOMAIN_APPLICATION_CERTIFICATE_TO_OBJECT: string = 'MECertificateToObject';
export const ME_DOMAIN_APPLICATION_CERTIFICATE_TO_COMPANY: string = 'MECertificateToObject$CertificateToCompany';
export const ME_DOMAIN_APPLICATION_CERTIFICATE_TO_MATERIAL: string = 'MECertificateToObject$CertificateToMaterial';
export const ME_DOMAIN_APPLICATION_COMPANY: string = 'MECompany';
export const ME_DOMAIN_APPLICATION_COMPANY_BRAND: string = 'MECompany$Brand';
export const ME_DOMAIN_APPLICATION_COMPANY_MANUFACTURER: string = 'MECompany$Manufacturer';
export const ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER: string = 'MECompany$Supplier';
export const ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER_MATERIAL: string = 'MECompany$Supplier$MaterialComponentSupplier';
export const ME_DOMAIN_APPLICATION_COLLECTION: string = 'MECollection';
export const ME_DOMAIN_APPLICATION_DOCUMENT: string = 'MEDocument';
export const ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL: string = 'MEDocument$Technical';
export const ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_CERTIFICATE: string = 'MEDocument$Technical$Certificate';
export const ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_GENERIC: string = 'MEDocument$Technical$Generic';
export const ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL: string = 'MEDocument$Visual';
export const ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL_VIDEO: string = 'MEDocument$Visual$Video';
export const ME_DOMAIN_APPLICATION_MATERIAL: string = 'MEMaterial';
export const ME_DOMAIN_APPLICATION_USER: string = 'MEUser';
export const ME_DOMAIN_APPLICATION_GROUP: string = 'MEGroup';
export const ME_DOMAIN_SYSTEM_COMPANY_MATERIAL_TYPES: string = 'MTCompanyMaterialTypes';
export const ME_DOMAIN_SYSTEM_ENUM_LIST: string = 'MTEnumList';
export const ME_DOMAIN_SYSTEM_ENUM_VALUE: string = 'MTEnumValue';
export const ME_DOMAIN_SYSTEM_FILE_STORE: string = 'MTFileStore';
export const ME_DOMAIN_SYSTEM_FILTER: string = 'MTFilter';
export const ME_DOMAIN_SYSTEM_GROUP: string = 'MTGroup';
export const ME_DOMAIN_SYSTEM_ORGANIZATION: string = 'MTOrganization';
export const ME_DOMAIN_SYSTEM_USER: string = 'MTUser';

// Links
export const ME_FDRA_SITE_URL: string = 'http://fdra.org/about-fdra/';
export const ME_USFIA_SITE_URL: string = 'https://www.usfashionindustry.com/';
export const ME_JOIN_MATERIAL_EXCHANGE: string = 'https://material-exchange.com/contact-us/';
export const ME_PRIVACY_NOTICE_URL: string = 'https://material-exchange.com/privacy-notice/';
export const ME_OMP_INTRO_VIDEO_URL: string = 'https://media.material-exchange.com/video/Darren%20-%20Welcome%20to%20Marketplace%201.mp4';
export const ME_OMP_USER_GUIDE_URL: string =
  'https://static.material-exchange.com/user-guides/Material_Exchange_Open_Marketplace_Full_Supplier_User_Guide.pdf';

// Device Breakpoints
export const ME_MOBILE: number = 480;
export const ME_TABLET_SMALL: number = 768;
export const ME_TABLET: number = 1024;
export const ME_LAPTOP: number = 1280;
export const ME_DESKTOP: number = 1680;
export const ME_DESKTOP_LARGE: number = 1681;

// RegEx
export const ME_URL_REGEX: RegExp = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
export const ME_USER_EMAIL_REGEX: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

// File Upload Types
export const ME_FILE_MATERIAL_THUMBNAIL = 'MATERIAL_THUMBNAIL';

// Login form content type
export const LOGIN_FORM_CONTENT_TYPE: { 'Content-Type': string } = { 'Content-Type': 'application/x-www-form-urlencoded' };

// Layout groups
export const MATERIAL_VIEW_GENERAL_INFO: string = 'materialGeneralInfo';
export const MATERIAL_VIEW_PHYSICAL_SAMPLE_BOOK_INFO: string = 'physicalSampleBookInfo';
export const MATERIAL_VIEW_BASIC_INFO: string = 'materialBasicInfo';

// Material create edit form attributes
export const MATERIAL_ATTRIBUTE_ASK_FOR_QUOTE: string = 'askForQuote';
export const MATERIAL_ATTRIBUTE_PRICE: string = 'price';
export const MATERIAL_ATTRIBUTE_PRICE_RMB: string = 'priceRMB';
export const MATERIAL_ATTRIBUTE_UOM: string = 'unitOfMeasure';

// Download
export const FORCE_DOWNLOAD: string = '?forceDownload=true';

// Default users group for adding users
export const DEFAULT_USERS_GROUP: string = 'Material Coordinators';

// Invited user placeholder
export const DEFAULT_INVITED_USER: string = '[INVITED USER]';
