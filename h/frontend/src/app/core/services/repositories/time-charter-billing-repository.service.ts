import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { TimeCharterKey } from '../../models/resources/ITimeCharter';
import {
  ITimeCharterBillingDTO,
  ITimeCharterBilling,
  TimeCharterBillingKey,
  ITimeCharterBillingCreate
} from '../../models/resources/ITimeCharterBilling';

export interface ITimeCharterBillingRepoParams extends RepoParams<ITimeCharterBilling> {
  queryParams?: {
    timeCharterId: TimeCharterKey;
    offset?: number;
    limit?: number;
  };
}

@Injectable({
  providedIn: 'root'
})
export class TimeCharterBillingRepositoryService extends AbstractRepository<
ITimeCharterBilling,
ITimeCharterBillingDTO,
ITimeCharterBillingCreate,
ITimeCharterBillingRepoParams,
TimeCharterBillingKey
>  {
  protected resourceType = 'TimeCharterBilling';
  protected sortKey = 'from';
  protected sortAsce = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/time-charter-billings';

  protected postProcess = (item: ITimeCharterBillingDTO): ITimeCharterBilling => item;
}
