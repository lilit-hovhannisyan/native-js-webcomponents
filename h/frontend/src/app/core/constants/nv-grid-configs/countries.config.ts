import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import {
  NvFilterControl, NvGridButtonsPosition, NvGridConfig, NvColumnDataType
} from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { Operations } from 'src/app/core/models/Operations';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'countriesGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 10
    },
    columns: [
      {
        key: 'countryFlagPath',
        title: wording.general.flag,
        width: 70,
        isSortable: false,
        filter: {
          values: []
        },
        customTooltip: () => wording.general.flag
      },
      {
        key: 'displayLabel',
        title: wording.general.name,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isoNo',
        title: wording.accounting.isoNumber,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isoAlpha2',
        title: wording.accounting.isoAlpha2,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Select,
          selectValues: []
        }
      },
      {
        key: 'isoAlpha3',
        title: wording.accounting.isoAlpha3,
        width: 150,
        isSortable: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Select,
          selectValues: []
        }
      },
      {
        key: 'nationality',
        title: wording.accounting.nationality,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'euMember',
        title: wording.accounting.euMember,
        width: 100,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'sepaParticipant',
        title: wording.accounting.sepaParticipant,
        width: 100,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'ibanLength',
        title: wording.accounting.invalidIban,
        width: 120,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'currencyIsoCode',
        title: wording.accounting.currencyIsoCode,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'bbkShort',
        title: wording.accounting.bbkShort,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'bbkNo',
        title: wording.accounting.bbkNo,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'bbkCode',
        title: wording.accounting.bbkCode,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      }
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: wording.general.edit,
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        func: actions.edit,
        hidden: !canPerform(Operations.UPDATE)
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: wording.general.delete,
        tooltip: wording.general.delete,
        func: actions.delete,
        hidden: !canPerform(Operations.DELETE)
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        func: actions.add,
        hidden: () => !canPerform(Operations.CREATE)
      }
    ],
    sortBy: 'displayLabel',
    isSortAscending: true,
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
