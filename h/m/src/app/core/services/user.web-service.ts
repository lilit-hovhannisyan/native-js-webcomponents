import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseWebService } from './base.web-service';

import { MARKETPLACE_BASE_API_URL, MATE_BASE_API_URL } from '@shared/constants';

import { UserProfileModel } from '@shared/models/user-profile/user-profile.model';

@Injectable({
  providedIn: 'root'
})
export class UserWebService {
  constructor(private baseWebService: BaseWebService) {}

  getCurrentUser(): Observable<UserProfileModel> {
    return this.baseWebService.getRequest<UserProfileModel>(`${MARKETPLACE_BASE_API_URL}/users/profile`, UserProfileModel);
  }

  logOut(): Observable<void> {
    return this.baseWebService.postRequest<void, undefined>(`${MATE_BASE_API_URL}/auth/logout`);
  }
}
