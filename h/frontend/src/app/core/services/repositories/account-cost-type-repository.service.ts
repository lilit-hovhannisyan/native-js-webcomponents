import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import {
  AccountCostTypeKey,
  IAccountCostType,
  IAccountCostTypeCreate,
  IAccountCostTypeDTO,
} from '../../models/resources/IAccountCostType';
import { RepositoryService } from '../repository.service';
import { HttpPost } from '../../models/http/http-post';
import { map, tap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';
import { IResourceCollection } from '../../models/resources/IResourceCollection';
import { Observable } from 'rxjs';
import { HttpRequest } from '@angular/common/http';
import { ICostType } from '../../models/resources/ICostTypes';

interface AccountCostTypeRepoParams extends RepoParams<IAccountCostType> {
  accountCostTypeId?: AccountCostTypeKey;
}
interface IAccountCostResp extends HttpResponse<IDataResponseBody<IResourceCollection<IAccountCostType>>> { }
@Injectable({
  providedIn: 'root'
})
export class AccountCostTypeRepositoryService extends AbstractRepository<
  IAccountCostType,
  IAccountCostTypeDTO,
  IAccountCostTypeCreate,
  AccountCostTypeRepoParams,
  AccountCostTypeKey
> {
  protected resourceType = 'AccountCostType';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl (repoParams: AccountCostTypeRepoParams): string {
    return (repoParams && repoParams?.accountCostTypeId)
      ? `/api/cost-accountings/${repoParams.accountCostTypeId}`
      : `/api/cost-accountings`;
  }

  protected postProcess(item: IAccountCostTypeDTO): IAccountCostType { return item; }

  /**
   *
   * @param {IAccountCostTypeCreate[]} ctList
   * @returns {Observable<IAccountCostType[]>}
   *
   * this entity has only 2 endpoints(Get, Post), and Post accepts list of items
   */
  public saveCtRelations(ctList: IAccountCostTypeCreate[]): Observable<IAccountCostType[]> {
    const request: HttpRequest<ICostType[]> = new HttpPost(this.getBaseUrl({}), ctList);

    return this.repositoryService.httpClient.request(request).pipe(
      map((response: IAccountCostResp) =>
          response.body.data.items
      ),
      tap((accountingcostTypes: IAccountCostType[]) => this.updateStream(accountingcostTypes))
    );
  }
}
