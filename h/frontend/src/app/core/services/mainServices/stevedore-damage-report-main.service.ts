import { Injectable } from '@angular/core';
import { forkJoin, Observable, combineLatest, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { IStevedoreDamageReport, IStevedoreDamageReportFull, IStevedoreDamageReportRow, reportTypeOptions, incidentOptions, positionSBPSOptions, positionFWDAFTOptions, repairByOptions, StevedoreDamageReportType, StevedoreDamageReportStatus } from 'src/app/core/models/resources/IStevedoreDamageReport';
import { RepoParams } from '../../abstracts/abstract.repository';
import { StevedoreDamageReportRepositoryService } from 'src/app/core/services/repositories/stevedore-damage-report-repository.service';
import { VoyagesRepositoryService } from '../repositories/voyages-repository.service';
import { DebitorCreditorRepositoryService } from 'src/app/core/services/repositories/debitor-creditor-repository.service';
import { PortRepositoryService } from '../repositories/port-repository.service';
import { IVoyage } from 'src/app/core/models/resources/IVoyage';
import { IVesselAccounting } from '../../models/resources/IVesselAccounting';
import { IDebitorCreditor } from '../../models/resources/IDebitorCreditor';
import { IPort } from '../../models/resources/IPort';
import { VesselAccountingRepositoryService } from '../repositories/vessel-accounting-repository.service';
import { CompanyRepositoryService } from '../repositories/company-repository.service';
import { ICompany } from '../../models/resources/ICompany';
import { VesselRepositoryService } from 'src/app/core/services/repositories/vessel-repository.service';
import { IVessel } from 'src/app/core/models/resources/IVessel';
import { IWording } from 'src/app/core/models/resources/IWording';
import { wording } from 'src/app/core/constants/wording/wording';
import { StevedoreRepairRepositoryService } from '../repositories/stevedore-repair-repository.service';
import { IStevedoreRepair, IStevedoreRepairFull } from '../../models/resources/IStevedoreRepair';
import { CurrencyRepositoryService } from '../repositories/currency-repository.service';
import { ICurrency } from '../../models/resources/ICurrency';
import { UserRepositoryService } from '../repositories/user-repository.service';
import { IUser, UserKey } from '../../models/resources/IUser';
import { SettingsService } from '../settings.service';
import { convertToMap, getDateTimeFormat } from 'src/app/shared/helpers/general';
import { getFullName } from '../../helpers/general';
import { IVoyageCharterName, IVoyageCharterNameFull } from '../../models/resources/IVoyageCharterName';

type IEntitesList<T> = [
  T,
  IVoyage[],
  IVesselAccounting[],
  IDebitorCreditor[],
  IPort[],
  ICompany[],
  IVessel[],
  IStevedoreRepair[],
  ICurrency[],
  IUser[],
];

@Injectable({
  providedIn: 'root'
})
export class StevedoreDamageReportMainService {
  private dtFormat: string;
  constructor(
    private stevedoreReportRepo: StevedoreDamageReportRepositoryService,
    private voyagesRepo: VoyagesRepositoryService,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
    private debitorCreditorRepo: DebitorCreditorRepositoryService,
    private portRepo: PortRepositoryService,
    private companyRepo: CompanyRepositoryService,
    private vesselRepo: VesselRepositoryService,
    private stevedoreRepairRepo: StevedoreRepairRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private userRepo: UserRepositoryService,
    private settingsService: SettingsService,
  ) {
    this.dtFormat = getDateTimeFormat(this.settingsService.locale);
  }

  public fetchAll(params: RepoParams<IStevedoreDamageReport>) {
    return combineLatest([
      this.stevedoreReportRepo.fetchAll(params),
      this.voyagesRepo.fetchAll({}),
      this.vesselAccountingRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
      this.portRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.vesselRepo.fetchAll({}),
      this.stevedoreRepairRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.userRepo.fetchAll({}),
    ]).pipe(map((res: IEntitesList<IStevedoreDamageReport[]>) =>
      this.getStevedoreReportFullList(...res))
    );
  }

  public fetchOne(id: number, params: RepoParams<IStevedoreDamageReport>) {
    return this.composeStevedoreReport(
      this.stevedoreReportRepo.fetchOne(id, params),
      this.voyagesRepo.fetchAll({}),
      this.vesselAccountingRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
      this.portRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.vesselRepo.fetchAll({}),
      this.stevedoreRepairRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.userRepo.fetchAll({}),
    );
  }

  /**
   * This method is usually used when we calculate stevedore damamge report status, and
   * need to update it on field value change(stevedoreDamageReport isn't saved yet) so we
   * pass form value as IStevedoreDamageReport, to be able to go further and get
   * IStevedoreDamageReportRow.
   */
  public fetchFullStevedoreDamageReport(stevedoreDamageReport: IStevedoreDamageReport) {
    return this.composeStevedoreReport(
      of(stevedoreDamageReport),
      this.voyagesRepo.fetchAll({}),
      this.vesselAccountingRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
      this.portRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.vesselRepo.fetchAll({}),
      this.stevedoreRepairRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.userRepo.fetchAll({}),
    );
  }

  public getStream(
    params = {},
    filterByVesselId?: (reports: IStevedoreDamageReport) => boolean
  ): Observable<IStevedoreDamageReportFull[]> {
    return forkJoin([
      this.stevedoreReportRepo.fetchAll(params),
      this.voyagesRepo.fetchAll({}),
      this.vesselAccountingRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
      this.portRepo.fetchAll({}),
      this.companyRepo.fetchAll({}),
      this.vesselRepo.fetchAll({}),
      this.stevedoreRepairRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.userRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.stevedoreReportRepo.getStream().pipe(
          map(reports => {
            return filterByVesselId ? reports.filter(filterByVesselId) : reports;
          })
        ),
        this.voyagesRepo.getStream({}),
        this.vesselAccountingRepo.getStream({}),
        this.debitorCreditorRepo.getStream({}),
        this.portRepo.getStream({}),
        this.companyRepo.getStream({}),
        this.vesselRepo.getStream({}),
        this.stevedoreRepairRepo.getStream({}),
        this.currencyRepo.getStream({}),
        this.userRepo.fetchAll({}),
      ])),
      map((res: IEntitesList<IStevedoreDamageReport[]>) => this.getStevedoreReportFullList(...res)),
    );
  }

  private getStevedoreReportFull(
    stevedoreReport: IStevedoreDamageReport,
    voyages: IVoyage[],
    vesselAccountings: IVesselAccounting[],
    debitorCreditors: IDebitorCreditor[],
    ports: IPort[],
    companies: ICompany[],
    vessels: IVessel[],
    stevedoreRepairs: IStevedoreRepair[],
    currencies: ICurrency[],
    users: IUser[],
  ): IStevedoreDamageReportFull {
    const voyage = voyages.find(v => v.id === stevedoreReport.voyageId);
    let vesselAccounting = vesselAccountings.find(v => v.id === stevedoreReport.vesselAccountingId);
    if (!vesselAccounting) {
      vesselAccounting = vesselAccountings.find(v => v.vesselId === stevedoreReport.vesselId);
    }
    const debitorCreditor = debitorCreditors.find(dc => dc.id === stevedoreReport.chartererId);
    const port = ports.find(p => p.id === stevedoreReport.portId);
    const company = companies.find(c => c.id === debitorCreditor.companyId);
    const vessel = vessels.find(v => v.id === stevedoreReport.vesselId);
    const stevedoreRepair = stevedoreRepairs.find(s => s.stevedoreDamageReportId === stevedoreReport.id);
    const stevedoreRepairFull = stevedoreRepair
      ? {
        ...stevedoreRepair,
        currency: currencies.find(c => c.id === stevedoreRepair.currencyId)
      } as IStevedoreRepairFull
      : undefined;

    const usersMap: Map<UserKey, IUser> = convertToMap(users, 'id');

    const invoiceApprovedByUser: IUser = usersMap.get(stevedoreReport.invoiceApprovedBy);
    const invoiceWaivedByUser: IUser = usersMap.get(stevedoreReport.invoiceWaivedBy);
    const invoiceCreatedByUser: IUser = usersMap.get(stevedoreReport.invoiceCreatedBy);
    const createdByUser: IUser = usersMap.get(stevedoreReport.createdBy);

    return {
      ...stevedoreReport,
      voyage,
      vesselAccounting,
      debitorCreditor,
      port,
      company,
      vessel,
      stevedoreRepair: stevedoreRepairFull,
      createdByUser,
      invoiceApprovedByUser,
      invoiceWaivedByUser,
      invoiceCreatedByUser,
    };
  }

  private getStevedoreReportFullList(
    stevedoreReports: IStevedoreDamageReport[],
    voyages: IVoyage[],
    vesselAccountings: IVesselAccounting[],
    debitorCreditors: IDebitorCreditor[],
    ports: IPort[],
    companies: ICompany[],
    vessels: IVessel[],
    stevedoreRepairs: IStevedoreRepair[],
    currencies: ICurrency[],
    users: IUser[],
  ): IStevedoreDamageReportFull[] {
    return stevedoreReports.map(stevedoreReport => this.getStevedoreReportFull(
      stevedoreReport,
      voyages,
      vesselAccountings,
      debitorCreditors,
      ports,
      companies,
      vessels,
      stevedoreRepairs,
      currencies,
      users,
    ));
  }

  private composeStevedoreReport(
    stevedoreReport$: Observable<IStevedoreDamageReport>,
    voyages$: Observable<IVoyage[]>,
    vesselAccountings$: Observable<IVesselAccounting[]>,
    debitorCreditors$: Observable<IDebitorCreditor[]>,
    ports$: Observable<IPort[]>,
    companies$: Observable<ICompany[]>,
    vessels$: Observable<IVessel[]>,
    stevedoreRepairs$: Observable<IStevedoreRepair[]>,
    currencies$: Observable<ICurrency[]>,
    users$: Observable<IUser[]>,
  ): Observable<IStevedoreDamageReportFull> {

    return forkJoin([
      stevedoreReport$,
      voyages$,
      vesselAccountings$,
      debitorCreditors$,
      ports$,
      companies$,
      vessels$,
      stevedoreRepairs$,
      currencies$,
      users$,
    ] as any).pipe(
      map((res: IEntitesList<IStevedoreDamageReport>) => this.getStevedoreReportFull(...res))
    );
  }

  public toRow(stevedoreReport: IStevedoreDamageReportFull): IStevedoreDamageReportRow {
    const {
      vesselAccounting,
      voyage,
      debitorCreditor,
      company,
      stevedoreRepair,
      port,
    } = stevedoreReport;
    const createdByUserFullName: string = getFullName(stevedoreReport.createdByUser);
    const invoiceApprovedByUserFullName: string = getFullName(stevedoreReport.invoiceApprovedByUser);
    const invoiceWaivedByUserFullName: string = getFullName(stevedoreReport.invoiceWaivedByUser);
    const invoiceCreatedByUserFullName: string = getFullName(stevedoreReport.invoiceCreatedByUser);

    return ({
      ...stevedoreReport,
      reportNo: stevedoreReport.reportType === StevedoreDamageReportType.CrewWork
        ? stevedoreReport.crewReportNo
        : stevedoreReport.sdReportNo,
      reportDate: stevedoreReport.reportType === StevedoreDamageReportType.CrewWork
        ? stevedoreReport.crewReportDate
        : stevedoreReport.sdReportDate,
      vesselName: vesselAccounting && vesselAccounting.vesselName,
      vesselNo: vesselAccounting && vesselAccounting.vesselNo,
      charterer: debitorCreditor && company
        ? `${debitorCreditor.no} ${company.companyName}`
        : '',
      voyageNo: voyage && `${voyage.no}`.padStart(3, '0'),
      voyageEnd: voyage && voyage.until,
      reportTypeLabel: reportTypeOptions
        .find(reportType => reportType.value === stevedoreReport.reportType).displayLabel,
      incidentLabel: incidentOptions
        .find(incident => incident.value === stevedoreReport.incident).displayLabel,
      positionSBPSLabel: stevedoreReport.positionSBPS
        ? positionSBPSOptions
          .find(positionSBPS => positionSBPS.value === stevedoreReport.positionSBPS).displayLabel
        : '',
      positionFWDAFTLabel: stevedoreReport.positionFWDAFT
        ? positionFWDAFTOptions
          .find(positionFWDAFT => positionFWDAFT.value === stevedoreReport.positionFWDAFT).displayLabel
        : '',
      statusLabel: this.getStatus(stevedoreReport),
      repairByLabel: stevedoreRepair && stevedoreRepair.reparationDoneBy
        ? repairByOptions
          .find(repairBy => repairBy.value === stevedoreReport.stevedoreRepair.reparationDoneBy)
          .displayLabel
        : undefined,
      expenses: stevedoreRepair ? stevedoreRepair.amount || 0 : 0,
      iso: stevedoreRepair && stevedoreRepair.currency
        ? stevedoreRepair.currency.isoCode
        : 'USD',
      portName: port?.name,
      createdByUserFullName,
      invoiceApprovedByUserFullName,
      invoiceWaivedByUserFullName,
      invoiceCreatedByUserFullName,
    });
  }

  private getStatus(stevedoreReport: IStevedoreDamageReportFull): IWording {
    switch (stevedoreReport.status) {
      case StevedoreDamageReportStatus.RatingRequired:
        return wording.technicalManagement.ratingRequired;

      case StevedoreDamageReportStatus.ReleaseRequired:
        return wording.technicalManagement.approvalRequired;

      case StevedoreDamageReportStatus.InProgress:
        return wording.technicalManagement.invoiceRequired;

      case StevedoreDamageReportStatus.InvoiceCreated:
        return wording.technicalManagement.administratorTabInvoiceCreated;

      case StevedoreDamageReportStatus.Cleared:
        return wording.technicalManagement.invoiceCleared;
    }
  }

  public toRows = (stevedoreReports: IStevedoreDamageReportFull[]): IStevedoreDamageReportRow[] => {
    return stevedoreReports.map((stevedoreReport) => this.toRow(stevedoreReport));
  }

  public toCharternameFull(charternames: IVoyageCharterName[]): IVoyageCharterNameFull[] {
    return charternames.map((c, index, array) => {
      return {
        ...c,
        until: array[index + 1]?.from,
      };
    });
  }
}
