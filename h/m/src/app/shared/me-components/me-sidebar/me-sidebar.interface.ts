import { MeButtonI } from '../me-button/me-button.interface';

export interface MeSidebarNavItemI {
  id: string;
  title: string;
  icon: string;
  activated: boolean;
  children?: MeSidebarNavItemI[];
  disabled?: boolean;
}

export interface MeSidebarI {
  logoUrl: string;
  isExpanded: boolean;
  isCollapsible: boolean;
  company: {
    logoUrl: string;
    title: string;
    editButton: MeButtonI | null;
    editButtonTitle: string;
  };
  navItems: MeSidebarNavItemI[];
}
