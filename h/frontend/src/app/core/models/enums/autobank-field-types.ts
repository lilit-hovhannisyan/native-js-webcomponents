export enum AutobankFieldTypes {
  GVC = 1,
  SenderName = 2,
  SenderSortCode = 3,
  SenderAccount = 4,
  SenderBIC = 5,
  SenderIBAN = 6,
  Reference = 7,
  Amount = 8,
  BookingText = 9,
}
