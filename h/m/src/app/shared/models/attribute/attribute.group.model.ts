import { Type } from 'class-transformer';
import { SingleAttributeGroupModel } from './single-attribute.group.model';

export class AttributeGroupModel {
  @Type(() => SingleAttributeGroupModel)
  attributes: SingleAttributeGroupModel[] = [];
  displayName: string | undefined = '';
  internalName: string | undefined = '';
  placement: number | undefined = 0;
}
