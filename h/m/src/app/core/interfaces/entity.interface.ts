import { KeyValueI } from './key-value.interface';

export interface EntityI {
  domainName: string;
  attributes: KeyValueI<string | number | boolean | string[]>;
}
