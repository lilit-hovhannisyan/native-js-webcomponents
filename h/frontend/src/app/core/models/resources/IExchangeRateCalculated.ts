import { IResource } from './IResource';
import { IWithLabel } from './IWithLabel';

export type ExchangeRateCalculatedKey = number;

interface ISharedProps {
  currencyId: number;
  baseCurrencyId: number;
  rate: number;
  date: string;
}

export interface IExchangeRateCalculatedBase extends ISharedProps, IResource<ExchangeRateCalculatedKey> { }

export interface IExchangeRateCalculatedCreate extends ISharedProps { }

export interface IExchangeRateCalculatedDTO extends IExchangeRateCalculatedBase { }

export interface IExchangeRateCalculated extends IExchangeRateCalculatedDTO, IWithLabel { }

export interface IExchangeRateCalculatedFull extends IExchangeRateCalculated { }
