import { Component, OnInit, Host, ViewChild, ElementRef, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NvGridConfig } from 'nv-grid';
import { forkJoin, Observable, ReplaySubject } from 'rxjs';
import { tap, map, distinctUntilChanged, skip, takeUntil, filter } from 'rxjs/operators';
import { ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';
import { AutobankPermissionKey, IAutobankPermission } from 'src/app/core/models/resources/IAutobankPermission';
import { IMandatorLookup } from 'src/app/core/models/resources/IMandator';
import { IUser } from 'src/app/core/models/resources/IUser';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { AutobankPermissionRepositoryService } from 'src/app/core/services/repositories/autobank-permission-repository.service';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { Language } from 'src/app/core/models/language';
import { Validators } from 'src/app/core/classes/validators';
import { Operations } from 'src/app/core/models/Operations';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { url, SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { PermissionsGroupsHelperService } from 'src/app/core/services/helperService/permissions-groups-helper.service';
import { IAutobankMandatorUserConnection } from 'src/app/core/models/resources/IAutobankMandatorUserConnection';
import { IPermissionRadioSelectItem } from 'src/app/core/models/resources/IPermissionGroup';
import { MandatorMainService } from 'src/app/core/services/mainServices/mandator-main.service';

@Component({
  templateUrl: './autobank-permissions.component.html',
  styleUrls: ['./autobank-permissions.component.scss'],
  providers: [ PermissionsGroupsHelperService ]
})
export class AutobankPermissionsComponent implements OnInit, OnDestroy {
  private componentDestroyed$ = new ReplaySubject<boolean>(1);

  public sitemap = sitemap;
  public ACL_ACTIONS = ACL_ACTIONS;
  public buttonsCount = 1;
  public wording = this.baseComponentService.wordingService.wording;
  public language: Language = this.baseComponentService.settingsService.language;
  public user: IUser = this.baseComponentService.authService.getSessionInfo().user;

  public permissionGroups: IPermissionRadioSelectItem<IAutobankMandatorUserConnection>[] = [];
  public mandators: IMandatorLookup[] = [];
  public allUsers: IUser[] = [];
  public selectedGroup: IPermissionRadioSelectItem<IAutobankMandatorUserConnection>;
  public formElement: ElementRef;

  @ViewChild('name', { static: false, read: ElementRef})
  public nameInputEl: ElementRef;
  public form: FormGroup;
  public mandatorsGridConfig: NvGridConfig;
  public usersGridConfig: NvGridConfig;
  public activeSitemapEntry: SitemapEntry;
  public permissionsSitemapNode: SitemapNode = sitemap.accounting.children.autobank.children.autobankPermissions;

  private get isNameEditable(): boolean {
    return this.helperService.createMode || this.user.isSuperAdmin || !this.selectedGroup?.isSystemDefault;
  }
  private get paramId(): string {
    return this.activatedRoute.children[0]?.snapshot.params.id;
  }

  constructor(
    @Host() public helperService: PermissionsGroupsHelperService<
      IAutobankMandatorUserConnection, IAutobankPermission
    >,
    private baseComponentService: BaseComponentService,
    private autobankPermissionsRepo: AutobankPermissionRepositoryService,
    private mandatorMainService: MandatorMainService,
    private userRepo: UserRepositoryService,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private el: ElementRef,
    private cdr: ChangeDetectorRef
  ) { }

  public ngOnInit() {
    forkJoin([
      this.fetchGroups(),
      this.fetchAutobankWorkflowMandators(),
      this.fetchAllUsers(),
    ]).subscribe(() => {
      if (this.paramId === 'new') {
        this.enterCreationMode();
      } else {
        this.initForm(this.selectedGroup);
        this.setSelectedGroupIdParams();
      }
    });

    this.activeSitemapEntry = SitemapEntry.getByUrl(this.router.url);
    this.setPaperViewButtonsCount();

    this.router.events.pipe(
      takeUntil(this.componentDestroyed$),
      filter(routerEvent => routerEvent instanceof NavigationEnd),
    )
    .subscribe(() => {
      const basicPath = this.permissionsSitemapNode.children.id.children.basic.path;
      this.activeSitemapEntry = SitemapEntry.getByUrl(this.router.url);

      if (this.activeSitemapEntry.key === basicPath && !this.helperService.editMode) {
        this.setPaperViewButtonsCount();
      } else {
        this.buttonsCount = 2;
      }
    });
  }

  private initForm(group?: IAutobankPermission): void {
    const allGroupNames = this.permissionGroups
      .filter(g => g.id !== group?.id)
      .map(g => g.name);
    const equalityChecker = (controlValue: string, itemValue: string) => {
      return controlValue?.toLowerCase() === itemValue?.toLowerCase();
    };

    this.form = this.fb.group({ name: [
      {value: group?.name, disabled: !this.isNameEditable },
      [ Validators.required, Validators.unique(allGroupNames, equalityChecker) ]
    ] });
    this.listenNameChange();
    this.helperService.selectedGroup$.next(group);
  }

  private listenNameChange(): void {
    this.form.controls.name.valueChanges.pipe(
      distinctUntilChanged(),
      skip(1),
    ).subscribe(() => this.helperService.hasChanges$.next(true));
  }

  private setPaperViewButtonsCount(): void {
    // the back button
    this.buttonsCount = 1;
    const authService = this.baseComponentService.authService;

    if (authService.canPerformOperationOnCurrentUrl(Operations.CREATE)) {
      this.buttonsCount++;
    }
    if (authService.canPerformOperationOnCurrentUrl(Operations.UPDATE)) {
      this.buttonsCount++;
    }
    if (authService.canPerformOperationOnCurrentUrl(Operations.DELETE)) {
      this.buttonsCount++;
    }
  }

  // fetch resources
  private fetchGroups(): Observable<IPermissionRadioSelectItem<IAutobankMandatorUserConnection>[]> {
    return this.autobankPermissionsRepo.fetchAll({}).pipe(
      map(permissions => {
        return this.helperService.map2RadioSelectItems(permissions);
      }),
      tap(groups => {
        this.permissionGroups = groups;
        this.selectedGroup = this.permissionGroups.find(item => {
          return item.id === +this.paramId;
        }) || groups[0];
      })
    );
  }

  private fetchAutobankWorkflowMandators(): Observable<IMandatorLookup[]> {
    return this.mandatorMainService.fetchLookupsWithRolesOnly().pipe(
      map(allMandators => allMandators.filter(m => m.isAutoBankWorkflow)),
      tap(mandators => this.helperService.mandators$.next(mandators)),
    );
  }

  private fetchAllUsers(): Observable<IUser[]> {
    return this.userRepo.fetchAll({}).pipe(
      tap(users => this.helperService.users$.next(users)),
    );
  }

  public delete(): void {
    if (!this.selectedGroup) {
      return;
    }

    this.baseComponentService.confirmationService.confirm({
      okButtonWording: this.wording.general.delete,
      cancelButtonWording: this.wording.general.cancel,
      wording: setTranslationSubjects(
        this.wording.accounting.areYourSureToDeletePermissionGroup,
        { subject: this.selectedGroup.name },
      ),
    }).subscribe(confirmed => {
      if (!confirmed) {
        return;
      }

      this.autobankPermissionsRepo.delete(this.selectedGroup.id, {}).subscribe(() => {
        this.permissionGroups = this.permissionGroups.filter(group => {
          return group.id !== this.selectedGroup.id;
        });
        this.selectedGroup = this.permissionGroups[0];
        this.initForm(this.selectedGroup);
      });
    });
  }

  public submit(): void {
    if (this.form.invalid) {
      return;
    }
    const relatedUsers = this.helperService.selectedUsers$.getValue();
    const relatedMandators = this.helperService.selectedMandators$.getValue();

    const usersLength = relatedUsers.length;
    const mandatorsLength = relatedMandators.length;

    if (!usersLength && mandatorsLength || usersLength && !mandatorsLength) {
      this.baseComponentService.confirmationService.warning({
        title: this.wording.general.warning,
        cancelButtonWording: null,
        okButtonWording: this.wording.general.ok,
        wording: this.wording.accounting.permissionGroupRequirementsMessage,
      });
      return;
    }

    const group = this.helperService.createMode ? {} as IAutobankPermission : this.selectedGroup;
    const freshResource: IAutobankPermission = {
      ...group,
      ...this.form.value,
      connections: this.helperService.entities2connections(
        relatedUsers,
        relatedMandators,
        group,
        'autobankPermissionId',
      ),
    };

    if (this.helperService.createMode) {
      this.autobankPermissionsRepo.create(freshResource, {}).subscribe((entity) => {
        this.selectedGroup = this.helperService.permission2radioSelectItem(entity);
        // emit change to refresh radioSelect
        this.permissionGroups = [...this.permissionGroups, this.selectedGroup];
        this.initForm(this.selectedGroup);
        this.leaveEditMode();
      });
    } else {
      this.autobankPermissionsRepo.update(freshResource, {}).subscribe((entity) => {
        const index = this.permissionGroups.findIndex(p => p.id === entity.id);
        this.selectedGroup = this.helperService.permission2radioSelectItem(entity);
        if (index !== -1) {
          this.permissionGroups.splice(index, 1, this.selectedGroup);
          // emit change to refresh radioSelect
          this.permissionGroups = [...this.permissionGroups];
        }
        this.initForm(this.selectedGroup);
        this.leaveEditMode();
      });
    }
  }

  private leaveEditMode(): void {
    this.helperService.inCreateMode$.next(false);
    this.helperService.inEditMode$.next(false);
    this.form.disable();
    this.helperService.hasChanges$.next(false);
    this.setPaperViewButtonsCount();

    this.router.navigate([this.selectedGroup.id], { relativeTo: this.activatedRoute });
  }

  public enterCreationMode(): void {
    this.helperService.inCreateMode$.next(true);
    this.initForm();
    this.enterEditMode();
  }

  public enterEditMode(): void {
    // only save and cancel buttons
    this.buttonsCount = 2;

    if (this.isNameEditable) {
      // needs to wait until the input will be appeared
      setTimeout(() => {
        this.form.enable();
        this.focusName();
      });
    }
    this.helperService.inEditMode$.next(true);

    // To trigger tabDisabler check in nv-router-tabs
    if (this.helperService.createMode) {
      this.router.navigate(['new'], { relativeTo: this.activatedRoute });
    } else {
      this.router.navigate([this.selectedGroup.id], { relativeTo: this.activatedRoute });
    }
  }

  private focusName(): void {
    const elem = this.nameInputEl.nativeElement;
    elem.focus();
    const length = elem.value.length;
    elem.setSelectionRange(length, length);
  }

  public cancelEditing(): void {
    this.leaveEditMode();
    this.initForm(this.selectedGroup || this.permissionGroups[0]);
  }

  public customValidation = (): boolean => !this.helperService.hasChanges$.getValue();

  public permissionGroupChange(selectedGroupId: AutobankPermissionKey): void {
    this.selectedGroup = this.permissionGroups.find(item => {
      return item.id === selectedGroupId;
    });
    this.initForm(this.selectedGroup);
  }

  public tabDisabler = (node: SitemapNode) => {
    const basicNode = sitemap.accounting.children.autobank.children.autobankPermissions.children.id.children.basic;
    // in creationMode just enable the basicTab
    if (this.permissionGroups.length) {
      return node === basicNode || !this.helperService.editMode;
    }
    return this.helperService.createMode && node === basicNode;
  }

  private setSelectedGroupIdParams(): void {
    if (!+this.paramId || this.paramId === 'new') {
      this.router.navigate([this.selectedGroup.id], { relativeTo: this.activatedRoute });
    }
  }

  public routeBack() {
    const parentUrl = url(sitemap.accounting.children.autobank);
    this.router.navigate([parentUrl], { relativeTo: this.activatedRoute });
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
