import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, OnInit, Output } from '@angular/core';
import { MeIntroductionMessageI } from './me-introduction-message.interface';

@Component({
  selector: 'me-introduction-message',
  templateUrl: './me-introduction-message.component.html',
  styleUrls: ['./me-introduction-message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeIntroductionMessageComponent implements OnInit {
  @Input() dataModel!: MeIntroductionMessageI;
  @Output() btnEvent: EventEmitter<void> = new EventEmitter();
  @HostBinding('class') class = 'i-msg-main';

  constructor() {}

  ngOnInit(): void {}

  onBtnClick(): void {
    this.btnEvent.emit();
  }
}
