import { Injectable } from '@angular/core';
import { VesselAccountingRepositoryService } from '../../../core/services/repositories/vessel-accounting-repository.service';
import { RegistrationVesselTasksRepositoryService } from '../../../core/services/repositories/registration-vessel-tasks-repository.service';
import { combineLatest, forkJoin, Observable, of } from 'rxjs';
import { EventInput } from '@fullcalendar/angular';
import { IRegistrationVesselTask } from '../../../core/models/resources/IRegistrationVesselTask';
import { IVesselAccounting } from '../../../core/models/resources/IVesselAccounting';
import { TaskType } from '../../../core/models/enums/vessel-registration-tasks';
import { IVessel} from '../../../core/models/resources/IVessel';
import { CalendarEventModalComponent } from './calendar-event-modal/calendar-event-modal.component';
import { DefaultModalOptions } from '../../../core/constants/modalOptions';
import { getVesselTaskModalConfig } from '../vessel-task-components/vessel-task/vessel-task.component.config';
import { IWording } from '../../../core/models/resources/IWording';
import { wording } from '../../../core/constants/wording/wording';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { ICalendar } from '../../../core/models/resources/ICalendar';
import { Calendar } from '@fullcalendar/core';
import { Language } from '../../../core/models/language';
import { CalendarEnvironment, CalendarEventsTypes, formatWithHours } from './calendar.helper';
import { CalendarEventsRepositoryService } from '../../../core/services/repositories/calendar-events-repository.service';
import { convertToMap, setTranslationSubjects } from '../../helpers/general';
import { NotificationType } from '../../../core/models/INotification';
import { ICalendarEvent } from '../../../core/models/resources/ICalendarEvent';
import { NotificationsService } from '../../../core/services/notification.service';
import { IVesselTaskFull } from 'src/app/core/models/resources/IVesselTask';
import { IVesselRegister, VesselRegisterKey } from 'src/app/core/models/resources/IVesselRegister';
import { VesselRegisterRepositoryService } from 'src/app/core/services/repositories/vessel-registeries-repository.service';
import { CountryRepositoryService } from 'src/app/core/services/repositories/country-repository.service';
import { CountryKey, ICountry } from 'src/app/core/models/resources/ICountry';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  private modalRef: NzModalRef;
  private language: Language;
  private allCalendars: ICalendar[];
  private fetchEvents: () => void;

  constructor(
    private modalService: NzModalService,
    private tasksRepo: RegistrationVesselTasksRepositoryService,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
    private vesselRegistersRepo: VesselRegisterRepositoryService,
    private countryRepo: CountryRepositoryService,
    private calendarEventsRepo: CalendarEventsRepositoryService,
    private notificationsService: NotificationsService,
  ) {
  }

  public getCalendarData(env: CalendarEnvironment): Observable<EventInput[]>  {
    switch (env) {
      case CalendarEnvironment.chartering:
        return this.getChartering();

      default:
        return this.getChartering();
    }
  }

  public initCalendarService(
    env: CalendarEnvironment,
    calendarApi: Calendar,
    language: Language,
    allCalendars: ICalendar[],
    fetchEvents: () => void
  ): void {
    this.language = language;
    this.allCalendars = allCalendars;
    this.fetchEvents = fetchEvents;

    switch (env) {
      case CalendarEnvironment.chartering:
        this.initChartering(calendarApi);
        break;
      default:
        this.initChartering(calendarApi);
    }
  }

  // -- CHARTERING
  private getChartering(): Observable<EventInput[]> {
    return forkJoin([
      this.tasksRepo.fetchAll({}),
      this.vesselAccountingRepo.fetchAll({}),
      this.countryRepo.fetchAll({}),
      this.vesselRegistersRepo.fetchAll({})
    ]);
  }

  private initChartering(calendarApi: Calendar): void {
    type RegTask = IVesselTaskFull<IRegistrationVesselTask>;

    const allTasks: RegTask[] = this.tasksRepo.getStreamValue();
    const registersMap: Map<VesselRegisterKey, IVesselRegister>
      = convertToMap(this.vesselRegistersRepo.getStreamValue());
    const countriesMap: Map<CountryKey, ICountry>
      = convertToMap(this.countryRepo.getStreamValue());

    // Tasks
    allTasks.forEach((task: RegTask) => {
      const vessel: IVesselAccounting = this.vesselAccountingRepo.getStreamValue()
        .find(va => va.vesselId === task.vesselId);

      const register: IVesselRegister = registersMap.get(task.vesselRegistrationId);
      const countryId: CountryKey = register?.secondaryCountryId || register?.primaryCountryId;
      const country: ICountry = countriesMap.get(countryId);
      const nameCountrySection: string = country?.nationality
        ? `(${country?.nationality})`
        : '';

      calendarApi.addEvent({
        id: `${task.id} ${task._type}`,
        classNames: `nv-task ${task.id} ${task._type}`,
        title: `${vessel.vesselName} ${nameCountrySection}`,
        start: task.useStartDate ? task.start : task.end,
        borderColor: task.color,
        textColor: task.color,
        startEditable: true,
        editable: true,
        display: 'list-item', // types: 'auto', 'block', 'list-item', 'background', 'inverse-background', or 'none'
        extendedProps: { // keep task data for update evn on drag&drop
          ...task
        }
      });
    });
  }

  private showCalendarEventModal(startDate?: string, data?: EventInput, isDisabled?: boolean): void {
    const eventTitle: IWording = data?.event.extendedProps.displayLabel;
    const title: string = isDisabled
      ? `${wording.chartering.event[this.language]}: ${eventTitle[this.language]}`
      : data
        ? `${wording.chartering.editEvent[this.language]}: ${eventTitle[this.language]}`
        : wording.chartering.addNewEvent[this.language];

    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: title,
      nzContent: CalendarEventModalComponent,
      nzClosable: true,
      nzWidth: 600,
      nzBodyStyle: {overflow: 'hidden', padding: '0'},
      nzComponentParams: {
        data: data,
        startDate: startDate,
        isDisabled,
        calendars: this.allCalendars,
        closeModalAndUpdateData: () => {
          this.closeModal();
          this.fetchEvents();
        },
      },
    });
  }

  public showTaskModal(startDate?: string, data?: EventInput, isDisabled = false): void {
    const vesselData: IVessel = data?.event.extendedProps || {};

    this.modalService.create({
      ...getVesselTaskModalConfig<IRegistrationVesselTask>({
        value: {
          start: startDate,
          ...vesselData,
        },
        type: TaskType.RegistrationVesselTask,
        isDisabled,
      }),
      nzOnOk: () => this.fetchEvents(),
    });
  }

  public showTodoModal(): void {
    alert('todo');
  }

  private closeModal = (): void => {
    this.modalRef.destroy();
  }

  public updateOnEventDrop(extendedProps: any, selectedDate: string): void { // TODO: type extendedProps
    if (extendedProps._type === CalendarEventsTypes.task) {
      const payload: IVesselTaskFull<IRegistrationVesselTask> = extendedProps.useStartDate
        ? {...extendedProps, start: selectedDate}
        : {...extendedProps, end: selectedDate};


      if (payload.start > payload.end) {
        this.notificationsService.notify(
          NotificationType.Error,
          wording.general.sent,
          setTranslationSubjects(
            wording.chartering.taskCantBeModifiedDueToDateOrder,
            {
              STARTDATE: formatWithHours(payload.start),
              ENDDATE: formatWithHours(payload.end)
            },
          ));
        this.fetchEvents();
        return;
      }

      this.tasksRepo.update(payload, {}).subscribe(() => {
        this.fetchEvents();
      });
    }

    if (extendedProps._type === CalendarEventsTypes.evn) {
      const payload: ICalendarEvent = {
        ...extendedProps,
        start: selectedDate
      };
      this.calendarEventsRepo.update(payload, {}).subscribe(() => {
        this.fetchEvents();
      });
    }
  }

  public deleteCalendarEvent(id: number, type: CalendarEventsTypes): void {
    const deleteReq$ = type === CalendarEventsTypes.task
      ? this.tasksRepo.delete(id, {})
      : type === CalendarEventsTypes.evn
        ? this.calendarEventsRepo.delete(id, {})
        : of(null); // TODO: for todo haha

    deleteReq$.subscribe(() => this.fetchEvents());
  }
}
