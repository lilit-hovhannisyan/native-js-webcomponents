import { MandatorBookingCodeKey } from './../../models/resources/IMandatorBookingCode';
import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IMandatorBookingCode, IMandatorBookingCodeDTO, IMandatorBookingCodeCreate } from '../../models/resources/IMandatorBookingCode';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class MandatorBookingCodeRepositoryService extends AbstractRepository<
  IMandatorBookingCode,
  IMandatorBookingCodeDTO,
  IMandatorBookingCodeCreate,
  RepoParams<IMandatorBookingCode>,
  MandatorBookingCodeKey
> {
  protected resourceType = 'MandatorBookingCode';
  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/mandator-booking-codes';
  }

  protected postProcess(item: IMandatorBookingCodeDTO): IMandatorBookingCode { return item; }
}
