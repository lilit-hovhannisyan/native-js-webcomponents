import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { AccountBalanceKey, IAccountBalance, IAccountBalanceCreate, IAccountBalanceDTO } from '../../models/resources/IAccountBalance';


@Injectable({
  providedIn: 'root'
})
export class AccountBalanceRepositoryService extends AbstractRepository<
IAccountBalance,
IAccountBalanceDTO,
IAccountBalanceCreate,
RepoParams<IAccountBalance>,
AccountBalanceKey
> {
  protected resourceType = 'AccountBalance';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(params: RepoParams<IAccountBalance>): string {
    return '/api/accountbalances';
  }

  public postProcess(item: IAccountBalanceDTO): IAccountBalance {
    return item;
  }

}
