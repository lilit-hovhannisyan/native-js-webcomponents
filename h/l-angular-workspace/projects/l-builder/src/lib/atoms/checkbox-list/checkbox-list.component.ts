import {Component, EventEmitter, Input, Output} from '@angular/core';
import {CheckboxOption} from '../checkbox/checkbox.component';
import {Option} from '../atoms.interfaces';
import {optionList} from '../atoms.testData';

@Component({
  selector: 'lavash-checkbox-list',
  templateUrl: './checkbox-list.component.html',
  styleUrls: ['./checkbox-list.component.less']
})

export class CheckboxListComponent {
  @Input() options: Option[] = optionList;

  @Output() selectOption = new EventEmitter<CheckboxOption[]>();

  onSelectOption(o) {
    this.selectOption.emit(this.options);
  }
}
