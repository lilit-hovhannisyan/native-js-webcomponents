import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, Input, ElementRef, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { NvGridConfig } from 'nv-grid';
import { forkJoin, combineLatest, Observable, ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Validators } from 'src/app/core/classes/validators';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { SitemapEntry, url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { ICompany } from 'src/app/core/models/resources/ICompany';
import { ICountry } from 'src/app/core/models/resources/ICountry';
import { IPortFull, PortKey, IPort } from 'src/app/core/models/resources/IPort';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { IVesselRegister, RegistrationTypes, VesselRegisterKey } from 'src/app/core/models/resources/IVesselRegister';
import { PortMainService } from 'src/app/core/services/mainServices/port-main.service';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { CountryRepositoryService } from 'src/app/core/services/repositories/country-repository.service';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { getGridConfig as getCompanyGridConfig } from 'src/app/core/constants/nv-grid-configs/companies.config';
import { getGridConfig as getPortsGridConfig } from 'src/app/core/constants/nv-grid-configs/ports.config';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { VesselRegisterRepositoryService } from 'src/app/core/services/repositories/vessel-registeries-repository.service';
import { VesselsHelperService } from 'src/app/features/chartering/components/misc/vessels/detail-view/vessels.helper.service';
import { findClosestWithFromUntil } from 'src/app/core/helpers/general';

const primaryFields: string[] = [
  'primaryPortId',
  'primaryCallSign',
  'primaryCompanyId',
  'secondaryCountryId',
];

const secondaryFields: string[] = [
  'secondaryPortId',
  'secondaryCallSign',
  'secondaryCompanyId',
];

const sharedFields: string[] = [
  'ssrNo',
  'registerCourt',
];

/** Values are related to pages in which the component is used. */
declare type ContextType = 'vessel-registration' | 'registration-fleet';

@Component({
  selector: 'nv-vessel-interval-register',
  templateUrl: './vessel-interval-register.component.html',
  styleUrls: ['./vessel-interval-register.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VesselIntervalRegisterComponent implements OnInit, OnChanges, OnDestroy {
  @Input() public node: SitemapNode;
  @Input() public vesselId: VesselKey;
  /** Page-related context value. */
  @Input() public context: ContextType = 'vessel-registration';

  @Output() private selectRegister: EventEmitter<IVesselRegister> = new EventEmitter();
  /** Emits on edit-mode change. */
  @Output() private editModeChange: EventEmitter<boolean> = new EventEmitter();

  @Input() public selectedRegister: IVesselRegister;
  public zone: string;
  public wording = wording;
  public form: FormGroup;
  public inEditMode = false;
  public inCreationMode: boolean;
  public registers: IVesselRegister[] = [];
  public currentCountryISO: string;
  public ports: IPortFull[] = [];
  public portsGridConfig: NvGridConfig = getPortsGridConfig({}, () => false);
  public portsMap = new Map<PortKey, IPortFull>();
  public companies: ICompany[] = [];
  public companyGridConfig: NvGridConfig = getCompanyGridConfig(this.router, () => false);

  private countries: ICountry[] = [];
  public RegistrationTypes = RegistrationTypes;
  public primaryCountryFilter: { checkSecondaryCountry: (country: ICountry) => boolean };
  public secondaryCountryFilter: { checkPrimaryCountry: (country: ICountry) => boolean };
  public isPrimaryGermany: boolean;
  public isSecondaryGermany: boolean;
  private componentDestroyed$ = new ReplaySubject<boolean>(1);
  private registerPath = url(sitemap.chartering.children
    .registrations.children
    .registrationsFleet.children
    .id.children.register
  );
  public formElement: ElementRef;

  public get showClientRemarkFieldset(): boolean {
    return this.context === 'registration-fleet';
  }

  constructor(
    private countryRepo: CountryRepositoryService,
    private portMainService: PortMainService,
    private router: Router,
    private companyRepo: CompanyRepositoryService,
    private vesselRegistriesRepo: VesselRegisterRepositoryService,
    private vesselsHelperService: VesselsHelperService,
    private fb: FormBuilder,
    public authService: AuthenticationService,
    private cdr: ChangeDetectorRef,
    private el: ElementRef,
  ) { }

  public ngOnInit(): void {
    this.zone = SitemapEntry.getByNode(this.node).toZone();
    this.initData();
  }

  public ngOnChanges({selectedRegister}: SimpleChanges): void {
    if (selectedRegister && this.selectedRegister && this.registers.length > 0) {
      this.selectVesselRegister(this.selectedRegister);
    }
  }

  private initData(registerIdToSelect: number = null): void {
    forkJoin([
      this.countryRepo.fetchAll({ useCache: true }),
      this.vesselRegistriesRepo.fetchAll({ queryParams: { vesselId: this.vesselId } }),
      this.portMainService.fetchAll(),
      this.companyRepo.fetchAll({}),
    ]).subscribe(([countries, vesselRegisters, ports, companies]) => {
      this.ports = ports.map(p => ({
        ...p,
        countryIsoAlpha3: `(${p.countryIsoAlpha3})`,
      }));
      this.ports.forEach(p => this.portsMap.set(p.id, p));
      this.countries = countries;
      this.registers = vesselRegisters;
      this.companies = companies;

      if (vesselRegisters.length) {
        const vesselRegister: IVesselRegister = registerIdToSelect
          ? vesselRegisters.find(r => r.id === registerIdToSelect)
          : this.selectedRegister || findClosestWithFromUntil(vesselRegisters);

        const startRegister: IVesselRegister = vesselRegister
          || vesselRegisters[vesselRegisters.length - 1];
        this.selectVesselRegister(startRegister);
      }
      this.cdr.markForCheck();
    });
  }

  private initForm(vesselRegister: IVesselRegister): void {
    this.form = this.fb.group({
      registerCourt: [vesselRegister.registerCourt, Validators.maxLength(30)],
      primaryPortId: [vesselRegister.primaryPortId],
      secondaryPortId: [vesselRegister.secondaryPortId],
      ssrNo: [vesselRegister.ssrNo],
      type: [vesselRegister.type || RegistrationTypes.Bareboat],
      primaryCountryId: [vesselRegister.primaryCountryId, Validators.required],
      secondaryCountryId: [vesselRegister.secondaryCountryId],
      primaryCallSign: [vesselRegister.primaryCallSign, Validators.maxLength(10)],
      secondaryCallSign: [vesselRegister.secondaryCallSign, Validators.maxLength(10)],
      primaryCompanyId: [vesselRegister.primaryCompanyId],
      secondaryCompanyId: [vesselRegister.secondaryCompanyId],
      from: [vesselRegister.from, Validators.required],
      until: [vesselRegister.until, Validators.isAfterFieldDate('from')],
      client: [vesselRegister.client, Validators.maxLength(500)],
      remarks: [vesselRegister.remarks],
      isTraining: [vesselRegister.isTraining],
    });

    this.initPrimaryCountryChangeListener();
    this.initSecondaryCountryChangeListener();
    this.updateUntilFieldOnFromChange();
    this.initPrimaryPortAndCountryChangeListener();
    this.initSecondaryPortAndCountryChangeListener();
    this.changePrimaryFieldsStatus();
    this.changeSecondaryFieldsStatus();
    this.form.disable();
    this.form.markAsPristine();
  }

  private initPrimaryCountryChangeListener(): void {
    const { primaryCountryId } = this.form.controls;

    primaryCountryId.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(value => {
        this.secondaryCountryFilter = {
          checkPrimaryCountry: (country: ICountry) => country.id !== value
        };
        this.changePrimaryFieldsStatus();
        this.cdr.markForCheck();
      });
  }

  private initSecondaryCountryChangeListener(): void {
    const { secondaryCountryId } = this.form.controls;

    secondaryCountryId.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(value => {
        this.primaryCountryFilter = {
          checkSecondaryCountry: (country: ICountry) => country.id !== value
        };
        this.changeSecondaryFieldsStatus();
      });
  }

  public changePrimaryFieldsStatus(): void {
    const { primaryCountryId, secondaryCountryId } = this.form.controls;

    // the form needs a little delay to set a new status correctly
    setTimeout(() => {
      this.isPrimaryGermany = this.countries
        .find(c => c.id === primaryCountryId.value)?.isoAlpha3 === 'DEU';
      this.isSecondaryGermany = this.countries
        .find(c => c.id === secondaryCountryId.value)?.isoAlpha3 === 'DEU';

      if (primaryCountryId.value && this.inEditMode) {
        primaryFields.forEach(field => this.form.get(field).enable());
        if (this.isPrimaryGermany) {
          this.enableSharedFields();
        }
      } else {
        primaryFields.forEach(field => this.form.get(field).disable());
        if (!primaryCountryId.value) {
          primaryFields.forEach(field => this.form.get(field).reset());
        }
      }
      if (!this.isSecondaryGermany && !this.isPrimaryGermany) {
        this.disableSharedFields();
      }
    });
  }

  public changeSecondaryFieldsStatus(): void {
    const { primaryCountryId, secondaryCountryId } = this.form.controls;

    // the form needs a little delay to set a new status correctly
    setTimeout(() => {
      this.isPrimaryGermany = this.countries
        .find(c => c.id === primaryCountryId.value)?.isoAlpha3 === 'DEU';
      this.isSecondaryGermany = this.countries
        .find(c => c.id === secondaryCountryId.value)?.isoAlpha3 === 'DEU';
      if (secondaryCountryId.value && this.inEditMode) {
        secondaryFields.forEach(field => this.form.get(field).enable());
        if (this.isSecondaryGermany) {
          this.enableSharedFields();
        }
      } else {
        secondaryFields.forEach(field => this.form.get(field).disable());
        if (!secondaryCountryId.value) {
          secondaryFields.forEach(field => this.form.get(field).reset());
        }
      }
      if (!this.isPrimaryGermany && !this.isSecondaryGermany) {
        this.disableSharedFields();
      }
    });
  }

  private disableSharedFields(): void {
    sharedFields.forEach(field => this.form.get(field).disable());
    sharedFields.forEach(field => this.form.get(field).reset());
  }

  private enableSharedFields(): void {
    sharedFields.forEach(field => this.form.get(field).enable());
  }

  private initPrimaryPortAndCountryChangeListener(): void {
    const { primaryCountryId, primaryPortId } = this.form.controls;

    combineLatest([
      primaryCountryId.valueChanges,
      primaryPortId.valueChanges
    ]).pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => this.validatePrimaryPort());
  }

  private initSecondaryPortAndCountryChangeListener(): void {
    const { secondaryCountryId, secondaryPortId } = this.form.controls;

    combineLatest([
      secondaryCountryId.valueChanges,
      secondaryPortId.valueChanges
    ]).pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => this.validateSecondaryPort());
  }

  private updateUntilFieldOnFromChange(): void {
    const { from, until } = this.form.controls;

    from.valueChanges
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => {
        until.updateValueAndValidity();
      });
  }

  private validatePrimaryPort(): void {
    const { primaryCountryId, primaryPortId } = this.form.controls;

    if (!primaryPortId.value || !primaryCountryId.value) {
      return null;
    }

    const port: IPort = this.portsMap.get(primaryPortId.value);

    if (primaryCountryId.value === port?.countryId) {
      return primaryPortId.setErrors(null);
    }

    markControlAsTouched(primaryPortId);
    primaryPortId.setErrors({
      invalidPort: {
        message: wording.chartering.portDoesntBelongToFlagstate
      }
    });
  }

  private validateSecondaryPort(): void {
    const { secondaryCountryId, secondaryPortId } = this.form.controls;

    if (!secondaryPortId.value || !secondaryCountryId.value) {
      return null;
    }

    const port: IPort = this.portsMap.get(secondaryPortId.value);

    if (secondaryCountryId.value === port.countryId) {
      return secondaryPortId.setErrors(null);
    }

    markControlAsTouched(secondaryPortId);
    secondaryPortId.setErrors({
      invalidPort: {
        message: wording.chartering.portDoesntBelongToFlagstate
      }
    });
  }

  public enterEditMode(): void {
    this.inEditMode = true;
    this.editModeChange.emit(this.inEditMode);
    // the form needs a little delay to set a new status correctly
    setTimeout(() => { this.form.enable(); });
  }

  public cancelEditing(): void {
    this.inEditMode = false;
    this.editModeChange.emit(this.inEditMode);
    this.inCreationMode = false;
    this.initForm(this.selectedRegister);
    // the form needs a little delay to set a new status correctly
    setTimeout(() => this.form.disable());
  }

  public saveClicked(): void {

    if (this.form.invalid) {
      return;
    }

    const value: IVesselRegister = {
      ...this.form.getRawValue(),
      vesselId: this.vesselId
    };
    const payload: IVesselRegister = this.inCreationMode
      ? { ...value }
      : { ...this.selectedRegister, ...value };

    const registerIdToSelect: VesselRegisterKey | null = this.inCreationMode
      ? null
      : this.selectedRegister.id;

    const request$: Observable<IVesselRegister> = this.inCreationMode
      ? this.vesselRegistriesRepo.create(payload, {})
      : this.vesselRegistriesRepo.update(payload, {});

    request$.subscribe(register => {
      this.cancelEditing();
      this.initData(registerIdToSelect);
      this.vesselsHelperService.isRegistrationMissing$.next(false);
    });
  }

  private selectVesselRegister = (vesselRegister: IVesselRegister): void => {
    this.selectedRegister = vesselRegister;
    this.initForm(vesselRegister);
    this.selectRegister.emit(this.selectedRegister);
    this.cdr.detectChanges();
  }

  public intervalClicked = (vesselRegisterId: VesselRegisterKey): void => {
    const vesselRegister: IVesselRegister = this.registers
      .find(v => v.id === vesselRegisterId);
    this.selectVesselRegister(vesselRegister);
  }

  public addIntervalClicked = (): void => {
    this.inCreationMode = true;
    this.selectedRegister = {} as IVesselRegister;

    const latestEntry: IVesselRegister = this.registers[this.registers.length - 1];
    const clonedRegister: IVesselRegister = latestEntry
      ? {
        ...latestEntry,
        from: latestEntry.until
          ? moment.utc(latestEntry.until).add(1, 'days').format()
          : '',
        until: undefined,
      }
      : {} as IVesselRegister;

    delete clonedRegister.id; // we don't need the id in creation mode
    this.initForm(clonedRegister); // cloning the most recent one
    this.enterEditMode();
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
