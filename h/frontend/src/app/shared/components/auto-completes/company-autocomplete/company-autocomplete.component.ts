import { Component, Input, OnInit } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup } from '@angular/forms';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/companies.config';
import { IWording } from 'src/app/core/models/resources/IWording';
import { Router } from '@angular/router';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { ICompany } from 'src/app/core/models/resources/ICompany';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'nv-company-autocomplete',
  templateUrl: './company-autocomplete.component.html',
  styleUrls: ['./company-autocomplete.component.scss']
})
export class CompanyAutocompleteComponent implements OnInit {
  @Input() public form: FormGroup;
  @Input() public controlName = 'companyId';
  @Input() public filters: object;
  @Input() public label: IWording | string = wording.system.company;
  @Input() public properties: string[] = ['companyName'];
  @Input() public showErrMessages = false;
  @Input() public streamFilter: (company: ICompany) => boolean;

  public wording = wording;
  public gridConfig = getGridConfig(this.router, () => false);
  public stream$: Observable<ICompany[]>;

  constructor(
    public companyRepo: CompanyRepositoryService,
    private router: Router,
  ) { }

  public ngOnInit() {
    this.companyRepo.fetchAll({}).subscribe();
    this.stream$ = this.streamFilter
      ? this.companyRepo.getStream().pipe(
          map(companies => companies.filter(c => this.streamFilter(c))),
        )
      : this.companyRepo.getStream();
  }
}
