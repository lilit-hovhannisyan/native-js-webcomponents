import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NvGridConfig } from 'nv-grid';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { getShareholdingsGridConfig } from 'src/app/core/constants/nv-grid-configs/shareholdings.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { IShareholding, ShareholdingsMainService } from 'src/app/core/services/mainServices/shareholdings-main.service';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ShareholdingModalComponent } from '../shareholding-modal/shareholding-modal.component';
import { Router } from '@angular/router';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { ILegalFormFilter } from '../../../../shared/legal-type-tree/legal-type-tree.component';

@Component({
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareholdingsListViewComponent implements OnInit {
  public dataSource$: Observable<IShareholding[]>;
  private legalNodeSubject$: BehaviorSubject<ILegalFormFilter>
    = new BehaviorSubject(undefined);
  public shareholdingsGridConfig: NvGridConfig;
  public wording = wording;
  private legalTypeFilter: ILegalFormFilter;

  constructor(
    public gridConfigService: GridConfigService,
    private shareholdingsMainService: ShareholdingsMainService,
    private authService: AuthenticationService,
    private modalService: NzModalService,
    private router: Router,
    private confirmationService: ConfirmationService,
  ) { }

  public ngOnInit(): void {
    this.dataSource$ = combineLatest([
      this.shareholdingsMainService.getStream(),
      this.legalNodeSubject$.asObservable(),
    ])
    .pipe(
      map(([shareholdings, legalTypeFilter]) => {
        this.legalTypeFilter = legalTypeFilter;
        const filteredShareholdings: IShareholding[] = !this.legalTypeFilter?.legalForm
          ? shareholdings
          : this.legalTypeFilter.legalFormType
            ? shareholdings.filter(s => s.legalFormType === legalTypeFilter.legalFormType)
            : shareholdings.filter(s => s.legalForm === legalTypeFilter.legalForm);

        return filteredShareholdings;
      })
    );

    const gridActions = {
      edit: (shareholding: IShareholding) => {
        this.router.navigate([`${this.router.url}/${shareholding.id}`]);
      },
      delete: (shareholding: IShareholding) => this.deleteShareholding(shareholding),
      add: () => this.createShareholding(),
    };

    const canPerform = (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation);
    this.shareholdingsGridConfig = getShareholdingsGridConfig(
      gridActions,
      canPerform
    );
  }

  public onLegalNodeSelect(legalTypeFilter: ILegalFormFilter): void {
    this.legalNodeSubject$.next(legalTypeFilter);
  }

  private createShareholding(): void {
    this.modalService.warning({
      ...DefaultModalOptions,
      nzClosable: false,
      nzContent: ShareholdingModalComponent,
      nzComponentParams: {
        legalForm: this.legalTypeFilter?.legalForm,
        legalFormType: this.legalTypeFilter?.legalFormType
      },
      nzWidth: 850,
      nzClassName: `nv-shareholding-modal`,
      nzFooter: null,
      nzOkText: null,
    });
  }

  private deleteShareholding(shareholding: IShareholding): void {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: setTranslationSubjects(
        wording.system.deleteShareholding,
        { subject: shareholding.companyName }
      ),
    }).subscribe(confirmed => {
      if (confirmed) {
        this.shareholdingsMainService
          .deactivateCompanyResponsibleForLegalData(shareholding);
      }
    });

  }
}
