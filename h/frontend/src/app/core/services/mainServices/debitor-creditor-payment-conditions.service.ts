import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { IPaymentCondition, IPaymentConditionLevel } from '../../models/resources/IPaymentConditions';
import { getPaymentDateOfCondition } from 'src/app/features/accounting/helpers/general';

@Injectable({
  providedIn: 'root'
})
export class DebitorCreditorPaymentConditionsService {

  // Gets next available payment date from payment condition and invoice date.
  // If no payment date is found, returns current date.
  public getSuggestedPaymentDate(paymentCondition: IPaymentCondition, invoiceDate: string): string {
    const today = new Date().toISOString();
    let paymentDate = today;
    if (paymentCondition) {

      paymentDate = getPaymentDateOfCondition(invoiceDate, paymentCondition.daysDiscountLevel1);
      if (moment(paymentDate).isSameOrBefore(moment(today))) {
        paymentDate = getPaymentDateOfCondition(invoiceDate, paymentCondition.daysDiscountLevel2);
        if (moment(paymentDate).isSameOrBefore(moment(today))) {
          paymentDate = getPaymentDateOfCondition(invoiceDate, paymentCondition.daysDiscountLevel3);
          if (moment(paymentDate).isSameOrBefore(moment(today))) {
            paymentDate = today;
          }
        }
      }
    }

    return paymentDate;
  }

  // Gets discount level for invoice date and payment date.
  // If no discount is found, return null.
  // Example:
  // Input: Invoice date: 1st (of month), payment date: 12th (of month),
  // Payment conditions: 3% (2%, 1%) discount within 10 (20, 30) days from invoice date.
  // Output: 2%, because payment date of 12th is in the 2% discount period.
  public getPaymentConditionLevelForPaymentDate(
    paymentCondition: IPaymentCondition,
    invoiceDate: Date,
    paymentDate: string
  ): IPaymentConditionLevel {
    const paymentDateNoTime = this.momentWithNoTime(moment(paymentDate));
    const invoiceDateNoTime = this.momentWithNoTime(moment(invoiceDate));

    const suggestedPaymentDate1 = this.addDays(invoiceDateNoTime, paymentCondition.daysDiscountLevel1);
    if (paymentDateNoTime.isSameOrAfter(invoiceDateNoTime) && paymentDateNoTime.isSameOrBefore(suggestedPaymentDate1)) {
      return this.extractPaymentConditionLevel(paymentCondition, 1);
    }

    const suggestedPaymentDate2 = this.addDays(invoiceDateNoTime, paymentCondition.daysDiscountLevel2);
    if (paymentDateNoTime.isSameOrAfter(suggestedPaymentDate1) && paymentDateNoTime.isSameOrBefore(suggestedPaymentDate2)) {
      return this.extractPaymentConditionLevel(paymentCondition, 2);
    }

    const suggestedPaymentDate3 = this.addDays(invoiceDateNoTime, paymentCondition.daysDiscountLevel3);
    if (paymentDateNoTime.isSameOrAfter(suggestedPaymentDate2) && paymentDateNoTime.isSameOrBefore(suggestedPaymentDate3)) {
      return this.extractPaymentConditionLevel(paymentCondition, 3);
    }

    return null;
  }

  private extractPaymentConditionLevel(paymentCondition: IPaymentCondition, level: number): IPaymentConditionLevel {
    switch (level) {
      case 1:
        return ({
          level: 1,
          percentageDiscount: paymentCondition.percentageDiscountLevel1,
          daysDiscount: paymentCondition.daysDiscountLevel1
        });
      case 2:
        return ({
          level: 2,
          percentageDiscount: paymentCondition.percentageDiscountLevel2,
          daysDiscount: paymentCondition.daysDiscountLevel2
        });
      case 3:
        return ({
          level: 3,
          percentageDiscount: paymentCondition.percentageDiscountLevel3,
          daysDiscount: paymentCondition.daysDiscountLevel3
        });
      default:
        return null;
    }
  }

  private addDays(date: moment.Moment, days: number): moment.Moment {
    return this.momentWithNoTime(date)
      .add(days, 'days');
  }

  private todayWithNoTime() {
    return moment().utcOffset(0).startOf('day');
  }

  public momentWithNoTime(date: moment.Moment) {
    return moment(date).utcOffset(0).startOf('day');
  }
}
