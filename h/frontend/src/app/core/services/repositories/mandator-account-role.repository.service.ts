import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MandatorAccountRoleKey } from 'src/app/core/models/resources/IMandatorAccountRole';
import { MandatorAccountKey } from 'src/app/core/models/resources/IMandatorAccount';
import { HttpPost } from 'src/app/core/models/http/http-post';
import { IResourceCollection } from 'src/app/core/models/resources/IResourceCollection';
import { IDataResponseBody } from 'src/app/core/models/responses/IDataResponseBody';
import { RoleKey, IRole } from 'src/app/core/models/resources/IRole';
import { RepoParams, AbstractRepository } from 'src/app/core/abstracts/abstract.repository';
import { IMandatorAccountRole, IMandatorAccountRoleDTO, IMandatorAccountRoleCreate } from 'src/app/core/models/resources/IMandatorAccountRole';
import { HttpGet } from 'src/app/core/models/http/http-get';
import { RepositoryService } from '../repository.service';


@Injectable({
  providedIn: 'root'
})
export class MandatorAccountRoleRepositoryService extends AbstractRepository<
IMandatorAccountRole,
IMandatorAccountRoleDTO,
IMandatorAccountRoleCreate,
RepoParams<IMandatorAccountRole>,
MandatorAccountRoleKey
> {
  protected resourceType = 'MandatorAccountRole';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/mandator-account-roles/';
  }

  public fetchRoles(accountId: MandatorAccountKey): Observable<IRole[]> {
    const request = new HttpGet(`/api/mandator-accounts/${accountId}/roles`);

    return this.repositoryService.httpClient.request(request).pipe(
      map((response: any) => response.body.data.items as IRole[])
    );
  }

  public setRolesForMandatorAccount(
    roleIds: RoleKey[],
    accountId: MandatorAccountKey,
 ): Observable<IMandatorAccountRole[]> {
    const url = `/api/mandator-accounts/${accountId}/roles`;
    const request = new HttpPost(url, roleIds);

    return this.repositoryService.httpClient.request(request).pipe(
      map((response: HttpResponse<
        IDataResponseBody<IResourceCollection<IMandatorAccountRole>>
      >) => response.body.data.items),
    );
  }

  protected postProcess(item: IMandatorAccountRoleDTO): IMandatorAccountRole { return item; }
}
