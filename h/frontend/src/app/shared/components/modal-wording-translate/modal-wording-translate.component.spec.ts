import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalWordingTranslateComponent } from './modal-wording-translate.component';

describe('WordingTemplateComponent', () => {
  let component: ModalWordingTranslateComponent;
  let fixture: ComponentFixture<ModalWordingTranslateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalWordingTranslateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalWordingTranslateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
