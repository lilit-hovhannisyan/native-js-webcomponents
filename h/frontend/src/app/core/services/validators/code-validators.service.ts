import { ValidationErrors, AbstractControl } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { Injectable } from '@angular/core';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { MandatorBookingCodeRepositoryService } from 'src/app/core/services/repositories/mandator-booking-code-repository.service';
import { IMandatorBookingCode } from 'src/app/core/models/resources/IMandatorBookingCode';
import { BookingCodeRepositoryService } from 'src/app/core/services/repositories/booking-code-repository.service';
import { IBookingCode, BookingCodeKey } from 'src/app/core/models/resources/IBookingCode';
import { MandatorKey } from 'src/app/core/models/resources/IMandator';

@Injectable({
  providedIn: 'root'
})
export class CodeValidatorsService {

  constructor(
    private bookingCodeRepo: BookingCodeRepositoryService,
    private mandatorCodeRepo: MandatorBookingCodeRepositoryService
  ) { }


  public doesBookingCodeExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const foundBookingCode = this.bookingCodeRepo.getStreamValue()
      .find(bookingCode => bookingCode.id === control.value);

    if (foundBookingCode) {
      return null;
    }

    markControlAsTouched(control);
    return {
      mandatorNotFound: {
        message: wording.accounting.notFoundCode
      }
    };
  }

  public doesBookingCodeNoExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const bookingCode: IBookingCode = this.bookingCodeRepo.getStreamValue()
      .find(b => b.no === control.value);

    if (bookingCode) {
      return null;
    }

    markControlAsTouched(control);
    return {
      mandatorNotFound: {
        message: wording.accounting.notFoundCode
      }
    };
  }

  public isBookingCodeActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const foundbookingCode = this.bookingCodeRepo.getStreamValue()
      .find(bookingCode => bookingCode.id === control.value);

    if (foundbookingCode?.isActive) {
      return null;
    }

    markControlAsTouched(control);
    return {
      notActiveCode: {
        message: wording.accounting.notActiveCode
      }
    };
  }

  public isBookingCodeNoActive = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const bookingCode: IBookingCode = this.bookingCodeRepo.getStreamValue()
      .find(b => b.no === control.value);

    if (bookingCode?.isActive) {
      return null;
    }

    markControlAsTouched(control);
    return {
      notActiveCode: {
        message: wording.accounting.notActiveCode
      }
    };
  }

  public isBookingCodeNoRelatedToMandator = (control: AbstractControl): ValidationErrors | null => {
    if (
      !control.value
      || !control.parent
      || !control.parent.get('mandatorId').value
    ) {
      return null;
    }

    const bookingCode: IBookingCode = this.bookingCodeRepo.getStreamValue()
      .find(b => b.no === control.value);

    let foundMandatorbookingCode: IMandatorBookingCode;
    if (bookingCode) {
      const mandatorId: MandatorKey = control.parent.get('mandatorId').value;
      const bookingCodeId: BookingCodeKey = bookingCode.id;

      foundMandatorbookingCode = this.mandatorCodeRepo.getStreamValue()
        .find(mandatorBookingCode =>
          mandatorBookingCode.bookingCodeId === bookingCodeId
          && mandatorBookingCode.mandatorId === mandatorId);
    }

    if (foundMandatorbookingCode) {
      return null;
    }

    markControlAsTouched(control);
    return {
      bookingCodeNotRelatedToMandator: {
        message: wording.general.bookingCodeNotRelatedToMandator
      }
    };
  }

  public isBookingCodeRelatedToBookingMandatorId = (control: AbstractControl): ValidationErrors | null => {
    if (
      !control.value
      || !control.parent
      || !control.parent.get('bookingMandatorId').value
    ) {
      return null;
    }

    const bookingMandatorId = control.parent.get('bookingMandatorId').value;
    const foundBookingCode: IMandatorBookingCode = this.mandatorCodeRepo.getStreamValue()
      .find(mandatorBookingCode =>
        mandatorBookingCode.bookingCodeId === control.value
        && mandatorBookingCode.mandatorId === bookingMandatorId);

    if (foundBookingCode) {
      return null;
    }

    markControlAsTouched(control);
    return {
      notRelatedToMandator: {
        message: wording.general.notRelatedToMandator
      }
    };
  }
}
