import {Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'lavash-img-slider',
  templateUrl: './img-slider.component.html',
  styleUrls: ['./img-slider.component.less']
})
export class ImgSliderComponent implements OnInit, OnChanges {
  @Input() sliderItem;
  @Input() sliderItems;
  @Input() imagePath;
  @Input() imgPosition: 'left'| 'right' | 'center' = 'center';

  @Input() prevIcon = 'icon-keyboard_arrow_left';
  @Input() nextIcon = 'icon-keyboard_arrow_right';

  @Output() nextImageEvent = new EventEmitter();
  @Output() prevImageEvent = new EventEmitter();

  defaultAlt = 'Lavash image';

  prev;
  next;
  curIndex;


  ngOnInit() {
    this.initSlider(this.sliderItem, this.sliderItems);
  }

  ngOnChanges() {
    this.initSlider(this.sliderItem, this.sliderItems);
  }

  initSlider(currentItem, arr) {
    this.sliderItem = currentItem;
    this.curIndex = arr.findIndex( i => i === currentItem);

    this.prev = this.curIndex === 0 ? arr[arr.length - 1] : arr[this.curIndex - 1];
    this.next = this.curIndex === arr.length - 1 ? arr[0] : arr[this.curIndex + 1];
  }

  onSlide(item) {
    this.initSlider(this[item], this.sliderItems);

    this[item + 'ImageEvent'].emit(this.sliderItem);
  }

  onKeydown(event) {
    const code = event.e.keyCode;
    if ( code === 39 ||  code === 37 || code === 40 ||  code === 38 ) {
      event.e.preventDefault();
      code === 39 || code === 40 ? this.onSlide('next') : this.onSlide('prev');
    }
  }

  onTouchEvent(event) {
    console.log( 'slider', event);

    if (event.tauchTargetMatch && event.touchTargetStart.indexOf('l-icon') >= 0) {
      return;
    }
    event.toLeft ? this.onSlide('prev') : this.onSlide('next');
  }
}
