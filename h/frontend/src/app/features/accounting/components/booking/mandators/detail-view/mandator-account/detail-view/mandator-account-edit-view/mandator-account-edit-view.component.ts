import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { combineLatest, ReplaySubject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { orderBy } from 'lodash';
import { IMandatorAccountFull } from 'src/app/core/models/resources/IMandatorAccount';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { MandatorKey } from 'src/app/core/models/resources/IMandator';
import { RoleRepositoryService } from 'src/app/core/services/repositories/role-repository.service';
import { IRole } from 'src/app/core/models/resources/IRole';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { SettingsService } from 'src/app/core/services/settings.service';
import { MandatorAccountRoleRepositoryService } from 'src/app/core/services/repositories/mandator-account-role.repository.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { SettingsRepositoryService } from 'src/app/core/services/repositories/settings-repository.service';
import { IStartEndRange } from 'src/app/core/models/resources/ISettings';
import { DEBITOR_CREDITOR } from 'src/app/core/models/enums/debitor-creditor';

export interface IRoleOptions extends IRole {
  checked: boolean;
  isVisible: boolean;
}

@Component({
  selector: 'nv-mandator-account',
  templateUrl: './mandator-account-edit-view.component.html',
  styleUrls: ['./mandator-account-edit-view.component.scss']
})
export class MandatorAccountEditViewComponent implements OnInit, OnDestroy {
  @Input() public mandatorId: MandatorKey;
  @Input() public canEdit: boolean;
  @Input() public mandatorAccount: IMandatorAccountFull;
  @Input() public closeModal: () => void;
  @Input() public showOnlyOpenItem: boolean;

  public creationMode: boolean;
  public form: FormGroup;
  public wording = wording;
  public canChangeRoles: boolean;
  public searchValue = '';
  public roles: IRoleOptions[] = [];
  public filteredRoles: IRoleOptions[] = [];
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  private debitorCreditorRanges: IStartEndRange<DEBITOR_CREDITOR>[];
  public isOpenItemDisabled: boolean;

  constructor(
    private fb: FormBuilder,
    public mandatorAccountRepo: MandatorAccountRepositoryService,
    private roleRepo: RoleRepositoryService,
    private mandatorAccountRoleRepo: MandatorAccountRoleRepositoryService,
    private settingsService: SettingsService,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    public settingsRepo: SettingsRepositoryService,
  ) { }

  public ngOnInit(): void {
    this.settingsRepo.fetchAll().subscribe(settings => {
      this.debitorCreditorRanges = settings.system.debitorCreditorRanges;
      this.isOpenItemDisabled = this.isInDebitorCreditorRange();
    });
    const labelKey = getLabelKeyByLanguage(this.settingsService.language);
    const currentUser = this.authService.getUser();
    this.canChangeRoles = currentUser.isSuperAdmin || currentUser.isAdmin;

    combineLatest([
      this.roleRepo.fetchAll({}),
      this.mandatorAccountRoleRepo.fetchRoles(this.mandatorAccount.id),
    ]).pipe(map(([
      roles,
      selectedRoles,
    ]) => {
      this.roles = orderBy(
        roles.map(role => ({
          ...role,
          isVisible: true,
          checked: this.isRoleAssignedToMandatorAccount(role, selectedRoles)
        })), [labelKey], ['asc']);
    })).subscribe(() => {
      this.search();
    });

    this.initForm();
  }

  public initForm(): void {
    this.form = this.fb.group({
      mandatorId: [this.mandatorId],
      bookingAccountId: [this.mandatorAccount.bookingAccountId],
      isOpenItem: [{
        value: this.mandatorAccount.isOpenItem,
        disabled: !this.canEdit,
      }],
      isOpex: [{
        value: this.mandatorAccount.isOpex,
        disabled: !this.canEdit,
      }],
      restrictedCurrencyId: [this.mandatorAccount.restrictedCurrencyId],
      needsAccountPermission: [{
        value: this.mandatorAccount.needsAccountPermission,
        disabled: !this.canChangeRoles || !this.canEdit,
      }],
      searchValue: [this.searchValue],
    });

    this.form.get('searchValue').valueChanges.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(() => this.search());
  }

  private isInDebitorCreditorRange(): boolean {
    return this.debitorCreditorRanges.some(range => this.mandatorAccount.no >= range.start
      && this.mandatorAccount.no <= range.end);
  }

  public saveClicked(): void {
    if (!this.showOnlyOpenItem && this.isAccountPermissionCheckedWithoutRoles()) {
      this.confirmationService.confirm({
        okButtonWording: wording.general.save,
        cancelButtonWording: wording.general.cancel,
        wording: wording.accounting.confirmSaveWithoutPermission,
      }).subscribe(confirmed => {
        if (confirmed) {
          this.submit();
        }
      });
    } else {
      this.submit();
    }
  }

  public search() {
    const searchValue = this.form.get('searchValue').value.toLowerCase();
    const labelKey = getLabelKeyByLanguage(this.settingsService.language);

    this.filteredRoles = this.roles.filter(role => {
      const title = role[labelKey].toLowerCase();
      const isVisible = !searchValue || title.includes(searchValue);

      return isVisible;
    });
  }

  private getFormValue(): IMandatorAccountFull {
    const formValue = { ...this.mandatorAccount, ...this.form.value };
    const { bookingAccount, bookingAccountLabel, ...value } = formValue;
    return value;
  }

  private submit(): void {
    if (!this.showOnlyOpenItem && this.canChangeRoles) {
      const selectedRoleIds = this.roles
        .filter(role => role.checked)
        .map(role => role.id);

      combineLatest([
        this.mandatorAccountRepo.update(this.getFormValue(), {}),
        this.mandatorAccountRoleRepo.setRolesForMandatorAccount(selectedRoleIds, this.mandatorAccount.id),
      ]).subscribe(() => this.closeModal());
    } else {
      this.mandatorAccountRepo.update(this.getFormValue(), {})
        .subscribe(() => this.closeModal());
    }
  }

  private isAccountPermissionCheckedWithoutRoles() {
    return this.form.get('needsAccountPermission').value
      && this.roles.filter(role => role.checked).length === 0;
  }

  private isRoleAssignedToMandatorAccount = (
    role: IRole,
    selectedRoles: IRole[],
  ): boolean => {
    return !!selectedRoles
      .find(mandatorAccoutRole => mandatorAccoutRole.id === role.id);
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
