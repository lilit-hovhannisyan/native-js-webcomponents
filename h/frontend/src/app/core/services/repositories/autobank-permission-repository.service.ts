import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { AutobankPermissionKey, IAutobankPermission, IAutobankPermissionCreate, IAutobankPermissionDTO } from '../../models/resources/IAutobankPermission';
import { RepositoryService } from '../repository.service';

export interface IAutobankPermissionGroupsParams extends RepoParams<IAutobankPermission> { }

@Injectable({
  providedIn: 'root'
})
export class AutobankPermissionRepositoryService extends AbstractRepository<
IAutobankPermission,
IAutobankPermissionDTO,
IAutobankPermissionCreate,
IAutobankPermissionGroupsParams,
AutobankPermissionKey
>  {
  protected resourceType = 'AutobankPermission';
  protected sortAsce = true;
  protected sortKey = 'id';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IAutobankPermissionDTO): IAutobankPermission => item;

  public getBaseUrl = () => '/api/autobank/permissions';
}
