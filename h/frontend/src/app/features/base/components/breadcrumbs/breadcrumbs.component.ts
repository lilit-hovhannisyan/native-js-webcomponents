import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { SitemapEntry, url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { getParentScreenUrl } from 'src/app/core/helpers/makeUrl';


@Component({
  selector: 'nv-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadCrumbsComponent implements OnInit {
  public url = url;
  public crumbs: SitemapNode[] = [];
  public isVisible = true;
  public showBackButton = false;

  constructor(
    private router: Router) { }

  public ngOnInit(): void {
    this.setShowBackButton();
    this.setCrumbs(this.router.url);
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.setShowBackButton();
        this.setCrumbs(this.router.url);
      }
    });
  }

  public setCrumbs(_url: string): void {

    this.crumbs = []; // start fresh with empty crumbs
    // getting the crumbs in this format: ['category', 'section', 'topic']
    let sitemapEntry = SitemapEntry.getByUrl(_url);
    while (sitemapEntry) {

      if (sitemapEntry.key !== 'id') {  // jump over id-nodes ( they are no real routes )
        this.crumbs = [sitemapEntry.node, ...this.crumbs];
      }

      sitemapEntry = sitemapEntry.parent;
    }
    this.isVisible = !!(this.crumbs[0] && !this.crumbs[0].hidden);
  }

  public back(): void {
    const parentScreenUrl = getParentScreenUrl(this.router.url);

    this.router.navigate([parentScreenUrl]);
  }

  private setShowBackButton() {
    const backSlashesCount = (this.router.url.match(/\//g) || []).length;

    this.showBackButton = backSlashesCount === 3;
  }
}
