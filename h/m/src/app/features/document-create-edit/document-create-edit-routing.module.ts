import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaveCheckGuard } from '@core-guards/leave-check.guard';

import { DocumentCreateComponent } from './document-create-edit.component';

const routes: Routes = [{ path: '', component: DocumentCreateComponent, canDeactivate: [LeaveCheckGuard] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentCreateRoutingModule {}
