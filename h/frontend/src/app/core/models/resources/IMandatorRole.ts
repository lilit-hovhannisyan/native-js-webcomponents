import { MandatorKey } from './IMandator';
import { IResource } from './IResource';
import { RoleKey } from './IRole';

export type MandatorRoleKey = number;

interface ISharedProps {
  mandatorId: MandatorKey;
  roleId: RoleKey;
}

export interface IMandatorRoleBase extends ISharedProps, IResource<MandatorRoleKey> { }

export interface IMandatorRoleCreate extends ISharedProps { }

export interface IMandatorRoleDTO extends IMandatorRoleBase { }

export interface IMandatorRole extends IMandatorRoleDTO { }
