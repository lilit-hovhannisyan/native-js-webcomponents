import { Injectable } from '@angular/core';
import { forkJoin, Observable, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { IArticleType } from '../../models/resources/IArticleType';
import { ArticleRepositoryService } from '../repositories/article-repository.service';
import { IArticle, ArticleKey, IArticleFull, IArticleRow } from '../../models/resources/IArticle';
import { ArticleUnitsRepositoryService } from '../repositories/article-units-repository.service';
import { ArticleTypeRepositoryService } from '../repositories/article-type-repository.service';
import { ArticleClassRepositoryService } from '../repositories/article-class-repository.service';
import { IArticleClass } from '../../models/resources/IArticleClass';
import { IArticleUnit } from 'src/app/core/models/resources/IArticleUnit';
import { RepoParams } from '../../abstracts/abstract.repository';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { ICurrency } from '../../models/resources/ICurrency';
import { DebitorCreditorRepositoryService } from '../repositories/debitor-creditor-repository.service';
import { IDebitorCreditor } from '../../models/resources/IDebitorCreditor';

@Injectable({
  providedIn: 'root'
})
export class ArticleMainService {

  constructor(
    private articleRepo: ArticleRepositoryService,
    private articleUnitRepo: ArticleUnitsRepositoryService,
    private articleTypeRepo: ArticleTypeRepositoryService,
    private articleClassRepo: ArticleClassRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private debitorCreditorRepo: DebitorCreditorRepositoryService,
  ) { }

  public fetchAll(params: RepoParams<IArticle>): Observable<IArticleFull[]> {
    return forkJoin([
      this.articleRepo.fetchAll(params),
      this.articleUnitRepo.fetchAll({}),
      this.articleTypeRepo.fetchAll({}),
      this.articleClassRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
    ]).pipe(map(res => this.getArticleFullList(...res)));
  }

  public fetchOne(articleId: ArticleKey, params: RepoParams<IArticle>) {
    return forkJoin([
      this.articleRepo.fetchOne(articleId, params),
      this.articleUnitRepo.fetchAll({}),
      this.articleTypeRepo.fetchAll({}),
      this.articleClassRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
    ]).pipe(map(res => this.getArticleFull(...res)));
  }

  private getArticleFullList(
    articles: IArticle[],
    articleUnits: IArticleUnit[],
    articleTypes: IArticleType[],
    articleClasses: IArticleClass[],
    currencies: ICurrency[],
    debitorCreditor: IDebitorCreditor[],
  ): IArticleFull[] {
    return articles.map(article => this.getArticleFull(
      article,
      articleUnits,
      articleTypes,
      articleClasses,
      currencies,
      debitorCreditor,
    ));
  }

  private getArticleFull(
    article: IArticle,
    articleUnits: IArticleUnit[],
    articleTypes: IArticleType[],
    articleClasses: IArticleClass[],
    currencies: ICurrency[],
    debitorCreditors: IDebitorCreditor[],
  ): IArticleFull {
    const articleUnit = articleUnits.find(au => au.id === article.articleUnitId);
    const articleType = articleTypes.find(at => at.id === article.articleTypeId);
    const articleClass = articleClasses.find(ac => ac.id === article.articleClassId);
    const currency = currencies.find(c => c.id === article.currencyId);
    const debitorCreditor = debitorCreditors.find(d => d.id === article.debitorCreditorId);

    return { ...article, articleUnit, articleType, articleClass, currency, debitorCreditor };
  }

  public getStream(
    params: RepoParams<IArticle>,
    filterParams: Partial<IArticle>
  ): Observable<IArticleFull[]> {
    return forkJoin([
      this.articleRepo.fetchAll(params),
      this.articleUnitRepo.fetchAll({}),
      this.articleTypeRepo.fetchAll({}),
      this.articleClassRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
      this.debitorCreditorRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.articleRepo.getStream(filterParams),
        this.articleUnitRepo.getStream(),
        this.articleTypeRepo.getStream(),
        this.articleClassRepo.getStream(),
        this.currencyRepo.fetchAll({}),
        this.debitorCreditorRepo.getStream({}),
      ])),
      map(collection => this.getArticleFullList(...collection))
    );
  }

  public toRow(article: IArticleFull): IArticleRow {
    return ({
      ...article,
      unitName: article.articleUnit.displayLabel,
      articleClassName: article.articleClass?.displayLabel,
      articleTypeName: article.articleType?.displayLabel,
      isoCode: article.currency ? article.currency.isoCode : '',
      debitorCreditorName: article.debitorCreditor?.displayLabel,
    });
  }

  public toRows(articles: IArticleFull[]): IArticleRow[] {
    return articles.map(a => this.toRow(a));
  }
}
