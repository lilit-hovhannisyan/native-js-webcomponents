import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { PaperViewStateService } from 'src/app/core/services/ui/paper-view-state.service';
import { getTitleByUrl } from 'src/app/core/constants/sitemap/sitemap-entry';
import { IWording } from 'src/app/core/models/resources/IWording';
import { getParentScreenUrl } from 'src/app/core/helpers/makeUrl';

/**
 * This component renders a paper like looking view,
 * which should be used to wrap and visualize any kind of content.
 * Each page should normally consist of minimum 1 paper view.
 */
@Component({
  selector: 'nv-paper-view',
  templateUrl: './paper-view.component.html',
  styleUrls: ['./paper-view.component.scss']
})
export class PaperViewComponent implements OnInit, OnChanges {
  /**
   * By passing a routeBack function, this component
   * will render a back button in the paper header, next to the title
   */
  @Input() public routeBack?: () => void;

  /**
   * Per default the title for the header of the paper
   * is being retrieved from the sitemap via the current URL.
   * By passing a value in the customTitle input, this behavior
   * can be overridden.
   */
  @Input() public customTitle?: string;


  @Input() public contentWrapperStyle = {};
  @Input() public tooltip: boolean;

  @Input() public state?: any;

  public showBackButton = false;

  constructor(
    private router: Router,
    public paperViewStateService: PaperViewStateService,
  ) { }

  public title: IWording;
  public tooltipTitle: string;

  public ngOnInit() {
    this.title = getTitleByUrl(this.router.url);
    this.setTooltipTitle();
    this.setShowBackButton();
  }

  public ngOnChanges({ customTitle, tooltip }: SimpleChanges) {
    if (customTitle || tooltip) {
      this.setTooltipTitle();
    }

    this.setShowBackButton();
  }

  private setTooltipTitle() {
    if (typeof this.tooltip !== 'undefined' && this.customTitle) {
      this.tooltipTitle = this.customTitle.replace(/<[^>]*>/g, '');
    } else {
      this.tooltipTitle = '';
    }
  }

  public back(): void {
    if (this.routeBack) {
      this.routeBack();
      return;
    }

    const parentScreenUrl = getParentScreenUrl(this.router.url);

    this.router.navigate([parentScreenUrl]);
  }

  private setShowBackButton() {
    const backSlashesCount = (this.router.url.match(/\//g) || []).length;

    this.showBackButton = backSlashesCount > 2 || !!this.routeBack;
  }
}
