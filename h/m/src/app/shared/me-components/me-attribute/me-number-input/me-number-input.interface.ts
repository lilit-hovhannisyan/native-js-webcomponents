import { FormGroup } from '@angular/forms';
import { MeInputErrorI } from '../me-input-error/me-input-error.interface';

export interface MeNumberInputI {
  form: FormGroup;
  inputErrors: MeInputErrorI;
  internalName: string;
  displayName: string;
  required: boolean;
  optRequired: boolean;
  value: number;
  minValue: number;
  maxValue: number;
  disabled: boolean;
}
