import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { StevedoreDamageReportMainService } from 'src/app/core/services/mainServices/stevedore-damage-report-main.service';
import { Language } from 'src/app/core/models/language';
import { SettingsService } from 'src/app/core/services/settings.service';
import { StevedoreDamagesHelperService } from './stevedore-damages.helper.service';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';

@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
})
export class StevedoreDamagesDetailViewComponent implements OnInit, OnDestroy {
  public sitemap = sitemap;
  public title: string;
  public language: Language;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  public get isCreationMode() {
    return this.route.snapshot.params.id === 'new';
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private stevedoreDamageReportMainService: StevedoreDamageReportMainService,
    private settingsService: SettingsService,
    private stevedoreDamagesHelperService: StevedoreDamagesHelperService,
  ) { }

  public ngOnInit() {
    this.language = this.settingsService.language;

    this.stevedoreDamagesHelperService.editViewTitle$.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(title => {
      this.title = title;
    });

    if (!this.isCreationMode) {
      const id = this.route.snapshot.params.id;
      this.stevedoreDamageReportMainService.fetchOne(id, {}).subscribe(report => {
        const sdrRow = this.stevedoreDamageReportMainService.toRow(report);
        this.stevedoreDamagesHelperService.generateTitle(sdrRow);
      });
    } else {
      this.title = wording.general.create[this.language];
    }
  }

  public routeBack = () => {
    this.router.navigate([
      url(sitemap.technicalManagement.children.misc.children.stevedoreDamages),
    ]);
  }

  public tabDisabler = (node: SitemapNode) => {
    const basicNode = sitemap.technicalManagement.children.misc.children.stevedoreDamages.children.id.children.basic;
    // in creationMode just enable the basicTab
    return this.isCreationMode ? node === basicNode : true;
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
