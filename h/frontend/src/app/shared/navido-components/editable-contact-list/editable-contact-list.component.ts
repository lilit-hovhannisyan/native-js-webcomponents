import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { ContactKey } from 'src/app/core/models/resources/IContact';
import { ContactDetailViewComponent } from './detail-view/contact-detail-view.component';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { ContactRepositoryService } from './../../../core/services/repositories/contact-repository.service';
import { IContactRow, ContactMainService } from './../../../core/services/mainServices/contact-main.service';
import { Component, Input, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { getContactGridConfig } from './contactGridConfig';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Language } from 'src/app/core/models/language';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  selector: 'nv-editable-contact-list',
  templateUrl: './editable-contact-list.component.html',
  styleUrls: ['./editable-contact-list.component.scss']
})

export class EditableContactListComponent implements OnInit {
  @Input() private companyId: number;
  public contactsGridConfig: NvGridConfig;
  public dataSource$: Observable<IContactRow[]>;
  private modalRef?: NzModalRef;

  constructor(
    private contactService: ContactMainService,
    private contactRepo: ContactRepositoryService,
    private modalService: NzModalService,
    public gridConfigService: GridConfigService,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private settingsService: SettingsService
  ) { }

  public ngOnInit(): void {
    const params = { companyId: this.companyId };

    this.dataSource$ = this.contactService.getStream(params)
      .pipe(map((contacts) => this.contactService.toRows(contacts)));

    const contactsNode = sitemap.system.children.misc.children.companies.children.id.children.contacts;
    const acitons = {
      edit: this.openDetailViewModal,
      delete: this.deleteRowClicked,
      add: this.openDetailViewModal,
    };

    this.contactsGridConfig = getContactGridConfig(
      acitons,
      (operation) => this.authService.canPerformOperationOnNode(operation, contactsNode)
    );
  }

  public openDetailViewModal = (contact: IContactRow = {} as IContactRow) => {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.accounting.contact[this.settingsService.language],
      nzContent: ContactDetailViewComponent,
      nzComponentParams: {
        contactId: contact.id,
        companyId: this.companyId,
        closeModal: () => this.modalRef.destroy()
      },
      nzFooter: null,
      nzClosable: false,
    });
  }

  public deleteRowClicked = (contact: IContactRow): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: '' },
    }).subscribe(confirmed => confirmed && this.delete(contact.id));
  }

  public delete(id: ContactKey): void {
    this.contactRepo.delete(id, {}).subscribe();
  }
}
