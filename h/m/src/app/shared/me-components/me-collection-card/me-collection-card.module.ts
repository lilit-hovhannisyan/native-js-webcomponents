import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';

import { MatCheckboxModule } from '@angular/material/checkbox';

import { IconsModule } from '@shared/modules/icons.module';

import { MeCollectionCardComponent } from './me-collection-card.component';
import { MeButtonModule } from '../me-button/me-button.module';
import { MeUserImageModule } from '../me-user-image/me-user-image.module';
import { MeContextMenuModule } from '../me-context-menu/me-context-menu.module';

@NgModule({
  declarations: [MeCollectionCardComponent],
  imports: [CommonModule, IconsModule, MeUserImageModule, MeButtonModule, MatCheckboxModule, MeContextMenuModule, FormsModule],
  exports: [MeCollectionCardComponent]
})
export class MeCollectionCardModule {}
