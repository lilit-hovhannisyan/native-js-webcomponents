import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import {ShareholdingsListViewComponent} from '../../list-view/list-view.component';

@Component({
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareholdingsDocumentsComponent implements OnInit {

  constructor() { }

  public ngOnInit(): void {
  }

}
