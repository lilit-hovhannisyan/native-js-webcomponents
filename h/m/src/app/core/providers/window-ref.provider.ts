import { Injectable } from '@angular/core';
import { ME_DESKTOP, ME_DESKTOP_LARGE, ME_LAPTOP, ME_MOBILE, ME_TABLET, ME_TABLET_SMALL } from 'src/app/shared/constants';

function _window(): Window {
  // return the global native browser window object
  return window;
}

@Injectable()
export class WindowRef {
  constructor() {}

  get nativeWindow(): Window {
    return _window();
  }

  openInNewWindow(url: string): void {
    this.nativeWindow.open(url, '_blank', 'noopener,noreferrer');
  }

  isMobile(): boolean {
    return this.nativeWindow.innerWidth <= ME_MOBILE;
  }

  isTabletSmall(): boolean {
    return this.nativeWindow.innerWidth <= ME_TABLET_SMALL;
  }

  isTablet(): boolean {
    return this.nativeWindow.innerWidth <= ME_TABLET;
  }

  isLaptop(): boolean {
    return this.nativeWindow.innerWidth <= ME_LAPTOP;
  }

  isDesktop(): boolean {
    return this.nativeWindow.innerWidth <= ME_DESKTOP;
  }

  isDesktopLarge(): boolean {
    return this.nativeWindow.innerWidth >= ME_DESKTOP_LARGE;
  }
}
