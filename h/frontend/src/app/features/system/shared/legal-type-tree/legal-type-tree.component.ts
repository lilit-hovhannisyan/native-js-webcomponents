import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, Input } from '@angular/core';
import { NzFormatEmitEvent, NzTreeComponent, NzTreeNode } from 'ng-zorro-antd/tree';
import { COMPANY_LEGAL_FORM, COMPANY_LEGAL_FORM_TYPE, getCompanyLegalForm, getCompanyLegalFormTypes} from 'src/app/core/models/enums/company-legal-form';
import { ISelectOption } from 'src/app/core/models/misc/selectOption';
import { SettingsService } from 'src/app/core/services/settings.service';
import { Language } from 'src/app/core/models/language';
import { wording } from 'src/app/core/constants/wording/wording';

interface ILegalFormNode {
  title: string;
  key: string;
  isLeaf?: boolean;
  expanded?: boolean;
  children?: ILegalFormNode[];
  disableCheckbox?: boolean;
}

export interface ILegalFormFilter {
  legalForm?: number;
  legalFormType?: number;
}

@Component({
  selector: 'nv-legal-type-tree',
  templateUrl: './legal-type-tree.component.html',
  styleUrls: ['./legal-type-tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LegalTypeTreeComponent implements OnInit {
  @Input() public title?: string;
  @Output() public legalNodeSelect: EventEmitter<ILegalFormFilter> = new EventEmitter();
  public nodes: ILegalFormNode[] = [];
  public customClassName = 'full-expanded';
  public legalForm: COMPANY_LEGAL_FORM;
  private legalFormType: COMPANY_LEGAL_FORM_TYPE;

  private expandedNodeList: string[] = [];

  public wording = wording;

  constructor(
    private settingsService: SettingsService,
  ) { }

  public ngOnInit(): void {
    const language: Language = this.settingsService.language;
    const legalForms: ISelectOption<COMPANY_LEGAL_FORM>[] = getCompanyLegalForm();

    legalForms.forEach(lf => {
      const legalFormTypes: ISelectOption<COMPANY_LEGAL_FORM_TYPE>[] = getCompanyLegalFormTypes(lf.value);
      const children: ILegalFormNode[] = legalFormTypes.map(lft => {
        return this.legalObjToNzNode(lft, language, 'legalFormType', true);
      });

      const lfNode: ILegalFormNode =
        this.legalObjToNzNode(lf, language, 'legalForm', false, children);
      this.nodes.push(lfNode);
    });

  }

  private legalObjToNzNode(
    obj: ISelectOption,
    language: Language,
    prefix: string,
    isLeaf: boolean = false,
    children?: ILegalFormNode[]
  ): ILegalFormNode {
    const expanded: boolean = !isLeaf && !!children.length;

    const node: ILegalFormNode = {
      title: obj.displayLabel[language],
      key: prefix + obj.value, // key must be unique, but value is repeated between legal type & legal type form
      isLeaf,
      expanded,
      children,
      disableCheckbox: expanded
    };

    if (expanded) {
      this.expandedNodeList.push(node.key);
    }

    return node;
  }

  public onClick(event: NzFormatEmitEvent, tree: NzTreeComponent): void {
    const nzParentNodes: NzTreeNode[] = tree.getTreeNodes();
    nzParentNodes.forEach(n => {
      n.isSelectable = true;

      if (n.key === event.node.key) {
        n.isSelected = true;
      }
    });

    const selection: string = event.node.key;
    const enumName: string = selection.slice(0, selection.length - 1);
    const enumKey: string = selection.slice(-1);

    this[enumName] = +enumKey;

    if (event.node.parentNode) {
      /**
       * if child is selected parent must be unselectable
       */
      const parent: NzTreeNode = tree.getTreeNodeByKey(event.node.parentNode.key);
      parent.isSelectable = false;
      this.legalForm = +event.node.parentNode.key.slice(-1);
    } else {
      this.legalFormType = null;
    }

    tree.renderTree();

    this.legalNodeSelect.emit({
      legalForm: this.legalForm,
      legalFormType: this.legalFormType
    });
  }

  public resetSelection(tree: NzTreeComponent): void {
    this.legalForm = null;
    this.legalFormType = null;

    const nzParentNodes: NzTreeNode[] = tree.getSelectedNodeList();
    nzParentNodes.forEach(n => {
      n.isSelected = false;
    });
    tree.renderTree();

    this.legalNodeSelect.emit({
      legalForm: this.legalForm,
      legalFormType: this.legalFormType
    });
  }

  public onExpandChange(event: NzFormatEmitEvent): void {
    !event.node.isExpanded
      ? this.expandedNodeList
          .splice(this.expandedNodeList.findIndex(n => n === event.node.key), 1)
      : this.expandedNodeList.push(event.node.key);

    if (!this.expandedNodeList.length) {
      this.customClassName = 'expanded-0';
    } else if (this.expandedNodeList.length === 2) {
      this.customClassName = 'full-expanded';
    } else {
      this.customClassName = 'expanded-' + this.expandedNodeList[0];
    }
  }
}
