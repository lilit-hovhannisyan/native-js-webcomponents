import { NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getCompanyCategoryModalGirdConfig = (): NvGridConfig => {
  return {
    gridName: 'companyCategoryModalConfig',
    title: wording.system.users,
    sortBy: 'id',
    isSortAscending: true,
    disableHideColumns: true,
    hideRefreshButton: true,
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    rowSelectionType: NvGridRowSelectionType.Checkbox,
    paging: {
      pageNumber: 1,
      pageSize: 10
    },
    columns: [
      {
        key: 'id',
        visible: false,
      },
      {
        key: 'displayLabel',
        title: wording.general.displayName,
        width: 400,
        filter: {
          values: []
        }
      },
    ],
  };
};

