import { AfterContentInit, Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { forkJoin, of } from 'rxjs';
import { AbstractValueAccessor, makeProvider } from 'src/app/core/abstracts/value-accessor';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { wording } from 'src/app/core/constants/wording/wording';
import { IVessel } from 'src/app/core/models/resources/IVessel';
import { IVesselAccountingFormattedDates } from 'src/app/core/models/resources/IVesselAccounting';
import { VesselAccountingRepositoryService } from 'src/app/core/services/repositories/vessel-accounting-repository.service';
import { VesselRepositoryService } from 'src/app/core/services/repositories/vessel-repository.service';
import { SettingsService } from 'src/app/core/services/settings.service';
import { IVesselValues, VesselSelectModalComponent } from 'src/app/shared/components/vessel-select-modal/vessel-select-modal.component';

@Component({
  selector: 'nv-vessel-select-field',
  templateUrl: './vessel-select-field.component.html',
  styleUrls: ['./vessel-select-field.component.scss'],
  providers: [makeProvider(VesselSelectFieldComponent)],
})
export class VesselSelectFieldComponent extends AbstractValueAccessor implements AfterContentInit {
  public control: FormControl = new FormControl(null);
  public modalRef: NzModalRef;

  constructor(
    private settingsService: SettingsService,
    private nzModalService: NzModalService,
    private vesselRepo: VesselRepositoryService,
    private vesselAccountingRepo: VesselAccountingRepositoryService,
  ) {
    super();
  }

  public ngAfterContentInit(): void {
    this.handleInputDisable();
  }

  public openVesselsModal(): void {
    if (this.disabled) {
      return;
    }
    this.modalRef = this.nzModalService.create({
      ...DefaultModalOptions,
      nzTitle: wording.technicalManagement.vesselName[this.settingsService.language],
      nzContent: VesselSelectModalComponent,
      nzClosable: true,
      nzFooter: null,
      nzBodyStyle: { padding: '0' },
      nzComponentParams: {
        value: this.value,
        submit: this.handleSubmit,
        cancel: () => {
          this.onChange(this.value && this.value.vesselId ? this.value : null);
        },
      },
    });
  }

  public writeValue(value: IVesselValues) {
    super.writeValue(value);
    this.setInitialText(value);

    this.handleInputDisable();
  }

  private setInitialText(value: IVesselValues): void {
    const { vesselId, vesselAccountingId } = value || {} as IVesselValues;
    forkJoin([
      vesselId ? this.vesselRepo.fetchOne(vesselId, {}) : of(null),
      vesselAccountingId ? this.vesselAccountingRepo.fetchOne(vesselAccountingId, {}) : of(null),
    ]).subscribe(([vessel, vesselAccounting]) => this.setVesselNameText(
      vessel,
      vesselAccounting ? this.vesselAccountingRepo.formatFromUntil(vesselAccounting) : null,
    ));

  }

  private handleSubmit = (vessel: IVessel, vesselAccounting: IVesselAccountingFormattedDates): void => {
    this.value = vessel
      ? {
        vesselId: vessel && vessel.id,
        vesselAccountingId: vesselAccounting && vesselAccounting.id,
        vesselName: vesselAccounting && vesselAccounting.vesselName,
      }
      : null;

    this.setVesselNameText(vessel, vesselAccounting);
    this.closeModal();
  }

  private setVesselNameText(vessel: IVessel, vesselAccounting: IVesselAccountingFormattedDates): void {
    const vesselAccountingText = vesselAccounting && `${vesselAccounting.vesselNo} ${vesselAccounting.vesselName}`;
    this.control.setValue(vesselAccountingText || vessel && vessel.imo || null);
  }

  private closeModal(): void {
    if (this.modalRef) {
      this.modalRef.close();
      this.modalRef = null;
    }
  }

  private handleInputDisable(): void {
    if (this.disabled) {
      this.control.disable();
    } else {
      this.control.enable();
    }
  }
}
