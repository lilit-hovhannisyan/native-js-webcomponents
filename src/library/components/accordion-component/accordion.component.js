import {BaseComponent} from '../../core/base-component/base.component.js';
import {
  externalStyleBasePath,
  createElementWithAttributes,
  canElementBeRendered,
  removeStaticAtributesFromElNotToRender
} from '../../helpers/random.helpers.js';

export class AccordionComponent extends BaseComponent {

  constructor() {
    super(externalStyleBasePath + 'accordion-component/accordion.component.css');
  }

  render = () => {
    const data = eval(this.getAttribute('data'))();

    let html = '';
    data.forEach( (item, index) => {
      const open = +this.getAttribute('open') === index;
      html += `
        <accordion-item-component  header="${item.title}" body="${item.body}" open="${open}"></accordion-item-component>
      `;
    });

    const accordionConfig = {
      tagName: 'div',
      attr: {
        class: 'accordion-container'
      },
      html
    };

    const accordionEl = createElementWithAttributes(accordionConfig);

    removeStaticAtributesFromElNotToRender(this, ['data']);
    this.shadow.appendChild(accordionEl);
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }
}