import { Component, Input, OnInit } from '@angular/core';
import { getDatesRatesConfig } from 'src/app/core/constants/nv-grid-configs/dates-rates.config';
import { NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export interface IDateRate {
  date: string;
  rate: number;
}

@Component({
  selector: 'nv-dates-rates-table',
  templateUrl: './dates-rates-table.component.html',
  styleUrls: ['./dates-rates-table.component.scss']
})
export class DatesRatesTableComponent implements OnInit {
  @Input() public rowSelected: (row: IDateRate) => void;
  @Input() public rates: IDateRate[] = [];
  @Input() public selectable = false;
  public gridConfig: NvGridConfig = getDatesRatesConfig();

  public ngOnInit() {
    if (this.selectable) {
      this.gridConfig.rowSelectionType = NvGridRowSelectionType.RadioButton;
      this.gridConfig.hideSettingsButton = true;
      this.gridConfig.isSortAscending = false;
      this.gridConfig.buttons = [{
        actOnDoubleClick: true,
        func: (row: IDateRate) => {
          this.rowSelected(row);
        },
        description: wording.general.select,
        icon: 'select',
        actOnEnter: true,
        tooltip: wording.general.select,
        name: 'select'
      }];
    }
  }
}
