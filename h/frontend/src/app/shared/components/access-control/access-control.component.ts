import { Component, Input, OnInit } from '@angular/core';
import { Operations } from '../../../core/models/Operations';
import { AuthenticationService } from '../../../authentication/services/authentication.service';

@Component({
  selector: 'nv-access-control',
  templateUrl: './access-control.component.html',
  styleUrls: ['./access-control.component.scss']
})
export class AccessControlComponent implements OnInit {

  @Input() public zone: string;
  @Input() public operations: number = Operations.READ;

  public canAccess: boolean;

  constructor(private authService: AuthenticationService) { }

  public ngOnInit(): void {
    this.canAccess = this.authService.can(this.operations, this.zone);
  }
}
