import { wording } from '../../constants/wording/wording';
import { ISelectOption } from '../misc/selectOption';

export enum OPEX_TYPES {
  Insurance = 1,
  InitialSupply = 2,
  PersonnelCosts = 3,
  Maintenance = 4,
  OtherShipOperationCosts = 5
}

export const opexTypesOptions: ISelectOption[] = [
  { value: OPEX_TYPES.Insurance, displayLabel: wording.accounting.insurance },
  { value: OPEX_TYPES.InitialSupply, displayLabel: wording.accounting.initialSupply },
  { value: OPEX_TYPES.PersonnelCosts, displayLabel: wording.accounting.personalCosts },
  { value: OPEX_TYPES.Maintenance, displayLabel: wording.accounting.maintenance },
  { value: OPEX_TYPES.OtherShipOperationCosts, displayLabel: wording.accounting.otherCosts },
];
