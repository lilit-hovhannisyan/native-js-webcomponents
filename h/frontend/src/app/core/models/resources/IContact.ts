import { IResource } from './IResource';
import { IContactRole } from './IContactRole';
import { IContactType } from './IContactType';

export type ContactKey = number;

interface ISharedProps {
  companyId: number;
  contactPerson: string;
  roleId: number;
  typeId: number;
  value: string;
  remark?: string;
}

export interface IContactBase extends ISharedProps, IResource<number> { }

export interface IContactCreate extends ISharedProps { }

export interface IContactDTO extends IContactBase { }

export interface IContact extends IContactDTO { }

