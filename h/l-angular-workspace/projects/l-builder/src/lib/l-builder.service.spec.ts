import { TestBed } from '@angular/core/testing';

import { LBuilderService } from './l-builder.service';

describe('LBuilderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LBuilderService = TestBed.get(LBuilderService);
    expect(service).toBeTruthy();
  });
});
