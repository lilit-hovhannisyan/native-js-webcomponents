import { NvGridButtonsPosition, NvGridConfig, NvGridRowSelectionType } from 'nv-grid';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';
import { wordingGeneral } from '../wording/general';
import { Operations } from '../../models/Operations';
import { wordingAccounting } from '../wording/accounting';

export const getMandatorCostUnitsGridConfig: (
  actions: IGridConfigActions,
  canPerform: (operations: Operations) => boolean,
  isEditMode: () => boolean,
) => NvGridConfig = (
  actions: IGridConfigActions,
  canPerform: (operations: Operations) => boolean,
  isEditMode: () => boolean,
): NvGridConfig => {
  return ({
    gridName: 'mandatorCostUnitsGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    rowSelectionType: actions ? null : NvGridRowSelectionType.RadioButton,
    hideRefreshButton: true,
    sortBy: 'no',
    isSortAscending: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 50
    },
    columns: [
      {
        key: 'no',
        title: wordingAccounting.no,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'shortName',
        title: wordingGeneral.shortNameFull,
        width: 300,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: actions
      ? [
          {
            icon: 'delete',
            description: wordingGeneral.delete,
            name: 'deleteCostUnitCategory',
            tooltip: wordingGeneral.delete,
            actOnDoubleClick: false,
            func: actions.delete,
            hidden: !canPerform(Operations.DELETE),
            disabled: () => !isEditMode()
          }
        ]
      : [],
    toolbarButtons: actions
      ? [
          {
            title: wordingGeneral.add,
            tooltip: wordingGeneral.add,
            icon: 'plus-circle-o',
            func: actions.add,
            hidden: () => !canPerform(Operations.CREATE),
            disabled: () => !isEditMode(),
          }
        ]
      : [],
  });
};

