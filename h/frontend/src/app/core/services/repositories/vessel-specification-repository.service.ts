import { IVesselSpecificationDTO, IVesselSpecificationCreate } from './../../models/resources/IVesselSpecification';
import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { VesselKey } from '../../models/resources/IVessel';
import { RepositoryService } from '../repository.service';
import { IVesselSpecification } from '../../models/resources/IVesselSpecification';
import { VESSEL_TYPE } from '../../models/enums/vessel-type';
import { wording } from '../../constants/wording/wording';

export interface VesselSpecificationRepoParams extends RepoParams<IVesselSpecification> {
  vesselId?: VesselKey;
}

export const vesselTypes = [
  { value: VESSEL_TYPE.None, displayLabel: wording.chartering.vesselTypeNone },
  { value: VESSEL_TYPE.Bulkcarrier, displayLabel: wording.chartering.vesselTypeBulkcarrier },
  { value: VESSEL_TYPE.Container, displayLabel: wording.chartering.vesselTypeContainer },
  { value: VESSEL_TYPE.RoRo, displayLabel: wording.chartering.vesselTypeRoRo },
  { value: VESSEL_TYPE.ConRo, displayLabel: wording.chartering.vesselTypeConRo },
  { value: VESSEL_TYPE.MPP, displayLabel: wording.chartering.vesselTypeMPP },
];

@Injectable({
  providedIn: 'root'
})
export class VesselSpecificationRepositoryService extends AbstractRepository<
  IVesselSpecification,
  IVesselSpecificationDTO,
  IVesselSpecificationCreate,
  VesselSpecificationRepoParams,
  VesselKey
> {

  protected resourceType = 'VesselSpecification';
  public sortKey = 'from';
  public sortAsce = true;
  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl (repoParams: VesselSpecificationRepoParams): string {
    return (repoParams && repoParams.vesselId)
      ? `/api/vessel-specifications/${repoParams.vesselId}`
      : `/api/vessel-specifications`;
  }

  protected postProcess(item: IVesselSpecificationDTO): IVesselSpecification { return item; }
}
