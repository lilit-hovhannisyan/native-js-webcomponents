import { IWithActive } from './IWithActive';
import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithSystemFlag } from './IWithSystemFlag';
import { IWithLabel } from './IWithLabel';

export type VoucherTypeKey = number;

interface ISharedProps extends IWithLabels, IWithSystemFlag, IWithActive {
  useCase: string;
  abbreviation: string;
  isAutomaticNumbering: boolean;
}

export interface IVoucherTypeBase extends ISharedProps, IResource<number> { }

export interface IVoucherTypeCreate extends ISharedProps { }

export interface IVoucherTypeDTO extends IVoucherTypeBase { }

export interface IVoucherType extends IVoucherTypeDTO, IWithLabel { }

export interface IVoucherTypeToRow extends IVoucherType { }
// TODO create a better name for IVoucherTypeToRow, IVoucherTypeFull sounds good.
