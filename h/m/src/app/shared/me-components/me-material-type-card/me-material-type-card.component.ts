import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { BaseLoggerComponent } from '@features/tracking-system';
import { MeMaterialTypeCardI } from './me-material-type-card.interface';

@Component({
  selector: 'me-material-type-card',
  templateUrl: './me-material-type-card.component.html',
  styleUrls: ['./me-material-type-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeMaterialTypeCardComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  componentVersion: string = '0.0.1';

  @Input() dataModel!: MeMaterialTypeCardI;
  @Output() clickEvent: EventEmitter<void> = new EventEmitter();

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {}

  emitClick(): void {
    this.clickEvent.emit();
  }

  ngOnDestroy(): object {
    return super.ngOnDestroy();
  }
}
