import { Activity } from './svg/activity';
import { Airplay } from './svg/airplay';
import { AlertCircle } from './svg/alert-circle';
import { AlertOctagon } from './svg/alert-octagon';
import { AlertTriangle } from './svg/alert-triangle';
import { AlignCenter } from './svg/align-center';
import { AlignJustify } from './svg/align-justify';
import { AlignLeft } from './svg/align-left';
import { AlignRight } from './svg/align-right';
import { Anchor } from './svg/anchor';
import { Aperture } from './svg/aperture';
import { ArrowDownLeft } from './svg/arrow-down-left';
import { ArrowDownRight } from './svg/arrow-down-right';
import { ArrowDown } from './svg/arrow-down';
import { ArrowLeft } from './svg/arrow-left';
import { ArrowRight } from './svg/arrow-right';
import { ArrowUpLeft } from './svg/arrow-up-left';
import { ArrowUpRight } from './svg/arrow-up-right';
import { ArrowUp } from './svg/arrow-up';
import { AtSign } from './svg/at-sign';
import { Award } from './svg/award';
import { BarChart2 } from './svg/bar-chart-2';
import { BarChart } from './svg/bar-chart';
import { BatteryCharging } from './svg/battery-charging';
import { Battery } from './svg/battery';
import { BellOff } from './svg/bell-off';
import { Bell } from './svg/bell';
import { Bluetooth } from './svg/bluetooth';
import { Bold } from './svg/bold';
import { Book } from './svg/book';
import { Bookmark } from './svg/bookmark';
import { Box } from './svg/box';
import { Briefcase } from './svg/briefcase';
import { BusinessInformation } from './svg/business-information';
import { Button } from './svg/button';
import { Calendar } from './svg/calendar';
import { CameraOff } from './svg/camera-off';
import { Camera } from './svg/camera';
import { Cast } from './svg/cast';
import { Certificates } from './svg/certificates';
import { CheckCircle } from './svg/check-circle';
import { CheckSquare } from './svg/check-square';
import { Check } from './svg/check';
import { ChemicalsMaterialType } from './svg/chemicals-material-type';
import { ChevronDown } from './svg/chevron-down';
import { ChevronLeft } from './svg/chevron-left';
import { ChevronRight } from './svg/chevron-right';
import { ChevronUp } from './svg/chevron-up';
import { ChevronsDown } from './svg/chevrons-down';
import { ChevronsLeft } from './svg/chevrons-left';
import { ChevronsRight } from './svg/chevrons-right';
import { ChevronsUp } from './svg/chevrons-up';
import { Chrome } from './svg/chrome';
import { Circle } from './svg/circle';
import { Clipboard } from './svg/clipboard';
import { Clock } from './svg/clock';
import { CloudDrizzle } from './svg/cloud-drizzle';
import { CloudLightning } from './svg/cloud-lightning';
import { CloudOff } from './svg/cloud-off';
import { CloudRain } from './svg/cloud-rain';
import { CloudSnow } from './svg/cloud-snow';
import { Cloud } from './svg/cloud';
import { Codepen } from './svg/codepen';
import { Command } from './svg/command';
import { Community } from './svg/community';
import { CompanyIntroduction } from './svg/company-introduction';
import { Compass } from './svg/compass';
import { ComponentsMaterialType } from './svg/components-material-type';
import { ContactInformation } from './svg/contact-information';
import { Copy } from './svg/copy';
import { CopyFilled } from './svg/copy-filled';
import { CornerDownLeft } from './svg/corner-down-left';
import { CornerDownRight } from './svg/corner-down-right';
import { CornerLeftDown } from './svg/corner-left-down';
import { CornerLeftUp } from './svg/corner-left-up';
import { CornerRightDown } from './svg/corner-right-down';
import { CornerRightUp } from './svg/corner-right-up';
import { CornerUpLeft } from './svg/corner-up-left';
import { CornerUpRight } from './svg/corner-up-right';
import { Cpu } from './svg/cpu';
import { CreditCard } from './svg/credit-card';
import { Crop } from './svg/crop';
import { Crosshair } from './svg/crosshair';
import { Delete } from './svg/delete';
import { Disc } from './svg/disc';
import { DownAndFeatherMaterialType } from './svg/down-and-feather-material-type';
import { DenimMaterialType } from './svg/denim-material-type';
import { DigitalProfile } from './svg/digital-profile';
import { DownloadCloud } from './svg/download-cloud';
import { Download } from './svg/download';
import { Droplet } from './svg/droplet';
import { Edit2 } from './svg/edit-2';
import { Edit3 } from './svg/edit-3';
import { Edit } from './svg/edit';
import { Employees } from './svg/employees';
import { EmptyCertificates } from './svg/empty-certificates';
import { EmptyColleagues } from './svg/empty-colleagues';
import { EmptyCollections } from './svg/empty-collections';
import { EmptyDocuments } from './svg/empty-documents';
import { EmptyFavouriteSuppliers } from './svg/empty-favourite-suppliers';
import { EmptyFolders } from './svg/empty-folders';
import { EmptyMaterials } from './svg/empty-materials';
import { EmptyTrash } from './svg/empty-trash';
import { ExternalLink } from './svg/external-link';
import { EyeOff } from './svg/eye-off';
import { Eye } from './svg/eye';
import { Facebook } from './svg/facebook';
import { FacebookFilled } from './svg/facebook-filled';
import { Factory } from './svg/factory';
import { FastForward } from './svg/fast-forward';
import { Feather } from './svg/feather';
import { File } from './svg/file';
import { FileDocx } from './svg/file-docx';
import { FileJpg } from './svg/file-jpg';
import { FileOther } from './svg/file-other';
import { FilePdf } from './svg/file-pdf';
import { FilePng } from './svg/file-png';
import { FileThreedm } from './svg/file-threedm';
import { FileXlsx } from './svg/file-xlsx';
import { FileMinus } from './svg/file-minus';
import { FilePlus } from './svg/file-plus';
import { FileText } from './svg/file-text';
import { Film } from './svg/film';
import { Filter } from './svg/filter';
import { Flag } from './svg/flag';
import { Folder } from './svg/folder';
import { Github } from './svg/github';
import { Gitlab } from './svg/gitlab';
import { Globe } from './svg/globe';
import { Grid } from './svg/grid';
import { HardwareMaterialType } from './svg/hardware-material-type';
import { Hash } from './svg/hash';
import { Headphones } from './svg/headphones';
import { Heart } from './svg/heart';
import { HelpCircle } from './svg/help-circle';
import { Home } from './svg/home';
import { Image } from './svg/image';
import { Inbox } from './svg/inbox';
import { Info } from './svg/info';
import { InsoleMaterialType } from './svg/insole-material-type';
import { Instagram } from './svg/instagram';
import { Italic } from './svg/italic';
import { LaceMaterialType } from './svg/lace-material-type';
import { LastMaterialType } from './svg/last-material-type';
import { Layers } from './svg/layers';
import { Layout } from './svg/layout';
import { LeatherMaterialType } from './svg/leather-material-type';
import { LifeBuoy } from './svg/life-buoy';
import { Link2 } from './svg/link-2';
import { Link } from './svg/link';
import { List } from './svg/list';
import { Loader } from './svg/loader';
import { Lock } from './svg/lock';
import { LogIn } from './svg/log-in';
import { LogOut } from './svg/log-out';
import { Mail } from './svg/mail';
import { MapPin } from './svg/map-pin';
import { Map } from './svg/map';
import { Material } from './svg/material';
import { Maximize2 } from './svg/maximize-2';
import { Maximize } from './svg/maximize';
import { Menu } from './svg/menu';
import { MessageCircle } from './svg/message-circle';
import { MessageSquare } from './svg/message-square';
import { MessengerFilled } from './svg/messenger-filled';
import { MicOff } from './svg/mic-off';
import { Mic } from './svg/mic';
import { Minimize2 } from './svg/minimize-2';
import { Minimize } from './svg/minimize';
import { MinusCircle } from './svg/minus-circle';
import { MinusSquare } from './svg/minus-square';
import { Minus } from './svg/minus';
import { MoldTreatment } from './svg/mold-treatment';
import { MoldMaterialType } from './svg/mold-material-type';
import { Monitor } from './svg/monitor';
import { Moon } from './svg/moon';
import { MoreHorizontal } from './svg/more-horizontal';
import { MoreVertical } from './svg/more-vertical';
import { Move } from './svg/move';
import { Music } from './svg/music';
import { Navigation2 } from './svg/navigation-2';
import { Navigation } from './svg/navigation';
import { Octagon } from './svg/octagon';
import { OutsoleMaterialType } from './svg/outsole-material-type';
import { Package } from './svg/package';
import { Paperclip } from './svg/paperclip';
import { PauseCircle } from './svg/pause-circle';
import { Pause } from './svg/pause';
import { Percent } from './svg/percent';
import { PhoneCall } from './svg/phone-call';
import { PhoneForwarded } from './svg/phone-forwarded';
import { PhoneIncoming } from './svg/phone-incoming';
import { PhoneMissed } from './svg/phone-missed';
import { PhoneOff } from './svg/phone-off';
import { PhoneOutgoing } from './svg/phone-outgoing';
import { Phone } from './svg/phone';
import { PieChart } from './svg/pie-chart';
import { PlayCircle } from './svg/play-circle';
import { Play } from './svg/play';
import { PlusCircle } from './svg/plus-circle';
import { PlusSquare } from './svg/plus-square';
import { Plus } from './svg/plus';
import { Pocket } from './svg/pocket';
import { Power } from './svg/power';
import { Printer } from './svg/printer';
import { Radio } from './svg/radio';
import { RefreshCcw } from './svg/refresh-ccw';
import { RefreshCw } from './svg/refresh-cw';
import { Repeat } from './svg/repeat';
import { Rewind } from './svg/rewind';
import { RotateCcw } from './svg/rotate-ccw';
import { RotateCw } from './svg/rotate-cw';
import { Save } from './svg/save';
import { Scissors } from './svg/scissors';
import { Search } from './svg/search';
import { Season } from './svg/season';
import { Server } from './svg/server';
import { Settings } from './svg/settings';
import { Share2 } from './svg/share-2';
import { Share } from './svg/share';
import { Shield } from './svg/shield';
import { ShoppingCart } from './svg/shopping-cart';
import { Shuffle } from './svg/shuffle';
import { Sidebar } from './svg/sidebar';
import { SkipBack } from './svg/skip-back';
import { SkipForward } from './svg/skip-forward';
import { Skype } from './svg/skype';
import { Slack } from './svg/slack';
import { Slash } from './svg/slash';
import { Sliders } from './svg/sliders';
import { Smartphone } from './svg/smartphone';
import { Speaker } from './svg/speaker';
import { Square } from './svg/square';
import { Star } from './svg/star';
import { StopCircle } from './svg/stop-circle';
import { Sun } from './svg/sun';
import { Sunrise } from './svg/sunrise';
import { Sunset } from './svg/sunset';
import { Sustainability } from './svg/sustainability';
import { SyntheticMaterialType } from './svg/synthetic-material-type';
import { Tablet } from './svg/tablet';
import { Tag } from './svg/tag';
import { Target } from './svg/target';
import { TextilesKnitMaterialType } from './svg/textiles-knit-material-type';
import { TextilesWovenMaterialType } from './svg/textiles-woven-material-type';
import { Thermometer } from './svg/thermometer';
import { ThreadAndYarnMaterialType } from './svg/thread-and-yarn-material-type';
import { ThumbsDown } from './svg/thumbs-down';
import { ThumbsUp } from './svg/thumbs-up';
import { ToggleLeft } from './svg/toggle-left';
import { ToggleRight } from './svg/toggle-right';
import { Trash2 } from './svg/trash-2';
import { Trash } from './svg/trash';
import { TrendingDown } from './svg/trending-down';
import { TrendingUp } from './svg/trending-up';
import { Triangle } from './svg/triangle';
import { Tv } from './svg/tv';
import { Twitter } from './svg/twitter';
import { TwitterFilled } from './svg/twitter-filled';
import { Type } from './svg/type';
import { Umbrella } from './svg/umbrella';
import { Underline } from './svg/underline';
import { Unlock } from './svg/unlock';
import { UploadCloud } from './svg/upload-cloud';
import { Upload } from './svg/upload';
import { UserCheck } from './svg/user-check';
import { UserMinus } from './svg/user-minus';
import { UserPlus } from './svg/user-plus';
import { UserX } from './svg/user-x';
import { User } from './svg/user';
import { Users } from './svg/users';
import { VideoOff } from './svg/video-off';
import { Video } from './svg/video';
import { Visuals } from './svg/visuals';
import { Voicemail } from './svg/voicemail';
import { Volume1 } from './svg/volume-1';
import { Volume2 } from './svg/volume-2';
import { VolumeX } from './svg/volume-x';
import { Volume } from './svg/volume';
import { Watch } from './svg/watch';
import { WebbingRibbonMaterialType } from './svg/webbing-ribbon-material-type';
import { WifiOff } from './svg/wifi-off';
import { Wifi } from './svg/wifi';
import { Wind } from './svg/wind';
import { XCircle } from './svg/x-circle';
import { XSquare } from './svg/x-square';
import { X } from './svg/x';
import { Year } from './svg/year';
import { Zap } from './svg/zap';
import { ZipperMaterialType } from './svg/zipper-material-type';
import { ZoomIn } from './svg/zoom-in';
import { ZoomOut } from './svg/zoom-out';

export const allIcons = {
  Activity,
  Airplay,
  AlertCircle,
  AlertOctagon,
  AlertTriangle,
  AlignCenter,
  AlignJustify,
  AlignLeft,
  AlignRight,
  Anchor,
  Aperture,
  ArrowDownLeft,
  ArrowDownRight,
  ArrowDown,
  ArrowLeft,
  ArrowRight,
  ArrowUpLeft,
  ArrowUpRight,
  ArrowUp,
  AtSign,
  Award,
  BarChart2,
  BarChart,
  BatteryCharging,
  Battery,
  BellOff,
  Bell,
  Bluetooth,
  Bold,
  Book,
  Bookmark,
  Box,
  Briefcase,
  BusinessInformation,
  Button,
  Calendar,
  CameraOff,
  Camera,
  Cast,
  Certificates,
  CheckCircle,
  CheckSquare,
  Check,
  ChemicalsMaterialType,
  ChevronDown,
  ChevronLeft,
  ChevronRight,
  ChevronUp,
  ChevronsDown,
  ChevronsLeft,
  ChevronsRight,
  ChevronsUp,
  Chrome,
  Circle,
  Clipboard,
  Clock,
  CloudDrizzle,
  CloudLightning,
  CloudOff,
  CloudRain,
  CloudSnow,
  Cloud,
  Codepen,
  Command,
  Community,
  CompanyIntroduction,
  Compass,
  ComponentsMaterialType,
  ContactInformation,
  Copy,
  CopyFilled,
  CornerDownLeft,
  CornerDownRight,
  CornerLeftDown,
  CornerLeftUp,
  CornerRightDown,
  CornerRightUp,
  CornerUpLeft,
  CornerUpRight,
  Cpu,
  CreditCard,
  Crop,
  Crosshair,
  Delete,
  Disc,
  DownAndFeatherMaterialType,
  DenimMaterialType,
  DigitalProfile,
  DownloadCloud,
  Download,
  Droplet,
  Edit2,
  Edit3,
  Edit,
  Employees,
  EmptyCertificates,
  EmptyColleagues,
  EmptyCollections,
  EmptyDocuments,
  EmptyFavouriteSuppliers,
  EmptyFolders,
  EmptyMaterials,
  EmptyTrash,
  ExternalLink,
  EyeOff,
  Eye,
  Facebook,
  FacebookFilled,
  Factory,
  FastForward,
  Feather,
  File,
  FileDocx,
  FileJpg,
  FileMinus,
  FileOther,
  FilePdf,
  FilePlus,
  FilePng,
  FileText,
  FileThreedm,
  FileXlsx,
  Film,
  Filter,
  Flag,
  Folder,
  Github,
  Gitlab,
  Globe,
  Grid,
  HardwareMaterialType,
  Hash,
  Headphones,
  Heart,
  HelpCircle,
  Home,
  Image,
  Inbox,
  Info,
  InsoleMaterialType,
  Instagram,
  Italic,
  LaceMaterialType,
  LastMaterialType,
  Layers,
  Layout,
  LeatherMaterialType,
  LifeBuoy,
  Link2,
  Link,
  List,
  Loader,
  Lock,
  LogIn,
  LogOut,
  Mail,
  MapPin,
  Map,
  Material,
  Maximize2,
  Maximize,
  Menu,
  MessageCircle,
  MessageSquare,
  MessengerFilled,
  MicOff,
  Mic,
  Minimize2,
  Minimize,
  MinusCircle,
  MinusSquare,
  Minus,
  MoldTreatment,
  MoldMaterialType,
  Monitor,
  Moon,
  MoreHorizontal,
  MoreVertical,
  Move,
  Music,
  Navigation2,
  Navigation,
  Octagon,
  OutsoleMaterialType,
  Package,
  Paperclip,
  PauseCircle,
  Pause,
  Percent,
  PhoneCall,
  PhoneForwarded,
  PhoneIncoming,
  PhoneMissed,
  PhoneOff,
  PhoneOutgoing,
  Phone,
  PieChart,
  PlayCircle,
  Play,
  PlusCircle,
  PlusSquare,
  Plus,
  Pocket,
  Power,
  Printer,
  Radio,
  RefreshCcw,
  RefreshCw,
  Repeat,
  Rewind,
  RotateCcw,
  RotateCw,
  Save,
  Scissors,
  Search,
  Season,
  Server,
  Settings,
  Share2,
  Share,
  Shield,
  ShoppingCart,
  Shuffle,
  Sidebar,
  SkipBack,
  SkipForward,
  Skype,
  Slack,
  Slash,
  Sliders,
  Smartphone,
  Speaker,
  Square,
  Star,
  StopCircle,
  Sun,
  Sunrise,
  Sunset,
  Sustainability,
  SyntheticMaterialType,
  Tablet,
  Tag,
  Target,
  TextilesKnitMaterialType,
  TextilesWovenMaterialType,
  Thermometer,
  ThreadAndYarnMaterialType,
  ThumbsDown,
  ThumbsUp,
  ToggleLeft,
  ToggleRight,
  Trash2,
  Trash,
  TrendingDown,
  TrendingUp,
  Triangle,
  Tv,
  Twitter,
  TwitterFilled,
  Type,
  Umbrella,
  Underline,
  Unlock,
  UploadCloud,
  Upload,
  UserCheck,
  UserMinus,
  UserPlus,
  UserX,
  User,
  Users,
  VideoOff,
  Video,
  Visuals,
  Voicemail,
  Volume1,
  Volume2,
  VolumeX,
  Volume,
  Watch,
  WebbingRibbonMaterialType,
  WifiOff,
  Wifi,
  Wind,
  XCircle,
  XSquare,
  X,
  Year,
  Zap,
  ZipperMaterialType,
  ZoomIn,
  ZoomOut
};
