export interface MeInfoAlertI {
  mode: 'regular' | 'loading';
  title: string;
  content: string;
  buttonText?: string;
}
