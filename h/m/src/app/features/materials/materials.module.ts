import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';
import { MeComponentLoadingModule } from '@shared-directives/me-component-loader/me-component-loader.module';
import { MeMaterialCardModule } from '@shared-components/me-material-card/me-material-card.module';
import { MeScrollToBottomModule } from '@shared/me-directives/me-scroll-to-bottom.module';

import { MeSearchBarModule } from '@shared/me-components/me-search-bar/me-search-bar.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MeNewCollectionCardModule } from '@shared/me-components/me-new-collection-card/me-new-collection-card.module';

import { MeEmptyContentModule } from '@shared/me-components/me-empty-content/me-empty-content.module';
import { MeAdvancedFilterModule } from '@shared/me-components/me-advanced-filter/me-advanced-filter.module';
import { MeMaterialTypeCardModule } from '@shared/me-components/me-material-type-card/me-material-type-card.module';
import { MeAttributeModule } from '@shared/me-components/me-attribute/me-attribute.module';

import { MaterialsRoutingModule } from './materials-routing.module';
import { MaterialsComponent } from './materials.component';
import { MePopupStatusChangeModule } from '@shared/me-components/me-popup-status-change/me-popup-status-change.module';
import { MeIntroductionMessageModule } from '@shared/me-components/me-introduction-message/me-introduction-message.module';

@NgModule({
  declarations: [MaterialsComponent],
  imports: [
    CommonModule,
    MaterialsRoutingModule,
    SharedModule,
    MeMaterialCardModule,
    MeComponentLoadingModule,
    MeSearchBarModule,
    IconsModule,
    MeNewCollectionCardModule,
    MeEmptyContentModule,
    MeScrollToBottomModule,
    MeAdvancedFilterModule,
    MeMaterialTypeCardModule,
    MeAttributeModule,
    MePopupStatusChangeModule,
    MeIntroductionMessageModule
  ]
})
export class MaterialsModule {}
