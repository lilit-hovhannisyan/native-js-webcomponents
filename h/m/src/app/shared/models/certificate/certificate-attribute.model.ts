export class CertificateAttributeModel {
  link: string = '';
  name: string = '';
  logoDefault: string = '';
  group: string = '';
  description: string = '';
  type: string = '';
}
