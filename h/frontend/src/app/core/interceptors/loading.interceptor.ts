import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpEventType } from '@angular/common/http';
import { EHttpRequest } from '../models/http/EHttpRequest';
import { Observable } from 'rxjs';
import { skipWhile, finalize } from 'rxjs/operators';
import { LoadingService } from '../services/loading.service';
import { RequestsService } from '../services/requests.service';

/**
 * Sets/Clears the loading spinner on screen
 */
@Injectable()
export class LoadingInterceptor implements HttpInterceptor {

  constructor(
    private loadingService: LoadingService,
    private requestsService: RequestsService
  ) { }

  public intercept(request: EHttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request?.options?.loadingIndicatorHidden) {
      return next.handle(request);
    }

    this.requestsService.addRequest(request);
    this.loadingService.isLoadingSubject.next(true);

    return next.handle(request).pipe(
      finalize(() => this.requestsService.removeRequest(request)),
      skipWhile((event: HttpEvent<any>) => event.type === HttpEventType.Sent),
    );
  }
}
