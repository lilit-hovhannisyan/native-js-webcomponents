import { NVFormControl } from 'src/app/core/classes/NVFormControl';

export const companyBasicFormControlsNames: string[] = [
  'salutationId',
  'titleId',
  'isPrivatePerson',
  'firstName',
  'lastName',
  'companyName',
  'taxNo',
  'vatId',
  'isActive',
  'logoId',
  'vatIdValid',
  'mandator',
  'debitorCreditor',
  'legalForm',
  'legalFormType',
  'isLegalDepartmentResponsible',
];

export interface ICompanyBasicFormControls {
  salutationId: NVFormControl;
  titleId: NVFormControl;
  isPrivatePerson: NVFormControl;
  firstName: NVFormControl;
  lastName: NVFormControl;
  companyName: NVFormControl;
  taxNo: NVFormControl;
  vatId: NVFormControl;
  isActive: NVFormControl;
  logoId: NVFormControl;
  vatIdValid: NVFormControl;
  mandator: NVFormControl;
  debitorCreditor: NVFormControl;
  legalForm: NVFormControl;
  legalFormType: NVFormControl;
  isLegalDepartmentResponsible: NVFormControl;
}
