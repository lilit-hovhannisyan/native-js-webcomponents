import { Component, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup } from '@angular/forms';
import { NvGridConfig } from 'nv-grid';
import { AutocompleteComponent } from 'src/app/shared/components/autocomplete/autocomplete.component';

@Component({
  selector: 'nv-stream-autocomplete-input-wrapper',
  templateUrl: './stream-autocomplete-input-wrapper.component.html',
  styleUrls: ['./stream-autocomplete-input-wrapper.component.scss']
})
export class StreamAutocompleteInputWrapperComponent {
  public wording = wording;

  @Input() public transformKey: string;
  @Input() public resourceToTransform: any[] = [];
  @Input() public transformfilters: object = {};
  @Input() public transformUniqe: string;

  @Input() public resource: any[] = [];
  @Input() public filters: object = {};
  @Input() public modalGridConfig: NvGridConfig;

  @Input() public propertyToDisplayModifier: (value: any) => any;
  @Input() public properities: string[];

  @Input() public form: FormGroup;
  @Input() public controlName: string;
  @Input() public label: string;
  @Input() public autofocus: boolean;
  @Input() public setNullIfNotFound = false;

  @Input() public setId = true;
  @Input() public displayOneProperty: boolean;
  @Input() public openListOnInit: boolean;
  @Input() public showErrMessages = false;
  @Input() public title: string;

  /**
   * Used to pass image property name that has the path
   * of the image to show
   */
  @Input() public imageProp?: string;

  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();
  @Output() public valueChange = new EventEmitter<any>();

  @ViewChild('autocomplete', { static: true }) public autocomplete: AutocompleteComponent;

  public focus() {
    this.autocomplete.focus();
  }
}
