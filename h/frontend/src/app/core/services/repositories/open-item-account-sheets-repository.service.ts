import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import {
  IOpenItemAccountSheets,
  IOpenItemAccountSheetsDTO,
  IOpenItemAccountSheetsCreate,
  OpenItemAccountSheetsKey
} from '../../models/resources/IOpenItemAccountSheets';
import { MandatorKey } from '../../models/resources/IMandator';
import { ACCOUNTING_TYPES } from '../../models/enums/accounting-types';
import { BookingAccountKey } from '../../models/resources/IBookingAccount';

export interface OpenItemAccountSheetsQueryParams {
  mandatorId?: MandatorKey;
  bookingAccountId?: BookingAccountKey;
  accountingType?: ACCOUNTING_TYPES;
}

export interface OpenItemAccountSheetsRepoParams extends RepoParams<IOpenItemAccountSheets> {
  queryParams?: OpenItemAccountSheetsQueryParams;
}

@Injectable({
  providedIn: 'root'
})
export class OpenItemAccountSheetsRepositoryService extends AbstractRepository<
IOpenItemAccountSheets,
IOpenItemAccountSheetsDTO,
IOpenItemAccountSheetsCreate,
OpenItemAccountSheetsRepoParams,
OpenItemAccountSheetsKey
> {
  protected resourceType = 'OpenItemAccountSheets';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(params: OpenItemAccountSheetsRepoParams): string {
    return '/api/openitems';
  }

  public postProcess(item: IOpenItemAccountSheetsDTO): IOpenItemAccountSheets { return item; }

}
