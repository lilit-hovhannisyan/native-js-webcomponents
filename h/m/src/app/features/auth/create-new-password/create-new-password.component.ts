import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Subscription } from 'rxjs';

import { AuthStoreService } from '@core-services/auth-store.service';
import { PatternValidator } from '@core-validators/custom-validators';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';

import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';

import { AuthWebService } from '../auth.web-service';

@Component({
  selector: 'me-create-new-password',
  templateUrl: './create-new-password.component.html',
  styleUrls: ['./create-new-password.component.scss']
})
export class CreateNewPasswordComponent implements OnInit, AfterViewInit, OnDestroy {
  passwordForm!: FormGroup;
  token: string = '';
  reset: boolean = false;
  resetPasswordSent: boolean = false;

  routeSubscription!: Subscription;
  resetPasswordSubscription!: Subscription;
  getSweetAlertSubscription!: Subscription;

  checkPasswordStrengthOpen: boolean = false;
  showPassword: boolean = false;

  @ViewChild('passwordField') passwordField!: ElementRef;

  get newPasswordControl(): AbstractControl | null {
    return this.passwordForm.get('newPassword');
  }

  get confirmPasswordControl(): AbstractControl | null {
    return this.passwordForm.get('confirmPassword');
  }

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authWebService: AuthWebService,
    private sweetAlertService: MeSweetAlertService,
    private authStoreService: AuthStoreService
  ) {}

  ngOnInit(): void {
    this.routeSubscription = this.route.params.subscribe((params: Params) => {
      this.token = params.token;
    });

    this.passwordForm = new FormGroup(
      {
        newPassword: new FormControl('', [
          Validators.required,
          // check whether the entered password has a number
          PatternValidator(/\d/, { hasNumber: true }),
          //  check whether the entered password has upper case letter
          PatternValidator(/[A-Z]/, { hasCapitalCase: true }),
          //  check whether the entered password has a lower-case letter
          PatternValidator(/[a-z]/, { hasSmallCase: true }),
          //  check whether the entered password has a special character
          PatternValidator(/[\!#$%&\\(\)@\_\.]/, { hasSpecialCharacters: true }),
          //  Has a minimum length of 8 characters
          Validators.minLength(8)
        ]),
        confirmPassword: new FormControl('', [Validators.required])
      },
      {
        validators: (control: AbstractControl) =>
          control.get('newPassword')?.value !== control.get('confirmPassword')?.value ? { noPassswordMatch: true } : null
      }
    );

    this.getSweetAlertSubscription = this.sweetAlertService.getDataBackFromSweetAlert().subscribe((data: MeSweetAlertI) => {
      if (data && data.confirmed && data.type.name === 'input') {
        this.authWebService.requestPasswordReset({ userID: data.userInput || '' }).subscribe(
          () => {
            this.authStoreService.setIsForgotPasswordEmailSent(true);
            this.router.navigate(['auth/forgot-password']);
          },
          () => {
            this.authStoreService.setIsForgotPasswordEmailSent(false);
          }
        );
      } else {
        this.passwordField.nativeElement.focus();
        this.passwordForm.reset();
        Object.keys(this.passwordForm.controls).forEach((key) => {
          this.passwordForm.controls[key].setErrors(null);
        });
      }
    });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.passwordField.nativeElement.focus();
    });
  }

  resetPassword(): void {
    this.reset = true;
    this.resetPasswordSubscription = this.authWebService
      .requestPasswordResetWithToken({
        newPassword: this.newPasswordControl?.value,
        token: this.token
      })
      .subscribe(
        () => {
          this.reset = false;
          this.resetPasswordSent = true;
        },
        (error: ErrorResponseI) => {
          if (error.status === 401) {
            this.sweetAlertService.openMeSweetAlert({
              icon: 'alert-triangle',
              title: error.error.title,
              message: error.error.details,
              mode: 'warning',
              type: {
                name: MeSweetAlertTypeEnum.input,
                buttons: {
                  submit: 'Submit'
                }
              },
              inputLabel: 'Username or email'
            });
          }

          this.reset = false;
          this.resetPasswordSent = false;
        }
      );
  }

  goToLogin(): void {
    this.router.navigate(['auth/login']);
  }

  checkPasswordStrength(): void {
    this.checkPasswordStrengthOpen = !this.checkPasswordStrengthOpen;
  }

  changeShowPasswordState(): void {
    this.showPassword = !this.showPassword;
  }

  ngOnDestroy(): void {
    if (this.resetPasswordSubscription) {
      this.resetPasswordSubscription.unsubscribe();
    }

    if (this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }

    if (this.getSweetAlertSubscription) {
      this.getSweetAlertSubscription.unsubscribe();
    }
  }
}
