import { Pipe, PipeTransform } from '@angular/core';
import { SettingsService } from 'src/app/core/services/settings.service';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';

@Pipe({
  name: 'labelKey'
})
export class LabelKeyPipe implements PipeTransform {

  constructor(private settingsService: SettingsService) {

  }
  public transform() {
    return getLabelKeyByLanguage(this.settingsService.language);
  }
}
