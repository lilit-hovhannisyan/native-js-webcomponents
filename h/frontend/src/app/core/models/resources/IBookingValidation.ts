import { IBooking } from './IBooking';
import { INavidoHTTPResponseError } from '../../interceptors/http-error-handler';

export interface IBookingValidation {
  booking: IBooking;
  errors: INavidoHTTPResponseError[];
}
