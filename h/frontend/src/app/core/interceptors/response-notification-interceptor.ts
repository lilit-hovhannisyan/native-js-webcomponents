import { HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { skipWhile, tap } from 'rxjs/operators';
import { EHttpRequest } from '../models/http/EHttpRequest';
import { NotificationType } from '../models/INotification';
import { IDataResponseBody } from '../models/responses/IDataResponseBody';
import { NotificationsService } from '../services/notification.service';
import { CREATED, NO_CONTENT } from 'http-status-codes';
import { wording } from 'src/app/core/constants/wording/wording';

/**
 * Displays Success notifications on Update/Delete
 */
@Injectable()
export class ResponseNotificationInterceptor implements HttpInterceptor {
  constructor(private notificationService: NotificationsService) { }

  public intercept(request: EHttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      skipWhile((event: HttpEvent<any>) => event.type === HttpEventType.Sent),
      tap((response: HttpResponse<IDataResponseBody<any>>) => {
        if (
          response.status >= 200
          && response.status < 227
          && !request.shouldHideNotification()
        ) {
          if (request.method === 'POST' && response.status === CREATED) {
            this.notificationService.notify(
              NotificationType.Success,
              wording.general.success,
              wording.general.createdSuccess
            );
          } else if (request.method === 'DELETE' && response.status === NO_CONTENT) {
            this.notificationService.notify(
              NotificationType.Success,
              wording.general.success,
              wording.general.deletedSuccess
            );
          } else if (request.method === 'PUT') {
            this.notificationService.notify(
              NotificationType.Success,
              wording.general.success,
              wording.general.updatedSuccess
            );
          }
        }
      }),
    );
  }
}
