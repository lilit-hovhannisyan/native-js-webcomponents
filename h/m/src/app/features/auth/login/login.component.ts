import { LoggingService } from './../../tracking-system/services/logging.service';
import { Component, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthStoreService } from '@core-services/auth-store.service';
import { WindowRef } from '@core-providers/window-ref.provider';

// import { UserProfileModel } from '@shared-models/user-profile/user-profile.model';
import { ME_PRIVACY_NOTICE_URL } from '@shared/constants';

import { AuthWebService } from '../auth.web-service';

@Component({
  selector: 'me-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent {
  @ViewChild('loginForm') loginForm!: NgForm;
  username: string = '';
  password: string = '';

  constructor(
    private authStoreService: AuthStoreService,
    private authWebService: AuthWebService,
    private router: Router,
    private windowRef: WindowRef,
    private loggingService: LoggingService
  ) {}

  performLogin(): void {
    const loginData: string = `username=${this.username}&password=${encodeURIComponent(this.password)}`;

    this.authWebService.login(loginData).subscribe((data) => {
      this.loggingService.appendToHeaders('Authorization', `Bearer ${data.jwt}`);
      this.router.navigate(['backoffice']);

      // this.authWebService.getCurrentUser().subscribe((response: UserProfileModel) => {
      //   this.authStoreService.user = response.user;
      //   this.authStoreService.company = response.company ? response.company : null;
      //   this.authStoreService.permissions = response.permissions ? response.permissions : null;
      //   this.router.navigate(['']);
      // });
    });
  }

  goToPrivacyNotice(event: MouseEvent): void {
    if (event.target instanceof HTMLAnchorElement) {
      this.windowRef.openInNewWindow(ME_PRIVACY_NOTICE_URL);
    }
  }

  goToForgotPassword(): void {
    this.authStoreService.setIsForgotPasswordEmailSent(false);
    this.router.navigate(['auth', 'forgot-password']);
  }
}
