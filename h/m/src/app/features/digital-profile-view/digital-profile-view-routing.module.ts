import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DigitalProfileViewComponent } from './digital-profile-view.component';

const routes: Routes = [{ path: '', component: DigitalProfileViewComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DigitalProfileViewRoutingModule {}
