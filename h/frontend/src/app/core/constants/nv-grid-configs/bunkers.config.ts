import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { wording } from 'src/app/core/constants/wording/wording';
import { IResource } from 'src/app/core/models/resources/IResource';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'BunkersGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'displayLabel',
        title: wording.general.name,
        width: 350,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'code',
        title: wording.general.code,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isActive',
        title: wording.accounting.active,
        width: 65,
        isSortable: true,
        dataType: NvColumnDataType.Boolean,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editBunker',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (entity: IResource<number>) => actions.edit(entity.id),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteBunker',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.UPDATE),
        func: (entity: IResource<number>) => actions.delete(entity),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.UPDATE),
        func: () => actions.add()
      }
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
