import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'lavash-radio-button',
  templateUrl: 'radio-button.component.html',
  styleUrls: ['radio-button.component.less']
})

export class RadioButtonComponent {
  @Input() uncheckedIcon: string;
  @Input() checkedIcon: string;

  @Input() option: RadioButtonOption = {
    key: 'no-key',
    value: '',
    selected: false,
  };

  @Output() selectOption = new EventEmitter<RadioButtonOption>();

  onSelectOption() {
    this.option.selected = !this.option.selected;
    this.selectOption.emit(this.option);
  }
}

export interface RadioButtonOption {
  key: string;
  value: string;
  selected?: boolean;
}
