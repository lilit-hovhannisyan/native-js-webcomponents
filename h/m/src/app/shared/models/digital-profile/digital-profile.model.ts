import { Type } from 'class-transformer';
import { DigitalProfileAttributesExtendedModel } from './digital-profile-attribites-extended.model';
// import { CompanyModel } from '@shared-models/company/company.model';
// import { AddressModel } from '../address/address.model';
import { DigitalProfileAttributesModel } from './digital-profile-attributes.model';
import { BaseModel } from '../base-model/base-model';

export class DigitalProfileModel extends BaseModel {
  // @Type(() => AddressModel)
  // address: AddressModel = new AddressModel();
  // @Type(() => CompanyModel)
  // company: CompanyModel = new CompanyModel();
  @Type(() => DigitalProfileAttributesModel)
  attributes: DigitalProfileAttributesModel = new DigitalProfileAttributesModel();
  @Type(() => DigitalProfileAttributesExtendedModel)
  attributesExtended: DigitalProfileAttributesExtendedModel = new DigitalProfileAttributesExtendedModel();
  creatorOID: string = '';
}
