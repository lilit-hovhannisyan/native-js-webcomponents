import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, forkJoin, ReplaySubject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { VesselKey } from 'src/app/core/models/resources/IVessel';
import { CommissionRepositoryService } from 'src/app/core/services/repositories/commission-repository.service';
import { VesselRepositoryService } from 'src/app/core/services/repositories/vessel-repository.service';
import { VesselSpecificationRepositoryService } from 'src/app/core/services/repositories/vessel-specification-repository.service';
import { VesselsHelperService } from './vessels.helper.service';
import { VesselSettingsRepositoryService } from 'src/app/core/services/repositories/vessel-settings-repository.service';
import { VesselRegisterRepositoryService } from 'src/app/core/services/repositories/vessel-registeries-repository.service';

@Component({
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
})
export class VesselDetailViewComponent implements OnInit, OnDestroy {
  private componentDestroyed$ = new ReplaySubject<boolean>(1);
  private vesselId: VesselKey;
  public title: string;
  public sitemap = sitemap;
  public wording = wording;
  public isCreationMode = false;

  constructor(
    private vesselRepo: VesselRepositoryService,
    private route: ActivatedRoute,
    private router: Router,
    private commissionsRepo: CommissionRepositoryService,
    private vesselSettingsRepo: VesselSettingsRepositoryService,
    private specificationRepo: VesselSpecificationRepositoryService,
    private vesselsHelperService: VesselsHelperService,
    private vesselRegitriesRepo: VesselRegisterRepositoryService,
  ) { }

  public ngOnInit() {
    this.route.params.subscribe(({ id }) => {
      this.vesselId = id;
      this.isCreationMode = id === 'new';
      if (this.isCreationMode) { return; }

      this.vesselsHelperService.generateTitle(+id);

      this.vesselsHelperService.editViewTitle$.pipe(
        takeUntil(this.componentDestroyed$)
      ).subscribe(title => {
        this.title = title;
      });

      this.checkForMissingData();
    });
  }

  public tabDisabler = (node: SitemapNode) => {
    const basicNode = sitemap.chartering.children.misc.children.vessels.children.id.children.basic;
    // in creationMode just enable the basicTab
    return node === basicNode || !this.isCreationMode;
  }

  public routeBack = () => {
    this.router.navigate([
      url(sitemap.chartering.children.misc.children.vessels)
    ]);
  }

  private checkForMissingData() {
    forkJoin([
      this.specificationRepo.fetchAll({ queryParams: { vesselId: this.vesselId } }),
      this.vesselRepo.fetchAccountingsByVesselId(this.vesselId),
      this.commissionsRepo.fetchAll({ queryParams: { vesselId: this.vesselId } }),
      this.vesselSettingsRepo.fetchAll({ queryParams: { vesselId: this.vesselId } }),
      this.vesselRegitriesRepo.fetchAll({ queryParams: { vesselId: this.vesselId }}),
    ]).pipe(
      tap(([specifications, accountings, commissions, settings, registrations]) => {
        this.vesselsHelperService.isSpecificationsMissing$.next(specifications.length === 0);
        this.vesselsHelperService.isAccountingsMissing$.next(accountings.length === 0);
        this.vesselsHelperService.isCommissionsMissing$.next(commissions.length === 0);
        this.vesselsHelperService.isSettingsMissing$.next(settings.filter(s => s.isActive).length === 0);
        this.vesselsHelperService.isRegistrationMissing$.next(!registrations.length);
      }),
    ).subscribe();
  }

  public isDataMissing = (node: SitemapNode): BehaviorSubject<boolean> => {
    switch (node.zone) {
      case 'commissions':
        return this.vesselsHelperService.isCommissionsMissing$;

      case 'specifications':
        return this.vesselsHelperService.isSpecificationsMissing$;

      case 'accountings':
        return this.vesselsHelperService.isAccountingsMissing$;

      case 'settings':
        return this.vesselsHelperService.isSettingsMissing$;

      case 'registrations':
        return this.vesselsHelperService.isRegistrationMissing$;
    }
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
