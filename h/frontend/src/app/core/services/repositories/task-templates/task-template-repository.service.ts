import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { ITaskTemplate, ITaskTemplateCreate, ITaskTemplateDTO, TaskTemplateKey } from 'src/app/core/models/resources/Task-Templates/ITaskTemplate';
import { TaskTemplateCategoryKey } from 'src/app/core/models/resources/Task-Templates/ITaskTemplateCategory';
import { RepositoryService } from '../../repository.service';


export interface TaskTemplateRepoParams extends RepoParams<ITaskTemplate> {
  queryParams?: {
    categoryId: TaskTemplateCategoryKey;
  };
}

@Injectable({
  providedIn: 'root'
})
export class TaskTemplateRepositoryService extends AbstractRepository<
ITaskTemplate,
ITaskTemplateDTO,
ITaskTemplateCreate,
TaskTemplateRepoParams,
TaskTemplateKey
> {
  protected resourceType = 'TaskTemplate';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: ITaskTemplateDTO): ITaskTemplate => item;

  public getBaseUrl = () => '/api/task-templates';
}
