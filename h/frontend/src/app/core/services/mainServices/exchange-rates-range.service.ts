import { Injectable } from '@angular/core';
import { IExchangeRate } from '../../models/resources/IExchangeRate';
import { BehaviorSubject } from 'rxjs';
import * as moment from 'moment';
/**
 * Used mainly for caching exchange rates in ranges { from, until, rates }[]
 */

export interface IExchangeRatesRange {
  from: string; // ISO String
  until: string; // ISO string
  currencyFrom: string; // ISO Code
  currencyTo: string; // ISO Code
  rates: IExchangeRate[];
}

@Injectable({
  providedIn: 'root'
})
export class ExchangeRatesRangeService {
  private ratesRanges = new BehaviorSubject<IExchangeRatesRange[]>([]);

  /**
   *
   * @param from date iso string
   * @param until date iso string
   * @param currencyFrom currency iso code
   * @param currencyTo  currency iso code
   * @returns If found `IExchangeRate[]`, otherwise `[]`
   */
  public getRatesInRange(
    from: string,
    until: string,
    currencyFrom: string,
    currencyTo: string
  ): IExchangeRate[] {
    const foundRates = this.ratesRanges
      .getValue()
      .find(
        r =>
          r.currencyFrom === currencyFrom &&
          r.currencyTo === currencyTo &&
          r.from === from &&
          r.until === until
      );
    return foundRates ? foundRates.rates : [];
  }

  /**
   *
   * @param from date iso string
   * @param until date iso string
   * @param currencyFrom aka localCurrency, currency iso code
   * @param currencyTo  aka foreign currency, currency iso code
   * @param rates  list of IExchangeRate
   * @returns `void`
   */
  public setRatesInRange(
    from: string,
    until: string,
    currencyFrom: string,
    currencyTo: string,
    rates: IExchangeRate[]
  ): void {
    if (
      moment(from).isValid() &&
      moment(until).isValid() &&
      currencyFrom &&
      currencyTo &&
      rates
    ) {
      this.ratesRanges.next([
        ...this.ratesRanges.value,
        { from, until, currencyFrom, currencyTo, rates }
      ]);
    }
  }

  /**
   * Clears cached `IExchangeRatesRange[]`
   */
  public clearCache = () => this.ratesRanges.next([]);
}
