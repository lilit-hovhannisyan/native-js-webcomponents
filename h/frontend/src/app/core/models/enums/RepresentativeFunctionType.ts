import { wording } from '../../constants/wording/wording';
import { IWording } from '../resources/IWording';

export type RepresentativeFunctionMap<T = IWording> = Map<RepresentativeFunctionType, T>;

export enum RepresentativeFunctionType {
  ManagingDirectorGeneralPartner = 1,
  ManagingPartner = 2,
  AuthorizedRepresentativeLimitedPartnership = 3,
  AuthorizedRepresentativeGeneralPartnership = 4,
  Liquidator = 5,
  InsolvencyAdministrator = 6,
  ManagingDirector = 7,
  AuthorizedRepresentative = 8,
  MemberOfManagementBoard = 9,
  MemberOfSupervisoryBoard = 10,
  ManagingDirectorManagingPartner = 11,
  AuthorizedRepresentativeManagingPartner = 12,
  Director = 13,
  CompanySecretary = 14,
}

export const representativeFunctionTypeOptions: RepresentativeFunctionMap = new Map([
  [
    RepresentativeFunctionType.ManagingDirectorGeneralPartner,
    wording.system.managingDirectorGeneralPartner
  ],
  [
    RepresentativeFunctionType.ManagingPartner,
    wording.system.managingPartner
  ],
  [
    RepresentativeFunctionType.AuthorizedRepresentativeLimitedPartnership,
    wording.system.authorizedLimitedPartnership
  ],
  [
    RepresentativeFunctionType.AuthorizedRepresentativeGeneralPartnership,
    wording.system.authorizedGeneralPartnership
  ],
  [
    RepresentativeFunctionType.Liquidator,
    wording.system.liquidator
  ],
  [
    RepresentativeFunctionType.InsolvencyAdministrator,
    wording.system.insolvencyAdministrator
  ],
  [
    RepresentativeFunctionType.ManagingDirector,
    wording.system.managingDirector
  ],
  [
    RepresentativeFunctionType.AuthorizedRepresentative,
    wording.system.authorizedRepresentative
  ],
  [
    RepresentativeFunctionType.MemberOfManagementBoard,
    wording.system.memberOfManagementBoard
  ],
  [
    RepresentativeFunctionType.MemberOfSupervisoryBoard,
    wording.system.memberOfSupervisoryBoard
  ],
  [
    RepresentativeFunctionType.ManagingDirectorManagingPartner,
    wording.system.managingDirectorManagingPartner
  ],
  [
    RepresentativeFunctionType.AuthorizedRepresentativeManagingPartner,
    wording.system.authorizedManagingPartner
  ],
  [ RepresentativeFunctionType.Director, wording.system.director ],
  [ RepresentativeFunctionType.CompanySecretary, wording.system.companySecretary ],
]);

export const getFunctionTypesForLimitedPartnership = (): RepresentativeFunctionMap => {
  const functionTypes: RepresentativeFunctionMap = representativeFunctionTypeOptions;
  const availableFunctionTypes: RepresentativeFunctionMap = new Map([
    [
      RepresentativeFunctionType.ManagingDirectorGeneralPartner,
      functionTypes.get(RepresentativeFunctionType.ManagingDirectorGeneralPartner),
    ],
    [
      RepresentativeFunctionType.ManagingPartner,
      functionTypes.get(RepresentativeFunctionType.ManagingPartner),
    ],
    [
      RepresentativeFunctionType.AuthorizedRepresentativeLimitedPartnership,
      functionTypes
        .get(RepresentativeFunctionType.AuthorizedRepresentativeLimitedPartnership),
    ],
    [
      RepresentativeFunctionType.AuthorizedRepresentativeGeneralPartnership,
      functionTypes
        .get(RepresentativeFunctionType.AuthorizedRepresentativeGeneralPartnership),
    ],
    [
      RepresentativeFunctionType.Liquidator,
      functionTypes.get(RepresentativeFunctionType.Liquidator),
    ],
    [
      RepresentativeFunctionType.InsolvencyAdministrator,
      functionTypes.get(RepresentativeFunctionType.InsolvencyAdministrator),
    ],
  ]);

  return availableFunctionTypes;
};

export const getFunctionTypesForLimitedLiability = (): RepresentativeFunctionMap => {
  const functionTypes: RepresentativeFunctionMap = representativeFunctionTypeOptions;
  const availableFunctionTypes: RepresentativeFunctionMap = new Map([
    [
      RepresentativeFunctionType.ManagingDirectorGeneralPartner,
      functionTypes.get(RepresentativeFunctionType.ManagingDirectorGeneralPartner),
    ],
    [
      RepresentativeFunctionType.ManagingDirector,
      functionTypes.get(RepresentativeFunctionType.ManagingDirector),
    ],
    [
      RepresentativeFunctionType.AuthorizedRepresentativeGeneralPartnership,
      functionTypes
        .get(RepresentativeFunctionType.AuthorizedRepresentativeGeneralPartnership),
    ],
    [
      RepresentativeFunctionType.AuthorizedRepresentative,
      functionTypes.get(RepresentativeFunctionType.AuthorizedRepresentative),
    ],
    [
      RepresentativeFunctionType.Liquidator,
      functionTypes.get(RepresentativeFunctionType.Liquidator),
    ],
    [
      RepresentativeFunctionType.InsolvencyAdministrator,
      functionTypes.get(RepresentativeFunctionType.InsolvencyAdministrator),
    ],
  ]);

  return availableFunctionTypes;
};

export const getFunctionTypesForStockCorporation = (): RepresentativeFunctionMap => {
  const functionTypes: RepresentativeFunctionMap = representativeFunctionTypeOptions;
  const availableFunctionTypes: RepresentativeFunctionMap = new Map([
    [
      RepresentativeFunctionType.MemberOfManagementBoard,
      functionTypes.get(RepresentativeFunctionType.MemberOfManagementBoard),
    ],
    [
      RepresentativeFunctionType.MemberOfSupervisoryBoard,
      functionTypes.get(RepresentativeFunctionType.MemberOfSupervisoryBoard),
    ],
    [
      RepresentativeFunctionType.AuthorizedRepresentative,
      functionTypes.get(RepresentativeFunctionType.AuthorizedRepresentative),
    ],
    [
      RepresentativeFunctionType.Liquidator,
      functionTypes.get(RepresentativeFunctionType.Liquidator),
    ],
    [
      RepresentativeFunctionType.InsolvencyAdministrator,
      functionTypes.get(RepresentativeFunctionType.InsolvencyAdministrator),
    ],
  ]);

  return availableFunctionTypes;
};

export const getFunctionTypesForCivilLawPartnership = (): RepresentativeFunctionMap => {
  const functionTypes: RepresentativeFunctionMap = representativeFunctionTypeOptions;
  const availableFunctionTypes: RepresentativeFunctionMap = new Map([
    [
      RepresentativeFunctionType.ManagingDirectorManagingPartner,
      functionTypes.get(RepresentativeFunctionType.ManagingDirectorManagingPartner),
    ],
    [
      RepresentativeFunctionType.AuthorizedRepresentativeManagingPartner,
      functionTypes
        .get(RepresentativeFunctionType.AuthorizedRepresentativeManagingPartner),
    ],
    [
      RepresentativeFunctionType.Liquidator,
      functionTypes.get(RepresentativeFunctionType.Liquidator),
    ],
    [
      RepresentativeFunctionType.InsolvencyAdministrator,
      functionTypes.get(RepresentativeFunctionType.InsolvencyAdministrator),
    ],
  ]);

  return availableFunctionTypes;
};

export const getFunctionTypesForForeignCompany = (): RepresentativeFunctionMap<string> => {
  const functionTypes: RepresentativeFunctionMap = representativeFunctionTypeOptions;
  const availableFunctionTypes: RepresentativeFunctionMap<string> = new Map([
    [
      RepresentativeFunctionType.Director,
      functionTypes.get(RepresentativeFunctionType.Director).en,
    ],
    [
      RepresentativeFunctionType.ManagingDirector,
      functionTypes.get(RepresentativeFunctionType.ManagingDirector).en,
    ],
    [
      RepresentativeFunctionType.CompanySecretary,
      functionTypes.get(RepresentativeFunctionType.CompanySecretary).en,
    ],
    [
      RepresentativeFunctionType.Liquidator,
      functionTypes.get(RepresentativeFunctionType.Liquidator).en,
    ],
    [
      RepresentativeFunctionType.InsolvencyAdministrator,
      functionTypes.get(RepresentativeFunctionType.InsolvencyAdministrator).en,
    ],
  ]);

  return availableFunctionTypes;
};
