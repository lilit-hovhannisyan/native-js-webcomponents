export class CompanyAttributesModel {
  companyName: string = '';
  email: string = '';
  name: string = '';
  phone: string = '';
  primaryLocation: string = '';
  isDataImported: boolean = false;
  logo: string = '';
  // tslint:disable-next-line:no-any
  [key: string]: any;
}
