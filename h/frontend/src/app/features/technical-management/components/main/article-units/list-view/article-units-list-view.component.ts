import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { getGridConfig } from '../../../../../../core/constants/nv-grid-configs/article-units.config';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { ArticleUnitsRepositoryService } from 'src/app/core/services/repositories/article-units-repository.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { IArticleUnit } from 'src/app/core/models/resources/IArticleUnit';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
@Component({
  selector: 'nv-article-units-list-view',
  templateUrl: './article-units-list-view.component.html',
  styleUrls: ['./article-units-list-view.component.scss']
})
export class ArticleUnitsListViewComponent implements OnInit {
  public dataSource$: Observable<IArticleUnit[]>;
  public generalUnitsGridConfig: NvGridConfig;
  constructor(
    private generalUnitsRepo: ArticleUnitsRepositoryService,
    private confirmationService: ConfirmationService,
    public gridConfigService: GridConfigService,
    private authService: AuthenticationService,
    private router: Router,
  ) { }

  public ngOnInit() {
    const actions: IGridConfigActions = {
      edit: (generalUnit: IArticleUnit) =>
        this.router.navigate([`${this.router.url}/${generalUnit.id}`]),
      delete: this.deleteGeneralUnit,
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    this.generalUnitsGridConfig = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );

    this.generalUnitsRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.generalUnitsRepo.getStream();

  }

  private deleteGeneralUnit = (generalUnit: IArticleUnit) => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: generalUnit.displayLabel },
    }).subscribe(confirmed => confirmed && this.generalUnitsRepo.delete(generalUnit.id, {}).subscribe());
  }
}
