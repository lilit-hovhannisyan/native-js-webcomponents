import { ThumbnailDocument } from '@shared-models/file-upload/file-upload.model';
import { MeButtonI } from '../me-button/me-button.interface';

export interface MeUploadImageI {
  title?: string;
  thumbnail?: string;
  thumbnailProperties?: ThumbnailDocument;
  uploadImageButton?: MeButtonI;
  isThumbnailStatusInvalid?: boolean;
  acceptPhotoExtensions?: string;
  supportedThumbnailFiles?: string;
  maxUploadSize?: string;
  metaDataToSend?: string;
  percentageDone?: number;
  showProgress?: boolean;
}
