import { NvColumnDataType, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';

export const getVesselRegisterAutocompleteGridConfig = (
  actions: IGridConfigActions = {},
): NvGridConfig => {
  return ({
    gridName: 'VesselRegisterAutocomplete',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'formattedFrom',
        title: wording.general.from,
        dataType: NvColumnDataType.String,
        width: 250,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'formattedUntil',
        title: wording.general.until,
        dataType: NvColumnDataType.String,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        actOnDoubleClick: true,
        func: row => actions.select(row),
        description: wording.autocomplete.selectRow,
        icon: 'select',
        actOnEnter: true,
        hidden: actions.select === undefined,
        tooltip: wording.general.select,
        name: 'select'
      }
    ],
    toolbarButtons: [],
  });
};
