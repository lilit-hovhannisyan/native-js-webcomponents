import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Language } from 'src/app/core/models/language';
import { SettingsService } from './settings.service';
import { NzModalService, ModalOptions } from 'ng-zorro-antd/modal';
import { wording } from 'src/app/core/constants/wording/wording';
import { IWording, ReplacementSubjects } from 'src/app/core/models/resources/IWording';
import { ModalConfirmationContentComponent } from 'src/app/shared/components/modal-confirmation-content/modal-confirmation-content.component';
import { take } from 'rxjs/operators';

export interface ConfirmationParams {
  title?: IWording;
  wording?: IWording;
  /**
   * when the value of `okButtonWording` or `cancelButtonWording` is `null`
   * 1. the button will be hidden,
   * when it's value is falsy, but not `null`
   * 2. the button will be shown `yes`/`no` appropriately
   */
  okButtonWording?: IWording | null;
  cancelButtonWording?: IWording | null;
  subjects?: ReplacementSubjects;
  className?: string;
}

type NzModalType = 'confirm' | 'info' | 'success' | 'warning' | 'create' | 'error';

const modalTypes = Object.freeze({
  confirm: 'confirm',
  info: 'info',
  success: 'success',
  warning: 'warning',
  create: 'create',
  error: 'error',
  delete: 'confirm',
});

@Injectable({
  providedIn: 'root'
})
export class ConfirmationService {
  private language: Language;
  constructor(
    private modalService: NzModalService,
    private settingsService: SettingsService
  ) {
    this.settingsService.languageSubject.subscribe(lang => {
      this.language = lang;
    });
  }

  private openModal(params: ConfirmationParams, type: NzModalType) {
    const confirmed: Subject<boolean> = new Subject<boolean>();

    setTimeout(() => {
      const previouslyFocusedElement = document.activeElement as HTMLElement;
      previouslyFocusedElement.blur();

      setTimeout(() => {
        previouslyFocusedElement.focus();
        this.modalService[type as any]({
          ...this.options(params, confirmed, type),
        });
      });
    });

    return confirmed.asObservable().pipe(take(1));
  }

  private options(params: ConfirmationParams, confirmed: Subject<boolean>, type: NzModalType): ModalOptions {
    const { okButtonWording, cancelButtonWording } = params;
    const nzCancelText = cancelButtonWording ? cancelButtonWording[this.language] : wording.general.no[this.language];
    const nzOkText = okButtonWording ? okButtonWording[this.language] : wording.general.yes[this.language];

    return ({
      nzClassName: `${params?.className || ''} nv-${type}-modal`,
      nzWidth: 'fit-content',
      nzClosable: true,
      nzOkText: okButtonWording === null ? null : nzOkText,
      nzCancelText: cancelButtonWording === null ? null : nzCancelText,
      nzTitle: params.title
        ? params.title[this.language]
        : wording.general.confirmation[this.language],
      nzContent: ModalConfirmationContentComponent,
      nzComponentParams: {
        wording: params.wording,
        subjects: params.subjects,
      },
      nzOnOk: () => confirmed.next(true),
      nzOnCancel: () => confirmed.next(false),
    });
  }
  public confirm(params: ConfirmationParams): Observable<boolean> {
    return this.openModal(params, 'confirm');
  }

  public warning(params: ConfirmationParams): Observable<boolean> {
    return this.openModal(params, 'warning');
  }

  public info(params: ConfirmationParams): Observable<boolean> {
    return this.openModal(params, 'info');
  }

  public success(params: ConfirmationParams): Observable<boolean> {
    return this.openModal(params, 'success');
  }

  public create(params: ConfirmationParams): Observable<boolean> {
    return this.openModal(params, 'create');
  }

  public error(params: ConfirmationParams): Observable<boolean> {
    return this.openModal(params, 'error');
  }
}
