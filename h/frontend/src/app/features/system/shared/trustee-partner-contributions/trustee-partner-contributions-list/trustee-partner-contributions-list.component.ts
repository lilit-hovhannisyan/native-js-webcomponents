import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Input, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { tap, map, takeUntil, switchMap } from 'rxjs/operators';
import { Observable, ReplaySubject } from 'rxjs';
import * as moment from 'moment';
import { ITrusteePartnerContribution, ITrusteePartnerContributionFull } from 'src/app/core/models/resources/ITrusteePartnerContribution';
import { wording } from 'src/app/core/constants/wording/wording';
import { CompanyKey } from 'src/app/core/models/resources/ICompany';
import { TrusteePartnerContributionMainService } from 'src/app/core/services/mainServices/trustee-partner-contributions-main.service';
import { COMPANY_CONTRIBUTION_TYPES } from '../enums/company-contributions';

@Component({
  selector: 'nv-trustee-partner-contributions-list',
  templateUrl: './trustee-partner-contributions-list.component.html',
  styleUrls: ['./trustee-partner-contributions-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TrusteePartnerContributionsListComponent implements OnInit, OnDestroy {
  @Input() public contributorType: COMPANY_CONTRIBUTION_TYPES;
  @Input() public companyId: CompanyKey;

  public wording = wording;
  public selectedFrom: string;
  public allContributions: ITrusteePartnerContributionFull[];
  public contributionsUniqueDates: string[];
  public nearestDate: string;
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

  constructor(
    private trusteeContributionMainService: TrusteePartnerContributionMainService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectionRef: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    this.getContributionsStream()
      .pipe(
        takeUntil(this.componentDestroyed$),
        tap(() => {
          this.navigate2NearestMovementWhenUnselected();
        }),
        switchMap(() => {
          return this.activatedRoute.queryParams.pipe(
            tap(params => {
              this.selectedFrom = params.from;
              this.nearestDate = this.getNearestDate();
              this.changeDetectionRef.detectChanges();
            })
          );
        }),
      ).subscribe();
  }

  /**
   * If `selectedFrom` is invalid or doesn't exist - check first movement.
   */
  private async navigate2NearestMovementWhenUnselected(): Promise<boolean> {
    if (
      !this.contributionsUniqueDates?.length
      || this.selectedFrom
      && this.contributionsUniqueDates.indexOf(this.selectedFrom) !== -1
    ) {
      return Promise.resolve(false);
    }

    this.selectedFrom = this.getNearestDate();
    const navigationDone = await this.router.navigate([], {
      queryParams: { from: this.selectedFrom },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });

    if (navigationDone) {
      /**
       * The ChangeDetection isn't detecting changes automatically
       * after navigation.
       */
      this.changeDetectionRef.detectChanges();
    }
    return navigationDone;
  }

  // add and edit
  public movementChangeHandler(contribution: ITrusteePartnerContribution): void {
    this.selectMovement(contribution.from);
    this.navigate2NearestMovementWhenUnselected();
  }

  public movementDeleteHandler(contribution: ITrusteePartnerContribution): void {
    this.navigate2NearestMovementWhenUnselected();
  }

  private async selectMovement(from: string): Promise<boolean> {
    const clearedFrom = from;

    if (this.contributionsUniqueDates.indexOf(clearedFrom) === -1) {
      return Promise.resolve(false);
    }

    const navigationDone: boolean = await this.router.navigate([], {
      queryParams: { from: clearedFrom },
      queryParamsHandling: 'merge',
      relativeTo: this.activatedRoute,
    });

    if (navigationDone) {
      this.selectedFrom = clearedFrom;
      this.changeDetectionRef.detectChanges();
    }
    return Promise.resolve(navigationDone);
  }

  private getContributionsStream(): Observable<ITrusteePartnerContributionFull[]> {
    return this.trusteeContributionMainService.getStream({
      queryParams: { cid: this.companyId },
      contributorType: this.contributorType,
    }).pipe(
      map(contributions => this.trusteeContributionMainService.sortByDate(contributions)),
      tap(allContributions => {
        this.allContributions = allContributions;
        this.contributionsUniqueDates = allContributions?.length
          ? this.getUniqueDates(this.allContributions)
          : [moment().utc().startOf('day').toISOString()];
      }),
    );
  }

  private getUniqueDates(contributions: ITrusteePartnerContribution[]): string[] {
    const dates: string[] = contributions.map(s => {
      const from: string = s.from;
      /**
       * The backend gives date with 'Z' at the end on time of creation
       * and with fetch gives us the same dates without 'Z'.
       * Since we are using getStream which is inserting the new created entity
       * (which was given by backend(POST response)) on stream data, we need to have
       * all dates in same format.
       */
      return from[from.length - 1] === 'Z' ? from : `${from}Z`;
    });
    const datesSet: Set<string> = new Set<string>(dates);

    return Array.from(datesSet).sort((a, b) => moment(a).isAfter(b) ? -1 : +1);
  }

  /**
   * According to AC: Should select nearest date to today.
   * 1. Select current date(today) if it exists
   * 2. Otherwise select past-nearest date if it exists.
   * 3. Otherwise select future-nearest date.
   */
  private getNearestDate(): string | null {
    if (!this.contributionsUniqueDates?.length) {
      return null;
    }

    return this.contributionsUniqueDates.reduce((result, date) => {
      if (moment().isSame(result, 'd')) {
        return result;
      }
      if (moment().isSame(date, 'd')) {
        return date;
      }

      const resultDiff: number = moment().diff(result);
      const dateDiff: number = moment().diff(date);
      if (resultDiff < 0 && dateDiff > 0) {
        return date;
      }
      if (resultDiff > 0 && dateDiff < 0) {
        return result;
      }
      return Math.abs(resultDiff) < Math.abs(dateDiff) ? result : date;
    });
  }

  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
