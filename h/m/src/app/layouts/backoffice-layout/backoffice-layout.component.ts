import { HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NavigationError, NavigationStart, Router } from '@angular/router';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { TranslateService } from '@ngx-translate/core';

import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { DefinitionsWebService } from '@shared/services/definitions.web-service';
import { BaseUploadModel } from '@shared/models/file-upload/file-upload.model';
import { MeFileUploadTypes } from '@shared/me-file-upload-types';
import { GlobalService } from '@core-services/global.service';
import { MeSidebarI } from '@shared/me-components/me-sidebar/me-sidebar.interface';
import { MeBreadcrumbI } from '@shared/me-components/me-breadcrumb/me-breadcrumb.interface';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';
import { MeBreadcrumbActionI } from '@shared/me-components/me-breadcrumb/me-breadcrumb-action.interface';

@Component({
  selector: 'me-backoffice-layout',
  templateUrl: './backoffice-layout.component.html',
  styleUrls: ['./backoffice-layout.component.scss']
})
export class BackofficeLayoutComponent implements OnInit {
  private _unsubscribeOnDestroy$: Subject<void> = new Subject();
  private subs = new SubSink();
  sidebar!: MeSidebarI;
  isLoading: boolean = false;

  breadcrumb: MeBreadcrumbI = {
    actions: []
  };
  i18nStrings: { [key: string]: string } = {
    logout: '',
    profileSettings: '',
    userGuide: '',
    importingData: '',
    importingDataDescription: '',
    importSettings: '',
    importSettingsDescription: '',
    editDigitalProfile: ''
  };

  constructor(
    private definitionsWebService: DefinitionsWebService,
    private definitionsStoreService: DefinitionsStoreService,
    public globalService: GlobalService,
    private translate: TranslateService,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private crf: ChangeDetectorRef,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this.subs.sink = this.translate.get(Object.keys(this.i18nStrings)).subscribe((translations) => {
      Object.keys(this.i18nStrings).forEach((key) => {
        this.i18nStrings[key] = translations[key];
      });
      this.sidebar = {
        isExpanded: true,
        isCollapsible: false,
        company: {
          editButton: null,
          editButtonTitle: this.i18nStrings.editDigitalProfile,
          logoUrl: '/assets/images/me-logo.png',
          title: 'Back Office'
        },
        logoUrl: '/assets/images/me-logo.png',
        navItems: [
          {
            id: 'backoffice/companies',
            title: 'Companies',
            icon: 'briefcase',
            activated: false
          },
          {
            id: 'backoffice/users-groups',
            title: 'Users/Groups',
            icon: 'users',
            activated: false
          }
        ]
      };
    });

    this.subs.sink = this.breadcrumbSupportService.breadcrumbActions$.subscribe((actions: MeBreadcrumbActionI[]) => {
      this.breadcrumb = { ...this.breadcrumb, actions };
    });

    this.getDefinitionsAndProperties();
    this.updateActiveSidebarItem(this._router.url);
    this._router.events.pipe(takeUntil(this._unsubscribeOnDestroy$)).subscribe((e) => {
      if (e instanceof NavigationStart) {
        this.updateActiveSidebarItem(e.url);
        this.breadcrumbSupportService.breadcrumbActions = [];
      } else if (e instanceof NavigationError) {
        this.breadcrumbSupportService.breadcrumbActions = [];
      }
    });
    this.globalService.isActivatedLoader$.pipe(takeUntil(this._unsubscribeOnDestroy$)).subscribe((active) => {
      this.isLoading = active;
      this.crf.detectChanges();
    });
  }

  getDefinitionsAndProperties(): void {
    const getDefinitions = this.definitionsWebService.getAllDefinitions().pipe(takeUntil(this._unsubscribeOnDestroy$));
    const getProperties = this.definitionsWebService.getDefinitionProperties().pipe(takeUntil(this._unsubscribeOnDestroy$));

    forkJoin([getDefinitions, getProperties]).subscribe(
      ([definitionsResponse, propertiesResponse]) => {
        if (definitionsResponse) {
          this.definitionsStoreService.domains = definitionsResponse.domains;
        }
        if (definitionsResponse.enums) {
          this.definitionsStoreService.enums = definitionsResponse.enums;
        }

        const thumbnailProperties: BaseUploadModel | undefined = propertiesResponse.fileUpload.find((property) => {
          return property.type === MeFileUploadTypes.thumbnail;
        });
        if (thumbnailProperties) {
          this.definitionsStoreService.thumbnailProperties = thumbnailProperties;
        }
        const documentProperties: BaseUploadModel | undefined = propertiesResponse.fileUpload.find((property) => {
          return property.type === MeFileUploadTypes.document;
        });
        if (documentProperties) {
          this.definitionsStoreService.documentProperties = documentProperties;
        }
        const certificateProperties: BaseUploadModel | undefined = propertiesResponse.fileUpload.find((property) => {
          return property.type === MeFileUploadTypes.certificate;
        });
        if (certificateProperties) {
          this.definitionsStoreService.certificateProperties = certificateProperties;
        }
        const logoProperties: BaseUploadModel | undefined = propertiesResponse.fileUpload.find((property) => {
          return property.type === MeFileUploadTypes.logo;
        });
        if (logoProperties) {
          this.definitionsStoreService.logoProperties = logoProperties;
        }

        const coverImageProperties: BaseUploadModel | undefined = propertiesResponse.fileUpload.find((property) => {
          return property.type === MeFileUploadTypes.coverImage;
        });
        if (coverImageProperties) {
          this.definitionsStoreService.coverImageProperties = coverImageProperties;
        }

        this.definitionsStoreService.dataLoaded = true;
      },
      (error: ErrorResponseI | HttpErrorResponse) => {
        console.error(error);
      }
    );
  }

  updateActiveSidebarItem(url: string): void {
    const id: string = url.slice(1, url.length);
    // console.log(`id`, id);
    this.sidebar = {
      ...this.sidebar,
      navItems: this.sidebar.navItems.map((item) => {
        return { ...item, activated: item.id === id };
      })
    };
  }

  sidebarEvents(event: { eventName: string; payload?: string }): void {
    console.log(`event`, event);
    switch (event.eventName) {
      case 'selectedItemId':
        this._router.navigate([event.payload]);
        // TODO do something with selectedItem
        break;
      case 'clickedEditButton':
        this._router.navigate(['digital-profile/edit']);
        break;
      case 'goToDigitalProfile':
        this._router.navigate(['digital-profile/view']);
        break;
    }
  }

  toggleSidebar(): void {
    this.sidebar = { ...this.sidebar, isExpanded: !this.sidebar.isExpanded };
  }

  goToHome(): void {
    this._router.navigate(['/backoffice']);
  }
}
