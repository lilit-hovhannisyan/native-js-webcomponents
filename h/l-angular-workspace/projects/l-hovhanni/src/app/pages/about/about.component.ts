import { Component } from '@angular/core';
import {ph, phLink} from '../../../../../shared-layer/l-hovhanni/dal/portfolio.model';

@Component ({
  selector : 'app-about',
  templateUrl : './about.component.html',
  styleUrls : ['./about.component.less']
})

export class AboutComponent {
  photographers = ph;
  phlinks = phLink;

  objectKeys = Object.keys;
}
