import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import * as moment from 'moment-timezone';
import { map, filter } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';

/**
 * Used to inform components that website is in maintenance, also stores the date of next maintenance
*/
@Injectable({
  providedIn: 'root'
})
export class MaintenanceService {
  public inMaintenanceMode = new BehaviorSubject<boolean>(false);
  public nextMaintenance = new BehaviorSubject<string>(null);

  constructor(
    private authService: AuthenticationService,
  ) {
    this.authService.isLoggedInSubject$
      .pipe(filter(isLoggedIn => !isLoggedIn))
      .subscribe(() => this.nextMaintenance.next(null));
  }

  public getNextMaintenanceDate(localTime = true): Observable<string> {
    return this.nextMaintenance.asObservable()
      .pipe(
        filter(d => moment(d).isValid() || d === undefined),
        map(date => {
          if (date === undefined) {
            return '';
          }
          return localTime
            ? moment(date).utc(localTime).format()
            : moment(date).add(moment().utcOffset(), 'minutes').format();
        })
      );
  }

  public setNextMaintenanceDate(date: string): void {
    this.nextMaintenance.next(date);
  }
}
