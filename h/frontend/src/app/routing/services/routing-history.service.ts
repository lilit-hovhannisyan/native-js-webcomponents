import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { BrowserTitleService } from './browser-title.service';

@Injectable({ providedIn: 'root' })
export class RoutingHistoryService {
  private history: string[] = [];

  constructor(
    private router: Router,
    private browserTitleService: BrowserTitleService,
  ) { }

  /**
   * @see https://angular.io/guide/router#router-events
   */
  public loadRouting(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const { url } = event;
        this.history.push(url);
        this.browserTitleService.setTitleByUrl(url);
      }
    });
  }

  public getHistory(): string[] {
    return this.history;
  }

  public getPreviousLocation(): string {
    return this.history[this.history.length - 2];
  }
}
