import { BaseLoggerComponent } from '@features/tracking-system';
import { ChangeDetectionStrategy, Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { MeButtonI } from './me-button.interface';
@Component({
  selector: 'me-button',
  templateUrl: './me-button.component.html',
  styleUrls: ['./me-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeButtonComponent extends BaseLoggerComponent implements OnInit {
  componentVersion: string = '0.0.1';
  @Input() dataModel!: MeButtonI;
  @Output() clickEvent: EventEmitter<void> = new EventEmitter();

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {}

  onClick(e: Event): void {
    e.preventDefault();
    this.clickEvent.emit();
  }
}
