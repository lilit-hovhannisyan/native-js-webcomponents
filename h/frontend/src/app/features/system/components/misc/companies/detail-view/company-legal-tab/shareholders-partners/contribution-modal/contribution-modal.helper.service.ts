
import { Injectable } from '@angular/core';
import { FormGroup, ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { ReplaySubject, combineLatest, Observable } from 'rxjs';
import { startWith, distinctUntilChanged, takeUntil, tap, map } from 'rxjs/operators';
import { ContributionMainService } from 'src/app/core/services/mainServices/company-contributions-main.service';
import { CompanyKey } from 'src/app/core/models/resources/ICompany';
import { DECIMAL_14_2 } from 'src/app/core/constants/min-max-values';
import { ICompanyPartnerContributionFull, AmountChangeType } from 'src/app/core/models/resources/ICompanyPartnerContribution';
import { getGreaterDate } from 'src/app/core/helpers/general';
import { SettingsService } from 'src/app/core/services/settings.service';
import { ITrusteePartnerContribution } from 'src/app/core/models/resources/ITrusteePartnerContribution';
import { wording } from 'src/app/core/constants/wording/wording';
import { setTranslationSubjects, safeFloatSum, safeFloatReduction } from 'src/app/shared/helpers/general';
import { ICompanyLegalEntity } from 'src/app/core/models/resources/ICompanyLegalEntity';

export const decimalValidators: ValidatorFn[] = [
  Validators.max(DECIMAL_14_2),
  Validators.maxDecimalDigits(2),
];

type ISubContributionsSumMap = Map<CompanyKey, number>;

/**
 *  Note about settimeouts
 *  If we change the control directly without setTimeout
 *  all later checks here will be done on changed value
*/
@Injectable()
export class ContributionModalHelperService {
  private latestContributions: Map<CompanyKey, ICompanyPartnerContributionFull>;
  private trustorsContributions: ITrusteePartnerContribution[];
  private subParticipantsContributions: ITrusteePartnerContribution[];
  private allContributions: ICompanyPartnerContributionFull[] = [];
  private legalEntity: ICompanyLegalEntity;
  private increaseShareholdersContributions: ICompanyPartnerContributionFull[] = [];
  private reduceShareholdersContributions: ICompanyPartnerContributionFull[] = [];
  private subContributionsSumMap: ISubContributionsSumMap;
  private form: FormGroup;
  private componentDestroyed$: ReplaySubject<unknown>;
  private companyId: CompanyKey;
  private isManagingPartnersVisible: boolean;
  private initialReduceManagingPartnerValue: boolean;

  private increasePartnerIdControl: AbstractControl;
  private increaseContributionControl: AbstractControl;
  private increaseContributionNewControl: AbstractControl;
  private reducePartnerIdControl: AbstractControl;
  private reduceContributionControl: AbstractControl;
  private reduceContributionNewControl: AbstractControl;
  private contributionChangeControl: AbstractControl;
  private fromControl: AbstractControl;
  private withdrawWithZeroValueControl: AbstractControl;
  private entryWithZeroValueControl: AbstractControl;

  private increasePartnerIdValue: number;
  private increaseContributionNewValue: number;
  private reducePartnerIdValue: number;
  private reduceContributionNewValue: number;
  private contributionChangeValue: number;
  public isIncreasePartnerInZeroValueState = false;
  public isReducePartnerInZeroValueState = false;
  private increasePartnerFinalAmount: number;
  private reducePartnerFinalAmount: number;

  constructor(
    private contributionsMainService: ContributionMainService,
    private settingsService: SettingsService,
  ) { }

  public init(
    form: FormGroup,
    componentDestroyed$: ReplaySubject<unknown>,
    companyId: CompanyKey,
    trustorsContributions: ITrusteePartnerContribution[],
    subParticipantsContributions: ITrusteePartnerContribution[],
    contributions: ICompanyPartnerContributionFull[],
    legalEntity: ICompanyLegalEntity,
    isManagingPartnersVisible: boolean,
  ) {
    this.form = form;
    this.componentDestroyed$ = componentDestroyed$;
    this.companyId = companyId;
    this.trustorsContributions = trustorsContributions;
    this.subParticipantsContributions = subParticipantsContributions;
    this.allContributions = contributions;
    this.legalEntity = legalEntity;
    this.isManagingPartnersVisible = isManagingPartnersVisible;

    this.setSubContributionsTotalAmount();
  }

  private setSubContributionsTotalAmount(): void {
    const subContributionsSumMap: ISubContributionsSumMap = new Map();
    const subContributions: ITrusteePartnerContribution[] = [
      ...this.trustorsContributions,
      ...this.subParticipantsContributions,
    ];

    subContributions.forEach((contribution: ITrusteePartnerContribution) => {
      const { shareholderId, contributionAmount } = contribution;
      const totalAmount: number = subContributionsSumMap.get(shareholderId) || 0;

      subContributionsSumMap.set(
        shareholderId,
        safeFloatSum(totalAmount, contributionAmount),
      );
    });

    this.subContributionsSumMap = subContributionsSumMap;
  }

  public initValidations = (
    latestContributions: Map<CompanyKey, ICompanyPartnerContributionFull>
  ): void => {
    this.latestContributions = latestContributions;

    const {
      increasePartnerId,
      increaseContribution,
      increaseContributionNew,
      reducePartnerId,
      reduceContribution,
      reduceContributionNew,
      contributionChange,
      from,
      withdrawWithZeroValue,
      entryWithZeroValue,
    } = this.form.controls;

    this.increasePartnerIdControl = increasePartnerId;
    this.increaseContributionControl = increaseContribution;
    this.increaseContributionNewControl = increaseContributionNew;
    this.reducePartnerIdControl = reducePartnerId;
    this.reduceContributionControl = reduceContribution;
    this.reduceContributionNewControl = reduceContributionNew;
    this.contributionChangeControl = contributionChange;
    this.fromControl = from;
    this.withdrawWithZeroValueControl = withdrawWithZeroValue;
    this.entryWithZeroValueControl = entryWithZeroValue;

    combineLatest([
      this.increasePartnerIdControl,
      this.increaseContributionNewControl,
      this.reducePartnerIdControl,
      this.reduceContributionNewControl,
      this.contributionChangeControl,
      this.fromControl,
    ].map((control: FormControl) => control.valueChanges.pipe(
      startWith(control.value),
      distinctUntilChanged(),
    )))
      .pipe(
        takeUntil(this.componentDestroyed$),
        tap(([
          increasePartnerIdValue,
          increaseContributionNewValue,
          reducePartnerIdValue,
          reduceContributionNewValue,
          contributionChangeValue,
        ]) => {
          this.increasePartnerIdValue = increasePartnerIdValue;
          this.increaseContributionNewValue = increaseContributionNewValue;
          this.reducePartnerIdValue = reducePartnerIdValue;
          this.reduceContributionNewValue = reduceContributionNewValue;
          this.contributionChangeValue = contributionChangeValue;

          this.setShareholdersContributions();
          this.setFinalAmounts();
          this.setZeroValueStates();
          this.setValidators();
          this.enableDisableFields();
          this.setCalculatedValues();

          this.fromControl.updateValueAndValidity();
          this.increasePartnerIdControl.updateValueAndValidity();
          this.increaseContributionNewControl.updateValueAndValidity();
          this.reducePartnerIdControl.updateValueAndValidity();
          this.reduceContributionNewControl.updateValueAndValidity();
          this.contributionChangeControl.updateValueAndValidity();
        })
      ).subscribe();

    this.initCheckboxValidationsAndBehaviors();
    this.clearNewContributionOnPartnerChanges();

    if (this.isManagingPartnersVisible) {
      this.initManagingPartners();
    }
  }

  public initFinalContribution = (
    partnerControlName: string,
    contributionControlName: string,
    considerDate = true,
  ): void => {
    const {
      from: fromControl,
      [partnerControlName]: partnerIdControl,
      [contributionControlName]: contributionControl,
    } = this.form.controls;

    combineLatest([
      fromControl.valueChanges.pipe(
        startWith(fromControl.value),
        distinctUntilChanged(),
      ),
      partnerIdControl.valueChanges.pipe(
        startWith(partnerIdControl.value),
        distinctUntilChanged(),
      ),
    ])
      .pipe(
        takeUntil(this.componentDestroyed$),
        tap(([
          fromValue,
          partnerIdValue,
        ]) => {
          if (partnerIdValue) {
            const partnersContribution: ICompanyPartnerContributionFull[]
              = this.allContributions.filter(c => c.partnerId === +partnerIdValue);
            const finalContribution: number = this.contributionsMainService.getFinalAmount(
              partnersContribution,
              considerDate ? fromValue : undefined,
            );

            contributionControl.setValue(finalContribution);
            this.setCalculatedValues();
          } else {
            contributionControl.setValue(0);
          }
        })
      ).subscribe();
  }

  private setShareholdersContributions(): void {
    this.increaseShareholdersContributions = this.allContributions.filter((c) => {
      return c.partnerId === +this.increasePartnerIdValue;
    });

    this.reduceShareholdersContributions = this.allContributions.filter((c) => {
      return c.partnerId === +this.reducePartnerIdValue;
    });
  }

  private setFinalAmounts(): void {
    this.increasePartnerFinalAmount = this.contributionsMainService.getFinalAmount(
      this.contributionsMainService.filterContributions(
        this.increaseShareholdersContributions,
        this.fromControl.value,
      ),
    );

    this.reducePartnerFinalAmount = this.contributionsMainService.getFinalAmount(
      this.contributionsMainService.filterContributions(
        this.reduceShareholdersContributions,
        this.fromControl.value,
      ),
    );
  }

  private setZeroValueStates(): void {
    this.isIncreasePartnerInZeroValueState = this.contributionsMainService.isInZeroValueState(
      this.contributionsMainService.filterContributions(
        this.increaseShareholdersContributions,
        this.fromControl.value,
      ),
    );

    this.isReducePartnerInZeroValueState = this.contributionsMainService.isInZeroValueState(
      this.contributionsMainService.filterContributions(
        this.reduceShareholdersContributions,
        this.fromControl.value,
      ),
    );
  }

  private setValidators(): void {
    this.setFromValidator();
    this.setIncreasePartnerIdValidator();
    this.setReducePartnerIdValidator();
    this.setIncreaseContributionNewValidator();
    this.setReduceContributionNewValidator();
    this.setContributionChangeValidator();
  }

  private enableDisableFields(): void {
    this.enableDisableIncreaseContributionNew();
    this.enableDisableReduceContributionNew();
    this.enableDisableContributionChange();
    this.enableDisableZeroValueCheckboxes();
  }

  private setCalculatedValues(): void {
    this.calculateIncreaseContributionNew();
    this.calculateReduceContributionNew();
    this.calculateContributionChange();
  }

  private setFromValidator(): void {
    const increasePartnerLastDate: string = this.latestContributions.get(this.increasePartnerIdValue)?.from;
    const reducePartnerLastDate: string = this.latestContributions.get(this.reducePartnerIdValue)?.from;
    let greaterDate: string;

    if (increasePartnerLastDate && reducePartnerLastDate) {
      greaterDate = getGreaterDate(increasePartnerLastDate, reducePartnerLastDate);
    } else if (increasePartnerLastDate) {
      greaterDate = increasePartnerLastDate;
    } else if (reducePartnerLastDate) {
      greaterDate = reducePartnerLastDate;
    }

    if (greaterDate) {
      this.fromControl.setValidators([
        Validators.required,
        Validators.dateShouldBeAfter(greaterDate, this.settingsService.locale, true)
      ]);
    } else {
      this.fromControl.setValidators([Validators.required]);
    }
  }

  private setIncreasePartnerIdValidator(): void {
    if (this.reducePartnerIdValue) {
      this.increasePartnerIdControl.clearValidators();
    } else {
      this.increasePartnerIdControl.setValidators(Validators.required);
    }
  }

  private setReducePartnerIdValidator(): void {
    if (this.increasePartnerIdValue) {
      this.reducePartnerIdControl.clearValidators();
    } else {
      this.reducePartnerIdControl.setValidators(Validators.required);
    }
  }

  private setIncreaseContributionNewValidator(): void {
    if (this.increasePartnerIdValue) {
      this.increaseContributionNewControl.setValidators(
        [
          Validators.required,
          ...decimalValidators,
          Validators.positiveNumberFloat,
          Validators.isGreaterThanFieldValue('increaseContribution'),
        ]
      );
    } else {
      this.increaseContributionNewControl.setValidators(decimalValidators);
    }
  }

  private setReduceContributionNewValidator(): void {
    if (this.reducePartnerIdValue) {
      const subContributionsSum = this.subContributionsSumMap.get(this.reducePartnerIdValue) || 0;
      const errorMessage = setTranslationSubjects(
        wording.system.theValueShouldBeGreaterThanContributionsSum,
        { subject: subContributionsSum.toFixed(2) },
      );

      this.reduceContributionNewControl.setValidators(
        [
          Validators.required,
          ...decimalValidators,
          Validators.notNegativeFloat,
          Validators.isLessThanFieldValue('reduceContribution', false, false, true),
          Validators.min(subContributionsSum, errorMessage),
        ]
      );
    } else {
      this.reduceContributionNewControl.setValidators(decimalValidators);
    }
  }

  private setContributionChangeValidator(): void {
    if (
      (
        this.increaseContributionNewValue
        || this.increaseContributionNewValue === 0
        || this.reduceContributionNewValue
        || this.reduceContributionNewValue === 0
      )
      || (!this.increasePartnerIdValue && !this.reducePartnerIdValue)
    ) {
      const contributionChangeValidator = [
        Validators.required,
        ...decimalValidators,
        Validators.positiveNumber,
      ];

      if (this.reducePartnerIdValue) {
        const subContributionsSum = this.subContributionsSumMap.get(this.reducePartnerIdValue) || 0;
        const maxReductionChangeAmount = safeFloatReduction(
          this.reduceContributionControl.value,
          subContributionsSum,
        );
        const errorMessage = setTranslationSubjects(
          wording.system.theValueShouldBeGreaterThanContributionsSum,
          { subject: maxReductionChangeAmount },
        );

        contributionChangeValidator.push(Validators.isLessThanFieldValue('reduceContribution', true, false));
        contributionChangeValidator.push(Validators.max(maxReductionChangeAmount, errorMessage));
      }

      this.contributionChangeControl.setValidators(contributionChangeValidator);
    } else {
      this.contributionChangeControl.setValidators(
        [Validators.required, ...decimalValidators, Validators.positiveNumber]
      );
    }
  }


  private enableDisableIncreaseContributionNew(): void {
    if (this.entryWithZeroValueControl.value) {
      return;
    }
    if (
      !this.increasePartnerIdValue
      || (
        this.contributionChangeControl.enabled
        && this.contributionChangeValue
      )
    ) {
      setTimeout(() => {
        this.increaseContributionNewControl.disable();
      });
    } else {
      setTimeout(() => {
        this.increaseContributionNewControl.enable();
      });
    }
  }


  private enableDisableReduceContributionNew(): void {
    // when withdraw with zero is checked we don't want to calcuate and we should set to 0
    if (this.withdrawWithZeroValueControl.value) {
      return;
    }
    if (
      !this.reducePartnerIdValue
      || (
        this.contributionChangeControl.enabled
        && this.contributionChangeValue
      )
    ) {
      setTimeout(() => {
        this.reduceContributionNewControl.disable();
      });
    } else {
      setTimeout(() => {
        this.reduceContributionNewControl.enable();
      });
    }
  }

  private enableDisableContributionChange(): void {
    if (
      (this.increaseContributionNewValue && this.increaseContributionNewControl.enabled)
      || (
        (this.reduceContributionNewValue || this.reduceContributionNewValue === 0)
        && this.reduceContributionNewControl.enabled
      )
      || (!this.increasePartnerIdValue && !this.reducePartnerIdValue)
    ) {
      setTimeout(() => {
        this.contributionChangeControl.disable();
      });
    } else {
      setTimeout(() => {
        // if either of checkboxes are checked the contrib. change amount field should be disabled
        // https://navido.atlassian.net/browse/NAVIDO-2815
        if (!(this.entryWithZeroValueControl.value || this.withdrawWithZeroValueControl.value)) {
          this.contributionChangeControl.enable();
        }
      });
    }
  }

  private enableDisableZeroValueCheckboxes(): void {
    if (this.isIncreasePartnerInZeroValueState || this.increasePartnerFinalAmount > 0) {
      this.entryWithZeroValueControl.disable();
      this.entryWithZeroValueControl.setValue(null);
    } else {
      this.entryWithZeroValueControl.enable();
    }

    if (!this.isReducePartnerInZeroValueState) {
      this.withdrawWithZeroValueControl.disable();
      this.withdrawWithZeroValueControl.setValue(null);
    } else {
      this.withdrawWithZeroValueControl.enable();
    }
  }


  private calculateIncreaseContributionNew(): void {
    if (this.entryWithZeroValueControl.value) {
      return;
    }
    if (
      this.increasePartnerIdValue
      && this.contributionChangeValue
      && this.contributionChangeControl.enabled
      && this.increaseContributionControl.disabled
    ) {
      setTimeout(() => {
        this.increaseContributionNewControl.setValue(
          safeFloatSum(
            this.increaseContributionControl.value,
            this.contributionChangeValue,
          ),
        );
      });
    }
    if (
      !this.increasePartnerIdValue
      || (!this.contributionChangeValue && this.increaseContributionNewControl.disabled)
    ) {
      setTimeout(() => {
        this.increaseContributionNewControl.setValue(null);
      });
    }
  }


  private calculateReduceContributionNew(): void {
    // when withdraw with zero is checked we don't want to calculate and we should set to 0
    if (this.withdrawWithZeroValueControl.value) {
      return;
    }
    if (
      this.reducePartnerIdValue
      && this.contributionChangeValue
      && this.contributionChangeControl.enabled
    ) {
      setTimeout(() => {
        this.reduceContributionNewControl.setValue(
          safeFloatReduction(
            this.reduceContributionControl.value,
            this.contributionChangeValue,
          )
        );
      });
    }
    if (
      !this.reducePartnerIdValue
      || (!this.contributionChangeValue && this.reduceContributionNewControl.disabled)
    ) {
      setTimeout(() => {
        this.reduceContributionNewControl.setValue(null);
      });
    }
  }

  private calculateContributionChange(): void {
    const increaseCalculatedValue: number =
      this.increaseContributionNewValue
        ? safeFloatReduction(
          this.increaseContributionNewValue,
          this.increaseContributionControl.value,
        )
        : null;
    const reduceCalculatedValue: number =
      this.reduceContributionNewValue || this.reduceContributionNewValue === 0
        ? safeFloatReduction(this.reduceContributionControl.value,
          this.reduceContributionNewValue,
        )
        : null;

    // If both values are entered and change is equal
    if (
      (
        (this.increaseContributionNewValue || this.increaseContributionNewValue === 0)
        && this.increaseContributionNewControl.enabled
      )
      && (
        (this.reduceContributionNewValue || this.reduceContributionNewValue === 0)
        && this.reduceContributionNewControl.enabled
      )
      && increaseCalculatedValue && increaseCalculatedValue === reduceCalculatedValue
    ) {
      setTimeout(() => {
        this.contributionChangeControl.setValue(increaseCalculatedValue);
      });
    }

    // If entered value only in increaseContributionNewControl
    if (
      increaseCalculatedValue
      && !reduceCalculatedValue
      && this.increaseContributionNewControl.enabled
    ) {
      if (!this.reducePartnerIdValue) {
        setTimeout(() => {
          this.contributionChangeControl.setValue(increaseCalculatedValue);
        });
      } else {
        setTimeout(() => {
          this.contributionChangeControl.setValue(null);
        });
      }
    }


    // If entered value only in reduceContributionNewControl
    if (
      (reduceCalculatedValue || reduceCalculatedValue === 0)
      && !increaseCalculatedValue
      && this.reduceContributionNewControl.enabled
    ) {
      if (!this.increasePartnerIdValue) {
        setTimeout(() => {
          this.contributionChangeControl.setValue(reduceCalculatedValue);
        });
      } else {
        setTimeout(() => {
          this.contributionChangeControl.setValue(null);
        });
      }
    }


    // If both values are entered but change is not equal
    // or both values are not entered
    if (
      (
        (
          this.increaseContributionNewValue
          && (this.reduceContributionNewValue || this.reduceContributionNewValue === 0)
          && increaseCalculatedValue !== reduceCalculatedValue
          && this.increaseContributionNewControl.enabled
        )
        || (
          !this.increaseContributionNewValue
          && !(this.reduceContributionNewValue || this.reduceContributionNewValue === 0)
          && this.increaseContributionNewControl.enabled
        )
      )
      && this.contributionChangeControl.disabled
    ) {
      setTimeout(() => {
        this.contributionChangeControl.setValue(null);
      });
    }

    // If both values are not entered and contributionChangeControl is disabled
    if (
      !this.increaseContributionNewValue
      && !(this.reduceContributionNewValue || this.reduceContributionNewValue === 0)
      && this.contributionChangeControl.disabled
    ) {
      setTimeout(() => {
        this.contributionChangeControl.setValue(null);
      });
    }
  }

  private clearNewContributionOnPartnerChanges(): void {
    if (this.entryWithZeroValueControl.value) {
      return;
    }
    this.increasePartnerIdControl.valueChanges.pipe(
      takeUntil(this.componentDestroyed$),
      distinctUntilChanged(),
    ).subscribe(() => {
      if (this.increaseContributionNewControl.enabled) {
        setTimeout(() => {
          this.increaseContributionNewControl.setValue(null);
        });

        if (this.reduceContributionNewValue && this.reduceContributionNewControl.enabled) {
          setTimeout(() => {
            this.contributionChangeControl.setValue(null);
          });
        }
      }
    });

    this.reducePartnerIdControl.valueChanges.pipe(
      takeUntil(this.componentDestroyed$),
      distinctUntilChanged(),
    ).subscribe(() => {
      if (this.reduceContributionNewControl.enabled) {
        setTimeout(() => {
          this.reduceContributionNewControl.setValue(null);
        });

        if (this.increaseContributionNewValue && this.increaseContributionNewControl.enabled) {
          setTimeout(() => {
            this.contributionChangeControl.setValue(null);
          });
        }
      }
    });
  }

  public initCheckboxValidationsAndBehaviors(): void {
    this.entryWithZeroValueControl.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed$),
        distinctUntilChanged(),
        tap(isChecked => this.onEntryWithZeroValueCheckboxChange(isChecked))
      ).subscribe();

    this.withdrawWithZeroValueControl.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed$),
        distinctUntilChanged(),
        tap(isChecked => this.onWithdrawWithZeroValueCheckboxChange(isChecked))
      ).subscribe();
  }

  private onEntryWithZeroValueCheckboxChange(isChecked: boolean): void {
    this.onZeroCheckboxChange(isChecked, this.increaseContributionNewControl);
  }

  private onWithdrawWithZeroValueCheckboxChange(isChecked: boolean): void {
    this.onZeroCheckboxChange(isChecked, this.reduceContributionNewControl);
  }

  private onZeroCheckboxChange(
    isChecked: boolean,
    contributionNewControl: AbstractControl,
  ): void {
    if (isChecked) {
      contributionNewControl.disable();
      contributionNewControl.patchValue(null);
      this.contributionChangeControl.disable();
      this.contributionChangeControl.patchValue(null);
    } else {
      contributionNewControl.enable();
    }
  }

  private initManagingPartners() {
    const {
      increasePartnerId,
      reducePartnerId,
      increaseManagingPartner,
      reduceManagingPartner,
    } = this.form.controls;

    this.getManagingPartnersValue(increasePartnerId)
      .subscribe(isManagingPartner => {
        increaseManagingPartner.patchValue(isManagingPartner);

        if (this.increasePartnerFinalAmount > 0) {
          increaseManagingPartner.disable();
        } else {
          increaseManagingPartner.enable();
        }
      });

    this.getManagingPartnersValue(reducePartnerId)
      .subscribe(isManagingPartner => {
        this.initialReduceManagingPartnerValue = isManagingPartner;
        reduceManagingPartner.patchValue(isManagingPartner);
      });

    this.reduceContributionNewControl.valueChanges.pipe(
      distinctUntilChanged(),
      startWith(this.reduceContributionNewControl.value),
      map(value => {
        if (value === 0) {
          reduceManagingPartner.patchValue(null);
        } else {
          reduceManagingPartner.patchValue(this.initialReduceManagingPartnerValue);
        }
      }),
    ).subscribe();
  }

  private getManagingPartnersValue(
    partnerIdControl: AbstractControl,
  ): Observable<boolean> {
    return partnerIdControl.valueChanges.pipe(
      distinctUntilChanged(),
      startWith(partnerIdControl.value),
      map(value => this.legalEntity.partners.includes(value) || null),
    );
  }
}
