import {Component, Input, OnInit} from '@angular/core';
import {SidebarService} from '../../services/sidebar.service';

@Component({
  selector: 'lavash-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less']
})
export class SidebarComponent implements OnInit {
  @Input() sidebarWidth = 300;
  @Input() openBtnSize = 40;

  @Input() showsidebar: boolean;

  @Input() sidebarPosition: 'top-left' | 'top-right' | 'bottom-left' | 'bottom-right' = 'top-right';
  @Input() sidebarParent: 'body' | 'container' = 'body';
  @Input() openBtn: 'fix' | 'move-in'| 'move-out' = 'move-out'; // default move wth navbar

  @Input() sidebarTitle?: string;

  @Input() sidebarVisiblityBreakpoint?: number;
  _sidebarVisibility = true;


  // toggle icon
  @Input() openText = '';
  @Input() closeText = '';
  @Input() openIcon = 'l-icon-menu';
  @Input() closeIcon = 'l-icon-times';

  // toggle icon tooltip
  @Input() tooltipText = '';
  _tooltipText = this.tooltipText;
  tooltipDirection = this.sidebarPosition === 'top-left' || this.sidebarPosition === 'bottom-left' ?
                     'right' : 'left';

  // asside content
  @Input() hasAssideContent: boolean;
  @Input() assideContentPosition: 'fix' | 'flex' = 'flex';

  constructor(private sidebarService: SidebarService) {}

  toggleSidebar() {
    this.showsidebar = !this.showsidebar;
    this.tooltipText = this.showsidebar ? '' : this._tooltipText;

    this.sidebarService.changeState(this.showsidebar);
  }

  ngOnInit() {
    if (this.sidebarVisiblityBreakpoint) {
      this._sidebarVisibility = this.sidebarVisiblityBreakpoint === window.innerWidth;
    }

    this.sidebarService.changeState(this.showsidebar);
    this.sidebarService.sidebarState.subscribe((observer) => {
        this.showsidebar = observer;
    });
  }

  onClickOut(event) {
    if (this.showsidebar && event.e.type === 'click' && !event.targetContains) {
        this.toggleSidebar();
    }
  }

  calcSidebarWrapper(position) {
    let positionValue = null;

    if (this.sidebarPosition === 'top-' + position ||
       this.sidebarPosition === 'bottom-'  + position) {

        positionValue = this.showsidebar ? 0 : -this.sidebarWidth;
    }

    return positionValue;
  }

  calcSidebarButtonX(position) {
      let positionValue = null;

      if (this.sidebarPosition === 'top-' + position ||
          this.sidebarPosition === 'bottom-'  + position) {

          positionValue = this.sidebarWidth;

          if (this.openBtn === 'fix') {
              positionValue = this.showsidebar ?  0 : positionValue;
          }

          if (this.openBtn === 'move-in') {
              positionValue = this.showsidebar ? this.sidebarWidth - this.openBtnSize : positionValue;
          }
      }

      return positionValue;
  }

  calcSidebarButtonY(position) {
    let positionValue = null;
    if (this.sidebarPosition.indexOf(position) >= 0) {
        positionValue = 0;
    }

    return positionValue;
  }

  calcAsside(position) {
      let positionValue = null;

      if (this.sidebarPosition === 'top-' + position ||
          this.sidebarPosition === 'bottom-'  + position) {

          positionValue = this.assideContentPosition === 'flex' ? (this.showsidebar ? this.sidebarWidth : 0) : 0;
      }

      return positionValue;
  }

  calcAssideContent() {
    let positionValue = 0;

    if (this.sidebarPosition.indexOf('right') >= 0) {
        positionValue = this.assideContentPosition === 'flex' ?
                       (this.showsidebar ?  this.sidebarWidth : positionValue) :
                       positionValue;
    }

    return positionValue;
  }

}
