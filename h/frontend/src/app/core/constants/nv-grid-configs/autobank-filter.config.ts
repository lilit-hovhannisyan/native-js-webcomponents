import { NvGridConfig, NvGridButtonsPosition } from 'nv-grid';
import { wording } from '../wording/wording';
import { Validators } from '../../classes/validators';
import { IAutobankRuleFilter } from '../../models/resources/IAutobankRule';
import { AbstractControl, ValidationErrors, FormGroup } from '@angular/forms';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';

export interface Formats {
  field: (id: number, property: string) => any;
  operation: (id: number, property: string) => any;
}


export interface CustomValidators {
  doesOperationTypeExist: (control: AbstractControl) => ValidationErrors | null;
  isRelated: (control: AbstractControl) => ValidationErrors | null;
  hasValue: (control: AbstractControl) => ValidationErrors | null;
}

export const autobankFilterConfig = (
  formats: Formats,
  validators: CustomValidators,
  actions: IGridConfigActions,
  onFieldTypeChange: (form: FormGroup) => any
): NvGridConfig => ({
  gridName: 'AutobankFilter',
  hideToolbar: true,
  hideAllFilters: true,
  showFooter: false,
  disableDragAndDrop: true,
  disableHideColumns: true,
  rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
  paging: {
    pageNumber: 1,
    pageSize: 10
  },
  editForm: {
    allowCreateNewRow: true,
    preventDiscardingFirstRow: true,
    showDiscardChangesButton: true,
  },
  columns: [
    {
      key: 'id',
      visible: false
    },
    {
      key: 'fieldTypeId',
      title: wording.accounting.fieldName,
      width: 250,
      isSortable: true,
      editControl: {
        editable: true,
        validation: {
          validators: [Validators.required],
        },
        onValueChange: onFieldTypeChange
      },
      customFormatFn: (row: IAutobankRuleFilter) => formats.field(row.fieldTypeId, 'displayLabel')
    },
    {
      key: 'operationTypeId',
      title: wording.accounting.operation,
      width: 250,
      isSortable: true,
      editControl: {
        editable: true,
        validation: {
          validators: [
            Validators.required,
            validators.doesOperationTypeExist,
            validators.isRelated,
          ],
        }
      },
      customFormatFn: (row: IAutobankRuleFilter) => formats.operation(row.operationTypeId, 'displayLabel')
    },
    {
      key: 'value',
      title: wording.accounting.value,
      width: 250,
      isSortable: true,
      editControl: {
        editable: true,
        disabled: false,
        validation: {
          validators: [validators.hasValue],
        }
      }
    }
  ],
  buttons: [
    {
      icon: 'delete',
      description: wording.general.delete,
      name: 'deleteFilter',
      tooltip: wording.general.delete,
      actOnDoubleClick: false,
      func: actions.delete,
    },
  ]
});
