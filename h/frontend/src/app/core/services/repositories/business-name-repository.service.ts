import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IBusinessName, IBusinessNameDTO, IBusinessNameCreate, BusinessNameKey } from '../../models/resources/IBusinessName';
import { CompanyKey } from '../../models/resources/ICompany';

export interface BusinessNameRepoParams extends RepoParams<IBusinessName> {
  queryParams?: {
    cid?: CompanyKey;
    offset?: number;
    limit?: number;
  };
 }

@Injectable({
  providedIn: 'root'
})
export class BusinessNameRepositoryService extends AbstractRepository<
  IBusinessName,
  IBusinessNameDTO,
  IBusinessNameCreate,
  BusinessNameRepoParams,
  BusinessNameKey> {

  protected resourceType = 'BusinessName';
  public sortKey = 'from';
  public sortAsce = false;

  constructor(
    repositoryService: RepositoryService,
  ) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/business-names';

  protected postProcess(item: IBusinessNameDTO): IBusinessName { return item; }
}
