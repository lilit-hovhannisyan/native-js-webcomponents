import { Component } from '@angular/core';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { ActivatedRoute, Router } from '@angular/router';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';

@Component({
  selector: 'nv-autobank-setting-detail-view',
  templateUrl: './autobank-setting-detail-view.component.html',
  styleUrls: ['./autobank-setting-detail-view.component.scss'],
})
export class AutobankSettingDetailViewComponent {
  public sitemap = sitemap;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) { }

  public routeBack(): void {
    const parentUrl = url(sitemap.accounting.children.autobank);
    this.router.navigate([parentUrl], { relativeTo: this.activatedRoute });
  }
}
