import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IAutobankOperationType, IAutobankOperationTypeDTO, IAutobankOperationTypeCreate, AutobankOperationTypeKey } from '../../models/resources/IAutobankOperationType';

export interface IAutobankOperationTypeParams extends RepoParams<IAutobankOperationType> { }

@Injectable({
  providedIn: 'root'
})
export class AutobankOperationTypeRepositoryService extends AbstractRepository<
IAutobankOperationType,
IAutobankOperationTypeDTO,
IAutobankOperationTypeCreate,
IAutobankOperationTypeParams,
AutobankOperationTypeKey
>  {
  protected resourceType = 'Autobank';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IAutobankOperationTypeDTO): IAutobankOperationType => item;

  public getBaseUrl = () => '/api/autobank/rule-field-operation-types';
}
