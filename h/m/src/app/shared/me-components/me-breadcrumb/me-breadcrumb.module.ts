import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BreadcrumbModule } from 'xng-breadcrumb';

import { IconsModule } from '@shared-modules/icons.module';

import { MeBreadcrumbComponent } from './me-breadcrumb.component';
import { MeButtonModule } from '../me-button/me-button.module';
import { MeEllipsisRowModule } from '../me-ellipsis-row/me-ellipsis-row.module';

@NgModule({
  declarations: [MeBreadcrumbComponent],
  imports: [CommonModule, IconsModule, BreadcrumbModule, MeButtonModule, MeEllipsisRowModule],
  exports: [MeBreadcrumbComponent]
})
export class MeBreadcrumbModule {}
