import { Injectable } from '@angular/core';
import { BehaviorSubject, forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BookingAccountKey } from '../../models/resources/IBookingAccount';
import { CostTypeKey, ICostType} from '../../models/resources/ICostTypes';
import { AccountCostTypeRepositoryService } from '../repositories/account-cost-type-repository.service';
import { CostTypeRepositoryService } from '../repositories/cost-type-repository.service';
import {
  IAccountCostType, IAccountCostTypeCreate,
  IAccountRelatedCostType
} from '../../models/resources/IAccountCostType';
import { ACCOUNTING_RELATION_CATEGORY } from '../../models/enums/accounting-relation-category';

@Injectable({
  providedIn: 'root'
})
export class AccountCostTypeMainService {
  /**
   * Cost type relations are manipulated only in frontend
   * and at the same route are rendered both related and not related ct lists
   * which can be dynamically changed by user, but network happens only once
   * when user submit `save` for booking account
   * so to avoid extra calls for related and not list table
   * 2 observables are created at once to handle content of these tables
   */
  private relatedCtList: IAccountRelatedCostType[] = [];
  private notRelatedCtList: IAccountRelatedCostType[] = [];
  public bookingIdRelatedCostTypeList$ = new BehaviorSubject<IAccountRelatedCostType[]>([]);
  public bookingIdNotRelatedCostTypeList$ = new BehaviorSubject<IAccountRelatedCostType[]>([]);


  constructor(
    private accountCostTypeRepo: AccountCostTypeRepositoryService,
    private costTypeRepo: CostTypeRepositoryService,
  ) {}

  private reset(accountingRelationCategory: ACCOUNTING_RELATION_CATEGORY): void {
    this.relatedCtList = [];
    this.notRelatedCtList = [];

    this.updateListStreams();
  }

  private updateListStreams(): void {
    this.bookingIdRelatedCostTypeList$.next(this.relatedCtList);
    this.bookingIdNotRelatedCostTypeList$.next(this.notRelatedCtList);
  }

  public getStream(params: {
    bookingAccountId?: BookingAccountKey,
    accountingRelationCategory?: ACCOUNTING_RELATION_CATEGORY
  } = {}
  ): Observable<IAccountRelatedCostType[]> {

    this.reset(params.accountingRelationCategory);

    return forkJoin([
      this.accountCostTypeRepo.fetchAll({}),
      this.costTypeRepo.fetchAll({}),
    ]).pipe(
      map(([allAccountCostTypes, costTypes]) => {
        return this.getBookingAccountRelatedCostTypes(
          allAccountCostTypes,
          costTypes,
          params.bookingAccountId,
          params.accountingRelationCategory
        );
      })
    );
  }

  private getBookingAccountRelatedCostTypes(
    allAccountCostTypes: IAccountCostType[],
    costTypes: ICostType[],
    bookingAccountId?: BookingAccountKey,
    accountingRelationCategory?: ACCOUNTING_RELATION_CATEGORY
  ): IAccountRelatedCostType[] {
    const bookingIdRelatedCostTypesMap: Map<CostTypeKey, IAccountCostType> =
      new Map<CostTypeKey, IAccountCostType>([]);

    const bookingIdRelatedCostTypes: IAccountCostType[] = allAccountCostTypes
      .filter(accCt => accCt.bookingAccountId === bookingAccountId);

    bookingIdRelatedCostTypes.forEach(b => {
      bookingIdRelatedCostTypesMap.set(b.costTypeId, b);
    });

    costTypes.forEach (ct => {
      if (ct.accountingRelation === accountingRelationCategory && ct.isActive) {
        const relation: IAccountCostType = bookingIdRelatedCostTypesMap.get(ct.id);

        relation
          ? this.relatedCtList.push({...ct, relationId: relation.id})
          : this.notRelatedCtList.push(ct);
      }
    });

    this.updateListStreams();

    return this.relatedCtList;
  }

  public addToCostTypeRelationList(idList: CostTypeKey[]): void {
    idList.forEach( ctId => {
      const ctIndex = this.notRelatedCtList.findIndex(ct => ct.id === ctId);

      this.notRelatedCtList[ctIndex]['_checked'] = false;
      this.relatedCtList.push(this.notRelatedCtList[ctIndex]);
      this.notRelatedCtList.splice(ctIndex, 1);
    });

    this.updateListStreams();
  }

  public removeFromCostTypeRelationList(id: CostTypeKey): void {
    const ctIndex = this.relatedCtList.findIndex(ct => ct.id === id);

    this.notRelatedCtList.push(this.relatedCtList[ctIndex]);
    this.relatedCtList.splice(ctIndex, 1);

    this.updateListStreams();
  }

  public createCtRelations(bookingAccountId: BookingAccountKey): void {
    if (!this.relatedCtList.length) {
      return;
    }

    const ctList: IAccountCostTypeCreate[] = this.relatedCtList.map(ct => {
      return {
        costTypeId: ct.id,
        bookingAccountId: bookingAccountId,
        id: ct.relationId
      };
    });

    this.accountCostTypeRepo.saveCtRelations(ctList).subscribe( relations => {
      this.relatedCtList.forEach( (relCt, index) => {
        this.relatedCtList[index].relationId = relations[index].id;
        this.updateListStreams();
      });
    });
  }

  public deleteCtRelations(): void {
    /**
     * delete is done just locally, as actual deletion happens
     * when booking account is `saved` and
     * saveCtRelations is called for POST, PUT, DELETE purposes
     */
    this.notRelatedCtList = [
      ...this.relatedCtList,
      ...this.notRelatedCtList,
    ];
    this.relatedCtList = [];

    this.updateListStreams();
  }

  public getAccountRelatedToCostType(ctId: CostTypeKey): Observable<IAccountCostType[]> {
   return this.accountCostTypeRepo.fetchAll({}).pipe(
      map(allRelct => allRelct.filter(relCt =>  relCt.costTypeId === ctId))
    );
  }
}
