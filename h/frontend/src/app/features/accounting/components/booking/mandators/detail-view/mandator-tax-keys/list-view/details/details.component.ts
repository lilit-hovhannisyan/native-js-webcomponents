import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IMandatorTaxKeyRow } from 'src/app/core/services/mainServices/mandator-tax-keys-main.service';
import { wording } from 'src/app/core/constants/wording/wording';
import { Language } from 'src/app/core/models/language';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class MandatorTaxKeyDetailsComponent implements OnInit {
  @Input() public mandatorTaxKey: IMandatorTaxKeyRow;
  @Input() public closeModal: () => void;

  public wording = wording;
  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private settingsService: SettingsService
  ) { }

  public ngOnInit() {
    const {
      accountLabel,
      accountNo,
      code,
      percentage,
      name,
    } = this.mandatorTaxKey;

    this.form = this.fb.group({
      accountLabel: [accountLabel[this.settingsService.language]],
      accountNo: [accountNo],
      code: [code],
      percentage: [percentage],
      name: [name[this.settingsService.language]],
    });
    this.form.disable();
  }
}
