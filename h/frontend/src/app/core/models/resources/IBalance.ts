import { IResource } from './IResource';
export type BalanceKey = number;

interface ISharedProps {
  bookingAccountId: number;
  creditAmountLocal: number;
  creditAmountTransaction: number;
  currencyId: number;
  debitAmountLocal: number;
  debitAmountTransaction: number;
  mandatorId: number;
  previousYearLocal: number;
  previousYearTransaction: number;
  year: number;
}


export interface IBalanceCreate extends ISharedProps { }

export interface IBalanceBase extends ISharedProps, IResource<number> { }

export interface IBalanceDTO extends IBalanceBase { }

export interface IBalance extends IBalanceDTO { }








