import { Component, ElementRef, OnDestroy, Optional, Self, OnInit, ViewChild, Injector } from '@angular/core';
import { FocusMonitor } from '@angular/cdk/a11y';
import { FormGroup, ControlValueAccessor, NgControl, AbstractControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';

import { Subject, Subscription } from 'rxjs';

import { MeAttributeComponentInterface } from '../me-attribute-component.interface';
import { MeTextAreaInputI } from './me-text-area-input.interface';
import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { BaseLoggerComponent } from '@features/tracking-system';
import { MeInputErrorI } from '../me-input-error/me-input-error.interface';

@Component({
  templateUrl: './me-text-area-input.component.html',
  styleUrls: ['./me-text-area-input.component.scss'],
  providers: [
    {
      provide: MatFormFieldControl,
      useExisting: MeTextAreaInputComponent
    }
  ]
})
export class MeTextAreaInputComponent
  extends BaseLoggerComponent
  implements MeAttributeComponentInterface, ControlValueAccessor, MatFormFieldControl<string>, OnInit, OnDestroy {
  // variables for MatFormFieldControl interface
  static nextId = 0;
  componentVersion: string = '0.0.1';
  currentValue: string = '';
  name: string = '';
  @ViewChild('autosize') autosize!: CdkTextareaAutosize;
  data!: MeTextAreaInputI;
  form!: FormGroup;
  meInputErrorData!: MeInputErrorI;
  private _placeholder!: string;
  private _required = false;
  private _disabled = false;
  private _valueChangeSubscription!: Subscription | undefined;
  stateChanges = new Subject<void>();
  focused = false;
  errorState = false;
  controlType = 'me-text-area-input';
  id = `me-text-area-input-${MeTextAreaInputComponent.nextId++}`;
  describedBy = '';
  // tslint:disable-next-line:no-any
  onChange = (_: any) => {};
  onTouched = () => {};

  get formControl(): AbstractControl | null {
    return this.form.get(this.data.internalName);
  }

  constructor(
    injector: Injector,
    private _focusMonitor: FocusMonitor,
    private _elementRef: ElementRef<HTMLElement>,

    @Optional() @Self() public ngControl: NgControl
  ) {
    super(injector);
    _focusMonitor.monitor(_elementRef, true).subscribe((origin) => {
      if (this.focused && !origin) {
        this.onTouched();
      }
      this.focused = !!origin;
      this.stateChanges.next();
    });

    if (this.ngControl !== null) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit(): void {
    this.form = this.data.form;
    this.name = this.data.internalName;
    this.currentValue = this.formControl?.value;
    this.disabled = this.data.disabled;
    this.meInputErrorData = this.data.inputErrors;
    this._valueChangeSubscription = this.formControl?.valueChanges.subscribe((val: string) => {
      this.currentValue = val;
    });
  }

  ngOnDestroy(): object {
    this.stateChanges.complete();
    this._focusMonitor.stopMonitoring(this._elementRef);
    if (this._valueChangeSubscription) {
      this._valueChangeSubscription.unsubscribe();
    }
    return super.ngOnDestroy();
  }

  // methods required by MatFormFieldControl interface
  get empty(): boolean {
    return false;
  }

  get shouldLabelFloat(): boolean {
    return this.focused || !this.empty;
  }

  get placeholder(): string {
    return this._placeholder;
  }
  set placeholder(value: string) {
    this._placeholder = value;
    this.stateChanges.next();
  }

  get required(): boolean {
    return this._required;
  }
  set required(value: boolean) {
    this._required = value;
    this.stateChanges.next();
  }

  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: boolean) {
    this._disabled = value;

    setTimeout(() => {
      if (this.form) {
        this._disabled ? this.formControl?.disable() : this.formControl?.enable();
        this.stateChanges.next();
      }
    }, 0);
  }

  get value(): string {
    return this.formControl?.value;
  }
  set value(value: string) {
    this.formControl?.setValue(value);
    this.stateChanges.next();
  }

  setDescribedByIds(ids: string[]): void {
    this.describedBy = ids.join(' ');
  }

  onContainerClick(): void {}

  writeValue(value: string): void {
    this.value = value;
  }
  // tslint:disable-next-line:no-any
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  // tslint:disable-next-line:no-any
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
}
