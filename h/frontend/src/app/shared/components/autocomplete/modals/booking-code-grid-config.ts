import { NvGridConfig, NvGridRowSelectionType, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const gridConfig: NvGridConfig = {
  gridName: 'bookingCodeSelectModalGrid',
  rowSelectionType: NvGridRowSelectionType.RadioButton,
  rowButtonsPosition: null,
  hideRefreshButton: true,
  hideSettingsButton: true,
  paging: {
    pageNumber: 1,
    pageSize: 10,
    showTotalItems: true
  },
  columns: [
    {
      key: 'no',
      title: wording.accounting.no,
      width: 90,
      isSortable: true,
      filter: {
        values: []
      }
    },
    {
      key: 'isActive',
      title: wording.accounting.active,
      width: 77,
      isSortable: true,
      filter: {
        controlType: NvFilterControl.Boolean,
        values: []
      },
      dataType: NvColumnDataType.Boolean
    },
    {
      key: 'isSystemDefault',
      title: wording.general.systemDefault,
      dataType: NvColumnDataType.Boolean,
      width: 125,
      isSortable: true,
      filter: {
        controlType: NvFilterControl.Boolean,
        values: []
      }
    },
    {
      key: 'isPaymentsRelevance',
      title: wording.accounting.paymentsRelevance,
      dataType: NvColumnDataType.Boolean,
      width: 165,
      isSortable: true,
      filter: {
        controlType: NvFilterControl.Boolean,
        values: []
      }
    },
    {
      key: 'displayLabel',
      title: wording.general.label,
      width: 150,
      isSortable: true,
      filter: {
        values: []
      }
    },
  ],
};
