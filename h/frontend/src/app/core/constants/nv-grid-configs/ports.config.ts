import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType } from 'nv-grid';
import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { IPort } from 'src/app/core/models/resources/IPort';
import { wording } from 'src/app/core/constants/wording/wording';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';

export const getGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'portsGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'countryFlagPath',
        title: wording.general.flag,
        width: 70,
        isSortable: false,
        filter: {
          values: []
        },
        customTooltip: () => wording.general.flag
      },
      {
        key: 'shortName',
        title: wording.general.shortName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'name',
        title: wording.general.name,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'unLoCode',
        title: wording.accounting.unLoCode,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'cordsLat',
        title: wording.general.latitude,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.Decimal,
        decimalDigitsInfo: '1.6-6',
        filter: {
          values: []
        }
      },
      {
        key: 'cordsLong',
        title: wording.general.longitude,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.Decimal,
        decimalDigitsInfo: '1.6-6',
        filter: {
          values: []
        }
      },
      {
        key: 'countryName',
        title: wording.general.country,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editPort',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (port: IPort) => actions.edit(port.id),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deletePort',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.UPDATE),
        func: (port: IPort) => actions.delete(port),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.UPDATE),
        func: () => actions.add()
      }
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
