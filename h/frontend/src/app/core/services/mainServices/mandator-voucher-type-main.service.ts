import { VoucherTypeRepositoryService } from './../repositories/voucher-type-repository.service';
import { Injectable } from '@angular/core';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { IMandatorVoucherType, IMandatorVoucherTypeFull } from '../../models/resources/IMandatorVoucherType';
import { map } from 'rxjs/operators';
import { MandatorVoucherTypeRepositoryService } from '../repositories/mandator-voucher-type-repository.service';
import { IVoucherType } from '../../models/resources/IVoucherType';
import { sortBy } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class MandatorVoucherTypeMainService {
  constructor(
    private repo: MandatorVoucherTypeRepositoryService,
    private voucherRepo: VoucherTypeRepositoryService,
  ) { }

  public fetchAll(): Observable<IMandatorVoucherTypeFull[]> {
    return forkJoin([
      this.repo.fetchAll({}),
      this.voucherRepo.fetchAll({}),
    ]).pipe(map(res => this.getMandatorVoucherTypesFull(...res)));
  }

  private getMandatorVoucherTypesFull(
    mandatorVoucherTypes: IMandatorVoucherType[],
    voucherTypes: IVoucherType[],
  ): IMandatorVoucherTypeFull[] {
    if (!mandatorVoucherTypes.length || !voucherTypes.length) { return []; }

    return mandatorVoucherTypes.map(mandatorVoucherType => {
      const voucherType = voucherTypes.find(c => c.id === mandatorVoucherType.voucherTypeId);
      return voucherType ? { ...voucherType, ...mandatorVoucherType } : null;
    }).filter(item => !!item);
  }

  public getStream(mandatorId: number): Observable<IMandatorVoucherTypeFull[]> {
    return combineLatest([
      this.repo.getStream(),
      this.voucherRepo.getStream(),
    ]).pipe(
      map(([mandators, vouchers]) => this.getMandatorVoucherTypesFull(mandators, vouchers)),
      map(res => res.filter(v => v.mandatorId === mandatorId)),
      map(res => sortBy(res, [v => v.abbreviation.toLowerCase()]))
    );
  }
}
