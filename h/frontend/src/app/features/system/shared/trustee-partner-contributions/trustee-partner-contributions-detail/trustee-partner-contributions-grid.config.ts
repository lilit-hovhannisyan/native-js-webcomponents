import { NvColumnDataType, NvGridConfig, NvGridButtonsPosition, NvColumnConfig, NvButton } from 'nv-grid';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { wording } from 'src/app/core/constants/wording/wording';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { formatNumber, formatPercent } from '@angular/common';
import { nvLocalMapper } from 'src/app/shared/pipes/localDecimal';
import { IWording } from 'src/app/core/models/resources/IWording';
import { CompanyKey } from 'src/app/core/models/resources/ICompany';
import { SettingsService } from 'src/app/core/services/settings.service';
import { system } from 'src/app/core/constants/wording/system';
import { ITrusteePartnerContribution, ITrusteePartnerContributionFull } from 'src/app/core/models/resources/ITrusteePartnerContribution';
import { COMPANY_LEGAL_FORM } from 'src/app/core/models/enums/company-legal-form';
import { TrusteeContributionsMap } from '../trustee-partner-contribution.helper';
import { COMPANY_CONTRIBUTION_TYPES, partnerTrusteeLabelsByLegal, trusteePartnerLabels } from '../enums/company-contributions';


export const getContributionsGridConfig = (
  isNonParValueShare: boolean,
  settingsService: SettingsService,
  contributionType: COMPANY_CONTRIBUTION_TYPES,
  legalForm: COMPANY_LEGAL_FORM,
  currency: ICurrency,
  latestContributionsMap: TrusteeContributionsMap<CompanyKey, CompanyKey, ITrusteePartnerContribution>,
  totalContributionAmount: number,
  canPerform?: (operation: Operations) => boolean,
  actions?: IGridConfigActions,
): NvGridConfig => {
  const isHistoryGrid: boolean = !!actions;
  const localeCode: string = nvLocalMapper[settingsService.locale];
  const digitsInfo = '.2-2';

  const partnerLabelWording: IWording = partnerTrusteeLabelsByLegal.get(legalForm).get(contributionType);
  const trusteePartnerLabelWording: IWording = trusteePartnerLabels.get(contributionType);

  const colId: NvColumnConfig = {
    key: 'id',
    visible: false,
  };

  const colPartnerName: NvColumnConfig = {
    key: 'trusteePartnerName',
    title: partnerLabelWording,
    width: 200,
    dataType: NvColumnDataType.String,
    footer: isHistoryGrid ? null : { label: system.sumContributions_v1, },
    filter: {
      values: []
    }
  };

  const colTrusteePartnerName: NvColumnConfig = {
    key: 'companyName',
    title: trusteePartnerLabelWording,
    width: 200,
    dataType: NvColumnDataType.String,
    filter: {
      values: []
    }
  };

  // Only for History Grid
  const colFrom: NvColumnConfig = {
    key: 'from',
    title: wording.general.date,
    width: 100,
    dataType: NvColumnDataType.Date,
    filter: {
      values: []
    }
  };

  const colContributionAmount: NvColumnConfig = {
    key: 'contributionAmount',
    title: isNonParValueShare ? system.numberOfNonParValueShares : system.contribution_v3,
    width: 150,
    dataType: NvColumnDataType.Decimal,
    customFormatFn: (row: ITrusteePartnerContribution) => {
      if (!row) {
        return;
      }
      const { contributionAmount } = row;
      const formattedValue: string = formatNumber(contributionAmount, localeCode, digitsInfo);
      return contributionAmount > 0 ? `+${formattedValue}` : formattedValue;
    },
    footer: isHistoryGrid
      ? {}
      : { label: formatNumber(totalContributionAmount, localeCode, digitsInfo) },
    filter: {
      values: []
    }
  };

  // both Grids
  const colCurrency: NvColumnConfig = {
    key: 'currency',
    title: wording.system.currency,
    width: 100,
    dataType: NvColumnDataType.Currency,
    customFormatFn: (row: ITrusteePartnerContribution) => row && currency.isoCode,
    footer: isHistoryGrid ? {} : { label: currency.isoCode },
    filter: {
      values: []
    }
  };


  // Only Result grid
  const colPercentage: NvColumnConfig = {
    key: 'percentage',
    title: wording.system.percentage,
    width: 100,
    customFormatFn: (row: ITrusteePartnerContributionFull) => {
      // `row` can be `undefined` when footer exists
      if (!row) {
        return;
      }

      const percentValue = row.contributionAmount && row.trusteePartner?.contributionAmount
        ? row.contributionAmount / row.trusteePartner.contributionAmount
        : 0;

      return formatPercent(percentValue, localeCode, digitsInfo);
    },
    filter: {
      values: []
    }
  };

  // Both Grids
  const colRemark: NvColumnConfig = {
    key: 'remark',
    title: wording.system.remark,
    width: 300,
    filter: {
      values: []
    },
  };

  const colFromArr: NvColumnConfig[] = isHistoryGrid ? [colFrom] : [];
  const colPercentageArr: NvColumnConfig[] = isHistoryGrid ? [] : [colPercentage];
  const colCurrencyArr: NvColumnConfig[] = isNonParValueShare ? [] : [colCurrency];

  const columns: NvColumnConfig[] = [
    colId,
    colPartnerName,
    colTrusteePartnerName,
    ...colFromArr,
    colContributionAmount,
    ...colCurrencyArr,
    ...colPercentageArr,
    colRemark,
  ];

  const actionsDisabler = (row: ITrusteePartnerContribution) => {
    const latestContributionOfCompany = latestContributionsMap.get(row.partnerId, row.shareholderId);
    return latestContributionOfCompany && latestContributionOfCompany.id !== row.id;
  };
  const editButton: NvButton = {
    icon: 'edit',
    description: wording.general.edit,
    name: 'edit',
    tooltip: wording.general.edit,
    hidden: !canPerform(Operations.UPDATE),
    disabled: actionsDisabler,
    func: (contribution: ITrusteePartnerContribution) => actions.edit(contribution),
  };
  const deleteButton: NvButton = {
    icon: 'delete',
    description: wording.general.delete,
    name: 'delete',
    tooltip: wording.general.delete,
    hidden: !canPerform(Operations.DELETE),
    disabled: actionsDisabler,
    func: (contribution: ITrusteePartnerContribution) => actions.delete(contribution),
  };

  const contributionTypeLabel = contributionType === COMPANY_CONTRIBUTION_TYPES.TrustorContribution
    ? 'trustors'
    : 'subParticipants';
  const gridLabel = isHistoryGrid
    ? 'historical'
    : 'result';

  return {
    gridName: `${contributionTypeLabel}-contributions-${gridLabel}-gridConfig`,
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    sortBy: 'from',
    isSortAscending: false,
    showPaging: false,
    hideToolbar: isHistoryGrid,
    showFooter: !isHistoryGrid,
    columns,
    buttons: isHistoryGrid ? [editButton, deleteButton] : [],
  };
};

