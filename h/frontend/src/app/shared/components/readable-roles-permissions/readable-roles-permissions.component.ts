import { Component, Input, OnInit } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { forkJoin } from 'rxjs';
import { SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { addTabsFullZoneAndTitle, SitemapEntry } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { IRole } from 'src/app/core/models/resources/IRole';
import { Operations } from 'src/app/core/models/Operations';
import { IUser } from 'src/app/core/models/resources/IUser';
import { IUserRole } from 'src/app/core/models/resources/IUserRole';
import { RoleRepositoryService } from 'src/app/core/services/repositories/role-repository.service';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { UserRoleRepositoryService } from 'src/app/core/services/repositories/user-role.repository.service';
import { specialZones } from 'src/app/authentication/services/zoneService/specialZones';

interface IUserWithPermission extends IUser {
  hasCreate: boolean;
  hasRead: boolean;
  hasUpdate: boolean;
  hasDelete: boolean;
  isCreateVisible: boolean;
  isReadVisible: boolean;
  isUpdateVisible: boolean;
  isDeleteVisible: boolean;
}

@Component({
  selector: 'nv-readable-roles-permissions',
  templateUrl: './readable-roles-permissions.component.html',
  styleUrls: [
    './readable-roles-permissions.component.scss',
    '../editable-roles-permissions/editable-roles-permissions.component.scss'
  ]
})
export class ReadableRolesPermissionsComponent implements OnInit {
  @Input() public selectedTopic: SitemapNode;
  public usersWithPermissions: IUserWithPermission[];
  public zoneOfSelectedTopic: string;
  public Operations = Operations;
  public allOperations = Object.values(Operations).filter(o => typeof o === 'number') as number[];
  public wording = wording;
  public users: IUser[] = [];
  public usersRoles: IUserRole[] = [];
  public roles: IRole[] = [];
  public tabs = [];

  constructor(
    private nzModalRef: NzModalRef,
    private roleRepository: RoleRepositoryService,
    private userRoleRepository: UserRoleRepositoryService,
    private userRepository: UserRepositoryService,
  ) {
  }

  public ngOnInit(): void {
    this.zoneOfSelectedTopic = SitemapEntry.getByNode(this.selectedTopic).toZone();
    this.getAllUserRoles();
    this.initTabs(this.selectedTopic);
    specialZones.forEach(zone => this.initTabs(zone));
  }

  public initTabs(sitemapNode: SitemapNode) {
    this.tabs = addTabsFullZoneAndTitle(sitemapNode, this.tabs);
  }

  private hasPermission = (permission: number, operation: Operations): boolean => {
    return (permission & operation) === operation;
  }

  private getAllUserRoles(): void {
    forkJoin([
      this.userRoleRepository.fetchAll({}),
      this.userRepository.fetchAll({}),
      this.roleRepository.fetchAll({}),
    ]).subscribe(([usersRoles, users, roles]) => {
      this.usersRoles = usersRoles;
      this.users = users;
      this.roles = roles;
      this.setUsersWithPermissions(users, usersRoles, roles);
    });
  }

  public onTabIndexChange(index: number) {
    this.zoneOfSelectedTopic = this.tabs[index].fullZone;
    this.setUsersWithPermissions(this.users, this.usersRoles, this.roles);
  }

  public setUsersWithPermissions(users: IUser[], usersRoles: IUserRole[], roles: IRole[]) {
    this.usersWithPermissions = users.map(user => {
      const userPermissions = usersRoles.filter(item => item.userId === user.id).map(item => {
        const userRole = roles.find(role => role.id === item.roleId);
        return userRole.permissions[this.zoneOfSelectedTopic];
      });

      /***
       * We can just do `bitwise-or` for all permissions and get maximal permission number
       *
       * problem: CR | RU = CRU
       * evidence:
       *   number | number = number;
       *   CR <=> C | R and CRU = C | R | U
       *   CR | RU = C | R | R | U = C | R | U = CRU
       * example:
       *   CR = C | R = 1 | 2 = 3
       *   RU = R | U = 2 | 4 = 6
       *   CR | RU = C | R | R | U = C | R | U = 1 | 2 | 4 = 7 = 3 | 6 = CR | RU
       */
      const permission = userPermissions.reduce((a, b) => (a | b), 0);
      const foundSpecialZone = specialZones.find(specialZone => specialZone.fullZone === this.zoneOfSelectedTopic);

      return {
        ...user,
        hasCreate: this.hasPermission(permission, Operations.CREATE),
        isCreateVisible: !!(!foundSpecialZone || (foundSpecialZone.maxOperations & Operations.CREATE) === Operations.CREATE),
        hasRead: this.hasPermission(permission, Operations.READ),
        isReadVisible: !!(!foundSpecialZone || (foundSpecialZone.maxOperations & Operations.READ) === Operations.READ),
        hasUpdate: this.hasPermission(permission, Operations.UPDATE),
        isUpdateVisible: !!(!foundSpecialZone || (foundSpecialZone.maxOperations & Operations.UPDATE) === Operations.UPDATE),
        hasDelete: this.hasPermission(permission, Operations.DELETE),
        isDeleteVisible: !!(!foundSpecialZone || (foundSpecialZone.maxOperations & Operations.DELETE) === Operations.DELETE),
      };
    });
  }

  public closeModal = (): void => this.nzModalRef.close();
}
