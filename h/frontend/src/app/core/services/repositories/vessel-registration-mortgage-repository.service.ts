import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { VesselKey } from '../../models/resources/IVessel';
import { IVesselRegistrationMortgage, IVesselRegistrationMortgageCreate, IVesselRegistrationMortgageDTO } from '../../models/resources/IVesselRegistrationMortgage';
import { RepositoryService } from '../repository.service';

export interface IVesselRepoParams extends RepoParams<IVesselRegistrationMortgage> {
  queryParams?: {
    vesselId: VesselKey;
  };
}

@Injectable({
  providedIn: 'root'
})
/**
  * DOESN;T HAVE DELETE
 */
export class VesselRegistrationMortgageRepositoryService extends AbstractRepository<
IVesselRegistrationMortgage,
IVesselRegistrationMortgageDTO,
IVesselRegistrationMortgageCreate,
IVesselRepoParams,
VesselKey
> {
  protected resourceType = 'VesselRegisterMortgages';
  public sortAsce = true;
  public sortKey = 'imo';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/vessel-register-mortgages';
  }

  protected postProcess(
    item: IVesselRegistrationMortgageDTO
  ): IVesselRegistrationMortgage {
    return item;
  }

}
