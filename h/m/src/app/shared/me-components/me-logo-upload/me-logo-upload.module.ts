import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeLogoUploadComponent } from './me-logo-upload.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MeButtonModule } from '@shared/me-components/me-button/me-button.module';
import { MeDragDropModule } from '@shared/me-directives/me-drag-drop/me-drag-drop.module';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';

@NgModule({
  declarations: [MeLogoUploadComponent],
  imports: [CommonModule, MatProgressBarModule, MeButtonModule, MeDragDropModule, MeIconsProviderModule],
  exports: [MeLogoUploadComponent]
})
export class MeLogoUploadModule {}
