import { main } from './main';
import { accounting } from './accounting';
import { payments } from './payments';
import { examples } from './examples';
import { chartering } from './chartering';
import { technicalManagement } from './technicalManagement';
import { system } from './system';
import { IWording } from 'src/app/core/models/resources/IWording';

export interface SitemapNode {
  path: string; // the url-segment
  zone: string;
  fullZone?: string; // e.g.: `accounting.mandator.bookingCodes`, it overrides the concatenated `zone` values.
  title: IWording;
  fullTitle?: IWording;
  isFakeChild?: boolean;
  description?: IWording;
  icon?: string;
  keywords?: string[];
  freeAccess?: boolean; // These nodes can be accessed by every user ( no permissions required )
  hidden?: boolean; // hidden topics don't appear in the NavigationBoard
  children?: { [key: string]: SitemapNode };
  parentFullZone?: string;
  maxOperations?: number;
  /**
   * The tooltip usually shown on tab buttons that contains info
   * when user isn't allowed to access that tab or it's just disabled
  */
  notAllowedTabMessage?: IWording;
}

// crazy typing: see https://stackoverflow.com/questions/52146544/why-autocomplete-stop-working-in-an-object-with-type-in-typescript
// we need this so we can type the sitemap and at the same time get intellisense when using the sitemap object.
// ( typing the sitemap like this 'sitemap: {[key: string]: SitemapCategory}' breaks intellisense )
export function asSitemap<T extends { [key: string]: SitemapNode }>(arg: T): T { return ( arg ); }

export const sitemap = asSitemap({
  main,
  accounting,
  payments,
  chartering,
  technicalManagement,
  system,
  examples,
  // invoiceManagement,
});
