import { DebitorCreditorKey } from './../../models/resources/IDebitorCreditor';
import { Injectable } from '@angular/core';
import { IDebitorCreditor, IDebitorCreditorDTO, IDebitorCreditorCreate } from '../../models/resources/IDebitorCreditor';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { CompanyKey } from '../../models/resources/ICompany';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DebitorCreditorRepositoryService extends AbstractRepository<
  IDebitorCreditor,
  IDebitorCreditorDTO,
  IDebitorCreditorCreate,
  RepoParams<IDebitorCreditor>,
  DebitorCreditorKey
> {
  protected resourceType = 'DebitorCreditor';
  public sortAsce = true;
  public sortKey = 'no';
  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess(item: IDebitorCreditorDTO): IDebitorCreditor { return item; }

  public getBaseUrl(): string {
    return '/api/debitor-creditors/';
  }

  public getByCompanyId(companyId: CompanyKey): Observable<IDebitorCreditor[]> {
    return super.fetchAll({ queryParams: { companyId } });
  }
}
