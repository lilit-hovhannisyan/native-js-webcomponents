import { CompanyKey } from 'src/app/core/models/resources/ICompany';
import { AddressKey } from './../../models/resources/IAddress';
import { Injectable } from '@angular/core';
import { IAddress, IAddressDTO, IAddressCreate } from '../../models/resources/IAddress';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';

export interface AddressParams extends RepoParams<IAddress> {
  companyId?: CompanyKey;
}

@Injectable({
  providedIn: 'root'
})
export class AddressRepositoryService extends AbstractRepository<
  IAddress,
  IAddressDTO,
  IAddressCreate,
  AddressParams,
  AddressKey
> {
  protected resourceType = 'Address';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(params: AddressParams): string {
    return `/api/addresses`;
  }

  public postProcess(item: IAddressDTO): IAddress { return item; }

}
