import { Injectable } from '@angular/core';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { ICommissionTypeCreate, ICommissionType, ICommissionTypeDTO, CommissionTypeKey } from '../../models/resources/ICommissionType';

@Injectable({
  providedIn: 'root'
})
export class CommissionTypeRepositoryService extends AbstractRepository<
  ICommissionType,
  ICommissionTypeDTO,
  ICommissionTypeCreate,
  RepoParams<ICommissionType>,
  CommissionTypeKey
>  {
  protected resourceType = 'CommissionType';
  public sortAsce = true;
  public sortKey = 'code';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = () => '/api/commission-types';

  protected postProcess = (item: ICommissionTypeDTO): ICommissionType => item;
}
