import { Injectable } from '@angular/core';
import { forkJoin, Observable, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { BookingAccountRepositoryService } from '../repositories/booking-account-repository.service';
import { CurrencyRepositoryService } from '../repositories/currency-repository.service';
import { IBookingAccount, IBookingAccountWithCurrency } from '../../models/resources/IBookingAccount';
import { ICurrency } from '../../models/resources/ICurrency';
import { getLabelKeyByLanguage } from '../../helpers/general';
import { sortBy } from 'lodash';
import { MandatorAccountRepositoryService } from '../repositories/mandator-account-repository.service';
import { MandatorKey } from '../../models/resources/IMandator';
import { SettingsService } from '../settings.service';



@Injectable({
  providedIn: 'root'
})
export class BookingAccountMainService {

  constructor(
    private bookingAccountRepo: BookingAccountRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private mandatorAccountRepositoryService: MandatorAccountRepositoryService,
    private settingsService: SettingsService
  ) { }

  public fetchAll(): Observable<IBookingAccount[]> {
    return forkJoin([
      this.bookingAccountRepo.fetchAll({}),
      this.currencyRepo.fetchAll({}),
    ]).pipe(map(entityCollections => this.getBookingAccountsWithCurrency(...entityCollections)));
  }

  public getStream(): Observable<IBookingAccount[]> {
    return forkJoin([
      this.bookingAccountRepo.fetchAll({}),
      this.currencyRepo.fetchAll({})
    ]).pipe(
      switchMap(() => combineLatest([
        this.bookingAccountRepo.getStream(),
        this.currencyRepo.getStream(),
      ])),
      map(entityCollections =>
        sortBy(this.getBookingAccountsWithCurrency(...entityCollections),
          [
            e => e[getLabelKeyByLanguage(this.settingsService.language)].toLowerCase()
          ])
      )
    );
  }

  private getBookingAccountsWithCurrency(
    bookingAccounts: IBookingAccount[],
    currencies: ICurrency[],
  ): IBookingAccountWithCurrency[] {

    return bookingAccounts.map(bookingAccount => {
      const foundCurrency = currencies
        .find(currency => currency.id === bookingAccount.currencyId);

      return {
        ...bookingAccount,
        currencyCode: foundCurrency ? foundCurrency.displayLabel : null,
      };
    });
  }

  public getBookingAccountsForMandator(mandatorId: MandatorKey): Observable<IBookingAccount[]> {
    return forkJoin([
      this.fetchAll(),
      this.mandatorAccountRepositoryService.fetchAll({ queryParams: { mandatorId } }),
    ]).pipe(
      map(([bookingAccounts, accounts]) => {
        return bookingAccounts.filter(bookingAccount => {
          return accounts.find(account => account.bookingAccountId === bookingAccount.id);
        });
      })
    );
  }
}
