import { HttpPost } from './../models/http/http-post';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { wording } from '../constants/wording/wording';
import { getCategoryByUrl } from '../constants/sitemap/sitemap-entry';
import { IWordingResource, WordingMap, IWordingEntry } from '../models/resources/IWording';
import { IDataResponseBody } from '../models/responses/IDataResponseBody';
import { filter, map, tap, catchError, finalize, shareReplay } from 'rxjs/operators';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { IResourceCollection } from '../models/resources/IResourceCollection';

import { HttpGet } from '../models/http/http-get';
import { HttpPut } from '../models/http/http-put';

enum LoadingStatus {
  loaded,
  loading,
  notLoaded // to be used if httpRequest failes
}

// to be used as abbreviations
type TranslationsResponse = HttpResponse<IDataResponseBody<IResourceCollection<IWordingResource>>>;

@Injectable({
  providedIn: 'root'
})
export class WordingService {

  private loadingStates: { [category: string]: LoadingStatus } = {};

  private wordingIsLoading = new BehaviorSubject(false);
  // used to stop the rendering of a category route until its wording is loaded
  public wordingIsLoading$ = this.wordingIsLoading.asObservable();

  public readonly wording = wording;

  constructor(
    private router: Router,
    private httpClient: HttpClient,
  ) { }

  public init() {
    this.loadWording(getCategoryByUrl(this.router.url));
    this.loadWording('general');

    this.router.events
      .pipe(filter(routerEvent => routerEvent instanceof NavigationEnd))
      .subscribe((routerEvent: NavigationEnd) => {
        const category = getCategoryByUrl(routerEvent.url);

        if (this.needsToGetLoaded(category)) {
          this.loadWording(category);
        }
      });
  }

  public loadWording(category: string): Observable<WordingMap> {

    return; // -> reactivate to allow dynamic wording editing

    // always return cached version if data is already loaded.
    if (this.loadingStates[category] === LoadingStatus.loaded) {
      return of(this.wording[category]);
    }

    this.loadingStates[category] = LoadingStatus.loading;
    this.updateLoadingFlag();


    const GETRequest = new HttpGet(`/api/translations?category=${category}`);

    GETRequest.setErrorHandler(error => {
      // makes the http-interceptor neglect 404 errors, as for the reason described underneath
      // so no error-Toasts pops up for the user.
      if (error.status !== 404) { throw error; }
    });

    const request = this.httpClient.request(GETRequest)
      .pipe(
        shareReplay(1),
        catchError(error => {
          // if there is not at least one wording in DB for the requested category.
          // ( This is intentional, expected REST behaviour )
          // the server responds withh a 404. So we transform it to an empty response.
          return (error.status === 404)
            ? of({ body: { data: { items: [] } } }) // empty IResourceCollection as response
            : throwError(error);
        }),
        tap(
          () => { this.loadingStates[category] = LoadingStatus.loaded; },
          () => { this.loadingStates[category] = LoadingStatus.notLoaded; } // the error case
        ),
        finalize(() => this.updateLoadingFlag()),
        map((response: TranslationsResponse) => response.body.data.items),
        tap((wordingElements: IWordingResource[]) => this.populateWordings(category, wordingElements)),
        map(() => wording[category])
      );

    request.subscribe(
      (_wording: WordingMap) => { this.wording[category] = _wording; },
      (error) => { console.log(error); }
    );
    return request;
  }

  public needsToGetLoaded(category: string): boolean {
    return (
      this.loadingStates[category] !== LoadingStatus.loaded &&
      this.loadingStates[category] !== LoadingStatus.loading
    );
  }

  private updateLoadingFlag(): void {
    const currentCategory = getCategoryByUrl(this.router.url);
    const isNotLoaded = (
      this.loadingStates[currentCategory] !== LoadingStatus.loaded ||
      this.loadingStates['general'] !== LoadingStatus.loaded
    );

    this.wordingIsLoading.next(isNotLoaded);
  }

  public populateWordings(category: string, wordingsArray: IWordingResource[]): void {
    wordingsArray.forEach(w => {
      if (wording[category][w.key]) {
        wording[category][w.key] = { id: w.id, en: w.en, de: w.de };
      }
    });
  }

  public updateWording(_wording: IWordingEntry, category: string): Observable<IWordingEntry> {
    const payload: IWordingResource = {
      _type: 'Translation',
      id: _wording.id || 0, // zero, in case the wording is not exist in server-database.
      key: _wording.key,
      en: _wording.en,
      de: _wording.de,
      categoryId: category
    };

    const request = _wording.id
      ? new HttpPut(`/api/translations/${payload.id}`, payload)
      : new HttpPost(`/api/translations`, payload);
    return this.httpClient.request(request).pipe(
      map((response: HttpResponse<IDataResponseBody<IWordingResource>>) => response.body.data),
      tap(w => this.wording[category][_wording.key] = { ...w }),
      map(w => w)
    );
  }
}
