import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getExchangeRatesGridConfig = (isoCode: string): NvGridConfig => {
  return ({
    gridName: 'currencyExchangeRatesGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 50
    },
    columns: [
      {
        key: 'date',
        title: wording.general.rateDate,
        width: 150,
        isSortable: true,
        dataType: NvColumnDataType.Date,
        filter: {
          values: []
        }
      },
      {
        key: 'rate',
        title: `EUR/${isoCode}`,
        width: 150,
        dataType: NvColumnDataType.Decimal,
        isSortable: true,
        filter: {
          values: []
        },
        decimalDigitsInfo: '1.5-5',
      },
      {
        key: 'rateUSD',
        title: `USD/${isoCode}`,
        width: 150,
        dataType: NvColumnDataType.Decimal,
        isSortable: true,
        filter: {
          values: []
        },
        decimalDigitsInfo: '1.5-5',
      },
      {
        key: 'issuingDate',
        title: wording.general.importDate,
        width: 150,
        dataType: NvColumnDataType.Date,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'source',
        title: wording.general.source,
        width: 150,
        dataType: NvColumnDataType.String,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
    buttons: [],
    toolbarButtons: []
  });
};
