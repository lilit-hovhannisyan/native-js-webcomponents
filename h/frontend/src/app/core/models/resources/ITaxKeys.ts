import { IWording } from 'src/app/core/models/resources/IWording';
import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithSystemFlag } from './IWithSystemFlag';
import { IWithActive } from './IWithActive';
import { IWithLabel } from './IWithLabel';

export type TaxKeyKey = number;

export type AccountKey = number;
interface ISharedProps extends IWithLabels, IWithSystemFlag, IWithActive {
  code: string;
  percentage: number;
  calculationType: CALCULATION_TYPES;
  taxCategory: TAX_CATEGORIES;
  bookingAccountId: number;
}
export enum CALCULATION_TYPES {
  include,
  exclude,
}
export enum TAX_CATEGORIES {
  inputTax,
  valueAddedTax,
}
export interface ITaxKeyBase extends ISharedProps, IResource<number> { }

export interface ITaxKeyCreate extends ISharedProps { }

export interface ITaxKeyDTO extends ITaxKeyBase { }

export interface ITaxKey extends ITaxKeyDTO, IWithLabel { }

export interface ITaxKeyRow extends ITaxKey {
  taxCategoryLabel: IWording;
  calculationTypeLabel: IWording;
}

