import { Injectable } from '@angular/core';
import { MeDocumentModel } from '@shared/models/document/document.model';

import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DocumentStoreService {
  private readonly _documentEntities = new BehaviorSubject<MeDocumentModel[]>([]);
  readonly documentEntities$ = this._documentEntities.asObservable();

  private readonly _documentNextId = new BehaviorSubject<number>(0);
  readonly documentNextId$ = this._documentNextId.asObservable();

  get documentEntities(): MeDocumentModel[] {
    return this._documentEntities.getValue();
  }

  set documentEntities(data: MeDocumentModel[]) {
    this._documentEntities.next(data);
  }

  appendDocumentEntities(data: MeDocumentModel[]): void {
    this.documentEntities = [...this.documentEntities, ...data];
  }

  get documentNextId(): number {
    return this._documentNextId.getValue();
  }

  set documentNextId(data: number) {
    this._documentNextId.next(data);
  }
}
