import { Injectable } from '@angular/core';
import { IBankAccount, IBankAccountRelation } from '../../models/resources/IBankAccount';
import { MandatorKey } from '../../models/resources/IMandator';
import { AccountingRelationRepositoryService } from '../repositories/accounting-relations-repository.service';
import { CurrencyRepositoryService } from '../repositories/currency-repository.service';
import { BankRepositoryService } from '../repositories/bank-repository.service';


@Injectable({
  providedIn: 'root'
})
export class AccountingRelationMainService {

  constructor(
    private accountingRelationRepo: AccountingRelationRepositoryService,
    private currencyRepo: CurrencyRepositoryService,
    private bankRepo: BankRepositoryService,
  ) { }

  /** setAccountingRelation
   * sets mandatorId / bookingAccountId / bankAccountNo / relationId
   * to each bank account
   * so the accounting relations grid can read it and modify it
   * */
  public setAccountingRelation(
    bankAccount: IBankAccount,
    mandatorId: MandatorKey,
    tempBankAccounts: IBankAccountRelation[] = []
  ): IBankAccountRelation {

    const foundTempRelation = tempBankAccounts
      .find(tempBankAccount =>
        tempBankAccount.mandatorId === mandatorId
        && tempBankAccount.id === bankAccount.id
      );

    const foundAccountingRelation = foundTempRelation
      || this.accountingRelationRepo.getStreamValue()
        .find(accountingRelation =>
          accountingRelation.mandatorId === mandatorId
          && accountingRelation.bankAccountId === bankAccount.id);

    // Note: id of this object is the bank account id
    return ({
      ...bankAccount,
      mandatorId: mandatorId,
      bookingAccountId: foundAccountingRelation?.bookingAccountId,
      bankAccountNo: foundAccountingRelation
        ? foundAccountingRelation.bankAccountNo
        : bankAccount.accountNo
          ? this.removeLeadingZeros(bankAccount.accountNo)
          + this.handleSpecialCase(bankAccount)
          : null,
      relationId: foundAccountingRelation?.id
    });
  }

  private handleSpecialCase(bankAccount: IBankAccount): string {
    const foundBank = this.bankRepo.getStreamValue()
      .find(bank => bank.id === bankAccount.bankId);
    if (foundBank.bic.startsWith('DEUTDE')) {
      const eurCurrency = this.currencyRepo.getStreamValue()
        .find(currency => currency.isoCode === 'EUR');
      const usdCurrency = this.currencyRepo.getStreamValue()
        .find(currency => currency.isoCode === 'USD');
      const jpyCurrency = this.currencyRepo.getStreamValue()
        .find(currency => currency.isoCode === 'JPY');
      const chfCurrency = this.currencyRepo.getStreamValue()
        .find(currency => currency.isoCode === 'CHF');
      switch (bankAccount.currencyId) {
        case eurCurrency.id: return '00';
        case usdCurrency.id: return '05';
        case jpyCurrency.id: return '88';
        case chfCurrency.id: return '01';
        default: return '';
      }
    } else {
      return '';
    }
  }

  private removeLeadingZeros(text: string): string {
    return text.replace(/^0+/, '');
  }
}
