import { IResource } from './IResource';
import { MandatorKey } from './IMandator';
import { DebitorCreditorKey } from './IDebitorCreditor';
import { BookingAccountKey } from './IBookingAccount';
import { CompanyKey } from './ICompany';
import { PaymentType } from '../enums/payment-type';
import { BankAccountKey } from './IBankAccount';
import { CountryKey } from './ICountry';
import { BookingCodeKey } from './IBookingCode';
import { AutobankFieldTypeKey } from './IAutobankFieldType';
import { AutobankOperationTypeKey } from './IAutobankOperationType';
import { IMeta } from './IMeta';
import { AutobankBusinessCaseKey } from './IAutobankBusinessCase';

export type AutobankRuleKey = number;

interface ISharedProps extends IMeta {
  isActive: boolean;
  mandatorId: MandatorKey;
  companyId: CompanyKey;
  filterName: string;
  isValidForAllBankAccounts: boolean;
  bankAccountId: BankAccountKey;
  autobankPaymentType: PaymentType;
  autobankRuleFields: any[];
  bookingMandatorId: MandatorKey;
  bookingAccountId: BookingAccountKey;
  debitorCreditorId: DebitorCreditorKey;
  bookingCodeId: BookingCodeKey;
  costCenter: string;
  countryId: CountryKey;
  bookingText: string;
}

export interface IAutobankRuleBase extends ISharedProps, IResource<AutobankRuleKey> { }

export interface IAutobankRuleCreate extends ISharedProps { }

export interface IAutobankRuleDTO extends IAutobankRuleBase { }

export interface IAutobankRule extends IAutobankRuleDTO { }

export interface IAutobankRuleFilter {
  id: number;
  fieldTypeId: AutobankFieldTypeKey;
  autobankRuleId: AutobankRuleKey;
  operationTypeId: AutobankOperationTypeKey;
  value: string;
  autobankBusinessCaseId: AutobankBusinessCaseKey;
}
