import { ChangeDetectionStrategy, EventEmitter, Output, Component, Input, Injector } from '@angular/core';
import { BaseLoggerComponent } from '@features/tracking-system';

import { MeCollectionCardI } from './me-collection-card.interface';

@Component({
  selector: 'me-collection-card',
  templateUrl: './me-collection-card.component.html',
  styleUrls: ['./me-collection-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeCollectionCardComponent extends BaseLoggerComponent {
  componentVersion: string = '0.0.1';
  @Input() dataModel: MeCollectionCardI = {
    selectionEnabled: false,
    contextMenu: {
      contextMenuItems: []
    },

    oid: '',
    userProfile: {
      type: 'initials',
      size: 'small',
      shape: 'circled',
      initials: 'NA',
      imageUrl: '',
      alt: ''
    },
    creator: '',
    name: '',
    numberOfMaterials: 0,
    numberOfCustomers: 0,
    numberOfTeams: 0,
    restMaterialsNumber: 0,
    firstThreeMaterialsThumbnailUrl: [],
    lastUpdatedPlaceholder: 'Last updated',
    dateDiff: '',
    selected: false
  };
  @Output() eventOccurs: EventEmitter<{ eventName: string; payload?: string }> = new EventEmitter();

  constructor(injector: Injector) {
    super(injector);
  }

  customEvent(eventName: string, payload?: string): void {
    this.eventOccurs.emit({ eventName, payload });
  }
}
