export const environment = {
  production: true,
  apiHost: 'http://nav1-dev.westeurope.cloudapp.azure.com:8081',
  apiBasicAuth: 'navido:navido2_hamburg',
  PREFER_FIXTURES: false,
};
