import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { Subscription } from 'rxjs';

import { TranslateService } from '@ngx-translate/core';

import { AuthStoreService } from '@core-services/auth-store.service';
import { environment } from '@environments/environment';

@Component({
  selector: 'me-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'market-place';
  private _getI18nStringsSubscription!: Subscription;

  @HostListener('window:beforeunload', ['$event'])
  perseveUser(): void {
    if (this.authStoreService.user) {
      sessionStorage.setItem('loggedIn', 'true');
    }
  }

  constructor(private translateService: TranslateService, private titleService: Title, private authStoreService: AuthStoreService) {
    // add list of languages for which we have json files available
    translateService.addLangs(['en', 'zh']);
    translateService.setDefaultLang('en');

    if (!environment.production) {
      console.log(`FE APP VERSION TAG: ${environment.BUILD_NUMBER}`);
    }
  }

  ngOnInit(): void {
    this.getTranslations();

    this.translateService.onLangChange.subscribe(() => {
      this.getTranslations();
    });
  }

  getTranslations(): void {
    this._getI18nStringsSubscription = this.translateService.get(['browserTitle']).subscribe((translations) => {
      this.titleService.setTitle(translations[`browserTitle`]);
    });
  }

  ngOnDestroy(): void {
    if (this._getI18nStringsSubscription) {
      this._getI18nStringsSubscription.unsubscribe();
    }
  }
}
