import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatProgressBarModule } from '@angular/material/progress-bar';
import { IconsModule } from '@shared-modules/icons.module';

import { MeButtonModule } from '../me-button/me-button.module';
import { MeUploadImageComponent } from './me-upload-image.component';
import { MeDragDropModule } from '@shared-directives/me-drag-drop/me-drag-drop.module';

@NgModule({
  declarations: [MeUploadImageComponent],
  imports: [CommonModule, MatProgressBarModule, MeButtonModule, IconsModule, MeDragDropModule],
  exports: [MeUploadImageComponent]
})
export class MeUploadImageModule {}
