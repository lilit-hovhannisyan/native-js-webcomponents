import { NgModule } from '@angular/core';

import { MeIconsModule } from '@shared-components/me-icons/me-icons.module';
import { allIcons } from '@shared-components/me-icons/icons/all-icons';

@NgModule({
  imports: [MeIconsModule.pick(allIcons)],
  exports: [MeIconsModule]
})
export class MeIconsProviderModule {}
