import { Component, OnInit, Input } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { IArticleType } from 'src/app/core/models/resources/IArticleType';
import { IArticleClass } from 'src/app/core/models/resources/IArticleClass';
import { IArticleUnit } from 'src/app/core/models/resources/IArticleUnit';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ArticleUnitsRepositoryService } from 'src/app/core/services/repositories/article-units-repository.service';
import { ArticleClassRepositoryService } from 'src/app/core/services/repositories/article-class-repository.service';
import { ArticleTypeRepositoryService } from 'src/app/core/services/repositories/article-type-repository.service';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { ArticleRepositoryService } from 'src/app/core/services/repositories/article-repository.service';
import { IArticle } from 'src/app/core/models/resources/IArticle';
import { wording } from 'src/app/core/constants/wording/wording';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'nv-repair-article-view',
  templateUrl: './repair-article-view.component.html',
  styleUrls: ['./repair-article-view.component.scss']
})
export class RepairArticleViewComponent implements  OnInit {

  @Input() public articleId: number;

  public articleUnits$: Observable<IArticleUnit[]>;
  public articleClasses$: Observable<IArticleClass[]>;
  public articleTypes$: Observable<IArticleType[]>;
  public currencies$: Observable<ICurrency[]>;
  public form: FormGroup;
  public wording = wording;

  constructor(
    private fb: FormBuilder,
    private articleUnitRepo: ArticleUnitsRepositoryService,
    private articleClassRepo: ArticleClassRepositoryService,
    private articleTypeRepo: ArticleTypeRepositoryService,
    private currenciesRepo: CurrencyRepositoryService,
    private articleRepo: ArticleRepositoryService,
  ) {
  }

  public ngOnInit() {
    this.articleUnits$ = this.articleUnitRepo.fetchAll({});
    this.articleTypes$ = this.articleTypeRepo.fetchAll({});
    this.articleClasses$ = this.articleClassRepo.fetchAll({});
    this.currenciesRepo.fetchAll({}).subscribe();
    this.articleRepo.fetchOne(this.articleId, {}).subscribe(article => {
      this.init(article);
    });
  }

  public init(article: IArticle = {} as IArticle): void {
    this.form = this.fb.group({
      articleUnitId: [article.articleUnitId],
      articleClassId: [article.articleClassId],
      articleTypeId: [article.articleTypeId],
      description: [article.description],
      salePrice: [article.salePrice],
      currencyId: [article.currencyId],
    });
    this.loadCurrencies();
    this.form.disable();
  }

  private loadCurrencies(): void {
    const currencyControl = this.form.controls.currencyId;
    this.currencies$ = combineLatest([
      this.currenciesRepo.getStream(),
      currencyControl.valueChanges.pipe(startWith(currencyControl.value))
    ]).pipe(
      map(([currencies, currencyId]) => this.currenciesRepo.filterActiveOnly(currencies, currencyId))
    );
  }

}
