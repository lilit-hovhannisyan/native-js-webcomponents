import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { NvColumnDataType, NvFilterControl, NvGridButtonsPosition, NvGridConfig } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IResource } from 'src/app/core/models/resources/IResource';


export const getStevedoreDamageDocumentGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'StevedoreDocumentGridConfig',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'isAttached',
        title: wording.technicalManagement.attach,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'filename',
        title: wording.technicalManagement.filename,
        width: 230,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'uploadedOn',
        title: wording.technicalManagement.uploadedOn,
        width: 200,
        isSortable: true,
        dataType: NvColumnDataType.DateTime,
        filter: {
          values: []
        },
      },
      {
        key: 'remark',
        title: wording.technicalManagement.remarks,
        width: 223,
        isSortable: true,
        filter: {
          values: []
        },
      },
    ],
    buttons: [
      {
        icon: 'download',
        description: wording.general.download,
        name: 'downloadStevedoreDocument',
        tooltip: wording.general.download,
        actOnEnter: true,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.UPDATE),
        func: (entity: IResource<number>) => actions.request(entity),
      },
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editStevedoreDocument',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.UPDATE),
        func: (entity: IResource<number>) => actions.edit(entity),
        disabled: true,
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteStevedoreDocument',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: (entity: IResource<number>) => actions.delete(entity),
        disabled: true,
      },
    ],
    toolbarButtons: [
      {
        title: wording.technicalManagement.upload,
        tooltip: wording.technicalManagement.upload,
        icon: 'upload',
        hidden: () => !canPerform(Operations.CREATE),
        func: () => actions.add(),
        disabled: () => actions.isInEditMode(),
      }
    ]
  });
};
