import { Pipe, PipeTransform } from '@angular/core';

/**
 * This is typically needed in the context of error handling to access the
 * first error on a field. Generally the order is trivial.
 */
@Pipe({
  name: 'objectFirstEl'
})
export class ObjectFirstElPipe implements PipeTransform {
  public transform(value: any): any {
    return value[Object.keys(value)[0]];
  }
}
