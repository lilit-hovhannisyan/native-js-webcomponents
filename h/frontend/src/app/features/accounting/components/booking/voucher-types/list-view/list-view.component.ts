import { VoucherTypeRepositoryService } from '../../../../../../core/services/repositories/voucher-type-repository.service';
import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NvGridConfig, GridComponent } from 'nv-grid';
import { Observable } from 'rxjs';
import { IVoucherType, VoucherTypeKey } from 'src/app/core/models/resources/IVoucherType';
import { NzModalService } from 'ng-zorro-antd/modal';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/voucher-types.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { SettingsService } from 'src/app/core/services/settings.service';

@Component({
  selector: 'nv-voucher-types-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VoucherTypesListViewComponent implements OnInit {
  public config: NvGridConfig;
  public dataSource$: Observable<IVoucherType[]>;
  @Input() public selectMode: boolean;
  @Input() public viewMode: boolean;
  @Input() public voucherTypeId: number;
  @Output() public voucherSelect: EventEmitter<IVoucherType> = new EventEmitter();
  @ViewChild('grid', { static: true }) public gridComponent: GridComponent;
  public wordingGeneral = wording.general;
  public wordingAccounting = wording.accounting;

  constructor(
    private modalService: NzModalService,
    private voucherTypeRepo: VoucherTypeRepositoryService,
    public gridConfigService: GridConfigService,
    private router: Router,
    private authService: AuthenticationService,
    private confirmationService: ConfirmationService,
    private settingsService: SettingsService
  ) { }

  public ngOnInit() {
    this.voucherTypeRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.voucherTypeRepo.getStream();
    this.initConfig();
  }

  private initConfig() {

    const actions = {
      edit: (id: VoucherTypeKey) => this.router.navigate([`${this.router.url}/${id}`]),
      delete: this.deleteClicked,
      add: () => this.router.navigate([`${this.router.url}/new`])
    };

    this.config = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  private deleteClicked = (voucherType: IVoucherType) => {
    if (voucherType.isSystemDefault && !this.authService.getUser().isSuperAdmin) {
      this.modalService.warning({
        nzTitle: this.wordingAccounting.canNotDeleteSystemVoucherTypes[this.settingsService.language],
      });
      return;
    }

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: voucherType.displayLabel },
    }).subscribe(confirmed => confirmed && this.voucherTypeRepo.delete(voucherType.id, {}).subscribe());
  }
}
