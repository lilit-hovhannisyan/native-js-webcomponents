import { Component, Input, OnInit } from '@angular/core';
import { ICompanyRepresentativeRow } from 'src/app/core/models/resources/ICompanyRepresentative';
import { Observable } from 'rxjs';
import { NvGridConfig } from 'nv-grid';
import { CompanyRepresentativeMainService } from '../../../../../../../../core/services/mainServices/company-representative-main.service';
import { CompanyRepresentativeRepositoryService } from '../../../../../../../../core/services/repositories/company-representative-repository.service';
import { ConfirmationService } from 'src/app/core/services/confirmation.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { map, tap } from 'rxjs/operators';
import { wording } from 'src/app/core/constants/wording/wording';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { getCompanyRepresentativesGridConfig } from 'src/app/core/constants/nv-grid-configs/company-representatives.config';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { COMPANY_LEGAL_FORM } from 'src/app/core/models/enums/company-legal-form';
import { IWording } from 'src/app/core/models/resources/IWording';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { RepresentativesDetailViewModalComponent } from './representatives-detail-view-modal/representatives-detail-view-modal.component';
import { ICompany, CompanyKey } from 'src/app/core/models/resources/ICompany';
import { CompaniesHelperService } from '../../companies.helper.service';

@Component({
  selector: 'nv-representatives',
  templateUrl: './representatives.component.html',
  styleUrls: ['./representatives.component.scss']
})
export class RepresentativesComponent implements OnInit {
  public dataSource$: Observable<ICompanyRepresentativeRow[]>;
  public representativesGridConfig: NvGridConfig;
  public title?: IWording;
  private singularTitle?: IWording;
  private definePrivatePersonTitle?: IWording;
  private memberToDefine?: IWording;
  private company: ICompany;
  private representatives: ICompanyRepresentativeRow[];

  @Input() public companyId: CompanyKey;

  constructor(
    private companyRepresentativeService: CompanyRepresentativeMainService,
    private companyRepresentativeRepo: CompanyRepresentativeRepositoryService,
    private confirmationService: ConfirmationService,
    public gridConfigService: GridConfigService,
    private authService: AuthenticationService,
    private companyRepo: CompanyRepositoryService,
    private modalService: NzModalService,
    private companiesHelperService: CompaniesHelperService,
  ) { }

  public ngOnInit(): void {
    this.initTitle();
    this.initGrid();
  }

  private initTitle(): void {
    this.companyId = this.companyId || this.companiesHelperService.company.id;

    this.companyRepo.fetchOne(this.companyId, {})
      .subscribe((company: ICompany) => {
        this.company = company;

        this.companiesHelperService.setCompany(this.companyId);

        switch (this.company.legalForm) {
          case COMPANY_LEGAL_FORM.LimitedPartnership:
            this.title = wording.system.managingPartnersAndRepresentatives;
            this.singularTitle = wording.system.managingPartnerAndRepresentative;
            this.definePrivatePersonTitle = wording.system
              .defineManagingPartnerAndRepresentative;
            this.memberToDefine = wording.system.managingPartnerOrRepresentative;
            break;
          case COMPANY_LEGAL_FORM.CompanyWithLimitedLiability:
            this.title = wording.system.managingPartnersAndRepresentatives;
            this.singularTitle = wording.system.managingPartnerAndRepresentative;
            this.definePrivatePersonTitle = wording.system
              .defineManagingPartnerAndRepresentative;
            this.memberToDefine = wording.system.managingPartnerOrRepresentative;
            break;
          case COMPANY_LEGAL_FORM.CivilLawPartnership:
            this.title = wording.system.managingPartnersAndRepresentatives;
            this.singularTitle = wording.system.managingPartnerAndRepresentative;
            this.definePrivatePersonTitle = wording.system
              .defineManagingPartnerAndRepresentative;
            this.memberToDefine = wording.system.managingPartnerOrRepresentative;
            break;
          case COMPANY_LEGAL_FORM.StockCorporation:
            this.title = wording.system.membersOfManagementBoard;
            this.singularTitle = wording.system.memberOfManagementBoard;
            this.definePrivatePersonTitle = wording.system.defineMemberOfManagementBoard;
            this.memberToDefine = wording.system.memberOfManagementBoard;
            break;
          case COMPANY_LEGAL_FORM.ForeignCompany:
            this.title = wording.system.directorsAndSecretaries;
            this.singularTitle = wording.system.directorAndSecretary;
            this.definePrivatePersonTitle = wording.system.defineDirectorAndSecretary;
            this.memberToDefine = wording.system.directorOrSecretary;
            break;
          default:
            this.title = undefined;
            this.singularTitle = undefined;
            this.definePrivatePersonTitle = undefined;
            this.memberToDefine = undefined;
        }
      });
  }

  private initGrid(): void {
    this.dataSource$ = this.companyRepresentativeService
      .getStream(this.companyId)
      .pipe(
        map(representatives => this.companyRepresentativeService.toRows(representatives)),
        tap(representatives => this.representatives = representatives)
      );

    const gridActions = {
      edit: (row: ICompanyRepresentativeRow) => this.openDetailViewModal(row),
      delete: (row: ICompanyRepresentativeRow) => this.delete(row),
      add: () => this.openDetailViewModal(),
    };

    this.representativesGridConfig = getCompanyRepresentativesGridConfig(
      this.companyId,
      gridActions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  public delete = (representative: ICompanyRepresentativeRow): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: setTranslationSubjects(
        wording.general.areYouSureYouWantToDelete,
        { subject: representative.privatePersonCompany.companyName }
      ),
    }).subscribe(confirmed => {
      if (confirmed) {
        this.companyRepresentativeRepo.delete(representative.id, {}).subscribe();
      }
    });
  }

  private openDetailViewModal = (row?: ICompanyRepresentativeRow): void => {
    this.modalService.create({
      ...DefaultModalOptions,
      nzClosable: false,
      nzContent: RepresentativesDetailViewModalComponent,
      nzComponentParams: {
        title: this.singularTitle,
        representative: row,
        company: this.company,
        representatives: this.representatives,
        defineCheckboxTitle: this.definePrivatePersonTitle,
        memberToDefineTitle: this.memberToDefine,
      },
    });
  }
}
