import { Injectable } from '@angular/core';
import { RepoParams, AbstractRepository } from 'src/app/core/abstracts/abstract.repository';
import { IVesselSettingCreate, IVesselSetting, IVesselSettingDTO, VesselSettingKey } from 'src/app/core/models/resources/IVesselSetting';
import { RepositoryService } from '../repository.service';

export interface IVesselSettingRepoParams extends RepoParams<IVesselSetting> { }

@Injectable({
  providedIn: 'root'
})
export class VesselSettingsRepositoryService extends AbstractRepository<
IVesselSetting,
IVesselSettingDTO,
IVesselSettingCreate,
IVesselSettingRepoParams,
VesselSettingKey
>  {
  protected resourceType = 'Setting';
  protected sortKey = 'from';
  protected sortAsce = true;

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl = (options: IVesselSettingRepoParams) =>
    `/api/vessel-settings`

  protected postProcess = (item: IVesselSettingDTO): IVesselSetting => item;
}
