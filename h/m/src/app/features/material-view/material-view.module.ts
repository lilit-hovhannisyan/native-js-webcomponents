import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';

import { MatCarouselModule } from '@ngmodule/material-carousel';
import { LazyLoadImageModule } from 'ng-lazyload-image';

import { IconsModule } from '@shared/modules/icons.module';
import { MeButtonModule } from '@shared-components/me-button/me-button.module';
import { MeComponentLoadingModule } from '@shared-directives/me-component-loader/me-component-loader.module';
import { MeDocumentCardModule } from '@shared/me-components/me-document-card/me-document-card.module';
import { MeContextMenuModule } from '@shared-components/me-context-menu/me-context-menu.module';
import { MeGroupAttributesModule } from '@shared-components/me-group-attributes/me-group-attributes.module';
import { MePopupStatusChangeModule } from '@shared/me-components/me-popup-status-change/me-popup-status-change.module';
import { MePopupVideoModule } from '@shared/me-components/me-popup-video/me-popup-video.module';

import { MaterialViewComponent } from './material-view.component';
import { MaterialViewRoutingModule } from './material-view-routing.module';
import { MeEllipsisRowModule } from '@shared/me-components/me-ellipsis-row/me-ellipsis-row.module';

@NgModule({
  declarations: [MaterialViewComponent],
  imports: [
    CommonModule,
    MaterialViewRoutingModule,
    MatTabsModule,
    MeComponentLoadingModule,
    MeContextMenuModule,
    MatCarouselModule,
    MeGroupAttributesModule,
    MeButtonModule,
    LazyLoadImageModule,
    IconsModule,
    MePopupStatusChangeModule,
    MeDocumentCardModule,
    MePopupVideoModule,
    MeEllipsisRowModule
  ]
})
export class MaterialViewModule {}
