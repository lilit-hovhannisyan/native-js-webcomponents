import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import {
  IAccountStatement,
  IAccountStatementCreate,
  IAccountStatementDTO,
  AccountStatementKey
} from '../../models/resources/IAccount-statement';
import { MandatorKey } from '../../models/resources/IMandator';
import { ACCOUNTING_TYPES } from '../../models/enums/accounting-types';
import { BookingAccountKey } from '../../models/resources/IBookingAccount';

export interface AccountStatementQueryParams {
  mandatorId?: MandatorKey;
  bookingAccountId?: BookingAccountKey;
  year?: number;
  accountingType?: ACCOUNTING_TYPES;
}

export interface AccountStatementRepoParams extends RepoParams<IAccountStatement> {
  queryParams?: AccountStatementQueryParams;
}

@Injectable({
  providedIn: 'root'
})
export class AccountStatementRepositoryService extends AbstractRepository<
IAccountStatement,
IAccountStatementDTO,
IAccountStatementCreate,
AccountStatementRepoParams,
AccountStatementKey
> {
  protected resourceType = 'AccountStatement';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(params: AccountStatementRepoParams): string {
    return '/api/accountstatements';
  }

  public postProcess(item: IAccountStatementDTO): IAccountStatement { return item; }

}
