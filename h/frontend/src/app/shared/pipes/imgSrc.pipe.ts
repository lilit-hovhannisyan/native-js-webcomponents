import { environment } from './../../../environments/environment';
import { Pipe, PipeTransform } from '@angular/core';
import { sortArray } from '../helpers/general';


// Turnes an uploadId to a src-path
@Pipe({
  name: 'imgSrc'
})
export class ImgSrc implements PipeTransform {

  public transform(uploadId: string , size?: 'tiny' | 'small'): string {
    if (!uploadId) { console.error('uploadId needs to be provided'); }
    let path = `${environment.apiHost}/api/uploads/${uploadId}/file`;
    if (size) { path += `?modify=${size}`; }

    return path;
  }
}

