import {BaseComponent} from './src/library/core/base-component/base.component.js';
import {declareComponents} from './src/library/components/component.declare.js';
import {declareGitAppComponents} from './src/projects/git-app/git-app-components/git-app-component.declare.js';
import {createElementWithAttributes} from './src/library/helpers/random.helpers.js';
import {markupGitApp} from './src/projects/git-app/git-app-components/git-app-markup.js';

export class AppRoot extends BaseComponent {
  constructor() {
    super('./styles-app-root.css');

    // init all components
    declareComponents();
    declareGitAppComponents();
  }

  render = () => {
    const appConfig = {
      tagName: 'div',
      attr: {
        id: 'app'
      },
      html: markupGitApp
    };

    const appEl = createElementWithAttributes(appConfig);


    this.shadow.appendChild(appEl);
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }
}

// init root
customElements.define('app-root', AppRoot);


