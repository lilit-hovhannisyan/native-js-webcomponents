import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { wording } from '../wording/wording';
import { ICompanyRepresentativeRow } from '../../models/resources/ICompanyRepresentative';
import { CompanyKey } from '../../models/resources/ICompany';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { IWording } from '../../models/resources/IWording';

const customTooltip: (row: ICompanyRepresentativeRow) => IWording = (
  row: ICompanyRepresentativeRow,
): IWording => {
  return setTranslationSubjects(
    wording.system.representativesOf,
    { subject: row.company.companyName }
  );
};

export const getCompanyRepresentativesGridConfig = (
  companyId: CompanyKey,
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'companyRepresentativesGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    columns: [
      {
        key: 'firstName',
        title: wording.general.firstName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        customTooltip,
      },
      {
        key: 'lastName',
        title: wording.general.lastName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        customTooltip,
      },
      {
        key: 'functionTypeLabel',
        title: wording.system.function,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        customTooltip,
      },
      {
        key: 'isActive',
        title: wording.general.active,
        width: 100,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: ['1']
        },
        dataType: NvColumnDataType.Boolean,
        customTooltip,
      },
      {
        key: 'from',
        title: wording.general.from,
        width: 150,
        isSortable: true,
        hidden: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Date,
        },
        dataType: NvColumnDataType.Date,
        customTooltip,
      },
      {
        key: 'to',
        title: wording.general.to,
        width: 150,
        isSortable: true,
        hidden: true,
        filter: {
          values: [],
          controlType: NvFilterControl.Date,
        },
        dataType: NvColumnDataType.Date,
        customTooltip,
      },
      {
        key: 'scopeOfRepresentationTypeLabel',
        title: wording.system.scopeOfRepresentation,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        customTooltip,
      },
      {
        key: 'releaseSec181_1',
        title: wording.system.releaseSec181_1,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean,
        customTooltip,
      },
      {
        key: 'releaseSec181_2',
        title: wording.system.releaseSec181_2,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean,
        customTooltip,
      },
      {
        key: 'remark',
        title: wording.accounting.remark,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        customTooltip,
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'editCompanyRepresentative',
        tooltip: wording.general.edit,
        actOnDoubleClick: true,
        func: (row?: ICompanyRepresentativeRow): void => {
          if (row?.companyId === companyId) {
            actions.edit(row);
          }
        },
        hidden: !canPerform(Operations.UPDATE),
        disabled: (row?: ICompanyRepresentativeRow): boolean => {
          return !canPerform(Operations.UPDATE) || row?.companyId !== companyId;
        },
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        name: 'deleteCompanyRepresentative',
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        func: actions.delete,
        hidden: !canPerform(Operations.DELETE),
        disabled: (row?: ICompanyRepresentativeRow): boolean => {
          return !canPerform(Operations.UPDATE) || row?.companyId !== companyId;
        },
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        func: actions.add,
        hidden: () => !canPerform(Operations.CREATE)
      }
    ],
  });
};
