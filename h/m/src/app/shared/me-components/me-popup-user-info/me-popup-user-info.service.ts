import { Component, Injectable, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MePopupUserInfoComponent } from './me-popup-user-info.component';
import { MePopupUserInfoI } from './me-popup-user-info.interface';

@Injectable()
export class MePopupUserInfoService {
  private _takeUntilObservable$: Subject<void> = new Subject<void>();
  private _currentInstance: MePopupUserInfoComponent | undefined;

  constructor(private dialog: MatDialog) {}

  openPopup(dataModel: MePopupUserInfoI, content?: TemplateRef<Component>): Subject<string> {
    const subjectToReturn = new Subject<string>();
    const dialogRef = this.dialog.open(MePopupUserInfoComponent, { data: { dataModel, content }, panelClass: 'user-info-container' });

    this._currentInstance = dialogRef.componentInstance;
    this._currentInstance.popupUserInfoEvent
      .pipe(takeUntil(this._takeUntilObservable$))
      .subscribe((event: { eventName: string; items?: [] }) => {
        if (event.eventName === 'close') {
          dialogRef.close();
        } else {
          subjectToReturn.next(event.eventName);
        }
      });

    dialogRef
      .afterClosed()
      .pipe(takeUntil(this._takeUntilObservable$))
      .subscribe(() => {
        this._takeUntilObservable$.next();
        this._takeUntilObservable$.complete();
        this._currentInstance = undefined;
        subjectToReturn.complete();
      });

    return subjectToReturn;
  }

  pushDataModelToPopup(data: MePopupUserInfoI): void {
    if (this._currentInstance) {
      this._currentInstance.dataModel = data;
    }
  }
}
