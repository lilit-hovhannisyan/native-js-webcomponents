import { Component, OnInit } from '@angular/core';
import { NvGridConfig } from 'nv-grid';
import { Observable } from 'rxjs';
import { IAutobankBusinessCase } from 'src/app/core/models/resources/IAutobankBusinessCase';
import { wording } from 'src/app/core/constants/wording/wording';
import { AutobankBusinessCaseRepositoryService } from 'src/app/core/services/repositories/autobank-business-case-repository.service';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/autobank-business-case.config';
import { BaseComponent } from 'src/app/core/abstracts/BaseComponent';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { AutobankBusinessCasesEditModalComponent } from '../autobank-business-cases-edit-modal/autobank-business-cases-edit-modal.component';
import { AutobankBusinessCasesDetailViewComponent } from '../autobank-business-cases-detail-view/autobank-business-cases-detail-view.component';
import { TranslatePipe } from 'src/app/shared/pipes/translate.pipe';

@Component({
  selector: 'nv-autobank-business-cases-view-list',
  templateUrl: './autobank-business-cases-view-list.component.html',
  styleUrls: ['./autobank-business-cases-view-list.component.scss']
})
export class AutobankBusinessCasesViewListComponent extends BaseComponent implements OnInit {
  private modalRef?: NzModalRef;
  public gridConfig: NvGridConfig;
  public dataSource$: Observable<IAutobankBusinessCase[]>;
  public wordingRef = wording.accounting;
  private translatePipe: TranslatePipe;

  constructor(
    private modalService: NzModalService,
    private autobankBusinessCaseRepo: AutobankBusinessCaseRepositoryService,
    public gridConfigService: GridConfigService,
    private bcs: BaseComponentService,
  ) {
    super(bcs);
  }

  public ngOnInit() {
    this.translatePipe = new TranslatePipe(this.settingsService);
    this.autobankBusinessCaseRepo.fetchAll({}).subscribe();
    this.dataSource$ = this.autobankBusinessCaseRepo.getStream();
    this.initConfig();
  }

  private initConfig() {

    const actions = {
      edit: this.openEditModal,
      delete: this.deleteClicked,
      add: this.openDetailViewModal
    };

    this.gridConfig = getGridConfig(
      actions,
      (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation)
    );
  }

  private deleteClicked = (autobankBusinessCase: IAutobankBusinessCase) => {

    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: autobankBusinessCase.displayLabel },
    }).subscribe(confirmed => confirmed
      && this.autobankBusinessCaseRepo.delete(autobankBusinessCase.id, {
      }).subscribe());
  }

  private openEditModal = (autobankBusinessCase: IAutobankBusinessCase) => {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzWidth: 350,
      nzTitle: `${this.translatePipe.transform(autobankBusinessCase.displayLabel)}`,
      nzClosable: true,
      nzContent: AutobankBusinessCasesEditModalComponent,
      nzComponentParams: {
        closeModal: this.closeModal,
        autobankBusinessCase,
      }
    });
  }

  private closeModal = () => {
    this.modalRef.destroy();
    this.autobankBusinessCaseRepo.getStream().subscribe();
  }

  private openDetailViewModal = () => {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzWidth: 350,
      nzTitle: this.wordingRef.businessCases[this.settingsService.language],
      nzClosable: true,
      nzContent: AutobankBusinessCasesDetailViewComponent,
      nzComponentParams: {
        closeModal: this.closeModal
      }
    });
  }
}
