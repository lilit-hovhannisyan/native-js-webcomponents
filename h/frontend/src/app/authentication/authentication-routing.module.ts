
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginViewComponent } from './components/login-view/login-view.component';
import { SendVerificationLinkComponent } from './components/send-verification-link/send-verification-link.component';
import { ResetPasswordViewComponent } from './components/reset-password-view/reset-password-view.component';
import { ResetPasswordGuard } from '../routing/services/reset-password.guard';
import { LoginGuard } from '../routing/services/login-guard';

const routes: Routes = [
  {
    path: 'login',
    component: LoginViewComponent,
    canActivate: [LoginGuard]

  },
  {
    path: 'reset-password',
    component: ResetPasswordViewComponent,
    canActivate: [ResetPasswordGuard]
  },
  {
    path: 'registration',
    component: SendVerificationLinkComponent,
    canActivate: [LoginGuard]
  },
  {
    path: '**',
    redirectTo: 'base/main',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
