import { Injectable } from '@angular/core';
import { IAutobankSetting } from '../../models/resources/IAutobankSetting';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { HttpGet } from '../../models/http/http-get';
import { map, catchError } from 'rxjs/operators';
import { IDataResponseBody } from '../../models/responses/IDataResponseBody';
import { HttpPut } from '../../models/http/http-put';
import { IResourceCollection } from '../../models/resources/IResourceCollection';

@Injectable({
  providedIn: 'root'
})
export class AutobankSettingRepositoryService {
  private baseUrl = '/api/autobank/settings';

  constructor(private http: HttpClient) { }

  public fetchAll(): Observable<IAutobankSetting[]> {
    const request = new HttpGet(this.baseUrl);
    return this.http.request(request).pipe(
      map(
        (res: HttpResponse<IDataResponseBody<IResourceCollection<IAutobankSetting>>>) => {
          return res.body.data.items;
        }),
      catchError(() => of({}) as Observable<IAutobankSetting[]>)
    );
  }

  public update(settings: IAutobankSetting): Observable<IAutobankSetting> {
    const request = new HttpPut(this.baseUrl, settings);
    return this.http.request(request).pipe(
      map((res: HttpResponse<IDataResponseBody<IAutobankSetting>>) => res.body.data)
    );
  }
}
