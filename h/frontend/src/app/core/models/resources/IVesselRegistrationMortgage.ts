import { IResource } from './IResource';
import { CurrencyKey } from './ICurrency';
import { VesselKey } from './IVessel';

export type VesselRegistrationMortgageKey = number;
export interface VesselRegistrationMortgageEntry {
  mortgagee: string;
  mortgageAmount: number; // min: 0, max: 999999999999.99
  currencyId: CurrencyKey;
}
interface ISharedProps {
  vesselId: VesselKey;
  remark: string;
  entries: VesselRegistrationMortgageEntry[];
}

export interface IVesselRegistrationMortgageCreate extends ISharedProps { }

export interface IVesselRegistrationMortgageBase
  extends ISharedProps, IResource<VesselRegistrationMortgageKey> { }

export interface IVesselRegistrationMortgageDTO extends IVesselRegistrationMortgageBase { }

export interface IVesselRegistrationMortgage extends IVesselRegistrationMortgageDTO { }
