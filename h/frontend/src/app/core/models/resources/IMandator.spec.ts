import { IMandator, getFiscalCalendarDate } from './IMandator';

describe('Fiscal Year test', () => {
  it('should start early if period is greater than current month', () => {

    expect(
      getFiscalCalendarDate(new Date('2002-07-13'), 6).getTime()
    ).toBe(new Date('2003-02-01T00:00:00').getTime());

    expect(
      getFiscalCalendarDate(new Date('2003-06-13'), 6).getTime()
    ).toBe(new Date('2004-01-01T00:00:00').getTime());

    expect(
      getFiscalCalendarDate(new Date('2004-12-13'), 12).getTime()
    ).toBe(new Date('2005-01-01T00:00:00').getTime());

  });

  it('should adjust period and stay in same year if period below current month', () => {

    expect(
      getFiscalCalendarDate(new Date('2003-02-13'), 6).getTime()
    ).toBe(new Date('2003-09-01T00:00:00').getTime());

    expect(
      getFiscalCalendarDate(new Date('2003-05-13'), 6).getTime()
    ).toBe(new Date('2003-12-01T00:00:00').getTime());

    expect(
      getFiscalCalendarDate(new Date('2004-02-13'), 4).getTime()
    ).toBe(new Date('2004-11-01T00:00:00').getTime());

  });

  it('should have a matching year and period if fiscal calendar is in-sync with normal calendar', () => {

    expect(
      getFiscalCalendarDate(new Date('2012-02-13'), 1).getTime()
    ).toBe(new Date('2012-02-01T00:00:00').getTime());

  });
});
