import { Directive, OnDestroy, ViewContainerRef, ComponentFactoryResolver, Input } from '@angular/core';
import { fromEvent, Subject, Subscription } from 'rxjs';

import CellClickEventI from './interfaces/cell-click-event.interface';
import RederedComponentI from './interfaces/rendered-component.interface';

interface ComponentProperties extends RederedComponentI {
  subject: Subject<CellClickEventI>;
  colIndex: number;
  rowOID: string;
  child?: {
    rowOID: string;
  };
}

@Directive({
  selector: '[meTableCellComponent]'
})
export class MeTableDirective implements OnDestroy {
  componentEventSubscription!: Subscription;
  componentOutputEventSubscription!: Subscription;

  constructor(private vc: ViewContainerRef, private cfr: ComponentFactoryResolver) {}

  @Input()
  set meTableCellComponent(cellParams: ComponentProperties) {
    if (!cellParams) {
      return;
    }

    this.loadComponent(cellParams);
  }

  private loadComponent(cellParams: ComponentProperties): void {
    const { component, componentData, rowOID, subject, colIndex, child } = cellParams;
    const componentFactory = this.cfr.resolveComponentFactory(component);
    const componentRef = this.vc.createComponent(componentFactory);
    const {
      instance,
      location: { nativeElement }
    } = componentRef;

    this.componentEventSubscription = fromEvent<MouseEvent>(nativeElement, 'click').subscribe((event) => event.stopPropagation());
    instance.data = componentData;

    if (instance.tableDataChangeEvent) {
      this.componentOutputEventSubscription = instance.tableDataChangeEvent.subscribe((data: any) => {
        if (child) {
          subject.next({ data, parentRowOID: rowOID, rowOID: child.rowOID, colIndex });
        } else {
          subject.next({ data, rowOID, colIndex });
        }
      });
    }
  }

  ngOnDestroy() {
    if (this.componentOutputEventSubscription) {
      this.componentOutputEventSubscription.unsubscribe();
    }

    this.componentEventSubscription.unsubscribe();
    this.vc.clear();
  }
}
