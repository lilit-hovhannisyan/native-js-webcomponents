import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BackofficeRoutingModule } from './backoffice-routing.module';
import { BackOfficeService } from './backoffice.web-service';
import { TableViewService } from '@shared/me-components/me-table/table-view.service';

@NgModule({
  imports: [CommonModule, BackofficeRoutingModule],
  providers: [BackOfficeService, TableViewService]
})
export class BackofficeModule {}
