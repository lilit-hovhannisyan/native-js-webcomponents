/* tslint:disable:max-line-length*/
export const wordingPayments = {
  paymentsTitle: { en: 'Payments', de: 'Zahlungen' },
  paymentsPermissionsTitle: { en: 'Payments Permissions', de: 'Zahlungsverkehr Berechtigungen' },
};
