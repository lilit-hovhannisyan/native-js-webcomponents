import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { FormGroup } from '@angular/forms';
import { IWording } from 'src/app/core/models/resources/IWording';
import { getCostUnitsGridConfig } from '../../../../core/constants/nv-grid-configs/cost-units.config';
import { CostUnitRepositoryService } from '../../../../core/services/repositories/cost-unit-repository.service';
import { MandatorCostUnitRepositoryService } from '../../../../core/services/repositories/mandator-cost-unit-repository.service';

@Component({
  selector: 'nv-cost-unit-autocomplete',
  templateUrl: './cost-unit-autocomplete.component.html',
  styleUrls: ['./cost-unit-autocomplete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CostUnitAutocompleteComponent implements OnInit {
  public wording = wording;
  public gridConfig = getCostUnitsGridConfig({}, () => false);

  @Input() public predefineIfOnlyField: boolean;
  @Input() public form: FormGroup;
  @Input() public controlName = 'costUnitId';
  @Input() public filters: object;
  @Input() public onlyOfMandator: boolean;
  @Input() public mandatorControlName = 'mandatorId';
  @Input() public showLabel = true;
  @Input() public label: IWording;

  @Input() public setId = true;
  @Input() public displayOneProperty: boolean;
  @Input() public openListOnInit: boolean;
  @Input() public autofocus: boolean;

  @Output() public tab = new EventEmitter<KeyboardEvent>();
  @Output() public escape = new EventEmitter<KeyboardEvent>();
  @Output() public enter = new EventEmitter<KeyboardEvent>();
  @Output() public blur = new EventEmitter<FocusEvent>();

  constructor(
    public costUnitRepo: CostUnitRepositoryService,
    public mandatorCostUnitRepo: MandatorCostUnitRepositoryService,
  ) { }

  public ngOnInit() {
    this.costUnitRepo.fetchAll({}).subscribe();
    this.mandatorCostUnitRepo.fetchAll({}).subscribe();
  }
}
