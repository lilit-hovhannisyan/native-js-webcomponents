import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'textReplace'
})
export class TextReplacePipe implements PipeTransform {

  public transform(value: string, from: string, to: string): any {
    if (value && from && to) {
      return value.replace(from, to);
    }
    return null;
  }

}
