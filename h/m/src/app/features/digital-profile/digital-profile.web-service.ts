import { Injectable } from '@angular/core';
import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { BaseWebService } from '@core-services/base.web-service';
import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { CertificateModel } from '@shared/models/certificate/certificate.model';
import { DigitalProfileModel } from '@shared/models/digital-profile/digital-profile.model';
import { Observable } from 'rxjs';
import { CertificateAssociationModel } from '@shared/models/certificate-association/certificate-association.model';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { constructUrl } from '@shared/utils';

@Injectable({
  providedIn: 'root'
})
export class DigitalProfileWebService {
  constructor(private baseWebService: BaseWebService) {}

  getCompnay(): Observable<ArrayResponseI<DigitalProfileModel>> {
    return this.baseWebService.getRequestForArray<DigitalProfileModel>(`${MARKETPLACE_BASE_API_URL}/companies`, DigitalProfileModel);
  }

  getCertificate(): Observable<ArrayResponseI<CertificateModel>> {
    return this.baseWebService.getRequestForArray<CertificateModel>(`${MARKETPLACE_BASE_API_URL}/certificates`, CertificateModel);
  }

  getCertificateAssociation(): Observable<ArrayResponseI<CertificateAssociationModel>> {
    return this.baseWebService.getRequestForArray<CertificateAssociationModel>(
      `${MARKETPLACE_BASE_API_URL}/companies/certificates/relationships`,
      CertificateAssociationModel
    );
  }

  getDocument(documentID: string): Observable<MeDocumentModel> {
    return this.baseWebService.getRequest<MeDocumentModel>(`${MARKETPLACE_BASE_API_URL}/documents/${documentID}`, MeDocumentModel);
  }

  removeCertificate(oid: string): Observable<void> {
    return this.baseWebService.deleteRequest<void>(`${MARKETPLACE_BASE_API_URL}/companies/certificates/relationships/${encodeURI(oid)}`);
  }

  searchCertificateAssociation(data: { criteriaQuick: string }): Observable<ArrayResponseI<CertificateAssociationModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/companies/certificates/relationships/search`, 0, 10000);
    return this.baseWebService.postRequestForArray<CertificateAssociationModel, object>(url, data, CertificateAssociationModel);
  }
}
