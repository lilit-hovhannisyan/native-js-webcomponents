import { ValidationErrors, AbstractControl } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { Injectable } from '@angular/core';
import { markControlAsTouched } from 'src/app/core/helpers/form';
import { BankAccountRepositoryService } from 'src/app/core/services/repositories/bank-account-repository.service';
import { IBankAccount } from 'src/app/core/models/resources/IBankAccount';
import { CompanyKey } from 'src/app/core/models/resources/ICompany';

@Injectable({
  providedIn: 'root'
})
export class BankAccountValidatorsService {

  constructor(private bankAccountRepo: BankAccountRepositoryService) { }

  public doesBankAccountExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const bankAccount: IBankAccount = this.bankAccountRepo.getStreamValue()
      .find(b => b.id === control.value);

    if (bankAccount) {
      return null;
    }

    markControlAsTouched(control);
    return {
      accountNotFound: {
        message: wording.accounting.notFoundAccount
      }
    };
  }

  public isBankAccountRelatedToCompany = (control: AbstractControl): ValidationErrors | null => {
    if (
      !control.value
      || !control.parent
      || !control.parent.get('companyId').value
    ) {
      return null;
    }

    const companyId: CompanyKey = control.parent.get('companyId').value;

    const bankAccount: IBankAccount = this.bankAccountRepo.getStreamValue()
      .find(ba => ba.id === control.value);

    const isValid: boolean = bankAccount && bankAccount.companyId === companyId;

    if (isValid) {
      return null;
    }

    markControlAsTouched(control);
    return {
      bankAccountNotRelatedToChosenCompany: {
        message: wording.general.bankAccountNotRelatedToCompany
      }
    };
  }
}
