// tslint:disable: max-line-length
export const wordingErrors = {
  'invalid-email-or-password': {
    de: 'Ungültige Email oder Passwort',
    en: 'Invalid Email or Password',
  },
  'from-until-is-outside-of-time-charter-range': {
    de: 'Rechungszeitraum außerhalb der Zeitcharter Range',
    en: 'From-until of time charter billing should be between the range of the connected time charter',
  },
  'from-is-greater-than-until': {
    de: '"Bis" Datum muss größer sein als "Von" Datum',
    en: 'Until-Date needs to be greater than From-Date',
  },
  'required': {
    de: 'Eine Eingabe wird benötigt',
    en: 'The field is required',
  },
  'unknown': {
    de: 'Ein Fehler ist aufgetreten',
    en: 'Something went wrong',
  },
  'wrong-string-length': {
    de: 'Feldlänge muss zwischen {LOWER_LEVEL} und {HIGHER_LEVEL} liegen',
    en: 'Length must be from {LOWER_LEVEL} to {HIGHER_LEVEL}',
  },
  'bad-request': {
    de: 'Ein Fehler ist aufgetreten',
    en: 'Something went wrong',
  },
  'wrong-decimal-digits': {
    de: 'Inkorrekter wert. Maximale Anzahl Dezimalstellen {ITEM}',
    en: 'Invalid value. Maximum {ITEM} decimal points is allowed',
  },
  'wrong-email-or-username': {
    de: 'Email/Benutzername ungültig',
    en: 'The email / username is not valid',
  },
  'wrong-level': {
    de: 'Wert muss zwischen {LOWER_LEVEL} und {HIGHER_LEVEL} liegen',
    en: 'The value should be between {LOWER_LEVEL} and {HIGHER_LEVEL}',
  },
  'forbidden': {
    de: 'Fehlende Berechtigung für diesen Vorgang',
    en: 'You do not have sufficient permissions to perform this operation',
  },
  // TODO: Delete this key once it be fixed from back-end
  'You do not have sufficient permissions to perform this operation': {
    de: 'Fehlende Berechtigung für diesen Vorgang',
    en: 'You do not have sufficient permissions to perform this operation',
  },
  'maintenance': {
    de: 'Der Server wird gerade gewartet',
    en: 'The server is currently undergoing maintenance',
  },
  'db-update': {
    de: 'Beim Update der Database ist ein Fehler aufgetreten',
    en: 'There was a problem during updating database',
  },
  'referencing-not-allowed': {
    de: 'Verweis auf dieses Objekt nicht erlaubt',
    en: 'Referencing this object is not allowed',
  },
  'date-in-past': {
    de: 'Datum in der Vergangenheit nicht erlaubt',
    en: 'Date can\'t be in past',
  },
  'cannot-delete-set-to-inactive': {
    de: 'Löschen nicht möglich',
    en: 'Can not delete',
  },
  'cannot-delete': {
    de: 'Löschen nicht möglich',
    en: 'Can not delete',
  },
  'service-unavailable': {
    de: 'Service ist nicht verfügbar',
    en: 'The service is unavailable',
  },
  'token-expired': {
    de: 'Token ist abgelaufen',
    en: 'Token has expired',
  },
  'unauthenticated': {
    de: 'Fehlende Berechtigung für diesen Vorgang',
    en: 'Missing authorization to perform this action',
  },
  'unsupported-media-type': {
    de: 'Dieser Media-Typ wird nicht unterstützt',
    en: 'This media type is not supported',
  },
  'only-superadmin-allowed-to-set-currency-active/inactive': {
    de: 'Währung kann nur durch einen SuperAdmin aktiv/inaktiv gesetzt werden',
    en: 'Only the superadmin is allowed to set the currency active/inactive',
  },
  'only-admin-allowed-to-set-currency-active/inactive': {
    de: 'Kann nur durch Admins aktiv/inaktiv gesetzt werden',
    en: 'Only the admins are allowed to set active/inactive',
  },
  'widget-not-owned-by-user': {
    de: 'Keine Rechte für dieses Widget',
    en: 'Widget not owned by user!',
  },
  'only-superadmin-can-change-admin-states': {
    de: 'Nur SuperAdmins können den Admin Status ändern',
    en: 'Only SuperAdmin can change admin states',
  },
  'invalid-username': {
    de: 'Ungültiger Benutzername: Benutzername ist zu kurz oder zu lang bzw. enthält ungültige Zeichen',
    en: 'Username invalid. The username is very short or too long or contain an invalid character',
  },
  'username-is-taken': {
    de: 'Benutzername wird bereits verwendet. Bitte einen neuen Benutzernamen eingeben',
    en: 'Username already exists. Please provide a new username',
  },
  'email-is-taken': {
    de: 'Email-Adresse wird bereits verwendet. Bitte eine neue Email-Adresse eingeben',
    en: 'Email address already exists. Please provide a new email address',
  },
  'cant-access-others-bookmarks': {
    de: 'Sie können nur auf Ihre Lesezeichen zugreifen',
    en: 'You can access only your bookmarks',
  },
  'cant-create-bookmarks-for-others': {
    de: 'Sie können nur eigene Lesezeichen erstellen',
    en: 'You can create bookmarks only for yourselves',
  },
  'cant-update-others-bookmarks': {
    de: 'Sie können nur Ihre Lesezeichen verwalten',
    en: 'You can update only your bookmarks',
  },
  'cant-remove-others-bookmarks': {
    de: 'Sie können nur Ihre Lesezeichen löschen',
    en: 'You can remove only your bookmarks',
  },
  'cannot-delete-when-booked': {
    de: 'Löschen nicht möglich, da bereits Buchungen erfolgt sind',
    en: 'Entity can\'t be deleted, once used/booked in the system',
  },
  'cant-change-time-charter': {
    de: 'Zeitcharter kann nicht geändert werden',
    en: 'Time charter can\'t be changed',
  },
  'only-super-admin-can-deactivate-system-default': {
    de: 'Standardeinstellungen können nur durch denSuperAdmin geändert werden',
    en: 'Only super-admin can deactivate system default entities',
  },
  'cant-update-booking-account-No-once-used': {
    de: 'Konto-Nr. kann nach erfolgter Buchung nicht geändert werden',
    en: 'Booking account\'s No can\'t be updated once used in the system',
  },
  'cannot-update-after-booking-closed': {
    de: 'Buchung kann nach erfolgter Festschreibung nicht geändert werden',
    en: 'You can\'t update Booking after it\'s been closed',
  },
  'account-debitor-creditor-relation-cannot-be-changed': {
    de: 'Debitor/Kreditor Konto Nr. kann nicht geändert werden',
    en: 'Accounts / Debtor-Creditor relation can\'t be changed by anyone',
  },
  'cannot-be-changed': {
    de: 'Feld kann nicht geändert werden',
    en: 'The filed can\'t be changed by anyone',
  },
  'mandator-Tax-relations-cannot-be-deleted-once-booked': {
    de: 'Mandanten-Steuer-Beziehung kann nach Verwendung beim Buchung nicht geändert werden',
    en: 'Mandator-Tax relations cannot be deleted, once one booking dataset based on this relation has been created in the system',
  },
  'cannot-remove-after-booking-closed': {
    de: 'Buchung kann nach erfolgter Festschreibung nicht gelöscht werden',
    en: 'You can\'t remove Booking after it\'s been closed',
  },
  'disabled': {
    de: 'Feld ist deaktiviert',
    en: 'Field is disabled',
  },
  'not-found': {
    de: 'Objekt existiert nicht',
    en: 'Object does not exist',
  },
  'route-argument-mismatch': {
    de: 'Eingegebener Wert {KEY} für {ARG1} stimmt nicht mit {ARG2} überein',
    en: 'The provided "{KEY}" "{ARG1}" does not match with "{ARG2}" contained in the route',
  },
  'account-not-related-to-mandator': {
    de: 'Keine Verknüpfung zwischen Mandant und Konto - Verbindung bitte einrichten',
    en: 'Account not yet related to chosen mandator, you first have to create this connection',
  },
  'supplier-does-not-have-discount': {
    de: 'Skonto für diesen Lieferanten nicht hinterlegt',
    en: 'Supplier does not have any discount',
  },
  'vessel-not-specified': {
    de: 'Verwendung der Voyage ohne Auswahl eines Schiffes nicht möglich',
    en: 'Not allowed to use voyage without specifying vessel',
  },
  'booking-code-not-related-to-mandator': {
    de: 'Keine Verknüpfung zwischen Mandant und Buchungsschlüssel - Verbindung bitte einrichten',
    en: 'Could not find any booking code with the current mandator',
  },
  'booking-code-not-assigned-to-mandator': {
    de: 'Der Buchungscode ist dem Mandanten nicht zugewiesen',
    en: 'Booking code is not assigned to mandator'
  },
  'cannot-change-currency-for-this-account': {
    de: 'Währung für dieses Konto kann nicht geändert werden',
    en: 'You can\'t change Currency in combination to this account',
  },
  'mandator-is-inactive-for-booking': {
    de: 'Mandant inaktiv - Buchungen nicht möglich',
    en: 'The mandator is inactive and cannot be used for bookings',
  },
  'mandator-is-closed-for-booking-year': {
    de: 'Buchungsjahr {YEAR} für Mandant {NO} abgeschlossen',
    en: 'Chosen mandator {NO} is closed for defined booking year {YEAR}',
  },
  'booking-year-is-blocked-for-mandator': {
    de: 'Buchungsjahr {YEAR} für Mandant {NO} ist geblockt von Benutzer {USERNAME}',
    en: '{YEAR} not allowed - Chosen booking year is blocked by {USERNAME} for mandator {NO}',
  },
  'period-is-less-than-last-closed-period': {
    de: 'Buchungsperiode muss größer sein als letzte geschlossene Periode ({LAST_CLOSED_PERIOD})',
    en: 'Period must be greater than last closed period ({LAST_CLOSED_PERIOD})',
  },
  'period-13-allowed-when-12-is-closed': {
    de: 'Buchungen in Periode 13 nur möglich, wenn Periode 12 für Mandant {NO} geschlossen',
    en: 'Period 13 is only allowed, when Period 12 is closed for mandator {NO}',
  },
  'booking-in-fiscal-year-is-blocked-for-mandator': {
    de: 'Buchungsjahr {YEAR} für Mandant {NO} ist geblockt von Benutzer {USERNAME}',
    en: 'Bookings in fiscal year {Year} for mandator {NO} are not allowed, because the year is blocked by {USERNAME}',
  },
  'booking-in-period-is-blocked-for-mandator': {
    de: 'Buchungen in Periode {PERIOD} für Mandant {NO} nicht möglich, da die Periode von Benutzer {USERNAME} geblockt ist',
    en: 'Bookings in period {PERIOD} for mandator {NO} are not allowed, because the period is blocked by {USERNAME}',
  },
  'invoices-older-than-10-years-or-in-future': {
    de: 'Rechnungsdatum älter als 10 Jahre bzw. mit Datum in der Zukunft nicht erlaubt',
    en: 'Invoices older than 10 years or in the future are not allowed',
  },
  'mandator-is-closed-for-invoice-year': {
    de: 'Buchungsjahr {YEAR} für Mandant abgeschlossen',
    en: 'Chosen mandator is closed for defined booking year {YEAR}',
  },
  'cant-change-mandator-first-booking-year-after-its-booked': {
    de: 'Erstes Buchungsjahr eines Mandanten kann nach erfolgter Buchung nicht geändert werden',
    en: 'You are not allowed to change the first booking year of a mandator that has already been booked',
  },
  'cant-change-mandator-currency-after-its-booked': {
    de: 'Hauswährung eines Mandanten kann nach erfolgter Buchung nicht geändert werden',
    en: 'You are not allowed to change the Currency of a mandator that has already been booked',
  },
  'mandator-start-year-should-be-lower-or-equal-than-first-booking-year': {
    de: 'Startjahr Mandant muss kleiner/gleich erstem Buchungsjahr sein',
    en: 'Mandator start year should be lower than or equal than first booking year',
  },
  'No-is-too-high': {
    de: 'Höchste mögliche Mandant Belegnummer {COUNTER} - verwendete Belegnummer zu hoch',
    en: 'Mandator / Voucher Type / Year combination has highest No. {COUNTER}, the chosen number is too high',
  },
  'billing-cycle-is-disabled-for-selected-type': {
    de: 'Rechnungslauf für gewählten Rechnungstyp deaktiviert',
    en: 'Billing Cycle is disabled for the selected billing cycle type',
  },
  'end-approx-is-lower-than-begin-and-minus-days-combined': {
    de: 'EndApprox muss größer sein als die Kombination aus Begin und MinusDays',
    en: 'EndApprox should be bigger than Begin and MinusDays combined',
  },
  'more-than-one-time-charter-rate': {
    de: 'Bei Time Charter Type "Fixed" nur eine Rate möglich',
    en: 'For fixed type TimeCharterRate should be one row',
  },
  'could-not-reference-to-multiple-stevedore-damage-repairs': {
    de: 'Referenz zu mehreren StevedoreDamageRepairs nicht möglich',
    en: 'Could not reference to multiple StevedoreDamageRepairs',
  },
  'no-permission-to-change-invoice-released': {
    de: 'Fehlende Berechtigung zur Änderung von freigegebenen Rechnungen',
    en: 'User don\'t have sufficient permissions to change InvoiceReleased',
  },
  'no-stevedore-damage-repair-item-to-release-invoice': {
    de: 'Rechnung kann nicht ohne zugeordneten Stevedore Artikel freigegeben werden',
    en: 'Could not release invoice without any stevedore damage repair item',
  },
  'vessel-not-referenced-to-vessel-accounting': {
    de: 'Keine Verknüpfung zwischen Schiff und Schiffsbuchhaltung vorhanden',
    en: 'Could not find connection between Vessel and VesselAccounting',
  },
  'connected-voyage-not-found': {
    de: 'Keine zugeordnete Voyage gefunden',
    en: 'Could not find connected Voyage',
  },
  'connected-time-charter-not-found': {
    de: 'Keine zugeordnete TimeCharter gefunden',
    en: 'Could not find connected TimeCharter',
  },
  'booking-No-is-in-reserved-settings-range': {
    de: 'Verwendete Kontonummer nicht zulässig',
    en: 'Not allowed to use a number within the reserved settings range',
  },
  'cannot-reassign-booking-No': {
    de: 'Gewählte Konto-Nr. gehört zum Bereich Debitoren/Kreditoren und kann nicht geändert werden',
    en: 'You cannot reassign the No of the account to a different range',
  },
  'does-not-match-settings-range': {
    de: 'Konto-Nr. entspricht nicht den Systemvalidierungen',
    en: 'Does not match the settings range',
  },
  'cant-update-booking-code-No-once-used': {
    de: 'Belegnummer kann nach Verwendung nicht mehr geändert werden',
    en: 'Booking code\'s No cannot be updated once used in the system',
  },
  'cannot-insert-duplicate-with-same-key': {
    de: 'Debitor/Kreditor sowohl in Prüfung Mahnlauf als auch im Zahlungsbedingungen ausgewählt',
    en: 'Cannot insert duplicate with the same key',
  },
  'lower-level-days-are-higher-than-higher-level-days': {
    de: 'Eingabe der Tage überprüfen - Die Reihenfolge ist nicht logisch',
    en: 'Days for level n must be lower than days for level n + 1',
  },
  'higher-level-percentages-are-higher-than-lower-level-percentages': {
    de: 'Eingabe der Prozentsätze prüfen - Die Wertevergabe ist nicht logisch',
    en: 'Percentage of level n should be higher than percentage of level n+1',
  },
  // TODO: Discuss this case. The original message is like: "You can't use that No for the Mandator,  as a Mandator with that same No already exists"
  'duplicate': {
    en: 'This value has been already used',
    de: 'Dieser Wert wurde bereits verwendet',
  },
  'cannot-deactivate-default-type': {
    de: 'Inaktiv Setzen von Standarddatensätzen nicht möglich',
    en: 'It\'s not allowed to manually set default type to inactive',
  },
  'cannot-change-default-type': {
    de: 'Ändern von Standarddatensätzen nicht möglich',
    en: 'It\'s not allowed to change default type',
  },
  'cannot-delete-default-type': {
    de: 'Löschen von Standarddatensätzen nicht möglich',
    en: 'It\'s not allowed to delete default type',
  },
  'not-all-entities-reference-same-mandator': {
    de: 'Backend Zusatzcheck Mandant geblocked und/oder Status Jahresabschluss',
    en: 'Not all entities reference the same mandator',
  },
  'blocking-period-is-less-than-closed-period': {
    de: 'Zu blockierende Buchungsperiode muss größer sein als letzte geschlossene Buchungsperiode',
    en: 'Blocking periods need to be greater than closed periods for a year',
  },
  'debitor-creditor-already-exists': {
    de: 'Debitor/Kreditor No. bereits vorhanden',
    en: 'Debtor or/and Creditor with the given No. already exists',
  },
  'from-until-is-outside-of-voyage-range': {
    de: 'Off Hire Datum außerhalb der Voyage Range',
    en: 'From-until should be between the range of the connected voyage',
  },
  'from-until-overlapped': {
    de: 'Keine Überschneidung von “from” - “until” zulässig',
    en: 'From-until should not overlap each other',
  },
  'invalid-maximum-timespan': {
    de: 'Maximum Zeitspanne zwischen Startdatum und Enddatum ist {MAX_ALLOWED_RATE_INTERVAL_IN_DAYS}',
    en: 'The maximum timespan between startdate and enddate is {MAX_ALLOWED_RATE_INTERVAL_IN_DAYS}',
  },
  'wrong-password': {
    de: 'Verwendetes Passwort inkorrekt',
    en: 'Current password is wrong',
  },
  'invalid': {
    de: 'Inkorrekter Wert',
    en: 'Invalid value',
  },
  'bank-account-is-required-when-filter-only-valid-for-is-selected': {
    de: 'Name Bankkonto muss ausgewählt werden, wenn der Filter "nur gültig für" gewählt wurde',
    en: 'Bank Account Name is required when the "Filter only valid for" is selected',
  },
  'reference-between-mandator-booking-account-already-exists': {
    de: 'Reference with the current Mandator and Chart of Account already exists',
    en: 'Reference with the current Mandator and Chart of Account already exists',
  },
  'reference-already-exists': {
    de: 'Verknüpfung bereits vorhanden',
    en: 'Reference already exists: {ARG1} and {ARG2}',
  },
  'duplicate-booking-account': {
    de: 'Buchungskonto existiert bereits in der Struktur',
    en: 'Chart of account can only be added once to the structure',
  },
  'account-nodes-not-allowed-to-have-children': {
    de: 'Konten können keine weiteren Ebenen enthalten',
    en: 'Account Nodes are not allowed to have children',
  },
  'not-valid-domain': {
    de: 'Dieser Benutzer kann mit der aktuellen Email-Domäne nicht aktiviert werden. Bitte wählen Sie eine zulässige Domäne um den Benutzer zu reaktivieren',
    en: 'This user cannot be activated with current email domain. Please use a valid domain to reactivate the user',
  },
  'domain-is-used-for-active-user': {
    de: 'Löschen nicht möglich, die Domäne wird in aktiven Benutzern verwendet',
    en: 'This domain is used in active users',
  },
  'expiration-date-is-higher-than-current-date': {
    de: 'Fälligkeitsdatum darf nicht kleiner sein als Tagesdatum',
    en: 'Expiration date should not be lower than current date',
  },
  'password-must-not-contain-first-lastname': {
    de: 'Darf nicht Ihren Vor- oder Nachnamen enthalten',
    en: 'Must not contain the first or last name of the user'
  },
  'password-use-at-least-three-of-four-characters': {
    de: 'Verwendung von min. 3 der 4 Zeichenarten (Großbuchstaben, Kleinbuchstaben, Zahlen, Sonderzeichen)',
    en: 'Use at least 3 of the 4 character types (upper case, lower case, numbers, special characters)'
  },
  'password-min-length-characters': {
    de: 'Länge min. 12 Zeichen',
    en: 'Length min. 12 characters'
  },
  'fieldname-not-allowed-to-operationtype': {
    de: 'Diese Operation ist nicht erlaubt für dieses Feld',
    en: 'This operation is not allowed for this field'
  },
  'voucher-type-is-not-assigned-to-mandator': {
    de: 'Die Belegart ist dem Mandanten nicht zugewiesen',
    en: 'Voucher type is not assigned to mandator'
  },
  'tax-key-not-related-to-mandator': {
    de: 'Der Steuerschlüssel ist dem Mandanten nicht zugewiesen',
    en: 'Tax key is not assigned to mandator'
  },
  'cannot-change-no-cost-center-is-booked': {
    en: 'Cost center has been booked, change of number is not allowed',
    de: 'Die Kostenstelle wurde bereits gebucht, eine Änderung der Nummer ist daher nicht mehr möglich',
  },
  'cannot-delete-booked-cost-center': {
    en: 'Cost center cannot be deleted due to existing booking',
    de: 'Die Kostenstelle wurde bereits gebucht und kann daher nicht gelöscht werden',
  },
  'autobank-business-case-gvc-duplicated': {
    en: 'Dataset for GVC-value is already present',
    de: 'Es liegt bereits ein Datensatz für diese GVC-Nummer vor',
  },
  'accrued-account-deferred-income': {
    en: 'The selected account is not defined as an accrual account for deferred income.',
    de: 'Das ausgewählte Konto ist nicht als PRAP-Konto definiert.',
  },
  'accrued-account-prepaid-expenses': {
    en: 'The selected account is not defined as an accrual account for prepaid expenses.',
    de: 'Das ausgewählte Konto ist nicht als ARAP-Konto definiert.',
  },
  'dont-change-accrued-account-prepaid-expenses': {
    en: 'This account is defined as default accrual account for prepaid expenses. Accrual accounting values cannot be modified.',
    de: 'Das Konto ist als Standardkonto ARAP definiert. Werte der periodengerechten Abgrenzung können nicht geändert werden.',
  },
  'dont-change-accrued-account-deferred-income': {
    en: 'This account is defined as default accrual account for deferred income. Accrual accounting values cannot be modified.',
    de: 'Das Konto ist als Standardkonto PRAP definiert. Werte der periodengerechten Abgrenzung können nicht geändert werden.',
  },
  'accrued-account-deferred-income-related': {
    en: 'This account is assigned as accrual account for deferred income to other booking accounts. Accrual accounting values cannot be modified.',
    de: 'Das Konto ist als Konto PRAP in anderen Konten hinterlegt. Werte der periodengerechten Abgrenzung können nicht geändert werden.',
  },
  'accrued-account-prepaid-expenses-related': {
    en: 'This account is assigned as accrual account for prepaid expenses to other booking accounts. Accrual accounting values cannot be modified.',
    de: 'Das Konto ist als Konto ARAP in anderen Konten hinterlegt. Werte der periodengerechten Abgrenzung können nicht geändert werden.',
  },
  'TC-balance-LC-balance-must-be-zero': {
    en: 'Sum check failed. TC balance and LC balance must be 0.00.',
    de: 'Die Summenprüfung schlug fehl. TW-Saldo und HW-Saldo müssen 0,00 sein.',
  },
  'LC-balance-must-be-zero': {
    en: 'Sum check failed. LC balance must be 0.00.',
    de: 'Die Summenprüfung schlug fehl. HW-Saldo muss 0,00 sein.',
  },
  'Required-automatic-booking-line': {
    en: 'The booking line {BOOKING_LINE} requires an automatic booking line',
    de: 'Die Buchungszeile {BOOKING_LINE} benötigt eine automatische Buchungszeile'
  },
  'Remaining-read-only-booking-line': {
    en: 'The automatic booking line {BOOKING_LINE} is not related to any tax booking line',
    de: 'Die automatische Buchungszeile {BOOKING_LINE} ist mit keiner Steurbuchunszeile verbindet'
  },
  'Incorrect-Text-read-only-booking-line': {
    en: 'The text for the automatic booking line {BOOKING_LINE} is not related to any tax booking line',
    de: 'Der Buchtext der automatischen Buchungszeile {BOOKING_LINE} ist mit keiner Steurbuchunszeile verbindet'
  },
  'Invalid-booking-code-read-only-booking-line': {
    en: 'The booking line {BOOKING_LINE} has an unvalid BookingCode',
    de: 'Die Buchungszeile {BOOKING_LINE} hat einen ungültigen Vorgangsschlüssel'
  },
  'Invalid-fields-read-only-booking-line': {
    en: 'Some fields in the booking line {BOOKING_LINE} are not valid',
    de: 'Die Buchungszeile {BOOKING_LINE} hat einige Felder die ungültig sind'
  },
  'Incorrect-Currency-read-only-booking-line' : {
    en: 'The currency fot the automatic booking line {BOOKING_LINE} is not related to any tax booking line',
    de: 'Die Währung der automatischen Buchungszeile {BOOKING_LINE} ist mit keiner Steurbuchunszeile verbindet'
  }
};
