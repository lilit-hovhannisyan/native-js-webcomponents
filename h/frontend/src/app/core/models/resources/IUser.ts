import { IWithActive } from './IWithActive';
import { IResource } from './IResource';
import { IUpload } from './IUpload';
import { Language } from '../language';
import { NvLocale } from '../dateFormat';
import { DEBIT_CREDIT } from '../enums/debitCredit';
import { Theme } from '../Theme.enum';

export type UserKey = number;
interface ISharedProps extends IWithActive {
  email?: string;
  firstName: string;
  lastName: string;
  username: string;
  displayName: string;
  language: Language;
  dateFormat?: NvLocale;
  tel: string;
  fax?: string;
  avatarId?: string;
  location?: string;
  isSuperAdmin: boolean;
  isAdmin: boolean;
  locale?: NvLocale;
  debitCredit?: DEBIT_CREDIT;
  avatar?: IUpload;
  theme?: Theme;
  expirationDate?: string;
  hasEmail: boolean;
}

export interface IUserBase extends ISharedProps, IResource<number> { }

export interface IUserCreate extends ISharedProps { }

export interface IUserDTO extends IUserBase { }

export interface IUser extends IUserDTO { }

export interface ITokenResource {
  accessToken: string;
  refreshToken: string;
}

export interface IUserAccountConfigGeneral {
  language: Language;
  locale: number;
}




