import {BaseComponent} from '../../../../../library/core/base-component/base.component.js';
import {basePath} from '../../../data/base.path.js';
import {getRoutes} from '../../../data/git.routes.js';

export class Body extends BaseComponent {
  constructor() {
    super(basePath + 'app-base-components/body/body.component.css');
  }

  render = () => {
    const config = {
      tagName: 'div',
      attr: {
        class: 'page'
      },
      subElements: [
        {
          tagName: 'route-manager',
          attr: {
            routes: getRoutes
          }
        }
      ]
    };

    const el = this.createElementWithAttributes(config);
    this.shadow.appendChild(el);
  }

  connectedCallback() {
    super.connectedCallback(this.render);
  }
}