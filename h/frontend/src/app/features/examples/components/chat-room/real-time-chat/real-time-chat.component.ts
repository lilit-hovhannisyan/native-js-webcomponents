import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { HubService } from 'src/app/core/services/hub.service';

enum GROUPS {
  HappyGroup = 'Happy Group',
  SadGroup = 'Sad Group',
  CoolGroup = 'Cool Group',
}

export interface IUserInfo {
  connectionId: string;
  name: string;
  userId: number;
}

export interface IMessageInfo {
  groupName: string;
  message: string;
  userInfo: IUserInfo;
}

export interface IMessage {
  message: string;
  senderName: string;
  isNotification?: boolean;
}

@Component({
  selector: 'nv-real-time-chat',
  templateUrl: './real-time-chat.component.html',
  styleUrls: ['./real-time-chat.component.scss'],
})
export class RealTimeChatComponent implements OnInit, OnDestroy {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  @Input() public receiverChannel = 'SendDirectMessage';
  @Input() public invokeMethod = 'SendDirectMessage';
  @Input() public mode: 'group' | 'direct' | 'all';

  @Output() public newOnlineUser: EventEmitter<IUserInfo> = new EventEmitter();
  @Output() public disconnectedUser: EventEmitter<IUserInfo> = new EventEmitter();

  public currentUser: IUserInfo;
  public directReceiver: IUserInfo;
  public onlineUsers: IUserInfo[] = [];
  public directRoomId: string;

  public messages: {[key: string]: IMessage[]} = {};
  public messageToSend: string;

  public GROUPS = GROUPS;
  public allGroups = Object.keys(GROUPS).map(key => ({ id: GROUPS[key], name: key }));
  public currentGroup: string;

  constructor(
    private hubService: HubService
  ) { }

  public async ngOnInit() {
    if (!this.hubService.connection) {
      this.hubService.initConnection();
      await this.hubService.startListening();
      this.connectUser();
    } else {
      this.currentUser = this.hubService.currentUser;
      this.messages = this.hubService.messages;
      if (this.mode === 'direct') {
        this.onlineUsers = this.hubService.onlineUsers;
      }
    }
    this.listenToNewOnlineUsers();
    this.listenToDisconnectedUser();
    if (this.mode === 'direct') {
      this.fetchOnlineUsers();
      this.receiveDirectMessage();
    } else if (this.mode === 'group') {
      this.currentGroup = 'HappyGroup';
      this.joinGroup();
      this.receiveGroupMessage();
      this.listenToGroupEvents();
    } else {
      this.receiveMessageForAll();
    }

  }

  public connectUser() {
    this.hubService.addListener('UserConnected', user => {
      if (!this.hubService.currentUser) {
        this.hubService.currentUser = user;
        this.currentUser = this.hubService.currentUser;
      }
    });
  }

  public fetchOnlineUsers() {
    this.hubService.addListener('OnlineUsers', onlineUsers => {
      this.onlineUsers = onlineUsers.filter(u => u.name !== this.currentUser.name);
    });
  }

  public listenToNewOnlineUsers() {
    this.hubService.addListener('NewOnlineUser', user => {
      this.newOnlineUser.emit(user);
    });
  }

  public listenToDisconnectedUser() {
    this.hubService.addListener('UserDisconnected', user => {
      this.disconnectedUser.emit(user);
    });
  }

  public receiveDirectMessage() {
    this.hubService.addListener(
      this.receiverChannel,
      (message: string, senderInfo: IUserInfo, receiverInfo: IUserInfo) => {
        const roomId = `${senderInfo.name}-${receiverInfo.name}`;
        const foundRoomId = Object.keys(this.messages)
          .find(v => v.includes(senderInfo.name) && v.includes(receiverInfo.name));

        if (!this.messages[roomId] && !this.messages[foundRoomId]) {
          this.messages[roomId] = [];
          this.messages[roomId].push({ message, senderName: senderInfo.name });

          if (
            this.directReceiver
            && roomId.includes(this.directReceiver.name)
            && roomId.includes(this.currentUser.name)
          ) {
            this.directRoomId = roomId;
          }
          return;
        }

        this.messages[foundRoomId].push({ message, senderName: senderInfo.name });

        if (
          this.directReceiver
          && foundRoomId.includes(this.directReceiver.name)
          && foundRoomId.includes(this.currentUser.name)
        ) {
          this.directRoomId = foundRoomId;
        }
        this.scrollToBottom();
      }
    );
  }

  public receiveGroupMessage() {
    this.hubService.addListener(this.receiverChannel, (messageInfo: IMessageInfo) => {
      if (!this.messages[messageInfo.groupName]) {
        this.messages[messageInfo.groupName] = [];
      }
      this.messages[messageInfo.groupName].push({
        message: messageInfo.message,
        senderName: messageInfo.userInfo.name,
      });
      this.scrollToBottom();
    });
  }

  public receiveMessageForAll() {
    this.hubService.addListener(this.receiverChannel, (message: string, senderInfo: IUserInfo) => {
      if (!this.messages[this.mode]) {
        this.messages[this.mode] = [];
      }
      this.messages[this.mode].push({
        message,
        senderName: senderInfo.name,
      });
      this.scrollToBottom();
    });
  }

  public sendMessage() {
    if (this.mode === 'direct') {
      this.hubService.invokeMethod(this.invokeMethod, this.messageToSend, this.directReceiver.userId);
    } else if (this.mode === 'group') {
      this.hubService.invokeMethod(this.invokeMethod, this.currentGroup, this.messageToSend);
    } else {
    this.hubService.invokeMethod(this.invokeMethod, this.messageToSend);
    }

    this.messageToSend = '';
  }

  public listenToGroupEvents() {
    this.hubService.addListener('SendGroupEvent', (messageInfo: IMessageInfo) => {
      if (!this.messages[messageInfo.groupName]) {
        this.messages[messageInfo.groupName] = [];
      }

      if (this.currentUser.name !== messageInfo.userInfo.name) {
        this.messages[messageInfo.groupName].push({
          message: messageInfo.message,
          senderName: messageInfo.userInfo.name,
          isNotification: true,
        });
        this.scrollToBottom();
      }
    });
  }

  public joinGroup() {
    this.hubService.invokeMethod('JoinGroup', this.currentGroup);
  }

  public changeGroup(receiver: string) {
    this.currentGroup = receiver;
    this.joinGroup();
  }

  public changeDirectReceiver(receiver: IUserInfo) {
    this.directReceiver = receiver;
    const roomId = Object.keys(this.messages)
      .find(v => v.includes(this.directReceiver.name) && v.includes(this.currentUser.name));

    if (!roomId) {
      this.directRoomId = `${this.currentUser.name}-${this.directReceiver.name}`;
    } else {
      this.directRoomId = roomId;
    }
  }

  public scrollToBottom(): void {
    setTimeout(() => {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    });
  }

  public ngOnDestroy(): void {
    this.hubService.messages = this.messages;
    if (this.mode === 'direct') {
      this.hubService.onlineUsers = this.onlineUsers;
    }
  }
}
