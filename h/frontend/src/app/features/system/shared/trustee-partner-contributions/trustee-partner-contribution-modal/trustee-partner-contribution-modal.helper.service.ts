import { Injectable } from '@angular/core';
import { FormGroup, ValidatorFn, AbstractControl } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { ReplaySubject, combineLatest } from 'rxjs';
import { startWith, distinctUntilChanged, takeUntil, tap } from 'rxjs/operators';
import { TrusteePartnerContributionMainService, LatestContributionsMap } from 'src/app/core/services/mainServices/trustee-partner-contributions-main.service';
import { CompanyKey } from 'src/app/core/models/resources/ICompany';
import { DECIMAL_14_2 } from 'src/app/core/constants/min-max-values';
import { getGreaterDate } from 'src/app/core/helpers/general';
import { SettingsService } from 'src/app/core/services/settings.service';
import { COMPANY_CONTRIBUTION_TYPES } from '../enums/company-contributions';
import { ICompanyPartnerContribution } from 'src/app/core/models/resources/ICompanyPartnerContribution';
import { ITrusteePartnerContributionFull, ITrusteePartnerContribution } from 'src/app/core/models/resources/ITrusteePartnerContribution';
import { ContributionMainService } from 'src/app/core/services/mainServices/company-contributions-main.service';
import { setTranslationSubjects, safeFloatSum, safeFloatReduction } from 'src/app/shared/helpers/general';
import { wording } from 'src/app/core/constants/wording/wording';
import { decimalValidators } from '../../../components/misc/companies/detail-view/company-legal-tab/shareholders-partners/contribution-modal/contribution-modal.helper.service';
import { fDecimal } from 'src/app/core/helpers/decimal-formatter';

type IShareholdersFinalContributionsMap = Map<CompanyKey, ICompanyPartnerContribution>;

@Injectable()
export class ContributionModalHelperService {
  private latestContributionsMap: LatestContributionsMap;
  private form: FormGroup;
  private componentDestroyed$: ReplaySubject<unknown>;
  private companyId: CompanyKey;
  private contributorType: COMPANY_CONTRIBUTION_TYPES;
  private shareholderLatestContributionsMap: IShareholdersFinalContributionsMap;
  private partnerLatestContributionsMap: Map<CompanyKey, ITrusteePartnerContributionFull>;
  private shareholdersContributions: ICompanyPartnerContribution[];
  private trustorsContributions: ITrusteePartnerContribution[];
  private theOtherContributions: ITrusteePartnerContribution[];
  private shareholdersContributionsFinalAmountMap: IShareholdersFinalContributionsMap;
  private shareholdersContributionsFinalAmountFilteredByDateMap: IShareholdersFinalContributionsMap;

  private increasePartnerIdControl: AbstractControl;
  private increaseShareholderIdControl: AbstractControl;
  private increaseContributionControl: AbstractControl;
  private increaseContributionNewControl: AbstractControl;
  private reducePartnerIdControl: AbstractControl;
  private reduceShareholderIdControl: AbstractControl;
  private reduceContributionControl: AbstractControl;
  private reduceContributionNewControl: AbstractControl;
  private contributionChangeControl: AbstractControl;
  private fromControl: AbstractControl;

  private increasePartnerIdValue: number;
  private increaseShareholderIdValue: number;
  private increaseContributionNewValue: number;
  private reducePartnerIdValue: number;
  private reduceShareholderIdValue: number;
  private reduceContributionNewValue: number;
  private contributionChangeValue: number;

  constructor(
    private trusteeContributionsMainService: TrusteePartnerContributionMainService,
    private shareholderContributionsMainService: ContributionMainService,
    private settingsService: SettingsService,
  ) { }

  public init(
    form: FormGroup,
    componentDestroyed$: ReplaySubject<unknown>,
    companyId: CompanyKey,
    contributorType: COMPANY_CONTRIBUTION_TYPES,
    shareholderLatestContributionsMap: IShareholdersFinalContributionsMap,
    latestContributionsMap: LatestContributionsMap,
    allContributions: ITrusteePartnerContributionFull[],
    shareholdersContributions: ICompanyPartnerContribution[],
    theOtherContributions: ITrusteePartnerContribution[],
  ) {
    this.form = form;
    this.componentDestroyed$ = componentDestroyed$;
    this.companyId = companyId;
    this.contributorType = contributorType;
    this.shareholderLatestContributionsMap = shareholderLatestContributionsMap;
    this.latestContributionsMap = latestContributionsMap;
    this.shareholdersContributions = shareholdersContributions;
    this.trustorsContributions = allContributions;
    this.theOtherContributions = theOtherContributions;
    this.partnerLatestContributionsMap = this.shareholderContributionsMainService
      .getCompaniesLatestContributions(allContributions);
  }

  public initValidations(): void {
    const {
      increasePartnerId,
      increaseShareholderId,
      increaseContribution,
      increaseContributionNew,
      reducePartnerId,
      reduceShareholderId,
      reduceContribution,
      reduceContributionNew,
      contributionChange,
      from,
    } = this.form.controls;

    this.increasePartnerIdControl = increasePartnerId;
    this.increaseShareholderIdControl = increaseShareholderId;
    this.increaseContributionControl = increaseContribution;
    this.increaseContributionNewControl = increaseContributionNew;
    this.reducePartnerIdControl = reducePartnerId;
    this.reduceShareholderIdControl = reduceShareholderId;
    this.reduceContributionControl = reduceContribution;
    this.reduceContributionNewControl = reduceContributionNew;
    this.contributionChangeControl = contributionChange;
    this.fromControl = from;

    combineLatest([
      this.increasePartnerIdControl.valueChanges.pipe(
        startWith(this.increasePartnerIdControl.value),
        distinctUntilChanged(),
      ),
      this.increaseShareholderIdControl.valueChanges.pipe(
        startWith(this.increasePartnerIdControl.value),
        distinctUntilChanged(),
      ),
      this.increaseContributionNewControl.valueChanges.pipe(
        startWith(this.increaseContributionNewControl.value),
        distinctUntilChanged(),
      ),
      this.reducePartnerIdControl.valueChanges.pipe(
        startWith(this.reducePartnerIdControl.value),
        distinctUntilChanged(),
      ),
      this.reduceShareholderIdControl.valueChanges.pipe(
        startWith(this.reducePartnerIdControl.value),
        distinctUntilChanged(),
      ),
      this.reduceContributionNewControl.valueChanges.pipe(
        startWith(this.reduceContributionNewControl.value),
        distinctUntilChanged(),
      ),
      this.contributionChangeControl.valueChanges.pipe(
        startWith(this.contributionChangeControl.value),
        distinctUntilChanged(),
      ),
      this.fromControl.valueChanges.pipe(
        startWith(this.fromControl.value),
        distinctUntilChanged(),
      ),
    ])
      .pipe(
        takeUntil(this.componentDestroyed$),
        tap(([
          increasePartnerIdValue,
          increaseShareholderIdValue,
          increaseContributionNewValue,
          reducePartnerIdValue,
          reduceShareholderIdValue,
          reduceContributionNewValue,
          contributionChangeValue,
        ]) => {
          this.increasePartnerIdValue = increasePartnerIdValue;
          this.increaseShareholderIdValue = increaseShareholderIdValue;
          this.increaseContributionNewValue = increaseContributionNewValue;
          this.reducePartnerIdValue = reducePartnerIdValue;
          this.reduceShareholderIdValue = reduceShareholderIdValue;
          this.reduceContributionNewValue = reduceContributionNewValue;
          this.contributionChangeValue = contributionChangeValue;

          this.calculateValues();
          this.setValidators();
          this.enableDisableFields();
          this.setCalculatedValues();

          this.fromControl.updateValueAndValidity();
          this.increasePartnerIdControl.updateValueAndValidity();
          this.increaseShareholderIdControl.updateValueAndValidity();
          this.increaseContributionNewControl.updateValueAndValidity();
          this.reducePartnerIdControl.updateValueAndValidity();
          this.reduceShareholderIdControl.updateValueAndValidity();
          this.reduceContributionNewControl.updateValueAndValidity();
          this.contributionChangeControl.updateValueAndValidity();
        })
      ).subscribe();

    this.clearNewContributionOnPartnerChanges();
    this.setShareholderOnPartnerChanges();
  }

  public initFinalContribution = (
    partnerControlName: string,
    trusteePartnerControlName: string,
    contributionControlName: string,
    considerDate = true,
  ): void => {
    const {
      from: fromControl,
      [partnerControlName]: partnerIdControl,
      [trusteePartnerControlName]: shareholderIdControl,
      [contributionControlName]: contributionControl,
    } = this.form.controls;

    combineLatest([
      fromControl.valueChanges.pipe(
        startWith(fromControl.value),
        distinctUntilChanged(),
      ),
      partnerIdControl.valueChanges.pipe(
        startWith(partnerIdControl.value),
        distinctUntilChanged(),
      ),
      shareholderIdControl.valueChanges.pipe(
        startWith(shareholderIdControl.value),
        distinctUntilChanged(),
      ),
    ])
      .pipe(
        takeUntil(this.componentDestroyed$),
        tap(([
          fromValue,
          partnerIdValue,
          shareholderIdValue,
        ]) => {
          if (partnerIdValue && shareholderIdValue) {
            this.trusteeContributionsMainService.getFinalContribution(
              this.contributorType,
              this.companyId,
              partnerIdValue,
              shareholderIdValue,
              considerDate ? fromValue : undefined,
            ).subscribe((finalContribution: number) => {
              contributionControl.setValue(finalContribution);
              this.setCalculatedValues();
            });
          } else {
            contributionControl.setValue(0);
            this.setCalculatedValues();
          }
        })
      ).subscribe();
  }

  private calculateValues(): void {
    this.calculateShareholdersAmount();
  }

  private calculateShareholdersAmount(): void {
    this.shareholderLatestContributionsMap = this.shareholderContributionsMainService
      .getCompaniesLatestContributions(this.shareholdersContributions);

    this.shareholdersContributionsFinalAmountMap
      = this.getShareholdersFinalContributionsSum(this.shareholdersContributions);

    this.shareholdersContributionsFinalAmountFilteredByDateMap
      = this.getShareholdersFinalContributionsSum(
        this.shareholderContributionsMainService.filterContributions(
          this.shareholdersContributions,
          this.fromControl.value,
        ),
      );
  }

  private getShareholdersFinalContributionsSum(
    shareholderContributions: ICompanyPartnerContribution[],
  ): IShareholdersFinalContributionsMap {
    const shareholdersLatestContributionsMap: IShareholdersFinalContributionsMap
      = this.shareholderContributionsMainService.getCompaniesLatestContributions(
        shareholderContributions,
      );
    const allContributions: ITrusteePartnerContribution[] = [
      ...this.trustorsContributions,
      ...this.theOtherContributions,
    ];

    allContributions.forEach((trusteeContribution) => {
      const shareholderContribution: ICompanyPartnerContribution
        = shareholdersLatestContributionsMap.get(trusteeContribution.shareholderId);

      if (shareholderContribution) {
        shareholdersLatestContributionsMap.set(trusteeContribution.shareholderId, {
          ...shareholderContribution,
          contributionAmount: safeFloatReduction(
            shareholderContribution.contributionAmount,
            trusteeContribution.contributionAmount,
          ),
        });
      }
    });

    return shareholdersLatestContributionsMap;
  }

  private setValidators(): void {
    this.setFromValidator();
    this.setIncreasePartnerIdValidator();
    this.setIncreaseShareholderIdValidator();
    this.setReducePartnerIdValidator();
    this.setReduceShareholderIdValidator();
    this.setIncreaseContributionNewValidator();
    this.setReduceContributionNewValidator();
    this.setContributionChangeValidator();
  }

  private enableDisableFields(): void {
    this.enableDisableIncreaseContributionNew();
    this.enableDisableReduceContributionNew();
    this.enableDisableContributionChange();
  }

  private setCalculatedValues(): void {
    this.calculateIncreaseContributionNew();
    this.calculateReduceContributionNew();
    this.calculateContributionChange();
  }

  private setFromValidator(): void {
    const increasePartnerLastDate: string
      = this.latestContributionsMap.get(this.increasePartnerIdValue, this.increaseShareholderIdValue)?.from;
    const reducePartnerLastDate: string
      = this.latestContributionsMap.get(this.reducePartnerIdValue, this.reduceShareholderIdValue)?.from;
    let greaterDate: string;

    if (increasePartnerLastDate && reducePartnerLastDate) {
      greaterDate = getGreaterDate(increasePartnerLastDate, reducePartnerLastDate);
    } else if (increasePartnerLastDate) {
      greaterDate = increasePartnerLastDate;
    } else if (reducePartnerLastDate) {
      greaterDate = reducePartnerLastDate;
    }

    if (greaterDate) {
      this.fromControl.setValidators([
        Validators.required,
        Validators.dateShouldBeAfter(greaterDate, this.settingsService.locale, true)
      ]);
    } else {
      this.fromControl.setValidators([Validators.required]);
    }
  }

  private setIncreasePartnerIdValidator(): void {
    if (this.reducePartnerIdValue && this.reduceShareholderIdValue) {
      this.increasePartnerIdControl.clearValidators();
    } else {
      this.increasePartnerIdControl.setValidators(Validators.required);
    }
  }

  private setIncreaseShareholderIdValidator(): void {
    if (this.reducePartnerIdValue && this.reduceShareholderIdValue) {
      this.increaseShareholderIdControl.clearValidators();
    } else {
      this.increaseShareholderIdControl.setValidators(Validators.required);
    }
  }

  private setReducePartnerIdValidator(): void {
    if (this.increasePartnerIdValue && this.increaseShareholderIdValue) {
      this.reducePartnerIdControl.clearValidators();
    } else {
      this.reducePartnerIdControl.setValidators(Validators.required);
    }
  }

  private setReduceShareholderIdValidator(): void {
    if (this.increasePartnerIdValue && this.increaseShareholderIdValue) {
      this.reduceShareholderIdControl.clearValidators();
    } else {
      this.reduceShareholderIdControl.setValidators(Validators.required);
    }
  }

  private getMaxContributionAmount(): number {
    const shareholdersRestAmount: number
      = this.shareholdersContributionsFinalAmountMap.get(
        this.increaseShareholderIdValue
      )?.contributionAmount || 0;

    const shareholdersRestAmountByDate: number
      = this.shareholdersContributionsFinalAmountFilteredByDateMap.get(
        this.increaseShareholderIdValue
      )?.contributionAmount || 0;

    const maxContributionAmount: number = Math.min(
      shareholdersRestAmount,
      shareholdersRestAmountByDate,
    );

    return maxContributionAmount;
  }

  private setIncreaseContributionNewValidator(): void {
    if (this.increasePartnerIdValue && this.increaseShareholderIdValue) {
      const maxContributionAmount = this.getMaxContributionAmount();
      const shareholdersContributionAmount: number = this.shareholderLatestContributionsMap
        .get(this.increaseShareholderIdValue).contributionAmount;

      const errorMessage = maxContributionAmount < shareholdersContributionAmount
        ? setTranslationSubjects(
          wording.system.theValueShouldNotBeGreaterDueToExistingContributions,
          { subject: fDecimal(maxContributionAmount) },
        )
        : undefined;

      this.increaseContributionNewControl.setValidators(
        [
          Validators.required,
          ...decimalValidators,
          Validators.positiveNumber,
          Validators.isGreaterThanFieldValue('increaseContribution'),
          // Max contribution can't be greater than shareholders remaining contribution
          Validators.max(maxContributionAmount, errorMessage),
        ]
      );
    } else {
      this.increaseContributionNewControl.setValidators(decimalValidators);
    }
  }

  private setReduceContributionNewValidator(): void {
    if (this.reducePartnerIdValue && this.reduceShareholderIdControl) {
      this.reduceContributionNewControl.setValidators(
        [
          Validators.required,
          ...decimalValidators,
          Validators.notNegative,
          Validators.isLessThanFieldValue('reduceContribution', false, false),
        ]
      );
    } else {
      this.reduceContributionNewControl.setValidators(decimalValidators);
    }
  }

  private setContributionChangeValidator(): void {
    const contributionChangeValidator = [
      ...decimalValidators,
      Validators.positiveNumber,
    ];

    if (!(
      (this.increaseContributionNewValue && this.increaseContributionNewControl.enabled)
      || ((this.reduceContributionNewValue || this.reduceContributionNewValue === 0)
        && this.reduceContributionNewControl.enabled
      )
      || (
        (!this.increasePartnerIdValue || !this.increaseShareholderIdValue)
        && (!this.reducePartnerIdValue || !this.reduceShareholderIdValue)
      )
    )) {
      contributionChangeValidator.push(Validators.required);
    }

    const isIncreaseFilledAndDisabled: boolean = this.increasePartnerIdValue
      && this.increaseShareholderIdValue
      && this.increaseContributionNewControl.disabled;

    const isReduceFilledAndDisabled: boolean = this.reducePartnerIdValue
      && this.reduceShareholderIdValue
      && this.reduceContributionNewControl.disabled;

    const maxIncreaseContributionAmount: number = safeFloatReduction(
      this.shareholderLatestContributionsMap.get(this.increaseShareholderIdValue)?.contributionAmount,
      this.increaseContributionControl.value,
    );

    const maxReduceContributionAmount: number = this.reduceContributionControl.value;


    if (isIncreaseFilledAndDisabled && isReduceFilledAndDisabled) {
      const maxContributionChangeAmount: number = safeFloatReduction(
        this.getMaxContributionAmount(),
        this.increaseContributionControl.value,
      );
      const smallerAmount: number = Math.min(
        maxIncreaseContributionAmount,
        maxReduceContributionAmount,
        maxContributionChangeAmount,
      );

      const errorMessage = smallerAmount === maxContributionChangeAmount
        && smallerAmount !== maxReduceContributionAmount
        && smallerAmount !== maxIncreaseContributionAmount
        ? setTranslationSubjects(
          wording.system.theValueShouldNotBeGreaterDueToExistingContributions,
          { subject: fDecimal(smallerAmount) },
        )
        : undefined;

      contributionChangeValidator.push(Validators.max(smallerAmount, errorMessage));
    } else if (isIncreaseFilledAndDisabled) {
      const maxContributionChangeAmount: number = safeFloatReduction(
        this.getMaxContributionAmount(),
        this.increaseContributionControl.value,
      );
      const smallerAmount: number = Math.min(
        maxIncreaseContributionAmount,
        maxContributionChangeAmount,
      );

      const errorMessage = smallerAmount === maxContributionChangeAmount
        && smallerAmount !== maxIncreaseContributionAmount
        ? setTranslationSubjects(
          wording.system.theValueShouldNotBeGreaterDueToExistingContributions,
          { subject: fDecimal(smallerAmount) },
        )
        : undefined;

      contributionChangeValidator.push(Validators.max(smallerAmount, errorMessage));
    } else if (isReduceFilledAndDisabled) {
      contributionChangeValidator.push(Validators.max(maxReduceContributionAmount));
    }

    this.contributionChangeControl.setValidators(contributionChangeValidator);
  }


  private enableDisableIncreaseContributionNew(): void {
    if (
      !this.increasePartnerIdValue
      || !this.increaseShareholderIdValue
      || (
        this.contributionChangeControl.enabled
        && this.contributionChangeValue
      )
    ) {
      this.disableControl(this.increaseContributionNewControl);
    } else {
      this.enableControl(this.increaseContributionNewControl);
    }
  }


  private enableDisableReduceContributionNew(): void {
    if (
      !this.reducePartnerIdValue
      || !this.reduceShareholderIdValue
      || (
        this.contributionChangeControl.enabled
        && this.contributionChangeValue
      )
    ) {
      this.disableControl(this.reduceContributionNewControl);
    } else {
      this.enableControl(this.reduceContributionNewControl);
    }
  }

  private enableDisableContributionChange(): void {
    if (
      (this.increaseContributionNewValue && this.increaseContributionNewControl.enabled)
      || (
        (this.reduceContributionNewValue || this.reduceContributionNewValue === 0)
        && this.reduceContributionNewControl.enabled
      )
      || (
        (!this.increasePartnerIdValue || !this.increaseShareholderIdValue)
        && (!this.reducePartnerIdValue || !this.reduceShareholderIdValue)
      )
    ) {
      this.disableControl(this.contributionChangeControl);
    } else {
      this.enableControl(this.contributionChangeControl);
    }
  }


  private calculateIncreaseContributionNew(): void {
    if (
      this.increasePartnerIdValue
      && this.increaseShareholderIdValue
      && this.contributionChangeValue
      && this.contributionChangeControl.enabled
      && this.increaseContributionControl.disabled
    ) {
      this.setValue(
        this.increaseContributionNewControl,
        safeFloatSum(
          this.increaseContributionControl.value,
          this.contributionChangeValue,
        ),
      );
    }
    if (
      !this.increasePartnerIdValue
      || !this.increaseShareholderIdValue
      || (!this.contributionChangeValue && this.increaseContributionNewControl.disabled)
    ) {
      this.setValue(this.increaseContributionNewControl, null);
    }
  }


  private calculateReduceContributionNew(): void {
    if (
      this.reducePartnerIdValue
      && this.reduceShareholderIdValue
      && this.contributionChangeValue
      && this.contributionChangeControl.enabled
    ) {
      this.setValue(
        this.reduceContributionNewControl,
        safeFloatReduction(
          this.reduceContributionControl.value,
          this.contributionChangeValue,
        )
      );
    }
    if (
      !(this.reducePartnerIdValue && this.reduceShareholderIdValue)
      || (!this.contributionChangeValue && this.reduceContributionNewControl.disabled)
    ) {
      this.setValue(this.reduceContributionNewControl, null);
    }
  }

  private calculateContributionChange(): void {
    const increaseCalculatedValue: number =
      this.increaseContributionNewValue
        ? safeFloatReduction(this.increaseContributionNewValue,
          this.increaseContributionControl.value,
        )
        : null;
    const reduceCalculatedValue: number =
      this.reduceContributionNewValue || this.reduceContributionNewValue === 0
        ? safeFloatReduction(
          this.reduceContributionControl.value,
          this.reduceContributionNewValue,
        )
        : null;

    // If both values are entered and change is equal
    if (
      (
        (this.increaseContributionNewValue || this.increaseContributionNewValue === 0)
        && this.increaseContributionNewControl.enabled
      )
      && (
        (this.reduceContributionNewValue || this.reduceContributionNewValue === 0)
        && this.reduceContributionNewControl.enabled
      )
      && increaseCalculatedValue && increaseCalculatedValue === reduceCalculatedValue
    ) {
      this.setValue(this.contributionChangeControl, increaseCalculatedValue);
    }

    // If entered value only in increaseContributionNewControl
    if (
      increaseCalculatedValue
      && !reduceCalculatedValue
      && this.increaseContributionNewControl.enabled
    ) {
      if (!this.reducePartnerIdValue) {
        this.setValue(this.contributionChangeControl, increaseCalculatedValue);
      } else {
        this.setValue(this.contributionChangeControl, null);
      }
    }


    // If entered value only in reduceContributionNewControl
    if (
      (reduceCalculatedValue || reduceCalculatedValue === 0)
      && !increaseCalculatedValue
      && this.reduceContributionNewControl.enabled
    ) {
      if (!this.increasePartnerIdValue) {
        this.setValue(this.contributionChangeControl, reduceCalculatedValue);
      } else {
        this.setValue(this.contributionChangeControl, null);
      }
    }


    // If both values are entered but change is not equal
    // or both values are not entered
    if (
      (
        (
          this.increaseContributionNewValue
          && (this.reduceContributionNewValue || this.reduceContributionNewValue === 0)
          && increaseCalculatedValue !== reduceCalculatedValue
          && this.increaseContributionNewControl.enabled
        )
        || (
          !this.increaseContributionNewValue
          && !(this.reduceContributionNewValue || this.reduceContributionNewValue === 0)
          && this.increaseContributionNewControl.enabled
        )
      )
      && this.contributionChangeControl.disabled
    ) {
      this.setValue(this.contributionChangeControl, null);
    }

    // If both values are not entered and contributionChangeControl is disabled
    if (
      !this.increaseContributionNewValue
      && !(this.reduceContributionNewValue || this.reduceContributionNewValue === 0)
      && this.contributionChangeControl.disabled
    ) {
      this.setValue(this.contributionChangeControl, null);
    }
  }

  private clearNewContributionOnPartnerChanges(): void {
    combineLatest([
      this.increasePartnerIdControl.valueChanges.pipe(distinctUntilChanged()),
      this.increaseShareholderIdControl.valueChanges.pipe(distinctUntilChanged()),
    ]).pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => {
        if (this.increaseContributionNewControl.enabled) {
          this.setValue(this.increaseContributionNewControl, null);

          if (this.reduceContributionNewValue && this.reduceContributionNewControl.enabled) {
            this.setValue(this.contributionChangeControl, null);
          }
        }
      });

    combineLatest([
      this.reducePartnerIdControl.valueChanges.pipe(distinctUntilChanged()),
      this.reduceShareholderIdControl.valueChanges.pipe(distinctUntilChanged()),
    ]).pipe(takeUntil(this.componentDestroyed$))
      .subscribe(() => {
        if (this.reduceContributionNewControl.enabled) {
          this.setValue(this.reduceContributionNewControl, null);

          if (this.increaseContributionNewValue && this.increaseContributionNewControl.enabled) {
            this.setValue(this.contributionChangeControl, null);
          }
        }
      });
  }

  private setShareholderOnPartnerChanges(): void {
    this.increasePartnerIdControl.valueChanges.pipe(
      distinctUntilChanged(),
    ).subscribe((partnerId) => {
      if (partnerId && !this.increaseShareholderIdValue) {
        const shareholderId: CompanyKey = this.partnerLatestContributionsMap.get(partnerId)?.shareholderId;

        if (shareholderId) {
          this.increaseShareholderIdControl.setValue(shareholderId);
        }
      }
    });

    this.reducePartnerIdControl.valueChanges.pipe(
      distinctUntilChanged(),
    ).subscribe((partnerId) => {
      if (partnerId && !this.reduceShareholderIdValue) {
        const shareholderId: CompanyKey = this.partnerLatestContributionsMap.get(partnerId)?.shareholderId;

        if (shareholderId) {
          this.reduceShareholderIdControl.setValue(shareholderId);
        }
      }
    });
  }

  private setValue(control: AbstractControl, value: null | string | number): void {
    /**
     *  If we change the control directly without setTimeout
     *  all later checks here will be done on changed value
    */
    setTimeout(() => control.setValue(value));
  }

  private disableControl(control: AbstractControl) {
    /**
     *  If we change the control directly without setTimeout
     *  all later checks here will be done on changed value
    */
    setTimeout(() => control.disable());
  }

  private enableControl(control: AbstractControl) {
    /**
     *  If we change the control directly without setTimeout
     *  all later checks here will be done on changed value
    */
    setTimeout(() => control.enable());
  }
}
