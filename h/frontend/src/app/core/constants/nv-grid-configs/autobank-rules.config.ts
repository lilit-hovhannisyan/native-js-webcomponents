import { NvGridButtonsPosition, NvGridConfig, NvFilterControl, NvColumnDataType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { Operations } from 'src/app/core/models/Operations';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { IAutobankRule } from '../../models/resources/IAutobankRule';
import { mandatorNoModifier } from '../../models/resources/IMandator';

export const getAutobankGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean,
  formats: { mandator: (id: number, property: string) => any }
): NvGridConfig => {
  return ({
    gridName: 'autobank',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 25,
    },
    columns: [
      {
        key: 'mandatorId',
        title: wording.accounting.mandator,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        customFormatFn: (row: IAutobankRule) => mandatorNoModifier(formats.mandator(row.mandatorId, 'no'))
      },
      {
        key: 'filterName',
        title: wording.accounting.filterName,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isActive',
        title: wording.accounting.active,
        width: 75,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        hidden: !canPerform(Operations.UPDATE),
        func: (row: IAutobankRule) => actions.edit(row.id),
      },
      {
        icon: 'delete',
        description: wording.general.delete,
        tooltip: wording.general.delete,
        actOnDoubleClick: false,
        hidden: !canPerform(Operations.DELETE),
        func: (autobank: IAutobankRule) => actions.delete(autobank),
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        hidden: () => !canPerform(Operations.CREATE),
        func: () => actions.add()
      },
    ],
    contextMenuButtons: canPerform(Operations.READ)
      ? getContextMenuButtonsConfig()
      : [],
  });
};
