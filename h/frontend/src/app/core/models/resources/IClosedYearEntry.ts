import { UserKey } from './IUser';

export type IClosedYearEntry = {
  year: number,
  period?: number,
  closedAt?: string,
  closedBy?: UserKey,
  id: number
};

