import { Component, OnInit, NgZone, Input, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { WeatherService } from '../../../../../examples/services/weather.service';
import { IWeather } from 'src/app/core/models/widgets/IWeather';
import { IWeatherWidget } from 'src/app/core/models/widgets/IWeatherWidget';
import { WeatherWidgetService } from '../../../../../examples/services/widgets/weather-widget-repository.service';
import { Subscription } from 'rxjs';
import { wording } from 'src/app/core/constants/wording/wording';

@Component({
  selector: 'nv-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeatherComponent implements OnInit, OnDestroy {
  public weather: IWeather;
  public editMode = false;
  @Input() private widget: IWeatherWidget;
  private subscription: Subscription;
  public wording = wording;

  constructor(
    private weatherService: WeatherService,
    private zone: NgZone,
    private weatherWidgetService: WeatherWidgetService,
    private cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    if (this.widget.geoLocation) {
      this.fetchWeather();
    } else {
      this.editMode = true;
    }
  }

  public ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }

  private fetchWeather() {
    this.subscription = this.weatherService.fetchWeatherByCityName(this.widget.geoLocation).subscribe(
      (weather: IWeather) => {
        this.weather = weather;
        this.cdr.detectChanges();
      }
    );
  }

  public setWeather(address: { name: string }): void {
    this.zone.run(() => {
      if (address.name) {
        this.weatherService.fetchWeatherByCityName(address.name).subscribe(
          res => {
            this.weather = res;
            this.widget = { ...this.widget, geoLocation: address.name };
            this.weatherWidgetService.updateWidget(this.widget)
              .pipe(finalize(() => {
                this.editMode = false;
                this.cdr.detectChanges();
              })).subscribe();
          });
      }
    });
  }

  public getWeatherIcon(): string {
    return `https://openweathermap.org/img/w/${this.weather.weather[0].icon}.png`;
  }

  public editWidget(): void {
    this.editMode = !this.editMode;
  }
}

