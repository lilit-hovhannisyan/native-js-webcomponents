export default interface RouterLoadedComponentLogI {
  eventType: string;
  data: {
    componentName: string;
  };
  userActionId: string | null;
}
