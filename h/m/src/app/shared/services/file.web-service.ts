import { Injectable, Type } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseWebService } from '@core-services/base.web-service';

import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { FileUploadModel } from '@shared-models/file-upload/file-upload.model';

@Injectable()
export class FileWebService {
  constructor(private baseWebService: BaseWebService) {}

  getFileById(fileOID: string): Observable<FileUploadModel> {
    return this.baseWebService.getRequest<FileUploadModel>(`${MARKETPLACE_BASE_API_URL}/definitions/${fileOID}`, FileUploadModel);
  }

  uploadFile<T>(data: FormData, classType: Type<T>): Observable<T> {
    return this.baseWebService.postRequest<T, FormData>(`${MARKETPLACE_BASE_API_URL}/files`, data, classType);
  }
}
