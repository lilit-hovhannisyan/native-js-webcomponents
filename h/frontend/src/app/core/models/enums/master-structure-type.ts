

import { wording } from '../../constants/wording/wording';
import { ISelectOption } from '../misc/selectOption';

export enum MASTER_STRUCTURE_TYPES {
  BALANCE_SHEET = 0,
  PROFIT_AND_LOSS_STATEMENT = 1,
  BUSINESS_MANAGEMENT_EVALUATION = 2,
}

export const masterStructureTypesOptions: ISelectOption<MASTER_STRUCTURE_TYPES>[] = [
  { value: MASTER_STRUCTURE_TYPES.BUSINESS_MANAGEMENT_EVALUATION, displayLabel: wording.accounting.businessManagementEvaluation },
  { value: MASTER_STRUCTURE_TYPES.BALANCE_SHEET, displayLabel: wording.accounting.balanceSheet },
  { value: MASTER_STRUCTURE_TYPES.PROFIT_AND_LOSS_STATEMENT, displayLabel: wording.accounting.profitAndLossStatement },
];
