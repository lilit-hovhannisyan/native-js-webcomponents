import {Component, Input} from '@angular/core';
import {NavigationItem} from '../molecules.interfaces';

@Component ({
  selector: 'lavash-navigation',
  templateUrl : './navigation.component.html',
  styleUrls: ['./navigation.component.less']
})

export class NavigationComponent {
  @Input() navItems: NavigationItem[] = [];
}


