import { SitemapNode } from '../../constants/sitemap/sitemap';

export interface ITopic extends SitemapNode {
  bookmarked?: boolean;
}
