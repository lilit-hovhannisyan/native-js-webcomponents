interface ColumnOptionsI {
  forField: string;
  headerName?: string;
  sortable?: boolean;
  hidden?: boolean;
}

export default ColumnOptionsI;
