import { NvGridConfig, NvColumnDataType, NvFilterControl, NvGridRowSelectionType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getDatesRatesConfig = (): NvGridConfig => ({
  gridName: 'datesRates',
  rowButtonsPosition: null,
  hideRefreshButton: true,
  isSortAscending: true,
  clearFiltersButton: true,
  sortBy: 'date',
  paging: {
    pageNumber: 1,
    pageSize: 10
  },
  columns: [
    {
      key: 'date',
      title: wording.general.date,
      width: 200,
      isSortable: true,
      dataType: NvColumnDataType.Date,
      filter: {
        controlType: NvFilterControl.FreeText,
        values: []
      }
    },
    {
      key: 'rate',
      title: wording.accounting.rate,
      dataType: NvColumnDataType.Decimal,
      decimalDigitsInfo: '1.5-5',
      width: 200,
      isSortable: true,
      filter: {
        values: []
      }
    },
  ],
  buttons: [],
  toolbarButtons: [],
});
