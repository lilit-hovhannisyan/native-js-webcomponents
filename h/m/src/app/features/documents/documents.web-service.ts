import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseWebService } from '../../core/services/base.web-service';

import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { BaseSearchModel } from '@shared/models/search/search-base.model';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { constructUrl } from '@shared/utils';

@Injectable()
export class DocumentsWebService {
  constructor(private baseWebService: BaseWebService) {}

  getMeDocuments(skip?: number, top?: number): Observable<ArrayResponseI<MeDocumentModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/documents`, skip, top);
    return this.baseWebService.getRequestForArray<MeDocumentModel>(url, MeDocumentModel);
  }

  deleteDocument(data: string[]): Observable<MeDocumentModel> {
    return this.baseWebService.deleteRequest<MeDocumentModel>(`${MARKETPLACE_BASE_API_URL}/documents`, data);
  }

  searchDocuments(data: BaseSearchModel, skip?: number, top?: number): Observable<ArrayResponseI<MeDocumentModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/documents/search`, skip, top);
    return this.baseWebService.postRequestForArray<MeDocumentModel, BaseSearchModel>(url, data, MeDocumentModel);
  }
}
