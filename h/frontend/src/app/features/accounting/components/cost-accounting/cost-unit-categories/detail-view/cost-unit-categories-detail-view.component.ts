import { Component, OnInit, ChangeDetectionStrategy, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ResourceDetailView } from 'src/app/core/abstracts/ResourceDetailView';
import { ICostUnitCategory, ICostUnitCategoryCreate } from 'src/app/core/models/resources/ICostUnitCategory';
import { RepoParams } from 'src/app/core/abstracts/abstract.repository';
import { FormGroup, FormBuilder } from '@angular/forms';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { CostUnitCategoryRepositoryService } from 'src/app/core/services/repositories/cost-unit-category-repository.service';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { ActivatedRoute } from '@angular/router';
import { Validators } from 'src/app/core/classes/validators';
import { ONLY_DIGITS_AND_LETTERS } from 'src/app/core/constants/regexp';

@Component({
  selector: 'nv-cost-unit-categories-detail-view',
  templateUrl: './cost-unit-categories-detail-view.component.html',
  styleUrls: ['./cost-unit-categories-detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CostUnitCategoriesDetailViewComponent
  extends ResourceDetailView<ICostUnitCategory, ICostUnitCategoryCreate>
  implements OnInit {

  protected repoParams: RepoParams<ICostUnitCategory> = {};
  public form: FormGroup;
  protected resourceDefault: ICostUnitCategory = {} as ICostUnitCategory;
  public routeBackUrl: string = url(
    sitemap.accounting.children.costAccounting.children.costUnitCategories
  );
  public formElement: ElementRef;

  constructor(
    baseComponentService: BaseComponentService,
    costUnitCategoryRepo: CostUnitCategoryRepositoryService,
    private fb: FormBuilder,
    private route: ActivatedRoute,
    public el: ElementRef,
    private cdr: ChangeDetectorRef,
  ) {
    super(costUnitCategoryRepo, baseComponentService);
  }

  public getTitle: () => string = (): string =>
    `${this.resource.displayLabel[this.settingsService.language]}`


  public ngOnInit(): void {
    this.initResource();
  }

  public init(costUnitCategory: ICostUnitCategory = this.resourceDefault): void {
    this.initForm(this.fb.group({
      labelDe: [
        costUnitCategory.labelDe,
        [Validators.required, Validators.maxLength(150)]
      ],
      labelEn: [
        costUnitCategory.labelEn,
        [Validators.required, Validators.maxLength(150)]
      ],
      abbreviation: [
        costUnitCategory.abbreviation,
        [Validators.required, Validators.maxLength(2), Validators.onlyNumberAndLetter]
      ],
    }));
  }

  public onAbbreviationKeyDown(event: KeyboardEvent): void {
    if (!event.key.match(ONLY_DIGITS_AND_LETTERS)) {
      event.preventDefault();
    }
  }

  private initResource(): void {
    const { id } = this.route.snapshot.params;

    id === 'new'
      ? this.enterCreationMode()
      : this.loadResource(id);
  }

  public submit(): void {
    if (!this.form.valid) {
      return;
    }

    const { value } = this.form;

    this.creationMode
      ? this.createResource(value)
      : this.updateResource({ ...this.resource, ...value });
  }

  public formInitialized(element: ElementRef) {
    this.formElement = element;
    this.cdr.detectChanges();
  }
}
