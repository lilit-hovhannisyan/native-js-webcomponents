import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountingRelationsComponent } from './accounting-relations.component';

describe('AccountingRelationsComponent', () => {
  let component: AccountingRelationsComponent;
  let fixture: ComponentFixture<AccountingRelationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountingRelationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountingRelationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
