import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { AfterViewInit, Component, Injector, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatHorizontalStepper, MatStep } from '@angular/material/stepper';
import { ActivatedRoute, Router } from '@angular/router';

import { finalize, take, takeUntil } from 'rxjs/operators';
import { BehaviorSubject, combineLatest, Subject } from 'rxjs';

import { BaseLoggerComponent } from '@features/tracking-system';

import { EntityI } from '@core-interfaces/entity.interface';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { InputAttributeService } from '@shared/services/input-attribute.service';
import { FileWebService } from '@shared/services/file.web-service';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';

import {
  MARKETPLACE_BASE_API_URL,
  ME_DOMAIN_APPLICATION_DOCUMENT,
  ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_CERTIFICATE,
  ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_GENERIC,
  ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL,
  ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL_VIDEO
} from '@shared/constants';
import { MeAttributeLayoutTypes } from '@shared/me-attribute-layout-types';
import { MeAttributeTypes } from '@shared/me-attribute-types';
import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';
import { MeSelectInputComponent } from '@shared/me-components/me-attribute/me-select-input/me-select-input.component';
import { MeBasicAlertI } from '@shared/me-components/me-basic-alert/me-basic-alert.interface';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { MeButtonI } from '@shared/me-components/me-button/me-button.interface';
import { MeSweetAlertI, MeSweetAlertTypeEnum } from '@shared/me-components/me-sweet-alert/me-sweet-alert.interface';
import { MeSweetAlertService } from '@shared/me-components/me-sweet-alert/me-sweet-alert.service';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { MeUploadFileI } from '@shared/me-components/me-upload-file/me-upload-file.interface';
import { bytesToSize } from '@shared/utils';
import { RequiredConstraintState } from '@shared/models/attribute/attribute.constraint.model';
import { AttributeModelTypes, DMChoiceAttributeModel } from '@shared/models/attribute/attribute.model';
import { MeSelectInputAttribute } from '@shared/models/attribute/me-select-input-attribute.model';
import { DomainModel } from '@shared/models/domain/domain.model';
import { EnumValueModel } from '@shared/models/enum/enum-value.model';
import { LayoutGroupAttributeModel } from '@shared/models/layout/layout-group-attribute.model';
import { LayoutGroupModel } from '@shared/models/layout/layout-group.model';
import { FileUploadResponseModel } from '@shared/models/file-upload/file-upload-response.model';
import { LayoutRowModel } from '@shared/models/layout/layout-row.model';

import { DocumentCreateWebService } from './document-create-edit.web-service';
import { GlobalService } from '@core-services/global.service';
import { DeactivatableComponent } from '@core-interfaces/deactivatable-component.interface';
import { RouterStoreService } from '@core-services/router-store.service';
import { BaseUploadModel } from '@shared/models/file-upload/file-upload.model';
import { MeTranslationI } from '@core-interfaces/me-translation.interface';
import { MeTranslationService } from '@core-services/me-translation.service';

@Component({
  selector: 'me-document-create-edit',
  templateUrl: './document-create-edit.component.html',
  styleUrls: ['./document-create-edit.component.scss'],
  providers: [DocumentCreateWebService, FileWebService, InputAttributeService]
})
export class DocumentCreateComponent extends BaseLoggerComponent implements OnInit, OnDestroy, AfterViewInit, DeactivatableComponent {
  componentVersion: string = '0.0.1';

  private _takeUntilDestroy$: Subject<void> = new Subject();
  applyCheck: boolean = false;
  leaveWarningModel!: MeSweetAlertI;

  componentLoading: boolean = true;
  document!: MeDocumentModel;
  documentId: string | null = null;
  isTypeChange: boolean = false;

  domain: DomainModel | undefined;
  groups: LayoutGroupModel[] = [];
  attributes: AttributeModelTypes[] = [];
  excludedDomains: string[] = [ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_GENERIC];

  // Tab 1
  stepOneGroups: string[] = ['generalInformation'];
  infoLayoutGroups!: LayoutGroupModel[];
  infoAttributeForm!: FormGroup;
  infoAttributeItemMap!: Map<string, MeAttributeItem>;

  selectedDocumentType!: string;
  selectedDocumentTypeUploadModel: BaseUploadModel | undefined;
  documentTypesAttribute: DMChoiceAttributeModel = new DMChoiceAttributeModel();
  documentTypeAttributeItem!: MeAttributeItem;
  videoSources: { source: string; type: string }[] = [];

  isDocumentUploadInProgress: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  // Tab 2
  attributesForm!: FormGroup | undefined;
  attributesLayoutGroup!: LayoutGroupModel[];
  attributesItemMap!: Map<string, MeAttributeItem>;

  buttonNext!: { label: string; data: MeButtonI };
  buttonBack!: { label: string; data: MeButtonI };
  buttonCreate!: { label: string; data: MeButtonI };
  buttonSubmit!: { label: string; data: MeButtonI };

  // Upload file
  isUploadEnabled: boolean = true;
  uploadFileData!: MeUploadFileI;
  private fileAttributeName = 'primaryContent';
  invalidFile: boolean = false;

  private appliedState: RequiredConstraintState = RequiredConstraintState.draft;

  @ViewChild('stepper') stepper!: MatHorizontalStepper;

  i18nStrings: MeTranslationI = {};

  // Buttons used in form
  buttons: Map<string, { label: string; data: MeButtonI }> = new Map();

  constructor(
    injector: Injector,
    private translate: MeTranslationService,
    private definitionsStoreService: DefinitionsStoreService,
    private meBasicAlertService: MeBasicAlertService,
    private meSweetAlertService: MeSweetAlertService,
    public inputAttributeService: InputAttributeService,
    private webService: DocumentCreateWebService,
    private router: Router,
    private fileWebService: FileWebService,
    private route: ActivatedRoute,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private globalService: GlobalService,
    private routerStoreService: RouterStoreService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.documentId = this.route.snapshot.paramMap.get('id');
    this.isTypeChange = JSON.parse(this.route.snapshot.queryParamMap.get('isTypeChange') || 'false');

    // subscribe to translations
    this.translate
      .getTranslations()
      .pipe(takeUntil(this._takeUntilDestroy$))
      .subscribe((translations) => {
        this.i18nStrings = translations;
        this.prepareButtons();
      });

    this.leaveWarningModel = {
      icon: 'alert-triangle',
      mode: 'warning',
      title: 'Warning',
      message: 'You are leaving page, data will be lost!',
      type: { name: MeSweetAlertTypeEnum.submit, buttons: { submit: 'Leave', cancel: 'Stay on page' } }
    };

    this.breadcrumbSupportService.breadcrumbActions = [
      {
        button: {
          id: 'cancelButton',
          data: { color: 'secondary', type: 'regular' },
          text: this.i18nStrings.cancel
        },
        callBack: () => {
          const lastRoute = this.routerStoreService.getPreviousUrl();
          if (lastRoute === decodeURI(this.router.routerState.snapshot.url)) {
            this.router.navigate(['documents']);
          } else {
            this.router.navigate([lastRoute]);
          }
        }
      }
    ];

    this.isDocumentUploadInProgress.pipe(takeUntil(this._takeUntilDestroy$)).subscribe((value) => {
      this.buttonSubmit.data = {
        ...this.buttonSubmit.data,
        disabled: value
      };
    });

    this.globalService.activateLoader();
    if (this.documentId) {
      this.initializeEdit();
    } else {
      this.initializeCreate();
    }
  }

  ngAfterViewInit(): void {
    // TODO: can be sync with new loader, now there is small delay on loading finish
    if (this.documentId && !this.isTypeChange) {
      this.stepper.animationDone.pipe(take(1), takeUntil(this._takeUntilDestroy$)).subscribe(() => {
        this.stepper.steps.forEach((s) => (s.interacted = true));
      });
    }
  }

  initializeCreate(): void {
    this.definitionsStoreService.dataLoaded$.pipe(takeUntil(this._takeUntilDestroy$)).subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.domain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_DOCUMENT);
        this.globalService.deactivateLoader();
        if (this.domain) {
          // transform each domain attribute to AttributeModel class or its extensions (classes that extend it)
          this.attributes = this.inputAttributeService.transformAttributesArray(this.domain.attributes);

          // populate groups from selected layout
          this.groups = this.inputAttributeService.getLayoutGroups(this.domain.layouts, MeAttributeLayoutTypes.CREATE);

          // sort and generate rows
          this.groups = this.inputAttributeService.generateLayoutRows(this.groups);

          // generate angular forms which will have form group as root for every tab
          // each attribute will be a form control
          // add validators to attribtes where applicable
          // load enums to single/multiple choices elements
          this.infoLayoutGroups = this.getLayoutForGroups(this.stepOneGroups);
          if (this.infoLayoutGroups && this.infoLayoutGroups.length) {
            this.infoAttributeForm = this.inputAttributeService.createFormGroup(
              this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.infoLayoutGroups),
              this.appliedState
            );

            this.infoAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
              this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.infoLayoutGroups),
              this.infoAttributeForm,
              this.appliedState
            );
          }

          this.attachDocumentTypeControl();

          // get attribute and create control for thumbnail
          // this.infoAttributeForm.addControl(this.fileAttributeName, new FormControl(null, Validators.required));

          this.infoAttributeForm.valueChanges.pipe(takeUntil(this._takeUntilDestroy$)).subscribe(() => {
            if (this.infoAttributeForm.dirty) {
              this.applyCheck = true;
            }
          });
        }
      }
    });
  }

  initializeEdit(): void {
    this.definitionsStoreService.dataLoaded$.pipe(takeUntil(this._takeUntilDestroy$)).subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.webService.getDocumentByOid(encodeURI(this.documentId as string)).subscribe((response) => {
          this.document = response;
          if (this.document.state) {
            this.appliedState = this.document.state;
          }
          this.attachFileUpload(this.document.domainName, response.attributesExtended.primaryContent);

          this.domain = this.definitionsStoreService.getDomain(this.document.domainName);
          this.globalService.deactivateLoader();

          if (this.domain) {
            this.attributes = this.inputAttributeService.transformAttributesArray(this.domain.attributes);
            this.inputAttributeService.applyUpdateFormValues(this.attributes, this.document);
            this.groups = this.inputAttributeService.getLayoutGroups(this.domain.layouts, MeAttributeLayoutTypes.UPDATE);
            this.groups = this.inputAttributeService.generateLayoutRows(this.groups);

            this.infoLayoutGroups = this.getLayoutForGroups(this.stepOneGroups);
            if (this.infoLayoutGroups && this.infoLayoutGroups.length) {
              this.infoAttributeForm = this.inputAttributeService.createFormGroup(
                this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.infoLayoutGroups),
                this.appliedState
              );

              // control for thumbnail
              // this.infoAttributeForm.addControl(this.fileAttributeName, new FormControl(this.document.attributes.name));

              this.infoAttributeItemMap = this.inputAttributeService.createAttributeItemMap(
                this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.infoLayoutGroups),
                this.infoAttributeForm,
                this.appliedState
              );
            }

            this.attributesLayoutGroup = this.getLayoutForGroupsExcluding(this.stepOneGroups);
            if (this.attributesLayoutGroup && this.attributesLayoutGroup.length) {
              this.attributesForm = this.inputAttributeService.createFormGroup(
                this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.attributesLayoutGroup),
                this.appliedState
              );

              this.attributesItemMap = this.inputAttributeService.createAttributeItemMap(
                this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.attributesLayoutGroup),
                this.attributesForm,
                this.appliedState
              );
            }

            this.attachDocumentTypeControl(!this.isTypeChange);
            this.checkToLoadVideo(response.attributes.primaryContent);

            // Track changes, if user changed value check when he try to leave page
            // Combine latest (instead of race) is currently best way to listen for multiple observables
            const observableArray = [this.infoAttributeForm.valueChanges];
            if (this.attributesForm) {
              observableArray.push(this.attributesForm.valueChanges);
            }
            combineLatest(observableArray)
              .pipe(takeUntil(this._takeUntilDestroy$))
              .subscribe(() => {
                if (this.infoAttributeForm.dirty || this.attributesForm?.dirty) {
                  this.applyCheck = true;
                }
              });
          }
        });
      }
    });
  }

  prepareButtons(): void {
    this.buttonNext = { label: this.i18nStrings.next, data: { type: 'regular', color: 'primary' } };
    this.buttonBack = { label: this.i18nStrings.back, data: { type: 'regular', color: 'secondary' } };
    this.buttonSubmit = {
      label: this.documentId ? this.i18nStrings.update : this.i18nStrings.create,
      data: { type: 'regular', color: 'primary' }
    };
  }

  getLayoutForGroups(layoutGroupNames: string[]): LayoutGroupModel[] {
    const layoutGroups = this.groups.filter((group) => layoutGroupNames.findIndex((name) => group.internalName === name) > -1);
    return layoutGroups;
  }

  getLayoutForGroupsExcluding(layoutGroupNames: string[]): LayoutGroupModel[] {
    const layoutGroups = this.groups.filter((group) => layoutGroupNames.findIndex((name) => group.internalName === name) === -1);

    return layoutGroups;
  }

  getRowIndex(_index: number, item: LayoutRowModel): number {
    return item.rowIndex;
  }

  getAttributeName(_index: number, item: LayoutGroupAttributeModel): string {
    return item.internalName;
  }

  stepSubmitEventHandler(stepIndex: number, form: FormGroup): void {
    if (stepIndex === 0) {
      this.checkIfFileIsSet();
    }
    this.handleDocument(form);
  }

  stepNextEventHandler(swithTo: number, emmitNext?: boolean): void {
    this.checkIfFileIsSet();
    this.checkStepBeforeSwitch(swithTo);
    if (emmitNext) {
      this.stepper.next();
    }
  }

  checkIfFileIsSet(): void {
    if (this.uploadFileData && !this.infoAttributeForm.controls[this.fileAttributeName].value) {
      this.uploadFileData = {
        ...this.uploadFileData,
        isStatusInvalid: this.infoAttributeForm.controls[this.fileAttributeName].hasError('required')
      };
    }
  }

  // On tab index change set previous step form as touched
  markFormAsTouched(changeObj: StepperSelectionEvent): void {
    changeObj.previouslySelectedStep.stepControl.markAllAsTouched();
  }

  checkStepBeforeSwitch(swithcedTo: number): void {
    const currentStep: MatStep = this.stepper.steps.toArray()[this.stepper.selectedIndex];
    if (!currentStep.completed) {
      if (this.stepper.selectedIndex !== swithcedTo) {
        currentStep.stepControl.markAllAsTouched();
      }
      if (this.stepper.selectedIndex < swithcedTo) {
        const emptyRequiredAttributes: AttributeModelTypes[] = [];
        const currentStepControl: FormGroup = currentStep.stepControl as FormGroup;
        // Collect all empty required attributes
        Object.keys(currentStepControl.controls).forEach((controlName) => {
          const validationErrors: ValidationErrors | null = currentStepControl.controls[controlName].errors;
          if (validationErrors && validationErrors.required) {
            const attribute = [...this.attributes, this.documentTypesAttribute].find((attr) => attr.internalName === controlName);
            if (attribute) {
              emptyRequiredAttributes.push(attribute);
            }
          }
        });

        if (emptyRequiredAttributes.length > 0) {
          const basicAlertData: MeBasicAlertI = {
            mode: 'error',
            title: this.i18nStrings.insertRequiredData,
            content: this.i18nStrings.requiredFields
          };
          emptyRequiredAttributes.forEach((attribute, index) => {
            basicAlertData.content += ` ${attribute.displayName}`;
            basicAlertData.content += emptyRequiredAttributes.length - 1 !== index ? ',' : '';
          });
          this.meBasicAlertService.openBasicAlert(basicAlertData);

          // const queryString = emptyRequiredAttributes.find((attr) => attr.internalName === 'thumbnail')
          //   ? '.thumbnail-container'
          //   : 'app-form-attribute mat-form-field.mat-form-field-invalid';

          // setTimeout(() => {
          //   this.scrollToElementProvider.scrollToElementByQuery(queryString);
          //   this.scrollToElementProvider.doScroll();
          // }, 100);
        }
      }
    }
  }

  updateDocumentProperties(documentType: string): void {
    this.domain = this.definitionsStoreService.getDomain(documentType);
    if (this.domain) {
      this.attributes = this.inputAttributeService.transformAttributesArray(this.domain.attributes);
      this.groups = this.inputAttributeService.getLayoutGroups(this.domain.layouts, MeAttributeLayoutTypes.CREATE);
      this.groups = this.inputAttributeService.generateLayoutRows(this.groups);

      this.infoLayoutGroups = this.getLayoutForGroups(this.stepOneGroups);
      if (this.infoLayoutGroups && this.infoLayoutGroups.length) {
        this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.infoLayoutGroups).forEach((attr) => {
          if (!this.infoAttributeForm.contains(attr.internalName)) {
            this.infoAttributeForm.addControl(
              attr.internalName,
              new FormControl(null, this.inputAttributeService.applyConstraints(attr, this.appliedState))
            );

            if (!this.infoAttributeItemMap.has(attr.internalName)) {
              const tempMap: Map<string, MeAttributeItem> = this.inputAttributeService.createAttributeItemMap(
                [attr],
                this.infoAttributeForm,
                this.appliedState
              );
              this.infoAttributeItemMap.set(attr.internalName, tempMap.get(attr.internalName) as MeAttributeItem);
            }
          }
        });
      }

      // Create step 2
      this.attributesLayoutGroup = this.getLayoutForGroupsExcluding(this.stepOneGroups);
      if (this.attributesLayoutGroup.length) {
        this.attributesForm = this.inputAttributeService.createFormGroup(
          this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.attributesLayoutGroup),
          this.appliedState
        );

        this.attributesItemMap = this.inputAttributeService.createAttributeItemMap(
          this.inputAttributeService.getAttributesByLayoutGroups(this.attributes, this.attributesLayoutGroup),
          this.attributesForm,
          this.appliedState
        );
      } else {
        this.attributesForm = undefined;
        this.attributesItemMap = new Map();
      }
    }
  }

  handleFileFormData(recievedFile: File): void {
    const formData = new FormData();
    formData.append(
      'meta',
      JSON.stringify({
        type: this.selectedDocumentTypeUploadModel?.type
      })
    );
    formData.append('file', recievedFile, recievedFile.name);

    this.isDocumentUploadInProgress.next(true);
    this.fileWebService
      .uploadFile(formData, FileUploadResponseModel)
      .pipe(
        takeUntil(this._takeUntilDestroy$),
        finalize(() => {
          this.isDocumentUploadInProgress.next(false);
        })
      )
      .subscribe(
        (response) => {
          if (response.attributes.name) {
            this.uploadFileData = {
              ...this.uploadFileData,
              fileName: response.attributes.name,
              showProgress: false
            };
            this.updateDocumentValue(response.oid);
          } else {
            this.uploadFileData = {
              ...this.uploadFileData,
              showProgress: false
            };
            this.updateDocumentValue(null);
          }
        },
        (_error: ErrorResponseI) => {
          this.uploadFileData = { ...this.uploadFileData, isStatusInvalid: true };
        }
      );
  }

  updateDocumentValue(documentOR: string | null): void {
    if (this.infoAttributeForm) {
      this.infoAttributeForm.markAsDirty();
      this.infoAttributeForm.controls[this.fileAttributeName].setValue(documentOR);
    }
    this.checkToLoadVideo(documentOR);
  }

  checkToLoadVideo(documentOR: string | null): void {
    if (this.domain?.internalName === ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL_VIDEO) {
      this.videoSources = !!documentOR
        ? [
            {
              source: `${MARKETPLACE_BASE_API_URL}/files/download/${documentOR}`,
              type: 'video/mp4'
            }
          ]
        : [];
    } else {
      this.videoSources = [];
    }
  }

  handleDocument(form: FormGroup): void {
    if (!this.isDocumentUploadInProgress.value) {
      if (this.documentId) {
        this.updateDocument(form);
      } else {
        this.createDocument(form);
      }
    }
  }

  createDocument(form: FormGroup): void {
    form.markAllAsTouched();
    if (form.valid) {
      this.globalService.activateLoader();
      const documentEntity: EntityI = {
        domainName: this.selectedDocumentType,
        attributes: {}
      };

      // Add step1 attributes and values to document entity
      if (this.infoLayoutGroups && this.infoLayoutGroups.length) {
        this.inputAttributeService
          .getAttributesByLayoutGroups(this.attributes, this.infoLayoutGroups)
          .forEach((attribute) => this.setAttributeWithValueToEntity(documentEntity, attribute, this.infoAttributeForm));
      }

      // Add file
      documentEntity.attributes[this.fileAttributeName] = this.infoAttributeForm.controls[this.fileAttributeName].value;

      // Add step2 attributes and values to document entity
      if (this.attributesLayoutGroup && this.attributesLayoutGroup.length) {
        this.inputAttributeService
          .getAttributesByLayoutGroups(this.attributes, this.attributesLayoutGroup)
          .forEach((attribute) => this.setAttributeWithValueToEntity(documentEntity, attribute, this.attributesForm));
      }

      this.webService
        .createDocument(documentEntity)
        .pipe(
          takeUntil(this._takeUntilDestroy$),
          finalize(() => {
            this.globalService.deactivateLoader();
          })
        )
        .subscribe((response) => {
          this.meSweetAlertService
            .getDataBackFromSweetAlert()
            .pipe(take(1), takeUntil(this._takeUntilDestroy$))
            .subscribe((data) => {
              this.infoAttributeForm.markAsUntouched();
              this.applyCheck = false;
              if (data.confirmed) {
                // TODO: PROCCED
                console.log(response);
              } else {
                this.redirectToDocuments();
              }
            });

          const sweetAlertModel: MeSweetAlertI = {
            mode: 'success',
            icon: 'check',
            type: {
              name: MeSweetAlertTypeEnum.confirm,
              buttons: {
                confirm: this.i18nStrings.finish
              }
            },
            title: this.i18nStrings.documentCreatedAlertTitle,
            message: this.i18nStrings.documentCreatedAlertMessage
          };
          this.meSweetAlertService.openMeSweetAlert(sweetAlertModel);
        });
    }
  }

  updateDocument(form: FormGroup): void {
    form.markAllAsTouched();
    if (form.valid) {
      const documentChanges: EntityI = {
        domainName: this.isTypeChange ? this.infoAttributeForm.controls.documentType.value : this.document.domainName,
        attributes: {}
      };

      // Add step1 attributes and values to document entity
      if (this.infoLayoutGroups && this.infoLayoutGroups.length) {
        this.infoLayoutGroups[0].attributes.forEach((attribute) =>
          this.setAttributeWithValueToEntity(documentChanges, attribute, this.infoAttributeForm)
        );
      }

      // Add file
      documentChanges.attributes[this.fileAttributeName] = this.isUploadEnabled
        ? this.infoAttributeForm.controls[this.fileAttributeName].value
        : null;

      // Add step2 attributes and values to document entity
      if (this.attributesLayoutGroup && this.attributesLayoutGroup.length) {
        this.inputAttributeService
          .getAttributesByLayoutGroups(this.attributes, this.attributesLayoutGroup)
          .forEach((attribute) => this.setAttributeWithValueToEntity(documentChanges, attribute, this.attributesForm));
      }

      if (this.isTypeChange) {
        this.webService
          .changeDocumentType(encodeURI(this.document.oid), documentChanges)
          .pipe(takeUntil(this._takeUntilDestroy$))
          .subscribe(() => {
            form.markAsUntouched();
            this.handleSuccesfullUpdate();
          });
      } else {
        this.webService
          .updateDocument(encodeURI(this.document.oid), documentChanges)
          .pipe(takeUntil(this._takeUntilDestroy$))
          .subscribe(() => {
            form.markAsUntouched();
            this.handleSuccesfullUpdate();
          });
      }
    }
  }

  private redirectToDocuments(): void {
    if (this.domain?.internalName.includes(ME_DOMAIN_APPLICATION_DOCUMENT_VISUAL)) {
      this.router.navigate(['documents', 'visuals']);
    } else {
      this.router.navigate(['documents']);
    }
  }

  private handleSuccesfullUpdate(): void {
    // start tracking for response
    this.meSweetAlertService
      .getDataBackFromSweetAlert()
      .pipe(take(1), takeUntil(this._takeUntilDestroy$))
      .subscribe(() => {
        this.applyCheck = false;
        this.redirectToDocuments();
      });

    const successAlertModel: MeSweetAlertI = {
      mode: 'success',
      icon: 'check',
      type: {
        name: MeSweetAlertTypeEnum.confirm,
        buttons: {
          confirm: this.i18nStrings.continue
        }
      },
      title: this.i18nStrings.success,
      message: this.i18nStrings.itemUpdated
    };
    this.meSweetAlertService.openMeSweetAlert(successAlertModel);
  }

  private getInstantiableDescendantDomainsEnum(documentDomain: DomainModel): EnumValueModel[] {
    const types: EnumValueModel[] = [];
    if (documentDomain.descendants?.length) {
      documentDomain.descendants.forEach((desc) => types.push(...this.getInstantiableDescendantDomainsEnum(desc)));
    } else if (documentDomain.instantiable && !this.excludedDomains.includes(documentDomain.internalName)) {
      const enumValue = new EnumValueModel();
      enumValue.displayName = documentDomain.displayName;
      enumValue.value = documentDomain.internalName;
      types.push(enumValue);
    }
    return types;
  }

  private attachDocumentTypeControl(disabled: boolean = false): void {
    // create attribute and control for document types
    this.documentTypesAttribute.dataTypeClass = MeAttributeTypes.CHOICE;
    this.documentTypesAttribute.displayName = this.i18nStrings.documentType;
    this.documentTypesAttribute.internalName = 'documentType';
    this.documentTypesAttribute.required = true;
    this.documentTypesAttribute.values = this.getInstantiableDescendantDomainsEnum(
      this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_DOCUMENT)
    );

    // THIS IS HARD CODED - remove Certificate document type from the list of Document types for creation
    if (!this.documentId) {
      this.documentTypesAttribute = {
        ...this.documentTypesAttribute,
        values: this.documentTypesAttribute.values.filter((item) => item.value !== ME_DOMAIN_APPLICATION_DOCUMENT_TECHNICAL_CERTIFICATE)
      };
    }

    this.documentTypesAttribute.disabled = disabled;
    if (disabled && this.domain) {
      this.documentTypesAttribute.value = this.domain.internalName;
    }

    this.infoAttributeForm.addControl(
      this.documentTypesAttribute.internalName,
      new FormControl(this.documentTypesAttribute.value, Validators.required)
    );

    const documentTypesInputAttribute: MeSelectInputAttribute = new MeSelectInputAttribute();
    documentTypesInputAttribute.data = this.documentTypesAttribute;
    documentTypesInputAttribute.form = this.infoAttributeForm;

    this.documentTypeAttributeItem = new MeAttributeItem(MeSelectInputComponent, documentTypesInputAttribute);

    // track changes of document type
    const documentTypeControl: AbstractControl | null = this.infoAttributeForm.get(this.documentTypesAttribute.internalName);
    if (documentTypeControl && !disabled) {
      documentTypeControl.valueChanges.pipe(takeUntil(this._takeUntilDestroy$)).subscribe((value) => {
        if (value) {
          this.selectedDocumentType = value;
          this.updateDocumentProperties(value);
          this.attachFileUpload(value);
          this.checkToLoadVideo(null);
        }
      });
    }
  }

  private attachFileUpload(domain: string, primaryContent?: string): void {
    this.selectedDocumentTypeUploadModel = this.definitionsStoreService.generalUploadProperties?.find((uploadModel) => {
      return uploadModel.limits.some((limit) => {
        return domain.indexOf(limit.domain) > -1 && limit.attribute === 'primaryContent';
      });
    });
    if (this.selectedDocumentTypeUploadModel) {
      this.uploadFileData = {
        fileName: primaryContent,
        isStatusInvalid: false,
        supportedFiles: this.selectedDocumentTypeUploadModel?.supportedFiles,
        maxUploadSize: this.selectedDocumentTypeUploadModel?.maxSize
          ? bytesToSize(this.selectedDocumentTypeUploadModel?.maxSize)
          : undefined
      };
    }
  }

  private setAttributeWithValueToEntity(
    entity: EntityI,
    attribute: AttributeModelTypes | LayoutGroupAttributeModel,
    formGroup?: FormGroup
  ): void {
    if (
      formGroup &&
      formGroup.controls.hasOwnProperty(attribute.internalName) &&
      formGroup.controls[attribute.internalName].hasOwnProperty('value') &&
      formGroup.controls[attribute.internalName].value !== null
    ) {
      entity.attributes[attribute.internalName] = formGroup.controls[attribute.internalName].value;
    }
  }

  ngOnDestroy(): object {
    this._takeUntilDestroy$.next();
    this._takeUntilDestroy$.complete();
    return super.ngOnDestroy();
  }
}
