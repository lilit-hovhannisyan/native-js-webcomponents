
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { wording } from 'src/app/core/constants/wording/wording';
import { SettingsService } from 'src/app/core/services/settings.service';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';

@Injectable()
export class CurreniesHelperService {
  public editViewTitleSubject: BehaviorSubject<string> = new BehaviorSubject('');
  public editViewTitle$ = this.editViewTitleSubject.asObservable();
  constructor(
    public settingsService: SettingsService,
  ) { }

  public generateTitle = (currency?: ICurrency) => {
    let title = wording.general.create[this.settingsService.language];

    if (currency) {
      title = `${currency.isoCode} ${currency.displayLabel[this.settingsService.language]}`;
    }

    this.editViewTitleSubject.next(title);
  }
}
