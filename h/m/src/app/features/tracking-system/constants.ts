export const PROPERTIES_TO_EXCLUDE = [
  'loggingService',
  'loggingConfig',
  'componentVersion',
  'elementClickEventsSubscription',
  'elementChangeEventsSubscription',
  'elementRef',
  '__ngContext__'
];

export const VALID_TAGS_FOR_INNER_TEXT = ['BUTTON', 'SPAN', 'LABEL', 'A', 'P', 'H1', 'H2', 'H3', 'H4'];

export const SKIP_ME_TRACKING_SYSTEM_REQUEST = 'SKIP_ME_TRACKING_SYSTEM_REQUEST';
