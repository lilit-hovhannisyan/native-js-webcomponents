import { Input, HostBinding, Directive } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { WizardService } from '../../services/wizard.service';

@Directive()
export abstract class WizardStep {

  @Input() public stepId: number;
  @Input() public title: string;

  public isSelected = false;
  public isValid: boolean;

  public form: FormGroup;

  constructor(protected wizardService: WizardService) { }

  // we use this function which is called by the wizard accounting on AfterContentInit
  // insted of ngOnInit here. ngOnInit might get overriden by
  // child-class of WizardStep

  public _init(): void {
    this.form && this.form.statusChanges.subscribe(() => {
      const validityChanged = this.isValid !== this.form.valid;
      validityChanged && this.updateValidity();
    });
  }

  public turnActive(): void {
    this.isSelected = true;
    this.updateValidity();
  }

  public turnInactive(): void {
    this.isSelected = false;
  }

  public updateValidity = (): void => {
    this.wizardService.setValidity(this.form.valid);
  }

  @HostBinding('hidden')
  public get hidden(): boolean { return !this.isSelected; }
}
