import { Component, OnInit } from '@angular/core';
import {Field} from 'l-builder';

@Component({
  selector: 'app-select-group',
  templateUrl: './select-group.component.html',
  styleUrls: ['./select-group.component.less']
})
export class SelectGroupComponent {
    fieldsConfig: Field[] = [
        {
            component: 'input',
            name: 'groupId',
            descriptors: {
                placeholder: 'select group: ex. lilit hovhannisyan'
            },
            validationRules: {
                required: {
                    value: true,
                    customErrMsg: 'Group name is required'
                }
            }
        }
    ];
}
