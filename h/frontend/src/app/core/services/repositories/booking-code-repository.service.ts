import { BookingCodeKey } from './../../models/resources/IBookingCode';
import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { IBookingCode, IBookingCodeDTO, IBookingCodeCreate } from '../../models/resources/IBookingCode';
import { RepositoryService } from '../repository.service';

export interface BookingCodeParams extends RepoParams<IBookingCode> {
  mandatorId?: BookingCodeKey;
}
@Injectable({
  providedIn: 'root'
})
export class BookingCodeRepositoryService extends AbstractRepository<
  IBookingCode,
  IBookingCodeDTO,
  IBookingCodeCreate,
  BookingCodeParams,
  BookingCodeKey
>  {
  protected resourceType = 'BookingCode';
  public sortAsce = true;
  public sortKey = 'no';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess(item: IBookingCodeDTO): IBookingCode { return item; }

  public getBaseUrl(params: BookingCodeParams): string {
    return params.mandatorId
      ? `/api/mandators/${params.mandatorId}/booking-codes`
      : `/api/booking-codes`;
  }
}
