import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BasePermissionGuard } from '@core-guards/base-permission.guard';
import { MePermissionTypes } from '@shared/me-permission-types';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'materials',
    loadChildren: () => import('@features/materials/materials.module').then((m) => m.MaterialsModule),
    canActivate: [BasePermissionGuard],
    data: {
      permission: MePermissionTypes.MATERIAL_VIEW
    }
  },
  // {
  //   path: 'dashboard',
  //   loadChildren: () => import('@features/dashboard/dashboard.module').then((m) => m.DashboardModule)
  // },
  {
    path: 'home',
    loadChildren: () => import('@features/home/home.module').then((m) => m.HomeModule)
  },
  {
    path: 'collections',
    loadChildren: () => import('@features/collections/collections.module').then((m) => m.CollectionsModule),
    canActivate: [BasePermissionGuard],
    data: {
      permission: MePermissionTypes.COLLECTION_VIEW
    }
  },
  {
    path: 'experiments',
    loadChildren: () => import('@features/experiments/experiments.module').then((m) => m.ExperimentsModule)
  },
  {
    path: 'documents',
    loadChildren: () => import('@features/documents/documents.module').then((m) => m.DocumentsModule),
    canActivate: [BasePermissionGuard],
    data: {
      permission: MePermissionTypes.DOCUMENT_VIEW
    }
  },
  {
    path: 'user-profile',
    loadChildren: () => import('@features/user-profile/user-profile.module').then((m) => m.UserProfileModule)
  },
  {
    path: 'messages',
    loadChildren: () => import('@features/messages/messages.module').then((m) => m.MessagesModule)
  },
  {
    path: 'about',
    loadChildren: () => import('@features/about/about.module').then((m) => m.AboutModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('@features/notifications/notifications.module').then((m) => m.NotificationsModule)
  },
  {
    path: 'digital-profile',
    loadChildren: () => import('@features/digital-profile/digital-profile.module').then((m) => m.DigitalProfileModule),
    data: { breadcrumb: 'Profile', permission: MePermissionTypes.COMPANY_VIEW }
  },
  {
    path: 'users',
    loadChildren: () => import('@features/users/users.module').then((m) => m.UsersModule),
    canActivate: [BasePermissionGuard],
    data: {
      permission: MePermissionTypes.COMPANY_USER_LIST
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {}
