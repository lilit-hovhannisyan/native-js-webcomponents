export enum VESSEL_TYPE {
  None = 0,
  Bulkcarrier = 1,
  Container = 2,
  RoRo = 3,
  ConRo = 4,
  MPP = 5
}
