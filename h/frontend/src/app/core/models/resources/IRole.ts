import { IWithSystemFlag } from './IWithSystemFlag';
import { IWithLabels } from './IWithLabels';
import { IResource } from './IResource';
import { IWithActive } from './IWithActive';
import { IWithLabel } from './IWithLabel';

export type RoleKey = number;
export type Permissions = { [zone: string]: number };

interface ISharedProps extends IWithLabels, IWithSystemFlag {
  description: string;
  remarks: string;
  permissions: Permissions;
}

export interface IRoleBase extends ISharedProps, IResource<RoleKey> { }

export interface IRoleCreate extends ISharedProps { }

export interface IRoleDTO extends IRoleBase { }

export interface IRole extends IRoleDTO, IWithLabel { }
