import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IPaymentPermission, IPaymentPermissionDTO, IPaymentPermissionCreate, PaymentPermissionKey } from '../../models/resources/IPaymentsPermission';

export interface IPaymentPermissionGroupsParams extends RepoParams<IPaymentPermission> { }

@Injectable({
  providedIn: 'root'
})
export class PaymentsPermissionRepositoryService extends AbstractRepository<
IPaymentPermission,
IPaymentPermissionDTO,
IPaymentPermissionCreate,
IPaymentPermissionGroupsParams,
PaymentPermissionKey
> {
  protected resourceType = 'PaymentsPermission';
  protected sortAsce = true;
  protected sortKey = 'id';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IPaymentPermissionDTO): IPaymentPermission => item;

  public getBaseUrl = () => '/api/payments/permissions';
}
