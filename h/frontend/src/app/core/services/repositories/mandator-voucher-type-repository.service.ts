import { MandatorVoucherTypeKey } from './../../models/resources/IMandatorVoucherType';
import { Injectable } from '@angular/core';
import { IMandatorVoucherTypeDTO, IMandatorVoucherType, IMandatorVoucherTypeCreate } from '../../models/resources/IMandatorVoucherType';
import { RepoParams, AbstractRepository } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class MandatorVoucherTypeRepositoryService extends AbstractRepository<
IMandatorVoucherType,
IMandatorVoucherTypeDTO,
IMandatorVoucherTypeCreate,
RepoParams<IMandatorVoucherType>,
MandatorVoucherTypeKey
> {
  protected resourceType = 'MandatorVoucherType';
  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/mandator-voucher-types/';
  }

  protected postProcess(item: IMandatorVoucherTypeDTO): IMandatorVoucherType { return item; }
}
