import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseWebService } from '../../core/services/base.web-service';

import { MARKETPLACE_BASE_API_URL, MATE_BASE_API_URL } from '@shared/constants';
import { UserProfileModel } from '@shared/models/user-profile/user-profile.model';
import { EntitiesUpdateI } from '@core-interfaces/entities-update.interface';

@Injectable()
export class UserProfileWebService {
  constructor(private baseWebService: BaseWebService) {}

  updateUser(entities: EntitiesUpdateI): Observable<UserProfileModel> {
    return this.baseWebService.putRequest<UserProfileModel, EntitiesUpdateI>(
      `${MARKETPLACE_BASE_API_URL}/users/profile`,
      entities,
      UserProfileModel
    );
  }

  changeUserPassword(data: { oldPassword: string; newPassword: string }): Observable<void> {
    return this.baseWebService.postRequest<void, { oldPassword: string; newPassword: string }>(`${MATE_BASE_API_URL}/auth/password`, data);
  }

  getUser(): Observable<UserProfileModel> {
    return this.baseWebService.getRequest<UserProfileModel>(`${MARKETPLACE_BASE_API_URL}/users/profile`, UserProfileModel);
  }
}
