import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Subject } from 'rxjs';

import { MePopupStatusChangeComponent } from './me-popup-status-change.component';
import { MePopupStatusChangeI } from './me-popup-status-change.interface';

@Injectable()
export class MePopupStatusChangeService {
  private _currentInstance: MePopupStatusChangeComponent | undefined;

  constructor(private dialog: MatDialog) {}

  openDialog(dataModel: MePopupStatusChangeI): Subject<{ eventName: string; payload?: string }> {
    const subjectToReturn = new Subject<{ eventName: string; payload?: string }>();
    const dialogRef = this.dialog.open(MePopupStatusChangeComponent, { data: dataModel, autoFocus: false });
    this._currentInstance = dialogRef.componentInstance;
    dialogRef.componentInstance.eventOccurs.subscribe((event: { eventName: string; payload?: string }) => {
      dialogRef.close();
      subjectToReturn.next(event);
    });
    return subjectToReturn;
  }

  refreshDialogData(dataModel: MePopupStatusChangeI): void {
    this._currentInstance?.refreshData(dataModel);
  }
}
