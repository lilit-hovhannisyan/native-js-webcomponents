import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MARKETPLACE_BASE_API_URL } from '@shared/constants';

@Component({
  selector: 'me-table-image-with-title',
  templateUrl: './table-image-with-title.component.html',
  styleUrls: ['./table-image-with-title.component.scss']
})
export class TableImageWithTitleComponent implements OnInit {
  baseUrl = MARKETPLACE_BASE_API_URL;

  @Input() data!: { url: string; title: string };
  @Output() tableDataChangeEvent = new EventEmitter<void>();

  constructor() {}

  ngOnInit(): void {}

  onClick() {
    this.tableDataChangeEvent.emit();
  }
}
