import { Component, OnInit, Input, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { IUser } from 'src/app/core/models/resources/IUser';
import { UserRepositoryService } from 'src/app/core/services/repositories/user-repository.service';
import { Subscription, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'nv-user-compact',
  templateUrl: './user-compact.component.html',
  styleUrls: ['./user-compact.component.scss']
})
export class UserCompactComponent implements OnInit, OnDestroy {
  @Input() public user?: IUser;
  @Input() public userId?: number;
  @Input() public imageShape;
  @Input() public navigateByClick?: boolean;
  @Input() public shape?: 'square' | 'circle';

  public _user: IUser;

  private destroy$: Subject<void> = new Subject();

  constructor(
    private userRepo: UserRepositoryService,
    private route: Router,
    private cdr: ChangeDetectorRef
  ) {
  }

  public ngOnInit(): void {
    if (this.user) {
      this._user = this.user;
      return;
    }
    if (!this.userId) { return; }
    this.userRepo.fetchOne(this.userId, {})
      .pipe(takeUntil(this.destroy$))
      .subscribe(user => {
      this._user = user;
      this.cdr.detectChanges();
    });
  }

  public navigateTo() {
    const usersUrl = url(sitemap.system.children.misc.children.users);
    this.navigateByClick && this.route.navigate([`${usersUrl}/${this.user.id}`]);
  }

  public ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
