import { Component, OnInit, OnDestroy, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { SubSink } from 'subsink';
import { TranslateService } from '@ngx-translate/core';

import { DefinitionsStoreService } from '@core-services/definitions-store.service';
import { GlobalService } from '@core-services/global.service';
import { TableViewService } from '@shared/me-components/me-table/table-view.service';
import { MeBasicAlertService } from '@shared/me-components/me-basic-alert/me-basic-alert.service';
import { BackOfficeService } from '../backoffice.web-service';

import {
  MeTableActionButtonPropertiesI,
  MeTableCellClickEventI,
  MeTableColumnI,
  MeTableColumnOptionsI,
  MeTableDataSourceI
} from '@shared/me-components/me-table';
import { ErrorResponseI } from '@core-interfaces/error-reponse.interface';
import { ArrayRequestI } from '@core-interfaces/array-request.interface';
import { EntityI } from '@core-interfaces/entity.interface';
import { DomainModel } from '@shared/models/domain/domain.model';
import { DigitalProfileModel } from '@shared/models/digital-profile/digital-profile.model';
import { MeAttributeTypes } from '@shared/me-attribute-types';
import { ME_DOMAIN_APPLICATION_COMPANY, ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER } from '@shared/constants';
import { MeSearchBarI } from '@shared/me-components/me-search-bar/me-search-bar.interface';
import { QuickSearchModel } from '@shared/models/search/quick-search.model';
import { BreadcrumbSupportService } from '@core-services/breadcrumb-support.service';

@Component({
  selector: 'me-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit, OnDestroy {
  private subs = new SubSink();
  domain: DomainModel = new DomainModel();
  tableItems: MeTableDataSourceI[] = [];
  tableColumns: MeTableColumnI[] = [];
  tableColumnOptions: MeTableColumnOptionsI[] = [];
  searchBarData!: MeSearchBarI;
  tableLoader = true;
  searchKeyword = '';

  _cellClickEventSubject!: Subscription;
  cellClickEventSubject = new Subject<MeTableCellClickEventI>();

  i18nStrings: { [key: string]: string } = {
    statusChange: '',
    statusChangeSuccess: '',
    successfullyUpdated: '',
    usersInGroupUpdated: '',
    searchFor: '',
    company: ''
  };

  @ViewChild('createButton', { read: TemplateRef }) createButton!: TemplateRef<Component>;

  constructor(
    private globalService: GlobalService,
    private backOfficeService: BackOfficeService,
    private definitionsStoreService: DefinitionsStoreService,
    private tableViewService: TableViewService,
    private basicAlertService: MeBasicAlertService,
    private translate: TranslateService,
    private breadcrumbSupportService: BreadcrumbSupportService,
    private _router: Router,
    private _activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.globalService.activateLoader();

    this.subs.sink = this.translate.get(Object.keys(this.i18nStrings)).subscribe((translations) => {
      Object.keys(this.i18nStrings).forEach((key) => {
        this.i18nStrings[key] = translations[key];
      });

      this.setSearchBarData();
    });

    this.subs.sink = this.definitionsStoreService.dataLoaded$.subscribe((dataLoaded) => {
      if (dataLoaded) {
        this.prepareTableColumns();
        this.getAllCompanies();
        this.globalService.deactivateLoader();
      }
    });

    this._cellClickEventSubject = this.cellClickEventSubject.subscribe(({ colIndex, data, rowOID }) => {
      switch (colIndex) {
        case 9:
          const { type, key } = data;

          if (type === 'edit') {
            this._router.navigate(['edit', rowOID], { relativeTo: this._activeRoute });
          } else if (type === 'view') {
            this._router.navigate(['view', rowOID], { relativeTo: this._activeRoute });
          } else if (type === 'selectedContextMenu') {
            this.activateOrDeactivateCompany(key, rowOID);
          }
          break;
      }
    });
  }

  ngAfterViewInit(): void {
    this.breadcrumbSupportService.breadcrumbActions = [
      {
        button: {
          id: '1',
          data: { color: 'primary', type: 'transparent' },
          text: this.i18nStrings.create,
          content: this.createButton
        },
        callBack: () => {
          this._router.navigate(['create'], { relativeTo: this._activeRoute });
        }
      }
    ];
  }

  setSearchBarData(): void {
    this.searchBarData = {
      filterEnabled: false,
      search: {
        placeholder: this.i18nStrings.searchFor,
        keyword: '',
        minCharacters: 1
      }
    };
  }

  activateOrDeactivateCompany(key: string, companyOID: string): void {
    switch (key) {
      case 'activate':
        const activateCompany = {
          entities: [
            {
              domainName: ME_DOMAIN_APPLICATION_COMPANY,
              attributes: {
                isActive: true
              }
            }
          ]
        };
        this.updateBoCompanyStatus(companyOID, activateCompany);
        break;
      case 'deactivate':
        const deactivateCompany = {
          entities: [
            {
              domainName: ME_DOMAIN_APPLICATION_COMPANY,
              attributes: {
                isActive: false
              }
            }
          ]
        };
        this.updateBoCompanyStatus(companyOID, deactivateCompany);
        break;
    }
  }

  updateBoCompanyStatus(companyOID: string, statusChange: ArrayRequestI<EntityI>): void {
    this.globalService.activateLoader();
    this.subs.sink = this.backOfficeService
      .updateBoCompany(companyOID, statusChange)
      .pipe(finalize(() => this.globalService.deactivateLoader()))
      .subscribe(
        (response: any) => {
          if (response) {
            this.getAllCompanies();
            this.basicAlertService.openBasicAlert({
              mode: 'success',
              title: this.i18nStrings.statusChanged,
              content: this.i18nStrings.successfullyUpdated
            });
          }
        },
        (error: ErrorResponseI) => {
          this.basicAlertService.openBasicAlert({
            mode: 'error',
            title: error.error.title,
            content: error.error.details
          });
        }
      );
  }

  getAllCompanies(): void {
    this.subs.sink = this.backOfficeService.getBoCompanies().subscribe((response) => {
      if (response && response.entities) {
        this.prepareCompaniesTableData(response.entities);
      }
    });
  }

  prepareTableColumns(): void {
    const customColumns: MeTableColumnI[] = [
      {
        field: 'status',
        headerName: 'Status'
      }
    ];
    const columnOptions: MeTableColumnOptionsI[] = [];

    this.domain = this.definitionsStoreService.getDomain(ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER);

    const { layouts, attributes } = this.domain;
    const columns = this.tableViewService.genereateColumns({
      layouts,
      attributes,
      customColumns
    });

    if (columns) {
      this.tableColumns = columns;
      this.tableColumnOptions = columnOptions;
    }
  }

  prepareCompaniesTableData(data: DigitalProfileModel[]): void {
    this.tableLoader = true;
    const cellComponents = this.tableViewService.genereateComponentsForCell(this.tableColumns, this.domain.attributes);
    const actionButtons: MeTableActionButtonPropertiesI[] = [
      {
        iconName: 'edit-2',
        type: 'edit'
      },
      {
        iconName: 'eye',
        type: 'view'
      },
      {
        contextMenuItems: [
          {
            isActiveAttrType: true,
            key: '',
            value: ''
          }
        ],
        type: 'selectedContextMenu'
      }
    ];
    const customComponents = data.map(({ oid, attributes }) => ({
      rowOID: oid,
      cells: [
        {
          fieldName: 'status',
          componentType: MeAttributeTypes.BOOLEAN,
          componentData: attributes.isActive
        }
      ]
    }));
    const dataSource = this.tableViewService.attechComponentDataAndRowOIDToCell({
      entities: data,
      cellComponents,
      actionButtonProperties: actionButtons,
      customComponents
    });

    this.tableItems = dataSource;
    this.tableLoader = false;
  }

  handleSearch(searchValue: string) {
    console.log(`searchValue`, searchValue);
    this.globalService.activateLoader();
    this.searchKeyword = searchValue;
    this.searchBarData = {
      ...this.searchBarData,
      search: {
        placeholder: this.i18nStrings.company,
        keyword: this.searchKeyword as string
      }
    };

    const searchFilter: QuickSearchModel = {
      domain: ME_DOMAIN_APPLICATION_COMPANY_SUPPLIER,
      criteriaQuick: searchValue.trim(),
      collectionOid: ''
    };

    this.subs.sink = this.backOfficeService.searchBoCompany(searchFilter).subscribe((response) => {
      this.prepareCompaniesTableData(response.entities);
      this.globalService.deactivateLoader();
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();

    if (this._cellClickEventSubject) {
      this._cellClickEventSubject.unsubscribe();
    }
  }
}
