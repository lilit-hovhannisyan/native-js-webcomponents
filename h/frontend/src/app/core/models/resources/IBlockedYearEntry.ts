import { UserKey } from './IUser';

export type IBlockedYearEntry = {
  year: number,
  period?: number,
  blockedAt?: string,
  blockedBy?: UserKey,
  id: number
};

