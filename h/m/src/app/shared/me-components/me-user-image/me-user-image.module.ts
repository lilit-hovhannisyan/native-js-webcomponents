import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IconsModule } from '@shared/modules/icons.module';

import { MeUserImageComponent } from './me-user-image.component';

@NgModule({
  declarations: [MeUserImageComponent],
  imports: [CommonModule, IconsModule],
  exports: [MeUserImageComponent]
})
export class MeUserImageModule {}
