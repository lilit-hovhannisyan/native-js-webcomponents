import {
  NvGridConfig,
  NvRowHeight,
} from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { IBankAccountRelation, bankAccountNoModifier } from '../../models/resources/IBankAccount';
import { Validators } from '../../classes/validators';
import { ValidatorFn } from '@angular/forms';
import { BankAccountRelationGridComponent } from 'src/app/shared/components/bank-account-relation-grid/bank-account-relation-grid.component';


export const getAccountingRelationsBankConfig = (): NvGridConfig => ({
  gridName: 'accountingRelations',
  hideAllFilters: true,
  showRowIndex: false,
  showPaging: false,
  showFooter: false,
  pinFirstRow: false,
  hideRefreshButton: true,
  rowHeight: NvRowHeight.midSize,
  expandableComponentConfig: {
    actOnDoubleClick: true,
    componentToPort: BankAccountRelationGridComponent
  },
  columns: [
    {
      key: 'id',
      visible: false
    },
    {
      key: 'name',
      title: wording.accounting.bankName,
      width: 150,
      isSortable: true,
      filter: {
        values: []
      }
    },
    {
      key: 'code',
      title: wording.accounting.bankCode,
      width: 120,
      isSortable: true,
    },
    {
      key: 'bic',
      title: wording.accounting.bic,
      width: 120,
      isSortable: true,
    },
    {
      key: 'zipCode',
      title: wording.general.zipCode,
      width: 100,
      isSortable: true,
    },
    {
      key: 'city',
      title: wording.general.city,
      width: 100,
      isSortable: true,
    },
    {
      key: 'street',
      title: wording.general.street,
      width: 120,
      isSortable: true,
    },
  ],
});

export const getAccountingRelationsAccountConfig = (
  validators: {
    bookingAccount: ValidatorFn[]
  },
  formats: {
    currency: (value: number, property: string) => any
    bookingAccount: (value: number, property: string) => any
  }
): NvGridConfig => ({
  gridName: 'accountingRelationsBankGrid',
  hideAllFilters: true,
  showRowIndex: false,
  showPaging: false,
  showFooter: false,
  pinFirstRow: false,
  hideToolbar: true,
  rowHeight: NvRowHeight.midSize,
  editForm: {
    disableEditColumns: true,
    allowCreateNewRow: false,
    allowJumpToNextCellIfInvalid: true,
    createFirstFormIfRowExist: true,
  },
  columns: [
    {
      // Represents bank account id
      key: 'id',
      visible: false
    },
    {
      key: 'mandatorId',
      visible: false
    },
    {
      key: 'bankId',
      visible: false
    },
    {
      // Represents accounting relation id, set to 0 for new relations
      key: 'relationId',
      visible: false
    },
    {
      key: 'accountNo',
      title: wording.accounting.bankAccount,
      width: 120,
      isSortable: true,
      editControl: {
        editable: false,
        disabled: true
      }
    },
    {
      key: 'iban',
      title: wording.accounting.iban,
      width: 120,
      isSortable: true,
      editControl: {
        editable: false,
        disabled: true
      }
    },
    {
      key: 'currencyId',
      title: wording.accounting.currency,
      width: 120,
      isSortable: true,
      editControl: {
        editable: false,
        disabled: true
      },
      customFormatFn: (row: IBankAccountRelation) => formats.currency(row.currencyId, 'isoCode')
    },
    {
      key: 'bookingAccountId',
      title: wording.accounting.bookingAccount,
      width: 150,
      isSortable: true,
      editControl: {
        editable: true,
        validation: {
          validators: [...validators.bookingAccount],
        },
      },
      customFormatFn: (row: IBankAccountRelation) => formats.bookingAccount(row.bookingAccountId, 'no')
    },
    {
      key: 'bankAccountNo',
      title: wording.accounting.importBankAccount,
      width: 120,
      isSortable: true,
      editControl: {
        editable: true,
        validation: {
          validators: [
            Validators.maxLength(13),
          ],
        },
      },
      customFormatFn: (row: IBankAccountRelation) => bankAccountNoModifier(row.bankAccountNo)
    },
  ],
});
