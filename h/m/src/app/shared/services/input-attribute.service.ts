import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators, AbstractControl, ValidatorFn } from '@angular/forms';

import { plainToClass } from 'class-transformer';

import { DefinitionsStoreService } from '@core-services/definitions-store.service';

import {
  AttributeModelTypes,
  DefaultAttributeModel,
  DMBooleanAttributeModel,
  DMChoiceAttributeModel,
  DMCompositeAttributeModel,
  DMDateTimeAttributeModel,
  DMDecimalAttributeModel,
  DMFloatAttributeModel,
  DMIntegerAttributeModel,
  DMLongAttributeModel,
  DMMOAListAttributeModel,
  DMObjectRefAttributeModel,
  DMObjectRefListAttributeModel,
  DMStringAttributeModel,
  DMTextAttributeModel,
  DMUrlAttributeModel
} from '@shared-models/attribute/attribute.model';
import {
  ConstraintModel,
  DateTimeRangeConstraintModel,
  DecimalRangeConstraintModel,
  FloatRangeConstraintModel,
  ImmutableConstraintModel,
  IntegerRangeConstraintModel,
  MultiValuedConstraintModel,
  ObjectRefConstraintModel,
  RequiredConstraintModel,
  RequiredConstraintState,
  SingleValuedConstraintModel,
  StringLengthConstraintModel
} from '@shared-models/attribute/attribute.constraint.model';
import { EnumModel } from '@shared-models/enum/enum.model';
import { EnumValueModel } from '@shared-models/enum/enum-value.model';
import { KeyValueI } from '@core-interfaces/key-value.interface';
import { LayoutModel } from '@shared-models/layout/layout.model';
import { LayoutGroupModel } from '@shared-models/layout/layout-group.model';
import { LayoutGroupAttributeModel } from '@shared-models/layout/layout-group-attribute.model';
import { LayoutRowModel } from '@shared-models/layout/layout-row.model';
import { MeAttributeTypes } from '@shared/me-attribute-types';
import { MeAttributeConstraintTypes } from '@shared/me-attribute-constraint-types';
import { MARKETPLACE_BASE_API_URL, ME_URL_REGEX } from '@shared/constants';
import { MeAttributeItem } from '@shared/me-components/me-attribute/me-attribute-item';
import { MeTextInputAttribute } from '@shared/models/attribute/me-text-input-attribute.model';
import { MeTextInputComponent } from '@shared/me-components/me-attribute/me-text-input/me-text-input.component';
import { MeMultiSelectInputAttribute } from '@shared/models/attribute/me-multi-select-input-attribute.model';
import { MeMultiSelectInputComponent } from '@shared/me-components/me-attribute/me-multi-select-input/me-multi-select-input.component';
import { MeSelectInputAttribute } from '@shared/models/attribute/me-select-input-attribute.model';
import { MeSelectInputComponent } from '@shared/me-components/me-attribute/me-select-input/me-select-input.component';
import { MeBooleanInputComponent } from '@shared/me-components/me-attribute/me-boolean-input/me-boolean-input.component';
import { MeBooleanInputAttribute } from '@shared/models/attribute/me-boolean-input-attribute.model';
import { MeTextAreaInputAttribute } from '@shared/models/attribute/me-text-area-input-attribute.model';
import { MeTextAreaInputComponent } from '@shared/me-components/me-attribute/me-text-area-input/me-text-area-input.component';
import { MeNumberInputAttribute } from '@shared/models/attribute/me-number-input-attribute.model';
import { MeNumberInputComponent } from '@shared/me-components/me-attribute/me-number-input/me-number-input.component';
import { MeCompositeInputAttribute } from '@shared/models/attribute/me-composite-input-attribute.model';
import { MeCompositeInputComponent } from '@shared/me-components/me-attribute/me-composite-input/me-composite-input.component';
import { KeyValue } from '@angular/common';
import { MeInputErrorType, MeInputErrorTypeEnum } from '@shared/me-components/me-attribute/me-input-error/me-input-error.interface';
import { MeDateTimeInputAttribute } from '@shared/models/attribute/me-date-time-input-attribute.model';
import { MeDateTimeInputComponent } from '@shared/me-components/me-attribute/me-date-time-input/me-date-time-input.component';
import { forkJoin, Observable, Subject, Subscription } from 'rxjs';
import { AttributeUniqueModel } from '@shared/models/attribute-unique/attribute-unique.model';
import { MeAttributeUniqueTypes } from '@shared/me-attribute-unique-types';
import { BaseWebService } from '@core-services/base.web-service';
import { MeTranslationService } from '@core-services/me-translation.service';

@Injectable()
export class InputAttributeService {
  constructor(
    private definitionsStoreService: DefinitionsStoreService,
    private translate: MeTranslationService,
    private baseWebService: BaseWebService
  ) {}

  /* FORM ATTRIBUTE TRANSFORMATION */

  /**
   * Transforms attributes array objects to some inheritance AttributeModel class
   *
   * @param attributes Array of attributes that are going to be transformed
   *
   * @returns Array of attributes that are based on inheritance of AttributeModel class
   */
  transformAttributesArray(attributes: AttributeModelTypes[], ignoreImmutable: boolean = false): AttributeModelTypes[] {
    return attributes.map((attribute) => {
      let transformed!: AttributeModelTypes;
      switch (attribute.dataTypeClass) {
        case MeAttributeTypes.BOOLEAN:
          transformed = plainToClass(DMBooleanAttributeModel, attribute);
          break;

        case MeAttributeTypes.CHOICE:
          transformed = plainToClass(DMChoiceAttributeModel, attribute);
          break;

        case MeAttributeTypes.COMPOSITE:
          transformed = plainToClass(DMCompositeAttributeModel, attribute);
          break;

        case MeAttributeTypes.DATE_TIME:
          transformed = plainToClass(DMDateTimeAttributeModel, attribute);
          break;

        case MeAttributeTypes.DECIMAL:
          transformed = plainToClass(DMDecimalAttributeModel, attribute);
          break;

        case MeAttributeTypes.FLOAT:
          transformed = plainToClass(DMFloatAttributeModel, attribute);
          break;

        case MeAttributeTypes.INTEGER:
          transformed = plainToClass(DMIntegerAttributeModel, attribute);
          break;

        case MeAttributeTypes.LONG:
          transformed = plainToClass(DMLongAttributeModel, attribute);
          break;

        case MeAttributeTypes.MOA_LIST:
          transformed = plainToClass(DMMOAListAttributeModel, attribute);
          break;

        case MeAttributeTypes.OBJECT_REF:
          transformed = plainToClass(DMObjectRefAttributeModel, attribute);
          break;

        case MeAttributeTypes.OBJECT_REF_LIST:
          transformed = plainToClass(DMObjectRefListAttributeModel, attribute);
          break;

        case MeAttributeTypes.STRING:
          transformed = plainToClass(DMStringAttributeModel, attribute);
          break;

        case MeAttributeTypes.TEXT:
          transformed = plainToClass(DMTextAttributeModel, attribute);
          break;

        case MeAttributeTypes.URL:
          transformed = plainToClass(DMUrlAttributeModel, attribute);
          break;

        default:
          transformed = plainToClass(DefaultAttributeModel, attribute);
      }

      // if constraints exist transform array of constraint objects to some inheritance of Constraint model class
      if (transformed.constraints && transformed.constraints.length > 0) {
        transformed.constraints = this.transformConstraints(transformed.constraints, ignoreImmutable);
      }

      return transformed;
    });
  }

  /**
   * Transforms constraints array objects to some inheritance of ConstraintModel class
   *
   * @param constraints Array of constraints that are going to be transformed
   *
   * @returns Array of constraints that are based on inheritance of ConstraintModel class
   */
  private transformConstraints(constraints: ConstraintModel[], ignoreImmutable: boolean = false): ConstraintModel[] {
    return constraints.map((constraint) => {
      let transformed!: ConstraintModel;
      switch (constraint.ruleClass) {
        case MeAttributeConstraintTypes.DATE_TIME_RANGE:
          transformed = plainToClass(DateTimeRangeConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.DECIMAL_RANGE:
          transformed = plainToClass(DecimalRangeConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.FLOAT_RANGE:
          transformed = plainToClass(FloatRangeConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.IMMUTABLE:
          if (!ignoreImmutable) {
            transformed = plainToClass(ImmutableConstraintModel, constraint);
          }
          break;

        case MeAttributeConstraintTypes.INTEGER_RANGE:
          transformed = plainToClass(IntegerRangeConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.MULTI_VALUED:
          transformed = plainToClass(MultiValuedConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.OBJECT_REF:
          transformed = plainToClass(ObjectRefConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.REQUIRED:
          transformed = plainToClass(RequiredConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.SINGLE_VALUED:
          transformed = plainToClass(SingleValuedConstraintModel, constraint);
          break;

        case MeAttributeConstraintTypes.STRING_LENGTH:
          transformed = plainToClass(StringLengthConstraintModel, constraint);
          break;

        default:
          transformed = plainToClass(ConstraintModel, constraint);
      }

      return transformed;
    });
  }

  /* Generating Form Group / Form Controls */

  /**
   * Creates Form Group with Form Controls (array of AttributeModel objects)
   *
   * @param attributes Array of AttributeModel attributes
   * @param layoutType layout mode which tell us what validators should be added (SEARCH or VIEW)
   *
   * @returns Created form group
   */
  createFormGroup(attributes: AttributeModelTypes[], appliedState?: RequiredConstraintState): FormGroup {
    const formGroup: KeyValueI<FormControl> = {};

    // create Form Control for each attribute and add validators if applicable
    attributes.forEach((attribute) => {
      formGroup[attribute.internalName] = new FormControl(attribute.value, this.applyConstraints(attribute, appliedState));
    });

    return new FormGroup(formGroup);
  }

  /**
   * Adds Validators to Form Control, also adds options from enum list
   *
   * @param attribute Attribute that has information about what validators (constraints) should be applied
   * @param layoutType  layout mode which tell us what validators should be added (SEARCH or VIEW)
   *
   * @return Array of Validators
   */
  applyConstraints(attribute: AttributeModelTypes, appliedState?: RequiredConstraintState): ValidatorFn[] {
    const validatorsArray: ValidatorFn[] = [];

    attribute.constraints.forEach((constraint) => {
      if (constraint instanceof DateTimeRangeConstraintModel) {
        // console.log("TODO: DateTimeRangeConstraintModel");
      }

      if (constraint instanceof DecimalRangeConstraintModel && attribute instanceof DMDecimalAttributeModel) {
        validatorsArray.push(Validators.min(constraint.data.min));
        validatorsArray.push(Validators.max(constraint.data.max));

        attribute.minValue = constraint.data.min;
        attribute.maxValue = constraint.data.max;
      }

      if (constraint instanceof FloatRangeConstraintModel && attribute instanceof DMFloatAttributeModel) {
        validatorsArray.push(Validators.min(constraint.data.min));
        validatorsArray.push(Validators.max(constraint.data.max));

        attribute.minValue = constraint.data.min;
        attribute.maxValue = constraint.data.max;
      }

      if (constraint instanceof ImmutableConstraintModel) {
        attribute.disabled = true;
      }

      if (
        constraint instanceof IntegerRangeConstraintModel &&
        (attribute instanceof DMIntegerAttributeModel || attribute instanceof DMLongAttributeModel)
      ) {
        validatorsArray.push(Validators.min(constraint.data.min));
        validatorsArray.push(Validators.max(constraint.data.max));

        attribute.minValue = constraint.data.min;
        attribute.maxValue = constraint.data.max;
      }

      if (
        constraint instanceof MultiValuedConstraintModel &&
        (attribute instanceof DMMOAListAttributeModel || attribute instanceof DMCompositeAttributeModel)
      ) {
        attribute.values = this.getEnumValues(constraint.data.enum);
      }

      if (constraint instanceof ObjectRefConstraintModel) {
        // console.log("TODO: ObjectRefConstraintModel");
      }

      if (constraint instanceof RequiredConstraintModel) {
        attribute.optRequired = true;
        if (
          appliedState &&
          constraint.data &&
          (constraint.data.states.length === 0 || constraint.data.states.findIndex((state) => state === appliedState) > -1)
        ) {
          validatorsArray.push(Validators.required);
          attribute.required = true;
        } else if (!appliedState) {
          validatorsArray.push(Validators.required);
          attribute.required = true;
        }
      }

      if (constraint instanceof SingleValuedConstraintModel && attribute instanceof DMChoiceAttributeModel) {
        attribute.values = this.getEnumValues(constraint.data.enum);
      }

      if (
        constraint instanceof StringLengthConstraintModel &&
        (attribute instanceof DMStringAttributeModel ||
          attribute instanceof DMUrlAttributeModel ||
          attribute instanceof DMTextAttributeModel)
      ) {
        validatorsArray.push(Validators.minLength(constraint.data.from));
        validatorsArray.push(Validators.maxLength(constraint.data.to));

        attribute.minLength = constraint.data.from;
        attribute.maxLength = constraint.data.to;
      }
    });

    if (attribute instanceof DMUrlAttributeModel) {
      validatorsArray.push(Validators.pattern(ME_URL_REGEX));
    }

    return validatorsArray;
  }

  /**
   * Reads enum objects from store
   * and returns list of Enum values (dropdown options) for desired enum
   *
   * @param enumName enum name for which we want to return values
   *
   * @returns list of Enum values (dropdown options)
   */
  private getEnumValues(enumName: string): EnumValueModel[] {
    const enums: EnumModel[] = this.definitionsStoreService.enums;
    const enumObject: EnumModel | undefined = enums.find((enumObj) => enumObj.name === enumName);

    return enumObject ? enumObject.values : [];
  }

  /**
   * Returns layout groups based on layout type.
   *
   * @param layoutsArray Array of all layouts
   * @param layoutTag tag which tell us which type of layout it is
   *
   * @returns Layout groups
   */
  getLayoutGroups(layoutsArray: LayoutModel[], layoutTag: string): LayoutGroupModel[] {
    const foundLayout: LayoutModel | undefined = layoutsArray.find((layout) => layout.tag === layoutTag);
    return foundLayout ? foundLayout.groups : [];
  }

  /**
   * Returns modified array of LayoutGroupModel class
   * Base values stay the same, only thing that is added is row objects
   * Frontend sorts attributes by row/column and generates those row objects
   * @param layoutGroups layout groups that we receive from backend
   *
   * @returns Modified LayoutGroupModel object that contains rows
   */
  generateLayoutRows(layoutGroups: LayoutGroupModel[]): LayoutGroupModel[] {
    return layoutGroups.map((layoutGroup) => {
      // sort attributes by row and colum properties
      const sortedLayoutGroupAttributes: LayoutGroupAttributeModel[] = layoutGroup.attributes.sort((a, b) => {
        return a.row - b.row || a.column - b.column;
      });

      // get how many unique row indexes are in attributes array
      const uniqueRows = [...new Set(layoutGroup.attributes.map((item) => item.row))];

      // generate new row with attributes
      const rowArray: LayoutRowModel[] = uniqueRows.map((row) => {
        const newRow: LayoutRowModel = new LayoutRowModel();
        newRow.rowIndex = row;

        // go through group attributes and place them in correct row
        newRow.attributes = sortedLayoutGroupAttributes.filter((attribute) => {
          return attribute.row === row;
        });

        newRow.attributes = newRow.attributes.map((attribute) => {
          if (attribute.width === '0%') {
            attribute.width = `${Math.floor(100 / newRow.attributes.length)}%`;
          }
          return attribute;
        });

        return newRow;
      });

      return { ...layoutGroup, rows: rowArray };
    });
  }

  /**
   * Returns attribute object of AttributeModel subclass
   *
   * @param attributes Array of all attributes
   * @param attributeObj Attribute object from layout group
   *
   * @returns Attribut Model object
   */
  getAttributeFromArray(attributes: AttributeModelTypes[], attributeObj: LayoutGroupAttributeModel): AttributeModelTypes | undefined {
    return attributes.find((attribute) => attribute.internalName === attributeObj.internalName);
  }

  /* ENTITY CREATE / UPDATE / SEARCH */

  /**
   * Returns attribute values that have been changed via UI.
   *
   * @param form Attribute form that needs to be filtered.
   *
   * @return Object with values that need to be updated via PUT service.
   */
  // tslint:disable-next-line:no-any
  getDirtyValues(form: FormGroup): KeyValueI<any> {
    // tslint:disable-next-line:no-any
    const updatedValues: KeyValueI<any> = {};

    for (const field in form.controls) {
      if (form.controls.hasOwnProperty(field)) {
        const control = form.get(field);
        if (control && control.dirty) {
          updatedValues[field] = control.value;
        }
      }
    }

    return updatedValues;
  }

  /**
   * Creates Entity Object that is going to be sent as payload with POST/PUT requests.
   *
   * @param domainName Domain name of Entity that needs to be created/updated.
   * @param attributes List of Entity attribute values.
   *
   * @return Created Entity object.
   */
  // tslint:disable-next-line:no-any
  createEntityObject(domainName: string, attributes: KeyValueI<any>): { domainName: string; attributes: KeyValueI<any> } {
    return {
      domainName,
      attributes
    };
  }

  /** Go through AttributeModel object array and adds value if it exists.
   *
   * @param attributes Array of AttributeModel objects
   * @param savedEntity Entity that contains saved attribute values
   *
   * @returns Attributes that are now populated with values
   */
  // tslint:disable-next-line:no-any
  applyUpdateFormValues(attributes: AttributeModelTypes[], savedEntity: { attributes: KeyValueI<any> }): AttributeModelTypes[] {
    if (savedEntity.attributes) {
      attributes.forEach((attribute) => {
        const currentValue = savedEntity.attributes[attribute.internalName];
        if (currentValue) {
          attribute.value = currentValue;
        }
      });
    }

    return attributes;
  }

  /**
   * Get number from OID.
   *
   * @param oid value of full OID.
   *
   * @return number from OID.
   */
  getNumberFromOID(oid: string): string {
    return oid.split(':').slice(-1)[0];
  }

  createAttributeItemMap(
    attributes: AttributeModelTypes[],
    formGroup: FormGroup,
    appliedState?: RequiredConstraintState
  ): Map<string, MeAttributeItem> {
    const meAttributeItemMap: Map<string, MeAttributeItem> = new Map();

    attributes.forEach((attribute) => {
      switch (attribute.dataTypeClass) {
        case MeAttributeTypes.BOOLEAN:
          const booleanAttribute: MeBooleanInputAttribute = new MeBooleanInputAttribute();
          booleanAttribute.data = attribute as DMBooleanAttributeModel;
          booleanAttribute.form = formGroup;
          const booleanAttributeItem: MeAttributeItem = new MeAttributeItem(MeBooleanInputComponent, booleanAttribute);
          meAttributeItemMap.set(attribute.internalName, booleanAttributeItem);
          break;

        case MeAttributeTypes.CHOICE:
          const choiceAttribute: MeSelectInputAttribute = new MeSelectInputAttribute();
          choiceAttribute.data = attribute as DMChoiceAttributeModel;
          choiceAttribute.form = formGroup;
          const choiceAttributeItem: MeAttributeItem = new MeAttributeItem(MeSelectInputComponent, choiceAttribute);
          meAttributeItemMap.set(attribute.internalName, choiceAttributeItem);
          break;

        case MeAttributeTypes.COMPOSITE:
          const compositeAttribute: MeCompositeInputAttribute = new MeCompositeInputAttribute();
          compositeAttribute.data = attribute as DMCompositeAttributeModel;
          compositeAttribute.form = formGroup;
          const compositeAttributeItem: MeAttributeItem = new MeAttributeItem(MeCompositeInputComponent, compositeAttribute);
          meAttributeItemMap.set(attribute.internalName, compositeAttributeItem);
          break;

        case MeAttributeTypes.DATE_TIME:
          const dateTimeAttribute: MeDateTimeInputAttribute = new MeDateTimeInputAttribute();
          dateTimeAttribute.data = attribute as DMDateTimeAttributeModel;
          dateTimeAttribute.form = formGroup;
          dateTimeAttribute.inputErrors = {
            formControl: formGroup.get(attribute.internalName) as AbstractControl,
            errors: this.applyConstraintsErrors(attribute, appliedState)
          };
          const dateTimeAttributeItem: MeAttributeItem = new MeAttributeItem(MeDateTimeInputComponent, dateTimeAttribute);
          meAttributeItemMap.set(attribute.internalName, dateTimeAttributeItem);
          break;

        case MeAttributeTypes.DECIMAL:
          const decimalAttribute: MeNumberInputAttribute = new MeNumberInputAttribute();
          decimalAttribute.data = attribute as DMDecimalAttributeModel;
          decimalAttribute.form = formGroup;
          decimalAttribute.inputErrors = {
            formControl: formGroup.get(attribute.internalName) as AbstractControl,
            errors: this.applyConstraintsErrors(attribute, appliedState)
          };
          const decimalAttributeItem: MeAttributeItem = new MeAttributeItem(MeNumberInputComponent, decimalAttribute);
          meAttributeItemMap.set(attribute.internalName, decimalAttributeItem);
          break;

        case MeAttributeTypes.FLOAT:
          const floatAttribute: MeNumberInputAttribute = new MeNumberInputAttribute();
          floatAttribute.data = attribute as DMFloatAttributeModel;
          floatAttribute.form = formGroup;
          floatAttribute.inputErrors = {
            formControl: formGroup.get(attribute.internalName) as AbstractControl,
            errors: this.applyConstraintsErrors(attribute, appliedState)
          };
          const floatAttributeItem: MeAttributeItem = new MeAttributeItem(MeNumberInputComponent, floatAttribute);
          meAttributeItemMap.set(attribute.internalName, floatAttributeItem);
          break;

        case MeAttributeTypes.INTEGER:
          const integerAttribute: MeNumberInputAttribute = new MeNumberInputAttribute();
          integerAttribute.data = attribute as DMIntegerAttributeModel;
          integerAttribute.form = formGroup;
          integerAttribute.inputErrors = {
            formControl: formGroup.get(attribute.internalName) as AbstractControl,
            errors: this.applyConstraintsErrors(attribute, appliedState)
          };
          const integerAttributeItem: MeAttributeItem = new MeAttributeItem(MeNumberInputComponent, integerAttribute);
          meAttributeItemMap.set(attribute.internalName, integerAttributeItem);
          break;

        case MeAttributeTypes.LONG:
          const longAttribute: MeNumberInputAttribute = new MeNumberInputAttribute();
          longAttribute.data = attribute as DMLongAttributeModel;
          longAttribute.form = formGroup;
          longAttribute.inputErrors = {
            formControl: formGroup.get(attribute.internalName) as AbstractControl,
            errors: this.applyConstraintsErrors(attribute, appliedState)
          };
          const longAttributeItem: MeAttributeItem = new MeAttributeItem(MeNumberInputComponent, longAttribute);
          meAttributeItemMap.set(attribute.internalName, longAttributeItem);
          break;

        case MeAttributeTypes.MOA_LIST:
          const moaListAttribute: MeMultiSelectInputAttribute = new MeMultiSelectInputAttribute();
          moaListAttribute.data = attribute as DMMOAListAttributeModel;
          moaListAttribute.form = formGroup;
          const moaListAttributeItem: MeAttributeItem = new MeAttributeItem(MeMultiSelectInputComponent, moaListAttribute);
          meAttributeItemMap.set(attribute.internalName, moaListAttributeItem);
          break;

        // case MeAttributeTypes.OBJECT_REF:
        //   attributes[index] = plainToClass(DMObjectRefAttributeModel, attribute);
        //   break;

        // case MeAttributeTypes.OBJECT_REF_LIST:
        //   attributes[index] = plainToClass(DMObjectRefListAttributeModel, attribute);
        //   break;

        case MeAttributeTypes.STRING:
          const textAttribute: MeTextInputAttribute = new MeTextInputAttribute();
          textAttribute.data = attribute as DMStringAttributeModel;
          textAttribute.form = formGroup;
          textAttribute.inputErrors = {
            formControl: formGroup.get(attribute.internalName) as AbstractControl,
            errors: this.applyConstraintsErrors(attribute, appliedState)
          };
          const textAttributeItem: MeAttributeItem = new MeAttributeItem(MeTextInputComponent, textAttribute);
          meAttributeItemMap.set(attribute.internalName, textAttributeItem);
          break;

        case MeAttributeTypes.TEXT:
          const textAreaAttribute: MeTextAreaInputAttribute = new MeTextAreaInputAttribute();
          textAreaAttribute.data = attribute as DMTextAttributeModel;
          textAreaAttribute.form = formGroup;
          textAreaAttribute.inputErrors = {
            formControl: formGroup.get(attribute.internalName) as AbstractControl,
            errors: this.applyConstraintsErrors(attribute, appliedState)
          };
          const textAreaAttributeItem: MeAttributeItem = new MeAttributeItem(MeTextAreaInputComponent, textAreaAttribute);
          meAttributeItemMap.set(attribute.internalName, textAreaAttributeItem);
          break;

        case MeAttributeTypes.URL:
          const urlAttribute: MeTextInputAttribute = new MeTextInputAttribute();
          urlAttribute.data = attribute as DMUrlAttributeModel;
          urlAttribute.form = formGroup;
          urlAttribute.inputErrors = {
            formControl: formGroup.get(attribute.internalName) as AbstractControl,
            errors: this.applyConstraintsErrors(attribute, appliedState)
          };
          const urlAttributeItem: MeAttributeItem = new MeAttributeItem(MeTextInputComponent, urlAttribute);
          meAttributeItemMap.set(attribute.internalName, urlAttributeItem);
          break;
      }
    });

    return meAttributeItemMap;
  }

  applyConstraintsErrors(attribute: AttributeModelTypes, appliedState?: RequiredConstraintState): KeyValue<MeInputErrorType, string>[] {
    const errorsArray: KeyValue<MeInputErrorType, string>[] = [];
    if (attribute.unique) {
      errorsArray.push({
        key: MeInputErrorTypeEnum.unique,
        value: this.translate.translate('formErrors.attUnique', { attribute: attribute.displayName })
      });
    }
    attribute.constraints.forEach((constraint) => {
      if (constraint instanceof DateTimeRangeConstraintModel) {
        // TODO date time range error
      }

      if (
        (constraint instanceof DecimalRangeConstraintModel && attribute instanceof DMDecimalAttributeModel) ||
        (constraint instanceof FloatRangeConstraintModel && attribute instanceof DMFloatAttributeModel) ||
        (constraint instanceof IntegerRangeConstraintModel &&
          (attribute instanceof DMIntegerAttributeModel || attribute instanceof DMLongAttributeModel))
      ) {
        errorsArray.push({
          key: MeInputErrorTypeEnum.max,
          value: this.translate.translate('formErrors.attMaxValue', {
            attribute: attribute.displayName,
            maxValue: constraint.data.max.toString()
          })
        });
        errorsArray.push({
          key: MeInputErrorTypeEnum.min,
          value: this.translate.translate('formErrors.attMinValue', {
            attribute: attribute.displayName,
            minValue: constraint.data.min.toString()
          })
        });
      }

      if (
        constraint instanceof RequiredConstraintModel &&
        appliedState &&
        constraint.data &&
        (constraint.data.states.length === 0 || constraint.data.states.findIndex((state) => state === appliedState) > -1)
      ) {
        errorsArray.push({
          key: MeInputErrorTypeEnum.required,
          value: this.translate.translate('formErrors.attRequired', { attribute: attribute.displayName })
        });
      } else if (constraint instanceof RequiredConstraintModel && !appliedState) {
        errorsArray.push({
          key: MeInputErrorTypeEnum.required,
          value: this.translate.translate('formErrors.attRequired', { attribute: attribute.displayName })
        });
      }

      if (
        constraint instanceof StringLengthConstraintModel &&
        (attribute instanceof DMStringAttributeModel ||
          attribute instanceof DMUrlAttributeModel ||
          attribute instanceof DMTextAttributeModel)
      ) {
        errorsArray.push({
          key: MeInputErrorTypeEnum.maxlength,
          value: this.translate.translate('formErrors.attMaxLength', {
            attribute: attribute.displayName,
            maxLength: constraint.data.to.toString()
          })
        });
        errorsArray.push({
          key: MeInputErrorTypeEnum.minlength,
          value: this.translate.translate('formErrors.attMinLength', {
            attribute: attribute.displayName,
            maxLength: constraint.data.from.toString()
          })
        });
      }
    });

    if (attribute instanceof DMUrlAttributeModel) {
      errorsArray.push({
        key: MeInputErrorTypeEnum.pattern,
        value: this.translate.translate('formErrors.attPattern', {
          attribute: attribute.displayName
        })
      });
    }

    return errorsArray;
  }
  /**
   * Returns attributes present in provided layout groups.
   *
   * @param attributes Array of all attributes
   * @param layoutGroups Array of layout groups
   *
   * @returns Attributes
   */
  getAttributesByLayoutGroups(attributes: AttributeModelTypes[], layoutGroups: LayoutGroupModel[]): AttributeModelTypes[] {
    const attributeMap: Map<string, AttributeModelTypes> = new Map();
    attributes.forEach((attribute) => {
      attributeMap.set(attribute.internalName, attribute);
    });
    const layoutAttributes: AttributeModelTypes[] = [];
    layoutGroups.forEach((group) => {
      group.attributes.forEach((attr) => {
        const tempAttr: AttributeModelTypes | undefined = attributeMap.get(attr.internalName);
        if (tempAttr) {
          layoutAttributes.push(tempAttr);
        }
      });
    });
    return layoutAttributes;
  }
  /**
   * Returns if attribute is required or not.
   *
   * @param attributes Array of all attributes
   * @param internalName String name of the attribute
   * @param appliedState Optional string param of the state of object
   *
   * @returns Boolean
   */
  isAttributeRequired(attributes: AttributeModelTypes[], internalName: string, appliedState?: RequiredConstraintState): boolean {
    for (const attribute of attributes) {
      if (attribute.internalName === internalName) {
        const attrIndex: number = attribute.constraints.findIndex((constraint) => constraint instanceof RequiredConstraintModel);
        const reqConstraint: RequiredConstraintModel = attribute.constraints[attrIndex] as RequiredConstraintModel;
        const hasReqAttr: boolean = attrIndex > -1;
        if (hasReqAttr) {
          if (!appliedState) {
            return true;
          }

          if (reqConstraint.data?.states.length === 0 || reqConstraint.data?.states.find((state) => state === appliedState)) {
            return true;
          }
        }

        return false;
      }
    }
    return false;
  }

  checkUniqueAttributes(oid: string, attributes: AttributeModelTypes[], domainName: string, formGroup: FormGroup): Subject<boolean> {
    const responseSubject: Subject<boolean> = new Subject();
    const attributeNames: string[] = [];
    const observableArray: Observable<AttributeUniqueModel>[] = [];

    attributes.forEach((attr) => {
      if (attr.unique) {
        const formControl: AbstractControl | null = formGroup.get(attr.internalName);
        if (formControl) {
          attributeNames.push(attr.internalName);
          observableArray.push(this._isUniqueAttributeValue(domainName, attr.internalName, formControl.value, oid));
        }
      }
    });

    if (observableArray.length) {
      const forkJoinSubs: Subscription = forkJoin(observableArray).subscribe((res) => {
        forkJoinSubs.unsubscribe();
        let unique: boolean = true;
        res.forEach((check, index) => {
          if (check.isUnique === MeAttributeUniqueTypes.NOT_UNIQUE) {
            const formControl: AbstractControl | null = formGroup.get(attributeNames[index]);
            if (formControl) {
              formControl.setErrors({ unique: true });
              unique = false;
            }
          }
        });

        if (unique) {
          responseSubject.next(unique);
        } else {
          responseSubject.error(unique);
        }
      });
    } else {
      setTimeout(() => {
        responseSubject.next(true);
      });
    }

    return responseSubject;
  }

  private _isUniqueAttributeValue(domainName: string, internalName: string, value: string, oid?: string): Observable<AttributeUniqueModel> {
    let params: string = `?domainName=${domainName}&internalName=${internalName}&value=${value}`;
    if (oid) {
      params = `${params}&oid=${oid}`;
    }
    return this.baseWebService.getRequest<AttributeUniqueModel>(
      `${MARKETPLACE_BASE_API_URL}/instances/unique-attr-check${encodeURI(params)}`,
      AttributeUniqueModel
    );
  }
}
