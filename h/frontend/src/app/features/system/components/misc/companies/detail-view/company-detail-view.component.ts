import { Component, OnInit, OnDestroy } from '@angular/core';
import { sitemap, SitemapNode } from 'src/app/core/constants/sitemap/sitemap';
import { ActivatedRoute, Router } from '@angular/router';
import { ICompany, CompanyKey } from 'src/app/core/models/resources/ICompany';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';
import { ReplaySubject, of, Observable, combineLatest, BehaviorSubject } from 'rxjs';
import { Language } from 'src/app/core/models/language';
import { url } from 'src/app/core/constants/sitemap/sitemap-entry';
import { wording } from 'src/app/core/constants/wording/wording';
import { CompaniesHelperService } from './companies.helper.service';
import { takeUntil, tap, map, switchMap, skipWhile } from 'rxjs/operators';
import { ContributionMainService } from 'src/app/core/services/mainServices/company-contributions-main.service';
import { legalEntityExclamationTooltipContributions, getContributionWordingKey } from 'src/app/core/models/enums/company-legal-form';
import { IWording } from 'src/app/core/models/resources/IWording';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';
import { CompanyLegalEntityRepositoryService } from 'src/app/core/services/repositories/company-legal-entity-repository.service';
import * as moment from 'moment';

@Component({
  selector: 'nv-company-detail-view',
  templateUrl: './company-detail-view.component.html',
  styleUrls: ['./company-detail-view.component.scss'],
})
export class CompanyDetailViewComponent implements OnInit, OnDestroy {
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public title: string;
  public company: ICompany;
  public lang: Language;
  public sitemap = sitemap;
  public wording = wording;
  private legalNode: SitemapNode = sitemap.system.children.misc.children.companies.children.id.children.legal;
  private showContributionWarning$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private legalTabDefaultWarning: IWording;
  public isCompanyResolved: boolean;

  public get isCreationMode() {
    return this.route.snapshot.params.id === 'new';
  }

  constructor(
    private companyRepository: CompanyRepositoryService,
    public companiesHelperService: CompaniesHelperService,
    private route: ActivatedRoute,
    private router: Router,
    private contributionsMainService: ContributionMainService,
    private legalEntityRepo: CompanyLegalEntityRepositoryService,
  ) { }

  public ngOnInit() {
    const idParam = this.route.snapshot.params.id;
    this.legalTabDefaultWarning = this.legalNode.notAllowedTabMessage;
    this.setContributionsWarning();

    if (this.isCreationMode) {
      this.companiesHelperService.generateTitle();
      this.isCompanyResolved = true; // in creation mode show tabs immediately
      this.companiesHelperService.reset();
    } else {
      const id = parseInt(idParam, 10);
      this.companiesHelperService.setCompany(id);

      this.companiesHelperService.company$.pipe(
        takeUntil(this.componentDestroyed$)
      ).subscribe( currentCompany => {
        this.legalNode.hidden = currentCompany?.isPrivatePerson;
        if (currentCompany) {
          /**
           * subscribe to make sure company is resolved and set in service
           * so that tabs/subtabs don't fail and there won't be need to do same network
           * over and over
           */
          this.isCompanyResolved = true;
          this.companiesHelperService.legalTabDataMissing$.next(!currentCompany.legalForm);
          this.companiesHelperService.generateTitle(currentCompany);
        }
      });

    }

    this.companiesHelperService.editViewTitle$.pipe(
      takeUntil(this.componentDestroyed$)
    ).subscribe(title => {
      this.title = title;
    });
  }

  public routeBack = () => {
    this.router.navigate([
      url(sitemap.system.children.misc.children.companies)
    ]);
    this.companiesHelperService.reset();
  }

  public tabDisabler = (node: SitemapNode) => {
    const basicNode = sitemap.system.children.misc.children.companies.children.id.children.basic;

    if (this.isCreationMode) {
      return node === basicNode;
    } else if (node === this.legalNode) {
      return !this.companiesHelperService.legalTabDataMissing$.value;
    } else {
      return true;
    }
  }

  public isDataMissing = (node: SitemapNode): Observable<boolean> => {
    if (node.path === sitemap.system.children.misc.children.companies.children.id.children.legal.path) {
      return combineLatest([
        this.companiesHelperService.legalTabDataMissing$,
        this.showContributionWarning$,
      ]).pipe(
        map((values: boolean[]) => values.some(v => v))
      );
    } else {
      return of(false);
    }
  }

  private setContributionsWarning(): void {
    let contributionWarning: IWording;
    let companyId: CompanyKey;

    this.route.params.pipe(
      // the value of `id` can be 'new' on creationMode
      skipWhile(({ id }) => !+id),
      tap(({ id }) => companyId = +id),
      switchMap(() => combineLatest([
        this.companyRepository.getStream({ id: companyId }),
        this.legalEntityRepo.getStream({ companyId }),
        this.contributionsMainService.getStream({ queryParams: { cid: companyId } }),
      ])),
      map(([[company], [legalEntity], contributions]) => {
        const showWarning: boolean = this.contributionsMainService.shouldShowLegalEntityTabWarning(
          company,
          legalEntity,
          this.contributionsMainService.filterContributions(
            contributions,
            moment().startOf('d').toISOString(),
          ),
        );

        if (showWarning) {
          const contributionTypeName: IWording =  legalEntityExclamationTooltipContributions
            .get(getContributionWordingKey(company.legalForm, legalEntity.capitalType));

          contributionWarning = setTranslationSubjects(
            wording.system.contributionDeviationDetected,
            { contributionTypeName },
          );
        }

        this.setLegalNodeWarningMessage(
          showWarning ? contributionWarning : this.legalTabDefaultWarning
        );
        return showWarning;
      }),
      takeUntil(this.componentDestroyed$),
    ).subscribe(show => this.showContributionWarning$.next(!!show));

    this.legalEntityRepo.fetchOne(companyId, {}).subscribe();
  }

  private setLegalNodeWarningMessage(messageWording: IWording): void {
    this.legalNode.notAllowedTabMessage = messageWording;
  }

  public ngOnDestroy() {
    this.setLegalNodeWarningMessage(this.legalTabDefaultWarning);
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }
}
