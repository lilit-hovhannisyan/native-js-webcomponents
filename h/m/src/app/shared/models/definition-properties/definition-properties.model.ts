import { BaseUploadModel } from '@shared-models/file-upload/file-upload.model';

export class DefinitionPropertiesModel {
  fileUpload: BaseUploadModel[] = [];
}
