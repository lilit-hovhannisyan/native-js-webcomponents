import { Injectable } from '@angular/core';
import { Observable, forkJoin, BehaviorSubject } from 'rxjs';
import { map, tap} from 'rxjs/operators';
import { VesselMainService } from 'src/app/core/services/mainServices/vessel-main.service';
import { IVesselAccountingListItem } from 'src/app/core/models/resources/IVesselAccounting';
import { SettingsService } from 'src/app/core/services/settings.service';
import { Language } from 'src/app/core/models/language';
import { wording } from 'src/app/core/constants/wording/wording';
import { IVessel, VesselKey } from 'src/app/core/models/resources/IVessel';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { RegistrationVesselTasksMainService } from 'src/app/core/services/mainServices/registration-vessel-tasks-main.service';
import { VesselRepositoryService } from '../../../../../../core/services/repositories/vessel-repository.service';
import { VesselTaskStatus } from 'src/app/core/models/enums/vessel-registration-tasks';
import { IVesselRegister, VesselRegisterKey } from 'src/app/core/models/resources/IVesselRegister';
import { VoyageCharternameRepositoryService } from '../../../../../../core/services/repositories/voyage-chartername-repository.service';
import { IVesselTaskFull } from 'src/app/core/models/resources/IVesselTask';

@Injectable()
export class RegistrationFleetHelperService {
  public buttonsCount = 2;
  private language: Language = this.settingsService.language;

  public selectedRegisterSubject: BehaviorSubject<IVesselRegister> = new BehaviorSubject(null);
  public registersSliderDisabledSubject: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private vesselMainService: VesselMainService,
    private vesselRepo: VesselRepositoryService,
    private registrationVesselTaskMainService: RegistrationVesselTasksMainService,
    private settingsService: SettingsService,
    private charternameRepo: VoyageCharternameRepositoryService,
  ) { }

  public generateTitle(vesselId: VesselKey): Observable<string> {
    return forkJoin([
      this.vesselMainService.fetchVesselsWithClosestAccounting(),
      this.vesselRepo.fetchAll({}),
      this.charternameRepo.fetchAllPreDeliveryCharternames(),
    ]).pipe(
      map(([vesselAccountingsListItems, vessels, charternames]) => {
        const registration: string = wording.chartering.registration[this.language];
        const vesselAccountingListItem: IVesselAccountingListItem
          = vesselAccountingsListItems.find(v => v.vesselId === vesselId);

        const vessel: IVessel = vessels.find(v => v.id === vesselId);
        const charternameValue: string = charternames.find(c => c.vesselId === vessel.id)?.charterName;

        if (!vesselAccountingListItem) {
          const title: string = charternameValue
            ? `${registration}: ${vessel?.imo} (${charternameValue})`
            : `${registration}: ${vessel?.imo}`;
          return title;
        }

        const { vesselName, charterName } = vesselAccountingListItem;
        const charterNameSection: string = charterName ? ` (${charterName})` : '';

        return `${registration}: ${vesselName}${charterNameSection}`;
      }),
    );
  }

  /** Adds tasks count on tab title. */
  public changeTasksTabTitle(
    vesselId: VesselKey,
    vesselRegisterId?: VesselRegisterKey,
  ): Observable<any> {
    const defaultWording = wording.chartering.tasks;
    const tasks = sitemap.chartering.children.registrations
      .children.registrationsFleet.children.id.children.tasks;

    return  this.registrationVesselTaskMainService.getStream(vesselId).pipe(
      map(allTasks => {
        return vesselRegisterId
          ? allTasks.filter(t => t.vesselRegistrationId === vesselRegisterId)
          : allTasks;
      }),
      tap((allTasks: IVesselTaskFull[]) => {
        const doneTasksCount: number = allTasks
          .filter(t => t.status === VesselTaskStatus.Done).length;

        tasks.title = {
          en: `${defaultWording.en} (${doneTasksCount} / ${allTasks.length})`,
          de: `${defaultWording.de} (${doneTasksCount} / ${allTasks.length})`,
        };
      })
    );
  }

  public resetTasksTabDefaultTitle(): void {
    sitemap.chartering.children.registrations.children.registrationsFleet.children
      .id.children.tasks.title = wording.chartering.tasks;
  }
}
