import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { MandatorRoleKey } from 'src/app/core/models/resources/IMandatorRole';
import { MandatorKey } from 'src/app/core/models/resources/IMandator';
import { HttpPost } from 'src/app/core/models/http/http-post';
import { IResourceCollection } from 'src/app/core/models/resources/IResourceCollection';
import { IDataResponseBody } from 'src/app/core/models/responses/IDataResponseBody';
import { RoleKey } from 'src/app/core/models/resources/IRole';
import { RepoParams, AbstractRepository } from 'src/app/core/abstracts/abstract.repository';
import { IMandatorRole, IMandatorRoleDTO, IMandatorRoleCreate } from 'src/app/core/models/resources/IMandatorRole';
import { RepositoryService } from '../repository.service';

@Injectable({
  providedIn: 'root'
})
export class MandatorRoleRepositoryService extends AbstractRepository<
  IMandatorRole,
  IMandatorRoleDTO,
  IMandatorRoleCreate,
  RepoParams<IMandatorRole>,
  MandatorRoleKey
> {
  protected resourceType = 'MandatorRole';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  public getBaseUrl(): string {
    return '/api/mandator-roles/';
  }

  public setRolesForMandator(roleIds: RoleKey[], mandatorId: MandatorKey): Observable<IMandatorRole[]> {
    const url = `/api/mandators/${mandatorId}/mandator-roles`;
    const request = new HttpPost(url, roleIds);
    const request$ = this.repositoryService.httpClient.request(request).pipe(
      map((response: HttpResponse<IDataResponseBody<IResourceCollection<IMandatorRole>>>) => response.body.data.items),
      tap((mandatorRoles: IMandatorRole[]) => { this.pushToStream(mandatorId, undefined, mandatorRoles); })
    );
    return request$;
  }

  public setMandatorsForRole(mandatorIds: MandatorKey[], roleId: RoleKey): Observable<IMandatorRole[]> {
    const url = `/api/roles/${roleId}/mandator-roles`;
    const request = new HttpPost(url, mandatorIds);
    const request$ = this.repositoryService.httpClient.request(request).pipe(
      map((response: HttpResponse<IDataResponseBody<IResourceCollection<IMandatorRole>>>) => response.body.data.items),
      tap((mandatorRoles: IMandatorRole[]) => { this.pushToStream(undefined, roleId, mandatorRoles); })
    );
    return request$;
  }

  public pushToStream(
    mandatorId: MandatorKey | undefined,
    roleId: RoleKey | undefined,
    mandatorRoles: IMandatorRole[]
  ) {
    const allMandatorRoles = this.streamSubject.getValue();
    // first remove all previous mandatorRoles of current mandator from stream
    const allMandatorRolesFiltered = allMandatorRoles.filter(mandatorRole => mandatorId
      ? mandatorRole.mandatorId !== mandatorId
      : mandatorRole.roleId !== roleId
    );
    // then add new mandatorRoles ( server sends down all mandatorRoles of current mandator )
    // ( this way we can also remove mandatorRoles from the stream )
    this.streamSubject.next([ ...allMandatorRolesFiltered, ...mandatorRoles ]);
  }



  protected postProcess(item: IMandatorRoleDTO): IMandatorRole { return item; }
}
