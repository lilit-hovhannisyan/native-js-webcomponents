import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { ITimeChartererRate } from 'src/app/core/models/resources/ITimeCharterPeriod';
import { NvLocale } from 'src/app/core/models/dateFormat';
import { wording } from 'src/app/core/constants/wording/wording';
import { IWording } from 'src/app/core/models/resources/IWording';
import { SettingsService } from 'src/app/core/services/settings.service';
import { setTranslationSubjects } from 'src/app/shared/helpers/general';

interface ITimeChartererRateRaw extends ITimeChartererRate {
  charterRateRaw?: number;
  charterRateTouched?: boolean;
  fromTouched?: boolean;
}

@Component({
  selector: 'nv-charterer-rates',
  templateUrl: './charterer-rates.component.html',
  styleUrls: ['./charterer-rates.component.scss']
})
export class ChartererRatesComponent implements OnInit {
  public _chartererRates: ITimeChartererRateRaw[];

  @Input() public showCreateRow = true;
  @Input() public disableFields: boolean;
  @Input() public inEditMode: boolean;
  @Input() public addNewRowOnInit: boolean;
  @Input() public set chartererRates(chartererRates: ITimeChartererRateRaw[]) {
    this._chartererRates = chartererRates;
    this._chartererRates.forEach(c => {
      c.charterRateRaw = c.charterRate;
    });
  }
  @Output() public valueChange: EventEmitter<ITimeChartererRate[]> = new EventEmitter<ITimeChartererRate[]>();

  // validation message assigned to the property for the sake of shortness in further use
  public shouldBeGreaterOrEqualMessage = setTranslationSubjects(
    wording.general.theValueShouldBeGreaterOrEqualThan,
    { subject: 0 },
  );

  public defaultCountsFrom: string;
  public defaultCharterRate: number;
  public maxCharterRate = 99999999.99;
  public wording = wording;

  constructor(private settingsService: SettingsService) { }

  public ngOnInit() {
    // used to avoid ExpressionHasChanged error
    setTimeout(() => {
      if (this.addNewRowOnInit && this.isListValid()) {
        this.addNewRow();
      }
    }, 0);
    this.sortCharterRates();
  }

  public onCountsFromChange(value: string, index: number) {
    if (this._chartererRates[index].from !== value) {
      this._chartererRates[index].from = value;
      this.sortCharterRates();
      this.valueChange.emit(this._chartererRates);
    }

    if (!this._chartererRates[index].fromTouched) {
      this._chartererRates[index].fromTouched = true;
    }
  }

  /**
   * Assigns value to charterRateRaw on every charterRate change
   * for checking validation status during typing
   */
  public onCharterRateChange(value: number, index: number) {
    this._chartererRates[index].charterRateRaw = value;
    this._chartererRates[index].charterRate = value;
    this._chartererRates[index].charterRateTouched = true;
    this.valueChange.emit(this._chartererRates);
  }

  public validateList() {
    this._chartererRates.forEach(c => {
      c.charterRateTouched = true;
      c.fromTouched = true;
    });
  }

  public addNewRow() {
    this._chartererRates = [
      ...this._chartererRates,
      { charterRate: undefined, from: undefined }
    ];
    this.valueChange.emit(this._chartererRates);
  }

  public isListValid(): boolean {
    return this._chartererRates
      .every(c => !!(
        c.from
        && c.charterRate !== undefined
        && c.charterRate < this.maxCharterRate
        && c.charterRate >= 0)
      );
  }

  public sortCharterRates() {
    this._chartererRates = this._chartererRates.sort((a, b) => {
      if (!a.from || !b.from) {
        return 0;
      }

      if (new Date(a.from) < new Date(b.from)) {
        return -1;
      }

      if (new Date(a.from) > new Date(b.from)) {
        return 1;
      }

      return 0;
    });
  }

  public getCharterRateErrorMessage(value: number): IWording {
    if (value === undefined) {
      return wording.general.theFieldIsRequired;
    }

    if (value < 0) {
      return this.shouldBeGreaterOrEqualMessage;
    }

    if (value > this.maxCharterRate) {
      const germanFormat = this.settingsService.locale === NvLocale.DE;
      const decimalSeparator = germanFormat ? ',' : '.';
      const thousandsSeparator = germanFormat ? '.' : ',';

      return setTranslationSubjects(
        wording.general.theValueShouldBeLessThan,
        { subject: `99${thousandsSeparator}999${thousandsSeparator}999${decimalSeparator}99` },
      );
    }
  }

  public delete(index: number) {
    this._chartererRates.splice(index, 1);
    this.valueChange.emit(this._chartererRates);
  }
}
