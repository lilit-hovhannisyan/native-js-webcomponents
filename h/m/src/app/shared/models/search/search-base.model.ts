export class BaseSearchModel {
  domain: string = '';
  collectionOid?: string = '';
  orderByAttr?: string = '';
  orderByDir?: string = '';
  states?: string[] = [];
}
