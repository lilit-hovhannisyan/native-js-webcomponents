import { Directive, ElementRef, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { google } from 'google-maps';

declare type GAutocomplete = google.maps.places.Autocomplete;

@Directive({
  selector: '[nvGooglePlaces]'
})
export class GooglePlacesDirective implements OnInit {
  @Output() public select: EventEmitter<any> = new EventEmitter();
  private element: HTMLInputElement;

  constructor(elRef: ElementRef) {
    this.element = elRef.nativeElement;
  }

  public ngOnInit(): void {
    const autocomplete: GAutocomplete = new google.maps.places.Autocomplete(this.element);
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      this.select.emit(autocomplete.getPlace());
    });
  }

}
