import { ExpensesDetailViewComponent } from 'src/app/features/chartering/components/misc/expenses/detail-view/detail-view.component';
import { ExpensesBasicComponent } from 'src/app/features/chartering/components/misc/expenses/detail-view/expeses-basic/expenses-basic.component';
import { ExpensesRemarksComponent } from 'src/app/features/chartering/components/misc/expenses/detail-view/expeses-remarks/expenses-remarks.component';
import { ExpensesListViewComponent } from 'src/app/features/chartering/components/misc/expenses/list-view/list-view.component';
import { VesselBasicComponent } from './components/misc/vessels/detail-view/vessel-basic/vessel-basic.component';
import { VesselListViewComponent } from './components/misc/vessels/list-view/list-view.component';
import { PortDetailViewComponent } from './components/misc/ports/detail-view/detail-view.component';
import { NgModule } from '@angular/core';
import { PageNotFoundComponent } from '../../shared/components/page-not-found/page-not-found.component';
import { NavigationBoardComponent } from '../../shared/components/navigation-board/navigation-board.component';
import { Routes, RouterModule } from '@angular/router';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { PortListViewComponent } from './components/misc/ports/list-view/list-view.component';
import { VesselDetailViewComponent } from './components/misc/vessels/detail-view/detail-view.component';
import { VesselSpecificationComponent } from './components/misc/vessels/detail-view/vessel-specification/vessel-specification.component';
import { ShipyardsListViewComponent } from './components/misc/shipyards/list-view/shipyards-list-view.component';
import { ShipyardTypesListViewComponent } from './components/misc/shipyard-types/list-view/shipyard-types-list-view.component';
import { CommissionTypesListViewComponent } from './components/misc/commission-types/list-view/commission-types-list-view.component';
import { VesselAccountingComponent } from './components/misc/vessels/detail-view/vessel-accounting/vessel-accounting.component';
import { CommissionTypesDetailViewComponent } from './components/misc/commission-types/detail-view/detail-view.component';
import { ShipyardTypesDetailViewComponent } from './components/misc/shipyard-types/detail-view/shipyard-types-detail-view.component';
import { ShipyardsDetailViewComponent } from './components/misc/shipyards/detail-view/shipyards-detail-view.component';
import { VoyageListViewComponent } from './components/misc/voyages/list-view/voyage-list-view.component';
import { VoyageDetailViewComponent } from './components/misc/voyages/detail-view/voyage-detail-view.component';
import { VesselCommissionComponent } from './components/misc/vessels/detail-view/vessel-commission/vessel-commission.component';
import { VoyageMainComponent } from './components/misc/voyages/main-view/voyage-main-view.component';
import { TimeCharterDetailViewComponent } from './components/misc/time-charter/time-charter-detail-view.component';
import { TimeCharterBasicComponent } from './components/misc/time-charter/time-charter-basic/time-charter-basic.component';
import { TimeCharterPeriodsComponent } from './components/misc/time-charter/time-charter-periods/time-charter-periods.component';
import { TimeCharterBillingComponent } from './components/misc/time-charter/time-charter-billing/time-charter-billing.component';
import { VoyageBasicComponent } from './components/misc/voyages/detail-view/voyage-basic/voyage-basic.component';
import { VoyageOffHireComponent } from './components/misc/voyages/detail-view/voyage-off-hire/voyage-off-hire.component';
import { UnsavedChangesGuard } from 'src/app/routing/services/unsaved-changes.guard';
import { BunkersListViewComponent } from './components/misc/bunkers/list-view/bunkers-list-view.component';
import { BunkersDetailViewComponent } from './components/misc/bunkers/detail-view/bunkers-detail-view.component';
import { VesselRegisterComponent } from './components/misc/vessels/detail-view/vessel-register/vessel-register.component';
import { VesselSettingsListViewComponent } from './components/misc/vessels/detail-view/vessel-settings/vessel-settings.component';
import { RegistrationsFleetListViewComponent } from './components/registrations/registrations-fleet/registrations-fleet-list-view/registrations-fleet-list-view.component';
import { RegistrationFleetBasicComponent } from './components/registrations/registrations-fleet/registrations-fleet-detail-view/registration-fleet-basic/registration-fleet-basic.component';
import { RegistrationFleetMortgageComponent } from './components/registrations/registrations-fleet/registrations-fleet-detail-view/registration-fleet-mortgage/registration-fleet-mortgage.component';
import { RegistrationFleetTechnicalComponent } from './components/registrations/registrations-fleet/registrations-fleet-detail-view/registration-fleet-technical/registration-fleet-technical.component';
import { VesselRolesComponent } from './components/misc/vessels/detail-view/vessel-roles/vessel-roles.component';
import { RegistrationsFleetDetailViewComponent } from './components/registrations/registrations-fleet/registrations-fleet-detail-view/registrations-fleet-detail-view.component';
import { RegistrationVesselTasksComponent } from './components/registrations/registrations-fleet/registrations-fleet-detail-view/registration-vessel-tasks/registration-vessel-tasks.component';
import { VesselRegistrationRegisterComponent } from './components/registrations/registrations-fleet/registrations-fleet-detail-view/vessel-registration-register/vessel-registration-register.component';
import { DashboardComponent } from './components/registrations/dashboard/dashboard.component';
import { RegistartionCalendarComponent } from './components/registrations/calendar/calendar.component';
import { VesselPrimaryAndSecondaryRegisterComponent } from './shared/vessel-primary-secondary-register/vessel-primary-secondary-register.component';

export const routes: Routes = [
  {
    path: '',
    component: NavigationBoardComponent,
    pathMatch: 'full'
  },
  {
    path: sitemap.system.children.misc.path,
    children: [
      {
        path: '',
        component: NavigationBoardComponent,
        pathMatch: 'full'
      },
      {
        path: sitemap.chartering.children.misc.children.ports.path,
        children: [
          {
            path: '',
            component: PortListViewComponent
          },
          {
            path: ':id',
            component: PortDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          }
        ]
      },
      {
        path: sitemap.chartering.children.misc.children.vessels.path,
        children: [
          {
            path: '',
            component: VesselListViewComponent
          },
          {
            path: ':id',
            component: VesselDetailViewComponent,
            children: [
              {
                path: sitemap.chartering.children.misc.children.vessels.children.id.children.basic.path,
                component: VesselBasicComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.misc.children.vessels.children.id.children.specification.path,
                component: VesselSpecificationComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.misc.children.vessels.children.id.children.accounting.path,
                component: VesselAccountingComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.misc.children.vessels.children.id.children.commissions.path,
                component: VesselCommissionComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.misc.children.vessels.children.id.children.registrations.path,
                component: VesselRegisterComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.misc.children.vessels.children.id.children.settings.path,
                component: VesselSettingsListViewComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.misc.children.vessels.children.id.children.roles.path,
                component: VesselRolesComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.chartering.children.misc.children.vessels.children.id.children.basic.path,
              },
            ]
          }
        ]
      },
      {
        path: sitemap.chartering.children.misc.children.shipyards.path,
        children: [
          {
            path: '',
            component: ShipyardsListViewComponent
          },
          {
            path: ':id',
            component: ShipyardsDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          },
        ]
      },
      {
        path: sitemap.chartering.children.misc.children.shipyardTypes.path,
        children: [
          {
            path: '',
            component: ShipyardTypesListViewComponent
          },
          {
            path: ':id',
            component: ShipyardTypesDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          },
        ]
      },
      {
        path: sitemap.chartering.children.misc.children.commissionTypes.path,
        children: [
          {
            path: '',
            component: CommissionTypesListViewComponent
          },
          {
            path: ':id',
            component: CommissionTypesDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          },
        ]
      },
      {
        path: sitemap.chartering.children.misc.children.voyages.path,
        children: [
          {
            path: '',
            component: VoyageMainComponent,
          },
          {
            path: ':id',
            component: VoyageDetailViewComponent,
            children: [
              {
                path: sitemap.chartering.children.misc.children.voyages.children.id.children.basic.path,
                component: VoyageBasicComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.misc.children.voyages.children.id.children.offHire.path,
                component: VoyageOffHireComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.chartering.children.misc.children.voyages.children.id.children.basic.path,
              }
            ]
          }
        ]
      },
      {
        path: sitemap.chartering.children.misc.children.timeCharters.path,
        children: [
          {
            path: ':id',
            component: TimeCharterDetailViewComponent,
            children: [
              {
                path: sitemap.chartering.children.misc.children.timeCharters.children.id.children.basic.path,
                component: TimeCharterBasicComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.misc.children.timeCharters.children.id.children.periods.path,
                component: TimeCharterPeriodsComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.misc.children.timeCharters.children.id.children.billing.path,
                component: TimeCharterBillingComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.chartering.children.misc.children.vessels.children.id.children.basic.path,
              },
            ],
          }
        ]
      },
      {
        path: sitemap.chartering.children.misc.children.expenses.path,
        children: [
          {
            path: '',
            pathMatch: 'full',
            component: ExpensesListViewComponent,
          },
          {
            path: ':id',
            component: ExpensesDetailViewComponent,
            children: [
              {
                path: sitemap.chartering.children.misc.children.expenses.children.id.children.basic.path,
                component: ExpensesBasicComponent,
                canDeactivate: [UnsavedChangesGuard],
              },
              {
                path: sitemap.chartering.children.misc.children.expenses.children.id.children.remarks.path,
                component: ExpensesRemarksComponent,
                canDeactivate: [UnsavedChangesGuard],
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.chartering.children.misc.children.expenses.children.id.children.basic.path,
              }
            ],
          },
        ]
      },
      {
        path: sitemap.chartering.children.misc.children.bunkers.path,
        children: [
          {
            path: '',
            component: BunkersListViewComponent
          },
          {
            path: ':id',
            component: BunkersDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          },
        ]
      },
    ],
  },
  {
    path: sitemap.chartering.children.registrations.path,
    children: [
      {
        path: '',
        component: NavigationBoardComponent,
        pathMatch: 'full'
      },
      {
        path: sitemap.chartering.children.registrations.children.dashboard.path,
        component: DashboardComponent,
        canDeactivate: [UnsavedChangesGuard],
      },
      {
        path: sitemap.chartering.children.registrations.children.registrationsFleet.path,
        children: [
          {
            path: '',
            component: RegistrationsFleetListViewComponent
          },
          {
            path: ':id',
            component: RegistrationsFleetDetailViewComponent,
            children: [
              {
                path: sitemap.chartering.children.registrations.children.registrationsFleet.children.id.children.basic.path,
                component: RegistrationFleetBasicComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.registrations.children.registrationsFleet.children.id.children.register.path,
                component: VesselRegistrationRegisterComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.registrations.children.registrationsFleet.children.id.children.technical.path,
                component: RegistrationFleetTechnicalComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.registrations.children.registrationsFleet.children.id.children.mortgage.path,
                component: RegistrationFleetMortgageComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.chartering.children.registrations.children.registrationsFleet.children.id.children.tasks.path,
                component: RegistrationVesselTasksComponent,
                canDeactivate: [UnsavedChangesGuard],
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.chartering.children.registrations.children.registrationsFleet.children.id.children.basic.path,
              }
            ],
          }
        ]
      },
      {
        path: sitemap.chartering.children.registrations.children.calendar.path,
        component: RegistartionCalendarComponent,
      },
    ],
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  }
];

export const CHARTERING_DECLARATIONS = [
  PortListViewComponent,
  PortDetailViewComponent,
  VesselListViewComponent,
  VesselDetailViewComponent,
  VesselSpecificationComponent,
  VesselBasicComponent,
  ShipyardsListViewComponent,
  ShipyardTypesListViewComponent,
  CommissionTypesListViewComponent,
  VesselAccountingComponent,
  VesselSettingsListViewComponent,
  CommissionTypesDetailViewComponent,
  ShipyardTypesDetailViewComponent,
  ShipyardsDetailViewComponent,
  VoyageListViewComponent,
  VoyageDetailViewComponent,
  VesselCommissionComponent,
  VoyageMainComponent,
  VoyageBasicComponent,
  VoyageOffHireComponent,
  TimeCharterDetailViewComponent,
  TimeCharterBasicComponent,
  TimeCharterPeriodsComponent,
  TimeCharterBillingComponent,
  ExpensesDetailViewComponent,
  ExpensesListViewComponent,
  ExpensesBasicComponent,
  ExpensesRemarksComponent,
  BunkersListViewComponent,
  BunkersDetailViewComponent,
  VesselRegisterComponent,
  RegistrationsFleetListViewComponent,
  RegistrationFleetBasicComponent,
  RegistrationFleetTechnicalComponent,
  VesselRolesComponent,
  RegistrationsFleetDetailViewComponent,
  RegistrationVesselTasksComponent,
  VesselRegistrationRegisterComponent,
  DashboardComponent,
  RegistartionCalendarComponent,
  VesselPrimaryAndSecondaryRegisterComponent,
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule { }
