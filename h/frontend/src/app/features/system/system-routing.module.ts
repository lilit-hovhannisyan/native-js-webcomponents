import { RolesDetailViewComponent } from './components/misc/roles/detail-view/roles-detail-view.component';
import { NgModule } from '@angular/core';
import { CurrencyDetailViewComponent } from './components/misc/currencies/detail-view/currency-detail-view.component';
import { CurrencyListViewComponent } from './components/misc/currencies/list-view/currency-list-view.component';
import { BankDetailViewComponent } from './components/misc/banks/detail-view/detail-view.component';
import { BankListViewComponent } from './components/misc/banks/list-view/list-view.component';
import { CompanyDetailViewComponent } from './components/misc/companies/detail-view/company-detail-view.component';
import { CompanyListViewComponent } from './components/misc/companies/list-view/company-list-view.component';
import { CountryDetailViewComponent } from './components/misc/countries/detailView/detail-view.component';
import { CountryListViewComponent } from './components/misc/countries/listView/list-view.component';
import { PageNotFoundComponent } from './../../shared/components/page-not-found/page-not-found.component';
import { NavigationBoardComponent } from './../../shared/components/navigation-board/navigation-board.component';
import { Routes, RouterModule } from '@angular/router';
import { sitemap } from 'src/app/core/constants/sitemap/sitemap';
import { RolesListViewComponent } from './components/misc/roles/list-view/roles-list-view.component';
import { UserListViewComponent } from './components/misc/users/list-view/users-list-view.component';
import { UserDetailViewComponent } from './components/misc/users/detail-view/users-detail-view.component';
import { GlobalSettingsComponent } from './components/settings/global/global-settings.component';
import { CompanyBasicComponent } from './components/misc/companies/detail-view/company-basic/company-basic.component';
import { CompanyBankAccountsTabComponent } from './components/misc/companies/detail-view/company-bank-accounts-tab/company-bank-accounts-tab.component';
import { CompanyAddressTabComponent } from './components/misc/companies/detail-view/company-address-tab/company-address-tab.component';
import { CompanyCategoriesTabComponent } from './components/misc/companies/detail-view/company-categories-tab/company-categories-tab.component';
import { UserBasicComponent } from './components/misc/users/detail-view/user-basic/user-basic.component';
import { UserRolesComponent } from './components/misc/users/detail-view/user-roles/user-roles.component';
import { RoleMandatorsComponent } from './components/misc/roles/detail-view/mandators/role-mandators.component';
import { RoleUsersComponent } from './components/misc/roles/detail-view/users/role-users.component';
import { CurrencyBasicComponent } from './components/misc/currencies/detail-view/currency-basic/currency-basic.component';
import { CurrencyExchangeRatesComponent } from './components/misc/currencies/detail-view/currency-exchange-rates/currency-exchange-rates.component';
import { RoleBasicComponent } from './components/misc/roles/detail-view/role-basic/role-basic.component';
import { UnsavedChangesGuard } from 'src/app/routing/services/unsaved-changes.guard';
import { MaintenanceComponent } from './components/settings/maintenance/maintenance.component';
import { MaintenanceDetailViewComponent } from './components/settings/maintenance/maintenance-detail-view/maintenance-detail-view.component';
import { CompanyCategoriesListViewComponent } from './components/misc/company-categories/listView/list-view.component';
import { CompanyCategoriesDetailViewComponent } from './components/misc/company-categories/detailView/detail-view.component';
import { CompanyContactTabComponent } from './components/misc/companies/detail-view/company-contact-tab/company-contact-tab.component';
import { CompanyLegalTabComponent } from './components/misc/companies/detail-view/company-legal-tab/company-legal-tab.component';
import { LegalEntityComponent } from './components/misc/companies/detail-view/company-legal-tab/legal-entity/legal-entity.component';
import { RepresentativesComponent } from './components/misc/companies/detail-view/company-legal-tab/representatives/representatives.component';
import { ShareholdersPartnersComponent } from './components/misc/companies/detail-view/company-legal-tab/shareholders-partners/shareholders-partners.component';
import { TrustorsComponent } from './components/misc/companies/detail-view/company-legal-tab/trustors/trustors.component';
import { SubParticipantsComponent } from './components/misc/companies/detail-view/company-legal-tab/sub-participants/sub-participants.component';
import { SilentPartnersComponent } from './components/misc/companies/detail-view/company-legal-tab/silent-partners/silent-partners.component';
import { DocumentsComponent } from './components/misc/companies/detail-view/company-legal-tab/documents/documents.component';
import { ContributionsDetailViewComponent } from './components/misc/companies/detail-view/company-legal-tab/shareholders-partners/contributions-detail-view/contributions-detail-view.component';
import { ShareholdingsListViewComponent } from './components/misc/shareholdings/list-view/list-view.component';
import { ShareholdingsDetailViewComponent } from './components/misc/shareholdings/detail-view/detail-view.component';
import { CompanyLegalEntityGuardService } from './services/company-legal-entity.guard';
import { CompanyGroupsDetailViewComponent } from './components/misc/company-groups/detail-view/detail-view.component';
import { CompanyGroupsListViewComponent } from './components/misc/company-groups/list-view/list-view.component';
import { RoleVesselsComponent } from './components/misc/roles/detail-view/role-vessels/role-vessels.component';
import { ShareholdingsRepresentativesComponent } from './components/misc/shareholdings/detail-view/representatives/representatives.component';
import { ShareholdingsShareholdersPartnersComponent } from './components/misc/shareholdings/detail-view/shareholders-partners/shareholders-partners.component';
import { ShareholdingsSilentPartnersComponent } from './components/misc/shareholdings/detail-view/silent-partners/silent-partners.component';
import { ShareholdingsTrustorsComponent } from './components/misc/shareholdings/detail-view/trustors/trustors.component';
import { ShareholdingsSubParticipantsComponent } from './components/misc/shareholdings/detail-view/sub-participants/sub-participants.component';
import { ShareholdingsDocumentsComponent } from './components/misc/shareholdings/detail-view/documents/documents.component';
import { ShareholdingsLegalEntityComponent } from './components/misc/shareholdings/detail-view/legal-entity/legal-entity.component';
import { TaskTemplatesComponent } from './components/misc/task-templates/task-templates.component';
import { MetaComponent } from 'src/app/shared/navido-components/meta/meta.component';
import { CompanyRepositoryService } from 'src/app/core/services/repositories/company-repository.service';

export const routes: Routes = [
  {
    path: '',
    component: NavigationBoardComponent,
    pathMatch: 'full'
  },
  {
    path: sitemap.system.children.misc.path,
    children: [
      {
        path: '',
        component: NavigationBoardComponent,
        pathMatch: 'full'
      },
      {
        path: sitemap.system.children.misc.children.countries.path,
        children: [
          {
            path: '',
            component: CountryListViewComponent,
          },
          {
            path: ':id',
            component: CountryDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]
          }
        ]
      },
      {
        path: sitemap.system.children.misc.children.companies.path,
        children: [
          {
            path: '',
            component: CompanyListViewComponent,
          },
          {
            path: ':id',
            component: CompanyDetailViewComponent,
            children: [
              {
                path: sitemap.system.children.misc.children.companies.children.id.children.basic.path,
                component: CompanyBasicComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.system.children.misc.children.companies.children.id.children.bankAccounts.path,
                component: CompanyBankAccountsTabComponent
              },
              {
                path: sitemap.system.children.misc.children.companies.children.id.children.addresses.path,
                component: CompanyAddressTabComponent
              },
              {
                path: sitemap.system.children.misc.children.companies.children.id.children.contacts.path,
                component: CompanyContactTabComponent
              },
              {
                path: sitemap.system.children.misc.children.companies.children.id.children.categories.path,
                component: CompanyCategoriesTabComponent,
              },
              {
                path: sitemap.system.children.misc.children.companies.children.id.children.legal.path,
                component: CompanyLegalTabComponent,
                canActivate: [CompanyLegalEntityGuardService],
                children: [
                  {
                    path: '',
                    redirectTo: sitemap.system.children.misc.children.companies.children.id.children.legal.children.legalEntity.path,
                    pathMatch: 'full',
                  },
                  {
                    path: sitemap.system.children.misc.children.companies.children.id.children.legal.children.legalEntity.path,
                    component: LegalEntityComponent,
                    canDeactivate: [UnsavedChangesGuard],
                  },
                  {
                    path: sitemap.system.children.misc.children.companies.children.id.children.legal.children.representatives.path,
                    component: RepresentativesComponent,
                  },
                  {
                    path: sitemap.system.children.misc.children.companies.children.id.children.legal.children.shareholdersPartners.path,
                    component: ShareholdersPartnersComponent,
                  },
                  {
                    path: sitemap.system.children.misc.children.companies.children.id.children.legal.children.trustors.path,
                    component: TrustorsComponent,
                  },
                  {
                    path: sitemap.system.children.misc.children.companies.children.id.children.legal.children.subParticipants.path,
                    component: SubParticipantsComponent,
                  },
                  {
                    path: sitemap.system.children.misc.children.companies.children.id.children.legal.children.silentPartners.path,
                    component: SilentPartnersComponent,
                  },
                  {
                    path: sitemap.system.children.misc.children.companies.children.id.children.legal.children.documents.path,
                    component: DocumentsComponent,
                  },
                  {
                    path: sitemap.system.children.misc.children.companies.children.id.children.legal.children.meta.path,
                    component: MetaComponent,
                    data: {
                      service: CompanyRepositoryService,
                    },
                    canDeactivate: [UnsavedChangesGuard]
                  },
                ]
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.system.children.misc.children.companies.children.id.children.basic.path,
              }
            ]
          }
        ]
      },
      {
        path: sitemap.system.children.misc.children.banks.path,
        children: [
          {
            path: '',
            component: BankListViewComponent,
          },
          {
            path: ':id',
            component: BankDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]

          }
        ]
      },
      {
        path: sitemap.system.children.misc.children.currencies.path,
        children: [
          {
            path: '',
            component: CurrencyListViewComponent,
          },
          {
            path: ':id',
            component: CurrencyDetailViewComponent,
            children: [
              {
                path: sitemap.system.children.misc.children.currencies.children.id.children.basic.path,
                component: CurrencyBasicComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.system.children.misc.children.currencies.children.id.children.exchangeRates.path,
                component: CurrencyExchangeRatesComponent,
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.system.children.misc.children.currencies.children.id.children.basic.path,
              }
            ]
          }
        ]
      },
      {
        path: sitemap.system.children.misc.children.roles.path,
        children: [
          {
            path: '',
            component: RolesListViewComponent,
          },
          {
            path: ':id',
            component: RolesDetailViewComponent,
            children: [
              {
                path: sitemap.system.children.misc.children.roles.children.id.children.basic.path,
                component: RoleBasicComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.system.children.misc.children.roles.children.id.children.mandators.path,
                component: RoleMandatorsComponent,
              },
              {
                path: sitemap.system.children.misc.children.roles.children.id.children.users.path,
                component: RoleUsersComponent,
              },
              {
                path: sitemap.system.children.misc.children.roles.children.id.children.vessels.path,
                component: RoleVesselsComponent,
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.system.children.misc.children.roles.children.id.children.basic.path,
              }
            ]
          }
        ]
      },
      {
        path: sitemap.system.children.misc.children.users.path,
        children: [
          {
            path: '',
            component: UserListViewComponent,
          },
          {
            path: ':id',
            component: UserDetailViewComponent,
            children: [
              {
                path: sitemap.system.children.misc.children.users.children.id.children.basic.path,
                component: UserBasicComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.system.children.misc.children.users.children.id.children.roles.path,
                component: UserRolesComponent
              },
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.system.children.misc.children.users.children.id.children.basic.path,
              }
            ]
          }
        ]
      },
      {
        path: sitemap.system.children.misc.children.companyCategories.path,
        children: [
          {
            path: '',
            component: CompanyCategoriesListViewComponent,
          },
          {
            path: ':id',
            component: CompanyCategoriesDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard],
          }
        ]
      },
      {
        path: sitemap.system.children.misc.children.shareholdings.path,
        children: [
          {
            path: '',
            component: ShareholdingsListViewComponent,
          },
          {
            path: ':id',
            component: ShareholdingsDetailViewComponent,
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: sitemap.system.children.misc.children.shareholdings.children.id.children.legalEntity.path,
              },
              {
                path: sitemap.system.children.misc.children.shareholdings.children.id.children.legalEntity.path,
                component: ShareholdingsLegalEntityComponent,
                canDeactivate: [UnsavedChangesGuard]
              },
              {
                path: sitemap.system.children.misc.children.shareholdings.children.id.children.representatives.path,
                component: ShareholdingsRepresentativesComponent,
              },
              {
                path: sitemap.system.children.misc.children.shareholdings.children.id.children.shareholdersPartners.path,
                component: ShareholdingsShareholdersPartnersComponent,
              },
              {
                path: sitemap.system.children.misc.children.shareholdings.children.id.children.trustors.path,
                component: ShareholdingsTrustorsComponent,
              },
              {
                path: sitemap.system.children.misc.children.shareholdings.children.id.children.subParticipants.path,
                component: ShareholdingsSubParticipantsComponent,
              },
              {
                path: sitemap.system.children.misc.children.shareholdings.children.id.children.silentPartners.path,
                component: ShareholdingsSilentPartnersComponent,
              },
              {
                path: sitemap.system.children.misc.children.shareholdings.children.id.children.documents.path,
                component: ShareholdingsDocumentsComponent,
              },
              {
                path: sitemap.system.children.misc.children.shareholdings.children.id.children.meta.path,
                component: MetaComponent,
                data: {
                  service: CompanyRepositoryService,
                  fetchOneMethodName: 'fetchByShareholdingId',
                },
                canDeactivate: [UnsavedChangesGuard]
              },
            ]
          }
        ]
      },
      {
        path: sitemap.system.children.misc.children.companyGroups.path,
        children: [
          {
            path: '',
            component: CompanyGroupsListViewComponent,
          },
          {
            path: ':id',
            component: CompanyGroupsDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard],
          }
        ]
      },
      {
        path: sitemap.system.children.misc.children.taskTemplates.path,
        component: TaskTemplatesComponent,
      }
    ]
  },
  {
    path: sitemap.system.children.settings.path,
    children: [
      {
        path: '',
        component: NavigationBoardComponent,
        pathMatch: 'full'
      },
      {
        path: sitemap.system.children.settings.children.global.path,
        component: GlobalSettingsComponent
      },
      {
        path: sitemap.system.children.settings.children.maintenance.path,
        children: [
          {
            path: '',
            component: MaintenanceComponent,
          },
          {
            path: ':id',
            component: MaintenanceDetailViewComponent,
            canDeactivate: [UnsavedChangesGuard]

          }
        ]
      },
    ]
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  }
];

export const SYSTEM_DECLARATIONS = [
  CompanyListViewComponent,
  CompanyDetailViewComponent,
  CurrencyListViewComponent,
  CurrencyDetailViewComponent,
  RolesListViewComponent,
  RolesDetailViewComponent,
  UserListViewComponent,
  UserDetailViewComponent,
  GlobalSettingsComponent,
  MaintenanceComponent,
  CompanyCategoriesListViewComponent,
  CompanyCategoriesDetailViewComponent,
  CompanyCategoriesTabComponent,
  CompanyLegalTabComponent,
  LegalEntityComponent,
  RepresentativesComponent,
  ShareholdersPartnersComponent,
  TrustorsComponent,
  SubParticipantsComponent,
  SilentPartnersComponent,
  DocumentsComponent,
  ContributionsDetailViewComponent,
  ShareholdingsListViewComponent,
  ShareholdingsDetailViewComponent,
  CompanyGroupsListViewComponent,
  CompanyGroupsDetailViewComponent,
  RoleVesselsComponent,
  ShareholdingsRepresentativesComponent,
  ShareholdingsShareholdersPartnersComponent,
  ShareholdingsSilentPartnersComponent,
  ShareholdingsTrustorsComponent,
  ShareholdingsSubParticipantsComponent,
  ShareholdingsDocumentsComponent,
  ShareholdingsLegalEntityComponent,
  TaskTemplatesComponent,
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule { }
