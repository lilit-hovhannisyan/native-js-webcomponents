import { Injectable } from '@angular/core';
import { Observable, forkJoin, combineLatest } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { MandatorCostUnitRepositoryService } from '../repositories/mandator-cost-unit-repository.service';
import { MandatorRepositoryService } from '../repositories/mandator-repository.service';
import { CostUnitRepositoryService } from '../repositories/cost-unit-repository.service';
import { IMandatorCostUnit, IMandatorCostUnitRow, IMandatorCostUnitFull } from '../../models/resources/IMandatorCostUnit';
import { CostUnitKey, ICostUnit } from '../../models/resources/ICostUnit';
import { convertToMap } from 'src/app/shared/helpers/general';
import { IMandatorLookup, MandatorKey } from '../../models/resources/IMandator';

@Injectable({
  providedIn: 'root'
})
export class MandatorCostUnitMainService {
  constructor(
    private mandatorCostUnitRepo: MandatorCostUnitRepositoryService,
    private mandatorRepo: MandatorRepositoryService,
    private costUnitRepo: CostUnitRepositoryService,
  ) { }

  public getStream(costUnitId: CostUnitKey): Observable<IMandatorCostUnitFull[]> {
    return forkJoin([
      this.mandatorCostUnitRepo.fetchAll({ queryParams: { costUnitId } }),
      this.mandatorRepo.fetchLookups({}),
      this.costUnitRepo.fetchAll({}),
    ]).pipe(
      switchMap(() => combineLatest([
        this.mandatorCostUnitRepo.getStream({ costUnitId }),
        this.mandatorRepo.getLookupStream()
          .pipe(map(mandators => convertToMap<IMandatorLookup, MandatorKey>(mandators))),
        this.costUnitRepo.getStream()
          .pipe(map(costUnits => convertToMap<ICostUnit, CostUnitKey>(costUnits))),
      ])),
      map(response => this.getMandatorCostUnitsFullList(...response))
    );
  }

  private getMandatorCostUnitsFullList(
    mandatorCostUnits: IMandatorCostUnit[],
    mandators: Map<MandatorKey, IMandatorLookup>,
    costUnits: Map<CostUnitKey, ICostUnit>,
  ): IMandatorCostUnitFull[] {
    return mandatorCostUnits.map(mandatorCostUnit => {
      return this.getMandatorCostUnitFull(
        mandatorCostUnit,
        mandators,
        costUnits,
      );
    });
  }

  private getMandatorCostUnitFull(
    mandatorCostUnit: IMandatorCostUnit,
    mandators: Map<MandatorKey, IMandatorLookup>,
    costUnits: Map<CostUnitKey, ICostUnit>,
  ): IMandatorCostUnitFull {
    return {
      ...mandatorCostUnit,
      mandator: mandators.get(mandatorCostUnit.mandatorId),
      costUnit: costUnits.get(mandatorCostUnit.costUnitId),
    };
  }

  public toRows (mandatorCostUnits: IMandatorCostUnitFull[]): IMandatorCostUnitRow[] {
    return mandatorCostUnits.map((mandatorCostUnit) => this.toRow(mandatorCostUnit));
  }

  public toRow(mandatorCostUnit: IMandatorCostUnitFull): IMandatorCostUnitRow {
    const { mandator } = mandatorCostUnit;

    return ({
      ...mandatorCostUnit,
      no: mandator.no.toString().padStart(4, '0'),
      shortName: mandator.name,
    });
  }
}
