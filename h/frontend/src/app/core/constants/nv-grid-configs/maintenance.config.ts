import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from '../wording/wording';
import { IGridConfigActions } from '../../models/misc/gridConfigActions';
import { Operations } from '../../models/Operations';
import { IMaintenanceSchedule } from '../../models/resources/IMaintenanceSchedule';

export const getMaintenanceGridConfig = (
  actions: IGridConfigActions,
  canPerform: (Operations) => boolean
): NvGridConfig => {
  return ({
    gridName: 'mandatorAccountCreateViewGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    clearFiltersButton: true,
    columns: [
      {
        key: 'message',
        title: wording.general.message,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'startTime',
        title: wording.general.startDate,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        dataType: NvColumnDataType.DateTime
      },
      {
        key: 'estimatedEndTime',
        title: wording.general.estimatedEndTime,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        dataType: NvColumnDataType.DateTime
      },
      {
        key: 'actualEndTime',
        title: wording.general.actualEndTime,
        width: 150,
        isSortable: true,
        filter: {
          values: []
        },
        dataType: NvColumnDataType.DateTime
      },
      {
        key: 'isActive',
        title: wording.accounting.active,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons: [
      {
        icon: 'edit',
        description: wording.general.edit,
        name: 'edit-maintenance',
        tooltip: wording.general.edit,
        actOnEnter: true,
        actOnDoubleClick: true,
        disabled: (maintenanceSchedule: IMaintenanceSchedule) => {
          return !canPerform(Operations.UPDATE)
            || (maintenanceSchedule
              && (!maintenanceSchedule.isActive || !!maintenanceSchedule.actualEndTime));
        },
        func: (maintenanceSchedule: IMaintenanceSchedule) => actions.edit(maintenanceSchedule.id),
      },
      {
        icon: 'stop',
        description: wording.general.stop,
        name: 'stop-maintenance',
        tooltip: wording.general.stop,
        func: (maintenanceSchedule: IMaintenanceSchedule) => actions.delete(maintenanceSchedule.id),
        disabled: (maintenanceSchedule: IMaintenanceSchedule) => {
          return !canPerform(Operations.DELETE)
            || (maintenanceSchedule
              && (!maintenanceSchedule.isActive || !!maintenanceSchedule.actualEndTime));
        },
      }
    ],
    toolbarButtons: [
      {
        title: wording.general.create,
        tooltip: wording.general.create,
        icon: 'plus-circle-o',
        func: () => actions.add(),
        hidden: () => !canPerform(Operations.CREATE),
      },
    ]
  });
};
