import { UserModel } from './user-data.model';
import { LegalAgreementModel } from './legal-agreement.model';
export class AccountModel {
  token: string = '';
  newPassword: string = '';
  user: UserModel = new UserModel();
  legalAgreement: Array<object> = [new LegalAgreementModel()];
}
