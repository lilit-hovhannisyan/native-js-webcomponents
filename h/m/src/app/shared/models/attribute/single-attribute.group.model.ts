export class SingleAttributeGroupModel {
  dataTypeClass: string = '';
  displayName: string = '';
  internalName: string = '';
  // tslint:disable-next-line:no-any
  value: any;
  convertedValue: string = '';
}
