import { Component, OnInit } from '@angular/core';
import { CurrencyRepositoryService } from 'src/app/core/services/repositories/currency-repository.service';
import { ICurrency } from 'src/app/core/models/resources/ICurrency';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'nv-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.scss']
})
export class CurrencyComponent implements OnInit {

  public currencies$: Observable<ICurrency[]>;

  constructor(private currencyRepo: CurrencyRepositoryService) { }

  public ngOnInit() {
    this.currencies$ = this.currencyRepo.fetchAll({})
      .pipe(
        map(currencies => this.currencyRepo.filterActiveOnly(currencies))
      );
  }
}
