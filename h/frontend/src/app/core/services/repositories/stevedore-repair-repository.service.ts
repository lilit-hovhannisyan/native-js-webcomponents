import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from 'src/app/core/abstracts/abstract.repository';
import {
  IStevedoreRepair,
  IStevedoreRepairCreate,
  IStevedoreRepairDTO,
  StevedoreRepairKey,
} from 'src/app/core/models/resources/IStevedoreRepair';
import { RepositoryService } from 'src/app/core/services/repository.service';

@Injectable({
  providedIn: 'root',
})
export class StevedoreRepairRepositoryService extends AbstractRepository<
  IStevedoreRepair,
  IStevedoreRepairDTO,
  IStevedoreRepairCreate,
  RepoParams<IStevedoreRepair>,
  StevedoreRepairKey
> {
  protected resourceType = 'StevedoreRepair';
  public sortAsce = true;
​
  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IStevedoreRepairDTO): IStevedoreRepair => item;
  public getBaseUrl = () => '/api/stevedore-damage-repairs';

}
