import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { wording } from 'src/app/core/constants/wording/wording';
import { AutobankOperationTypeRepositoryService } from 'src/app/core/services/repositories/autobank-operation-type.repository.service';
import { FieldOperationTypeRepositoryService } from 'src/app/core/services/repositories/field-operation-type-repository.service';
import { markControlAsTouched } from 'src/app/core/helpers/form';

@Injectable({
  providedIn: 'root'
})
export class OperationTypeValidatorsService {

  constructor(
    private operationTypeRepo: AutobankOperationTypeRepositoryService,
    private fieldOperationTypeRepo: FieldOperationTypeRepositoryService,
  ) { }

  public doesExist = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    const foundRole = this.operationTypeRepo.getStreamValue()
      .find(operationType => operationType.id === control.value);

    if (foundRole) {
      return null;
    }

    markControlAsTouched(control);
    return {
      mandatorHasNoRoles: {
        message: wording.general.doesNotExist
      }
    };
  }

  public isRelatedToFieldType = (control: AbstractControl): ValidationErrors | null => {
    if (!control.value || !control.parent) {
      return null;
    }
    const fieldTypeId = control.parent.get('fieldTypeId').value;
    const foundRelation = this.fieldOperationTypeRepo.getStreamValue()
      .find(fieldOperation =>
        fieldOperation.fieldTypeId === fieldTypeId
        && fieldOperation.operationTypeId === control.value
      );

    if (foundRelation) {
      return null;
    }

    markControlAsTouched(control);
    return {
      mandatorHasNoParentOrVesselType: {
        message: wording.general.notAllowed
      }
    };
  }
}
