export enum CHARTER_DURATION_TYPE {
  RedeliveryDate,
  FixedPeriod,
  MinMaxPeriod,
  MinMaxDates,
}
