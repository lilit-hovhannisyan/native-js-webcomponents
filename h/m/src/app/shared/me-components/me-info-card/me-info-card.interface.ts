import { MeContextMenuI } from '../me-context-menu/me-context-menu-inteface';
import { MeLabelI } from '../me-label/me-label-inteface';

export interface MeInfoCardI {
  title: string;
  subtitle: string;
  description: string;
  contextMenu: MeContextMenuI;
  thumbnail?: string;
  label?: MeLabelI;
  icon?: string;
}
