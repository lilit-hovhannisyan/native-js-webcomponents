import { Type } from 'class-transformer';
import { UserAttributesModel } from './user-attributes.model';
import { UserAttributesExtendedModel } from './user-attibutes-extended.model';
export class UserModel {
  domainName: string = '';
  oid: string = '';
  numberOfGroups?: number = 0;
  isCompanyAdmin: boolean = false;
  @Type(() => UserAttributesModel)
  attributes: UserAttributesModel = new UserAttributesModel();
  @Type(() => UserAttributesExtendedModel)
  attributesExtended: UserAttributesExtendedModel = new UserAttributesExtendedModel();
}
