import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { wording } from 'src/app/core/constants/wording/wording';
import { TaskLanguageType } from 'src/app/core/models/resources/IVesselTask';
import { ITaskTemplate } from 'src/app/core/models/resources/Task-Templates/ITaskTemplate';

@Component({
  selector: 'nv-task-template-checklist',
  templateUrl: './task-template-checklist.component.html',
  styleUrls: ['./task-template-checklist.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TaskTemplateChecklistComponent implements OnInit {
  @Input() public taskTemplate: ITaskTemplate;
  public wording = wording;
  public taskLanguageType = TaskLanguageType;

  constructor() { }

  public ngOnInit(): void {  }
}
