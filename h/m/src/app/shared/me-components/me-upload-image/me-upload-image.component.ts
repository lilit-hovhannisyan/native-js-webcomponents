import { BaseLoggerComponent } from '@features/tracking-system/base-logger.component';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';

import { MeUploadImageI } from './me-upload-image.interface';
import { Injector } from '@angular/core';

@Component({
  selector: 'me-upload-image',
  templateUrl: './me-upload-image.component.html',
  styleUrls: ['./me-upload-image.component.scss']
})
export class MeUploadImageComponent extends BaseLoggerComponent implements OnInit, OnDestroy {
  componentVersion: string = '0.0.1';
  @Input() uploadImageData!: MeUploadImageI;
  @Output() returnImageFormData: EventEmitter<File> = new EventEmitter();

  fileToUpload!: File | null;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    // disable drag & drop files on page, except on element that has a css class 'droparea'
    window.addEventListener('dragover', this.disableHandleEvent, false);
    window.addEventListener('drop', this.disableHandleEvent, false);
  }

  handleFileInput(event: Event | DragEvent): void {
    if (event.type === 'change') {
      const element = event.currentTarget as HTMLInputElement;
      const files: FileList | null = element.files;

      if (files) {
        if (this.uploadImageData.thumbnail) {
          this.fileToUpload = null;
        }

        this.fileToUpload = files.item(0);

        if (this.fileToUpload) {
          this.returnImageFormData.emit(this.fileToUpload);
        }
      }
    } else if (event.type === 'drop') {
      const element = event as DragEvent;
      const files: FileList | undefined = element.dataTransfer?.files;

      if (files) {
        if (this.uploadImageData.thumbnail) {
          this.fileToUpload = null;
        }

        this.fileToUpload = files.item(0);

        if (this.fileToUpload) {
          this.returnImageFormData.emit(this.fileToUpload);
        }
      }
    }
  }

  onDropped(event: DragEvent): void {
    this.handleFileInput(event);
  }

  private disableHandleEvent(event: Event): boolean | void {
    const element = event.target as HTMLElement;
    return !element.classList.contains('droparea') && event && event.preventDefault();
  }

  ngOnDestroy(): object {
    window.removeEventListener('dragover', this.disableHandleEvent, false);
    window.removeEventListener('drop', this.disableHandleEvent, false);

    return super.ngOnDestroy();
  }
}
