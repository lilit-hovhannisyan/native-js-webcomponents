import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { BaseWebService } from '../../core/services/base.web-service';

import { CollectionModel } from '@shared/models/collection/collection.model';
import { ArrayResponseI } from '@core-interfaces/array-response.interface';
import { MaterialModel } from '@shared/models/materials/material-model';
import { BaseSearchModel } from '@shared/models/search/search-base.model';
import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { constructUrl } from '@shared/utils';
import { MeDocumentModel } from '@shared/models/document/document.model';
import { CurrentStateModel } from '@shared/models/current-state/current-state.model';

@Injectable()
export class CollectionsWebService {
  constructor(private baseWebService: BaseWebService) {}

  getMeCollections(skip?: number, top?: number): Observable<ArrayResponseI<CollectionModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/collections`, skip, top);
    return this.baseWebService.getRequestForArray<CollectionModel>(url, CollectionModel);
  }

  getMeCollectionByOid(oid: string): Observable<CollectionModel> {
    return this.baseWebService.getRequest<CollectionModel>(`${MARKETPLACE_BASE_API_URL}/collections/${encodeURI(oid)}`, CollectionModel);
  }

  deleteCollection(data: string[]): Observable<void> {
    return this.baseWebService.deleteRequest<void>(`${MARKETPLACE_BASE_API_URL}/collections`, data);
  }

  getMeCollectionMaterials(oid: string, skip?: number, top?: number): Observable<ArrayResponseI<MaterialModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/collections/${encodeURI(oid)}/materials`, skip, top);
    return this.baseWebService.getRequestForArray<CollectionModel>(url, CollectionModel);
  }

  getMeCollectionDocuments(oid: string, from?: number, to?: number): Observable<ArrayResponseI<MeDocumentModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/collections/${encodeURI(oid)}/documents`, from, to);
    return this.baseWebService.getRequestForArray<MeDocumentModel>(url, MeDocumentModel);
  }

  addMaterialsToMeCollection(oid: string, materialsOid: string[]): Observable<void> {
    return this.baseWebService.postRequest<void, string[]>(
      `${MARKETPLACE_BASE_API_URL}/collections/${encodeURI(oid)}/materials`,
      materialsOid
    );
  }

  getCollectionStates(collectionOID: string): Observable<ArrayResponseI<CurrentStateModel>> {
    return this.baseWebService.getRequestForArray<CurrentStateModel>(
      `${MARKETPLACE_BASE_API_URL}/collections/${collectionOID}/states`,
      CurrentStateModel
    );
  }

  changeCollectionState(collectionOID: string, newState: string): Observable<CollectionModel> {
    return this.baseWebService.postRequest<CollectionModel, { state: string }>(
      `${MARKETPLACE_BASE_API_URL}/collections/${collectionOID}/state`,
      { state: newState },
      CollectionModel
    );
  }

  searchCollections(data: BaseSearchModel, skip?: number, top?: number): Observable<ArrayResponseI<CollectionModel>> {
    const url: string = constructUrl(`${MARKETPLACE_BASE_API_URL}/collections/search`, skip, top);
    return this.baseWebService.postRequestForArray<CollectionModel, BaseSearchModel>(url, data, CollectionModel);
  }

  removeMaterialsFromCollection(collectionOID: string, materialsOIDs: string[]): Observable<string> {
    return this.baseWebService.deleteRequest<string>(`${MARKETPLACE_BASE_API_URL}/collections/${collectionOID}/materials`, materialsOIDs);
  }

  removeDocuments(collectionOid: string, documentOids: string[]): Observable<boolean> {
    return this.baseWebService.deleteRequest(`${MARKETPLACE_BASE_API_URL}/collections/${collectionOid}/documents`, documentOids);
  }
}
