import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { url } from '../../../../../../core/constants/sitemap/sitemap-entry';
import { sitemap, SitemapNode } from '../../../../../../core/constants/sitemap/sitemap';
import { ActivatedRoute, Router} from '@angular/router';
import { CompaniesHelperService } from '../../companies/detail-view/companies.helper.service';
import { Observable, of, ReplaySubject } from 'rxjs';
import { CompanyKey } from '../../../../../../core/models/resources/ICompany';
import { wording } from '../../../../../../core/constants/wording/wording';
import { SettingsService } from '../../../../../../core/services/settings.service';
import { IShareholding, ShareholdingsMainService } from '../../../../../../core/services/mainServices/shareholdings-main.service';
import { ShareholdingsHelperService } from '../shareholdings.helper.service';
import { takeUntil } from 'rxjs/operators';
import { ContributionMainService } from '../../../../../../core/services/mainServices/company-contributions-main.service';
import { AmountChangeType, ICompanyPartnerContribution, ICompanyPartnerContributionFull } from '../../../../../../core/models/resources/ICompanyPartnerContribution';
import { setTranslationSubjects } from '../../../../../../shared/helpers/general';
import { CAPITAL_TYPE, COMPANY_LEGAL_FORM, getContributionWordingKey, legalEntityExclamationTooltipContributions } from '../../../../../../core/models/enums/company-legal-form';
import { legalEntitySiteMap } from '../../../../../../core/constants/sitemap/system';
import { IWording } from '../../../../../../core/models/resources/IWording';

@Component({
  selector: 'nv-shareholdings-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShareholdingsDetailViewComponent implements OnInit, OnDestroy {
  private componentDestroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  public title: string;
  public sitemap = sitemap;
  public hasContributions: boolean;
  private hasContributionDeviation: boolean;
  private shareholding: IShareholding;

  public isComponentAccesible: boolean;

  public get isCreationMode() {
    return this.activatedRoute.snapshot.params.id === 'new';
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public companiesHelperService: CompaniesHelperService,
    public settingsService: SettingsService,
    private shareholdingsMainService: ShareholdingsMainService,
    private cdr: ChangeDetectorRef,
    private shareholdingsHelperService: ShareholdingsHelperService,
    private contributionsMainService: ContributionMainService,
  ) {}

  public ngOnInit(): void {
    if (this.isCreationMode) {
      this.title =  wording.general.create[this.settingsService.language];
      this.shareholdingsHelperService.updateModeState(true);
      this.isComponentAccesible = true;
    } else {
      this.shareholdingsHelperService.updateModeState(false);
      this.init();
    }

    this.shareholdingsHelperService.isInCreationMode$
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe(mode => {
        if (!mode) {
          this.init();
        }
      });
  }

  private init(): void {
    this.isComponentAccesible = false;
    const shareholdingId: CompanyKey = +this.activatedRoute.snapshot.params.id;

    this.shareholdingsMainService.getShareholdingById(shareholdingId).subscribe(shareholding => {
      if (shareholding) {
        this.shareholding = shareholding;
        this.shareholdingsHelperService.setCompany(shareholding);
        this.title = shareholding.companyName;
        this.hasContributions = !shareholding.contributions.length;

        // check contribution deviation
        const filteredContributions: ICompanyPartnerContribution[] = this.contributionsMainService
          .filterContributions(shareholding.contributions, new Date().toDateString());
        const latestContributions: Map<CompanyKey, ICompanyPartnerContribution> = this.contributionsMainService
          .getCompaniesLatestContributions(filteredContributions);

        const resultData = Array.from(latestContributions.values())
          .filter(contribution => {
            return (
              !!contribution.contributionAmount
              || (
                contribution.contributionIsZero
                && contribution.amountChangeType === AmountChangeType.Increase
              )
            );
          }) as ICompanyPartnerContributionFull[];

        const totalContributionAmount = this.contributionsMainService.getTotalContributionAmount(resultData);

        this.hasContributionDeviation = this.isAgCompanyWithNonParValue(shareholding)
          ? shareholding.legalEntity?.shareCapital !== shareholding.legalEntity?.sumOfNonParValueShares
          : totalContributionAmount !== shareholding.shareCapitalOption;

        if (shareholding.shareCapitalOption === undefined && !resultData.length) {
          this.hasContributionDeviation = false;
        }
        this.isComponentAccesible = true;
        this.cdr.markForCheck();
      }
    });
  }

  private isAgCompanyWithNonParValue(shareholding: IShareholding): boolean {
    return shareholding.legalForm === COMPANY_LEGAL_FORM.StockCorporation
      && shareholding.legalEntity.capitalType === CAPITAL_TYPE.NonParValueShare;
  }

  public routeBack = () => {
    this.router.navigate([
      url(sitemap.system.children.misc.children.shareholdings)
    ]);
  }

  public tabDisabler = (node?: SitemapNode) => {
    const basicNode: SitemapNode = sitemap.system.children.misc.children.shareholdings.children.id.children.legalEntity;
    const trustorsNode: SitemapNode =
      sitemap.system.children.misc.children.shareholdings.children.id.children.trustors;
    const subParticipantsNode: SitemapNode =
      sitemap.system.children.misc.children.shareholdings.children.id.children.subParticipants;

    if (this.isCreationMode) {
      return node === basicNode;
    } else if (!this.isCreationMode && this.hasContributions) {
      return node !== trustorsNode && node !== subParticipantsNode;
    } else {
      return true;
    }
  }

  public isDataMissing = (node: SitemapNode): Observable<boolean> => {
    const basePath = sitemap.system.children.misc.children.shareholdings.children.id.children;
    if (!this.isCreationMode &&
        (node.path === basePath.subParticipants.path || node.path === basePath.trustors.path)
    ) {
      return of(this.hasContributions);
    } else if (!this.isCreationMode && node.path === basePath.legalEntity.path) {
      if (this.shareholding) {
        const contributionTypeName: IWording = legalEntityExclamationTooltipContributions
          .get(getContributionWordingKey(
            this.shareholding.legalForm,
            this.shareholding.legalEntity?.capitalType)
          );

        const legalEntityExclamationTooltip: IWording = setTranslationSubjects(
          wording.system.contributionDeviationDetected,
          { contributionTypeName },
        );

        legalEntitySiteMap.legalEntity['notAllowedTabMessage'] = legalEntityExclamationTooltip;

        return of(this.hasContributionDeviation);
      }
    } else {
      return of(false);
    }
  }

  public ngOnDestroy() {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
    this.shareholdingsHelperService.setCompany(null);
  }
}
