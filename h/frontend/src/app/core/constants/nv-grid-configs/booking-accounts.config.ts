import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl, NvToolbarButton, NvButton } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';
import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { Operations } from 'src/app/core/models/Operations';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { IBookingAccount, IBookingAccountRow } from '../../models/resources/IBookingAccount';

export const getGridConfig = (
  actions: IGridConfigActions,
  isInDebiroCreditorRange: (bookingAccount: IBookingAccount) => boolean,
  canPerform?: (Operations) => boolean,
): NvGridConfig => {
  const buttons: NvButton[] = [];
  const toolbarButtons: NvToolbarButton[] = [];
  const contextMenuButtons: NvButton[] = [];

  if (canPerform && canPerform(Operations.READ)) {
    contextMenuButtons.push(...getContextMenuButtonsConfig());
  }
  if (actions.clone && canPerform && canPerform(Operations.CREATE)) {
    contextMenuButtons.push({
      icon: 'copy',
      description: wording.general.useAsTemplate,
      func: actions.clone,
      hidden: (bookingAccount: IBookingAccount) => isInDebiroCreditorRange(bookingAccount)
    });
  }
  if (actions.edit) {
    buttons.push({
      icon: 'edit',
      description: wording.general.edit,
      name: 'editBookingAccount',
      tooltip: wording.general.edit,
      actOnEnter: true,
      actOnDoubleClick: true,
      hidden: canPerform ? !canPerform(Operations.UPDATE) : true,
      func: (bookingAccount: IBookingAccount) => {
        actions.edit(bookingAccount.id);
      },
    });
  }
  if (actions.delete) {
    buttons.push({
      icon: 'delete',
      description: wording.general.delete,
      name: 'deleteBookingAccount',
      tooltip: wording.general.delete,
      actOnDoubleClick: false,
      hidden: canPerform ? !canPerform(Operations.DELETE) : true,
      func: (bookingAccount: IBookingAccount) => {
        actions.delete(bookingAccount);
      },
    });
  }
  if (actions.select) {
    buttons.push({
      actOnDoubleClick: true,
      func: (bookingAccount: IBookingAccountRow) => actions.select(bookingAccount),
      description: wording.autocomplete.selectRow,
      icon: 'select',
      actOnEnter: true,
      tooltip: wording.general.select,
      name: 'select'
    });
  }

  if (actions.add) {
    toolbarButtons.push({
      title: wording.general.create,
      tooltip: wording.general.create,
      icon: 'plus-circle-o',
      func: () => actions.add(),
      hidden: canPerform ? () => !canPerform(Operations.CREATE) : () => true
    });
  }

  return ({
    gridName: 'bookingAccountsGrid',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    sortBy: 'no',
    isSortAscending: true,
    clearFiltersButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 10
    },
    columns: [
      {
        key: 'no',
        title: wording.accounting.no,
        width: 90,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'displayLabel',
        title: wording.accounting.accountName,
        width: 340,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'currencyCode',
        title: wording.accounting.restrictedCurrency,
        width: 200,
        isSortable: true,
        filter: {
          values: []
        }
      },
      {
        key: 'isCostAccounting',
        title: wording.accounting.costAccounting,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isOpenItem',
        title: wording.accounting.openItem,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isOpex',
        title: wording.accounting.opex,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'assetAccount',
        title: wording.accounting.assetAccount,
        width: 150,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
      {
        key: 'isActive',
        title: wording.accounting.active,
        width: 80,
        isSortable: true,
        filter: {
          controlType: NvFilterControl.Boolean,
          values: []
        },
        dataType: NvColumnDataType.Boolean
      },
    ],
    buttons,
    toolbarButtons,
    contextMenuButtons,
  });
};
