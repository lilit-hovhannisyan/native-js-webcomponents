import { Component, OnInit } from '@angular/core';
import { CompanyKey } from '../../../../../../../../core/models/resources/ICompany';
import { ActivatedRoute } from '@angular/router';
import { COMPANY_CONTRIBUTION_TYPES } from '../../../../../../shared/trustee-partner-contributions/enums/company-contributions';

@Component({
  selector: 'nv-sub-participants',
  templateUrl: './sub-participants.component.html',
  styleUrls: ['./sub-participants.component.scss']
})
export class SubParticipantsComponent implements OnInit {
  public contributorType: COMPANY_CONTRIBUTION_TYPES = COMPANY_CONTRIBUTION_TYPES.SubParticipantContribution;
  public companyId: CompanyKey;

  constructor(
    private activatedRoute: ActivatedRoute,
  ) { }

  public ngOnInit(): void {
    this.companyId = +this.activatedRoute.parent.parent.snapshot.params.id;
  }

}
