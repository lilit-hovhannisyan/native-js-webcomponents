import { Injectable, TemplateRef } from '@angular/core';
import { IMasterStructureNode, IMasterStructureNodeTemplate } from 'src/app/core/models/resources/IMasterStructureNode';
import { wording } from 'src/app/core/constants/wording/wording';
import { getLabelKeyByLanguage } from 'src/app/core/helpers/general';
import { Language } from 'src/app/core/models/language';
import * as jsPDF from 'jspdf';
import { StructureRepositoryService } from 'src/app/core/services/repositories/structure-repository.service';
import { IMasterStructure, StructureKey } from 'src/app/core/models/resources/IMasterStructure';
import { v4 as uuidv4 } from 'uuid';
import { MasterStructureNodeHelperService } from '../master-structure-nodes/services/master-structure-node-helper.service';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';

@Injectable({
  providedIn: 'root'
})
export class MasterStructureExportService {
  private language: Language;
  private downloadModalRef: NzModalRef;

  constructor(
    private nodesHelperService: MasterStructureNodeHelperService,
    private nzModalService: NzModalService,
    private structureRepo: StructureRepositoryService,
  ) { }

  public setLanguage(language: Language): void {
    this.language = language;
  }

  public download(template: TemplateRef<any>, structure: IMasterStructure): void {
    this.downloadModalRef = this.nzModalService.create({
      nzTitle: structure.isActive
        ? wording.accounting.downloadWithAssignedAccounts[this.language]
        : wording.accounting.structureIsUnderDevelopment[this.language],
      nzContent: template,
      nzFooter: null,
      nzClosable: false,
    });
  }

  public closeDownloadModal() {
    this.downloadModalRef.close();
  }

  private downloadExcel(
    structureId: StructureKey,
    exportWithAccounts: boolean,
  ): void {
    this.structureRepo.downloadExcel(
      structureId,
      this.language,
      exportWithAccounts,
    ).subscribe(() => {
      this.closeDownloadModal();
    });
  }

  private downloadPdf(
    structure: IMasterStructure,
    exportWithAccounts: boolean,
    nodes: IMasterStructureNodeTemplate[],
  ): void {
    const doc: jsPDF = new jsPDF('l');
    let row = 20;
    const column = 10;
    const spaces = '        ';

    doc.setFontSize(16);
    doc.setFontType('bold');
    doc.text(column, row, structure.name);
    doc.setFontSize(12);

    let level = 0;
    const levels: { [key: number]: number } = {};
    const label: string = getLabelKeyByLanguage(this.language);

    this.nodesHelperService.mapNodes(
      nodes,
      (node: IMasterStructureNode) => {
        if (node.bookingAccountId && !exportWithAccounts) {
          return;
        }

        this.setPdfFontType(node, doc);

        if ((row + 30) >= doc.internal.pageSize.height) {
          doc.addPage('l');
          row = 20;
        } else {
          row += 10;
        }

        if (node.level === 1) {
          level = 0;
        }

        if (node.childNodes.length > 0) {
          if (levels[node.parentNodeId]) {
            levels[node.id] = levels[node.parentNodeId] + 1;
          } else {
            levels[node.id] = levels[node.id] || ++level;
          }
        }

        if (node.parentNodeId && node.level !== 1) {
          let text =  '';

          for (let i = 0; i <= levels[node.parentNodeId]; i++) {
            text += spaces;
          }
          text += node[label];

          if (text.length > 85) {
            row = this
              .splitLongTextAndGetRowSize(node, doc, text, row, column, spaces, levels);
          } else {
            doc.text(column, row, text);
          }
        } else {
          doc.text(column, row, `${spaces}${node[label]}`);
        }
      }
    );
    doc.save('master-structures.pdf');
    this.closeDownloadModal();
  }

  private setPdfFontType(node: IMasterStructureNode, doc: jsPDF): void {
    if (node.isHighlighted) {
      doc.setFontType('bold');
    } else if (node.bookingAccountId) {
      doc.setFontType('italic');
    } else {
      doc.setFontType('normal');
    }
  }

  private splitLongTextAndGetRowSize(
    node: IMasterStructureNode,
    doc: jsPDF,
    text: string,
    rawRow: number,
    column: number,
    spaces: string,
    levels: { [key: number]: number },
  ): number {
    let row: number = rawRow;
    const docText: string[] = doc.splitTextToSize(text, 100);

    docText.forEach((line, key) => {
      if ((row + 30) >= doc.internal.pageSize.height) {
        doc.addPage('l');
        row = 20;
      }

      let lineText: string = line;

      if (key === 0) {
        doc.text(column, row, lineText);
        row += 10;
        return row;
      }

      for (let i = 0; i <= levels[node.parentNodeId]; i++) {
        lineText = `${spaces}${spaces}${lineText}`;
      }

      doc.text(column, row, lineText);

      if (key !== docText.length - 1) {
        row += 10;
      }
    });

    return row;
  }

  public startDownloading(
    structure: IMasterStructure,
    nodes: IMasterStructureNodeTemplate[],
    exportWithAccounts: boolean,
    downloadAs: 'excel' | 'pdf',
  ): void {
    if (downloadAs === 'excel') {
      this.downloadExcel(structure.id, exportWithAccounts);
    } else {
      this.downloadPdf(structure, exportWithAccounts, nodes);
    }
  }

  public copyStructure(
    currentStructure: IMasterStructure,
    currentNodes: IMasterStructureNodeTemplate[],
  ): IMasterStructure {
    const oldNodeNewIds: { [key: string]: string } = {};

    const nodes: IMasterStructureNodeTemplate[] = this.nodesHelperService.mapNodes(
      this.nodesHelperService.copyNodes(currentNodes),
      (node: IMasterStructureNode) => {
        const newId: string = uuidv4();
        node.masterStructureId = 0;
        oldNodeNewIds[node.id] = newId;
        node.id = newId;
      }
    );

    this.nodesHelperService.mapNodes(
      nodes,
      (node: IMasterStructureNode) => {
        node.parentNodeId = oldNodeNewIds[node.parentNodeId] || node.parentNodeId;
        if (node.switchingNodeId) {
          node.switchingNodeId = oldNodeNewIds[node.switchingNodeId];
        }
      }
    );

    const payload: IMasterStructure = {
      ...currentStructure,
      nodes: nodes,
      id: 0,
      name: `COPY_${currentStructure.name}`
    };

    return payload;
  }
}
