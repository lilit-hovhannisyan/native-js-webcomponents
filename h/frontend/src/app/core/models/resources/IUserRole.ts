import { IResource } from './IResource';
import { UserKey } from './IUser';
import { RoleKey } from './IRole';

export type UserRoleKey = number;

interface ISharedProps {
  userId: UserKey;
  roleId: RoleKey;
}

export interface IUserRoleBase extends ISharedProps, IResource<UserRoleKey> { }

export interface IUserRoleCreate extends ISharedProps { }

export interface IUserRoleDTO extends IUserRoleBase { }

export interface IUserRole extends IUserRoleDTO { }
