import { getContextMenuButtonsConfig } from 'src/app/core/helpers/general';
import { IGridConfigActions } from 'src/app/core/models/misc/gridConfigActions';
import { IVoucherType } from 'src/app/core/models/resources/IVoucherType';
import { NvGridButtonsPosition, NvGridConfig, NvColumnDataType, NvFilterControl } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getVesselTaskTemplateGridConfig = (): NvGridConfig => {
  return ({
    gridName: 'vesselTaskTemplateConfig',
    rowButtonsPosition: NvGridButtonsPosition.ExpandedRight,
    hideRefreshButton: true,
    paging: {
      pageNumber: 1,
      pageSize: 100
    },
    clearFiltersButton: true,
    columns: [
      {
        key: 'displayLabel',
        title: wording.general.name,
        width: 350,
        isSortable: true,
        filter: {
          values: []
        }
      },
    ],
  });
};
