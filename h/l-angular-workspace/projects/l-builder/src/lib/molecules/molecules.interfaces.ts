import {Observable} from 'rxjs';

export class UserApi {
    login: (data: any, rememberMe?: boolean)  => Observable<any>;
    // register
    // signOut
}

export class LavashPpopupInterface {
    openPopup: () => Observable<any>;
}


export interface Field {
    component: 'input' | 'textarea' |  'datepicker' | 'file';
    name: string; // identifier
    label?: string;
    descriptors: any;
    validationRules?: any; // todo refer to interface
    errorMessage?: string;
    hasError?: boolean;
}

export interface NavigationItem {
    icon?: string;
    text?: string;
    href: string;
}



