# Navido

## Architecture

![](docs/architecture.png)

## Docker

To use the same setup as the staging server, you can create a docker image using
the dockerfile provided in the project

    docker build --pull --build-arg APP_ENVIRONMENT=staging --tag staging --file etc/docker/main/Dockerfile .

Use this image as follows:

    docker run -p 4200:80 staging

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running end-to-end tests

Please visit our [navido e2e project](https://gitlab.com/navido-hamburg/navido/e2e-tests).
