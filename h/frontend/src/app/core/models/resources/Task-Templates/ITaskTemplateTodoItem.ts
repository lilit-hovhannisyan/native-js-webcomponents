import { IResource } from '../IResource';
import { IWithLabels } from '../IWithLabels';
import { TaskTemplateKey } from './ITaskTemplate';

export type TaskTemplateTodoItemKey = number;

interface ISharedProps extends IWithLabels {
  /**
   * extends
   * labelDe: string;
   * labelEn: string;
   */
  taskTemplateId: TaskTemplateKey;
}

export interface ITaskTemplateTodoItemCreate extends ISharedProps { }

export interface ITaskTemplateTodoItemBase extends ISharedProps, IResource<TaskTemplateTodoItemKey> { }

export interface ITaskTemplateTodoItemDTO extends ITaskTemplateTodoItemBase { }

export interface ITaskTemplateTodoItem extends ITaskTemplateTodoItemDTO { }
