import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { EntityI } from '@core-interfaces/entity.interface';
import { BaseWebService } from '@core-services/base.web-service';

import { MARKETPLACE_BASE_API_URL } from '@shared/constants';
import { MeDocumentModel } from '@shared/models/document/document.model';

@Injectable()
export class DocumentCreateWebService {
  constructor(private baseWebService: BaseWebService) {}

  createDocument(data: EntityI): Observable<MeDocumentModel> {
    return this.baseWebService.postRequest<MeDocumentModel, EntityI>(`${MARKETPLACE_BASE_API_URL}/documents`, data, MeDocumentModel);
  }

  getDocumentByOid(documentOID: string): Observable<MeDocumentModel> {
    return this.baseWebService.getRequest<MeDocumentModel>(`${MARKETPLACE_BASE_API_URL}/documents/${documentOID}`, MeDocumentModel);
  }

  updateDocument(documentOID: string, data: EntityI): Observable<MeDocumentModel> {
    return this.baseWebService.putRequest<MeDocumentModel, EntityI>(
      `${MARKETPLACE_BASE_API_URL}/documents/${documentOID}`,
      data,
      MeDocumentModel
    );
  }

  changeDocumentType(documentOID: string, data: EntityI): Observable<MeDocumentModel> {
    return this.baseWebService.postRequest<MeDocumentModel, EntityI>(
      `${MARKETPLACE_BASE_API_URL}/documents/${documentOID}/change-domain`,
      data,
      MeDocumentModel
    );
  }
}
