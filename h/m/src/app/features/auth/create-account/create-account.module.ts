import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateAccountRoutingModule } from './create-account-routing.module';
import { CreateAccountComponent } from './create-account.component';
import { SharedModule } from '@shared/shared.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MePopupAccountModule } from '@shared/me-components/me-popup-account/me-popup-account.module';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MePopupAccountService } from '@shared/me-components/me-popup-account/me-popup-account.service';
import { I18nModule } from '@shared/modules/i18n.module';

@NgModule({
  declarations: [CreateAccountComponent],
  imports: [
    CommonModule,
    CreateAccountRoutingModule,
    SharedModule,
    IconsModule,
    MatTooltipModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MePopupAccountModule,
    ReactiveFormsModule,
    MatButtonModule,
    I18nModule
  ],
  providers: [MePopupAccountService]
})
export class CreateAccountModule {}
