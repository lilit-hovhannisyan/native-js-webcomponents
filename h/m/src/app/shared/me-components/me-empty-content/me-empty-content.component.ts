import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MeButtonI } from '../me-button/me-button.interface';
import { MeEmptyContentI } from './me-empty-content-interface';

@Component({
  selector: 'me-empty-content',
  templateUrl: './me-empty-content.component.html',
  styleUrls: ['./me-empty-content.component.scss']
})
export class MeEmptyContentComponent {
  @Input() dataModel!: MeEmptyContentI;
  @Output() clicked: EventEmitter<void> = new EventEmitter();

  addButton: MeButtonI = {
    color: 'secondary',
    type: 'regular'
  };

  constructor() {}

  clickEventHandeler(): void {
    this.clicked.emit();
  }
}
