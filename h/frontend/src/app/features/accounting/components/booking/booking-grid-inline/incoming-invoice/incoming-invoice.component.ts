import { Component, OnInit, Input, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, AbstractControl } from '@angular/forms';
import { Validators } from 'src/app/core/classes/validators';
import { wording } from 'src/app/core/constants/wording/wording';
import { MandatorKey, IMandator } from 'src/app/core/models/resources/IMandator';
import { tap, takeUntil, take, map } from 'rxjs/operators';
import { MandatorRepositoryService } from 'src/app/core/services/repositories/mandator-repository.service';
import { ReplaySubject, forkJoin } from 'rxjs';
import { InvoiceLedgerRepositoryService } from 'src/app/core/services/repositories/invoice-ledger-repository.service';
import { IInvoiceLedgerRow, IInvoiceLedger } from 'src/app/core/models/resources/IInvoiceLedger';
import { InvoiceLedgerMainService } from 'src/app/core/services/mainServices/invoice-ledger-main.service';
import { NotificationsService } from 'src/app/core/services/notification.service';
import { NotificationType } from 'src/app/core/models/INotification';
import { SettingsService } from 'src/app/core/services/settings.service';
import { Wording } from 'src/app/core/classes/wording';

@Component({
  selector: 'nv-incoming-invoice',
  templateUrl: './incoming-invoice.component.html',
  styleUrls: ['./incoming-invoice.component.scss']
})
export class IncomingInvoiceComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public wording = wording;
  @Input() public closeModal: (value?: IInvoiceLedgerRow) => void;
  @Input() public mandatorId?: MandatorKey;

  private componentDestroyed$ = new ReplaySubject<boolean>(1);
  private invoiceLedgers: IInvoiceLedger[] = [];

  constructor(
    private fb: FormBuilder,
    private mandatorRepo: MandatorRepositoryService,
    private cdr: ChangeDetectorRef,
    private invoiceLedgerRepo: InvoiceLedgerRepositoryService,
    private invoicLedgerService: InvoiceLedgerMainService,
    private notificationService: NotificationsService,
    private settingsService: SettingsService,
  ) { }

  public ngOnInit(): void {
    forkJoin([
      this.invoiceLedgerRepo.fetchAll({}),
      this.mandatorRepo.fetchAll({}),
    ]).subscribe(([invoiceLedgers]) => {
      this.invoiceLedgers = invoiceLedgers;
      this.initForm();
    });
  }

  private initForm(): void {
    this.form = this.fb.group({
      barcode: [''],
      mandatorId: [this.mandatorId, [Validators.required]],
      voucherType: [{ value: null, disabled: true }, Validators.positiveNumber],
      invoiceLedgerId: [{ value: null, disabled: true }, [Validators.required, Validators.positiveNumber]],
      mandatorNo: [{ value: null, disabled: true }]
    });
    const mandatorIdCtrl: AbstractControl = this.form.get('mandatorId');

    mandatorIdCtrl.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed$),
        tap(() => this.onMandatorChange())
      ).subscribe();
    this.onMandatorChange();
    this.cdr.detectChanges();
  }

  private onMandatorChange() {
    const mandatorId: MandatorKey = this.form.get('mandatorId').value;
    const voucherTypeCtrl: AbstractControl = this.form.get('voucherType');
    const invoiceLedgerIdCtrl: AbstractControl = this.form.get('invoiceLedgerId');
    const mandatorNoCtrl: AbstractControl = this.form.get('mandatorNo');
    const barcodeCtrl: AbstractControl = this.form.get('barcode');

    barcodeCtrl.patchValue(null);
    if (!mandatorId) {
      voucherTypeCtrl.patchValue(null);
      invoiceLedgerIdCtrl.patchValue(null);
      mandatorNoCtrl.patchValue(null);
      invoiceLedgerIdCtrl.disable();
      return;
    }
    invoiceLedgerIdCtrl.enable();

    const mandator: IMandator = this.mandatorRepo.getStreamValue()
      .find(m => m.id === mandatorId);

    mandatorNoCtrl.patchValue(mandator.no.toString().padStart(4, '0'));
    voucherTypeCtrl.patchValue('REE');
  }

  public save() {
    const { value } = this.form;
    this.invoicLedgerService.getStream({ useCache: true })
      .pipe(
        takeUntil(this.componentDestroyed$),
        take(1),
        map(invoices => {
          return this.invoicLedgerService
            .toRow(invoices.find(i => i.id === value.invoiceLedgerId));
        })
      )
      .subscribe(invoice => this.closeModal(invoice));
  }


  public ngOnDestroy(): void {
    this.componentDestroyed$.next(true);
    this.componentDestroyed$.complete();
  }

  public onBarcodeEnter() {
    const barcodeCtrl: AbstractControl = this.form.get('barcode');
    const barcode: string = barcodeCtrl.value;
    const invoice: IInvoiceLedger = this.invoiceLedgers.find(i => {
      return i.barcode === barcode && !i.booked;
    });
    if (invoice) {
      this.form.patchValue({
        barcode: invoice.barcode,
        mandatorId: invoice.mandatorId,
        invoiceLedgerId: invoice.id,
      });
      barcodeCtrl.patchValue(barcode);
    } else {
      this.notificationService.notify(NotificationType.Warning,
        wording.general.warning,
        Wording.replace(
          wording.accounting.barcodeNotFound,
          '{}',
          { en: barcodeCtrl.value, de: barcodeCtrl.value }
        )
      );
      barcodeCtrl.patchValue('');
    }
  }

}
