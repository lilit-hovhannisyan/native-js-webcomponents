import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';

import { MeAttributeModule } from '@shared/me-components/me-attribute/me-attribute.module';
import { MeButtonModule } from '@shared/me-components/me-button/me-button.module';
import { MeUploadImageModule } from '@shared/me-components/me-upload-image/me-upload-image.module';
import { MeComponentLoadingModule } from '@shared/me-directives/me-component-loader/me-component-loader.module';
import { IconsModule } from '@shared/modules/icons.module';
import { MeEmptyContentModule } from '@shared/me-components/me-empty-content/me-empty-content.module';
import { MeDocumentCardModule } from '@shared/me-components/me-document-card/me-document-card.module';
import { MeMaterialCardModule } from '@shared/me-components/me-material-card/me-material-card.module';
import { MeSearchBarModule } from '@shared/me-components/me-search-bar/me-search-bar.module';
import { MeIconsProviderModule } from '@shared/modules/me-icons.module';
import { MeScrollToBottomModule } from '@shared/me-directives/me-scroll-to-bottom.module';
import { MePopupVideoModule } from '@shared/me-components/me-popup-video/me-popup-video.module';

import { CollectionCreateEditRoutingModule } from './collection-create-edit-routing.module';
import { CollectionCreateEditComponent } from './collection-create-edit.component';

@NgModule({
  declarations: [CollectionCreateEditComponent],
  imports: [
    CommonModule,
    CollectionCreateEditRoutingModule,
    MeButtonModule,
    MeAttributeModule,
    MatStepperModule,
    IconsModule,
    MeUploadImageModule,
    MeComponentLoadingModule,
    ReactiveFormsModule,
    MeEmptyContentModule,
    MeDocumentCardModule,
    MeMaterialCardModule,
    MeSearchBarModule,
    MeIconsProviderModule,
    MeScrollToBottomModule,
    MePopupVideoModule
  ]
})
export class CollectionCreateEditModule {}
