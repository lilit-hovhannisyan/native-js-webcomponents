import { IResource } from './IResource';
import { IWithLabels } from './IWithLabels';
import { IWithLabel } from './IWithLabel';


export type AutobankFieldTypeKey = number;

interface ISharedProps extends IWithLabels { }

export interface IAutobankFieldTypeBase extends ISharedProps, IResource<AutobankFieldTypeKey> { }

export interface IAutobankFieldTypeCreate extends ISharedProps { }

export interface IAutobankFieldTypeDTO extends IAutobankFieldTypeBase { }

export interface IAutobankFieldType extends IAutobankFieldTypeDTO, IWithLabel { }

