import { NvGridConfig, NvColumnDataType } from 'nv-grid';
import { wording } from 'src/app/core/constants/wording/wording';

export const getGridConfig = (): NvGridConfig => {
  return ({
    gridName: 'VesselBasicAccountingsGrid',
    title: wording.general.history,
    hideRefreshButton: true,
    showPaging: false,
    showFooter: false,
    sortBy: 'from',
    hideAllFilters: true,
    hideToolbar: true,
    hideSettingsButton: true,
    disableDragAndDrop: true,
    disableHideColumns: true,
    disableSortColumns: false,
    columns: [
      {
        key: 'vesselNo',
        title: wording.general.number,
        width: 180,
        isSortable: true,
        resizeable: false,
      },
      {
        key: 'vesselName',
        title: wording.general.name,
        width: 180,
        isSortable: true,
        resizeable: false,
      },
      {
        key: 'from',
        title: wording.general.from,
        dataType: NvColumnDataType.Date,
        width: 180,
        isSortable: true,
        resizeable: false,
      },
    ],
  });
};
