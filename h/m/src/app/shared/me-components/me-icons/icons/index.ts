export { Activity } from './svg/activity';
export { Airplay } from './svg/airplay';
export { AlertCircle } from './svg/alert-circle';
export { AlertOctagon } from './svg/alert-octagon';
export { AlertTriangle } from './svg/alert-triangle';
export { AlignCenter } from './svg/align-center';
export { AlignJustify } from './svg/align-justify';
export { AlignLeft } from './svg/align-left';
export { AlignRight } from './svg/align-right';
export { Anchor } from './svg/anchor';
export { Aperture } from './svg/aperture';
export { ArrowDownLeft } from './svg/arrow-down-left';
export { ArrowDownRight } from './svg/arrow-down-right';
export { ArrowDown } from './svg/arrow-down';
export { ArrowLeft } from './svg/arrow-left';
export { ArrowRight } from './svg/arrow-right';
export { ArrowUpLeft } from './svg/arrow-up-left';
export { ArrowUpRight } from './svg/arrow-up-right';
export { ArrowUp } from './svg/arrow-up';
export { AtSign } from './svg/at-sign';
export { Award } from './svg/award';
export { BarChart2 } from './svg/bar-chart-2';
export { BarChart } from './svg/bar-chart';
export { BatteryCharging } from './svg/battery-charging';
export { Battery } from './svg/battery';
export { BellOff } from './svg/bell-off';
export { Bell } from './svg/bell';
export { Bluetooth } from './svg/bluetooth';
export { Bold } from './svg/bold';
export { Book } from './svg/book';
export { Bookmark } from './svg/bookmark';
export { Box } from './svg/box';
export { Briefcase } from './svg/briefcase';
export { BusinessInformation } from './svg/business-information';
export { Button } from './svg/button';
export { Calendar } from './svg/calendar';
export { CameraOff } from './svg/camera-off';
export { Camera } from './svg/camera';
export { Cast } from './svg/cast';
export { Certificates } from './svg/certificates';
export { CheckCircle } from './svg/check-circle';
export { CheckSquare } from './svg/check-square';
export { Check } from './svg/check';
export { ChemicalsMaterialType } from './svg/chemicals-material-type';
export { ChevronDown } from './svg/chevron-down';
export { ChevronLeft } from './svg/chevron-left';
export { ChevronRight } from './svg/chevron-right';
export { ChevronUp } from './svg/chevron-up';
export { ChevronsDown } from './svg/chevrons-down';
export { ChevronsLeft } from './svg/chevrons-left';
export { ChevronsRight } from './svg/chevrons-right';
export { ChevronsUp } from './svg/chevrons-up';
export { Chrome } from './svg/chrome';
export { Circle } from './svg/circle';
export { Clipboard } from './svg/clipboard';
export { Clock } from './svg/clock';
export { CloudDrizzle } from './svg/cloud-drizzle';
export { CloudLightning } from './svg/cloud-lightning';
export { CloudOff } from './svg/cloud-off';
export { CloudRain } from './svg/cloud-rain';
export { CloudSnow } from './svg/cloud-snow';
export { Cloud } from './svg/cloud';
export { Codepen } from './svg/codepen';
export { Command } from './svg/command';
export { Community } from './svg/community';
export { CompanyIntroduction } from './svg/company-introduction';
export { Compass } from './svg/compass';
export { ComponentsMaterialType } from './svg/components-material-type';
export { ContactInformation } from './svg/contact-information';
export { Copy } from './svg/copy';
export { CopyFilled } from './svg/copy-filled';
export { CornerDownLeft } from './svg/corner-down-left';
export { CornerDownRight } from './svg/corner-down-right';
export { CornerLeftDown } from './svg/corner-left-down';
export { CornerLeftUp } from './svg/corner-left-up';
export { CornerRightDown } from './svg/corner-right-down';
export { CornerRightUp } from './svg/corner-right-up';
export { CornerUpLeft } from './svg/corner-up-left';
export { CornerUpRight } from './svg/corner-up-right';
export { Cpu } from './svg/cpu';
export { CreditCard } from './svg/credit-card';
export { Crop } from './svg/crop';
export { Crosshair } from './svg/crosshair';
export { Delete } from './svg/delete';
export { Disc } from './svg/disc';
export { DownAndFeatherMaterialType } from './svg/down-and-feather-material-type';
export { DenimMaterialType } from './svg/denim-material-type';
export { DigitalProfile } from './svg/digital-profile';
export { DownloadCloud } from './svg/download-cloud';
export { Download } from './svg/download';
export { Droplet } from './svg/droplet';
export { Edit2 } from './svg/edit-2';
export { Edit3 } from './svg/edit-3';
export { Edit } from './svg/edit';
export { Employees } from './svg/employees';
export { EmptyCertificates } from './svg/empty-certificates';
export { EmptyColleagues } from './svg/empty-colleagues';
export { EmptyCollections } from './svg/empty-collections';
export { EmptyDocuments } from './svg/empty-documents';
export { EmptyFavouriteSuppliers } from './svg/empty-favourite-suppliers';
export { EmptyFolders } from './svg/empty-folders';
export { EmptyMaterials } from './svg/empty-materials';
export { EmptyTrash } from './svg/empty-trash';
export { ExternalLink } from './svg/external-link';
export { EyeOff } from './svg/eye-off';
export { Eye } from './svg/eye';
export { Facebook } from './svg/facebook';
export { FacebookFilled } from './svg/facebook-filled';
export { Factory } from './svg/factory';
export { FastForward } from './svg/fast-forward';
export { Feather } from './svg/feather';
export { File } from './svg/file';
export { FileDocx } from './svg/file-docx';
export { FileJpg } from './svg/file-jpg';
export { FileMinus } from './svg/file-minus';
export { FileOther } from './svg/file-other';
export { FilePdf } from './svg/file-pdf';
export { FilePlus } from './svg/file-plus';
export { FilePng } from './svg/file-png';
export { FileText } from './svg/file-text';
export { FileThreedm } from './svg/file-threedm';
export { FileXlsx } from './svg/file-xlsx';
export { Film } from './svg/film';
export { Filter } from './svg/filter';
export { Flag } from './svg/flag';
export { Folder } from './svg/folder';
export { Github } from './svg/github';
export { Gitlab } from './svg/gitlab';
export { Globe } from './svg/globe';
export { Grid } from './svg/grid';
export { HardwareMaterialType } from './svg/hardware-material-type';
export { Hash } from './svg/hash';
export { Headphones } from './svg/headphones';
export { Heart } from './svg/heart';
export { HelpCircle } from './svg/help-circle';
export { Home } from './svg/home';
export { Image } from './svg/image';
export { Inbox } from './svg/inbox';
export { Info } from './svg/info';
export { InsoleMaterialType } from './svg/insole-material-type';
export { Instagram } from './svg/instagram';
export { Italic } from './svg/italic';
export { LaceMaterialType } from './svg/lace-material-type';
export { LastMaterialType } from './svg/last-material-type';
export { Layers } from './svg/layers';
export { Layout } from './svg/layout';
export { LeatherMaterialType } from './svg/leather-material-type';
export { LifeBuoy } from './svg/life-buoy';
export { Link2 } from './svg/link-2';
export { Link } from './svg/link';
export { List } from './svg/list';
export { Loader } from './svg/loader';
export { Lock } from './svg/lock';
export { LogIn } from './svg/log-in';
export { LogOut } from './svg/log-out';
export { Mail } from './svg/mail';
export { MapPin } from './svg/map-pin';
export { Map } from './svg/map';
export { Material } from './svg/material';
export { Maximize2 } from './svg/maximize-2';
export { Maximize } from './svg/maximize';
export { Menu } from './svg/menu';
export { MessageCircle } from './svg/message-circle';
export { MessageSquare } from './svg/message-square';
export { MessengerFilled } from './svg/messenger-filled';
export { MicOff } from './svg/mic-off';
export { Mic } from './svg/mic';
export { Minimize2 } from './svg/minimize-2';
export { Minimize } from './svg/minimize';
export { MinusCircle } from './svg/minus-circle';
export { MinusSquare } from './svg/minus-square';
export { Minus } from './svg/minus';
export { MoldTreatment } from './svg/mold-treatment';
export { MoldMaterialType } from './svg/mold-material-type';
export { Monitor } from './svg/monitor';
export { Moon } from './svg/moon';
export { MoreHorizontal } from './svg/more-horizontal';
export { MoreVertical } from './svg/more-vertical';
export { Move } from './svg/move';
export { Music } from './svg/music';
export { Navigation2 } from './svg/navigation-2';
export { Navigation } from './svg/navigation';
export { Octagon } from './svg/octagon';
export { OutsoleMaterialType } from './svg/outsole-material-type';
export { Package } from './svg/package';
export { Paperclip } from './svg/paperclip';
export { PauseCircle } from './svg/pause-circle';
export { Pause } from './svg/pause';
export { Percent } from './svg/percent';
export { PhoneCall } from './svg/phone-call';
export { PhoneForwarded } from './svg/phone-forwarded';
export { PhoneIncoming } from './svg/phone-incoming';
export { PhoneMissed } from './svg/phone-missed';
export { PhoneOff } from './svg/phone-off';
export { PhoneOutgoing } from './svg/phone-outgoing';
export { Phone } from './svg/phone';
export { PieChart } from './svg/pie-chart';
export { PlayCircle } from './svg/play-circle';
export { Play } from './svg/play';
export { PlusCircle } from './svg/plus-circle';
export { PlusSquare } from './svg/plus-square';
export { Plus } from './svg/plus';
export { Pocket } from './svg/pocket';
export { Power } from './svg/power';
export { Printer } from './svg/printer';
export { Radio } from './svg/radio';
export { RefreshCcw } from './svg/refresh-ccw';
export { RefreshCw } from './svg/refresh-cw';
export { Repeat } from './svg/repeat';
export { Rewind } from './svg/rewind';
export { RotateCcw } from './svg/rotate-ccw';
export { RotateCw } from './svg/rotate-cw';
export { Save } from './svg/save';
export { Scissors } from './svg/scissors';
export { Search } from './svg/search';
export { Season } from './svg/season';
export { Server } from './svg/server';
export { Settings } from './svg/settings';
export { Share2 } from './svg/share-2';
export { Share } from './svg/share';
export { Shield } from './svg/shield';
export { ShoppingCart } from './svg/shopping-cart';
export { Shuffle } from './svg/shuffle';
export { Sidebar } from './svg/sidebar';
export { SkipBack } from './svg/skip-back';
export { SkipForward } from './svg/skip-forward';
export { Skype } from './svg/skype';
export { Slack } from './svg/slack';
export { Slash } from './svg/slash';
export { Sliders } from './svg/sliders';
export { Smartphone } from './svg/smartphone';
export { Speaker } from './svg/speaker';
export { Square } from './svg/square';
export { Star } from './svg/star';
export { StopCircle } from './svg/stop-circle';
export { Sun } from './svg/sun';
export { Sunrise } from './svg/sunrise';
export { Sunset } from './svg/sunset';
export { Sustainability } from './svg/sustainability';
export { SyntheticMaterialType } from './svg/synthetic-material-type';
export { Tablet } from './svg/tablet';
export { Tag } from './svg/tag';
export { Target } from './svg/target';
export { TextilesKnitMaterialType } from './svg/textiles-knit-material-type';
export { TextilesWovenMaterialType } from './svg/textiles-woven-material-type';
export { Thermometer } from './svg/thermometer';
export { ThreadAndYarnMaterialType } from './svg/thread-and-yarn-material-type';
export { ThumbsDown } from './svg/thumbs-down';
export { ThumbsUp } from './svg/thumbs-up';
export { ToggleLeft } from './svg/toggle-left';
export { ToggleRight } from './svg/toggle-right';
export { Trash2 } from './svg/trash-2';
export { Trash } from './svg/trash';
export { TrendingDown } from './svg/trending-down';
export { TrendingUp } from './svg/trending-up';
export { Triangle } from './svg/triangle';
export { Tv } from './svg/tv';
export { Twitter } from './svg/twitter';
export { TwitterFilled } from './svg/twitter-filled';
export { Type } from './svg/type';
export { Umbrella } from './svg/umbrella';
export { Underline } from './svg/underline';
export { Unlock } from './svg/unlock';
export { UploadCloud } from './svg/upload-cloud';
export { Upload } from './svg/upload';
export { UserCheck } from './svg/user-check';
export { UserMinus } from './svg/user-minus';
export { UserPlus } from './svg/user-plus';
export { UserX } from './svg/user-x';
export { User } from './svg/user';
export { Users } from './svg/users';
export { VideoOff } from './svg/video-off';
export { Video } from './svg/video';
export { Visuals } from './svg/visuals';
export { Voicemail } from './svg/voicemail';
export { Volume1 } from './svg/volume-1';
export { Volume2 } from './svg/volume-2';
export { VolumeX } from './svg/volume-x';
export { Volume } from './svg/volume';
export { Watch } from './svg/watch';
export { WebbingRibbonMaterialType } from './svg/webbing-ribbon-material-type';
export { WifiOff } from './svg/wifi-off';
export { Wifi } from './svg/wifi';
export { Wind } from './svg/wind';
export { XCircle } from './svg/x-circle';
export { XSquare } from './svg/x-square';
export { X } from './svg/x';
export { Year } from './svg/year';
export { Zap } from './svg/zap';
export { ZipperMaterialType } from './svg/zipper-material-type';
export { ZoomIn } from './svg/zoom-in';
export { ZoomOut } from './svg/zoom-out';

export { allIcons } from './all-icons';
