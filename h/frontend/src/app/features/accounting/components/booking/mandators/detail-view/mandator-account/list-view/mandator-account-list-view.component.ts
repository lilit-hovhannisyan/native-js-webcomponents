import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NvGridConfig } from 'nv-grid';
import { GridConfigService } from 'src/app/core/services/grid-config.service';
import { DefaultModalOptions } from 'src/app/core/constants/modalOptions';
import { IMandatorAccountRow, IMandatorAccountFull } from 'src/app/core/models/resources/IMandatorAccount';
import { MandatorAccountMainService } from 'src/app/core/services/mainServices/mandator-account-main.service';
import { MandatorAccountRepositoryService } from 'src/app/core/services/repositories/mandator-account-repository.service';
import { Language } from 'src/app/core/models/language';
import { getGridConfig } from 'src/app/core/constants/nv-grid-configs/mandator-account.config';
import { wording } from 'src/app/core/constants/wording/wording';
import { BaseComponent } from 'src/app/core/abstracts/BaseComponent';
import { BaseComponentService } from 'src/app/core/services/base-component.service';
import { MandatorAccountCreateViewComponent } from '../detail-view/mandator-account-create-view/mandator-account-create-view.component';
import { MandatorAccountEditViewComponent } from '../detail-view/mandator-account-edit-view/mandator-account-edit-view.component';
import { ACL_ACTIONS } from 'src/app/authentication/services/authentication.service';

@Component({
  selector: 'nv-mandator-account-list-view',
  templateUrl: './mandator-account-list-view.component.html',
  styleUrls: ['./mandator-account-list-view.component.scss']
})
export class MandatorAccountListViewComponent extends BaseComponent implements OnInit {

  public mandatorId: number;
  public mandatorAccountGridConfig: NvGridConfig;
  public dataSource$: Observable<IMandatorAccountRow[]>;
  private modalRef?: NzModalRef;
  private language: Language;

  constructor(
    baseComponentService: BaseComponentService,
    private mandatorAccountMainService: MandatorAccountMainService,
    private mandatorAccountRepo: MandatorAccountRepositoryService,
    public gridConfigService: GridConfigService,
    public route: ActivatedRoute,
    private modalService: NzModalService,
  ) {
    super(baseComponentService);
  }

  public ngOnInit(): void {
    this.language = this.settingsService.language;
    this.mandatorId = parseInt(this.route.parent.snapshot.params.id, 10);

    this.dataSource$ = this.mandatorAccountMainService
      .getStream({ mandatorId: this.mandatorId });

    const actions = {
      add: () => this.openDetailViewModal(
        MandatorAccountCreateViewComponent,
        {
          mandatorId: this.mandatorId,
        },
        wording.accounting.bookingAccountsTitle[this.language]
      ),
      edit: (mandatorAccount: IMandatorAccountFull) => this.openDetailViewModal(
        MandatorAccountEditViewComponent,
        {
          mandatorId: this.mandatorId,
          canEdit: this.authService.canPerformOperationOnCurrentUrl(ACL_ACTIONS.UPDATE),
          mandatorAccount,
        },
        `${mandatorAccount.no} ${mandatorAccount.displayLabel[this.language]}`,
      ),
      delete: (mandatorAccount: IMandatorAccountFull) =>
        this.deleteRowClicked(mandatorAccount)
    };

    const canPerform = (operation: number) => this.authService.canPerformOperationOnCurrentUrl(operation);
    this.mandatorAccountGridConfig = getGridConfig(actions, canPerform);
  }

  public openDetailViewModal(component: any, componentParams: any, title: string): void {
    this.modalRef = this.modalService.create({
      ...DefaultModalOptions,
      nzTitle: title,
      nzContent: component,
      nzMaskClosable: true,
      nzComponentParams: {
        ...componentParams,
        closeModal: () => this.modalRef.destroy()
      },
      nzFooter: null
    });
  }

  public deleteRowClicked = (mandatorAccount: IMandatorAccountFull): void => {
    this.confirmationService.confirm({
      okButtonWording: wording.general.delete,
      cancelButtonWording: wording.general.cancel,
      wording: wording.general.areYouSureYouWantToDelete,
      subjects: { subject: mandatorAccount.displayLabel },
    }).subscribe(confirmed => confirmed && this.mandatorAccountRepo.delete(mandatorAccount.id, {}).subscribe());
  }
}
