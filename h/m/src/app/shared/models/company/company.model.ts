import { Type } from 'class-transformer';
import { CompanyAttributesModel } from './company-attributes.model';
import { CompanyAttributesExtendedModel } from './company-attributes-extended.model';
import { BaseModel } from '../base-model/base-model';

export class CompanyModel extends BaseModel {
  domainName: string = '';
  oid: string = '';
  @Type(() => CompanyAttributesModel)
  attributes: CompanyAttributesModel = new CompanyAttributesModel();
  @Type(() => CompanyAttributesExtendedModel)
  attributesExtended: CompanyAttributesExtendedModel = new CompanyAttributesExtendedModel();
  creatorOID: string = '';
  ownerOID: string = '';
}
