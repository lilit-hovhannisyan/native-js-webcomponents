import { Injectable } from '@angular/core';
import { AbstractRepository, RepoParams } from '../../abstracts/abstract.repository';
import { RepositoryService } from '../repository.service';
import { IAutobankBusinessCase, IAutobankBusinessCaseDTO, IAutobankBusinessCaseCreate, AutobankBusinessCaseKey } from '../../models/resources/IAutobankBusinessCase';
@Injectable({
  providedIn: 'root'
})
export class AutobankBusinessCaseRepositoryService extends AbstractRepository<
IAutobankBusinessCase,
IAutobankBusinessCaseDTO,
IAutobankBusinessCaseCreate,
RepoParams<IAutobankBusinessCase>,
AutobankBusinessCaseKey
>  {
  protected resourceType = 'AutobankBusinessCase';
  public sortAsce = true;
  public sortKey = 'gvc';

  constructor(repositoryService: RepositoryService) {
    super(repositoryService);
  }

  protected postProcess = (item: IAutobankBusinessCaseDTO): IAutobankBusinessCase => item;

  public getBaseUrl = () => '/api/autobank/business-cases';
}
