import { IWithActive } from './IWithActive';
import { IResource } from './IResource';
import { IWording } from './IWording';
import { ISalutation } from './ISalutation';
import { ITitle } from './ITitle';
import { COMPANY_LEGAL_FORM, COMPANY_LEGAL_FORM_TYPE } from '../enums/company-legal-form';
import { IBusinessName } from './IBusinessName';
import { CompanyGroupKey } from './ICompanyGroup';

export type CompanyKey = number;

interface ISharedProps extends IWithActive {
  firstName: string;
  lastName: string;
  companyName: string;
  taxNo: string;
  vatId: string;
  logoId?: string;
  salutationId: number;
  titleId: number;
  isPrivatePerson: boolean;
  legalForm: COMPANY_LEGAL_FORM;
  legalFormType: COMPANY_LEGAL_FORM_TYPE;
  isLegalDepartmentResponsible: boolean;
  groupOfCompanyId: CompanyGroupKey;
  businessNames: IBusinessName[];
}

export interface ICompanyBase extends ISharedProps, IResource<CompanyKey> { }

export interface ICompanyCreate extends ISharedProps { }

export interface ICompanyDTO extends ICompanyBase { }

export interface ICompany extends ICompanyDTO { }

export interface ICompanyFull extends ICompany {
  salutation?: ISalutation;
  title?: ITitle;
}

export interface ICompanyRow extends ICompanyFull {
  salutationLabel?: IWording;
  titleLabel?: IWording;
}
