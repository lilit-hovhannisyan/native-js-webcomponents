export interface ResetPassword {
    newPassword: string;
    userId: number;
    ConfirmationCode: string;
}
