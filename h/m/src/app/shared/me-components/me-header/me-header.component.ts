import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, ViewChild, OnDestroy } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';

import { Subscription, fromEvent } from 'rxjs';

import { WindowRef } from '@core-providers/window-ref.provider';

import { MeHeaderI } from './me-header.interface';
@Component({
  selector: 'me-header',
  templateUrl: './me-header.component.html',
  styleUrls: ['./me-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MeHeaderComponent implements OnDestroy {
  isInfoVisible: boolean = false;
  isUserMenuVisible: boolean = false;

  @Input() dataModel!: MeHeaderI;
  @Output() goToNotificationsEvent: EventEmitter<void> = new EventEmitter();
  @Output() goToMessagessEvent: EventEmitter<void> = new EventEmitter();
  @Output() goToUserProfileEvent: EventEmitter<void> = new EventEmitter();
  @Output() openUserGuideEvent: EventEmitter<void> = new EventEmitter();
  @Output() logOutEvent: EventEmitter<void> = new EventEmitter();
  @Output() helpMenuItemClickEvent: EventEmitter<string> = new EventEmitter();

  _clickTrackSubscription!: Subscription;
  activeTrigger!: MatMenuTrigger | null;
  activatorElement!: HTMLElement;
  @ViewChild('infoTrigger', { read: MatMenuTrigger }) infoTriggerObj!: MatMenuTrigger;
  @ViewChild('userTrigger', { read: MatMenuTrigger }) userTriggerObj!: MatMenuTrigger;

  constructor(private windowRef: WindowRef) {}

  checkToClose(event: Event): void {
    if (this.activeTrigger) {
      let caller: HTMLElement | null = event.target as HTMLElement;
      while (caller && caller.tagName !== 'BODY') {
        if (caller.classList.contains('cdk-overlay-pane') || this.activatorElement === caller) {
          return;
        }
        caller = caller.parentElement;
      }

      this.activeTrigger.closeMenu();
      this.activeTrigger = null;
      this._clickTrackSubscription.unsubscribe();
    }
  }

  startTrackClicks(activeTrigger: MatMenuTrigger, activatorElement: HTMLElement): void {
    // this event is called first and we have to call check if manu is active
    if (this.activeTrigger) {
      this.checkToClose({ target: activatorElement as EventTarget } as Event);
      // case of closing menu with same activatorElement (button) should not clear current trigger
      if (this.activeTrigger) {
        this.activeTrigger = null;
        this._clickTrackSubscription.unsubscribe();
      }
    }

    this.activeTrigger = activeTrigger;
    this.activatorElement = activatorElement;
    this._clickTrackSubscription = fromEvent(this.windowRef.nativeWindow, 'click').subscribe((value: Event) => {
      this.checkToClose(value);
    });
  }

  // goToNotifications(): void {
  //   this.goToNotificationsEvent.emit();
  // }

  // goToMessages(): void {
  //   this.goToMessagessEvent.emit();
  // }

  goToUserProfile(): void {
    this.goToUserProfileEvent.emit();
  }

  openUserGuide(): void {
    this.openUserGuideEvent.emit();
  }

  logOut(): void {
    this.logOutEvent.emit();
  }

  helpMenuItemClick(key: string): void {
    this.helpMenuItemClickEvent.emit(key);
  }

  itemKey(_index: number, item: { key: string }): string {
    return item.key;
  }

  ngOnDestroy(): void {
    if (this._clickTrackSubscription) {
      this._clickTrackSubscription.unsubscribe();
    }
  }
}
